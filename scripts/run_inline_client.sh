#!/bin/sh

HYI_CREAM_HOME=/home/hyi/cream

# Classpath for Cream
CLASSPATH="${HYI_CREAM_HOME}/cream.jar:${HYI_CREAM_HOME}/conf"
for n in ${HYI_CREAM_HOME}/lib/*.jar ; do
  CLASSPATH="${CLASSPATH}:${n}"
done

SC_IP=`echo "select value from property where name='SCIPAddress'"|psql -t cream|head -1`
MAIN_CLASS=hyi.cream.inline.Client

/usr/bin/java -Djava.library.path=${HYI_CREAM_HOME} -cp $CLASSPATH ${MAIN_CLASS} ${SC_IP} 3000
