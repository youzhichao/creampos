#!/bin/sh
#
# Script for run CreamPOS.
#
export LANG=zh_TW.UTF-8
cd /home/hyi/cream

sudo chmod a+rw /dev/ttyS*

# Cream POS CLASSPATH settings --

HYI_CREAM_HOME=/home/hyi/cream

# Classpath for Cream --

if test -n "$CLASSPATH" ; then
  CLASSPATH="${HYI_CREAM_HOME}/cream.jar:${HYI_CREAM_HOME}/conf:${CLASSPATH}"
else
  CLASSPATH="${HYI_CREAM_HOME}/cream.jar:${HYI_CREAM_HOME}/conf"
fi

for n in ${HYI_CREAM_HOME}/lib/*.jar ; do
  CLASSPATH="${n}:${CLASSPATH}"
done

# Upgrade cream --

if [ -f cream.jar.pack.gz ]; then
  /usr/bin/unpack200 cream.jar.pack.gz cream.jar.hot
  /bin/mv -f cream.jar.pack.gz cream.jar.pack.gz.`date +%Y-%m-%d`
fi

if [ -f cream.jar.hot ]; then
  /bin/mv -f cream.jar cream.jar.`date +%Y-%m-%d` 
  /bin/mv -f cream.jar.hot cream.jar
fi

# Run Cream POS --

export MAIN_CLASS=hyi.cream.POSTerminalApplication
/usr/bin/java -Djava.library.path=${HYI_CREAM_HOME} -Dfile.encoding=UTF-8 -cp $CLASSPATH ${MAIN_CLASS} \
  -fullscreen > stdout.log 2> stderr.log
