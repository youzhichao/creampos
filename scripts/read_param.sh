#!/bin/sh
# read_param.sh
#   Script to query value from property.
#   This script is able to split property.value into lines by delimiter '\n' in its value.
# By: Bruce You 2009/06/04

psql -U postgres cream -t -c \
  "select value from property where name='$1'" | \
  sed 's!^ !!g' | sed 's!\\n!\
!g'
