#!/bin/bash
cd /home/hyi/cream
POSNO=$(psql -U postgres cream -t -c "select value from property where name='TerminalNumber'")
MAXZ=$(psql -U postgres cream -t -c "select max(eodcnt) from z")
read MAXZ <<< "$MAXZ" 
echo z=$MAXZ  > /home/hyi/cream/balanced.log
if [ ! $MAXZ ]; then
  exit
fi

VALUE1=$(psql -U postgres cream -t -c "\
  SELECT SUM(amount) FROM zex WHERE posno=${POSNO} AND eodcnt=${MAXZ} AND \
  (code LIKE 'PAYAMT_%' OR code LIKE 'RTNAMT_%')")
read VALUE1 <<< "$VALUE1" 
if [ ! $VALUE1 ]; then
  VALUE1=0
fi
echo value1=$VALUE1 >> /home/hyi/cream/balanced.log

VALUE2=$(psql -U postgres cream -t -c "\
  (SELECT round(sum(netsaletotalamount), 0) FROM depsales \
  WHERE posnumber=${POSNO} AND sequencenumber=${MAXZ})")
read VALUE2 <<< "$VALUE2" 
if [ ! $VALUE2 ]; then
  VALUE2=0
fi
echo value2=$VALUE2 >> /home/hyi/cream/balanced.log

if [ $VALUE1 -eq $VALUE2 ]; then  
   echo Balanced.
else
   echo Not balanced. >> /home/hyi/cream/balanced.log
   python ReportFatalError.py "DEPSALES AND ZEX PAYMENTS ARE NOT BALANCED!"
fi
  
#BALANCED=$(psql -t -c "\
#  select true where \
#  (SELECT SUM(amount) FROM zex WHERE posno=${POSNO} AND eodcnt=${MAXZ} AND \
#  (code LIKE 'PAYAMT_%' OR code LIKE 'RTNAMT_%')) = \
#  (SELECT sum(netsaletotalamount) FROM depsales \
#  WHERE posnumber=${POSNO} AND sequencenumber=${MAXZ})")
#if [ $MAXZ -a $BALANCED -a $BALANCED = 't' ]; then
#   echo Balanced.
#else
#   echo Not balanced.
#   python ReportFatalError.py "DEPSALES AND ZEX PAYMENTS ARE NOT BALANCED!"
#fi
