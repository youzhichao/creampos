#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <sys/io.h>

JNIEXPORT jchar JNICALL Java_hyi_jpos_services_CashDrawerLocal_opencashdrawer (JNIEnv* env, jclass cl)
{
if ( iopl( 3 ) < 0 )
{
        perror( "iopl()" );
	fprintf( stderr, "This program must be run as root\n" );
	return( EXIT_FAILURE );
}
outb(inb(0x310)|(1<<6), 0x310);
}
			
			
JNIEXPORT jint JNICALL Java_hyi_jpos_services_CashDrawerLocal_getdrawerstate (JNIEnv* env, jclass cl)
{
int bb,b;
if ( iopl( 3 ) < 0 )
{
	perror( "iopl()" );
	fprintf( stderr, "This program must be run as root\n" );
	return( EXIT_FAILURE );
}
bb = inb(0x310);
b = bb & 64;
return b;
}
