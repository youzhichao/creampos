// setnumlock.c
//
// gcc -I/usr/X11R6/include -L/usr/X11R6/lib -o setnumlock setnumlock.c -lX11 -lXtst

#include <X11/extensions/XTest.h>
#include <X11/keysym.h>

int main(void) {
        Display* disp = XOpenDisplay(NULL);
        if (disp == NULL)
                return 1;
        XTestFakeKeyEvent(disp, XKeysymToKeycode( disp, XK_Num_Lock), True, CurrentTime);
        XTestFakeKeyEvent(disp, XKeysymToKeycode( disp, XK_Num_Lock), False, CurrentTime);
        XCloseDisplay(disp);
        return 0;
}
