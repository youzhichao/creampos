/*
 * Compiling command for cashdrawer_a10:
 *    gcc cashdrawer_toshiba_a10.c -o cashdrawer_a10
 */

//#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <sys/io.h>

#include <linux/kernel.h> /* printk() */
#include <linux/poll.h>   /* copy_to_user() */
#include <linux/ioctl.h>

////////////////////////////////////////////////////////////////////////////////
//    Local Struct Declaration
////////////////////////////////////////////////////////////////////////////////
typedef struct _BRUGGECONTROLVALUE {
    unsigned char    cdsen;              // 抽屜状態比較用
    unsigned char    open_anddata;       // 抽屜Open時And値
    unsigned char    open_ordata;        // 抽屜Open時Or値
    unsigned char    close_anddata;      // 抽屜Close時And値
    unsigned char    close_ordata;       // 抽屜Close時Or値
    unsigned char    defopen;            // Port値無視時抽屜Open値
    unsigned char    defclose;           // Port値無視時抽屜Close値
} BRUGGECONTROLVALUE , *PBRUGGECONTROLVALUE ;


BRUGGECONTROLVALUE outval[3] =
{
    { 0x01, 0xEF, 0x40, 0xBF, 0x10, 0x40, 0x10},   // Drawer1
    { 0x02, 0xDF, 0x80, 0x7F, 0x20, 0x80, 0x20},   // Drawer2
    { 0x03, 0xCF, 0xC0, 0x3F, 0x30, 0xC0, 0x30}    // Drawer1 and Drawer2
};

//==========================================================
//        Open Status 
//==========================================================
#define   DRW1_OPN            0x00000001        // Drawer1 Open
#define   DRW2_OPN            0x00000002        // Drawer2 Open
#define   DRW1and2_OPN        0x00000003        // Drawer1 and Drawer2 Open

static int  IoBase = 0x448;                 // Port address


//JNIEXPORT jchar JNICALL Java_hyi_jpos_services_ToshibaA10CashDrawerNative_openCashDrawer(JNIEnv* env, jclass cl, jint no)
//{
//    OpenDrawer(no);
//}
//
//
//JNIEXPORT jint JNICALL Java_hyi_jpos_services_ToshibaA10CashDrawerNative_getDrawerState(JNIEnv* env, jclass cl, jint no)
//{
//    return GetDrawerStatus();
//}


////////////////////////////////////////////////////////////////////////////////
//
//    FUNCTION    :    DrawerOpenforBrugge
//
//    RESULT        :    OK:0    NG: < 0
//    MEMO        :    Brugge用drawer open
//
////////////////////////////////////////////////////////////////////////////////
int OpenDrawer(int drawer_number)
{
    char    busy;
    char    sts;
    int     cnt;
    int     timeval;
    int     ret = 0;
    char    datval;

    if (iopl(3) < 0) {
        perror("iopl()");
        fprintf(stderr, "This program must be run as root\n");
        return EXIT_FAILURE;
    }

    do {
        // 檢查參數
        if (drawer_number != DRW1_OPN && drawer_number != DRW2_OPN && drawer_number != DRW1and2_OPN) {
            printf("DRAWER STATUSWRITE PARAMETER Error!!\n");
            ret = -EFAULT;
            break;
        }

        // read実施時は一旦Portを読み込んで読み込んだ値に対してBIT操作を行う
        // ドロワーのBusyチェック
        busy = inb(IoBase);
        if (busy & 0x80) {
            printf("DRAWER STATUSWRITE Drawer is Busy!!\n");
            break;
        }

        datval = inb(IoBase+5);

        datval &= outval[drawer_number-1].open_anddata;
        datval |= outval[drawer_number-1].open_ordata;

        // 打開抽屜
        outb(datval, IoBase + 5);
        printf("DRAWER open sent!!\n");

        // // 100ms以上待つ必要があるので、約200ms待つ
        // // HZはシステムデファインで約1秒を表すので1/5で約200ms
        // // ドライバなどSleepを使いにくい場合はこのscheduleを使用するべきだそうだ
        // // jiffiesはPC起動からの経過時間(単位は不明)
        // // jiffiesに200msを足した時間timevalを作り、jiffiesがtimevalになるまで待つ
        // // schedule()実行中は制御は他に移っているので(アイドル状態になるとのこと)他のドライバ等は動作可能
        // // このドライバは同期でしか動作しないので、drw_ioctlが抜けなくても問題ない
        // timeval = jiffies + (HZ / 5);
        // while (jiffies<timeval)
        // {
        //     schedule();
        // }
        usleep(1000000 / 5);

        // Open時同様、Closeするための値を決定する
        datval &= outval[drawer_number-1].close_anddata;
        datval |= outval[drawer_number-1].close_ordata;

        // ドロワーをCloseする
        // このドロワーは自動でPort状態がClose状態に戻らないので設定してあげる必要がある
        // ドロワーをCloseすると言っても、閉じる訳ではなく、Portの状態をCloseにすると言う事
        outb(datval, IoBase + 5);

        // ドロワーが開いたか確認する
        // 1秒(100ms×10回)実施する
        for (cnt = 0 ; cnt < 10 ; cnt++) {
            // ドロワーの状態をチェック
            sts = inb(IoBase+1);
            // 開いているならばチェック終了、開いていなければ100ms待ってリトライ
            if (sts & outval[drawer_number-1].cdsen)
            {
                break;
            }
            // // 100ms待つ
            // timeval = jiffies + (HZ / 10);
            // while (jiffies<timeval)
            // {
            //     schedule();
            // }
            usleep(1000000 / 10);
        }
        // 1秒待っても開いていなければエラー
        if (cnt > 10) {
            printf("DRAWER STATUSWRITE OPEN1 Error!!\n");
        }
    } while (0);

    return (ret);
}

int GetDrawerStatus() {

    if (iopl(3) < 0) {
        perror("iopl()");
        fprintf(stderr, "This program must be run as root\n");
        return EXIT_FAILURE;
    }

    int status = inb(IoBase+1);	// #5 Add
    // Bruggeの場合、BIT位置、BIT方向がioctlの戻りとPortの値と同じなのでマスクするだけ
    status &= 0x03;			// #5 Add

    //printf("Status=%d\n", status);
    return status == 3 ? 1 : 0;
}

void help() {
    printf("Toshiba Drawer Utility. Copyright(c) 2009, Hongyuan Software(Shanghai).\n");
    printf("Usage:\n");
    printf("   cashdrawer_a10 open [n]\n");
    printf("   cashdrawer_a10 status\n");
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        help();
        return 1;
    }

    if (strcmp(argv[1], "open") == 0) {
        if (argc < 3) {
            help();
            return 1;
        }
        return OpenDrawer(atoi(argv[2]));

    } else if (strcmp(argv[1], "status") == 0) {
       int opened = GetDrawerStatus();
       printf("Drawer is %sopend.\n", opened == 1 ? "" : "not ");
       return opened;
    }
    return 1;
}
