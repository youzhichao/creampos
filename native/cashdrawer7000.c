#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <sys/io.h>

#define IO_PORT 0x146
#define DRW1_OPEN 1
#define DRW2_OPEN 2

JNIEXPORT jchar JNICALL Java_hyi_jpos_services_CashDrawer7000Local_openCashDrawer(JNIEnv* env, jclass cl, jint no)
{
    if (iopl(3) < 0)
    {
        perror("iopl()");
	fprintf(stderr, "This program must be run as root\n");
	return(EXIT_FAILURE);
    }
    if (no == 1)
        outb(0x01, IO_PORT);
    else
        outb(0x02, IO_PORT);
}


JNIEXPORT jint JNICALL Java_hyi_jpos_services_CashDrawer7000Local_getDrawerState(JNIEnv* env, jclass cl, jint no)
{
    int bb,b;

    if (iopl(3) < 0)
    {
	perror("iopl()");
	fprintf(stderr, "This program must be run as root\n");
	return(EXIT_FAILURE);
    }
    sleep(1);
    bb = inb(IO_PORT);
    return bb;
}

JNIEXPORT jchar JNICALL test(jint no)
{
    if (iopl(3) < 0)
    {
        perror("iopl()");
	fprintf(stderr, "This program must be run as root\n");
	return(EXIT_FAILURE);
    }
    printf("Before-%x\n", inb(IO_PORT));
    //outb((inb(IO_PORT) & 0x04) | (DRW1_OPEN << 6), IO_PORT);
    if (no == 1)
        outb(0x01, IO_PORT);
    else
        outb(0x02, IO_PORT);
    sleep(3);
    printf("After-%x\n", inb(IO_PORT));
}

main(int argc, char* argv[])
{
    //Java_com_tec_jpos_devices_St7000CashDrawerDevice_openCashDrawer(NULL, NULL);
    if (argc == 2)
        test(atoi(argv[1]));
}
