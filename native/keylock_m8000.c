/*
 * Compiling command for shared library of JNI:
 *   gcc -o libkeylock_m8000.so -shared -Wl,-soname,libkeylock_m8000.so -I${JAVA_HOME}/include
 *     -I${JAVA_HOME}/include/linux keylock_m8000.c -static -lc
 *
 * Compiling command for testing with main():
 *   gcc -o keylock_m8000 -I${JAVA_HOME}/include -I${JAVA_HOME}/include/linux keylock_m8000.c
 *
 */
#include <jni.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/msg.h>

#define MQ_KEY 0xABCDEF00

struct Msg {
    long msgtyp;
    int keylock_pos;
};

int mq_id = -1;
struct Msg msg;

JNIEXPORT jint JNICALL Java_hyi_jpos_services_TecM8000Keylock_waitForKeylockEvent(JNIEnv* env, jclass cl)
{
    if (mq_id == -1) {
        mq_id = msgget(MQ_KEY, 0666 | IPC_CREAT);
        if (mq_id == -1) {
            printf("Cannot open message queue.\n");
            return -1;
        }
    }
    msg.msgtyp = 1;

    // Blocking read message sent from m8000_keylock_emitter
    if (-1 == msgrcv(mq_id, &msg, sizeof(int), 0, 0)) {
        printf("Cannot receive data from  message queue.\n");
        return -1;
    }
    return msg.keylock_pos;
}

JNIEXPORT jint JNICALL Java_hyi_spos_services_TecM8000Keylock_waitForKeylockEvent(JNIEnv* env, jclass cl)
{
    if (mq_id == -1) {
        mq_id = msgget(MQ_KEY, 0666 | IPC_CREAT);
        if (mq_id == -1) {
            printf("Cannot open message queue.\n");
            return -1;
        }
    }
    msg.msgtyp = 1;

    // Blocking read message sent from m8000_keylock_emitter
    if (-1 == msgrcv(mq_id, &msg, sizeof(int), 0, 0)) {
        printf("Cannot receive data from  message queue.\n");
        return -1;
    }
    return msg.keylock_pos;
}

/*
main(int argc, char* argv[])
{
    while (1) {
        jint p = Java_hyi_jpos_services_TecM8000Keylock_waitForKeylockEvent(NULL, NULL);
        printf("Receive keylock pos = %d\n", p);
    }
}
*/
