// gcc -o liblinedisplay.so -shared linedisplay.c

#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <sys/io.h>
#include "linedisplay.h"

#define REG_RX          port
#define REG_TX          port
#define REG_IENA        port+1
#define REG_ISTAT       port+2
#define REG_LC          port+3
#define REG_MC          port+4
#define REG_LS          port+5
#define REG_MS          port+6
#define REG_SPAD        port+7
#define REG_BORL        port
#define REG_BORH        port+1

#define COM1    0x3f8
#define COM2    0x2f8
#define COM3    0x3e8
#define COM4    0x270
#define COM5    0x128
#define COM6    0x2f0
#define COM8    0x200

int port=COM8;

JNIEXPORT void JNICALL Java_hyi_jpos_services_LineDisplayLocal_ini(JNIEnv* env, jclass cl)
{
    if ( iopl( 3 ) < 0 )
    {
        perror( "iopl()" );
        fprintf( stderr, "This program must be run as root\n" );
        return;
    }
    outb(0x80,REG_LC);
    outb(12,REG_BORL);
    outb(0,REG_BORH);
    outb(0,REG_LC);
    outb(0,REG_IENA);
    outb(0x0b,REG_LC);
    outb(0x0b,REG_MC);
    inb(REG_RX);
    inb(REG_ISTAT);
    inb(REG_LS);
    inb(REG_MS);
}

JNIEXPORT jint JNICALL Java_hyi_jpos_services_LineDisplayLocal_show(JNIEnv* env, jclass cl, jbyteArray b, jint ii)
{     
    int i,j,wk;
    const jbyteArray *str; 
    str = (*env)->GetByteArrayElements(env, b, JNI_FALSE);

    //printf("-------------------");
    //printf(ii);    
    if (ii == 1) {
	//printf("ii == 1");
        liu_send("\033[1;1H",6);	//set cursor 1,1
    } else if (ii == 2) {
	//printf("ii == 2");
        liu_send("\033[2;1H",6);	//set cursor 1,2
    }
    //printf("send str");
    liu_send(str, strlen(str) + 1);
    (*env)->ReleaseByteArrayElements(env, b, str, 0);
}

void liu_send(const char * buf,int len)
{
    int i,wk;
     while (len) {
	 for (i=0; i<2000; i++) {
	     wk=inb_p(REG_LS);
	     if (wk&0x20) break;
	 }
	 if (i==2000) return;
	 for (i=0; i<2000; i++) {
	     wk=inb_p(REG_MS);
	     if (wk&0x10) break;
	 }
	 if (i==2000) return;
	 outb_p(*buf,REG_TX);
	 buf++; len--;
	 }
}
