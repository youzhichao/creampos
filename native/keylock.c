#include	<stdio.h>
#include	<string.h>
#include    <sys/io.h>
//#include    <asm/system.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <unistd.h>

/* BIOS address definitions */
#define BIOS_TIME	0x40006CL	// BIOS CLOCK address
#define BIOS_KB_FLAG_2	0x400097L	// KEY BOARD CONTROL FLAG 2

/* KB_FLGA_2 STATUS definitions */
#define KB_FA		0x10		// ACK received flag
#define KB_FE		0x20		// Resend(NAC) received flag
#define	KB_ERR		0x80		// keybord transmit error flag

/* KEYBOARD INTERFACE(8042) CONTROL PORT */
#define PORT_A		0x60		// 8042 KB Data Register
#define STATUS_PORT	0x64		// 8042 KB status Register

#define INPT_BUF_FULL	0x02		// 8042 Status 1:KB BUFFER FULL

//unsigned long	GetTickCount();

unsigned long	BIOS_Tick_Count;	// BIOS Tick Count
unsigned char   BIOS_Key_Status[8];	// BIOS Key Status
unsigned char   *BIOS_Key_Status2;	// BIOS Key Status

int main()
{
	//BIOS_Tick_Count  = (long _far *)BIOS_TIME;
	
	//BIOS_Key_Status2 = (unsigned char _far *)BIOS_KB_FLAG_2;
	snd_data(0xD2);
	snd_data(0x0);
}

/*unsigned long	GetTickCount()
{
	static unsigned long	save_time;
	unsigned long	now;

	_enable();
	now = 55L * BIOS_Tick_Count[0];
	if (now < save_time)	now += save_time;
	save_time = now;
	return (now);
}*/

int snd_data(dt)
unsigned char dt;
{
    //long wait_time;
    int ix,iy;
    int ly;
    unsigned char wk;
    unsigned long flags;
    off_t offset;
    ssize_t count;
	int	ret;

    /*int memfd = open("/dev/mem", O_RDWR);
    printf("010 memfd=%d  BIOS_Key_Status=%x\n", memfd, BIOS_Key_Status);
    BIOS_Key_Status2 = mmap((void*)BIOS_Key_Status, 2, PROT_READ | PROT_WRITE, MAP_FIXED, memfd, 0x400097L);
    if (BIOS_Key_Status2 == MAP_FAILED) {
        perror("mmap()");
        return;
    }*/
    if (iopl(3) < 0) {
        perror("iopl()");
        fprintf( stderr, "This program must be run as root\n" );
        return;
    }              

	for(ix = 0; ix < 3000; ix++)
		outb(0, 0xED);

    printf("020 BIOS_Key_Status2=%x\n", BIOS_Key_Status2);
    for (ix = 0; ix < 50; ix++) {
        printf("030\n");

        //_disable();
        //save_flags(flags);
        //cli();

        //BIOS_Key_Status2[0] &= 0xff ^ (KB_FE | KB_FA);
        /*offset = lseek(memfd, BIOS_KB_FLAG_2, SEEK_SET);
        printf("020 offset=%x\n", offset);
        
        count = read(memfd, BIOS_Key_Status, 1);
        printf("030 read count=%d\n", count);

        BIOS_Key_Status[0] &= 0xff ^ (KB_FE | KB_FA);
        printf("040 offset=%x\n", offset);
        
        offset = lseek(memfd, BIOS_KB_FLAG_2, SEEK_SET);
        count = write(memfd, BIOS_Key_Status, 1);
        printf("050 write count=%d\n", count);*/

        
		ly = 100000L;			// #2
		while (--ly) {
			ret = inb(STATUS_PORT) & INPT_BUF_FULL;
			if (!ret)
				break;
			outb(0, 0xED);		// #3
			outb(0, 0xED);		// #3
			outb(0, 0xED);		// #3
			outb(0, 0xED);		// #3
		}
		if(!ly) {
		    printf("inp failed");
		    return(-2);
		}
		
        iy = 30000;
        while ((inb_p(STATUS_PORT) & INPT_BUF_FULL) && --iy)
            ;
        if (!iy)
            return(-2);
        

        outb_p(dt, PORT_A);

        //_enable();
        //restore_flags(flags);        
        
        //wait_time = GetTickCount() + 100;
        //while (wait_time > GetTickCount()) {


        /*while (1) {
            usleep(10);

            //wk = BIOS_Key_Status2[0];
            offset = lseek(memfd, BIOS_KB_FLAG_2, SEEK_SET);
            printf("060 offset=%x\n", offset);
            count = read(memfd, BIOS_Key_Status, 1);
            wk = BIOS_Key_Status[0];
            printf("070 read count=%d wk=%x\n", count, wk);
            
            if (wk & (KB_FE | KB_FA)) {
                if (wk & KB_FA)
                    return(0);
                else
                    break;
            }
        }*/

        break;
    }
    //(void)munmap(BIOS_Key_Status2, 2);
    //close(memfd);
    return(-1);
}
