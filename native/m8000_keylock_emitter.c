// Keylock enabler and emitter for Toshiba TEC M-8000.
//
// Compile with: gcc -o m8000_keylock_emitter m8000_keylock_emitter.c 
//
// Bruce You @ Hongyuan Software(Shanghai), since 2010-11-03

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <sys/ioctl.h>
#include <sys/msg.h>
#include "tec_usbposkbd.h"
#include "tec_usbextkbd.h"

static int fd = -1;
static short sRet;
static unsigned int retsize = 0;
static char szBuff[16];
static char rtBuff[16];
static int snd_sz = 0;
static TEC_USBPOSKBD_IORW iorw;

#define MQ_KEY 0xABCDEF00

struct Msg {
    long msgtyp;
    int keylock_pos;
};

void send_command() {
    iorw.lpvInBuffer = szBuff;
    iorw.cbInBuffer = snd_sz;
    iorw.lpvOutBuffer = rtBuff;
    iorw.cbOutBuffer = sizeof(rtBuff);
    iorw.lpcbBytesReturned = &retsize;

    sRet = ioctl(fd, TECUSBPOSKBD_POSKEY_CONTROL, &iorw);
    if (sRet != 0)
        printf("RESULT:%d err:%d size:%ld %02x:%02x\n", sRet, errno, retsize, rtBuff[0], rtBuff[1]);
}

void key_mode() {
    memset(szBuff, 0, sizeof(szBuff));
    memset(rtBuff, 0, sizeof(rtBuff));
    snd_sz = 1;
    szBuff[0] = 0xd2;
    send_command();
}

void enable_keylock() {
    memset(szBuff, 0, sizeof(szBuff));
    memset(rtBuff, 0, sizeof(rtBuff));
    snd_sz = 2;
    szBuff[0] = 0xd5;
    szBuff[1] = 0x00;
    send_command();
}

// Get keylock position into global variable: keylock_pos
int get_keylock_pos() {
    int curr_keylock_pos = -1;

    retsize = 0;
    memset(szBuff, 0x00, sizeof(szBuff));
    iorw.lpvInBuffer = NULL;
    iorw.cbInBuffer = 0;
    iorw.lpvOutBuffer = szBuff;
    iorw.cbOutBuffer = sizeof(szBuff);
    iorw.lpcbBytesReturned = &retsize;
    sRet = ioctl(fd, TECUSBPOSKBD_READ_KEYLOCK, &iorw);
    if (sRet == 0) {
        int keylock_data = atoi(szBuff);
        curr_keylock_pos = keylock_data - 249;
        if (curr_keylock_pos < 0 || curr_keylock_pos > 9)
            curr_keylock_pos = -1;
        //printf("Keylock position: %d\n", keylock_pos);
    } else {
        curr_keylock_pos = -1;
        printf("RESULT:%d err:%d size:%ld\n", sRet, errno, retsize);    
    }
    return curr_keylock_pos;
}

main(int argc, char** argv)
{
    int keylock_pos = -1;
    struct Msg msg;

    fd = open("/dev/usbposkbd", O_RDWR);
    if (fd < 0) {
        perror("Open /dev/usbposkbd failed.");
        return -1;
    }

    key_mode();
    enable_keylock();
    sleep(2); // wait for 2 seconds
    
    int mq_id = msgget(MQ_KEY, 0666 | IPC_CREAT);
    if (mq_id == -1) {
        printf("Create message queue failed.\n");
        return -1;
    }
    msg.msgtyp = 1;

    while (1) {
        int curr_keylock_pos = get_keylock_pos();
        if (curr_keylock_pos != -1 && keylock_pos != curr_keylock_pos) {
            keylock_pos = curr_keylock_pos;
            printf("keylock pos = %d\n", keylock_pos);

            msg.keylock_pos = keylock_pos;
            if (-1 == msgsnd(mq_id, &msg, sizeof(int), 0))
                printf("Cannot send message (errno=%d).\n", errno);
        }
        usleep(300000L); // wait for 300ms
    }
}

