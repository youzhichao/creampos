#!/bin/sh
APPDIR=`dirname $0`;

SRC_DIR=$APPDIR/src
CLASS_FILE_DIR=$APPDIR/out/production/
GWT_USR_JAR=/Users/youbruce/Java/gwt-mac-1.5.1/gwt-user.jar
GWT_DEV_JAR=/Users/youbruce/Java/gwt-mac-1.5.1/gwt-dev-mac.jar

java -XstartOnFirstThread -Xmx256M -cp "$APPDIR/src:$APPDIR/bin:$GWT_USR_JAR:$GWT_DEV_JAR" \
    com.google.gwt.dev.GWTCompiler -out "$APPDIR/www" "$@" hyi.cream.gwt.CreamGWT
