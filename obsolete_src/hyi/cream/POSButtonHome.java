package hyi.cream;

import hyi.cream.event.POSButtonEvent;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.state.StateMachine;
import hyi.cream.uibeans.DaiShouPluButton;
import hyi.cream.uibeans.POSButton;
import hyi.cream.uibeans.touch.TouchPane;
import hyi.cream.util.CreamToolkit;

import java.awt.Canvas;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import jpos.JposException;
import jpos.POSKeyboard;
import jpos.events.DataEvent;
import jpos.events.DataListener;

public class POSButtonHome extends Canvas implements /*POSButtonListener, */DataListener {
    private static final long serialVersionUID = 1L;

//    private int row = 0;
//    private int col = 0;
//  private String className = "";
//  private String buttonMode = "";
//  private String label = "";
//  private String imageFile = "";
//  private String parameters1 = "";
//  private String parameters2 = "";
//  private ArrayList configArrayList = new ArrayList();
//  private ArrayList buttonArrayList = new ArrayList();
//  private ArrayList listener = new ArrayList();
    private Map touchButtonMap = new HashMap();
  private Map touchPaneAllowButtonMap = new HashMap();
//    private static Color BUTTON_BG_COLOR = new Color(107, 133, 254);
  private List touchAllowButton = new ArrayList();

    private static POSButtonHome posButtonHome;
    private static POSKeyboard posKeyboard;
    private static StateMachine stateMachine;
    private final int minimal = 6;
    private final int maximal = 11;
    private final String defaultImage = "screenbutton0.jpg";
    private final String defaultRGB = "255";
    private boolean enabled;
    private int pageUpCode;
    private int pageDownCode;
    private int clearCode;
    private int enterCode;
    private ArrayList numberCodeArray = new ArrayList();
    private ArrayList numberButtonArray = new ArrayList();
    private ArrayList spcButtonArray = new ArrayList();
    private Map<Integer, POSButton> keyCodeToButtonMap = new HashMap<Integer, POSButton>();
    //private Class c = null;

    public static POSButtonHome getInstance() {
        try {
            if (posButtonHome == null)
                posButtonHome = new POSButtonHome();
        } catch (ConfigurationNotFoundException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InstantiationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return posButtonHome;
    }

    private POSButtonHome() throws ConfigurationNotFoundException, InstantiationException {

        // read POSButton configration file
        // construct all POSButton defined in the file

        if (GetProperty.isEnableTouchPanel(true)) {
            File buttonsFile = CreamToolkit.getConfigurationFile(TouchPane.class);
            if (buttonsFile.exists()) {
                processLine2(buttonsFile);
            }
        }

        File propFile = CreamToolkit.getConfigurationFile(POSButtonHome.class);
        processLine(propFile);

        if (!GetProperty.getPreventScannerCatNO().trim().equals(""))
            processDaiShouButton();

        try {
            posKeyboard = POSPeripheralHome.getInstance().getPOSKeyboard();
            posKeyboard.claim(0);
            posKeyboard.setDeviceEnabled(true);
            posKeyboard.setDataEventEnabled(true);
            posKeyboard.addDataListener(this);
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }

        stateMachine = StateMachine.getInstance();
        posButtonHome = this;
    }

    private void processLine2(File propFile) {
        String line = "";
        try {
            FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = null;
            inst = new InputStreamReader(filein, GetProperty
                    .getConfFileLocale("GBK"));
            BufferedReader in = new BufferedReader(inst);
            do {
                line = in.readLine();
                if (line == null) {
                    return;
                }
                touchAllowButton.add(line.trim());
            } while (true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processLine(File propFile) {
        Class[] cla = null;
        Object[] obj = null;
        String line = "";
        try {
            FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = null;
            inst = new InputStreamReader(filein, GetProperty.getConfFileLocale("GBK"));
            BufferedReader in = new BufferedReader(inst);
            char ch = ' ';
            int m;

            do {
                do {
                    line = in.readLine();
                    if (line == null) {
                        return;
                    }
                    while (line.equals("")) {
                        line = in.readLine();
                    }
                    m = 0;
                    do {
                        ch = line.charAt(m);
                        m++;
                    } while ((ch == ' ' || ch == '\t') && m < line.length());
                } while (ch == '#' || ch == ' ' || ch == '\t');

                StringTokenizer t = new StringTokenizer(line, ",", true);

                ArrayList<String> strings = new ArrayList<String>();

                while (t.hasMoreElements()) {
                    String next = t.nextToken().trim();
                    if (!next.equals(",")) {
                        strings.add(next);
                    } else {
                        if (t.hasMoreElements()) {
                            next = t.nextToken().trim();
                            while (next.equals(",")) {
                                strings.add("");
                                next = t.nextToken().trim();
                            }
                            strings.add(next);
                        }
                    }
                }

                String type = strings.get(5);

                if (strings.size() == minimal) {
                    strings.add("");
                    strings.add(defaultImage);
                } else if (strings.size() == minimal + 1) {
                    strings.add(defaultImage);
                } else if (strings.size() == minimal + 2) {
                    String bitmap = strings.get(minimal + 1);
                    if (bitmap.trim().equals("")) {
                        strings.set(minimal + 1, defaultImage);
                    }
                } else if (strings.size() > minimal + 2
                        && !type.equalsIgnoreCase("k")
//                        && !type.startsWith("t")
                        ) {
                    String bitmap = strings.get(minimal + 1);
                    int base = 2;
                    int differ = maximal - strings.size();
                    if (differ > 0)
                        for (int i = 0; i < differ; i++)
                            strings.add("");

                    String r = strings.get(minimal + base);
                    String g = strings.get(minimal + base + 1);
                    String b = strings.get(minimal + base + 2);
                    if (bitmap.trim().equals("")) {
                        if (!r.trim().equals("") || !g.trim().equals("")
                                || !b.trim().equals("")) {
                            if (r.trim().equals(""))
                                strings.set(minimal + base, defaultRGB);
                            if (g.trim().equals(""))
                                strings.set(minimal + base + 1, defaultRGB);
                            if (b.trim().equals(""))
                                strings.set(minimal + base + 2, defaultRGB);
                        } else
                            strings.set(minimal + 1, defaultImage);
                    } else {// set color information to empty string;
                        strings.set(minimal + base, "");
                        strings.set(minimal + base + 1, "");
                        strings.set(minimal + base + 2, "");
                    }
                }
                int differ = maximal - strings.size();
                for (int i = 0; i < differ; i++)
                    strings.add(" ");

                Class<?> buttonClass = Class.forName("hyi.cream.uibeans." + strings.get(4));
                int len = strings.size();
                cla = new Class[len - 7];

                // 1236
                ArrayList classParas = new ArrayList();

                for (int i = 0; i < strings.size(); i++) {
                    if (i == 1 || i == 2 || i == 3 || i == 6 || i > 10)
                        classParas.add(strings.get(i));
                }

                obj = classParas.toArray();
                Integer keyCode = null;
                for (int i = 0; i < classParas.size(); i++) {
                    if (i < 3) {
                        cla[i] = int.class;
                        obj[i] = new Integer((String) obj[i]);
                    } else if (i < 4) {
                        cla[i] = String.class;
                    } else {
                        if ((strings.get(5).startsWith("k")
                            || strings.get(5).startsWith("t"))
                            && i == 4) {
                            cla[i] = int.class;
                            obj[i] = keyCode = new Integer((String) obj[i]);
                        } else {
                            cla[i] = String.class;
                        }
                    }
                }

                Constructor constructor = buttonClass.getConstructor(cla);
                POSButton posButton = (POSButton)constructor.newInstance(obj);

                if (keyCode != null)
                    keyCodeToButtonMap.put(keyCode, posButton);

                if (((String) strings.get(5)).equalsIgnoreCase("s")) {
                    int base = 2;
                    String r = strings.get(minimal + base);
                    String g = strings.get(minimal + base + 1);
                    String b = strings.get(minimal + base + 2);
                    posButton.becomeScreenButton(strings.get(7), strings.get(0), r, g, b);
                    //posButton.addPOSButtonListener(this);

                } else if (GetProperty.getPosType("").equalsIgnoreCase("tec7000")) {
                    if (strings.get(5).equalsIgnoreCase("k") ||
                        strings.get(5).equalsIgnoreCase("k7") ||
                        strings.get(5).toLowerCase().startsWith("t")) {
                        //posButton.becomeKeyboardButton();
                        //posButton.addPOSButtonListener(this);
                        if (posButton.getKeyCode() > 999) {
                            spcButtonArray.add(posButton);
                        }
                        if (strings.get(5).toLowerCase().startsWith("t")) {
                            int base = 1;
                            String r = strings.get(base);
                            String g = strings.get(base + 1);
                            String b = strings.get(base + 2);
                            Color color = null;
                            try {
                                color = new Color(Integer.parseInt(r), Integer.parseInt(g), Integer.parseInt(b));
                            } catch (Exception e) {
                            }

                            if (GetProperty.isEnableTouchPanel(true)) {
                                String btType = ((String) strings.get(5));
                                posButton.setBackground(color);
                                if (touchButtonMap.containsKey(btType)) {
                                    List list = (List) touchButtonMap.get(btType);
                                    list.add(posButton);
                                } else {
                                    List list = new ArrayList();
                                    list.add(posButton);
                                    touchButtonMap.put(btType, list);
                                }
                            }
                        }

                        if (strings.get(4).equals("PageUpButton")) {
                            pageUpCode = Integer.parseInt(strings.get(11));
                        } else if (strings.get(4).equals("PageDownButton")) {
                            pageDownCode = Integer.parseInt(strings.get(11));
                        } else if (strings.get(4).equals("NumberButton")) {
                            numberCodeArray.add(strings.get(11));
                            numberButtonArray.add(posButton);
                        } else if (strings.get(4).equals("ClearButton")) {
                            clearCode = Integer.parseInt(strings.get(11));
                        } else if (strings.get(4).equals("EnterButton")) {
                            enterCode = Integer.parseInt(strings.get(11));
                        }

                        if (GetProperty.isEnableTouchPanel(true)) {
                            if (((String)strings.get(4)).equals("TouchMenuButton"))
                                touchPaneAllowButtonMap.put(new Integer(keyCode), keyCode);
                            if (touchAllowButton.contains((String)strings.get(4)))
                                touchPaneAllowButtonMap.put(new Integer(keyCode), keyCode);
                        }
                    }
                } else {
                    if (strings.get(5).equalsIgnoreCase("k") ||
                        strings.get(5).equalsIgnoreCase("k6")) {
                        //posButton.becomeKeyboardButton();
                        //posButton.addPOSButtonListener(this);
                        if (posButton.getKeyCode() > 999) {
                            spcButtonArray.add(posButton);
                        }

                        if (strings.get(4).equals("PageUpButton")) {
                            pageUpCode = Integer.parseInt((String) strings.get(11));
                        } else if (strings.get(4).equals("PageDownButton")) {
                            pageDownCode = Integer.parseInt(strings.get(11));
                        } else if (strings.get(4).equals("NumberButton")) {
                            numberCodeArray.add(strings.get(11));
                            numberButtonArray.add(posButton);
                        } else if (strings.get(4).equals("ClearButton")) {
                            clearCode = Integer.parseInt(strings.get(11));
                        } else if (strings.get(4).equals("EnterButton")) {
                            enterCode = Integer.parseInt(strings.get(11));
                        }

                        if (GetProperty.isEnableTouchPanel(true)) {
                            if (strings.get(4).equals("TouchMenuButton"))
                                touchPaneAllowButtonMap.put(new Integer(keyCode), keyCode);
                            if (touchAllowButton.contains((String)strings.get(4)))
                                touchPaneAllowButtonMap.put(new Integer(keyCode), keyCode);
                        }
                    }
                }
            } while (true);
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
            CreamToolkit.logMessage("Error line : " + line);
        }
    }

    public void processDaiShouButton() {
        FileFilter fileFilter = new FileFilter() {
            public boolean accept(File file) {
                if (file.getName().startsWith("daishou")
                        && (file.getName().indexOf(".conf") > 1))
                    return true;
                return false;
            }
        };
        File dir = new File(CreamToolkit.getConfigDir());
        File[] files = dir.listFiles(fileFilter);

        Map btMap = new HashMap();
        for (int l = 0; l < files.length; l++) {
            File propFile = files[l];
            String key = propFile.getName().substring(propFile.getName().indexOf("daishou") + 7
                    , propFile.getName().indexOf(".conf"));
            ArrayList menuArrayList = new ArrayList();
            try {
                FileInputStream filein = new FileInputStream(propFile);
                InputStreamReader inst = null;
                inst = new InputStreamReader(filein, GetProperty.getConfFileLocale("GBK"));
                BufferedReader in = new BufferedReader(inst);
                String line;
                char ch = ' ';
                int i;

                do {
                    do {
                        line = in.readLine();
                        if (line == null) {
                            break;
                        }
                        while (line.equals("")) {
                            line = in.readLine();
                        }
                        i = 0;
                        do {
                            ch = line.charAt(i);
                            i++;
                        } while ((ch == ' ' || ch == '\t') && i < line.length());
                    } while (ch == '#' || ch == ' ' || ch == '\t');

                    if (line == null) {
                        break;
                    }
                    menuArrayList.add(line);
                } while (true);
                btMap.put(key, menuArrayList);
            } catch (FileNotFoundException e) {
                CreamToolkit.logMessage("File is not found: " + propFile
                        + ", at daishousales");
                continue;
            } catch (IOException e) {
                CreamToolkit.logMessage("IO exception at daishousales");
                continue;
            }
        }

        List list = new ArrayList();
        for (Iterator it = btMap.keySet().iterator(); it.hasNext();) {
            list.add(it.next());
        }

        Collections.sort(list);
        for (int ii = 0; ii < list.size(); ii++) {
            String key = (String) list.get(ii);
            List menuArrayList = (List) btMap.get(key);
            for (int j = 0; j < menuArrayList.size(); j++) {
                String line = "";
                try {
                    line = (String) menuArrayList.get(j);
                    StringTokenizer t = new StringTokenizer(line, ",.");
                    String key2 = t.nextToken();
                    String pluN = t.nextToken();
                    String value2 = t.nextToken();
                    String r = "";
                    String g = "";
                    String b = "";
                    String btType = "";
                    if (t.hasMoreTokens()) {
                        t.nextToken();
                        r = t.nextToken();
                        g = t.nextToken();
                        b = t.nextToken();
                        btType = t.nextToken();
                    }
                    int keyCode = 1000 + j;
                    DaiShouPluButton daiShouPluButton = new DaiShouPluButton(0, 0, 0, pluN,
                        keyCode, value2, key, key2);
                    //pb.becomeKeyboardButton();
                    //pb.addPOSButtonListener(this);
                    Color c = null;
                    try {
                        c = new Color (Integer.parseInt(r), Integer.parseInt(g), Integer.parseInt(b));
                    } catch (Exception e) {
                    }
                    daiShouPluButton.setBackground(c);

                    //keyCodeToButtonMap.put(keyCode, daiShouPluButton);

                    if (GetProperty.isEnableTouchPanel(true)) {
                        touchPaneAllowButtonMap.put(new Integer(1000 + j), daiShouPluButton);
                        if (touchButtonMap.containsKey(btType)) {
                            List tmp = (List) touchButtonMap.get(btType);
                            tmp.add(daiShouPluButton);
                        } else {
                            List tmp = new ArrayList();
                            tmp.add(daiShouPluButton);
                            touchButtonMap.put(btType, tmp);
                        }
                    }
                } catch (Exception e) {
                    CreamToolkit.logMessage("Error daishou" + key + ".conf Line : " + line);
                }
            }
        }
    }

    public void setEventForwardEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getPageUpCode() {
        return pageUpCode;
    }

    public int getPageDownCode() {
        return pageDownCode;
    }

    public int getClearCode() {
        return clearCode;
    }

    public int getEnterCode() {
        return enterCode;
    }

    public void setEnterCode(int code) {
        enterCode = code;
    }

    public ArrayList getNumberCode() {
        return numberCodeArray;
    }

    public ArrayList getNumberButton() {
        return numberButtonArray;
    }

    public ArrayList getSpcButton() {
        return spcButtonArray;
    }

    public List getTouchButton(String type) {
        return (List) touchButtonMap.get(type);
    }

    public POSButton getTouchPaneAllowButton(int keyCode)
    {
        return (POSButton)touchPaneAllowButtonMap.get(new Integer(keyCode));
    }

    public void buttonPressed(POSButtonEvent e) {
        if (enabled)
            stateMachine.processEvent(e);
    }

    public void dataOccurred(DataEvent e)
    {
        try {
            POSKeyboard posKeyboard = (POSKeyboard)e.getSource();
            POSButton posButton = keyCodeToButtonMap.get(posKeyboard.getPOSKeyData());
            if (posButton != null)
                stateMachine.processEvent(new POSButtonEvent(posButton));
        } catch (JposException ex) {
            ex.printStackTrace(CreamToolkit.getLogger());
        }
    }
}
