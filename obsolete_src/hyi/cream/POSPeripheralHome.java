package hyi.cream;

import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.state.StateMachine;
import hyi.cream.util.CreamToolkit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import jpos.BaseControl;
import jpos.CashDrawer;
import jpos.JposException;
import jpos.Keylock;
import jpos.LineDisplay;
import jpos.MSR;
import jpos.PINPad;
import jpos.POSKeyboard;
import jpos.POSPrinter;
import jpos.ToneIndicator;
import jpos.events.DataEvent;
import jpos.events.DataListener;
import jpos.events.DirectIOEvent;
import jpos.events.DirectIOListener;
import jpos.events.ErrorEvent;
import jpos.events.ErrorListener;
import jpos.events.OutputCompleteEvent;
import jpos.events.OutputCompleteListener;
import jpos.events.StatusUpdateEvent;
import jpos.events.StatusUpdateListener;

/**
 * Container class to implement events listener and initialize device control instances.
 *
 * @author Slackware, Bruce You
 * @since 2000
 */
public class POSPeripheralHome extends Object implements DataListener, DirectIOListener,
    ErrorListener, OutputCompleteListener, StatusUpdateListener {

    private static POSPeripheralHome instance = null;

    /** Private fields which store the device control instance in a Map. */
    private Map deviceControlsMap = new HashMap();

    /** Private fields which store the device control's logical name in a Map. */
    private HashMap deviceControlsLogicalNameMap = new HashMap();

    private boolean eventForwardEnabled = false; // flag to enable the event forwarding of

	static {
        try {
            if (instance == null) {
                instance = new POSPeripheralHome();
            }
        } catch (ConfigurationNotFoundException ce) {
            ce.printStackTrace(CreamToolkit.getLogger());
        } catch (JposException je) {
            je.printStackTrace(CreamToolkit.getLogger());
        }
	}

    /** Hidden default constructor for creating singleton instance. */
    private POSPeripheralHome() throws JposException, ConfigurationNotFoundException {
        openDeviceControls();
    }

    public static POSPeripheralHome getInstance() {
        try {
            if (instance == null) {
                instance = new POSPeripheralHome();
            }
        } catch (ConfigurationNotFoundException ce) {
            ce.printStackTrace(CreamToolkit.getLogger());
            return null;
        } catch (JposException je) {
            je.printStackTrace(CreamToolkit.getLogger());
            return null;
        }
        return instance;
    }

    public void setEventForwardEnabled (boolean enabled) {
        this.eventForwardEnabled = enabled;
    }

    public boolean getEventForwardEnabled () {
        return eventForwardEnabled;
    }

    // implements the listener interface for event forwarding ---

    public void dataOccurred(DataEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine eventProcessing = StateMachine.getInstance();
        eventProcessing.processEvent(e);
    }

    public void directIOOccurred(DirectIOEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine eventProcessing = StateMachine.getInstance();
        eventProcessing.processEvent(e);
    }

    public void errorOccurred(ErrorEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine eventProcessing = StateMachine.getInstance();
        eventProcessing.processEvent(e);
    }

    public void outputCompleteOccurred(OutputCompleteEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine eventProcessing = StateMachine.getInstance();
        eventProcessing.processEvent(e);
    }

    public void statusUpdateOccurred(StatusUpdateEvent e) {
        if (!getEventForwardEnabled())
			return;
        // if (e.getSource() instanceof CashDrawer
        // && hyi.cream.dac.Transaction.getStatusFlag() != 0)
        // hyi.cream.dac.Transaction.setStatusFlag(1, true);
		StateMachine.getInstance().processEvent(e);
    }
    //end of event forwarding

    // Methods to get a single device control instance from the map, six posdevices available now ---

    public POSPrinter getPOSPrinter() throws NoSuchPOSDeviceException {
        if (deviceControlsMap.containsKey("POSPrinter")) {
            POSPrinter printer = (POSPrinter)deviceControlsMap.get("POSPrinter");
            return printer;
        } else
            throw new NoSuchPOSDeviceException();
    }

    public ToneIndicator getToneIndicator() throws NoSuchPOSDeviceException {
        if (deviceControlsMap.containsKey("ToneIndicator")) {
            ToneIndicator tone = (ToneIndicator)deviceControlsMap.get("ToneIndicator");
            return tone;
        } else
            throw new NoSuchPOSDeviceException();
    }

    public CashDrawer getCashDrawer() throws NoSuchPOSDeviceException {
        if (deviceControlsMap.containsKey("CashDrawer")) {
            CashDrawer cash = (CashDrawer)deviceControlsMap.get("CashDrawer");
            return cash;
        } else
            throw new NoSuchPOSDeviceException();
    }

    public LineDisplay getLineDisplay() throws NoSuchPOSDeviceException {
        if (deviceControlsMap.containsKey("LineDisplay")) {
            LineDisplay ld = (LineDisplay)deviceControlsMap.get("LineDisplay");
            return ld;
        } else
            throw new NoSuchPOSDeviceException();
    }

    public String getLineDisplayLogicalName() {
        return (String)deviceControlsLogicalNameMap.get("LineDisplay");
    }

    public Keylock getKeylock() throws NoSuchPOSDeviceException {
        if (deviceControlsMap.containsKey("Keylock")) {
            Keylock keyLock = (Keylock)deviceControlsMap.get("Keylock");
            return keyLock;
        } else
            throw new NoSuchPOSDeviceException();
    }

    public String getKeylockLogicalName() {
        return (String)deviceControlsLogicalNameMap.get("Keylock");
    }

    public POSKeyboard getPOSKeyboard() throws NoSuchPOSDeviceException {
       if (deviceControlsMap.containsKey("POSKeyboard")) {
            POSKeyboard keyBoard = (POSKeyboard)deviceControlsMap.get("POSKeyboard");
           return keyBoard;
       } else
           throw new NoSuchPOSDeviceException();
    }

    public PINPad getPINPad() throws NoSuchPOSDeviceException {
       if (deviceControlsMap.containsKey("PINPad")) {
            PINPad pinpad = (PINPad)deviceControlsMap.get("PINPad");
           return pinpad;
       } else
           throw new NoSuchPOSDeviceException();
    }

    public jpos.Scanner getScanner() throws NoSuchPOSDeviceException {
       if (deviceControlsMap.containsKey("Scanner")) {
       	   jpos.Scanner scanner = (jpos.Scanner)deviceControlsMap.get("Scanner");
           return scanner;
       } else
           throw new NoSuchPOSDeviceException();
    }

    public MSR getMSR() throws NoSuchPOSDeviceException {
       if (deviceControlsMap.containsKey("MSR")) {
            MSR msr = (MSR)deviceControlsMap.get("MSR");
           return msr;
       } else
           throw new NoSuchPOSDeviceException();
    }

    //Private methods for utility
    //Using the java.util.Properties to read posdevice.conf
    private Properties loadProperties() throws  ConfigurationNotFoundException {
        Properties properties = new Properties();
        try {
            File posdevicesFile = CreamToolkit.getConfigurationFile(POSPeripheralHome.class);
            //File (filePath); //get the configuration file
            FileInputStream posdevicesInput = new FileInputStream (posdevicesFile);
            properties.load(posdevicesInput);
        } catch (FileNotFoundException fe) {
            fe.printStackTrace(CreamToolkit.getLogger());
            throw new ConfigurationNotFoundException("Can't access the availiable configuration!");
        } catch (IOException ie) {
            ie.printStackTrace(CreamToolkit.getLogger());
            throw new ConfigurationNotFoundException("Can't access the availiable configuration!");
        }
        return properties;
    }

    private BaseControl createDeviceControl(String controlClassName) throws ConfigurationNotFoundException {
        BaseControl controlInstance = null;
        String deviceName = null;
        try {
            deviceName = "jpos." + controlClassName;
            //System.out.println("POSPeripheralHome.createInstance()> create " + deviceName);
            Class controlClass = Class.forName(deviceName);
            controlInstance =(BaseControl)controlClass.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            throw new ConfigurationNotFoundException("Can't access the availiable configuration!");
        } catch (InstantiationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            throw new ConfigurationNotFoundException("Can't access the availiable configuration!");
        } catch (ClassNotFoundException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            throw new ConfigurationNotFoundException("Can't access the availiable configuration!");
        }
        return controlInstance;
    }

    /**
     * Create all instances and open relevant services by reading posdevices.conf and store them
     * in a Hashtable.
     */
    public void openDeviceControls() throws ConfigurationNotFoundException, JposException {
        Properties properties = loadProperties();
        Enumeration keys = properties.propertyNames(); // Get key enumeration
        for (int i = 0; i < properties.size(); i++) {
            String controlType = (String)keys.nextElement();

            // Call createDeviceControl() to create device control instance and open relevant services
            try {
                BaseControl deviceControl = createDeviceControl(controlType);
                String logicalName = properties.getProperty(controlType);
                deviceControl.open(logicalName);
                deviceControlsMap.put(controlType, deviceControl);
                deviceControlsLogicalNameMap.put(controlType, logicalName);
                System.out.println("POSPeripheralHome.openPosDevices()> Successfully open device: " + logicalName);
            } catch (Exception e) {
                e.printStackTrace();
                e.printStackTrace(CreamToolkit.getLogger());
            }
        }
    }

    /** Method to close all pos device controls. */
    public void closeAll() throws JposException {
        if (deviceControlsMap!=null) {
            BaseControl control = null;
            Iterator elements = deviceControlsMap.values().iterator();
            while (elements.hasNext()) {
                control = (BaseControl)elements.next();
                control.close();
            }
        }
    }

    public void clearAll() {
        if (deviceControlsMap!=null) {
            //closeAll();
            deviceControlsMap.clear();
            deviceControlsMap = null;
        }
    }

    //JposEntry part....................;
    /*private static void createPOSKeyBoardEntry() {
        JposEntry jposEntry = new SimpleEntry();
        jposEntry.addProperty(JposEntry.LOGICAL_NAME_PROP_NAME, "POSKeyboard");
        jposEntry.addProperty(JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.cream.jpos.CreamJposServiceInstanceFactory");
        jposEntry.addProperty(JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.cream.jpos.POSKeyboardService");
        jposEntry.addProperty(JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp.");
        jposEntry.addProperty(JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.tw");
        jposEntry.addProperty(JposEntry.DEVICE_CATEGORY_PROP_NAME, "POSKeyboard");
        jposEntry.addProperty(JposEntry.JPOS_VERSION_PROP_NAME, "1.4a");
        jposEntry.addProperty(JposEntry.PRODUCT_NAME_PROP_NAME, "POSKeyboard JavaPOS Service");
        jposEntry.addProperty(JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "Example POSKeyboard JavaPOS Service from HYI Corporation");
        jposEntry.addProperty(JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.tw");
        JposServiceLoader.getManager().getEntryRegistry().addJposEntry("POSKeyboard", jposEntry);
    }

    private static void createToneIndicatorEntry()  {
        JposEntry jposEntry = new SimpleEntry();
        jposEntry.addProperty( JposEntry.LOGICAL_NAME_PROP_NAME, "PcSpeaker" );
        jposEntry.addProperty( JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.jpos.loader.ServiceInstanceFactory" );
        jposEntry.addProperty( JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.jpos.services.ToneIndicatorPCSpeaker" );
        jposEntry.addProperty( JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp." );
        jposEntry.addProperty( JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.tw" );
        jposEntry.addProperty( JposEntry.DEVICE_CATEGORY_PROP_NAME, "ToneIndicator" );
        jposEntry.addProperty( JposEntry.JPOS_VERSION_PROP_NAME, "1.4a" );
        jposEntry.addProperty( JposEntry.PRODUCT_NAME_PROP_NAME, "ToneIndicator JavaPOS Service" );
        jposEntry.addProperty( JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "ToneIndicator, the PC speaker version of JavaPOS Service by HYI Corporation" );
        jposEntry.addProperty( JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.tw" );
        JposServiceLoader.getManager().getEntryRegistry().addJposEntry( "PcSpeaker", jposEntry );
   }

   private static void createPOSPrinterEntry() {
        JposEntry jposEntry = new SimpleEntry();
        jposEntry.addProperty( JposEntry.LOGICAL_NAME_PROP_NAME, "EpsonRPU420" );
        jposEntry.addProperty( JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.jpos.loader.ServiceInstanceFactory" );
        jposEntry.addProperty( JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.jpos.services.POSPrinterRPU420" );
        jposEntry.addProperty( "PORT_NAME", "COM2" );
        jposEntry.addProperty( JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp." );
        jposEntry.addProperty( JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.tw" );
        jposEntry.addProperty( JposEntry.DEVICE_CATEGORY_PROP_NAME, "POSPrinter" );
        jposEntry.addProperty( JposEntry.JPOS_VERSION_PROP_NAME, "1.4a" );
        jposEntry.addProperty( JposEntry.PRODUCT_NAME_PROP_NAME, "POSPrinter JavaPOS Service" );
        jposEntry.addProperty( JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "POSPrinter, the Epson RP-U420 of JavaPOS Service by HYI Corporation" );
        jposEntry.addProperty( JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.tw" );
        JposServiceLoader.getManager().getEntryRegistry().addJposEntry( "EpsonRPU420", jposEntry);
   }

   private static void createCashDrawerEntry() {
        JposEntry jposEntry = new SimpleEntry();
        jposEntry.addProperty( JposEntry.LOGICAL_NAME_PROP_NAME, "EpsonRPU420Drawer" );
        jposEntry.addProperty( JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.jpos.loader.ServiceInstanceFactory" );
        jposEntry.addProperty( JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.jpos.services.CashDrawerService" );
        jposEntry.addProperty( "PORT_NAME", "COM2" );
        jposEntry.addProperty( JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp." );
        jposEntry.addProperty( JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.tw" );
        jposEntry.addProperty( JposEntry.DEVICE_CATEGORY_PROP_NAME, "CashDrawer" );
        jposEntry.addProperty( JposEntry.JPOS_VERSION_PROP_NAME, "1.4a" );
        jposEntry.addProperty( JposEntry.PRODUCT_NAME_PROP_NAME, "POSPrinter JavaPOS Service" );
        jposEntry.addProperty( JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "POSPrinter, the Epson RP-U420 of JavaPOS Service by HYI Corporation" );
        jposEntry.addProperty( JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.tw" );
        JposServiceLoader.getManager().getEntryRegistry().addJposEntry( "EpsonRPU420Drawer", jposEntry);
   }

    private static void createKeyLockEntry() {
        JposEntry jposEntry = new SimpleEntry();

        jposEntry.addProperty(JposEntry.LOGICAL_NAME_PROP_NAME, "PartnerK78Keylock");
        jposEntry.addProperty(JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.jpos.ServiceInstanceFactory");
        jposEntry.addProperty(JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.jpos.PartnerK78Keylock");
        jposEntry.addProperty("COMP_CLASS_PROP_NAME", "hyi.cream.KeylockFrame");
        jposEntry.addProperty(JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp.");
        jposEntry.addProperty(JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.cn");
        jposEntry.addProperty(JposEntry.DEVICE_CATEGORY_PROP_NAME, "Keylock");
        jposEntry.addProperty(JposEntry.JPOS_VERSION_PROP_NAME, "1.4a");
        jposEntry.addProperty(JposEntry.PRODUCT_NAME_PROP_NAME, "Keylock JavaPOS Service");
        jposEntry.addProperty(JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "Example Keylock JavaPOS Service from HYI Corporation");
        jposEntry.addProperty(JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.cn");

        JposServiceLoader.getManager().getEntryRegistry().addJposEntry("PartnerK78Keylock", jposEntry);
    }

    private static void createLineDisplayEntry() {
        JposEntry jposEntry = new SimpleEntry();

        jposEntry.addProperty(JposEntry.LOGICAL_NAME_PROP_NAME, "Cd5220");
        jposEntry.addProperty(JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.jpos.CreamJposServiceInstanceFactory");
        jposEntry.addProperty(JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.jpos.LineDisplayCD5220");
        jposEntry.addProperty("DevicePortName", "COM2");
        jposEntry.addProperty(JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp.");
        jposEntry.addProperty(JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.cn");
        jposEntry.addProperty(JposEntry.DEVICE_CATEGORY_PROP_NAME, "LineDisplay");
        jposEntry.addProperty(JposEntry.JPOS_VERSION_PROP_NAME, "1.4a");
        jposEntry.addProperty(JposEntry.PRODUCT_NAME_PROP_NAME, "LineDisplay JavaPOS Service");
        jposEntry.addProperty(JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "LineDisplay JavaPOS Service from HYI Corporation");
        jposEntry.addProperty(JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.cn");

        JposServiceLoader.getManager().getEntryRegistry().addJposEntry("Cd5220", jposEntry);
    }*/


    //main thread for testing purpose.....
    //Sample program for using the POSPeripheralHome and get relevant control instances
    /*public static void main(String[] args) {
        POSPeripheralHome jposHome = null;

       try {
        jposHome = POSPeripheralHome.getInstance();

            Hashtable allControlInstance = jposHome.getHash();
            //ToneIndicator-----------------------------------------------------
            ToneIndicator tone = (ToneIndicator)allControlInstance.get("ToneIndicator");
            tone.setDeviceEnabled(true);
            tone.setAsyncMode(true);
            tone.sound(5, 500);
            /*
            //POSPrinter--------------------------------------------------------
            POSPrinter printer = null;
            try {
                 printer = jposHome.getPOSPrinter();
            } catch (NoSuchPOSDeviceException ne) { }
            printer.setDeviceEnabled(true);
            printer.claim(0);
            printer.claim(-1);
            printer.claim(-1);
            //for (int i=0;i<4;i++)
            printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001Bd\u0004\n");
            //for (int i=0;i<21;i++)
            printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001Bd\u0014\n");
            for (int i=0;i<11;i++)
            printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
            printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001B|sL\u000C\n");
            //CashDrawer--------------------------------------------------------
            CashDrawer cash = (CashDrawer)allControlInstance.get("CashDrawer");
            cash.setDeviceEnabled(true);
            //cash.openDrawer();
            cash.waitForDrawerClose(50000,0,500,500);
            //
       } catch ( JposException je ) { System.out.println(je); }
          //catch ( ConfigurationNotFoundException ce ) { System.out.println(ce); }
        //JposEntryEditor.setDefaultFrameCloseOperation( JposEntryEditor.HIDE_ON_CLOSE );
        JposEntryEditor.setFrameVisible( true );
        System.out.println(jposHome.getClass().getName()+"'s Unit Test program!");
        return;
    }//*/
}