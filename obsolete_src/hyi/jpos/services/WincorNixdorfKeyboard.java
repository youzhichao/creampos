package hyi.jpos.services;

import hyi.cream.POSButtonHome;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.DacTransfer;
import hyi.cream.state.GetProperty;
import hyi.cream.uibeans.DacViewer;
import hyi.cream.uibeans.ItemList;
import hyi.cream.uibeans.PayingPaneBanner;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.uibeans.SignOffForm;
import hyi.cream.util.CreamToolkit;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import jpos.JposConst;
import jpos.JposException;
import jpos.POSKeyboard;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.events.DataEvent;
import jpos.services.EventCallbacks;
import jpos.services.POSKeyboardService14;

/**
 * POS keyboard service for Wincor Nixdorf.
 * 
 * @author Bruce
 */
public class WincorNixdorfKeyboard implements POSKeyboardService14, KeyListener, ContainerListener {

    static private POSKeyboard claimedControl;
    static private Object mutex = new Object();
    static private final boolean DEBUG = false;

    protected EventCallbacks eventCallbacks = null;
    protected JposEntry entry = null;
    protected Container comp = null;
    protected Container comp2 = null;
    protected Container comp3 = null;
    protected int posKeyData = 0;

    private boolean claimed = false;
    private boolean deviceEnabled = false;
    private int idxKeylock;
    private boolean keylockDataIn;
    private int enterButtonCount;
    private int scannerPrefixCharValue;

    /**
     * Constructor
     */
    public WincorNixdorfKeyboard(SimpleEntry entry) {
        this.entry = entry;
    }

    // Capabilities
    public boolean getCapKeyUp() throws JposException {
        return true;
    }

    public int getCapPowerReporting() throws JposException {
        return 0;
    }

    // Properties
    public boolean getAutoDisable() throws JposException {
        return true;
    }

    public void setAutoDisable(boolean autoDisable) throws JposException {
    }

    public int getDataCount() throws JposException {
        return 0;
    }

    public boolean getDataEventEnabled() throws JposException {
        return true;
    }

    public void setDataEventEnabled(boolean dataEventEnabled) throws JposException {
    }

    public int getEventTypes() throws JposException {
        return 0;
    }

    public void setEventTypes(int eventTypes) throws JposException {
    }

    public int getPOSKeyData() throws JposException {
        return posKeyData;
    }

    public int getPOSKeyEventType() throws JposException {
        return 0;
    }

    public int getPowerNotify() throws JposException {
        return 0;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
    }

    public int getPowerState() throws JposException {
        return 0;
    }

    public String getCheckHealthText() throws JposException {
        return "";
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    private boolean isActive = false;
    private boolean fkey = false;
    private String fkeyString = "";
    private boolean msr = false;
    private boolean scannerDataIn = false;
    private HashMap fkeys = new HashMap();
    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        if (!claimed) {
            //System.out.println("must claim it first");
            return;
        }
        this.deviceEnabled = deviceEnabled;
        if (deviceEnabled) {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComponent(className);
                registerKeyListener(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComponent(className2);
                registerKeyListener(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComponent(className3);
                registerKeyListener(comp3);
            }
        } else {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComponent(className);
                unregisterKeyListener(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComponent(className2);
                unregisterKeyListener(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComponent(className3);
                unregisterKeyListener(comp3);
            }
        }
    }

    public String getDeviceServiceDescription() throws JposException {
        return "";
    }

    public int getDeviceServiceVersion() throws JposException {
        return 0;
    }

    public boolean getFreezeEvents() throws JposException {
        return true;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return "";
    }

    public String getPhysicalDeviceName() throws JposException {
        return "";
    }

    public int getState() throws JposException {
        return 0;
    }

    public void clearInput() throws JposException {
    }

    public void deleteInstance() throws JposException {
    }

    public void open(String logicalName, EventCallbacks eventCallbacks) throws JposException {
        this.eventCallbacks = eventCallbacks;
        claimed = false;
        deviceEnabled = false;

        scannerPrefixCharValue = Integer.parseInt(
            GetProperty.getScannerPrefixCharValue("32"));
    }

    public void close() throws JposException {
        claimed = false;
        deviceEnabled = false;
        claimedControl = null;
    }

    public void claim(int timeout) throws JposException {
        if (claimed) {
            if (DEBUG)
                System.out.println("device has been claimed");
            return;
        }

        synchronized (mutex) {
            if (claimedControl == null) {
                Object obj = eventCallbacks.getEventSource();
                claimedControl = (POSKeyboard)obj;
                claimed = true;
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                    claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                    claimed = true;
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        if (claimedControl == null) {
                            claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                            claimed = true;
                            return;
                        }
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        }
        claimed = true;
    }

    public void checkHealth(int level) throws JposException {
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
    }

    public void release() throws JposException {
        synchronized (mutex) {
            if (!claimed) {
                System.out.println("device has been release");
            }
            claimed = false;
            deviceEnabled = false;
            claimedControl = null;
            mutex.notifyAll();
        }
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        PopupMenuPane pop = app.getPopupMenuPane();
        SignOffForm signOffForm = app.getSignOffForm();
        ItemList itemList = app.getItemList();
        DacViewer dacViewer = app.getDacViewer();
        PayingPaneBanner payingPane = app.getPayingPane();

        if (DEBUG)
            System.out.println("WincorNixdorfKeyboard> KeyCode = " + e.getKeyCode());

        posKeyData = e.getKeyCode();

        if (posKeyData == POSButtonHome.getInstance().getEnterCode()) {
            enterButtonCount++;
            if (enterButtonCount == 20) {
                CreamToolkit.logMessage("System is going to shutdown caused by pressing many Enter.");
                DacTransfer.shutdown(1);
            }
        } else {
            enterButtonCount = 0;
        }

        // skip keylock data (e.g., 17, 17, 75, 48)
        if (!keylockDataIn && posKeyData == 17) {
            keylockDataIn = true;
            idxKeylock = 1;
            if (DEBUG)
                System.out.println("WincorNixdorfKeyboard> Keylock sequence begins...");
            return;
        }
        if (keylockDataIn && idxKeylock < 3) { // for skip keylock key sequence
            idxKeylock++;
            return;
        } else if (keylockDataIn && idxKeylock >= 3) {
            keylockDataIn = false;
            idxKeylock = 0;
            return;
        }

        // skip scanner data
        if (!scannerDataIn && posKeyData == scannerPrefixCharValue) { // 'T'
            scannerDataIn = true;
            if (DEBUG)
                System.out.println("WincorNixdorfKeyboard> Scanner sequence begins...");
            return;
        }
        if (scannerDataIn && posKeyData != 10) {
            if (DEBUG)
                System.out.println("WincorNixdorfKeyboard> Getting Scanner data...");
            return;
        } else if (scannerDataIn && posKeyData == 10) {
            if (DEBUG)
                System.out.println("WincorNixdorfKeyboard> Get last Scanner data");
            scannerDataIn = false;
            return;
        }

        int pageUp = POSTerminalApplication.getInstance().getPOSButtonHome().getPageUpCode();
        int pageDown = POSTerminalApplication.getInstance().getPOSButtonHome().getPageDownCode();

        if (signOffForm != null && signOffForm.isVisible()) {
            if (DEBUG)
                System.out.println("WincorNixdorfKeyboard> send key to SignOffForm");
            signOffForm.keyDataListener(posKeyData);
            return;
        } else if (pop != null && pop.isVisible()) {
            if (DEBUG)
                System.out.println("WincorNixdorfKeyboard> send key to PopupMenuPane");
            pop.keyDataListener(posKeyData);
            return;
        } else if (itemList != null && itemList.isVisible() && (posKeyData == pageUp || posKeyData == pageDown)) {
            if (DEBUG)
                System.out.println("WincorNixdorfKeyboard> send key to ItemList");
            itemList.keyDataListener(posKeyData);
        } else if (payingPane != null && payingPane.isVisible()) {
            if (DEBUG)
                System.out.println("WincorNixdorfKeyboard> send key to PayingPane");
            payingPane.keyDataListener(posKeyData);
        }
        if (dacViewer != null && dacViewer.isVisible())
            dacViewer.keyDataListener(posKeyData);
        if (DEBUG)
            System.out.println("WincorNixdorfKeyboard> keyPressed(): fire DataEvent code=" + posKeyData);
        eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
    }

    synchronized public void keyTyped(KeyEvent e) {
    }

    public Container createComponent(String className) {
        try {
            Class compClass = Class.forName(className);
            Method compMethod = compClass.getDeclaredMethod("getInstance", new Class[0]);
            comp = (Container) (compMethod.invoke(null, new Object[0]));
        } catch (ClassNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Class is not found at " + this);
        } catch (IllegalAccessException ex) {
            CreamToolkit.logMessage(ex.toString());
            CreamToolkit.logMessage("Illegal access exception at " + this);
        } catch (NoSuchMethodException exc) {
            CreamToolkit.logMessage(exc.toString());
            CreamToolkit.logMessage("No such method at " + this);
        } catch (InvocationTargetException exce) {
            CreamToolkit.logMessage(exce.toString());
            CreamToolkit.logMessage("Invocation exception at " + this);
        }
        return comp;
    }

    public void registerKeyListener(Container comp) {
        comp.addKeyListener(this);
        comp.addContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                registerKeyListener(compo);
            } else {
                co.addKeyListener(this);
            }
        }
    }

    public void unregisterKeyListener(Container comp) {
        comp.removeKeyListener(this);
        comp.removeContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                unregisterKeyListener(compo);
            } else {
                co.removeKeyListener(this);
            }
        }
    }

    public void componentAdded(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            registerKeyListener((Container)e.getChild());
        } else {
            e.getChild().addKeyListener(this);
        }
    }

    public void componentRemoved(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            unregisterKeyListener((Container)e.getChild());
        } else {
            e.getChild().removeKeyListener(this);
        }
    }
}
