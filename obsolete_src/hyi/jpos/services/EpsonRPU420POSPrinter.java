package hyi.jpos.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import javax.comm.SerialPortEvent;
import javax.comm.SerialPortEventListener;

import jpos.JposConst;
import jpos.JposException;
import jpos.POSPrinterConst;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.events.ErrorEvent;
import jpos.services.EventCallbacks;
import jpos.services.POSPrinterService19;

import org.apache.log4j.Logger;

public class EpsonRPU420POSPrinter extends AbstractDeviceService19 implements
    SerialPortEventListener, POSPrinterService19 {

    static Logger logger = Logger.getLogger(EpsonRPU420POSPrinter.class);

    private Object waitObject = new Object();
    private Object waitForPrinterStatus = new Object();

    private StringParsingPP2000 parse = null;
    private OutputStreamWriter dataOutput = null;
    // private InputStreamReader dataInput = null;
    private ShareOutputPort shareOutput = null;
    static private String logicalName = null;
    private StringBuffer buffer = null;

    private String portName;

    /** for printTransaction() */
    private boolean transactionJournalMode = false;
    private boolean transactionReceiptMode = false;
    private boolean transactionSlipMode = false;
    private StringBuffer transactionJournalBuffer = new StringBuffer();
    private StringBuffer transactionReceiptBuffer = new StringBuffer();
    private StringBuffer transactionSlipBuffer = new StringBuffer();

    /** input & output Stream attached to POSPrinter */
    private OutputStream outputStream = null;
    private InputStream inputStream = null;

    /** serial port current feedback status got asyncronously */
    private char statusCode = 0x00;

    private JposEntry POSPrinterentry;

    /** insertion and removal flags */
    private boolean insertionFlag = false;

    private boolean asyncMode = false;

    /** CharacterSet initialized */
    private int characterSet = POSPrinterConst.PTR_CS_ASCII;

    /** a flag */
    private boolean flagWhenIdle = true;
    private int outputID = 0;

    private String toplogo = null;
    private String bottomlogo = null;

    public String getTopLogo() throws JposException {
        if (toplogo != null)
            return toplogo;
        else
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Null pointer logo detected!");
    }

    public String getBottomLogo() throws JposException {
        if (bottomlogo != null)
            return bottomlogo;
        else
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Null pointer logo detected!");
    }

    // constructor block here
    public EpsonRPU420POSPrinter() {
        parse = new StringParsingPP2000(this);
        buffer = new StringBuffer();
    }

    /**
     * constructor with JposEntry parameter
     */
    public EpsonRPU420POSPrinter(SimpleEntry entry) {
        POSPrinterentry = entry;
        parse = new StringParsingPP2000(this);
        buffer = new StringBuffer();
    }

    /**
     * Service13 Capabilities
     */
    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_STANDARD;
    }

    /**
     * Service12 Capabilities
     */
    public int getCapCharacterSet() throws JposException {
        return POSPrinterConst.PTR_CCS_ASCII;
    }

    // Journal and Receipt stations can print at the same time.
    public boolean getCapConcurrentJrnRec() throws JposException {
        return true;
    }

    // If true, then the Journal and Slip stations can print at the same time.
    public boolean getCapConcurrentJrnSlp() throws JposException {
        return false;
    }

    // If true, then the Receipt and Slip stations can print at the same time.
    public boolean getCapConcurrentRecSlp() throws JposException {
        return false;
    }

    // If true, then the printer has a "cover open§ sensor.
    public boolean getCapCoverSensor() throws JposException {
        return false;
    }

    // If true, then the journal can print dark plus an alternate color.
    public boolean getCapJrn2Color() throws JposException {
        return false;
    }

    // If true, then the journal can print bold characters.
    public boolean getCapJrnBold() throws JposException {
        return false;
    }

    // If true, then the journal can print double high characters.
    public boolean getCapJrnDhigh() throws JposException {
        return false;
    }

    // If true, then the journal can print double wide characters.
    public boolean getCapJrnDwide() throws JposException {
        return true;
    }

    // If true, then the journal can print double high / double wide characters.
    public boolean getCapJrnDwideDhigh() throws JposException {
        return false;
    }

    // If true, then the journal has an out-of-paper sensor.
    public boolean getCapJrnEmptySensor() throws JposException {
        return false;
    }

    // If true, then the journal can print italic characters.
    public boolean getCapJrnItalic() throws JposException {
        return false;
    }

    // If true, then the journal has a low paper sensor.
    public boolean getCapJrnNearEndSensor() throws JposException {
        return false;
    }

    // If true, then the journal print station is present.
    public boolean getCapJrnPresent() throws JposException {
        return true;
    }

    // If true, then the journal can underline characters.
    public boolean getCapJrnUnderline() throws JposException {
        return false;
    }

    // If true, then the receipt can print dark plus an alternate color.
    public boolean getCapRec2Color() throws JposException {
        return false;
    }

    // If true, then the receipt has bar code printing capability.
    public boolean getCapRecBarCode() throws JposException {
        return false;
    }

    // If true, then the receipt can print bitmaps.
    public boolean getCapRecBitmap() throws JposException {
        return true;
    }

    // If true, then the receipt can print bold characters.
    public boolean getCapRecBold() throws JposException {
        return false;
    }

    // If true, then the receipt can print double high characters.
    public boolean getCapRecDhigh() throws JposException {
        return false;
    }

    // If true, then the receipt can print double wide characters.
    public boolean getCapRecDwide() throws JposException {
        return true;
    }

    // If true, then the receipt can print double wide/high characters.
    public boolean getCapRecDwideDhigh() throws JposException {
        return false;
    }

    // If true, then the receipt has an out-of-paper sensor.
    public boolean getCapRecEmptySensor() throws JposException {
        return true;
    }

    // If true, then the receipt can print italic characters.
    public boolean getCapRecItalic() throws JposException {
        return false;
    }

    // If true, then the receipt can print in rotated 90～ left mode.
    public boolean getCapRecLeft90() throws JposException {
        return false;
    }

    // If true, then the receipt has a low paper sensor.
    public boolean getCapRecNearEndSensor() throws JposException {
        return true;
    }

    // If true, then the receipt can perform paper cuts.
    public boolean getCapRecPapercut() throws JposException {
        return true;
    }

    // If true, then the receipt print station is present.
    public boolean getCapRecPresent() throws JposException {
        return true;
    }

    // If true, then the receipt can print in a rotated 90～ right mode.
    public boolean getCapRecRight90() throws JposException {
        return false;
    }

    // If true, then the receipt can print in a rotated upside down mode.
    public boolean getCapRecRotate180() throws JposException {
        return false;
    }

    // If true, then the receipt has a stamp capability.
    public boolean getCapRecStamp() throws JposException {
        return true;
    }

    // If true, then the receipt can underline characters.
    public boolean getCapRecUnderline() throws JposException {
        return false;
    }

    // If true, then the slip can print dark plus an alternate color.
    public boolean getCapSlp2Color() throws JposException {
        return false;
    }

    // If true, then the slip has bar code printing capability.
    public boolean getCapSlpBarCode() throws JposException {
        return false;
    }

    // If true, then the slip can print bitmaps.
    public boolean getCapSlpBitmap() throws JposException {
        return false;
    }

    // If true, then the slip can print bold characters.
    public boolean getCapSlpBold() throws JposException {
        return false;
    }

    // If true, then the slip can print double high characters.
    public boolean getCapSlpDhigh() throws JposException {
        return false;
    }

    // If true, then the slip can print double wide characters.
    public boolean getCapSlpDwide() throws JposException {
        return true;
    }

    // If true, then the slip can print double high / double wide characters.
    public boolean getCapSlpDwideDhigh() throws JposException {
        return false;
    }

    // If true, then the slip has a "slip in" sensor.
    public boolean getCapSlpEmptySensor() throws JposException {
        return true;
    }

    /*
     * If true, then the slip is a full slip station. It can print full-length forms. If false, then
     * the slip is a ※validation§ type station. This usually limits the number of print lines, and
     * disables access to the receipt and/or journal stations while the validation slip is being
     * used.
     */
    public boolean getCapSlpFullslip() throws JposException {
        return false;
    }

    // If true, then the slip can print italic characters.
    public boolean getCapSlpItalic() throws JposException {
        return false;
    }

    // If true, then the slip can print in a rotated 90～ left mode.
    public boolean getCapSlpLeft90() throws JposException {
        return false;
    }

    // If true, then the slip has a ※slip near end§ sensor.
    public boolean getCapSlpNearEndSensor() throws JposException {
        return false;
    }

    // If true, then the slip print station is present.
    public boolean getCapSlpPresent() throws JposException {
        return true;
    }

    // If true, then the slip can print in a rotated 90～ right mode.
    public boolean getCapSlpRight90() throws JposException {
        return false;
    }

    // If true, then the slip can print in a rotated upside down mode.
    public boolean getCapSlpRotate180() throws JposException {
        return false;
    }

    // If true, then the slip can underline characters.
    public boolean getCapSlpUnderline() throws JposException {
        return false;
    }

    // If true, then printer transactions are supported by each station.
    public boolean getCapTransaction() throws JposException {
        return true;
    }

    // the end of Service12 capabilities

    /**
     * Service13 Properties
     */
    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "No Service!");
    }

    /**
     * n0: "S"
     * n1: 收執聯位置 "0":定位 "1":未定位
     * n2: 存根聯位置 "0":定位 "1":未定位
     * n3: 紙張是否用完 "0":否 "1":是
     * n4: 印證紙張定位 "0":無紙 "1":有紙
     * n5: 發票印表機發生錯誤 "0":沒發生 "1":發生
     * n6: 是否列印中 "0":否 "1":是
     * n7: 列印超過40行 "0":否 "1":是
     * n8: 資料緩衝區是否已滿 "0":否 "1":是
     * n9: 資料緩衝區是否為空 "0":是 "1":否                
     */
    @Override
    public String getCheckHealthText() throws JposException {
        getPowerState();
        return super.getCheckHealthText();
    }

    public int getPowerState() throws JposException {
        synchronized (waitForPrinterStatus) {
            try {
                setCheckHealthText(null);
                printerStatus = null;
                outputStream.write(27);
                outputStream.write(27);
                outputStream.write(79);
                outputStream.write(13);
                waitForPrinterStatus.wait(5000); // wait at most 5 seconds

                setCheckHealthText(printerStatus);
                return (printerStatus.length() == 10) ? JposConst.JPOS_PS_ONLINE : JposConst.JPOS_PS_OFFLINE; 
            } catch (IndexOutOfBoundsException e) {
                logger.error("Get status failed", e);
                return JposConst.JPOS_PS_OFFLINE;
            } catch (InterruptedException e) {
                logger.error("Get status failed", e);
                return JposConst.JPOS_PS_OFFLINE;
            } catch (IOException e) {
                logger.error("Get status failed", e);
                return JposConst.JPOS_PS_OFFLINE;
            }
        }
    }

    /**
     * Service12 Properties
     */

    // Sync/Async mode
    public boolean getAsyncMode() throws JposException {
        return asyncMode;
    }

    public void setAsyncMode(boolean asyncMode) throws JposException {
        if (getAsyncMode() && !asyncMode) {
            this.asyncMode = false;
            System.out.println("POSPrinter runs in SyncMode!");
            return;
        } else if (!getAsyncMode() && asyncMode) {
            this.asyncMode = true;
            System.out.println("POSPrinter runs in AsyncMode!");
            return;
        } else
            return;
        // throw new JposException(JposConst.JPOS_E_ILLEGAL, "Expected mode can't be obtained:
        // Change to the same mode!");
    }

    public int getCharacterSet() throws JposException {
        return characterSet;
    }

    public void setCharacterSet(int characterSet) throws JposException {
        this.characterSet = characterSet;
    }

    // Holds the character set numbers. It consists of ASCII numeric set numbers
    // separated by commas.
    public String getCharacterSetList() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE);
    }

    // If true, then the printer＊s cover is open.
    public boolean getCoverOpen() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE);
    }

    // Holds the severity of the error condition. It has one of the following values:
    // PTR_EL_NONE No error condition is present.
    // PTR_EL_RECOVERABLE A recoverable error has occurred.(Example: Out of paper.)
    // PTR_EL_FATAL A non-recoverable error has occurred.(Example: Internal printer failure.)
    // --------------------------------------------------------------------------
    // This property is set just before delivering an ErrorEvent. When the error is
    // cleared, then the property is changed to PTR_EL_NONE.
    // --------------------------------------------------------------------------

    private int errorLevel = POSPrinterConst.PTR_EL_NONE;

    public int getErrorLevel() throws JposException {
        return errorLevel;
    }

    /*
     * Holds the station or stations that were printing when an error was detected. This property
     * will be set to one of the following values: PTR_S_JOURNAL PTR_S_RECEIPT PTR_S_SLIP
     * PTR_S_JOURNAL_RECEIPT PTR_S_JOURNAL_SLIP PTR_S_RECEIPT_SLIP PTR_TWO_RECEIPT_JOURNAL
     * PTR_TWO_SLIP_JOURNAL PTR_TWO_SLIP_RECEIPT This property is only valid if the ErrorLevel is
     * not equal to PTR_EL_NONE. It is set just before delivering an ErrorEvent.
     */

    private int errorStation = 0;

    public int getErrorStation() throws JposException {
        return errorStation;
    }

    /*
     * Holds a vendor-supplied description of the current error. This property is set just before
     * delivering an ErrorEvent. If no description is available, the property is set to an empty
     * string. When the error is cleared, then the property is changed to an empty string.
     */
    private String errorString = "";

    public String getErrorString() throws JposException {
        return errorString;
    }

    /*
     * If true, a StatusUpdateEvent will be enqueued when the device is in the idle state. This
     * property is automatically reset to false when the status event is delivered. The main use of
     * idle status event that is controlled by this property is to give the application control when
     * all outstanding asynchronous outputs have been processed. The event will be enqueued if the
     * outputs were completed successfully or if they were cleared by the clearOutput method or by
     * an ErrorEvent handler. If the State is already set to JPOS_S_IDLE when this property is set
     * to true, then a StatusUpdateEvent is enqueued immediately. The application can therefore
     * depend upon the event, with no race condition between the starting of its last asynchronous
     * output and the setting of this flag. This property is initialized to false by the open
     * method.
     */
    public boolean getFlagWhenIdle() throws JposException {
        return flagWhenIdle;
    }

    public void setFlagWhenIdle(boolean flagWhenIdle) throws JposException {
        this.flagWhenIdle = flagWhenIdle;
    }

    /*
     * Holds the fonts and/or typefaces that are supported by the printer. The string consists of
     * font or typeface names separated by commas. This property is initialized by the open method.
     */
    // borrow this method name to get status code - Andy
    // public String getFontTypefaceList() throws JposException {
    // return "";
    // }
    public boolean getJrnEmpty() throws JposException {
        if (!getCapJrnEmptySensor())
            throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
        return false;
    }

    public boolean getJrnLetterQuality() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setJrnLetterQuality(boolean jrnLetterQuality) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    // Holds the number of characters that may be printed on a journal line.
    private int jrnLineChars = 24;

    public int getJrnLineChars() throws JposException {
        return jrnLineChars;
    }

    // This property is initialized to the printer＊s default line character width
    // when the device is first enabled following the open method.
    public void setJrnLineChars(int jrnLineChars) throws JposException {
        if (jrnLineChars < 0 || jrnLineChars > 24)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid agrument:" + jrnLineChars);
        this.jrnLineChars = jrnLineChars;
    }

    public String getJrnLineCharsList() throws JposException {
        return "12,24";
    }

    public int getJrnLineHeight() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setJrnLineHeight(int jrnLineHeight) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getJrnLineSpacing() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setJrnLineSpacing(int jrnLineSpacing) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getJrnLineWidth() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public boolean getJrnNearEnd() throws JposException {
        if (!getCapJrnNearEndSensor())
            throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
        return false;
    }

    public int getMapMode() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setMapMode(int mapMode) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public String getRecBarCodeRotationList() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public boolean getRecEmpty() throws JposException {
        if (!getCapRecEmptySensor())
            throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
        if (getAsyncMode()) {
            // Code goes here....................................................
        } else {
            synchronized (waitObject) {
                try {
                    outputStream.write(27);
                    outputStream.write(27);
                    outputStream.write(79);
                    outputStream.write(13);
                    try {
                        waitObject.wait();
                    } catch (InterruptedException ie) {
                        throw new JposException(JposConst.JPOS_E_ILLEGAL,
                            "Printer wait interrupted", ie);
                    }// */
                } catch (IOException e) {
                    throw new JposException(JposConst.JPOS_E_ILLEGAL,
                        "I/O error occured in printing", e);
                }
            }
        }
        synchronized (shareOutput.waitObject) {
            try {
                shareOutput.waitObject.wait(2500);
            } catch (InterruptedException ie) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL, "Printer wait interrupted", ie);
            }
        }
        if (shareOutput.getStatusCode().length() == 8)
            statusCode = shareOutput.getStatusCode().charAt(5);
        else
            return true;
        if (statusCode == '0')
            return false;
        else if (statusCode == '1')
            return true;
        return true;
    }

    public boolean getRecLetterQuality() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setRecLetterQuality(boolean recLetterQuality) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    private int recLineChars = 24;

    public int getRecLineChars() throws JposException {
        return recLineChars;
    }

    public void setRecLineChars(int recLineChars) throws JposException {
        if (recLineChars < 0 || recLineChars > 24)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid agrument:" + jrnLineChars);
        this.recLineChars = recLineChars;
    }

    public String getRecLineCharsList() throws JposException {
        return "12,24";
    }

    public int getRecLineHeight() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setRecLineHeight(int recLineHeight) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getRecLineSpacing() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setRecLineSpacing(int recLineSpacing) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    private int recLinesToPaperCut = 0;

    public int getRecLinesToPaperCut() throws JposException {
        return recLinesToPaperCut;
    }

    public int getRecLineWidth() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public boolean getRecNearEnd() throws JposException {
        if (!getCapRecNearEndSensor())
            throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
        if (getAsyncMode()) {
            // Code goes here....................................................
        } else {
            synchronized (waitObject) {
                try {
                    outputStream.write(27);
                    outputStream.write(27);
                    outputStream.write(79);
                    outputStream.write(13);
                    try {
                        waitObject.wait();
                    } catch (InterruptedException ie) {
                        throw new JposException(JposConst.JPOS_E_ILLEGAL,
                            "Printer wait interrupted", ie);
                    }
                } catch (IOException e) {
                    throw new JposException(JposConst.JPOS_E_ILLEGAL,
                        "I/O error occured in printing", e);
                }
            }
        }
        synchronized (shareOutput.waitObject) {
            try {
                shareOutput.waitObject.wait(2500);
            } catch (InterruptedException ie) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL, "Printer wait interrupted", ie);
            }
        }
        if (shareOutput.getStatusCode().length() == 8)
            statusCode = shareOutput.getStatusCode().charAt(5);
        else
            return true;
        if (statusCode == '0')
            return false;
        else if (statusCode == '1')
            return true;
        return true;
    }

    public int getRecSidewaysMaxChars() throws JposException {
        return 0;
    }

    public int getRecSidewaysMaxLines() throws JposException {
        return 0;
    }

    public int getRotateSpecial() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setRotateSpecial(int rotateSpecial) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public String getSlpBarCodeRotationList() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public boolean getSlpEmpty() throws JposException {
        return false;
    }

    public String getFontTypefaceList() throws JposException {
        return null;
    }

    public boolean getSlpLetterQuality() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setSlpLetterQuality(boolean recLetterQuality) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    // --------------------------------------------------------------------------
    private int slpLineChars = 55;

    private String printerStatus;
    private char drawerStatus;

    public int getSlpLineChars() throws JposException {
        return slpLineChars;
    }

    public void setSlpLineChars(int slpLineChars) throws JposException {
        if (slpLineChars < 0 || slpLineChars > 55)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid agrument:" + slpLineChars);
        this.slpLineChars = slpLineChars;
    }

    // --------------------------------------------------------------------------

    public String getSlpLineCharsList() throws JposException {
        return "27,55";
    }

    public int getSlpLineHeight() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setSlpLineHeight(int slpLineHeight) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getSlpLinesNearEndToEnd() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getSlpLineSpacing() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setSlpLineSpacing(int slpLineSpacing) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getSlpLineWidth() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getSlpMaxLines() throws JposException {
        return 1;
    }

    public boolean getSlpNearEnd() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getSlpSidewaysMaxChars() throws JposException {
        return 0;
    }

    public int getSlpSidewaysMaxLines() throws JposException {
        return 0;
    }

    // --------------------------------------------------------------------------
    public int getOutputID() throws JposException {
        return outputID;
    }

    // new...begin Service12 Methods
    // first sector the synchronized method
    synchronized public void beginInsertion(int timeout) throws JposException {
        if (timeout < -1)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Illegal parameter:" + timeout);
        if (!getCapSlpPresent())
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not supported");
    }

    synchronized public void endInsertion() throws JposException {
        insertionFlag = true;
    }

    synchronized public void beginRemoval(int timeout) throws JposException {
        if (timeout < -1)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Illegal parameter:" + timeout);
        if (!getCapSlpPresent())
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not supported");
    }

    synchronized public void endRemoval() throws JposException {
        insertionFlag = true;
    }

    // end of first sector the synchronized method

    public void clearOutput() throws JposException {
        outputID = 0;
    }

    public void cutPaper(int percentage) throws JposException {

        // check out the situation to make sure that the printer can be operated
        if (percentage == 0)
            return;
        if (percentage < 0 || percentage > 100)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Illegal argument:" + percentage
                + " detected!");

        // locate the next black block of the receipt and cut paper, print stamp
        printTwoNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001B|sP", "\u001B|sP");
        
//        if (!getClaimed())
//            throw new JposException(JposConst.JPOS_E_NOTCLAIMED,
//                "Please claim the printer before the manipulation could start!");
//        if (!getDeviceEnabled())
//            throw new JposException(JposConst.JPOS_E_DISABLED, "Printer is not enabled!");
//
//        // asychronized mode and syncronized mode
//        String command = "";
//        if (percentage < 100)
//            command = parse.getCommandStart() + "c" + parse.getCommandEnd();
//        else
//            command = parse.getCommandStart() + "C" + parse.getCommandEnd();
//        
//        
//        if (!getAsyncMode()) {
//            synchronized (waitObject) {
//                try {
//                    dataOutput.write(command);
//                    dataOutput.flush();
//                    try {
//                        waitObject.wait();
//                    } catch (InterruptedException ie) {
//                        throw new JposException(JposConst.JPOS_E_FAILURE, "Cut paper interupted!",
//                            ie);
//                    }
//                } catch (IOException ie) {
//                    throw new JposException(JposConst.JPOS_E_FAILURE, "IO error in cutting paper!",
//                        ie);
//                }
//            }
//        } else {
//            /*
//             * OutputCompleteEvent outputComplete = new
//             * OutputCompleteEvent(this.eventCallbacks.getEventSource(), outputID);
//             * eventCallbacks.fireOutputCompleteEvent(outputComplete);//
//             */
//        }
    }// end of cutPaper

    public void printBarCode(int station, String data, int symbology, int height, int width,
        int alignment, int textPosition) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not support!");
    }// end of printBarCode

    public void printBitmap(int station, String fileName, int width, int alignment)
        throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not support!");
    }// end of printBitmap

    /*
     * JPOS_E_EXTENDED ErrorCodeExtended = JPOS_EPTR_COVER_OPEN: The printer cover is open.
     * ErrorCodeExtended = JPOS_EPTR_JRN_EMPTY: The journal station was specified but is out of
     * paper. ErrorCodeExtended = JPOS_EPTR_REC_EMPTY: The receipt station was specified but is out
     * of paper. ErrorCodeExtended = JPOS_EPTR_SLP_EMPTY: The slip station was specified, but a form
     * is not inserted.
     */

    // synchronized printing method
    public void printImmediate(int station, String data) throws JposException {
        if (!getClaimed())
            throw new JposException(JposConst.JPOS_E_NOTCLAIMED,
                "Please claim the printer before manipulation!");
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_DISABLED, "Printer is not enabled!");
        String realCommand = parse.getValidCommand(station, data);
        if (realCommand == null)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid station argument:" + station);
        else if (data.equals(""))
            return;
        try {
            dataOutput.write(realCommand);
            dataOutput.flush();
            //Bruce> Don't wait for faster speed.
            // synchronized (waitObject) {
            // try {
            // waitObject.wait();
            // } catch (InterruptedException ie) {
            // throw new JposException(JposConst.JPOS_E_ILLEGAL);
            // }
            // }
        } catch (IOException ie) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL);
        }
    }// end of printImmediate()

    // Main
    public void printNormal(int station, String data) throws JposException {
        // Check out if the station argument is legal;
        if (data.equals(""))
            return;
        String realCommand = parse.getValidCommand(station, data);// "\u001B\u001BPR1lo\u000En\u000Eo\r";
        if (realCommand == null)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid arguments!");
        if (!getClaimed())
            throw new JposException(JposConst.JPOS_E_NOTCLAIMED,
                "Please claim the printer before the manipulation could start!");
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_DISABLED, "Printer is not enabled!");
        // ----------------------------------------------------------------------
        if (station == POSPrinterConst.PTR_S_JOURNAL && transactionJournalMode == true) {
            transactionJournalBuffer.append(realCommand);
            return;
        }
        if (station == POSPrinterConst.PTR_S_RECEIPT && transactionReceiptMode == true) {
            transactionReceiptBuffer.append(realCommand);
            return;
        }
        if (station == POSPrinterConst.PTR_S_SLIP && transactionSlipMode == true) {
            transactionSlipBuffer.append(realCommand);
            return;
        }
        // ______________________________________________________________________
        // ----------------------------------------------------------------------
        if (getAsyncMode()) {
            // Code goes here....................................................
        } else {
            synchronized (waitObject) {
                try {
                    dataOutput.write(realCommand);
                    dataOutput.flush();
                    //Bruce> Don't wait for faster speed.
                    // try {
                    // waitObject.wait();
                    // } catch (InterruptedException ie) {
                    // throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    // "Printer wait interrupted", ie);
                    // }
                } catch (IOException e) {
                    throw new JposException(JposConst.JPOS_E_ILLEGAL,
                        "I/O error occured in printing", e);
                }
            }
        }
        // ----------------------------------------------------------------------
    }

    // end of printNormal()

    public void printTwoNormal(int stations, String data1, String data2) throws JposException {
        logger.debug("printTwoNormal(" + stations + "," + data1 + "," + data2 + ")");
        try {
            if (data1.equals("") && data2.equals(""))
                return;
            String realCommand = parse.getValidCommand(stations, data1, data2);
            if (realCommand == null)
                throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid arguments!");
            if (!getClaimed())
                throw new JposException(JposConst.JPOS_E_NOTCLAIMED,
                    "Please claim the printer before manipulation!");

            if (!getDeviceEnabled())
                throw new JposException(JposConst.JPOS_E_DISABLED, "Printer is not enabled!");

            if (getAsyncMode()) {
                // Code goes here....................................................
            } else {    
                synchronized (waitObject) {
                    try {
                        dataOutput.write(realCommand);
//                        logger.debug("  ->" + realCommand);
//                        dataOutput.write("\u001B\u001BPB0" + data1);
//                        dataOutput.write(0x0d);
                        dataOutput.flush();
                        //Bruce> Don't wait for faster speed.
                        // try {
                        // waitObject.wait();
                        // } catch (InterruptedException ie) {
                        // throw new JposException(JposConst.JPOS_E_ILLEGAL,
                        // "Printer wait interrupted", ie);
                        // }
                    } catch (IOException e) {
                        throw new JposException(JposConst.JPOS_E_ILLEGAL,
                            "I/O error occured in printing", e);
                    }
                }
            }
        } catch (JposException e) {
            throw e;
        } catch (Exception e) {
            logger.error("print fail", e);
            throw new JposException(JposConst.JPOS_E_FAILURE, e.getMessage());
        }
    }// end of printTwoNormal

    public void rotatePrint(int station, int rotation) throws JposException {
        throw new JposException(JposConst.JPOS_E_FAILURE, "Not supported in PP2000!");
    }// end of rotatePrint

    public void setBitmap(int bitmapNumber, int station, String fileName, int width, int alignment)
        throws JposException {
        throw new JposException(JposConst.JPOS_E_FAILURE, "Not supported in PP2000!");
    }// end of setBitmap

    public void setLogo(int location, String data) throws JposException {
        // The logo to be set. Location may be PTR_L_TOP or PTR_L_BOTTOM.
        if (location == POSPrinterConst.PTR_L_TOP)
            toplogo = data;
        else if (location == POSPrinterConst.PTR_L_BOTTOM)
            bottomlogo = data;
        else
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "An invalid location was specified:"
                + location + ".");
    }// end of setLogo

    // transactionPrint(int station, int control) throws JposException
    public void transactionPrint(int station, int control) throws JposException {
        if (!getCapTransaction())
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Transaction mode is not supported!");
        switch (station) {
        case POSPrinterConst.PTR_S_JOURNAL:
            if (!getCapJrnPresent())
                throw new JposException(JposConst.JPOS_E_ILLEGAL, "The station:" + station
                    + " is not present");
            if (control == POSPrinterConst.PTR_TP_TRANSACTION)
                transactionJournalMode = true;
            else if (control == POSPrinterConst.PTR_TP_NORMAL)
                transactionJournalMode = false;
            else
                throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "Error occured--illegal argument:" + control);
            break;
        case POSPrinterConst.PTR_S_RECEIPT:
            if (!getCapRecPresent())
                throw new JposException(JposConst.JPOS_E_ILLEGAL, "The station:" + station
                    + " is not present");
            if (control == POSPrinterConst.PTR_TP_TRANSACTION)
                transactionReceiptMode = true;
            else if (control == POSPrinterConst.PTR_TP_NORMAL)
                transactionReceiptMode = false;
            else
                throw new JposException(JposConst.JPOS_E_ILLEGAL, "This");
            break;
        case POSPrinterConst.PTR_S_SLIP:
            if (!getCapSlpPresent())
                throw new JposException(JposConst.JPOS_E_ILLEGAL, "The station:" + station
                    + " is not present");
            if (control == POSPrinterConst.PTR_TP_TRANSACTION)
                transactionSlipMode = true;
            else if (control == POSPrinterConst.PTR_TP_NORMAL)
                transactionSlipMode = false;
            else
                throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "Error occured--illegal argument:" + control);
            break;
        default:
            throw new JposException(JposConst.JPOS_E_ILLEGAL,
                "This station is not supported by hyi.jpos.EpsonRPU420POSPrinter!");
        }
    }// end of transactionPrint()

    public void validateData(int station, String data) throws JposException {
        if (!(station == POSPrinterConst.PTR_S_JOURNAL || station == POSPrinterConst.PTR_S_RECEIPT || station == POSPrinterConst.PTR_S_SLIP))
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid station argument:" + station);
        if (data.equals(""))
            return;
        String item = null;
        String[] commonillegal = { "\u001B|rA", "\u001B|cA", "\u001B|3C", "\u001B|4C", "\u001B|rC",
            "\u001B|rvC", "\u001B|iC", "\u001B|bC" };
        ArrayList ESCSequence = parse.getESCSequence(data);
        ArrayList JposESCSequence = parse.getJposESCSequence(data);
        if (ESCSequence.isEmpty())
            return;
        if (JposESCSequence.size() != ESCSequence.size())
            throw new JposException(JposConst.JPOS_E_FAILURE,
                "Escape sequence is not supported by the printer:" + data);
        for (int k = 0; k < JposESCSequence.size(); k++) {
            item = (String)JposESCSequence.get(k);
            if (parse.contains(commonillegal, item))
                throw new JposException(JposConst.JPOS_E_FAILURE, "Not supported by the printer:"
                    + data);
            else if (item.substring(item.length() - 1).equals("B")
                || item.substring(item.length() - 2).equals("uF")
                || item.substring(item.length() - 2).equals("rF")
                || item.substring(item.length() - 2).equals("fT")
                || item.substring(item.length() - 2).equals("uC")
                || item.substring(item.length() - 2).equals("sC")
                || item.substring(item.length() - 2).equals("hC")
                || item.substring(item.length() - 2).equals("vC"))
                throw new JposException(JposConst.JPOS_E_FAILURE, "Not supported by the printer:"
                    + data);
        }
        switch (station) {
        case POSPrinterConst.PTR_S_JOURNAL:
            for (int k = 0; k < JposESCSequence.size(); k++) {
                item = (String)JposESCSequence.get(k);
                if (item.substring(item.length() - 1).equals("P")
                    || item.substring(item.length() - 2).equals("sL"))
                    throw new JposException(JposConst.JPOS_E_FAILURE,
                        "Appointed manipulation is not supported at the specified station:"
                            + "station=" + station + " string=" + data);
            }
            break;
        case POSPrinterConst.PTR_S_RECEIPT:
            for (int k = 0; k < JposESCSequence.size(); k++) {
                item = (String)JposESCSequence.get(k);
                if (item.charAt(item.length() - 1) == 'P' && item.length() != 3)
                    try {
                        if (Integer.decode(item.substring(2, item.length() - 1)).intValue() < 0
                            || Integer.decode(item.substring(2, item.length() - 1)).intValue() > 100)
                            throw new JposException(JposConst.JPOS_E_ILLEGAL,
                                "Assigned value is illegal:" + item.substring(2, item.length() - 1));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
            break;
        case POSPrinterConst.PTR_S_SLIP:
            for (int k = 0; k < JposESCSequence.size(); k++) {
                item = (String)JposESCSequence.get(k);
                if (item.substring(item.length() - 1).equals("P")
                    || item.substring(item.length() - 2).equals("sL")
                    || item.substring(item.length() - 2).equals("lF"))
                    throw new JposException(JposConst.JPOS_E_FAILURE,
                        "Appointed manipulation is not supported at the specified station:"
                            + "station=" + station + " string=" + data);
            }
            break;
        }
    }

    // The end of Service12 Methods

    // --------------------------------------------------------------------------

    // new...begin BaseService Methods supported by all device services.
    public void claim(int timeout) throws JposException {
        int innertime = timeout;
        if (innertime < -1)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Illegal timeout parameter detected:"
                + timeout + ".");
        if (claimed)
            return;
        shareOutput = ShareOutputPort.getInstance();
        if (POSPrinterentry != null)
            portName = (String)POSPrinterentry.getPropertyValue("portName");
        if (innertime == -1) {
            innertime = 2147483647;
        }
        try {
            if (!shareOutput.isSerialPortOpened())
                shareOutput.open(logicalName, portName, innertime, this);
            else {//
                String owner = shareOutput.getOwnerName();
                if (owner.equals(logicalName)) {//
                    shareOutput.open(logicalName, portName, innertime, this);
                } else {//
                    shareOutput.close();
                    shareOutput.open(logicalName, portName, innertime, this);
                }
            }//
        } catch (Exception e) {//
            throw new JposException(JposConst.JPOS_E_ILLEGAL, e.getMessage(), e);
        }
        outputStream = shareOutput.getOutputStream();
        inputStream = shareOutput.getInputStream();
        dataOutput = shareOutput.getOutputStreamWriter();
        // dataInput = shareOutput.getInputStreamReader();
        super.claim(innertime);
    }

    public void release() throws JposException {
        if (!getClaimed())
            return;
        shareOutput.close();
        outputStream = null;
        inputStream = null;
        dataOutput = null;
        super.release();
    }

    // --------------------------------------------------------------------------
    // Close the device and unlink it from the device control!
    // --------------------------------------------------------------------------

    public void close() throws JposException {
        super.close();
        if (getClaimed())
            release();
        if (getAsyncMode())
            clearOutput();
        asyncMode = false;
        EpsonRPU420POSPrinter.logicalName = null;
    }

    // The end of close()
    public void checkHealth(int level) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, this.toString());
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, this.toString());
    }

    public void open(String logicalName, EventCallbacks cb) throws JposException {
        if (cb != null)
            super.open(logicalName, cb);
        asyncMode = false;
        outputID = 0;
        EpsonRPU420POSPrinter.logicalName = logicalName;
    }

    // new...end BaseService Methods supported by all device services.

    // --------------------------------------------------------------------------
    /**
     * serialEventListenner implementation, it is in another thread, executed automatically
     */

    synchronized public void serialEvent(SerialPortEvent event) {
        if (!shareOutput.getCurrentListener().getClass().getName()
            .equals(this.getClass().getName())) {
            return;
        }

        switch (event.getEventType()) {
        case SerialPortEvent.BI:
        case SerialPortEvent.OE: // for errorEventListener callback
        case SerialPortEvent.FE: // need defined altogether with other devices */
        case SerialPortEvent.PE:
            /*
             * JPOS_ER_RETRY /Retry the asynchronous output. The error state is exited. The default.
             * JPOS_ER_CLEAR /Clear the asynchronous output or buffered input data. The error state
             * is exited.
             */
            ErrorEvent error = new ErrorEvent(this.eventCallbacks.getEventSource(), 0, 0,
                JposConst.JPOS_EL_OUTPUT, JposConst.JPOS_ER_CLEAR);
            eventCallbacks.fireErrorEvent(error);
            if (freezeEvents == false)
                state = JposConst.JPOS_S_ERROR;
            break;
        case SerialPortEvent.CD:
            break;
        case SerialPortEvent.CTS:
            break;
        case SerialPortEvent.DSR:
            break;
        case SerialPortEvent.RI:
            break;
        case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
            try {
                if (!getAsyncMode()) {
                    Thread t = new Thread() {
                        public void run() {
                            synchronized (waitObject) {
                                waitObject.notifyAll();
                            }
                        }
                    };
                    t.start();
                } else {
                    outputID++;
                }
            } catch (JposException e) {
                e.printStackTrace();
            }//
            /*
             * for outputCompleteEventListener callback Notifies the application that the queued
             * output request associated with theOutputID property has completed successfully.
             */

            /*
             * StatusUpdateEvent PTR_SUE_IDLE All asynchronous output has finished, either
             * successfully or because output has been cleared. The printer State is now
             * JPOS_S_IDLE. The FlagWhenIdle property must be true for this event to be delivered,
             * and the property is automatically reset to false just before the event is delivered.
             */
            if (!freezeEvents)
                state = JposConst.JPOS_S_BUSY;
            break;

        case SerialPortEvent.DATA_AVAILABLE:
            synchronized (waitForPrinterStatus) {
                try {
                    char ch = 0;
                    while (inputStream.available() > 0) {
                        ch = (char)inputStream.read();
                        if (ch == 'S' && buffer.length() > 0) { // first char of response
                            printerStatus = null;
                            buffer.setLength(0);
                        }
                        buffer.append(ch);
                    }
                    if (buffer.length() == 0) {
                        logger.info("Printer status code is null!");
                    } else if ((int)ch == 10) { // end char of response
                        printerStatus = buffer.toString();
                    }
                } catch (IOException e) {
                    logger.error("Data status failed", e);
                } finally {
                    if (printerStatus != null) {
                        waitForPrinterStatus.notifyAll();
                    }
                }
            }
            break;
        default:
            break;

        }
    }

    // end of serialEvent()

    // --------------------------------------------------------------------
    public void deleteInstance() throws JposException {
    }

    // Following methods are introduced in jpos1.5, and they have not been implemented yet in this
    // version.
    public void markFeed(int type) throws JposException {
    }

    public void changePrintSide(int side) throws JposException {
        ;
    }

    public int getSlpPrintSide() throws JposException {
        return 0;
    }

    public void setSlpCurrentCartridge(int arg) throws JposException {

    }

    public int getSlpCurrentCartridge() throws JposException {
        return 0;
    }

    public int getSlpCartridgeState() throws JposException {
        return 0;
    }

    public void setRecCurrentCartridge(int arg) throws JposException {

    }

    public int getRecCurrentCartridge() throws JposException {
        return 0;
    }

    public int getRecCartridgeState() throws JposException {
        return 0;
    }

    public int getCapJrnCartridgeSensor() throws JposException {
        return 0;
    }

    public int getCapJrnColor() throws JposException {
        return 0;
    }

    public int getCapRecCartridgeSensor() throws JposException {
        return 0;
    }

    public int getCapRecColor() throws JposException {
        return 0;
    }

    public int getCapRecMarkFeed() throws JposException {
        return 0;
    }

    public boolean getCapSlpBothSidesPrint() throws JposException {
        return false;
    }

    public int getCapSlpCartridgeSensor() throws JposException {
        return 0;
    }

    public int getCapSlpColor() throws JposException {
        return 0;
    }

    public int getCartridgeNotify() throws JposException {
        return 0;
    }

    public void setCartridgeNotify(int p0) throws JposException {

    }

    public int getJrnCartridgeState() throws JposException {
        return 0;
    }

    public int getJrnCurrentCartridge() throws JposException {
        return 0;
    }

    public void setJrnCurrentCartridge(int p0) throws JposException {
    }

    public void clearPrintArea() throws JposException {
    }

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapConcurrentPageMode() throws JposException {
        return false;
    }

    public boolean getCapRecPageMode() throws JposException {
        return false;
    }

    public boolean getCapSlpPageMode() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public String getPageModeArea() throws JposException {
        return null;
    }

    public int getPageModeDescriptor() throws JposException {
        return 0;
    }

    public int getPageModeHorizontalPosition() throws JposException {
        return 0;
    }

    public String getPageModePrintArea() throws JposException {
        return null;
    }

    public int getPageModePrintDirection() throws JposException {
        return 0;
    }

    public int getPageModeStation() throws JposException {
        return 0;
    }

    public int getPageModeVerticalPosition() throws JposException {
        return 0;
    }

    public void pageModePrint(int control) throws JposException {
    }

    public void setPageModeHorizontalPosition(int position) throws JposException {
    }

    public void setPageModePrintArea(String area) throws JposException {
    }

    public void setPageModePrintDirection(int direction) throws JposException {
    }

    public void setPageModeStation(int station) throws JposException {
    }

    public void setPageModeVerticalPosition(int position) throws JposException {
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }

    public boolean getCapMapCharacterSet() throws JposException {
        return false;
    }

    public boolean getMapCharacterSet() throws JposException {
        return false;
    }

    public String getRecBitmapRotationList() throws JposException {
        return null;
    }

    public String getSlpBitmapRotationList() throws JposException {
        return null;
    }

    public void setMapCharacterSet(boolean mapCharacterSet) throws JposException {
    }
}