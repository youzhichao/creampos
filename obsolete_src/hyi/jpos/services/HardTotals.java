package hyi.jpos.services;

import jpos.JposException;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.services.EventCallbacks;
import jpos.services.HardTotalsService14;

/**
 */
public class HardTotals implements HardTotalsService14 {
    private JposEntry entry                         = null;
                          
    // Capabilities
    private boolean capErrorDetection = false;
    private boolean capSingleFile = false;
    private boolean capTransactions = false;
    private int capPowerReporting = 0;    

    // Properties
    private String checkHealthText = ""; 
    private boolean claimed = false;   
    private boolean deviceEnabled = false;  
    private String deviceServiceDescription = ""; 
    private int deviceServiceVersion = 0;   
    private boolean freezeEvents = false;    
    private String physicalDeviceDescription = "";  
    private String physicalDeviceName = "";      
    private int state = 0;               
    private int freeData = 0;         
    private int numberOfFiles = 0;       
    private int totalsSize = 0;         
    private boolean transactionInProgress = false;    
    private int powerNotify = 0;           
    private int powerState = 0;

    /**
     * Constructor
     */
    public HardTotals(SimpleEntry entry) {
        this.entry = entry;
    }

    // Capabilities
    public boolean getCapErrorDetection() throws JposException {
        return capErrorDetection;
    }

    public boolean getCapSingleFile() throws JposException {
        return capSingleFile;
    }

    public boolean getCapTransactions() throws JposException {
        return capTransactions;
    }

    public int     getCapPowerReporting() throws JposException {
        return capPowerReporting;
    }

    // Properties
    public String  getCheckHealthText() throws JposException {
        return checkHealthText;
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    public void    setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        this.deviceEnabled = deviceEnabled;
    }

    public String  getDeviceServiceDescription() throws JposException {
        return deviceServiceDescription;
    }

    public int     getDeviceServiceVersion() throws JposException {
        return deviceServiceVersion;
    }

    public boolean getFreezeEvents() throws JposException {
        return freezeEvents;
    }

    public void    setFreezeEvents(boolean freezeEvents) throws JposException {
        this.freezeEvents = freezeEvents;
    }

    public String  getPhysicalDeviceDescription() throws JposException {
        return physicalDeviceDescription;
    }

    public String  getPhysicalDeviceName() throws JposException {
        return physicalDeviceName;
    }

    public int     getState() throws JposException {
        return state;
    }

    public int     getFreeData() throws JposException {
        return freeData;
    }

    public int     getNumberOfFiles() throws JposException {
        return numberOfFiles;
    }

    public int     getTotalsSize() throws JposException {
        return totalsSize;
    }

    public boolean getTransactionInProgress() throws JposException {
        return transactionInProgress;
    }

    public int     getPowerNotify() throws JposException {
        return powerNotify;
    }

    public void    setPowerNotify(int powerNotify) throws JposException {
        this.powerNotify = powerNotify;
    }

    public int     getPowerState() throws JposException {
        return powerState;
    }

    // Methods                                            
    public void deleteInstance() throws JposException {
    }
    
    public void    claim(int timeout) throws JposException {
    }

    public void    close() throws JposException {
    }

    public void    checkHealth(int level) throws JposException {
    }

    public void    directIO(int command, int[] data, Object object)
                       throws JposException {
    }

    public void    open(String logicalName, EventCallbacks cb)
                        throws JposException {
    }

    public void    release() throws JposException {
    }

    public void    beginTrans() throws JposException {
    }

    public void    claimFile(int hTotalsFile, int timeout)
                       throws JposException {
    }

    public void    commitTrans() throws JposException {
    }

    public void    create(String fileName, int[] hTotalsFile, int size,
                       boolean errorDetection) throws JposException {
    }

    public void    delete(String fileName) throws JposException {
    }

    public void    find(String fileName, int[] hTotalsFile, int[] size)
                       throws JposException {
    }

    public void    findByIndex(int index, String[] fileName)
                       throws JposException {
    }

    public void    read(int hTotalsFile, byte[] data, int offset,
                       int count) throws JposException {
    }

    public void    recalculateValidationData(int hTotalsFile)
                       throws JposException {
    }

    public void    releaseFile(int hTotalsFile) throws JposException {
    }

    public void    rename(int hTotalsFile, String fileName)
                       throws JposException {
    }

    public void    rollback() throws JposException {
    }

    public void    setAll(int hTotalsFile, byte value) throws JposException {
    }

    public void    validateData(int hTotalsFile) throws JposException {
    }

    public void    write(int hTotalsFile, byte[] data, int offset, int count)
                       throws JposException {
    }
}

