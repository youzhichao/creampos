package hyi.jpos.services;

import java.util.Enumeration;

import jpos.JposConst;
import jpos.JposException;
import jpos.LineDisplayConst;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.services.EventCallbacks;
import jpos.services.LineDisplayService19;

import org.apache.log4j.Logger;

public class VirtualLineDisplay extends AbstractDeviceService19 implements LineDisplayService19 {

    static Logger logger = Logger.getLogger(VirtualLineDisplay.class);

    static private Object mutex = new Object();
    static private char[] ch = new char[40];

    private boolean capDescriptors = false;
    private int characterSet = 0;
    private String characterSetList = "";
    private boolean claimed = false;
    private int columns = 0;
    private int currentWindow = 0;
    private int cursorColumn = 0;
    private int cursorRow = 0;
    private boolean cursorUpdate = false;
    private int deviceBrightness = 0;
    private int deviceColumns = 0;
    private int deviceDescriptors = 0;
    private boolean deviceEnabled = false;
    private int deviceRows = 0;
    private int deviceWindows = 0;
    private int interCharacterWait = 0;
    private int marqueeFormat = 0;
    private int marqueeRepeatWait = 0;
    private int marqueeType = 0;
    private int marqueeUnitWait = 0;
    private int rows = 0;
    // private boolean opened = false;

    protected String checkHealthText = "";
    protected String deviceServiceDescription = "JavaPOS Service from HYI Corporation";
    protected int deviceServiceVersion = 1006000;
    protected boolean freezeEvents = false;
    protected String physicalDeviceDescription = "";
    protected String physicalDeviceName = "";
    protected int state = JposConst.JPOS_S_CLOSED;
    protected int powerNotify = JposConst.JPOS_PN_DISABLED;
    protected int powerState = JposConst.JPOS_PS_UNKNOWN;

    protected EventCallbacks eventCallbacks = null;
    protected JposEntry entry = null;

    static Enumeration portList;
//    static CommPortIdentifier portId;
//    static SerialPort serialPort;
//    static OutputStream outputStream;

    /**
     * Constructor
     */
    public VirtualLineDisplay(SimpleEntry entry) {
        this.entry = entry;
    }

    // Capabilities
    public int getCapBlink() throws JposException {
        return LineDisplayConst.DISP_CB_NOBLINK;
    }

    public boolean getCapBrightness() throws JposException {
        return false;
    }

    public int getCapCharacterSet() throws JposException {
        return LineDisplayConst.DISP_CCS_ASCII;
    }

    public boolean getCapDescriptors() throws JposException {
        return capDescriptors;
    }

    public boolean getCapHMarquee() throws JposException {
        return true;
    }

    public boolean getCapICharWait() throws JposException {
        return false;
    }

    public boolean getCapVMarquee() throws JposException {
        return false;
    }

    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties
    public String getCheckHealthText() throws JposException {
        return checkHealthText;
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        if (claimed) {
            this.deviceEnabled = deviceEnabled;
        } else {
            logger.debug("lineDisplay error: must claim it first");
        }
    }

    public String getDeviceServiceDescription() throws JposException {
        return deviceServiceDescription;
    }

    public int getDeviceServiceVersion() throws JposException {
        return deviceServiceVersion;
    }

    public boolean getFreezeEvents() throws JposException {
        return freezeEvents;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        this.freezeEvents = freezeEvents;
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return physicalDeviceDescription;
    }

    public String getPhysicalDeviceName() throws JposException {
        return physicalDeviceName;
    }

    public int getState() throws JposException {
        return state;
    }

    public int getCharacterSet() throws JposException {
        return characterSet;
    }

    public void setCharacterSet(int characterSet) throws JposException {
        this.characterSet = characterSet;
    }

    public String getCharacterSetList() throws JposException {
        return characterSetList;
    }

    public int getColumns() throws JposException {
        return columns;
    }

    public int getCurrentWindow() throws JposException {
        return currentWindow;
    }

    public void setCurrentWindow(int currentWindow) throws JposException {
        this.currentWindow = currentWindow;
    }

    public int getCursorColumn() throws JposException {
        return cursorColumn;
    }

    public void setCursorColumn(int cursorColumn) throws JposException {
        this.cursorColumn = cursorColumn;
    }

    public int getCursorRow() throws JposException {
        return cursorRow;
    }

    public void setCursorRow(int cursorRow) throws JposException {
        this.cursorRow = cursorRow;
    }

    public boolean getCursorUpdate() throws JposException {
        return cursorUpdate;
    }

    public void setCursorUpdate(boolean cursorUpdate) throws JposException {
        this.cursorUpdate = cursorUpdate;
    }

    public int getDeviceBrightness() throws JposException {
        return deviceBrightness;
    }

    public void setDeviceBrightness(int deviceBrightness) throws JposException {
        this.deviceBrightness = deviceBrightness;
    }

    public int getDeviceColumns() throws JposException {
        return deviceColumns;
    }

    public int getDeviceDescriptors() throws JposException {
        return deviceDescriptors;
    }

    public int getDeviceRows() throws JposException {
        return deviceRows;
    }

    public int getDeviceWindows() throws JposException {
        return deviceWindows;
    }

    public int getInterCharacterWait() throws JposException {
        return interCharacterWait;
    }

    public void setInterCharacterWait(int interCharacterWait) throws JposException {
        this.interCharacterWait = interCharacterWait;
    }

    public int getMarqueeFormat() throws JposException {
        return marqueeFormat;
    }

    public void setMarqueeFormat(int marqueeFormat) throws JposException {
        this.marqueeFormat = marqueeFormat;
    }

    public int getMarqueeRepeatWait() throws JposException {
        return marqueeRepeatWait;
    }

    public void setMarqueeRepeatWait(int marqueeRepeatWait) throws JposException {
        this.marqueeRepeatWait = marqueeRepeatWait;
    }

    public int getMarqueeType() throws JposException {
        return marqueeType;
    }

    public void setMarqueeType(int marqueeType) throws JposException {
        this.marqueeType = marqueeType;
    }

    public int getMarqueeUnitWait() throws JposException {
        return marqueeUnitWait;
    }

    public void setMarqueeUnitWait(int marqueeUnitWait) throws JposException {
        this.marqueeUnitWait = marqueeUnitWait;
    }

    public int getRows() throws JposException {
        return rows;
    }

    public int getPowerNotify() throws JposException {
        return powerNotify;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        this.powerNotify = powerNotify;
    }

    public int getPowerState() throws JposException {
        return powerState;
    }

    public void connect() {
        logger.debug("VirtualLineDisplay connect success!");
    }

    // Methods supported by all device services.
    public void claim(int timeout) throws JposException {
        logger.debug("lineDisplay claim success");
    }

    public void close() throws JposException {
        claimed = false;
        deviceEnabled = false;
        // opened = false;
    }

    public void checkHealth(int level) throws JposException {
    }

    public void deleteInstance() throws JposException {
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
    }

    public void open(String logicalName, EventCallbacks cb) throws JposException {
        eventCallbacks = cb;
        this.setCursorColumn(0);
        this.setCursorRow(0);
        String str = "                                        ";
        ch = str.toCharArray();
        // opened = true;
        claimed = false;
        deviceEnabled = false;
    }

    synchronized public void release() throws JposException {
            portList = null;
            claimed = false;
            deviceEnabled = false;
            mutex.notifyAll();
    }

    public void clearDescriptors() throws JposException {
        if (!capDescriptors) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL);
        } else {
            deviceServiceDescription = "";
            physicalDeviceDescription = "";
        }
    }

    public void clearText() throws JposException {
        if (deviceEnabled) {
            setCursorColumn(0);
            setCursorRow(0);
            String str = "                                        ";
            ch = str.toCharArray();
        } else {
            logger.debug("lineDisplay error : Not DeviceEnabled");
        }
    }

    public void createWindow(int viewportRow, int viewportColumn, int viewportHeight,
        int viewportWidth, int windowHeight, int windowWidth) throws JposException {
    }

    public void destroyWindow() throws JposException {
    }

    public void displayText(String data, int attribute) throws JposException {
        logger.debug("output:[" + data + "]");
        claimed = true;
        deviceEnabled = true;
    }

    public void displayTextAt(int y, int x, String data, int attribute) throws JposException {
        logger.debug("output(" + x + "," + y + "):[" + data + "]");
        claimed = true;
        deviceEnabled = true;
    }

    public void refreshWindow(int window) throws JposException {
    }

    public void scrollText(int direction, int units) throws JposException {
    }

    public void setDescriptor(int descriptor, int attribute) throws JposException {
    }

    // Capabilities
    public boolean getCapBlinkRate() throws JposException {
        return false;
    }

    public int getCapCursorType() throws JposException {
        return 0;
    }

    public boolean getCapCustomGlyph() throws JposException {
        return false;
    }

    public int getCapReadBack() throws JposException {
        return 0;
    }

    public int getCapReverse() throws JposException {
        return 0;
    }

    // Properties
    public int getBlinkRate() throws JposException {
        return 0;
    }

    public void setBlinkRate(int blinkRate) throws JposException {
    }

    public int getCursorType() throws JposException {
        return 0;
    }

    public void setCursorType(int cursorType) throws JposException {
    }

    public String getCustomGlyphList() throws JposException {
        return "";
    }

    public int getGlyphHeight() throws JposException {
        return 0;
    }

    public int getGlyphWidth() throws JposException {
        return 0;
    }

    // Methods
    public void defineGlyph(int glyphCode, byte[] glyph) throws JposException {
    }

    public void readCharacterAtCursor(int[] aChar) throws JposException {
    }

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }

    public void displayBitmap(String fileName, int width, int alignmentX, int alignmentY)
        throws JposException {
    }

    public boolean getCapBitmap() throws JposException {
        return false;
    }

    public boolean getCapMapCharacterSet() throws JposException {
        return false;
    }

    public boolean getCapScreenMode() throws JposException {
        return false;
    }

    public boolean getMapCharacterSet() throws JposException {
        return false;
    }

    public int getMaximumX() throws JposException {
        return 0;
    }

    public int getMaximumY() throws JposException {
        return 0;
    }

    public int getScreenMode() throws JposException {
        return 0;
    }

    public String getScreenModeList() throws JposException {
        return null;
    }

    public void setBitmap(int bitmapNumber, String fileName, int width, int alignmentX,
        int alignmentY) throws JposException {
    }

    public void setMapCharacterSet(boolean mapCharacterSet) throws JposException {
    }

    public void setScreenMode(int screenMode) throws JposException {
    }
    
}
