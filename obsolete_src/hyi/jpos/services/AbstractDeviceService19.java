package hyi.jpos.services;

import jpos.*;
import jpos.services.*;

/**
 * DeviceService implements BaseService to define all of the common part needed
 * by the device services.
 * 
 * @author Bruce You @ Hongyuan Software
 * @since 2007-2-13
 */
public abstract class AbstractDeviceService19 implements BaseService {

    protected String checkHealthText = "";
    protected boolean claimed;
    protected boolean deviceEnabled;
    protected String deviceServiceDescription = "";
    protected int deviceServiceVersion;
    protected boolean freezeEvents;
    protected String physicalDeviceDescription = "";
    protected String physicalDeviceName = "";
    protected int state = JposConst.JPOS_S_CLOSED;
    protected EventCallbacks eventCallbacks;

    public AbstractDeviceService19() {
    }

    // Common properties...

    public String getCheckHealthText() throws JposException {
        return checkHealthText;
    }

    protected void setCheckHealthText(String checkHealthText) {
        this.checkHealthText = checkHealthText;
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    protected void setClaimed(boolean b) {
        claimed = b;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        this.deviceEnabled = deviceEnabled;
    }

    public String getDeviceServiceDescription() throws JposException {
        return deviceServiceDescription;
    }

    protected void setDeviceServiceDescription(String s) {
        deviceServiceDescription = s;
    }

    public int getDeviceServiceVersion() throws JposException {
        return deviceServiceVersion;
    }

    public boolean getFreezeEvents() throws JposException {
        return freezeEvents;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        this.freezeEvents = freezeEvents;
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return physicalDeviceDescription;
    }

    public void setPhysicalDeviceDescription(String s) {
        physicalDeviceDescription = s;
    }

    public String getPhysicalDeviceName() throws JposException {
        return physicalDeviceName;
    }

    public void setPhysicalDeviceName(String s) {
        physicalDeviceName = s;
    }

    public int getState() throws JposException {
        return state;
    }

    protected void setState(int s) {
        state = s;
    }

    public void claim(int timeout) throws JposException {
        claimed = true;
    }

    public void release() throws JposException {
        claimed = false;
    }

    public void close() throws JposException {
        if (getClaimed())
            release();
        checkHealthText = "";
        claimed = false;
        deviceEnabled = false;
        freezeEvents = false;
        state = JposConst.JPOS_S_CLOSED;
        eventCallbacks = null;
    }

    public void open(String logicalName, EventCallbacks cb) throws JposException {
        eventCallbacks = cb;
        checkHealthText = "Well now!";
        claimed = false;
        deviceEnabled = false;
        deviceServiceVersion = 1009000;
        freezeEvents = false;
        state = JposConst.JPOS_S_IDLE;
    }
}
