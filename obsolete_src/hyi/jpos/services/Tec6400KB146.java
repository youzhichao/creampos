
// Copyright (c) 2000 HYI
package hyi.jpos.services;

import hyi.cream.POSButtonHome;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.DacTransfer;
import hyi.cream.state.GetProperty;
import hyi.cream.uibeans.DacViewer;
import hyi.cream.uibeans.ItemList;
import hyi.cream.uibeans.PayingPaneBanner;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.uibeans.SignOffForm;
import hyi.cream.util.CreamToolkit;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import jpos.JposConst;
import jpos.JposException;
import jpos.POSKeyboard;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.events.DataEvent;
import jpos.services.EventCallbacks;
import jpos.services.POSKeyboardService14;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class Tec6400KB146 implements POSKeyboardService14, KeyListener, ContainerListener {

    static private POSKeyboard claimedControl;
    static private Object mutex = new Object();

    protected EventCallbacks eventCallbacks      = null;
    protected JposEntry entry                    = null;
    protected Container comp                     = null;
    protected Container comp2                    = null;
    protected Container comp3                    = null;
    protected Container comp4                    = null;

    private boolean capDescriptors               = false;
    private boolean claimed                      = false;
    private int deviceDescriptors                = 0;
    private boolean deviceEnabled                = false;

    protected int posKeyData                     = 0;
    protected String checkHealthText             = "";
    protected String deviceServiceDescription    = "JavaPOS Service from HYI Corporation";
    protected int deviceServiceVersion           = 1005000;
    protected boolean freezeEvents               = false;
    protected String physicalDeviceDescription   = "";
    protected String physicalDeviceName          = "";
    protected int state                          = JposConst.JPOS_S_CLOSED;
    protected int powerNotify                    = JposConst.JPOS_PN_DISABLED;
    protected int powerState                     = JposConst.JPOS_PS_UNKNOWN;

    private int prompt;
    private Map keyMap = new HashMap();
    private int scannerPrefixCharValue;


    /**
     * Constructor
     */
    public Tec6400KB146(SimpleEntry entry) {
        this.entry = entry;

        keyMap.put("Shift-F1", new Integer(159));
        keyMap.put("Shift-F2", new Integer(143));
        keyMap.put("Shift-F3", new Integer(142));
        keyMap.put("Shift-F4", new Integer(158));
        keyMap.put("Shift-F5", new Integer(157));
        keyMap.put("Shift-F6", new Integer(141));
        keyMap.put("Shift-F7", new Integer(156));
        keyMap.put("Shift-F8", new Integer(140));
        keyMap.put("Shift-F9", new Integer(139));
        keyMap.put("Shift-F10", new Integer(155));
        keyMap.put("Shift-F11", new Integer(138));
        keyMap.put("Shift-F12", new Integer(154));
        keyMap.put("Alt-K", new Integer(152));
        keyMap.put("Alt-L", new Integer(80));

        keyMap.put("F1", new Integer(175));
        keyMap.put("F2", new Integer(191));
        keyMap.put("F3", new Integer(190));
        keyMap.put("F4", new Integer(174));
        keyMap.put("F5", new Integer(173));
        keyMap.put("F6", new Integer(188));
        keyMap.put("F7", new Integer(172));
        keyMap.put("F8", new Integer(171));
        keyMap.put("F9", new Integer(170));
        keyMap.put("F10", new Integer(153));
        keyMap.put("F11", new Integer(137));
        keyMap.put("F12", new Integer(168));
        keyMap.put("Shift-Minus", new Integer(160));
        keyMap.put("Alt-W", new Integer(114));

        keyMap.put("Shift-1", new Integer(111));
        keyMap.put("Shift-2", new Integer(110));
        keyMap.put("Shift-3", new Integer(109));
        keyMap.put("Shift-4", new Integer(189));
        keyMap.put("Alt-X", new Integer(108));
        keyMap.put("Shift-6", new Integer(107));
        keyMap.put("Shift-7", new Integer(187));
        keyMap.put("Shift-8", new Integer(186));
        keyMap.put("Shift-9", new Integer(169));
        keyMap.put("Shift-0", new Integer(185));
        keyMap.put("Alt-M", new Integer(184));
        keyMap.put("Alt-N", new Integer(176));
        keyMap.put("Alt-O", new Integer(129));
        keyMap.put("Alt-P", new Integer(115));

        keyMap.put("Alt-A", new Integer(95));
        keyMap.put("Alt-B", new Integer(94));
        keyMap.put("Alt-C", new Integer(93));
        keyMap.put("Alt-D", new Integer(92));
        keyMap.put("Alt-E", new Integer(91));
        keyMap.put("Alt-F", new Integer(90));
        keyMap.put("Alt-G", new Integer(106));
        keyMap.put("Alt-H", new Integer(105));
        keyMap.put("Alt-I", new Integer(104));
        keyMap.put("Alt-J", new Integer(88));
        keyMap.put("Minus", new Integer(96));
        keyMap.put("=", new Integer(177));
        keyMap.put("Shift-=", new Integer(130));
        keyMap.put("Shift-\\", new Integer(131));

        keyMap.put("Q", new Integer(79));
        keyMap.put("W", new Integer(78));
        keyMap.put("E", new Integer(77));
        keyMap.put("R", new Integer(76));
        keyMap.put("T", new Integer(75));
        keyMap.put("Y", new Integer(74));
        keyMap.put("U", new Integer(89));
        keyMap.put("I", new Integer(72));
        keyMap.put("O", new Integer(64));
        keyMap.put("P", new Integer(65));
        keyMap.put("Shift-Q", new Integer(178));
        keyMap.put("Shift-U", new Integer(162));
        keyMap.put("Shift-V", new Integer(163));
        keyMap.put("Shift-W", new Integer(116));


        keyMap.put("A", new Integer(63));
        keyMap.put("S", new Integer(62));
        keyMap.put("D", new Integer(61));
        keyMap.put("F", new Integer(60));
        keyMap.put("G", new Integer(59));
        keyMap.put("H", new Integer(58));
        keyMap.put("J", new Integer(73));
        keyMap.put("K", new Integer(56));
        keyMap.put("L", new Integer(48));
        keyMap.put("Shift-;", new Integer(49));
        keyMap.put("Alt-Q", new Integer(97));
        keyMap.put("Shift-Quote", new Integer(146));
        keyMap.put("Quote", new Integer(165));
        keyMap.put("Ctrl-1", new Integer(147));

        keyMap.put("Z", new Integer(47));
        keyMap.put("X", new Integer(46));
        keyMap.put("C", new Integer(45));
        keyMap.put("V", new Integer(44));
        keyMap.put("B", new Integer(43));
        keyMap.put("N", new Integer(42));
        keyMap.put("M", new Integer(57));
        keyMap.put("Shift-,", new Integer(41));
        keyMap.put("Shift-.", new Integer(40));
        keyMap.put(",", new Integer(32));
        keyMap.put("Shift-/", new Integer(33));
        keyMap.put("/", new Integer(161));
        keyMap.put("Alt-Z", new Integer(179));
        keyMap.put("Alt-R", new Integer(145));

        keyMap.put("Escape", new Integer(98));
        keyMap.put("Shift-A", new Integer(83));
        keyMap.put("Shift-B", new Integer(67));
        keyMap.put("Shift-C", new Integer(66));
        keyMap.put("Shift-D", new Integer(68));
        keyMap.put("Shift-E", new Integer(69));
        keyMap.put("Shift-F", new Integer(70));
        keyMap.put("Page Up", new Integer(71));
        keyMap.put("Shift-G", new Integer(99));
        keyMap.put("Shift-Backspace", new Integer(128));
        keyMap.put("Shift-J", new Integer(100));
        keyMap.put("Shift-K", new Integer(84));
        keyMap.put("Shift-L", new Integer(85));
        keyMap.put("Shift-M", new Integer(86));
        keyMap.put("Page Down", new Integer(87));
        keyMap.put("Shift-N", new Integer(117));
        keyMap.put("1", new Integer(121));
        keyMap.put("2", new Integer(126));
        keyMap.put("3", new Integer(120));
        keyMap.put("4", new Integer(124));
        keyMap.put("5", new Integer(127));
        keyMap.put("6", new Integer(112));
        keyMap.put("7", new Integer(123));
        keyMap.put("8", new Integer(122));
        keyMap.put("9", new Integer(113));
        keyMap.put("Shift-O", new Integer(180));
        keyMap.put("Shift-P", new Integer(101));
        keyMap.put("Up", new Integer(102));
        keyMap.put("Shift-R", new Integer(103));
        keyMap.put("Shift-S", new Integer(133));
        keyMap.put("Shift-T", new Integer(148));
        keyMap.put("Left", new Integer(181));
        keyMap.put("Down", new Integer(182));
        keyMap.put("Right", new Integer(183));
        keyMap.put("Ctrl-2", new Integer(132));
        keyMap.put("Shift-X", new Integer(36));
        keyMap.put("Shift-Y", new Integer(149));
        keyMap.put("Alt-S", new Integer(151));
        keyMap.put("Alt-Y", new Integer(144));
        keyMap.put("Shift-H", new Integer(125));
        keyMap.put("Alt-T", new Integer(119));
        keyMap.put(".", new Integer(118));
        keyMap.put("Alt-U", new Integer(166));
        keyMap.put("Alt-V", new Integer(164));
        keyMap.put("Enter", new Integer(167));
    }

    // Capabilities
    public boolean getCapKeyUp() throws JposException {
        return true;
    }

    public int getCapPowerReporting() throws JposException {
        return 0;
    }

    // Properties
    public boolean getAutoDisable() throws JposException {
        return true;
    }

    public void setAutoDisable(boolean autoDisable) throws JposException {
    }

    public int getDataCount() throws JposException {
        return 0;
    }

    public boolean getDataEventEnabled() throws JposException {
        return true;
    }

    public void setDataEventEnabled(boolean dataEventEnabled)
                       throws JposException {
    }

    public int getEventTypes() throws JposException {
        return 0;
    }

    public void setEventTypes(int eventTypes) throws JposException {
    }

    public int getPOSKeyData() throws JposException {
        return posKeyData;
    }

    public int getPOSKeyEventType() throws JposException {
        return 0;
    }

    public int getPowerNotify() throws JposException {
        return 0;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
    }

    public int getPowerState() throws JposException {
        return 0;
    }

    public String getCheckHealthText() throws JposException {
        return "";
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    private boolean isActive = false;
    private boolean fkey = false;
    private String fkeyString = "";
    private HashMap fkeys = new HashMap();

    public void setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        if (!claimed) {
            System.out.println("must claim it first");
            return;
        }
        this.deviceEnabled = deviceEnabled;
        if (deviceEnabled) {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                adkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                adkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                adkl(comp3);
            }
            if (entry.hasPropertyWithName("COMP4_CLASS_PROP_NAME")) {
                String className4 = (String)entry.getPropertyValue("COMP4_CLASS_PROP_NAME");
                comp4 = createComp(className4);
                adkl(comp4);
            }
        } else {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                rmkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                rmkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                rmkl(comp3);
            }
            if (entry.hasPropertyWithName("COMP4_CLASS_PROP_NAME")) {
                String className4 = (String)entry.getPropertyValue("COMP4_CLASS_PROP_NAME");
                comp4 = createComp(className4);
                rmkl(comp4);
            }
        }
    }

    public String getDeviceServiceDescription() throws JposException {
        return "";
    }

    public int getDeviceServiceVersion() throws JposException {
        return 0;
    }

    public boolean getFreezeEvents() throws JposException {
        return true;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return "";
    }

    public String getPhysicalDeviceName() throws JposException {
        return "";
    }

    public int getState() throws JposException {
        return 0;
    }

    // Methods
    public void clearInput() throws JposException {
    }

    /**
     * Indicate the ServiceInstance that its connection has been dropped
     * @since 0.1 (Philly 99 meeting)
     * @throws jpos.JposException if any error occurs
     */
    public void deleteInstance() throws JposException {
    }

    //--------------------------------------------------------------------------
    // Common overridden methods
    //

    public void open(String logicalName, EventCallbacks eventCallbacks)
                    throws JposException {
        this.eventCallbacks = eventCallbacks;
        claimed = false;
        deviceEnabled = false;

        scannerPrefixCharValue = Integer.parseInt(
            GetProperty.getScannerPrefixCharValue("32"));
    }

    public void close() throws JposException {
        claimed = false;
        deviceEnabled = false;
        claimedControl = null;
    }

    public void claim(int timeout) throws JposException {
        if (claimed) {
            System.out.println("device has been claimed");
            return;
        }

        synchronized (mutex) {
            if (claimedControl == null) {
                Object obj = eventCallbacks.getEventSource();
                claimedControl = (POSKeyboard)obj;
                claimed = true;
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                    claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                    claimed = true;
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        if (claimedControl == null) {
                            claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                            claimed = true;
                            return;
                        }
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        }
        claimed = true;
    }

    public void checkHealth(int level) throws JposException {
    }

    public void directIO(int command, int[] data, Object object)
                       throws JposException {
    }

    public void release() throws JposException {
        synchronized (mutex) {
            if (!claimed) {
                System.out.println("device has been release");
            }
            claimed = false;
            deviceEnabled = false;
            claimedControl = null;
            mutex.notifyAll();
        }
    }



    //event
    public void keyReleased(KeyEvent e) {
    }

    private boolean checked = false;
    private boolean twoCode = false;
    private int keyCode = 0;
    private String buttonCode = "";
    private boolean keylock = false;
    private boolean scanner = false;
    private int enterButtonCount = 0;
    public void keyPressed(KeyEvent e) {

        keyCode = e.getKeyCode();

        if (checked) {
        /*
            System.out.println("   ****************************************");
            System.out.println("   *** KeyCode = " + keyCode + " ***");
            System.out.println("   ****************************************");
            //return;*/
        }

        if (keyCode == 48 && e.getModifiers() == 0 && !ARKScanner.keyStart) {
            keylock = true;
            return;
        } else if (keylock) {
            if (keyCode == 10) {
                keylock = false;
            }
            return;
        } else if (keyCode == scannerPrefixCharValue) {
            scanner = true;
            //System.out.println("Tec6400KB146 --> scanner is true");
            return;
        } else if (scanner) {
            if (keyCode == 10) {
            //System.out.println("Tec6400KB146 --> scanner is false");
                scanner = false;
            }
            return;
        } else {
            if (e.getModifiers() == 0) {
                buttonCode = KeyEvent.getKeyText(keyCode);
            } else {
                buttonCode = KeyEvent.getKeyModifiersText(e.getModifiers()) + "-" + KeyEvent.getKeyText(keyCode);
            }
            //System.out.println("**************************");
//            System.out.println("buttonCode = " + buttonCode);
            //System.out.println();
        }

        if (keyMap.containsKey(buttonCode)) {
            posKeyData = ((Integer)keyMap.get(buttonCode)).intValue();
        } else {
            return;
        }

        if (posKeyData == POSButtonHome.getInstance().getEnterCode()) {
            enterButtonCount++;
            if (enterButtonCount == 15) {
                CreamToolkit.logMessage("System is going to shutdown caused by pressing many Enter.");
                DacTransfer.shutdown(1);
            }
        } else {
            enterButtonCount = 0;
        }

        POSTerminalApplication app = POSTerminalApplication.getInstance();
        PopupMenuPane pop = app.getPopupMenuPane();
        SignOffForm signOffForm = app.getSignOffForm();
        ItemList itemList = app.getItemList();
        DacViewer dacViewer = app.getDacViewer();
        PayingPaneBanner payingPane = app.getPayingPane();

        //int pageUp = POSTerminalApplication.getInstance().getPOSButtonHome().getPageUpCode();
        //int pageDown = POSTerminalApplication.getInstance().getPOSButtonHome().getPageDownCode();

//        System.out.println("Tec6400KB146 **** you get key code = " + posKeyData + " ****");

        if (signOffForm != null && signOffForm.isVisible()) {
            signOffForm.keyDataListener(posKeyData);
            return;
        } else if (pop.isVisible()) {
            pop.keyDataListener(posKeyData);
            return;
        } else if (itemList.isVisible()) {
            itemList.keyDataListener(posKeyData);
        } else if (payingPane.isVisible()) {
            payingPane.keyDataListener(posKeyData);
        }
        if (dacViewer.isVisible())
            dacViewer.keyDataListener(posKeyData);
//        System.out.println("    **** you get key code = " + posKeyData + " ****");
        eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
    }

    synchronized public void keyTyped(KeyEvent e) {
    }

    public Container createComp(String className) {
        try {
            Class compClass = Class.forName(className);
            Method compMethod = compClass.getDeclaredMethod("getInstance",
                                                            new Class[0]);
            comp = (Container)(compMethod.invoke(null, new Object[0]));
        } catch (ClassNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Class is not found at " + this);
        } catch (IllegalAccessException ex) {
            CreamToolkit.logMessage(ex.toString());
            CreamToolkit.logMessage("Illegal access exception at " + this);
        } catch (NoSuchMethodException exc) {
            CreamToolkit.logMessage(exc.toString());
            CreamToolkit.logMessage("No such method at " + this);
        } catch (InvocationTargetException exce) {
            CreamToolkit.logMessage(exce.toString());
            CreamToolkit.logMessage("Invocation exception at " + this);
        }
        return comp;
    }

    public void adkl(Container comp) {
        comp.addKeyListener(this);
        comp.addContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                adkl(compo);
            } else {
                co.addKeyListener(this);
            }
        }
    }

    public void rmkl(Container comp) {
        comp.removeKeyListener(this);
        comp.removeContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                rmkl(compo);
            } else {
                co.removeKeyListener(this);
            }
        }
    }

    public void componentAdded(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            adkl((Container)e.getChild());
        } else {
            e.getChild().addKeyListener(this);
        }
    }

    public void componentRemoved(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            rmkl((Container)e.getChild());
        } else {
            e.getChild().removeKeyListener(this);
        }
    }

    private int keyMode = 0;
    private int count1 = 0;
    private int count2 = 0;
    private int proKeyCode = 0;
}

/*class Th implements Runnable {
    public void run() {
        char ch = ' ';
        ch = Passc.pr();
        System.out.println("######## finished ###########");
    }
}

class Passc {

    static {
        System.loadLibrary("test");
    }

    public native static char pr();
}*/
