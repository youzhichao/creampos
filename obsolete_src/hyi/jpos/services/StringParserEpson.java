package hyi.jpos.services;

import java.util.ArrayList;

import jpos.JposConst;
import jpos.JposException;
import jpos.POSPrinterConst;

public class StringParserEpson extends StringParsing {
    //Properties
    private String commandStart = "\u001B";
    private String commandEnd = "";
    private String LF = "\n";
    private String CR = "\r";
    private String FF = "\u000C";
    private String RS = "\u001E";
    private String GS = "\u001D";
    private String DLE = "\u0010";
    private boolean TaiwanMode = true;
    private POSPrinterEpsonU210 printer;

    public StringParserEpson(POSPrinterEpsonU210 printer) {
        this.printer = printer;
    }

    public StringParserEpson() throws Exception {
        throw new Exception("This class must be attached to a relevant POS printer device!");
    }
    public String getCommandStart() {
        return commandStart;
    }
    public String getCommandEnd() {
        return commandEnd;
    }

    //Methods
    //--------------------------------------------------------------------------
    //Method to parse DRJST51 related instruction which can transform the Jpos
    //standard string to the control instruction of DRJST51
    //--------------------------------------------------------------------------

    private String obtainFirstEscape(String data) {
        ArrayList ESC = getESCSequence(data);
        if (!ESC.isEmpty())
            return (String) ESC.get(0);
        else
            return "";
    }

    //--------------------------------------------------------------------------
    //Method to parse Epson RP-U420 related instruction which can transform the Jpos
    //standard string to the command of RP-U420 specificed.
    //--------------------------------------------------------------------------
    public String getValidCommand(int station, String data1, String data2) {
        String dataI = data1;
        String dataII = data2;
        StringBuffer validCommand = new StringBuffer();
        String validString = null;
        switch (station) {
            case POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL :
                break;
            case POSPrinterConst.PTR_TWO_SLIP_JOURNAL :
                return null;
            case POSPrinterConst.PTR_TWO_SLIP_RECEIPT :
                return null;
            default :
                return null;
        }
        if (dataI.equals(""))
            return null;
        else {
            if (dataII.equals(""))
                dataII = dataI;
            else {
                if (!dataI.equals(data2))
                    return null;
            }
        }
        try {
            if (validateData(POSPrinterConst.PTR_S_RECEIPT, dataI).size()
                != validateData(POSPrinterConst.PTR_S_JOURNAL, dataII).size())
                return null;
        } catch (JposException je) {
            System.out.println(je);
        }
        validString =
            getValidCommand(POSPrinterConst.PTR_S_JOURNAL_RECEIPT, dataI);
        validCommand.append(validString);
        return validCommand.toString();
    }

    public String getValidCommand(int station, String data) {
        //----------------------------------------------------------------------
        StringBuffer validCommand = new StringBuffer();
        String position = null;
        String escape = null;
        ArrayList DRJST51Escape = null;
        //----------------------------------------------------------------------
        switch (station) {
            case POSPrinterConst.PTR_S_JOURNAL :
                position = "\u0001";
                break;
            case POSPrinterConst.PTR_S_RECEIPT :
                position = "\u0002";
                break;
            case POSPrinterConst.PTR_S_SLIP :
                position = "\u0008";
                break;
            case POSPrinterConst.PTR_S_JOURNAL_RECEIPT :
                position = "\u0003";
                break;
            default :
                return null;
        }
        if (data.equals(""))
            return "";
        try {
            if (station == POSPrinterConst.PTR_S_JOURNAL_RECEIPT) {
                DRJST51Escape =
                    validateData(POSPrinterConst.PTR_S_JOURNAL, data);
                validCommand.append(
                    getCommandStart()
                        + "c0"
                        + position
                        + getCommandStart()
                        + "z\u0001");
            } else if (
                station == POSPrinterConst.PTR_S_RECEIPT
                    || station == POSPrinterConst.PTR_S_JOURNAL) {
                DRJST51Escape = validateData(station, data);
                validCommand.append(
                    getCommandStart()
                        + "c0"
                        + position
                        + getCommandStart()
                        + "z\u0090");
            } else {
                DRJST51Escape = validateData(station, data);
                validCommand.append(getCommandStart() + "c0" + position);
            }
        } catch (JposException je) {
            System.out.println(je);
        }
        if (DRJST51Escape.isEmpty()) {
            validCommand.append(data);
            return validCommand.toString();
        }
        //Use the code below to parse the
        for (int i = 0; i < data.length(); i++) {
            if (data.substring(i).startsWith("\u001B|")) {
                escape = obtainFirstEscape(data.substring(i));
                if (DRJST51Escape.contains(escape)) {
                    try {
                        switch (escape.charAt(escape.length() - 1)) {
                            case 'P' :
                                if (escape.charAt(escape.length() - 2) == 'f'
                                    || escape.charAt(escape.length() - 2)
                                        == 's') {
                                    for (int j = 0;
                                        j < printer.getRecLinesToPaperCut();
                                        j++)
                                        validCommand.append(LF);
                                    if (isNumber(escape
                                        .substring(2, escape.length() - 2))) {
                                        int percent =
                                            Integer
                                                .decode(
                                                    escape.substring(
                                                        2,
                                                        escape.length() - 2))
                                                .intValue();
                                        if (percent == 100)
                                            validCommand.append(GS + "V\u0001");
                                        else
                                            validCommand.append(GS + "V\u0002");
                                    } else {
                                        validCommand.append(GS + "V\u0001");
                                    }
                                } else {
                                    if (isNumber(escape
                                        .substring(2, escape.length() - 1))) {
                                        int percent =
                                            Integer
                                                .decode(
                                                    escape.substring(
                                                        2,
                                                        escape.length() - 1))
                                                .intValue();
                                        if (percent == 100) {
                                            validCommand.append(GS + "V\u0001");
                                        } else
                                            validCommand.append(GS + "V\u0002");
                                    } else {
                                        validCommand.append(GS + "V\u0001");
                                    }
                                }
                                if (escape.charAt(escape.length() - 2) == 's')
                                    validCommand.append(
                                        getCommandStart() + "o");
                                break;
                            case 'L' :
                                if (escape.charAt(escape.length() - 2) == 't')
                                    validCommand.append(
                                        getValidCommand(
                                            station,
                                            printer.getTopLogo()));
                                else if (
                                    escape.charAt(escape.length() - 2) == 'b')
                                    validCommand.append(
                                        getValidCommand(
                                            station,
                                            printer.getBottomLogo()));
                                else
                                    validCommand.append(
                                        getCommandStart() + "o");
                                break;
                            case 'F' :
                                if (!escape.equals("\u001B|lF")) {
                                    int times =
                                        Integer
                                            .decode(
                                                escape.substring(
                                                    2,
                                                    escape.length() - 2))
                                            .intValue();
                                    char feed = (char) times;
                                    validCommand.append(
                                        "\u001Bd" + new Character(feed));
                                } else
                                    validCommand.append(LF);
                                break;
                            case 'C' :
                                if (escape.charAt(escape.length() - 2) == '1')
                                    validCommand.append(
                                        getCommandStart() + "!\u0000");
                                else
                                    validCommand.append(
                                        getCommandStart() + "!\u0020");
                                break;
                            case 'N' :
                                validCommand.append(
                                    getCommandStart() + "!\u0000");
                        }
                    } catch (JposException je) {
                        System.out.println(je);
                    }
                    //----------------------------------------------------------
                } else {
                    validCommand.append(escape); //
                }
                i += escape.length() - 1;
            } else {
                validCommand.append(data.charAt(i));
            }
        }
        if (station == POSPrinterConst.PTR_S_SLIP
            && !validCommand.toString().endsWith("\n"))
            validCommand.append(LF);
        return validCommand.toString();
    }

    public ArrayList validateData(int station, String data)
        throws JposException {
        if (!(station == POSPrinterConst.PTR_S_JOURNAL
            || station == POSPrinterConst.PTR_S_RECEIPT
            || station == POSPrinterConst.PTR_S_SLIP))
            throw new JposException(
                JposConst.JPOS_E_ILLEGAL,
                "Invalid station argument: station=" + station);
        String item = null;
        String[] common =
            {
                "\u001B|rA",
                "\u001B|cA",
                "\u001B|3C",
                "\u001B|4C",
                "\u001B|rC",
                "\u001B|rvC",
                "\u001B|iC",
                "\u001B|bC" };
        ArrayList ESCSequence = getESCSequence(data);

        if (ESCSequence.isEmpty())
            return ESCSequence;

        ArrayList JposESCSequence = getJposESCSequence(data);

        ArrayList DRJST51ESCSequence = JposESCSequence;

        for (int k = JposESCSequence.size() - 1; k > -1; k--) {
            item = (String) JposESCSequence.get(k);
            if (contains(common, item))
                DRJST51ESCSequence.remove(k);
            else if (
                item.substring(item.length() - 1).equals("B")
                    || item.substring(item.length() - 2).equals("uF")
                    || item.substring(item.length() - 2).equals("rF")
                    || item.substring(item.length() - 2).equals("fT")
                    || item.substring(item.length() - 2).equals("uC")
                    || item.substring(item.length() - 2).equals("sC")
                    || item.substring(item.length() - 2).equals("hC")
                    || item.substring(item.length() - 2).equals("vC"))
                DRJST51ESCSequence.remove(k);
        }
        switch (station) {
            case POSPrinterConst.PTR_S_JOURNAL :
                for (int k = JposESCSequence.size() - 1; k > -1; k--) {
                    item = (String) JposESCSequence.get(k);
                    if (item.substring(item.length() - 1).equals("P")
                        || item.substring(item.length() - 2).equals("sL"))
                        DRJST51ESCSequence.remove(k);
                }
                break;
            case POSPrinterConst.PTR_S_RECEIPT :
                break;
            case POSPrinterConst.PTR_S_SLIP :
                for (int k = JposESCSequence.size() - 1; k > -1; k--) {
                    item = (String) JposESCSequence.get(k);
                    if (item.substring(item.length() - 1).equals("P")
                        || item.substring(item.length() - 2).equals("sL")
                        || item.substring(item.length() - 2).equals("lF"))
                        DRJST51ESCSequence.remove(k);
                }
                break;
        }
        return DRJST51ESCSequence;
    }
}
