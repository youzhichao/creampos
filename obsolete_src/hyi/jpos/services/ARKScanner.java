// Copyright (c) 2000 HYI
package hyi.jpos.services;

import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Properties;

import javax.comm.SerialPort;

import jpos.JposConst;
import jpos.JposException;
import jpos.ScannerConst;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.events.DataEvent;
import jpos.services.EventCallbacks;
import jpos.services.ScannerService14;
import jpos.services.ScannerService19;


/**
 * A Jpos service class.
 * <P>
 * @author slackware
 */
 
public class ARKScanner extends DeviceService implements ScannerService19, KeyListener, ContainerListener {
    private int scannerPrefixCharValue;
    static private ARKScanner claimedControl;
    static private Object mutex = new Object();
    private JposEntry scannerEntry = null;
    private String portName = "COM1";        

    private static String upca = "";
    private static String upce = "";
    private static String ean13 = "";
    private static String ean8 = "";
    private static String code39 = "";
    private static boolean isKeyType = true;
    
    private boolean decodeData = false;
    private byte[] scanData = new byte[0];
    private byte[] scanDataLabel = new byte[0];
    private int scanDataType =ScannerConst.SCAN_SDT_UNKNOWN;

    private boolean autoDisable = false;
    private boolean dataEventEnabled = false;
    private int dataCount = 0;

    //properties for serial port events.........................................
    private ArrayList stringBuffered = new ArrayList();
    private ArrayList dataEvents = new ArrayList();

    // JavaComm related properties
    private SerialPort serialPort = null;
    private OutputStream outputStream = null;
    private InputStream inputStream = null;

    protected Container comp                     = null;
    protected Container comp2                    = null;
    protected Container comp3                    = null; 
    protected Container comp4                    = null; 
    private byte[] b1                            = null;

    private boolean check = false;
    public static boolean keyStart = false;
    private String dataStr = "";
    private int keyMode = 0;
    private int count1 = 0;
    private int count2 = 0;
    private int proKeyCode = 0;

	public ARKScanner (SimpleEntry entry) { 
		scannerEntry = entry;
        ini();
    }

    public static void ini() {

        Properties p = new Properties();
        try {
            File f = new File(CreamToolkit.getConfigDir() + "scanner.conf");
            FileInputStream in = new FileInputStream(f);
            p.load(in);
        } catch (FileNotFoundException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return;
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return;
        }
        String scanner = p.getProperty("ScannerType");
        String type = scanner.substring(scanner.indexOf(':') + 1);
        String name = scanner.substring(0, scanner.indexOf(':'));

        //  get scanner type: key type or com type
        if (type.equalsIgnoreCase("com")) {
            isKeyType = false;
        } else if (type.equalsIgnoreCase("key")) {
            isKeyType = true;
        }

        //  get defined file
        Properties pr = new Properties();
        try {
            File f = new File(CreamToolkit.getConfigDir() + type + "scanner" + name + ".conf");
            FileInputStream in = new FileInputStream(f);
            pr.load(in);
        } catch (FileNotFoundException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return;
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return;
        }

        // get properties
        upca = pr.get("upca").toString();
        upce = pr.get("upce").toString();
        ean13 = pr.get("ean13").toString();
        ean8 = pr.get("ean8").toString();
        code39 = pr.get("code39").toString();
    }
                                                

	public ARKScanner () {
		 if (scannerEntry != null)
    		 portName = (String)scannerEntry.getPropertyValue("DevicePortName");
	}

    //AutoDisable boolean R/W open
	public boolean getAutoDisable() throws JposException {
		return autoDisable;
	}
	public void setAutoDisable(boolean autoDisable) throws JposException {
		this.autoDisable = autoDisable;
	}

	public int getDataCount() throws JposException {
	    return dataCount;
	}

	public boolean getDataEventEnabled() throws JposException {
        return dataEventEnabled; 
	}
	public void setDataEventEnabled(boolean dataEventEnabled)
					   throws JposException {
		this.dataEventEnabled = dataEventEnabled;
	}                      

    public void setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        if (!claimed) {
            System.out.println("must claim it first");
            return;
        }
        this.deviceEnabled = deviceEnabled;
        if (deviceEnabled) {
            if (scannerEntry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)scannerEntry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                adkl(comp);
            }
            if (scannerEntry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)scannerEntry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                adkl(comp2);
            }
            if (scannerEntry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)scannerEntry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                adkl(comp3);
            }
            if (scannerEntry.hasPropertyWithName("COMP4_CLASS_PROP_NAME")) {
                String className4 = (String)scannerEntry.getPropertyValue("COMP4_CLASS_PROP_NAME");
                comp4 = createComp(className4);
                adkl(comp4);
            }
        } else {
            if (scannerEntry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)scannerEntry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                rmkl(comp);
            }
            if (scannerEntry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)scannerEntry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                rmkl(comp2);
            }
            if (scannerEntry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)scannerEntry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                rmkl(comp3);
            }
            if (scannerEntry.hasPropertyWithName("COMP4_CLASS_PROP_NAME")) {
                String className4 = (String)scannerEntry.getPropertyValue("COMP4_CLASS_PROP_NAME");
                comp4 = createComp(className4);
                rmkl(comp4);
            }
        }
    }

	public boolean getDecodeData() throws JposException {
	    return decodeData;
	}

	public void setDecodeData(boolean decodeData) throws JposException {
		this.decodeData = decodeData;
	}

	public byte[] getScanData() throws JposException {
		return scanData;
	}

	public byte[] getScanDataLabel() throws JposException {
		return scanDataLabel;
	}
	
	//When DecodeData is false, this property is set to SCAN_SDT_UNKNOWN.
	public int getScanDataType() throws JposException {
		return scanDataType;
	}

	//Properties 13
	public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        throw new JposException (JposConst.JPOS_E_NOSERVICE, "No service available now!");
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
	}
	//end of properties definition

	//Common method -- Not supported
    public void checkHealth(int level)  throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
	}

	public void directIO(int command, int[] data, Object object) throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
	}

	//Common methods -- Supported

	public void clearInput() throws JposException {
		 if (dataEvents != null)
			 dataEvents.clear();
			 dataCount = 0;
         if (stringBuffered != null)
             stringBuffered.clear();
	}
	//
	public void open(String logicalName, EventCallbacks cb)throws JposException {
		super.open(logicalName, cb);
        this.eventCallbacks = cb; //eventCallbacks;
        claimed = false;
        deviceEnabled = false;

        scannerPrefixCharValue = Integer.parseInt(
            GetProperty.getScannerPrefixCharValue("32"));
	}
	
	public void close() throws JposException  {
        claimed = false;
        deviceEnabled = false;
        claimedControl = null;
	}              

    public void claim(int timeout) throws JposException {
        if (claimed) {
            System.out.println("device has been claimed");
            return;
        }

        /*synchronized (mutex) {
            if (claimedControl == null) {
                Object obj = eventCallbacks.getEventSource();
                claimedControl = (ARKScanner)obj;
                claimed = true;
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                    claimedControl = (ARKScanner)eventCallbacks.getEventSource();
                    claimed = true;
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        if (claimedControl == null) {
                            claimedControl = (ARKScanner)eventCallbacks.getEventSource();
                            claimed = true;
                            return;
                        }
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        }*/
        claimed = true;
    }

	public void deleteInstance() throws JposException {
    }

    //event
    public void keyReleased(KeyEvent e) {
    }

	public void keyPressed(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
        char keyChar = e.getKeyChar();
        int keyCode = (int)keyChar;
        //if (keyChar == ' ') {
        if (keyCode == scannerPrefixCharValue) {
            keyStart = true;
        } else if (keyStart && keyCode == 10) {
            try {
                if (getAutoDisable())
                    setDeviceEnabled(false);
            } catch (JposException ex) {
                ex.printStackTrace(CreamToolkit.getLogger());
            }

            try {
                if (!"".equals(upca) && dataStr.startsWith(upca)) {
                    scanDataType = ScannerConst.SCAN_SDT_UPCA;
                    scanDataLabel = dataStr.substring(upca.length()).getBytes(CreamToolkit.getEncoding());
                    eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
                } else if (!"".equals(upce) && dataStr.startsWith(upce)) {
                    scanDataType = ScannerConst.SCAN_SDT_UPCE;
                    scanDataLabel = dataStr.substring(upce.length()).getBytes(CreamToolkit.getEncoding());
                    eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
                } else if (!"".equals(ean13) && dataStr.startsWith(ean13)) {
                    scanDataType = ScannerConst.SCAN_SDT_EAN13;
                    scanDataLabel = dataStr.substring(ean13.length()).getBytes(CreamToolkit.getEncoding());
                    eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
                } else if (!"".equals(ean8) && dataStr.startsWith(ean8)) {
                    scanDataType = ScannerConst.SCAN_SDT_EAN8;
                    scanDataLabel = dataStr.substring(ean8.length()).getBytes(CreamToolkit.getEncoding());
                    eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
                } else if (!"".equals(code39) && dataStr.startsWith(code39)) {
                    scanDataType = ScannerConst.SCAN_SDT_Code39;

                    //Bruce/2003-06-26/
                    // Modify for CStore. Ref. StateMachine class.
                    ///scanDataLabel = dataStr.substring(code39.length(), code39.length() + 7).getBytes(CreamToolkit.getEncoding());
                    scanDataLabel = dataStr.substring(1).getBytes(CreamToolkit.getEncoding());

                    eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
                } else {
                    if ("".equals(upca) || "".equals(upce) || "".equals(ean13) || "".equals(ean8) || "".equals(code39))
                        scanDataType = ScannerConst.SCAN_SDT_EAN13;
                    else
                        scanDataType = ScannerConst.SCAN_SDT_UNKNOWN;
                    scanDataLabel = dataStr.getBytes(CreamToolkit.getEncoding());
                    eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
                }
            } catch (StringIndexOutOfBoundsException ex) {
                ex.printStackTrace(CreamToolkit.getLogger());
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace(CreamToolkit.getLogger());
            }

            scanDataLabel = null;
            keyStart = false;
            dataStr = "";
        } else if (keyStart && keyCode != 10) {
            dataStr = dataStr + keyChar;
        }
	}

    public Container createComp(String className) {
        try {
            Class compClass = Class.forName(className);
            Method compMethod = compClass.getDeclaredMethod("getInstance",
                                                            new Class[0]);
            comp = (Container)(compMethod.invoke(null, new Object[0]));
        } catch (ClassNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Class is not found at " + this);
        } catch (IllegalAccessException ex) {
            CreamToolkit.logMessage(ex.toString());
            CreamToolkit.logMessage("Illegal access exception at " + this);
        } catch (NoSuchMethodException exc) {
            CreamToolkit.logMessage(exc.toString());
            CreamToolkit.logMessage("No such method at " + this);
        } catch (InvocationTargetException exce) {
            CreamToolkit.logMessage(exce.toString());
            CreamToolkit.logMessage("Invocation exception at " + this);
        }
        return comp;
    }

	public void adkl(Container comp) {
		comp.addKeyListener(this);
		comp.addContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                adkl(compo);
            } else {
				co.addKeyListener(this);
            }
        }
    }

	public void rmkl(Container comp) {
		comp.removeKeyListener(this);
		comp.removeContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
				rmkl(compo);
            } else {
				co.removeKeyListener(this);
            }
        }
    }

    public void componentAdded(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            adkl((Container)e.getChild());
        } else {
            e.getChild().addKeyListener(this);
        }
    }

    public void componentRemoved(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            rmkl((Container)e.getChild());
        } else {
            e.getChild().removeKeyListener(this);
        }
    }

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }
}

