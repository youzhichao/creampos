package hyi.jpos.services;

//for JavaCommAPI
import hyi.cream.state.GetProperty;

import java.awt.Toolkit;

import jpos.CashDrawerConst;
import jpos.JposConst;
import jpos.JposException;
import jpos.config.simple.SimpleEntry;
import jpos.events.StatusUpdateEvent;
import jpos.services.CashDrawerService19;
import jpos.services.EventCallbacks;

public class Tec7000CashDrawer extends DeviceService implements CashDrawerService19 {
    //--------------------------------------------------------------------------
    // Variables
    //--------------------------------------------------------------------------
    
//    private OutputStreamWriter dataOutput = null;
//    static private PrinterAndDrawerPort shareOutput = null;  //Singleton object;
    private String logicalName = null;//
    
//    private JposEntry cashDrawerEntry = null;
//    private OutputStream outputStream = null;
//    private InputStream inputStream = null;
//	private String portName = "COM2";
    
    private Object waitBeep = new Object();//for wait purpose;
    private boolean drawerClosed;
    
    private int cashNo;
    //--------------------------------------------------------------------------
    //private byte[] readPort = new byte[10];
//	private char statusCode = '0';

    //Constructor
    public Tec7000CashDrawer (SimpleEntry entry) {
//        cashDrawerEntry = entry;
    	try {
			cashNo = Integer.parseInt(GetProperty.getTec7000CashDrawerNo("1"));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Tec7000CashDrawer () {
    	try {
			cashNo = Integer.parseInt(GetProperty.getTec7000CashDrawerNo("1"));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    // Capabilities
    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties 13
    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        throw new JposException (JposConst.JPOS_E_NOSERVICE, "No service available now!");
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    //Common method -- Not supported
    public void checkHealth(int level)  throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    // Properties specified
    public boolean getCapStatus() throws JposException {
        return true;
    }
    
	public boolean getDrawerOpened() throws JposException {
        if (CashDrawer7000Local.getDrawerState(cashNo) == 0) {
            return true;
        } else {
            return false;
        }
    }

    // Methods
	public void openDrawer() throws JposException {
        if (!getDeviceEnabled())
			throw new JposException(JposConst.JPOS_E_DISABLED,"Cash drawer is not enabled!");
        CashDrawer7000Local.openCashDrawer(cashNo);
    }
    
    
	public void waitForDrawerClose(int beepTimeout, int beepFrequency,
					   int beepDuration, int beepDelay) throws JposException {
        if (!getDeviceEnabled())
			throw new JposException(JposConst.JPOS_E_DISABLED,"Cash drawer is not enabled!");

        drawerClosed = false;

        long start = System.currentTimeMillis();
        long waitTime = beepTimeout;
        while (true) {
            if (!getDrawerOpened()) {
                drawerClosed = true;
                break;
            }
            if (waitTime > 0) {
            	waitTime = beepTimeout - (System.currentTimeMillis() - start);
                try {
                	Thread.sleep(100);
                } catch (Exception e) {
                }
            } else {
                Toolkit.getDefaultToolkit().beep();
                try {
                	Thread.sleep(beepDelay);
                } catch (Exception e) {
                }
            }
        }
        /*
		new Beep(beepTimeout, beepDelay);
        while (!drawerClosed) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
        }
        */

        //synchronized (waitBeep) {
        //    try {
		//		waitBeep.wait();
        //    } catch (InterruptedException ie) {
        //        if (!getDrawerOpened())
        //            return;
        //        else
        //            throw new JposException (JposConst.JPOS_E_FAILURE, this.toString(), ie);
        //    }
        //}
        eventCallbacks.fireStatusUpdateEvent(new StatusUpdateEvent(eventCallbacks.getEventSource(),
			CashDrawerConst.CASH_SUE_DRAWERCLOSED));//*/
    }

    class Beep extends Thread {
        private int beepTimeout;
        private int beepDelay;
        public Beep() {
            start();
        }
        
        public Beep(int beepTimeout, int beepDelay) {
            this.beepTimeout = beepTimeout;
            this.beepDelay = beepDelay;
            start();
        }
        public void run() {
            try {
                long start = System.currentTimeMillis();
                long waitTime = beepTimeout;
                try {
					while (waitTime > 0) {
                        if (!getDrawerOpened()) {
                            drawerClosed = true;
                            return;
							//synchronized (waitBeep) {
                            //    waitBeep.notifyAll();
                            //    return;
                            //}
                        }
                        Thread.sleep(100);
						waitTime = beepTimeout - (System.currentTimeMillis() - start);
                    }
                    while (true){
                        if (!getDrawerOpened()) {
                            drawerClosed = true;
                            return;
							//synchronized (waitBeep) {
                            //    waitBeep.notifyAll();
                            //    return;
                            //}
                        }
                        Toolkit.getDefaultToolkit().beep();
						sleep(beepDelay);
						//System.out.println(beepDelay);
					}
                } catch (JposException je) {
                        System.out.println(je + this .toString() );
                }
            } catch (InterruptedException e) {
						System.out.println(e + this .toString());
			}
        }
    }

	public void open(String logicalName, EventCallbacks cb) throws JposException {
		//System.out.println(logicalName);
		super.open (logicalName, cb);
		this.logicalName = logicalName;
    }

    public void close() throws JposException {
        if (getClaimed())
            release();
        super.close();
        this.logicalName = null;
    }

    public void claim(int timeout) throws JposException {
    }

    public void release() throws JposException {  
	}

	public void deleteInstance() throws JposException {
    }

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }

    public boolean getCapStatusMultiDrawerDetect() throws JposException {
        return false;
    }
}