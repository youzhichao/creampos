
// Copyright (c) 2000 HYI
package hyi.jpos.services;

import jpos.*;
import jpos.services.*;
import jpos.events.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.*;
import jpos.config.*;
import jpos.config.simple.*;
import hyi.cream.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PartnerKB128Keylock extends Keylock implements KeylockService14, KeyListener, ContainerListener {
    protected EventCallbacks eventCallbacks      = null;
    protected JposEntry entry                    = null;
    protected Container comp                     = null;
    protected Container comp2                    = null;
    protected Container comp3                    = null;

    private boolean capDescriptors   = false;
    private boolean claimed          = false;
    private int deviceDescriptors    = 0;
    private boolean deviceEnabled    = false;
    private int keyPosition          = 1;
    private int positionCount        = 0;

    protected String checkHealthText             = "";
    protected String deviceServiceDescription    = "JavaPOS Service from HYI Corporation";
    protected int deviceServiceVersion           = 1005000;
    protected boolean freezeEvents               = false;
    protected String physicalDeviceDescription   = "";
    protected String physicalDeviceName          = "";
    protected int state                          = JposConst.JPOS_S_CLOSED;
    protected int powerNotify                    = JposConst.JPOS_PN_DISABLED;
    protected int powerState                     = JposConst.JPOS_PS_UNKNOWN;

    private final static int ks1                 = 112;
    private final static int ks2                 = 113;
    private final static int ks3                 = 114;
    private final static int ks4                 = 115;
    private final static int ks5                 = 116;
    private final static int ks6                 = 117;
    private final static int ks7                 = 118;
    private final static int ks8                 = 119;
    private final static int ks9                 = 120;

    /**
     * Constructor
     */
    public PartnerKB128Keylock(SimpleEntry entry) {
        this.entry = entry;
    }

    /**
     * Indicate the ServiceInstance that its connection has been dropped
     * @since 0.1 (Philly 99 meeting)
     * @throws jpos.JposException if any error occurs
     */
    public void deleteInstance() throws JposException {
    }

    public int getCapPowerReporting() throws JposException {
        return 0;
    }

    // Properties
    public String getCheckHealthText() throws JposException {
        return checkHealthText;
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    public void setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        this.deviceEnabled = deviceEnabled;
        if (deviceEnabled) {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                adkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                adkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                adkl(comp3);
            }
        } else {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                rmkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                rmkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                rmkl(comp3);
            }
        }
        keyPosition = 1;
    }

    public String getDeviceServiceDescription() throws JposException {
        return deviceServiceDescription;
    }

    public int getDeviceServiceVersion() throws JposException {
        return deviceServiceVersion;
    }

    public boolean getFreezeEvents() throws JposException {
        return freezeEvents;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        this.freezeEvents = freezeEvents;
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return physicalDeviceDescription;
    }

    public String getPhysicalDeviceName() throws JposException {
        return physicalDeviceName;
    }

    public int getKeyPosition() throws JposException {
        return keyPosition;
    }

    public int getPositionCount() throws JposException {
        return positionCount;
    }

    public int getPowerNotify() throws JposException {
        return 0;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        this.powerNotify = powerNotify;
    }

    public int getPowerState() throws JposException {
        return powerNotify;
    }


    public void claim(int timeout) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL);
    }

    public void close() throws JposException {
        if (comp != null) {
            comp.removeKeyListener(this);
        }
        this.deviceEnabled = false;
    }

    public void checkHealth(int level) throws JposException {
    }

    public void directIO(int command, int[] data, Object object)
                       throws JposException {
    }

    public void open(String logicalName, EventCallbacks eventCallbacks)
                        throws JposException {
        this.eventCallbacks = eventCallbacks;
        deviceEnabled = false;
        positionCount = 6;
        keyPosition = 1;
    }

    public void release() throws JposException {
        if (comp != null) {
            comp.removeKeyListener(this);
        }
        deviceEnabled = false;
        keyPosition = 1;
    }

    synchronized public void waitForKeylockChange(int position, int timeout)
                       throws JposException {
        if (deviceEnabled == false) {
            System.out.println("device not enabled");
            throw new JposException(JposConst.JPOS_E_ILLEGAL);
        }

        comp.requestFocus();

        waitStatus = true;
        if (position == getKeyPosition()) {
            waitStatus = false;
            return;
        }

        if (timeout < -1) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL);
        } else if (timeout == JposConst.JPOS_FOREVER) {
            try {
                while (position != getKeyPosition()) {
                    wait();
                }
                waitStatus = false;
                return;
            } catch (InterruptedException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        } else {
            try {
                long start = System.currentTimeMillis();
                long waitTime = timeout;

                while (waitTime > 0) {
                    wait(waitTime);
                    if (position == getKeyPosition()) {
                        waitStatus = false;
                        return;
                    }
                    waitTime = timeout - (System.currentTimeMillis() - start);
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace(CreamToolkit.getLogger());
            }
            waitStatus = false;
            throw new JposException(JposConst.JPOS_E_TIMEOUT);
        }
    }

    public void keyReleased(KeyEvent e) {
    }

    synchronized public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        //System.out.println("key code = " + keyCode);

        if (keyCode < ks1 || keyCode > ks9) {
            return;
        }

        int code = 0;
        switch (keyCode) {
        case ks1:
            code = 1;
            break;
        case ks2:
            code = 2;
            break;
        case ks3:
            code = 3;
            break;
        case ks4:
            code = 4;
            break;
        case ks5:
            code = 6;   //Bruce/20021101 treat F5 as ConfigState key (6)
            break;
        case ks6:
            code = 6;
            break;
        case ks7:
            code = 7;
            break;
        case ks8:
            code = 8;
            break;
        case ks9:
            code = 9;
            break;
        }
        keyPosition = code;
        //System.out.println("    **** key position = " + code + " ****");
        eventCallbacks.fireStatusUpdateEvent(new StatusUpdateEvent(eventCallbacks.getEventSource(), code));

        if (waitStatus) {
            notifyAll();
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    private boolean keyMode = false;
    private int count = 0;
    private boolean waitStatus = false;

    public Container createComp(String className) {
        try {
            Class compClass = Class.forName(className);
            Method compMethod = compClass.getDeclaredMethod("getInstance",
                                                            new Class[0]);
            comp = (Container)(compMethod.invoke(null, new Object[0]));
        } catch (ClassNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Class is not found at " + this);
        } catch (IllegalAccessException ex) {
            CreamToolkit.logMessage(ex.toString());
            CreamToolkit.logMessage("Illegal access exception at " + this);
        } catch (NoSuchMethodException exc) {
            CreamToolkit.logMessage(exc.toString());
            CreamToolkit.logMessage("No such method at " + this);
        } catch (InvocationTargetException exce) {
            CreamToolkit.logMessage(exce.toString());
            CreamToolkit.logMessage("Invocation exception at " + this);
        }
        return comp;
    }

    public void adkl(Container comp) {
        comp.addKeyListener(this);
        comp.addContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                adkl(compo);
            } else {
                co.addKeyListener(this);
            }
        }
    }

    public void rmkl(Container comp) {
        comp.removeKeyListener(this);
        comp.removeContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                rmkl(compo);
            } else {
                co.removeKeyListener(this);
            }
        }
    }

    public void componentAdded(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            adkl((Container)e.getChild());
        } else {
            e.getChild().addKeyListener(this);
        }
    }

    public void componentRemoved(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            rmkl((Container)e.getChild());
        } else {
            e.getChild().removeKeyListener(this);
        }
    }

    private int count1 = 0;
    private int count2 = 0;
    private int proKeyCode = 0;
}
