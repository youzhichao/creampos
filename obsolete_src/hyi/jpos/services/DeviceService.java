package hyi.jpos.services;

import jpos.JposConst;
import jpos.JposException;
import jpos.services.BaseService;
import jpos.services.EventCallbacks;

/**
 * DeviceService class
 * NOTE: implements BaseService to define all of the common services
 * needed by the controller
 * @since 2000
 * @author slackware
 */

public abstract class DeviceService implements BaseService {

    /** Default ctor */

    public DeviceService() {}

    //--------------------------------------------------------------------------
    // Common properties
    //--------------------------------------------------------------------------
    public String getCheckHealthText() throws JposException {
        return checkHealthText;
    }

    public boolean getClaimed() throws JposException  {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        this.deviceEnabled = deviceEnabled;
    }

    public String getDeviceServiceDescription() throws JposException {
        return deviceServiceDescription;
    }

    public int getDeviceServiceVersion() throws JposException {
        return deviceServiceVersion;
    }

    public boolean getFreezeEvents() throws JposException {
        return freezeEvents;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        this.freezeEvents = freezeEvents;
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return physicalDeviceDescription;
    }

    public String getPhysicalDeviceName() throws JposException {
        return physicalDeviceName;
    }

    public int getState() throws JposException {
        return state;
    }

    //--------------------------------------------------------------------------
    // Common methods
    //--------------------------------------------------------------------------
    
    /*close
      claim
      release
      checkHealth
      clearInput
      clearOutput
      directIO*/

    public void claim(int timeout) throws JposException {
        claimed = true;
    }


    public void release() throws JposException {
        claimed = false;
    }

    public void close() throws JposException {
        checkHealthText = "";
        claimed = false;
        deviceEnabled = false;
        deviceServiceDescription = "";
        deviceServiceVersion = 1004000;
        freezeEvents = false;
        physicalDeviceDescription = "";
        physicalDeviceName = "";
        state = JposConst.JPOS_S_CLOSED;
        eventCallbacks  = null;
    }


    public void open(String logicalName, EventCallbacks cb) throws JposException {
        eventCallbacks = cb;
        checkHealthText = "Well now!";
        claimed = false;
        deviceEnabled = false;
        deviceServiceDescription = "JavaPOS "+ cb.getEventSource( ).toString()+"Service version 1.4";
        deviceServiceVersion = 1005000;
        freezeEvents = false;
        physicalDeviceDescription = "JavaPOS Peripheral of " + logicalName + " Service";
        physicalDeviceName = logicalName;
        state = JposConst.JPOS_S_IDLE;
    }



    //--------------------------------------------------------------------------
    // jpos.config.JposServiceInstance methods
    //--------------------------------------------------------------------------

    //abstract public void deleteInstance() throws JposException;

    //--------------------------------------------------------------------------
    // System Instance variables
    //--------------------------------------------------------------------------

    protected String checkHealthText             = "";
    protected boolean claimed                    = false;
    protected boolean deviceEnabled              = false;
    protected String deviceServiceDescription    = "";
    protected int deviceServiceVersion           = 1005000;
    protected boolean freezeEvents               = false;
    protected String physicalDeviceDescription   = "";
    protected String physicalDeviceName          = "";
    protected int state                          = JposConst.JPOS_S_CLOSED;
    protected EventCallbacks eventCallbacks      = null;
}

/*Properties
Name Usage Notes Ver Type Access
AutoDisable 1 boolean R/W
CapPowerReporting 1.3 int R
CheckHealthText String R
Claimed boolean R
DataCount 1 int R
DataEventEnabled 1 boolean R/W
DeviceEnabled boolean R/W
FreezeEvents boolean R/W
OutputID 2 int R
PowerNotify 1.3 int R/W
PowerState 1.3 int R
State int R
DeviceControlDescription String R
DeviceControlVersion int R
DeviceServiceDescription String R
DeviceServiceVersion int R
PhysicalDeviceDescription String R
PhysicalDeviceName String R*/
