
// Copyright (c) 2000 hyi
package hyi.jpos.services;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public interface KBListener {

    public void receiveKeyEvent(int keyCode);
}

 
