package hyi.jpos.services;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jpos.JposConst;
import jpos.JposException;
import jpos.KeylockConst;
import jpos.config.simple.SimpleEntry;
import jpos.events.StatusUpdateEvent;
import jpos.services.EventCallbacks;
import jpos.services.KeylockService19;


/**
 * This is device service for PartnerKB78 Keylock.
 * <P>
 * <br>
 * The following properties are supported:
 * <li> FreezeEvents
 * <p>
 * <br>
 * The followings are the limitation of current implementation:
 * <li> CapPowerReporting is JPOS_PR_NONE.
 * <li> PowerNotify is JPOS_PN_DISABLED.
 * <li> PowerState is JPOS_PS_UNKNOWN.
 * <li> CapCompareFirmwareVersion is false
 * <li> CapStatisticsReporting is false
 * <li> CapUpdateFirmware is false
 * <li> CapUpdateStatistics is false
 * <li> Do not generate DirectIOEvent.
 * <p>
 * <br>
 * Note: Because this JavaPOS device service intercept and get keyboard event from AWT thread,
 * application cannot receive any POSKeyboard/Keylock/MSR event from within AWT event processing.
 * Our JavaPOS user should not expect to get any JavaPOS event within their AWT event processing
 * method. <br/><p/>
 * 
 * 
 * <pre>
 *  [L0]: [ctrl]0
 *  [L1]: [ctrl]1
 *  [L2]: [ctrl]2
 *  [L3]: [ctrl]3
 *  [L4]: [ctrl]4
 *  [LP]: [ctrl]5
 *
 * Sample JCL config:
 * 
 * &lt;JposEntry logicalName="PartnerKB78Keylock"&gt;
 *    &lt;creation factoryClass="hyi.jpos.loader.ServiceInstanceFactory"
 *          serviceClass="hyi.jpos.services.PartnerKB78Keylock"/&gt;
 *    &lt;vendor name="Partner" url="http://www.hyi.com.cn"/&gt;
 *    &lt;jpos category="Keylock" version="1.9"/&gt;
 *    &lt;product description="PartnerKB78 Keylock" name="PartnerKB78 Keylock"
 *         url="http://www.hyi.com.cn"/&gt;
 * &lt;/JposEntry&gt;
 * </pre>
 * @author James Zheng @ Hongyuan Software
 * @since 2008-2-22
 */
public class PartnerKB78Keylock extends AbstractDeviceService19 implements KeylockService19 {

    private int keyPosition = -1; // an unknown position
    private int positionCount;
    private KeyEventDispatcher keyEventInterceptor;
    private List eventQueue = new ArrayList();
    private Map keylockCodeMap = new HashMap();
    private boolean inWaitForKeylockChange;

    /**
     * Default constructor.
     */
    public PartnerKB78Keylock() {
        setDeviceServiceDescription("PartnerKB78 Keylock JavaPOS Device Service from HYI");
        setPhysicalDeviceDescription("PartnerKB78 Keylock");
        setPhysicalDeviceName("PartnerKB78 Keylock");
        createKeyEventInterceptor();
        createKeylockCodeMap();
        setState(JposConst.JPOS_S_CLOSED);
    }

    /**
     * Constructor with a SimpleEntry.
     * 
     * @param entry
     *            The registry entry for Keylock.
     */
    public PartnerKB78Keylock(SimpleEntry entry) {
        this();
    }

    private void createKeylockCodeMap() {
        // Keylock response when turning it
    	// Please see (java.awt.event.KeyEvent)
		// public static final int VK_0 = 0x30;
		// public static final int VK_1 = 0x31;
		// public static final int VK_2 = 0x32;
		// public static final int VK_3 = 0x33;
		// public static final int VK_4 = 0x34;
		// public static final int VK_5 = 0x35;
		// public static final int VK_6 = 0x36;
		// public static final int VK_NUMPAD0 = 0x60;
		// public static final int VK_NUMPAD1 = 0x61;
		// public static final int VK_NUMPAD2 = 0x62;
		// public static final int VK_NUMPAD3 = 0x63;
		// public static final int VK_NUMPAD4 = 0x64;
		// public static final int VK_NUMPAD5 = 0x65;
		// public static final int VK_NUMPAD6 = 0x66;
        keylockCodeMap.put("48", new Integer(KeylockConst.LOCK_KP_LOCK)); // L0 `0
        keylockCodeMap.put("96", new Integer(KeylockConst.LOCK_KP_LOCK)); // L0
        keylockCodeMap.put("49", new Integer(KeylockConst.LOCK_KP_NORM)); // L1 `1
        keylockCodeMap.put("97", new Integer(KeylockConst.LOCK_KP_NORM)); // L1
        keylockCodeMap.put("50", new Integer(KeylockConst.LOCK_KP_SUPR)); // L2 `2
        keylockCodeMap.put("98", new Integer(KeylockConst.LOCK_KP_SUPR)); // L2
        keylockCodeMap.put("51", new Integer(KeylockConst.LOCK_KP_SUPR + 1)); // L3 `3
        keylockCodeMap.put("99", new Integer(KeylockConst.LOCK_KP_SUPR + 1)); // L3
        keylockCodeMap.put("52", new Integer(KeylockConst.LOCK_KP_SUPR + 2)); // L4 `4
        keylockCodeMap.put("100", new Integer(KeylockConst.LOCK_KP_SUPR + 2)); // L4
        keylockCodeMap.put("53", new Integer(KeylockConst.LOCK_KP_SUPR + 3)); // LP `5
        keylockCodeMap.put("101", new Integer(KeylockConst.LOCK_KP_SUPR + 3)); // LP

        // 怕 [LP] 設錯成 `6
        keylockCodeMap.put("54", new Integer(KeylockConst.LOCK_KP_SUPR + 3)); // LP `6
        keylockCodeMap.put("102", new Integer(KeylockConst.LOCK_KP_SUPR + 3)); // LP

        positionCount = 6; // KeylockConst.LOCK_KP_SUPR + 3;
    }

    /**
     * Fire the the keylock StatusUpdateEvent according to keylock code sequence.
     * 
     * @param keylockData
     *            Keylock code sequence.
     */
    synchronized private void fireKeylockStatusUpdateEvent(String keylockData) {
        Integer pos = (Integer) keylockCodeMap.get(keylockData);
        if (pos != null) {
            keyPosition = pos.intValue();
            if (inWaitForKeylockChange)
                notifyAll();
            else
                eventCallbacks.fireStatusUpdateEvent(new StatusUpdateEvent(eventCallbacks
                    .getEventSource(), 0));
        }
    }

    /**
     * Create a keyboard event interceptor for firing Keylock event. The interceptor is an AWT's
     * KeyEventDispatcher, and then it'll be added into KeyboardFocusManager when enabling this
     * device.
     */
    private void createKeyEventInterceptor() {
        keyEventInterceptor = new KeyEventDispatcher() {
            int waitLastTwoEvents;
        	
        	public boolean dispatchKeyEvent(KeyEvent e) {
                try {
                    setState(JposConst.JPOS_S_BUSY);
                    if (e.getID() != KeyEvent.KEY_PRESSED) {
                        if (waitLastTwoEvents > 0) {
                            waitLastTwoEvents--;
                            return true;
                        }
                    }
                    
                    if (e.isControlDown()){
                    	// The last char is KEYLOCK_END_CODE, but AWT system will still generate
                    	// two KeyEvents for it: one is KEY_TYPED, and follow by a KEY_RELEASE.
                    	// We also want to ignore and absort those two KeyEvents, so we set a
                    	// count here for doing this.
                    	waitLastTwoEvents = 2;
                    	String codeStr = String.valueOf(e.getKeyCode());
                    	
                        if (getFreezeEvents()) {
                            // Append into event queue if now the
                            // FreezeEvent is true
                            eventQueue.add(codeStr);
                        } else {
                            // Fire the DataEvent
                        	fireKeylockStatusUpdateEvent(codeStr);
                        }
                    	
                    	return true;
                    } else {
                    	return false;
                    } 
                } catch (Exception e1) {
                    e1.printStackTrace();
                    return false;
                } finally {
                    setState(JposConst.JPOS_S_IDLE);
                }
            }
        };
    }

    /**
     * Indicate the ServiceInstance that its connection has been dropped
     * 
     * @since 0.1 (Philly 99 meeting)
     * @throws jpos.JposException
     *             if any error occurs
     */
    public void deleteInstance() throws JposException {
    }

    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties

    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        super.setDeviceEnabled(deviceEnabled);

        // Setup a key event interceptor when enabling device
        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        if (deviceEnabled)
            kfm.addKeyEventDispatcher(keyEventInterceptor);
        else
            kfm.removeKeyEventDispatcher(keyEventInterceptor);
    }

    private void fireEventsInQueue() {
        try {
            setState(JposConst.JPOS_S_BUSY);
            if (eventCallbacks != null && getDeviceEnabled() && !getFreezeEvents()) {
                while (!eventQueue.isEmpty()) {
                    fireKeylockStatusUpdateEvent((String) eventQueue.get(0));
                    eventQueue.remove(0);
                }
            }
        } catch (JposException e) {
            e.printStackTrace();
        } finally {
            setState(JposConst.JPOS_S_IDLE);
        }
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        System.out.println("St7000Keylock: setFreezeEvents()");
        super.setFreezeEvents(freezeEvents);
        fireEventsInQueue();
    }

    public int getKeyPosition() throws JposException {
        return keyPosition;
    }

    public int getPositionCount() throws JposException {
        return positionCount;
    }

    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    public void claim(int timeout) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL);
    }

    public void checkHealth(int level) throws JposException {
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
    }

    public void open(String logicalName, EventCallbacks eventCallbacks) throws JposException {
        super.open(logicalName, eventCallbacks);
        setState(JposConst.JPOS_S_IDLE);
    }

    public void close() throws JposException {
        setClaimed(false);
        setDeviceEnabled(false);
        setState(JposConst.JPOS_S_CLOSED);
    }

    public void release() throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL);
    }

    synchronized public void waitForKeylockChange(int position, int timeout) throws JposException {
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Keylock is not enabled.");
        if (getFreezeEvents())
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Keylock is freezed now.");
        if (timeout < -1)
            throw new JposException(JposConst.JPOS_E_ILLEGAL,
                "waitForKeylockChange's timeout value is not valid.");

        if (position == getKeyPosition())
            return;

        inWaitForKeylockChange = true;

        if (timeout == JposConst.JPOS_FOREVER) {
            try {
                while (position != getKeyPosition()) {
                    wait();
                }
                inWaitForKeylockChange = false;
                return;
            } catch (InterruptedException e) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "waitForKeylockChange is being interrupted.");
            }
        } else {
            try {
                long start = System.currentTimeMillis();
                long waitTime = timeout;

                while (waitTime > 0) {
                    wait(waitTime);
                    if (position == getKeyPosition()) {
                        inWaitForKeylockChange = false;
                        return;
                    }
                    waitTime = timeout - (System.currentTimeMillis() - start);
                }
                inWaitForKeylockChange = false;
                throw new JposException(JposConst.JPOS_E_TIMEOUT, "waitForKeylockChange() timeout.");
            } catch (InterruptedException ex) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "waitForKeylockChange is being interrupted.");
            }
        }
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }
}