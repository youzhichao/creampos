
// Copyright (c) 2000 HYI
package hyi.jpos.services;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import jpos.JposConst;
import jpos.JposException;
import jpos.MSR;
import jpos.MSRConst;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.services.EventCallbacks;
import jpos.services.MSRService14;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PartnerK19MSR implements MSRService14, KeyListener, ContainerListener {

    static private MSR claimedControl               = null;
    static private Object mutex                     = new Object();
    
    private String accountNumber                    = "";
    private boolean claimed                         = false;
    private boolean deviceEnabled                   = false;
    private int tracksToRead                        = MSRConst.MSR_TR_1_2_3;
    private JposEntry entry                         = null;
    private EventCallbacks eventCallbacks           = null;
    private byte[] b1                               = null;
    private byte[] b2                               = null;
    private byte[] b3                               = null;
    protected Container comp                      = null;
    protected Container comp2                     = null;
    protected Container comp3                     = null;


    /**
     * Constructor
     */
    public PartnerK19MSR(SimpleEntry entry) {
        this.entry = entry;
    }

    // Capabilities
    public boolean getCapISO() throws JposException {
        return true;
    }

    public boolean getCapJISOne() throws JposException {
        return false;
    }

    public boolean getCapJISTwo() throws JposException {
        return false;
    }

	public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties
    public String getCheckHealthText() throws JposException {
        return "";
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public String  getDeviceControlDescription() {
        return "";
    }

    public int     getDeviceControlVersion() {
        return 0;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    public void    setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        if (!claimed) {
            System.out.println("must claim it first");
            return;
        }
        if (deviceEnabled) {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                adkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                adkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                adkl(comp3);
            }
        } else {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                rmkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                rmkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                rmkl(comp3);
            }
        }
    }

    public String  getDeviceServiceDescription() throws JposException {
        return "";
    }

    public int     getDeviceServiceVersion() throws JposException {
        return 0;
    }

    public boolean getFreezeEvents() throws JposException {
        return false;
    }

    public void    setFreezeEvents(boolean freezeEvents) throws JposException {
    }

    public String  getPhysicalDeviceDescription() throws JposException {
        return "";
    }

    public String  getPhysicalDeviceName() throws JposException {
        return "";
    }

    public int     getState() {
        return JposConst.JPOS_S_CLOSED;
    }

    public String  getAccountNumber() throws JposException {
        return accountNumber;
    }

    public boolean getAutoDisable() throws JposException {
        return true;
    }

    public void    setAutoDisable(boolean autoDisable) throws JposException {
    }

    public int     getDataCount() throws JposException {
        return 0;
    }

    public boolean getDataEventEnabled() throws JposException {
        return true;
    }

    public void    setDataEventEnabled(boolean dataEventEnabled)
                       throws JposException {
    }

    public boolean getDecodeData() throws JposException {
        return true;
    }

    public void    setDecodeData(boolean decodeData) throws JposException {
    }

    public int     getErrorReportingType() throws JposException {
        return MSRConst.MSR_ERT_CARD;
    }

    public void    setErrorReportingType(int errorReportingType)
                       throws JposException {
    }

    public String  getExpirationDate() throws JposException {
        return "";
    }

    public String  getFirstName() throws JposException {
        return "";
    }

    public String  getMiddleInitial() throws JposException {
        return "";
    }

    public boolean getParseDecodeData() throws JposException {
        return true;
    }

    public void    setParseDecodeData(boolean parseDecodeData)
                       throws JposException {
    }

    public String  getServiceCode() throws JposException {
        return "";
    }

    public String  getSuffix() throws JposException {
        return "";
    }

    public String  getSurname() throws JposException {
        return "";
    }

    public String  getTitle() throws JposException {
        return "";
    }

    public byte[]  getTrack1Data() throws JposException {
        return b1;
    }

    public byte[]  getTrack1DiscretionaryData() throws JposException {
        return null;
    }

    public byte[]  getTrack2Data() throws JposException {
        return b2;
    }

    public byte[]  getTrack2DiscretionaryData() throws JposException {
        return null;
    }

    public byte[]  getTrack3Data() throws JposException {
        return b3;
    }     

    public byte[]  getTrack4Data() throws JposException {
        return null;
    }

    public int     getTracksToRead() throws JposException {
        return tracksToRead;
    }

    public void    setTracksToRead(int tracksToRead) throws JposException {
    }

	public int     getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

	public void    setPowerNotify(int powerNotify) throws JposException {
    }

	public int     getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    public void setTransmitSentinels(boolean b) {
    }

    public boolean getTransmitSentinels() {
        return false;
    }

    public boolean getCapTransmitSentinels() {
        return false;
    }

    // Methods
    public void deleteInstance() throws JposException {
    }
    
    public void    claim(int timeout) throws JposException {
        if (claimed) {
            System.out.println("device has been claimed");
            return;
        }
        synchronized (mutex) {
            if (claimedControl == null) {
                claimedControl = (MSR)eventCallbacks.getEventSource();
                claimed = true;
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                    claimedControl = (MSR)eventCallbacks.getEventSource();
                    claimed = true;
                    return;
                } catch (InterruptedException ex1) {
                    System.out.println(ex1.toString());
                    System.out.println("Interrupted exception at " + this);
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        if (claimedControl == null) {
                            claimedControl = (MSR)eventCallbacks.getEventSource();
                            claimed = true;
                            return;
                        }
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    System.out.println("device is busy");
                    return;
                } catch (InterruptedException ex1) {
                    System.out.println(ex1.toString());
                    System.out.println("Interrupted exception at " + this);
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        }
    }

    public void    close() throws JposException {
        claimed = false;
        deviceEnabled = false;
        claimedControl = null;
    }

    public void    checkHealth(int level) throws JposException {
    }

    public void    directIO(int command, int[] data, Object object)
                       throws JposException {
    }

    public void    open(String logicalDeviceName, EventCallbacks eventCallbacks)
            throws JposException {
        this.eventCallbacks = eventCallbacks;
    }

    public void    release() throws JposException {
        synchronized (mutex) {
            if (!claimed) {
                System.out.println("device has been release");
            }
            claimed = false;
            deviceEnabled = false;
            claimedControl = null;
            mutex.notifyAll();
        }
    }

    public void    clearInput() throws JposException {
    }

    //event 
    public void keyReleased(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {

        char keyChar = e.getKeyChar();
        int keyCode = (int)keyChar;
        if (keyChar == ';') {
            keyStart = true;
        }

        if (keyStart && keyCode == 10) {
            //  b is result
            System.out.println(b);
            keyStart = false;
        } else if (keyStart) {
            b = b + keyChar;
        }
    }

    public Container createComp(String className) {
        try {
            Class compClass = Class.forName(className);
            Method compMethod = compClass.getDeclaredMethod("getInstance",
                                                            new Class[0]);
            comp = (Container)(compMethod.invoke(null, new Object[0]));
        } catch (ClassNotFoundException e) {
            System.out.println(e.toString());
            System.out.println("Class is not found at " + this);
        } catch (IllegalAccessException ex) {
            System.out.println(ex.toString());
            System.out.println("Illegal access exception at " + this);
        } catch (NoSuchMethodException exc) {
            System.out.println(exc.toString());
            System.out.println("No such method at " + this);
        } catch (InvocationTargetException exce) {
            System.out.println(exce.toString());
            System.out.println("Invocation exception at " + this);
        }
        return comp;
    }

    public void adkl(Container comp) {
        comp.addKeyListener(this);
        comp.addContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                adkl(compo);
            } else {
                co.addKeyListener(this);
            }
        }
    }

    public void rmkl(Container comp) {
        comp.removeKeyListener(this);
        comp.removeContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                rmkl(compo);
            } else {
                co.removeKeyListener(this);
            }
        }
    }

    public void componentAdded(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            adkl((Container)e.getChild());
        } else {
            e.getChild().addKeyListener(this);
        }
    }

    public void componentRemoved(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            rmkl((Container)e.getChild());
        } else {
            e.getChild().removeKeyListener(this);
        }
    }

    private String b = "";
    private String keyString = "";
    private boolean keyStart = false;
    private boolean endCountStart = false;
    private int endCount = 0;
    int i = 0;
}

