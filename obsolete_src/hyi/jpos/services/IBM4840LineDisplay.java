package hyi.jpos.services;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Enumeration;

import javax.comm.CommPortIdentifier;
import javax.comm.NoSuchPortException;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.UnsupportedCommOperationException;

import jpos.JposConst;
import jpos.JposException;
import jpos.LineDisplay;
import jpos.LineDisplayConst;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.services.EventCallbacks;
import jpos.services.LineDisplayService19;

//import org.apache.log4j.Logger;

public class IBM4840LineDisplay extends AbstractDeviceService19 implements LineDisplayService19 {

//    static Logger logger = Logger.getLogger(IBM4840LineDisplay.class);

    static private LineDisplay claimedControl;
    static private Object mutex = new Object();
    static private char[] ch = new char[40];

    private boolean capDescriptors = false;
    private int characterSet = 0;
    private String characterSetList = "";
    private boolean claimed = false;
    private int columns = 0;
    private int currentWindow = 0;
    private int cursorColumn = 0;
    private int cursorRow = 0;
    private boolean cursorUpdate = false;
    private int deviceBrightness = 0;
    private int deviceColumns = 0;
    private int deviceDescriptors = 0;
    private boolean deviceEnabled = false;
    private int deviceRows = 0;
    private int deviceWindows = 0;
    private int interCharacterWait = 0;
    private int marqueeFormat = 0;
    private int marqueeRepeatWait = 0;
    private int marqueeType = 0;
    private int marqueeUnitWait = 0;
    private int rows = 0;
    // private boolean opened = false;

    protected String checkHealthText = "";
    protected String deviceServiceDescription = "JavaPOS Service from HYI Corporation";
    protected int deviceServiceVersion = 1006000;
    protected boolean freezeEvents = false;
    protected String physicalDeviceDescription = "";
    protected String physicalDeviceName = "";
    protected int state = JposConst.JPOS_S_CLOSED;
    protected int powerNotify = JposConst.JPOS_PN_DISABLED;
    protected int powerState = JposConst.JPOS_PS_UNKNOWN;

    protected EventCallbacks eventCallbacks = null;
    protected JposEntry entry = null;

    static Enumeration portList;
    static CommPortIdentifier portId;
    static SerialPort serialPort;
    static OutputStream outputStream;

    /**
     * Constructor
     */
    public IBM4840LineDisplay(SimpleEntry entry) {
        this.entry = entry;
    }

    // Capabilities
    public int getCapBlink() throws JposException {
        return LineDisplayConst.DISP_CB_NOBLINK;
    }

    public boolean getCapBrightness() throws JposException {
        return false;
    }

    public int getCapCharacterSet() throws JposException {
        return LineDisplayConst.DISP_CCS_ASCII;
    }

    public boolean getCapDescriptors() throws JposException {
        return capDescriptors;
    }

    public boolean getCapHMarquee() throws JposException {
        return true;
    }

    public boolean getCapICharWait() throws JposException {
        return false;
    }

    public boolean getCapVMarquee() throws JposException {
        return false;
    }

    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties
    public String getCheckHealthText() throws JposException {
        return checkHealthText;
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        if (claimed) {
            this.deviceEnabled = deviceEnabled;
        } else {
//            logger.debug("lineDisplay error: must claim it first");
        }
    }

    public String getDeviceServiceDescription() throws JposException {
        return deviceServiceDescription;
    }

    public int getDeviceServiceVersion() throws JposException {
        return deviceServiceVersion;
    }

    public boolean getFreezeEvents() throws JposException {
        return freezeEvents;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        this.freezeEvents = freezeEvents;
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return physicalDeviceDescription;
    }

    public String getPhysicalDeviceName() throws JposException {
        return physicalDeviceName;
    }

    public int getState() throws JposException {
        return state;
    }

    public int getCharacterSet() throws JposException {
        return characterSet;
    }

    public void setCharacterSet(int characterSet) throws JposException {
        this.characterSet = characterSet;
    }

    public String getCharacterSetList() throws JposException {
        return characterSetList;
    }

    public int getColumns() throws JposException {
        return columns;
    }

    public int getCurrentWindow() throws JposException {
        return currentWindow;
    }

    public void setCurrentWindow(int currentWindow) throws JposException {
        this.currentWindow = currentWindow;
    }

    public int getCursorColumn() throws JposException {
        return cursorColumn;
    }

    public void setCursorColumn(int cursorColumn) throws JposException {
        this.cursorColumn = cursorColumn;
    }

    public int getCursorRow() throws JposException {
        return cursorRow;
    }

    public void setCursorRow(int cursorRow) throws JposException {
        this.cursorRow = cursorRow;
    }

    public boolean getCursorUpdate() throws JposException {
        return cursorUpdate;
    }

    public void setCursorUpdate(boolean cursorUpdate) throws JposException {
        this.cursorUpdate = cursorUpdate;
    }

    public int getDeviceBrightness() throws JposException {
        return deviceBrightness;
    }

    public void setDeviceBrightness(int deviceBrightness) throws JposException {
        this.deviceBrightness = deviceBrightness;
    }

    public int getDeviceColumns() throws JposException {
        return deviceColumns;
    }

    public int getDeviceDescriptors() throws JposException {
        return deviceDescriptors;
    }

    public int getDeviceRows() throws JposException {
        return deviceRows;
    }

    public int getDeviceWindows() throws JposException {
        return deviceWindows;
    }

    public int getInterCharacterWait() throws JposException {
        return interCharacterWait;
    }

    public void setInterCharacterWait(int interCharacterWait) throws JposException {
        this.interCharacterWait = interCharacterWait;
    }

    public int getMarqueeFormat() throws JposException {
        return marqueeFormat;
    }

    public void setMarqueeFormat(int marqueeFormat) throws JposException {
        this.marqueeFormat = marqueeFormat;
    }

    public int getMarqueeRepeatWait() throws JposException {
        return marqueeRepeatWait;
    }

    public void setMarqueeRepeatWait(int marqueeRepeatWait) throws JposException {
        this.marqueeRepeatWait = marqueeRepeatWait;
    }

    public int getMarqueeType() throws JposException {
        return marqueeType;
    }

    public void setMarqueeType(int marqueeType) throws JposException {
        this.marqueeType = marqueeType;
    }

    public int getMarqueeUnitWait() throws JposException {
        return marqueeUnitWait;
    }

    public void setMarqueeUnitWait(int marqueeUnitWait) throws JposException {
        this.marqueeUnitWait = marqueeUnitWait;
    }

    public int getRows() throws JposException {
        return rows;
    }

    public int getPowerNotify() throws JposException {
        return powerNotify;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        this.powerNotify = powerNotify;
    }

    public int getPowerState() throws JposException {
        return powerState;
    }

    public void connect() {
        deviceEnabled = false;
        try {
//            logger.debug("設定IBM4840LineDisplay Serial port:" + (String)entry.getPropertyValue("portName"));
            portId = CommPortIdentifier.getPortIdentifier((String)entry
                .getPropertyValue("portName"));
            // if (portId.isCurrentlyOwned()) {
            // logger.debug("lineDisplay error : The device is being use!");
            // return;
            // }
//            logger.debug("serialPort open...");
            serialPort = (SerialPort)portId.open(entry.getLogicalName(), 10000);
//            logger.debug("getOutputStream...");
            outputStream = serialPort.getOutputStream();
//            logger.debug("setSerialPortParams");

            serialPort.setSerialPortParams((Integer)entry.getPropertyValue("baudRate"),
                (Integer)entry.getPropertyValue("dataBits"), (Integer)entry
                    .getPropertyValue("stopBits"), (Integer)entry.getPropertyValue("parity"));
            // serialPort.setFlowControlMode((Integer)entry.getPropertyValue("flowControl"));
            outputStream.write(0x1f); // initialize display
            outputStream.write(0x11);
            outputStream.write(0x14);
        } catch (NoSuchPortException e) {
//            logger.error("no port: " + (String)entry.getPropertyValue("portName")
//                + "on this machine", e);
        } catch (PortInUseException e1) {
//            logger.error("Port in use: " + portId, e1);
        } catch (IOException e2) {
//            logger.error("", e2);
        } catch (UnsupportedCommOperationException e3) {
//            logger.error("Unsupported comm operation", e3);
        } catch (Exception oe) {
//            logger.error("", oe);
        }
        return;
    }

    // Methods supported by all device services.
    public void claim(int timeout) throws JposException {
        if (claimed) {
//            logger.debug("lineDisplay error : device has been claimed");
            return;
        }
        // synchronized (mutex) {
        if (claimedControl == null) {
            claimedControl = (LineDisplay)eventCallbacks.getEventSource();
            connect();
            claimed = true;
            return;
        }
        if (timeout < -1) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL);
        } else if (timeout == JposConst.JPOS_FOREVER) {
            try {
                while (claimedControl != null) {
                    mutex.wait();
                }
                claimedControl = (LineDisplay)eventCallbacks.getEventSource();
                connect();
                return;
            } catch (InterruptedException ex1) {
//                logger.warn("Interrupted exception", ex1);
            }
        } else {
            try {
                long start = System.currentTimeMillis();
                long waitTime = timeout;

                while (waitTime > 0) {
                    mutex.wait(waitTime);
                    // wait() returns when time is up or being notify().

                    // ///////// plus checking claimedControl with "this"
                    if (claimedControl == null) {
                        claimedControl = (LineDisplay)eventCallbacks.getEventSource();
                        connect();
                        return;
                    }

                    // if wait not enough time, it should keep wait() again....
                    waitTime = timeout - (System.currentTimeMillis() - start);
                }
//                logger.debug("lineDisplay error : device is busy");
                return;
            } catch (InterruptedException ex1) {
//                logger.warn("Interrupted exception", ex1);
            }

            throw new JposException(JposConst.JPOS_E_TIMEOUT);
        }
        // }
    }

    public void close() throws JposException {
        claimed = false;
        deviceEnabled = false;
        // opened = false;
    }

    public void checkHealth(int level) throws JposException {
    }

    public void deleteInstance() throws JposException {
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
    }

    public void open(String logicalName, EventCallbacks cb) throws JposException {
        eventCallbacks = cb;
        this.setCursorColumn(0);
        this.setCursorRow(0);
        String str = "                                        ";
        ch = str.toCharArray();
        // opened = true;
        claimed = false;
        deviceEnabled = false;
    }

    synchronized public void release() throws JposException {
        synchronized (mutex) {
            if (!claimed) {
//                logger.debug("lineDisplay error : device has been release");
            }
            try {
                outputStream.write(12); // clear display screen, and clear string mode
                outputStream.write(27); // set cursor OFF
                outputStream.write(95);
                outputStream.write(0);
            } catch (IOException e4) {
//                logger.error("release() fail", e4);
            }
            serialPort.close();
            outputStream = null;
            portList = null;
            portId = null;
            serialPort = null;
            claimed = false;
            deviceEnabled = false;
            claimedControl = null;
            mutex.notifyAll();
        }
    }

    public void clearDescriptors() throws JposException {
        if (!capDescriptors) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL);
        } else {
            deviceServiceDescription = "";
            physicalDeviceDescription = "";
        }
    }

    public void clearText() throws JposException {
        if (deviceEnabled) {
            try {
                outputStream.write(12); // clear display screen, and clear string mode
            } catch (IOException e4) {
//                logger.error("clearText() fail", e4);
            }
            setCursorColumn(0);
            setCursorRow(0);
            String str = "                                        ";
            ch = str.toCharArray();
        } else {
//            logger.debug("lineDisplay error : Not DeviceEnabled");
        }
    }

    public void createWindow(int viewportRow, int viewportColumn, int viewportHeight,
        int viewportWidth, int windowHeight, int windowWidth) throws JposException {
    }

    public void destroyWindow() throws JposException {
    }

    public void displayText(String data, int attribute) throws JposException {
        if (!claimed) {
//            logger.debug("lineDisplay error : Please first claim it!");
            return;
        }
        if (!deviceEnabled) {
//            logger.debug("lineDisplay error : Not setDeviceEnable");
            return;
        }

        try {
//            logger.debug("output:[" + data + "]");
            OutputStreamWriter dataOutput = new OutputStreamWriter(outputStream);
            dataOutput.write(0x10);
            dataOutput.write(0x00);
            dataOutput.write(data);
            dataOutput.flush();
        } catch (IOException e4) {
//            logger.error("displayText fail", e4);
        }

        claimed = true;
        deviceEnabled = true;
    }

    public void displayTextAt(int y, int x, String data, int attribute) throws JposException {
        if (!claimed) {
//            logger.debug("lineDisplay error : Please first claim it!");
            return;
        }
        if (!deviceEnabled) {
//            logger.debug("lineDisplay error : Not setDeviceEnable");
            return;
        }

        try {
//            logger.debug("output(" + x + "," + y + "):[" + data + "]");
            outputStream.write(27); // set cursor on x,y
            outputStream.write(108);
            outputStream.write(x + 1);
            outputStream.write(y + 1);

            OutputStreamWriter dataOutput = new OutputStreamWriter(outputStream);
            dataOutput.write(data);
            dataOutput.flush();
        } catch (IOException e4) {
//            logger.error("displayTextAt() fail", e4);
        }
        setCursorColumn(x);
        setCursorRow(y + 1);
        claimed = true;
        deviceEnabled = true;
    }

    public void refreshWindow(int window) throws JposException {
    }

    public void scrollText(int direction, int units) throws JposException {
        // this.direction = direction;
        // this.units = units;
        String s = "                                        ";
        char[] c = s.toCharArray();
        if (direction == DISP_ST_LEFT) {
            for (int i = 0; i < (20 - units); i++) {
                c[i] = ch[units + i];
                c[20 + i] = ch[20 + units + i];
            }
        } else if (direction == DISP_ST_RIGHT) {
            for (int i = 0; i < (20 - units); i++) {
                c[units + i] = ch[i];
                c[20 + units + i] = ch[20 + i];
            }
        }
        try {
            outputStream.write(12); // clear screen
            for (int i = 0; i < 40; i++) {
                outputStream.write(c[i]); // display string
            }
        } catch (IOException e4) {
//            logger.error("write data fail", e4);
        }
    }

    public void setDescriptor(int descriptor, int attribute) throws JposException {
    }

    // Capabilities
    public boolean getCapBlinkRate() throws JposException {
        return false;
    }

    public int getCapCursorType() throws JposException {
        return 0;
    }

    public boolean getCapCustomGlyph() throws JposException {
        return false;
    }

    public int getCapReadBack() throws JposException {
        return 0;
    }

    public int getCapReverse() throws JposException {
        return 0;
    }

    // Properties
    public int getBlinkRate() throws JposException {
        return 0;
    }

    public void setBlinkRate(int blinkRate) throws JposException {
    }

    public int getCursorType() throws JposException {
        return 0;
    }

    public void setCursorType(int cursorType) throws JposException {
    }

    public String getCustomGlyphList() throws JposException {
        return "";
    }

    public int getGlyphHeight() throws JposException {
        return 0;
    }

    public int getGlyphWidth() throws JposException {
        return 0;
    }

    // Methods
    public void defineGlyph(int glyphCode, byte[] glyph) throws JposException {
    }

    public void readCharacterAtCursor(int[] aChar) throws JposException {
    }

    public static int DISP_ST_UP = 0;
    public static int DISP_ST_DOWN = 1;
    public static int DISP_ST_LEFT = 2;
    public static int DISP_ST_RIGHT = 3;

    // private int direction = DISP_ST_LEFT;
    // private int units = 0;

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }

    public void displayBitmap(String fileName, int width, int alignmentX, int alignmentY)
        throws JposException {
    }

    public boolean getCapBitmap() throws JposException {
        return false;
    }

    public boolean getCapMapCharacterSet() throws JposException {
        return false;
    }

    public boolean getCapScreenMode() throws JposException {
        return false;
    }

    public boolean getMapCharacterSet() throws JposException {
        return false;
    }

    public int getMaximumX() throws JposException {
        return 0;
    }

    public int getMaximumY() throws JposException {
        return 0;
    }

    public int getScreenMode() throws JposException {
        return 0;
    }

    public String getScreenModeList() throws JposException {
        return null;
    }

    public void setBitmap(int bitmapNumber, String fileName, int width, int alignmentX,
        int alignmentY) throws JposException {
    }

    public void setMapCharacterSet(boolean mapCharacterSet) throws JposException {
    }

    public void setScreenMode(int screenMode) throws JposException {
    }
}
