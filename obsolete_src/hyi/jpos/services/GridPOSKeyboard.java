package hyi.jpos.services;

import hyi.cream.POSButtonHome;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.DacTransfer;
import hyi.cream.uibeans.DacViewer;
import hyi.cream.uibeans.ItemList;
import hyi.cream.uibeans.PayingPaneBanner;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.uibeans.SignOffForm;
import hyi.cream.util.CreamToolkit;
import jpos.*;
import jpos.services.*;
import jpos.events.*;
import java.awt.*;
import java.awt.event.*;
import jpos.config.simple.*;
import java.util.*;
import java.util.List;

/**
 * This is device service for Posiflex KB6600 POS Keyboard.
 * <P>
 * <BR>
 * The following properties are supported:
 * <LI> AutoDisable
 * <LI> DataCount
 * <LI> DataEventEnabled
 * <LI> FreezeEvents
 * <P>
 * <BR>
 * But the following properties are not supported and sticked to fixed values:
 * <LI> CapCompareFirmwareVersion is false
 * <LI> CapPowerReporting is JPOS_PR_NONE
 * <LI> CapStatisticsReporting is false
 * <LI> CapUpdateFirmware is false
 * <LI> CapUpdateStatistics is false
 * <LI> PowerNotify is always JPOS_PN_DISABLED
 * <LI> PowerState is always JPOS_PS_UNKNOWN
 * <LI> CapKeyUp is false
 * <LI> EventTypes is ignored, now we only support key down events
 * <LI> POSKeyEventType is always POSKeyboardConst.KBD_ET_DOWN
 * <P>
 * <BR>
 * This device service doesn't consider thread-safe issue yet.
 * <P>
 * <BR>
 * Note: Because this JavaPOS device service intercept and get keyboard event from AWT thread,
 * application cannot receive any POSKeyboard/Keylock/MSR event from within AWT event processing.
 * Our JavaPOS user should not expect to get any JavaPOS event within their AWT event processing
 * method. <p/> Key board code map:
 * 
 * <pre>
 *   Q (1)  W (7)   E (13)  R (19)  T (25)  Y (31)  U (37)  I (43)  O (49)  P (55)  F1 (61) F7 (67)         
 *   A (2)  S (8)   D (14)  F (20)  G (26)  H (32)  J (38)  K (44)  L (50)  : (56)  F2 (62) F8 (68)          
 *   Z (3)  X (9)   C (15)  V (21)  B (27)  N (33)  M (39)  &lt; (45)  &gt; (51)  ? (57)  F3 (63) 7 (69) 8 (73)     9 (76)  - (80)
 *   q (4)  w (10)  e (16)  r (22)  t (28)  y (34)  u (40)  i (46)  o (52)  p (58)  F4 (64) 4 (70) 5 (74)     6 (77)  + (81)
 *   a (5)  s (11)  d (17)  f (23)  g (29)  h (35)  j (41)  k (47)  l (53)  ; (59)  F5 (65) 1 (71) 2 (75)     3 (78)  ENTER (82)
 *   z (6)  x (12)  c (18)  v (24)  b (30)  n (36)  m (42)  , (48)  ' (54)  / (60)  F6 (66) 0 (72) 00 (72,72) . (79)    
 * 
 *   Sample JCL config:
 *   
 *  &lt;JposEntry logicalName=&quot;PosiflexKB6600POSKeyboard&quot;&gt;
 *    &lt;creation factoryClass=&quot;hyi.jpos.loader.ServiceInstanceFactory&quot;
 *      serviceClass=&quot;hyi.jpos.services.PosiflexKB6600POSKeyboard&quot; /&gt;
 *    &lt;vendor name=&quot;Posiflex&quot; url=&quot;http://www.hyi.com.tw&quot; /&gt;
 *    &lt;jpos category=&quot;POSKeyboard&quot; version=&quot;1.9&quot; /&gt;
 *    &lt;product description=&quot;Posiflex KB-6600 POS Keyboard&quot;
 *      name=&quot;Posiflex KB-6600 POS Keyboard&quot; url=&quot;http://www.hyi.com.tw&quot; /&gt;
 * 
 *    &lt;!-- Key Code Mapping --&gt;
 *    &lt;prop name=&quot;K1&quot; type=&quot;String&quot; value=&quot;Shift-Q&quot; /&gt;
 *    &lt;prop name=&quot;K2&quot; type=&quot;String&quot; value=&quot;Shift-A&quot; /&gt;
 *    &lt;prop name=&quot;K3&quot; type=&quot;String&quot; value=&quot;Shift-Z&quot; /&gt;
 *    &lt;prop name=&quot;K4&quot; type=&quot;String&quot; value=&quot;Q&quot; /&gt;
 *    &lt;prop name=&quot;K5&quot; type=&quot;String&quot; value=&quot;A&quot; /&gt;
 *    &lt;prop name=&quot;K6&quot; type=&quot;String&quot; value=&quot;Z&quot; /&gt;  
 *    &lt;prop name=&quot;K7&quot; type=&quot;String&quot; value=&quot;Shift-W&quot; /&gt;
 *    &lt;prop name=&quot;K8&quot; type=&quot;String&quot; value=&quot;Shift-S&quot; /&gt;
 *    &lt;prop name=&quot;K9&quot; type=&quot;String&quot; value=&quot;Shift-X&quot; /&gt;
 *    &lt;prop name=&quot;K10&quot; type=&quot;String&quot; value=&quot;W&quot; /&gt;
 *    &lt;prop name=&quot;K11&quot; type=&quot;String&quot; value=&quot;S&quot; /&gt;
 *    &lt;prop name=&quot;K12&quot; type=&quot;String&quot; value=&quot;X&quot; /&gt;  
 *    &lt;prop name=&quot;K13&quot; type=&quot;String&quot; value=&quot;Shift-E&quot; /&gt;
 *    &lt;prop name=&quot;K14&quot; type=&quot;String&quot; value=&quot;Shift-D&quot; /&gt;
 *    &lt;prop name=&quot;K15&quot; type=&quot;String&quot; value=&quot;Shift-C&quot; /&gt;
 *    &lt;prop name=&quot;K16&quot; type=&quot;String&quot; value=&quot;E&quot; /&gt;
 *    &lt;prop name=&quot;K17&quot; type=&quot;String&quot; value=&quot;D&quot; /&gt;
 *    &lt;prop name=&quot;K18&quot; type=&quot;String&quot; value=&quot;C&quot; /&gt;  
 *    &lt;prop name=&quot;K19&quot; type=&quot;String&quot; value=&quot;Shift-R&quot; /&gt;
 *    &lt;prop name=&quot;K20&quot; type=&quot;String&quot; value=&quot;Shift-F&quot; /&gt;
 *    &lt;prop name=&quot;K21&quot; type=&quot;String&quot; value=&quot;Shift-V&quot; /&gt;
 *    &lt;prop name=&quot;K22&quot; type=&quot;String&quot; value=&quot;R&quot; /&gt;
 *    &lt;prop name=&quot;K23&quot; type=&quot;String&quot; value=&quot;F&quot; /&gt;
 *    &lt;prop name=&quot;K24&quot; type=&quot;String&quot; value=&quot;V&quot; /&gt;  
 *    &lt;prop name=&quot;K25&quot; type=&quot;String&quot; value=&quot;Shift-T&quot; /&gt;
 *    &lt;prop name=&quot;K26&quot; type=&quot;String&quot; value=&quot;Shift-G&quot; /&gt;
 *    &lt;prop name=&quot;K27&quot; type=&quot;String&quot; value=&quot;Shift-B&quot; /&gt;
 *    &lt;prop name=&quot;K28&quot; type=&quot;String&quot; value=&quot;T&quot; /&gt;
 *    &lt;prop name=&quot;K29&quot; type=&quot;String&quot; value=&quot;G&quot; /&gt;
 *    &lt;prop name=&quot;K30&quot; type=&quot;String&quot; value=&quot;B&quot; /&gt;  
 *    &lt;prop name=&quot;K31&quot; type=&quot;String&quot; value=&quot;Shift-Y&quot; /&gt;
 *    &lt;prop name=&quot;K32&quot; type=&quot;String&quot; value=&quot;Shift-H&quot; /&gt;
 *    &lt;prop name=&quot;K33&quot; type=&quot;String&quot; value=&quot;Shift-N&quot; /&gt;
 *    &lt;prop name=&quot;K34&quot; type=&quot;String&quot; value=&quot;Y&quot; /&gt;
 *    &lt;prop name=&quot;K35&quot; type=&quot;String&quot; value=&quot;H&quot; /&gt;
 *    &lt;prop name=&quot;K36&quot; type=&quot;String&quot; value=&quot;N&quot; /&gt;  
 *    &lt;prop name=&quot;K37&quot; type=&quot;String&quot; value=&quot;Shift-U&quot; /&gt;
 *    &lt;prop name=&quot;K38&quot; type=&quot;String&quot; value=&quot;Shift-J&quot; /&gt;
 *    &lt;prop name=&quot;K39&quot; type=&quot;String&quot; value=&quot;Shift-M&quot; /&gt;
 *    &lt;prop name=&quot;K40&quot; type=&quot;String&quot; value=&quot;U&quot; /&gt;
 *    &lt;prop name=&quot;K41&quot; type=&quot;String&quot; value=&quot;J&quot; /&gt;
 *    &lt;prop name=&quot;K42&quot; type=&quot;String&quot; value=&quot;M&quot; /&gt;  
 *    &lt;prop name=&quot;K43&quot; type=&quot;String&quot; value=&quot;Shift-I&quot; /&gt;
 *    &lt;prop name=&quot;K44&quot; type=&quot;String&quot; value=&quot;Shift-K&quot; /&gt;
 *    &lt;prop name=&quot;K45&quot; type=&quot;String&quot; value=&quot;Shift-Comma&quot; /&gt;
 *    &lt;prop name=&quot;K46&quot; type=&quot;String&quot; value=&quot;I&quot; /&gt;
 *    &lt;prop name=&quot;K47&quot; type=&quot;String&quot; value=&quot;K&quot; /&gt;
 *    &lt;prop name=&quot;K48&quot; type=&quot;String&quot; value=&quot;Comma&quot; /&gt;
 *    &lt;prop name=&quot;K49&quot; type=&quot;String&quot; value=&quot;Shift-O&quot; /&gt;
 *    &lt;prop name=&quot;K50&quot; type=&quot;String&quot; value=&quot;Shift-L&quot; /&gt;
 *    &lt;prop name=&quot;K51&quot; type=&quot;String&quot; value=&quot;Shift-Period&quot; /&gt;
 *    &lt;prop name=&quot;K52&quot; type=&quot;String&quot; value=&quot;O&quot; /&gt;
 *    &lt;prop name=&quot;K53&quot; type=&quot;String&quot; value=&quot;L&quot; /&gt;
 *    &lt;prop name=&quot;K54&quot; type=&quot;String&quot; value=&quot;Quote&quot; /&gt;
 *    &lt;prop name=&quot;K55&quot; type=&quot;String&quot; value=&quot;Shift-P&quot; /&gt;
 *    &lt;prop name=&quot;K56&quot; type=&quot;String&quot; value=&quot;Shift-Semicolon&quot; /&gt;
 *    &lt;prop name=&quot;K57&quot; type=&quot;String&quot; value=&quot;Shift-Slash&quot; /&gt;
 *    &lt;prop name=&quot;K58&quot; type=&quot;String&quot; value=&quot;P&quot; /&gt;
 *    &lt;prop name=&quot;K59&quot; type=&quot;String&quot; value=&quot;Semicolon&quot; /&gt;
 *    &lt;prop name=&quot;K60&quot; type=&quot;String&quot; value=&quot;Slash&quot; /&gt;
 *    &lt;prop name=&quot;K61&quot; type=&quot;String&quot; value=&quot;F1&quot; /&gt;
 *    &lt;prop name=&quot;K62&quot; type=&quot;String&quot; value=&quot;F2&quot; /&gt;
 *    &lt;prop name=&quot;K63&quot; type=&quot;String&quot; value=&quot;F3&quot; /&gt;
 *    &lt;prop name=&quot;K64&quot; type=&quot;String&quot; value=&quot;F4&quot; /&gt;
 *    &lt;prop name=&quot;K65&quot; type=&quot;String&quot; value=&quot;F5&quot; /&gt;
 *    &lt;prop name=&quot;K66&quot; type=&quot;String&quot; value=&quot;F6&quot; /&gt;
 *    &lt;prop name=&quot;K67&quot; type=&quot;String&quot; value=&quot;F7&quot; /&gt;
 *    &lt;prop name=&quot;K68&quot; type=&quot;String&quot; value=&quot;F8&quot; /&gt;
 *    &lt;prop name=&quot;K69&quot; type=&quot;String&quot; value=&quot;7&quot; /&gt;
 *    &lt;prop name=&quot;K70&quot; type=&quot;String&quot; value=&quot;4&quot; /&gt;
 *    &lt;prop name=&quot;K71&quot; type=&quot;String&quot; value=&quot;1&quot; /&gt;
 *    &lt;prop name=&quot;K72&quot; type=&quot;String&quot; value=&quot;0&quot; /&gt;
 *    &lt;prop name=&quot;K73&quot; type=&quot;String&quot; value=&quot;8&quot; /&gt;
 *    &lt;prop name=&quot;K74&quot; type=&quot;String&quot; value=&quot;5&quot; /&gt;
 *    &lt;prop name=&quot;K75&quot; type=&quot;String&quot; value=&quot;2&quot; /&gt;
 *    &lt;prop name=&quot;K76&quot; type=&quot;String&quot; value=&quot;9&quot; /&gt;
 *    &lt;prop name=&quot;K77&quot; type=&quot;String&quot; value=&quot;6&quot; /&gt;
 *    &lt;prop name=&quot;K78&quot; type=&quot;String&quot; value=&quot;3&quot; /&gt;
 *    &lt;prop name=&quot;K79&quot; type=&quot;String&quot; value=&quot;Period&quot; /&gt;
 *    &lt;prop name=&quot;K80&quot; type=&quot;String&quot; value=&quot;NumPad -&quot; /&gt;
 *    &lt;prop name=&quot;K81&quot; type=&quot;String&quot; value=&quot;NumPad +&quot; /&gt;
 *    &lt;prop name=&quot;K82&quot; type=&quot;String&quot; value=&quot;Enter&quot; /&gt;
 *  &lt;/JposEntry&gt;
 * </pre>
 * 
 * @author Bruce You @ Hongyuan Software(Shanghai)
 * @since 2007-2-13
 */
public class GridPOSKeyboard extends AbstractDeviceService19 implements
    POSKeyboardService19 {

    private static final int MSR_START_CODE = 222;

    private int keylockStartCode;
    private int keylockCharCount;
    private static POSKeyboard claimedControl;
    private static Object mutex = new Object();
    private int posKeyData;
    private boolean dataEventEnabled;
    private Map<String, Integer> keyMap = new HashMap<String, Integer>();
    private KeyEventDispatcher keyEventInterceptor;
    private List eventQueue = new ArrayList();
    private boolean autoDisable;
	private int clearButtonCount = 0;

    /**
     * Default constructor of PosiflexKB6600POSKeyboard. This constructor will create a key
     * name/code map and a KeyEvent interceptor of the Java AWT system.
     */
    public GridPOSKeyboard() {
    	System.out.println("Constructing GridPOSKeyboard...");
        setDeviceServiceDescription("Posiflex KB6600 POS Keyboard JavaPOS Device Service from HYI");
        setPhysicalDeviceDescription("Posiflex KB6600 POS Keyboard");
        setPhysicalDeviceName("Posiflex KB6600 POS Keyboard");
        createKeyEventInterceptor();
        setState(JposConst.JPOS_S_CLOSED);
    }

    /**
     * Constructor of PosiflexKB6600POSKeyboard which accepts a JavaPOS registry entry.
     * 
     * @param entry
     *            A JavaPOS registry SimpleEntry.
     */
    public GridPOSKeyboard(SimpleEntry entry) {
        this();
        try {
        	createKeyMap(entry);
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    /**
     * Create key name and key code map.
     */
    private void createKeyMap(SimpleEntry entry) {
    	keylockStartCode = (Integer)entry.getPropertyValue("KeylockPrefixCharacter");
    	keylockCharCount = (Integer)entry.getPropertyValue("KeylockCharCount");

    	Enumeration iter = entry.getPropertyNames();
    	while (iter.hasMoreElements()) {
    		String propName = (String)iter.nextElement();
    		if (propName.startsWith("K")) {
    			try {
    				String keyName = (String)entry.getPropertyValue(propName);
    				keyMap.put(keyName, Integer.parseInt(propName.substring(1)));
    				//System.out.println("GridPOSKeyboard> " + keyName + "=" + Integer.parseInt(propName.substring(1)));
    			} catch (Exception e) {
    			}
    		}
    	}
    }

    /**
     * Create a keyboard event interceptor for firing POS keyboard event. The interceptor is an
     * AWT's KeyEventDispatcher, and then it'll be added into KeyboardFocusManager when enabling
     * this device.
     */
    private void createKeyEventInterceptor() {

        keyEventInterceptor = new KeyEventDispatcher() {
            boolean withinControlSeq;
            boolean absorbRelatedKeyEvent;
            boolean bypassRelatedKeyEvent;
            int keylockCharCountGot;

            private boolean withinKeylockMSRControlSequence(int code) {
                if (code == keylockStartCode) { // || code == MSR_START_CODE) {
                    withinControlSeq = true;
                    keylockCharCountGot = 1;
                    return true;
                } else if (withinControlSeq) {
                	if (++keylockCharCountGot >= keylockCharCount) {
	                    withinControlSeq = false;
	                    bypassRelatedKeyEvent = true;
                	}
                    return true;
                }
                return false;
            }

            public boolean dispatchKeyEvent(KeyEvent e) {
                try {
                    setState(JposConst.JPOS_S_BUSY);

                    if (e.getID() != KeyEvent.KEY_PRESSED) {

                        if (absorbRelatedKeyEvent) {
                            if (e.getID() == KeyEvent.KEY_RELEASED)
                                absorbRelatedKeyEvent = false;
                            return true;
                        }

                        if (bypassRelatedKeyEvent) {
                            if (e.getID() == KeyEvent.KEY_RELEASED)
                                bypassRelatedKeyEvent = false;
                            return false;
                        }

                        // While in-between control sequence of keylock, return false
                        // for not absorting any key event and sending them to application,
                        // or other key dispatcher like Keylock or MSRs'
                        if (withinControlSeq)
                            return false;

                        return false;
                    }

                    int code = e.getKeyCode();
                    int modifiers = e.getModifiers();

                    if (withinKeylockMSRControlSequence(code))
                        return false;

                    String keyName = ((modifiers != 0) ? KeyEvent.getKeyModifiersText(modifiers)
                        + "-" : "")
                        + KeyEvent.getKeyText(code);
                    Integer javaPosKeyData = (Integer)keyMap.get(keyName);
//                     System.out.println("PosiflexKB6600POSKeyboard key=" + keyName + ", code=" +
//                         javaPosKeyData) ;
                    if (javaPosKeyData != null) {
                        if (!getDataEventEnabled() || getFreezeEvents()) {
                        	
                            // Append into event queue if now the DataEventEnabled is
                            // false or FreezeEvent is true
                            eventQueue.add(javaPosKeyData);
                        } else {
                            // If AutoDisable is true, then automatically disable myself
                            if (getAutoDisable())
                                setDeviceEnabled(false);

                            if (javaPosKeyData.intValue() == POSButtonHome.getInstance().getClearCode()) {
                                clearButtonCount++;
//                                System.out.println("PosiflexKB6600POSKeyboard clearButtonCount : " + clearButtonCount);
                                if (clearButtonCount == 20) {
                                    CreamToolkit.logMessage("System is going to shutdown caused by pressing many Clear.");
                                    DacTransfer.shutdown(1);
                                }
                            } else {
                                clearButtonCount = 0;
//                                System.out.println("PosiflexKB6600POSKeyboard clearButtonCount = 0 ");
                            }

                            // Fire the DataEvent
                            setPOSKeyData(javaPosKeyData.intValue());
                            fireEvent();
                        }

                        // The following two KeyEvents will be KEY_TYPED and KEY_RELEASE
                        // for this key, we want to ignore and absort those two KeyEvents,
                        // so we set a flag here for doing this.
                        absorbRelatedKeyEvent = true;
                        return true;
                    }

                    // The similar situation, the following two KeyEvents may be KEY_TYPED
                    // and/or KEY_RELEASE for this key, we have no interest about these keys
                    // and want to bypass them and let AWT system to be able to dispatch
                    // them, so we set a flag here for doing this.
                    bypassRelatedKeyEvent = true;
                    return false;
                } catch (JposException e1) {
                    e1.printStackTrace();
                    return false;
                } finally {
                    setState(JposConst.JPOS_S_IDLE);
                }
            }
        };
    }

    // Common properties...

    public void setAutoDisable(boolean autoDisable) throws JposException {
        this.autoDisable = autoDisable;
    }

    public boolean getAutoDisable() throws JposException {
        return autoDisable;
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public int getDataCount() throws JposException {
        return eventQueue.size();
    }

    public boolean getDataEventEnabled() throws JposException {
        return dataEventEnabled;
    }

    private void fireEvent() {
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        PopupMenuPane popupMenu = app.getPopupMenuPane();
        SignOffForm signOffForm = app.getSignOffForm();
        ItemList itemList = app.getItemList();
        DacViewer dacViewer = app.getDacViewer();
        PayingPaneBanner payingPane = app.getPayingPane();
        int pageUp = POSTerminalApplication.getInstance().getPOSButtonHome().getPageUpCode();
        int pageDown = POSTerminalApplication.getInstance().getPOSButtonHome().getPageDownCode();

        if (signOffForm != null && signOffForm.isVisible()) {
            signOffForm.keyDataListener(posKeyData);
            return;
        } else if (popupMenu != null && popupMenu.isVisible()) {
            popupMenu.keyDataListener(posKeyData);
            return;
        } else if (itemList != null && itemList.isVisible()
            && (posKeyData == pageUp || posKeyData == pageDown))
            itemList.keyDataListener(posKeyData);
        else if (payingPane != null && payingPane.isVisible())
            payingPane.keyDataListener(posKeyData);
        else if (dacViewer != null && dacViewer.isVisible())
            dacViewer.keyDataListener(posKeyData);
        eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
    }

    private void fireEventsInQueue() {
        try {
            setState(JposConst.JPOS_S_BUSY);
            if (eventCallbacks != null && getClaimed() && getDeviceEnabled()
                && getDataEventEnabled() && !getFreezeEvents()) {
                while (!eventQueue.isEmpty()) {
                    // If AutoDisable is true, then automatically disable myself
                    if (getAutoDisable())
                        setDeviceEnabled(false);

                    Integer javaPosKeyData = (Integer)eventQueue.get(0);
                    eventQueue.remove(0);
                    setPOSKeyData(javaPosKeyData.intValue());
                    fireEvent();

                    // If AutoDisable is true, then automatically disable myself
                    if (getAutoDisable())
                        break;
                }
            }
        } catch (JposException e) {
            e.printStackTrace();
        } finally {
            setState(JposConst.JPOS_S_IDLE);
        }
    }

    public void setDataEventEnabled(boolean dataEventEnabled) throws JposException {
        this.dataEventEnabled = dataEventEnabled;
        fireEventsInQueue();
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        super.setFreezeEvents(freezeEvents);
        fireEventsInQueue();
    }

    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        if (deviceEnabled && !getClaimed()) {
            System.out.println("St7000POSKeyboard must claim it before enabling it");
            // we are not throwing any exception...may need to throw a JposException here
            return;
        }
        super.setDeviceEnabled(deviceEnabled);

        // Setup a key event interceptor when enabling device
        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        if (deviceEnabled)
            kfm.addKeyEventDispatcher(keyEventInterceptor);
        else
            kfm.removeKeyEventDispatcher(keyEventInterceptor);
    }

    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    // Specific properties...

    public boolean getCapKeyUp() throws JposException {
        return false;
    }

    public int getEventTypes() throws JposException {
        return POSKeyboardConst.KBD_ET_DOWN;
    }

    public void setEventTypes(int eventTypes) throws JposException {
    }

    public int getPOSKeyData() throws JposException {
        return posKeyData;
    }

    public void setPOSKeyData(int keyCode) {
        posKeyData = keyCode;
    }

    public int getPOSKeyEventType() throws JposException {
        return POSKeyboardConst.KBD_ET_DOWN;
    }

    // Methods
    public void checkHealth(int level) throws JposException {
    }

    public void clearInput() throws JposException {
        eventQueue.clear();
    }

    /**
     * Indicate the ServiceInstance that its connection has been dropped
     */
    public void deleteInstance() throws JposException {
    }

    public void open(String logicalName, final EventCallbacks eventCallbacks) throws JposException {
        super.open(logicalName, eventCallbacks);
        setState(JposConst.JPOS_S_IDLE);
    }

    public void close() throws JposException {
        setClaimed(false);
        claimedControl = null;
        setDeviceEnabled(false);
        setState(JposConst.JPOS_S_CLOSED);
    }

    public void claim(int timeout) throws JposException {
        if (getClaimed())
            return;

        synchronized (mutex) {
            if (claimedControl == null) {
                Object obj = eventCallbacks.getEventSource();
                claimedControl = (POSKeyboard)obj;
                setClaimed(true);
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                    claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                    setClaimed(true);
                    return;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        if (claimedControl == null) {
                            claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                            setClaimed(true);
                            return;
                        }
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    return;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        }
        setClaimed(true);
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
    }

    public void release() throws JposException {
        synchronized (mutex) {
            if (!getClaimed()) {
                System.out.println("Device has been released");
            }
            setClaimed(false);
            setDeviceEnabled(false);
            claimedControl = null;
            mutex.notifyAll();
        }
    }

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }
}
