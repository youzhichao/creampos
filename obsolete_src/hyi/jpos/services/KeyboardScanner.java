package hyi.jpos.services;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import jpos.JposConst;
import jpos.JposException;
import jpos.Scanner;
import jpos.ScannerConst;
import jpos.config.simple.SimpleEntry;
import jpos.events.DataEvent;
import jpos.services.EventCallbacks;
import jpos.services.ScannerService19;

/**
 * This is device service for any keyboard scanner.
 * 
 * <P>
 * <BR>
 * The following properties are supported:
 * <LI> AutoDisable
 * <LI> DataCount
 * <LI> DataEventEnabled
 * <LI> FreezeEvents
 * <P>
 * <BR>
 * The followings are the limitation of current implementation:
 * <LI> DecodeData is always true.
 * <LI> CapPowerReporting is JPOS_PR_NONE.
 * <LI> PowerNotify is JPOS_PN_DISABLED.
 * <LI> PowerState is JPOS_PS_UNKNOWN.
 * <LI> CapCompareFirmwareVersion is false
 * <LI> CapStatisticsReporting is false
 * <LI> CapUpdateFirmware is false
 * <LI> CapUpdateStatistics is false
 * <LI> Do not generate DirectIOEvent and StatusUpdateEvent.
 * <P>
 * <BR>
 * Note: Because this JavaPOS device service intercept and get keyboard event from AWT thread,
 * application cannot receive any POSKeyboard/Keylock/MSR/Scanner event from within AWT event
 * processing. Our JavaPOS user should not expect to get any JavaPOS event within their AWT event
 * processing method.
 * 
 * <p/>Sample JCL config:
 * 
 * <pre>
 *   &lt;JposEntry logicalName=&quot;KeyboardScanner&quot;&gt;
 *       &lt;creation factoryClass=&quot;hyi.jpos.loader.ServiceInstanceFactory&quot;
 *                 serviceClass=&quot;hyi.jpos.services.KeyboardScanner&quot;/&gt;
 *       &lt;vendor name=&quot;Any vendor&quot; url=&quot;http://www.hyi.com.tw&quot;/&gt;
 *       &lt;jpos category=&quot;Scanner&quot; version=&quot;1.9&quot;/&gt;
 *       &lt;product description=&quot;Any POS Scanner&quot; name=&quot;Any POS Scanner&quot; url=&quot;http://www.hyi.com.tw&quot;/&gt;
 * 
 *       &lt;!-- Scanner Data Header --&gt;
 *       &lt;prop name=&quot;PrefixCharacter&quot; type=&quot;Character&quot; value=&quot;\&quot;/&gt;
 * 
 *       &lt;!-- Code Identifiers --&gt;
 *       &lt;prop name=&quot;UPCA&quot; type=&quot;String&quot; value=&quot;A&quot;/&gt;
 *       &lt;prop name=&quot;UPCE&quot; type=&quot;String&quot; value=&quot;E&quot;/&gt;
 *       &lt;prop name=&quot;JAN8&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;EAN8&quot; type=&quot;String&quot; value=&quot;FF&quot;/&gt;
 *       &lt;prop name=&quot;JAN13&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;EAN13&quot; type=&quot;String&quot; value=&quot;F&quot;/&gt;
 *       &lt;prop name=&quot;TF&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;ITF&quot; type=&quot;String&quot; value=&quot;I&quot;/&gt;
 *       &lt;prop name=&quot;Codabar&quot; type=&quot;String&quot; value=&quot;N&quot;/&gt;
 *       &lt;prop name=&quot;Code39&quot; type=&quot;String&quot; value=&quot;M&quot;/&gt;
 *       &lt;prop name=&quot;Code93&quot; type=&quot;String&quot; value=&quot;L&quot;/&gt;
 *       &lt;prop name=&quot;Code128&quot; type=&quot;String&quot; value=&quot;K&quot;/&gt;
 *       &lt;prop name=&quot;UPCA_S&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCE_S&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCD1&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCD2&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCD3&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCD4&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCD5&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;EAN8_S&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;EAN13_S&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;EAN128&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;OCRA&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;OCRB&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;RSS14&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;RSS_EXPANDED&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;CCA&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;CCB&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;CCC&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;PDF417&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;MAXICODE&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *   &lt;/JposEntry&gt;
 *  
 * </pre>
 * 
 * @author Bruce You @ Hongyuan Software
 * @since 2007-2-14 Valentine's Day
 */
public class KeyboardScanner extends AbstractDeviceService19 implements ScannerService19 {

    private int prefixCharacter;
    private Object[][] identifierCodes = { { new Integer(ScannerConst.SCAN_SDT_UPCA), "UPCA" },
        { new Integer(ScannerConst.SCAN_SDT_UPCE), "UPCE" },
        { new Integer(ScannerConst.SCAN_SDT_JAN8), "JAN8" },
        { new Integer(ScannerConst.SCAN_SDT_EAN8), "EAN8" },
        { new Integer(ScannerConst.SCAN_SDT_JAN13), "JAN13" },
        { new Integer(ScannerConst.SCAN_SDT_EAN13), "EAN13" },
        { new Integer(ScannerConst.SCAN_SDT_TF), "TF" },
        { new Integer(ScannerConst.SCAN_SDT_ITF), "ITF" },
        { new Integer(ScannerConst.SCAN_SDT_Codabar), "Codabar" },
        { new Integer(ScannerConst.SCAN_SDT_Code39), "Code39" },
        { new Integer(ScannerConst.SCAN_SDT_Code93), "Code93" },
        { new Integer(ScannerConst.SCAN_SDT_Code128), "Code128" },
        { new Integer(ScannerConst.SCAN_SDT_UPCA_S), "UPCA_S" },
        { new Integer(ScannerConst.SCAN_SDT_UPCE_S), "UPCE_S" },
        { new Integer(ScannerConst.SCAN_SDT_UPCD1), "UPCD1" },
        { new Integer(ScannerConst.SCAN_SDT_UPCD2), "UPCD2" },
        { new Integer(ScannerConst.SCAN_SDT_UPCD3), "UPCD3" },
        { new Integer(ScannerConst.SCAN_SDT_UPCD4), "UPCD4" },
        { new Integer(ScannerConst.SCAN_SDT_UPCD5), "UPCD5" },
        { new Integer(ScannerConst.SCAN_SDT_EAN8_S), "EAN8_S" },
        { new Integer(ScannerConst.SCAN_SDT_EAN13_S), "EAN13_S" },
        { new Integer(ScannerConst.SCAN_SDT_EAN128), "EAN128" },
        { new Integer(ScannerConst.SCAN_SDT_OCRA), "OCRA" },
        { new Integer(ScannerConst.SCAN_SDT_OCRB), "OCRB" },
        { new Integer(ScannerConst.SCAN_SDT_RSS14), "RSS14" },
        { new Integer(ScannerConst.SCAN_SDT_RSS_EXPANDED), "RSS_EXPANDED" },
        { new Integer(ScannerConst.SCAN_SDT_CCA), "CCA" },
        { new Integer(ScannerConst.SCAN_SDT_CCB), "CCB" },
        { new Integer(ScannerConst.SCAN_SDT_CCC), "CCC" },
        { new Integer(ScannerConst.SCAN_SDT_PDF417), "PDF417" },
        { new Integer(ScannerConst.SCAN_SDT_MAXICODE), "MAXICODE" } };
    private Map identifierCodeMap = new TreeMap();

    private static Scanner claimedControl;
    private static Object mutex = new Object();
    private KeyEventDispatcher keyEventInterceptor;
    private byte[] scanData = new byte[0];
    private byte[] scanDataLabel = new byte[0];
    private int scanDataType = ScannerConst.SCAN_SDT_UNKNOWN;
    private List eventQueue = new ArrayList();
    private boolean autoDisable = false;
    private boolean dataEventEnabled = false;

    public KeyboardScanner() {
        setDeviceServiceDescription("Keyboard scanner JavaPOS Device Service from HYI");
        setPhysicalDeviceDescription("Keyboard scanner");
        setPhysicalDeviceName("Keyboard scanner");
        createKeyEventInterceptor();
        setState(JposConst.JPOS_S_CLOSED);
    }

    public KeyboardScanner(SimpleEntry entry) {
        this();
        prefixCharacter = ((Character)entry.getPropertyValue("PrefixCharacter")).charValue();

        for (int i = 0; i < identifierCodes.length; i++) {
            String identifierCode = (String)entry.getPropertyValue((String)identifierCodes[i][1]);
            if (identifierCode != null && identifierCode.length() > 0
                && !identifierCode.equals("?"))
                identifierCodeMap.put(identifierCode, identifierCodes[i][0]);
        }
    }

    /**
     * Create a keyboard event interceptor for accepting and firing MSR event. The interceptor is an
     * AWT's KeyEventDispatcher, and then it'll be added into KeyboardFocusManager when enabling
     * this device.
     */
    private void createKeyEventInterceptor() {

        keyEventInterceptor = new KeyEventDispatcher() {
            boolean withinControlSeq;
            StringBuffer scannerData = new StringBuffer();
            int waitLastEvent;

            public boolean dispatchKeyEvent(KeyEvent e) {
                try {
                    setState(JposConst.JPOS_S_BUSY);

                    int code = e.getKeyCode();

                    if (e.getID() == KeyEvent.KEY_PRESSED && code == prefixCharacter) {
                        scannerData.setLength(0);
                        withinControlSeq = true;
                        return true;
                    }

                    if (e.getID() != KeyEvent.KEY_TYPED) {
                        if (waitLastEvent > 0) {
                            waitLastEvent--;
                            return true;
                        }

                        // While in-between control sequence of MSR, return true
                        // to absort any key event for preventing from sending
                        // them to application
                        return withinControlSeq;
                    }

                    char keyChar = e.getKeyChar();

                    if (withinControlSeq) {
                        if (keyChar == 10) { // End Symbol
                            withinControlSeq = false;

                            // The last char is 10, but AWT system will still generate
                            // a KEY_RELEASE event. We also want to ignore and absort this
                            // KeyEvents, so we set a count here for doing this.
                            waitLastEvent = 1;

                            if (!getDataEventEnabled() || getFreezeEvents()) {
                                // Append into event queue if now the DataEventEnabled is
                                // false or FreezeEvent is true
                                eventQueue.add(scannerData.toString());
                            } else {
                                // If AutoDisable is true, then automatically disable myself
                                if (getAutoDisable())
                                    setDeviceEnabled(false);

                                // Fire the DataEvent
                                fireDataEvent(scannerData.toString());
                            }
                        } else {
                            scannerData.append(keyChar);
                        }
                        return true;
                    }
                    return false;
                } catch (JposException e2) {
                    e2.printStackTrace();
                    return false;
                } finally {
                    setState(JposConst.JPOS_S_IDLE);
                }
            }
        };
    }

    // AutoDisable boolean R/W open
    public boolean getAutoDisable() throws JposException {
        return autoDisable;
    }

    public void setAutoDisable(boolean autoDisable) throws JposException {
        this.autoDisable = autoDisable;
    }

    public int getDataCount() throws JposException {
        return eventQueue.size();
    }

    public boolean getDataEventEnabled() throws JposException {
        return dataEventEnabled;
    }

    private void fireEventsInQueue() {
        try {
            setState(JposConst.JPOS_S_BUSY);
            if (eventCallbacks != null && getClaimed() && getDeviceEnabled()
                && getDataEventEnabled() && !getFreezeEvents()) {
                while (!eventQueue.isEmpty()) {
                    // If AutoDisable is true, then automatically disable myself
                    if (getAutoDisable())
                        setDeviceEnabled(false);

                    fireDataEvent((String)eventQueue.get(0));
                    eventQueue.remove(0);

                    if (getAutoDisable())
                        break;
                }
            }
        } catch (JposException e) {
            e.printStackTrace();
        } finally {
            setState(JposConst.JPOS_S_IDLE);
        }
    }

    public void setDataEventEnabled(boolean dataEventEnabled) throws JposException {
        this.dataEventEnabled = dataEventEnabled;
        fireEventsInQueue();
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        super.setFreezeEvents(freezeEvents);
        fireEventsInQueue();
    }

    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        if (deviceEnabled && !getClaimed()) {
            System.out.println("Keyboard scanner must claim it before enabling it.");
            // we are not throwing any exception...may need to throw a
            // JposException here
            return;
        }
        super.setDeviceEnabled(deviceEnabled);

        // Setup a key event interceptor when enabling device
        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        if (deviceEnabled)
            kfm.addKeyEventDispatcher(keyEventInterceptor);
        else
            kfm.removeKeyEventDispatcher(keyEventInterceptor);
    }

    public byte[] getScanData() throws JposException {
        return scanData;
    }

    public byte[] getScanDataLabel() throws JposException {
        return scanDataLabel;
    }

    public int getScanDataType() throws JposException {
        return scanDataType;
    }

    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "No service available now!");
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    // end of properties definition

    // Common method -- Not supported
    public void checkHealth(int level) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    // Common methods -- Supported
    public void clearInput() throws JposException {
        eventQueue.clear();
    }

    public void open(String logicalName, EventCallbacks eventCallbacks) throws JposException {
        super.open(logicalName, eventCallbacks);
        setState(JposConst.JPOS_S_IDLE);
        setDecodeData(true);
        setClaimed(false);
    }

    public void close() throws JposException {
        setClaimed(false);
        claimedControl = null;
        setDeviceEnabled(false);
        setState(JposConst.JPOS_S_CLOSED);
    }

    public void release() throws JposException {
        synchronized (mutex) {
            if (!claimed) {
                System.out.println("device has been release");
            }
            setClaimed(false);
            setDeviceEnabled(false);
            claimedControl = null;
            mutex.notifyAll();
        }
    }

    public void claim(int timeout) throws JposException {
        if (claimed) {
            System.out.println("KeyboardScanner has been claimed");
            return;
        }
        synchronized (mutex) {
            if (claimedControl == null) {
                claimedControl = (Scanner)eventCallbacks.getEventSource();
                claimed = true;
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                    claimedControl = (Scanner)eventCallbacks.getEventSource();
                    claimed = true;
                    return;
                } catch (InterruptedException ex1) {
                    System.out.println(ex1.toString());
                    System.out.println("Interrupted exception at " + this);
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        if (claimedControl == null) {
                            claimedControl = (Scanner)eventCallbacks.getEventSource();
                            claimed = true;
                            return;
                        }
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    System.out.println("KeyboardScanner is busy");
                    return;
                } catch (InterruptedException ex1) {
                    System.out.println(ex1.toString());
                    System.out.println("Interrupted exception at " + this);
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        }
    }

    public void deleteInstance() throws JposException {
    }

    private void fireDataEvent(String scannerData) {
        scanData = scanDataLabel = null;
        scannerData = scannerData.substring(1); // remove prefix
        scanDataType = ScannerConst.SCAN_SDT_UNKNOWN;
        Object[] ids = identifierCodeMap.keySet().toArray();
        for (int i = ids.length - 1; i >= 0; i--) {
            String identifierCode = (String)ids[i];
            if (scannerData.startsWith(identifierCode)) {
                scanDataType = ((Integer)identifierCodeMap.get(identifierCode)).intValue();
                scanData = scanDataLabel = scannerData.substring(identifierCode.length())
                    .getBytes();
                break;
            }
        }
        if (scanData == null)
            scanData = scanDataLabel = scannerData.getBytes();
        eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
    }

    public void compareFirmwareVersion(String arg0, int[] arg1) throws JposException {
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public void updateFirmware(String arg0) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String arg0) throws JposException {
    }

    public void retrieveStatistics(String[] arg0) throws JposException {
    }

    public void updateStatistics(String arg0) throws JposException {
    }

    public boolean getDecodeData() throws JposException {
        return true;
    }

    public void setDecodeData(boolean arg0) throws JposException {
    }
}