package hyi.jpos.services;

import hyi.cream.util.CreamToolkit;

import java.io.UnsupportedEncodingException;

import jpos.JposConst;
import jpos.JposException;
import jpos.LineDisplay;
import jpos.LineDisplayConst;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.services.EventCallbacks;
import jpos.services.LineDisplayService19;

public class Tec6400Linedisplay extends AbstractDeviceService19 implements LineDisplayService19 {

    static private LineDisplay claimedControl;
    static private Object mutex = new Object();
    static private char[] ch = new char[40];

    private boolean capDescriptors   = false;
    private int characterSet         = 0;
    private String characterSetList  = "";
    private boolean claimed          = false;
    private int columns              = 0;
    private int currentWindow        = 0;
    private int cursorColumn         = 0;
    private int cursorRow            = 0;
    private boolean cursorUpdate     = false;
    private int deviceBrightness     = 0;
    private int deviceColumns        = 0;
    private int deviceDescriptors    = 0;
    private boolean deviceEnabled     = false;
    private int deviceRows           = 0;
    private int deviceWindows        = 0;
    private int interCharacterWait   = 0;
    private int marqueeFormat        = 0;
    private int marqueeRepeatWait    = 0;
    private int marqueeType          = 0;
    private int marqueeUnitWait      = 0;
    private int rows                 = 0;
    private boolean opened              = false;

    protected String checkHealthText             = "";
    protected String deviceServiceDescription    = "JavaPOS Service from HYI Corporation";
    protected int deviceServiceVersion           = 1005000;
    protected boolean freezeEvents               = false;
    protected String physicalDeviceDescription   = "";
    protected String physicalDeviceName          = ""; 
    protected int state                          = JposConst.JPOS_S_CLOSED;
    protected int powerNotify                    = JposConst.JPOS_PN_DISABLED;
    protected int powerState                     = JposConst.JPOS_PS_UNKNOWN;

    protected EventCallbacks eventCallbacks      = null;
    protected JposEntry entry                    = null;

    //static Enumeration portList;
    //static CommPortIdentifier portId;
    //static SerialPort serialPort;
    //static OutputStream outputStream;

    /**
     * Constructor
     */
    public Tec6400Linedisplay(SimpleEntry entry) {
        this.entry = entry;
    }
    
    // Capabilities
    public int getCapBlink() throws JposException {
        return LineDisplayConst.DISP_CB_NOBLINK;
    }

    public boolean getCapBrightness() throws JposException {
        return false;
    }

    public int getCapCharacterSet() throws JposException {
        return LineDisplayConst.DISP_CCS_ASCII;
    }

    public boolean getCapDescriptors() throws JposException {
        return capDescriptors;
    }

    public boolean getCapHMarquee() throws JposException {
        return true;
    }

    public boolean getCapICharWait() throws JposException {
        return false;
    }

    public boolean getCapVMarquee() throws JposException {
        return false;
    }

    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties
    public String  getCheckHealthText() throws JposException {
        return checkHealthText;
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    public void setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        if (claimed) {
            this.deviceEnabled = deviceEnabled;
        } else {
            CreamToolkit.logMessage("lineDisplay error: must claim it first");
        }
    }

    public String getDeviceServiceDescription() throws JposException {
        return deviceServiceDescription;
    }

    public int getDeviceServiceVersion() throws JposException {
        return deviceServiceVersion;
    }

    public boolean getFreezeEvents() throws JposException {
        return freezeEvents;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        this.freezeEvents = freezeEvents;
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return physicalDeviceDescription;
    }

    public String getPhysicalDeviceName() throws JposException {
        return physicalDeviceName;
    }

    public int getState() throws JposException {
        return state;
    }

    public int getCharacterSet() throws JposException {
        return characterSet;
    }

    public void setCharacterSet(int characterSet) throws JposException {
        this.characterSet = characterSet;
    }

    public String getCharacterSetList() throws JposException {
        return characterSetList;
    }

    public int getColumns() throws JposException {
        return columns;
    }

    public int getCurrentWindow() throws JposException {
        return currentWindow;
    }

    public void setCurrentWindow(int currentWindow) throws JposException {
        this.currentWindow = currentWindow;
    }

    public int getCursorColumn() throws JposException {
        return cursorColumn;
    }

    public void setCursorColumn(int cursorColumn) throws JposException {
        this.cursorColumn = cursorColumn;
    }

    public int getCursorRow() throws JposException {
        return cursorRow;
    }

    public void setCursorRow(int cursorRow) throws JposException {
        this.cursorRow = cursorRow;
    }

    public boolean getCursorUpdate() throws JposException {
        return cursorUpdate;
    }

    public void setCursorUpdate(boolean cursorUpdate) throws JposException {
        this.cursorUpdate = cursorUpdate;
    }

    public int getDeviceBrightness() throws JposException {
        return deviceBrightness;
    }

    public void setDeviceBrightness(int deviceBrightness)
                       throws JposException {
        this.deviceBrightness = deviceBrightness;
    }

    public int getDeviceColumns() throws JposException {
        return deviceColumns;
    }

    public int getDeviceDescriptors() throws JposException {
        return deviceDescriptors;
    }

    public int getDeviceRows() throws JposException {
        return deviceRows;
    }

    public int getDeviceWindows() throws JposException {
        return deviceWindows;
    }

    public int getInterCharacterWait() throws JposException {
        return interCharacterWait;
    }

    public void setInterCharacterWait(int interCharacterWait)
                       throws JposException {
        this.interCharacterWait = interCharacterWait;
    }

    public int getMarqueeFormat() throws JposException {
        return marqueeFormat;
    }

    public void setMarqueeFormat(int marqueeFormat) throws JposException {
        this.marqueeFormat = marqueeFormat;
    }

    public int getMarqueeRepeatWait() throws JposException {
        return marqueeRepeatWait;
    }

    public void setMarqueeRepeatWait(int marqueeRepeatWait)
                       throws JposException {
        this.marqueeRepeatWait = marqueeRepeatWait;
    }

    public int getMarqueeType() throws JposException {
        return marqueeType;
    }

    public void setMarqueeType(int marqueeType) throws JposException {
        this.marqueeType = marqueeType;
    }

    public int getMarqueeUnitWait() throws JposException {
        return marqueeUnitWait;
    }

    public void setMarqueeUnitWait(int marqueeUnitWait)
                       throws JposException {
        this.marqueeUnitWait = marqueeUnitWait;
    }

    public int getRows() throws JposException {
        return rows;
    }

    public int getPowerNotify() throws JposException {
        return powerNotify;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        this.powerNotify = powerNotify;
    }

    public int getPowerState() throws JposException {
        return powerState;
    }

    public void connect() {
        /*portList = CommPortIdentifier.getPortIdentifiers();
        while (portList.hasMoreElements()) {
            portId = (CommPortIdentifier) portList.nextElement();
            //System.out.println("portlist = " + portId + " at " + this);
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL
                && portId.getName().equals((String)entry.getPropertyValue("DevicePortName"))) {
                //System.out.println("find port at " + (String)entry.getPropertyValue("DevicePortName"));
                if (portId.isCurrentlyOwned()) {
                    CreamToolkit.logMessage("lineDisplay error : The device is being use!");
                    return;
                }
                try {
                    serialPort = (SerialPort)portId.open("CD7220", 10000);
                } catch (PortInUseException e1) {
                    CreamToolkit.logMessage(e1.toString());
                    CreamToolkit.logMessage("Port in use: " + portId + ", at " + this);
                    break;
                }
                try {
                    outputStream = serialPort.getOutputStream();
                } catch (IOException e2) {
                    CreamToolkit.logMessage(e2.toString());
                    CreamToolkit.logMessage("IO exception at " + this);
                    break;
                }
            }
        }
        try {
            serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,
                                                 SerialPort.STOPBITS_1,
                                                 SerialPort.PARITY_NONE);
        } catch (UnsupportedCommOperationException e3) {
            CreamToolkit.logMessage(e3.toString());
            CreamToolkit.logMessage("Unsupported comm operation at " + this);
            return;
        }

        try {
            outputStream.write(27);     //initialize display
            outputStream.write(64);
            outputStream.write(27);     //set overwrite mode
            outputStream.write(17);
        } catch (IOException e4) {
            CreamToolkit.logMessage(e4.toString());
            CreamToolkit.logMessage("IO exception at " + this);
        }*/

        LineDisplayLocal.ini();

        claimed = true;
        deviceEnabled = false;
        return;
    }

    // Methods supported by all device services.
    public void claim(int timeout) throws JposException {
        if (claimed) {
            //CreamToolkit.logMessage("lineDisplay error : device has been claimed");
            return;
        }
        //synchronized (mutex) {
            if (claimedControl == null) {
                claimedControl = (LineDisplay)eventCallbacks.getEventSource();
                connect();
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                    claimedControl = (LineDisplay)eventCallbacks.getEventSource();
                    connect();
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        // wait() returns when time is up or being notify().

                        /////////// plus checking claimedControl with "this"
                        if (claimedControl == null) {
                            claimedControl = (LineDisplay)eventCallbacks.getEventSource();
                            connect();
                            return;
                        }

                        // if wait not enough time, it should keep wait() again....
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    CreamToolkit.logMessage("lineDisplay error : device is busy");
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        //}
    }

    public void close() throws JposException {
        claimed = false;
        deviceEnabled = false;
        opened = false;
    }

    public void checkHealth(int level) throws JposException {
    }
    
    public void deleteInstance() throws JposException {
    }

    public void directIO(int command, int[] data, Object object)
                       throws JposException {
    }

    public void open(String logicalName, EventCallbacks cb)
                        throws JposException {
        eventCallbacks = cb;
        this.setCursorColumn(0);
        this.setCursorRow(0);
        String str = "                                        ";
        ch = str.toCharArray();
        opened = true;
        claimed = false;
        deviceEnabled = false;
    }

    synchronized public void release() throws JposException {
        synchronized (mutex) {
            if (!claimed) {
                CreamToolkit.logMessage("lineDisplay error : device has been release");
            }
            /*try {
                outputStream.write(12);    //clear display screen, and clear string mode
                outputStream.write(27);    //set cursor OFF
                outputStream.write(95);
                outputStream.write(0);
            } catch (IOException e4) {
                CreamToolkit.logMessage(e4.toString());
                CreamToolkit.logMessage("IO exception at " + this);
            }
            serialPort.close();
            outputStream = null;
            portList = null;
            portId = null;
            serialPort = null;*/
            claimed = false;
            deviceEnabled = false;
            claimedControl = null;
            mutex.notifyAll();
        }
    }

    public void clearDescriptors() throws JposException {
        if (!capDescriptors) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL);
        } else {
            deviceServiceDescription = "";
            physicalDeviceDescription = "";
        }
    }
    
    public void clearText() throws JposException {
        if (deviceEnabled) {
            /*try {
                outputStream.write(12);   //clear display screen, and clear string mode
            } catch (IOException e4) {
                CreamToolkit.logMessage(e4.toString());
                CreamToolkit.logMessage("IO exception at " + this);
            }*/

            byte[] b1;
            try {
                b1 = "                    ".getBytes(CreamToolkit.getEncoding());
                byte[] b2 = new byte[b1.length + 1];
                for (int i = 0; i < b1.length; i++) {
                    b2[i] = b1[i];
                }
                b2[b1.length] = 0;
                LineDisplayLocal.ini();
                LineDisplayLocal.show(b2, 1);
                LineDisplayLocal.show(b2, 2);
    
                setCursorColumn(0);
                setCursorRow(0);
                String str = "                    ";
                ch = str.toCharArray();
            } catch (UnsupportedEncodingException e) {
            }
        } else {
            CreamToolkit.logMessage("lineDisplay error : Not DeviceEnabled");
        }
    }

    public void createWindow(int viewportRow, int viewportColumn,
                       int viewportHeight, int viewportWidth, int windowHeight,
                       int windowWidth) throws JposException {
    }

    public void destroyWindow() throws JposException {
    }

    public void displayText(String data, int attribute)
                       throws JposException {
        if (!claimed) {
            CreamToolkit.logMessage("lineDisplay error : Please first claim it!");
            return;
        }
        if (!deviceEnabled) {
            CreamToolkit.logMessage("lineDisplay error : Not setDeviceEnable");
            return;
        }

        /*try {
            OutputStreamWriter dataOutput = new OutputStreamWriter(outputStream, "GBK");
            dataOutput.write(data);
            dataOutput.flush();
        } catch (IOException e4) {
            CreamToolkit.logMessage(e4.toString());
            CreamToolkit.logMessage("IO exception at " + this);
        }*/

        while (data.length() > 20) {
            data = data.substring(0, data.length() - 2);
        }
        while (data.length() < 20) {
            data = data + " ";
        }
        //System.out.println("Tec6400Linedisplay displayText() = " + data);
        try {
            byte[] b1 = data.getBytes(CreamToolkit.getEncoding());
            byte[] b2 = new byte[b1.length + 1];
            for (int i = 0; i < b1.length; i++) {
                b2[i] = b1[i];
            }
            b2[b1.length] = 0;
            LineDisplayLocal.ini();
            LineDisplayLocal.show(b2, attribute);
        } catch (UnsupportedEncodingException e) {
        }

        claimed = true;
        deviceEnabled = true;
    }

    public void displayTextAt(int y, int x, String data,
                       int attribute) throws JposException {
        if (!claimed) {
            CreamToolkit.logMessage("lineDisplay error : Please first claim it!");
            return;
        }
        if (!deviceEnabled) {
            CreamToolkit.logMessage("lineDisplay error : Not setDeviceEnable");
            return;
        }

        /*try {
            outputStream.write(27);    //set cursor on x,y
            outputStream.write(108);
            outputStream.write(x + 1);
            outputStream.write(y + 1);

            OutputStreamWriter dataOutput = new OutputStreamWriter(outputStream, "GBK");
            dataOutput.write(data);
            dataOutput.flush();
        } catch (IOException e4) {
            CreamToolkit.logMessage(e4.toString());
            CreamToolkit.logMessage("IO exception at " + this);
        }*/
        setCursorColumn(x);
        setCursorRow(y + 1);
        claimed = true;
        deviceEnabled = true;
    }

    public void refreshWindow(int window) throws JposException {
    }

    public void scrollText(int direction, int units) throws JposException {
        this.direction = direction;
        this.units = units;
        String s = "                                        ";
        char[] c = s.toCharArray();
        if (direction == DISP_ST_LEFT) {
            for (int i = 0; i < (20 - units); i++) {
                c[i] = ch[units + i];
                c[20 + i] = ch[20 + units + i];
            }
        } else if (direction == DISP_ST_RIGHT) {
            for (int i = 0; i < (20 - units); i++) {
                c[units + i] = ch[i];
                c[20 + units + i] = ch[20 + i];
            }
        }
        /*try {
            outputStream.write(12);              //clear screen
            for (int i = 0; i < 40; i++) {
                outputStream.write(c[i]);       //display string
            }
        } catch (IOException e4) {
            CreamToolkit.logMessage(e4.toString());
            CreamToolkit.logMessage("IO exception at " + this);
        }*/
    }

    public void setDescriptor(int descriptor, int attribute)
                       throws JposException {
    }

    public static int DISP_ST_UP = 0;
    public static int DISP_ST_DOWN = 1;
    public static int DISP_ST_LEFT = 2;
    public static int DISP_ST_RIGHT = 3;
    private int direction = DISP_ST_LEFT;
    private int units = 0;

//    public static void main(String[] args) {
//        LineDisplay ld = new LineDisplay();
//        try {
//        ld.open("CD7220");
//        ld.claim(0);
//        ld.setDeviceEnabled(true);
//        ld.displayText("hahaha", 0);
//        Thread.sleep(5000);
//        ld.clearText();
//        ld.displayText("closed!!!!!!!", 0);
//        Thread.sleep(5000);
//        ld.setDeviceEnabled(false);
//        ld.release();
//        ld.close();
//        System.out.println("close");
//        } catch (JposException e) {
//            System.out.println(e);
//        } catch (InterruptedException e) {
//            System.out.println(e);
//        }
//    }

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }

    public void displayBitmap(String fileName, int width, int alignmentX, int alignmentY) throws JposException {
    }

    public boolean getCapBitmap() throws JposException {
        return false;
    }

    public boolean getCapMapCharacterSet() throws JposException {
        return false;
    }

    public boolean getCapScreenMode() throws JposException {
        return false;
    }

    public boolean getMapCharacterSet() throws JposException {
        return false;
    }

    public int getMaximumX() throws JposException {
        return 0;
    }

    public int getMaximumY() throws JposException {
        return 0;
    }

    public int getScreenMode() throws JposException {
        return 0;
    }

    public String getScreenModeList() throws JposException {
        return null;
    }

    public void setBitmap(int bitmapNumber, String fileName, int width, int alignmentX, int alignmentY) throws JposException {
    }

    public void setMapCharacterSet(boolean mapCharacterSet) throws JposException {
    }

    public void setScreenMode(int screenMode) throws JposException {
    }

    public void defineGlyph(int glyphCode, byte[] glyph) throws JposException {
    }

    public int getBlinkRate() throws JposException {
        return 0;
    }

    public boolean getCapBlinkRate() throws JposException {
        return false;
    }

    public int getCapCursorType() throws JposException {
        return 0;
    }

    public boolean getCapCustomGlyph() throws JposException {
        return false;
    }

    public int getCapReadBack() throws JposException {
        return 0;
    }

    public int getCapReverse() throws JposException {
        return 0;
    }

    public int getCursorType() throws JposException {
        return 0;
    }

    public String getCustomGlyphList() throws JposException {
        return null;
    }

    public int getGlyphHeight() throws JposException {
        return 0;
    }

    public int getGlyphWidth() throws JposException {
        return 0;
    }

    public void readCharacterAtCursor(int[] aChar) throws JposException {
    }

    public void setBlinkRate(int blinkRate) throws JposException {
    }

    public void setCursorType(int cursorType) throws JposException {
    }
}