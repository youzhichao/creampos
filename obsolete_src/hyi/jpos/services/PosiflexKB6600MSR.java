package hyi.jpos.services;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import jpos.*;
import jpos.services.*;
import jpos.events.*;
import jpos.config.simple.*;

/**
 * This is device service for Posiflex KB6600 MSR.
 * 
 * <P>
 * <BR>
 * The following properties are supported:
 * <LI> AutoDisable
 * <LI> DataCount
 * <LI> DataEventEnabled
 * <LI> FreezeEvents
 * <P>
 * <BR>
 * The followings are the limitation of current implementation:
 * <LI> TracksToRead is always MSRConst.MSR_TR_1.
 * <LI> DecodeData is always true.
 * <LI> CapPowerReporting is JPOS_PR_NONE.
 * <LI> PowerNotify is JPOS_PN_DISABLED.
 * <LI> PowerState is JPOS_PS_UNKNOWN.
 * <LI> CapCompareFirmwareVersion is false
 * <LI> CapStatisticsReporting is false
 * <LI> CapUpdateFirmware is false
 * <LI> CapUpdateStatistics is false
 * <LI> Do not generate DirectIOEvent and StatusUpdateEvent.
 * <P>
 * <BR>
 * Note: Because this JavaPOS device service intercept and get keyboard event from AWT thread,
 * application cannot receive any POSKeyboard/Keylock/MSR event from within AWT event processing.
 * Our JavaPOS user should not expect to get any JavaPOS event within their AWT event processing
 * method.
 * 
 * <p/>Sample JCL config:
 * 
 * <pre>
 *     &lt;JposEntry logicalName=&quot;PosiflexKB6600MSR&quot;&gt;
 *        &lt;creation factoryClass=&quot;hyi.jpos.loader.ServiceInstanceFactory&quot;
 *                  serviceClass=&quot;hyi.jpos.services.PosiflexKB6600MSR&quot;/&gt;
 *        &lt;vendor name=&quot;Posiflex&quot; url=&quot;http://www.hyi.com.tw&quot;/&gt;
 *        &lt;jpos category=&quot;MSR&quot; version=&quot;1.9&quot;/&gt;
 *        &lt;product description=&quot;Posiflex KB-6600 MSR&quot; name=&quot;Posiflex KB-6600 MSR&quot;
 *                 url=&quot;http://www.hyi.com.tw&quot;/&gt;
 *     /JposEntry&gt;
 * </pre>
 * 
 * @author Bruce You @ Hongyuan Software
 * @since 2007-2-13
 */
public class PosiflexKB6600MSR extends AbstractDeviceService19 implements MSRService19 {

    private static final char MSR_START_CHAR = '%';
    private static final char MSR_TRACK1_START_SENTINEL = '%';
    private static final char MSR_TRACK2_START_SENTINEL = ';';
    private static final char MSR_TRACK3_START_SENTINEL = ';';
    private static final char MSR_END_SENTINEL = '?';
    // private static final int FORMAT_JIS_II = 1;
    private static final int FORMAT_ISO = 2;
    // private static final boolean DEBUG = false;

    private static MSR claimedControl;
    private static Object mutex = new Object();
    private boolean autoDisable;
    private boolean dataEventEnabled;
    private KeyEventDispatcher keyEventInterceptor;
    private int tracksToRead = MSRConst.MSR_TR_1;
    private List eventQueue = new ArrayList();
    private byte[] track1Data = new byte[0];
    private String track1DataString = "";
    private byte[] track1DiscretionaryData = new byte[0];
    private byte[] track2Data = new byte[0];
    private String track2DataString = "";
    private byte[] track2DiscretionaryData = new byte[0];
    private int dataFormat = FORMAT_ISO;
    private byte[] track3Data = new byte[0];
    private String track3DataString = "";
    private String accountNumber = "";
    private boolean parseDecodeData;
    private boolean transmitSentinels;
    private String surname = "";
    private String suffix = "";
    private String serviceCode = "";
    private String title = "";
    private String middleInitial = "";
    private String firstName = "";
    private String expirationDate = "";
    private int errorReportingType = MSRConst.MSR_ERT_CARD;

    /**
     * Default constructor.
     */
    public PosiflexKB6600MSR() {
        setDeviceServiceDescription("Posiflex KB6600 MSR JavaPOS Device Service from HYI");
        setPhysicalDeviceDescription("Posiflex KB6600 MSR");
        setPhysicalDeviceName("Posiflex KB6600 MSR");
        createKeyEventInterceptor();
        setState(JposConst.JPOS_S_CLOSED);
    }

    /**
     * Constructor with a SimpleEntry.
     * 
     * @param entry
     *            The registry entry for Keylock.
     */
    public PosiflexKB6600MSR(SimpleEntry entry) {
        this();
    }

    private String removeEndSentinelAndLRC(String data) {
        int x = data.lastIndexOf('?');
        if (x != -1)
            data = data.substring(0, x);
        return data;
    }

    private boolean splitIntoTracks(String data) {
        // The first char of data will be MSR_START_CHAR, and the last char will
        // be '\n'

        try {
            String[] tracks = data.substring(1).split("\\?");
            track1DataString = tracks[0];
            if (tracks.length >= 2)
                track2DataString = tracks[1].substring(1);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Split name by this format:<BR>
     * [Surname]/[First Name] [Middle Initial].[Title]
     * 
     * @param fullName
     *            Name field in track data.
     */
    private void splitName(String fullName) {
        try {
            int idx;

            idx = fullName.indexOf('/');
            if (idx == -1) {
                surname = fullName;
                return;
            }
            surname = fullName.substring(0, idx);
            idx++;

            int idx2 = fullName.lastIndexOf('.');
            if (idx2 != -1) {
                title = fullName.substring(idx2 + 1);
                fullName = fullName.substring(0, idx2 - 1);
            }

            idx2 = fullName.indexOf(' ', idx);
            if (idx2 == -1) {
                firstName = fullName.substring(idx);
                return;
            }
            firstName = fullName.substring(idx, idx2);
            idx = idx2 + 1;

            middleInitial = fullName.substring(idx2);
        } catch (IndexOutOfBoundsException e) {
        }
    }

    private void clearEventData() {
        accountNumber = surname = firstName = middleInitial = title = suffix = expirationDate = "";
        serviceCode = track1DataString = track2DataString = track3DataString = "";
        track1Data = track1DiscretionaryData = track2Data = track2DiscretionaryData = track3Data = new byte[0];
    }

    private void parseAndFireMSRDataEvent(String msrData) {
        clearEventData();
        boolean result = splitIntoTracks(msrData);
        if (!result)
            return;

        if (track1DataString.length() != 0 && (getTracksToRead() & MSRConst.MSR_TR_1) != 0) {
            track1DataString = removeEndSentinelAndLRC(track1DataString);
            // We'll not parse for JIS-II format
            if (getParseDecodeData() && dataFormat == FORMAT_ISO)
                parseTrack1Data(track1DataString);
            if (getTransmitSentinels())
                track1Data = (MSR_TRACK1_START_SENTINEL + track1DataString + MSR_END_SENTINEL)
                    .getBytes();
            else
                track1Data = track1DataString.getBytes();
        }

        if (track2DataString.length() != 0 && (getTracksToRead() & MSRConst.MSR_TR_2) != 0) {
            track2DataString = removeEndSentinelAndLRC(track2DataString);
            if (getParseDecodeData())
                parseTrack2Data(track2DataString);
            if (getTransmitSentinels())
                track2Data = (MSR_TRACK2_START_SENTINEL + track2DataString + MSR_END_SENTINEL)
                    .getBytes();
            else
                track2Data = track2DataString.getBytes();
        }

        if (track3DataString.length() != 0 && (getTracksToRead() & MSRConst.MSR_TR_3) != 0) {
            track3DataString = removeEndSentinelAndLRC(track3DataString);
            // We'll not parse track 3 data
            if (getTransmitSentinels())
                track3Data = (MSR_TRACK3_START_SENTINEL + track3DataString + MSR_END_SENTINEL)
                    .getBytes();
            else
                track3Data = track3DataString.getBytes();
        }

        eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(),
            track1Data.length | track2Data.length << 8 | track3Data.length << 16));
    }

    /**
     * Parse track 1 data in IATA format (ISO 7813).
     * 
     * @param msrData
     *            Track 1 data without start and end sentinels.
     */
    private void parseTrack1Data(String msrData) {
        try {
            String fullName;
            int idxHead = 0;
            int idxTail;

            // FC: Format Code. 1 character (alphabetic only):
            // A: Reserved for proprietary use of card issuer.
            // B: Bank/financial. This is the format described here.
            // C-M: Reserved for use by ANSI Subcommittee X3B10.
            // N-Z: Available for use by individual card issuers.
            char formatCode = msrData.charAt(idxHead++);

            // If it is not format B, we'll not parse anything
            if (formatCode != 'B' && formatCode != 'b')
                return;

            // PAN: Primary Account Number. Up to 19 digits:
            idxTail = msrData.indexOf('^', idxHead);
            if (idxTail == -1) {
                accountNumber = msrData.substring(idxHead);
                return;
            }
            accountNumber = msrData.substring(idxHead, idxTail);
            idxHead = idxTail + 1;

            // CC: Country Code. 3 digits: Only if PAN starts with 59
            // (MasterCard).
            if (accountNumber.startsWith("59"))
                idxHead += 3;

            // NM: Name. 2-26 characters
            idxTail = msrData.indexOf('^', idxHead);
            if (idxTail == -1) {
                fullName = msrData.substring(idxHead);
                splitName(fullName);
                return;
            }
            fullName = msrData.substring(idxHead, idxTail);
            splitName(fullName);
            idxHead = idxTail + 1;

            // ED: Expiry Date. 4 digits: YYMM. Required by MasterCard and VISA
            expirationDate = msrData.substring(idxHead, idxHead + 4);
            idxHead += 4;

            // SC: Service Code. 3 digits. Required by MasterCard and VISA.
            serviceCode = msrData.substring(idxHead, idxHead + 3);
            idxHead += 3;

            // PVV: Offset or PVV (PIN Verification Value). 5 digits
            idxHead += 5;

            // DD: Discretionary Data. Rest of characters: Reserved for
            // proprietary use of card issuer.
            track1DiscretionaryData = msrData.substring(idxHead).getBytes();

        } catch (StringIndexOutOfBoundsException e) {
            // ignore it
        }
    }

    /**
     * Parse track 2 data in ABA format (ISO 7813).
     * 
     * @param msrData
     *            Track 2 data without start and end sentinels.
     */
    private void parseTrack2Data(String msrData) {
        try {
            int idxHead = 0;
            int idxTail;

            // PAN: Primary Account Number. Up to 19 digits
            idxTail = msrData.indexOf('^', idxHead);
            if (idxTail == -1) {
                idxTail = msrData.indexOf('=', idxHead);
                if (idxTail == -1) {
                    accountNumber = msrData.substring(idxHead);
                    return;
                }
            }
            accountNumber = msrData.substring(idxHead, idxTail);
            idxHead = idxTail + 1;

            // CC: Country Code. 3 digits: Only if PAN starts with 59
            // (MasterCard).
            if (accountNumber.startsWith("59"))
                idxHead += 3;

            // ED: Expiry Date. 4 digits: YYMM. Required by MasterCard and VISA
            expirationDate = msrData.substring(idxHead, idxHead + 4);
            idxHead += 4;

            // SC: Service Code. 3 digits. Required by MasterCard and VISA.
            serviceCode = msrData.substring(idxHead, idxHead + 3);
            idxHead += 3;

            // PVV: Offset or PVV (PIN Verification Value). 5 digits
            idxHead += 5;

            // DD: Discretionary Data. Rest of characters: Reserved for
            // proprietary use of card issuer.
            track2DiscretionaryData = msrData.substring(idxHead).getBytes();

        } catch (StringIndexOutOfBoundsException e) {
            // ignore it
        }
    }

    /**
     * Create a keyboard event interceptor for accepting and firing MSR event. The interceptor is an
     * AWT's KeyEventDispatcher, and then it'll be added into KeyboardFocusManager when enabling
     * this device.
     */
    private void createKeyEventInterceptor() {

        keyEventInterceptor = new KeyEventDispatcher() {
            boolean withinControlSeq;
            StringBuffer msrData = new StringBuffer();
            int waitLastEvent;

            public boolean dispatchKeyEvent(KeyEvent e) {
                try {
                    setState(JposConst.JPOS_S_BUSY);

                    if (e.getID() != KeyEvent.KEY_TYPED) {
                        if (waitLastEvent > 0) {
                            waitLastEvent--;
                            return true;
                        }

                        // While in-between control sequence of MSR, return true
                        // to absort any key event for preventing from sending
                        // them to application
                        return withinControlSeq;
                    }

                    char keyChar = e.getKeyChar();
                    // System.out.println("char=" + keyChar + "(" + (int)keyChar + ")");

                    if (keyChar == MSR_START_CHAR) {
                        msrData.setLength(0);
                        msrData.append(keyChar);
                        withinControlSeq = true;

                    } else if (withinControlSeq) {
                        msrData.append(keyChar);

                        if (keyChar == 10) { // End Symbol
                            withinControlSeq = false;

                            // The last char is 10, but AWT system will still generate
                            // a KEY_RELEASE event. We also want to ignore and absort this
                            // KeyEvents, so we set a count here for doing this.
                            waitLastEvent = 1;

                            if (getFreezeEvents()) {
                                // Append into event queue if now the FreezeEvent is true
                                eventQueue.add(msrData.toString());
                            } else if (getDataEventEnabled()) {
                                // If AutoDisable is true, then automatically disable myself
                                if (getAutoDisable())
                                    setDeviceEnabled(false);

                                // Fire the DataEvent
                                parseAndFireMSRDataEvent(msrData.toString());
                            }
                        }
                        return true;
                    }
                    return false;
                } catch (JposException e1) {
                    e1.printStackTrace();
                    return false;
                } finally {
                    setState(JposConst.JPOS_S_IDLE);
                }
            }
        };
    }

    // Capabilities
    public boolean getCapISO() throws JposException {
        return true;
    }

    public boolean getCapJISOne() throws JposException {
        return true;
    }

    public boolean getCapJISTwo() throws JposException {
        return false;
    }

    public boolean getCapTransmitSentinels() {
        return true;
    }

    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Common properties...

    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        if (deviceEnabled && !getClaimed()) {
            System.out.println("Posiflex KB-6600 MSR must claim it before enabling it.");
            // we are not throwing any exception...may need to throw a
            // JposException here
            return;
        }
        super.setDeviceEnabled(deviceEnabled);

        // Setup a key event interceptor when enabling device
        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        if (deviceEnabled)
            kfm.addKeyEventDispatcher(keyEventInterceptor);
        else
            kfm.removeKeyEventDispatcher(keyEventInterceptor);
    }

    public void setAutoDisable(boolean autoDisable) throws JposException {
        this.autoDisable = autoDisable;
    }

    public boolean getAutoDisable() throws JposException {
        return autoDisable;
    }

    public int getDataCount() throws JposException {
        return eventQueue.size();
    }

    public boolean getDataEventEnabled() throws JposException {
        return dataEventEnabled;
    }

    private void fireEventsInQueue() {
        try {
            setState(JposConst.JPOS_S_BUSY);
            if (eventCallbacks != null && getClaimed() && getDeviceEnabled()
                && getDataEventEnabled() && !getFreezeEvents()) {
                while (!eventQueue.isEmpty()) {
                    // If AutoDisable is true, then automatically disable myself
                    if (getAutoDisable())
                        setDeviceEnabled(false);

                    parseAndFireMSRDataEvent((String)eventQueue.get(0));
                    eventQueue.remove(0);

                    if (getAutoDisable())
                        break;
                }
            }
        } catch (JposException e) {
            e.printStackTrace();
        } finally {
            setState(JposConst.JPOS_S_IDLE);
        }
    }

    public void setDataEventEnabled(boolean dataEventEnabled) throws JposException {
        this.dataEventEnabled = dataEventEnabled;
        fireEventsInQueue();
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        super.setFreezeEvents(freezeEvents);
        fireEventsInQueue();
    }

    // Specific properties...

    public String getAccountNumber() throws JposException {
        return accountNumber;
    }

    public boolean getDecodeData() throws JposException {
        // Always DecodeData
        return true;
    }

    public void setDecodeData(boolean decodeData) throws JposException {
    }

    public int getErrorReportingType() {
        return errorReportingType;
    }

    public void setErrorReportingType(int errorReportingType) throws JposException {
        this.errorReportingType = errorReportingType;
    }

    public String getExpirationDate() throws JposException {
        return expirationDate;
    }

    public String getFirstName() throws JposException {
        return firstName;
    }

    public String getMiddleInitial() throws JposException {
        return middleInitial;
    }

    public boolean getParseDecodeData() {
        return parseDecodeData;
    }

    public void setParseDecodeData(boolean parseDecodeData) {
        this.parseDecodeData = parseDecodeData;
    }

    public String getServiceCode() throws JposException {
        return serviceCode;
    }

    public String getSuffix() throws JposException {
        return suffix;
    }

    public String getSurname() throws JposException {
        return surname;
    }

    public String getTitle() throws JposException {
        return title;
    }

    public byte[] getTrack1Data() throws JposException {
        return track1Data;
    }

    public byte[] getTrack1DiscretionaryData() throws JposException {
        return track1DiscretionaryData;
    }

    public byte[] getTrack2Data() throws JposException {
        return track2Data;
    }

    public byte[] getTrack2DiscretionaryData() throws JposException {
        return track2DiscretionaryData;
    }

    public byte[] getTrack3Data() throws JposException {
        return track3Data;
    }

    public byte[] getTrack4Data() throws JposException {
        return new byte[0];
    }

    public int getTracksToRead() {
        return tracksToRead;
    }

    public void setTracksToRead(int tracksToRead) {
        this.tracksToRead = tracksToRead;
    }

    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    public void setTransmitSentinels(boolean transmitSentinels) {
        this.transmitSentinels = transmitSentinels;
    }

    public boolean getTransmitSentinels() {
        return transmitSentinels;
    }

    // Methods
    public void deleteInstance() throws JposException {
    }

    public void claim(int timeout) throws JposException {
        if (claimed) {
            System.out.println("device has been claimed");
            return;
        }
        synchronized (mutex) {
            if (claimedControl == null) {
                claimedControl = (MSR)eventCallbacks.getEventSource();
                claimed = true;
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                    claimedControl = (MSR)eventCallbacks.getEventSource();
                    claimed = true;
                    return;
                } catch (InterruptedException ex1) {
                    System.out.println(ex1.toString());
                    System.out.println("Interrupted exception at " + this);
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        if (claimedControl == null) {
                            claimedControl = (MSR)eventCallbacks.getEventSource();
                            claimed = true;
                            return;
                        }
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    System.out.println("device is busy");
                    return;
                } catch (InterruptedException ex1) {
                    System.out.println(ex1.toString());
                    System.out.println("Interrupted exception at " + this);
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        }
    }

    public void checkHealth(int level) throws JposException {
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
    }

    public void open(String logicalName, EventCallbacks eventCallbacks) throws JposException {
        super.open(logicalName, eventCallbacks);
        setState(JposConst.JPOS_S_IDLE);
        setDecodeData(true);
        setParseDecodeData(true);
    }

    public void close() throws JposException {
        setClaimed(false);
        claimedControl = null;
        setDeviceEnabled(false);
        setState(JposConst.JPOS_S_CLOSED);
    }

    public void release() throws JposException {
        synchronized (mutex) {
            if (!claimed) {
                System.out.println("device has been release");
            }
            setClaimed(false);
            setDeviceEnabled(false);
            claimedControl = null;
            mutex.notifyAll();
        }
    }

    public void clearInput() throws JposException {
        eventQueue.clear();
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }
}