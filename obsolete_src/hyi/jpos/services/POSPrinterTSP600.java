package hyi.jpos.services;

import hyi.cream.util.BMPImageProcessor;
import hyi.cream.util.CreamToolkit;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;

import javax.comm.CommPortIdentifier;
import javax.comm.ParallelPort;
import javax.comm.ParallelPortEvent;
import javax.comm.ParallelPortEventListener;
import javax.comm.PortInUseException;
import javax.swing.ImageIcon;

import jpos.JposConst;
import jpos.JposException;
import jpos.POSPrinterConst;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.services.EventCallbacks;
import jpos.services.POSPrinterService14;
import jpos.services.POSPrinterService19;

public class POSPrinterTSP600 extends DeviceService implements
		ParallelPortEventListener, POSPrinterService19 {
	private Object waitObject = new Object();

	private StringParsingDRJST51 parse;

	private OutputStreamWriter dataOutput;

	private ParallelPort parallelPort;

	private CommPortIdentifier portId;

	private Enumeration portList;

	static private String logicalName;

	private boolean transactionJournalMode;

	private boolean transactionReceiptMode;

	private boolean transactionSlipMode;

	private StringBuffer transactionJournalBuffer = new StringBuffer();

	private StringBuffer transactionReceiptBuffer = new StringBuffer();

	private StringBuffer transactionSlipBuffer = new StringBuffer();

	private InputStream inputStream;

	/** serial port current feedback status got asyncronously */
	private byte[] readPort = new byte[10];

	private byte statusCode = 0x00;

	private JposEntry POSPrinterentry;

	private boolean insertionFlag = false;

	/** AsyncMode flag definition */
	private boolean asyncMode = false;

	private int characterSet = POSPrinterConst.PTR_CS_ASCII;

	private boolean flagWhenIdle = true;

	private int outputID = 0;

	/**
	 * Define Specified Font File
	 */

	private RandomAccessFile fontFile;

	private OutputStream out = null;

	private int[] bitmaps = new int[16];

	private String toplogo = null;

	private String bottomlogo = null;

	static private POSPrinterTSP600 instance;

	public String getTopLogo() throws JposException {
		if (toplogo != null)
			return toplogo;
		else
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Null pointer logo detected!");
	}

	public String getBottomLogo() throws JposException {
		if (bottomlogo != null)
			return bottomlogo;
		else
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Null pointer logo detected!");
	}

	public POSPrinterTSP600() throws IOException {
		if (System.getProperties().get("os.name").toString().indexOf("Windows") != -1)
			out = new FileOutputStream("lpt1");
		else
			out = new FileOutputStream("/dev/lp0");

		// openPrinter();
		dataOutput = new OutputStreamWriter(out);
		out.write(0x1B);
		out.write('@');
		instance = this;
	}

	private void openPrinter() {
		if (portId != null) {
			try {
				parallelPort = (ParallelPort) portId.open(logicalName, 10000);
				out = parallelPort.getOutputStream();
				return;
			} catch (PortInUseException e) {
			} catch (IOException e) {
			}
		}

		portList = CommPortIdentifier.getPortIdentifiers();
		while (portList.hasMoreElements()) {
			portId = (CommPortIdentifier) portList.nextElement();
			if (portId.getPortType() == CommPortIdentifier.PORT_PARALLEL
					&& portId.getName().equalsIgnoreCase("LPT1")) {
				try {
					parallelPort = (ParallelPort) portId.open(logicalName,
							10000);
					out = parallelPort.getOutputStream();
					return;
				} catch (PortInUseException e) {
					CreamToolkit.logMessage("openPrinter(): " + e.getMessage());
				} catch (IOException e) {
					CreamToolkit.logMessage("openPrinter(): " + e.getMessage());
				}
				break;
			}
		}
		portId = null;
	}

	static public POSPrinterTSP600 getInstance() {
		return instance;
	}

	/**
	 * constructor with JposEntry parameter
	 */
	public POSPrinterTSP600(SimpleEntry entry) throws IOException {
		this();
		POSPrinterentry = entry;
	}

	/**
	 * Service13 Capabilities
	 */
	public int getCapPowerReporting() throws JposException {
		return JposConst.JPOS_PR_STANDARD;
	}

	// --------------------------------------------------------------------------

	/**
	 * Service12 Capabilities
	 */
	public int getCapCharacterSet() throws JposException {
		return POSPrinterConst.PTR_CCS_ASCII;
	}

	// Journal and Receipt stations can print at the same time.
	public boolean getCapConcurrentJrnRec() throws JposException {
		return true;
	}

	// If true, then the Journal and Slip stations can print at the same time.
	public boolean getCapConcurrentJrnSlp() throws JposException {
		return false;
	}

	// If true, then the Receipt and Slip stations can print at the same time.
	public boolean getCapConcurrentRecSlp() throws JposException {
		return false;
	}

	// If true, then the printer has a "cover open” sensor.
	public boolean getCapCoverSensor() throws JposException {
		return false;
	}

	// If true, then the journal can print dark plus an alternate color.
	public boolean getCapJrn2Color() throws JposException {
		return false;
	}

	// If true, then the journal can print bold characters.
	public boolean getCapJrnBold() throws JposException {
		return false;
	}

	// If true, then the journal can print double high characters.
	public boolean getCapJrnDhigh() throws JposException {
		return false;
	}

	// If true, then the journal can print double wide characters.
	public boolean getCapJrnDwide() throws JposException {
		return true;
	}

	// If true, then the journal can print double high / double wide characters.
	public boolean getCapJrnDwideDhigh() throws JposException {
		return false;
	}

	// If true, then the journal has an out-of-paper sensor.
	public boolean getCapJrnEmptySensor() throws JposException {
		return true;
	}

	// If true, then the journal can print italic characters.
	public boolean getCapJrnItalic() throws JposException {
		return false;
	}

	// If true, then the journal has a low paper sensor.
	public boolean getCapJrnNearEndSensor() throws JposException {
		return true;
	}

	// If true, then the journal print station is present.
	public boolean getCapJrnPresent() throws JposException {
		return true;
	}

	// If true, then the journal can underline characters.
	public boolean getCapJrnUnderline() throws JposException {
		return false;
	}

	// If true, then the receipt can print dark plus an alternate color.
	public boolean getCapRec2Color() throws JposException {
		return false;
	}

	// If true, then the receipt has bar code printing capability.
	public boolean getCapRecBarCode() throws JposException {
		return false;
	}

	// If true, then the receipt can print bitmaps.
	public boolean getCapRecBitmap() throws JposException {
		return true;
	}

	// If true, then the receipt can print bold characters.
	public boolean getCapRecBold() throws JposException {
		return false;
	}

	// If true, then the receipt can print double high characters.
	public boolean getCapRecDhigh() throws JposException {
		return false;
	}

	// If true, then the receipt can print double wide characters.
	public boolean getCapRecDwide() throws JposException {
		return true;
	}

	// If true, then the receipt can print double wide/high characters.
	public boolean getCapRecDwideDhigh() throws JposException {
		return false;
	}

	// If true, then the receipt has an out-of-paper sensor.
	public boolean getCapRecEmptySensor() throws JposException {
		return true;
	}

	// If true, then the receipt can print italic characters.
	public boolean getCapRecItalic() throws JposException {
		return false;
	}

	// If true, then the receipt can print in rotated 90° left mode.
	public boolean getCapRecLeft90() throws JposException {
		return false;
	}

	// If true, then the receipt has a low paper sensor.
	public boolean getCapRecNearEndSensor() throws JposException {
		return true;
	}

	// If true, then the receipt can perform paper cuts.
	public boolean getCapRecPapercut() throws JposException {
		return true;
	}

	// If true, then the receipt print station is present.
	public boolean getCapRecPresent() throws JposException {
		return true;
	}

	// If true, then the receipt can print in a rotated 90° right mode.
	public boolean getCapRecRight90() throws JposException {
		return false;
	}

	// If true, then the receipt can print in a rotated upside down mode.
	public boolean getCapRecRotate180() throws JposException {
		return false;
	}

	// If true, then the receipt has a stamp capability.
	public boolean getCapRecStamp() throws JposException {
		return true;
	}

	// If true, then the receipt can underline characters.
	public boolean getCapRecUnderline() throws JposException {
		return false;
	}

	// If true, then the slip can print dark plus an alternate color.
	public boolean getCapSlp2Color() throws JposException {
		return false;
	}

	// If true, then the slip has bar code printing capability.
	public boolean getCapSlpBarCode() throws JposException {
		return false;
	}

	// If true, then the slip can print bitmaps.
	public boolean getCapSlpBitmap() throws JposException {
		return false;
	}

	// If true, then the slip can print bold characters.
	public boolean getCapSlpBold() throws JposException {
		return false;
	}

	// If true, then the slip can print double high characters.
	public boolean getCapSlpDhigh() throws JposException {
		return false;
	}

	// If true, then the slip can print double wide characters.
	public boolean getCapSlpDwide() throws JposException {
		return true;
	}

	// If true, then the slip can print double high / double wide characters.
	public boolean getCapSlpDwideDhigh() throws JposException {
		return false;
	}

	// If true, then the slip has a "slip in" sensor.
	public boolean getCapSlpEmptySensor() throws JposException {
		return true;
	}

	/*
	 * If true, then the slip is a full slip station. It can print full-length
	 * forms. If false, then the slip is a “validation” type station. This
	 * usually limits the number of print lines, and disables access to the
	 * receipt and/or journal stations while the validation slip is being used.
	 */
	public boolean getCapSlpFullslip() throws JposException {
		return false;
	}

	// If true, then the slip can print italic characters.
	public boolean getCapSlpItalic() throws JposException {
		return false;
	}

	// If true, then the slip can print in a rotated 90° left mode.
	public boolean getCapSlpLeft90() throws JposException {
		return false;
	}

	// If true, then the slip has a “slip near end” sensor.
	public boolean getCapSlpNearEndSensor() throws JposException {
		return false;
	}

	// If true, then the slip print station is present.
	public boolean getCapSlpPresent() throws JposException {
		return true;
	}

	// If true, then the slip can print in a rotated 90° right mode.
	public boolean getCapSlpRight90() throws JposException {
		return false;
	}

	// If true, then the slip can print in a rotated upside down mode.
	public boolean getCapSlpRotate180() throws JposException {
		return false;
	}

	// If true, then the slip can underline characters.
	public boolean getCapSlpUnderline() throws JposException {
		return false;
	}

	// If true, then printer transactions are supported by each station.
	public boolean getCapTransaction() throws JposException {
		return true;
	}

	// the end of Service12 capabilities

	/**
	 * Service13 Properties
	 */
	public int getPowerNotify() throws JposException {
		return JposConst.JPOS_PN_DISABLED;
	}

	public void setPowerNotify(int powerNotify) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "No Service!");
	}

	public int getPowerState() throws JposException {
		return JposConst.JPOS_PS_UNKNOWN;
	}

	/**
	 * Service12 Properties
	 */

	// Sync/Async mode
	public boolean getAsyncMode() throws JposException {
		return asyncMode;
	}

	public void setAsyncMode(boolean asyncMode) throws JposException {
		if (getAsyncMode() && !asyncMode) {
			this.asyncMode = false;
			System.out.println("POSPrinter runs in SyncMode!");
			return;
		} else if (!getAsyncMode() && asyncMode) {
			this.asyncMode = true;
			System.out.println("POSPrinter runs in AsyncMode!");
			return;
		} else
			return;
		// throw new JposException(JposConst.JPOS_E_ILLEGAL, "Expected mode
		// can't be obtained: Change to the same mode!");
	}

	public int getCharacterSet() throws JposException {
		return characterSet;
	}

	public void setCharacterSet(int characterSet) throws JposException {
		this.characterSet = characterSet;
	}

	// Holds the character set numbers. It consists of ASCII numeric set numbers
	// separated by commas.
	public String getCharacterSetList() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE);
	}

	// If true, then the printer’s cover is open.
	public boolean getCoverOpen() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE);
	}

	// Holds the severity of the error condition. It has one of the following
	// values:
	// PTR_EL_NONE No error condition is present.
	// PTR_EL_RECOVERABLE A recoverable error has occurred.(Example: Out of
	// paper.)
	// PTR_EL_FATAL A non-recoverable error has occurred.(Example: Internal
	// printer failure.)
	// --------------------------------------------------------------------------
	// This property is set just before delivering an ErrorEvent. When the error
	// is
	// cleared, then the property is changed to PTR_EL_NONE.
	// --------------------------------------------------------------------------

	private int errorLevel = POSPrinterConst.PTR_EL_NONE;

	public int getErrorLevel() throws JposException {
		return errorLevel;
	}

	/*
	 * Holds the station or stations that were printing when an error was
	 * detected. This property will be set to one of the following values:
	 * PTR_S_JOURNAL PTR_S_RECEIPT PTR_S_SLIP PTR_S_JOURNAL_RECEIPT
	 * PTR_S_JOURNAL_SLIP PTR_S_RECEIPT_SLIP PTR_TWO_RECEIPT_JOURNAL
	 * PTR_TWO_SLIP_JOURNAL PTR_TWO_SLIP_RECEIPT This property is only valid if
	 * the ErrorLevel is not equal to PTR_EL_NONE. It is set just before
	 * delivering an ErrorEvent.
	 */

	private int errorStation = 0;

	public int getErrorStation() throws JposException {
		return errorStation;
	}

	/*
	 * Holds a vendor-supplied description of the current error. This property
	 * is set just before delivering an ErrorEvent. If no description is
	 * available, the property is set to an empty string. When the error is
	 * cleared, then the property is changed to an empty string.
	 */
	private String errorString = "";

	public String getErrorString() throws JposException {
		return errorString;
	}

	/*
	 * If true, a StatusUpdateEvent will be enqueued when the device is in the
	 * idle state. This property is automatically reset to false when the status
	 * event is delivered. The main use of idle status event that is controlled
	 * by this property is to give the application control when all outstanding
	 * asynchronous outputs have been processed. The event will be enqueued if
	 * the outputs were completed successfully or if they were cleared by the
	 * clearOutput method or by an ErrorEvent handler. If the State is already
	 * set to JPOS_S_IDLE when this property is set to true, then a
	 * StatusUpdateEvent is enqueued immediately. The application can therefore
	 * depend upon the event, with no race condition between the starting of its
	 * last asynchronous output and the setting of this flag. This property is
	 * initialized to false by the open method.
	 */
	public boolean getFlagWhenIdle() throws JposException {
		return flagWhenIdle;
	}

	public void setFlagWhenIdle(boolean flagWhenIdle) throws JposException {
		this.flagWhenIdle = flagWhenIdle;
	}

	/*
	 * Holds the fonts and/or typefaces that are supported by the printer. The
	 * string consists of font or typeface names separated by commas. This
	 * property is initialized by the open method.
	 */
	public String getFontTypefaceList() throws JposException {
		return "";
	}

	public boolean getJrnEmpty() throws JposException {
		if (!getCapJrnEmptySensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(),
					ie);
		}
		statusCode = readPort[0];
		if ((statusCode & 0x01) == 0x00)
			return false;
		else
			return true;
	}

	public boolean getJrnLetterQuality() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setJrnLetterQuality(boolean jrnLetterQuality)
			throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	// Holds the number of characters that may be printed on a journal line.
	private int jrnLineChars = 24;

	public int getJrnLineChars() throws JposException {
		return jrnLineChars;
	}

	// This property is initialized to the printer’s default line character
	// width
	// when the device is first enabled following the open method.
	public void setJrnLineChars(int jrnLineChars) throws JposException {
		if (jrnLineChars < 0 || jrnLineChars > 24)
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Invalid agrument:" + jrnLineChars);
		this.jrnLineChars = jrnLineChars;
	}

	public String getJrnLineCharsList() throws JposException {
		return "12,24";
	}

	public int getJrnLineHeight() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setJrnLineHeight(int jrnLineHeight) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getJrnLineSpacing() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setJrnLineSpacing(int jrnLineSpacing) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getJrnLineWidth() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getJrnNearEnd() throws JposException {
		if (!getCapJrnNearEndSensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(),
					ie);
		}
		statusCode = readPort[0];
		if ((statusCode & 0x01) == 0x00)
			return false;
		else
			return true;
	}

	public int getMapMode() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setMapMode(int mapMode) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public String getRecBarCodeRotationList() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getRecEmpty() throws JposException {
		if (!getCapRecEmptySensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(),
					ie);
		}
		statusCode = readPort[0];// if statusCode equals zero, the paper is
		// present
		if ((statusCode & 0x02) == 0x00)
			return false;
		else
			return true;
	}

	public boolean getRecLetterQuality() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRecLetterQuality(boolean recLetterQuality)
			throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	private int recLineChars = 24;

	public int getRecLineChars() throws JposException {
		return recLineChars;
	}

	public void setRecLineChars(int recLineChars) throws JposException {
		if (recLineChars < 0 || recLineChars > 24)
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Invalid agrument:" + jrnLineChars);
		this.recLineChars = recLineChars;
	}

	public String getRecLineCharsList() throws JposException {
		return "12,24";
	}

	public int getRecLineHeight() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRecLineHeight(int recLineHeight) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getRecLineSpacing() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRecLineSpacing(int recLineSpacing) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	private int recLinesToPaperCut = 0;

	public int getRecLinesToPaperCut() throws JposException {
		return recLinesToPaperCut;
	}

	public int getRecLineWidth() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getRecNearEnd() throws JposException {
		if (!getCapRecNearEndSensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(),
					ie);
		}
		statusCode = readPort[0]; // if statusCode equals zero, the paper is
		// present
		if ((statusCode & 0x02) == 0x00)
			return false;
		else
			return true;
	}

	public int getRecSidewaysMaxChars() throws JposException {
		return 0;
	}

	public int getRecSidewaysMaxLines() throws JposException {
		return 0;
	}

	public int getRotateSpecial() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRotateSpecial(int rotateSpecial) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public String getSlpBarCodeRotationList() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getSlpEmpty() throws JposException {
		if (!getCapSlpEmptySensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(),
					ie);
		}
		statusCode = readPort[0];// if statusCode equals zero, the paper is
		// present
		if ((statusCode & 0x20) == 0x00)
			return false;
		else
			return true;

	}

	public boolean getSlpLetterQuality() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setSlpLetterQuality(boolean recLetterQuality)
			throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	// --------------------------------------------------------------------------
	private int slpLineChars = 55;

	public int getSlpLineChars() throws JposException {
		return slpLineChars;
	}

	public void setSlpLineChars(int slpLineChars) throws JposException {
		if (slpLineChars < 0 || slpLineChars > 55)
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Invalid agrument:" + slpLineChars);
		this.slpLineChars = slpLineChars;
	}

	// --------------------------------------------------------------------------

	public String getSlpLineCharsList() throws JposException {
		return "27,55";
	}

	public int getSlpLineHeight() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setSlpLineHeight(int slpLineHeight) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpLinesNearEndToEnd() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpLineSpacing() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setSlpLineSpacing(int slpLineSpacing) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpLineWidth() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpMaxLines() throws JposException {
		return 1;
	}

	public boolean getSlpNearEnd() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpSidewaysMaxChars() throws JposException {
		return 0;
	}

	public int getSlpSidewaysMaxLines() throws JposException {
		return 0;
	}

	// --------------------------------------------------------------------------
	public int getOutputID() throws JposException {
		return outputID;
	}

	// new...begin Service12 Methods
	// first sector the synchronized method
	synchronized public void beginInsertion(int timeout) throws JposException {
		if (timeout < -1)
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Illegal parameter:" + timeout);
		if (!getCapSlpPresent())
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not supported");
	}

	synchronized public void endInsertion() throws JposException {
		insertionFlag = true;
	}

	synchronized public void beginRemoval(int timeout) throws JposException {
		if (timeout < -1)
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Illegal parameter:" + timeout);
		if (!getCapSlpPresent())
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not supported");
	}

	synchronized public void endRemoval() throws JposException {
		insertionFlag = true;
	}

	// end of first sector the synchronized method

	public void clearOutput() throws JposException {
		outputID = 0;
	}

	public void printBarCode(int station, String data, int symbology,
			int height, int width, int alignment, int textPosition)
			throws JposException {

		throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not support!");
	}// end of printBarCode

	public void printBitmap(int station, String fileName, int width,
			int alignment) throws JposException {
//		if (!getClaimed())
//			throw new JposException(JposConst.JPOS_E_NOTCLAIMED,
//					"Please claim the printer before manipulation!");
//		if (!getDeviceEnabled())
//			throw new JposException(JposConst.JPOS_E_DISABLED,
//					"Printer is not enabled!");
		try {
			Image image = getImageFromFile(fileName);
			printBitmap(station, image, width, alignment);
		} catch (JposException jposexception) {
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not support!");
		}
	}// end of printBitmap

	private synchronized void printBitmap(int station, Image image, int width,
			int alignment) throws JposException {
		// = 1
		int recLineWidth = 576;
		BufferedImage bufferedImage = null;
		int height = 0;
		try {
			if (width != -11)
				image = new ImageIcon(image.getScaledInstance(width, -1, 1))
						.getImage();
			width = image.getWidth(null);
			height = image.getHeight(null);
		} catch (Exception e) {
			e.printStackTrace(CreamToolkit.getLogger());
			throw new JposException(114, 207);
		}

		if (width > recLineWidth)
			throw new JposException(114, 206);
		switch (alignment) {
		case -1:
			alignment = 0;
			break;
		case -2:
			alignment = (recLineWidth - width) / 2;
			break;
		case -3:
			alignment = recLineWidth - width;
			break;
		}
		if (width + alignment > recLineWidth)
			throw new JposException(106);

		if (width <= 0 || alignment < 0)
			throw new JposException(106);
		try {
			bufferedImage = new BufferedImage(image.getWidth(null), image
					.getHeight(null), 1);
			java.awt.Graphics g = bufferedImage.createGraphics();
			g.setColor(Color.white);
			g.fillRect(0, 0, image.getWidth(null), image.getHeight(null));
			g.drawImage(image, 0, 0, null);
			g.dispose();
		} catch (Exception e) {
			e.printStackTrace(CreamToolkit.getLogger());
			throw new JposException(114, 207);
		}

		int length = 0;
		int bufferIdx = 0;
		int i = 0;
		int j = 0;
		byte[] buffer;
		try {

			if ((alignment % 8 + width) % 8 != 0)
				length = (29 + (3 + (alignment % 8 + width) / 8 + 1) * height + 4);
			else
				length = 29 + (3 + (alignment % 8 + width) / 8) * height + 4;
			buffer = new byte[length];
			buffer[bufferIdx++] = (byte) 27;
			buffer[bufferIdx++] = (byte) 42;
			buffer[bufferIdx++] = (byte) 114;
			buffer[bufferIdx++] = (byte) 82;
			buffer[bufferIdx++] = (byte) 27;
			buffer[bufferIdx++] = (byte) 42;
			buffer[bufferIdx++] = (byte) 114;
			buffer[bufferIdx++] = (byte) 65;
			buffer[bufferIdx++] = (byte) 27;
			buffer[bufferIdx++] = (byte) 42;
			buffer[bufferIdx++] = (byte) 114;
			buffer[bufferIdx++] = (byte) 69;
			buffer[bufferIdx++] = (byte) 49;
			buffer[bufferIdx++] = (byte) 0;
			buffer[bufferIdx++] = (byte) 27;
			buffer[bufferIdx++] = (byte) 42;
			buffer[bufferIdx++] = (byte) 114;
			buffer[bufferIdx++] = (byte) 80;
			buffer[bufferIdx++] = (byte) 48;
			buffer[bufferIdx++] = (byte) 0;
			buffer[bufferIdx++] = (byte) 27;
			buffer[bufferIdx++] = (byte) 42;
			buffer[bufferIdx++] = (byte) 114;
			buffer[bufferIdx++] = (byte) 109;
			buffer[bufferIdx++] = (byte) 108;
			buffer[bufferIdx++] = (byte) (alignment / 800 % 10 + 48);
			buffer[bufferIdx++] = (byte) (alignment / 80 % 10 + 48);
			buffer[bufferIdx++] = (byte) (alignment / 8 % 10 + 48);
			buffer[bufferIdx++] = (byte) 0;
			i = 0;
			while (i < height) {
				if ((alignment % 8 + width) % 8 != 0) {
					buffer[bufferIdx++] = (byte) 98;
					buffer[bufferIdx++] = (byte) (((alignment % 8 + width) / 8 + 1) % 256);
					buffer[bufferIdx++] = (byte) (((alignment % 8 + width) / 8 + 1) / 256);
				} else {
					buffer[bufferIdx++] = (byte) 98;
					buffer[bufferIdx++] = (byte) ((alignment % 8 + width) / 8 % 256);
					buffer[bufferIdx++] = (byte) ((alignment % 8 + width) / 8 / 256);
				}
				j = 0;
				switch (alignment % 8) {
				case 1:
					if (j < width
							&& (convertColor2Mono(bufferedImage.getRGB(j++, i)) == true))
						buffer[bufferIdx] |= 0x40;
					/* fall through */
				case 2:
					if (j < width
							&& (convertColor2Mono(bufferedImage.getRGB(j++, i)) == true))
						buffer[bufferIdx] |= 0x20;
					/* fall through */
				case 3:
					if (j < width
							&& (convertColor2Mono(bufferedImage.getRGB(j++, i)) == true))
						buffer[bufferIdx] |= 0x10;
					/* fall through */
				case 4:
					if (j < width
							&& (convertColor2Mono(bufferedImage.getRGB(j++, i)) == true))
						buffer[bufferIdx] |= 0x8;
					/* fall through */
				case 5:
					if (j < width
							&& (convertColor2Mono(bufferedImage.getRGB(j++, i)) == true))
						buffer[bufferIdx] |= 0x4;
					/* fall through */
				case 6:
					if (j < width
							&& (convertColor2Mono(bufferedImage.getRGB(j++, i)) == true))
						buffer[bufferIdx] |= 0x2;
					/* fall through */
				case 7:
					if (j < width
							&& (convertColor2Mono(bufferedImage.getRGB(j++, i)) == true))
						buffer[bufferIdx] |= 0x1;
					/* fall through */
				default:
					if (alignment % 8 != 0)
						bufferIdx++;
					while (j < width) {
						switch (width - j) {
						default:
							if ((convertColor2Mono(bufferedImage.getRGB(j + 7,
									i))) == true)
								buffer[bufferIdx] |= 0x1;
							/* fall through */
						case 7:
							if ((convertColor2Mono(bufferedImage.getRGB(j + 6,
									i))) == true)
								buffer[bufferIdx] |= 0x2;
							/* fall through */
						case 6:
							if ((convertColor2Mono(bufferedImage.getRGB(j + 5,
									i))) == true)
								buffer[bufferIdx] |= 0x4;
							/* fall through */
						case 5:
							if ((convertColor2Mono(bufferedImage.getRGB(j + 4,
									i))) == true)
								buffer[bufferIdx] |= 0x8;
							/* fall through */
						case 4:
							if ((convertColor2Mono(bufferedImage.getRGB(j + 3,
									i))) == true)
								buffer[bufferIdx] |= 0x10;
							/* fall through */
						case 3:
							if ((convertColor2Mono(bufferedImage.getRGB(j + 2,
									i))) == true)
								buffer[bufferIdx] |= 0x20;
							/* fall through */
						case 2:
							if ((convertColor2Mono(bufferedImage.getRGB(j + 1,
									i))) == true)
								buffer[bufferIdx] |= 0x40;
							/* fall through */
						case 1:
							if (convertColor2Mono(bufferedImage.getRGB(j, i)) == true)
								buffer[bufferIdx] |= 0x80;
							j += 8;
							bufferIdx++;
						}
					}
					i++;
				}
			}
			buffer[bufferIdx++] = (byte) 27;
			buffer[bufferIdx++] = (byte) 42;
			buffer[bufferIdx++] = (byte) 114;
			buffer[bufferIdx++] = (byte) 66;
			try {
				// print bitmap
				out.write(buffer);
//				out.write("\n".getBytes(CreamToolkit.getEncoding()));
			} catch (IOException e) {
				e.printStackTrace(CreamToolkit.getLogger());
			}

		} catch (Exception e) {
			e.printStackTrace(CreamToolkit.getLogger());
		}
	}

	public void printImmediate(String str) {
		Iterator iter = new CharIterator(str);
		while (iter.hasNext()) {
			String c = (String) iter.next();
			if (isChineseChar(c)) {
				printChineseCharacter(c);
			} else {
				printAlphanumeric((byte) c.charAt(0));
			}
		}
	}

	public void printImmediate(int station, String data) throws JposException {
		if (!getClaimed())
			throw new JposException(JposConst.JPOS_E_NOTCLAIMED,
					"Please claim the printer before manipulation!");
		if (!getDeviceEnabled())
			throw new JposException(JposConst.JPOS_E_DISABLED,
					"Printer is not enabled!");
		String realCommand = parse.getValidCommand(station, data);
		if (realCommand == null)
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Invalid station argument:" + station);
		else if (data.equals(""))
			return;
		try {
			printImmediateWithLineFeed(realCommand);
			dataOutput.write(realCommand);
			dataOutput.flush();
			synchronized (waitObject) {
				try {
					waitObject.wait();
				} catch (InterruptedException ie) {
					throw new JposException(JposConst.JPOS_E_ILLEGAL);
				}
			}
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_ILLEGAL);
		}
	}// end of printImmediate()

	// Main
	public void printNormal(int station, String data) throws JposException {
		if (data.equals(""))
			return;
		printdata(data);
	}

	public void printTwoNormal(int stations, String data1, String data2)
			throws JposException {
		if (data1.equals("") && data2.equals(""))
			return;
		printdata(data1);
		// printImmediateWithLineFeed(data1);
	}

	public void rotatePrint(int station, int rotation) throws JposException {
		throw new JposException(JposConst.JPOS_E_FAILURE,
				"Not supported in DRJST51!");
	}// end of rotatePrint

	public void setBitmap(int bitmapNumber, int station, String fileName,
			int width, int alignment) throws JposException {
		throw new JposException(JposConst.JPOS_E_FAILURE,
				"Not supported in DRJST51!");
	}// end of setBitmap

	public void setLogo(int location, String data) throws JposException {
		// The logo to be set. Location may be PTR_L_TOP or PTR_L_BOTTOM.
		if (location == POSPrinterConst.PTR_L_TOP)
			toplogo = data;
		else if (location == POSPrinterConst.PTR_L_BOTTOM)
			bottomlogo = data;
		else
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"An invalid location was specified:" + location + ".");
	}// end of setLogo

	// transactionPrint(int station, int control) throws JposException
	public void transactionPrint(int station, int control) throws JposException {
		if (!getCapTransaction())
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Transaction mode is not supported!");
		switch (station) {
		case POSPrinterConst.PTR_S_JOURNAL:
			if (!getCapJrnPresent())
				throw new JposException(JposConst.JPOS_E_ILLEGAL,
						"The station:" + station + " is not present");
			if (control == POSPrinterConst.PTR_TP_TRANSACTION)
				transactionJournalMode = true;
			else if (control == POSPrinterConst.PTR_TP_NORMAL)
				transactionJournalMode = false;
			else
				throw new JposException(JposConst.JPOS_E_ILLEGAL,
						"Error occured--illegal argument:" + control);
			break;
		case POSPrinterConst.PTR_S_RECEIPT:
			if (!getCapRecPresent())
				throw new JposException(JposConst.JPOS_E_ILLEGAL,
						"The station:" + station + " is not present");
			if (control == POSPrinterConst.PTR_TP_TRANSACTION)
				transactionReceiptMode = true;
			else if (control == POSPrinterConst.PTR_TP_NORMAL)
				transactionReceiptMode = false;
			else
				throw new JposException(JposConst.JPOS_E_ILLEGAL, "This");
			break;
		case POSPrinterConst.PTR_S_SLIP:
			if (!getCapSlpPresent())
				throw new JposException(JposConst.JPOS_E_ILLEGAL,
						"The station:" + station + " is not present");
			if (control == POSPrinterConst.PTR_TP_TRANSACTION)
				transactionSlipMode = true;
			else if (control == POSPrinterConst.PTR_TP_NORMAL)
				transactionSlipMode = false;
			else
				throw new JposException(JposConst.JPOS_E_ILLEGAL,
						"Error occured--illegal argument:" + control);
			break;
		default:
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"This station is not supported by hyi.jpos.POSPrinterDRJST51!");
		}
	}// end of transactionPrint()

	public void validateData(int station, String data) throws JposException {
		if (!(station == POSPrinterConst.PTR_S_JOURNAL
				|| station == POSPrinterConst.PTR_S_RECEIPT || station == POSPrinterConst.PTR_S_SLIP))
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Invalid station argument:" + station);
		if (data.equals(""))
			return;
		String item = null;
		String[] commonillegal = { "\u001B|rA", "\u001B|cA", "\u001B|3C",
				"\u001B|4C", "\u001B|rC", "\u001B|rvC", "\u001B|iC",
				"\u001B|bC" };
		ArrayList ESCSequence = parse.getESCSequence(data);
		ArrayList JposESCSequence = parse.getJposESCSequence(data);
		if (ESCSequence.isEmpty())
			return;
		if (JposESCSequence.size() != ESCSequence.size())
			throw new JposException(JposConst.JPOS_E_FAILURE,
					"Escape sequence is not supported by the printer:" + data);
		for (int k = 0; k < JposESCSequence.size(); k++) {
			item = (String) JposESCSequence.get(k);
			if (parse.contains(commonillegal, item))
				throw new JposException(JposConst.JPOS_E_FAILURE,
						"Not supported by the printer:" + data);
			else if (item.substring(item.length() - 1).equals("B")
					|| item.substring(item.length() - 2).equals("uF")
					|| item.substring(item.length() - 2).equals("rF")
					|| item.substring(item.length() - 2).equals("fT")
					|| item.substring(item.length() - 2).equals("uC")
					|| item.substring(item.length() - 2).equals("sC")
					|| item.substring(item.length() - 2).equals("hC")
					|| item.substring(item.length() - 2).equals("vC"))
				throw new JposException(JposConst.JPOS_E_FAILURE,
						"Not supported by the printer:" + data);
		}
		switch (station) {
		case POSPrinterConst.PTR_S_JOURNAL:
			for (int k = 0; k < JposESCSequence.size(); k++) {
				item = (String) JposESCSequence.get(k);
				if (item.substring(item.length() - 1).equals("P")
						|| item.substring(item.length() - 2).equals("sL"))
					throw new JposException(JposConst.JPOS_E_FAILURE,
							"Appointed manipulation is not supported at the specified station:"
									+ "station=" + station + " string=" + data);
			}
			break;
		case POSPrinterConst.PTR_S_RECEIPT:
			for (int k = 0; k < JposESCSequence.size(); k++) {
				item = (String) JposESCSequence.get(k);
				if (item.charAt(item.length() - 1) == 'P' && item.length() != 3)
					try {
						if (Integer
								.decode(item.substring(2, item.length() - 1))
								.intValue() < 0
								|| Integer.decode(
										item.substring(2, item.length() - 1))
										.intValue() > 100)
							throw new JposException(JposConst.JPOS_E_ILLEGAL,
									"Assigned value is illegal:"
											+ item.substring(2,
													item.length() - 1));
					} catch (Exception e) {
						System.out.println(e);
					}
			}
			break;
		case POSPrinterConst.PTR_S_SLIP:
			for (int k = 0; k < JposESCSequence.size(); k++) {
				item = (String) JposESCSequence.get(k);
				if (item.substring(item.length() - 1).equals("P")
						|| item.substring(item.length() - 2).equals("sL")
						|| item.substring(item.length() - 2).equals("lF"))
					throw new JposException(JposConst.JPOS_E_FAILURE,
							"Appointed manipulation is not supported at the specified station:"
									+ "station=" + station + " string=" + data);
			}
			break;
		}
	}

	// The end of Service12 Methods

	// --------------------------------------------------------------------------

	// new...begin BaseService Methods supported by all device services.
	public void claim(int timeout) throws JposException {
		super.claim(timeout);
	}

	public void release() throws JposException {
		if (!getClaimed())
			return;
		try {
			if (out != null)
				out.close();
			out = null;
			if (inputStream != null)
				inputStream.close();
			inputStream = null;
			if (dataOutput != null)
				dataOutput.close();
			dataOutput = null;
			super.release();
		} catch (IOException e) {
		}
	}

	// --------------------------------------------------------------------------
	// Close the device and unlink it from the device control!
	// --------------------------------------------------------------------------

	public void close() throws JposException {
		super.close();
		if (getClaimed())
			release();
		if (getAsyncMode())
			clearOutput();
		asyncMode = false;
		this.logicalName = null;
	}

	// The end of close()
	public void checkHealth(int level) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, this.toString());
	}

    /**
     * n0: "S"
     * n1: 收執聯位置 "0":定位 "1":未定位
     * n2: 存根聯位置 "0":定位 "1":未定位
     * n3: 紙張是否用完 "0":否 "1":是
     * n4: 印證紙張定位 "0":無紙 "1":有紙
     * n5: 發票印表機發生錯誤 "0":沒發生 "1":發生
     * n6: 是否列印中 "0":否 "1":是
     * n7: 列印超過40行 "0":否 "1":是
     * n8: 資料緩衝區是否已滿 "0":否 "1":是
     * n9: 資料緩衝區是否為空 "0":是 "1":否                
     */
    @Override
    public String getCheckHealthText() throws JposException {
        return "S000100000";
    }

	public void directIO(int command, int[] data, Object object)
			throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, this.toString());
	}

	public void open(String logicalName, EventCallbacks cb)
			throws JposException {
		super.open(logicalName, cb);
		asyncMode = false;
		outputID = 0;
		this.logicalName = logicalName;
	}

	// new...end BaseService Methods supported by all device services.

	// --------------------------------------------------------------------------
	/**
	 * serialEventListenner implementation, it is in another thread, executed
	 * automatically
	 */
	public void parallelEvent(ParallelPortEvent ev) {
	}

	public void deleteInstance() throws JposException {
	}

	private void printdata(String str) {
		try {
			out.write(str.getBytes(CreamToolkit.getEncoding()));
			out.flush();
		} catch (IOException ex) {
			ex.printStackTrace(System.err);
		}

	}

	private void printAlphanumeric(byte b) {
		try {
			out.write(b);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	private void printBitmap(int[] bytes) {
		// System.out.print("printBitmap(): ");
		try {
			// print bitmap
			out.write(new byte[] { 0x1B, (byte) '*', 0x01, // 0: 8-dot single
					// density, 1: 8-dot
					// double density
					(byte) bytes.length, // number of dots in horizontal
					// direction
					0x00 // number of another 256 dots in horizontal
					// direction
					});
			for (int i = 0; i < bytes.length; i++) {
				out.write(bytes[i]);
				// System.out.print(bytes[i] + ",");
			}
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	public void openDrawer() {
		try {
			out.write(0x1B);
			out.write('p');
			out.write(0x00);
			out.write(0x19);
			out.write(0xF0);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	public void setRedColor() {
		try {
			out.write(0x1B);
			out.write('r');
			out.write(0x01);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	public void setBlackColor() {
		try {
			out.write(0x1B);
			out.write('r');
			out.write(0x00);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	public void cutPaper(int percentage) throws JposException {
		try {
			String ESC = ((char) 0x1b) + "";
			for (int i = 0; i < 3; i++) { //6==>3
				out.write("\n".getBytes(CreamToolkit.getEncoding()));
			}
			out.write((ESC + "d1").getBytes(CreamToolkit.getEncoding()));
			out.flush();
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Inner class for iterating Chinese (in GB code) or alphanumeric
	 * characters.
	 */
	private class CharIterator implements Iterator {

		private byte[] str;

		private int index;

		private byte[] chineseChar = new byte[2];

		private byte[] alphanumChar = new byte[1];

		CharIterator(String str0) {
			try {
				if (str0 != null)
					str = str0.getBytes(CreamToolkit.getEncoding());
			} catch (UnsupportedEncodingException e) {
			}
		}

		public boolean hasNext() {
			return (str == null) ? false : (index < str.length);
		}

		public Object next() {
			if (!hasNext())
				return null;

			int b = str[index] & 0xff;
			// System.out.print("b=" + Integer.toHexString(b) + " ");
			if (index + 1 < str.length) {
				int b2 = str[index + 1] & 0xff;
				if ((b >= 0xA1 && b <= 0xA9 || b >= 0xB0 && b <= 0xF7) // first-byte
						// is a
						// GB
						// code
						&& (b2 >= 0xA1 && b2 <= 0xFE) // second-byte is a GB
				// code
				) {
					chineseChar[0] = (byte) b;
					chineseChar[1] = (byte) b2;
					index += 2;
					try {
						return new String(chineseChar, "ISO-8859-1");
					} catch (UnsupportedEncodingException e) {
						return "";
					}
				}
			}
			alphanumChar[0] = (byte) b;
			index++;
			return new String(alphanumChar);
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	private boolean isChineseChar(String str) {
		return (str.length() == 2); // 2-byte if it is a Chinese char
	}

	/**
	 * Get the Chinese font bitmap into upperBits and lowerBits
	 */
	private void getBitmap(String c) {
		byte[] b = new byte[32]; // bitmap bytes read from font file
		int b1 = c.charAt(0);
		int b2 = c.charAt(1);
		int offset;
		offset = ((b1 - 0xA1) * (0xFE - 0xA0) + (b2 - 0xA1)) * 32;
		try {
			fontFile.seek(offset);
			fontFile.read(b);
		} catch (IOException e) {
			e.printStackTrace(System.err);
			return;
		}
		int[] bb = new int[8];
		int i, j, mask;
		for (i = 0; i < 8; i++) {
			mask = 0x80 >>> i;
			for (j = 0; j <= 30; j += 4) {
				bb[j / 4] = ((b[j] & mask) != 0 || (b[j + 2] & mask) != 0) ? 1
						: 0;
			}
			bitmaps[i] = bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4
					| bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
		}
		for (i = 0; i < 8; i++) {
			mask = 0x80 >>> i;
			for (j = 1; j <= 31; j += 4) {
				bb[j / 4] = ((b[j] & mask) != 0 || (b[j + 2] & mask) != 0) ? 1
						: 0;
			}
			bitmaps[i + 8] = bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4
					| bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
		}
	}

	private void getBitmap(String c, int[] upperBits, int[] lowerBits) {
		byte[] b = new byte[18]; // bitmap bytes read from font file
		int b1 = c.charAt(0);
		int b2 = c.charAt(1);
		int offset;

		if (b1 <= 0xA9)
			offset = ((b1 - 0xA1) * (0xFE - 0xA0) + (b2 - 0xA1)) * 18;
		else
			offset = (9 * (0xFE - 0xA0) + (b1 - 0xB0) * (0xFE - 0xA0) + (b2 - 0xA1)) * 18;
		try {
			fontFile.seek(offset);
			fontFile.read(b);
		} catch (IOException e) {
			e.printStackTrace(System.err);
			System.exit(2);
		}
		int[] bb = new int[8];
		int i, j, mask;
		for (i = 0; i < 8; i++) {
			mask = 0x80 >>> i;
			for (j = 0; j <= 14; j += 2) {
				bb[j / 2] = ((b[j] & mask) != 0) ? 1 : 0;
			}
			upperBits[i] = bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4
					| bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
			lowerBits[i] = ((b[16] & mask) != 0) ? 0x80 : 0x00;
		}
		for (i = 0; i < 8; i++) {
			mask = 0x80 >>> i;
			for (j = 1; j <= 15; j += 2) {
				bb[j / 2] = ((b[j] & mask) != 0) ? 1 : 0;
			}
			upperBits[i + 8] = bb[0] << 7 | bb[1] << 6 | bb[2] << 5
					| bb[3] << 4 | bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
			lowerBits[i + 8] = ((b[17] & mask) != 0) ? 0x80 : 0x00;
		}
	}

	private void printChineseCharacter(String c) {
		int[] upperBits = new int[16];
		int[] lowerBits = new int[16];

		getBitmap(c, upperBits, lowerBits);
		try {
			// define user-defined character
			out.write(0x1B);
			out.write('&');
			out.write(0x02); // 2-byte in vertical direction
			out.write('A'); // define 'A' to 'B'
			out.write('B');
			for (int i = 0; i < 2; i++) {
				out.write(0x08); // number of bytes in horizontal direction
				for (int j = 0; j < 8; j++) {
					out.write(upperBits[8 * i + j]);
					out.write(lowerBits[8 * i + j]);
				}
			}
			out.write(0x1B);
			out.write('%');
			out.write(0x01);
			out.write('A');
			out.write('B');
			out.write(0x1B);
			out.write('?');
			out.write('A');
			out.write(0x1B);
			out.write('?');
			out.write('B');
			out.write(0x1B);
			out.write('%');
			out.write(0x00);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Print by bitmap command. With automatically adding an line-feed at end.
	 */
	public void printImmediateWithLineFeed(String str) {
		Arrays.fill(bitmaps, 0);
		Iterator iter = new CharIterator(str);
		while (iter.hasNext()) {
			String c = (String) iter.next();
			if (isChineseChar(c)) {
				getBitmap(c); // into upperBits
				printBitmap(bitmaps);
			} else {
				printAlphanumeric((byte) c.charAt(0));

			}
		}

	}

	private synchronized Image getImageFromFile(String fileName_)
			throws JposException {
		Image image = null;
		java.awt.MediaTracker tracker = null;
		try {
			if (fileName_.toLowerCase().endsWith(".bmp") == true)
				image = BMPImageProcessor.getImageFromBMPFile(fileName_);
			else
				image = new ImageIcon(fileName_).getImage();
		} catch (Exception e) {
			throw new JposException(109);
		}
		if (image.getWidth(null) <= 0)
			throw new JposException(109);
		return image;
	}

	protected boolean convertColor2Mono(int pixel) {
		int red = pixel >> 16 & 0xff;
		int green = pixel >> 8 & 0xff;
		int blue = pixel & 0xff;
		boolean rtn = false;
		if (red * 30 + green * 59 + blue * 11 < 12800)
			rtn = true;
		return rtn;
	}

    public void clearPrintArea() throws JposException {
    }

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapConcurrentPageMode() throws JposException {
        return false;
    }

    public boolean getCapRecPageMode() throws JposException {
        return false;
    }

    public boolean getCapSlpPageMode() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public String getPageModeArea() throws JposException {
        return null;
    }

    public int getPageModeDescriptor() throws JposException {
        return 0;
    }

    public int getPageModeHorizontalPosition() throws JposException {
        return 0;
    }

    public String getPageModePrintArea() throws JposException {
        return null;
    }

    public int getPageModePrintDirection() throws JposException {
        return 0;
    }

    public int getPageModeStation() throws JposException {
        return 0;
    }

    public int getPageModeVerticalPosition() throws JposException {
        return 0;
    }

    public void pageModePrint(int control) throws JposException {
    }

    public void setPageModeHorizontalPosition(int position) throws JposException {
    }

    public void setPageModePrintArea(String area) throws JposException {
    }

    public void setPageModePrintDirection(int direction) throws JposException {
    }

    public void setPageModeStation(int station) throws JposException {
    }

    public void setPageModeVerticalPosition(int position) throws JposException {
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }

    public boolean getCapMapCharacterSet() throws JposException {
        return false;
    }

    public boolean getMapCharacterSet() throws JposException {
        return false;
    }

    public String getRecBitmapRotationList() throws JposException {
        return null;
    }

    public String getSlpBitmapRotationList() throws JposException {
        return null;
    }

    public void setMapCharacterSet(boolean mapCharacterSet) throws JposException {
    }

    public void changePrintSide(int side) throws JposException {
    }

    public int getCapJrnCartridgeSensor() throws JposException {
        return 0;
    }

    public int getCapJrnColor() throws JposException {
        return 0;
    }

    public int getCapRecCartridgeSensor() throws JposException {
        return 0;
    }

    public int getCapRecColor() throws JposException {
        return 0;
    }

    public int getCapRecMarkFeed() throws JposException {
        return 0;
    }

    public boolean getCapSlpBothSidesPrint() throws JposException {
        return false;
    }

    public int getCapSlpCartridgeSensor() throws JposException {
        return 0;
    }

    public int getCapSlpColor() throws JposException {
        return 0;
    }

    public int getCartridgeNotify() throws JposException {
        return 0;
    }

    public int getJrnCartridgeState() throws JposException {
        return 0;
    }

    public int getJrnCurrentCartridge() throws JposException {
        return 0;
    }

    public int getRecCartridgeState() throws JposException {
        return 0;
    }

    public int getRecCurrentCartridge() throws JposException {
        return 0;
    }

    public int getSlpCartridgeState() throws JposException {
        return 0;
    }

    public int getSlpCurrentCartridge() throws JposException {
        return 0;
    }

    public int getSlpPrintSide() throws JposException {
        return 0;
    }

    public void markFeed(int type) throws JposException {
    }

    public void setCartridgeNotify(int notify) throws JposException {
    }

    public void setJrnCurrentCartridge(int cartridge) throws JposException {
    }

    public void setRecCurrentCartridge(int cartridge) throws JposException {
    }

    public void setSlpCurrentCartridge(int cartridge) throws JposException {
    }
}
