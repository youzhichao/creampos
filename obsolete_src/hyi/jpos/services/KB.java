
// Copyright (c) 2000 hyi
package hyi.jpos.services;

import java.util.*;
import hyi.cream.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class KB{

    private static ArrayList kbList = new ArrayList();
    private int keyCode = 0;
    private static HashMap fkeys = new HashMap();

	static KB kb = null;
    public static KB getInstance() {
        try {
            if (kb == null) {
                kb = new KB();
            }
        } catch (InstantiationException ex) {
            CreamToolkit.logMessage("" + ex.toString());
            CreamToolkit.logMessage("" + "Instantiation at " + KB.class);
        }
        return kb;
    }

    /**
     * Constructor
     */
    public KB() throws InstantiationException { 

        fkeys.put("279149126", "36000");
        fkeys.put("279151126", "127000");
        fkeys.put("279152126", "35000");
        fkeys.put("279153126", "33000");
        fkeys.put("279154126", "34000");
        //  F1 -- F6
        fkeys.put("27919165", "1000000");
        fkeys.put("27919166", "2000000");
        fkeys.put("27919167", "3000000");
        fkeys.put("27919168", "4000000");
        fkeys.put("27919169", "5000000");
        fkeys.put("27914955126", "6000000");
         
        Thread t = new Thread(new Runnable() {
            public void run() {
                int keyCode = 0;
                boolean fk = false;
                String fkstring = "";
                while (true) {
                    char ch = KBRead.read();     
                    keyCode = (int)ch;
                    //System.out.println("##### read key = " + keyCode + " #####");
                    if (!fk) {
                        if (keyCode == 27) {
                            fk = true;
                            fkstring = fkstring + keyCode;
                        } else {
                            KB.getInstance().fireEvent(keyCode);
                            //System.out.println("##### fire event " + keyCode + " #####");
                        }
                    } else {
                        fkstring = fkstring + keyCode;
                        if (keyCode == 65 || keyCode == 66 || keyCode == 67 || keyCode == 68 || keyCode == 69 || keyCode == 126) {
                            fk = false;
                            keyCode = Integer.parseInt((String)fkeys.get(fkstring));
                            KB.getInstance().fireEvent(keyCode);
                            fkstring = "";
                        }
                    }
                }
            }
        });
        t.start();
    }

    public static HashMap getFunctionKeys() {
        return fkeys;
    }

    public static void addList(KBListener k) {
        kbList.add(k);
    }

    public static void removeListener(KBListener k) {
        kbList.remove(k);
    }

    private void fireEvent(final int keyCode) {
        /*class Fire implements Runnable {
            public void run() {
                for (int i = 0; i < KB.this.kbList.size(); i++) {
                    ((KBListener)KB.this.kbList.get(i)).receiveKeyEvent(keyCode);
                }
            }
        }
        Fire fire = new Fire();
        Thread t = new Thread(fire);
        t.start();

        this.keyCode = keyCode;*/

        for (int i = 0; i < kbList.size(); i++) {
            ((KBListener)kbList.get(i)).receiveKeyEvent(keyCode);
            //System.out.println("##### fire event to " + kbList.get(i) + " #####");
        }
    }
}
