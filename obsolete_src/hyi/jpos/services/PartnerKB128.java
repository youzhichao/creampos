
// Copyright (c) 2000 HYI
package hyi.jpos.services;

import hyi.cream.POSTerminalApplication;
import hyi.cream.state.GetProperty;
import hyi.cream.uibeans.ItemList;
import hyi.cream.uibeans.PayingPaneBanner;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.uibeans.SignOffForm;
import hyi.cream.uibeans.touch.TouchPane;
import hyi.cream.util.CreamToolkit;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import jpos.JposConst;
import jpos.JposException;
import jpos.POSKeyboard;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.events.DataEvent;
import jpos.services.EventCallbacks;
import jpos.services.POSKeyboardService14;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PartnerKB128 implements POSKeyboardService14, KeyListener, ContainerListener {

    static private POSKeyboard claimedControl;
    static private Object mutex = new Object();

    protected EventCallbacks eventCallbacks      = null;
    protected JposEntry entry                    = null;
    protected Container comp                     = null;
    protected Container comp2                    = null;
    protected Container comp3                    = null;
    protected Container comp4                    = null;

    private boolean capDescriptors               = false;
    private boolean claimed                      = false;
    private int deviceDescriptors                = 0;
    private boolean deviceEnabled                = false;

    protected int posKeyData                     = 0;
    protected String checkHealthText             = "";
    protected String deviceServiceDescription    = "JavaPOS Service from HYI Corporation";
    protected int deviceServiceVersion           = 1005000;
    protected boolean freezeEvents               = false;
    protected String physicalDeviceDescription   = "";
    protected String physicalDeviceName          = "";
    protected int state                          = JposConst.JPOS_S_CLOSED;
    protected int powerNotify                    = JposConst.JPOS_PN_DISABLED;
    protected int powerState                     = JposConst.JPOS_PS_UNKNOWN;

    private int prompt;
    private Map keyMap = new HashMap();
    private int scannerPrefixCharValue;


    /**
     * Constructor
     */
    public PartnerKB128(SimpleEntry entry) {
        this.entry = entry;
    }

    // Capabilities
    public boolean getCapKeyUp() throws JposException {
        return true;
    }

    public int getCapPowerReporting() throws JposException {
        return 0;
    }

    // Properties
    public boolean getAutoDisable() throws JposException {
        return true;
    }

    public void setAutoDisable(boolean autoDisable) throws JposException {
    }

    public int getDataCount() throws JposException {
        return 0;
    }

    public boolean getDataEventEnabled() throws JposException {
        return true;
    }

    public void setDataEventEnabled(boolean dataEventEnabled)
                       throws JposException {
    }

    public int getEventTypes() throws JposException {
        return 0;
    }

    public void setEventTypes(int eventTypes) throws JposException {
    }

    public int getPOSKeyData() throws JposException {
        return posKeyData;
    }

    public int getPOSKeyEventType() throws JposException {
        return 0;
    }

    public int getPowerNotify() throws JposException {
        return 0;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
    }

    public int getPowerState() throws JposException {
        return 0;
    }

    public String getCheckHealthText() throws JposException {
        return "";
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    private boolean isActive = false;
    private boolean fkey = false;
    private String fkeyString = "";
    private HashMap fkeys = new HashMap();

    public void setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        if (!claimed) {
            System.out.println("must claim it first");
            return;
        }
        this.deviceEnabled = deviceEnabled;
        if (deviceEnabled) {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                adkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                adkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                adkl(comp3);
            }
            if (entry.hasPropertyWithName("COMP4_CLASS_PROP_NAME")) {
                String className4 = (String)entry.getPropertyValue("COMP4_CLASS_PROP_NAME");
                comp4 = createComp(className4);
                adkl(comp4);
            }
        } else {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                rmkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                rmkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                rmkl(comp3);
            }
            if (entry.hasPropertyWithName("COMP4_CLASS_PROP_NAME")) {
                String className4 = (String)entry.getPropertyValue("COMP4_CLASS_PROP_NAME");
                comp4 = createComp(className4);
                rmkl(comp4);
            }
        }
    }

    public String getDeviceServiceDescription() throws JposException {
        return "";
    }

    public int getDeviceServiceVersion() throws JposException {
        return 0;
    }

    public boolean getFreezeEvents() throws JposException {
        return true;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return "";
    }

    public String getPhysicalDeviceName() throws JposException {
        return "";
    }

    public int getState() throws JposException {
        return 0;
    }

    // Methods
    public void clearInput() throws JposException {
    }

    /**
     * Indicate the ServiceInstance that its connection has been dropped
     * @since 0.1 (Philly 99 meeting)
     * @throws jpos.JposException if any error occurs
     */
    public void deleteInstance() throws JposException {
    }

    //--------------------------------------------------------------------------
    // Common overridden methods
    //

    public void open(String logicalName, EventCallbacks eventCallbacks)
                    throws JposException {
        this.eventCallbacks = eventCallbacks;
        claimed = false;
        deviceEnabled = false;

        scannerPrefixCharValue = Integer.parseInt(
            GetProperty.getScannerPrefixCharValue("32"));
    }

    public void close() throws JposException {
        claimed = false;
        deviceEnabled = false;
        claimedControl = null;
    }

    public void claim(int timeout) throws JposException {
        if (claimed) {
            System.out.println("device has been claimed");
            return;
        }

        synchronized (mutex) {
            if (claimedControl == null) {
                Object obj = eventCallbacks.getEventSource();
                claimedControl = (POSKeyboard)obj;
                claimed = true;
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                    claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                    claimed = true;
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        if (claimedControl == null) {
                            claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                            claimed = true;
                            return;
                        }
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        }
        claimed = true;
    }

    public void checkHealth(int level) throws JposException {
    }

    public void directIO(int command, int[] data, Object object)
                       throws JposException {
    }

    public void release() throws JposException {
        synchronized (mutex) {
            if (!claimed) {
                System.out.println("device has been release");
            }
            claimed = false;
            deviceEnabled = false;
            claimedControl = null;
            mutex.notifyAll();
        }
    }



    //event
    public void keyReleased(KeyEvent e) {
    }

    private boolean checked = false;
    private boolean twoCode = false;
    private int keyCode = 0;
    private String buttonCode = "";
    private boolean keylock = false;
    private boolean scanner = false;
    private int enterButtonCount = 0;
    boolean msrState;
    boolean msr;
    public void keyPressed(KeyEvent e) {
//        System.out.println("keyPressed   *** KeyCode = " + e.getKeyCode() + " ***");
        PopupMenuPane pop = POSTerminalApplication.getInstance().getPopupMenuPane();
        TouchPane touch = TouchPane.getInstance();
        SignOffForm signOffForm = POSTerminalApplication.getInstance().getSignOffForm();
        ItemList itemList = POSTerminalApplication.getInstance().getItemList();
        PayingPaneBanner payingPane = POSTerminalApplication.getInstance().getPayingPane();
        char keyChar = e.getKeyChar();
        posKeyData = (int)keyChar;
        //if (check) {
//            System.out.println("*** " + keyChar + " = " + posKeyData + " ***");
        //    //System.out.println(e.getComponent());
        //    //return;
        //}

        if (keyChar == ':') {
            msrState = false;
        }

        //msr
        if (keyChar == ';' || keyChar == '%') {
            msr = true;
            return;
        }
        if (msr && posKeyData == 10) {
            msr = false;
            return;
        }

        //scanner
        //if (keyChar == ' ') {
        //System.out.println("PartnerKey> get " + (int)keyChar + "scannerPrefixCharValue=" + scannerPrefixCharValue);
        if ((int)keyChar == scannerPrefixCharValue) {
            scanner = true;
            return;
        }
        if (scanner && posKeyData == 10) {
            scanner = false;
            return;
        }

        if (!msr && !scanner) {
            //  check password dialog if show
//            if (pd.isVisible()) {
//                if (pd.keyListener(prompt, keyChar)) {
//                    return;
//                }
//            }

            //popup menu's selection
            if (signOffForm != null && signOffForm.isVisible()) {
                signOffForm.keyDataListener(posKeyData);
                return;
            } else if (pop.isVisible()) {
                if (pop.keyDataListener(keyChar)) {
                    return;
                }
            } else if (touch.isVisible()) {
                if (touch.keyDataListener(keyChar)) {
                    return;
                }
            } else if (itemList.isVisible()) {
                itemList.keyDataListener(posKeyData);
            } else if (payingPane.isVisible()) {
                payingPane.keyDataListener(posKeyData);
            }

//            System.out.println("keyPressed(): fire DataEvent code=" + posKeyData);
            eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
        }
    }

    synchronized public void keyTyped(KeyEvent e) {
//        System.out.println("keyTyped   *** KeyCode = " + e.getKeyCode() + " ***");
    }

    public Container createComp(String className) {
        try {
            Class compClass = Class.forName(className);
            Method compMethod = compClass.getDeclaredMethod("getInstance",
                                                            new Class[0]);
            comp = (Container)(compMethod.invoke(null, new Object[0]));
        } catch (ClassNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Class is not found at " + this);
        } catch (IllegalAccessException ex) {
            CreamToolkit.logMessage(ex.toString());
            CreamToolkit.logMessage("Illegal access exception at " + this);
        } catch (NoSuchMethodException exc) {
            CreamToolkit.logMessage(exc.toString());
            CreamToolkit.logMessage("No such method at " + this);
        } catch (InvocationTargetException exce) {
            CreamToolkit.logMessage(exce.toString());
            CreamToolkit.logMessage("Invocation exception at " + this);
        }
        return comp;
    }

    public void adkl(Container comp) {
        comp.addKeyListener(this);
        comp.addContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                adkl(compo);
            } else {
                co.addKeyListener(this);
            }
        }
    }

    public void rmkl(Container comp) {
        comp.removeKeyListener(this);
        comp.removeContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                rmkl(compo);
            } else {
                co.removeKeyListener(this);
            }
        }
    }

    public void componentAdded(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            adkl((Container)e.getChild());
        } else {
            e.getChild().addKeyListener(this);
        }
    }

    public void componentRemoved(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            rmkl((Container)e.getChild());
        } else {
            e.getChild().removeKeyListener(this);
        }
    }

    private int keyMode = 0;
    private int count1 = 0;
    private int count2 = 0;
    private int proKeyCode = 0;
}

/*class Th implements Runnable {
    public void run() {
        char ch = ' ';
        ch = Passc.pr();
        System.out.println("######## finished ###########");
    }
}

class Passc {

    static {
        System.loadLibrary("test");
    }

    public native static char pr();
}*/
