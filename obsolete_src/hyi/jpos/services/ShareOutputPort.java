package hyi.jpos.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.TooManyListenersException;

import javax.comm.CommPortIdentifier;
import javax.comm.NoSuchPortException;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.SerialPortEventListener;
import javax.comm.UnsupportedCommOperationException;

import jpos.JposConst;
import jpos.JposException;

public class ShareOutputPort {// Singleton class to implement the function of sharing one
    // serial port between the printer and cash drawer
    private static ShareOutputPort instance = new ShareOutputPort();
    private static OutputStreamWriter dataOutput = null;
    private static InputStreamReader dataInput = null;

    /** input & output Stream attached to POSPrinter */
    private static OutputStream outputStream = null;
    private static InputStream inputStream = null;

    private static SerialPortEventListener currentListener = null;
    /** serial port current feedback status got asyncronously */
    //
    // --------------------------------------------------------------------------
    /** serialport related variables, used in open() */
    // private Enumeration portList = null;
    private CommPortIdentifier portId = null;
    private SerialPort serialPort = null;
    private String statusCode = null;
    private int drawerStatus = 0;

    // --------------------------------------------------------------------------
    // private String eventSource;
    // private SerialPortEventListener source;
    // --------------------------------------------------------------------------

    public Object waitObject = new Object();

    private ShareOutputPort() {
        statusCode = "";
    }

    public static ShareOutputPort getInstance() {
        return instance;
    }

    // Status code received from the printer
    public void setStatusCode(String code) {
        statusCode = code;
    }

    public String getStatusCode() {
        return statusCode;
    }

    // Cashdrawer status code received from the printer
    public void setDrawerStatus(int s) {
        drawerStatus = s;
    }

    public int getDrawerStatus() {
        return drawerStatus;
    }

    synchronized public SerialPortEventListener getCurrentListener() {
        return currentListener;
    }

    // Properties related method;
    public OutputStreamWriter getOutputStreamWriter() {
        return dataOutput;
    }

    public InputStreamReader getInputStreamReader() {
        return dataInput;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getOwnerName() {
        return portId.getCurrentOwner();
    }

    public void open(String logicalName, String portName, int timeout,
        SerialPortEventListener listener) throws JposException {
        try {
            portId = CommPortIdentifier.getPortIdentifier(portName);
            serialPort = (SerialPort)portId.open(logicalName, timeout);
            // get the output handler from which you can output a byte
            outputStream = serialPort.getOutputStream();
            dataOutput = new OutputStreamWriter(outputStream, "Big5");
            // get the input handler from which you can get status
            inputStream = serialPort.getInputStream();
            dataInput = new InputStreamReader(inputStream, "Big5");
            // set the port parameter, this can get from the entry properties
            // add SerialEventListener, SerialPort.open() extends the addEventListener
            serialPort.addEventListener(listener);
            ShareOutputPort.currentListener = listener;
//            serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
//                SerialPort.PARITY_NONE);
            //serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_XONXOFF_IN);
            //serialPort.enableReceiveFraming(1);
            serialPort.notifyOnDataAvailable(true);
            serialPort.notifyOnOutputEmpty(true);
            serialPort.notifyOnBreakInterrupt(true);
            serialPort.notifyOnCarrierDetect(true);
            serialPort.notifyOnCTS(true);
            serialPort.notifyOnDSR(true);
            serialPort.notifyOnFramingError(true);
            serialPort.notifyOnOverrunError(true);
            serialPort.notifyOnParityError(true);
            serialPort.notifyOnRingIndicator(true);
        } catch (NoSuchPortException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, portName
                + " not found on this machine.", e);
        } catch (PortInUseException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL,
                portName + " in use on this machine.", e);
        } catch (IOException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, e.getMessage(), e);
//        } catch (UnsupportedCommOperationException e) {
//            e.printStackTrace();
            // by-pass UnsupportedCommOperationException
        } catch (TooManyListenersException e5) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, e5.getMessage(), e5);
        }
    }

    synchronized public boolean isSerialPortOpened() {
        if (serialPort == null)
            return false;
        else
            return true;
    }

    public void close() throws JposException {
        outputStream = null;
        inputStream = null;
        serialPort.close();
        serialPort = null;
        portId = null;
        // portList = null;
    }
}