package hyi.jpos.services;

import hyi.cream.util.CreamToolkit;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.TooManyListenersException;

import javax.comm.CommPortIdentifier;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.SerialPortEvent;
import javax.comm.SerialPortEventListener;
import javax.comm.UnsupportedCommOperationException;

import jpos.JposConst;
import jpos.JposException;
import jpos.ScannerConst;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.events.DataEvent;
import jpos.events.ErrorEvent;
import jpos.services.EventCallbacks;
import jpos.services.ScannerService14;

public class SerialPortScanner extends DeviceService implements ScannerService14, SerialPortEventListener {
    //private int scannerPrefixCharValue;
    //static private SerialPortScanner claimedControl;
    //private static String upca = "";
    //private static String upce = "";
    //private static String ean13 = "";
    //private static String ean8 = "";
    //private static String code39 = "";

    private JposEntry scannerEntry = null;
    private boolean decodeData = false;
    private byte[] scanData = new byte[0];
    private byte[] scanDataLabel = new byte[0];
    private int scanDataType = ScannerConst.SCAN_SDT_UNKNOWN;
    private boolean autoDisable = false;
    private boolean dataEventEnabled = false;
    private int dataCount = 0;
    //private ArrayList stringBuffered = new ArrayList();
    private ArrayList dataEvents = new ArrayList();
    private SerialPort serialPort = null;
    private InputStream inputStream = null;
    private StringBuffer buffer = new StringBuffer();
    private boolean DEBUG;
    private String devicePortName;
    private int baudRate;
    private int dataBits;
    private int stopBits;
    private int parity;

    public SerialPortScanner(SimpleEntry entry) {
        scannerEntry = entry;
        loadParametersFromJposEntry();
    }

    public void loadParametersFromJposEntry() {
        if (scannerEntry != null) {
            devicePortName = (String)scannerEntry.getPropertyValue("DevicePortName");
            if (DEBUG)
                System.out.println("SerialPortScanner(): Read DevicePortName=" + devicePortName);
            if (devicePortName == null)
                devicePortName = "COM2";

            Integer baudRateObj = (Integer)scannerEntry.getPropertyValue("BaudRate");
            if (DEBUG)
                System.out.println("SerialPortScanner(): Read BaudRate=" + baudRateObj);
            baudRate = (baudRateObj == null) ? 9600 : baudRateObj.intValue();  

            Integer dataBitsObj = (Integer)scannerEntry.getPropertyValue("DataBits");
            if (DEBUG)
                System.out.println("SerialPortScanner(): Read DataBits=" + dataBitsObj);
            dataBits = (dataBitsObj == null) ? 8 : dataBitsObj.intValue();
            
            Integer stopBitsObj = (Integer)scannerEntry.getPropertyValue("StopBits");
            if (DEBUG)
                System.out.println("SerialPortScanner(): Read StopBits=" + stopBitsObj);
            stopBits = (stopBitsObj == null) ? 8 : stopBitsObj.intValue();

            Integer parityObj = (Integer)scannerEntry.getPropertyValue("Parity");
            if (DEBUG)
                System.out.println("SerialPortScanner(): Read Parity=" + parityObj);
            parity = (parityObj == null) ? 8 : parityObj.intValue();
        }
    }

    public boolean getAutoDisable() throws JposException {
        return autoDisable;
    }
    public void setAutoDisable(boolean autoDisable) throws JposException {
        this.autoDisable = autoDisable;
    }

    public int getDataCount() throws JposException {
        return dataCount;
    }

    public boolean getDataEventEnabled() throws JposException {
        return dataEventEnabled; 
    }
    public void setDataEventEnabled(boolean dataEventEnabled)
                       throws JposException {
        this.dataEventEnabled = dataEventEnabled;
    }                      

    public void setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        if (!claimed) {
            if (DEBUG)
                System.out.println("must claim it first");
            return;
        }
        this.deviceEnabled = deviceEnabled;
    }

    public boolean getDecodeData() throws JposException {
        return decodeData;
    }

    public void setDecodeData(boolean decodeData) throws JposException {
        this.decodeData = decodeData;
    }

    public byte[] getScanData() throws JposException {
        return scanData;
    }

    public byte[] getScanDataLabel() throws JposException {
        return scanDataLabel;
    }
    
    //When DecodeData is false, this property is set to SCAN_SDT_UNKNOWN.
    public int getScanDataType() throws JposException {
        return scanDataType;
    }

    //Properties 13
    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        throw new JposException (JposConst.JPOS_E_NOSERVICE, "No service available now!");
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }
    //end of properties definition

    //Common method -- Not supported
    public void checkHealth(int level)  throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    //Common methods -- Supported
    public void clearInput() throws JposException {
         if (dataEvents != null)
             dataEvents.clear();
         dataCount = 0;
         if (buffer != null)
             buffer.setLength(0);
    }
    //
    public void open(String logicalName, EventCallbacks cb) throws JposException {
        if (DEBUG)
            System.out.println("SerialPortScanner.open()");
        super.open(logicalName, cb);
        this.eventCallbacks = cb; //eventCallbacks;
        claimed = false;
        deviceEnabled = false;
        openComPort(logicalName);
    }

    private void openComPort(String logicalName) throws JposException {
        if (DEBUG)
            System.out.println("SerialPortScanner.openComPort()> open port=" + devicePortName);
        Enumeration portList = CommPortIdentifier.getPortIdentifiers();
        boolean foundPort = false;
        CommPortIdentifier portId = null;
        while (portList.hasMoreElements()) {
            portId = (CommPortIdentifier)portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL &&
                portId.getName().equals(devicePortName)) {
                    foundPort = true;
                    break;
            }
        }
        if (!foundPort)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Serial port " + devicePortName + " is not available!");

        try {
            serialPort = (SerialPort)portId.open(logicalName, 15000);
            //outputStream = serialPort.getOutputStream();
            inputStream = serialPort.getInputStream();
            //dataOutput = new OutputStreamWriter(outputStream,  CreamToolkit.getEncoding());
            //dataInput  = new InputStreamReader(inputStream,  CreamToolkit.getEncoding());

            serialPort.setSerialPortParams(baudRate, dataBits, stopBits, parity);
            //serialPort.setSerialPortParams(9600, //19200,
            //    SerialPort.DATABITS_8,
            //    SerialPort.STOPBITS_1,
            //    SerialPort.PARITY_NONE);
            //serialPort.setFlowControlMode(SerialPort. FLOWCONTROL_XONXOFF_IN);
            serialPort.notifyOnDataAvailable(true);
            serialPort.notifyOnOutputEmpty(true);
            serialPort.notifyOnBreakInterrupt(true);
            serialPort.notifyOnCarrierDetect(true);
            serialPort.notifyOnCTS(true);
            serialPort.notifyOnDSR(true);
            serialPort.notifyOnFramingError(true);
            serialPort.notifyOnOverrunError(true);
            serialPort.notifyOnParityError(true);
            serialPort.notifyOnRingIndicator(true);
            serialPort.addEventListener(this);
        } catch (PortInUseException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, e.toString(), e);
        } catch (IOException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, e.toString(), e);
        } catch (UnsupportedCommOperationException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, e.toString(), e);
        } catch (TooManyListenersException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, e.toString(), e);
        }
    }

    public void close() throws JposException  {
        claimed = false;
        deviceEnabled = false;
    }              

    public void claim(int timeout) throws JposException {
        claimed = true;
    }

    public void deleteInstance() throws JposException {
    }

    public void serialEvent(SerialPortEvent event) {
        if (DEBUG)  
            System.out.println("SerialPortScanner.serialEvent()> EventType=" + event.getEventType());
        switch (event.getEventType()) {
        case SerialPortEvent.BI:
        case SerialPortEvent.OE: // for errorEventListener callback
        case SerialPortEvent.FE: // need defined altogether with other devices */
        case SerialPortEvent.PE:
            /*
             JPOS_ER_RETRY /Retry the asynchronous output. The error state is exited. The default.
             JPOS_ER_CLEAR /Clear the asynchronous output or buffered input data. The error state is exited.
             */
            ErrorEvent error = new ErrorEvent(eventCallbacks.getEventSource(),
                0, 0, JposConst.JPOS_EL_OUTPUT, JposConst.JPOS_ER_CLEAR);
            eventCallbacks.fireErrorEvent(error);
            state = JposConst.JPOS_S_ERROR;
            break;
        case SerialPortEvent.CD:
            break;
        case SerialPortEvent.CTS:
            break;
        case SerialPortEvent.DSR:
            break;
        case SerialPortEvent.RI:
            break;
        case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
            if (DEBUG)
                System.out.println("SerialPortScanner.serialEvent()> OUTPUT_BUFFER_EMPTY");
            break;
        case SerialPortEvent.DATA_AVAILABLE: //data-inputing
            if (DEBUG)
                System.out.println("SerialPortScanner.serialEvent()> DATA_AVAILABLE:");
            try {
                while (true) {
                    int numBytes;
                    if (DEBUG)
                        System.out.print("   inputStream.available() = ");
                    numBytes = inputStream.available();
                    if (DEBUG)
                        System.out.println("" + numBytes);
                    if (numBytes <= 0)
                        break;
                    char x = (char)inputStream.read();
                    if (DEBUG)
                        System.out.print(" " +(int)x);
                    if (x == '\n') {
                        scanDataType = ScannerConst.SCAN_SDT_EAN13;
                        scanDataLabel = buffer.toString().getBytes(CreamToolkit.getEncoding());
                        eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
                        scanDataLabel = null;
                        buffer.setLength(0);
                        if (DEBUG)
                            System.out.println();
                    } else {
                        buffer.append(x);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            if (DEBUG)  
                System.out.println("SerialPortScanner.serialEvent()> Out of DATA_AVAILABLE");
            break;
        default:
            break;

        }
    }

}
