package hyi.jpos.util;

import java.util.*;

public abstract class StringParsing {//implements BaseParsing {

	public final static String[] JposESC = { "\u001B|rA", "\u001B|cA", "\u001B|1C", "\u001B|2C", "\u001B|3C",
                           "\u001B|4C", "\u001B|rC", "\u001B|rvC", "\u001B|iC", "\u001B|bC", "\u001B|N",
                           "\u001B|P", "\u001B|fP", "\u001B|sP", "\u001B|lF", "\u001B|sL", "\u001B|bL",
						   "\u001B|tL", "\u001B|uC", "\u001B|sC", "\u001B|V" };

    public abstract String getCommandStart ( );    
    public abstract String getCommandEnd ( );
    
    //Method to get ESC sequence from a string object;
    //A ESC sequence here means a string starts with "\u001B|" and terminates with
    //a uppercase alphabetic characters or any character before the next ESC sequence  
    public ArrayList getESCSequence ( String data ) {
        int begin = 0;
        int end = 0;
        ArrayList escape = new ArrayList ( );
        for (int i=0; i< data.length(); i++ ) {
            if ( data.substring(i).startsWith ( "\u001B|" ) ) {
                begin = i;
                end = 0;
                i += 2;
                for ( int j = i; j < data.length(); j++ ) {
                    if ( j < data.length()-2 ) {
                        if ( data.substring(j,j+2).equals("\u001B|")
                            || data.substring(j,j+1).equals("\n")
                            || data.substring(j,j+1).equals("\r") ) {
                            end = j - 1;
                            j = data.length();
                        } else if ( data.charAt(j) >= 'A' && data.charAt(j) <= 'Z' ) {
                            end = j;
                            j = data.length();
                        }
                    } else if ( j < data.length()-1 ) {
                        if ( data.substring(j,j+1).equals( "\n" )
                            || data.substring(j,j+1).equals( "\r" ) ) {
                            end = j - 1;
                            j = data.length();
                        } else if ( data.charAt(j)>='A' && data.charAt(j)<='Z' ) {
                            end = j;
                            j = data.length();
                        }
                    } else {
                        if ( data.charAt(j)>='A' && data.charAt(j)<='Z' ) {
                            end = j;
                            j = data.length();
                        }
                    }
                }
                if ( end == 0 ) {
                    escape.add ( data.substring ( begin ) );
                    i = data.length ( );
                } else {
                    escape.add ( data.substring ( begin, end + 1 ) );
                    i = end;
                }
            }
        }
               return escape;
    }

    public ArrayList getValidESCSequence ( String data ) {
        ArrayList escape = getESCSequence ( data );
        String element = null; 
        for ( int i = escape.size ( ) - 1; i > -1;  i-- ) {
            element = ( String ) escape.get ( i );
            if ( element.charAt ( element.length( ) - 1 ) < 'A'
                || element.charAt ( element.length( ) - 1 ) > 'Z' )
                escape.remove ( i );
        }
        return escape;
    }

    //The method will invoke getESCSequence firstly, then it parses the sequence,
    //remove the element that is not in accordance with JPOS specification.
    
    public ArrayList getJposESCSequence ( String data ) {
        ArrayList escape = getValidESCSequence ( data );
        String item = null;
        for ( int i = escape.size ( ) - 1; i > -1;  i-- ) {
            item = ( String ) escape.get ( i );
            if ( ! contains( JposESC, item ) ) {
                if ( item.charAt( item.length() - 1 ) == 'B'
                    || item.charAt( item.length() - 1 ) == 'P' ) {
                    if ( !isNumber ( item.substring ( 2, item.length() - 1 ) ) )
                        escape.remove ( i );
                } else if ( item.substring( item.length() - 2 ).equals( "uF" )
                    || item.substring( item.length() - 2 ).equals("rF")
                    || item.substring( item.length() - 2 ).equals("lF")
                    || item.substring( item.length() - 2 ).equals("fT")
                    || item.substring( item.length() - 2 ).equals("uC")
                    || item.substring( item.length() - 2 ).equals("sC")
                    || item.substring( item.length() - 2 ).equals("hC")
                    || item.substring( item.length() - 2 ).equals("vC")
                    || item.substring( item.length() - 2 ).equals("fP")
                    || item.substring( item.length() - 2 ).equals("sP") ) {
                    if ( !isNumber ( item.substring ( 2, item.length() - 2 ) ) )
                        escape.remove ( i );
                } else {
                    escape.remove ( i );
                }
            }
        }
        return escape;
    }
    
    public boolean contains ( String data[], String item ) {
        boolean contain = false;
        for ( int j = 0; j < data.length ; j++ ) 
            if ( item.equals ( data[ j ] ) ) {
                contain = true;
                j = data.length;
            }
        return contain; 
    }


    public boolean isNumber ( String data ) {
        boolean isnumber = true;
        String number = data;
        if ( data.length ( ) > 0 ) {
            if ( data.startsWith( "-" ) )
                number = data.substring ( 1 );
            for ( int i = 0; i < number.length(); i++ )
                if ( number.charAt ( i ) < '0' || number.charAt ( i ) > '9' ) {
                    isnumber = false;
                    i = data.length();
                }
        } else
            return false;
        return isnumber;
    }

   
    public abstract String getValidCommand ( int station, String data );

    public abstract String getValidCommand ( int station, String data1, String data2 );
}

