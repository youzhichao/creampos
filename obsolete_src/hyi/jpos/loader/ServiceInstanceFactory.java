
package hyi.jpos.loader;

import java.lang.reflect.*;

import jpos.*;
import jpos.loader.*;
import jpos.config.*;

/**
 * ServiceInstanceFactory uses reflection to create a service instance dynamically
 * @since 2000
 * @author slackware
 */
 
public final class ServiceInstanceFactory extends Object implements JposServiceInstanceFactory {

    /**
     * Default ctor 
    */
    
    public ServiceInstanceFactory() {}

    /**
     * Simply creates an instance of a service.
     * NOTE: future version will use reflection to create the service instance
     * @param logicalName the logical name for this entry
     * @param entry the JposEntry with properties for creating the service
     * @since 2000
     * @exception jpos.JposException in case the factory cannot create service or service throws exception
     */

    public JposServiceInstance createInstance(String logicalName, JposEntry entry) throws JposException {
        if (!entry.hasPropertyWithName(JposEntry.SERVICE_CLASS_PROP_NAME ))
            throw new JposException(JposConst.JPOS_E_NOSERVICE, "The JposEntry does not contain the 'ServiceClass' property!" );
        JposServiceInstance serviceInstance = null;
        try {
            String serviceClassName = (String)entry.getPropertyValue( JposEntry.SERVICE_CLASS_PROP_NAME );
            Class serviceClass = Class.forName( serviceClassName );
            Class[] params = new Class[ 1 ];
            params[0] = entry.getClass();
            Constructor ctor = serviceClass.getConstructor(params);
            Object[] arg = new Object[1];
            arg[0]= entry;
            serviceInstance = (JposServiceInstance)ctor.newInstance(arg);
        } catch( Exception e ){
            System.out.println(e);
            throw new JposException( JposConst.JPOS_E_NOSERVICE, "Could not create the service instance!", e );
        }
        return serviceInstance;
   }
}
 /* /** JposEntry property name defining the JposServiceInstanceFactory class for this JposEntry
    public static final String SI_FACTORY_CLASS_PROP_NAME = "serviceInstanceFactoryClass";

    /** JposEntry property name that must be defined by all JposEntry objects used to create services
    public static final String LOGICAL_NAME_PROP_NAME = "logicalName";
    */
