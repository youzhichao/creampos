TouchPOS按键配置程序 Ver 1.0.0

安装说明：

1. 安装JRE 1.5.x（至任意目录）。

2. 把CacaoPOSSettings/ 目录下的文件全部复制到任意安装目录。

   文件清单：
   +- readme.txt                 → 本文件
   +- POSSettings.exe            → 主程序执行文件
   +- POSSettings.exe.manifest   → WinXP L&F配置文件
   +- Ts_plubutton.sql           → 总部Ts_pluButton表脚本
   +- lib\
   |  +- swt-win32-3138.dll        → 程序库
   |  +- msbase.jar                → 程序库
   |  +- mssqlserver.jar           → 程序库
   |  +- msutil.jar                → 程序库
   |  +- mysql-connector-java-3.0.9-stable-bin.jar → 程序库
   |  +- org.eclipse.swt.win32.win32.x86_3.1.0.jar → 程序库
   + conf\
      +- namedquery_hq.xml     → 总部数据库以及查询命令配置
      +- namedquery_store.xml  → 门市数据库以及查询命令配置

3. 如果需要美观一点的Windows XP界面风格，在运行POSSettings.exe一次后，
   把POSSettings.exe.manifest文件复制到JRE所在目录下的launch4j-tmp子
   目录（%JRE_HOME%\launch4j-tmp\）下，这样就会有WinXP的Look&Feel。

使用说明：

1. 直接运行POSSettings.exe将启动总部端的按键配置程序，总部SQL Server
   需增加Ts_pluButton表，文件Ts_plubutton.sql是相关SQL脚本。如果连接
   数据有问题，可以修改配置文件conf\namedquery_hq.xml。

2. 运行POSSettings.exe时若带参数"-store"，则启动门市端的按键配置程序，
   缺省会读取MySQL数据库，若非使用MySQL数据库，可以修改配置文件
   conf\namedquery_store.xml。

Bruce You @ Hongyuan Software(Shanghai) 2006-4-14