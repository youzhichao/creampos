SET SRC_DIR=.\src
SET CLASS_FILE_DIR=.\out\production\CreamPOS_trunk

SET GWT_USR_JAR=C:\Applications\gwt-windows-1.5.1\gwt-user.jar
SET GWT_DEV_JAR=C:\Applications\gwt-windows-1.5.1\gwt-dev-windows.jar
SET GWTEXT_JAR=lib\gwtext.jar
SET GWTEXTUX_JAR=lib\gwtextux.jar

REM GWT_USR_JAR=~\Java\gwt-linux-1.5.3\gwt-user.jar
REM GWT_DEV_JAR=~\Java\gwt-linux-1.5.3\gwt-dev-linux.jar
REM SMARTGWT_JAR=lib\smartgwt.jar
REM SMARTGWT_SKINS_JAR=lib\smartgwt-skins.jar

SET CLASS_PATH=%SRC_DIR%;%CLASS_FILE_DIR%;%GWT_USR_JAR%;%GWT_DEV_JAR%;%GWTEXT_JAR%;%GWTEXTUX_JAR%
java -Xmx256M -classpath "%CLASS_PATH%" com.google.gwt.dev.GWTCompiler -out ".\www" "$@" hyi.cream.gwt.CreamGWT
