package crack;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import protector.Protector;

public class MainWindow extends Frame {
	MainWindow(String title) {
		super(title);
		Panel backPanel = new Panel();
		this.add(backPanel);
		backPanel.setLayout(new GridLayout(2, 2, 6, 6));
		Label machineIDLabel = new Label("Machine ID:", Label.RIGHT);
		Label licenseIDLabel = new Label("License ID:", Label.RIGHT);
		String machineID = Protector.getMachineUniqueID();
		final TextField machineIDTextField = new TextField(machineID);
		final TextField licenseIDTextField = new TextField(
				Protector.generateLicenseID(machineID));
		machineIDTextField.setFont(new Font("Serif", Font.PLAIN, 15));
		machineIDTextField.selectAll();
		licenseIDTextField.setFont(new Font("Serif", Font.PLAIN, 15));
		licenseIDTextField.setEditable(false);
		backPanel.add(machineIDLabel);
		backPanel.add(machineIDTextField);
		backPanel.add(licenseIDLabel);
		backPanel.add(licenseIDTextField);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = 370;
		int height = 90;
		setSize(width, height);
		setLocation((screenSize.width - width) / 2,
				(screenSize.height - height) / 2);
		machineIDTextField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				licenseIDTextField.setText(Protector.generateLicenseID(machineIDTextField
						.getText()));
			}
		});
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	public static void main(String[] args) {
		MainWindow frame = new MainWindow("泓远ePOS授权码生成器");
		frame.setVisible(true);
		// String m;
		// System.out.println("Machine ID = " + (m = getMachineUniqueID()));
		// System.out.println("License ID = " + generateLicenseID(m));
	}
}