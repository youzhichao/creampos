package hyi.spos

import hyi.cream.util.HYIDouble;

/**
 * Base class for Credit Authorization Terminal (CAT) device. Implemented by Groovy.
 * <p/>
 * 說明請參閱PDF文件 of "UnifiedPOS Retail Peripheral Architecture."<br/>
 * See: http://www.nrf-arts.org/UnifiedPOS
 *
 * @author Bruce You
 * @since 2008/8/29 上午 11:10:59
 */
abstract class CAT extends BaseControl {

    // Capabilities
    boolean getCapAdditionalSecurityInformation() { return false }
    boolean getCapAuthorizeCompletion()           { return false }
    boolean getCapAuthorizePreSales()             { return false }
    boolean getCapAuthorizeRefund()               { return false }
    boolean getCapAuthorizeVoid()                 { return false }
    boolean getCapAuthorizeVoidPreSales()         { return false }
    boolean getCapCenterResultCode()              { return false }
    boolean getCapCheckCard()                     { return false }
    int     getCapDailyLog()                      { return CATConst.CAT_DL_NONE }
    boolean getCapInstallments()                  { return false }
    boolean getCapPaymentDetail()                 { return false }
    boolean getCapTaxOthers()                     { return false }
    boolean getCapTransactionNumber()             { return false }
    boolean getCapTrainingMode()                  { return false }

    // Properties
    Date    transactionDateTime
    String  transactionNumber
    int     transactionType
    String  accountNumber
    String  expireDate
    String  terminalId
    String  additionalSecurityInformation
    String  approvalCode
    String  cardCompanyID
    String  centerResultCode
    String  dailyLog
    int     paymentCondition
    int     paymentMedia
    String  paymentDetail
    int     sequenceNumber
    String  slipNumber
    boolean trainingMode

    // Methods
    void accessDailyLog(int sequenceNumber, int type, int timeout)
        throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL)
    }

    void authorizeCompletion(int sequenceNumber, HYIDouble amount, long taxOthers,
        int timeout) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL)
    }

    void authorizePreSales(int sequenceNumber, HYIDouble amount,
        long taxOthers, int timeout) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL)
    }

    abstract void authorizeRefund(int sequenceNumber, HYIDouble amount, long taxOthers, int timeout)
        throws JposException

    abstract void authorizeSales(int sequenceNumber, HYIDouble amount, long taxOthers, int timeout)
        throws JposException

    abstract void authorizeVoid(int sequenceNumber, HYIDouble amount, long taxOthers, int timeout)
        throws JposException

    void authorizeVoidPreSales(int sequenceNumber, HYIDouble amount,
        long taxOthers, int timeout) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL)
    }

    void checkCard(int sequenceNumber, int timeout) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL)
    }
}
