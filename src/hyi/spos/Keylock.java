package hyi.spos;

import hyi.spos.events.*;
import hyi.cream.gwt.client.device.POSKeyboardData;
import hyi.cream.gwt.client.device.KeylockData;

/**
 * Keylock control base class.
 * <p/>
 * Modified from Dai's version.
 * 
 * @author Bruce You
 */
abstract public class Keylock extends BaseControl {

    public int keyPosition          = 1;
    private int positionCount        = 0;

    private final static int ks1                 = 112;
    private final static int ks2                 = 113;
    private final static int ks3                 = 114;
    private final static int ks4                 = 115;
    private final static int ks5                 = 116;
    private final static int ks6                 = 117;

    /**
     * Constructor
     */
    public Keylock() {
    }

    public int getKeyPosition() throws JposException {
        return keyPosition;
    }

    public String getKeyPositionName(int keyPosition) {
        switch (keyPosition) {
        case KeylockConst.LOCK_KP_LOCK:
            return "Lock";

        case KeylockConst.LOCK_KP_NORM:
            return "Sale";

        case KeylockConst.LOCK_KP_SUPR:
            return "Supr";

        default:
            return "Other";
        }
    }

    public void waitForKeylockChange(int position, int timeout)
        throws JposException {
    }

    /**
     * Get POS keylock detail information, used by GWT service.
     */
    public KeylockData getKeylockData() {
        return null;
    }

    /**
     * Fire the the keylock StatusUpdateEvent according to keylock position.
     */
    public void fireEvent(int keyPosition) {
        this.keyPosition = keyPosition;
        System.out.println("Keylock event sent from web console: fire keylock pos=" + keyPosition);
        fireStatusUpdateEvent(new StatusUpdateEvent(this));
    }
}
