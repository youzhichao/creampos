package hyi.spos;

import hyi.spos.events.*;
import java.util.*;

/**
 * Simple JavaPOS's base service class.
 */
abstract public class BaseControl {
                                 
    public String checkHealthText = ""; 
    public boolean claimed = true;     
    public String deviceControlDescription = ""; 
    public int deviceControlVersion = 0;    
    public boolean deviceEnabled = false;  
    public String deviceServiceDescription = "";  
    public int deviceServiceVersion = 0;    
    public boolean freezeEvents = false;  
    public String physicalDeviceDescription = ""; 
    public String physicalDeviceName = "";
    public String logicalDeviceName;
    public int state = 0;                    
    public Object property = null;

    public ArrayList statusUpdateListeners = new ArrayList(); 
    public ArrayList dataListeners = new ArrayList();      
    public ArrayList errorListeners = new ArrayList();

    // Properties
    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public void setClaimed(boolean claimed) throws JposException {
        this.claimed = claimed;
    }

    public boolean getDeviceEnabled() throws JposException{
        return deviceEnabled;
    }

    public void setDeviceEnabled(boolean deviceEnabled)
                       throws JposException{
        this.deviceEnabled = deviceEnabled;
    }

    public int powerState = 0;
    public int getPowerState() throws JposException {
        return powerState;
    }

    public void setPowerState(int powerState) throws JposException {
        this.powerState = powerState;
    }

    public boolean dataEventEnabled = false;
    public boolean getDataEventEnabled() throws JposException {
        return dataEventEnabled;
    }

    public void setDataEventEnabled(boolean dataEventEnabled) throws JposException {
        this.dataEventEnabled = dataEventEnabled;
    }

    public boolean getFreezeEvents() throws JposException{
        return freezeEvents;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        this.freezeEvents = freezeEvents;
    }

    public int getState() throws JposException {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setProperty(Object o) {
        this.property = o;
    }

    public Object getProperty() {
        return property;
    }

    // Methods
    abstract public void claim(int timeout) throws JposException;

    abstract public void close() throws JposException;

    public void directIO(int command, int[] data, Object object)
        throws JposException {
    }

    abstract public void open(String logicalName) throws JposException;

    abstract public void release() throws JposException;

    public void clearOutput() throws JposException {
    }

    public void clearInput() throws JposException {
    }

    public void addStatusUpdateListener(StatusUpdateListener o) {
        statusUpdateListeners.add(o);
    }

    public void removeStatusUpdateListener(StatusUpdateListener o) {
        statusUpdateListeners.remove(o);
    }

    public void fireStatusUpdateEvent(StatusUpdateEvent e) {
        for (int i = 0; i < statusUpdateListeners.size(); i++) {
            ((StatusUpdateListener)statusUpdateListeners.get(i)).statusUpdateOccurred(e);
        }
    }

    public void addDataListener(DataListener o) {
        dataListeners.add(o);
    }

    public void removeDataListener(DataListener o) {
        dataListeners.remove(o);
    }

    public void fireDataEvent(DataEvent e) {
        for (int i = 0; i < dataListeners.size(); i++) {
            ((DataListener)dataListeners.get(i)).dataOccurred(e);
        }
    }

    public void addErrorListener(ErrorListener o) {
        errorListeners.add(o);
    }

    public void removeErrorListener(ErrorListener o) {
        errorListeners.remove(o);
    }

    public void fireErrorEvent(ErrorEvent e) {
        for (int i = 0; i < errorListeners.size(); i++) {
            ((ErrorListener)errorListeners.get(i)).errorOccurred(e);
        }
    }

    public String getLogicalDeviceName() {
        return logicalDeviceName;
    }

    public void setLogicalDeviceName(String logicalDeviceName) {
        this.logicalDeviceName = logicalDeviceName;
    }
}
