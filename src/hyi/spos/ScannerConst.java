package hyi.spos;

public class ScannerConst {
    //###################################################################
    //#### Scanner Constants
    //###################################################################

    /////////////////////////////////////////////////////////////////////
    // "ScanDataType" Property Constants
    /////////////////////////////////////////////////////////////////////

    // One dimensional symbologies
    public static final int SCAN_SDT_UPCA     = 101;  // Digits
    public static final int SCAN_SDT_UPCE     = 102;  // Digits
    public static final int SCAN_SDT_JAN8     = 103;  // = EAN 8
    public static final int SCAN_SDT_EAN8     = 103;  // = JAN 8
    public static final int SCAN_SDT_JAN13    = 104;  // = EAN 13
    public static final int SCAN_SDT_EAN13    = 104;  // = JAN 13
    public static final int SCAN_SDT_TF       = 105;  // (Discrete 2 of 5)
                                                      //   Digits
    public static final int SCAN_SDT_ITF      = 106;  // (Interleaved 2 of 5)
                                                      //   Digits
    public static final int SCAN_SDT_Codabar  = 107;  // Digits, -, $, :, /, .,
                                                      //   +; 4 start/stop
                                                      //   characters (a, b, c,
                                                      //   d)
    public static final int SCAN_SDT_Code39   = 108;  // Alpha, Digits, Space,
                                                      //   -, ., $, /, +, %;
                                                      //   start/stop (*)
                                                      // Also has Full Ascii
                                                      //   feature
    public static final int SCAN_SDT_Code93   = 109;  // Same characters as
                                                      //   Code 39
    public static final int SCAN_SDT_Code128  = 110;  // 128 data characters
    public static final int SCAN_SDT_UPCA_S   = 111;  // UPC-A with
                                                      //   supplemental barcode
    public static final int SCAN_SDT_UPCE_S   = 112;  // UPC-E with
                                                      //   supplemental barcode
    public static final int SCAN_SDT_UPCD1    = 113;  // UPC-D1
    public static final int SCAN_SDT_UPCD2    = 114;  // UPC-D2
    public static final int SCAN_SDT_UPCD3    = 115;  // UPC-D3
    public static final int SCAN_SDT_UPCD4    = 116;  // UPC-D4
    public static final int SCAN_SDT_UPCD5    = 117;  // UPC-D5
    public static final int SCAN_SDT_EAN8_S   = 118;  // EAN 8 with
                                                      //   supplemental barcode
    public static final int SCAN_SDT_EAN13_S  = 119;  // EAN 13 with
                                                      //   supplemental barcode
    public static final int SCAN_SDT_EAN128   = 120;  // EAN 128
    public static final int SCAN_SDT_OCRA     = 121;  // OCR "A"
    public static final int SCAN_SDT_OCRB     = 122;  // OCR "B"

    // Two dimensional symbologies
    public static final int SCAN_SDT_PDF417   = 201;
    public static final int SCAN_SDT_MAXICODE = 202;

    // Special cases
    public static final int SCAN_SDT_OTHER    = 501;  // Start of Scanner-
                                                      //   Specific bar code
                                                      //   symbologies
    public static final int SCAN_SDT_UNKNOWN  =   0;  // Cannot determine the
                                                      //   barcode symbology.
    //add by zzy
    public static final String[] ScannerList={"cipher:key","orbit:key","voyager:key"};
   //the order of the list is "upca","upce","ean13","ean8","code39"
    public static final String[][] KEY={{"cipherupca","cipherupce","cipherean13","cipherean8","ciphercode39"},{"orbitupca","orbitupce","orbitean13","orbitean8","orbitcode39"},{"voyagerupca","voyagerupce","voyagerean13","voyagerean8","voyagercode39"}};
    public static final String[][] KEYVALUE={{"]C1","P","V","M","A"},{"a","b","c","d","e"},{"b","c","d","e","a"}};
    //-add by zzy

    // -add by chao (jpos.ScannerConst.class)
    public static final int SCAN_SDT_RSS14 = 131;
    public static final int SCAN_SDT_RSS_EXPANDED = 132;
    public static final int SCAN_SDT_CCA = 151;
    public static final int SCAN_SDT_CCB = 152;
    public static final int SCAN_SDT_CCC = 153;
}
