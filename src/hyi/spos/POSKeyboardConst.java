package hyi.spos;

public interface POSKeyboardConst {
    //###################################################################
    //#### POS Keyboard Constants
    //###################################################################

    /////////////////////////////////////////////////////////////////////
    // "EventTypes" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int KBD_ET_DOWN          =   1;
    public static final int KBD_ET_DOWN_UP       =   2;


    /////////////////////////////////////////////////////////////////////
    // "POSKeyEventType" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int KBD_KET_KEYDOWN      =   1;
    public static final int KBD_KET_KEYUP        =   2;
}
