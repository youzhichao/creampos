package hyi.spos;

import hyi.spos.events.*;

abstract public class ToneIndicator extends BaseControl
{
    private boolean asyncMode = false;

    public ToneIndicator()
    {
    }

    public boolean getAsyncMode() throws JposException
    {
        return asyncMode;
    }

    public void setAsyncMode(boolean asyncMode) throws JposException
    {
        this.asyncMode = asyncMode;
    }

    public void sound(int numberOfCycles, int interSoundWait) throws JposException
    {
    }

    public void soundImmediate() throws JposException
    {
    }
}
