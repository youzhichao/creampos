package hyi.spos.events;

public interface ErrorListener extends java.util.EventListener {
    public void errorOccurred(ErrorEvent e);
}
