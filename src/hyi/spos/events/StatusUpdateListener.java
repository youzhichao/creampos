package hyi.spos.events;

public interface StatusUpdateListener extends java.util.EventListener {
    public void statusUpdateOccurred(StatusUpdateEvent e);
}
