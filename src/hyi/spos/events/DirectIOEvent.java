package hyi.spos.events;

public class DirectIOEvent extends JposEvent {
    public DirectIOEvent(Object source, int eventNumber, int data, 
                         Object object) {
        super(source);

        this.eventNumber = eventNumber;
        this.data = data;
        this.object = object;
    }

    public int getEventNumber() {
        return eventNumber;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    protected int    eventNumber;
    protected int    data;
    protected Object object;
}
