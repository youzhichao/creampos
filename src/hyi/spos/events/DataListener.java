package hyi.spos.events;

public interface DataListener extends java.util.EventListener {
    public void dataOccurred(DataEvent e);
}
