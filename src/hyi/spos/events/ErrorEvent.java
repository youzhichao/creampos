package hyi.spos.events;

public class ErrorEvent extends JposEvent {
    public ErrorEvent(Object source, int errorCode, int errorCodeExtended,
                      int errorLocus, int errorResponse) {
        super(source);

        this.errorCode = errorCode;
        this.errorCodeExtended = errorCodeExtended;
        this.errorLocus = errorLocus;
        this.errorResponse = errorResponse;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public int getErrorCodeExtended() {
        return errorCodeExtended;
    }

    public int getErrorLocus() {
        return errorLocus;
    }

    public int getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(int errorResponse) {
        this.errorResponse = errorResponse;
    }

    protected int errorCode;
    protected int errorCodeExtended;
    protected int errorLocus;
    protected int errorResponse;
}
