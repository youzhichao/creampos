package hyi.spos.events;

public class OutputCompleteEvent extends JposEvent {
    public OutputCompleteEvent(Object source, int outputID) {
        super(source);

        this.outputID = outputID;
    }

    public int getOutputID() {
        return(outputID);
    }

    protected int outputID;
}
