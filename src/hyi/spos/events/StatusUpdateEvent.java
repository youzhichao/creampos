package hyi.spos.events;

public class StatusUpdateEvent extends java.util.EventObject {
    public StatusUpdateEvent(Object source) {
        super(source);
    }

    public StatusUpdateEvent(Object source, int i) {
        super(source);
    }
}
