package hyi.spos.events;

public abstract class JposEvent extends java.util.EventObject {
    public JposEvent(Object source) {
        super(source);
        updateSequenceNumber();
    }

    public synchronized final void updateSequenceNumber() {
        sequenceNumber = incrSequenceNumber();
    }

    public static final synchronized long incrSequenceNumber() {
        return ++globalSequenceNumber;
    }
    
    public final long getSequenceNumber() {
      return sequenceNumber;
    }

    public long getWhen() {
        return when;
    }

    protected      long   sequenceNumber;
    private static long   globalSequenceNumber = 0;
    private        long   when;
}
