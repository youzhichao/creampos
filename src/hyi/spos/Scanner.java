package hyi.spos;

abstract public class Scanner extends BaseControl {

    abstract public byte[] getScanData(int seq) throws JposException;

    //abstract public byte[] getScanDataLabel() throws JposException;

    abstract public int getScanDataType(int seq) throws JposException;

    abstract public boolean getAutoDisable() throws JposException;

    abstract public void setAutoDisable(boolean autoDisable) throws JposException;

    // Used by GWT service
    abstract public void fireDataEvent(int scanDataType, String scanData);
}

