package hyi.spos;

import hyi.cream.util.HYIDouble;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.LineItem;

/**
 * Base class for LineDisplay.
 *
 * @author Bruce You
 */
abstract public class LineDisplay extends BaseControl {

    abstract public void clearText() throws JposException;

    abstract public void displayText(String data, int lineNo)
        throws JposException;

    abstract public void displayTextAt(int y, int x, String data, int lineNo)
        throws JposException;

    abstract public void showSalesInfo(Transaction transaction, LineItem lineItem);

    abstract public void showTenderInfo(Transaction transaction);
    
    /**
     * Get current LineDisplay text, each line is seperated by \n.
     */
    abstract public String getLineDisplayText();
}


