package hyi.spos;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.ScreenCapture;
import hyi.cream.groovydac.Param;
import hyi.cream.autotest.AutoTester;
import hyi.cream.gwt.client.device.POSButtonData;
import hyi.cream.gwt.client.device.POSKeyboardData;
import hyi.cream.uibeans.*;
import hyi.spos.events.DataEvent;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.io.File;

/**
 * Base class for POS keyboard.
 *
 * @author dai, Bruce
 */
abstract public class POSKeyboard extends BaseControl {

    protected int posKeyData;

    /**
     * Constructor
     */
    public POSKeyboard() {
    }

    /**
     * Get last key code.
     */
    public int getPOSKeyData() throws JposException
    {
        return posKeyData;
    }

    public void setPOSKeyData(int posKeyData) {
        this.posKeyData = posKeyData;
    }

    public void fireEvent() {
        POSButtonHome2 posButtonHome = POSTerminalApplication.getInstance().getPOSButtonHome();

        if (posButtonHome.findPosButton(posKeyData) instanceof ReplayButton) {
            System.out.println("POSKeyboard.fireEvent(): Autotesting...");
            new Thread(new Runnable() {
                public void run() {
                    AutoTester.run();
                }
            }, "AutoTester").start();
            return;
        }
        if (posKeyData == posButtonHome.getScreenCaptureCode()) {
            System.out.println("POSKeyboard.fireEvent(): Screen capturing...");
            ScreenCapture.capture();
            return;
        }
        if (posKeyData == posButtonHome.getStraitButtonCode()) {
            Param.getInstance().switchBetweenTraditionalAndSimplifiedChinese();
            POSTerminalApplication.getInstance().repaintEverything();
            return;
        }

        POSTerminalApplication app = POSTerminalApplication.getInstance();
        PopupMenuPane popupMenu = app.getPopupMenuPane();
        SignOffForm signOffForm = app.getSignOffForm();
        CreditCardForm creditCardForm = app.getCreditCardForm();
        ItemList itemList = app.getItemList();
        DacViewer dacViewer = app.getDacViewer();
        PayingPaneBanner payingPane = app.getPayingPane();
        int pageUp = posButtonHome.getPageUpCode();
        int pageDown = posButtonHome.getPageDownCode();

        if (signOffForm != null && signOffForm.isVisible()) {
            signOffForm.keyDataListener(posKeyData);
            return;
        } else if (creditCardForm != null && creditCardForm.isVisible()) {
            creditCardForm.keyDataListener(posKeyData);
            return;
        } else if (popupMenu != null && popupMenu.isVisible()) {
            popupMenu.keyDataListener(posKeyData);
            return;
        } else if (itemList != null && itemList.isVisible()
            && (posKeyData == pageUp || posKeyData == pageDown))
            itemList.keyDataListener(posKeyData);
        else if (payingPane != null && payingPane.isVisible())
            payingPane.keyDataListener(posKeyData);
        else if (dacViewer != null && dacViewer.isVisible())
            dacViewer.keyDataListener(posKeyData);
        fireDataEvent(new DataEvent(this));
    }

    /**
     * Get POS keyboard detail information, used by GWT service.
     */
    public POSKeyboardData getPOSKeyboardData() {
        return null;
    }

    /**
     * Helper method for getting keyboard info.
     *
     * @param keyCodes Key codes of all keys.
     * @param rowColStyleCode true if key code 101 means at first row and first column,
     *          key code 203 means at second row and third column
     * @param rows Total number of rows in keyboard.
     *
     * @return A POSKeyboardData object containing keyboard info.
     */
    protected POSKeyboardData getPOSKeyboardData(Collection<Integer> keyCodes,
        boolean rowColStyleCode, int rows)
    {
        POSKeyboardData posKeyboardData = new POSKeyboardData();
        posKeyboardData.setLogicalName(getLogicalDeviceName());
        Map<Integer, POSButton> keyCodeToButtonMap =
            POSButtonHome2.getInstance().getKeyCodeToButtonMap();
        for (Integer keyCode : keyCodes) {
            POSButton button = keyCodeToButtonMap.get(keyCode);
            if (button != null) {
                POSButtonData bd = new POSButtonData();
                bd.setKeyCode(keyCode);
                if (button instanceof EmptyButton)
                    bd.setLabel("(空)");
                else
                    bd.setLabel(button.getLabel());
                if (rowColStyleCode) {
                    bd.setRow(keyCode / 100 - 1);
                    bd.setCol(keyCode % 100 - 1);
                } else {
                    // place button by column-major order
                    bd.setRow((keyCode - 1) % rows);
                    bd.setCol((keyCode - 1) / rows);
                }
                posKeyboardData.getPosButtons().add(bd);
            }
        }
        // sort buttons by row-major order
        Collections.sort(posKeyboardData.getPosButtons(),
            new Comparator<POSButtonData>() {
                public int compare(POSButtonData o1, POSButtonData o2) {
                    return (o1.getRow() < o2.getRow()) ? -1 :
                        (o1.getRow() > o2.getRow()) ? 1 :
                        (o1.getCol() < o2.getCol()) ? -1 :
                        (o1.getCol() > o2.getCol()) ? 1 : 0;
                }
            });
        return posKeyboardData;
    }
}
