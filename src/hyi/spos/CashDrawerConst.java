package hyi.spos;

public interface CashDrawerConst {
    //###################################################################
    //#### Cash Drawer Constants
    //###################################################################

    /////////////////////////////////////////////////////////////////////
    // "StatusUpdateEvent" Event: "status" Parameter Constants
    /////////////////////////////////////////////////////////////////////
    public static final int CASH_SUE_DRAWERCLOSED =   0;
    public static final int CASH_SUE_DRAWEROPEN   =   1;

}
