package hyi.spos;

/**
 * Base class for SPOS POSPrinter driver.
 */
abstract public class POSPrinter extends BaseControl {

    abstract public void printNormal(int i, String s) throws JposException;

    abstract public void printTwoNormal(int i, String s1, String s2) throws JposException;

    abstract public void cutPaper(int i) throws JposException;

    abstract public String getCheckHealthText() throws JposException;

    abstract public void setAlwaysHealthy(boolean alwaysHealthy);

    abstract public String retrievePrintingLines();

    //abstract public void openDrawer()throws JposException;
    //abstract public int getRecLinesToPaperCut() throws JposException;
}



