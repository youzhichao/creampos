package hyi.spos;   

import hyi.spos.events.StatusUpdateEvent;

import java.awt.Toolkit;


abstract public class CashDrawer extends BaseControl {

    public CashDrawer() {
    }

    // private boolean drawerOpened = false;
    // public boolean getDrawerOpened() throws JposException {
    // return drawerOpened;
    // }
    abstract public boolean getDrawerOpened() throws JposException;

    abstract public  void openDrawer() throws JposException;


    //private boolean drawerClosed;

    public void waitForDrawerClose(int beepTimeout, int beepFrequency,
            int beepDuration, int beepDelay) throws JposException {
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_DISABLED,
                    "Cash drawer is not enabled!");

        //drawerClosed = false;
        long start = System.currentTimeMillis();
        long waitTime = beepTimeout;
        while (true) {
            if (!getDrawerOpened()) {
                //drawerClosed = true;
                break;
            }
            if (waitTime > 0) {
                waitTime = beepTimeout - (System.currentTimeMillis() - start);
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                }
            } else {
                Toolkit.getDefaultToolkit().beep();
                try {
                    Thread.sleep(beepDelay);
                } catch (Exception e) {
                }
            }
        }
        /*
         * new Beep(beepTimeout, beepDelay); while (!drawerClosed) { try {
         * Thread.sleep(200); } catch (InterruptedException e) { } }
         */

        // synchronized (waitBeep) {
        // try {
        // waitBeep.wait();
        // } catch (InterruptedException ie) {
        // if (!getDrawerOpened())
        // return;
        // else
        // throw new JposException (JposConst.JPOS_E_FAILURE, this.toString(),
        // ie);
        // }
        // }
        fireStatusUpdateEvent(new StatusUpdateEvent(this,
                CashDrawerConst.CASH_SUE_DRAWERCLOSED));
    }
}