package hyi.spos;

import hyi.cream.uibeans.CreditCardForm;
import hyi.cream.POSTerminalApplication;
import hyi.spos.events.DataEvent;

abstract public class MSR extends BaseControl {

    abstract public String getAccountNumber();

    abstract public String getExpirationDate();

    abstract public int getTracksToRead();

    abstract public byte[] getTrack1Data() throws JposException;

    abstract public byte[] getTrack2Data() throws JposException;

    abstract public byte[] getTrack3Data() throws JposException;

    abstract public void fireEvent(String accountNumber, String expirationDate);

    protected void fireEvent() {
        CreditCardForm creditCardForm = POSTerminalApplication.getInstance().getCreditCardForm();
        if (creditCardForm != null && creditCardForm.isVisible())
            creditCardForm.sendMSRData(getAccountNumber(), getExpirationDate());
        else
            fireDataEvent(new DataEvent(this));
    }

}
