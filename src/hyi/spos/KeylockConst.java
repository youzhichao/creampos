package hyi.spos;

public interface KeylockConst {
    //###################################################################
    //#### Keylock Constants
    //###################################################################

    /////////////////////////////////////////////////////////////////////
    // "KeyPosition" Property Constants
    // "WaitForKeylockChange" Method: "KeyPosition" Parameter
    // "StatusUpdateEvent" Event: "status" Parameter
    /////////////////////////////////////////////////////////////////////

    public static final int LOCK_KP_ANY          = 0; // WaitForKeylockChange Only
    public static final int LOCK_KP_LOCK         = 1;
    public static final int LOCK_KP_NORM         = 2;
    public static final int LOCK_KP_SUPR         = 3;
}
