package hyi.spos.services;

import hyi.spos.CashDrawer;
import hyi.spos.JposException;

import java.util.Map;

/**
 * SPOS driver for virtual cash drawer.
 *
 * @author Bruce You
 */
public class VirtualCashDrawer extends CashDrawer {


    public VirtualCashDrawer(Map<String, Object> entry) {
    }

    @Override
    public void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName);
    }

    @Override
    public boolean getDrawerOpened() throws JposException {
        return false;
    }

    @Override
    public void openDrawer() throws JposException {
    }

    @Override
    public void claim(int timeout) throws JposException {
    }

    @Override
    public void close() throws JposException {
    }

    @Override
    public void release() throws JposException {
    }

    public char getDrawerStatus() {
        return '0';
    }

    public void setDrawerStatus(char drawerStatus) {
    }

    public String getStatusCode() {
        return "0";
    }

    public void setStatusCode(String statusCode) {
    }
}