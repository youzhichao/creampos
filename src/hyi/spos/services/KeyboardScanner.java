package hyi.spos.services;

import hyi.spos.JposConst;
import hyi.spos.JposException;
import hyi.spos.Scanner;
import hyi.spos.ScannerConst;
import hyi.spos.events.DataEvent;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.*;
import java.util.List;

/**
 * This is SPOS device service for any keyboard scanner.
 * <p/>
 * <p/>
 * <BR>
 * The following properties are supported:
 * <LI> AutoDisable
 * <LI> DataCount
 * <LI> DataEventEnabled
 * <LI> FreezeEvents
 * <p/>
 * <BR>
 * The followings are the limitation of current implementation:
 * <LI> DecodeData is always true.
 * <LI> CapPowerReporting is JPOS_PR_NONE.
 * <LI> PowerNotify is JPOS_PN_DISABLED.
 * <LI> PowerState is JPOS_PS_UNKNOWN.
 * <LI> CapCompareFirmwareVersion is false
 * <LI> CapStatisticsReporting is false
 * <LI> CapUpdateFirmware is false
 * <LI> CapUpdateStatistics is false
 * <LI> Do not generate DirectIOEvent and StatusUpdateEvent.
 * <p/>
 * <BR>
 * Note: Because this JavaPOS device service intercept and get keyboard event from AWT thread,
 * application cannot receive any POSKeyboard/Keylock/MSR/Scanner event from within AWT event
 * processing. Our JavaPOS user should not expect to get any JavaPOS event within their AWT event
 * processing method.
 * <p/>
 * <p/>Sample JCL config:
 * <p/>
 * <pre>
 *   &lt;JposEntry logicalName=&quot;KeyboardScanner&quot;&gt;
 *       &lt;creation factoryClass=&quot;hyi.spos.loader.ServiceInstanceFactory&quot;
 *                 serviceClass=&quot;hyi.spos.services.KeyboardScanner&quot;/&gt;
 *       &lt;vendor name=&quot;Any vendor&quot; url=&quot;http://www.hyi.com.tw&quot;/&gt;
 *       &lt;jpos category=&quot;Scanner&quot; version=&quot;1.9&quot;/&gt;
 *       &lt;product description=&quot;Any POS Scanner&quot; name=&quot;Any POS Scanner&quot; url=&quot;http://www.hyi.com.tw&quot;/&gt;
 *
 *       &lt;!-- Scanner Data Header --&gt;
 *       &lt;prop name=&quot;PrefixCharacter&quot; type=&quot;Character&quot; value=&quot;\&quot;/&gt;
 *
 *       &lt;!-- Code Identifiers --&gt;
 *       &lt;prop name=&quot;UPCA&quot; type=&quot;String&quot; value=&quot;A&quot;/&gt;
 *       &lt;prop name=&quot;UPCE&quot; type=&quot;String&quot; value=&quot;E&quot;/&gt;
 *       &lt;prop name=&quot;JAN8&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;EAN8&quot; type=&quot;String&quot; value=&quot;FF&quot;/&gt;
 *       &lt;prop name=&quot;JAN13&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;EAN13&quot; type=&quot;String&quot; value=&quot;F&quot;/&gt;
 *       &lt;prop name=&quot;TF&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;ITF&quot; type=&quot;String&quot; value=&quot;I&quot;/&gt;
 *       &lt;prop name=&quot;Codabar&quot; type=&quot;String&quot; value=&quot;N&quot;/&gt;
 *       &lt;prop name=&quot;Code39&quot; type=&quot;String&quot; value=&quot;M&quot;/&gt;
 *       &lt;prop name=&quot;Code93&quot; type=&quot;String&quot; value=&quot;L&quot;/&gt;
 *       &lt;prop name=&quot;Code128&quot; type=&quot;String&quot; value=&quot;K&quot;/&gt;
 *       &lt;prop name=&quot;UPCA_S&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCE_S&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCD1&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCD2&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCD3&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCD4&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;UPCD5&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;EAN8_S&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;EAN13_S&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;EAN128&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;OCRA&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;OCRB&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;RSS14&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;RSS_EXPANDED&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;CCA&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;CCB&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;CCC&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;PDF417&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *       &lt;prop name=&quot;MAXICODE&quot; type=&quot;String&quot; value=&quot;?&quot;/&gt;
 *   &lt;/JposEntry&gt;
 *
 * </pre>
 *
 * @author Bruce You @ Hongyuan Software
 * @since 2007-2-14 Valentine's Day
 */
public class KeyboardScanner extends Scanner {

    private static final Object[][] IDENTIFIER_CODES = {
        { ScannerConst.SCAN_SDT_UPCA, "UPCA" },
        { ScannerConst.SCAN_SDT_UPCE, "UPCE" },
        { ScannerConst.SCAN_SDT_JAN8, "JAN8" },
        { ScannerConst.SCAN_SDT_EAN8, "EAN8" },
        { ScannerConst.SCAN_SDT_JAN13, "JAN13" },
        { ScannerConst.SCAN_SDT_EAN13, "EAN13" },
        { ScannerConst.SCAN_SDT_TF, "TF" },
        { ScannerConst.SCAN_SDT_ITF, "ITF" },
        { ScannerConst.SCAN_SDT_Codabar, "Codabar" },
        { ScannerConst.SCAN_SDT_Code39, "Code39" },
        { ScannerConst.SCAN_SDT_Code93, "Code93" },
        { ScannerConst.SCAN_SDT_Code128, "Code128" },
        { ScannerConst.SCAN_SDT_UPCA_S, "UPCA_S" },
        { ScannerConst.SCAN_SDT_UPCE_S, "UPCE_S" },
        { ScannerConst.SCAN_SDT_UPCD1, "UPCD1" },
        { ScannerConst.SCAN_SDT_UPCD2, "UPCD2" },
        { ScannerConst.SCAN_SDT_UPCD3, "UPCD3" },
        { ScannerConst.SCAN_SDT_UPCD4, "UPCD4" },
        { ScannerConst.SCAN_SDT_UPCD5, "UPCD5" },
        { ScannerConst.SCAN_SDT_EAN8_S, "EAN8_S" },
        { ScannerConst.SCAN_SDT_EAN13_S, "EAN13_S" },
        { ScannerConst.SCAN_SDT_EAN128, "EAN128" },
        { ScannerConst.SCAN_SDT_OCRA, "OCRA" },
        { ScannerConst.SCAN_SDT_OCRB, "OCRB" },
        { ScannerConst.SCAN_SDT_RSS14, "RSS14" },
        { ScannerConst.SCAN_SDT_RSS_EXPANDED, "RSS_EXPANDED" },
        { ScannerConst.SCAN_SDT_CCA, "CCA" },
        { ScannerConst.SCAN_SDT_CCB, "CCB" },
        { ScannerConst.SCAN_SDT_CCC, "CCC" },
        { ScannerConst.SCAN_SDT_PDF417, "PDF417" },
        { ScannerConst.SCAN_SDT_MAXICODE, "MAXICODE" }
    };

    private class BarcodeData {
        int scanDataType;
        byte[] scanData;

        BarcodeData(int scanDataType, byte[] scanData) {
            this.scanDataType = scanDataType;
            this.scanData = scanData;
        }
    };

    private int prefixCharacter;
    private Map<String, Integer> identifierCodeMap = new TreeMap<String, Integer>();
    private Map<Integer, BarcodeData> barcodeMap = new HashMap<Integer, BarcodeData>();
    private int currentSeq;
    private KeyEventDispatcher keyEventInterceptor;
    private byte[] scanData = new byte[0];
    private int scanDataType = ScannerConst.SCAN_SDT_UNKNOWN;
    private List<String> eventQueue = new ArrayList<String>();
    private boolean autoDisable = false;
    private boolean dataEventEnabled = false;

    public KeyboardScanner() {
        createKeyEventInterceptor();
        setState(JposConst.JPOS_S_CLOSED);
    }

    public KeyboardScanner(Map<String, Object> entry) {
        this();
        prefixCharacter = (Character)entry.get("PrefixCharacter");

        for (Object[] idCode : IDENTIFIER_CODES) {
            String identifierCode = (String)entry.get((String)idCode[1]);
            if (identifierCode != null && identifierCode.length() > 0
                && !identifierCode.equals("?"))
                identifierCodeMap.put(identifierCode, (Integer)idCode[0]);
        }
    }

    /**
     * Create a keyboard event interceptor for accepting and firing MSR event. The interceptor is an
     * AWT's KeyEventDispatcher, and then it'll be added into KeyboardFocusManager when enabling
     * this device.
     */
    private void createKeyEventInterceptor() {

        keyEventInterceptor = new KeyEventDispatcher() {
            boolean withinControlSeq;
            StringBuffer scannerData = new StringBuffer();
            int waitLastEvent;

            public boolean dispatchKeyEvent(KeyEvent e) {
                try {
                    setState(JposConst.JPOS_S_BUSY);

                    int code = e.getKeyCode();

                    if (e.getID() == KeyEvent.KEY_PRESSED && code == prefixCharacter) {
                        scannerData.setLength(0);
                        withinControlSeq = true;
                        return true;
                    }

                    if (e.getID() != KeyEvent.KEY_TYPED) {
                        if (waitLastEvent > 0) {
                            waitLastEvent--;
                            return true;
                        }

                        // While in-between control sequence of MSR, return true
                        // to absort any key event for preventing from sending
                        // them to application
                        return withinControlSeq;
                    }

                    char keyChar = e.getKeyChar();

                    if (withinControlSeq) {
                        if (keyChar == 10) { // End Symbol
                            withinControlSeq = false;

                            // The last char is 10, but AWT system will still generate
                            // a KEY_RELEASE event. We also want to ignore and absort this
                            // KeyEvents, so we set a count here for doing this.
                            waitLastEvent = 1;

                            if (!getDataEventEnabled() || getFreezeEvents()) {
                                // Append into event queue if now the DataEventEnabled is
                                // false or FreezeEvent is true
                                eventQueue.add(scannerData.toString());
                            } else {
                                // If AutoDisable is true, then automatically disable myself
                                if (getAutoDisable())
                                    setDeviceEnabled(false);

                                // Fire the DataEvent
                                fireDataEvent(scannerData.toString());
                            }
                        } else {
                            scannerData.append(keyChar);
                        }
                        return true;
                    }
                    return false;
                } catch (JposException e2) {
                    e2.printStackTrace();
                    return false;
                } finally {
                    setState(JposConst.JPOS_S_IDLE);
                }
            }
        };
    }

    // AutoDisable boolean R/W open
    public boolean getAutoDisable() throws JposException {
        return autoDisable;
    }

    public void setAutoDisable(boolean autoDisable) throws JposException {
        this.autoDisable = autoDisable;
    }

    public int getDataCount() throws JposException {
        return eventQueue.size();
    }

    @Override
    public boolean getDataEventEnabled() throws JposException {
        return dataEventEnabled;
    }

    private void fireEventsInQueue() {
        try {
            setState(JposConst.JPOS_S_BUSY);
            if (getDeviceEnabled()
                && getDataEventEnabled() && !getFreezeEvents()) {
                while (!eventQueue.isEmpty()) {
                    // If AutoDisable is true, then automatically disable myself
                    if (getAutoDisable())
                        setDeviceEnabled(false);

                    fireDataEvent(eventQueue.get(0));
                    eventQueue.remove(0);

                    if (getAutoDisable())
                        break;
                }
            }
        } catch (JposException e) {
            e.printStackTrace();
        } finally {
            setState(JposConst.JPOS_S_IDLE);
        }
    }

    @Override
    public void setDataEventEnabled(boolean dataEventEnabled) throws JposException {
        this.dataEventEnabled = dataEventEnabled;
        fireEventsInQueue();
    }

    @Override
    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        super.setFreezeEvents(freezeEvents);
        fireEventsInQueue();
    }

    @Override
    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        if (getDeviceEnabled()) {
            System.out.println("KeyboardScanner is already enabled.");
            return;
        }
        super.setDeviceEnabled(deviceEnabled);

        // Setup a key event interceptor when enabling device
        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        if (deviceEnabled)
            kfm.addKeyEventDispatcher(keyEventInterceptor);
        else
            kfm.removeKeyEventDispatcher(keyEventInterceptor);
    }

    private void removeOldBarcode() {
        if (barcodeMap.size() < 16)
            return;
        List<Integer> toBeRemoved = new ArrayList<Integer>();
        for (int seq : barcodeMap.keySet()) {
            if (seq < currentSeq - 6)
                toBeRemoved.add(seq);
        }
        for (int seq : toBeRemoved)
            barcodeMap.remove(seq);
    }

    @Override
    public byte[] getScanData(int seq) throws JposException {
        removeOldBarcode();
        return barcodeMap.get(seq).scanData;
    }

    @Override
    public int getScanDataType(int seq) throws JposException {
        return barcodeMap.get(seq).scanDataType;
    }

    @Override
    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    @Override
    public void clearInput() throws JposException {
        eventQueue.clear();
    }

    @Override
    public void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName);
        setState(JposConst.JPOS_S_IDLE);
    }

    @Override
    public void close() throws JposException {
        setDeviceEnabled(false);
        setState(JposConst.JPOS_S_CLOSED);
    }

    @Override
    public void release() throws JposException {
    }

    @Override
    public void claim(int timeout) throws JposException {
    }

    private void fireDataEvent(String scannerData) {
        scannerData = scannerData.substring(1); // remove prefix
        Object[] ids = identifierCodeMap.keySet().toArray();
        for (int i = ids.length - 1; i >= 0; i--) {
            String identifierCode = (String)ids[i];
            if (scannerData.startsWith(identifierCode)) {
                fireDataEvent(identifierCodeMap.get(identifierCode),
                    scannerData.substring(identifierCode.length()));
                return;
            }
        }
        fireDataEvent(ScannerConst.SCAN_SDT_UNKNOWN, scannerData);
    }

    @Override
    public void fireDataEvent(int scanDataType, String scanData) {
        this.scanDataType = scanDataType;
        this.scanData = scanData.getBytes();
        DataEvent dataEvent = new DataEvent(this);
        dataEvent.seq = currentSeq++;
        BarcodeData barcodeData = new BarcodeData(this.scanDataType, this.scanData);
        barcodeMap.put(dataEvent.seq, barcodeData);
        fireDataEvent(dataEvent);
    }
}