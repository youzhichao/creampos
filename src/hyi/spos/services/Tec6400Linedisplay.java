package hyi.spos.services;

import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.util.CreamToolkit;
import hyi.jpos.services.LineDisplayLocal;
import hyi.spos.JposException;
import hyi.spos.LineDisplay;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class Tec6400Linedisplay extends LineDisplay {

    private static final String BLANK20 = "                    ";
    protected Map<String, Object> entry;
    private String textLine1 = BLANK20;
    private String textLine2 = BLANK20;
    private String itemPattern;
    private String subtotalPattern;
    private String changePattern;

    public Tec6400Linedisplay(Map<String, Object> entry) {
        this.entry = entry;
        itemPattern = (String)entry.get("ItemPattern");
        subtotalPattern = (String)entry.get("SubtotalPattern");
        changePattern = (String)entry.get("ChangePattern");

        if (itemPattern == null)
            CreamToolkit.logMessage("Err> Lack 'ItemPattern' property in Tec6400Linedisplay.");
        if (subtotalPattern == null)
            CreamToolkit.logMessage("Err> Lack 'SubtotalPattern' property in Tec6400Linedisplay.");
        if (changePattern == null)
            CreamToolkit.logMessage("Err> Lack 'ChangePattern' property in Tec6400Linedisplay.");
    }

    public void connect() {
        LineDisplayLocal.ini();
        return;
    }

    public void claim(int timeout) throws JposException {
    }

    public void close() throws JposException {
    }

    public void open(String logicalName) throws JposException {
    }

    synchronized public void release() throws JposException {
    }

    public void clearText() throws JposException {
        byte[] b1;
        try {
            b1 = "                    ".getBytes(CreamToolkit.getEncoding());
            byte[] b2 = new byte[b1.length + 1];
            for (int i = 0; i < b1.length; i++) {
                b2[i] = b1[i];
            }
            b2[b1.length] = 0;
            LineDisplayLocal.ini();
            LineDisplayLocal.show(b2, 1);
            LineDisplayLocal.show(b2, 2);

        } catch (UnsupportedEncodingException e) {
        }
    }

    public void displayText(String data, int lineNo) throws JposException {
        while (data.length() > 20) {
            data = data.substring(0, data.length() - 1);
        }
        while (data.length() < 20) {
            data = data + " ";
        }

        if (lineNo == 1)
            textLine1 = data;
        else
            textLine2 = data;

        try {
            byte[] b1 = data.getBytes(CreamToolkit.getEncoding());
            byte[] b2 = new byte[b1.length + 1];
            for (int i = 0; i < b1.length; i++) {
                b2[i] = b1[i];
            }
            b2[b1.length] = 0;

            //for (int i = 0; i < b2.length; i++)
            //    System.out.println(b2[i] + ",");
            
            LineDisplayLocal.ini();
            Thread.sleep(10); //Bruce/20090305/ sleep a while to fix problem
            LineDisplayLocal.show(b2, lineNo);
            Thread.sleep(10); //Bruce/20090305/ sleep a while to fix problem
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayTextAt(int y, int x, String data, int attribute) throws JposException {
        // not implemented yet
    }

    public void showSalesInfo(Transaction transaction, LineItem lineItem) {
        try {
            clearText();
            displayText(CreamToolkit.dacToString(itemPattern, lineItem), 1);
            displayText(CreamToolkit.dacToString(subtotalPattern, transaction), 2);
        } catch (JposException e) {
        }
    }

    public void showTenderInfo(Transaction transaction) {
        try {
            clearText();
            displayText(CreamToolkit.dacToString(changePattern, transaction), 1);
            displayText(CreamToolkit.dacToString(subtotalPattern, transaction), 2);
        } catch (JposException e) {
        }
    }

    public String getLineDisplayText() {
        return textLine1 + '\n' + textLine2;
    }
}