package hyi.spos.services

import hyi.cream.util.CreamToolkit
import hyi.spos.JposConst
import hyi.spos.JposException
import hyi.spos.POSPrinter
import org.apache.commons.lang.StringUtils

import javax.comm.SerialPortEvent
import javax.comm.SerialPortEventListener

/**
 * SPOS driver for Epson printer.
 *
 * @author Bruce You
 */
class EpsonPOSPrinter extends POSPrinter implements SerialPortEventListener {

    private final PrinterAndDrawerPort printerPort
    //private StringParsingPP2000 parse
    private StringBuilder buffer
    private String portName
    private OutputStream outputStream
    private InputStream inputStream
    private OutputStreamWriter dataOutput
    private String printerStatus
    private boolean alwaysHealthy
    private boolean serialPortMissing
    private PrinterLoggerQueue printerLoggerQueue

    /** true if it's Epson command, or TP-3688/7688 command when false. */
    private boolean epsonCommand

    private int baudRate

    private boolean readyToGetResponse

    private String cutPaperCommand

    EpsonPOSPrinter(Map<String, Object> entry) {
        //parse = new StringParsingPP2000(this)
        buffer = new StringBuilder()
        printerLoggerQueue = new PrinterLoggerQueue()
        printerPort = PrinterAndDrawerPort.getInstance()
        this.portName = (String) entry.get("portName")
        String commandSet = (String) entry.get("commandSet")
        epsonCommand = "Epson".equalsIgnoreCase(commandSet)
        try {
            baudRate = Integer.parseInt((String) entry.get("baudRate"))
        } catch (Exception e) {
            baudRate = 19200
        }
        if (entry.containsKey("cutPaper")) {
            cutPaperCommand = (String) entry.get("cutPaper")
        }
    }

    /**
     * Set printer status to always online and healthy, used by GWT service.
     */
    void setAlwaysHealthy(boolean alwaysHealthy) {
        this.alwaysHealthy = alwaysHealthy
    }

    /**
     * n0: "S"
     * n1: 收執聯位置 "0":定位 "1":未定位
     * n2: 存根聯位置 "0":定位 "1":未定位
     * n3: 紙張是否用完 "0":否 "1":是
     * n4: 印證紙張定位 "0":無紙 "1":有紙
     * n5: 發票印表機發生錯誤 "0":沒發生 "1":發生
     * n6: 是否列印中 "0":否 "1":是
     * n7: 列印超過40行 "0":否 "1":是
     * n8: 資料緩衝區是否已滿 "0":否 "1":是
     * n9: 資料緩衝區是否為空 "0":是 "1":否
     */
    String getCheckHealthText() throws JposException {
        getPowerState()
        if (StringUtils.isEmpty(printerStatus)) {
            CreamToolkit.logMessage("EpsonPOSPrinter.getCheckHealthText(): didn't get printer status!")
            return "S110000000"
        }

        if (epsonCommand) {
            int status = printerStatus.charAt(0)
            String journalOK = (status & 0x20) != 0 ? "0" : "1"
            String receiptOK = (status & 0x40) != 0 ? "0" : "1"
            String retStatus = "S" + receiptOK + journalOK + "0000000"
            //System.out.println("EpsonPOSPrinter: getCheckHealthText()=" + retStatus)
            return retStatus
        } else
            return printerStatus
    }

    @Override
    int getPowerState() {
        if (alwaysHealthy) {
            printerStatus = epsonCommand ? "\u0060" : "S000000000"
            return JposConst.JPOS_PS_ONLINE
        }

        printerStatus = null
        if (serialPortMissing)
            return JposConst.JPOS_PS_OFFLINE

        synchronized (printerPort) {
            try {
                readyToGetResponse = true
                if (epsonCommand) {
                    outputStream.write(0x10)
                    outputStream.write(0x04)
                    outputStream.write(0x04)
                    outputStream.flush()
                } else {
                    outputStream.write(0x1B)
                    outputStream.write(0x1B)
                    outputStream.write(0x4F)
                    outputStream.write(0x0D)
                    outputStream.flush()
                }
                printerPort.wait(5000); // wait at most 5 seconds

                if (epsonCommand)
                    return (printerStatus != null && printerStatus.length() == 1) ?
                        JposConst.JPOS_PS_ONLINE : JposConst.JPOS_PS_OFFLINE
                else
                    return (printerStatus != null && printerStatus.length() == 10) ?
                        JposConst.JPOS_PS_ONLINE : JposConst.JPOS_PS_OFFLINE

            } catch (Exception e) {
                CreamToolkit.logMessage(e)
                return JposConst.JPOS_PS_OFFLINE
            }
        }
    }

    @Override
    void cutPaper(int percentage) throws JposException {
        // check out the situation to make sure that the printer can be operated
        if (percentage == 0)
            return
        if (percentage < 0 || percentage > 100)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Illegal argument:" + percentage
                + " detected!")

        printerLoggerQueue.add("-------CUT PAPER (" + percentage + ")------")

        if (serialPortMissing) {
            if (alwaysHealthy)
                return
            else
                throw new JposException(JposConst.JPOS_E_FAILURE, "Printer is not ready!")
        }

        // locate the next black block of the receipt and cut paper, print stamp
        try {
            if (cutPaperCommand) {
                outputStream.write(cutPaperCommand.getBytes())
            } else if (epsonCommand) {
                outputStream.write(0x0c)
            } else {
                outputStream.write(0x1b)
                outputStream.write(0x1b)
                outputStream.write(0x56)
                outputStream.write(0x42)
                outputStream.write(0x0d)
            }
            outputStream.flush()
        } catch (IOException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Write cut paper failed")
        }
    }

    void printImmediate(int station, String data) throws JposException {
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_DISABLED, "Printer is not enabled!")

        if (data == null || data.length() == 0)
            return

        String realCommand = (epsonCommand) ? data : "\u001B\u001BPB0" + data
        printerLoggerQueue.add(realCommand)

        if (serialPortMissing) {
            if (alwaysHealthy)
                return
            else
                throw new JposException(JposConst.JPOS_E_FAILURE, "Printer is not ready!")
        }

        try {
            dataOutput.write(realCommand)
            dataOutput.flush()
            Thread.sleep(20)
        } catch (Exception ie) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL)
        }
    }

    @Override
    void printNormal(int station, String data) throws JposException {
        printImmediate(station, data)
    }

    @Override
    void printTwoNormal(int station, String data1, String data2) throws JposException {
        String data = (data1 == null || data1.length() == 0) ? data2 : data1
        printImmediate(station, data)
    }

    @Override
    String retrievePrintingLines() {
        return printerLoggerQueue.retrieve()
    }

    @Override
    void claim(int timeout) throws JposException {
    }

    @Override
    void release() throws JposException {
    }

    @Override
    void close() throws JposException {
        printerPort.close()
        //printerPort = null
        outputStream = null
        inputStream = null
        dataOutput = null
    }

    @Override
    void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName)
        if (!printerPort.isSerialPortOpened()) {
            try {
                printerPort.open(logicalName, portName, baudRate)
            } catch (JposException e) {
            }
        }

        if (printerPort.isSerialPortOpened()) {
            printerPort.addSerialPortEventListener(this)
            outputStream = printerPort.getOutputStream()
            inputStream = printerPort.getInputStream()
            dataOutput = printerPort.getOutputStreamWriter()

            if (epsonCommand) {
                try {
                    dataOutput.write("\u001B@")
                    dataOutput.flush()
                } catch (IOException ie) {
                    throw new JposException(JposConst.JPOS_E_ILLEGAL)
                }
            } else {
                try {
                    dataOutput.write("\u001B\u001BR\r")
                    dataOutput.flush()
                } catch (IOException ie) {
                    throw new JposException(JposConst.JPOS_E_ILLEGAL)
                }
            }
        } else {
            serialPortMissing = true
        }
    }

    /**
     * serialEventListenner implementation, it is in another thread, executed automatically
     */
    synchronized public void serialEvent(SerialPortEvent event) {
        synchronized (printerPort) {
            if (!readyToGetResponse) {
                return
            }
            //try {
            //    System.out.println("EpsonPOSPrinter: serialEvent(), inputStream" + inputStream.toString() + ", available=" + inputStream.available())
            //} catch (IOException e) {
            //    e.printStackTrace()
            //}

            switch (event.getEventType()) {
                case SerialPortEvent.BI:
                case SerialPortEvent.OE:
                case SerialPortEvent.FE:
                case SerialPortEvent.PE:
                case SerialPortEvent.CD:
                case SerialPortEvent.CTS:
                case SerialPortEvent.DSR:
                case SerialPortEvent.RI:
                case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break

                case SerialPortEvent.DATA_AVAILABLE:
                    try {
                        char ch = 0
                        if (epsonCommand) {
                            buffer.setLength(0)
                            //System.out.println("EpsonPOSPrinter: reading printer status, inputStream" + inputStream.toString() + ", available=" + inputStream.available())
                            ch = (char)inputStream.read()
                            //System.out.println("EpsonPOSPrinter: get printer status=" + (int)ch)
                            buffer.append(ch)
                            printerStatus = buffer.toString()
                        } else {
                            // 如果不加上sleep()，下面這個loop會沒有辦法一次拿到完整的10 bytes
                            Thread.sleep(50)
                            while (inputStream.available() > 0) {
                                ch = (char)inputStream.read()
                                if (ch == 'S' && buffer.length() > 0) { // first char of response
                                    printerStatus = null
                                    buffer.setLength(0)
                                }
                                buffer.append(ch)
                            }
                            if (buffer.length() == 0) {
                                System.out.println("Printer status code is null!")
                            } else if ((int) ch == 10) { // end char of response
                                printerStatus = buffer.toString()
                            }
                            System.out.println("EpsonPOSPrinter: get printer status=" + printerStatus)
                        }
                    } catch (Exception e) {
                        CreamToolkit.logMessage(e)
                    } finally {
                        if (printerStatus != null) {
                            System.out.println("EpsonPOSPrinter: notifyAll(), get printer status=" + printerStatus)
                            printerPort.notifyAll()
                        }
                    }
                    break

                default:
                    break

            }
            readyToGetResponse = false
        }
    }
}