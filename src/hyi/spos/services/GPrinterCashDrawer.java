package hyi.spos.services;

import java.util.Map;

public class GPrinterCashDrawer extends hyi.spos.CashDrawer {

    public GPrinterCashDrawer(Map<String, Object> entry) {
    }

    public GPrinterCashDrawer() {
    }

    public void checkHealth(int level) {
    }

    public void directIO(int command, int[] data, Object object) {
    }

    @Override
    public void open(String logicalName) throws hyi.spos.JposException {

    }

    public boolean getDrawerOpened() {
        return false;
    }

    // Methods
    public void openDrawer() {
        GPrinterPOSPrinter p = GPrinterPOSPrinter.getInstance();
        if (p != null)
            p.openDrawer();
    }

    public void close() {
    }

    public void claim(int timeout) {
    }

    public void release() {
    }
}
