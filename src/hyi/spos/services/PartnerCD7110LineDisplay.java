package hyi.spos.services;

import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.spos.JposException;
import hyi.spos.LineDisplay;

import javax.comm.CommPortIdentifier;
import javax.comm.SerialPort;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;

/**
 * Simple Java POS device driver for PartnerCD7110LineDisplay.
 *
 * @author Bruce You
 */
public class PartnerCD7110LineDisplay extends LineDisplay {

    private static final boolean DEBUG = false;

    private static final String BLANK10 = "          ";
    private static final String BLANK20 = "                    ";

    /**
     * 5、小灯控制
     * ESC  s1  单价灯亮
     * 格式：[1BH][73H][31H]
     *
     * ESC  s2  总计灯亮
     * 格式：[1BH][73H][32H]
     *
     * ESC s3 收款灯亮
     * 格式：[1BH][73H][33H]
     *
     * ESC s4 找零灯亮
     * 格式：[1BH][73H][34H]
     *
     * ESC s0 状态灯全灭
     * 格式：[1BH][73H][30H]
     */
    private static int STATE_EMPTY = 0; //多谢惠顾
    private static int STATE_UNIT_PRICE = 5; //单价
    private static int STATE_TOTAL = 2; //合计
    private static int STATE_CASH = 3; //收款
    private static int STATE_CHANGE = 4; //找零

    private Map<String, Object> registry;
    private CommPortIdentifier commPortId;
    private SerialPort serialPort;
    private OutputStream outputStream;
    private OutputStreamWriter dataOutput;
    private boolean serialPortMissing;
    private String textLine1 = BLANK20;
    private String textLine2 = BLANK20;

    /**
     * Constructor
     */
    public PartnerCD7110LineDisplay(Map<String, Object> registry) {
        this.registry = registry;
    }

    public void connect() {
        deviceEnabled = false;
        try {
            CreamToolkit.logMessage("PartnerCD7220LineDisplay Serial port:" + registry.get("portName"));
            commPortId = CommPortIdentifier.getPortIdentifier((String)registry.get("portName"));

            //if (commPortId.isCurrentlyOwned()) {
            //    logger.debug("lineDisplay error : The device is being use!");
            //    return;
            //}

            serialPort = (SerialPort)commPortId.open(getLogicalDeviceName(), 10000);
            outputStream = serialPort.getOutputStream();
            dataOutput = new OutputStreamWriter(outputStream);

            try {
                serialPort.setSerialPortParams(
                        (Integer)registry.get("baudRate"),
                        (Integer)registry.get("dataBits"),
                        (Integer)registry.get("stopBits"),
                        (Integer)registry.get("parity"));
                // serialPort.setFlowControlMode((Integer)registry.get("flowControl"));
            } catch (Exception e) {
                e.printStackTrace();
                // retry to work around the bug of Sun's JavaComm
                try {
                    serialPort.setSerialPortParams(
                            (Integer)registry.get("baudRate"),
                            (Integer)registry.get("dataBits"),
                            (Integer)registry.get("stopBits"),
                            (Integer)registry.get("parity"));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }

            // initialize display
            outputStream.write(0x1B); // initialize display
            outputStream.write(0x40); // initialize display
            outputStream.flush();
            sleep();
            outputStream.write(0x0C); // 清屏
            outputStream.flush();
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
            serialPortMissing = true;
        }
    }

    /**
     * 5、小灯控制
     * ESC  s1  单价灯亮
     * 格式：[1BH][73H][31H]
     *
     * ESC  s2  总计灯亮
     * 格式：[1BH][73H][32H]
     *
     * ESC s3 收款灯亮
     * 格式：[1BH][73H][33H]
     *
     * ESC s4 找零灯亮
     * 格式：[1BH][73H][34H]
     *
     * ESC s0 状态灯全灭
     * 格式：[1BH][73H][30H]
     */
    private void displayState(int state) {
        try {
            outputStream.write(0x1B);
            outputStream.write(0x73);
            outputStream.write(state + 0x30);
            outputStream.flush();
            sleep();
        } catch (IOException e4) {
            CreamToolkit.logMessage(e4.toString());
        }
    }

    private void sleep() {
        try {
            Thread.sleep(99);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void claim(int timeout) throws JposException {
    }

    @Override
    public void close() throws JposException {
    }

    @Override
    public void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName);
        connect();
    }

    @Override
    public void release() throws JposException {
        try {
            if (!serialPortMissing) {
                outputStream.write(12);    //clear display screen, and clear string mode
                sleep();
                dataOutput.close();
                serialPort.close();
                outputStream = null;
                commPortId = null;
                serialPort = null;
                claimed = false;
                deviceEnabled = false;
            }
        } catch (IOException e) {
            CreamToolkit.logMessage(e);
        }
    }

    @Override
    public void clearText() throws JposException {
        try {
            outputStream.write(12);   //clear display screen, and clear string mode
            sleep();
        } catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
        }
    }

    @Override
    public void displayText(String data, int attribute) throws JposException {
        try {
            if (DEBUG) System.out.println("CD7110> attr=" + attribute + " |　data=" + data);

            boolean initState = attribute == 1; // For show welcome message
            if (initState)
                data = "      0.00";

            // 4、显示数字
            // ESC  Q  A  12.1  CR
            // 格式：
            // [1BH][51H][41H]
            // [31H][32H][2EH]
            // [31H][0DH]

            textLine1 = data;

            outputStream.write(0x1B);
            outputStream.write(0x51);
            outputStream.write(0x41);
            char[] buf = data.toCharArray();
            for (char aBuf : buf)
                outputStream.write(aBuf);
            outputStream.write(0x0D);
	        outputStream.flush();
            sleep();

            final int unitPriceIndicator = 1;
            displayState(
                    initState ? STATE_EMPTY :
                    attribute == STATE_UNIT_PRICE ? unitPriceIndicator :
                    attribute);

            textLine2 = initState ? "[状态灯全灭]" :
                    attribute == STATE_UNIT_PRICE ? "[单价]" :
                    attribute == STATE_TOTAL ? "[合计]" :
                    attribute == STATE_CASH ? "[收款]" :
                    attribute == STATE_CHANGE ? "[找零]" : "??";

        } catch (Exception e) {
            CreamToolkit.logMessage(e.toString());
        }
    }

    @Override
    public void displayTextAt(int y, int x, String data, int attribute) throws JposException {
    }

    public void showSalesInfo(Transaction transaction, LineItem lineItem) {
        try {
            if (DEBUG) System.out.println("CD7110> showSalesInfo");
            clearText();
            displayText(pad(lineItem.getAmount()), STATE_UNIT_PRICE);
        } catch (JposException ignored) {
        }
    }

    private static final HYIDouble ZERO = HYIDouble.zero();
    public void showTenderInfo(Transaction transaction) {
        try {
            if (DEBUG) System.out.println("CD7110> showTenderInfo");
            clearText();

            HYIDouble changeAmount = transaction.getChangeAmount();
            if (changeAmount != null && !changeAmount.equals(ZERO))
                displayText(pad(transaction.getChangeAmount()), STATE_CHANGE);
            else
                displayText(pad(transaction.getNetSalesAmount()), STATE_TOTAL);
        } catch (JposException ignored) {
        }
    }

    private static String pad(HYIDouble x) {
        if (x == null)
            return "          ";
        else {
            return String.format("%1$#10s", x.toString());
        }
    }

    public String getLineDisplayText() {
        return textLine1 + '\n' + textLine2;
    }
}