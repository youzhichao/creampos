package hyi.spos.services;

import hyi.cream.gwt.client.device.KeylockData;
import hyi.spos.JposException;
import hyi.spos.Keylock;
import hyi.spos.KeylockConst;
import hyi.spos.events.StatusUpdateEvent;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Device service for keylock of TEC M-8000.<br/>
 * <p/>
 * What you need:
 * <ol>
 * <li>A JPOS entry of this service in <code>jpos.xml<code></li>
 * <li>Has a running instance of <code>m8000_keylock_emitter</code> installed in
 * <code>/etc/inittab<code> like:<br/>
 *   <code>ke:35:respawn:/root/m8000_keylock_emitter</code></li>
 * <li>A native JNI library <code>libkeylock_m8000.so<code> installed in java.library.path</li>
 * <ol>
 *
 * @author Bruce You
 * @since 2011-09-27
 */
public class TecM8000Keylock extends Keylock {

    private Thread keylockEventListeningThread;
    private Map<String, String> keylockNameMap = new HashMap<String, String>();

    /** Native method implemented in keylock_m8000.c */
    native static int waitForKeylockEvent();

    static {
        // This library could be compiled from keylock_m8000.c into libkeylock_m8000.so
        System.loadLibrary("keylock_m8000");
    }

    /**
     * Constructor with a SimpleEntry.
     *
     * @param entry
     *            The registry entry for Keylock.
     */
    public TecM8000Keylock(Map<String, Object> entry) {
        createKeylockCodeMap(entry);
    }

    @Override
    public void claim(int timeout) throws JposException {
    }

    @Override
    public void close() throws JposException {
        setDeviceEnabled(false);
    }

    @Override
    public void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName);
    }

    @Override
    public void release() throws JposException {
    }

    public int getKeyPosition() throws JposException {
        return keyPosition;
    }

    public int getPositionCount() throws JposException {
        return 6;
    }

    private void createKeylockCodeMap(Map<String, Object> entry) {
        Iterator<String> iter = entry.keySet().iterator();
        while (iter.hasNext()) {
            try {
                String propName = iter.next();
                if (propName.startsWith("NameOf")) {
                    String value = (String)entry.get(propName);
                    keylockNameMap.put(propName.substring(6), value);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String getKeyPositionName(int keyPosition)
    {
        switch (keyPosition) {
        case KeylockConst.LOCK_KP_LOCK:
            return keylockNameMap.get("LOCK");

        case KeylockConst.LOCK_KP_NORM:
            return keylockNameMap.get("NORM");

        case KeylockConst.LOCK_KP_SUPR:
            return keylockNameMap.get("SUPR");

        default:
            return "Other";
        }
    }

    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        this.deviceEnabled = deviceEnabled;
        if (deviceEnabled) {
            if (keylockEventListeningThread == null || !keylockEventListeningThread.isAlive()) {
                keylockEventListeningThread = new Thread(new Runnable() {
                    public void run() {
                        for (;;) {
                            int keylockPos = waitForKeylockEvent();
                            if (keylockEventListeningThread.isInterrupted())
                                break;
                            keyPosition = keylockPos;
                            System.out.println("TecM8000Keylock: fire keylock pos=" + keyPosition);
                            fireStatusUpdateEvent(new StatusUpdateEvent(TecM8000Keylock.this));
                        }
                    }
                });
                keylockEventListeningThread.setName("TecM800KeylockReader");
                keylockEventListeningThread.setDaemon(true);
                keylockEventListeningThread.start();
            }
        } else {
            if (keylockEventListeningThread != null && keylockEventListeningThread.isAlive())
                keylockEventListeningThread.interrupt();
        }
    }

    /**
     * Get POS keylock detail information, used by GWT service.
     */
    @Override
    public KeylockData getKeylockData() {
        KeylockData keylockData = new KeylockData();
        keylockData.setLogicalName(getLogicalDeviceName());


        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_LOCK);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_NORM);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR + 1);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR + 2);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR + 3);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR + 4);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR + 5);

        keylockData.getPositionNames().add(keylockNameMap.get("LOCK"));
        keylockData.getPositionNames().add(keylockNameMap.get("NORM"));
        keylockData.getPositionNames().add(keylockNameMap.get("SUPR"));
        keylockData.getPositionNames().add("K4");
        keylockData.getPositionNames().add("K5");
        keylockData.getPositionNames().add("K6");
        keylockData.getPositionNames().add("K7");
        keylockData.getPositionNames().add("K8");

        return keylockData;
    }
}
