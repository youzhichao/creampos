package hyi.spos.services;

import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.JposConst;

import javax.comm.CommPortIdentifier;
import javax.comm.SerialPort;
import javax.comm.SerialPortEventListener;
import javax.comm.SerialPortEvent;
import java.io.*;
import java.util.TooManyListenersException;
import java.util.List;
import java.util.ArrayList;

/**
 * Singleton class to implement the function of sharing one
 * serial port between the printer and cash drawer.
 */
public class PrinterAndDrawerPort implements SerialPortEventListener {

    private static PrinterAndDrawerPort instance = new PrinterAndDrawerPort();

    private CommPortIdentifier portId = null;
    private SerialPort serialPort = null;
    private OutputStream outputStream = null;
    private InputStream inputStream = null;
    private OutputStreamWriter dataOutput = null;
    private InputStreamReader dataInput = null;
    private List<SerialPortEventListener> listeners = new ArrayList<SerialPortEventListener>();

    private PrinterAndDrawerPort() {
    }

    public static PrinterAndDrawerPort getInstance() {
        return instance;
    }

    public OutputStreamWriter getOutputStreamWriter() {
        return dataOutput;
    }

    public InputStreamReader getInputStreamReader() {
        return dataInput;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getOwnerName() {
        return portId.getCurrentOwner();
    }

    public void addSerialPortEventListener(SerialPortEventListener listener) {
        if (listener != null && !listeners.contains(listener))
            listeners.add(listener);
    }

    public void open(String logicalName, String portName, int baudRate) throws JposException {
        try {
            portId = CommPortIdentifier.getPortIdentifier(portName);
            serialPort = (SerialPort) portId.open(logicalName, 9999);

            outputStream = serialPort.getOutputStream();
            dataOutput = new OutputStreamWriter(outputStream,
                CreamToolkit.getPrinterCharacterEncoding());

            inputStream = serialPort.getInputStream();
            dataInput = new InputStreamReader(inputStream,
                CreamToolkit.getPrinterCharacterEncoding());

            serialPort.setSerialPortParams(baudRate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
               SerialPort.PARITY_NONE);
            //serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_XONXOFF_IN);
            //serialPort.enableReceiveFraming(1);
            serialPort.notifyOnDataAvailable(true);
            serialPort.notifyOnOutputEmpty(true);
            serialPort.notifyOnBreakInterrupt(true);
            serialPort.notifyOnCarrierDetect(true);
            serialPort.notifyOnCTS(true);
            serialPort.notifyOnDSR(true);
            serialPort.notifyOnFramingError(true);
            serialPort.notifyOnOverrunError(true);
            serialPort.notifyOnParityError(true);
            serialPort.notifyOnRingIndicator(true);

            serialPort.addEventListener(this);

        } catch (Exception e) {
            outputStream = null;
            inputStream = null;
            dataOutput = null;
            dataInput = null;
            serialPort = null;
            portId = null;
            CreamToolkit.logMessage("Cannot open: " + portName);
            CreamToolkit.logMessage(e);
            throw new JposException(JposConst.JPOS_E_FAILURE, "Cannot open: " + portName);
        }
    }

    synchronized public boolean isSerialPortOpened() {
        return serialPort != null && dataInput != null;
    }

    public void close() throws JposException {
        try {
            if (serialPort != null)
                serialPort.close();
            if (dataOutput != null)
                dataOutput.close();
            if (dataInput != null)
                dataInput.close();
        } catch (IOException e) {
            CreamToolkit.logMessage(e);
        } finally {
            outputStream = null;
            inputStream = null;
            dataOutput = null;
            dataInput = null;
            serialPort = null;
            portId = null;
        }
    }

    public void serialEvent(SerialPortEvent event) {
        try {
            Thread.sleep(10);
            if (inputStream.available() == 0)
                return;
            //System.out.println("PrinterAndDrawerPort: serialEvent(), inputStream" + inputStream.toString() + ", available=" + inputStream.available());
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (SerialPortEventListener listener : listeners)
            listener.serialEvent(event);

        try {
            while (inputStream.available() > 0)
                inputStream.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}