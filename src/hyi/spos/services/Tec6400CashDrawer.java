package hyi.spos.services;

//for JavaCommAPI
import hyi.spos.CashDrawer;
import hyi.spos.JposConst;
import hyi.spos.JposException;

import java.util.Map;

public class Tec6400CashDrawer extends CashDrawer {
    // --------------------------------------------------------------------------
    // Variables
    // --------------------------------------------------------------------------

    // private OutputStreamWriter dataOutput = null;
    // static private PrinterAndDrawerPort shareOutput = null; //Singleton object;
    private String logicalName = null;//

    // private JposEntry cashDrawerEntry = null;
    // private OutputStream outputStream = null;
    // private InputStream inputStream = null;
    // private String portName = "COM2";

    private Object waitBeep = new Object();// for wait purpose;
    //private boolean drawerClosed;
    
    // --------------------------------------------------------------------------
    // private byte[] readPort = new byte[10];
    // private char statusCode = '0';

    // Constructor
    public Tec6400CashDrawer(Map<String, Object> entry) {
    }

    public Tec6400CashDrawer() {
    }

    // Capabilities
    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties 13
    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE,
                "No service available now!");
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    // Common method -- Not supported
    public void checkHealth(int level) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL,
                "No service available now!");
    }

    public void directIO(int command, int[] data, Object object)
            throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL,
                "No service available now!");
    }

    // Properties specified
    public boolean getCapStatus() throws JposException {
        return true;
    }

    public boolean getDrawerOpened() throws JposException {
        if (hyi.jpos.services.CashDrawerLocal.getdrawerstate() != 0) {
            return true;
        } else {
            return false;
        }
    }

    // Methods
    public void openDrawer() throws JposException {
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_DISABLED,
                    "Cash drawer is not enabled!");
        hyi.jpos.services.CashDrawerLocal.opencashdrawer();
    }


//    class Beep extends Thread {
//        private int beepTimeout;
//
//        private int beepDelay;
//
//        public Beep() {
//            start();
//        }
//
//        public Beep(int beepTimeout, int beepDelay) {
//            this.beepTimeout = beepTimeout;
//            this.beepDelay = beepDelay;
//            start();
//        }
//
//        public void run() {
//            try {
//                long start = System.currentTimeMillis();
//                long waitTime = beepTimeout;
//                try {
//                    while (waitTime > 0) {
//                        if (!getDrawerOpened()) {
//                            drawerClosed = true;
//                            return;
//                            // synchronized (waitBeep) {
//                            // waitBeep.notifyAll();
//                            // return;
//                            // }
//                        }
//                        Thread.sleep(100);
//                        waitTime = beepTimeout
//                                - (System.currentTimeMillis() - start);
//                    }
//                    while (true) {
//                        if (!getDrawerOpened()) {
//                            drawerClosed = true;
//                            return;
//                            // synchronized (waitBeep) {
//                            // waitBeep.notifyAll();
//                            // return;
//                            // }
//                        }
//                        Toolkit.getDefaultToolkit().beep();
//                        sleep(beepDelay);
//                        // System.out.println(beepDelay);
//                    }
//                } catch (JposException je) {
//                    System.out.println(je + this.toString());
//                }
//            } catch (InterruptedException e) {
//                System.out.println(e + this.toString());
//            }
//        }
//    }

    public void open(String logicalName) throws JposException {
        this.logicalName = logicalName;
    }

    public void close() throws JposException {
        if (getClaimed())
            release();
        this.logicalName = null;
    }

    public void claim(int timeout) throws JposException {
    }

    public void release() throws JposException {
    }

    public void deleteInstance() throws JposException {
    }
}
