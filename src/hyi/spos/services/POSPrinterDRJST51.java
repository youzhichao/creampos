package hyi.spos.services;

import hyi.cream.util.CreamToolkit;
import hyi.spos.JposConst;
import hyi.spos.JposException;
import hyi.spos.POSPrinter;

import java.io.*;
import java.util.Iterator;
import java.util.Map;

/**
 * TEC6400 printer.
 *
 * @author Wissly, Bruce You
 * @since 2002/3. Simplified on 2008-8-24.
 */
public class POSPrinterDRJST51 extends POSPrinter {

    private static final String FONT_FILENAME = "gb16x9.bin";

    private StringParsingDRJST51 parse;
    private OutputStreamWriter dataOutput;
    private InputStream inputStream;
    private RandomAccessFile fontFile;
    private OutputStream out = null;
    private PrinterLoggerQueue printerLoggerQueue;

    public POSPrinterDRJST51(Map<String, Object> entry) throws IOException {
        printerLoggerQueue = new PrinterLoggerQueue();
        parse = new StringParsingDRJST51(this);
        fontFile = new RandomAccessFile(FONT_FILENAME, "r");
        parse = new StringParsingDRJST51(this);

        /**
         * Without JavaComm Support
         */
        if (System.getProperties().get("os.name").toString().indexOf("Windows") != -1)
            out = new FileOutputStream("lpt1");
        else
            out = new FileOutputStream("/dev/lp0");

        dataOutput = new OutputStreamWriter(out);

//        CommPortIdentifier portId = null;
//        Enumeration portList = CommPortIdentifier.getPortIdentifiers();
//        boolean yolicaFlag = false;
//        while (portList.hasMoreElements()) {
//            portId = (CommPortIdentifier)portList.nextElement();
//
//            System.out.println("TECPrinter> enumerate port: " + portId.getName());
//
//            if ((portId.getPortType() == CommPortIdentifier.PORT_PARALLEL)
//                && (portId.getName().equals("/dev/lp0"))) {
//                yolicaFlag = true;
//                break;
//            }
//        }
//        if (!yolicaFlag)
//            return;
//        //throw new JposException(JposConst.JPOS_E_ILLEGAL, "Parallel port is not available!");
//
//        try {
//            parallelPort = (ParallelPort)portId.open(logicalName, 10000);
//            out = parallelPort.getOutputStream();
//            inputStream = parallelPort.getInputStream();
//            dataOutput = new OutputStreamWriter(out, "GBK");
//            dataInput  = new InputStreamReader(inputStream, "GBK");
//        } catch (PortInUseException e) {
//            throw new IOException("Parallel port in use.");
//        }
//
//        try {
//            parallelPort.addEventListener(this);
//        } catch (TooManyListenersException e) {
//            //ignore!
//        }
    }

    /**
     * Set printer status to always online and healthy, used by GWT service.
     */
    @Override
    public void setAlwaysHealthy(boolean alwaysHealthy) {
    }

    /**
     * n0: "S"
     * n1: 收執聯位置 "0":定位 "1":未定位
     * n2: 存根聯位置 "0":定位 "1":未定位
     * n3: 紙張是否用完 "0":否 "1":是
     * n4: 印證紙張定位 "0":無紙 "1":有紙
     * n5: 發票印表機發生錯誤 "0":沒發生 "1":發生
     * n6: 是否列印中 "0":否 "1":是
     * n7: 列印超過40行 "0":否 "1":是
     * n8: 資料緩衝區是否已滿 "0":否 "1":是
     * n9: 資料緩衝區是否為空 "0":是 "1":否
     */
    @Override
    public String getCheckHealthText() throws JposException {
        return "S000000000";
    }

    /**
     * Cut paper.
     * When percentage is 99, it immediately cuts;
     * when percentage is 100, it will cut at black mark.
     */
    @Override
    public void cutPaper(int percentage) throws JposException {
        if (percentage == 0)
            return;
        if (percentage < 0 || percentage > 100)
            throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "Illegal argument:" + percentage + " detected!");

        printerLoggerQueue.add("-------CUT PAPER (" + percentage + ")------");

        try {
            if (percentage <= 99) {
                for (int i = 0; i < 6; i++) {
                    out.write('\n');
                }
                dataOutput.write(0x1B);     // full cut
                dataOutput.write('i');
                dataOutput.flush();
            } else {
                out.write(new byte[]{0x1B, (byte)'N'});
                out.write(0x99);                // Journal and receipt both feed, sense and stop at Top Mark
                out.write(0x20);                // Max 32 lines
                for (int i = 0; i < 6; i++) {
                    out.write('\n');
                }
                dataOutput.write(0x1B);
                dataOutput.write('i');
                dataOutput.flush();
            }

        } catch (IOException ie) {
            throw new JposException(JposConst.JPOS_E_FAILURE, "IO error in cutting paper!", ie);
        }
    }

    public void printImmediate(int station, String data) throws JposException {
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_DISABLED, "Printer is not enabled!");
        if (data == null || data.length() == 0)
            return;

        String realCommand = parse.getValidCommand(station, data);
        if (realCommand == null)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid station argument:" + station);

        printerLoggerQueue.add(realCommand);

        try {
            printImmediateWithLineFeed(realCommand);
            dataOutput.write(realCommand);
            dataOutput.flush();
        } catch (IOException ie) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL);
        }
    }

    @Override
    public void printNormal(int station, String data) throws JposException {
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_DISABLED, "Printer is not enabled!");
        if (data == null || data.equals(""))
            return;

        String realCommand = parse.getValidCommand(station, data);
        if (realCommand == null)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid arguments!");

        printerLoggerQueue.add(realCommand);

        try {
            printImmediateWithLineFeed(realCommand);
        } catch (Exception e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "I/O error occured in printing", e);
        }
    }

    @Override
    public void printTwoNormal(int stations, String data1, String data2) throws JposException {
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_DISABLED, "Printer is not enabled!");
        if (data1.equals("") && data2.equals(""))
            return;

        String realCommand = parse.getValidCommand(stations, data1, data2);
        if (realCommand == null)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid arguments!");

        printerLoggerQueue.add(realCommand);

        try {
            printImmediateWithLineFeed(realCommand);
            dataOutput.flush();
        } catch (IOException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "I/O error occured in printing", e);
        }
    }

    @Override
    public String retrievePrintingLines() {
        return printerLoggerQueue.retrieve();
    }

    @Override
    public void claim(int timeout) throws JposException {
    }

    @Override
    public void release() throws JposException {
    }

    @Override
    public void close() throws JposException {
        try {
            if (out != null)
                out.close();
            out = null;
            if (inputStream != null)
                inputStream.close();
            inputStream = null;
            if (dataOutput != null)
                dataOutput.close();
            dataOutput = null;
        } catch (IOException e) {
            CreamToolkit.logMessage(e);
        }
    }

    @Override
    public void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName);
    }

    private void printAlphanumeric(byte b) {
        try {
            out.write(b);
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    private void printBitmap(int[] upperBits, int[] lowerBits) {
        try {
            // print bitmap
            out.write(new byte[] { 0x1B, (byte)'*', 0x03, // 0: 8-dot single density, 1: 8-dot double density
                    (byte)upperBits.length,               // number of dots in horizontal direction
                    0x00                                  // number of another 256 dots in horizontal direction
            });
            for (int i = 0; i < upperBits.length; i++) {
                out.write(upperBits[i]);
                out.write(lowerBits[i]);
            }
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Inner class for iterating Chinese (in GB code) or alphanumeric characters.
     */
    private class CharIterator implements Iterator {

        private byte[] str;
        private int index;
        private byte[] chineseChar = new byte[2];
        private byte[] alphanumChar = new byte[1];

        CharIterator(String str0) {
            if (str0 != null) {
                try {
                    str = str0.getBytes("GBK" /*CreamToolkit.getEncoding()*/);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }

        public boolean hasNext() {
            return (str != null) && (index < str.length);
        }

        public Object next() {
            if (!hasNext())
                return null;

            int b = str[index] & 0xff;
            //System.out.print("b=" + Integer.toHexString(b) + " ");
            if (index + 1 < str.length) {
                int b2 = str[index + 1] & 0xff;
                //System.out.println("b2=" +Integer.toHexString(b2) + " ");
                if ((b >= 0xA1 && b <= 0xA9 || b >= 0xB0 && b <= 0xF7)
                        && // first-byte is a GB code
                        (b2 >= 0xA1 && b2 <= 0xFE) // second-byte is a GB code
                        ) {
                    chineseChar[0] = (byte)b;
                    chineseChar[1] = (byte)b2;
                    index += 2;
                    try {
                        return new String(chineseChar, "ISO-8859-1");
                    } catch (UnsupportedEncodingException e) {
                        return "";
                    }
                }
            }
            alphanumChar[0] = (byte)b;
            index++;
            return new String(alphanumChar);
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private boolean isChineseChar(String str) {
        return (str.length() == 2); // 2-byte if it is a Chinese char
    }

    /**
     * Get the Chinese font bitmap into upperBits and lowerBits
     */
    private void getBitmap(String c, int[] upperBits, int[] lowerBits) {
        byte[] b = new byte[18]; // bitmap bytes read from font file
        int b1 = c.charAt(0);
        int b2 = c.charAt(1);
        int offset;

        if (b1 <= 0xA9)
            offset = ((b1 - 0xA1) * (0xFE - 0xA0) + (b2 - 0xA1)) * 18;
        else
            offset = (9 * (0xFE - 0xA0) + (b1 - 0xB0) * (0xFE - 0xA0) + (b2 - 0xA1)) * 18;
        try {
            fontFile.seek(offset);
            fontFile.read(b);
        } catch (IOException e) {
            e.printStackTrace(System.err);
            return;
        }
        int[] bb = new int[8];
        int i, j, mask;
        for (i = 0; i < 8; i++) {
            mask = 0x80 >>> i;
            for (j = 0; j <= 14; j += 2) {
                bb[j / 2] = ((b[j] & mask) != 0) ? 1 : 0;
            }
            upperBits[i] = bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4 |
                    bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
            lowerBits[i] = ((b[16] & mask) != 0) ? 0x80 : 0x00;
        }
        for (i = 0; i < 8; i++) {
            mask = 0x80 >>> i;

            for (j = 1; j <= 15; j += 2) {

                bb[j / 2] = ((b[j] & mask) != 0) ? 1 : 0;
            }
            upperBits[i + 8] = bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4 |
                    bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
            lowerBits[i + 8] = ((b[17] & mask) != 0) ? 0x80 : 0x00;
        }
    }

    /**
     * Print by bitmap command. With automatically adding an line-feed at end.
     */
    public void printImmediateWithLineFeed(String str) {

        int[] upperBits = new int[18];
        int[] lowerBits = new int[18];

        Iterator iter = new CharIterator(str);
        while (iter.hasNext()) {
            String c = (String)iter.next();

            if (isChineseChar(c)) {
                getBitmap(c, upperBits, lowerBits); // into upperBits and lowerBits
                printBitmap(upperBits, lowerBits);
            } else {
                printAlphanumeric((byte)c.charAt(0));
            }
        }
        //setLineSpace(8);
        ////        printAlphanumeric((byte)'\n');
        //printLowerBits();
        //setLineSpace(3);
        //printAlphanumeric((byte)'\n');
        //setDefaultLineSpace();
    }
}
