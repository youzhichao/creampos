package hyi.spos.services;

import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.spos.JposException;
import hyi.spos.LineDisplay;

import javax.comm.CommPortIdentifier;
import javax.comm.SerialPort;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * Simple Java POS device driver for IBM4840LineDisplay.
 *
 * @author Bruce You
 */
public class PartnerCD7220LineDisplay extends LineDisplay {

    private static final String BLANK20 = "                    ";
    private static int STATE_1 = 1; // 小计
    private static int STATE_2 = 2; // 合计
    private static int STATE_3 = 3; // 单价
    private static int STATE_4 = 4; // 收款
    private static int STATE_5 = 5; // 找零

    private Map<String, Object> registry;
    private CommPortIdentifier commPortId;
    private SerialPort serialPort;
    private OutputStream outputStream;
    private OutputStreamWriter dataOutput;
    private boolean serialPortMissing;
    private String textLine1 = BLANK20;
    private String textLine2 = BLANK20;
    private String itemPattern;
    private String subtotalPattern;
    private String changePattern;
    private String type;

    /**
     * Constructor
     */
    public PartnerCD7220LineDisplay(Map<String, Object> registry) {
        this.registry = registry;
        itemPattern = (String)registry.get("ItemPattern");
        subtotalPattern = (String)registry.get("SubtotalPattern");
        changePattern = (String)registry.get("ChangePattern");
        type = (String)registry.get("Type");
        if (StringUtils.isEmpty(type))
            type = "IBM";

        if (itemPattern == null)
            CreamToolkit.logMessage("Err> Lack 'ItemPattern' property in PartnerCD7220LineDisplay.");
        if (subtotalPattern == null)
            CreamToolkit.logMessage("Err> Lack 'SubtotalPattern' property in PartnerCD7220LineDisplay.");
        if (changePattern == null)
            CreamToolkit.logMessage("Err> Lack 'ChangePattern' property in PartnerCD7220LineDisplay.");
    }

    public void connect() {
        deviceEnabled = false;
        try {
            CreamToolkit.logMessage("PartnerCD7220LineDisplay Serial port:" + registry.get("portName"));
            commPortId = CommPortIdentifier.getPortIdentifier((String)registry.get("portName"));

            // if (commPortId.isCurrentlyOwned()) {
            // logger.debug("lineDisplay error : The device is being use!");
            // return;
            // }

            serialPort = (SerialPort)commPortId.open(getLogicalDeviceName(), 10000);
            outputStream = serialPort.getOutputStream();
            dataOutput = new OutputStreamWriter(outputStream);

            try {
                serialPort.setSerialPortParams(
                        (Integer)registry.get("baudRate"),
                        (Integer)registry.get("dataBits"),
                        (Integer)registry.get("stopBits"),
                        (Integer)registry.get("parity"));
                // serialPort.setFlowControlMode((Integer)registry.get("flowControl"));
            } catch (Exception e) {
                e.printStackTrace();
                // retry to work around the bug of Sun's JavaComm
                try {
                    serialPort.setSerialPortParams(
                            (Integer)registry.get("baudRate"),
                            (Integer)registry.get("dataBits"),
                            (Integer)registry.get("stopBits"),
                            (Integer)registry.get("parity"));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }

            // initialize display
            outputStream.write(27); // initialize display
            outputStream.write(64);
            outputStream.write(12); // 清屏
            sleep();
            outputStream.write(27); // 光标移到第一位
            outputStream.write(98);
            outputStream.write(72);
            sleep();
            outputStream.flush();
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
            serialPortMissing = true;
        }
    }

    private void displayState(int state, int flag) {
        try {
            outputStream.write(27);
            outputStream.write(115);
            outputStream.write(state);
            outputStream.write(flag);
            sleep();
            outputStream.flush();
        } catch (IOException e4) {
            CreamToolkit.logMessage(e4.toString());
        }
    }

    private void sleep() {
        try {
            Thread.currentThread().sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Methods supported by all device services.
    @Override
    public void claim(int timeout) throws JposException {
    }

    @Override
    public void close() throws JposException {
    }

    @Override
    public void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName);
        connect();
    }

    @Override
    public void release() throws JposException {
        try {
            textLine1 = textLine2 = BLANK20;
            if (!serialPortMissing) {
                outputStream.write(12);    //clear display screen, and clear string mode
                outputStream.write(27);    //set cursor OFF
                outputStream.write(95);
                outputStream.write(0);
                sleep();
                dataOutput.close();
                serialPort.close();
                outputStream = null;
                commPortId = null;
                serialPort = null;
                claimed = false;
                deviceEnabled = false;
            }
        } catch (IOException e) {
            CreamToolkit.logMessage(e);
        }
    }

    @Override
    public void clearText() throws JposException {
        try {
            outputStream.write(12);   //clear display screen, and clear string mode
            sleep();
        } catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
        }
    }

    @Override
    public void displayText(String data, int attribute) throws JposException {
        try {
        	System.out.println("---- cd7220 : " + attribute + " |　" + data + " | " + data.length());
        	clearText();

        	displayState(STATE_2, 0);
			displayState(STATE_3, 0);
			displayState(STATE_4, 0);
			displayState(STATE_5, 0);
			if (attribute == 2) { // 单价
				displayState(STATE_3, 1);
			} else if (attribute == 3) { // 合计
				displayState(STATE_2, 1);
			} else if (attribute == 4) { // 收款
				displayState(STATE_4, 1);
			} else if (attribute == 5) { // 找零
				displayState(STATE_5, 1);
			}

        	outputStream.write(27);
	        outputStream.write(81);
	        outputStream.write(65);
	        sleep();
        	char[] buf = data.toCharArray();
        	for (int i = 0; i < buf.length; i++) {
        		outputStream.write(buf[i]);
        	}
	        outputStream.write(13);
	        outputStream.flush();

        } catch (Exception e) {
            CreamToolkit.logMessage(e.toString());
        }
    }

    @Override
    public void displayTextAt(int y, int x, String data, int attribute) throws JposException {
        try {
            //logger.debug("output(" + x + "," + y + "):[" + data + "]");
            System.out.println("output(" + x + "," + y + "):[" + data + "]");
            outputStream.write(27);    //set cursor on x,y
            outputStream.write(108);
            outputStream.write(x + 1);
            outputStream.write(y + 1);
            sleep();
            OutputStreamWriter dataOutput = new OutputStreamWriter(outputStream, "GBK");
            dataOutput.write(data);
            sleep();
            dataOutput.flush();
        } catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
        }
    }

    public void showSalesInfo(Transaction transaction, LineItem lineItem) {
        try {
            clearText();
            displayText(lineItem.getUnitPrice().toString(), 2);
            displayText(lineItem.getAmount().toString(), 3);
            //String line1 = CreamToolkit.dacToString(itemPattern, lineItem);
            //String line2 = CreamToolkit.dacToString(subtotalPattern, transaction);
            //displayText(line1 + line2, 1);
        } catch (JposException e) {
        }
    }

    public void showTenderInfo(Transaction transaction) {
        try {
            clearText();
            displayText(transaction.getChangeAmount().toString(), 5);
            displayText(transaction.getNetSalesAmount().toString(), 3);
            //String line1 = CreamToolkit.dacToString(changePattern, transaction);
            //String line2 = CreamToolkit.dacToString(subtotalPattern, transaction);
            //displayText(line1 + line2, 1);
        } catch (JposException e) {
        }
    }

    public String getLineDisplayText() {
        return textLine1 + '\n' + textLine2;
    }
}