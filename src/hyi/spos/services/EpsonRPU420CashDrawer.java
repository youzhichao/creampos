package hyi.spos.services;

import hyi.cream.util.CreamToolkit;
import hyi.spos.CashDrawer;
import hyi.spos.JposConst;
import hyi.spos.JposException;

import javax.comm.SerialPortEvent;
import javax.comm.SerialPortEventListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.Map;

/**
 * SPOS driver for Epson RP-U420 cash drawer.
 *
 * @author Bruce You
 */
public class EpsonRPU420CashDrawer extends CashDrawer implements SerialPortEventListener {

    private final String ESC_GET_DRAWER_STATUS_3688 = "\u001Bu\u0000\r";
    private final String ESC_GET_DRAWER_STATUS_EPSON = "\u0010\u0004\u0001";
    //private final String ESC_GET_DRAWER_STATUS_EPSON = "\u001D\u0072\u0002";
    private final String ESC_OPEN_DRAWER_3688 = "\u001BG\r";
    private final String ESC_OPEN_DRAWER_EPSON = "\u001B\u0070\u0000\u0032\u00FA";

    private PrinterAndDrawerPort drawerPort;
    private InputStream inputStream;
    private StringBuilder buffer = new StringBuilder();
    private String portName = "COM2";
    private char drawerStatus;
    private String statusCode;
    private boolean statusCodeIsReady;
    private boolean epsonCommand; // Epson command or 3688 command
    private boolean readyToGetResponse;
    private int baudRate;

    public EpsonRPU420CashDrawer(Map<String, Object> entry) {
        this.portName = (String) entry.get("portName");
        String commandSet = (String) entry.get("commandSet");
        epsonCommand = "Epson".equalsIgnoreCase(commandSet);
        try {
            baudRate = Integer.parseInt((String) entry.get("baudRate"));
        } catch (Exception e) {
            baudRate = 19200;
        }
    }

    @Override
    public void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName);
        drawerPort = PrinterAndDrawerPort.getInstance();
        if (!drawerPort.isSerialPortOpened()) {
            try {
                drawerPort.open(logicalName, portName, baudRate);
            } catch (JposException e) {
            }
        }

        if (drawerPort.isSerialPortOpened()) {
            drawerPort.addSerialPortEventListener(this);
            inputStream = drawerPort.getInputStream();
        }
    }

    @Override
    public boolean getDrawerOpened() throws JposException {
        try {
            if (!drawerPort.isSerialPortOpened())
                return false;

            statusCodeIsReady = false;
            OutputStreamWriter dataOutput = drawerPort.getOutputStreamWriter();
            readyToGetResponse = true;
            dataOutput.write(epsonCommand ? ESC_GET_DRAWER_STATUS_EPSON : ESC_GET_DRAWER_STATUS_3688);
            dataOutput.flush();

            synchronized (drawerPort) {
                try {
                    if (!statusCodeIsReady)
                        drawerPort.wait(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            readyToGetResponse = false;

            if (!statusCodeIsReady) { // if still not ready, tell them I'm "closed"
                //System.out.println("EpsonRPU420CashDrawer: statusCodeIsReady is false");
                return false;
            } else {
                //System.out.println("EpsonRPU420CashDrawer: return drawerStatus=" + (int)getDrawerStatus());
                if (epsonCommand)
                    return (getDrawerStatus() & 0x04) != 0;
                else
                    return (getDrawerStatus() == 1);
            }

        } catch (IOException e) {
            CreamToolkit.logMessage(e);
            return false; // if failed, always tell them I'm "closed"
        }
    }

    @Override
    public void openDrawer() throws JposException {
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_DISABLED, "Cash drawer is not enabled!");

        if (!drawerPort.isSerialPortOpened())
            return;

        try {
            OutputStreamWriter dataOutput = drawerPort.getOutputStreamWriter();

            dataOutput.write(epsonCommand ? ESC_OPEN_DRAWER_EPSON : ESC_OPEN_DRAWER_3688);
            dataOutput.flush();
        } catch (IOException e) {
            CreamToolkit.logMessage(e);
        }
    }

    @Override
    public void claim(int timeout) throws JposException {
    }

    @Override
    public void close() throws JposException {
        drawerPort.close();
        inputStream = null;
        drawerPort = null;
    }

    @Override
    public void release() throws JposException {
    }

    public char getDrawerStatus() {
        return drawerStatus;
    }

    public void setDrawerStatus(char drawerStatus) {
        this.drawerStatus = drawerStatus;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public void serialEvent(SerialPortEvent event) {
        synchronized (drawerPort) {
            if (!readyToGetResponse)
                return;
            //try {
            //    System.out.println("EpsonRPU420CashDrawer: serialEvent(), available=" + inputStream.available());
            //} catch (IOException e) {
            //    e.printStackTrace();
            //}

            switch (event.getEventType()) {
                case SerialPortEvent.BI:
                case SerialPortEvent.OE:
                case SerialPortEvent.FE:
                case SerialPortEvent.PE:
                case SerialPortEvent.CD:
                case SerialPortEvent.CTS:
                case SerialPortEvent.DSR:
                case SerialPortEvent.RI:
                case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;

                case SerialPortEvent.DATA_AVAILABLE:
                    try {
                        if (epsonCommand) {
                            buffer.setLength(0);
                            //System.out.println("EpsonRPU420CashDrawer: reading drawer status, inputStream" + inputStream.toString() + ", available=" + inputStream.available());
                            char ch = (char)inputStream.read();
                            //System.out.println("EpsonRPU420CashDrawer: get drawer status=" + (int)ch);
                            buffer.append(ch);
                        } else {
                            int init = inputStream.available();
                            while (inputStream.available() > 0) {
                                char x = (char) inputStream.read();
                                if (init > 1) {
                                    if (x == 83 && buffer.length() > 0)
                                        buffer.setLength(0);
                                    buffer.append(x);
                                } else {
                                    if (buffer.length() > 0)
                                        buffer.setLength(0);
                                    buffer.append(x);
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (buffer.length() == 0)
                        return;
                    if (buffer.charAt(buffer.length() - 1) == '\r')
                        setStatusCode(buffer.charAt(1) + "");
                    else
                        setDrawerStatus(buffer.charAt(0));

                    System.out.println("EpsonRPU420CashDrawer: notifyAll()");
                    drawerPort.notifyAll();
                    statusCodeIsReady = true;
    
                    break;

                default:
                    break;
            }
            readyToGetResponse = false;
        }
    }
}
