package hyi.spos.services;

import hyi.spos.JposConst;
import hyi.spos.JposException;
import hyi.spos.Keylock;
import hyi.spos.KeylockConst;
import hyi.spos.events.StatusUpdateEvent;
import hyi.cream.gwt.client.device.KeylockData;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.*;


/**
 * This is device service for Generic Keylock.
 * <P>
 * <br>
 * The following properties are supported:
 * <li> FreezeEvents
 * <p>
 * <br>
 * The followings are the limitation of current implementation:
 * <li> CapPowerReporting is JPOS_PR_NONE.
 * <li> PowerNotify is JPOS_PN_DISABLED.
 * <li> PowerState is JPOS_PS_UNKNOWN.
 * <li> CapCompareFirmwareVersion is false
 * <li> CapStatisticsReporting is false
 * <li> CapUpdateFirmware is false
 * <li> CapUpdateStatistics is false
 * <li> Do not generate DirectIOEvent.
 * <p>
 * <br>
 * Note: Because this JavaPOS device service intercept and get keyboard event from AWT thread,
 * application cannot receive any POSKeyboard/Keylock/MSR event from within AWT event processing.
 * Our JavaPOS user should not expect to get any JavaPOS event within their AWT event processing
 * method. <br/><p/>
 * 
 * Sample JCL config:
 * 
 * <pre>
 * &lt;JposEntry logicalName="GenericKeylock"&gt;
 *    &lt;creation factoryClass="hyi.jpos.loader.ServiceInstanceFactory"
 *          serviceClass="hyi.jpos.services.GenericKeylock"/&gt;
 *    &lt;vendor name="HYI" url="http://www.hyi.com.tw"/&gt;
 *    &lt;jpos category="Keylock" version="1.9"/&gt;
 *    &lt;product description="Posiflex KB6600 Keylock" name="Posiflex KB6600 Keylock"
 *         url="http://www.hyi.com.tw"/&gt;
 *
 *      &lt;prop name="PrefixCharacter" type="Integer" value="48" /&gt;
 *      &lt;prop name="KeylockCharCount" type="Integer" value="4" /&gt;
 *      &lt;prop name="K48,50,53,54" type="String" value="LOCK" /&gt;
 *      &lt;prop name="K48,50,53,48" type="String" value="NORM" /&gt;
 *      &lt;prop name="K48,50,53,49" type="String" value="SUPR" /&gt;
 *      &lt;prop name="K48,50,53,50" type="String" value="SUPR+1" /&gt;
 *      &lt;prop name="K48,50,53,51" type="String" value="SUPR+2" /&gt;
 *      &lt;prop name="K48,50,53,52" type="String" value="SUPR+3" /&gt;
 *      &lt;prop name="K48,50,53,53" type="String" value="SUPR+4" /&gt;
 *      &lt;prop name="K48,50,53,55" type="String" value="SUPR+5" /&gt;
 *      &lt;prop name="K48,50,53,56" type="String" value="SUPR+6" /&gt;
 *
 * &lt;/JposEntry&gt;
 * </pre>
 * @author Bruce You @ Hongyuan Software
 * @since 2007-2-13
 */
public class GenericKeylock extends Keylock {

    private int keylockStartCode;
    private int keylockCharCount;

    private int keyPosition = -1; // an unknown position
    private int positionCount;
    private KeyEventDispatcher keyEventInterceptor;
    private List eventQueue = new ArrayList();
    private Map keylockCodeMap = new HashMap();
    private Map<String, String> keylockNameMap = new HashMap();
    private boolean inWaitForKeylockChange;

    /**
     * Default constructor.
     */
    private GenericKeylock() {
//        setDeviceServiceDescription("Generic Keylock JavaPOS Device Service from HYI");
//        setPhysicalDeviceDescription("Generic Keylock");
//        setPhysicalDeviceName("Generic Keylock");
        createKeyEventInterceptor();
        setState(JposConst.JPOS_S_CLOSED);
    }

    /**
     * Constructor with a SimpleEntry.
     * 
     * @param entry
     *            The registry entry for Keylock.
     */
    public GenericKeylock(Map<String, Object> entry) {
        this();
        createKeylockCodeMap(entry);
    }

    private void createKeylockCodeMap(Map<String, Object> entry) {
        keylockStartCode = (Integer)entry.get("PrefixCharacter");
        keylockCharCount = (Integer)entry.get("KeylockCharCount");

//        Enumeration<String> iter = entry.getPropertyNames();
        Iterator<String> iter = entry.keySet().iterator();
        while (iter.hasNext()) {
            try {
                String propName = iter.next();
                //System.out.println("GenericKeylock: propName=" + propName);
                if (propName.startsWith("K") && !propName.equals("KeylockCharCount")) {
                    String value = (String)entry.get(propName);
                    int keylockValue = -1;
                    if (value.equals("LOCK"))
                        keylockValue = KeylockConst.LOCK_KP_LOCK;
                    else if (value.equals("NORM"))
                        keylockValue = KeylockConst.LOCK_KP_NORM;
                    else if (value.equals("SUPR"))
                        keylockValue = KeylockConst.LOCK_KP_SUPR;
                    else if (value.startsWith("SUPR+")) {
                        keylockValue = KeylockConst.LOCK_KP_SUPR
                            + Integer.parseInt(value.substring("SUPR+".length()));
                    }
                    if (keylockValue != -1) {
                        keylockCodeMap.put(propName.substring(1), keylockValue);
                        // like: put("48,50,53,54", 1)
                        //System.out.println("GenericKeylock: " + propName.substring(1) + "," + keylockValue);
                        positionCount = Math.max(positionCount, keylockValue);
                    }
                } else if (propName.startsWith("NameOf")) {
                    String value = (String)entry.get(propName);
                    keylockNameMap.put(propName.substring(6), value);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String getKeyPositionName(int keyPosition)
    {
        switch (keyPosition) {
        case KeylockConst.LOCK_KP_LOCK:
            return keylockNameMap.get("LOCK");

        case KeylockConst.LOCK_KP_NORM:
            return keylockNameMap.get("NORM");

        case KeylockConst.LOCK_KP_SUPR:
            return keylockNameMap.get("SUPR");

        default:
            return "Other";
        }
    }

    /**
     * Get POS keylock detail information, used by GWT service.
     */
    @Override
    public KeylockData getKeylockData() {
        KeylockData keylockData = new KeylockData();
        keylockData.setLogicalName(getLogicalDeviceName());


        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_LOCK);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_NORM);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR + 1);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR + 2);

        keylockData.getPositionNames().add(keylockNameMap.get("LOCK"));
        keylockData.getPositionNames().add(keylockNameMap.get("NORM"));
        keylockData.getPositionNames().add(keylockNameMap.get("SUPR"));
        keylockData.getPositionNames().add("L3");
        keylockData.getPositionNames().add("L4");

        return keylockData;
    }

    public void fireEvent(int keyPosition) {
        this.keyPosition = keyPosition;
        if (inWaitForKeylockChange)
            notifyAll();
        else
           fireStatusUpdateEvent(new StatusUpdateEvent(this));
    }

    /**
     * Fire the the keylock StatusUpdateEvent according to keylock code sequence.
     * 
     * @param keylockData
     *            Keylock code sequence.
     */
    synchronized private void fireKeylockStatusUpdateEvent(String keylockData) {
        Integer pos = (Integer) keylockCodeMap.get(keylockData);
        if (pos != null)
            fireEvent(pos);
    }

    /**
     * Create a keyboard event interceptor for firing Keylock event. The interceptor is an AWT's
     * KeyEventDispatcher, and then it'll be added into KeyboardFocusManager when enabling this
     * device.
     */
    private void createKeyEventInterceptor() {

        keyEventInterceptor = new KeyEventDispatcher() {
            boolean withinControlSeq;

            StringBuffer keylockData = new StringBuffer(16);

            int waitLastTwoEvents;

            public boolean dispatchKeyEvent(KeyEvent e) {
                try {
                    setState(JposConst.JPOS_S_BUSY);

                    if (e.getID() != KeyEvent.KEY_PRESSED) {

                        if (waitLastTwoEvents > 0) {
                            waitLastTwoEvents--;
                            return true;
                        }

                        // While in-between control sequence of keylock, return true
                        // to absort any key event for preventing from sending them
                        // to application
                        return withinControlSeq;
                    }

                    int code = e.getKeyCode();

                    //System.out.println("GenericKeylock: Recieve key=" + code);

                    if (code == keylockStartCode) {
                        keylockData.setLength(0);
                        keylockData.append(code);
                        withinControlSeq = true;
                        return true;

                    } else if (withinControlSeq) {
                        keylockData.append(',');
                        keylockData.append(code);

                        if (keylockData.toString().split(",").length == keylockCharCount) { // End Symbol
                            withinControlSeq = false;
    
                            //System.out.println("GenericKeylock: keylock keys=" + keylockData);
                            
                            // The last char is KEYLOCK_END_CODE, but AWT system will still generate
                            // two KeyEvents for it: one is KEY_TYPED, and follow by a KEY_RELEASE.
                            // We also want to ignore and absort those two KeyEvents, so we set a
                            // count here for doing this.
                            waitLastTwoEvents = 2;
    
                            if (getFreezeEvents()) {
                                // Append into event queue if now the
                                // FreezeEvent is true
                                eventQueue.add(keylockData.toString());
                            } else {
                                // Fire the DataEvent
                                fireKeylockStatusUpdateEvent(keylockData.toString());
                            }
                        }
                        return true;
                    }
                    return false;
                } catch (JposException e1) {
                    e1.printStackTrace();
                    return false;
                } finally {
                    setState(JposConst.JPOS_S_IDLE);
                }
            }
        };
    }

    /**
     * Indicate the ServiceInstance that its connection has been dropped
     * 
     * @since 0.1 (Philly 99 meeting)
     * @throws jpos.JposException
     *             if any error occurs
     */
    public void deleteInstance() throws JposException {
    }

    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties

    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        if (getDeviceEnabled()) {
            System.out.println("GenericKeylock is already enabled.");
            return;
        }
        super.setDeviceEnabled(deviceEnabled);

        // Setup a key event interceptor when enabling device
        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        if (deviceEnabled)
            kfm.addKeyEventDispatcher(keyEventInterceptor);
        else
            kfm.removeKeyEventDispatcher(keyEventInterceptor);
    }

    private void fireEventsInQueue() {
        try {
            setState(JposConst.JPOS_S_BUSY);
            if (getDeviceEnabled() && !getFreezeEvents()) {
                while (!eventQueue.isEmpty()) {
                    fireKeylockStatusUpdateEvent((String) eventQueue.get(0));
                    eventQueue.remove(0);
                }
            }
        } catch (JposException e) {
            e.printStackTrace();
        } finally {
            setState(JposConst.JPOS_S_IDLE);
        }
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        //System.out.println("St7000Keylock: setFreezeEvents()");
        super.setFreezeEvents(freezeEvents);
        fireEventsInQueue();
    }

    public int getKeyPosition() throws JposException {
        return keyPosition;
    }

    public int getPositionCount() throws JposException {
        return positionCount;
    }

    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    public void claim(int timeout) throws JposException {
    }

    public void checkHealth(int level) throws JposException {
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
    }

    public void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName);
        setState(JposConst.JPOS_S_IDLE);
    }

    public void close() throws JposException {
        setDeviceEnabled(false);
        setState(JposConst.JPOS_S_CLOSED);
    }

    public void release() throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL);
    }

    synchronized public void waitForKeylockChange(int position, int timeout) throws JposException {
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Keylock is not enabled.");
        if (getFreezeEvents())
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Keylock is freezed now.");
        if (timeout < -1)
            throw new JposException(JposConst.JPOS_E_ILLEGAL,
                "waitForKeylockChange's timeout value is not valid.");

        if (position == getKeyPosition())
            return;

        inWaitForKeylockChange = true;

        if (timeout == JposConst.JPOS_FOREVER) {
            try {
                while (position != getKeyPosition()) {
                    wait();
                }
                inWaitForKeylockChange = false;
                return;
            } catch (InterruptedException e) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "waitForKeylockChange is being interrupted.");
            }
        } else {
            try {
                long start = System.currentTimeMillis();
                long waitTime = timeout;

                while (waitTime > 0) {
                    wait(waitTime);
                    if (position == getKeyPosition()) {
                        inWaitForKeylockChange = false;
                        return;
                    }
                    waitTime = timeout - (System.currentTimeMillis() - start);
                }
                inWaitForKeylockChange = false;
                throw new JposException(JposConst.JPOS_E_TIMEOUT, "waitForKeylockChange() timeout.");
            } catch (InterruptedException ex) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "waitForKeylockChange is being interrupted.");
            }
        }
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }
}