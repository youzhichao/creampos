package hyi.spos.services;

import hyi.cream.util.CreamToolkit;
import hyi.spos.JposConst;
import hyi.spos.JposException;
import hyi.spos.POSPrinter;

import javax.comm.CommPortIdentifier;
import javax.comm.ParallelPort;
import javax.comm.ParallelPortEvent;
import javax.comm.ParallelPortEventListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.util.Map;

/**
 * SPOS driver for parallel port printer.
 *
 * @author Bruce You
 */
public class ParallelPortPOSPrinter extends POSPrinter implements ParallelPortEventListener {

    private String portName;
    private String encoding;
    ParallelPort parallelPort;
    private OutputStream outputStream;
    private OutputStreamWriter dataOutput;
    private boolean alwaysHealthy;
    private boolean parallelPortMissing;
    private PrinterLoggerQueue printerLoggerQueue;

    public ParallelPortPOSPrinter(Map<String, Object> entry) {
        printerLoggerQueue = new PrinterLoggerQueue();
        portName = (String) entry.get("portName");
        encoding = (String) entry.get("encoding");
    }

    public void openParallelPort() throws JposException {
        try {
            /*
            CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(portName);
            ParallelPort parallelPort = (ParallelPort) portId.open(logicalName, 9999);
            outputStream = parallelPort.getOutputStream();
            dataOutput = new OutputStreamWriter(outputStream, encoding);
            //inputStream = parallelPort.getInputStream();
            //dataInput = new InputStreamReader(inputStream, encoding);
            parallelPort.addEventListener(this);
            */

            outputStream = new FileOutputStream(portName);
            dataOutput = new OutputStreamWriter(outputStream, encoding);

        } catch (Exception e) {
            parallelPortMissing = true;
            outputStream = null;
            dataOutput = null;
            CreamToolkit.logMessage(e);
            throw new JposException(JposConst.JPOS_E_FAILURE, "Cannot open: " + portName);
        }
    }


    /**
     * Set printer status to always online and healthy, used by GWT service.
     */
    public void setAlwaysHealthy(boolean alwaysHealthy) {
        this.alwaysHealthy = alwaysHealthy;
    }

    /**
     * n0: "S"
     * n1: 收執聯位置 "0":定位 "1":未定位
     * n2: 存根聯位置 "0":定位 "1":未定位
     * n3: 紙張是否用完 "0":否 "1":是
     * n4: 印證紙張定位 "0":無紙 "1":有紙
     * n5: 發票印表機發生錯誤 "0":沒發生 "1":發生
     * n6: 是否列印中 "0":否 "1":是
     * n7: 列印超過40行 "0":否 "1":是
     * n8: 資料緩衝區是否已滿 "0":否 "1":是
     * n9: 資料緩衝區是否為空 "0":是 "1":否
     */
    public String getCheckHealthText() throws JposException {
        return "S000000000";
    }

    @Override
    public int getPowerState() {
        return JposConst.JPOS_PS_ONLINE;
    }

    @Override
    public void cutPaper(int percentage) throws JposException {
    }

    public void printImmediate(int station, String data) throws JposException {
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_DISABLED, "Printer is not enabled!");

        if (data == null || data.length() == 0)
            return;

        printerLoggerQueue.add(data);

        if (parallelPortMissing) {
            if (alwaysHealthy)
                return;
            else
                throw new JposException(JposConst.JPOS_E_FAILURE, "Printer is not ready!");
        }

        try {
            dataOutput.write(data);
            dataOutput.flush();
        } catch (IOException e) {
            e.printStackTrace();
            throw new JposException(JposConst.JPOS_E_ILLEGAL);
        }
    }

    @Override
    public void printNormal(int station, String data) throws JposException {
        printImmediate(station, data);
    }

    @Override
    public void printTwoNormal(int station, String data1, String data2) throws JposException {
        String data = (data1 == null || data1.length() == 0) ? data2 : data1;
        printImmediate(station, data);
    }

    @Override
    public String retrievePrintingLines() {
        return printerLoggerQueue.retrieve();
    }

    @Override
    public void claim(int timeout) throws JposException {
    }

    @Override
    public void release() throws JposException {
    }

    @Override
    public void close() throws JposException {
        //printerPort = null;
        outputStream = null;
        dataOutput = null;
        parallelPort.close();
    }

    @Override
    public void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName);
        openParallelPort();
    }

    public void parallelEvent(ParallelPortEvent parallelPortEvent) {
        System.out.println("ParallelPortPOSPrinter> eventType=" + parallelPortEvent.getEventType());
    }
}