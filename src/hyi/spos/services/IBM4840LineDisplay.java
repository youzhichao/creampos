package hyi.spos.services;

import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.spos.JposException;
import hyi.spos.LineDisplay;

import javax.comm.CommPortIdentifier;
import javax.comm.SerialPort;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * Simple Java POS device driver for IBM4840LineDisplay.
 *
 * @author Bruce You
 */
public class IBM4840LineDisplay extends LineDisplay {

    private static final String BLANK20 = "                    ";
    private boolean claimed = false;
    private Map<String, Object> registry;
    private CommPortIdentifier commPortId;
    private SerialPort serialPort;
    private OutputStream outputStream;
    private OutputStreamWriter dataOutput;
    private boolean serialPortMissing;
    private String textLine1 = BLANK20;
    private String textLine2 = BLANK20;
    private String itemPattern;
    private String subtotalPattern;
    private String changePattern;
    private String type;

    /**
     * Constructor
     */
    public IBM4840LineDisplay(Map<String, Object> registry) {
        this.registry = registry;
        itemPattern = (String)registry.get("ItemPattern");
        subtotalPattern = (String)registry.get("SubtotalPattern");
        changePattern = (String)registry.get("ChangePattern");
        type = (String)registry.get("Type");
        if (StringUtils.isEmpty(type))
            type = "IBM";

        if (itemPattern == null)
            CreamToolkit.logMessage("Err> Lack 'ItemPattern' property in IBM4840LineDisplay.");
        if (subtotalPattern == null)
            CreamToolkit.logMessage("Err> Lack 'SubtotalPattern' property in IBM4840LineDisplay.");
        if (changePattern == null)
            CreamToolkit.logMessage("Err> Lack 'ChangePattern' property in IBM4840LineDisplay.");
    }

    public void connect() {
        deviceEnabled = false;
        try {
            CreamToolkit.logMessage("IBM4840LineDisplay Serial port:" + registry.get("portName"));
            commPortId = CommPortIdentifier.getPortIdentifier((String)registry.get("portName"));

            // if (commPortId.isCurrentlyOwned()) {
            // logger.debug("lineDisplay error : The device is being use!");
            // return;
            // }

            serialPort = (SerialPort)commPortId.open(getLogicalDeviceName(), 10000);
            outputStream = serialPort.getOutputStream();
            dataOutput = new OutputStreamWriter(outputStream);

            try {
                serialPort.setSerialPortParams(
                        (Integer)registry.get("baudRate"),
                        (Integer)registry.get("dataBits"),
                        (Integer)registry.get("stopBits"),
                        (Integer)registry.get("parity"));
                // serialPort.setFlowControlMode((Integer)registry.get("flowControl"));
            } catch (Exception e) {
                e.printStackTrace();
                // retry to work around the bug of Sun's JavaComm
                try {
                    serialPort.setSerialPortParams(
                            (Integer)registry.get("baudRate"),
                            (Integer)registry.get("dataBits"),
                            (Integer)registry.get("stopBits"),
                            (Integer)registry.get("parity"));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }

            // initialize display
            outputStream.write(0x1f);
            outputStream.write(0x11);
            outputStream.write(0x14);
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
            serialPortMissing = true;
        }
    }

    // Methods supported by all device services.
    @Override
    public void claim(int timeout) throws JposException {
        if (!claimed) {
            //claimedControl = this;
            connect();
            claimed = true;
        }

//        if (claimed) {
//            System.out.println("lineDisplay error : device has been claimed");
//            return;
//        }
//        // synchronized (mutex) {
//        if (claimedControl == null) {
//            claimedControl = (LineDisplay)this;
//            connect();
//            claimed = true;
//            return;
//        }
//        if (timeout < -1) {
//            throw new JposException(JposConst.JPOS_E_ILLEGAL);
//        } else if (timeout == JposConst.JPOS_FOREVER) {
//            try {
//                while (claimedControl != null) {
//                    mutex.wait();
//                }
//                claimedControl = (LineDisplay)this;
//                connect();
//                return;
//            } catch (InterruptedException ex1) {
//                logger.warn("Interrupted exception", ex1);
//            }
//        } else {
//            try {
//                long start = System.currentTimeMillis();
//                long waitTime = timeout;
//
//                while (waitTime > 0) {
//                    mutex.wait(waitTime);
//                    // wait() returns when time is up or being notify().
//
//                    // ///////// plus checking claimedControl with "this"
//                    if (claimedControl == null) {
//                        claimedControl = (LineDisplay)eventCallbacks.getEventSource();
//                        connect();
//                        return;
//                    }
//
//                    // if wait not enough time, it should keep wait() again....
//                    waitTime = timeout - (System.currentTimeMillis() - start);
//                }
//                logger.debug("lineDisplay error : device is busy");
//                return;
//            } catch (InterruptedException ex1) {
//                logger.warn("Interrupted exception", ex1);
//            }
//
//            throw new JposException(JposConst.JPOS_E_TIMEOUT);
//        }
    }

    @Override
    public void close() throws JposException {
        claimed = false;
        deviceEnabled = false;
    }

    @Override
    public void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName);
        //this.setCursorColumn(0);
        //this.setCursorRow(0);
        claimed = false;
        deviceEnabled = false;
    }

    @Override
    public void release() throws JposException {
        if (claimed) {
            try {
                textLine1 = textLine2 = BLANK20;
                if (!serialPortMissing) {
                    outputStream.write(12); // clear display screen, and clear string mode
                    outputStream.write(27); // set cursor OFF
                    outputStream.write(95);
                    outputStream.write(0);
                    dataOutput.close();
                    serialPort.close();
                    outputStream = null;
                    commPortId = null;
                    serialPort = null;
                    claimed = false;
                    deviceEnabled = false;
                }
            } catch (IOException e) {
                CreamToolkit.logMessage(e);
            }
        }
    }

    @Override
    public void clearText() throws JposException {
        if (deviceEnabled) {
            try {
                textLine1 = textLine2 = BLANK20;
                if (!serialPortMissing) {
                    outputStream.write(12); // clear display screen, and clear string mode
                    //setCursorColumn(0);
                    //setCursorRow(0);
                }
            } catch (IOException e) {
                CreamToolkit.logMessage(e);
            }
        } else {
            CreamToolkit.logMessage("IBM4840LineDisplay.clearText(): Not DeviceEnabled");
        }
    }

    @Override
    public void displayText(String data, int lineNo) throws JposException {
        if (!claimed) {
            CreamToolkit.logMessage("IBM4840LineDisplay.displayText(): Please claim it first!");
            return;
        }
        if (!deviceEnabled) {
            CreamToolkit.logMessage("IBM4840LineDisplay.displayText(): Not setDeviceEnable");
            return;
        }

        try {
            textLine1 = data.substring(0, Math.min(20, data.length()));
            if (data.length() > 20)
                textLine2 = data.substring(20);

            if (!serialPortMissing) {
                if ("IBM".equals(type)) {
                    dataOutput.write(0x10);
                    dataOutput.write(0x00);
                } else {
                    dataOutput.write(0x0C);
                }
                dataOutput.write(data);
                dataOutput.flush();
            }
        } catch (IOException e) {
            CreamToolkit.logMessage(e);
        }
    }

    @Override
    public void displayTextAt(int y, int x, String data, int attribute) throws JposException {
        if (!claimed) {
            CreamToolkit.logMessage("IBM4840LineDisplay.displayText(): Please claim it first!");
            return;
        }
        if (!deviceEnabled) {
            CreamToolkit.logMessage("IBM4840LineDisplay.displayText(): Not setDeviceEnable");
            return;
        }

        try {
            StringBuilder sb = new StringBuilder(textLine1 + textLine2);
            int pos = x * 20 + y;
            if (pos < 40) {
                sb.replace(pos, pos + data.length(), data);
                if (sb.length() > 40)
                    sb.setLength(40);
                textLine1 = sb.substring(0, Math.min(20, sb.length()));
                if (sb.length() > 20)
                    textLine2 = sb.substring(20);
            }

            if (!serialPortMissing) {
                outputStream.write(27); // set cursor on x,y
                outputStream.write(108);
                outputStream.write(x + 1);
                outputStream.write(y + 1);

                dataOutput.write(data);
                dataOutput.flush();

                //setCursorColumn(x);
                //setCursorRow(y + 1);
            }
        } catch (IOException e) {
            CreamToolkit.logMessage(e);
        }
    }

    public void showSalesInfo(Transaction transaction, LineItem lineItem) {
        try {
            String line1 = CreamToolkit.dacToString(itemPattern, lineItem);
            String line2 = CreamToolkit.dacToString(subtotalPattern, transaction);
            displayText(line1 + line2, 1);
        } catch (JposException e) {
        }
    }

    public void showTenderInfo(Transaction transaction) {
        try {
            String line1 = CreamToolkit.dacToString(changePattern, transaction);
            String line2 = CreamToolkit.dacToString(subtotalPattern, transaction);
            displayText(line1 + line2, 1);
        } catch (JposException e) {
        }
    }

    public String getLineDisplayText() {
        return textLine1 + '\n' + textLine2;
    }
}
