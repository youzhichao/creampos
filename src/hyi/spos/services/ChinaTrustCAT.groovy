package hyi.spos.services

import hyi.cream.util.HYIDouble
import static hyi.cream.util.CreamToolkit.logMessage
import hyi.spos.CAT
import hyi.spos.CATConst
import hyi.spos.JposConst
import hyi.spos.JposException
import org.apache.commons.lang.StringUtils
import java.text.SimpleDateFormat

/**
 * CAT device for Taiwan's "ChinaTrust Commercial Bank EDC."
 *
 * @author Bruce You
 * @since 2008/8/29 下午 12:24:59
 */
class ChinaTrustCAT extends CAT {

    private static final String BLANK = ' ';

    private static final String WORKING_DIR = "." + File.separator + "cat"

    String respCode              // 回覆代碼
    String hostId                // 授權銀行編碼
    String refNo                 // 序號
    HYIDouble bonusPaid          // 紅利抵用的實付金額
    HYIDouble bonusDiscount      // 紅利抵用的折抵金額
    int installmentPeriod        // 期數
    HYIDouble installmentFee     // 手續費
    String installmentType       // 收費方式（'1':一般分期、'2':收費分期）
    String installmentPutOffType // 延後付款方式（'1':無延後付款、'2':延後付款、'3':彈性付款）
    HYIDouble installmentAmount1 // 首期金額（若為彈性付款，此欄位為後期的首期金額）
    HYIDouble installmentAmount2 // 每期金額（若為彈性付款，此欄位為後期的每期金額）
    HYIDouble installmentAmount3 // 彈性付款前期的每期金額
    String rawData               // CAT機返回的原始數據（目前中國信託的應該是144 bytes）

    public boolean waitForCATResponse
    public String requestPacket
    public String responsePacket

    private Map<String, Object> entry
    private boolean useSimulator
    private String portName
    private int requestCardNumberTimeout
    private int transacationTimeout
    private int baudRate
    private int dataBits
    private String parity   // Parity: Parity Odd='O' Even='E' None='N'
    private int stopBits
    private int retryTimes
    private int debugMode   // None=0, Log=1, View=2, Log & View=3

    private errorCodeMap2 = [
        '00': 'OK',                  '54': '卡片過期',
        '01': '請查詢銀行',          '59': '無此帳號',
        '02': '請查詢銀行',          '61': '提款超過限額',
        '25': '請查詢銀行',          '63': '主機安全碼有誤',
        '30': '請查詢銀行',          '64': 'TKF不存在 64',
        '03': '未核准之特約店',      '65': 'PTD不存在 65',
        '04': '非正常卡',            '66': 'KEY不存在 66',
        '41': '非正常卡',            '75': '密碼超過次數',
        '43': '非正常卡',            '77': '總額不符',
        '62': '非正常卡',            '79': '批號已開啟',
        '05': '拒絕交易',            '80': '批號錯誤',
        '12': '本交易不接受',        '85': '無此批號',
        '31': '本交易不接受',        '88': '特約店帳號不符',
        '57': '本交易不接受',        '89': '終端機號錯誤',
        '58': '本交易不接受',        '91': '請查詢發卡銀行',
        '13': '金額不符',            '94': '傳輸重覆',
        '14': '帳號不符',            '95': '檔案傳輸中',
        '15': '發卡行不符',          '96': '主機系統故障',
        '19': '請重試交易',          'TO': 'EDC等待主機超時',
        '21': '無此交易',            'NC': '線路不良',
        '51': '餘額不足',            'RV': '線路不良',
        '55': '密碼錯期']

    private errorCodeMap1 = ['00': '成功授權', '01': '無法授權', '02': '請聯繫銀行', '03': '通訊超時']

    ChinaTrustCAT(Map<String, Object> entry) {
        this.entry = entry

        useSimulator = 'yes'.equalsIgnoreCase(entry.get("UseSimulator"))
        portName = entry.get("PortName")
        requestCardNumberTimeout = entry.get("RequestCardNumberTimeout")
        transacationTimeout = entry.get("TransacationTimeout")
        baudRate = entry.get("BaudRate")
        dataBits = entry.get("DataBits")
        parity = entry.get("Parity")
        stopBits = entry.get("StopBits")
        retryTimes = entry.get("RetryTimes")
        debugMode = entry.get("DebugMode")

        deleteOutdatedFiles()
    }

    // Supported capabilities
    @Override boolean getCapAdditionalSecurityInformation() { return true }
    @Override boolean getCapAuthorizeRefund()               { return true }
    @Override boolean getCapAuthorizeVoid()                 { return true }
    @Override boolean getCapCenterResultCode()              { return true }
    @Override boolean getCapCheckCard()                     { return true }
    @Override boolean getCapInstallments()                  { return true }
    @Override boolean getCapPaymentDetail()                 { return true }
    @Override boolean getCapTransactionNumber()             { return true }
    @Override boolean getCapTrainingMode()                  { return true }

    @Override
    void clearOutput() {
        transactionDateTime = null
        transactionNumber = null
        transactionType = 0
        accountNumber = null
        expireDate = null
        terminalId = null
        additionalSecurityInformation = null
        approvalCode = null
        cardCompanyID = null
        centerResultCode = null
        dailyLog = null
        paymentCondition = 0
        paymentMedia = 0
        paymentDetail = null
        sequenceNumber = 0
        slipNumber = null
        trainingMode = false
        respCode = null
        hostId = null
        refNo = null
        bonusPaid = null
        bonusDiscount = null
        installmentPeriod = 0
        installmentFee = null
        installmentType = null
        installmentPutOffType = null
        installmentAmount1 = null
        installmentAmount2 = null
        installmentAmount3 = null
        rawData = null
    }

    /**
     * Convert "000000001234" to HYIDouble of value 12.34.
     */
    private stringToHYIDouble(String amount) {
        try {
            return new HYIDouble(new StringBuilder(amount).insert(10, '.').toString())
        } catch (Exception e) {
            return new HYIDouble(0)
        }
    }

    /**
     * CAT sales.
     *
     * @param sequenceNumber the original invoice no.
     */
    void authorizeSales(int sequenceNumber, HYIDouble amount, long taxOthers, int timeout)
        throws JposException {

        amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP)

        // preparing the request packet
        transactionType   = CATConst.CAT_TRANSACTION_SALES
        cardCompanyID     = BLANK * 2
        transactionNumber = BLANK * 6      // as invoiceNo 調閱編號
        accountNumber     = BLANK * 19
        expireDate        = BLANK * 4
        def transAmt      = sprintf("%013.2f", amount.toBigDecimal()) - '.'  // 交易金額：12位數字，兩位小數但不包含小數點
        def transDate     = BLANK * 6
        def transTime     = BLANK * 6
        approvalCode      = BLANK * 9
        def amount1       = BLANK * 12
        respCode          = BLANK * 4
        terminalId        = BLANK * 8
        refNo             = BLANK * 12
        def amount2       = BLANK * 12
        def storeId       = BLANK * 16
        def amount3       = BLANK * 12
        def reserved      = BLANK * 2

        // Please don't indent the second and third lines
        requestPacket = """${getTransactionTypeChars()}${cardCompanyID}${transactionNumber}${accountNumber}${expireDate}\
${transAmt}${transDate}${transTime}${approvalCode}${amount1}${respCode}${terminalId}${refNo}\
${amount2}${storeId}${amount3}${reserved}"""

        // send the request packet to CAT and wait

        if (useSimulator) {
            synchronized (this) {
                waitForCATResponse = true
                wait(); // wait until we get responsePacket
                waitForCATResponse = false
            }
        } else {
            responsePacket = communicateWithCAT(requestPacket)
        }

        def i = 2
        cardCompanyID     = responsePacket[i..<i + 2]; i += 2
        transactionNumber = responsePacket[i..<i + 6]; i += 6
        accountNumber     = responsePacket[i..<i + 19]; i += 19
        expireDate        = responsePacket[i..<i + 4]; i += 4
        i += 12 // skip transAmt
        transDate         = responsePacket[i..<i + 6]; i += 6
        transTime         = responsePacket[i..<i + 6]; i += 6
        approvalCode      = responsePacket[i..<i + 9]; i += 9
        amount1           = responsePacket[i..<i + 12]; i += 12
        respCode          = responsePacket[i..<i + 4]; i += 4
        terminalId        = responsePacket[i..<i + 8]; i += 8
        refNo             = responsePacket[i..<i + 12]; i += 12
        amount2           = responsePacket[i..<i + 12]

        hostId = cardCompanyID 
        centerResultCode = respCode
        rawData = responsePacket
        paymentMedia = CATConst.CAT_MEDIA_CREDIT

        try {
            def year = transDate[0..<2].toInteger() + 2000
            def month = transDate[2..<4].toInteger()
            def day = transDate[4..<6].toInteger()
            def hour = transTime[0..<2].toInteger()
            def minute = transTime[2..<4].toInteger()
            def second = transTime[4..<6].toInteger()
            transactionDateTime = [year - 1900, month - 1, day, hour, minute, second] as Date;
        } catch (Exception e) {
            logMessage(e)
            transactionDateTime = new Date()
        }

        // check to see if it's got a bonus discount
        if (!"03".equals(hostId) && !StringUtils.isEmpty(amount1.trim()) && !StringUtils.isEmpty(amount2.trim())) {
        // hostId="03" 是信用卡分期付款；要非分期付款，而且amount1 and amount2都有值，才是紅利抵用交易
            paymentCondition = CATConst.CAT_PAYMENT_BONUS_COMBINATION_1
            bonusPaid = stringToHYIDouble(amount1)              // 紅利抵用的實付金額
            bonusDiscount = stringToHYIDouble(amount2).negate() // 紅利抵用的折抵金額, 正常交易為負數
        } else {
            paymentCondition = CATConst.CAT_PAYMENT_LUMP
        }
    }

    /**
     * CAT void.
     *
     * @param sequenceNumber the original invoice no.(調閱編號)
     */
    public void authorizeVoid(int sequenceNumber, HYIDouble amount, long taxOthers, int timeout) throws JposException {
        if (sequenceNumber == 0)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "CAT.authorizeRefund() lacks sequenceNumber.")
        if (amount.compareTo(new HYIDouble(0)) >= 0)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "CAT.authorizeRefund() only accept negative amount.")
        if (StringUtils.isEmpty(cardCompanyID))
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "CAT.authorizeRefund() needs host ID.")

        amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP)

        // preparing the request packet --

        transactionType   = CATConst.CAT_TRANSACTION_VOID
        transactionNumber = sprintf('%06d', sequenceNumber)      // as invoiceNo 調閱編號
        accountNumber     = BLANK * 19
        expireDate        = BLANK * 4
        def transAmt      = BLANK * 12
        def transDate     = BLANK * 6
        def transTime     = BLANK * 6
        approvalCode      = BLANK * 9
        def amount1       = BLANK * 12
        respCode          = BLANK * 4
        terminalId        = BLANK * 8
        refNo             = BLANK * 12
        def amount2       = BLANK * 12
        def storeId       = BLANK * 16
        def amount3       = BLANK * 12
        def reserved      = BLANK * 2

        // Please don't indent the second and third lines
        requestPacket = """${getTransactionTypeChars()}${cardCompanyID}${transactionNumber}${accountNumber}${expireDate}\
${transAmt}${transDate}${transTime}${approvalCode}${amount1}${respCode}${terminalId}${refNo}\
${amount2}${storeId}${amount3}${reserved}"""

        // send the request packet to CAT and wait
        if (useSimulator) {
            synchronized (this) {
                waitForCATResponse = true
                wait(); // wait until we get responsePacket
                waitForCATResponse = false
            }
        } else {
            responsePacket = communicateWithCAT(requestPacket)
        }

        def i = 2
        cardCompanyID     = responsePacket[i..<i + 2]; i += 2
        transactionNumber = responsePacket[i..<i + 6]; i += 6
        accountNumber     = responsePacket[i..<i + 19]; i += 19
        expireDate        = responsePacket[i..<i + 4]; i += 4
        i += 12 // skip transAmt
        transDate         = responsePacket[i..<i + 6]; i += 6
        transTime         = responsePacket[i..<i + 6]; i += 6
        approvalCode      = responsePacket[i..<i + 9]; i += 9
        amount1           = responsePacket[i..<i + 12]; i += 12
        respCode          = responsePacket[i..<i + 4]; i += 4
        terminalId        = responsePacket[i..<i + 8]; i += 8
        refNo             = responsePacket[i..<i + 12]; i += 12
        amount2           = responsePacket[i..<i + 12]

        transAmt = amount   // 將交易金額設成傳進來的金額
        hostId = cardCompanyID
        centerResultCode = respCode
        rawData = responsePacket
        paymentMedia = CATConst.CAT_MEDIA_CREDIT

        try {
            def year = transDate[0..<2].toInteger() + 2000
            def month = transDate[2..<4].toInteger()
            def day = transDate[4..<6].toInteger()
            def hour = transTime[0..<2].toInteger()
            def minute = transTime[2..<4].toInteger()
            def second = transTime[4..<6].toInteger()
            transactionDateTime = [year - 1900, month - 1, day, hour, minute, second] as Date;
        } catch (Exception e) {
            logMessage(e)
            transactionDateTime = new Date()
        }

        // check to see if it's got a bonus discount
        if (!StringUtils.isEmpty(amount1.trim()) && !StringUtils.isEmpty(amount2.trim())) {
            paymentCondition = CATConst.CAT_PAYMENT_BONUS_COMBINATION_1
            bonusPaid = stringToHYIDouble(amount1).negate()     // 紅利抵用的實付金額, 退貨交易為負數
            bonusDiscount = stringToHYIDouble(amount2)          // 紅利抵用的折抵金額, 退貨交易為正數
        } else {
            paymentCondition = CATConst.CAT_PAYMENT_LUMP
        }
    }

    /**
     * CAT refund.
     */
    void authorizeRefund(int sequenceNumber, HYIDouble amount, long taxOthers, int timeout) throws JposException {
        if (amount.compareTo(new HYIDouble(0)) >= 0)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "CAT.authorizeRefund() only accept negative amount." + amount) 

        amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP)

        // preparing the request packet --

        def bonusRefund = refNo != null && terminalId != null // 紅利抵用交易退貨

        transactionType   = CATConst.CAT_TRANSACTION_REFUND
        cardCompanyID     = BLANK * 2
        transactionNumber = BLANK * 6      // as invoiceNo 調閱編號
        accountNumber     = BLANK * 19
        expireDate        = BLANK * 4
        def transAmt      = sprintf('%013.2f', amount.negate().toBigDecimal()) - '.'  // 交易金額：12位數字，兩位小數但不包含小數點
        def transDate     = BLANK * 6
        def transTime     = BLANK * 6
        approvalCode      = BLANK * 9
        def amount1       = BLANK * 12
        respCode          = BLANK * 4
        if (!bonusRefund) {
            terminalId    = BLANK * 8
            refNo         = BLANK * 12
        }
        def amount2       = BLANK * 12
        def storeId       = BLANK * 16
        def amount3       = BLANK * 12
        def reserved      = BLANK * 2

        // Please don't indent the second and third lines
        requestPacket = """${getTransactionTypeChars()}${cardCompanyID}${transactionNumber}${accountNumber}${expireDate}\
${transAmt}${transDate}${transTime}${approvalCode}${amount1}${respCode}${terminalId}${refNo}\
${amount2}${storeId}${amount3}${reserved}"""

        // send the request packet to CAT and wait
        if (useSimulator) {
            synchronized (this) {
                waitForCATResponse = true
                wait(); // wait until we get responsePacket
                waitForCATResponse = false
            }
        } else {
            responsePacket = communicateWithCAT(requestPacket)
        }

        def i = 2
        cardCompanyID     = responsePacket[i..<i + 2]; i += 2
        transactionNumber = responsePacket[i..<i + 6]; i += 6
        accountNumber     = responsePacket[i..<i + 19]; i += 19
        expireDate        = responsePacket[i..<i + 4]; i += 4
        i += 12 // skip transAmt
        transDate         = responsePacket[i..<i + 6]; i += 6
        transTime         = responsePacket[i..<i + 6]; i += 6
        approvalCode      = responsePacket[i..<i + 9]; i += 9
        amount1           = responsePacket[i..<i + 12]; i += 12
        respCode          = responsePacket[i..<i + 4]; i += 4
        terminalId        = responsePacket[i..<i + 8]; i += 8
        refNo             = responsePacket[i..<i + 12]; i += 12
        amount2           = responsePacket[i..<i + 12]

        hostId = cardCompanyID
        centerResultCode = respCode
        rawData = responsePacket
        paymentMedia = CATConst.CAT_MEDIA_CREDIT

        try {
            def year = transDate[0..<2].toInteger() + 2000
            def month = transDate[2..<4].toInteger()
            def day = transDate[4..<6].toInteger()
            def hour = transTime[0..<2].toInteger()
            def minute = transTime[2..<4].toInteger()
            def second = transTime[4..<6].toInteger()
            transactionDateTime = [year - 1900, month - 1, day, hour, minute, second] as Date;
        } catch (Exception e) {
            //logMessage(e)
            transactionDateTime = new Date()
        }

        // check to see if it's got a bonus discount
        if (!StringUtils.isEmpty(amount1.trim()) && !StringUtils.isEmpty(amount2.trim())) {
            paymentCondition = CATConst.CAT_PAYMENT_BONUS_COMBINATION_1
            bonusPaid = stringToHYIDouble(amount1).negate()     // 紅利抵用的實付金額, 退貨交易為負數
            bonusDiscount = stringToHYIDouble(amount2)          // 紅利抵用的折抵金額, 退貨交易為正數
        } else {
            paymentCondition = CATConst.CAT_PAYMENT_LUMP
        }
    }

    def getTransactionTypeChars() {
        switch (transactionType) {
        case CATConst.CAT_TRANSACTION_SALES: return '01'
        case CATConst.CAT_TRANSACTION_REFUND: return '02'
        case CATConst.CAT_TRANSACTION_OFFLINE: return '03'
        case CATConst.CAT_TRANSACTION_PREAUTH: return '04'
        case CATConst.CAT_TRANSACTION_VOID: return '30'
        case CATConst.CAT_TRANSACTION_TIP: return '40'
        case CATConst.CAT_TRANSACTION_ADJUST: return '41'
        case CATConst.CAT_TRANSACTION_SETTLEMENT: return '50'
        default: return '??'
        }
    }

    def getCenterResultCodeDescription1() {
        return errorCodeMap1[centerResultCode[2..3]]
    }

    def getCenterResultCodeDescription2() {
        return errorCodeMap2[centerResultCode[0..1]]
    }

    private communicateWithCAT(String requestPacket) {
        createECR_DAT()
        createIN_DAT(requestPacket)
        executeNativeProgram()
        String responsePacket = getCATResponse()
        backupFile()
        return responsePacket
    }

    private boolean createECR_DAT() {
        Writer bw = null
        try {
            FileOutputStream fos = new FileOutputStream([WORKING_DIR, 'ECR.DAT'] as File)
            bw = new BufferedWriter(new OutputStreamWriter(fos, 'latin1'))
            bw.writeLine(portName)
            bw.writeLine(requestCardNumberTimeout.toString())
            bw.writeLine(transacationTimeout.toString())
            bw.writeLine(baudRate.toString())
            bw.writeLine(dataBits.toString() + parity + stopBits.toString())
            bw.writeLine(retryTimes.toString())
            bw.writeLine(debugMode.toString())
            return true
        } catch (Exception e) {
            logMessage(e)
            return false
        } finally {
            if (bw != null)
                bw.close()
        }
    }

    private boolean createIN_DAT(String requestPacket) {
        Writer writer = null
        try {
            FileOutputStream fos = new FileOutputStream([WORKING_DIR, 'IN.DAT'] as File)
            writer = new BufferedWriter(new OutputStreamWriter(fos, "latin1"))
            writer.write requestPacket
            return true;
        } catch (Exception e) {
            logMessage(e)
            return false;
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    private boolean executeNativeProgram() {
        try {
            File executeFile = [WORKING_DIR, "ecr"] as File
            def outFile = [WORKING_DIR, 'OUT.DAT'] as File
            if (outFile.exists() && !outFile.delete()) {
                logMessage('Cannot delete last OUT.DAT')
                return false;
            }

            Runtime runtime = Runtime.getRuntime()
            String commands = executeFile.getAbsolutePath()
            final Process p = runtime.exec(commands, null, executeFile.getParentFile())
            Thread.start {
                try {
                    p.getInputStream().withReader { reader ->
                        def line
                        while ((line = reader.readLine()) != null)
                            println(line)
                    }
                } catch (IOException e) {
                    logMessage(e)
                }
            }

            Thread.start {
                try {
                    p.getErrorStream().withReader { reader ->
                        def line
                        while ((line = reader.readLine()) != null)
                            println(line)
                    }
                } catch (IOException e) {
                    logMessage(e)
                }
            }
            p.waitFor();
            return true;
        } catch (InterruptedException e) {
            logMessage('通訊程式被中斷');
            logMessage(e);
        } catch (Exception e) {
            logMessage(e);
        }
        return false;
    }

    private String getCATResponse() {
        return ([WORKING_DIR, 'OUT.DAT'] as File).text
    }

    /**
     * 將通訊用的檔案備份到指定的備份路徑,檔名加上當前系統時間。
     */
    private void backupFile() {
        File backupPath = new File(WORKING_DIR + File.separator + "log")
        if (!backupPath.exists()) {
            if (!backupPath.mkdirs()) {
                logMessage("Backup path:" + backupPath.getAbsolutePath() + " not exists")
                return
            }
        }

        def dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date())

        File inFile = new File(WORKING_DIR, 'IN.DAT')
        if (inFile.exists())
            inFile.renameTo(new File(backupPath, "IN.DAT.${dateString}"))

        File outFile = new File(WORKING_DIR, 'OUT.DAT')
        if (outFile.exists())
            outFile.renameTo(new File(backupPath, "OUT.DAT.${dateString}"))
    }

    void deleteOutdatedFiles() {
        try {
            long boundary = new Date().getTime() - 3 * 1000L * 3600 * 24;
            new File(WORKING_DIR + File.separator + "log").eachFile {
                if (it.lastModified() < boundary)
                    it.delete();
            }
        } catch (Exception e) {
        }
    }

    void claim(int timeout) throws JposException {
    }

    void close() throws JposException {
    }

    void open(String logicalName) throws JposException {
    }

    void release() throws JposException {
    }
}
