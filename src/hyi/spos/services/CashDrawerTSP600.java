package hyi.spos.services;

import java.util.Map;

public class CashDrawerTSP600 extends hyi.spos.CashDrawer {

    public CashDrawerTSP600(Map<String, Object> entry) {
    }

    public CashDrawerTSP600() {
    }

    public void checkHealth(int level) {
    }

    public void directIO(int command, int[] data, Object object) {
    }

    @Override
    public void open(String logicalName) throws hyi.spos.JposException {

    }

    public boolean getDrawerOpened() {
        return false;
    }

    // Methods
    public void openDrawer() {
        POSPrinterTSP600 p = POSPrinterTSP600.getInstance();
        if (p != null)
            p.openDrawer();
    }

    public void close() {
    }

    public void claim(int timeout) {
    }

    public void release() {
    }
}
