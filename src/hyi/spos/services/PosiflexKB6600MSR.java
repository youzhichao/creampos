package hyi.spos.services;

import hyi.cream.util.CreamToolkit;
import hyi.cream.uibeans.CreditCardForm;
import hyi.spos.JposConst;
import hyi.spos.JposException;
import hyi.spos.MSR;
import hyi.spos.MSRConst;
import hyi.spos.events.DataEvent;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This is device service for Posiflex KB6600 MSR.
 * <p/>
 * <p/>
 * <BR>
 * The following properties are supported:
 * <LI> AutoDisable
 * <LI> DataCount
 * <LI> DataEventEnabled
 * <LI> FreezeEvents
 * <p/>
 * <BR>
 * The followings are the limitation of current implementation:
 * <LI> TracksToRead is always MSRConst.MSR_TR_1.
 * <LI> DecodeData is always true.
 * <LI> CapPowerReporting is JPOS_PR_NONE.
 * <LI> PowerNotify is JPOS_PN_DISABLED.
 * <LI> PowerState is JPOS_PS_UNKNOWN.
 * <LI> CapCompareFirmwareVersion is false
 * <LI> CapStatisticsReporting is false
 * <LI> CapUpdateFirmware is false
 * <LI> CapUpdateStatistics is false
 * <LI> Do not generate DirectIOEvent and StatusUpdateEvent.
 * <p/>
 * <BR>
 * Note: Because this JavaPOS device service intercept and get keyboard event from AWT thread,
 * application cannot receive any POSKeyboard/Keylock/MSR event from within AWT event processing.
 * Our JavaPOS user should not expect to get any JavaPOS event within their AWT event processing
 * method.
 * <p/>
 * <p/>Sample JCL config:
 * <p/>
 * <pre>
 *     &lt;JposEntry logicalName=&quot;PosiflexKB6600MSR&quot;&gt;
 *        &lt;creation factoryClass=&quot;hyi.spos.loader.ServiceInstanceFactory&quot;
 *                  serviceClass=&quot;hyi.spos.services.PosiflexKB6600MSR&quot;/&gt;
 *        &lt;vendor name=&quot;Posiflex&quot; url=&quot;http://www.hyi.com.tw&quot;/&gt;
 *        &lt;jpos category=&quot;MSR&quot; version=&quot;1.9&quot;/&gt;
 *        &lt;product description=&quot;Posiflex KB-6600 MSR&quot; name=&quot;Posiflex KB-6600 MSR&quot;
 *                 url=&quot;http://www.hyi.com.tw&quot;/&gt;
 *     /JposEntry&gt;
 * </pre>
 *
 * @author Bruce You @ Hongyuan Software
 * @since 2007-2-13
 */
public class PosiflexKB6600MSR extends MSR {

    private static final char MSR_START_CHAR = '%';
    private static final char MSR_TRACK1_START_SENTINEL = '%';
    private static final char MSR_TRACK2_START_SENTINEL = ';';
    private static final char MSR_TRACK3_START_SENTINEL = ';';
    private static final char MSR_END_SENTINEL = '?';
    // private static final int FORMAT_JIS_II = 1;
    private static final int FORMAT_ISO = 2;
    // private static final boolean DEBUG = false;

    private boolean autoDisable;
    private boolean dataEventEnabled;
    private KeyEventDispatcher keyEventInterceptor;
    private int tracksToRead = MSRConst.MSR_TR_1;
    private List<String> eventQueue = new ArrayList<String>();
    private byte[] track1Data = new byte[0];
    private String track1DataString = "";
    private byte[] track1DiscretionaryData = new byte[0];
    private byte[] track2Data = new byte[0];
    private String track2DataString = "";
    private byte[] track2DiscretionaryData = new byte[0];
    private int dataFormat = FORMAT_ISO;
    private byte[] track3Data = new byte[0];
    private String track3DataString = "";
    private String accountNumber = "";
    private boolean parseDecodeData;
    private boolean transmitSentinels;
    private String surname = "";
    private String suffix = "";
    private String serviceCode = "";
    private String title = "";
    private String middleInitial = "";
    private String firstName = "";
    private String expirationDate = "";
    private int errorReportingType = MSRConst.MSR_ERT_CARD;

    /**
     * Constructor with a SPOS entry.
     *
     * @param entry The registry entry for Keylock.
     */
    public PosiflexKB6600MSR(Map<String, Object> entry) {
        createKeyEventInterceptor();
        setState(JposConst.JPOS_S_CLOSED);
    }

    private String removeEndSentinelAndLRC(String data) {
        int x = data.lastIndexOf('?');
        if (x != -1)
            data = data.substring(0, x);
        return data;
    }

    private boolean splitIntoTracks(String data) {
        // The first char of data will be MSR_START_CHAR, and the last char will
        // be '\n'

        try {
            String[] tracks = data.substring(1).split("\\?");
            track1DataString = tracks[0];
            if (tracks.length >= 2)
                track2DataString = tracks[1].substring(1);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Split name by this format:<BR>
     * [Surname]/[First Name] [Middle Initial].[Title]
     *
     * @param fullName Name field in track data.
     */
    private void splitName(String fullName) {
        try {
            int idx;

            idx = fullName.indexOf('/');
            if (idx == -1) {
                surname = fullName;
                return;
            }
            surname = fullName.substring(0, idx);
            idx++;

            int idx2 = fullName.lastIndexOf('.');
            if (idx2 != -1) {
                title = fullName.substring(idx2 + 1);
                fullName = fullName.substring(0, idx2 - 1);
            }

            idx2 = fullName.indexOf(' ', idx);
            if (idx2 == -1) {
                firstName = fullName.substring(idx);
                return;
            }
            firstName = fullName.substring(idx, idx2);
            //idx = idx2 + 1;

            middleInitial = fullName.substring(idx2);
        } catch (IndexOutOfBoundsException e) {
            CreamToolkit.logMessage(e);
        }
    }

    private void clearEventData() {
        accountNumber = surname = firstName = middleInitial = title = suffix = expirationDate = "";
        serviceCode = track1DataString = track2DataString = track3DataString = "";
        track1Data = track1DiscretionaryData = track2Data = track2DiscretionaryData = track3Data = new byte[0];
    }

    private void parseAndFireMSRDataEvent(String msrData) {
        clearEventData();
        boolean result = splitIntoTracks(msrData);
        if (!result)
            return;

        if (track1DataString.length() != 0 && (getTracksToRead() & MSRConst.MSR_TR_1) != 0) {
            track1DataString = removeEndSentinelAndLRC(track1DataString);
            // We'll not parse for JIS-II format
            if (getParseDecodeData() && dataFormat == FORMAT_ISO)
                parseTrack1Data(track1DataString);
            if (getTransmitSentinels())
                track1Data = (MSR_TRACK1_START_SENTINEL + track1DataString + MSR_END_SENTINEL)
                    .getBytes();
            else
                track1Data = track1DataString.getBytes();
        }

        if (track2DataString.length() != 0 && (getTracksToRead() & MSRConst.MSR_TR_2) != 0) {
            track2DataString = removeEndSentinelAndLRC(track2DataString);
            if (getParseDecodeData())
                parseTrack2Data(track2DataString);
            if (getTransmitSentinels())
                track2Data = (MSR_TRACK2_START_SENTINEL + track2DataString + MSR_END_SENTINEL)
                    .getBytes();
            else
                track2Data = track2DataString.getBytes();
        }

        if (track3DataString.length() != 0 && (getTracksToRead() & MSRConst.MSR_TR_3) != 0) {
            track3DataString = removeEndSentinelAndLRC(track3DataString);
            // We'll not parse track 3 data
            if (getTransmitSentinels())
                track3Data = (MSR_TRACK3_START_SENTINEL + track3DataString + MSR_END_SENTINEL)
                    .getBytes();
            else
                track3Data = track3DataString.getBytes();
        }

        fireEvent();
        //track1Data.length | track2Data.length << 8 | track3Data.length << 16));
    }

    /**
     * Fire MSR event, used by GWT service.
     */
    public void fireEvent(String accountNumber, String expirationDate) {
        this.accountNumber = accountNumber;
        this.expirationDate = expirationDate;
        fireEvent();
    }

    /**
     * Parse track 1 data in IATA format (ISO 7813).
     *
     * @param msrData Track 1 data without start and end sentinels.
     */
    private void parseTrack1Data(String msrData) {
        try {
            String fullName;
            int idxHead = 0;
            int idxTail;

            // FC: Format Code. 1 character (alphabetic only):
            // A: Reserved for proprietary use of card issuer.
            // B: Bank/financial. This is the format described here.
            // C-M: Reserved for use by ANSI Subcommittee X3B10.
            // N-Z: Available for use by individual card issuers.
            char formatCode = msrData.charAt(idxHead++);

            // If it is not format B, we'll not parse anything
            if (formatCode != 'B' && formatCode != 'b')
                return;

            // PAN: Primary Account Number. Up to 19 digits:
            idxTail = msrData.indexOf('^', idxHead);
            if (idxTail == -1) {
                accountNumber = msrData.substring(idxHead);
                return;
            }
            accountNumber = msrData.substring(idxHead, idxTail);
            idxHead = idxTail + 1;

            // CC: Country Code. 3 digits: Only if PAN starts with 59
            // (MasterCard).
            if (accountNumber.startsWith("59"))
                idxHead += 3;

            // NM: Name. 2-26 characters
            idxTail = msrData.indexOf('^', idxHead);
            if (idxTail == -1) {
                fullName = msrData.substring(idxHead);
                splitName(fullName);
                return;
            }
            fullName = msrData.substring(idxHead, idxTail);
            splitName(fullName);
            idxHead = idxTail + 1;

            // ED: Expiry Date. 4 digits: YYMM. Required by MasterCard and VISA
            expirationDate = msrData.substring(idxHead, idxHead + 4);
            idxHead += 4;

            // SC: Service Code. 3 digits. Required by MasterCard and VISA.
            serviceCode = msrData.substring(idxHead, idxHead + 3);
            idxHead += 3;

            // PVV: Offset or PVV (PIN Verification Value). 5 digits
            idxHead += 5;

            // DD: Discretionary Data. Rest of characters: Reserved for
            // proprietary use of card issuer.
            track1DiscretionaryData = msrData.substring(idxHead).getBytes();

        } catch (StringIndexOutOfBoundsException e) {
            // ignore it
        }
    }

    /**
     * Parse track 2 data in ABA format (ISO 7813).
     *
     * @param msrData Track 2 data without start and end sentinels.
     */
    private void parseTrack2Data(String msrData) {
        try {
            int idxHead = 0;
            int idxTail;

            // PAN: Primary Account Number. Up to 19 digits
            idxTail = msrData.indexOf('^', idxHead);
            if (idxTail == -1) {
                idxTail = msrData.indexOf('=', idxHead);
                if (idxTail == -1) {
                    accountNumber = msrData.substring(idxHead);
                    return;
                }
            }
            accountNumber = msrData.substring(idxHead, idxTail);
            idxHead = idxTail + 1;

            // CC: Country Code. 3 digits: Only if PAN starts with 59
            // (MasterCard).
            if (accountNumber.startsWith("59"))
                idxHead += 3;

            // ED: Expiry Date. 4 digits: YYMM. Required by MasterCard and VISA
            expirationDate = msrData.substring(idxHead, idxHead + 4);
            idxHead += 4;

            // SC: Service Code. 3 digits. Required by MasterCard and VISA.
            serviceCode = msrData.substring(idxHead, idxHead + 3);
            idxHead += 3;

            // PVV: Offset or PVV (PIN Verification Value). 5 digits
            idxHead += 5;

            // DD: Discretionary Data. Rest of characters: Reserved for
            // proprietary use of card issuer.
            track2DiscretionaryData = msrData.substring(idxHead).getBytes();

        } catch (StringIndexOutOfBoundsException e) {
            // ignore it
        }
    }

    /**
     * Create a keyboard event interceptor for accepting and firing MSR event. The interceptor is an
     * AWT's KeyEventDispatcher, and then it'll be added into KeyboardFocusManager when enabling
     * this device.
     */
    private void createKeyEventInterceptor() {

        keyEventInterceptor = new KeyEventDispatcher() {
            boolean withinControlSeq;
            StringBuffer msrData = new StringBuffer();
            int waitLastEvent;

            public boolean dispatchKeyEvent(KeyEvent e) {
                try {
                    setState(JposConst.JPOS_S_BUSY);

                    if (e.getID() != KeyEvent.KEY_TYPED) {
                        if (waitLastEvent > 0) {
                            waitLastEvent--;
                            return true;
                        }

                        // While in-between control sequence of MSR, return true
                        // to absort any key event for preventing from sending
                        // them to application
                        return withinControlSeq;
                    }

                    char keyChar = e.getKeyChar();
                    // System.out.println("char=" + keyChar + "(" + (int)keyChar + ")");

                    if (keyChar == MSR_START_CHAR) {
                        msrData.setLength(0);
                        msrData.append(keyChar);
                        withinControlSeq = true;

                    } else if (withinControlSeq) {
                        msrData.append(keyChar);

                        if (keyChar == 10) { // End Symbol
                            withinControlSeq = false;

                            // The last char is 10, but AWT system will still generate
                            // a KEY_RELEASE event. We also want to ignore and absort this
                            // KeyEvents, so we set a count here for doing this.
                            waitLastEvent = 1;

                            if (getFreezeEvents()) {
                                // Append into event queue if now the FreezeEvent is true
                                eventQueue.add(msrData.toString());
                            } else if (getDataEventEnabled()) {
                                // If AutoDisable is true, then automatically disable myself
                                if (getAutoDisable())
                                    setDeviceEnabled(false);

                                // Fire the DataEvent
                                parseAndFireMSRDataEvent(msrData.toString());
                            }
                        }
                        return true;
                    }
                    return false;
                } catch (JposException e1) {
                    e1.printStackTrace();
                    return false;
                } finally {
                    setState(JposConst.JPOS_S_IDLE);
                }
            }
        };
    }


    @Override
    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        if (getDeviceEnabled()) {
            System.out.println("PosiflexKB6600MSR is already enabled.");
            return;
        }
        super.setDeviceEnabled(deviceEnabled);

        // Setup a key event interceptor when enabling device
        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        if (deviceEnabled)
            kfm.addKeyEventDispatcher(keyEventInterceptor);
        else
            kfm.removeKeyEventDispatcher(keyEventInterceptor);
    }

    public void setAutoDisable(boolean autoDisable) throws JposException {
        this.autoDisable = autoDisable;
    }

    public boolean getAutoDisable() throws JposException {
        return autoDisable;
    }

    public int getDataCount() throws JposException {
        return eventQueue.size();
    }

    @Override
    public boolean getDataEventEnabled() throws JposException {
        return dataEventEnabled;
    }

    private void fireEventsInQueue() {
        try {
            setState(JposConst.JPOS_S_BUSY);
            if (getDeviceEnabled()
                && getDataEventEnabled() && !getFreezeEvents()) {
                while (!eventQueue.isEmpty()) {
                    // If AutoDisable is true, then automatically disable myself
                    if (getAutoDisable())
                        setDeviceEnabled(false);

                    parseAndFireMSRDataEvent(eventQueue.get(0));
                    eventQueue.remove(0);

                    if (getAutoDisable())
                        break;
                }
            }
        } catch (JposException e) {
            e.printStackTrace();
        } finally {
            setState(JposConst.JPOS_S_IDLE);
        }
    }

    @Override
    public void setDataEventEnabled(boolean dataEventEnabled) throws JposException {
        this.dataEventEnabled = dataEventEnabled;
        fireEventsInQueue();
    }

    @Override
    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        super.setFreezeEvents(freezeEvents);
        fireEventsInQueue();
    }

    // Specific properties...

    @Override
    public String getAccountNumber() {
        return accountNumber;
    }

    public boolean getDecodeData() throws JposException {
        // Always DecodeData
        return true;
    }

    //public void setDecodeData(boolean decodeData) throws JposException {
    //}

    public int getErrorReportingType() {
        return errorReportingType;
    }

    public void setErrorReportingType(int errorReportingType) throws JposException {
        this.errorReportingType = errorReportingType;
    }

    @Override
    public String getExpirationDate() {
        return expirationDate;
    }

    public String getFirstName() throws JposException {
        return firstName;
    }

    public String getMiddleInitial() throws JposException {
        return middleInitial;
    }

    public boolean getParseDecodeData() {
        return parseDecodeData;
    }

    public void setParseDecodeData(boolean parseDecodeData) {
        this.parseDecodeData = parseDecodeData;
    }

    public String getServiceCode() throws JposException {
        return serviceCode;
    }

    public String getSuffix() throws JposException {
        return suffix;
    }

    public String getSurname() throws JposException {
        return surname;
    }

    public String getTitle() throws JposException {
        return title;
    }

    @Override
    public byte[] getTrack1Data() throws JposException {
        return track1Data;
    }

    public byte[] getTrack1DiscretionaryData() throws JposException {
        return track1DiscretionaryData;
    }

    @Override
    public byte[] getTrack2Data() throws JposException {
        return track2Data;
    }

    public byte[] getTrack2DiscretionaryData() throws JposException {
        return track2DiscretionaryData;
    }

    @Override
    public byte[] getTrack3Data() throws JposException {
        return track3Data;
    }

    public byte[] getTrack4Data() throws JposException {
        return new byte[0];
    }

    @Override
    public int getTracksToRead() {
        return tracksToRead;
    }

    public void setTracksToRead(int tracksToRead) {
        this.tracksToRead = tracksToRead;
    }

    public void setTransmitSentinels(boolean transmitSentinels) {
        this.transmitSentinels = transmitSentinels;
    }

    public boolean getTransmitSentinels() {
        return transmitSentinels;
    }

    // Methods
    public void deleteInstance() throws JposException {
    }

    @Override
    public void claim(int timeout) throws JposException {
    }

    @Override
    public void open(String logicalName) throws JposException {
        setLogicalDeviceName(logicalName);
        setState(JposConst.JPOS_S_IDLE);
        //setDecodeData(true);
        setParseDecodeData(true);
    }

    @Override
    public void close() throws JposException {
        setDeviceEnabled(false);
        setState(JposConst.JPOS_S_CLOSED);
    }

    @Override
    public void release() throws JposException {
    }

    @Override
    public void clearInput() throws JposException {
        eventQueue.clear();
    }
}