package hyi.spos.services;

import hyi.spos.Keylock;
import hyi.spos.JposConst;
import hyi.spos.KeylockConst;
import hyi.spos.JposException;
import hyi.spos.events.StatusUpdateEvent;

import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * This is device service for Posiflex KB6600 Keylock.
 * <P>
 * <br>
 * The following properties are supported:
 * <li> FreezeEvents
 * <p>
 * <br>
 * The followings are the limitation of current implementation:
 * <li> CapPowerReporting is JPOS_PR_NONE.
 * <li> PowerNotify is JPOS_PN_DISABLED.
 * <li> PowerState is JPOS_PS_UNKNOWN.
 * <li> CapCompareFirmwareVersion is false
 * <li> CapStatisticsReporting is false
 * <li> CapUpdateFirmware is false
 * <li> CapUpdateStatistics is false
 * <li> Do not generate DirectIOEvent.
 * <p>
 * <br>
 * Note: Because this JavaPOS device service intercept and get keyboard event from AWT thread,
 * application cannot receive any POSKeyboard/Keylock/MSR event from within AWT event processing.
 * Our JavaPOS user should not expect to get any JavaPOS event within their AWT event processing
 * method. <br/><p/>
 * 
 * Please set the key lock sequence as follows (` is back-quote):
 * 
 * <pre>
 *  [L0]: `0
 *  [L1]: `1
 *  [L2]: `2
 *  [L3]: `3
 *  [L4]: `4
 *  [LP]: `5
 *
 * Sample JCL config:
 * 
 * &lt;JposEntry logicalName="PosiflexKB6600Keylock"&gt;
 *    &lt;creation factoryClass="hyi.jpos.loader.ServiceInstanceFactory"
 *          serviceClass="hyi.jpos.services.PosiflexKB6600Keylock"/&gt;
 *    &lt;vendor name="Posiflex" url="http://www.hyi.com.tw"/&gt;
 *    &lt;jpos category="Keylock" version="1.9"/&gt;
 *    &lt;product description="Posiflex KB6600 Keylock" name="Posiflex KB6600 Keylock"
 *         url="http://www.hyi.com.tw"/&gt;
 * &lt;/JposEntry&gt;
 * </pre>
 * @author Bruce You @ Hongyuan Software
 * @since 2007-2-13
 */
public class PosiflexKB6600Keylock extends Keylock {

    private static final int KEYLOCK_START_CODE = KeyEvent.VK_BACK_QUOTE;

    private int keyPosition = -1; // an unknown position
    private int positionCount;
    private KeyEventDispatcher keyEventInterceptor;
    private List<String> eventQueue = new ArrayList<String>();
    private Map<String, Integer> keylockCodeMap = new HashMap<String, Integer>();
    private boolean inWaitForKeylockChange;

    /**
     * Default constructor.
     */
    public PosiflexKB6600Keylock() {
        //setDeviceServiceDescription("Posiflex KB6600 Keylock JavaPOS Device Service from HYI");
        //setPhysicalDeviceDescription("Posiflex KB6600 Keylock");
        //setPhysicalDeviceName("Posiflex KB6600 Keylock");
        createKeyEventInterceptor();
        createKeylockCodeMap();
        setState(JposConst.JPOS_S_CLOSED);
    }

    /**
     * Constructor with a SimpleEntry.
     * 
     * @param entry
     *            The registry entry for Keylock.
     */
    public PosiflexKB6600Keylock(Map<String, Object> entry) {
        this();
    }

    private void createKeylockCodeMap() {
        // Keylock response when turning it
    	// Please see (java.awt.event.KeyEvent)!
		// public static final int VK_0 = 0x30;
		// public static final int VK_1 = 0x31;
		// public static final int VK_2 = 0x32;
		// public static final int VK_3 = 0x33;
		// public static final int VK_4 = 0x34;
		// public static final int VK_5 = 0x35;
		// public static final int VK_6 = 0x36;
		//
		// public static final int VK_NUMPAD0 = 0x60;
		// public static final int VK_NUMPAD1 = 0x61;
		// public static final int VK_NUMPAD2 = 0x62;
		// public static final int VK_NUMPAD3 = 0x63;
		// public static final int VK_NUMPAD4 = 0x64;
		// public static final int VK_NUMPAD5 = 0x65;
		// public static final int VK_NUMPAD6 = 0x66;
    	
        keylockCodeMap.put("19248", KeylockConst.LOCK_KP_LOCK); // L0 `0
        keylockCodeMap.put("19296", KeylockConst.LOCK_KP_LOCK); // L0
        keylockCodeMap.put("19249", KeylockConst.LOCK_KP_NORM); // L1 `1
        keylockCodeMap.put("19297", KeylockConst.LOCK_KP_NORM); // L1
        keylockCodeMap.put("19250", KeylockConst.LOCK_KP_SUPR); // L2 `2
        keylockCodeMap.put("19298", KeylockConst.LOCK_KP_SUPR); // L2
        keylockCodeMap.put("19251", KeylockConst.LOCK_KP_SUPR + 1); // L3 `3
        keylockCodeMap.put("19299", KeylockConst.LOCK_KP_SUPR + 1); // L3
        keylockCodeMap.put("19252", KeylockConst.LOCK_KP_SUPR + 2); // L4 `4
        keylockCodeMap.put("192100", KeylockConst.LOCK_KP_SUPR + 2); // L4
        keylockCodeMap.put("19253", KeylockConst.LOCK_KP_SUPR + 3); // LP `5
        keylockCodeMap.put("192101", KeylockConst.LOCK_KP_SUPR + 3); // LP

        // 怕 [LP] 設錯成 `6
        keylockCodeMap.put("19254", KeylockConst.LOCK_KP_SUPR + 3); // LP `6
        keylockCodeMap.put("192102", KeylockConst.LOCK_KP_SUPR + 3); // LP

        positionCount = 6; // KeylockConst.LOCK_KP_SUPR + 3;
    }

    /**
     * Fire the the keylock StatusUpdateEvent according to keylock code sequence.
     * 
     * @param keylockData
     *            Keylock code sequence.
     */
    synchronized private void fireKeylockStatusUpdateEvent(String keylockData) {
        Integer pos = keylockCodeMap.get(keylockData);
        if (pos != null)
            fireEvent(pos);
    }

    public void fireEvent(int keyPosition) {
        this.keyPosition = keyPosition;
        if (inWaitForKeylockChange)
            notifyAll();
        else
           fireStatusUpdateEvent(new StatusUpdateEvent(this));
    }
    
    /**
     * Create a keyboard event interceptor for firing Keylock event. The interceptor is an AWT's
     * KeyEventDispatcher, and then it'll be added into KeyboardFocusManager when enabling this
     * device.
     */
    private void createKeyEventInterceptor() {

        keyEventInterceptor = new KeyEventDispatcher() {
            boolean withinControlSeq;

            StringBuffer keylockData = new StringBuffer(16);

            int waitLastTwoEvents;

            public boolean dispatchKeyEvent(KeyEvent e) {
                try {
                    setState(JposConst.JPOS_S_BUSY);

                    if (e.getID() != KeyEvent.KEY_PRESSED) {

                        if (waitLastTwoEvents > 0) {
                            waitLastTwoEvents--;
                            return true;
                        }

                        // While in-between control sequence of keylock, return true
                        // to absort any key event for preventing from sending them
                        // to application
                        return withinControlSeq;
                    }

                    int code = e.getKeyCode();

                    if (code == KEYLOCK_START_CODE) {
                        keylockData.setLength(0);
                        keylockData.append(code);
                        withinControlSeq = true;
                        return true;

                    } else if (withinControlSeq) {
                        keylockData.append(code);

                        // if (code == KEYLOCK_END_CODE) { // End Symbol
                        withinControlSeq = false;

                        // The last char is KEYLOCK_END_CODE, but AWT system will still generate
                        // two KeyEvents for it: one is KEY_TYPED, and follow by a KEY_RELEASE.
                        // We also want to ignore and absort those two KeyEvents, so we set a
                        // count here for doing this.
                        waitLastTwoEvents = 2;

                        if (getFreezeEvents()) {
                            // Append into event queue if now the
                            // FreezeEvent is true
                            eventQueue.add(keylockData.toString());
                        } else {
                            // Fire the DataEvent
                            fireKeylockStatusUpdateEvent(keylockData.toString());
                        }
                        // }
                        return true;
                    }
                    return false;
                } catch (JposException e1) {
                    e1.printStackTrace();
                    return false;
                } finally {
                    setState(JposConst.JPOS_S_IDLE);
                }
            }
        };
    }

    /**
     * Indicate the ServiceInstance that its connection has been dropped
     * 
     * @since 0.1 (Philly 99 meeting)
     * @throws jpos.JposException
     *             if any error occurs
     */
    public void deleteInstance() throws JposException {
    }

    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties

    public void setDeviceEnabled(boolean deviceEnabled) throws JposException {
        super.setDeviceEnabled(deviceEnabled);

        // Setup a key event interceptor when enabling device
        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        if (deviceEnabled)
            kfm.addKeyEventDispatcher(keyEventInterceptor);
        else
            kfm.removeKeyEventDispatcher(keyEventInterceptor);
    }

    private void fireEventsInQueue() {
        try {
            setState(JposConst.JPOS_S_BUSY);
            if (getDeviceEnabled() && !getFreezeEvents()) {
                while (!eventQueue.isEmpty()) {
                    fireKeylockStatusUpdateEvent(eventQueue.get(0));
                    eventQueue.remove(0);
                }
            }
        } catch (JposException e) {
            e.printStackTrace();
        } finally {
            setState(JposConst.JPOS_S_IDLE);
        }
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        System.out.println("St7000Keylock: setFreezeEvents()");
        super.setFreezeEvents(freezeEvents);
        fireEventsInQueue();
    }

    public int getKeyPosition() throws JposException {
        return keyPosition;
    }

    public int getPositionCount() throws JposException {
        return positionCount;
    }

    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    public void claim(int timeout) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL);
    }

    public void checkHealth(int level) throws JposException {
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
    }

    public void open(String logicalName) throws JposException {
        setState(JposConst.JPOS_S_IDLE);
    }

    public void close() throws JposException {
        setClaimed(false);
        setDeviceEnabled(false);
        setState(JposConst.JPOS_S_CLOSED);
    }

    public void release() throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL);
    }

    synchronized public void waitForKeylockChange(int position, int timeout) throws JposException {
        if (!getDeviceEnabled())
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Keylock is not enabled.");
        if (getFreezeEvents())
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Keylock is freezed now.");
        if (timeout < -1)
            throw new JposException(JposConst.JPOS_E_ILLEGAL,
                "waitForKeylockChange's timeout value is not valid.");

        if (position == getKeyPosition())
            return;

        inWaitForKeylockChange = true;

        if (timeout == JposConst.JPOS_FOREVER) {
            try {
                while (position != getKeyPosition()) {
                    wait();
                }
                inWaitForKeylockChange = false;
                return;
            } catch (InterruptedException e) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "waitForKeylockChange is being interrupted.");
            }
        } else {
            try {
                long start = System.currentTimeMillis();
                long waitTime = timeout;

                while (waitTime > 0) {
                    wait(waitTime);
                    if (position == getKeyPosition()) {
                        inWaitForKeylockChange = false;
                        return;
                    }
                    waitTime = timeout - (System.currentTimeMillis() - start);
                }
                inWaitForKeylockChange = false;
                throw new JposException(JposConst.JPOS_E_TIMEOUT, "waitForKeylockChange() timeout.");
            } catch (InterruptedException ex) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "waitForKeylockChange is being interrupted.");
            }
        }
    }

    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public void compareFirmwareVersion(String firmwareFileName, int[] result) throws JposException {
    }

    public void updateFirmware(String firmwareFileName) throws JposException {
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String statisticsBuffer) throws JposException {
    }

    public void retrieveStatistics(String[] statisticsBuffer) throws JposException {
    }

    public void updateStatistics(String statisticsBuffer) throws JposException {
    }
}