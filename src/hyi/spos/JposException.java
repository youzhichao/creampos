package hyi.spos;

public class JposException extends java.lang.Exception {
    public JposException(int errorCode) {
        this(errorCode, 0, "" + errorCode, null);
    }

    public JposException(int errorCode, int errorCodeExtended) {
        this(errorCode, errorCodeExtended,
             "" + errorCode + ", " + errorCodeExtended, null);
    }

    public JposException(int errorCode, String description) {
        this(errorCode, 0, description, null);
    }

    public JposException(int errorCode, int errorCodeExtended,
        String description) {
        this(errorCode, errorCodeExtended, description, null);
    }

    public JposException(int errorCode, String description,
        Exception origException) {
        this(errorCode, 0, description, origException);
    }

    public JposException(int errorCode, int errorCodeExtended,
        String description, Exception origException) {
        super(description);
        this.errorCode = errorCode;
        this.errorCodeExtended = errorCodeExtended;
        this.origException = origException;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public int getErrorCodeExtended() {
        return errorCodeExtended;
    }

    public Exception getOrigException() {
        return origException;
    }

    protected int errorCode;
    protected int errorCodeExtended;
    private Exception origException;
}
