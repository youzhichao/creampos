package hyi.spos;

public interface LineDisplayConst {
    //###################################################################
    //#### Line Display Constants
    //###################################################################

    /////////////////////////////////////////////////////////////////////
    // "CapBlink" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int DISP_CB_NOBLINK      = 0;
    public static final int DISP_CB_BLINKALL     = 1;
    public static final int DISP_CB_BLINKEACH    = 2;


    /////////////////////////////////////////////////////////////////////
    // "CapCharacterSet" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int DISP_CCS_NUMERIC     =   0;
    public static final int DISP_CCS_ALPHA       =   1;
    public static final int DISP_CCS_ASCII       = 998;
    public static final int DISP_CCS_KANA        =  10;
    public static final int DISP_CCS_KANJI       =  11;


    /////////////////////////////////////////////////////////////////////
    // "CharacterSet" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int DISP_CS_ASCII        = 998;
    public static final int DISP_CS_ANSI         = 999;


    /////////////////////////////////////////////////////////////////////
    // "MarqueeType" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int DISP_MT_NONE         = 0;
    public static final int DISP_MT_UP           = 1;
    public static final int DISP_MT_DOWN         = 2;
    public static final int DISP_MT_LEFT         = 3;
    public static final int DISP_MT_RIGHT        = 4;
    public static final int DISP_MT_INIT         = 5;


    /////////////////////////////////////////////////////////////////////
    // "MarqueeFormat" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int DISP_MF_WALK         = 0;
    public static final int DISP_MF_PLACE        = 1;


    /////////////////////////////////////////////////////////////////////
    // "DisplayText" Method: "Attribute" Property Constants
    // "DisplayTextAt" Method: "Attribute" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int DISP_DT_NORMAL       = 0;
    public static final int DISP_DT_BLINK        = 1;


    /////////////////////////////////////////////////////////////////////
    // "ScrollText" Method: "Direction" Parameter Constants
    /////////////////////////////////////////////////////////////////////

    public static final int DISP_ST_UP           = 1;
    public static final int DISP_ST_DOWN         = 2;
    public static final int DISP_ST_LEFT         = 3;
    public static final int DISP_ST_RIGHT        = 4;


    /////////////////////////////////////////////////////////////////////
    // "SetDescriptor" Method: "Attribute" Parameter Constants
    /////////////////////////////////////////////////////////////////////

    public static final int DISP_SD_OFF          = 0;
    public static final int DISP_SD_ON           = 1;
    public static final int DISP_SD_BLINK        = 2;
}
