package hyi.spos;

public interface POSPrinterConst {
    //###################################################################
    //#### POS Printer Constants
    //###################################################################

    /////////////////////////////////////////////////////////////////////
    // Printer Station Constants
    /////////////////////////////////////////////////////////////////////

    public static final int PTR_S_JOURNAL        = 1;
    public static final int PTR_S_RECEIPT        = 2;
    public static final int PTR_S_SLIP           = 4;

    public static final int PTR_S_JOURNAL_RECEIPT= PTR_S_JOURNAL | PTR_S_RECEIPT;
    public static final int PTR_S_JOURNAL_SLIP   = PTR_S_JOURNAL | PTR_S_SLIP   ;
    public static final int PTR_S_RECEIPT_SLIP   = PTR_S_RECEIPT | PTR_S_SLIP   ;

    public static final int PTR_TWO_RECEIPT_JOURNAL  = 0x8000 + PTR_S_JOURNAL_RECEIPT;
    public static final int PTR_TWO_SLIP_JOURNAL     = 0x8000 + PTR_S_JOURNAL_SLIP   ;
    public static final int PTR_TWO_SLIP_RECEIPT     = 0x8000 + PTR_S_RECEIPT_SLIP   ;


    /////////////////////////////////////////////////////////////////////
    // "CapCharacterSet" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int PTR_CCS_ALPHA        =   1;
    public static final int PTR_CCS_ASCII        = 998;
    public static final int PTR_CCS_KANA         =  10;
    public static final int PTR_CCS_KANJI        =  11;


    /////////////////////////////////////////////////////////////////////
    // "CharacterSet" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int PTR_CS_ASCII         = 998;
    public static final int PTR_CS_ANSI          = 999;


    /////////////////////////////////////////////////////////////////////
    // "ErrorLevel" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int PTR_EL_NONE          = 1;
    public static final int PTR_EL_RECOVERABLE   = 2;
    public static final int PTR_EL_FATAL         = 3;


    /////////////////////////////////////////////////////////////////////
    // "MapMode" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int PTR_MM_DOTS          = 1;
    public static final int PTR_MM_TWIPS         = 2;
    public static final int PTR_MM_ENGLISH       = 3;
    public static final int PTR_MM_METRIC        = 4;


    /////////////////////////////////////////////////////////////////////
    // "CutPaper" Method Constant
    /////////////////////////////////////////////////////////////////////

    public static final int PTR_CP_FULLCUT       = 100;


    /////////////////////////////////////////////////////////////////////
    // "PrintBarCode" Method Constants:
    /////////////////////////////////////////////////////////////////////

    //   "Alignment" Parameter
    //     Either the distance from the left-most print column to the start
    //     of the bar code, or one of the following:

    public static final int PTR_BC_LEFT          = -1;
    public static final int PTR_BC_CENTER        = -2;
    public static final int PTR_BC_RIGHT         = -3;

    //   "TextPosition" Parameter

    public static final int PTR_BC_TEXT_NONE     = -11;
    public static final int PTR_BC_TEXT_ABOVE    = -12;
    public static final int PTR_BC_TEXT_BELOW    = -13;

    //   "Symbology" Parameter:

    //     One dimensional symbologies
    public static final int PTR_BCS_UPCA         = 101;  // Digits
    public static final int PTR_BCS_UPCE         = 102;  // Digits
    public static final int PTR_BCS_JAN8         = 103;  // = EAN 8
    public static final int PTR_BCS_EAN8         = 103;  // = JAN 8 (added in 1.2)
    public static final int PTR_BCS_JAN13        = 104;  // = EAN 13
    public static final int PTR_BCS_EAN13        = 104;  // = JAN 13 (added in 1.2)
    public static final int PTR_BCS_TF           = 105;  // (Discrete 2 of 5) Digits
    public static final int PTR_BCS_ITF          = 106;  // (Interleaved 2 of 5) Digits
    public static final int PTR_BCS_Codabar      = 107;  // Digits, -, $, :, /, ., +;
                                                         //   4 start/stop characters
                                                         //   (a, b, c, d)
    public static final int PTR_BCS_Code39       = 108;  // Alpha, Digits, Space, -, .,
                                                         //   $, /, +, %; start/stop (*)
                                                         // Also has Full ASCII feature
    public static final int PTR_BCS_Code93       = 109;  // Same characters as Code 39
    public static final int PTR_BCS_Code128      = 110;  // 128 data characters
    //        (The following were added in Release 1.2)
    public static final int PTR_BCS_UPCA_S       = 111;  // UPC-A with supplemental
                                                         //   barcode
    public static final int PTR_BCS_UPCE_S       = 112;  // UPC-E with supplemental
                                                         //   barcode
    public static final int PTR_BCS_UPCD1        = 113;  // UPC-D1
    public static final int PTR_BCS_UPCD2        = 114;  // UPC-D2
    public static final int PTR_BCS_UPCD3        = 115;  // UPC-D3
    public static final int PTR_BCS_UPCD4        = 116;  // UPC-D4
    public static final int PTR_BCS_UPCD5        = 117;  // UPC-D5
    public static final int PTR_BCS_EAN8_S       = 118;  // EAN 8 with supplemental
                                                         //   barcode
    public static final int PTR_BCS_EAN13_S      = 119;  // EAN 13 with supplemental
                                                         //   barcode
    public static final int PTR_BCS_EAN128       = 120;  // EAN 128
    public static final int PTR_BCS_OCRA         = 121;  // OCR "A"
    public static final int PTR_BCS_OCRB         = 122;  // OCR "B"


    //     Two dimensional symbologies
    public static final int PTR_BCS_PDF417       = 201;
    public static final int PTR_BCS_MAXICODE     = 202;

    //     Start of Printer-Specific bar code symbologies
    public static final int PTR_BCS_OTHER        = 501;


    /////////////////////////////////////////////////////////////////////
    // "PrintBitmap" Method Constants:
    /////////////////////////////////////////////////////////////////////

    //   "Width" Parameter
    //     Either bitmap width or:

    public static final int PTR_BM_ASIS          = -11;  // One pixel per printer dot

    //   "Alignment" Parameter
    //     Either the distance from the left-most print column to the start
    //     of the bitmap, or one of the following:

    public static final int PTR_BM_LEFT          = -1;
    public static final int PTR_BM_CENTER        = -2;
    public static final int PTR_BM_RIGHT         = -3;


    /////////////////////////////////////////////////////////////////////
    // "RotatePrint" Method: "Rotation" Parameter Constants
    // "RotateSpecial" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int PTR_RP_NORMAL        = 0x0001;
    public static final int PTR_RP_RIGHT90       = 0x0101;
    public static final int PTR_RP_LEFT90        = 0x0102;
    public static final int PTR_RP_ROTATE180     = 0x0103;


    /////////////////////////////////////////////////////////////////////
    // "SetLogo" Method: "Location" Parameter Constants
    /////////////////////////////////////////////////////////////////////

    public static final int PTR_L_TOP            = 1;
    public static final int PTR_L_BOTTOM         = 2;


    /////////////////////////////////////////////////////////////////////
    // "TransactionPrint" Method: "Control" Parameter Constants
    /////////////////////////////////////////////////////////////////////

    public static final int PTR_TP_TRANSACTION   = 11;
    public static final int PTR_TP_NORMAL        = 12;


    /////////////////////////////////////////////////////////////////////
    // "StatusUpdateEvent" Event: "status" Parameter Constants
    /////////////////////////////////////////////////////////////////////

    public static final int PTR_SUE_COVER_OPEN   =   11;
    public static final int PTR_SUE_COVER_OK     =   12;

    public static final int PTR_SUE_JRN_EMPTY    =   21;
    public static final int PTR_SUE_JRN_NEAREMPTY=   22;
    public static final int PTR_SUE_JRN_PAPEROK  =   23;

    public static final int PTR_SUE_REC_EMPTY    =   24;
    public static final int PTR_SUE_REC_NEAREMPTY=   25;
    public static final int PTR_SUE_REC_PAPEROK  =   26;

    public static final int PTR_SUE_SLP_EMPTY    =   27;
    public static final int PTR_SUE_SLP_NEAREMPTY=   28;
    public static final int PTR_SUE_SLP_PAPEROK  =   29;

    public static final int PTR_SUE_IDLE         = 1001;


    /////////////////////////////////////////////////////////////////////
    // "ResultCodeExtended" Property Constants for Printer
    /////////////////////////////////////////////////////////////////////

    public static final int JPOS_EPTR_COVER_OPEN = 1 + JposConst.JPOSERREXT; // (Several)
    public static final int JPOS_EPTR_JRN_EMPTY  = 2 + JposConst.JPOSERREXT; // (Several)
    public static final int JPOS_EPTR_REC_EMPTY  = 3 + JposConst.JPOSERREXT; // (Several)
    public static final int JPOS_EPTR_SLP_EMPTY  = 4 + JposConst.JPOSERREXT; // (Several)
    public static final int JPOS_EPTR_SLP_FORM   = 5 + JposConst.JPOSERREXT; // EndRemoval
    public static final int JPOS_EPTR_TOOBIG     = 6 + JposConst.JPOSERREXT; // PrintBitmap
    public static final int JPOS_EPTR_BADFORMAT  = 7 + JposConst.JPOSERREXT; // PrintBitmap
}
