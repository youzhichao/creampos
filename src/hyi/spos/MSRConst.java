package hyi.spos;

/**
 * Constances for MST.
 *
 * @author Bruce You
 * @since Aug 26, 2008 7:39:41 PM
 */
public interface MSRConst {

    //###################################################################
    //#### MSR Constants
    //###################################################################

    /////////////////////////////////////////////////////////////////////
    // "TracksToRead" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int MSR_TR_1         = 1;
    public static final int MSR_TR_2         = 2;
    public static final int MSR_TR_3         = 4;
    public static final int MSR_TR_4         = 8;

    public static final int MSR_TR_1_2       = MSR_TR_1 | MSR_TR_2;
    public static final int MSR_TR_1_3       = MSR_TR_1 | MSR_TR_3;
    public static final int MSR_TR_1_4       = MSR_TR_1 | MSR_TR_4;
    public static final int MSR_TR_2_3       = MSR_TR_2 | MSR_TR_3;
    public static final int MSR_TR_2_4       = MSR_TR_2 | MSR_TR_4;
    public static final int MSR_TR_3_4       = MSR_TR_3 | MSR_TR_4;

    public static final int MSR_TR_1_2_3     = MSR_TR_1 | MSR_TR_2 | MSR_TR_3;
    public static final int MSR_TR_1_2_4     = MSR_TR_1 | MSR_TR_2 | MSR_TR_4;
    public static final int MSR_TR_1_3_4     = MSR_TR_1 | MSR_TR_3 | MSR_TR_4;
    public static final int MSR_TR_2_3_4     = MSR_TR_2 | MSR_TR_3 | MSR_TR_4;

    public static final int MSR_TR_1_2_3_4   = MSR_TR_1 | MSR_TR_2 |
                                               MSR_TR_3 | MSR_TR_4;


    /////////////////////////////////////////////////////////////////////
    // "ErrorReportingType" Property Constants
    /////////////////////////////////////////////////////////////////////

    public static final int MSR_ERT_CARD         = 0;
    public static final int MSR_ERT_TRACK        = 1;


    /////////////////////////////////////////////////////////////////////
    // "ErrorEvent" Event: "ResultCodeExtended" Parameter Constants
    /////////////////////////////////////////////////////////////////////

    public static final int JPOS_EMSR_START      = 1 + JposConst.JPOSERREXT;
    public static final int JPOS_EMSR_END        = 2 + JposConst.JPOSERREXT;
    public static final int JPOS_EMSR_PARITY     = 3 + JposConst.JPOSERREXT;
    public static final int JPOS_EMSR_LRC        = 4 + JposConst.JPOSERREXT;
}
