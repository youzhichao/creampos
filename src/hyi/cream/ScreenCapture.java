package hyi.cream;

import hyi.cream.util.CreamToolkit;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenCapture {

    public static File nextCaptureFile;

    public static void main(String[] args) {
        capture();
    }

    /**
     * Capture current screenshot to "./capture/capture_{HHmmss}.png" or the given
     * nextCaptureFile.
     */
    public static void capture() {
        File imageFile;
        if (nextCaptureFile != null) {
            imageFile = nextCaptureFile;
        } else {
            File dir = new File("." + File.separator + "capture");
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    CreamToolkit.logMessage("Capture path:" + dir.getAbsolutePath() + " cannot be created.");
                    return;
                }
            }
            imageFile = new File(dir, "capture_" + new SimpleDateFormat("HHmmss").format(new Date()) + ".png");
        }
        Rectangle rect = POSTerminalApplication.getInstance().getMainFrame().getBounds();

        for (GraphicsDevice gd :
            GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()) {
            try {
                BufferedImage image = new Robot(gd).createScreenCapture(
                    new Rectangle(0, 0, rect.width, rect.height));
                ImageIO.write(image, "png", imageFile);
                // "capture" + gd.getIDstring().substring(1) + ".png"
            } catch (Exception e) {
                CreamToolkit.logMessage(e);
            }
        }
        nextCaptureFile = null;
    }
}
