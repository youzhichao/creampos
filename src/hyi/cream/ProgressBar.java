package hyi.cream;

import java.awt.*;

public class ProgressBar extends Frame
{
    private static final long serialVersionUID = 1L;

    private static final int GAP = 6;
    private Image offscreen;
    private int offScreenH;
    private int offScreenW;
    private int count;
    private int max;
    private Font font;

//    public static void main(String[] args)
//    {
//        ProgressBar p = new ProgressBar();
//        p.setMax(10);
//        p.setCount(6);
//        p.setResizable(true);
//        p.setVisible(true);
//    }

    public ProgressBar()
    {
        setLayout(null);
        setUndecorated(true);
        centeringFrame();
    }

    private void centeringFrame()
    {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int w = dim.width * 4 / 5;
        int h = dim.height / 6;
        setSize(w, h);
        setLocation(dim.width / 2 - w / 2, dim.height / 2 - h / 2);
        font = new Font("Serif", Font.BOLD, 18);
    }

    public void updateProgress()
    {
        ++count;
        if (max <= 100 || count % (max / 100) == 0)
            repaint();
    }

    @Override
    public void update(Graphics g)
    {
        paint(g);
    }

    @Override
    public void paint(Graphics g)
    {
        if (max <= 0)
            return;

        if (offscreen == null || offScreenW != getWidth() || offScreenH != getHeight()) {
            offscreen = createImage(getWidth(), getHeight());
            offScreenW = getWidth();
            offScreenH = getHeight();
        }
        Graphics og = offscreen.getGraphics();

        og.setColor(Color.WHITE);
        og.fillRect(0, 0, offScreenW, offScreenH);
        og.setColor(Color.GRAY);
        og.drawRect(1, 1, offScreenW - 2, offScreenH - 2);

        ((Graphics2D)og).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        // Build a string showing the % completed as a numeric value.
        double percentComplete = (double)count * 100.0 / (double)max;
        String s = String.valueOf((int)percentComplete) + "%";

        og.setColor(Color.BLACK);
        og.setFont(font);
        og.drawString("ePOS Loading: " + s, GAP * 2, offScreenH / 2 - GAP * 2);

        int x = GAP;
        int y = offScreenH / 2 + GAP * 2;
        int w = offScreenW - GAP * 2;
        int h = offScreenH / 2 - GAP * 4;

        // Fill the bar the appropriate percent full.
        og.setColor(Color.GRAY);
        og.fillRect(x, y, w, h);
        og.setColor(Color.BLUE);
        og.fill3DRect(x + 1, y + 1, (w - 2) * count / max, h - 2, true);
        og.setColor(Color.BLACK);
        og.draw3DRect(x, y, w, h, false);

        g.drawImage(offscreen, 0, 0, this);
        og.dispose();
    }

    public int getMax()
    {
        return max;
    }

    public void setMax(int max)
    {
        this.max = max;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }
}
