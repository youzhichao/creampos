package hyi.cream;

public final class Version {
    private static final String versionID = "T5.5.5";
    private static final String versionDate = "2019-12-23";

    /**
     * <P>版本编号{Major}.{Minor}.{Revision}
     * <P>编号变化规则：
     *     <LI>{Major}表示大的版本变动
     *     <LI>{Minor}表示功能有所增修
     *     <LI>{Revision}表示功能有局部修改但是不会全面影响所有客户
     * <p/>
     * <p/>Change history:<br/>s
     * <p>Ver T5.5.1 @pingping 2019-05-28
     *    <li> 增加外离、外连
     *    <li> 修改warningIndicator字体加粗
     *    <li> 修改按钮按下去的颜色
     * <p/>
     * <p>Ver T5.4.8 @fei 2019-01-25
     *    <li> 改教超促销版本号property.MixAndMatchVersion=3
     * <p/>
     * <p>Ver T5.4.7 @fei 2018-12-17
     *    <li> 小票增加总件数
     * <p/>
     * <p>Ver T5.4.4 @pingping 2018-04-20
     *    <li> 增加税率5到10
     * <p/>
     * <p/>Change history:<br/>
     * <p>Ver T5.3.8 @pingping 2017-09-20:
     *    <li> 增加微信支付
     * <p/>
     * <p>Ver T5.3.7 @Bruce 2017-08-15:
     *    <li> 增加佳博打印機驅動GPrinterPOSPrinter
     * <p>Ver T5.3.6.3 @Bruce 2017-05-29:
     *    <li> 修改传给支付宝的price改成unitPrice
     * <p>Ver T5.3.6.2 @pingping 2017-05-22:
     *    <li> 修改支付宝明细传送折扣后的金额
     * <p/>
     * <p>Ver T5.3.6 @pingping 2016-09-22:
     *    <li> 增加参数控制门店电话是否打印区号(PrintAreaNumber)，默认为true
     * <p>Ver T5.3.5 @pingping 2016-08-26:
     *    <li>修改移动和包 1.商户退款： 101450调整为101456。2.订单状态接口查询：101455调整为101457。
     * <p>Ver T5.3.4 @pingping 2016-06-14:
     *    <li>修改盘点时商品重复更正的问题
     * <p>Ver T5.3.3 @Bruce 2016-04-12:
     *    <li>解決Server端單品促銷結束時間填錯的問題.
     * <p>Ver T5.3.2 @pingping 2016-03-10:
     *    <li>增加参数控制每天日结次数限制
     *    <li>增加参数控制是否允许立即更正
     * <p>Ver T5.3.1 @pingping 2015-10-29:
     *    <li>移动支付增加PIKFLG参数，默认为1
     * <p>Ver T5.3.0 @pingping 2015-05-22:
     *    <li>增加支付方式移动支付
     * <p>Ver T5.2.9:
     *    <li>修改支付宝，当detail_error_code=TRADE_HAS_SUCCESS，我们就直接成功
     * <p>Ver T5.2.9 @Bruce 2015-08-05:
     *    <li>加快PostgreSQL plu表下傳。
     * <p>Ver T5.2.8 @pingping 2015-05-13:
     *    <li>增加第二屏
     * <p>Ver T5.2.7:
     *    <li>property表增加参数reprintGenerateVoidTransaction，用来控制重印是否产生新交易
     * <p>Ver T5.2.6:
     *    <li>控制期刊领取与退刊不允许使用支付宝
     *    <li>控制期刊领取退货时判断是否已领刊，是否已退刊
     * <p>Ver T5.2.5:
     *    <li>增加支付宝支付方式
     * * <p>Ver T5.2.4:
     *    <li>修改下班考勤的一些字段记录
     * <p>Ver T5.2.3:
     *    <li>修改团购结算和赊账翻倍的问题
     * <p>Ver T5.2.2:
     *    <li> POS在主档下传时，后端不检查是否正在换日（PostProcessorImplA and PostProcessorImplB）。(2013-05-10)
     *    <li> 上下班考勤。(2013-05-17)
     * <p>Ver T5.2.1:
     *    <li> 期刊商品銷售仍然記帳於銷售毛額與淨額。(2012-10-17)
     *    <li> 为了缩减ssmp_log的数据量，减少这两种操作的记录：POS手输条码或品号、单品更正。(2012-11-21)
     *    <li> 在主档下传后，删除7天前的ssmp_log记录。(2012-11-21)
     *    <li> 在synctrans时不传putSsmpLog，在上传ssmp log时增加离线retry机制。(2012-11-21)
     * <p>Ver T5.1.8:
     *    <li> 增加期刊部分退貨功能
     * <p>Ver T5.1.7:
     *    <li>Fix bug: 在教育訓練模式下，使用信用卡支付時不與任何連線的EDC/CAT進行授權通訊。
     * <p>Ver T5.1.6:
     *    <li>Fix bug: 做了一筆有指定更正的正常交易後，按【退貨】、輸入不存在的機號、交易序號、店號後，按【清除】鍵取消，
     *        此時畫面背景仍顯示兩個“退貨”的大字，如果繼續按正常交易輸入商品並結帳，此筆正常交易就被標記成退貨0-3-4了。
     * <p>Ver T5.1.5:
     *    <li>Fix bug: 0元發票金額交易[0-0-0]做部份退貨後，部退交易[*-0-0]沒有支付ID。
     *    <li>Fix bug: 投庫完成後按[發票重印]鍵，立刻按[確定]鍵，發票機打印發票。(借零亦同)
     *    <li>Fix bug: 由於現在Nitori單品數接近2萬，在有些度速較慢的POS機上，在下傳PLU的時候可能會發生timeout的情況。現在把timeout時間加長，以避免這種問題發生。（高雄大樂店2010/07/18的POS1號機發生此現象，後來重啟重新下傳就好了。）
     * <p>Ver T5.1.4:
     *    <li>Fix bug: 在輸入交易過程中，若發票紙卷到最後一張時更換發票號碼後，會在tranhead的發票號碼欄位填
     *        第二張發票號。改成填原來第一張的發票號。
     * <p>Ver T5.1.3:
     *    <li>增加兩個參數：<br/>
     *     * InputMemberCardIDInTheMiddleOfTransaction: 是否可以在交易當中輸入會員卡號 (Nitori default為yes)<br/>
     *     * GenerateSSMPLog: 是否生成和上傳SSMP Log (Nitori default為no)
     *    <li>修改三個繁體錯字in Param:<br/>
     *     * 四“舍”五入 → 四“捨”五入<br/>
     *     * 設“靈” → 設“零”<br/>
     *     * “稱”重 → “秤”重
     *    <li>升級Groovy到1.6.4
     *    <li>解決零發票金額沒有印“對獎無效”文字問題。
     *    <li>會員編號長度改成30: cream.tranhead.memberid, cake.posul_tranhead.memberID, cake.commul_tranhead.memberID
     * <p>Ver T5.1.2:
     *    <li>增加称重商品销售功能.
     *    <li>增加专柜商品销售功能.
     *    <li>增加四舍五入功能.
     * <p>Ver T5.1.1:
     *      Add parameters editor on web console.
     * <p>Ver T5.1.0:
     *      Add auto-testing functionality.
     * <p>Ver T5.0.8 (2009-03-01):
     *      POS支援多個連線或離線的CAT（但目前一筆交易仍只能使用一種信用卡）。
     * <p>Ver T5.0.7 (2009-02-23):
     *      修改內容：
     *      <li>修改小票打印內容超過一頁，而且property 'InvoiceID'的值為空時，會不斷切紙的問題。
     *          然後導致發票張數字段過大，上傳到後台會失敗。
     *      <li>修改前後台交易同步過程中若有其中一筆上傳失敗，會導致後面的補傳動作也不做。
     *      <li>解決若商品找不著時速度會很慢的問題。原因是plu表沒有建品號欄位的索引。
     *      <li>試圖解決從支付界面回到銷售界面速度慢的問題：在結帳完成後，立即準備下一筆交易對象(in DrawerOpenState)，
     *          這樣在下次回到銷售界面時，可以省去生成交易對象的時間，速度也許可以快一點。
     *      <li>交易上傳後立即插入數據庫：修改後端PostProcessorImplA和PostProcessorImplB。
     * <p>Ver T5.0.6:
     *      Merging from James' change in revision 283:
     *      <li>Modify: the tranhead that "book daipei cash return" insert adition change to return flag(dealType1,2,3: 000->034)(voidTransaction empty).
     *      <li>Improvement: add property "IsDaiShou2BillCanRepeate", if false, forbid daishou2 billno repeated, foucs in "book daipei".
     *      <li>Improvement: show version when start up inline server.
     *      <li>Improvement: show ip + port when inline server and client connected or disconnected each other.
     *      <li>Improvement: ListWholesaleState addition check first if billno exist and reject it in pos tranhead
     *        with "daipei" or "daipei return"(wholesalehead.billType is 7 or 8) before connect to inline server.
     *      <li>Modify file(s): DrawerOpenState,Transaction,Client,Server,ServerThread,DaiShou2State,GetProperty,State,StateMachine,
     *        ListWholesaleState,CreamResource_zh_CN,CreamResource_zh_TW.
     *      <p/>
     *      其他見http://211.72.195.217/CreamPOS/wiki/ReleaseT5.0.6
     * <p>Ver T5.0.4
     *      <li>把tranhead中的淨額改成不去扣減“已開發票”支付的金額（只有發票金額會扣除已開發票支付金額）。
     *          否則這樣會導致（tranhead的淨額合計）不等於（trandetail的aftdscamt合計）；
     *          （tranhead的淨額合計）也會不等於（depsales的淨額合計）
     *      <li>Fix bug：如果有程序文件下傳，必須在下次啟動的時候重新做PLU object DB，否則有可能會由於
     *          class serialVerionUID不一樣導致class incompatible的問題。
     *      <li>將shrink log file大小改為10MB。
     *      <li>Web console增加NitoriPOSSettingsWindow。
     * <p>Ver T5.0.3
     *     着付二期.
     * <p>Ver T5.0.2 2008-09-17 James
     *     <li>新需求: 放弃原有（专柜）部门93做法，新增【专柜】【专柜预收】,
     *         a. 【专柜】部门931, 现金日报科目715， 【专柜预收】部门932, 现金日报科目716
     *         b. 711 包含 715, c.720包含931, 932部门净额
     *         c. 新增熟悉getExclusiveZhunguiDeps, 默认为空,Nitori release 为"931:932"
     *         d. 禁止专柜与普通商品混合销售,安照depID判断
     *         e. 专柜商品不可以做部门输入. 输入了专柜，要拦住其他商品通过部门输入进来
     *      修改： PostProcessorImplA, PleReadyState,GetProperty, CategoryInputState
     * <p/>Ver T5.0.1
     *     <li>Please check: http://211.72.195.217/CreamPOS/wiki/ReleaseT5.0.1
     * <p/>Ver T5.0.0
     *     <li>Merging version.
     * <p/>Ver T4.48.11 2008-08-15 James
     *     <li>修改: 开机时如果打印机不正常就不用打印机了, 添加POSPeeripheralHome2.openPrinterForTimeOut(...),
     *         用于open 和尝试打印。GetProperty.getOpenPrinterWaitTime().
     *         如果参数大于0并且open或者尝试打印不成功或超时，将就强行将内存中isPrintToConsoleInsteadOfPrinter设为true.
     *         0 或者不设参数，就按照原有的逻辑检查打印机, 在打印机正常的时候才能使用系统
     *     <li>将以前通过log4j记录的发票电子存根，改为对打印内容录制到文件中，文件名和路径由GetProperty.getPrinterRecordFile()确定
     * <p/>Ver T4.48.10 2008-07-03 James、Bruce
     *     <li>修正Bug: Transaction.getActualReceivedPaymentAmount
     *         没有跳过Payment.queryByPaymentID(getPayNumberX()) == null的情况
     *     <li>Commit conf/MeiLin_6400/jpos.conf: 将扫描枪的前置码改为空格、调整几个TEC-6400 keyboard的
     *         按键配置的错误。
     *     <li>在POS的主画面增加版本信息。
     *     <li>解决扫描枪KeyboardScanner无法使用的问题。
     *     <li>在版本号前面加上字母"T"，以说明是Trunk版本。
     * <p/>Ver 4.48.09 2008-06-29 James
     *     <li>刷新时间不准时，改为继承TimerTask，由Timer来定时执行,POSTerminalApplicaton.ShowTime
     *     <li>delay create instance for ThreadWatchDog in POSTerminalApplication, not static
     * <p/>Ver 4.48.08 2008-06-25 Bruce
     *     <li>盘点打印结束增加切纸。
     *     <li>交易取消后面多加两行回车，已因应当receipt.conf的“P” section不是遇黑点切纸的情况。
     *     <li>解决促销特卖价在特卖时间到了之后没有恢复原价的问题。
     *     <li>ZReport.update() 时不需重新query current Z number，以加快结账速度。
     *     <li>Commit conf/MeiLin_6400/receipt.conf
     * <p/>Ver 4.48.07 2008-06-23 James
     *     <li>修正Bug:
     *
     * <p/>Ver 4.48.06 2008-06-16 James
     *     <li>修正Bug: Transaction.tran_Processing(...)没有通过DaishouSales.queryByFirstNumberAndSecondNumber()更新DaiShouSales表和shif/z表
     *         因为createDaishouSales时，没有把firstnumber,secondnumber的前导0去掉，导致查找时找不到.
     * <p/>Ver 4.48.05 2008-06-09 James
     *     <li>修正Bug: DaiShouButton,PaymentMenu2Button,PluMenuButton,TaxedAmountButton,TouchMenuButton 中弹出菜单没有被POSButtonHome2运行
     * <p/>Ver 4.48.04 2008-06-09 James
     *     <LI>LineItem下列方法改为不用每次都去查询数据库.getUnitPrice2(), getAmount2(),getSpecification(), getDescriptionAndSpecification(),isOpenPrice
     *     <li>NormalCombPromotion.match要传达transaction给它.
     *     <li>CreamToolkit获得配置文件jpos.xml时，如果在conf目录找不到，就到上一级目录去找.
     *     <li>优化Simple的java pos 驱动接口定义。
     *     <li>添加本机开发调试用的驱动程序
     * <p/>Ver 4.48.03 Bruce
     * Performance tuning:
     *     <LI>PluReadyState.createLineItem() don't have to query next transaction number from database.
     *     <LI> LineItem.getIsScaleItem() don't have to query PLU every time when painting. Cache the flag into LineItem.
     *     <LI> Transaction.tran_Processing() don't have to query every PLU for depID. Cache the depID into LineItem.
     *     <LI> Create another index for speedup the query in Transaction.getBeginAndEndTranNumber():
     *       <BR/>CREATE INDEX tranhead_idx_posno_sysdate ON tranhead (posno, sysdate);
     *     <LI> Create another index for speedup the query in ZReport.queryNotUploadReport():
     *       <BR/>CREATE INDEX z_idx_accdate_tcpflg ON z (accdate, tcpflg);
     * <p/>Ver 4.48.02 Bruce
     *     <LI>签到时显示收银员工号及姓名。
     * <p/>Ver 4.48.01 Bruce
     *     瘦身工程：
     *     <LI>拿掉JavaPOS，改用自己写的Simple JavaPOS接口。
     *     <LI>暂时拿掉使用log4j的相关代码。但现在receipt/invoice log没写。
     *     <LI>减少线程个数：Indicator在必要的时候才create thread。
     *     <LI>X windows降为256色，调整了xorg.conf。
     *     <LI>database connection pool的idle connection设为1个。改dbcp.conf（dbcp.initialSize=1、dbcp.maxIdle=1）。
     *     <LI>增加了Bootstrapper，在系统启动的时候preload classes并显示进度条。配置文件在conf/preload_classes.conf。
     *     <LI>简化了POSButton/POSButtonHome的按键事件监听机制，不再是每个POSButton都是POSKeyboard的DataListener，由POSButtonHome集中监听。
     *     <LI>为了可以不让加载swing classes：
     *         1.增加property "EnableTouchPanel"，true时才让触碰TouchPane相关代码，缺省为true
     *         2.暂时拿掉game相关的代码
     *         3.POSTerminalApplication extends Frame, instead of JFrame
     *     <LI>为了加快商品查询速度，增加了PLU object database，由三个文件构成：plu.odb、plu_pluno.idx、
     *         plu_itemno.idx；在主档下传时生成；手工删除这三个文件后，也会再自动生成。代码见CreamCache。
     *         现在在TEC-6400内存64M的机器上，7474笔PLU，buildPLUObjectDB()需要40几秒，但PLU查询速度只要几十个ms。
     *     <LI>原CreamCache中的plu cache暂时拿掉。
     *     <LI>将session data（within POS transaction scope）放到CreamSession中。
     *     <LI>DacBase supports POJO with property fields (see PLU)。DacBase增加getSinglePojoObject()、constructPojoFromResultSet()等方法。
     *     <LI>POSTerminalApplication中记录的currentTransaction不再是固定不变的，每次新的交易开始时，会通过
     *         POSTerminalApplication的cloneFromBlankTransaction()方法造出新的Transaction object，
     *         cloneFromBlankTransaction()由Transaction.generateABrandNewTransaction()调用。
     *     <LI>ZReport.getCurrentZNumber() 现在有cache。
     *     <LI>增加一个property "JustCalcMMAtSummaryState": 是否只在按小计或客层键后计算组合促销折扣。
     *     <LI>CreamToolkit增加deepClone()方法（James，hyi.cream.util.Util我拿掉了）。
     *     <LI>这次有关Simple JavaPOS的修改：增加hyi.spos.MSR 、Simple JavaPOS的BaseControl和其control classes改为abstract class、去掉POSButtonHome and POSPeripheralHome。
     * <p/>
     * <p/>Ver 4.47.12 james
     *        <LI>(2008-04-09)
     *          1.修正：bug， 图书代配退回时没有采用支付信息不正确（支付计算没有充分按照负向销售处理）
     *            修改: DrawerOpenState, Paying3State
     *          2.修正：bug， 图书代配退回, 附加的代收交易明细daiShouAmount2金额符合不对
     *            修改: DrawerOpenState
     *          3.修正bug:Transaction deepClone会有疏漏，无法把tran的lineitem clone, 现在改为用SerializationUtils.clone()
     *            修改 Transaction,  添加flag isUseSpecialReadWriteObject
     * <p>
     * <p>Ver 4.47.11 james
     *        <LI>(2008-04-08)
     *          1.修正：bug， 代配退回时有问题单据检核不符合画面仍然显示
     *            修改: ListWholesaleState, WholesaleUtils, CreamResource_zh_CN, CreamResource_zh_TW
     * <p>Ver 4.47.10 james
     *        <LI>(2008-04-07)
     *          1.修正：bug， CreamSession.getAttribute()没有回避处理空指针异常问题。
     *            添加 参数IsDaiShou2MixSales：是否可以Daishou2与正常商品在同一笔交易中混合录入
     * <p>Ver 4.47.09 james
     *        <LI>(2008-04-03)
     *          1.修正：bug， 在transaction.writeTransactionLog(...)使用的connection 是空的，代收取消保存是会报错。
     *            修改：Transaction.writeTransactionLog(...)
     * <p>Ver 4.47.08 james
     *        <LI>(2008-04-03)
     *          1.修正：2007-08-15版本有错误，借零dealType应为H而不是Q，现在改回错误。
     *             2.新增：代配交款(使用代收)，代配领取(dealType2=P,state1=D)，代配退回(dealType2=P,state1=B) 流程.
     *          3.修正：开票单tran.state1 错误填写T的错误，应填P, 开票单也切换到支付画面，但是画面显示空白，不显示支付金额
     *          4.修改：整理优化代码.
     *         修改: CreamSession,DacBase,DaiShouDef,Transaction,Client,PostProcessorImplB,ServerThread,POSTerminalApplication,
     *         CashInIdleState,DaiShou2CheckState,DaiShou2ReadyState,DaiShou2State,DrawerOpenState,GetProperty,IdleState,
     *         Numbering2State,NumberingState,Paying1State,PeiDaState,GetPeriodicalNoState,PeriodicalNoReadyState,
     *         PeriodicalUtils,ReturnSummaryState,StateMachine,SummaryState,WeiXiuState,GetWholesaleDateState,
     *         GetWholesaleNoState,ListWholesaleState,WholesaleDateNumberingState,WholesaleDateReadyState,
     *         WholesaleEndState,WholesaleIdleState,WholesaleNoNumberingState,SystemInfo,ItemList,PayingPaneBanner,
     *         CreamResource_zh_CN,CreamResource_zh_TW,CreamToolkit
     * <p>Ver 4.47.07 james
     *        <LI>(2008-03-21)
     *             1.修改：只有是当月或者之前月份的刊物才可以领刊
     *         修改: ServerThread.processBookedInfos()
     * <p>Ver 4.47.06 james
     *        <LI>(2008-01-08)
     *             1. 修正Bug: 从cake.itembookcodes中获得可定刊号列表时，
     *           server端数据库连接没有释放，导致多次使用数据库连接被占用.
     *          2. 提高速度: InlineServer收到大批量盘点数据时的处理，先删除后插入，避免大量输出log
     *          3. 提供查找不到对应交易时不报写log的方法，用于找不到交易也没有必要写log的场合
     *         修改: PeriodicalUtils.processGetBookableSubIssns,
     *           PostProcessorImplB(A).afterReceivingInventory
     *           Transaction.queryByTransactionNumber
     * <p>Ver 4.47.05 james
     *        <LI>(2007-11-15) 修正putZ成功后，根据该z对应的会计日更新前后台的会计日
     *           tranhead, shift, depsales, daishousales, daishousales2
     *           posul_tranhead, posul_shift, posul_depsales, posul_daishousales, posul_daishousales2
     *         修改: Client.updateAccountDateByZNo(), PostProcessorImplB.afterReceivingZReport()
     * <p>Ver 4.47.04 james
     *        <LI>(2007-11-01) 修正putZ不成功后，自动补传Z没有默认以第一次后台更新z对应的会计日为准
     *         修改: PostProcessorImplB
     * <p>Ver 4.47.03
     *        <LI>(2007-10-08) 优化盘点商品输入限制
     *         盘点输入非15位期刊条码后，如果从plu.itemtype中判断出它  是期刊商品, 当成不存在该商品处理
     *         盘点输入  15位期刊条码后，如果从plu.itemtype中判断出它不是期刊商品, 当成不存在该商品处理
     *         修改:InventoryIdleState
     * <p>Ver 4.47.02
     *        <LI>(2007-10-02) 在团购会员还款功能基础上, 增加团购会员预收功能
     *     修改:Transaction, ItemList, DrawOpenConfirmState, Paying3State,
     *         WholesaleClearingState,CreamResource_zh_CN,CreamResource_zh_TW
     * <p>Ver 4.47.01
     *        <LI>(2007-08-10) 期刊预订领用，，期刊销售，盘点，团购结算，团购会员还款
     * <p>Ver 4.48.55 2008-07-15 James
     * <li>1.修正：bug， 對傳票，傳票，傳票退貨，配達, 前筆誤打, 卡紙重印，掛單調出等特殊交易，不限制MaxLineItems,
     *      Transaction添加方法addLineItem帶isLimitMaxLineItems開關。
     * <p>Ver 4.48.54 2008-07-04 James
     * <li>新需求: 新增（专柜）部门93, 现金日报科目715。 a. 93部门填715, b.711 包含 715, c.720包含93部门净额
     *     修改： PostProcessorImplA
     * <p>Ver 4.48.53 2008-07-03 James
     * <li>修正Bug: Transaction.getActualReceivedPaymentAmount
     *     没有跳过Payment.queryByPaymentID(getPayNumberX()) == null的情况
     * <p>Ver 4.46.52
     *        <LI>(2008-06-22)
     *       1. 修正bug：PostProcessorImplA没有像PostProcessorImplB那样是用 afterReceivingZReport(ZReport z, StringBuffer accdateStrBuf)
     *       没有返回accountDate给pos端来更新会计日，导致在SC过期交易被删除的情况下，pos自动重传了，其会计日还是写着1970的.
     * <p>Ver 4.46.51
     *        <LI>(2008-05-20)
     *       1. 修正bug：传票退货汇总确认画面有做过按【清除】键回退，会导致多打印一张负向全退交易的发票.
     *       修改：ReturnShowState.entry()
     * <p>Ver 4.46.50
     *        <LI>(2008-03-10)
     *       1. 修改中写死卡号最长16位长度限制。
     *       修改：ReadCrdNoState,CardNumberingState, GetProperyty
     * <p>Ver 4.46.49
     *        <LI>(2008-02-29)
     *       1. 新增屬性GetProperty.getMemberCardIDPrefix(), default: 2991
     *       2. 提供只讀屬性getMemberIDShort(), 截掉GetProperty.getMemberCardIDPrefix()和較驗位.
     *       3. 将开机启动Inline Clinet 放在启动 app画面后面, 否则离线开机时间太长。
     *       修改：Transaction, PluReady, Member, MemberState,GetProperty, PosTerminalApplication.
     * <p>Ver 4.46.48
     *        <LI>(2008-02-29)
     *       1. 提供8位會員號碼輸入查詢，提供模糊會員(根據Property.getIsQueryMemberBlur
     *       修改：Member,GetProperty
     * <p>Ver 4.46.47
     *        <LI>(2008-02-28)
     *       1. LineItem提供顯示plu.PrintName.的方法.
     *       2. Add Property "ShePaymentID", "IsForbitShePayment", normallly not forbit ShePayment
     *       修改：LineItem
     * <p>Ver 4.46.46
     *        <LI>(2008-02-25)
     *       1. 修改传票输入错误时，ToneIndicator发声音无法停止的bug.
     *       修改：PeiDa3IdleState
     * <p>Ver 4.46.45
     *        <LI>(2008-02-24)
     *       1. 升级CD7110 For 为jpos1.9驱动
     *       2. 升级POSPartnerPrinter 为jpos1.9驱动, 修正GenericCreamPrinter定位检测条件。
     *       3. 新增PartnerKB78 键盘驱动
     *       4. 新增PartnerKB78Keylock驱动, 添加对应转钥匙说明国际化字符串
     *       5. 修正POSButtonHome, 读入配置文件最后一行为空串会报错的bug
     *       6. 补充修改[会员键]功能，支持批发零售销售形态切换
     *       7. 整理log输出
     *       8. 修改变价功能，当cashier.casrights第10位为1时，允许低于最低批发价销售
     *       修改：POSPartnerPrinter, PartnerKB78, PartnerKB78Keylock, CD7110,jpos.xml, POSButtonHome,
     *            GenericCreamPrinter,CashDrawerPartner, InitialState, CreamResource_zh_TW, CreamResource_zh_CN,
     *            NumberingState, POSPeripheralHome, posdevice3.conf, posbutton3.conf,
     *            OverrideAmountState, CashierRightsCheckState
     * <p>Ver 4.46.44
     *        <LI>(2008-02-02)
     *       TranProcessorThread.syncTranheadAccountDateFromZ()中的SQL語句在數據量大的時候，執行時間太長了，而且會把posul_z lock住，導致Z帳上傳失敗
     * <p>Ver 4.46.43
     *        <LI>(2008-01-30)
     *       1.修正bug: 多重支付有找零退货时，请不再考虑退货剩余的支付方式是否可以找零。
     * <p>Ver 4.46.42
     *        <LI>(2008-01-29)
     *       1.修正bug: zex中没有累计折扣的税金
     * <p>Ver 4.46.41
     *        <LI>(2008-01-25)
     *       1.按客户需要, 交易明细中税金按照毛额计算，折扣也填税金(税率从参数设定).
     *       2.修正bug: 有退货调整费时没有保存退货调整费的交易明细（错误判断为全退，应该是部分退货）
     *                 (原先判断净额为0就是全退，改为所有未标记为删除的明细金额都不为0才可以是全退)
     *       3.修正bug：传票结帐输入结帐金额过程中，按清除键不能返回传票明细画面导致除了按[交易取消](会生成2笔正负对冲交易)无法退出交易的情况。
     *                 另配达交易明细到达后台后，对tran.dealType="*"的不更新传票状态
     *       4.改善性能：DacBase.getMultipleObjects()中，將fieldNameMap提到循環前面
     *       修改：GetProperty, Transaction, LineItem, PostProcessorImplA, SIDiscountState, StateToolkit,
     *            SummaryState, PostProcessorAdapter, DacBase
     * <p>Ver 4.46.40
     *        <LI>(2008-01-03)
     *       1.修正bug：解决POS画面右上角的通报走马灯文字有时会只显示灰底的问题
     *       修改：Indicator
     * <p>Ver 4.46.39
     *        <LI>(2007-12-29)
     *       1.修正bug：Pos做清除历史交易后，因为没有shift账，钱箱提醒失败导致无法从InitialState返回IdleState
     *       2.修正bug：InitialStateState查询钱箱金额时ShiftReport.getCurrentShift没有新取数据库连接池忘记关闭
     *       3.修正bug：钱箱已满条件不满足后，信息没有清空，而是信息停滞在那里的问题。
     *       修改：InitialStateState, ShiftReport, SystemInfo
     * <p>Ver 4.46.38
     *        <LI>(2007-12-28)
     *       1.修正bug：显示会员输入错误后不能再响应动作, 没有对WarningState设置返回State
     *       修改：PluReadyState
     * <p>Ver 4.46.37
     *        <LI>(2007-12-26)
     *       1.只有“批发”会员才能用“批发”交易类型
     *       2.配送传票POS结清后没有填入POS机号bug修正
     *       3.钱箱提醒功能增强, 在InitialStateState向IdleState跳转以前，更新钱箱金额信息
     *       修改：InitialStateState, PostProcessorAdapter, IdleState, CreamResource_zh_CN, CreamResource_zh_TW
     * <p>Ver 4.46.36
     *        <LI>(2007-12-25)
     *       1.修正bug: 交易结束紧接着就做日结，Server端该交易的会计日不会被更新的问题。
     *         在途中的交易(还未进入数据库）, 在交易由上传文件插入数据库后，针对设定了会计日的z, 如果该z对应交易还未填会计日，则填写会计日。
     *       修改：TranProcessorThread
     * <p>Ver 4.46.35
     *        <LI>(2007-12-25)
     *       1.对会员取单品价格不一定去plu.memberprice, 按member.favourable取价:
     *         (1-会员价 2-会员折扣 3-售价)，favourable未设或是其他值，就仍旧取plu.memberprice
     *       2.单品价格，在允许取价格条件下，按照低价原则取价
     *       修改：PluReadyState, PLU, Member
     * <p>Ver 4.46.34
     *        <LI>(2007-12-21)
     *       1.StateToolkit.calculateTaxAmt(..) 防止除以0的错.
     *       2.新增属性：IsQueryMemberFromPos, 是否直接从pos查询member, 默认no, 添加下传member的机制，提供member在pos本地查询机制
     *       3.将多处new HYIDouble(String) 改为 new HYIDouble以节省资源
     *       修改：StateToolkit, GetProperty, Client, ServerThread
     * <p>Ver 4.46.33
     *        <LI>(2007-12-20)
     *       1.zex表添加SITAX_xx记录的按单价计税截取尾数后再累计分摊得到的折扣税金, 用于现金日报显示折扣的未税金额.
     *       修改：Transaction, PostProcessorImplA
     * <p>Ver 4.46.32
     *        <LI>(2007-12-19)
     *       1.变更: 注意：depsales.siPlusTotalAmount 现在挪用为毛额税金的累计
     *       2.修正：如果按单价计税截取尾数后再累计，InlineServer填写现金日报（Nitori版）未能正确计算各部门未税毛额和税金的bug
     *       修改：Transaction, LineItem, PostProcessorImplA
     * <p>Ver 4.46.31
     *        <LI>(2007-12-19)
     *       1.添加从Reason中读入传票前缀，并在查询中以带前缀的单号查询, 离线新增也在交易中填入带前缀的传票单号
     *       2.修正：不正确判断取回传票类型是否在允许的配达传票类型类型AnnotatedTypeList之内的bug, 会导致传票无法结帐!
     *       修正：PeiDa3IdleState, PeiDa3OnlineState, PeiDa3OfflineState
     * <p>Ver 4.46.30
     *        <LI>(2007-12-17)
     *       1.修正遗留没有根据CalculateTaxAmtByUnitPrice处理税金计算问题的bug. PluReadyState.createLineItem中不再根据外含税重设单价
     *       修正： PluReadyState, DiscountRateState
     * <p>Ver 4.46.29
     *        <LI>(2007-12-15)
     *       1.新增属性: CalculateTaxAmtByUnitPrice， 默认no. 如果是yes, 则按照商品单价计税金并作按税别定义规则截取尾数后再按数量累加总税金.
     *          修改：GetProperty,StateToolkit,LineItem, Transaction, DiscountAmountState,
     *            MixAndMatchState, NormalCombPromotion, OverriceAmount2State, PluReadState, ReturnSelect2State,
     *            ReturnSummaryState, SetQuantityState,YellowAmountState
     *       2.修改bug: 小计后折扣，在交易明细中多添加的一行中，origprice 正负号应该与unitpirce 一样(除了加成以外一般为付), 修正前符号相反了。
     *       修改：SIDiscountState.processDiscount(...)
     * <p>Ver 4.46.28
     *        <LI>(2007-12-13)
     *       1.新增属性: AnnotatedTypeWholesale 批发传票销售类型
     *         修改： getAnnotatedTypeWholesale, PeiDa3IdleState
     * <p>Ver 4.46.27
     *        <LI>(2007-12-09)
     *       1.交易溢收統計區分正常銷售和退貨, 填写现金日报时把溢收金额现金日报中从新加会到各种支付方式中。 加减返回了
     *         修改： PostProcessorImplA
     * <p>Ver 4.46.26
     *        <LI>(2007-12-08)
     *       1.修正按小计后折扣以后按清除键返回单品画面，再按交易取消键无法取消交易的bug
     *       2.新增属性：允许的配达传票类型类型AnnotatedTypeList
     *       3.交易溢收統計區分正常銷售和退貨, 填写现金日报时把溢收金额现金日报中从新加会到各种支付方式中
     *         修改： Transaction, PostProcessorImplA, PostProcessorImplB, PostProcessorAdapter, GetProperty
     * <p>Ver 4.46.25
     *        <LI>(2007-12-07)
     *       1.修正后台统计更新传票时没有把团购'H'当作传票的bug
     *       2.修正将updatePeiDaPayStatus, 移动到 PostProcessorAdapter， 便于公用
     *       3.交易溢收統計區分正常銷售和退貨, 填写现金日报时把溢收金额现金日报中从新加会到各种支付方式中
     *         修改： Transaction, PostProcessorImplA, PostProcessorImplB, PostProcessorAdapter
     * <p>Ver 4.46.24
     *        <LI>(2007-12-07)
     *       1.修正 交易结束对各支付别溢收金额累计部分源代码被误删除的bug
     *       2.交易溢收統計區分正常銷售和退貨, 填写现金日报时把溢收金额现金日报中从新加会到各种支付方式中
     *         修改： Transaction, PostProcessorImplA
     * <p>Ver 4.46.23
     *        <LI>(2007-12-06)
     *       1.审核帐号填入trandetail.content中, 从后台传票读入saleMan, memberID, authorNo
     *         修改： PaiDa3OnlineState, StateToolkit
     * <p>Ver 4.46.22
     *        <LI>(2007-12-04)
     *       1.新增属性 如果CatNoToDepIdStrategy=3， 则对部门输入, 因为不知道对应的plu.depID,
     *         所以考虑连接分类(大中小)(大中)(大)在依次构成depID,依次猜测查找相应存在的depsales记录，直到找到为止
     *         修改： GetProperty, Transaction
     * <p>Ver 4.46.21
     *        <LI>(2007-12-04)
     *       1.新增属性 MoveOutBagAmountToOtherPayment, MoveOutSendFeeToOtherPayment,
     *         通过开关控制是否在填写现金日报时从各金额中扣除，放到"其他"金种中
     *         修改： GetProperty, Transaction, PostProcessorImplA
     * <p>Ver 4.46.20
     *        <LI>(2007-11-30)
     *       1.新增属性 GatherShoppingBagInfo, GatherSendFeeInfo,
     *             BagAmtPaymentID, SendFeeAmtPaymentID,ShoppingBagDepID,SendFeeDepID
     *       2.交易带累计应扣购物袋和配送费之支付金额和退货金额，填写现在日报时从各金额中扣除，放到"其他"金种中
     *         修改： GetProperty, Transaction, PostProcessorImplA
     * <p>Ver 4.46.19
     *        <LI>(2007-11-28)
     *       1.新增属性TerminalName记录Pos机名，在screenBanner中显示POS机名
     *       2.新增属性ShowMemberActionBonus是否显示会员积分
     *       2.SystemInfo添加TranTypeName显示（零售或批发）
     *         修改： GetProperty, IdleState, SystemInfo, CreamResource, ShowMemberInfoState,PluReadyState
     * <p>Ver 4.46.18
     *        <LI>(2007-11-26)
     *       1.新增属性TerminalName记录Pos机名，在screenBanner中显示POS机名
     *       2.添加钱箱已满提醒功能（交易完成时把shift.totalCashAmount记入SystemInfo中，进入IdleState后提醒, 这样比较节约性能）
     *         修改： 1.GetProperty,screenbaner2.conf, SystemInfo.
     *            2.GetProperty,IdleState, SystemInfo, Transaction,CreamResource
     * <p>Ver 4.46.17
     *        <LI>(2007-11-25)
     *       1.plu启用备用字段pluopprice1(会员最低价)pluopprice2(批发最低价)，变价输入后校验这两个价格，
     *         当employee.cashierRight第9位（是否允许变价低于会员最低价）通过
     *       2.从新添加字段trandetail.content, 用于保存授权变价之收银员代号
     *         修改： 1.OverrideAmount2State, OverrideAmountState, CashierCheckState
     * <p>Ver 4.46.16
     *        <LI>(2007-11-22) 1.InlineServer 添加手工重算depsale to accdayrpt 相关科目的功能
     *         修改：PostProcessor,PostProcessorImplA,PostProcessorAdapter,Server
     *        <LI>(2007-11-13) 新增属性SaleManIDMaxLen, SaleManIDMinLen, 输入销售员工号从代码写死改为配置化
     *         修改：GetProperty, GetSaleManNumberingState, SaleManNumberingState
     * <p>Ver 4.46.15
     *        <LI>(2007-11-08) 1.修正填写现金日报部门账部分的时候，[未税金额]=[销售毛额]-[净额税金]的bug,
     *          现在改为 [未税金额] = [销售毛额]/1.05 (nitori 税率统一为0.05)
     *         修改：PostProcessorImplA
     * <p>Ver 4.46.14
     *        <LI>(2007-10-21) 1.退货引起的新增交易中统编号码不清空，与原有交易设成一样
     *         修改：TransactionHole, CancleState, RetrunSummary, TransactionVoidState, ReturnShowState
     * <p>Ver 4.46.12
     *        <LI>(2007-10-10) 1.現金日報科目908（現金找零）只累計禮券(PayID=02)引起的現金找零
     *         修改：PostProcessorImplA
     * <p>Ver 4.46.11
     *        <LI>(2007-09-24) 1.修正传票退货bug
     *         修改：ReturnSummaryState, ReturnShowState
     * <p>Ver 4.46.10
     *        <LI>(2007-09-23) 1.修正溢收找零折扣全退和部分退货相关bug 2.顺便整理相关代码增强代码封装性和可读性
     *         修改：ReturnSummaryState, Transaction, PostProcessorImplA, ReturnAllState,
     *             ReturnNumber2State, ReturnSelect1State, MixAndMatchState
     *             ReturnShowState, StateToolkit, PayingPaneBanner, CreamToolkit
     * <p>Ver 4.46.09
     *        <LI>(2007-09-19) 交易找零累计，折扣累计，填入现金日报, 修改9，91，92部门统计
     * <p>Ver 4.46.06
     *        <LI>(2007-03-08) 挂单
     *     新增属性:EODWithHoldTrn,ShiftWithHoldTrn
     *        <LI>(2007-03-08) 发票
     *     新增属性:PrintLineItemPerPage
     * <p>Ver 4.46.05
     *        <LI>(2007-02-27) inline server gui pos状态固定显示9台
     *     修改:POSStatusTableModel
     * <p>Ver 4.46.04
     *        <LI>(2007-02-26) 变价原因由button-->state
     *         新增:OverrideReasonState
     *         修改:OverrideAmountState
     * <p>Ver 4.46.03
     *        <LI>(2007-02-25) 挂单功能
     *     修改:CancelState,IdleState,TransactionHoldShowState,TransactionHoldState
     *     TransactionUnHoldState,LineItemHold,TransactionHold
     * <p>Ver 4.46.02
     *        <LI>(2007-02-12) 传票(配达)
     *         传票数据来源于sc,通过http.InlineClient取得数据,转换成tranhead/dtl
     *         修改:Transaction,
     *         新增:hyi.cream.inline.http.InlineClient,Util
     *         PeiDa3IdleState,PeiDa3OnlineState,PeiDa3OfflineState,PeiDa3AmountState
     * <p>Ver 4.46.01
     *        <LI>(2007-02-07) 增加配达方式
     *             修改属性:AutoCreatePeiDaNumber-->PeiDaVersion
     *             PeiDaVersion=1 (配达单号自动生成)
     *             PeiDaVersion=2 (配达单号手动录入)
     *             PeiDaVersion=3 (配达明细从sc获得,配达单号扫描获得)
     * <p>Ver 4.45.02
     *        <LI>(2007-02-05) 台湾统一编号功能
     *             修改:GetBuyerNumberState
     * <p>Ver 4.45.01
     *        <LI>(2007-02-05) 台湾发票功能
     *             1)tran,shift,z要记录相关的开始结束发票号
     *             修改:Transaction,ShiftReport,ZReport,
     *             2)发票号码录入功能(英文+数字)
     *             修改:StateChar3.conf,posbutton3.
     *             新增:InvoiceNumberReadingState,AlphabetReadingState,InvoiceButton
     *             修改:CancelState,
     *        <LI>(2007-02-05) 发票少于10张的提示功能
     *             修改:InitialState
     *        <LI>(2007-02-05) 发票序号保存
     *             修改:TransactionVoidState,Transation,ReturnSummaryState
     *                 VoidReadyState
     * <p>Ver 4.44.01
     *        <LI>(2007-02-05) 负项销售也能打印明细
     *             修改:DrawerOpenState
     *        <LI>(2007-02-05)    DaiFu,PaidIn,PaidOut打印修改
     *        <LI>(2007-01-30) 做退货打印时如果商品打印是多页,第二页以后的交易序号会不正确,
     *             原因是:原来打印第二页header时,打印的是currentTran,而currentTran是全负的那一笔(退货生成的第一笔交易),
     *             要打印的是未退货的(退货生成的第二笔交易).
     *             解决方法:新增接口    CreamPrinter.printLineItem(Transaction tran, LineItem lineItem);
     *             修改:CreamPrinter,CreamPrinter_zh_CN
     *        <LI>(2007-01-30) 打印交易方式增加一次性打印
     *             新增属性:TranPrintType= once(一次性打印) step(明细逐个打印)
     *             在所以打印的程序前加了对TranPrintType的判断,
     *             TranPrintType=once,打印的程序:DrawerOpenState,
     *             修改:PrintPluState,ReprintState,ReturnSummaryState,DrawerOpenState
     * <p>Ver 4.43.15
     *        <LI>(2007-01-22) CombPromotionHeader cache功能的bug,日期选择错误,出错也可用
     *             修改:NormalCombPromotion,CombPromotionHeaderV1.4
     *        <LI>(2007-01-22) 每次运行pos是在log输出版本信息
     *             修改:POSTerminalApplication
     * <p>Ver 4.43.14
     *        <LI>(2007-01-16) 统一编号的录入
     *             统一编号现在规定长度为8，只有打印了header后就不能在录入/修改了
     *             存放在tranhead.idno中，屏幕显示字段是BuyerID
     *             修改:BuyerStateV1.3
     *        <LI>(2007-01-16) 修改paidin/paidout中z/shift的统计数据的bug
     *             原先 取消的paidin/paidout数据 也是累加进z/shift
     *             修改：Transaction
     * <p>Ver 4.43.13
     *        <LI>(2007-01-15) 修改整笔打折功能（SI折扣）
     *             修改：DiscountStateV1.3,DrawerOpenStateV1.3,GetPropertyV1.3
     *                  ReturnOneStateV1.3,
     * <p>Ver 4.43.12
     *        <LI>(2007-01-12) 增加CombPromotionHeader表的全局cache功能
     *             新增属性：CombPromotionHeaderUserCache=true[是否开启CombPromotionHeader的cache功能 默认=true]
     *             cache的CombPromotionHeader范围是 昨天·今天·明天 三天可用的促销资料
     *             修改：CombPromotionHeader,GetProperty,MixAndMatchState
     *        <LI>(2007-01-12) plu cache数量满300才clear
     *             修改：CreamCacheV1.6
     * <p>Ver 4.43.11
     *        <LI>(2006-12-20) 历史小票打印可放在任意keylock*.conf
     *             修改：PrintHisTramsSelectModeState,PrintHisTransState
     *             statechar3.conf
     *        <LI>(2006-12-20) 历史小票打印会把rounddown打印出来
     *             修改：CreamPrinter_zh_CN
     * <p>Ver 4.43.10
     *        <LI>(2006-12-18) 修改第2件商品打X折的bug
     *             修改：NormalCombPromotion
     * <p>Ver 4.43.09
     *        <LI>(2006-12-12) 新增打印驱动,Epson TM-T88IIIP(切纸型)
     *             新增：POSPrinterEpsonU210
     *             修改: jpos.cfg
     * <p>Ver 4.43.08
     *        <LI>(2006-12-08) 全家报交易上传到sc后，head同detail的时间会有不匹配的现象
     *             特加log，下发门市调查。
     *        <LI>(2006-12-08) 全家报交易上传到sc后，只有detail没有head，而且重传不成功。
     *             发现如果重传时如果发现有重复detail会出错导致重传不成功。
     *             但当insert head失败时程序会del detail的，应该不会发生重复detail的现象。
     *             特加log，下发门市调查。
     *             修改：FamilymartPostProcessorV1.9
     * <p>Ver 4.43.07
     *        <LI>(2006-12-05) 优化（全家版）组合促销
     *             修改：NormalCombPromotion,CombSaleDetail,DacBase,
     *        <LI>(2006-12-05) 修正赠送商品组合促销无法使用的bug
     *             修改：NormalCombPromotion
     * <p>Ver 4.43.06
     *        <LI>(2006-11-21) 一般组合促销，采用低价优先原则(MixAndMatchVersion=2)
     *        <LI>(2006-11-21) 打印小票上折扣合计的bug(MixAndMatchVersion=2)
     *             修改：NormalCombPromotion,
     * <p>Ver 4.43.05
     *        <LI>(2006-11-17) 增加一般组合促销同折扣的低价优先原则
     *             修改：NormalCombPromotion
     * <p>Ver 4.43.04
     *        <LI>(2006-11-16) 增加历史交易打印流程,固定放在keylock2.conf中（按时间）
     *             修改：PrintHisTramsSelectModeState,PrintHisTransState
     *             statechar3.conf新增以下流程：
     *                #PrintHisTramsSelectModeState
     *                hyi.cream.state.PrintHisTramsSelectModeState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.NumberButton ,
     *                hyi.cream.state.PrintHisTramsSelectModeState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.ClearButton , hyi.cream.state.KeyLock2State
     *        <LI>(2006-11-16) 不同打印机可以采用不同receipt.conf
     *            新增属性：ReceiptConfName=receipt.conf[默认值]
     * <p>Ver 4.43.03
     *        <LI>(2006-11-07) 客户需要打印原价合计
     *             lineItem.getTotalOriginalAmount(),transaction.getTotalOriginalAmount(),
     *             修改：LineItem,Transaction
     * <p>Ver 4.43.02
     *        <LI>(2006-11-07) 有组合促销时应收金额显示又问题，现改成
     *                 非促销商品用：curLineItem.setAfterDiscountAmount(curLineItem.getAmount());(原来全部用这个，折扣会显示不对)
     *                促销商品用：curLineItem.setAfterDiscountAmount(curLineItem.getOriginalPrice().multiply(curLineItem.getQuantity()).setScale(2, BigDecimal.ROUND_HALF_UP));// 促销开始前将AFTDSCAMT设值为AMT
     * <p>Ver 4.43.01
     *        <LI>(2006-11-07) 增加历史交易打印流程,固定放在keylock2.conf中（按序号）
     *             新增属性：IsHisTransPrintNeedCut
     *                 历史交易打印，是否需要切纸（多个交易之间） 默认＝false（不切纸）
     *             statechar3.conf新增以下流程：
     *             #PrintHisTrans
     *             hyi.cream.state.PrintHisTransState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.NumberButton , hyi.cream.state.PrintHisTransState
     *             hyi.cream.state.PrintHisTransState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.ClearButton ,
     *             hyi.cream.state.PrintHisTransState, hyi.cxream.event.POSButtonEvent, hyi.cream.uibeans.EnterButton,
     *             修改：PrintHisTransState,GetProperty,CreamPrinter_zh_CN,CreamPrinter
     * <p>Ver 4.42.01
     *        <LI>(2006-10-25) 在原价基础上进行促销及打折
     *        <LI>(2006-10-25) 组合促销同折扣的低价优先原则
     *        <LI>(2006-10-25) 第2件商品打6折
     *             以价格最高的商品作为第二商品,配对原则是价格最高的打折商品和价格最高的没
     *             打折商品配对，当价格相同的情况下，以扫描的先后次序作为配对条件
     *             CombPromotionHeader.DiscountPercentage存放折扣率,如6折=6    [默认为6]
     *             新增属性：OriginalPriceVersion=1(lineItem.originalprice = 特价/区间特价)
     *                     OriginalPriceVersion=2(lineItem.originalprice = 原价)
     *                     由于该促销比较特别，需要指定id号
     *                     SecondPLUPromotionID=[默认为9](指定第2件商品打6折的id，对应combination_discount_type.cdt_id)
     *             修改：LineItemV1.3,NormalCombPromotion,PluGrpSubMatch,PluReadyState,PricingState
     *                  GetProperty,
     *        <LI>(2006-10-25) 用inline传输时如果daishou2..表是空的，就会抛出异常
     *             这样导致z上传失败，现改成不抛出异常
     *            修改：ClientV1.10
     *        <LI>(2006-10-25) putz是才传输HistoryTrans Cargo中传
     *             修改：ZStateV1.7
     *        <LI>(2006-10-25) 为HYIDouble增加Comparable接口
     *             修改：HYIDoubleV1.2
     * <p>Ver 4.41.01
     *        <LI>(2006-10-18) 调整黑标切纸位置  （0X0A==>0X20）
     *             修改：POSPrinterSP500V1.3
     *        <LI>(2006-10-18) 多页打印，小计没有右移
     *             新增属性：DepSalesPrintBlank=(右移的空格数)
     *             修改：CreamPrinter_zh_CNV1.7
     *        <LI>(2006-10-12) 重算z时删除daishou2数据(znumber,posnumber)
     *             修改：POSTerminalApplicationV1.13,DaiShouSales2V1.2
     *        <LI>(2006-10-12) 查价画面，如果没有会员价，屏幕会显示null
     *             现修改为 无会员价，就显示售价(unitprice)
     *             修改：PricingStateV1.3
     * <p>Ver 4.40.10 [cvs版本制成]
     *        <LI>(2006-10-10) 全家要求itemlist中的origprice取原价而不是特价，
     *             新增属性：OriginalPriceVersion＝1（取特价） ＝2（取原价）
     *             修改：PluReadyStateV1.8
     *        <LI>(2006-10-10) 小票要打印 原价-售价 所得的总折扣金额
     *             TotalDiscountAmount
     *            参考receipt.conf其它金额的显示方式，
     *                 折扣合计:[TotalDiscountAmount,-###0.00,R7]
     *             修改：TransactionV1.9
     * <p>Ver 4.40.09
     *        <LI>(2006-09-21) 全家pos端z,dep..表的accdate不会填写正确的日期，
     *             经查发现sybase字段名是小写,取字段值时应该要注意
     *             修改：FamilymartPosProcessor
     *        <LI>(2006-09-12) 全家pos端z,dep..表的accdate不会填写正确的日期，
     *             上传后sc端会填写当前accdate。问题在于上传历史z..时也会填写当前accdate。
     *             目前的方法是：上传z...时先在sc找到相同的posul_z获得正确的accdate,
     *             然后填写上传z...。
     *             修改：FamilymartPosProcessor
     *        <LI>(2006-09-12) 打印z中的dep，dep的打印规则不是从zreport.conf中获得，
     *             而是程序直接写，无法配置。由于客户修改打印格式，所以增加配置
     *             DepSalesPrintBlank＝11，希望右移的空格数
     *             修改：ReportGeneratorV1.3
     * <p>Ver 4.40.08
     *        <LI>(2006-09-08) 修改上传交易server端先insert detail后head,保证head数据一致性
     *             修改：TranProcessorThread,FamilymartPostProcessor
     * <p>Ver 4.40.07
     *        <LI>(2006-09-07) 读取代收定义档是如果发现错误程序会连读5次，现在去掉该功能，只读一次
     *             修改：DaishouSalesV1.5
     *        <LI>(2006-09-07) 修改代收定义档
     *             由于代收由原来代收键（会弹出明细窗口的）改成触屏键（直接列出代收明细）
     *             为配置简单，扩展daishou*.conf的规格，代收商品的触屏键直接从daishou*.conf生成
     *            这样代收档需在原来的基础上增加颜色及触屏代号的信息,5个参数
     *            01.市内传真,28330066   －－－》 01.市内传真,28330066,0,223,43,61,t
     *            代收商品的触屏键keycode在1000以上(程序定)，注意不能重复
     *            property.PreventScannerCatNO 不为空 该功能可用
     *            修改：POSButtonHomeV1.10
     * <p>Ver 4.40.06
     *        <LI>(2006-09-06) family 希望catno='000'(代收商品)能自动归为代收，trandetail.codetx='I'等，
     *             现利用PreventScannerCatNO参数 表明哪个分类是代收商品
     *             修改：PluReadState
     * <p>Ver 4.40.05
     *        <LI>(2006-09-05) family 希望备份plu等下传表
     *             修改：DacBase
     *        <LI>(2006-09-05) FamilymartPostProcessor Z 有问题，传历史z时，会把accday更新成当前sc的accday
     *             现该成：如果上传的z中的accday＝null or ＝197...，就取当前accday，否则不改变accday
     *             修改：FamilymartPostProcessor
     * <p>Ver 4.40.04
     *        <LI>(2006-08-22) historytrans,新增accdate [数据库需设default 1970-01-01]字段，并且要填写accdate
     *             修改：Client,Cargo,FamilymartPostProcessor,
     * <p>Ver 4.40.03
     *        <LI>(2006-08-21) 触屏画面显示时，如果触发了touchallowbutton.conf中
     *                 定义的键，则触屏画面自动消失
     *             新增：touchallowbutton.conf
     *             修改：CreamToolkitV1.8,POSButtonHomeV1.9,TouchPaneV1.5
     * <p>Ver 4.40.02
     *        <LI>(2006-08-09) 扫描商品时如果触屏画面显示，则触屏画面自动消失
     *             修改：PluReadyState
     * <p>Ver 4.40.01
     *        <LI>(2006-08-08) 新增挂单功能
     *             新增：TransactionHoldButton,dac.TransactionHold,dac.LineItemHold
     *                  TransactionHoldShowState,TransactionHoldState,TransactionUnHoldState
     *          修改：IdleState,Paying1State,Paying3State,SummaryState,
     * <p>Ver 4.39.01
     *        <LI>(2006-07-28)
     *             电子发票功能：通过pos日结产生historytrans数据，传到总部，该表
     *                 包含发票打印的数据(tranhead,trandtl,payment,plu...),
     *             pos部分只包含转档及inline上传
     *             新增属性:IsGenHistoryTrans,默认为no
     *             新增：HistoryTrans
     *             修改:ClientV1.6,FamilymartPostProcessorV1.3
     *                 PostProcessorV1.3,PostProcessorAdapterV1.3
     *                 ServerThreadV1.2,ZStateV1.5
     * <p>Ver 4.38.05
     *        <LI>(2006-07-27)
     *             修改代收bug
     *             修改:DaiShouDefV1.6,DaiShou2StateV1.9
     * <p>Ver 4.38.04
     *        <LI>(2006-07-20)
     *             支持新打印机StarTSP600,由于切纸指令不一样,需修改代码
     *             新增属性:PrinterCutPaperType,默认为空，＝star
     *             修改：POSPrinterPartner,CreamPrinter_zh_CNV1.4
     * <p>Ver 4.38.03
     *        <LI>(2006-07-18)
     *             java中如果月>12,会在年份上＋1，故要判断月份是否在12以内
     *             修改：DaiShou2StateV1.5
     * <p>Ver 4.38.02
     *        <LI>(2006-07-11)
     *             1.由于tec6400,tec7000相同位置的键值不一定相同,所以希望在posbutton3.conf
     *               中新增type, k7=tec7000专用键,k6=tec6400专用键
     *               k=共用键
     *             2.触屏显示时，允许按【触屏】键，切换触屏画面
     *             3.触屏显示时，如果出现警告信息,按【清除】键，先清除警告信息，
     *               接着按【清除】键，推出触屏.
     *             4.触屏也支持代售商品可自动归类
     *             修改：POSButtonHome,PluReadyState,PluMenuButton,
     *                  TouchMenuButton,TouchButtonCard2,TouchPane
     *                  GetPropertyV1.10,CreamPrinter_zh_CNV1.3
     * <p>Ver 4.38.01
     *        <LI>(2006-06-27)
     *             1.st7000触屏,st6400的posbutton3.conf合并
     *             2.触屏画面按钮数可定制
     *             3.按钮颜色可定制
     *             4.代售商品可自动归类
     *          修改：POSButtonHomeV1.7,POSTerminalApplicationV1.8,
     *              ScreenButtonV1.3,TouchMenuButtonV1.4,TouchButtonCard2V1.3
     *              TouchPaneV1.3,GetPropertyV1.9,CreamResource_zh_CNV1.9
     *          配置：ScreenButtonFont改名成TouchButtonFont:触屏按钮字体
     *               TouchButtonFontSize:触屏按钮字体大小  13
     *               TouchButtonXCount:触屏页面按钮的列数  12
     *                  TouchButtonYCount:触屏页面按钮的行数  5
     * <p>Ver 4.37.03
     *        <LI>(2006-06-06)代收如果没有匹配的就一个也不显示
     *             修改：DaiShou2State
     *        <LI>(2006-06-06)代收前缀prefix == null && prefix == "" 得到的结果应该一致
     *             修改：DaiShouDef,CreamResource_zh_CN
     *        <LI>(2006-06-05)连手输也要挡掉，只能用代售键选择
     *             修改：PluReadyState
     * <p>Ver 4.37.02  [待售商品不能通过扫描销售，cvs版本制成]
     *        <LI>(2006-06-01)客户希望 待售商品不能通过扫描 销售
     *             新增:属性PreventScannerCatNO＝'', 要阻止扫描商品的 catno
     *             修改:PluReadyState,GetProperty,CreamResource_zh_CN
     *        <LI>(2006-06-01)新增参数GetProperty.getConfFileLocale("GBK")
     *             用于画面配置文件的encode
     *             修改：Transaction,ConfigState,DaiShou3IdleState
     *                  KeyLock1State,Banner,DaiShouButton,ItemList,
     *                  PaymentMenuButton,PluMenuButton,ProductInfoBanner
     *                  TotalAmountBanner,CustomerBanner,CustomerTextBanner
     *                  CustPayingPaneBanner,CustProductInfoBanner
     *                  ReceiptGenerator,
     *        <LI>(2006-06-01)双屏时，popupmenu没有在销售画面居中
     *             修改：PopupMenuPane
     *        <LI>(2006-06-01)双屏,触屏按钮来源于posbutton3.conf
     *             修改：TouchButton,TouchButtonCard2,TouchPane
     * <p>Ver 4.37.01
     *        <LI>(2006-05-11)促销版本=3时出现bug,如果某一商品出现在2个促销中,则只会找最大id的那个促销
     *             修改：MixAndMatchState
     *        <LI>(2006-05-11)触发touch屏幕的按钮需要显示在主画面上
     *             新增:属性UseTouchButtonInScreen=no
     *             修改:POSTerminalApplication,EPOSBackground,TouchButton,
     *                 GetProperty,
     * <p>Ver 4.36.09
     *        <LI>(2006-05-10)去掉touchbutton.conf文件，仍然用posbutton3.conf
     *             修改：POSButtonHome
     * <p>Ver 4.36.08
     *        <LI>(2006-04-28)日结时,如果无法连到inline,可能会到当前dep等数据传到sc
     *             对策1:把sync交易放到日结结束后,避免inline过忙导致putz失败
     * <p>Ver 4.36.07
     *        <LI>(2006-04-27)st7000客显最后一位无法显示,
     *             LineDisplay=ST7000时不在最后一位显示数据
     * <p>Ver 4.36.06
     *        <LI>(2006-04-20)修改UpLoadZReportState,UpLoadWorkCheckState,选0时出错的问题
     * <p>Ver 4.36.05
     *        <LI>(2006-04-19)新增属性AutoSyncTransactionTimes
     *             方便更改sync的间隔时间
     * <p>Ver 4.36.04
     *        <LI>(2006-04-18)fix itemlist跳页的问题
     * <p>Ver 4.36.03
     *        <LI>(2006-04-14)修改了代收bug
     *                修改:DaiShou2State
     * <p>Ver 4.36.02
     *        <LI>(2006-04-06)修改了putz的bug
     *                原来只有RecalcAfterZ=yes 才会上传z,现在不判断此参数，一直会putz
     *                修改:ZState,Transaction
     * <p>Ver 4.36.01
     *        <LI>(2006-04-05) 把部分键盘移到触屏上（tec7000用）
     *              新增:TouchMenuButton.java,
     *                     new package : hyi.cream.uibeans.touch
     *                     键盘文件分成2个，一个用于触屏显示
     *              修改:PartnerKB128,Tec6400KB146,Tec6400KB81(键盘驱动)
     *                     POSButton,POSButtonHome,POSTerminalApplication
     *                     jpos.cfg文件 新增comp4:touchpane
     * <p>Ver 4.35.03
     *        <LI>(2006-03-31) 代收条码分析增强
     *            允许prefixlen任意长度
     *              daishoudef数据库修改，新增字段 PrefixLen
     *
     * <p>Ver 4.35.02
     *        <LI>(2006-03-20) 修改bug:输入代收条码时会出现多个代收定义。
     *                现在提供时间的check来确保唯一
     *              daishoudef数据库修改，新增字段 YearStartPos,YearLen,MonthStartPos,MonthLen
     *              修改:DaiShou2State
     *     <LI>(2006-03-20) 修改bug: shift提交wockcheck,version信息时会抛出Duplicate Exception
     *             先query再insert
     *             修改: dac.Version,dac.Attendance
     *     <LI>(2006-03-17) 修改bug: 刷员工卡bug
     *             修改: InOutNumberState
     * <p>Ver 4.35.01
     *     <LI>(2006-03-16) 梅林新需求，某些支付如（信用卡等）需要记录卡号，由于灿坤是以有
     *         此功能，现需要把该功能扩展到其他支付
     *      注意：目前tranhead只能记录一个卡号
     *       修改：payment.paytype 第6位 =1 需要记录卡号 ＝0 不需要记录
     *       卡号记录在tranhead.CRDNO  varchar(16)
     *            #Crd
     *                hyi.cream.state.Numbering2State, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.PaymentButton ,
     *                hyi.cream.state.ReadCrdNoState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.NumberButton ,
     *                hyi.cream.state.ReadCrdNoState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.EnterButton,
     *                hyi.cream.state.ReadCrdNoState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.ClearButton,hyi.cream.dac.Payment;Numbering2State;
     * <p>Ver 4.34.01
     *    <LI>(2005-12-12)对MixAndMatch设置cache,只导入符合日期(不包含time)的促销定义
     *          修改:MixAndMatch.java
     * <p>Ver 4.33.01
     *    <LI>(2005-12-12)新增驱动:Tec7000钱箱的驱动
     *        新增属性：Tec7000CashDrawerNo='1'钱箱的编号
     *        修改： CashDrawer7000Local.java,Tec7000CashDrawer.java
     *              cashdrawer7000.c
     * <p>Ver 4.32.02
     *    <LI>(2005-12-08)Trigger改成普通类,每次ruok后调用trigger(定时下传主档功能)
     * <p>Ver 4.32.01
     *    <LI>(2005-11-29)新增属性：RebootAfterDownMaster=yes
     *         不管是否有程序更新，只要属性＝yes pos就要重起，
     *         【例】梅林每天1点就会重起，以释放pos机资源
     *    <LI>(2005-11-29)修改：原来下传主档有2个入口，现在并在一个，并且根据
     *        /conf/downdaclist.conf里的表进行下载,如果配置文件不存在则用旧版程序
     *        Client.java,PluUpdateState.java,GetProerty.java
     *
     * <p>Ver 4.31.05
     *    <LI>(2005-11-14)新增：Client新增入口，运行时带auto参数就可直接运行程序
     *        用于程序或脚本自动调用
     * <p>Ver 4.31.04
     *    <LI>(2005-11-09)新增属性：KeyboardType=""  【默认＝""】
     *        不同pos机读不同配置文件，有利于版本更新
     *        KeyboardType不存在，就读默认的posbutton3.conf
     *        例：KeyboardType="tec",就读posbutton3tec.conf
     *        修改：CreamToolkit.java
     * <p>Ver 4.31.03
     *    <LI>(2005-11-07)优化，RebootAfterZ=no
     *                    做完z后是否需要reboot,以释放资源【目前不能更新，因为无法判断z是否正常结束】
     * <p>Ver 4.31.02
     *    <LI>(2005-11-04)优化，把常用的dateformate放入CreamCache中。
     *
     * <p>Ver 4.31.01
     *    <LI>(2005-11-01)把所有属性放入GetProperty中，一个属性对应一个方法
     *                      取属性的值都应通过GetProperty
     *                    CreamProperties 改名 CreamPropertyUtil[成为工具，而非取属性的地方，
     *                    保存属性功能目前保留]
     * <p>Ver 4.30.08
     *    <LI>(2005-10-31)新增支付规则，某些支付溢收不允许结帐。
     *                      系统能先处理此种支付，在处理其它
     *                    去掉property:DontAllowGenerateSpillAmount
     *                      修改：Paying3State
     * <p>Ver 4.30.07
     *    <LI>(2005-10-28)修改SignOffForm.java,能在jdk1.5下运行
     * <p>Ver 4.30.06
     *    <LI>(2005-10-25)把inline server端和POS端的的property "OverrideUsePlu" 改成
     *                    "DownloadMinPrice"，表示server是否下传minPrice字段和POS是否检查
     *                    此最低变价金额
     *    <LI>(2005-10-25)去掉"ItemListNeedBackLabel" property
     *
     * <p>Ver 4.30.05
     *       <LI>(2005-10-20)梅林取最大交易序号有问题，应以max(sysdate)为依据而非max(transeq)
     *           修改：SequenceNumberGetter
     * <p>Ver 4.30.04
     *    <LI>(2005-10-19)
     *            梅林促销新增[批发]方式
     *            mixandmatch: type=5   pricetype=1 只有group1qty有效
     *          修改：MixAndMatchState,CreamCache
     * <p>Ver 4.30.03
     *    <LI>(2005-10-17)修改InOutNumberState.java
     * <p>Ver 4.30.02
     *    <LI>(2005-10-10)代收增加机能：代收水电煤或电话费时，输入条码后，弹出窗口进行再此确认
     *        新增属性：DaiShou2NeedWarning,默认为'no', 为'yes'则弹出警告窗口
     *        新增:DaiShou2WarningState
     *        修改：DaiShou2State.java,DaiShou2ReadyState.java,
     *             CreamResource_zh_CN.java
     *             StateChar3.conf
     *        #       DaiShou2State   2003-06-19   DaiShou2CheckState   DaiShou2ReadyState
     *        #                       2005-10-10   DaiShou2WarningState
     *        hyi.cream.state.IdleState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.DaiShouButton2, hyi.cream.state.DaiShou2State
     *        hyi.cream.state.DaiShou2State, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.ClearButton,
     *        hyi.cream.state.DaiShou2State, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.NumberButton, hyi.cream.state.DaiShou2State
     *        hyi.cream.state.DaiShou2State, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.EnterButton,
     *        hyi.cream.state.DaiShou2State, jpos.events.DataEvent, jpos.Scanner,
     *        #hyi.cream.state.DaiShou2CheckState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.SelectButton, hyi.cream.state.DaiShou2ReadyState
     *        hyi.cream.state.DaiShou2CheckState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.SelectButton, hyi.cream.state.DaiShou2WarningState
     *        hyi.cream.state.DaiShou2CheckState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.ClearButton, hyi.cream.state.IdleState
     *        hyi.cream.state.DaiShou2WarningState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.SelectButton,
     *        hyi.cream.state.DaiShou2WarningState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.ClearButton, hyi.cream.state.IdleState
     *        hyi.cream.state.DaiShou2ReadyState,,,
     * <p>Ver 4.30.01
     *       <LI>(2005-10-04)为了台湾�坤繁体版做的一些修改：
     * 　　　　　　////－使用mysql_jdbc_big5.jar以解决big5码有'\'字符的问题
     * 　　　　　　////property中的DatabaseDriverName改为旧的org.gjt.mm.mysql.Driver
     *          －PopupMenuPane改成"Window" class，以解决在JRE 1.5无法显示的问题
     *          －加快Indicator显示
     *          －修改DacBase.getField()以解决使用新版本MySQL JDBC driver时，tinyint字段会返回Short object的问题
     *
     * <p>Ver 4.24.03
     *       <LI>(2005-09-28)每次新交易之前gc一次
     *                    修改：IdleState.java
     * <p>Ver 4.24.02
     *    <LI>(2005-09-23)全家报：在夜间定时启动的更新主挡及程序时有时会停在
     *                    "程序更新中....".经查发现
     *                    DacTransfer.reboot（）中如果reboot前throw Exception
     *                    则不会运行reboot脚本，已修正。
     *                    DacTransfer.java
     * <p>Ver 4.24.01
     *       <LI>(2005-09-13) 新增plu全局cache,每次新交易时清除cache
     *                      新增：CreamCache.java
     *                       修改：PLU.java
     *
     *    <LI>(2005-09-13) 新增diy功能
     *                     灿坤需要在pos实现销售【组装电脑功能】,对所选的配件输入一个打折金额,然后分摊到diy明细
     *                     diy变价部分整合到促销金额中，并在trandetail中会生成一笔codetx='M'的明细
     *                     修改：
     *                         properties.DIYSupport 默认为no,
     *                         properties.MaxDIYDiscountRate 最大折扣
     *                         trandetail.diyIndex char(1)
     *                     CreamResource_zh_CN.java,IdleState.java,ItemList.java
     *                     MixAndMatchState.java,PluReadyState.java,Transaction.java
     *                     新增：
     *                     DIYButton,DIYOverrideButton,DIYOverrideAmountState,
     *                     InputDIYIndexState.java
     *                     payingpang1.conf
     *                          组合促销,DisplayTotalMMAmount,n
     *                          DIY折扣,TotalDIYOverrideAmount,n
     *                     statechar3.conf
     *                          #DIYButton
     *                          hyi.cream.state.IdleState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.DIYButton, hyi.cream.state.InputDIYIndexState
     *                          #DIYOverrideButton
     *                          hyi.cream.state.IdleState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.DIYOverrideButton, hyi.cream.state.DIYOverrideAmountState
     *                          hyi.cream.state.DIYOverrideAmountState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.NumberButton, hyi.cream.state.DIYOverrideAmountState
     *                          hyi.cream.state.DIYOverrideAmountState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.ClearButton,
     *                          hyi.cream.state.DIYOverrideAmountState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.EnterButton,
     *                          #InputDIYIndexState
     *                          hyi.cream.state.InputDIYIndexState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.NumberButton, hyi.cream.state.InputDIYIndexState
     *                          hyi.cream.state.InputDIYIndexState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.ClearButton,
     *                          hyi.cream.state.InputDIYIndexState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.EnterButton,
     *
     * <p>Ver 4.23.03
     *    <LI>(2005-07-01) Client.java  建议隔60秒ruok  隔10分钟sync
     *                     properties.RuOkInterval = 60000
     *                     去掉trigger线程，放入Client
     *                     把版本信息移入Version.java中
     * <p>Ver 4.23.02
     *    <LI>(2005-06-20) 灿坤黄卡修正
     *    <LI>(2005-06-20) 灿坤需结帐时一起打印小票
     *                       修改：property.ReceiptPrint=no
     *                          DrawerOpenState.java目前只打印detailcode='s'的item
     * <p>Ver 4.23.01
     *    <LI>(2005-06-13) 灿坤黄卡
     *                       新增:YellowAmountState
     *                     修改：LineItem,OverridePopupMenu,OverrideAmountState
     *                          Transaction,ItemLine,IdleState,CashierRightCheckState
     *                          CreamResource_zh_CN
     *                        property表
     *                        OverrideVersion=2   //适用于黄卡变价
     *                        plu表
     *                        新增字段：yellowLimit //黄卡折扣最低限值
     *                        trandtl表
     *                        新增字段：yellowamount //黄卡折扣金额
     * <p>Ver 4.22.01
     *    <LI>(2005-06-03) 梅林盘点Bug，
     *                     修改:ServerThread,
     *                     说明：新增加处于盘点数据导入中的状态（9），也应属于未结束盘点单中的一种。
     * <p>Ver 4.21.01
     *    <LI>(2005-03-23) 灿坤促销，
     *                       修改:LineItem,PLU,Transaction,MixAndMatchState,PluReadyState,
     *                           StateToolkit,SummaryState,ItemList,CreamResource_zh_CN
     * <p>Ver 4.20.03
     *    <LI>(2005-01-04) 每次做z,shift前会做SyncTransaction
     *                       修改:ShiftState,ZState
     *    <LI>(2005-01-04) 自编码如果是8码的价格从plu中取并且可以用数量键
     *                     其他的从条码中去，无法使用数量键
     *                     修改：PluReadyState,SetQuantityState,LineItem,Transaction
     *    <LI>(2005-01-04) 算完z并在打印z之前，如果发现z和tranhead金额不一致时重算z
     *                     修改:新增属性 RecalcAfterZ=no
     *                     ZState,CreamReasource_zh_CN
     *    <LI>(2005-01-04) 减少pos端与inline的连接频率
     *                       修改:新增属性 TransactionCheckerThread=true
     *                       SystemInfo,Client,Server,TransactionCheckerThread
     * <p>Ver 4.20.02
     *    <LI>(2004-12-06) 使ItemList能显示背景字，适用于[退货]等功能
     *                     修改:新增属性 ItemListNeedBackLabel=no
     *                     ItemList,ReturnNumberState,ReturnSummaryState,CreamResource_zh_CN
     * <p>Ver 4.20.01
     *    <LI>(2004-12-05) 灿坤信用卡支付
     *                     1)有多种信用卡支付，采用menubutton,[PaymentMenu2Button]
     *                     2)在支付画面中弹出信用卡支付类型 POSTermainalApplication.setCrdPaymentMenuVisible(true)
     *                     3)选择支付方式后，要输入卡号 ReadCrdNoState
     *                     修改:POSTermainalApplication,Transaction,Numbering2State,Paying1State
     *                         SummaryState,PaymentMenu2Button,PopupMenuPane,CreamResource_zh_CN
     * <p>Ver 4.19.01
     *    <LI>(2004-10-22) 灿坤需要代售电话卡，但不能打印在发票上
     *                     修改：DaiShouButton,PluReadyState,LineItem,CreamPrinter_zh_CN
     * <p>Ver 4.18.04
     *    <LI>(2004-09-07)pos日结账时取，server端取日历当前会计日作为z帐的会计日，Client端根据返回的会计日填写pos机上的z,tranhead的会计日
     *                    重传z帐时，若不给定会计日，而且如果pos机z帐含会计日，则以pos机z帐原有会计日为准，
     *                                                 如果pos机z帐不含会计日，以日历当前会计日为准；
     *                    如果重传z帐时，给定了会计日，则以给定的为准
     *
     * <p>Ver 4.18.03
     *    <LI>(2004-08-17)增加输入销退原因（Reason表中reasonCategory=19）写入trandetail表中discno字段
     *                    参数为ReturnReasonPopup=yes
     *
     * <p>Ver 4.18.02
     *    <LI>(2004-08-16)将盘点时输入大数量警告条件，修改为对（正、负）大数量都警告
     *
     * <p>Ver 4.18.01
     *    <LI>(2004-08-10)在增加每笔交易必须输入营业员功能(针对灿坤)
     *
     * <p>Ver 4.17.01
     *    <LI>(2004-07-12)在上传盘点数据画面中，上传后校验前后台盘点记录数是否一致，如果一致则驱动后台导入盘点数据
     *                    同时作相应的成功或错误信息提示
     *                    InventoryExitState.java， ServerThread.java, Client.java
     *    <LI>(2004-07-12)盘点画面结束时自动跳转到上传盘点数据画面
     *                    InventoryExitState.java
     *    <LI>(2004-07-12)进入盘点画面时检查后台是否已经盘点初始化，如果还未进行，则提示错误信息，
     *                    InventoryState.java， ServerThread.java, Client.java
     *    <LI>(2004-07-12)针对 梅林 将POS机"盘点初始化"改成"清除历史盘点资料(盘点初始化)"
     *                    Keylock2.conf, Keylock2.conf
     *
     * <p>Ver 4.16.01
     *    <LI>(2004-07-08)盘点时如果输入的数量超过Property.MaxInventoryQty所限定的数量，
     *          则警告用户再次确认数量，如果Property.MaxInventoryQty=0可屏蔽该功能
     *          InventoryQuantityState.java
     *
     * <P>Ver 4.15.02
     *    <LI>(2004-07-03)修改DacBase update 方法， 在修更新表记录时只列出有变化的字段，
     *                    以节约传输，解析,更新数据库资源消耗, DacBase.java
     *  <P>Ver 4.15.01
     *    <LI>(2004-06-03)BigDecimal改为HYIDouble类型
     *    <LI>(2004-06-03)DacBase中String改为使用类型StringBuffer
     *    <LI>(2004-06-03)DacTransfer中reboot命令改为/usr/bin/reboot，halt命令改为/usr/bin/halt
     * <P>Ver 4.14.03
     *    <LI>(2004-04-18)把log的时间格式改成yyyy-MM-dd，方便查询
     *    <LI>(2004-04-18)增加inline server的log
     *    <LI>(2004-04-15)发现DacBase.flushOSBuffer有问题，改成只在Transaction.store()才
     *             调用，并且设置sync的timeout(5秒)
     *       <LI>(2004-04-15) 变价规则，Property.OverrideUseCat='yes',可以在cat中获得变价范围
     *             在cat表中如果发现 MICROCATNO.OVERRIDEAMOUNTLIMIT<>(null && "" && "1")可获得变价范围，否则
     *             MIDCATNO.OVERRIDEAMOUNTLIMIT<>(null && "" && "1")可获得变价范围，否则
     *             CATNO.OVERRIDEAMOUNTLIMIT<>(null && "" && "1")可获得变价范围，否则
     *             Property.OverrideAmountRangeLimit<>(null && "" && "1")可获得变价范围，否则
     *             可无限变价[0：不能变价,1:无限变价]
     *          更改OverrideAmountState
     *       <LI>(2004-04-15) 连续刷员工卡时增加成功提示功能.InOutNumberState.java
     * <P>Ver 4.14.02
     *    <LI>(2004-04-12) pos仍有死机现象,日结[发生在z.update,dep.update时]
     *             Transaction.tran_Processing中增加log
     *             和交易[发生在关抽屉后],在DrawerOpenState.exit(),InitialState.entry()
     *          中增加log,
     *       <LI>(2004-04-03) DacBase.flushOSBuffer[2003-09-08 (Ver 4.05)}此方法会导致inlineserver
     *              性能下降,已去掉
     *    <LI>(2004-04-03) 更改update Z，先update depsales在update Z，并增加了
     *               try ... catch 及 log
     *       <LI>(2004-04-03) 发现盘点没有数据时传输会抛出错误，现在改成传输正常
     *                 盘点时等待反馈的timeout时间太短，现设为60秒
     *    <LI>(2004-04-03) 发现Tec6400CashDrawer.waitForDrawerClose()逻辑有问题，
     *              drawerClosed设初始值的地方不对,并且没有必要开一个Thread,
     *              现已经修改。可以避免应为检测关抽屉而死机的问题
     *    <LI>(2004-04-02) 交易无法正常结束,在DrawerOpenState.java中增加log,方便调查
     *    <LI>(2004-03-31) z序号有取错和断号的问题,在ZReport.java中增加log,方便调查
     *    <LI>(2004-03-30) 交易无法正常结束,在Transaction.java中增加log,方便调查
     *
     * <P>Ver 4.14.01
     *    <LI> 2004-03-10 上传数据首先放入Cargo的队列中，然后再统一处理，用户选择关机命令
     *    <LI>      以后，等数据全部上传完成以后自动关机
     *
     * <P>Ver 4.13.12
     *    <LI> 修改OpenPrice商品折扣后金额填写有误的问题
     *
     * <P>Ver 4.13.11
     *    <LI> 更正TranProcessorThread中更新历史库存时的错误
     *    <LI> 新增铁通通信费代收项目
     *
     * <P>Ver 4.13.10
     *    <LI> 增加 ThreadWatchDog 在Server里防止Server 端处理交易的线程死掉
     *    <LI>                     在ePOS里防止Client 线程死掉
     *    <LI> 更新下传主档Cache功能，现在由eComm下传完主档以后调用Server -getmaster准备主档文件
     *
     * <P>Ver 4.13.09
     *    <LI>修改ZReport.createZReport()取得Z序号的逻辑。原逻辑不正确，如果Z上传失败会导致Z序号错误。
     *    <LI>修改ZReport.queryByDateTime()，预防万一在有多笔1970-01-01记录的情况下，也让它去取最大
     *        序号的那一笔。
     *
     * <P>Ver 4.13.08
     *    <LI>加长Client的time-out时间。
     *    <LI>在校验代收条码时增加catch NumberFormatException。
     *
     * <P>Ver 4.13.03
     *    <LI>(2004-01-12)
     *
     * <P>Ver 4.13.02
     *    <LI>(2004-01-06)InlineServer接收到交易以后，先保存到临时文件，然后再重命名
     *    <LI>(2004-01-06)解决退货、交易取消计算来客数错误的问题
     *
     * <P>Ver 4.13.01
     *    <LI>(2003-12-25)解决交易序号跳号会使重算被中断的问题。
     *    <LI>(2003-12-25)解决纯代收应该不算来客数的问题。
     *
     * <P>Ver 4.13
     *    <LI>(2003-12-25)增加解交易线程TranProcessThread，每一分钟解交易一次。
     *                    如果要使用此线程解读交易，则在Server端的cream.conf中：
     *                       PostProcessor=PostProcessorImplA
     *                    另外必须增加一个程序增加包：jdom.jar
     *    <LI>(2003-12-25)解决交易序号可能会变成0，还有交易重印后交易序号会重复的问题。
     *    <LI>(2003-12-25)存储交易前先记录配达单号，以避免配达单号重复的问题。
     *    <LI>(2003-12-25)为上海领袖服饰增加“营业员”按键。
     *    <LI>(2003-12-25)增加property "LicenseInfo"表示要显示的license info，如果"DontShowLicenseInfo"为"no"，
     *                    则会显示"LicenseInfo"内容，如果此"LicenseInfo"不存在，则显示resource字符"CompanyTitle"
     *
     * <P>Ver 4.12.05
     *    <LI>(2003-12-19)因为用JDK1.4.2 compile不过，将Protector移到protector package下
     *    <LI>(2003-12-24)解决MixAndMatchVersion=2时，若从结账画面返回至销售画面时，会因调用
     *                    Transaciton.initPaymentInfo()导致LineItem的AfterDiscountAmount的值变成与Amount相同
     *    <LI>(2003-12-24)解决部分退货会多打印payment的问题。
     *    <LI>(2003-12-24)解决keylock1.conf设置CheckCashierState时，做完退货操作后会死机的问题。
     *
     * <P>Ver 4.12.04
     *    <LI>(2003-12-12)修改盘点输入无法更正的问题。
     *    <LI>(2003-12-17)增加property "PrintImmediateRemove"(default is "no"),可设置立即更正是否需要打印。
     *    <LI>(2003-12-17)解决负项销售没有打印payment部分的问题。
     *    <LI>(2003-12-17)解决负项销售状态结束时应返回至Keylock菜单的问题。(必须更换statechart3.conf)
     *    <LI>(2003-12-17)解决负项销售找零金额计算错误的问题。
     *    <LI>(2003-12-17)解决SignOn状态无法再按〔收银员〕键进入收银员菜单的问题。(必须更换statechart3.conf)
     *
     * <P>Ver 4.12.03
     *    <LI>(2003-12-05)因为灿坤的receipt.conf中交易日期给的长度不够，造成如果要trim length的话会把后面砍掉，
     *                    但是灿坤的receipt.conf格式文件种类众多，不想更新receipt.conf，所以在遇到日期pattern
     *                    的时候，就不要right trim了。 蛮tricky!
     *    <LI>(2003-12-05)修改ReceiptGenerator字符长度没有切准的问题。
     *
     * <P>Ver 4.12.02
     *    <LI>(2003-12-05)解决交易取消在使用receipt.conf时，会跑出很长一段纸的问题。
     *    <LI>(2003-12-05)解决cashier表没有CASRIGHTS字段的时候借零投库会死掉的问题。
     *
     * <P>Ver 4.12.01
     *    <LI>(2003-12-04)修改syncTransaction查找本机相关序号时，增加"POSNO=xx"的条件。
     *                    这样可以让它即使在POS更换机号的时候，也不会让后台误认为有数据未上传。
     *    <LI>(2003-12-04)把检查单头单身不符的时间拉长为14天。
     *
     * <P>Ver 4.12
     *    <LI>(2003-12-01)有两种决定会计日期的models:
     *                    如果AccountingDateShiftTime="afternoon"(default)，则超过换日时间后为(d+1)日；
     *                    否则(="morning")超过换日时间后仍为(d)日，在换日之前为(d-1)日。
     *    <LI>(2003-12-01)增加inline "putobject2" protocol，可以加快盘点数据上传速度。
     *    <LI>(2003-12-01)修正没有update "NextTransactionNumber"的问题。
     *    <LI>(2003-12-01)如果交易数据存储失败，仍将"NextTransactionNumber"往上加。以免在有残余交易记录
     *                    存在数据库的情况下，会导致一直无法成功存储的问题；并且禁止用户继续操作，必须叫修
     *                    或重启。
     *    <LI>(2003-12-01)在交易数据存储成功之后，才会执行打印动作。
     *                    结账操作程序：开抽屉->存储交易->打印支付部分。
     *                    所以如果数据存储失败，发票下方的支付部分不会打印出来。
     *    <LI>(2003-12-01)增加一个property "ShowAmountWithScale2" (default is "no"), 表示销售画面
     *                    是否金额要显示两位小数。
     *    <LI>(2003-12-01)现金支付也必须看是否为“自动结清余额”(payment.PAYTYPE第7位），如果是"0"的话,
     *                    在结账的时候不能直接按确认键就自动结账。(必须同时更换statechart3.conf）
     *    <LI>(2003-12-01)增加property "ChangeAmountUpperLimit"(default is 100)，如果找零金额大于等于此
     *                    值，则会报警。
     *    <LI>(2003-12-01)更换后台inline server使用的MySQL JDBC driver。
     *    <LI>(2003-12-01)修改第二次盘点时数据无法显示的问题。
     *    <LI>(2003-12-02)减少盘点录入时所使用的内存空间，这样在大量数据录入的情况下不易使速度变慢。
     *    <LI>(2003-12-02)盘点结束应回到原Keylock菜单。(必须同时更换statechart3.conf）
     *    <LI>(2003-12-02)输入的金额小于结帐金额时，在提示"付款金额不足"后，增加发出一声报警声。
     *    <LI>(2003-12-02)喜士多启用receipt.conf打印发票。并解决收执联和存根联应同步卷动的问题。
     *
     * <P>Ver 4.11.06
     *    <LI>(2003-11-26)修改queryMember时先查找commdl_member再查找member.
     *    <LI>(2003-11-26)修改还圆金发票格式。
     *    <LI>(2003-11-26)取消还圆金输入金额有2000元的上限。
     *
     * <P>Ver 4.11.05
     *    <LI>(2003-11-24)还元金（红利）输入金额不能超过2000元和非会员不能使用还元金支付要提示用户相应信息。
     *    <LI>(2003-11-24)盘底结束要回到原始KeylockState。
     *    <LI>(2003-11-24)查询单品时要显示还圆金比率。
     *    <LI>(2003-11-24)修改变价后还圆金没有重新计算的问题。
     *    <LI>(2003-11-24)修改发票中会员前日累计还圆金可能有错误的问题。
     *
     * <P>Ver 4.11.04
     *    <LI>(2003-11-21)CStorePostProcessor:因为怀疑JRE可能造成停滞在lockObject，所以先拿掉，先舍弃更新库存可能不准的问题
     *
     * <P>Ver 4.11.03
     *    <LI>(2003-11-21)还元金（红利）输入金额不能超过2000元。
     *    <LI>(2003-11-21)非会员不能使用还元金支付。
     *    <LI>(2003-11-21)丢弃property "NextTransactionSequenceNumber".
     *
     * <P>Ver 4.11.02
     *    <LI>(2003-11-19)增加POSPrinterTEC6400，梅林改用receipt.conf。
     *    <LI>(2003-11-19)让交班日结结束后可以回到原Keylock state。
     *
     * <P>Ver 4.11.01
     *    <LI>(2003-11-17)CStorePostProcessor在insert交易之前不lock lockObject，因为怀疑Java因失误
     *                    造成停滞在synchronized lockObject上。
     *    <LI>(2003-11-17)修改Tec6400CashDrawer, 改用busy wait的方式等待抽屉关闭，同样因为怀疑Java
     *                    会在notifyAll()的时候没有通知到wait()，使得偶有wait forever现象。
     *    <LI>(2003-11-18)增加property "ConfuseTranNumber"(缺省为"no")，表示是否使用乱数交易序号作为
     *                    显示和打印。若要在画面上显示乱数交易序号，在screenbanner.conf中把交易序号的
     *                    property name同时改为"PrintTransactionNumber"。
     *    <LI>(2003-11-18)借零、投库输入金额不可为零。
     *
     * <P>Ver 4.11
     *    <LI>(2003-11-13)为灿坤增加还元金功能。
     *                - property增加"OnlyMemberCanUseRabate"和"RebatePaymentID"
     *                - tranhead和trandetail各增加3个字段
     *                - 增加一个下传表rebate
     *
     * <P>Ver 4.10
     *    <LI>(2003-11-04)增加Store.createCache(),避免在store表变化后下传会不起作用。
     *    <LI>(2003-11-04)加长Client的ACK_TIMEOUT_INTERVAL，可能会解决有时候上传Z以后会离线的问题，
     *                    因为原来Client在发出对象以后，只等5秒，现在加长为15秒。
     *    <LI>(2003-11-04)Inline server增加一个TransactionCheckerThread，用来检查是否存在tranhead与
     *                    trandetail笔数不符的交易，如果有的话将之删除，以便下次让inline client再重新
     *                    送上来。这样可以保证后台不会有partial transaction。
     *    <LI>(2003-11-04)Inline client第一次连到server后，会先通过queryMaxTransactionNumber和
     *                    queryMaxZNumber两个命令询问后台的交易序号和Z帐序号最大值。若后台的序号比前台
     *                    大，则起始序号必须用后台的最大序号加1。这样可以解决两个问题：
     *                    a. 若前台机器换机，那新装的POS机可以继续先前的序号往上加。这样总部不会收到有
     *                       相同序号的记录。
     *                    b. 若POS因不当关机或其他硬件因素导致已上传的交易丢失，也不会造成交易序号重复
     *                       的问题。
     *    <LI>(2003-11-06)增加property "UseFloppyToSaveData"(缺省为"no")，表示在交班或日结时若离线或
     *                    上传失败是否需要插软盘。
     *    <LI>(2003-11-10)Inline server property增加"UpdateInventory"(缺省为"yes")，表示在交易上传
     *                    时是否需要更新库存表。灿坤暂时设置成"no"。
     *    <LI>(2003-11-11)增加propert "ComputeDiscountRate"(缺省为"no")，表示是否计算trandetail中的
     *                    折扣率和折扣代号字段。除了铠捷以外应设置为"no"，这样可以减少在变价的时候在
     *                    cream.conf里面会有因为找不到distype表而抛出的意外log。
     *    <LI>(2003-11-11)避免结账后死机的问题：等待交易储存结束设time-out时间。
     *    <LI>(2003-11-11)在结账后若出现“请关上抽屉”，关上抽屉后会将讯息清除。
     *
     * <P>Ver 4.09
     *    1. (2003-10-30)再次修改交易资料删除历史数据时会误把10天前的都删掉。
     *    2. (2003-10-30)退货时若发现trandetail的数据一条都没有的时候，提示用户到后台退货。
     *    3. (2003-10-30)避免若有很多交易未上传会导致结账时等候，感觉像死机一样。
     *    4. (2003-10-30)修改价格为0时会无法变价的问题。
     *    5. (2003-10-30)上传交易时会检查交易头记录的身笔数和实际的身笔数是否一致，如果不一致，丢弃它。
     *    6. (2003-10-30)修改退货或交班可能会造成交易序号跳号的问题。
     *
     * <P>Ver 4.08
     *    (2003-10-23)修改交易资料删除历史数据时会误把10天前的都删掉。
     *
     * <P>Ver 4.07
     *    1. (2003-10-19)修改上传历史depsales, daishousales, daishousales2时会计日期会填错的问题
     *    2. (2003-10-22)修改如果序号大的交易时间比序号小的时间早的话，会造成拼命上传交易的问题。
     *
     * <P>Ver 4.06.02
     *    1. (2003-10-15)允许cream.jar时若带参数-version会显示程序版本号和machine ID。
     *    2. (2003-10-15)增加property "ContinuousLogon"(表示在上下班登录时，是否可以连续登录，缺省为"no"）
     *       灿坤设置为"yes": INSERT INTO property (name, value) VALUES ('ContinuousLogon', 'yes');
     *
     * <P>Ver 4.06.01
     *    1. (2003-9-25)解决喜士多的“打印X帐”结束后会回到Keylock3菜单的问题。statechart3.conf和keylock2.conf要跟着修改。
     *    2. (2003-10-8)解决有可能因为机器速度慢，导致储存交易的线程还没结束之前交易数据就被清除了。
     *       此问题现象：会导致交易数据的序号不断重复，无法储存交易数据。
     *    3. (2003-10-8)解决删除trandetail过期数据因逻辑修改产生的bug。
     *       此问题现象：在cream.log中每天凌晨会看到什么"SYSDATE"字段不存在的错误信息，而且trandetail
     *       表中60天前的历史数据无法删除。
     *
     * <P>Ver 4.06
     *    1. (2003-9-24)修改历史交易数据上传后，CStorePostProcessor要增加历史inv库存表销售数、和减少之后日期的期初库存数
     *    2. (2003-9-25)解决查询会员会始终要用户确认的问题(Client.commandLineMode default设为false)
     *
     * <P>2003-09-08 (Ver 4.05)
     *    1. 防止变价时输入太大的数字，导致trandetail会有999999999999.99的超大数值。
     *    2. 在insert/update/delete数据库记录后，立即用"sync"命令flush OS buffer，以避免数据因不当
     *       关机而丢失。
     *    3. 交易中的tranhead.MNMCNT0用来记录组合促销成立的总次数，其他MNMCNT[1..4]废弃不用。
     *    4. 增加property "MaxTransactionNumber"(最大交易序号，到达此交易序号后归1，缺省为99999999)
     *       和"TransactionReserved"(交易保留天数，缺省为60)。
     *       修改inline "synctrans" protocol。
     *    5. 增加细分类。后台posdl_cat, posdl_plu, posdl_trandtl表增加thinCategoryNumber，前台cat,
     *       plu, trandetail表增加THINCATNO。
     *    6. 灿坤的posdl_plu和plu增加单位字段(SmallUnit)，销售画面增加显示"单位"(修改itemlist.conf)。
     *    7. 灿坤销售画面增加显示"货号"(修改itemlist.conf)。
     *    8. 灿坤去掉结账画面的"客层"(修改payingpane1.conf)。
     *    9. 增加property "TurnKeyAtOverrideAmount"(变价时是否需要转动钥匙（缺省为no）)。
     *       -> 灿坤必需设置为yes。
     *   10. 增加property "CanDaiShou2Return"(公共事业费代收是否允许退货（缺省为yes）)。
     *       -> 喜士多要求设置成no。
     *   11. 增加property "IgnoreKeylockWithinTransaction"(在交易输入当中忽略钥匙转动事件（缺省为no）)。
     *       -> 铠捷必需设置为yes。
     *   12. 铠捷的画面按键增加了"支票"和"查价"按钮。
     *   13. 增加软件防拷功能，需要输入注册码。
     *   14. ePOS's background is anti-aliased.
     *   15. 增加即时备份机制。增加property "MySQLRemoteBackupDirectory"(MySQL远端备份目录
     *       （rsync server directory）)，必需开启备份机的rsync server服务，并设置备份目录。
     *       config.conf可以增加一个功能:
     *         XX.从备份机回复数据库,recoverDBFromBackUp
     *       -> 此功能测试中，暂不对外开放。
     *   16. Remove unused items in CreamResource_zh_CN.
     *   17. (2003-9-18)修正可能因为单品数太多导致PLU下传时会time-out。
     *
     * <P>2003-8-25 (Ver 4.04.03)
     *    1. 防止更正记录的AfterDiscountAmount会不等于Amount(Transaction)(虽然不知道为何会有这种情况发生)。
     *    2. Trigger.shrinkLogFiles()时先close log files，以免会有log file无法写入的问题。
     *
     * <P>2003-8-19 (Ver 4.04.02)
     *    1. 为喜士多增加公共事业费代收的会计科目300(CStorePostProcessor)。
     *    2. 修改DacViewer.paint()有NullPointerException的问题。
     *
     * <P>2003-8-18 (Ver 4.04.02)
     *    修改重新计算功能，重新计算时,shift,z,depsales,daishousale 一并重新计算
     *
     * <P>2003-8-16 (Ver 4.04.01)
     *    修改公共事业费代收明细上传时，会计日期应取决于Z帐序号而不是当前的会计日。
     *
     * <P>2003-8-14 (Ver 4.04)
     *    修改秤重商品应该不可以修改数量的问题。
     *
     * <P>2003-8-10 (Ver 4.03)
     *    增加property "DontNeedTurnKeyAtStart", 表示可以设置在程序开始的时候是否需要先转钥匙到OFF再转到销售位置。
     *
     * <P>2003-08-05 (Ver 4.3)
     *    1. 增加是否显示Button panel property (property "CreateButtonPanel").
     *    2. 增加是否显示mouse cursor property (property "ShowMouseCursor").
     *    3. 发布公共事业费代收功能。
     *    4. 手工主档下传若没有下传任何文件，则不需要重新启动。
     *    5. 解决shift帐的price open金额不对的问题。
     *    6. 按[0..9]* -> EnterButton 亦可输入商品。要配合更换statechart3.conf。
     *    7. 增加DiscountRateIDButton, DiscountRateButton.
     *
     * <P>2003-07-28 (Ver 4.2)
     *    1. 增加盘点录入功能。
     *
     * <P>2003-07-25 (Ver 4.1)
     *    1. 修改通报信息显示的线程。
     *    2. Z/Shift上传所填的会计日期，不要取当前的后台会计日期，而是以Z/Shift的结束时间和会计换日
     *       时间来决定。PostProcesser在收到Z之后，必须找到现金日报的那一个日期的数据，并且将该日的
     *       所有POS上传Z重新累加到现金日报资料。(注意：cake.calendar中必须要设置会计换日时间！）
     *    3. 接并口钱箱(CashDrawerPartner)若CheckDrawerClose设置成yes，不会造成死机。
     *
     * <P>2003-07-08 (Ver 4.0)
     *    1. New UI
     *    2. 所有config file目录全部取自property的“configDir”（若不存在则default为当前目录）。
     *
     * <P>2003-07-07 (Ver 3.10)
     *    修改磅秤商品金额数量不对的问题。
     *
     * <P>2003-06-25 (Ver 3.09)
     *    1. 修改问题：修改指定更正的lineitem的税额字段可能为正值的问题。
     *    2. 修改SC的pricechghead的限量促销商品的实际销售量字段的缺省值设置为1。
     *
     * <P>2003-06-23 (Ver 3.08)
     *    1. 修改若画面上看不到任何商品（例如可能被更正掉），仍允许输入会员编号。
     *    2. 增加限量促销功能。
     *
     * <P>2003-06-20 (Ver 3.07)
     *    修改灿坤发票打印格式。
     *
     * <P>2003-06-06 (Ver3.06)
     *    修改南昌好佳好发票打印格式。
     *
     * <P>2003-05-29 (Ver3.05)
     *    1. 增加会员查询功能。
     *    2. 解决同一时间上传日结报表会导致后台现金日报表不正确的问题。
     *    3. 解决未signon之前应不可以重复选择日结的问题。
     */
    public static String getVersion() {
        return versionID;
    }

    public static String getVersionID() {
        return versionID;
    }

    public static String getVersionDate() {
        return versionDate;
    }
}
