package hyi.cream.gwt.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.groovydac.Param;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;
import hyi.cream.dac.PLU;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.gwt.client.ICreamGWTService;
import hyi.cream.gwt.client.device.*;
import hyi.spos.KeylockConst;
import hyi.spos.POSKeyboard;
import hyi.spos.ScannerConst;
import hyi.spos.LineDisplay;
import hyi.spos.services.ChinaTrustCAT;

import java.util.Random;
import java.util.HashMap;
import java.io.*;
import java.net.Inet4Address;
import java.net.UnknownHostException;

/**
 * Cream GWT Service implementations. Running at server side.
 *
 * @author Bruce You
 */
public class CreamGWTService extends RemoteServiceServlet implements ICreamGWTService {

    private boolean testMode = false;

    public String getMessage(String msg) {
        return "Client said: \"" + msg + "\"<br>Server answered: \"Hi!\"";
    }

    public void pressPOSButton(int keyCode) {
        if (testMode)
            return;

        try {
            POSKeyboard kb = POSPeripheralHome3.getInstance().getPOSKeyboard();
            kb.setPOSKeyData(keyCode);
            kb.fireEvent();
        } catch (Exception ne) {
            CreamToolkit.logMessage(ne);
        }
    }

    public POSKeyboardAndKeylockData getPOSKeyboardAndKeylockData() {
        if (testMode)
            return getTestPOSKeyboardAndKeylockData();

        try {
            POSKeyboardData posKeyboardData =
                POSPeripheralHome3.getInstance().getPOSKeyboard().getPOSKeyboardData();
            KeylockData keylockData =
                POSPeripheralHome3.getInstance().getKeylock().getKeylockData();
            return new POSKeyboardAndKeylockData(posKeyboardData, keylockData);
        } catch (Exception ne) {
            CreamToolkit.logMessage(ne);
            return null;
        }
    }

    private POSKeyboardAndKeylockData getTestPOSKeyboardAndKeylockData() {
        POSKeyboardData kbd = new POSKeyboardData();
        kbd.setLogicalName("TestPOSKeyboard");

        POSButtonData btn = new POSButtonData();
        btn.setRow(0);
        btn.setCol(0);
        btn.setKeyCode(54);
        btn.setLabel("西瓜");
        kbd.getPosButtons().add(btn);

        btn = new POSButtonData();
        btn.setRow(1);
        btn.setCol(0);
        btn.setKeyCode(55);
        btn.setLabel("鳳梨");
        kbd.getPosButtons().add(btn);

        KeylockData kld = getTestKeylockData();

        return new POSKeyboardAndKeylockData(kbd, kld);
    }

    public KeylockData getKeylockData() {
        if (testMode)
            return getTestKeylockData();

        try {
            return POSPeripheralHome3.getInstance().getKeylock().getKeylockData();
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
            return null;
        }
    }

    private KeylockData getTestKeylockData() {
        KeylockData keylockData = new KeylockData();
        keylockData.setLogicalName("TestKeylock");

        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_LOCK);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_NORM);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR + 1);
        keylockData.getPositionCodes().add(KeylockConst.LOCK_KP_SUPR + 2);

        keylockData.getPositionNames().add("LOCK");
        keylockData.getPositionNames().add("NORM");
        keylockData.getPositionNames().add("SUPR");
        keylockData.getPositionNames().add("SUPR+1");
        keylockData.getPositionNames().add("SUPR+2");
        return keylockData;
    }

    public void turnKeylock(int position) {
        if (testMode)
            return;

        try {
            POSPeripheralHome3.getInstance().getKeylock().fireEvent(position);
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        }
    }

    public String getLineDisplayText() {
        if (testMode) {
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 20; i++)
                sb.append(random.nextInt(10));
            sb.append('\n');
            for (int i = 0; i < 20; i++)
                sb.append(random.nextInt(10));
            return sb.toString();
        }

        try {
            return POSPeripheralHome3.getInstance().getLineDisplay().getLineDisplayText();
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
            return "<LineDisplay is not found>";
        }
    }

    public String retrievePrintingLines() {
        if (testMode) {
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            int len = random.nextInt(20);
            for (int i = 0; i < len; i++)
                sb.append(random.nextInt(10));
            sb.append('\n');
            len = random.nextInt(20);
            for (int i = 0; i < len; i++)
                sb.append(random.nextInt(10));
            sb.append('\n');
            return sb.toString();
        }

        try {
            return POSPeripheralHome3.getInstance().getPOSPrinter().retrievePrintingLines();
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
            return "<POSPrinter is not found>";
        }
    }

    public void setPrinterAlwaysHealthy(boolean alwaysHealthy) {
        if (testMode)
            return;

        try {
            POSPeripheralHome3.getInstance().getPOSPrinter().setAlwaysHealthy(alwaysHealthy);
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        }
    }

    /**
     * Send scanner event to front-end POS.
     */
    public void scanBarcode(String barcode) {
        if (testMode)
            return;

        try {
            POSPeripheralHome3.getInstance().getScanner()
                .fireDataEvent(ScannerConst.SCAN_SDT_EAN13, barcode);
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        }
    }

    /**
     * Get candidate item with barcodePreifx.
     */
    public String[] getItemCandidate(String codePrefix) {
        if (testMode)
            return new String[] { codePrefix + "00001", codePrefix + "00002",
                codePrefix + "00003", codePrefix + "00004",
                codePrefix + "00005", codePrefix + "00006" };

        return PLU.querySuggestItem(codePrefix);
    }

    /**
     * Send MSR event to front-end POS.
     */
    public void swipeCreditCard(String cardNumber, String expirationDate) {
        if (testMode)
            return;

        try {
            POSPeripheralHome3.getInstance().getMSR().fireEvent(cardNumber, expirationDate);
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        }
    }

    /**
     * Execute a shell command.<p/>
     * Special build-in commands:<br/>
     * <li>REBOOT
     * <LI>HALT
     * <LI>AP_RESTART
     */
    public String executeCommand(String[] command) {
        try {
            if ("REBOOT".equals(command[0])) {
                CreamToolkit.reboot();
                return "";
            } else if ("HALT".equals(command[0])) {
                CreamToolkit.halt();
                return "";
            } else if ("AP_RESTART".equals(command[0])) {
                CreamToolkit.stopPos(0);
                return "";
            }

            String os = CreamToolkit.getOsName();
            if (os.equals("MacOS") || os.contains("Windows"))
                return "";
            
            StringBuilder result = new StringBuilder();
            for (String a : command)
                System.out.println(a);
            ProcessBuilder pb = new ProcessBuilder(command);
            pb.redirectErrorStream(true);
            Process process = pb.start();
            BufferedReader r = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF-8"));
            String line;
            while ((line = r.readLine()) != null) {
                result.append(line);
            }
            r.close();
            process.waitFor();
            System.out.println("result> " + result);
            return result.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    /**
     * Query database and output in HTML format.
     */
    public String queryDatabase(String sqlStatement) {
        if (testMode)
            return "Query Result";

        return getSQLResult(sqlStatement, "-H"); // -H: HTML table output mode
    }

    /**
     * Query a single value by a SQL command.
     */
    public String querySingleValue(String sqlStatement) {
        String result = getSQLResult(sqlStatement, "-t").trim();
        return result.replaceAll("\n", "");
    }

    private String getSQLResult(String sqlStatement, String formatOption) {
        try {
            StringBuilder result = new StringBuilder();
            Process process = startPSQLProcess(sqlStatement, formatOption);
            BufferedReader r = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF-8"));
            String line;
            while ((line = r.readLine()) != null) {
                result.append(line);
            }
            r.close();
            process.waitFor();
            return result.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
        /* For Windows --
        try {
            StringBuilder result = new StringBuilder();
            Process process = startPSQLProcess(sqlStatement, formatOption);
            BufferedReader r = new BufferedReader(new InputStreamReader(process.getInputStream(), "GBK"));
            String line;
            while ((line = r.readLine()) != null) {
                result.append(line);
            }
            r.close();
            process.waitFor();
            return result.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
        */
    }

    private Process startPSQLProcess(String sqlStatement, String formatOption) throws IOException {
        //System.out.println("Encoding=" + Converters.getDefaultEncodingName());
        //System.out.println("SQL> " + sqlStatement);
        ProcessBuilder pb = new ProcessBuilder("psql", CreamToolkit.getDatabaseName(), formatOption,
                "-h" + CreamToolkit.getDatabaseHost(), "-Upostgres", "-c" + sqlStatement);

//        ProcessBuilder pb = new ProcessBuilder("c:\\Program Files\\PostgreSQL\\8.3\\bin\\psql.exe", formatOption,
//            "-h" + CreamToolkit.getDatabaseHost(), "-Upostgres", "-cset client_encoding to 'gbk';" + sqlStatement,
//            CreamToolkit.getDatabaseName());
        pb.redirectErrorStream(true);
        Process process = pb.start();
        return process;
        /* Windows
        ProcessBuilder pb = new ProcessBuilder("c:\\Program Files\\PostgreSQL\\8.3\\bin\\psql.exe", formatOption,
            "-h" + CreamToolkit.getDatabaseHost(), "-Upostgres", "-cset client_encoding to 'gbk';" + sqlStatement,
            CreamToolkit.getDatabaseName());
        pb.redirectErrorStream(true);
        Process process = pb.start();
        return process;
        */
    }

    /**
     * Query last transaction.
     *
     * @return [0] is a HTML table for tranhead, [1] is for trandetail, [2] is transactionNumber
     */
    public String[] queryLastTransaction() {
        if (testMode)
            return new String[] {"tranhead result", "trandetail result", "123"};

        String[] result = new String[] {
            getSQLResult(
            "SELECT tmtranseq AS 交易序號, dealtype1||'-'||dealtype2||'-'||dealtype3 AS 交易種別," +
            "  sysdate AS 交易時間, invnohead||invno AS 發票號碼, invcnt AS 發票張數, eodcnt AS \"Z帳序號\", signonid AS \"SHIFT帳序號\"," +
            "  voidseq AS 作廢序號, detailcnt AS 明細筆數," +
            "  case payno1 IS NOT NULL" +
            "    when true then (SELECT payname FROM payment WHERE payid=tranhead.payno1)||payamt1" +
            "    else NULL" +
            "  end AS 支付1," +
            "  case payno2 IS NOT NULL" +
            "    when true then (SELECT payname FROM payment WHERE payid=tranhead.payno2)||payamt2" +
            "    else NULL" +
            "  end AS 支付2," +
            "  case payno3 IS NOT NULL" +
            "    when true then (SELECT payname FROM payment WHERE payid=tranhead.payno3)||payamt3" +
            "    else NULL" +
            "  end AS 支付3," +
            "  case payno4 IS NOT NULL" +
            "    when true then (SELECT payname FROM payment WHERE payid=tranhead.payno4)||payamt4" +
            "    else NULL" +
            "  end AS 支付4," +
            "  changeamt AS 找零金額, overamt AS 溢收金額," +
            "  crdtype AS 信用卡類型, crdno AS 信用卡編號, crdend AS 信用卡截止日," +
            "  empno AS 員工編號, saleman AS 銷售員編號, memberid AS 會員編號," +
            "  grosalamt AS 銷售毛額, sipamt0+sipamt1+sipamt2+sipamt3+sipamt4 AS \"SI折扣%\"," +
            "  simamt0+simamt1+simamt2+simamt3+simamt4 AS \"SI折讓\"," +
            "  mnmamt0+mnmamt1+mnmamt2+mnmamt3+mnmamt4 AS 組合促銷折扣," +
            "  rcpgifamt0+rcpgifamt1+rcpgifamt2+rcpgifamt3+rcpgifamt4 AS 不計發票金額," +
            "  netsalamt AS 銷售淨額," +
            "  daishouamt AS 代售金額, daishouamt2 AS 代收金額, daifuamt AS 代付金額," +
            "  saleamt AS 銷售收入," +
            "  invamt AS 發票金額," +
            "  (SELECT taxnm FROM taxtype WHERE taxid='0')||'稅 '||taxamt0 AS 稅0," +
            "  (SELECT taxnm FROM taxtype WHERE taxid='1')||'稅 '||taxamt1 AS 稅1," +
            "  annotatedtype as 附加信息類型, annotatedid as 附加信息 " +
            "FROM tranhead " +
            "WHERE tmtranseq=(SELECT tmtranseq FROM tranhead ORDER BY sysdate desc,tmtranseq desc LIMIT 1)",

            "-xtH"  // -x: turn on expanded table output
                    // -t: print rows only
                    // -H: HTML table output mode
            ),

            getSQLResult(
            "SELECT itemseq AS 單品順序, codetx AS 種類," +
            "  catno || '-' || midcatno || '-' || microcatno AS 大中小分類," +
            "  pluno AS 商品條碼, itemno AS 品號, unitprice AS 單價, qty AS 數量, amt AS 金額," +
            "  taxamt AS 稅額, aftdscamt AS 折扣後金額, origprice AS 原價," +
            "  disctype AS 折扣類型, discno AS 折扣編號 " +
            "FROM trandetail " +
            "WHERE tmtranseq=(SELECT tmtranseq FROM tranhead ORDER BY sysdate desc,tmtranseq desc LIMIT 1)",

            "-H" // -H: HTML table output mode
            ),

            getSQLResult("SELECT tmtranseq FROM tranhead ORDER BY sysdate desc LIMIT 1",
            "-At")  // -A: unaligned table output mode
                    // -t: print rows only
        };
        return result;
    }

    /**
     * Query transaction.
     *
     * @return [0] is a HTML table for tranhead, [1] is for trandetail
     */
    public String[] queryTransaction(int transactionNumber) {
        if (testMode)
            return new String[] {"tranhead result", "trandetail result"};

        return new String[] {
            getSQLResult(
            "SELECT tmtranseq AS 交易序號, dealtype1||'-'||dealtype2||'-'||dealtype3 AS 交易種別," +
            "  sysdate AS 交易時間, invnohead||invno AS 發票號碼, invcnt AS 發票張數, eodcnt AS \"Z帳序號\", signonid AS \"SHIFT帳序號\"," +
            "  voidseq AS 作廢序號, detailcnt AS 明細筆數," +
            "  case payno1 IS NOT NULL" +
            "    when true then (SELECT payname FROM payment WHERE payid=tranhead.payno1)||payamt1" +
            "    else NULL" +
            "  end AS 支付1," +
            "  case payno2 IS NOT NULL" +
            "    when true then (SELECT payname FROM payment WHERE payid=tranhead.payno2)||payamt2" +
            "    else NULL" +
            "  end AS 支付2," +
            "  case payno3 IS NOT NULL" +
            "    when true then (SELECT payname FROM payment WHERE payid=tranhead.payno3)||payamt3" +
            "    else NULL" +
            "  end AS 支付3," +
            "  case payno4 IS NOT NULL" +
            "    when true then (SELECT payname FROM payment WHERE payid=tranhead.payno4)||payamt4" +
            "    else NULL" +
            "  end AS 支付4," +
            "  changeamt AS 找零金額, overamt AS 溢收金額," +
            "  crdtype AS 信用卡類型, crdno AS 信用卡編號, crdend AS 信用卡截止日," +
            "  empno AS 員工編號, saleman AS 銷售員編號, memberid AS 會員編號," +
            "  grosalamt AS 銷售毛額, sipamt0+sipamt1+sipamt2+sipamt3+sipamt4 AS \"SI折扣%\"," +
            "  simamt0+simamt1+simamt2+simamt3+simamt4 AS \"SI折讓\"," +
            "  mnmamt0+mnmamt1+mnmamt2+mnmamt3+mnmamt4 AS 組合促銷折扣," +
            "  rcpgifamt0+rcpgifamt1+rcpgifamt2+rcpgifamt3+rcpgifamt4 AS 不計發票金額," +
            "  netsalamt AS 銷售淨額," +
            "  daishouamt AS 代售金額, daishouamt2 AS 代收金額, daifuamt AS 代付金額," +
            "  saleamt AS 銷售收入," +
            "  invamt AS 發票金額," +
            "  (SELECT taxnm FROM taxtype WHERE taxid='0')||'稅 '||taxamt0 AS 稅0," +
            "  (SELECT taxnm FROM taxtype WHERE taxid='1')||'稅 '||taxamt1 AS 稅1," +
            "  annotatedtype as 附加信息類型, annotatedid as 附加信息 " +
            "FROM tranhead " +
            "WHERE tmtranseq=" + transactionNumber,

            "-xtH"  // -x: turn on expanded table output
                    // -t: print rows only
                    // -H: HTML table output mode
            ),

            getSQLResult(
            "SELECT itemseq AS 單品順序, codetx AS 種類," +
            "  catno || '-' || midcatno || '-' || microcatno AS 大中小分類," +
            "  pluno AS 商品條碼, itemno AS 品號, unitprice AS 單價, qty AS 數量, amt AS 金額," +
            "  taxamt AS 稅額, aftdscamt AS 折扣後金額, origprice AS 原價," +
            "  disctype AS 折扣類型, discno AS 折扣編號 " +
            "FROM trandetail " +
            "WHERE tmtranseq=" + transactionNumber,

            "-H" // -H: HTML table output mode
            )
        };
    }

    /**
     * Check to see if it's waiting for CAT response.
     */
    public Boolean waitForCATResponse() {
        try {
            POSPeripheralHome3 p = POSPeripheralHome3.getInstance();
            if (p.existsChinaTrustCAT()) {
                ChinaTrustCAT cat = (ChinaTrustCAT)p.getCAT();
                return cat.waitForCATResponse;
            }
        } catch (Exception e) {
        }
        return false;
    }

    /**
     * Get CAT request.
     */
    public String getCATRequest() {
        try {
            POSPeripheralHome3 p = POSPeripheralHome3.getInstance();
            if (p.existsChinaTrustCAT()) {
                ChinaTrustCAT cat = (ChinaTrustCAT)p.getCAT();
                return cat.requestPacket;
            }
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * Send CAT response.
     */
    public void sendCATResponse(String response) {
        try {
            POSPeripheralHome3 p = POSPeripheralHome3.getInstance();
            if (p.existsChinaTrustCAT()) {
                ChinaTrustCAT cat = (ChinaTrustCAT)p.getCAT();
                synchronized (cat) {
                    cat.responsePacket = response;
                    cat.waitForCATResponse = false;
                    cat.notifyAll();
                }
            }
        } catch (Exception e) {
        }
    }

    public String[] queryLastZReport() {
        if (testMode)
            return new String[] {"z result", "z result", "123"};

        String[] result = new String[] {
            getSQLResult(
            "SELECT posno AS \"POS機號\", eodcnt AS \"Z帳序號\",\n" +
            "  case accdate\n" +
            "    when '1970-01-01' then '尚未日結'\n" +
            "    when '1971-01-01' then '已日結'\n" +
            "    else accdate||'會計日'\n" +
            "  end AS 日結狀態,\n" +
            "  zbegdttm AS 起始時間,\n" +
            "  case zenddttm\n" +
            "    when TIMESTAMP '1970-01-01 00:00:00' then '尚未日結'\n" +
            "    else ''||zenddttm\n" +
            "  end AS 日結時間,\n" +
            "  sgnonseq AS 起始序號, sgnoffseq AS 結束序號,\n" +
            "  sgnoninv AS 起始發票號, sgnoffinv AS 結束發票號,\n" +
            "  case tcpflg\n" +
            "    when '0' then '未上傳'\n" +
            "    when '1' then '已上傳'\n" +
            "    when '2' then '上傳失敗'\n" +
            "    else '未知狀態'\n" +
            "  end AS 上傳狀態,\n" +
            "  cashier AS 收銀員,\n" +
            "  trancnt AS 交易筆數, custcnt AS 來客數,\n" +
            "  salamttax0 AS \"毛額(稅0)\",salamttax1 AS \"毛額(稅1)\",salamttax2 AS \"毛額(稅2)\",\n" +
            "  salamttax3 AS \"毛額(稅3)\",salamttax4 AS \"毛額(稅4)\",\n" +
            "  salamttax0+salamttax1+salamttax2+salamttax3+salamttax4 AS \"+總銷售毛額\",\n" +
            "  sipamt0+sipamt1+sipamt2+sipamt3+sipamt4 AS \"-SI百分比折扣\",\n" +
            "  simamt0+simamt1+simamt2+simamt3+simamt4 AS \"-SI金額折扣\",\n" +
            "  mnmamt0+mnmamt1+mnmamt2+mnmamt3+mnmamt4 AS \"-組合促銷折扣\",\n" +
            "  rcpgifamt0+rcpgifamt1+rcpgifamt2+rcpgifamt3+rcpgifamt4 AS 不計發票金額,\n" +
            "  netsalamt0 AS \"淨額(稅0)\", netsalamt1 AS \"淨額(稅1)\", netsalamt2 AS \"淨額(稅2)\",\n" +
            "  netsalamt3 AS \"淨額(稅3)\", netsalamt4 AS \"淨額(稅4)\",\n" +
            "  netsalamt0+netsalamt1+netsalamt2+netsalamt3+netsalamt4 AS \"=總銷售淨額\",\n" +
            "  daishouamt||'('||daishoucnt||')' AS \"+代售金額(次數)\",\n" +
            "  daishouamt2||'('||daishoucnt2||')' AS \"+代收金額(次數)\",\n" +
            "  daifuamt||'('||daifucnt||')' AS \"-代付金額(次數)\",\n" +
            "  overamt||'('||overcnt||')' AS \"+溢收金額(次數)\",\n" +
            "  pinamt||'('||pincnt||')' AS \"+PaidIn金額(次數)\",\n" +
            "  poutamt||'('||poutcnt||')' AS \"+PaidOut金額(次數)\",\n" +
            "  netsalamt0+netsalamt1+netsalamt2+netsalamt3+netsalamt4+daishouamt+\n" +
            "  daishouamt2-daifuamt+overamt+pinamt-poutamt AS \"=合計金額\",\n" +
            "  (SELECT payname FROM payment WHERE payid='00')||pay00amt AS 支付00,\n" +
            "  (SELECT payname FROM payment WHERE payid='01')||pay01amt AS \"+支付01\",\n" +
            "  (SELECT payname FROM payment WHERE payid='02')||pay02amt AS \"+支付02\",\n" +
            "  (SELECT payname FROM payment WHERE payid='03')||pay03amt AS \"+支付03\",\n" +
            "  (SELECT payname FROM payment WHERE payid='04')||pay04amt AS \"+支付04\",\n" +
            "  (SELECT payname FROM payment WHERE payid='05')||pay05amt AS \"+支付05\",\n" +
            "  (SELECT payname FROM payment WHERE payid='06')||pay06amt AS \"+支付06\",\n" +
            "  (SELECT payname FROM payment WHERE payid='07')||pay07amt AS \"+支付07\",\n" +
            "  (SELECT payname FROM payment WHERE payid='08')||pay08amt AS \"+支付08\",\n" +
            "  (SELECT payname FROM payment WHERE payid='09')||pay09amt AS \"+支付09\",\n" +
            "  (SELECT payname FROM payment WHERE payid='10')||pay10amt AS \"+支付10\",\n" +
            "  pay00amt+pay01amt+pay02amt+pay03amt+pay04amt+pay05amt+pay06amt+pay07amt+pay08amt+\n" +
            "  pay09amt+pay10amt+pay11amt+pay12amt+pay13amt+pay14amt+pay15amt+pay16amt+pay17amt+\n" +
            "  pay18amt+pay19amt+pay20amt+pay21amt+pay22amt+pay23amt+pay24amt+pay25amt+pay26amt+\n" +
            "  pay27amt+pay28amt+pay29amt AS \"=支付合計\",\n" +
            "  rtnamt||'('||rtncnt||')' AS \"退貨金額(次數)\",\n" +
            "  voidinvamt||'('||voidinvcnt||')' AS \"前筆誤打金額(次數)\",\n" +
            "  cancelamt||'('||cancelcnt||')' AS \"交易取消金額(次數)\",\n" +
            "  UPDAMT||'('||UPDcnt||')' AS \"指定更正金額(次數)\",\n" +
            "  NOWUPDAMT||'('||NOWUPDcnt||')' AS \"立即更正金額(次數)\",\n" +
            "  DROPENCNT AS \"開錢櫃次數\",\n" +
            "  REPRTAMT||'('||REPRTcnt||')' AS \"重印金額(次數)\",\n" +
            "  ENTRYAMT||'('||ENTRYcnt||')' AS \"手輸應免稅金額(次數)\",\n" +
            "  POENTRYAMT||'('||POENTRYcnt||')' AS \"PRICE OPEN金額(次數)\",\n" +
            "  CASHINAMT||'('||CASHINcnt||')' AS \"借零金額(次數)\",\n" +
            "  CASHOUTAMT||'('||CASHOUTcnt||')' AS \"投庫金額(次數)\"\n" +
            " FROM z\n" +
            " ORDER BY eodcnt DESC LIMIT 1",

            "-xtH"  // -x: turn on expanded table output
                    // -t: print rows only
                    // -H: HTML table output mode
            )
        };
        return result;
    }

    public String getLineDisplayWelcomeMessage() {
        return Param.getInstance().getWelcomeMessage();
    }

    public void setLineDisplayWelcomeMessage(String welcomeMessage) {
        Param.getInstance().updateWelcomeMessage(welcomeMessage);
    }

    public String getLocalIPAddress() {
        try {
            return Inet4Address.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            return "?";
        }
    }

    public void setEpsonPrinterCommandSet(String commandSet) {
        executeCommand(new String[] {
            "/bin/sed", "-i.old0",
            "s/commandSet\" type=\"String\" value=\".*\"/commandSet\" type=\"String\" value=\"" + commandSet + "\"/g",
            CreamToolkit.getConfigDir() + "jpos.xml"});
    }

    public void setLineDisplayType(String type) {
        executeCommand(new String[] {
            "/bin/sed", "-i.old1",
            "s/name=\"Type\" type=\"String\" value=\".*\"/name=\"Type\" type=\"String\" value=\"" + type + "\"/g",
            CreamToolkit.getConfigDir() + "jpos.xml"});
    }

    public ParamData getParamData() {
        return Param.getInstance().retrieveAll();
    }

    public void saveModifiedProperties(HashMap<String, Object> modifiedProperties) {
        Param.getInstance().updateParams(modifiedProperties);
    }
}