package hyi.cream.gwt;

import hyi.cream.gwt.server.CreamGWTService;
import hyi.cream.util.CreamToolkit;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.handler.ResourceHandler;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;

/**
 * Start up the Jetty servlet engine for RPC services of our GWT web app.
 *
 * @author Bruce You
 */
public class JettyStarter
{
    private static final String WEB_BASE = "./www/hyi.cream.gwt.CreamGWT";
    //private static final String CREAMGWT2_BASE = "./www/hyi.cream.gwt.CreamGWT2";
    private static final String GWTEXT_WEB_BASE = "./www/com.gwtext.sample.showcase2.Showcase2";
    //private static final String SMARTGWT_WEB_BASE = "./www/com.smartgwt.sample.showcase.Showcase";

    private static Server server;

    public static void main(String[] args) {
        start();
    }

    public static void start() {
        server = new Server(8888);

        // CreamGWT -> Root
        ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setResourceBase(WEB_BASE);
        Context rootContext = new Context();
        rootContext.setContextPath("/");
        rootContext.setHandler(resourceHandler);
        server.setHandler(rootContext);

        //// CreamGWT2 -> /creamgwt2
        //mountPath(server, "/creamgwt2", CREAMGWT2_BASE);

        // GWTExt -> /gwtext
        mountPath(server, "/gwtext", GWTEXT_WEB_BASE);
        
        //// SmartGWT -> smartgwt
        //mountPath(server, "/smartgwt", SMARTGWT_WEB_BASE);

        // CreamGWT service
        Context gwtServletContext = new Context(server, "/CreamGWT", Context.SESSIONS);
        gwtServletContext.addServlet(new ServletHolder(new CreamGWTService()), "/*");
        server.addHandler(gwtServletContext);

//////////////////////////////////////////////////////
//        Context context2 = new Context(server, "/creamgwt.CreamGWT", Context.SESSIONS);
//        context2.addServlet(new ServletHolder(new CreamGWTService()), "/creamgwt.CreamGWT/*");
//
//        HandlerList handlers = new HandlerList();
//        handlers.setHandlers(new Handler[] {rh, new DefaultHandler()});
//        server.setHandler(handlers);
//
//        ContextHandler context = new ContextHandler();
//        context.setContextPath("/");
//        context.setResourceBase("/Users/youbruce/IntelliJIDEA_Projects/CreamPOS_trunk/gwt/www");
//        context.setClassLoader(Thread.currentThread().getContextClassLoader());
//        server.setHandler(context);
//
//        ServletHandler handler = new ServletHandler();
//        handler.addServletWithMapping("creamgwt.server.CreamGWTService", "/creamgwt.CreamGWT/CreamGWTService");
//        server.addHandler(handler);
//////////////////////////////////////////////////////

        try {
            server.start();
            System.out.println("Jetty startups.");
            //server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void stop() {
        try {
            server.stop();
            server = null;
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        }
    }

    private static void mountPath(Server server, String urlPath, String physicalPath) {
        ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setResourceBase(physicalPath);
        Context context = new Context();
        context.setContextPath(urlPath);
        context.setHandler(resourceHandler);
        server.addHandler(context);
    }
}
