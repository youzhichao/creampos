package hyi.cream.gwt.client;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.HTML;

/**
 * LineDisplay window.
 *
 * @author Bruce You
 */
public class LineDisplayWindow extends DialogBox {

    private Label line1;
    private Label line2;

    public LineDisplayWindow() {
        super(true, false);

        setText("Line Display");

        VerticalPanel mainPanel = new VerticalPanel();
        line1 = new Label("[---LINE DISPLAY---]");
        line2 = new Label("[  BY: BRUCE @ HYI ]");
        line1.addStyleName("LineDisplayText");
        line2.addStyleName("LineDisplayText");
        mainPanel.add(line1);
        mainPanel.add(line2);
        setWidget(mainPanel);

        // setup timer to refresh list automatically
        Timer refreshTimer = new Timer() {
            public void run() {
                refresh();
            }
        };
        refreshTimer.scheduleRepeating(1500);
    }

    private void refresh() {
        ICreamGWTService.App.getInstance().getLineDisplayText(new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {
                line1.setText("Failed to get text. ");
                line2.setText("                    ");
            }

            public void onSuccess(String text) {
                String[] lines = text.split("\\n");
                line1.setText(lines[0]);
                if (lines.length > 1)
                    line2.setText(lines[1]);
            }
        });
    }
}