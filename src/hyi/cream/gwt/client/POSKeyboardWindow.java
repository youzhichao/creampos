package hyi.cream.gwt.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import hyi.cream.gwt.client.device.POSButtonData;
import hyi.cream.gwt.client.ICreamGWTService;
import hyi.cream.gwt.client.device.POSKeyboardData;
import hyi.cream.gwt.client.device.KeylockData;
import hyi.cream.gwt.client.device.POSKeyboardAndKeylockData;

/**
 * POS keyboard and keylock window. Upper panel is the keylock, lower panel is the POS
 * keyboard.
 *
 * @author Bruce You
 */
public class POSKeyboardWindow extends DialogBox {

    public POSKeyboardWindow(POSKeyboardAndKeylockData posKeyboardAndKeylockData) {
        super(true, false);

        KeylockData keylockData = posKeyboardAndKeylockData.getKeylockData();
        POSKeyboardData posKeyboardData = posKeyboardAndKeylockData.getPosKeyboardData();

        setText(posKeyboardAndKeylockData.getKeylockData().getLogicalName()
            + " and " + posKeyboardAndKeylockData.getPosKeyboardData().getLogicalName());

        VerticalPanel mainPanel = new VerticalPanel();

        FlowPanel keylockPanel = new FlowPanel();
        for (int i = 0; i < keylockData.getPositionCodes().size(); i++) {
            final int keyPosition = keylockData.getPositionCodes().get(i);
            Button keylockButton = new Button();
            keylockButton.setText(keylockData.getPositionNames().get(i));
            keylockButton.addClickListener(new ClickListener() {
                public void onClick(Widget sender) {
                    ICreamGWTService.App.getInstance().turnKeylock(keyPosition,
                        new AsyncCallback() { // don't have to do anything when callbacks
                            public void onFailure(Throwable caught) {
                            }

                            public void onSuccess(Object result) {
                            }
                        });
                }
            });
            keylockPanel.add(keylockButton);
        }
        mainPanel.add(keylockPanel);

        FlexTable keyTable = new FlexTable();
        //keyTable.setBorderWidth(1);
        //keyTable.setCellPadding(3);
        for (POSButtonData posButton : posKeyboardData.getPosButtons()) {
            final int keyCode = posButton.getKeyCode();
            //Button htmlButton = new Button();
            Label htmlButton = new Label();
            htmlButton.addStyleName("gwt-Button");
            htmlButton.addStyleName("BlackText");
            htmlButton.setSize("2em", "2em");
            keyTable.setWidget(posButton.getRow(), posButton.getCol(), htmlButton);
            htmlButton.setText(posButton.getLabel());
            htmlButton.addClickListener(new ClickListener() {
                public void onClick(Widget sender) {
                    ICreamGWTService.App.getInstance().pressPOSButton(keyCode,
                        new AsyncCallback() { // don't have to do anything when callbacks
                            public void onFailure(Throwable caught) {
                            }

                            public void onSuccess(Object result) {
                            }
                        });
                }
            });
        }
        mainPanel.add(keyTable);

        setWidget(mainPanel);
    }
}
