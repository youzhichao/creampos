package hyi.cream.gwt.client;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.Timer;

import java.util.Date;

/**
 * Simulator for China Trust CAT.
 *
 * @author Bruce You
 * @since 2008/9/14 上午 11:19:06
 */
public class ChinaTrustCATWindow extends DialogBox {

    Timer timer;
    Button sendButton;

    public ChinaTrustCATWindow() {
        super(true, false);

        setText("China Trust CAT");
        VerticalPanel mainPanel = new VerticalPanel();

        FlexTable input = new FlexTable();

        // First column --

        int row = 0;

        final Label transType = new Label();
        input.setWidget(row, 0, new Label("交易類別"));
        input.setWidget(row, 1, transType);
        row++;

        final Label transAmt = new Label();
        input.setWidget(row, 0, new Label("交易金額"));
        input.setWidget(row, 1, transAmt);
        row++;

        input.setWidget(row, 0, new Label("授權結果"));
        final ListBox respCode2 = new ListBox();
        respCode2.setVisibleItemCount(1);
        input.setWidget(row, 1, respCode2);
        respCode2.addItem("00:成功授權");
        respCode2.addItem("01:無法授權");
        respCode2.addItem("02:請聯繫銀行");
        respCode2.addItem("03:通訊超時");
        row++;

        input.setWidget(row, 0, new Label("主機回應"));
        final ListBox respCode1 = new ListBox();
        respCode1.setVisibleItemCount(1);
        input.setWidget(row, 1, respCode1);
        respCode1.addItem("00:OK");
        respCode1.addItem("01:請查詢銀行");
        respCode1.addItem("02:請查詢銀行");
        respCode1.addItem("25:請查詢銀行");
        respCode1.addItem("30:請查詢銀行");
        respCode1.addItem("03:未核准之特約店");
        respCode1.addItem("04:非正常卡");
        respCode1.addItem("41:非正常卡");
        respCode1.addItem("43:非正常卡");
        respCode1.addItem("62:非正常卡");
        respCode1.addItem("05:拒絕交易");
        respCode1.addItem("12:本交易不接受");
        respCode1.addItem("31:本交易不接受");
        respCode1.addItem("57:本交易不接受");
        respCode1.addItem("58:本交易不接受");
        respCode1.addItem("13:金額不符");
        respCode1.addItem("14:帳號不符");
        respCode1.addItem("15:發卡行不符");
        respCode1.addItem("19:請重試交易");
        respCode1.addItem("21:無此交易");
        respCode1.addItem("51:餘額不足");
        respCode1.addItem("55:密碼錯期");
        respCode1.addItem("54:卡片過期");
        respCode1.addItem("59:無此帳號");
        respCode1.addItem("61:提款超過限額");
        respCode1.addItem("63:主機安全碼有誤");
        respCode1.addItem("64:TKF不存在 64");
        respCode1.addItem("65:PTD不存在 65");
        respCode1.addItem("66:KEY不存在 66");
        respCode1.addItem("75:密碼超過次數");
        respCode1.addItem("77:總額不符");
        respCode1.addItem("79:批號已開啟");
        respCode1.addItem("80:批號錯誤");
        respCode1.addItem("85:無此批號");
        respCode1.addItem("88:特約店帳號不符");
        respCode1.addItem("89:終端機號錯誤");
        respCode1.addItem("91:請查詢發卡銀行");
        respCode1.addItem("94:傳輸重覆");
        respCode1.addItem("95:檔案傳輸中");
        respCode1.addItem("96:主機系統故障");
        respCode1.addItem("TO:EDC等待主機超時");
        respCode1.addItem("NC:線路不良");
        respCode1.addItem("RV:線路不良");
        row++;

        input.setWidget(row, 0, new Label("授權銀行編碼"));
        final ListBox hostId = new ListBox();
        hostId.setVisibleItemCount(1);
        input.setWidget(row, 1, hostId);
        hostId.addItem("01:信用卡");
        hostId.addItem("02:紅利抵用");
        hostId.addItem("03:分期付款");
        hostId.addItem("11:AMEX");
        hostId.addItem("12:DINERS");
        row++;

        input.setWidget(row, 0, new Label("調閱編號(6碼)"));
        final TextBox invoiceNo = new TextBox();
        invoiceNo.setWidth("6em");
        invoiceNo.setMaxLength(6);
        input.setWidget(row, 1, invoiceNo);
        row++;

        input.setWidget(row, 0, new Label("卡號"));
        final TextBox cardNo = new TextBox();
        cardNo.setWidth("12em");
        cardNo.setMaxLength(19);
        input.setWidget(row, 1, cardNo);
        row++;

        input.setWidget(row, 0, new Label("有效期(MMYY)"));
        final TextBox cardExpDate = new TextBox();
        cardExpDate.setWidth("3em");
        cardExpDate.setMaxLength(4);
        input.setWidget(row, 1, cardExpDate);

        // Second column --

        row = 0;

        input.setWidget(row, 2, new Label("交易日期(YYMMDD)"));
        final TextBox transDate = new TextBox();
        transDate.setWidth("5em");
        transDate.setMaxLength(6);
        input.setWidget(row, 3, transDate);
        row++;

        input.setWidget(row, 2, new Label("交易時間(HHMMSS)"));
        final TextBox transTime = new TextBox();
        transTime.setWidth("5em");
        transTime.setMaxLength(6);
        input.setWidget(row, 3, transTime);
        row++;

        input.setWidget(row, 2, new Label("授權碼(9碼)"));
        final TextBox approveCode = new TextBox();
        approveCode.setWidth("8em");
        approveCode.setMaxLength(9);
        input.setWidget(row, 3, approveCode);
        row++;

        input.setWidget(row, 2, new Label("端末機編號(8碼)"));
        final TextBox terminalId = new TextBox();
        terminalId.setWidth("7em");
        terminalId.setMaxLength(8);
        input.setWidget(row, 3, terminalId);
        row++;

        input.setWidget(row, 2, new Label("序號(Ref No:12碼)"));
        final TextBox refNo = new TextBox();
        refNo.setWidth("8em");
        refNo.setMaxLength(12);
        input.setWidget(row, 3, refNo);
        row++;

        input.setWidget(row, 2, new Label("紅利折抵金額"));
        final TextBox bonusDiscount = new TextBox();
        bonusDiscount.setWidth("6em");
        bonusDiscount.setMaxLength(13);
        input.setWidget(row, 3, bonusDiscount);
        row++;

        input.setWidget(row, 2, new Label("紅利後實付金額"));
        final TextBox bonusPaid = new TextBox();
        bonusPaid.setWidth("6em");
        bonusPaid.setMaxLength(13);
        input.setWidget(row, 3, bonusPaid);
        row++;

        sendButton = new Button("Send", new ClickListener() {
            public void onClick(Widget sender) {
                // preparing responsePacket
                //"""${getTransactionTypeChars()}${cardCompanyID}${transactionNumber}${accountNumber}${expireDate}\
                //${transAmt}${transDate}${transTime}${approvalCode}${amount1}${respCode}${terminalId}${refNo}\
                //${amount2}${storeId}${amount3}${reserved}"""
                String responsePacket = transType.getText().substring(0, 2);
                responsePacket += hostId.getItemText(hostId.getSelectedIndex()).substring(0, 2);
                responsePacket += padRight(invoiceNo.getText(), 6); 
                responsePacket += padRight(cardNo.getText(), 19);
                responsePacket += padRight(cardExpDate.getText(), 4);
                responsePacket += toCATAmountString(transAmt.getText());
                responsePacket += padRight(transDate.getText(), 6);
                responsePacket += padRight(transTime.getText(), 6);
                responsePacket += padRight(approveCode.getText(), 9);
                responsePacket += toCATAmountString(bonusPaid.getText());
                responsePacket += respCode1.getItemText(respCode1.getSelectedIndex()).substring(0, 2);
                responsePacket += respCode2.getItemText(respCode2.getSelectedIndex()).substring(0, 2);
                responsePacket += padRight(terminalId.getText(), 8);
                responsePacket += padRight(refNo.getText(), 12);
                responsePacket += toCATAmountString(bonusDiscount.getText());
                responsePacket += "                "; // store id
                responsePacket += "              ";   // amount3 + reserved

                getService().sendCATResponse(responsePacket, new AsyncCallback() {
                    public void onFailure(Throwable throwable) { }
                    public void onSuccess(Object o) { }
                });

                timer.scheduleRepeating(2000);
                //sendButton.setEnabled(false);
            }
        });
        //sendButton.setEnabled(false);
        mainPanel.add(input);
        mainPanel.add(sendButton);
        setWidget(mainPanel);

        timer = new Timer() { public void run() {
            getService().waitForCATResponse(new AsyncCallback<Boolean>() {
                public void onFailure(Throwable caught) {}
                public void onSuccess(Boolean readyForSwipingCard) {
                    if (readyForSwipingCard) {
                        timer.cancel(); // stop the timer when ready for swiping card, until data have sent out
                        getService().getCATRequest(new AsyncCallback<String>() {
                            public void onFailure(Throwable throwable) {}
                            public void onSuccess(String requestPacket) {
                                int i = 0;
                                String transTypeCode     = requestPacket.substring(i, i + 2); i += 2;
                                String cardCompanyID     = requestPacket.substring(i, i + 2); i += 2;
                                String transactionNumber = requestPacket.substring(i, i + 6); i += 6;
                                String accountNumber     = requestPacket.substring(i, i + 19); i += 19;
                                String expireDate        = requestPacket.substring(i, i + 4); i += 4;
                                String transAmtValue     = requestPacket.substring(i, i + 12); i += 12;
                                String transDateValue    = requestPacket.substring(i, i + 6); i += 6;
                                String transTimeValue    = requestPacket.substring(i, i + 6); i += 6;
                                String approvalCodeValue = requestPacket.substring(i, i + 9); i += 9;
                                String amount1           = requestPacket.substring(i, i + 12); i += 12;
                                String respCodeValue     = requestPacket.substring(i, i + 4); i += 4;
                                String terminalIdValue   = requestPacket.substring(i, i + 8); i += 8;
                                String refNoValue        = requestPacket.substring(i, i + 12); i += 12;
                                String amount2           = requestPacket.substring(i, i + 12);

                                if ("01".equals(transTypeCode))
                                    transType.setText("01:銷售SALE");
                                else if ("02".equals(transTypeCode))
                                    transType.setText("02:退貨REFUND");
                                else if ("30".equals(transTypeCode))
                                    transType.setText("30:取消REFUND");
                                else if ("41".equals(transTypeCode))
                                    transType.setText("41:調整ADJUST");

                                for (i = 0; i < hostId.getItemCount(); i++) {
                                    if (hostId.getItemText(i).startsWith(cardCompanyID))
                                        hostId.setSelectedIndex(i);
                                }

                                invoiceNo.setText(transactionNumber.trim());
                                cardNo.setText(accountNumber.trim());
                                cardExpDate.setText(expireDate.trim());
                                transAmt.setText(fromCATAmountString(transAmtValue).trim());
                                approveCode.setText(approvalCodeValue.trim());
                                bonusPaid.setText(fromCATAmountString(amount1).trim());
                                bonusDiscount.setText(fromCATAmountString(amount2).trim());
                                refNo.setText(refNoValue.trim());
                                terminalId.setText(terminalIdValue.trim());

                                Date now = new Date();
                                String year = "" + (now.getYear() + 1900) % 100;
                                if (year.length() == 1)
                                    year = "0" + year;
                                String month = "" + (now.getMonth() + 1);
                                if (month.length() == 1)
                                    month = "0" + month;
                                String day = "" + now.getDate();
                                if (day.length() == 1)
                                    day = "0" + day;
                                String hour = "" + now.getHours();
                                if (hour.length() == 1)
                                    hour = "0" + hour;
                                String minute = "" + now.getMinutes();
                                if (minute.length() == 1)
                                    minute = "0" + minute;
                                String second = "" + now.getSeconds();
                                if (second.length() == 1)
                                    second = "0" + second;

                                transDate.setText(year + month + day);
                                transTime.setText(hour + minute + second);

                                sendButton.setEnabled(true);
                            }
                        });
                    } else {
                        transType.setText("");
                        transAmt.setText("");
                        //sendButton.setEnabled(false);
                    }
                }
            });
        }};
        timer.scheduleRepeating(2000);
    }

    private ICreamGWTServiceAsync getService() {
        return ICreamGWTService.App.getInstance();
    }

    private static String padRight(String s, int len) {
        if (s.length() > len)
            return s.substring(0, len);
        while (s.length() < len)
            s += " ";
        return s;
    }

    private static String padLeft(String s, int len) {
        if (s.length() > len)
            return s.substring(0, len);
        while (s.length() < len)
            s = "0" + s;
        return s;
    }

    /**
     * "000000001234" -> "12.34"
     */
    private static String fromCATAmountString(String s) {
        try {
            return new Double(s.substring(0, 10) + "." + s.substring(10, 12)).toString();
        } catch (Exception e) {
            return s;
        }
    }

    /**
     * "12.34" -> "000000001234"
     */
    private static String toCATAmountString(String s) {
        try {
            String x = "" + new Double(Double.parseDouble(s) * 100D).intValue();
            return padLeft(x, 12);
        } catch (Exception e) {
            return "            ";
        }
    }
}
