package hyi.cream.gwt.client;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

/**
 * POS printer window.
 *
 * @author Bruce You
 * @since Aug 23, 2008 11:23:02 AM
 */
public class POSPrinterWindow extends DialogBox {

    ScrollPanel receiptPanel;
    private Label lines;

//Bruce> 不知道為什麼StringBuffer在Browser端有問題
//  private StringBuffer buffer = new StringBuffer("[---POS Printer----]\n[  BY: BRUCE @ HYI ]\n");
    private String buffer = "[---POS Printer----]\n[  BY: BRUCE @ HYI ]\n";

    public POSPrinterWindow() {
        super(true, false);

        setText("POS Printer");

        VerticalPanel mainPanel = new VerticalPanel();
        final CheckBox alwaysHealthy = new CheckBox("Always healthy", false);
        alwaysHealthy.addClickListener(new ClickListener() {
            public void onClick(Widget sender) {
                ICreamGWTService.App.getInstance().setPrinterAlwaysHealthy(alwaysHealthy.isChecked(),
                    new AsyncCallback<Void>() {
                        public void onFailure(Throwable caught) {
                        }
                        public void onSuccess(Void text) {
                        }
                    });
            }
        });
        mainPanel.add(alwaysHealthy);

        receiptPanel = new ScrollPanel();
        lines = new Label(buffer.toString());
        lines.setStyleName("POSPrinterText");
        lines.setSize("22em", "30em");
        receiptPanel.add(lines);
        mainPanel.add(receiptPanel);
        
        setWidget(mainPanel);

        // setup timer to refresh list automatically
        Timer refreshTimer = new Timer() {
            public void run() {
                refresh();
            }
        };
        refreshTimer.scheduleRepeating(1500);
    }

    private void refresh() {
        ICreamGWTService.App.getInstance().retrievePrintingLines(new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {
            }

            public void onSuccess(String text) {
                //buffer.append(text);
                buffer += text;
                lines.setText(buffer);
                receiptPanel.scrollToBottom();
            }
        });
    }
}
