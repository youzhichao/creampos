package hyi.cream.gwt.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import hyi.cream.gwt.client.device.POSKeyboardAndKeylockData;
import hyi.cream.gwt.client.device.ParamData;

import java.util.Map;
import java.util.HashMap;

/**
 * Create GWT service interface.
 *
 * @author Bruce You
 */
@RemoteServiceRelativePath("CreamGWT/CreamGWTService")
public interface ICreamGWTService extends RemoteService {
    
    /**
     * Utility/Convenience class.
     * Use ICreamGWTService.App.getInstance() to access static instance of CreamGWTServiceAsync.
     */
    public static class App {
        private static ICreamGWTServiceAsync app = null;

        public static synchronized ICreamGWTServiceAsync getInstance() {
            if (app == null) {
                app = (ICreamGWTServiceAsync)GWT.create(ICreamGWTService.class);
                //((ServiceDefTarget)app).setServiceEntryPoint(
                //        GWT.getModuleBaseURL() + "hyi.cream.gwt.CreamGWT/CreamGWTService");
            }
            return app;
        }
    }

    String getMessage(String msg);

    /**
     * Get POS keyboard and keylock detail information.
     */
    POSKeyboardAndKeylockData getPOSKeyboardAndKeylockData();

    /**
     * Send POS keyboard event to front-end POS.
     */
    void pressPOSButton(int keyCode);

    /**
     * Send keylock event to front-end POS.
     */
    void turnKeylock(int position);

    /**
     * Get current LineDisplay text, each line is seperated by \n.
     */
    String getLineDisplayText();

    /**
     * Get the latest printing lines to POS printer.
     */
    String retrievePrintingLines();

    /**
     * Set "alwaysHealthy" flag for POS Printer.
     */
    void setPrinterAlwaysHealthy(boolean alwaysHealthy);

    /**
     * Send scanner event to front-end POS.
     */
    void scanBarcode(String barcode);

    /**
     * Get candidate item with codePreifx.
     */
    String[] getItemCandidate(String codePrefix);

    /**
     * Send MSR event to front-end POS.
     */
    void swipeCreditCard(String cardNumber, String expirationDate);

    /**
     * Query database.
     */
    String queryDatabase(String sqlStatement);

    /**
     * Query last transaction.
     */
    String[] queryLastTransaction();

    /**
     * Query transaction.
     */
    String[] queryTransaction(int transactionNumber);

    /**
     * Query last z.
     */
    String[] queryLastZReport();

    /**
     * Check to see if it's waiting for CAT response.
     */
    Boolean waitForCATResponse();

    /**
     * Get CAT request.
     */
    String getCATRequest();

    /**
     * Send CAT response.
     */
    void sendCATResponse(String response);

    /**
     * Query a single value by a SQL command.
     */
    String querySingleValue(String sqlStatement);
    
    /**
     * Get POS IP address.
     */
    String getLocalIPAddress();

    /**
     * Get line display welcome message.
     */
    String getLineDisplayWelcomeMessage();

    /**
     * Set line display welcome message in application-level cache.
     */
    void setLineDisplayWelcomeMessage(String welcomeMessage);

    /**
     * Execute a shell command.
     */
    String executeCommand(String[] command);

    /**
     * Set Epson printer command set.
     */
    void setEpsonPrinterCommandSet(String commandSet);

    /**
     * Set Nitori IBM4840LineDisplay line display type.
     */
    void setLineDisplayType(String type);

    /**
     * Get all system parameters (or properties).
     */
    ParamData getParamData();

    /**
     * Save modified system parameters.
     */
    void saveModifiedProperties(HashMap<String, Object> modifiedProperties); 
}
