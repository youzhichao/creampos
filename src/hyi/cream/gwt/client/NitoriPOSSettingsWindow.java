package hyi.cream.gwt.client;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Nitori POS settings window.
 *
 * @author Bruce You
 * @since Dec 10, 2008
 */
public class NitoriPOSSettingsWindow extends DialogBox {

    TextBox storeId;
    TextBox terminalNumber;
    String terminalNumberString;
    ListBox lineDisplayType;
    TextBox welcomeMessage;
    ListBox printerCommandSet;
    TextBox posEth;
    TextBox posIP;
    TextBox scIP;

    public NitoriPOSSettingsWindow() {
        super(true, false);
        setText("Nitori POS Settings");

        VerticalPanel mainPanel = new VerticalPanel();
        Grid upperPanel = new Grid(8, 2);
        int row = 0;

        // 門市代號
        upperPanel.setWidget(row, 0, new Label("門市代號:"));
        storeId = new TextBox();
        storeId.setWidth("8em");
        storeId.addStyleName("CodeText");
        upperPanel.setWidget(row, 1, storeId);
        row++;

        // 客顯類型
        upperPanel.setWidget(row, 0, new Label("客顯類型:"));
        lineDisplayType = new ListBox();
        lineDisplayType.setVisibleItemCount(1);
        lineDisplayType.addItem("IBM");
        lineDisplayType.addItem("nonIBM");
        upperPanel.setWidget(row, 1, lineDisplayType);
        row++;

        // 客顯歡迎訊息
        upperPanel.setWidget(row, 0, new Label("客顯歡迎訊息:"));
        welcomeMessage = new TextBox();
        welcomeMessage.setWidth("40em");
        welcomeMessage.setMaxLength(40);
        welcomeMessage.addStyleName("CodeText");
        upperPanel.setWidget(row, 1, welcomeMessage);
        row++;

        // 印表機指令集
        upperPanel.setWidget(row, 0, new Label("印表機指令集:"));
        printerCommandSet = new ListBox();
        printerCommandSet.setVisibleItemCount(1);
        printerCommandSet.addItem("Epson");
        printerCommandSet.addItem("TP-3688");
        upperPanel.setWidget(row, 1, printerCommandSet);
        row++;

        // POS機號
        upperPanel.setWidget(row, 0, new Label("POS機號:"));
        terminalNumber = new TextBox();
        terminalNumber.setWidth("5em");
        terminalNumber.addStyleName("CodeText");
        upperPanel.setWidget(row, 1, terminalNumber);
        row++;

        // POS以太網路界面
        upperPanel.setWidget(row, 0, new Label("POS以太網路界面:"));
        posEth = new TextBox();
        posEth.setWidth("4em");
        posEth.setMaxLength(4);
        posEth.addStyleName("CodeText");
        posEth.setText("eth1");
        upperPanel.setWidget(row, 1, posEth);
        row++;

        // POS IP
        upperPanel.setWidget(row, 0, new Label("POS IP地址:"));
        posIP = new TextBox();
        posIP.setWidth("15em");
        posIP.setMaxLength(15);
        posIP.addStyleName("CodeText");
        upperPanel.setWidget(row, 1, posIP);
        row++;

        // SC IP
        upperPanel.setWidget(row, 0, new Label("SC IP地址:"));
        scIP = new TextBox();
        scIP.setWidth("15em");
        scIP.setMaxLength(15);
        scIP.addStyleName("CodeText");
        upperPanel.setWidget(row, 1, scIP);
        row++;

        loadSettings();

        Button saveButton = new Button("Save");
        saveButton.addClickListener(new ClickListener() {
            public void onClick(Widget sender) {
                // 門市代號
                executeUpdate("UPDATE store SET storeid='" + storeId.getText() + "'");

                // 客顯類型
                setLineDisplayType(lineDisplayType);

                // 客顯歡迎訊息
                executeUpdate("UPDATE property SET value='" + welcomeMessage.getText() + "' WHERE name='welcomeMessage'");
                setLineDisplayWelcomeMessage(welcomeMessage); // update application-level cache

                // 印表機指令集
                setPrinterCommandSet(printerCommandSet);

                // POS機號
                executeUpdate("UPDATE property SET value='" + terminalNumber.getText() + "' WHERE name='TerminalNumber'");
                executeCommand(new String[] {"/bin/hostname",  "pos" + terminalNumber.getText()});

                // SC IP
                executeUpdate("UPDATE property SET value='" + scIP.getText() + "' WHERE name='SCIPAddress'");

                // POS IP
                executeCommand(new String[] {"/bin/sed", "-i.old",
                    "s/address.*/address " + posIP.getText() + "/",
                    "/etc/network/interfaces"});
                executeCommand(new String[] {"/bin/sed", "-i.old",
                    "s/.*pos.*/" + posIP.getText() + "\\tpos" + terminalNumberString + "/",
                    "/etc/hosts"});
                executeCommand(new String[] {"/sbin/ifconfig", posEth.getText(), posIP.getText()});

                NitoriPOSSettingsWindow.this.setVisible(false);
            }
        });

        Button cancelButton = new Button("Cancel");
        cancelButton.addClickListener(new ClickListener() {
            public void onClick(Widget sender) {
                NitoriPOSSettingsWindow.this.setVisible(false);
            }
        });

        FlowPanel lowerPanel = new FlowPanel();
        lowerPanel.addStyleName("LowerRightButton");
        lowerPanel.add(saveButton);
        lowerPanel.add(cancelButton);

        mainPanel.add(upperPanel);
        //mainPanel.add(midPanel);
        mainPanel.add(lowerPanel);

        setWidget(mainPanel);
    }

    private void loadSettings() {
        loadValue(storeId, "SELECT storeid FROM store");
        loadValue(terminalNumber, "SELECT value FROM property WHERE name='TerminalNumber'");
        loadLineDisplayWelcomeMessage(welcomeMessage);
        loadPOSIPAddress(posIP);
        loadValue(scIP, "SELECT value FROM property WHERE name='SCIPAddress'");
    }

    private void loadValue(final TextBox textBox, String statement) {
        ICreamGWTService.App.getInstance().querySingleValue(
            statement, new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {}
            public void onSuccess(String result) {
                if (textBox == terminalNumber)
                    terminalNumberString = result;        
                textBox.setText(result);
            }
        });
    }

    private void loadLineDisplayWelcomeMessage(final TextBox textBox) {
        ICreamGWTService.App.getInstance().getLineDisplayWelcomeMessage(
            new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {}
            public void onSuccess(String result) {
                textBox.setText(result);
            }
        });
    }

    private void setLineDisplayWelcomeMessage(final TextBox textBox) {
        ICreamGWTService.App.getInstance().setLineDisplayWelcomeMessage(textBox.getText(),
            new AsyncCallback<Void>() {
            public void onFailure(Throwable caught) {}
            public void onSuccess(Void result) {
            }
        });
    }

    private void setPrinterCommandSet(final ListBox printerCommandSet) {
        ICreamGWTService.App.getInstance().setEpsonPrinterCommandSet(
            printerCommandSet.getItemText(printerCommandSet.getSelectedIndex()),
            new AsyncCallback() {
            public void onFailure(Throwable caught) {}
            public void onSuccess(Object result) {
            }
        });
    }

    private void setLineDisplayType(final ListBox lineDisplayType) {
        ICreamGWTService.App.getInstance().setLineDisplayType(
            lineDisplayType.getItemText(lineDisplayType.getSelectedIndex()),
            new AsyncCallback() {
            public void onFailure(Throwable caught) {}
            public void onSuccess(Object result) {
            }
        });
    }

    private void loadPOSIPAddress(final TextBox textBox) {
        ICreamGWTService.App.getInstance().getLocalIPAddress(
            new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {}
            public void onSuccess(String result) {
                textBox.setText(result);
            }
        });
    }

    private void executeUpdate(String updateStatement) {
        ICreamGWTService.App.getInstance().queryDatabase(updateStatement,
            new AsyncCallback<String>() {
                public void onFailure(Throwable x) {}
                public void onSuccess(String x) {}
            });
    }

    private void executeCommand(String[] command) {
        ICreamGWTService.App.getInstance().executeCommand(command,
            new AsyncCallback<String>() {
                public void onFailure(Throwable x) {}
                public void onSuccess(String x) {}
            });
    }
}