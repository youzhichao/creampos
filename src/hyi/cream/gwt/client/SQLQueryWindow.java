package hyi.cream.gwt.client;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * SQL query window.
 *
 * @author Bruce You
 * @since Aug 26, 2008 3:57:45 PM
 */
public class SQLQueryWindow extends DialogBox {

    public SQLQueryWindow() {
        super(true, false);
        setText("SQL Query");

        VerticalPanel mainPanel = new VerticalPanel();
        Grid upperPanel = new Grid(1, 3);
        upperPanel.setWidget(0, 0, new Label("SQL Statement:"));
        final TextArea sqlStatement = new TextArea();
        sqlStatement.setWidth("50em");
        sqlStatement.setHeight("3em");
        sqlStatement.addStyleName("CodeText");
        upperPanel.setWidget(0, 1, sqlStatement);
        Button queryButton = new Button("Query");
        //final TextArea resultBox = new TextArea();
        final HTML resultBox = new HTML();
        ScrollPanel midPanel = new ScrollPanel(resultBox);
        resultBox.setWidth("64em");
        resultBox.setHeight("30em");
        resultBox.setStyleName("SQLResultText");
        queryButton.addClickListener(new ClickListener() {
            public void onClick(Widget sender) {
                ICreamGWTService.App.getInstance().queryDatabase(sqlStatement.getText(), new AsyncCallback<String>() {
                    public void onFailure(Throwable caught) {
                    }

                    public void onSuccess(String result) {
                        resultBox.setHTML(result);
                    }
                });
            }
        });
        upperPanel.setWidget(0, 2, queryButton);

        Button closeButton = new Button("Close");
        closeButton.addClickListener(new ClickListener() {
            public void onClick(Widget sender) {
                SQLQueryWindow.this.setVisible(false);
            }
        });

        SimplePanel lowerPanel = new SimplePanel();
        lowerPanel.addStyleName("LowerRightButton");
        lowerPanel.add(closeButton);

        mainPanel.add(upperPanel);
        mainPanel.add(midPanel);
        mainPanel.add(lowerPanel);

        setWidget(mainPanel);
    }
}
