package hyi.cream.gwt.client;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * MSR window.
 *
 * @author Bruce You
 * @since Aug 25, 2008 11:31:19 AM
 */
public class MSRWindow extends DialogBox {

    public MSRWindow() {
        super(true, false);
        setText("MSR");
        VerticalPanel mainPanel = new VerticalPanel();

        FlexTable input = new FlexTable();
        final TextBox cardNumber;
        final TextBox expirationDate;
        input.setWidget(0, 0, new Label("Card Number"));
        input.setWidget(1, 0, cardNumber = new TextBox());
        input.setWidget(0, 1, new Label("Expr.(MMYY)"));
        input.setWidget(1, 1, expirationDate = new TextBox());
        cardNumber.setWidth("16em");
        cardNumber.setMaxLength(16);
        expirationDate.setWidth("4em");
        expirationDate.setMaxLength(4);

        Button sendButton = new Button("Send", new ClickListener() {
            public void onClick(Widget sender) {
                ICreamGWTService.App.getInstance().swipeCreditCard(
                    cardNumber.getText(), expirationDate.getText(), new AsyncCallback() {
                    public void onFailure(Throwable caught) {
                    }

                    public void onSuccess(Object result) {
                    }
                });
            }
        });
        mainPanel.add(input);
        mainPanel.add(sendButton);
        setWidget(mainPanel);
    }
}
