package hyi.cream.gwt.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import hyi.cream.gwt.client.device.POSKeyboardData;
import hyi.cream.gwt.client.device.KeylockData;
import hyi.cream.gwt.client.device.POSKeyboardAndKeylockData;
import hyi.cream.gwt.client.device.ParamData;

import java.util.Map;
import java.util.HashMap;

/**
 * Create GWT service interface async. version.
 *
 * @author Bruce You
 */
public interface ICreamGWTServiceAsync {

    void getMessage(String msg, AsyncCallback<String> async);

    /**
     * Get POS keyboard and keylock detail information.
     */
    void getPOSKeyboardAndKeylockData(AsyncCallback<POSKeyboardAndKeylockData> async);

    /**
     * Send POS keyboard event to front-end POS.
     */
    void pressPOSButton(int keyCode, AsyncCallback<Void> async);

    /**
     * Send keylock event to front-end POS.
     */
    void turnKeylock(int position, AsyncCallback<Void> async);

    /**
     * Get current LineDisplay text, each line is seperated by \n.
     */
    void getLineDisplayText(AsyncCallback<String> async);

    /**
     * Get the latest printing lines to POS printer.
     */
    void retrievePrintingLines(AsyncCallback<String> async);

    /**
     * Set "alwaysHealthy" flag for POS Printer.
     */
    void setPrinterAlwaysHealthy(boolean alwaysHealthy, AsyncCallback<Void> async);

    /**
     * Send scanner event to front-end POS.
     */
    void scanBarcode(String barcode, AsyncCallback<Void> async);

    /**
     * Get candidate item with codePreifx.
     */
    void getItemCandidate(String codePrefix, AsyncCallback<String[]> async);

    /**
     * Send MSR event to front-end POS.
     */
    void swipeCreditCard(String cardNumber, String expirationDate, AsyncCallback<Void> async);

    /**
     * Query database.
     */
    void queryDatabase(String sqlStatement, AsyncCallback<String> async);

    /**
     * Query last transaction.
     */
    void queryLastTransaction(AsyncCallback<String[]> async);

    /**
     * Query transaction.
     */
    void queryTransaction(int transactionNumber, AsyncCallback<String[]> async);

    /**
     * Query last z.
     */
    void queryLastZReport(AsyncCallback<String[]> async);

    /**
     * Check to see if it's waiting for CAT response.
     */
    void waitForCATResponse(AsyncCallback<Boolean> async);

    /**
     * Get CAT request.
     */
    void getCATRequest(AsyncCallback<String> async);

    /**
     * Send CAT response.
     */
    void sendCATResponse(String response, AsyncCallback<Void> async);

    /**
     * Query a single value by a SQL command.
     */
    void querySingleValue(String sqlStatement, AsyncCallback<String> async);

    /**
     * Get POS IP address.
     */
    void getLocalIPAddress(AsyncCallback<String> async);

    /**
     * Get line display welcome message.
     */
    void getLineDisplayWelcomeMessage(AsyncCallback<String> async);

    /**
     * Set line display welcome message in application-level cache.
     */
    void setLineDisplayWelcomeMessage(String welcomeMessage, AsyncCallback<Void> async);

    /**
     * Execute a shell command.
     */
    void executeCommand(String[] command, AsyncCallback<String> async);

    /**
     * Set Epson printer command set.
     */
    void setEpsonPrinterCommandSet(String commandSet, AsyncCallback<Void> async);

    /**
     * Set Nitori IBM4840LineDisplay line display type.
     */
    void setLineDisplayType(String type, AsyncCallback<Void> async);

    /**
     * Get all system parameters (or properties).
     */
    void getParamData(AsyncCallback<ParamData> async);

    /**
     * Save modified system parameters.
     */
    void saveModifiedProperties(HashMap<String, Object> modifiedProperties, AsyncCallback<Void> async);
}
