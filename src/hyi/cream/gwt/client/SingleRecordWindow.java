package hyi.cream.gwt.client;

import com.google.gwt.user.client.ui.*;

/**
 * SQL query result window for showing one record one time.
 *
 * @author Bruce You
 * @since 2008/8/31 下午 04:25:17
 */
public class SingleRecordWindow extends DialogBox {

//    private int transactionNumber;
    private HorizontalPanel contentPanel;
    private HTML recordTable;
//    private HTML trandetailTable;

    /**
     * Constructor of TransactionWindow.
     *
     * @param sqlResult [0] is a HTML table for the record
     */
    public SingleRecordWindow(String title, String[] sqlResult) {
        super(true, false);
        setText(title);

//        transactionNumber = Integer.parseInt(sqlResult[2]);

        VerticalPanel mainPanel = new VerticalPanel();

        contentPanel = new HorizontalPanel();
        ScrollPanel scrollPanel = new ScrollPanel(contentPanel);

//        //--BEGIN CHANGE--
//        //Prevent IE standard mode bug when AbsolutePanel is among our children
//        //Have not found any negative side-effects to doing this...
//        DOM.setStyleAttribute(scrollPanel.getElement(), "position", "relative");
//        //--END CHANGE--

        createDataTable(sqlResult);

//        Button prevButton = new Button("Previous");
//        prevButton.addClickListener(new ClickListener() {
//            public void onClick(Widget sender) {
//                if (transactionNumber > 1) {
//                    transactionNumber--;
//                    queryAndShowTransaction();
//                }
//            }
//        });
//
//        Button nextButton = new Button("Next");
//        nextButton.addClickListener(new ClickListener() {
//            public void onClick(Widget sender) {
//                transactionNumber++;
//                queryAndShowTransaction();
//            }
//        });

        Button closeButton = new Button("Close");
        closeButton.addClickListener(new ClickListener() {
            public void onClick(Widget sender) {
                SingleRecordWindow.this.setVisible(false);
                //CreamGWTApp.instance.destroyTransactionWindow();
            }
        });

        FlowPanel lowerPanel = new FlowPanel();
        lowerPanel.addStyleName("LowerRightButton");
//        lowerPanel.add(prevButton);
//        lowerPanel.add(nextButton);
        lowerPanel.add(closeButton);

        mainPanel.add(scrollPanel);
        mainPanel.add(lowerPanel);

        setWidget(mainPanel);
    }

//    private void queryAndShowTransaction() {
//        ICreamGWTService.App.getInstance().queryTransaction(transactionNumber, new AsyncCallback<String[]>() {
//            public void onFailure(Throwable caught) {
//            }
//
//            /**
//             * @param sqlResult [0] is a HTML table for tranhead, [1] is for trandetail
//             */
//            public void onSuccess(String[] sqlResult) {
//                createDataTable(sqlResult);
//            }
//        });
//    }

    private void createDataTable(String[] sqlResult) {
        if (recordTable != null)
            contentPanel.remove(recordTable);
//        if (trandetailTable != null)
//            contentPanel.remove(trandetailTable);

        recordTable = new HTML();
        //recordTable.setWidth("200px");
        recordTable.setHeight("36em");
        recordTable.setStyleName("SQLResultText");
        recordTable.setHTML(sqlResult[0]);
//        trandetailTable = new HTML();
//        //trandetailTable.setWidth("640px");
//        trandetailTable.setHeight("30em");
//        trandetailTable.setStyleName("SQLResultText");
//        trandetailTable.setHTML(sqlResult[1]);

        contentPanel.add(recordTable);
//        contentPanel.add(trandetailTable);
    }
}