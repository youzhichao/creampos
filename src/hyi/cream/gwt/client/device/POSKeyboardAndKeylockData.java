package hyi.cream.gwt.client.device;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Union of POS keyboard and keylock data.
 *
 * @author Bruce You
 */
public class POSKeyboardAndKeylockData implements IsSerializable {

    private POSKeyboardData posKeyboardData;
    private KeylockData keylockData;

    public POSKeyboardAndKeylockData() {
    }

    public POSKeyboardAndKeylockData(POSKeyboardData posKeyboardData, KeylockData keylockData) {
        this.posKeyboardData = posKeyboardData;
        this.keylockData = keylockData;
    }

    public POSKeyboardData getPosKeyboardData() {
        return posKeyboardData;
    }

    public void setPosKeyboardData(POSKeyboardData posKeyboardData) {
        this.posKeyboardData = posKeyboardData;
    }

    public KeylockData getKeylockData() {
        return keylockData;
    }

    public void setKeylockData(KeylockData keylockData) {
        this.keylockData = keylockData;
    }
}
