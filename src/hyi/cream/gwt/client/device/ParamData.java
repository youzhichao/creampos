package hyi.cream.gwt.client.device;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.Map;
import java.util.HashMap;

/**
 * Param container.
 *
 * @author Bruce You
 * @since 2009/3/30 16:18:21
 */
public class ParamData implements IsSerializable {

    //          Category    Name    Values
    private Map<String, Map<String, ParamValues>> paramData =
            new HashMap<String, Map<String, ParamValues>>();

    public Map<String, Map<String, ParamValues>> getParamData() {
        return paramData;
    }

//    public ParamData insercrtTestData() {
//        Map params = new HashMap<String, ParamValues>();
//
//        ParamValues paramValues = new ParamValues();
//        paramValues.setComment("");
//        paramValues.setDescription("組態檔案目錄");
//        paramValues.setName("ConfigDir");
//        //paramValues.setPossibleValues(Arrays.asList(new String[] {"1", "2", "3"}));
//        paramValues.setPossibleValues(null);
//        paramValues.setValue("./conf/nitori");
//        params.put("ConfigDir", paramValues);
//
//        paramValues = new ParamValues();
//        paramValues.setComment("");
//        paramValues.setDescription("組態檔案目錄2");
//        paramValues.setName("ConfigDir2");
//        //paramValues.setPossibleValues(Arrays.asList(new String[] {"1", "2", "3"}));
//        paramValues.setPossibleValues(null);
//        paramValues.setValue("./conf/nitori2");
//        params.put("ConfigDir2", paramValues);
//
//        paramData.put("SystemParameter1", params);
//        return this;
//    }
}
