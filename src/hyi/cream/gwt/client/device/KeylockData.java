package hyi.cream.gwt.client.device;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.List;
import java.util.ArrayList;

/**
 * Class containing POS Keylock data.
 *
 * @author Bruce You
 */
public class KeylockData implements IsSerializable {

    private String logicalName;
    private List<String> positionNames = new ArrayList<String>();
    private List<Integer> positionCodes = new ArrayList<Integer>();

    public String getLogicalName() {
        return logicalName;
    }

    public void setLogicalName(String logicalName) {
        this.logicalName = logicalName;
    }

    public List<String> getPositionNames() {
        return positionNames;
    }

    public void setPositionNames(List<String> positionNames) {
        this.positionNames = positionNames;
    }

    public List<Integer> getPositionCodes() {
        return positionCodes;
    }

    public void setPositionCodes(List<Integer> positionCodes) {
        this.positionCodes = positionCodes;
    }
}
