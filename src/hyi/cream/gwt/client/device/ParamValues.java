package hyi.cream.gwt.client.device;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;
import java.util.List;

/**
 * Param container.
 *
 * @author Bruce You
 * @since 2009/3/30 16:18:21
 */
public class ParamValues implements IsSerializable {

    private String name;            // param name in 'property' table
    private String fieldName;       // field name in groovy class Param
    private String description;
    private String comment;
    private Serializable value;
    private List<? extends Serializable> possibleValues;
    private int seq;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Serializable getValue() {
        return value;
    }

    public void setValue(Serializable value) {
        this.value = value;
    }

    public List<? extends Serializable> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(List<? extends Serializable> possibleValues) {
        this.possibleValues = possibleValues;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }
}
