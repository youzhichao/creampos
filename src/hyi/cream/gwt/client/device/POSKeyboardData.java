package hyi.cream.gwt.client.device;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.io.Serializable;

/**
 * Class containing POS Keyboard and keylock data.
 *
 * @author Bruce You
 */
public class POSKeyboardData implements IsSerializable {

    private String logicalName;
    private List<POSButtonData> posButtons = new ArrayList<POSButtonData>();

    public String getLogicalName() {
        return logicalName;
    }

    public void setLogicalName(String logicalName) {
        this.logicalName = logicalName;
    }

    public List<POSButtonData> getPosButtons() {
        return posButtons;
    }

    public void setPosButtons(List<POSButtonData> posButtons) {
        this.posButtons = posButtons;
    }
}
