package hyi.cream.gwt.client.device;

import com.google.gwt.user.client.rpc.IsSerializable;

public class POSButtonData implements IsSerializable {

    private int row;
    private int col;
    private int keyCode;
    private String label;

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(int keyCode) {
        this.keyCode = keyCode;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
