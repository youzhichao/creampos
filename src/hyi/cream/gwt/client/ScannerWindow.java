package hyi.cream.gwt.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import java.util.List;
import java.util.ArrayList;

/**
 * Scanner window.
 *
 * @author Bruce You
 * @since Aug 24, 2008 5:13:17 PM
 */
public class ScannerWindow extends DialogBox {

    public ScannerWindow() {
        super(true, false);
        setText("Scanner");
        FlowPanel mainPanel = new FlowPanel();
        //final TextBox barcodeBox = new TextBox();

        SuggestOracle or = new SuggestOracle() {

            /**
             * Generate a {@link com.google.gwt.user.client.ui.SuggestOracle.Response} based on a specific
             * {@link com.google.gwt.user.client.ui.SuggestOracle.Request}. After the
             * {@link com.google.gwt.user.client.ui.SuggestOracle.Response} is created, it is passed into
             * {@link com.google.gwt.user.client.ui.SuggestOracle.Callback#onSuggestionsReady(com.google.gwt.user.client.ui.SuggestOracle.Request, com.google.gwt.user.client.ui.SuggestOracle.Response)}.
             *
             * @param request  the request
             * @param callback the callback to use for the response
             */
            public void requestSuggestions(final Request request, final Callback callback) {
                String barcodePrefix = request.getQuery();
                ICreamGWTService.App.getInstance().getItemCandidate(barcodePrefix, new AsyncCallback<String[]>() {
                    public void onFailure(Throwable caught) {
                    }

                    public void onSuccess(String[] candidates) {
                        List<Suggestion> suggestions = new ArrayList<Suggestion>();
                        for (final String item : candidates) {
                            Suggestion suggestion = new Suggestion() {
                                public String getDisplayString() {
                                    return item;
                                }

                                public String getReplacementString() {
                                    return item.split("\\s")[0];
                                }
                            };
                            suggestions.add(suggestion);
                        }
                        Response response = new Response(suggestions);
                        callback.onSuggestionsReady(request, response);
                    }
                });

            }
        };
        final SuggestBox barcodeBox = new SuggestBox(or);

        Button sendButton = new Button("Send", new ClickListener() {
            public void onClick(Widget sender) {
                String barcode = barcodeBox.getText();
                ICreamGWTService.App.getInstance().scanBarcode(barcode, new AsyncCallback() {
                    public void onFailure(Throwable caught) {
                    }

                    public void onSuccess(Object result) {
                    }
                });
            }
        });
        mainPanel.add(barcodeBox);
        mainPanel.add(sendButton);
        setWidget(mainPanel);
    }
}
