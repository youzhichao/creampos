package hyi.cream.gwt.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.NameValuePair;
import com.gwtext.client.core.RegionPosition;
import com.gwtext.client.data.SimpleStore;
import com.gwtext.client.data.Store;
import com.gwtext.client.widgets.*;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.grid.GridEditor;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.grid.GridView;
import com.gwtext.client.widgets.grid.PropertyGridPanel;
import com.gwtext.client.widgets.grid.event.GridRowListener;
import com.gwtext.client.widgets.grid.event.PropertyGridPanelListener;
import com.gwtext.client.widgets.layout.BorderLayout;
import com.gwtext.client.widgets.layout.BorderLayoutData;
import hyi.cream.gwt.client.device.ParamData;
import hyi.cream.gwt.client.device.ParamValues;

import java.util.*;

/**
 * POS settings window.
 *
 * @author Bruce You
 * @since Mar 26, 2009
 */
public class POSSettingsWindow extends Window {

    private Panel descriptionPanel;
    private HashMap<String, Object> modifiedProperties = new HashMap<String, Object>();
    private ParamData paramDataStore;

    public POSSettingsWindow(ParamData paramDataStore) {
        this.paramDataStore = paramDataStore;
        Map<String, Map<String, ParamValues>> paramData = paramDataStore.getParamData();

        BorderLayoutData centerData = new BorderLayoutData(RegionPosition.CENTER);
        centerData.setMargins(0, 0, 0, 0);

        //center panel
        TabPanel tabPanel = new TabPanel();
        tabPanel.setActiveTab(0);
        tabPanel.setAnimScroll(true);
        tabPanel.setEnableTabScroll(true);

        int i = 0;
        Object[] categories = paramData.keySet().toArray();
        Arrays.sort(categories);
        for (Object categoryObj : categories) {
            String categoryEnumName = (String)categoryObj;
            Map<String, ParamValues> params = paramData.get(categoryEnumName);

            String category = ((String)categoryObj).substring(3);
            if (category.equals("System1")) category = "系統1";
            else if (category.equals("System2")) category = "系統2";
            else if (category.equals("System3")) category = "系統3";
            else if (category.equals("Member")) category = "會員";
            else if (category.equals("Printing")) category = "列印";
            else if (category.equals("PriceOverriding")) category = "變價";
            else if (category.equals("Peripherals")) category = "週邊設備";
            else if (category.equals("Inline")) category = "資料傳輸";
            else if (category.equals("Cashier")) category = "收銀員";
            else if (category.equals("UserInterface")) category = "使用界面1";
            else if (category.equals("UserInterface2")) category = "使用界面2";
            else if (category.equals("Numbers")) category = "計數";
            else if (category.equals("Constrains")) category = "限制";
            else if (category.equals("CampusMart")) category = "教育超市";

            Panel tab = new Panel();
            tab.setTitle(category);
            tab.setAutoScroll(true);
            tab.setLayout(new BorderLayout());
            tab.add(createPropertyPanel(params), centerData);
            tabPanel.add(tab);
        }

        setTitle("POS Settings");
        setClosable(true);
        setWidth(600);
        setHeight(550);
        setPlain(true);
        setLayout(new BorderLayout());
        add(tabPanel, centerData);

        createDescriptionPanel();
        createOkCancelButtons();

        setCloseAction(Window.HIDE);
    }

    private void createDescriptionPanel() {
        descriptionPanel = new Panel();
        descriptionPanel.setTitle("參數說明");
        descriptionPanel.setHeight(100);
        descriptionPanel.setAutoScroll(true);
        descriptionPanel.setHtml("");
        descriptionPanel.setBodyStyle("background-color:#CDEB8B");
        descriptionPanel.setCollapsible(true);
        BorderLayoutData southData = new BorderLayoutData(RegionPosition.SOUTH);
        southData.setMinSize(100);
        southData.setMaxSize(200);
        southData.setMargins(0, 0, 0, 0);
        southData.setSplit(true);
        add(descriptionPanel, southData);
    }

    private void createOkCancelButtons() {
        Button saveButton = new Button("儲存", new ButtonListenerAdapter() {
            public void onClick(Button button, EventObject e) {
                MessageBox.confirm("Confirm", "確認儲存嗎？",
                    new MessageBox.ConfirmCallback() {
                        public void execute(String btnID) {
                            //MessageBox.alert(btnID);
                            if ("yes".equals(btnID)) {
                                ICreamGWTService.App.getInstance().saveModifiedProperties(
                                    modifiedProperties,new AsyncCallback() {
                                    public void onFailure(Throwable caught) {}
                                    public void onSuccess(Object result) {}
                                });
                                CreamGWTApp.getInstance().closePOSSettingsWindow();
                            }
                        }
                    });
            }
        });
        addButton(saveButton);

        Button cancelButton = new Button("取消", new ButtonListenerAdapter() {
            public void onClick(Button button, EventObject e) {
                CreamGWTApp.getInstance().closePOSSettingsWindow();
            }
        });
        addButton(cancelButton);
    }

    private Panel createPropertyPanel(final Map<String, ParamValues> params) {
        Map customEditors = new HashMap();
        final NameValuePair[] source = new NameValuePair[params.size()];

        Panel outterPanel = new Panel();
        PropertyGridPanel propertyPanel = new PropertyGridPanel();
        //propertyPanel.setId("props-propertyPanel");
        //propertyPanel.setNameText("Properties Grid");
        //propertyPanel.setWidth(300);
        //propertyPanel.setHeight(100);
        propertyPanel.setAutoScroll(true);
        propertyPanel.setAutoHeight(true);
        propertyPanel.setSorted(false);
        propertyPanel.setHideColumnHeader(true);

        GridView view = new GridView();
        view.setForceFit(true);
        view.setScrollOffset(2); // the propertyPanel will never have scrollbars
        propertyPanel.setView(view);

        propertyPanel.addGridRowListener(new GridRowListener() {
            public void onRowClick(GridPanel grid, int rowIndex, EventObject e) {
                String description = source[rowIndex].getName();
                String paramName = params.get(description).getName();
                String comment = params.get(description).getComment();
                descriptionPanel.setHtml("<p>參數：" + paramName + "<br/>" +
                    ((comment == null || comment.trim().length() == 0) ? "" : ("說明：" + comment)) +
                    "</p>");
            }

            public void onRowDblClick(GridPanel grid, int rowIndex, EventObject e) {
            }

            public void onRowContextMenu(GridPanel grid, int rowIndex, EventObject e) {
            }
        });

        propertyPanel.addPropertyGridPanelListener(new PropertyGridPanelListener() {
            public boolean doBeforePropertyChange(PropertyGridPanel source, String recordID, Object value, Object oldValue) {
                return true;
            }

            public void onPropertyChange(PropertyGridPanel source, String description, Object newValue, Object oldValue) {
                Map<String, Map<String, ParamValues>> paramData = paramDataStore.getParamData();
                for (String category : paramData.keySet()) {
                    Map<String, ParamValues> params = paramData.get(category);
                    ParamValues paramValues = params.get(description);
                    if (paramValues != null) {
                        // put groovy class Param's field name as key
                        modifiedProperties.put(paramValues.getFieldName(), newValue);
                        return;
                    }
                }
            }
        });

        int i = 0;
        Object[] paramSet = params.keySet().toArray(); // param descriptions
        Arrays.sort(paramSet, new Comparator() {
            public int compare(Object o1, Object o2) {
                ParamValues paramValues1 = params.get(o1);
                ParamValues paramValues2 = params.get(o2);
                return paramValues1.getSeq() > paramValues2.getSeq() ? 1 :
                    paramValues1.getSeq() < paramValues2.getSeq() ? -1 : 0;
            }
        });
        for (Object param : paramSet) {
            String description = (String)param;
            ParamValues paramValues = params.get(description);
            //String description = paramValues.getDescription();
            Object value = paramValues.getValue();
            List possibleValues = paramValues.getPossibleValues();
            if (value instanceof String)
                source[i] = new NameValuePair(description, (String)value);
            else if (value instanceof Integer)
                source[i] = new NameValuePair(description, (Integer)value);
            else if (value instanceof Boolean)
                source[i] = new NameValuePair(description, (Boolean)value);
            else if (value instanceof Float)
                source[i] = new NameValuePair(description, (Float)value);
            else if (value instanceof Date)
                source[i] = new NameValuePair(description, (Date)value);
            else
                source[i] = new NameValuePair(description, value.toString());
            i++;

            if (possibleValues != null && !possibleValues.isEmpty()) {
                GridEditor comboEditor = new GridEditor(createComboBox(possibleValues));
                customEditors.put(description, comboEditor);
            }
        }

        propertyPanel.setCustomEditors(customEditors);
        propertyPanel.setSource(source);

        outterPanel.setLayout(new BorderLayout());
        outterPanel.add(propertyPanel, new BorderLayoutData(RegionPosition.CENTER));
        outterPanel.setAutoScroll(true);
        return outterPanel;
    }

    private ComboBox createComboBox(List possibleValues) {
        //create a Store using local array data
        final Store store = new SimpleStore("choice", possibleValues.toArray());
        store.load();

        final ComboBox cb = new ComboBox();
        cb.setEditable(false);
        cb.setForceSelection(true);
        //cb.setMinChars(1);
        cb.setFieldLabel("choice");
        cb.setStore(store);
        cb.setDisplayField("choice");
        cb.setMode(ComboBox.LOCAL);
        cb.setTriggerAction(ComboBox.ALL);
        cb.setEmptyText("Enter choice");
        cb.setLoadingText("Searching...");
        cb.setTypeAhead(true);
        cb.setSelectOnFocus(true);
        //cb.setWidth(200);
        cb.setValueField("choice");
        //cb.setTpl("<tpl for=\".\"><div ext:qtip=\"{state}. {nick}\" class=\"x-combo-list-item\">{state}</div></tpl>");

        //this hides the dropdown besides the ComboBox field
        cb.setHideTrigger(false);
        return cb;
    }
}