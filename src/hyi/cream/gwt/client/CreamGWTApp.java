package hyi.cream.gwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.RootPanel;
import com.gwtext.client.widgets.MessageBox;
import hyi.cream.gwt.client.device.POSKeyboardAndKeylockData;
import hyi.cream.gwt.client.device.ParamData;

/**
 * GWT-based Cream management console application.
 */
public class CreamGWTApp implements EntryPoint {

    static CreamGWTApp instance;

    private POSKeyboardWindow posKeyboardWindow;
    private LineDisplayWindow lineDisplayWindow;
    private POSPrinterWindow posPrinterWindow;
    private ScannerWindow scannerWindow;
    private MSRWindow msrWindow;
    private SQLQueryWindow sqlQueryWindow;
    private NitoriPOSSettingsWindow nitoriPOSSettingsWindow;
    private POSSettingsWindow posSettingsWindow;
    private TransactionWindow transactionWindow;
    private ChinaTrustCATWindow chinaTrustCATWindow;
    private SingleRecordWindow zReportWindow;

    public CreamGWTApp() {
        instance = this;
    }

    public static CreamGWTApp getInstance() {
        return instance;
    }
    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        createMenuBar();
    }

    private void createMenuBar() {
        MenuBar firstMenu = new MenuBar(true);
        firstMenu.addItem("Open All Devices", new Command() {
            public void execute() {
                openPOSKeyboardWindow();
                openLineDisplayWindow();
                openPOSPrinterWindow();
                openScannerWindow();
                openMSRWindow();
                openChinaTrustCATWindow();
            }
        });
        firstMenu.addSeparator();
        firstMenu.addItem("POS Keyboard", new Command() {
            public void execute() {
                openPOSKeyboardWindow();
            }
        });
        firstMenu.addItem("Line Display", new Command() {
            public void execute() {
                openLineDisplayWindow();
            }
        });
        firstMenu.addItem("POS Printer", new Command() {
            public void execute() {
                openPOSPrinterWindow();
            }
        });
        firstMenu.addItem("Scanner", new Command() {
            public void execute() {
                openScannerWindow();
            }
        });
        firstMenu.addItem("MSR", new Command() {
            public void execute() {
                openMSRWindow();
            }
        });
        firstMenu.addItem("China Trush CAT", new Command() {
            public void execute() {
                openChinaTrustCATWindow();
            }
        });
        firstMenu.addSeparator();
        firstMenu.addItem("Close All Devices", new Command() {
            public void execute() {
                closeAllDevices();
            }
        });

        // Make a new menu bar, adding a few cascading menus to it.
        MenuBar menu = new MenuBar();
        menu.addItem("Virtual Devices", firstMenu);

        MenuBar secondMenu = new MenuBar(true);
        secondMenu.addItem("SQL Query Tool", new Command() {
            public void execute() {
                openSQLQueryWindow();
            }
        });
        secondMenu.addItem("Query Last Transaction", new Command() {
            public void execute() {
                openTransactionWindow();
            }
        });
        secondMenu.addItem("Query Last Z Report", new Command() {
            public void execute() {
                openZReportWindow();
            }
        });
        menu.addItem("Query", secondMenu);

        MenuBar thirdMenu = new MenuBar(true);
        thirdMenu.addItem("POS Settings", new Command() {
            public void execute() {
                openPOSSettingsWindow();
            }
        });
        thirdMenu.addItem("Nitori-Specific Settings", new Command() {
            public void execute() {
                openNitoriPOSSettingsWindow();
            }
        });
        thirdMenu.addSeparator();
        thirdMenu.addItem("Recalculate Reports", new Command() {
            public void execute() {
            }
        });
        thirdMenu.addSeparator();
        thirdMenu.addItem("POS App Restart", new Command() {
            public void execute() {
                executeCommandWithPassword("POS App Restart", "AP_RESTART");
            }
        });
        thirdMenu.addItem("POS Reboot", new Command() {
            public void execute() {
                executeCommandWithPassword("POS Reboot", "REBOOT");
            }
        });
        thirdMenu.addItem("POS Shutdown", new Command() {
            public void execute() {
                executeCommandWithPassword("POS Shutdown", "HALT");
            }
        });
        menu.addItem("Tools", thirdMenu);

        // Add it to the root panel.
        //RootPanel.get("menuBar").add(menu);
        RootPanel.get().add(menu);
    }

    private void executeCommandWithPassword(String title, final String command) {
        MessageBox.prompt(title, "Password:", new MessageBox.PromptCallback() {
            public void execute(String btnID, String text) {
                if ("ok".equals(btnID)) {
                    if ("bingo".equals(text)) {
                        ICreamGWTService.App.getInstance().executeCommand(
                            new String[] { command },
                            new AsyncCallback<String>() {
                                public void onFailure(Throwable _) {}
                                public void onSuccess(String _) {}
                            });
                    } else
                        MessageBox.alert("Wrong password!");
                }
            }
        },
        false);
    }

    private void openSQLQueryWindow() {
        if (sqlQueryWindow == null) {
            sqlQueryWindow = new SQLQueryWindow();
            RootPanel.get().add(sqlQueryWindow, 80, 40);
        } else {
            sqlQueryWindow.setVisible(true);
        }
    }

    private void openNitoriPOSSettingsWindow() {
        if (nitoriPOSSettingsWindow == null) {
            nitoriPOSSettingsWindow = new NitoriPOSSettingsWindow();
            RootPanel.get().add(nitoriPOSSettingsWindow, 80, 40);
        } else {
            nitoriPOSSettingsWindow.setVisible(true);
        }
    }

    private void openPOSSettingsWindow() {
        ICreamGWTService.App.getInstance().getParamData(new AsyncCallback<ParamData>() {
            public void onFailure(Throwable caught) {
            }

            public void onSuccess(ParamData paramData) {
                if (posSettingsWindow == null) {
                    posSettingsWindow = new POSSettingsWindow(paramData);
                    RootPanel.get().add(posSettingsWindow, 80, 40);
                    posSettingsWindow.setVisible(true); // default is invisible
                } else {
                    posSettingsWindow.setVisible(true);
                }
            }
        });
    }

    public void closePOSSettingsWindow() {
        posSettingsWindow.setVisible(false);
        posSettingsWindow.destroy();
        posSettingsWindow = null;
    }

    private void openTransactionWindow() {
        ICreamGWTService.App.getInstance().queryLastTransaction(new AsyncCallback<String[]>() {
            public void onFailure(Throwable caught) {
            }

            /**
             * @param sqlResult [0] is a HTML table for tranhead, [1] is for trandetail
             */
            public void onSuccess(String[] sqlResult) {
                if (transactionWindow == null) {
                    transactionWindow = new TransactionWindow(sqlResult);
                    RootPanel.get().add(transactionWindow, 10, 20);
                } else {
                    transactionWindow.setVisible(true);
                }
            }
        });
    }

    private void openZReportWindow() {
        ICreamGWTService.App.getInstance().queryLastZReport(new AsyncCallback<String[]>() {
            public void onFailure(Throwable caught) {
            }

            /**
             * @param sqlResult [0] is a HTML table for the record data
             */
            public void onSuccess(String[] sqlResult) {
                if (zReportWindow == null) {
                    zReportWindow = new SingleRecordWindow("Z Report", sqlResult);
                    RootPanel.get().add(zReportWindow, 20, 20);
                } else {
                    zReportWindow.setVisible(true);
                }
            }
        });
    }

    private void openPOSKeyboardWindow() {
        ICreamGWTService.App.getInstance().getPOSKeyboardAndKeylockData(new AsyncCallback<POSKeyboardAndKeylockData>() {
            public void onFailure(Throwable caught) {
            }

            public void onSuccess(POSKeyboardAndKeylockData posKeyboardAndKeylockData) {
                if (posKeyboardWindow == null) {
                    posKeyboardWindow = new POSKeyboardWindow(posKeyboardAndKeylockData);
                    RootPanel.get().add(posKeyboardWindow, 10, 40);
                } else {
                    //if (!posKeyboardWindow.isVisible())
                    posKeyboardWindow.setVisible(true);
                }
            }
        });
    }

    private void closeAllDevices() {
        if (posKeyboardWindow != null) {
            posKeyboardWindow.setVisible(false);
            posKeyboardWindow = null;
        }
        if (lineDisplayWindow != null) {
            lineDisplayWindow.setVisible(false);
            lineDisplayWindow = null;
        }
        if (posPrinterWindow!= null) {
            posPrinterWindow.setVisible(false);
            posPrinterWindow = null;
        }
        if (scannerWindow != null) {
            scannerWindow.setVisible(false);
            scannerWindow = null;
        }
        if (msrWindow != null) {
            msrWindow.setVisible(false);
            msrWindow = null;
        }
        if (chinaTrustCATWindow != null) {
            chinaTrustCATWindow.setVisible(false);
            chinaTrustCATWindow = null;
        }
    }

    private void openMSRWindow() {
        if (msrWindow == null) {
            msrWindow = new MSRWindow();
            RootPanel.get().add(msrWindow, 270, 430);
        } else {
            msrWindow.setVisible(true);
        }
    }

    private void openChinaTrustCATWindow() {
        if (chinaTrustCATWindow == null) {
            chinaTrustCATWindow = new ChinaTrustCATWindow();
            RootPanel.get().add(chinaTrustCATWindow, 10, 560);
        } else {
            msrWindow.setVisible(true);
        }
    }

    private void openScannerWindow() {
        if (scannerWindow == null) {
            scannerWindow = new ScannerWindow();
            RootPanel.get().add(scannerWindow, 270, 350);
        } else {
            scannerWindow.setVisible(true);
        }
    }

    private void openPOSPrinterWindow() {
        if (posPrinterWindow == null) {
            posPrinterWindow = new POSPrinterWindow();
            RootPanel.get().add(posPrinterWindow, 650, 40);
        } else {
            posPrinterWindow.setVisible(true);
        }
    }

    private void openLineDisplayWindow() {
        if (lineDisplayWindow == null) {
            lineDisplayWindow = new LineDisplayWindow();
            RootPanel.get().add(lineDisplayWindow, 10, 350);
        } else {
            //if (!lineDisplayWindow.isVisible())
            lineDisplayWindow.setVisible(true);
        }
    }

    void destroyTransactionWindow() {
        if (transactionWindow != null) {
            transactionWindow.setVisible(false);
            transactionWindow = null;
        }
    }

//    static class MyAsyncCallback implements AsyncCallback {
//        public void onSuccess(Object object) {
//            DOM.setInnerHTML(label.getElement(), (String) object);
//        }
//
//        public void onFailure(Throwable throwable) {
//            label.setText("Failed to receive answer from server!");
//        }
//
//        Label label;
//
//        public MyAsyncCallback(Label label) {
//            this.label = label;
//        }
//    }
}
