package hyi.cream

import groovy.sql.Sql
import hyi.cream.dac.Store
import hyi.cream.groovydac.Param
import hyi.cream.inline.NitoriPostProcessor
import hyi.cream.inline.Server
import hyi.cream.inline.ServerThread
import hyi.cream.util.CreamToolkit

import java.sql.SQLException

import static hyi.cream.util.CreamToolkit.getPooledConnection
import static hyi.cream.util.CreamToolkit.logMessage

/**
 * Cream healthy checker.
 *
 * @author Bruce You
 * @since Aug 27, 2008 5:23:54 PM
 */
class HealthyChecker {

    static Sql sql
    static boolean nitoriPOS
    static boolean meilinPOS
    static boolean tk3cPOS      // 燦坤3C

    static private setupSql() {
        println "os.name=${System.getProperty('os.name')}"
        sql = new Sql(getPooledConnection())

        if (!Server.isAtServerSide()) {
            def param = Param.instance
            def welcomeMessage = param.welcomeMessage
            def companyName = param.companyName
            nitoriPOS = welcomeMessage?.contains('NITORI')
            meilinPOS = welcomeMessage?.contains('GUANGMING')
            tk3cPOS = (companyName == 'TK3C')
            println "welcomeMessage=${welcomeMessage}, companyName=${companyName}"
        }
    }

    static private tableExists(String tableName) {
        try {
            sql.rows("SELECT * FROM ${tableName} LIMIT 1".toString()) {}
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    static private fieldExists(String tableName, String fieldName) {
        try {
            sql.rows("SELECT ${fieldName} FROM ${tableName} LIMIT 1".toString()) {}
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    static private rowExists(String sqlStatement) {
        try {
            return sql.firstRow(sqlStatement) != null
        } catch (SQLException e) {
            return false;
        }
    }

    static private execute(message, showErrorLog, sqlStatement) {
        try {
            // 這裡一定要.toString(), 因為用execute(GString)版本會有問題
            sql.execute(sqlStatement.toString())
            logMessage(message)
        } catch (Exception e) {
            if (showErrorLog) {
                logMessage("SQL> ${sqlStatement}")
                logMessage(e)
            }
        }
    }

    static private void setPOSProperty(String name, String value) {
        if (!rowExists("select * from property where name='${name}'")) {
            execute("HealthyChecker: Property ${name} is inserted.", true,
            "INSERT INTO property (name,value) VALUES ('${name}','${value}')")
        } else if (!rowExists("select * from property where name='${name}' AND value='${value}'")) {
            execute("HealthyChecker: Property ${name} is updated.", true,
            "UPDATE property SET value='${value}' WHERE name='${name}'")
        }
    }

    static private checkPOSForT5_0_0() {
        if (nitoriPOS) {
            setPOSProperty('ExclusiveZhunguiDeps', '931:932')
            setPOSProperty('CalculateTaxAmtByUnitPrice', 'yes')
            setPOSProperty('IsGrossTrandtlTaxAmt', 'yes')
            setPOSProperty('DiscountTaxId', '1')
            setPOSProperty('ActionAfterMasterDownload', 'AP Restart')
            setPOSProperty('EnableTouchPanel', 'yes') // 否則輸入發票字軌的Window會出不來
            setPOSProperty('ChangeAmountUpperlimit', '2000')
        } else {
            // 其他不用發票字軌的客戶，字軌也是要設置，否則在切紙的時候會引發
            // GenericCreamPrinter.printLineItemByReceiptGenerator()的一個問題：
            // lineItem.printed永遠不會被設成true
            if (rowExists("select * from property where name='InvoiceID' and trim(value)=''"))
                setPOSProperty('InvoiceID', 'AA')
        }

        execute('HealthyChecker.checkPOSForT5_0_0(): index idx_itemno is created.', false,
            'create index idx_itemno on plu (itemno)')

        if (!tableExists('carddetail')) {
            execute('HealthyChecker.checkForT5_0_0(): carddetail is created.', true, """
            CREATE TABLE carddetail
            (
              id serial NOT NULL, -- Unique ID
              storeId varchar(6) NOT NULL, -- 門市代號
              accountDate date NOT NULL, -- 營業日期
              posNumber smallint NOT NULL, -- POS機號
              transactionNumber integer NOT NULL, --  交易序號
              zSequenceNumber integer NOT NULL, -- Z帳序號
              isVoid char(1), -- 是否被作廢（取消/退貨/調整）（'0':沒有被作廢、'1':被作廢）
              voidTransactionNumber  integer NOT NULL, -- 若此筆為被作廢交易，記錄作廢此筆交易的相應取消/退貨/調整的交易序號；若此筆為作廢交易（取消/退貨/調整），則記錄被作廢的交易序號
              transDateTime timestamp without time zone NOT NULL, -- 交易日期與時間
              cardNo char(19) NOT NULL, -- 卡號
              cardExpDate char(4) NOT NULL, -- 有效期
              approvalCode char(9) NOT NULL, -- 授權碼
              terminalId char(8) NOT NULL, -- 端末機編號
              transAmt numeric(12,2) NOT NULL, -- 總交易金額
              source char(1) NOT NULL, -- 數據來源 ('C': CAT數據、'P': 收銀機輸入數據、'L': 缺乏明細數據、'S': SC補登數據)
              updateDate timestamp without time zone NOT NULL, -- 最後修改日期
              updateUserId varchar(8), -- 數據補登人員代號
              CONSTRAINT pk_carddetail PRIMARY KEY (id),
              CONSTRAINT idx_posnumber_transactionnumber UNIQUE (posnumber, transactionnumber)
            );
            ALTER TABLE carddetail OWNER TO postgres;
            COMMENT ON COLUMN carddetail.id IS 'Unique ID';
            COMMENT ON COLUMN carddetail.storeID IS '門市代號';
            COMMENT ON COLUMN carddetail.accountDate IS '營業日期';
            COMMENT ON COLUMN carddetail.posNumber IS 'POS機號';
            COMMENT ON COLUMN carddetail.transactionNumber IS '交易序號';
            COMMENT ON COLUMN carddetail.zSequenceNumber IS 'Z帳序號';
            COMMENT ON COLUMN carddetail.isVoid IS '是否被作廢（取消/退貨/調整）（0:沒有被作廢、1:被作廢）';
            COMMENT ON COLUMN carddetail.voidTransactionNumber IS '若此筆為被作廢交易，記錄作廢此筆交易的相應取消/退貨/調整的交易序號；若此筆為作廢交易（取消/退貨/調整），則記錄被作廢的交易序號';
            COMMENT ON COLUMN carddetail.transDateTime IS '交易日期與時間';
            COMMENT ON COLUMN carddetail.cardNo IS '卡號';
            COMMENT ON COLUMN carddetail.cardExpDate IS '有效期';
            COMMENT ON COLUMN carddetail.approvalCode IS '授權碼';
            COMMENT ON COLUMN carddetail.terminalID IS '端末機編號';
            COMMENT ON COLUMN carddetail.transAmt IS '總交易金額';
            COMMENT ON COLUMN carddetail.source IS '數據來源 (C: CAT數據、P: 收銀機輸入數據、L: 缺乏明細數據、S: SC補登數據)';
            COMMENT ON COLUMN carddetail.updateDate IS '最後修改日期';
            COMMENT ON COLUMN carddetail.updateUserId IS '數據補登人員代號';
            """)
        }
        if (!tableExists('carddetail_chinatrust')) {
            execute('HealthyChecker.checkForT5_0_0(): carddetail_chinatrust is created.', true, """
            CREATE TABLE carddetail_chinatrust
            (
              id serial NOT NULL, -- Unique ID
              carddetailId integer, -- FK to carddetail.id
              posNumber smallint NOT NULL, -- POS機號
              storeId varchar(6) NOT NULL, -- 門市代號
              accountDate date NOT NULL, -- 營業日期
              zSequenceNumber integer NOT NULL, -- COMMENT 'Z帳序號',
              transType char(2), -- 交易類別（'01':SALE銷售、'02':REFUND退貨、'03':OFFLINE離線、'04':PRE_AUTH預取授權、'30':VOID取消、'40':TIP小費、'41':ADJUST調整、'50':SETTLEMENT調整、'98':ECHO測試）
              respCode char(4), -- 回覆代碼
              hostId char(2), -- 授權銀行編碼
              invoiceNo char(6), -- 調閱編號
              refNo char(12), -- 序號
              bonusPaid numeric(12,2), -- 紅利抵用的實付金額
              bonusDiscount numeric(12,2), -- 紅利抵用的折抵金額
              installmentPeriod smallint, -- 期數
              installmentFee numeric(12,2), -- 手續費
              installmentType char(1), -- 收費方式（'1':一般分期、'2':收費分期）
              installmentPutOffType char(1), -- 延後付款方式（'1':無延後付款、'2':延後付款、'3':彈性付款）
              installmentAmount1 numeric(12,2), -- 首期金額（若為彈性付款，此欄位為後期的首期金額）
              installmentAmount2 numeric(12,2), -- 每期金額（若為彈性付款，此欄位為後期的每期金額）
              installmentAmount3 numeric(12,2), -- 彈性付款前期的每期金額
              rawData char(144), -- CAT機返回的原始數據（目前中國信託的應該是144 bytes）
              CONSTRAINT pk_carddetail_chinatrust PRIMARY KEY (id),
              CONSTRAINT fk_carddetail_chinatrust_carddetail_id FOREIGN KEY (carddetailId)
                  REFERENCES carddetail (id) MATCH SIMPLE
                  ON UPDATE NO ACTION ON DELETE NO ACTION
            );
            ALTER TABLE carddetail_chinatrust OWNER TO postgres;
            COMMENT ON COLUMN carddetail_chinatrust.id IS 'Unique ID';
            COMMENT ON COLUMN carddetail_chinatrust.carddetailId IS 'FK to carddetail.id';
            COMMENT ON COLUMN carddetail_chinatrust.posNumber IS 'POS機號';
            COMMENT ON COLUMN carddetail_chinatrust.storeID IS '門市代號';
            COMMENT ON COLUMN carddetail_chinatrust.accountDate IS '營業日期';
            COMMENT ON COLUMN carddetail_chinatrust.zSequenceNumber IS 'Z帳序號';
            COMMENT ON COLUMN carddetail_chinatrust.transType IS '交易類別（01:SALE銷售、02:REFUND退貨、03:OFFLINE離線、04:PRE_AUTH預取授權、30:VOID取消、40:TIP小費、41:ADJUST調整、50:SETTLEMENT調整、98:ECHO測試）';
            COMMENT ON COLUMN carddetail_chinatrust.respCode IS '回覆代碼';
            COMMENT ON COLUMN carddetail_chinatrust.hostID IS '授權銀行編碼';
            COMMENT ON COLUMN carddetail_chinatrust.invoiceNo IS '調閱編號';
            COMMENT ON COLUMN carddetail_chinatrust.refNo IS '序號';
            COMMENT ON COLUMN carddetail_chinatrust.bonusPaid IS '紅利抵用的實付金額';
            COMMENT ON COLUMN carddetail_chinatrust.bonusDiscount IS '紅利抵用的折抵金額';
            COMMENT ON COLUMN carddetail_chinatrust.installmentPeriod IS '期數';
            COMMENT ON COLUMN carddetail_chinatrust.installmentFee IS '手續費';
            COMMENT ON COLUMN carddetail_chinatrust.installmentType IS '收費方式（1:一般分期、2:收費分期）';
            COMMENT ON COLUMN carddetail_chinatrust.installmentPutOffType IS '延後付款方式（1:無延後付款、2:延後付款、3:彈性付款）';
            COMMENT ON COLUMN carddetail_chinatrust.installmentAmount1 IS '首期金額（若為彈性付款，此欄位為後期的首期金額）';
            COMMENT ON COLUMN carddetail_chinatrust.installmentAmount2 IS '每期金額（若為彈性付款，此欄位為後期的每期金額）';
            COMMENT ON COLUMN carddetail_chinatrust.installmentAmount3 IS '彈性付款前期的每期金額';
            COMMENT ON COLUMN carddetail_chinatrust.rawData IS 'CAT機返回的原始數據（目前中國信託的應該是144 bytes）';
            """)
        }
        if (nitoriPOS && !rowExists("select * from si where sitype='5'")) {
            execute('HealthyChecker.checkForT5_0_0(): bonus discount SI is inserted.', true, """
            delete from si where siid='03';
            insert into si (siid, sicnames, sinamep, sino, catno, sitype, dvalue, dbase, attr, bdate, edate, btime, etime, cycle)
            values ('03','紅利折抵','紅利折抵','00000000','00',5,0,'R','R010000A','2000-01-01','2099-12-31',
            '00:00:00','23:59:59','1111111');
            """)
        }
        if (nitoriPOS && !rowExists("select * from si where sicnames like '廠商折扣%'")) {
            execute('HealthyChecker.checkForT5_0_0(): vendor discount SI is inserted.', true, """
            delete from si where siid='02';
            delete from si where siid='04';
            insert into si (siid, sicnames, sinamep, sino, catno, sitype, dvalue, dbase, attr, bdate, edate, btime, etime, cycle)
            values ('02','廠商折扣','廠商折扣','00000000','00',0,0.1,'R','R010000A','2000-01-01','2099-12-31',
            '00:00:00','23:59:59','1111111');
            """)
        }
        execute('HealthyChecker.checkForT5_0_0(): tranhead_idx_posno_sysdate is created.', false,
            'CREATE INDEX tranhead_idx_posno_sysdate ON tranhead USING btree (posno, sysdate)')
        execute('HealthyChecker.checkForT5_0_0(): tranhead_idx_sysdate_tmtranseq is created.', false,
            'CREATE INDEX tranhead_idx_sysdate_tmtranseq ON tranhead USING btree (sysdate, tmtranseq)')
        execute('HealthyChecker.checkForT5_0_0(): tranhead_idx_posno_tmtranseq is created.', false,
            'CREATE INDEX tranhead_idx_posno_tmtranseq ON tranhead USING btree (posno, tmtranseq)')
        execute('HealthyChecker.checkForT5_0_0(): z_idx_accdate_tcpflg is created.', false,
            'CREATE INDEX z_idx_accdate_tcpflg ON z (accdate, tcpflg)')
    }

    static private checkPOSForT5_0_1() {
        checkPOSForT5_0_0()
    }

    static private checkPOSForT5_0_2() {
        checkPOSForT5_0_1()

        if (nitoriPOS && !rowExists("SELECT * FROM payment WHERE paycategory='CB'")) {
            execute('HealthyChecker.checkForT5_0_2(): Credit card bonus payment is inserted.', true, """
            INSERT INTO payment (payid, payname, paycname, paytype, payrate, denomination, paycategory, priority, seqno)
            VALUES ('07','紅利抵用','紅利抵用','11000000','0',1.00,'CB',0,'07');""")
        }
    }

    static private checkPOSForT5_0_3() {
        checkPOSForT5_0_2()

        /*if (!rowExists("SELECT * FROM payment WHERE paycategory='DP'")) {
            execute('HealthyChecker.checkForT5_0_3(): Back payment is inserted.', true, """
            INSERT INTO payment (payid, payname, paycname, paytype, payrate, denomination, paycategory, priority, seqno)
            VALUES ('08','應收尾款','應收尾款','00000010','0',1.00,'DP',0,'08');""")
        }*/
        // 應收尾款直接改用原來編號06的“應收款”, 但PAYTYPE要是'00000000', 並且PAYCATEGORY要是'DP'
        if (nitoriPOS && !rowExists("SELECT * FROM payment WHERE paycategory='DP'")) {
            execute('HealthyChecker.checkForT5_0_3(): Back payment is updated.', true, """
            UPDATE payment SET paytype='00000000',paycategory='DP' WHERE payid='06';""")
        }
    }

    static private checkPOSForT5_0_4() {
        checkPOSForT5_0_3()

        if (nitoriPOS) {
            setPOSProperty('NeedReturnAdjust', 'no')
        }
    }

    static private checkPOSForT5_0_5() {
        checkPOSForT5_0_4()

        if (nitoriPOS) {
            if (!rowExists("select * from si where sitype='4'")) {
                execute('HealthyChecker.checkForT5_0_0(): manually input discount percentage SI is inserted.', true,
                """delete from si where siid='04';
                insert into si (siid, sicnames, sinamep, sino, catno, sitype, dvalue, dbase, attr, bdate, edate, btime, etime, cycle)
                values ('04','手輸折扣%','手輸折扣%','00000000','00',4,0.00,'R','R010000A','2000-01-01','2099-12-31',
                '00:00:00','23:59:59','1111111');""")
            }
            if (!rowExists("select * from si where sitype='6'")) {
                execute('HealthyChecker.checkForT5_0_0(): manually input discount amount SI is inserted.', true,
                """delete from si where siid='05';
                insert into si (siid, sicnames, sinamep, sino, catno, sitype, dvalue, dbase, attr, bdate, edate, btime, etime, cycle)
                values ('05','手輸折讓','手輸折讓','00000000','00',6,0.00,'R','R010000A','2000-01-01','2099-12-31',
                '00:00:00','23:59:59','1111111');""")
            }
        }
    }

    static private checkPOSForT5_0_6() {
        checkPOSForT5_0_5()

        if (nitoriPOS && !rowExists("SELECT * FROM payment WHERE payid='08'")) {
            execute('HealthyChecker.checkForT5_0_6(): Consumer voucher payment is inserted.', true, """
            INSERT INTO payment (payid, payname, paycname, paytype, payrate, denomination, paycategory, priority, seqno)
            VALUES ('08','消費券','消費券','10100000','0',1.00,'',0,'95');""")
        }

        // 將目前所有使用這個版本的最大交易序號置頂
        Param.instance.maxTransactionNumber = '99999999'
    }

    static private checkPOSForT5_0_7() {
        checkPOSForT5_0_6()
    }

    static private checkPOSForT5_0_8() {
        checkPOSForT5_0_7()
    }

    static private checkPOSForT5_1_0() {
        checkPOSForT5_0_8()

        if (tk3cPOS) {
            // plu
            execute('HealthyChecker.checkPOSForT5_1_0(): plu.specification is created.', false,
                'ALTER TABLE plu ADD COLUMN specification character varying(26)')
            execute('HealthyChecker.checkPOSForT5_1_0(): plu.minPrice is created.', false,
                'ALTER TABLE plu ADD COLUMN minPrice numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): plu.yellowLimit is created.', false,
                'ALTER TABLE plu ADD COLUMN yellowLimit numeric(12,2)')

            // tranhead
            execute('HealthyChecker.checkPOSForT5_1_0(): tranhead.authorno is created.', false,
                'ALTER TABLE tranhead ADD COLUMN authorno character varying(10)')
            execute('HealthyChecker.checkPOSForT5_1_0(): tranhead.totalRebateAmount is created.', false,
                'ALTER TABLE tranhead ADD COLUMN totalRebateAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): tranhead.rebateAmount is created.', false,
                'ALTER TABLE tranhead ADD COLUMN rebateAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): tranhead.rebateChangeAmount is created.', false,
                'ALTER TABLE tranhead ADD COLUMN rebateChangeAmount numeric(12,2)')

            // tranheadhold
            execute('HealthyChecker.checkPOSForT5_1_0(): tranheadhold.authorno is created.', false,
                'ALTER TABLE tranheadhold ADD COLUMN authorno character varying(10)')
            execute('HealthyChecker.checkPOSForT5_1_0(): tranheadhold.totalRebateAmount is created.', false,
                'ALTER TABLE tranheadhold ADD COLUMN totalRebateAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): tranheadhold.rebateAmount is created.', false,
                'ALTER TABLE tranheadhold ADD COLUMN rebateAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): tranheadhold.rebateChangeAmount is created.', false,
                'ALTER TABLE tranheadhold ADD COLUMN rebateChangeAmount numeric(12,2)')

            // trandetail
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetail.discountRateID is created.', false,
                'ALTER TABLE trandetail ADD COLUMN discountRateID character varying(6)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetail.discountRate is created.', false,
                'ALTER TABLE trandetail ADD COLUMN discountRate numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetail.rebateAmount is created.', false,
                'ALTER TABLE trandetail ADD COLUMN rebateAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetail.afterRebateAmount is created.', false,
                'ALTER TABLE trandetail ADD COLUMN afterRebateAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetail.addRebateAmount is created.', false,
                'ALTER TABLE trandetail ADD COLUMN addRebateAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetail.yellowAmount is created.', false,
                'ALTER TABLE trandetail ADD COLUMN yellowAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetail.diyIndex is created.', false,
                'ALTER TABLE trandetail ADD COLUMN diyIndex character varying(3)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetail.dtlcode1 is created.', false,
                'ALTER TABLE trandetail ADD COLUMN dtlcode1 character varying(20)')

            // trandetailhold
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetailhold.discountRateID is created.', false,
                'ALTER TABLE trandetailhold ADD COLUMN discountRateID character varying(6)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetailhold.discountRate is created.', false,
                'ALTER TABLE trandetailhold ADD COLUMN discountRate numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetailhold.rebateAmount is created.', false,
                'ALTER TABLE trandetailhold ADD COLUMN rebateAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetailhold.afterRebateAmount is created.', false,
                'ALTER TABLE trandetailhold ADD COLUMN afterRebateAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetailhold.addRebateAmount is created.', false,
                'ALTER TABLE trandetailhold ADD COLUMN addRebateAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetailhold.yellowAmount is created.', false,
                'ALTER TABLE trandetailhold ADD COLUMN yellowAmount numeric(12,2)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetailhold.diyIndex is created.', false,
                'ALTER TABLE trandetailhold ADD COLUMN diyIndex character varying(3)')
            execute('HealthyChecker.checkPOSForT5_1_0(): trandetailhold.dtlcode1 is created.', false,
                'ALTER TABLE trandetailhold ADD COLUMN dtlcode1 character varying(20)')

            if (!tableExists('rebate')) {
                execute('HealthyChecker.checkPOSForT5_1_0(): rebate is created.', true, """
                CREATE TABLE rebate (
                  storeID varchar(6) NOT NULL default '', -- 店號
                  itemNumber varchar(13) NOT NULL default '',  -- 商品品號
                  beginDate date NOT NULL default '1970-01-01',  -- 銷售開始日
                  endDate date NOT NULL default '1970-01-01',  -- 銷售結束日
                  rebateRate decimal(6,2) NOT NULL default '0.00',  -- 還圓金比率
                  discountBeginDate date NOT NULL default '1970-01-01',  -- 促銷開始日
                  discountEndDate date NOT NULL default '1970-01-01',  -- 促銷結束日
                  discountRebateRate decimal(6,2) NOT NULL default '0.00',  -- 促銷還圓金比率
                  CONSTRAINT rebate_pkey PRIMARY KEY  (storeID,itemNumber,beginDate,discountBeginDate)
                );
                ALTER TABLE rebate OWNER TO postgres;
                COMMENT ON COLUMN rebate.storeID IS '店號';
                COMMENT ON COLUMN rebate.itemNumber IS '商品品號';
                COMMENT ON COLUMN rebate.beginDate IS '銷售開始日';
                COMMENT ON COLUMN rebate.endDate IS '銷售結束日';
                COMMENT ON COLUMN rebate.rebateRate IS '還圓金比率';
                COMMENT ON COLUMN rebate.discountBeginDate IS '促銷開始日';
                COMMENT ON COLUMN rebate.discountEndDate IS '促銷結束日';
                COMMENT ON COLUMN rebate.discountRebateRate IS '促銷還圓金比率';
                """)
            }
        }
    }

    static private checkPOSForT5_1_1() {
        checkPOSForT5_1_0()

        if (!tableExists('ssmp_log')) {
            execute('HealthyChecker.checkForT5_1_1(): ssmp_log is created.', true, """
            CREATE TABLE ssmp_log
            (
              id serial NOT NULL, -- Unique ID
              sc_id bigint, -- Unique ID on SC
              store_id varchar(8) NOT NULL, -- '门市代号'
              log_time timestamp without time zone NOT NULL, -- '日期时间'
              log_level char(1) NOT NULL, -- 'I': 信息, 'W': 警告, 'E': 错误'
              severity char(1), -- 1 ~ 5: 轻微 ~ 致命 (type=''I'' 时可以为NULL)
              source varchar(3) NOT NULL, -- 'SC': SC, 'EC': eComm, '001': POS1, '002': POS2, ...
              cat char(2) NOT NULL, -- '事件分类',
              midcat char(4) NOT NULL, -- 'SA': 销售, 'OR': 订货, 'DL': 验收, 'PD': 配送传票, ...
              code smallint, -- 错误代码 (type='I' 时可以为NULL)
              brief varchar(64) NOT NULL, -- '简要说明'
              detail varchar(512), -- '详细描述或程序Exception log'
              CONSTRAINT pk_ssmp_log PRIMARY KEY (id)
            );
            ALTER TABLE ssmp_log OWNER TO postgres;
            COMMENT ON COLUMN ssmp_log.id IS 'Unique ID';
            COMMENT ON COLUMN ssmp_log.sc_id IS 'Unique ID on SC';
            COMMENT ON COLUMN ssmp_log.store_id IS '门市代号';
            COMMENT ON COLUMN ssmp_log.log_time IS '日期时间';
            COMMENT ON COLUMN ssmp_log.log_level IS 'I: 信息、W: 警告、E: 错误';
            COMMENT ON COLUMN ssmp_log.severity IS '1 ~ 5: 轻微 ~ 致命 (type="I" 时可以为NULL)';
            COMMENT ON COLUMN ssmp_log.source IS 'SC: SC、EC: eComm、001: POS1、002: POS2, ...';
            COMMENT ON COLUMN ssmp_log.cat IS '事件分类';
            COMMENT ON COLUMN ssmp_log.midcat IS 'SA: 销售、OR: 订货、DL: 验收、PD: 配送传票, ...';
            COMMENT ON COLUMN ssmp_log.code IS '错误代码 (type="I" 时可以为NULL)';
            COMMENT ON COLUMN ssmp_log.brief IS '简要说明';
            COMMENT ON COLUMN ssmp_log.detail IS '详细描述或程序Exception log';
            CREATE INDEX idx_sc_id ON ssmp_log USING btree (sc_id);
            """)
        }
        if (!tableExists('master_version')) {
            execute('HealthyChecker.checkForT5_1_1(): master_version is created.', true, """
            CREATE TABLE master_version
            (
              storeID character varying(8) NOT NULL, -- 门市代号
              masterName character varying(64) NOT NULL, -- 主档名称
              masterVersion character varying(20) DEFAULT NULL, -- 主档版本
              CONSTRAINT pk_master_version PRIMARY KEY (masterName)
            );
            ALTER TABLE master_version OWNER TO postgres;
            COMMENT ON COLUMN master_version.storeID IS '门市代号';
            COMMENT ON COLUMN master_version.masterName IS '主档名称';
            COMMENT ON COLUMN master_version.masterVersion IS '主档版本';
            """)
        }

        //execute('HealthyChecker.checkPOSForT5_1_1(): property.value length is altered.', false,
        //    'ALTER TABLE property ALTER "value" TYPE character varying(512)')
    }

    static private checkPOSForT5_1_2() {
        checkPOSForT5_1_1()
    }

    static private checkPOSForT5_1_3() {
        checkPOSForT5_1_2()

        if (nitoriPOS) {
            def param = Param.instance
            setPOSProperty('InputMemberCardIDInTheMiddleOfTransaction', 'yes')
            setPOSProperty('GenerateSSMPLog', 'no')
            setPOSProperty('QueryMemberFromPos', 'yes')
            setPOSProperty('QueryMemberInfo', 'no')
            setPOSProperty('MemberUseRegularPrice', 'yes')
            setPOSProperty('MemberCardIDPrefix', '')
            //setPOSProperty('OverrideAmountRangeLimit', '0.91')
            param.@inputMemberCardIDInTheMiddleOfTransaction = true
            param.@generateSsmpLog = false
            param.@queryMemberFromPos = true
            param.@queryMemberInfo = false
            param.@memberUseRegularPrice = true
            param.@memberCardIDPrefix = ''
            //param.@overrideAmountRangeLimit = '0.91'
        }
        //execute('HealthyChecker.checkPOSForT5_1_3(): tranhead.memberid length is altered.', false,
        //    'ALTER TABLE tranhead ALTER "memberid" TYPE character varying(30)')
    }

    static private checkPOSForT5_1_4() {
        checkPOSForT5_1_3()
    }

    static private checkPOSForT5_1_5() {
        checkPOSForT5_1_4()
    }

    static private checkPOSForT5_1_6() {
        checkPOSForT5_1_5()
    }

    static private checkPOSForT5_1_7() {
        checkPOSForT5_1_6()
    }

    static private checkPOSForT5_1_8() {
        checkPOSForT5_1_7()
    }

    static private checkPOSForT5_1_9() {
        checkPOSForT5_1_8()
    }

    static private checkPOSForT5_2_0() {
        checkPOSForT5_1_9()
    }

    static private checkPOSForT5_2_1() {
        checkPOSForT5_2_0()

        //Bruce/20121017/ /*增加*/教超捨零去分设置
        def param = Param.instance
        param.roundDown = false
        //param.payingpane1 = /fontSizeH=18,fontNameH=SimHei,fontSizeV=20,fontNameV=SimHei\n客层,CustomerAgeLevel,y\n总计,GrossSalesAmount,y\n代售金额,DaiShouAmount,n\n代收金额,DaiShouAmount2,n\n代付金额,DaiFuAmount,n\nPAIDIN,PaidInAmount,n\nPAIDOUT,PaidOutAmount,n\n组合促销,DisplayTotalMMAmount,n\n黄卡折扣,YellowCardDiscountAmount,n\nSI折扣,SI,n\nDIY折扣,TotalDIYOverrideAmount,n\n捨零去分,RoundDownAmount,n\n未付金额,Balance,y/
        //param.roundItemCategory = '100804'
    }

    static private checkPOSForT5_2_2() {
        checkPOSForT5_2_1()
         if (!tableExists('attendance')) {
            execute('HealthyChecker.checkForT5_2_2(): attendance is created.', true, """
                CREATE TABLE attendance
                (
                    id serial not null,
                    zNumber integer default -1,
                    storeID character varying(6) NOT NULL,
                    posNumber integer not null default 0,
                    busiDate DATE NOT NULL DEFAULT '1970-01-01'::date,
                    dayDate DATE NOT NULL DEFAULT (now())::DATE,
                    employeeID character varying(20) NOT NULL ,
                    attType character(2) NOT NULL DEFAULT '00'::bpchar,
                    beginTime timestamp without time zone,
                    endTime  timestamp without time zone,
                    createUserID character varying(20),
                    createDate timestamp without time zone DEFAULT (now())::timestamp without time zone,
                    TCPFLAG integer NOT NULL DEFAULT 0,
                    primary key(id,storeID,posNumber,busiDate,dayDate,employeeID,attType)
                );
            """)
         }
    }

    static private checkPOSForT5_2_3() {
        checkPOSForT5_2_2()
    }

    static private checkPOSForT5_2_4() {
        checkPOSForT5_2_3()
    }

    static private checkPOSForT5_2_5() {
        checkPOSForT5_2_4()
        if (!tableExists('alipay_detail')) {
            execute('HealthyChecker.checkForT5_2_5(): alipay_detail is created.', true, """
                CREATE TABLE alipay_detail (
                  storeID varchar(6) NOT NULL DEFAULT '' ,
                  posNumber integer NOT NULL DEFAULT 0,
                  transactionNumber integer NOT NULL DEFAULT 0 ,
                  zseq int not null default 0,
                  TOTAL_FEE decimal(12,2) DEFAULT NULL ,
                  TRADE_NO varchar(64) DEFAULT NULL ,
                  systemDate timestamp DEFAULT NULL ,
                  SEQ varchar(2) DEFAULT NULL  ,
                  BUYER_LOGON_ID varchar(100) DEFAULT NULL ,
                  PRIMARY KEY(storeID,posNumber,transactionNumber)
                );
            """)
        }
        if (!rowExists("select * from property where name='AlipayURL'")) {
            execute("HealthyChecker: Property AlipayURL is inserted.", true,
                    "INSERT INTO property (name,value) VALUES ('AlipayURL','https://mapi.alipay.com/gateway.do?')")
        }
        if (!fieldExists("store","alipay_partner_id")) {
            execute("HealthyChecker: store alipay_partner_id is add.", true,
                    "ALTER TABLE store ADD COLUMN alipay_partner_id varchar(20)")
        }
         if (!fieldExists("store", "alipay_security_code")) {
            execute("HealthyChecker: store alipay_security_code is add.", true,
                    "ALTER TABLE store ADD COLUMN alipay_security_code varchar(40)")
        }
    }

    static private checkPOSForT5_2_6(){
        checkPOSForT5_2_5();
    }

    static private checkPOSForT5_2_7() {
        checkPOSForT5_2_6();
        if (!nitoriPOS) {
            if (!rowExists("select * from property where name='ReprintGenerateVoidTransaction'")) {
                execute("HealthyChecker: Property ReprintGenerateVoidTransaction is inserted.", true,
                        "INSERT INTO property (name,value) VALUES ('ReprintGenerateVoidTransaction','false')")
            }
        }
    }

    static private checkPOSForT5_2_8(){
        checkPOSForT5_2_7();
        if (!nitoriPOS) {
            if (!rowExists("select * from property where name='SecondScreenMarquee'")) {
                execute("HealthyChecker: Property SecondScreenMarquee is inserted.", true,
                        "INSERT INTO property (name,value) VALUES ('SecondScreenMarquee','welcome')")
            }
        }
    }
    static private checkPOSForT5_3_0() {
        checkPOSForT5_2_9()
        if (!tableExists('cmpay_detail')) {
            execute('HealthyChecker.checkForT5_2_9(): cmpay_detail is created.', true, """
                CREATE TABLE cmpay_detail (
                  storeID varchar(6) NOT NULL DEFAULT '' ,
                  posNumber integer NOT NULL DEFAULT 0,
                  transactionNumber integer NOT NULL DEFAULT 0 ,
                  zseq integer NOT NULL DEFAULT 0 ,
                  orderId varchar(64) DEFAULT NULL ,
                  mid varchar(64) DEFAULT NULL ,
                  amt decimal(12,2) DEFAULT NULL ,
                  coupamt decimal(12,2) DEFAULT NULL ,
                  vchamt decimal(12,2) DEFAULT NULL ,
                  cashamt decimal(12,2) DEFAULT NULL ,
                  systemDate timestamp DEFAULT NULL ,
                  phonenumber varchar(50) DEFAULT NULL ,
                  mark varchar(50) DEFAULT NULL ,
                  PRIMARY KEY(storeID,posNumber,transactionNumber)
                );
            """)
        }
        if (!rowExists("select * from property where name='CMPAYAddress'")) {
            execute("HealthyChecker: Property CMPAYAddress is inserted.", true,
                    "INSERT INTO property (name,value) VALUES ('CMPAYAddress','http://211.136.101.107:7008/CMPay/CMPAYProcess')")
        }
        if (!rowExists("select * from property where name='CMPAYMerId'")) {
            execute("HealthyChecker: Property CMPAYMerId is inserted.", true,
                    "INSERT INTO property (name,value) VALUES ('CMPAYMerId','888000959990013')")
        }

    }
    static private checkPOSForT5_3_1() {
        checkPOSForT5_3_0()
        if (!rowExists("select * from property where name='CMPAYPIKFLG'")) {
            execute("HealthyChecker: Property CMPAYPIKFLG is inserted.", true,
                    "INSERT INTO property (name,value) VALUES ('CMPAYPIKFLG','1')")
        }
    }

    static private checkPOSForT5_2_9() {
        checkPOSForT5_2_8();
        if (CreamToolkit.getOsName().contains("Linux")) {
            // Add a parameter "stats_start_collector = on" for PostgreSQL
            ['/bin/sed', '-i.old', 's/\\#stats_start_collector/stats_start_collector/', '/etc/postgresql/8.1/main/postgresql.conf']
                    .execute().waitFor()
        }

        if (nitoriPOS) {
            // Change the class of PLU to PluInPostgreSQL for faster transfer speed
            ['/bin/sed', '-i.old', 's/hyi.cream.dac.PLU/hyi.cream.dac.PluInPostgreSQL/', '/home/hyi/cream/conf/downloadlist.conf']
                .execute().waitFor()
        }
    }

    static private checkPOSForT5_3_2() {
        checkPOSForT5_3_1()
        if (!rowExists("select * from property where name='DailyLimit'")) {
            execute("HealthyChecker: Property DailyLimit is inserted.", true,
                    "INSERT INTO property (name,value) VALUES ('DailyLimit','2')")
        }
        if (!rowExists("select * from property where name='ImmediateRemove'")) {
            execute("HealthyChecker: Property ImmediateRemove is inserted.", true,
                    "INSERT INTO property (name,value) VALUES ('ImmediateRemove','no')")
        }
    }

    static private checkPOSForT5_3_3() {
        checkPOSForT5_3_2()
    }

    static private checkPOSForT5_3_4() {
        checkPOSForT5_3_3()
    }
    static private checkPOSForT5_3_5() {
        checkPOSForT5_3_4()
    }

    static private checkPOSForT5_3_6() {
        checkPOSForT5_3_5()
        if (!rowExists("select * from property where name='PrintAreaNumber'")) {
            execute("HealthyChecker: Property PrintAreaNumber is inserted.", true,
                    "INSERT INTO property (name,value) VALUES ('PrintAreaNumber','true')")
        }
    }
    static private checkPOSForT5_3_6_1() {
        checkPOSForT5_3_6()
    }
    static private checkPOSForT5_3_6_2() {
        checkPOSForT5_3_6_1()
    }
    static private checkPOSForT5_3_6_3() {
        checkPOSForT5_3_6_2()
    }
    static private checkPOSForT5_3_7() {
        checkPOSForT5_3_6_3()
    }

     static private checkPOSForT5_3_8() {
         checkPOSForT5_3_7()
         if (!tableExists('weixin_detail')) {
             execute('HealthyChecker.checkForT5_3_8(): weixin_detail is created.', true, """
                CREATE TABLE weixin_detail (
                    storeID varchar(6)   NOT NULL,
                    POSNUMBER integer  DEFAULT 0 NOT NULL,
                    TRANSACTIONNUMBER integer DEFAULT 0 NOT NULL,
                    ZSEQ integer DEFAULT 0 NOT NULL,
                    TOTAL_FEE decimal(12,2) DEFAULT 0,
                    TRANSACTION_ID varchar(32),
                    systemdate timestamp,
                    seq varchar(2),
                    REFUND_ID varchar(32),
                    OPENID varchar(128),
                    COUPON_FEE decimal(12,2) DEFAULT 0,
                    NON_COUPOND_FEE decimal(12,2) DEFAULT 0,
                    CONSTRAINT "weixin_detail_pkey" PRIMARY KEY (storeID, posnumber, transactionnumber)
                    )
            """)
         }
         if (!rowExists("select * from property where name='WeiXinDeviceInfo'")) {
             execute("HealthyChecker: Property WeiXinDeviceInfo is inserted.", true,
                     "insert into property values('WeiXinDeviceInfo','123456')")
         }
         if (!rowExists("select * from property where name='WeiXinUrl'")) {
             execute("HealthyChecker: Property WeiXinUrl is inserted.", true,
                     "insert into property values('WeiXinUrl','https://api.mch.weixin.qq.com/')")
         }
         if (!fieldExists("store","weixinAppId")) {
             execute("HealthyChecker: store weixinAppId is add.", true,
                     "alter table store add column weixinAppId varchar(32)")
         }
         if (!fieldExists("store", "weixinMchId")) {
             execute("HealthyChecker: store weixinMchId is add.", true,
                     "alter table store add column weixinMchId varchar(32)")
         }
         if (!fieldExists("store", "weixinPartnerKey")) {
             execute("HealthyChecker: store weixinPartnerKey is add.", true,
                     "alter table store add column weixinPartnerKey varchar(32)")
         }
         if (!fieldExists("store", "weiXinKeyStore")) {
             execute("HealthyChecker: store weiXinKeyStore is add.", true,
                     "alter table store add column weiXinKeyStore varchar(32)")
         }
         if (!fieldExists("store", "weiXinKeyPassword")) {
             execute("HealthyChecker: store weiXinKeyPassword is add.", true,
                     "alter table store add column weiXinKeyPassword varchar(32)")
         }




    }
    static private checkPOSForT5_3_9() {
        checkPOSForT5_3_8()
    }
    static private checkPOSForT5_4_0() {
        checkPOSForT5_3_9()
    }

    static private checkPOSForT5_4_1() {
        checkPOSForT5_4_0()
    }

    static private checkPOSForT5_4_2() {
        checkPOSForT5_4_1()
    }

    static private checkPOSForT5_4_3() {
        checkPOSForT5_4_2()
    }
    static private checkPOSForT5_4_4() {
        checkPOSForT5_4_3()
    }
    static private checkPOSForT5_4_5() {
        checkPOSForT5_4_4()
    }
    static private checkPOSForT5_4_6() {
        checkPOSForT5_4_4()
    }
    static private checkPOSForT5_4_7() {
        checkPOSForT5_4_4()
    }
    static private checkPOSForT5_4_8() {
        setPOSProperty('MixAndMatchVersion', '3')
    }
    static private checkPOSForT5_4_9() {
        checkPOSForT5_4_8()
    }

    static private checkPOSForT5_5_0() {
        checkPOSForT5_4_8()
    }
    static private checkPOSForT5_5_1() {
        checkPOSForT5_4_8()
    }
    static private checkPOSForT5_5_2() {
        checkPOSForT5_4_8()
    }
    static private checkPOSForT5_5_3() {
        checkPOSForT5_5_2()
        if (!fieldExists("store", "selfbuyShopid")) {
            execute("HealthyChecker: store selfbuyShopid is add.", true,
                    "alter table store add column selfbuyShopid varchar(40)")
        }
    }
    static private checkPOSForT5_5_4(){
        checkPOSForT5_5_3()
    }
    static private checkPOSForT5_5_5(){
        checkPOSForT5_5_4()
        if (!fieldExists("weixin_detail", "promotion_id")) {
            execute("HealthyChecker: weixin_detail promotion_id is add.", true,
                    "alter table weixin_detail add column promotion_id varchar(32)")
        }
    }

    // Server-side ////////////////////////////////////////////////////////////////////////////////

    static private checkServerForT5_0_0() {
        if (!tableExists('posul_carddetail')) {
            // 不知為什麼MySQL的comment進去後會變亂碼，先拿掉
            execute('HealthyChecker.checkServerForT5_0_0(): posul_carddetail is created.', true, """
            CREATE TABLE posul_carddetail
            (
              id integer NOT NULL auto_increment, -- COMMENT 'Unique ID',
              storeID varchar(6) NOT NULL, -- COMMENT '門市代號',
              accountDate date NOT NULL, -- COMMENT '營業日期',
              posNumber smallint NOT NULL, -- COMMENT 'POS機號',
              transactionNumber integer NOT NULL, -- COMMENT '交易序號',
              zSequenceNumber integer NOT NULL, -- COMMENT 'Z帳序號',
              isVoid char(1), -- COMMENT '是否被作廢（取消/退貨/調整）（0:沒有被作廢、1:被作廢）',
              voidTransactionNumber integer NOT NULL, -- COMMENT '若此筆為被作廢交易，記錄作廢此筆交易的相應取消/退貨/調整的交易序號；若此筆為作廢交易（取消/退貨/調整），則記錄被作廢的交易序號',
              transDateTime datetime NOT NULL, -- COMMENT '交易日期與時間',
              cardNo char(19) NOT NULL, -- COMMENT '卡號',
              cardExpDate char(4) NOT NULL, -- COMMENT '有效期',
              approvalCode char(9) NOT NULL, -- COMMENT '授權碼',
              terminalId char(8) NOT NULL, -- COMMENT '端末機編號',
              transAmt numeric(12,2) NOT NULL, -- COMMENT '總交易金額',
              source char(1) NOT NULL, -- 數據來源 ('C': CAT數據、'P': 收銀機輸入數據、'L': 缺乏明細數據、'S': SC補登數據)
              updateDate datetime NOT NULL, -- 最後修改日期
              updateUserId varchar(8), -- 數據補登人員代號
              CONSTRAINT pk_posul_carddetail PRIMARY KEY (id),
              CONSTRAINT idx_posnumber_transactionnumber UNIQUE (posnumber, transactionnumber, transDateTime),
              KEY idx_storeid_posno_zno (storeID, posNumber, zSequenceNumber)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            """)
        }
        if (!tableExists('posul_carddetail_chinatrust')) {
            execute('HealthyChecker.checkServerForT5_0_0(): posul_carddetail_chinatrust is created.', true, """
            CREATE TABLE posul_carddetail_chinatrust
            (
              id integer NOT NULL auto_increment, -- COMMENT 'Unique ID',
              carddetailId integer, -- COMMENT 'FK to posul_carddetail.id',
              posNumber smallint NOT NULL, -- COMMENT 'POS機號',
              storeID varchar(6) NOT NULL, -- COMMENT '門市代號',
              accountDate date NOT NULL, -- COMMENT '營業日期',
              zSequenceNumber integer NOT NULL, -- COMMENT 'Z帳序號',
              transType char(2), -- COMMENT '交易類別（01:SALE銷售、02:REFUND退貨、03:OFFLINE離線、04:PRE_AUTH預取授權、30:VOID取消、40:TIP小費、41:ADJUST調整、50:SETTLEMENT調整、98:ECHO測試）',
              respCode char(4), -- COMMENT '回覆代碼',
              hostId char(2), -- COMMENT '授權銀行編碼',
              invoiceNo char(6), -- COMMENT '調閱編號',
              refNo char(12), -- COMMENT '序號',
              bonusPaid numeric(12,2), -- COMMENT '紅利抵用的實付金額',
              bonusDiscount numeric(12,2), -- COMMENT '紅利抵用的折抵金額',
              installmentPeriod smallint, -- COMMENT '期數',
              installmentFee numeric(12,2), -- COMMENT '手續費',
              installmentType char(1), -- COMMENT '收費方式（1:一般分期、2:收費分期）',
              installmentPutOffType char(1), -- COMMENT '延後付款方式（1:無延後付款、2:延後付款、3:彈性付款）',
              installmentAmount1 numeric(12,2), -- COMMENT '首期金額（若為彈性付款，此欄位為後期的首期金額）',
              installmentAmount2 numeric(12,2), -- COMMENT '每期金額（若為彈性付款，此欄位為後期的每期金額）',
              installmentAmount3 numeric(12,2), -- COMMENT '彈性付款前期的每期金額',
              rawData char(144), -- COMMENT 'CAT機返回的原始數據（目前中國信託的應該是144 bytes）',
              CONSTRAINT pk_posul_carddetail_chinatrust PRIMARY KEY (id),
              CONSTRAINT fk_posul_carddetail_chinatrust_posul_carddetail_id FOREIGN KEY (carddetailId)
                  REFERENCES posul_carddetail (id) MATCH SIMPLE
                  ON UPDATE NO ACTION ON DELETE NO ACTION,
              KEY idx_storeid_posno_zno (storeID, posNumber, zSequenceNumber)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            """)
        }
        if (!tableExists('commul_carddetail')) {
            execute('HealthyChecker.checkServerForT5_0_0(): commul_carddetail is created.', true, """
            CREATE TABLE commul_carddetail
            (
              updateBeginDate date NOT NULL default '1970-01-01',
              sequenceNumber tinyint(3) unsigned NOT NULL default '0',
              id integer NOT NULL, -- COMMENT 'Unique ID',
              storeID varchar(6) NOT NULL, -- COMMENT '門市代號',
              posNumber smallint NOT NULL, -- COMMENT 'POS機號',
              transactionNumber integer NOT NULL, -- COMMENT '交易序號',
              zSequenceNumber integer NOT NULL, -- COMMENT 'Z帳序號',
              isVoid char(1), -- COMMENT '是否被作廢（取消/退貨/調整）（0:沒有被作廢、1:被作廢）',
              voidTransactionNumber integer NOT NULL, -- COMMENT '若此筆為被作廢交易，記錄作廢此筆交易的相應取消/退貨/調整的交易序號；若此筆為作廢交易（取消/退貨/調整），則記錄被作廢的交易序號',
              transDateTime datetime NOT NULL, -- COMMENT '交易日期與時間',
              cardNo char(19) NOT NULL, -- COMMENT '卡號',
              cardExpDate char(4) NOT NULL, -- COMMENT '有效期',
              approvalCode char(9) NOT NULL, -- COMMENT '授權碼',
              terminalId char(8) NOT NULL, -- COMMENT '端末機編號',
              transAmt numeric(12,2) NOT NULL, -- COMMENT '總交易金額',
              source char(1) NOT NULL, -- 數據來源 ('C': CAT數據、'P': 收銀機輸入數據、'L': 缺乏明細數據、'S': SC補登數據)
              updateDate datetime NOT NULL, -- 最後修改日期
              updateUserId varchar(8), -- 數據補登人員代號
              CONSTRAINT pk_commul_carddetail PRIMARY KEY (updateBeginDate, sequenceNumber, storeID, id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            """)
        }
        if (!tableExists('commul_carddetail_chinatrust')) {
            execute('HealthyChecker.checkServerForT5_0_0(): commul_carddetail_chinatrust is created.', true, """
            CREATE TABLE commul_carddetail_chinatrust
            (
              updateBeginDate date NOT NULL default '1970-01-01',
              sequenceNumber tinyint(3) unsigned NOT NULL default '0',
              id integer NOT NULL, -- COMMENT 'Unique ID',
              carddetailId integer, -- COMMENT 'FK to posul_carddetail.id',
              posNumber smallint NOT NULL, -- COMMENT 'POS機號',
              storeID varchar(6) NOT NULL, -- COMMENT '門市代號',
              zSequenceNumber integer NOT NULL, -- COMMENT 'Z帳序號',
              transType char(2), -- COMMENT '交易類別（01:SALE銷售、02:REFUND退貨、03:OFFLINE離線、04:PRE_AUTH預取授權、30:VOID取消、40:TIP小費、41:ADJUST調整、50:SETTLEMENT調整、98:ECHO測試）',
              respCode char(4), -- COMMENT '回覆代碼',
              hostId char(2), -- COMMENT '授權銀行編碼',
              invoiceNo char(6), -- COMMENT '調閱編號',
              refNo char(12), -- COMMENT '序號',
              bonusPaid numeric(12,2), -- COMMENT '紅利抵用的實付金額',
              bonusDiscount numeric(12,2), -- COMMENT '紅利抵用的折抵金額',
              installmentPeriod smallint(6), -- COMMENT '期數',
              installmentFee numeric(12,2), -- COMMENT '手續費',
              installmentType char(1), -- COMMENT '收費方式（1:一般分期、2:收費分期）',
              installmentPutOffType char(1), -- COMMENT '延後付款方式（1:無延後付款、2:延後付款、3:彈性付款）',
              installmentAmount1 numeric(12,2), -- COMMENT '首期金額（若為彈性付款，此欄位為後期的首期金額）',
              installmentAmount2 numeric(12,2), -- COMMENT '每期金額（若為彈性付款，此欄位為後期的每期金額）',
              installmentAmount3 numeric(12,2), -- COMMENT '彈性付款前期的每期金額',
              rawData char(144), -- COMMENT 'CAT機返回的原始數據（目前中國信託的應該是144 bytes）',
              CONSTRAINT pk_commul_carddetail_chinatrust PRIMARY KEY (updateBeginDate, sequenceNumber, id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            """)
        }
        /*
        if (!rowExists("select * from posdl_si where sitype='5'")) {
            sql.execute("""
                insert into posdl_si (storeID, siID, siScreenName, siPrintName, siGroupNumber, siType, discountValue, discountBase,
                 discountAttribute, beginDate, endDate, beginTime, endTime, weekCycle) values
                 ('05','紅利折抵','紅利折抵','00000000',5,0,'R','R010000A','2000-01-01','2099-12-31','00:00:00','23:59:59','1111111')""")
        }
        */
        execute('HealthyChecker.checkServerForT5_0_0(): idx1_deliveryhead is created.', false,
            "ALTER TABLE `deliveryhead` ADD INDEX idx1_deliveryhead(`storeID`, `deliveryNo`)")
        execute('HealthyChecker.checkServerForT5_0_0(): index zseq_posno is created.', false,
            'ALTER TABLE posul_tranhead ADD INDEX zseq_posno (zSequenceNumber, posNumber)')
        execute('HealthyChecker.checkServerForT5_0_0(): index accdate_posno_zseq is created.', false,
            'ALTER TABLE posul_tranhead ADD INDEX accdate_posno_zseq (accountDate,storeID,posNumber,zSequenceNumber)')

        if (ServerThread.getPostProcessor() instanceof NitoriPostProcessor)
            'checkServerForNitoriT5_0_0'()
    }

    static private checkServerForNitoriT5_0_0() {
        def tableName = 'item_site_planning'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_0_0(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
                storeID varchar(6) NOT NULL DEFAULT '',
                siteId varchar(6) NOT NULL,
                itemNumber varchar(20) NOT NULL,
                location varchar(8) DEFAULT NULL,
                status varchar(1) NOT NULL DEFAULT '0',
                PRIMARY KEY (storeID,siteId,itemNumber)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;""")
        }
        tableName = 'site'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_0_0(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
                storeID varchar(6) NOT NULL DEFAULT '',
                siteId varchar(6) NOT NULL,
                siteName varchar(24) DEFAULT NULL,
                siteType varchar(40) DEFAULT NULL,
                address varchar(84) DEFAULT NULL,
                telphoneNumber varchar(26) DEFAULT NULL,
                status varchar(1) NOT NULL DEFAULT '0',
                PRIMARY KEY (storeID,siteId)
            ) ENGINE=InnoDB DEFAULT charset=utf8;""")
        }
        tableName = 'zip_site_map'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_0_0(): ${tableName} is created.", true, """
            create table ${tableName} (
                storeID varchar(6) NOT NULL,
                siteId varchar(6) NOT NULL,
                zipCode varchar(8) NOT NULL,
                city varchar(20) DEFAULT NULL,
                cityArea varchar(20) DEFAULT NULL,
                status varchar(1) NOT NULL DEFAULT '0',
                PRIMARY KEY (storeID,siteId,zipCode)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;""")
        }
        tableName = 'price_card_master'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_0_0(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
                ITEM_CD  varchar(10) NOT NULL,
                ITEM_DEPT varchar(2) NOT NULL,
                ITEM_CLASS varchar(2) NOT NULL,
                ITEM_ORDER_TYPE varchar(2) NOT NULL,
                SUPPLIER_CD varchar(4) NOT NULL,
                DELIV_FLG varchar(1) NOT NULL,
                ORDER_UNIT decimal(5,0) NOT NULL,
                SET_FLG varchar(1) NOT NULL,
                DISP_WIDTH decimal(3,0) NOT NULL,
                DISP_DEPTH decimal(3,0) NOT NULL,
                DISP_HEIGHT decimal(3,0) NOT NULL,
                ITEMNAME1 varchar(40) ,
                ITEMNAME VARCHAR(48),
                COLOR VARCHAR(24),
                MATERIAL VARCHAR(24),
                ITEMNAME2 varchar(40),
                COLOR1 VARCHAR(24),
                OLDSELLPRICE decimal(8,0) NOT NULL,
                NEWSELLPRICE decimal(8,0) NOT NULL,
                PRIMARY KEY(ITEM_CD)
            ) ENGINE=InnoDB DEFAULT charset=utf8;""")
        }
        tableName = 'price_card_master_name'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_0_0(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
                ITEM_CD  varchar(10),
                ITEM_POS_NAME varchar(25) NOT NULL,
                PRIMARY KEY(ITEM_CD)
            ) ENGINE=InnoDB DEFAULT charset=utf8;""")
        }
        tableName = 'commdl_item_site_planning'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_0_0(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
               updateBeginDate date NOT NULL DEFAULT '1970-01-01',
               sequenceNumber tinyint(3) unsigned NOT NULL DEFAULT '0',
               storeID varchar(6) NOT NULL DEFAULT '',
               siteId varchar(6) NOT NULL DEFAULT '',
               itemNumber varchar(20) NOT NULL,
               location varchar(8) DEFAULT NULL,
               status varchar(1) NOT NULL DEFAULT '0',
               PRIMARY KEY (updateBeginDate,sequenceNumber,storeID,siteId,itemNumber)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;""")
        }
        tableName = 'commdl_site'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_0_0(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
               updateBeginDate date NOT NULL DEFAULT '1970-01-01',
               sequenceNumber tinyint(3) unsigned NOT NULL DEFAULT '0',
               storeID varchar(6) NOT NULL DEFAULT '',
               siteId varchar(6) NOT NULL DEFAULT '',
               siteName varchar(24) DEFAULT NULL,
               siteType varchar(40) DEFAULT NULL,
               address varchar(84) DEFAULT NULL,
               telphoneNumber varchar(26) DEFAULT NULL,
               status varchar(1) NOT NULL DEFAULT '0',
               PRIMARY KEY (updateBeginDate,sequenceNumber,storeID,siteId)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;""")
        }
        tableName = 'commdl_zip_site_map'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_0_0(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
               updateBeginDate date NOT NULL DEFAULT '1970-01-01',
               sequenceNumber tinyint(3) unsigned NOT NULL DEFAULT '0',
               storeID varchar(6) NOT NULL,
               siteId varchar(6) NOT NULL,
               zipCode varchar(8) NOT NULL,
               city varchar(20) DEFAULT NULL,
               cityArea varchar(20) DEFAULT NULL,
               status varchar(1) NOT NULL DEFAULT '0',
               PRIMARY KEY (updateBeginDate,sequenceNumber,storeID,siteId,zipCode)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;""")
        }
        tableName = 'commdl_price_card_master'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_0_0(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
                updateBeginDate date NOT NULL DEFAULT '1970-01-01',
                sequenceNumber tinyint(3) NOT NULL,
                storeID varchar(6) NOT NULL DEFAULT '',
                ITEM_CD varchar(10) NOT NULL,
                ITEM_DEPT varchar(2) DEFAULT NULL,
                ITEM_CLASS varchar(2) DEFAULT NULL,
                ITEM_ORDER_TYPE varchar(2) DEFAULT NULL,
                SUPPLIER_CD varchar(4) DEFAULT NULL,
                DELIV_FLG varchar(1) DEFAULT NULL,
                ORDER_UNIT decimal(5,0) DEFAULT NULL,
                SET_FLG varchar(1) DEFAULT NULL,
                DISP_WIDTH decimal(3,0) NOT NULL,
                DISP_DEPTH decimal(3,0) NOT NULL,
                DISP_HEIGHT decimal(3,0) NOT NULL,
                ITEMNAME1 varchar(40) DEFAULT NULL,
                ITEMNAME varchar(48) DEFAULT NULL,
                COLOR varchar(24) DEFAULT NULL,
                MATERIAL varchar(24) DEFAULT NULL,
                ITEMNAME2 varchar(40) default NULL,
                COLOR1 varchar(24) DEFAULT NULL,
                OLDSELLPRICE decimal(8,0) NOT NULL,
                NEWSELLPRICE decimal(8,0) NOT NULL,
                PRIMARY KEY (updateBeginDate,sequenceNumber,storeID,ITEM_CD)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;""")
        }
        tableName = 'commdl_price_card_master_name'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_0_0(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
                updateBeginDate date NOT NULL DEFAULT '1970-01-01',
                sequenceNumber tinyint(3) NOT NULL,
                storeID varchar(6) NOT NULL DEFAULT '',
                ITEM_CD varchar(10) NOT NULL,
                ITEM_POS_NAME varchar(25) NOT NULL,
                primary key(updateBeginDate,sequenceNumber,storeID,ITEM_CD)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;""")
        }

        // aaconf表中增加配置
        def storeNo = Store.getStoreID()
        if (!rowExists("select * from aaconf where confName='showCarryTypeList'")) {
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): aaconf showCarryTypeList records are inserted.', true,
                "insert into aaconf values('${storeNo}','showCarryTypeList','DC:Shop','all is DC:WH:Shop:Supplier')")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): aaconf carrierTypeShop records are inserted.', true,
                "insert into aaconf values('${storeNo}','carrierTypeShop','Shop','Shop')")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): aaconf carrierTypeDC records are inserted.', true,
                "insert into aaconf values('${storeNo}','carrierTypeDC','DC','DC')")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): aaconf carrierTypeWH records are inserted.', true,
                "insert into aaconf values('${storeNo}','carrierTypeWH','WH','WH')")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): aaconf saleType records are inserted.', true,
                "insert into aaconf values('${storeNo}','saleType','A:B:C','ALL TYPE IS A:B:C')")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): aaconf zhuanguiType records are inserted.', true,
                "insert into aaconf values('${storeNo}','zhuanguiType','931:932','ALL TYPE IS 931:932')")
        }

        // 修改menu表增加‘信用卡銷售資料明細’業務
        if (!rowExists("select * from menu where menuID='0507'")) {
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): menuID 0507 is inserted.', true, """
                insert into menu (storeID,menuID,parentID,menuIndex,menuType,title,description,programFile,menuStatus,updateUserID,updateDateTime)
                values ('${storeNo}','0507','05',1,1,'信用卡銷售資料明細','信用卡銷售資料明細',
                'com.hyi.amber.app.course.acc.PosulCardReportViewer','1','99999990','2007-03-06 14:50:08')""")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): groupmenu 0507 is inserted.', true, """
                insert into groupmenu(storeID,groupID,menuID,updateUserID,updateDateTime)
                values ('${storeNo}','000','0507','99999990','2008-10-01 22:06:27')""")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): menu 0605 is updated.', true,
                "update menu set title='折扣一覽表',description='折扣一覽表' where menuID='0605' and parentID='06'")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): menu 0606 is updated.', true,
                "update menu set title='簡要信用卡銷售一覽',description='簡要信用卡銷售一覽' where menuID='0606' and parentID='06'")
        }

        if (!rowExists("select * from commtablelist where tableName='commdl_price_card_master'")) {
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): commtablelist commdl_price_card_master is inserted.', true, """
                insert into commtablelist (storeID,commType,tableName,tableDescription)
                values ('${storeNo}','d','commdl_price_card_master', '門市下傳-標價卡檔');""")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): commtablelist commdl_price_card_master_name is inserted.', true, """
                insert into commtablelist (storeID,commType,tableName,tableDescription)
                values ('${storeNo}','d','commdl_price_card_master_name', '門市下傳-標價卡日本名稱檔');""")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): commtablelist commdl_zip_site_map is inserted.', true, """
                insert into commtablelist (storeID,commType,tableName,tableDescription)
                values ('${storeNo}','d','commdl_zip_site_map', '門市下傳-郵遞區域配送部門關系檔');""")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): commtablelist commdl_site is inserted.', true, """
                insert into commtablelist (storeID,commType,tableName,tableDescription)
                values ('${storeNo}','d','commdl_site', '門市下傳-配送部門檔');""")
            execute('HealthyChecker.checkServerForNitoriT5_0_0(): commtablelist commdl_item_site_planning is inserted.', true, """
                insert into commtablelist (storeID,commType,tableName,tableDescription)
                values ('${storeNo}','d','commdl_item_site_planning', '門市下傳-商品默認配送部門在庫場所檔');""")
        }
    }

    static private checkServerForT5_0_1() {
        checkServerForT5_0_0()
    }

    static private checkServerForT5_0_2() {
        checkServerForT5_0_1()
    }

    static private checkServerForT5_0_3() {
        checkServerForT5_0_2()
    }

    static private checkServerForT5_0_4() {
        checkServerForT5_0_3()
    }

    static private checkServerForT5_0_5() {
        checkServerForT5_0_4()
    }

    static private checkServerForT5_0_6() {
        checkServerForT5_0_5()
    }

    static private checkServerForT5_0_7() {
        checkServerForT5_0_6()
    }

    static private checkServerForT5_0_8() {
        checkServerForT5_0_7()
    }

    static private checkServerForT5_1_0() {
        checkServerForT5_0_8()

        //if (tk3cPOS) {
        if (ServerThread.getPostProcessor().class.name.contains('TK3C')) {
            // plu, posdl_plu, commdl_plu
            execute('HealthyChecker.checkServerForT5_1_0(): plu.specification is created.', false,
                'ALTER TABLE plu ADD COLUMN specification varchar(26)')
            execute('HealthyChecker.checkServerForT5_1_0(): posdl_plu.specification is created.', false,
                'ALTER TABLE posdl_plu ADD COLUMN specification varchar(26)')
            execute('HealthyChecker.checkServerForT5_1_0(): commdl_plu.specification is created.', false,
                'ALTER TABLE commdl_plu ADD COLUMN specification varchar(26)')

            // posul_trandetail, commul_trandtl
            execute('HealthyChecker.checkServerForT5_1_0(): posul_trandtl.discountRateID is created.', false,
                'ALTER TABLE posul_trandtl ADD COLUMN discountRateID varchar(6)')
            execute('HealthyChecker.checkServerForT5_1_0(): posul_trandtl.discountRate is created.', false,
                'ALTER TABLE posul_trandtl ADD COLUMN discountRate numeric(12,2)')
            execute('HealthyChecker.checkServerForT5_1_0(): posul_trandtl.yellowAmount is created.', false,
                'ALTER TABLE posul_trandtl ADD COLUMN yellowAmount numeric(12,2)')
            execute('HealthyChecker.checkServerForT5_1_0(): posul_trandtl.diyIndex is created.', false,
                'ALTER TABLE posul_trandtl ADD COLUMN diyIndex varchar(3)')
            execute('HealthyChecker.checkServerForT5_1_0(): posul_trandtl.dtlcode1 is created.', false,
                'ALTER TABLE posul_trandtl ADD COLUMN dtlcode1 varchar(20)')
            execute('HealthyChecker.checkServerForT5_1_0(): commul_trandtl.discountRateID is created.', false,
                'ALTER TABLE commul_trandtl ADD COLUMN discountRateID varchar(6)')
            execute('HealthyChecker.checkServerForT5_1_0(): commul_trandtl.discountRate is created.', false,
                'ALTER TABLE commul_trandtl ADD COLUMN discountRate numeric(12,2)')
            execute('HealthyChecker.checkServerForT5_1_0(): commul_trandtl.yellowAmount is created.', false,
                'ALTER TABLE commul_trandtl ADD COLUMN yellowAmount numeric(12,2)')
            execute('HealthyChecker.checkServerForT5_1_0(): commul_trandtl.diyIndex is created.', false,
                'ALTER TABLE commul_trandtl ADD COLUMN diyIndex varchar(3)')
            execute('HealthyChecker.checkServerForT5_1_0(): commul_trandtl.dtlcode1 is created.', false,
                'ALTER TABLE commul_trandtl ADD COLUMN dtlcode1 varchar(20)')
        }
    }

    static private checkServerForT5_1_1() {
        checkServerForT5_1_0()

        def tableName = 'commul_ssmp_log'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_0_0(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
              `id` int(10) unsigned NOT NULL auto_increment COMMENT 'Unique id',
              `storeID` varchar(8) NOT NULL COMMENT '门市代号',
              `updateBeginDate` date NOT NULL COMMENT '传输用日期',
              `sequenceNumber` tinyint(3) NOT NULL COMMENT '传输用当日序号',
              `logTime` datetime NOT NULL COMMENT '日期时间',
              `logLevel` char(1) character set latin1 NOT NULL COMMENT '''I'': 信息, ''W'': 警告, ''E'': 错误',
              `severity` char(1) character set latin1 default NULL COMMENT '1 ~ 5: 轻微 ~ 致命 (type=''I'' 时可以为NULL)',
              `source` varchar(3) character set latin1 NOT NULL COMMENT '''SC'': SC, ''EC'': eComm, ''001’: POS1, ''002'': POS2, ...',
              `cat` char(2) character set latin1 NOT NULL COMMENT '事件分类',
              `midcat` char(4) character set latin1 NOT NULL COMMENT '''SA'': 销售, ''OR'': 订货, ''DL'': 验收, ''PD'': 配送传票, ...',
              `code` smallint(6) default NULL COMMENT '错误代码 (type=''I'' 时可以为NULL)',
              `brief` varchar(64) NOT NULL COMMENT '简要说明',
              `detail` varchar(512) default NULL COMMENT '详细描述或程序Exception log',
              PRIMARY KEY  (`id`),
              KEY `storeID` (`storeID`,`updateBeginDate`,`sequenceNumber`),
              KEY `idx_date` (`logTime`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;""")
        }
        tableName = 'master_version'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_1_1(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
              `storeID` varchar(8) NOT NULL COMMENT '门市代号',
              `masterName` varchar(64) NOT NULL COMMENT '主档名称',
              `masterVersion` varchar(20) DEFAULT NULL COMMENT '主档版本',
              PRIMARY KEY  (`masterName`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8""")
        }
    }

    static private checkServerForT5_1_2() {
        checkServerForT5_1_1()
    }

    static private checkServerForT5_1_3() {
        checkServerForT5_1_2()

        execute('HealthyChecker.checkServerForT5_1_3(): posul_tranhead.memberID is altered.', false,
            'ALTER TABLE posul_tranhead MODIFY COLUMN memberID varchar(30)')
        execute('HealthyChecker.checkServerForT5_1_3(): commul_tranhead.memberID is altered.', false,
            'ALTER TABLE posul_tranhead MODIFY COLUMN memberID varchar(30)')
    }

    static private checkServerForT5_1_4() {
        checkServerForT5_1_3()
    }

    static private checkServerForT5_1_5() {
        checkServerForT5_1_4()
    }

    static private checkServerForT5_1_6() {
        checkServerForT5_1_5()
    }

    static private checkServerForT5_1_7() {
        checkServerForT5_1_6()
    }

    static private checkServerForT5_1_8() {
        checkServerForT5_1_7()
    }

    static private checkServerForT5_1_9() {
        checkServerForT5_1_8()
    }

    static private checkServerForT5_2_0() {
        checkServerForT5_1_9()
    }

    static private checkServerForT5_2_1() {
        checkServerForT5_2_0()
    }

    static private checkServerForT5_2_2() {
        checkServerForT5_2_1()
        Server.log("***********************T5_2_2*****************************");
        def tableName = 'posul_attendance'
        if (!tableExists(tableName)) {
            execute("HealthyChecker.checkServerForNitoriT5_2_2(): ${tableName} is created.", true, """
            CREATE TABLE ${tableName} (
             ID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
             storeID VARCHAR(6) NOT NULL,
             posNumber smallint NOT NULL DEFAULT 0,
             busiDate DATE NOT NULL DEFAULT '1970-01-01',
             dayDate DATE NOT NULL ,
             employeeID VARCHAR(20) NOT NULL,
             attType VARCHAR(2) NOT NULL,
             beginTime DATETIME,
             endTime  datetime,
             createUserID VARCHAR(20),
             createDate dateTime,
             primary key(ID,storeID,posNumber,busiDate,dayDate,employeeID,attType)
            );""")
        }
    }
    static private checkServerForT5_2_3() {
        checkServerForT5_2_2()
    }

    static private checkServerForT5_2_4() {
        checkServerForT5_2_3()
    }

    static private checkServerForT5_2_5() {
        checkServerForT5_2_4()
        if (!tableExists('posul_alipay_detail')) {
            execute('HealthyChecker.checkForT5_2_5(): posul_alipay_detail is created.', true, """
                CREATE TABLE `posul_alipay_detail` (
                `storeID` varchar(6) NOT NULL DEFAULT '',
                `posNumber` int(11) NOT NULL DEFAULT '0',
                `transactionNumber` int(11) NOT NULL DEFAULT '0',
                `zseq` int not null default 0,
                `TOTAL_FEE` decimal(12,2) DEFAULT NULL,
                `TRADE_NO` varchar(64) DEFAULT NULL COMMENT '支付宝交易号',
                `systemDate` datetime DEFAULT NULL,
                `SEQ` varchar(2) DEFAULT NULL,
                `BUYER_LOGON_ID` varchar(100) DEFAULT NULL COMMENT '支付宝付款账号',
                 PRIMARY KEY (`posNumber`,`storeID`,`transactionNumber`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            """)
        }
        if (!fieldExists("store","alipay_partner_id")) {
            execute("HealthyChecker: store alipay_partner_id is add.", true,
                    "ALTER TABLE store ADD COLUMN alipay_partner_id varchar(20)")
        }
        if (!fieldExists("store", "alipay_security_code")) {
            execute("HealthyChecker: store alipay_security_code is add.", true,
                    "ALTER TABLE store ADD COLUMN alipay_security_code varchar(40)")
        }
    }

    static private checkServerForT5_2_6() {
        checkServerForT5_2_5();
    }

    static private checkServerForT5_2_7() {
        checkServerForT5_2_6();
    }

    static private checkServerForT5_2_8() {
        checkServerForT5_2_7();
    }

    static private checkServerForT5_2_9() {
        checkServerForT5_2_8();
    }

    static private checkServerForT5_3_0() {
        checkServerForT5_2_9();
    }
    static private checkServerForT5_3_1() {
        checkServerForT5_3_0();
    }
    static private checkServerForT5_3_2() {
        checkServerForT5_3_1();
    }
    static private checkServerForT5_3_3() {
        checkServerForT5_3_2();
    }
    static private checkServerForT5_3_4() {
        checkServerForT5_3_3();
    }
    static private checkServerForT5_3_5() {
        checkServerForT5_3_4();
    }
    static private checkServerForT5_3_6() {
        checkServerForT5_3_5();
    }
    static private checkServerForT5_3_6_1() {
        checkServerForT5_3_6();
    }
    static private checkServerForT5_3_6_2() {
        checkServerForT5_3_6_1()
    }
    static private checkServerForT5_3_6_3() {
        checkServerForT5_3_6_2()
    }
    static private checkServerForT5_3_7() {
        checkServerForT5_3_6_3()
    }
    static private checkServerForT5_3_8() {
        checkServerForT5_3_7()
        if (!tableExists('posul_weixin_detail')) {
            execute('HealthyChecker.checkForT5_3_8(): posul_weixin_detail is created.', true, """
                CREATE TABLE `posul_weixin_detail` (
  `storeid` varchar(6) NOT NULL DEFAULT '',
  `posnumber` int(11) NOT NULL DEFAULT '0',
  `transactionnumber` int(11) NOT NULL DEFAULT '0',
  `zseq` int(11) NOT NULL DEFAULT '0',
  `total_fee` decimal(12,2) DEFAULT NULL,
  `transaction_id` varchar(32) DEFAULT '',
  `systemDate` datetime DEFAULT NULL,
  `seq` varchar(2) DEFAULT NULL,
  `refund_id` varchar(32) DEFAULT NULL,
  `openid` varchar(128) DEFAULT NULL,
  `coupon_fee` decimal(12,2) DEFAULT NULL,
  `non_coupond_fee` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`storeid`,`posnumber`,`transactionnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            """)
        }
        if (!fieldExists("store","weixinAppId")) {
            execute("HealthyChecker: store weixinAppId is add.", true,
                    "alter table store add column weixinAppId varchar(32)")
        }
        if (!fieldExists("store", "weixinMchId")) {
            execute("HealthyChecker: store weixinMchId is add.", true,
                    "alter table store add column weixinMchId varchar(32)")
        }
        if (!fieldExists("store", "weixinPartnerKey")) {
            execute("HealthyChecker: store weixinPartnerKey is add.", true,
                    "alter table store add column weixinPartnerKey varchar(32)")
        }
        if (!fieldExists("store", "weiXinKeyStore")) {
            execute("HealthyChecker: store weiXinKeyStore is add.", true,
                    "alter table store add column weiXinKeyStore varchar(32)")
        }
        if (!fieldExists("store", "weiXinKeyPassword")) {
            execute("HealthyChecker: store weiXinKeyPassword is add.", true,
                    "alter table store add column weiXinKeyPassword varchar(32)")
        }

    }

    static private checkServerForT5_3_9() {
        checkServerForT5_3_8()
    }
   static private checkServerForT5_4_0() {
       checkServerForT5_3_9()
    }
   static private checkServerForT5_4_1() {
       checkServerForT5_4_0()
    }
   static private checkServerForT5_4_4() {
       checkServerForT5_4_1()
    }
    static private checkServerForT5_4_5() {
       checkServerForT5_4_4()
    }
    static private checkServerForT5_4_6() {
        checkServerForT5_4_4()
    }

    static private checkServerForT5_4_8() {
        setPOSProperty('MixAndMatchVersion', '3')
    }

    static private checkServerForT5_5_0() {
        checkServerForT5_4_8()
    }
    static private checkServerForT5_5_1() {
        checkServerForT5_5_0()
    }
    static private checkServerForT5_5_2() {
        checkServerForT5_5_1()
    }
    static private checkServerForT5_5_3() {
        checkServerForT5_5_2()
    }
    static private checkServerForT5_5_4() {
        checkServerForT5_5_3()
    }
    static private checkServerForT5_5_5() {
        checkServerForT5_5_4()
        if (!fieldExists("posul_weixin_detail", "promotion_id")) {
            execute("HealthyChecker: posul_weixin_detail promotion_id is add.", true,
                    "alter table posul_weixin_detail add column promotion_id varchar(32)")
        }
    }

    /** Replace version number's dot to u
     * nderscore. */
    static private versionTag() { Version.getVersion().replaceAll(/\./, '_') }

    /**
     * POS-side health checking by current version number.
     */
    static checkForPOS() {
        try {
            setupSql();
            "checkPOSFor${versionTag()}"()
        } catch (SQLException e) {
            logMessage(e)
        } finally {
            sql?.close()
            sql = null
        }
    }

    /**
     * SC-side health checking by current version number.
     */
    static checkForServer() {
        try {
            Server.setFakeExist()
            setupSql();
            "checkServerFor${versionTag()}"()
        } catch (SQLException e) {
            logMessage(e)
        } finally {
            sql?.close()
            sql = null
        }
    }

    def static void main(args) {
        checkForPOS()
    }
}
