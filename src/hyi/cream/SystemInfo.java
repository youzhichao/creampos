package hyi.cream;

import hyi.cream.dac.Cashier;
import hyi.cream.dac.Inventory;
import hyi.cream.dac.Message;
import hyi.cream.dac.Store;
import hyi.cream.dac.Transaction;
import hyi.cream.event.SystemInfoEvent;
import hyi.cream.event.SystemInfoListener;
import hyi.cream.event.TransactionEvent;
import hyi.cream.event.TransactionListener;
import hyi.cream.inline.Client;
import hyi.cream.state.GetProperty;
import hyi.cream.state.InventoryIdleState;
import hyi.cream.state.StateMachine;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.util.PCSpeaker;
import hyi.cream.groovydac.Param;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import org.apache.commons.lang.StringUtils;

public class SystemInfo extends Thread implements TransactionListener, Serializable {
    private static final long serialVersionUID = -5503853593868690435L;

    private static ResourceBundle res = CreamToolkit.GetResource();

    private Transaction trans = null;

    private SystemInfoListener systemInfoListener = null;

    private String storeName = "";

    private String storeNumber = "";

    private String buyerID = "";

    private String zNumber = "";

    private String invoiceNumber = "";

    private Date businessDate = null;

    private String terminalID = "";

    private String transactionNumber = "";

    private String cashierNumber = "";

    private String cashierName = "";

    private String memberNumber = "";

    private String peiDaNumber = "";

    private ArrayList listener = new ArrayList();

    private static boolean isDaiFu = false;

    private static boolean connected = false;

    private static boolean wanConnected = false;

    private static int inter = -2;

    private static boolean dontShowLicenseInfo;

    private String salesManNumber = "";

    private String pageNumber = "1";

    private String currentLineItemName = "";

    private String currentLineItemAmount = "";

    private String subtotalAmount = ""; // 小计

    private String totalMMAmount = ""; // 折扣

    private String salesAmount = ""; // 总计

    private String balanceAmount = ""; // 余额

    private String changeAmount = ""; // 找零

    private String companyName = Param.getInstance().getCompanyName();

    // 记录已下载的commdl_message并要显示的最大的id
    private int maxMessageID = 0;

    private HYIDouble shiftTotalCashAmount = new HYIDouble(0);

    public SystemInfo() {
        // Bruce/20030317
        // For Familymart: add "DontShowLicenseInfo"
        dontShowLicenseInfo = !Param.getInstance().getShowLicenseInfo();
        start();
    }

    /**
     * Sets the associated transaction object. It'll also register itself as the
     * transaction listener of the transaction object.
     *
     * @param trans
     *            the transaction object
     */
    public void setTransaction(Transaction trans) {
        this.trans = trans;
    }

    /**
     * Get the store name.
     *
     * @return the store name.
     */
    public String getStoreName() {
        if (POSTerminalApplication.getInstance().getTrainingMode()) {
            return CreamToolkit.GetResource().getString("TrainingMode");
        } else {
            return Store.getStoreChineseName();
        }
    }

    /**
     * Get the store name and number
     *
     * @return the store name.
     */
    public String getStoreNameAndNumber() {
        if (StateMachine.getInstance().getRecorded() == 0 && POSTerminalApplication.getInstance().getTrainingMode()) {
            return CreamToolkit.GetResource().getString("TrainingMode");
        } else if (StateMachine.getInstance().getRecorded() == 1) {
            return "Record";
        } else if (StateMachine.getInstance().getRecorded() == 2) {
            return "Replay";
        } else {
            return Store.getStoreChineseName() + "(" + Store.getStoreID() + ")";
        }
    }

    public boolean getIsDaiFu() {
        return isDaiFu;
    }

    public void setIsDaiFu(boolean isDaiFu) {
        SystemInfo.isDaiFu = isDaiFu;
    }

    /**
     * Get the store number.
     *
     * @return the store number.
     */
    public String getStoreNumber() {
        storeNumber = Store.getStoreID();
        return storeNumber;
    }

    /**
     * Get the buyer's 统一编号.
     *
     * @return the buyer's 统一编号.
     */
    public String getBuyerID() {
        buyerID = trans.getBuyerNumber();
        if (buyerID == null)
            return "";
        return buyerID;
    }

    /**
     * Get the 配达number
     *
     * @return 配达number
     */
    public String getPeiDaNumber() {
        String type = trans.getAnnotatedType();
        if (type != null && type.equals("P")) {
            peiDaNumber = trans.getAnnotatedId();
            if (peiDaNumber == null)
                return "";
        } else
            return "";
        return peiDaNumber;
    }

    static private HashMap showMsgs = new HashMap();

    static private void extractMsg(Object[] msgs) {
        for (int i = 0; i < msgs.length; i++) {
            Message msg = (Message) msgs[i];
            showMsgs.put(msg.getId() + msg.getSource(), msg.getMessage());
        }
    }

    static private boolean showMessageIsRunning;

    synchronized static private void setShowMessageIsRunning(boolean b) {
        showMessageIsRunning = b;
    }

    synchronized static private boolean getShowMessageIsRunning() {
        return showMessageIsRunning;
    }

    public void run() {
        int count = 0;
        while (true) {
            POSTerminalApplication app = POSTerminalApplication.getInstance();
            try {
                count++;
                Object[] msgs = null;
                StringBuffer messageString = new StringBuffer();

                Client client = Client.getInstance();
                if (client.isConnected()) {
                    synchronized (client) {
                        Collection objs = client.queryMultipleObjects("hyi.cream.dac.Message");
                        if (objs != null)
                            msgs = objs.toArray();
                    }
                }
                if (msgs != null) {
                    for (int i = 0; i < msgs.length; i++) {
                        Message msg = (Message) msgs[i];
                        if (msg.getMessage() != null
                                && !msg.getMessage().trim().equals("")) {
                            messageString.append(msg.getMessage());
                            messageString.append("          ");
                        }
                    }
                    // add 2004.03.18
                    processMessage(msgs);
                }
              
//                messageString.append("訂貨換日即將開始，請儘快完成門市訂貨！        ");
//                messageString.append("消息區顯示正常！        ");
//                messageString.append("今日有新促銷商品，請各門店注意！");

//                messageString.append("消息区显示正常！        ");
//                messageString.append("今日有新促销商品，请各门店注意！        ");
                // 从sc.commdl_head中获得新主档的信息,并在pos上提示下载
                if (Param.getInstance().getNeedUpdateHead() && (count % 30 == 0)) {
                    boolean needDown = Client.getInstance().needDownMasterVersion();
                    if (needDown) {
                        messageString.append(res.getString("NeedDownMaster"));
                        messageString.append("          ");
                        PCSpeaker.getInstance().speeker(PCSpeaker.MESSAGE_BEEP);
                    }
                }

                if (messageString.length() == 0) {
                    //String licenseInfo = GetProperty.getLicenseInfo("");
                    //if (StringUtils.isEmpty(licenseInfo))
                    //    licenseInfo = res.getString("CompanyTitle");
                    String licenseInfo = res.getString("CompanyTitle");
                    app.getInformationIndicator().setMessage(
                            dontShowLicenseInfo ? "" : licenseInfo);
                    // TODO 这里会把其他地方设到 InformationIndicator 的信息覆盖掉!
                } else {
                    if (Param.getInstance().getPosType().equalsIgnoreCase("wincor"))
                        app.getWarningIndicator().setMessage(messageString.toString());
                    else
                        app.getInformationIndicator().setMessage(messageString.toString());
                }

                Thread.sleep(120000); // 2分钟
            } catch (Exception e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        }
    }

    /*
     * 全家功能：如果出现新的总部发布的通报 pos会鸣叫 新增属性MessageSound=yes
     */
    private void processMessage(Object[] msgs) {
        if (!Param.getInstance().isMessageSound())
            return;
        int oldMaxID = maxMessageID;
        for (int i = 0; i < msgs.length; i++) {
            try {
                Message msg = (Message) msgs[i];
                // '1':总部通告信息
                if (msg.getSource().equals("1") && msg.getMessage() != null
                        && !msg.getMessage().trim().equals("")) {
                    if (msg.getId() > maxMessageID)
                        maxMessageID = msg.getId();
                }
            } catch (Exception e) {
            }
        }
        if (maxMessageID > oldMaxID) {
            // 鸣叫
            PCSpeaker.getInstance().speeker(PCSpeaker.MESSAGE_BEEP);
        }
    }

    // static class ShowMessage implements Runnable {
    // public void run() {
    // try {
    // if (!showMsgs.isEmpty()) {
    // if
    // (POSTerminalApplication.getInstance().getInformationIndicator().getDisplay())
    // return;
    // Iterator itr = showMsgs.keySet().iterator();
    // if (itr.hasNext()) {
    // Object nextKey = itr.next();
    // String next = (String)showMsgs.remove(nextKey);
    // //(String)showMsgs.get(nextKey);
    // POSTerminalApplication.getInstance().getInformationIndicator().setMessage(next);
    // }
    // return;
    // } else {
    // if
    // (!POSTerminalApplication.getInstance().getInformationIndicator().getDisplay())
    // {
    // if (dontShowLicenseInfo)
    // POSTerminalApplication.getInstance().getInformationIndicator().setMessage("");
    // else
    // POSTerminalApplication.getInstance().getInformationIndicator().setMessage(
    // res.getString("CompanyTitle"));
    // }
    // }
    // if (!Client.getInstance().isConnected())
    // return;
    //
    // Client client = Client.getInstance();
    // Object[] msgs = null;
    // synchronized (client) {
    // msgs = client.downloadDac0("hyi.cream.dac.Message");
    // try {
    // client.sendMessage("OK");
    // } catch (IOException e1) {
    // e1.printStackTrace(Client.getLogger());
    // }
    // }
    // if (msgs != null)
    // extractMsg(msgs);
    // } catch (ClientCommandException e) {
    // return;
    // } finally {
    // setShowMessageIsRunning(false);
    // }
    // }
    // }
    //
    // static public void showMsg() {
    // if (!getShowMessageIsRunning()) {
    // Thread show = new Thread(new ShowMessage());
    // show.start();
    // setShowMessageIsRunning(true);
    // }
    // }

    /**
     * Get the Connection status between SC and POS.
     *
     * @return the status string.
     */
    static public String getConnected() {
        // Bruce/20030318
        // 1. 解决连、离线的字样和图标不同步的问题
        // 2. 延长取得后台通报信息的间隔
        connected = Client.getInstance().isConnected();
        // showMsg();
        // if (inter > 10 || inter == 0) {
        // inter = 1;
        // //connected = DacTransfer.getInstance().isConnected();
        // //DacTransfer.getInstance().showMsg();
        // showMsg();
        // } else {
        // inter++;
        // }
        if (connected)
            return res.getString("Online");
        else
            return res.getString("Offline");
    }
    public static String getWanConnected() {
        if (wanConnected)
            return res.getString("WanOnline");
        else
            return res.getString("WanOffline");

    }

    public static boolean isWanConnected() {
        return wanConnected;
    }

    public static void setWanConnected(boolean b) {
        wanConnected = b;
    }


    public String getTransactionType() {
        // Display transaction type in Itemlist head
        if (this.getIsDaiFu()) {
            return CreamToolkit.GetResource().getString("DaiFu");
        }
        String type = "";
        if (trans != null) {
            type = trans.getDealType2();
        }
        if (type == null || type.trim().length() == 0)
            type = "0";

        ResourceBundle res = CreamToolkit.GetResource();
        String displayName = res.getString("td" + type);
        return displayName;
    }

    public String getZNumber() {
        return Param.getInstance().getZNumber() + "";
    }

    /**
     * Get the invoice number.
     *
     * @return the invoice number.
     */
    public String getInvoiceNumber() {
        invoiceNumber = Param.getInstance().getNextInvoiceNumber();
        return invoiceNumber;
    }

    /**
     * Get the business date.
     *
     * @return the business date.
     */
    public Date getBusinessDate() {
        businessDate = new Date();
        return businessDate;
    }

    /**
     * Get the terminal ID.
     *
     * @return the terminal ID.
     */
    public String getTerminalID() {
        Integer tm = trans.getTerminalNumber();
        if (tm == null)
            return "??";
        String tmno = tm.toString();
        if (tmno.length() == 1)
            tmno = "0" + tmno;
        return tmno;
    }

    /**
     * Get the terminal Name.
     * @return the terminal Name, 如果取不到，就返回问号
     */
    public String getTerminalName() {
        return Param.getInstance().getTerminalName();
        // if (terminalName == null){
        //     terminalName = GetProperty.getTerminalName();
        // }
        // return terminalName;
    }

    /**
     * Get the sequence number of the transaction.
     *
     * @return the transaction number.
     */
    public String getTransactionNumber() {
        transactionNumber = String.valueOf(Transaction
                .getNextTransactionNumber());
        // GetProperty.getProperty("NextTransactionSequenceNumber");
        if (transactionNumber.equals("-1") && trans != null
                && !trans.getDealType2().equalsIgnoreCase("D")) {
            POSTerminalApplication.getInstance().getWarningIndicator()
                    .setMessage(res.getString("LostSQLConnection"));
            hyi.cream.state.StateMachine.getInstance().setEventProcessEnabled(
                    false);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                CreamToolkit.logMessage(e.getMessage());
            }
            System.exit(1);
        }
        while (transactionNumber.length() < 8) { // added by wissly
                                                    // transactionNumber=8
            transactionNumber = "0" + transactionNumber;
        }
        // if (transactionNumber.length() > 8) {
        // transactionNumber =
        // transactionNumber.substring(transactionNumber.length() - 5);
        // }
        return transactionNumber;
    }

    /**
     * 获取显示的交易号码，可能是经过打乱的
     *
     * @return String
     */
    public String getPrintTransactionNumber() {
        return this.trans.getPrintTranNumber();
    }

    /**
     * Get the cashier number.
     *
     * @return the cashier number.
     */
    public String getCashierNumber() {
        return Param.getInstance().getCashierNumber();
    }

    /**
     * Get the cashier name.
     *
     * @return the cashier name.
     */
    public String getCashierName() {
        Cashier cashier = Cashier.queryByCashierID(getCashierNumber());
        if (cashier == null)
            return "";
        else
            return cashier.getCashierName();
    }

    /**
     * Get the drawer number.
     *
     * @return the drawer number.
     */
    public String getMemberNumber() {
        memberNumber = trans.getMemberID();
        if (memberNumber == null) {
            memberNumber = "";
        }
        return memberNumber;
    }

    /**
     * Invoked when transaction has been changed.
     *
     * @param e
     *            an event object represents the changes.
     */
    public void transactionChanged(TransactionEvent e) {
        this.setTransaction((Transaction) e.getSource());
        fireEvent(new SystemInfoEvent(this));
    }

    public void addSystemInfoListener(SystemInfoListener s) {
        listener.add(s);
    }

    public void fireEvent(SystemInfoEvent e) {
        Iterator iter = listener.iterator();
        while (iter.hasNext()) {
            SystemInfoListener s = (SystemInfoListener) iter.next();
            s.systemInfoChanged(e);
        }
    }

    /**
     * @return String
     */
    public String getSalesManNumber() {
        return salesManNumber;
    }

    /**
     * Sets the salesManNumber.
     *
     * @param salesManNumber
     *            The salesManNumber to set
     */
    public void setSalesManNumber(String salesManNumber) {
        this.salesManNumber = salesManNumber;
        fireEvent(new SystemInfoEvent(this));
    }

    /*
     * 页数
     */
    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
        fireEvent(new SystemInfoEvent(this));
    }

    public String getPageNumber() {
        return " " + pageNumber;
    }

    public String getCurrentLineItemName() {
        if (POSTerminalApplication.getInstance().getDacViewer().isVisible()) {
            Inventory inv = InventoryIdleState.getCurrentInventory();
            return (inv != null) ? inv.getDescription() : "";
        }

        if (trans != null && trans.getCurrentLineItem() != null
                && !trans.getCurrentLineItem().getDetailCode().equals("D"))
            return trans.getCurrentLineItem().getDescriptionAndSpecification();
        else
            return "";
    }

    public String getCurrentLineItemAmount() {
        if (POSTerminalApplication.getInstance().getDacViewer().isVisible()) {
            Inventory inv = InventoryIdleState.getCurrentInventory();
            return (inv != null) ? inv.getUnitPrice().toString() : "";
        }
        if (trans != null && trans.getCurrentLineItem() != null
                && !trans.getCurrentLineItem().getDetailCode().equals("D"))
            return trans.getCurrentLineItem().getAmount().toString();
        else
            return "";
    }

    public String getCurrentLineItemQty() {
        if (POSTerminalApplication.getInstance().getDacViewer().isVisible()) {
            Inventory inv = InventoryIdleState.getCurrentInventory();
            return (inv != null) ? inv.getUnitPrice().toString() : "";
        }
        if (trans != null && trans.getCurrentLineItem() != null
                && !trans.getCurrentLineItem().getDetailCode().equals("D"))
            return trans.getCurrentLineItem().getQuantity().toString();
        else
            return "";
    }

    public void setSubtotalAmount(String subtotalAmount) {
        this.subtotalAmount = subtotalAmount;
        fireEvent(new SystemInfoEvent(this));
    }

    public String getSubtotoalAmount() {
        return subtotalAmount;
    }

    public void setTotalMMAmount(String totalMMAmount) {
        this.totalMMAmount = totalMMAmount;
        fireEvent(new SystemInfoEvent(this));
    }

    public String getTotoalMMAmount() {
        return totalMMAmount;
    }

    public void setSalesAmount(String salesAmount) {
        this.salesAmount = salesAmount;
        fireEvent(new SystemInfoEvent(this));
    }

    public String getSalesAmount() {
        return salesAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
        fireEvent(new SystemInfoEvent(this));
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setChangeAmount(String changeAmount) {
        this.changeAmount = changeAmount;
        fireEvent(new SystemInfoEvent(this));
    }

    public String getChangeAmount() {
        return changeAmount;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getSpecialTimeFormate() {
        String str = "";
        int num = 0;
        Calendar cal = Calendar.getInstance();
        num = cal.get(Calendar.MONTH) + 1;
        if (num < 10)
            str += " ";
        str += num;
        str += res.getString("Month");

        num = cal.get(Calendar.DAY_OF_MONTH);
        if (num < 10)
            str += " ";
        str += num;

        str += res.getString("Day");

        str += " " + res.getString("Week");
        num = cal.get(Calendar.DAY_OF_WEEK) - 1;
        switch (num) {
        case 1:
            str += res.getString("ShortMonday");
            break;
        case 2:
            str += res.getString("ShortTuesday");
            break;
        case 3:
            str += res.getString("ShortWednesday");
            break;
        case 4:
            str += res.getString("ShortThursday");
            break;
        case 5:
            str += res.getString("ShortFriday");
            break;
        case 6:
            str += res.getString("ShortSaturday");
            break;
        case 7:
            str += res.getString("ShortSunday");
            break;
        }

        str += " ";
        num = cal.get(Calendar.HOUR_OF_DAY);
        if (num < 10)
            str += "0";
        str += num;

        str += ":";
        num = cal.get(Calendar.MINUTE);
        if (num < 10)
            str += "0";
        str += num;
        return str;
    }

    public String getAdvertWord() {
        return "This is an advertisement word!";
    }

    public String[] getWelcomeWord() {
        return new String[] {"Line One "};
    }

    ////////////////////////////////////////////////////////////////////////////////////////

    private static LinkedList<String> stateHistoryList = new LinkedList<String>();

    public static void stateEntry(hyi.cream.state.State state) {
        stateHistoryList.add(state.getClass().getSimpleName());
        trimStateHistoryList();
        POSTerminalApplication.getInstance().repaintScreenBanner2();
    }

    public static void stateExit(hyi.cream.state.State state) {
        String stateName = state.getClass().getSimpleName();
        String lastState = stateHistoryList.size() > 0 ? stateHistoryList.getLast() : "";
        if (!stateName.equals(lastState))
            stateHistoryList.add(stateName);

        stateHistoryList.add("←");
        trimStateHistoryList();
        POSTerminalApplication.getInstance().repaintScreenBanner2();
    }

    private static void trimStateHistoryList() {
        while (stateHistoryList.size() > 30)
            stateHistoryList.removeFirst();
    }

    /**
      * A system property for displaying state traversing path.
      */    
    public String getStateLog() {
        StringBuilder log = new StringBuilder();
        for (int i = stateHistoryList.size() - 1; i >= 0; i--)
            log.append(stateHistoryList.get(i).toString());
        return log.toString();
    }

    public void setShiftTotalCashAmount(HYIDouble shiftTotalAmount) {
        this.shiftTotalCashAmount = shiftTotalAmount;
    }

    public HYIDouble getShiftTotalCashAmount() {
        return shiftTotalCashAmount;
    }

    public String getTranType1Name() {
        if ("1".equals(trans.getTranType1())) {
            return res.getString("TranType1Name1");
        } else {
            return res.getString("TranType1Name0");
        }
    }
}
