package hyi.cream;

public enum WorkingStateEnum {
	IDLE_STATE,
	PERIODICAL_STATE,
	PERIODICAL_ORDER_STATE,  // 期刊订货
	PERIODICAL_DRAW_STATE,   // 期刊领刊
	PERIODICAL_RETURN_STATE, // 期刊退刊
	WHOLESALE_STATE,         // 团购
	WHOLESALECLEARING_STATE, // 团购结算
}
