package hyi.cream.inline;

import hyi.cream.dac.DacBase;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.util.Iterator;
import java.util.Map;

public class DacObjectOutputStream {

    private ObjectOutputStream oos;
    private DataOutputStream dos;
    private boolean inlineClientUseJVM;
    private boolean serverExist;

    public DacObjectOutputStream(OutputStream out)
        throws IOException, StreamCorruptedException {
        this.oos = new ObjectOutputStream(out);
        this.dos = new DataOutputStream(out);
        //String inlineClientUseJVMString = GetProperty.getInlineClientUseJVM("");
        //inlineClientUseJVM = (inlineClientUseJVMString != null &&
        //    inlineClientUseJVMString.compareToIgnoreCase("YES") == 0);
        serverExist = Server.serverExist();
    }

    /**
     * DAC sending protocol:
     *
     * sender                                           receiver
     *   |          {DAC classname(String)}                 |
     *   | -----------------------------------------------> |
     *   |          {number of fields(int)}                 |
     *   | -----------------------------------------------> |
     *   |    {first field name(String)}                    |
     *   | -----------------------------------------------> |
     *   |    {first field value classname(String)}         |
     *   | -----------------------------------------------> |
     *   |    {first field value(String)}                   |if it is a Date, send its getTime() as value; otherwise send its toString()
     *   | -----------------------------------------------> |if it is a null value, send String("^null^")
     *   |    {second field name(String)}                   |
     *   | -----------------------------------------------> |
     *   |    {second field value classname(String)}        |
     *   | -----------------------------------------------> |
     *   |    {second field value(String)}                  |
     *   | -----------------------------------------------> |
     *   |        . . .                                     |
     *   | -----------------------------------------------> |
     */
    public void writeObject(DacBase obj) throws IOException {
        oos.writeObject(new String(obj.getClass().getName()));
        Map fieldMap = obj.getFieldMap();
        dos.writeInt(fieldMap .size());
        //System.out.println("fieldCount=" + fieldMap .size());
        Iterator iter = fieldMap.keySet().iterator();
        //int i = 0;
        while (iter.hasNext()) {
            String fieldName = (String)iter.next();
            oos.writeObject(new String(fieldName));
            //i++;
            //System.out.println(i + "fieldName=" + fieldName);
            Object value = fieldMap.get(fieldName);
            if (value == null) {
                oos.writeObject(String.class.toString());
                oos.writeObject("^null^");
                //System.out.println(i + "  fieldValue=null");
            } else {
                oos.writeObject(new String(value.getClass().getName()));
                //System.out.print(i + "  fieldValue:" + value.getClass().getName());
                if (value instanceof java.sql.Date) {
                    dos.writeLong(((java.sql.Date)value).getTime());
                    //System.out.println("=" + ((java.sql.Date)value).getTime());
                } else if (value instanceof java.util.Date) {
                    dos.writeLong(((java.util.Date)value).getTime());
                    //System.out.println("=" + ((java.util.Date)value).getTime());
                } else if (value instanceof java.sql.Timestamp) {
                    dos.writeLong(((java.sql.Timestamp)value).getTime());
                    //System.out.println("=" + ((java.sql.Timestamp)value).getTime());
                } else if (value instanceof java.sql.Time) {
                    dos.writeLong(((java.sql.Time)value).getTime());
                    //System.out.println("=" + ((java.sql.Time)value).getTime());
/*
                } else if (serverExist && !inlineClientUseJVM && value instanceof String) {
                // means client uses gcj, but because gcj doesn't not support
                // Unicode, so....
                    //System.out.print("1:" + value);
                    byte[] bs = ((String)value).getBytes(CreamToolkit.getEncoding());
                    value = new String(bs, "8859_1");
                    //System.out.print(" 2:" + value);
                    oos.writeObject(value);
                    //System.out.println(" ok");
*/
                } else {
                    oos.writeObject(new String(value.toString()));
                    //System.out.println(i + " " + value.toString());
                }
            }
        }
    }
}