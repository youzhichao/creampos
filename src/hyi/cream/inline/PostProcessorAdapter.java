package hyi.cream.inline;

import hyi.cream.dac.ShiftReport;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.ZReport;
import hyi.cream.groovydac.SsmpLog;
import hyi.cream.util.*;

import java.math.BigDecimal;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.*;
import java.util.Date;


public class PostProcessorAdapter implements PostProcessor {

    private static final String UPLOADED_STATUS = "2";
    private static final String STORE_ID = getStoreID(); //缓存StoreID

    public boolean afterReceivingShiftReport(ShiftReport shift) {return true;}
    public boolean afterReceivingZReport(ZReport z) {return true;}
    public boolean afterReceivingZReport(ZReport z, StringBuffer accdateStrBuf) {
        return afterReceivingZReport(z);
    }
    public boolean afterReceivingTransaction(Object[] trans) {return true;}
    public boolean afterReceivingDepSales(Object[] depSales)  {return true;}
    public boolean afterReceivingDaishouSales(Object[] daishouSales) {return true;}
    public boolean afterReceivingCashForm(Object[] cashForm) {return true;} 
    public boolean afterReceivingDaiShouSales2(Object[] daiShouSales2) {return true;}
    public boolean afterReceivingInventory(Object[] inventory) {return true;}   
    public boolean beforeDownloadingMaster() {return true;}
    public boolean canSyncTransaction() { return true;}
    public void afterDonePluPriceChange(String ID, String posID, boolean success) {};
    public boolean afterReceivingAttendance(Object[] attendance) {return true;}
    public boolean afterReceivingAttendance1(Object[] attendance1) {return true;}
    public boolean afterReceivingHistoryTrans(Object[] hts) {return true;} 
    public boolean afterReceivingPosVersion(Object[] posVersion) {return true;} 
    public boolean recaleAfterReceivingDepSales(DbConnection connection, Date accountingDateObj) throws SQLException  {return true;}

    public static ArrayList constructFromResultSet(ResultSet resultSet) throws SQLException {
        ArrayList table = new ArrayList();
        HashMap newMap = null;
        while (resultSet.next()) {
            newMap = new HashMap();
            ResultSetMetaData metaData = resultSet.getMetaData();
            int t = 0;
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                Object o = resultSet.getObject(i);
                if (o != null) {
                    t = metaData.getColumnType(i);
                    if (t == Types.NUMERIC || t == Types.DECIMAL) {
                        o = new HYIDouble(((BigDecimal)o).doubleValue());
                    }
                }
                newMap.put(metaData.getColumnName(i).toLowerCase(), o);
            }
            table.add(newMap);
        }
        return table;
    }
    
    public static HashMap constructFromResultSet2(ResultSet resultSet) throws SQLException {
        HashMap newMap = new HashMap();
        if (resultSet.next()) {
            ResultSetMetaData metaData = resultSet.getMetaData();
            int t = 0;
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                Object o = resultSet.getObject(i);
                if (o != null) {
                    t = metaData.getColumnType(i);
                    if (t == Types.NUMERIC || t == Types.DECIMAL) {
                        o = new HYIDouble(((BigDecimal)o).doubleValue());
                    }
                }
                newMap.put(metaData.getColumnName(i).toLowerCase(), o);
            }
        }
        return newMap;
    }
    
    public static Map query(DbConnection connection, String selectStatement) throws SQLException {
        return DBToolkit.query(connection, selectStatement);
    }

    public static HYIDouble queryAmount(DbConnection connection, String selectStatement)
        throws SQLException {
        ResultSet resultSet = null;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            if (resultSet.next()) {
                int t = 0;
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object o = resultSet.getObject(i);
                    if (o != null) {
                        t = metaData.getColumnType(i);
                        if (t == Types.NUMERIC || t == Types.DECIMAL) {
                            return new HYIDouble(((BigDecimal)o).doubleValue());
                        }
                    }
                }
            }
            return new HYIDouble(0);
        } finally {
            if (resultSet != null)
                resultSet.close();
            if (statement != null)
                statement.close();
        }
    }
    
    public static boolean hasAccountDate(DbConnection connection, String accountDate)
            throws SQLException {
        ResultSet resultSet = null;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM accdayrpt WHERE accountDate='" + accountDate + "'");
            ResultSetMetaData metaData = resultSet.getMetaData();
            if (resultSet.next()) {
                return true;
            }
            return false;
        } finally {
            if (resultSet != null)
                resultSet.close();
            if (statement != null)
                statement.close();
        }
    }

//    public static boolean update(String updateStatement) {
//        DbConnection connection  = null;
//        Statement  statement   = null;
//        ResultSet  resultSet   = null;
//        try {
//            connection = CreamToolkit.getPooledConnection();
//            statement = connection.createStatement();
//            statement.executeUpdate(updateStatement);
//            return true;
//        } catch (SQLException e) {
//            e.printStackTrace(Server.getLogger());
//            return false;
//        } finally {
//            try {
//                if (resultSet != null)
//                    resultSet.close();
//                if (statement != null)
//                    statement.close();
//                if (connection != null)
//                    CreamToolkit.releaseConnection(connection);
//            } catch (SQLException e) {
//                e.printStackTrace(Server.getLogger());
//            }
//        }
//    }

    public static int executeQuery(DbConnection connection, String query) throws SQLException {
        Statement statement = connection.createStatement();
        Server.log("executeQuery : " + query);
        int rowAffected = statement.executeUpdate(query);
        statement.close();
        return rowAffected;
    }

    public static ArrayList getCollectionOfStatement(DbConnection connection, String selectStatement)
            throws SQLException {

        ResultSet resultSet = null;
        ArrayList collection = new ArrayList();

        Statement statement = connection.createStatement();
        resultSet = statement.executeQuery(selectStatement);
        ResultSetMetaData metaData = resultSet.getMetaData();
        while (resultSet.next()) {
            HashMap map = new HashMap();
            int t = 0;
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                Object o = resultSet.getObject(i);
                if (o != null) {
                    t = metaData.getColumnType(i);
                    if (t == Types.NUMERIC || t == Types.DECIMAL) {
                        o = new HYIDouble(((BigDecimal)o).doubleValue());
                    }
                }
                map.put(metaData.getColumnName(i).toLowerCase(), o);
            }
            collection.add(map);
        }

        if (resultSet != null)
            resultSet.close();
        if (statement != null)
            statement.close();

        return collection;
    }
    
    public static void insert(DbConnection connection, HashMap fieldMap, String tableName) throws SQLException {
        Iterator iterator = ((Set)fieldMap.keySet()).iterator();
        Object fieldValue = null;
        String fieldName = "";
        Statement statement = null;
        String nameParams = "";
        String valueParams = "";
        while (iterator.hasNext()) {
            fieldName = (String)iterator.next();
            if (fieldMap.get(fieldName) == null) {
                fieldValue = new Object();
            } else {
                fieldValue = fieldMap.get(fieldName);
                if (fieldValue instanceof java.util.Date) {
                    SimpleDateFormat DF = CreamCache.getInstance().getDateTimeFormate();
                    java.util.Date d = (java.util.Date)fieldValue;
                    nameParams  += fieldName + ",";
                    valueParams += "'" + DF.format(d).toString() + "',";
                } else if (fieldValue instanceof String) {
                    nameParams  += fieldName + ",";
                    valueParams += "'" + fieldValue + "',";
                } else {
                   nameParams  += fieldName + ",";
                   valueParams += "'" + fieldValue + "',";
                }
            }
        }
        String insertString = "INSERT INTO " + tableName + "(" + nameParams.substring(0, nameParams.length()-1) + ")"
            + " VALUES (" + valueParams.substring(0,  valueParams.length()-1) + ")";

        statement = connection.createStatement();
        statement.execute(insertString);
        statement.close();
    }   

    public static String getStoreID() {
        Server.setFakeExist();
        
        if (STORE_ID != null && !STORE_ID.equals(""))
            return STORE_ID;
        
        //// Try to get storeID from CreamProperties first
        String storeID = "";
        //String storeID = GetProperty.getStoreID("");
        //if (storeID.equals("")) {
        String query = "SELECT storeid FROM store";
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Map value = query(connection, query);
            if (value != null) {
                if (value.containsKey("storeid"))
                   storeID = value.get("storeid").toString();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return storeID;
    }

    private Date getCalendarDate(DbConnection connection, String dateType) {
        try {
            String storeID = getStoreID();
            Statement statement;
            ResultSet resultSet;
            Date accDate = null;
            String select = "SELECT currentDate FROM calendar WHERE storeID='" + storeID + "' AND dateType='"
                + dateType + "'";
            //Server.log(select);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(select);
            if (resultSet.next())
                accDate = resultSet.getDate(1);

            if (statement != null)
                statement.close();
            if (resultSet != null)
                resultSet.close();

            if (accDate == null) {
                accDate = new Date();
                Server.log("Cannot get date from table calendar!");
            }
            return accDate;

        } catch (Exception e) {
            Server.log("Cannot get date from table calendar!");
            e.printStackTrace();
            return new Date();
        }
    }

    /**
     * 取得系统业务当前的会计日
     * @return Date 系统业务当前的会计日
     * @version 2004-08-25 by James
     */
    protected Date getAccountingDate(DbConnection connection) {
        return getCalendarDate(connection, "1");
    }

    @Override
    public int hashCode() {
        return super.hashCode();    //To change body of overridden methods use File | Settings | File Templates.
    }

    /**
     * 取得系统业务当前的会计日
     * @return Date 系统业务当前的会计日
     * @version 2004-08-25 by James
     */
    protected Date getBusinessDate(DbConnection connection) {
        return getCalendarDate(connection, "0");
    }

    /**
     * 取得指定表的字段名列表
     * @param tableName
     * @return Collection of fields
     */
    public static Collection getFieldList(String tableName) {
        DbConnection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        List list = new ArrayList();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM " + tableName + " WHERE 1=2");
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                list.add(metaData.getColumnName(i).toLowerCase());
            }
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return list;
    }

    public boolean afterReceivingShiftEx(Object[] shiftExex) {
        return false;
    }

    public boolean afterReceivingZEx(Object[] zExex) {
        return false;
    }

    public static void updatePeiDaPayStatus(Transaction tran) {
        Statement statement = null;
        ResultSet resultSet = null;
        DbConnection connection = null;
        if ("*".equals(tran.getDealType1())){
            return;
        }
        try {
            if (tran.isPeiDa()) {
                connection = CreamToolkit.getPooledConnection();
                String storeID = tran.getStoreNumber();
                String deliveryID = tran.getAnnotatedId();
                String tranNo = tran.getTransactionNumber().toString();
                String empId = tran.getCashierNumber();
                String deliveryNo = deliveryID.substring(0, 8);

                statement = connection.createStatement();
                resultSet = statement.executeQuery("SELECT ver FROM deliveryhead "
                    + "WHERE storeID='" + storeID + "' AND deliveryNo='" + deliveryNo + "' AND payStatus='2'");
                if (resultSet.next()) // 如果此筆傳票已付款，就不做任何更新動作
                    return;

                // 否則將更新為已付款狀態，並將版本號加1
                String sql = "UPDATE deliveryhead SET payStatus='2'" +
                    ", updateUserId='" + empId + "', transactionNumber=" + tranNo +
                    ", posNumber=" + tran.getTerminalNumber()
                    + ",ver=ver+1 WHERE storeID='" + storeID + "' AND deliveryNo='" + deliveryNo + "'";
                executeQuery(connection, sql);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
                CreamToolkit.releaseConnection(connection);
            } catch (Exception e) {
            }
        }
    }

    private static int getCommulMaxSequenceNumber(Connection conn, java.sql.Date ubd,
        String tableName) throws SQLException {
        String sqlString = "SELECT MAX(sequenceNumber) FROM commul_head"
            + " WHERE updateBeginDate = ? AND storeID = ? AND tableName = ?"
            + " AND uploadResult = '" + UPLOADED_STATUS + "'";
        PreparedStatement ps = null;
        ResultSet rs = null;
        int maxNo = 0;
        try {
            ps = conn.prepareStatement(sqlString);
            ps.setDate(1, ubd);
            ps.setString(2, STORE_ID);
            ps.setString(3, tableName);
            rs = ps.executeQuery();
            if (rs.next()) {
                maxNo = rs.getInt(1);
            }
            return maxNo;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (ps != null) ps.close();
                if (rs != null) rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }
        }
    }

    private static void makeCommulHeadRecord(Connection con, String store, int seq, java.sql.Date ubd,
        String tableName, int recordCount, String updateType, String uploadResult) {
    	ResultSet rs = null;
    	PreparedStatement ps = null;
        System.out.println(" Log upload info record of table '" + tableName + "' for check in commul_head...");
		try {
	        int uploadCount = 0;
	        int failCount = 0;
	        String makeUserID = "00000000"; //DateShiftCommon.UPDATE_USERID;
	        Date makeDate = new Date(); //DateShiftCommon.getSqlNow();
            DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
	        String makeDateStr = df.format(makeDate); //DateShiftCommon.getDateTime(makeDate, "yyyyMMddHHmmss");
	        String whereCluase = " WHERE updateBeginDate = ? "
	                + "AND sequenceNumber = ? AND storeID = ? AND tableName = ?";
	        String sqlString1 = "SELECT recordCount FROM commul_head " + whereCluase;
	        String sqlString2 = "INSERT INTO commul_head(updateBeginDate, sequenceNumber, "
	            + "storeID, tableName, recordCount, uploadCount, failCount, updateType, uploadResult, makeUserID, makeDate) "
	            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	        String sqlString4 = "UPDATE commul_head set "
		        + "recordCount=?, uploadCount=?, failCount=?, updateType=?, uploadResult=?, makeUserID=?, makeDate=? "
		        + whereCluase;
            ps = con.prepareStatement(sqlString1);
            ps.setDate(1, ubd);
            ps.setInt(2, seq);
            ps.setString(3, store);
            ps.setString(4, tableName);
            rs = ps.executeQuery();
            if (rs.next()) {
                System.out.println(" Old record exist. update it...");
                ps = con.prepareStatement(sqlString4);
                int i = 1;
            	ps.setInt(i++, recordCount);
            	ps.setInt(i++, uploadCount);
            	ps.setInt(i++, failCount);
            	ps.setString(i++, updateType);
            	ps.setString(i++, uploadResult);
            	ps.setString(i++, makeUserID);
            	ps.setString(i++, makeDateStr);
            	ps.setDate(i++, ubd);
            	ps.setInt(i++, seq);
            	ps.setString(i++, store);
            	ps.setString(i++, tableName);
                if (ps.executeUpdate() <= 0) {
                    System.out.println(" Exception: Delete fail");
                } else {
                    System.out.println(" update success, continue going...");
                }
            } else{
            	System.out.println(" Old record new exist. insert new...");
            	ps = con.prepareStatement(sqlString2);
            	int i = 1;
            	ps.setDate(i++, ubd);
            	ps.setInt(i++, seq);
            	ps.setString(i++, store);
            	ps.setString(i++, tableName);
            	ps.setInt(i++, recordCount);
            	ps.setInt(i++, uploadCount);
            	ps.setInt(i++, failCount);
            	ps.setString(i++, updateType);
            	ps.setString(i++, uploadResult);
            	ps.setString(i++, makeUserID);
            	ps.setString(i++, makeDateStr);
            	if (ps.executeUpdate() != 1) {
            		System.out.println(" DateShiftCommon.getErrorInfo(1011)");
            	}
            }
        } catch (SQLException e) {
            System.out.println(" DateShiftCommon.getErrorInfo(1008)");
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
            } catch (SQLException e) {
                System.out.println(" DateShiftCommon.getErrorInfo(1091)");
                e.printStackTrace();
            }
        }
        //DateShiftCommon.writeMessage(TITLE + " Log success, updateBeginDate: "
        //    + DateShiftCommon.getDateTime(ubd, "yyyy-MM-dd") + " sequenceNumber: " + seq
        //    + " recordCount: " + recordCount);
    }

    private static void updateCommulHead(Connection conn, java.sql.Date date,
        int seq) throws SQLException {
        String sqlString = "SELECT COUNT(1) FROM commul_ssmp_log"
                + " WHERE updateBeginDate=? AND sequenceNumber=?"
                + " AND storeID=?";
        PreparedStatement ps = null;
        ResultSet rs = null;
        int recCnt = 0;
        try {
            ps = conn.prepareStatement(sqlString);
            ps.setDate(1, date);
            ps.setInt(2, seq);
            ps.setString(3, STORE_ID);
            rs = ps.executeQuery();
            if (rs.next()) {
                recCnt = rs.getInt(1);
            }
            makeCommulHeadRecord(conn, STORE_ID, seq, date, "commul_ssmp_log", recCnt, "0", "0");
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (ps != null) ps.close();
                if (rs != null) rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }
        }
    }

    public boolean afterReceivingSsmpLog(SsmpLog ssmpLog, StringBuilder generatedId) {
        DbConnection conn = null;
        try {
            conn = CreamToolkit.getTransactionalConnection();

            java.sql.Date busiDate = new java.sql.Date(getBusinessDate(conn).getTime());
            int seq = 1 + getCommulMaxSequenceNumber(conn,
                new java.sql.Date(busiDate.getTime()), "commul_ssmp_log");

            ssmpLog.setId(0); // discard POS's id (although no need here)
            ssmpLog.setUpdateBeginDate(busiDate);
            ssmpLog.setSequenceNumber((short)seq);
            Integer id = (Integer)ssmpLog.insert(conn, true);
            if (generatedId != null) {
                generatedId.setLength(0);
                generatedId.append(id);
            }
            updateCommulHead(conn, busiDate, seq);
            conn.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (conn != null)
                CreamToolkit.releaseConnection(conn);
        }
    }
    //查询团购或赊账总额
    public static ArrayList getAllAmount(DbConnection conn, String sqlStr) throws SQLException {
        Server.log("getAllAmout:" + sqlStr);
        Map query = query(conn,sqlStr);
        ArrayList value = new ArrayList();
        if (query.size() != 0) {
            value.add(query.get("storeid").toString());
            if (query.get("sum(salesamount)") != null)
                value.add((HYIDouble)query.get("sum(salesamount)"));
            else if (query.get("sum(invcamount)") != null)
                value.add((HYIDouble)query.get("sum(invcamount)"));
            value.add(query.get("updateuserid").toString());
        }
        return value;
    }
    //查询团购收款支付方式
    public static ArrayList getAccdayrptPayment(DbConnection conn, String sqlStr) throws SQLException {
        Server.log(new StringBuilder().append("getAccdayrptPayment:").append(sqlStr).toString());
        Map query = query(conn, sqlStr);
        ArrayList value = new ArrayList();
        if (query.size() != 0) {
            value.add((HYIDouble)query.get("posamount"));
            value.add((HYIDouble)query.get("storeamount"));
        }
        return value;
    }

}
