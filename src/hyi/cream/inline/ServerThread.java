package hyi.cream.inline;

import com.hyi.cake.background.DateShift;
import com.hyi.cake.background.Pandian;
import hyi.cream.dac.*;
import hyi.cream.groovydac.SsmpLog;
import hyi.cream.state.GetProperty;
import hyi.cream.state.periodical.BookedInfo;
import hyi.cream.state.periodical.PeriodicalUtils;
import hyi.cream.state.wholesale.WholesaleUtils;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;

import java.io.*;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Inline server thread class.
 *
 * @author Bruce
 * @version 1.7
 */
public class ServerThread extends Thread {

    /**
     * /Bruce/1.8/2003-05-27/
     *    修改getobject protocol, 以增加查询会员资料功能。
     *
     * /Bruce/1.7/2002-04-27/
     *    增加上传代收明细帐.
     *
     * /Bruce/1.6/2002-04-10/
     *    在下传主档之前，判断是否现在正在进行营业换日，若是，或者换日失败，
     *    则禁止做主档下传。
     *
     * /Bruce/1.5/2002-04-01/
     *    1. syncTransaction(): If date shift is running, don't send any request.
     *    2. Keep log file size to at least 10 MB.
     */
    transient public static final String VERSION = "1.8";
    transient public static final String FILE_ENCODING = "UTF-8";
    transient public static final int DEFAULT_TIME_OUT = 70000;
    transient private static Object lockObject = new Object();

    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private ObjectInputStream objectIn;
    private ObjectOutputStream objectOut;
    private DacObjectOutputStream dacOut;
    private DacObjectInputStream dacIn;
    private StringBuffer buf = new StringBuffer();
    private int posNumber;
    private ArrayList listener = new ArrayList();
    //private POSStatusEvent event = new POSStatusEvent(this, posNumber, 0, false, false, "");
    private boolean enableLog = true;
    private static String connectionStatusFile;
    private boolean hasBeenWrittenConnectionStatusFile;
    private static String INVENTORY_INIT_STATUS = "1";
    private static String INVENTORY_LOADING_STATUS = "9";
    private static String INVENTORY_LOADED_STATUS = "2";
    private static String storeID;

    /**
     * Meyer/2003-02-10
     *         Get PostProcessor
     */
    private static PostProcessor pp = getPostProcessor();

    public static PostProcessor getPostProcessor() {
    //private static PostProcessor getPostProcessor() {
        PostProcessor p = null;
        boolean isValidPostProcessor = false;
        try {
            connectionStatusFile = GetProperty.getConnectionStatusFile("");
            String postProcessorClass = GetProperty.getPostProcessor("");
            if (!postProcessorClass.equals(""))
                p = (PostProcessor) (Class.forName(postProcessorClass).newInstance());
 
            isValidPostProcessor = true;
        } catch (Exception e) {
            e.printStackTrace(Server.getLogger());
        }
        
        if (!isValidPostProcessor) {
            Server.log("Invlaid PostProcessor Property! Default PostProcessor Used");
            p = new CStorePostProcessor(); //Default PostProcessor
        }    
        return p;            
    }
    
    ServerThread(Socket socket) throws InstantiationException {
        if (Server.serverExist())
            addPOSStatusListener(Server.getInstance());

        this.socket = socket;
//        log(socket.getInetAddress().getHostName() + ":" +
        if (pp == null)
            pp = getPostProcessor();
        
        try {
            socket.setSoTimeout(DEFAULT_TIME_OUT);
            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());
            dacOut = new DacObjectOutputStream(socket.getOutputStream());
            dacIn = new DacObjectInputStream(socket.getInputStream());
            //objectIn = new ObjectInputStream(new BufferedInputStream(in));
        } catch (IOException e) {
            log(e.toString());
            throw new InstantiationException();
        }
        //log(socket.getInetAddress().getHostAddress() + ":" + socket.getPort() + " is connected.");
    }

    private void log(String message) {
        if (!enableLog)
            return;

        if (posNumber == 0)
            message = "[ ] " + message;
        else
            message = "[" + posNumber + "] " + message;

        fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_LOGGING, false, false, message));
        Server.log(message);
    }

    public int getPOSNumber() {
        return posNumber;
    }

    public void addPOSStatusListener(POSStatusListener l) {
        listener.add(l);
    }

    private Iterator getPOSStatusListener() {
        return listener.iterator();
    }

    /**
     * Fire event to every POSStatusListener
     */
    private void fireEvent(POSStatusEvent e) {
        Iterator iter = getPOSStatusListener();
        while (iter.hasNext()) {
            POSStatusListener l = (POSStatusListener) iter.next();
            l.posStatusChanged(e);
        }
    }

//    private boolean putCompressedFile(String fileName) {
//
//        //OutputStream buffOut = new BufferedOutputStream(out);
//        File file = new File(fileName);
//        InputStream read = null;
//
//        if (file.isFile()) {
//            try {
//                GZIPOutputStream z = new GZIPOutputStream(socket.getOutputStream());
//                DataOutputStream zipOut = new DataOutputStream(z);
//                //out.writeInt(1);
//                //putMessage(file.getName());         // file name
//                log("Send " + file.getName());
//                read = new BufferedInputStream(new FileInputStream(file));
//                zipOut.writeInt((int) file.length()); // file size
//                byte[] all = new byte[(int) file.length()];
//                //int remainLength = read.available();
//                read.read(all);
//
//                // file content
//                //buffOut.write(all);
//                //buffOut.flush();
//                zipOut.write(all);
//                zipOut.flush();
//
//                ////for (int j = 0; j < (all.length - 1) / 4096 + 1; j++) {
//                ////    zipOut.write(all, j * 4096, Math.min(remainLength, 4096));
//                ////    zipOut.flush();
//                ////    //System.out.println("sent " + Math.min(remainLength, 4096) + " bytes");
//                ////    remainLength -= 4096;
//                ////}
//                read.close();
//                z.finish();
//                //z.flush();
//            } catch (IOException fe) {
//                log(fe.toString());
//                fe.printStackTrace();
//                if (read != null) {
//                    try {
//                        read.close();
//                    } catch (IOException e) {
//                    }
//                }
//                return false;
//            }
//        }
//        return true;
//    }

    private static String[] getDownloadSQLFile(String className) throws Exception {

        String[] retFiles = new String[2];
        java.util.Date today = new java.util.Date();
        synchronized (ServerThread.class) {

            // Check if download cache file exists, if yes, return directly
            File sqlFilePath = new File(className); 
            File recCountFilePath = new File(sqlFilePath.getAbsolutePath() + ".cnt");

            boolean filesExist = sqlFilePath.exists() && recCountFilePath.exists();
            boolean isFilesValid = false;   //最后修改时间距离当前时间10分鐘内有效
            if (filesExist) {
                long timeDifference = today.getTime() - sqlFilePath.lastModified();
                isFilesValid = (timeDifference >= 0L && timeDifference < 1000L * 60 * 10);
            }

            if (!filesExist || !isFilesValid) {
                //regenerate the download SQL file

                if (filesExist) {
                    sqlFilePath.delete();
                    recCountFilePath.delete();
                }
                
                // Invoke the DAC's getAllObjectsForPOS()
                Collection dacs = (Collection) Class.forName(className).getDeclaredMethod(
                    "getAllObjectsForPOS").invoke(null);
                if (dacs == null || dacs.size() == 0)
                    return null;

                Object dacArray[] = dacs.toArray();
                int recordCount = -1;
                if (dacArray[0] instanceof File) {
                    File sqlDumpFile = (File)dacArray[0];
                    if (!sqlDumpFile.renameTo(sqlFilePath))
                        Server.log("Failed when moving " + sqlDumpFile.getAbsolutePath() + " to " +
                                sqlFilePath.getAbsolutePath());
                    recordCount = (Integer)dacArray[1];

                } else {
                    OutputStream sqlFile = new BufferedOutputStream(new FileOutputStream(sqlFilePath));
                    for (int i = 0; i < dacArray.length; i++) {
                        DacBase dac = (DacBase)dacArray[i];
                        //System.out.println("---------------- dac : " + dac.getFieldMap());
                        try {
                            String insertString = dac.getInsertSqlStatement(true);
                            sqlFile.write(insertString.getBytes(FILE_ENCODING));
                            sqlFile.write(';');
                            sqlFile.write('\n');
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    sqlFile.close();
                }

                // create record count file
                DataOutputStream recCountFile = new DataOutputStream(new FileOutputStream(recCountFilePath));
                recCountFile.write(((recordCount != -1 ? recordCount : dacArray.length) + "").getBytes(FILE_ENCODING));
                recCountFile.close();
            }
            retFiles[0] = sqlFilePath.getAbsolutePath();
            retFiles[1] = recCountFilePath.getAbsolutePath();
            return retFiles;
        }
    }

    /**
     * Inline protocol:
     *
     *                              Client                                             Server
     *                              ------                                             ------
     *                                 |                                                  |
     *                                 |                "quit\n"                          |
     *                                 | -----------------------------------------------> |
     *
     *
     *                             sendRuok()
     *                                 |                                                  |
     *                                 |             "ruok?{Store number}\n"              |
     *                                 | -----------------------------------------------> |
     *                                 |            {POS number(int)}                     |
     *                                 | -----------------------------------------------> |
     *                                 |         "yes[max id in posdl_deltaplu]\n"        |
     *                                 |         or "no\n"                                | if store number is not the same as SC
     *                                 | <----------------------------------------------- |
     *                                 |                                                  |
     *
     *
     *  NOT YET IMPLEMENTED ->         |                                                  |
     *                                 |                "ruok?\n"                         |
     *                                 | -----------------------------------------------> |
     *                                 |            {POS number(int)}                     |
     *                                 | -----------------------------------------------> |
     *                                 |                "getzlist\n"                      | piggyback command
     *                                 | <----------------------------------------------- |
     *                                 |                {number of z requested(int)}      |
     *                                 | <----------------------------------------------- |
     * Client retrieves at most number |     {number of z(int)}                           |
     * of z requested                  | -----------------------------------------------> |
     *                                 |     {first ZReport}                              |
     *                                 | -----------------------------------------------> |
     *                                 |     {second ZReport}                             |
     *                                 | -----------------------------------------------> |
     *                                 |       . . .                                      |
     *
     *
     *                                 |                                                  |
     *  NOT YET IMPLEMENTED ->         |                "ruok?\n"                         |
     *                                 | -----------------------------------------------> |
     *                                 |            {POS number(int)}                     |
     *                                 | -----------------------------------------------> |
     *                                 |                "getshiftlist\n"                  |
     *                                 | <----------------------------------------------- |
     *                                 |                {number of shift requested(int)}  |
     *                                 | <----------------------------------------------- |
     * Client retrieves at most number |     {number of shift(int)}                       |
     * of shift requested              | -----------------------------------------------> |
     *                                 |     {first ShiftReport}                          |
     *                                 | -----------------------------------------------> |
     *                                 |     {second ShiftReport}                         |
     *                                 | -----------------------------------------------> |
     *                                 |       . . .                                      |
     *
     *
     *                             sendSyncTransaction()
     *                                 |                                                  |
     *                                 |    "synctrans\n"                                 |
     *                                 | -----------------------------------------------> |
     *                                 |    {POS number(int)}                             |
     *                                 | -----------------------------------------------> |
     *                                 |    {Max transaction number(int)}                 |
     *                                 | -----------------------------------------------> |
     *                                 |    {Begin transaction number(int)}               |  
     *                                 | -----------------------------------------------> |
     *                                 |    {End transaction number(int)}                 |  
     *                                 | -----------------------------------------------> |
     *                                 |    {Number of transactions request(int)}         | 0 if nothing to request
     *                                 | <----------------------------------------------- |
     *                                 |    {First transaction number(int)}               |
     *                                 | <----------------------------------------------- |
     *                                 |    {Second transaction number(int)}              |
     *                                 | <----------------------------------------------- |
     *                                 |       . . .                                      |
     *
     *
     *                             sendObject()                                       getObject()
     *                                 |                                                  |
     *                                 |   "putobject {class_name} [number_of_objects]\n" |
     *                                 | -----------------------------------------------> |
     *                                 |       {(boolean)}                                | true: is a DAC object, false: not a DAC object
     *                                 |       {First serialized object stream}           | Send/recevie by DacObjectOutputStream/DacObjectInputStream
     *                                 | -----------------------------------------------> |     or ObjectOutputStream/ObjectInputStream if not a DAC object
     *                                 |       {(boolean)}                                |
     *                                 |       {Second serialized object stream}          |
     *                                 | -----------------------------------------------> |
     *                                 |       . . .                                      |
     *                                 |       . . .                                      |
     *                                 |             "OK\n" or "Invalid command.\n"       |
     *                                 | <----------------------------------------------- |
     *
     * 
     *                             sendObject()                                       getObject()
     *                                 |                                                  |
     *                                 |   "putobject2\n"                                 |
     *                                 | -----------------------------------------------> |
     *                                 |   {A String object with the format:              |
     *                                 |  "classname\t                                    |
     *                                 |  "[class_name]\t[first_field]\t[second_field]\t...\n" +
     *                                 |  "[class_name]\t[first_field]\t[second_field]\t...\n" + 
     *                                 |  ...                                             |
     *                                 | -----------------------------------------------> |
     *                                 |             "OK\n" or "Invalid command.\n"       |
     *                                 | <----------------------------------------------- |
     *                              
     *
     *                             requestObjects()                                   putObject()
     *                                 |                                                  |
     *                                 |   "getobject {class_name} [arguments]\n"         | arguments is option
     *                                 | -----------------------------------------------> |
     *                                 |    {number of objects} (by an Integer object)    | -1 if server failed
     *                                 | <----------------------------------------------- |
     *                                 |                                                  | Retrieve objects by DAC's getAllObjectsForPOS(),
     *                                 |                                                  | if arguments is not null, then will get by queryObject(argument).
     *                                 |                                                  |
     *                                 |        {(boolean)}                               | true: is a DAC object, false: not a DAC object
     *                                 |        {First serialized object stream}          | Send/recevie by DacObjectOutputStream/DacObjectInputStream.
     *                                 | <----------------------------------------------- |     or ObjectOutputStream/ObjectInputStream if not a DAC object
     *                                 |                                                  |
     *                                 |        {(boolean)}                               | true: is a DAC object, false: not a DAC object
     *                                 |        {First serialized object stream}          | Send/recevie by DacObjectOutputStream/DacObjectInputStream.
     *                                 | <----------------------------------------------- |     or ObjectOutputStream/ObjectInputStream if not a DAC object
     *                                 |       . . .                                      |
     *                                 |       . . .                                      |
     *                                 |                   "OK\n"                         |
     *                                 | -----------------------------------------------> |
     *                                 |                                                  |
     *
     *
     *                             requestObjects2()                                  putObject()
     *                                 |                                                  |
     *                                 |   "getobject2 {class_name}\n"                    |
     *                                 | -----------------------------------------------> |
     *                                 |    {number of objects(int)}                      | Retrieve objects by DAC's getAllObjectsForPOS()
     *                                 | <----------------------------------------------- | if it is 0, don't send INSERT stmt file
     *                                 |   {INSERT statement file size(int)}              |
     *                                 | <----------------------------------------------- |
     *                                 |   {INSERT statement file content}                |
     *                                 | <----------------------------------------------- |
     *                                 |                   "OK\n"                         |
     *                                 | -----------------------------------------------> |
     *                                 |                                                  |
     *
     *
     *                             requestFiles()                                     putFile()
     *                                 |                                                  |
     *                                 |  "getfile {server_dir_name}\n"                   |
     *                                 | -----------------------------------------------> |
     *                                 |               {number of files}                  | -1 if server failed
     *                                 | <----------------------------------------------- |
     *                                 |   {first file name}\n                            |
     *                                 | <----------------------------------------------- |
     *                                 |   {first file size(int)}                         |
     *                                 | <----------------------------------------------- |
     *                                 |            {first file content}                  |
     *                                 | <----------------------------------------------- |
     *                                 |   {second file name}\n                           |
     *                                 | <----------------------------------------------- |
     *                                 |   {second file size(int)}                        |
     *                                 | <----------------------------------------------- |
     *                                 |            {second file content}                 |
     *                                 | <----------------------------------------------- |
     *                                 |                   . . .                          |
     *                                 | <----------------------------------------------- |
     *
     *
     *                             requestFiles()                                     putFile()
     *                                 |                                                  |
     *                                 |  "movefile {server_dir_name} {client_dir_name}\n"|
     *                                 | -----------------------------------------------> |
     *                                 |               {number of files}                  | -1 if server failed
     *                                 | <----------------------------------------------- |
     *                                 |   {file name}\n                                  |
     *                                 | <----------------------------------------------- |
     *                                 |   {file size(int)}                               |
     *                                 | <----------------------------------------------- |
     *                                 |            {first file content}                  |
     *                                 | <----------------------------------------------- |
     *                                 |   {file name}\n                                  |
     *                                 | <----------------------------------------------- |
     *                                 |   {file size(int)}                               |
     *                                 | <----------------------------------------------- |
     *                                 |            {second file content}                 |
     *                                 | <----------------------------------------------- |
     *                                 |                   . . .                          |
     *                                 | <----------------------------------------------- |
     *
     *
     *         DacObjectOutputStream.writeObject(DacBase dac)              DacObjectInputStream.readObject()
     *                                 |          {DAC classname(String)}                 |
     *                                 | -----------------------------------------------> |
     *                                 |          {number of fields(int)}                 |
     *                                 | -----------------------------------------------> |
     *                                 |    {first field name(String)}                    |
     *                                 | -----------------------------------------------> |
     *                                 |    {first field value classname(String)}         |
     *                                 | -----------------------------------------------> |
     *                                 |    {first field value(String)}                   | if it is a Date/Time/Timestamp, send its getTime() as value; otherwise send its toString()
     *                                 | -----------------------------------------------> | if it is a null value, send String("^null^")
     *                                 |    {second field name(String)}                   |
     *                                 | -----------------------------------------------> |
     *                                 |    {second field value classname(String)}        |
     *                                 | -----------------------------------------------> |
     *                                 |    {second field value(String)}                  |
     *                                 | -----------------------------------------------> |
     *                                 |        . . .                                     |
     *                                 | -----------------------------------------------> |
     *
     *                           countDoingInventory()                          processCountDoingInventory()
     *                                 |                                                  |
     *                                 |            "countDoingInventory\n"               |
     *                                 | -----------------------------------------------> |
     *                                 |         "-1,0,1,2..(int)"(-1)mean fail           |
     *                                 | <----------------------------------------------- |
     *
     *                           turnSingleUlTable()                            processTurnSingleUlTable()
     *                                 |                                                  |
     *                                 |            "turnSingleUlTable\n"                 |
     *                                 | -----------------------------------------------> |
     *                                 |            {ulTableName(String}                  |
     *                                 | -----------------------------------------------> |
     *                                 |            "OK\n" or "Invalid command\n"         |
     *                                 | <----------------------------------------------- |
     *
     *                           countPosul_inventoryRec()                       processCountPosul_inventoryRec()
     *                                 |                                                  |
     *                                 |            "countPosul_inventoryRec\n"           |
     *                                 | -----------------------------------------------> |
     *                                 |            {POS number(int)}                     |
     *                                 | -----------------------------------------------> |
     *                                 |            "-1,0,1,2,...(int)" (-1)mean fail     |
     *                                 | <----------------------------------------------- |
     *
     *                           getAccountDateOfZ()                             processGetAccountDateOfZ()
     *                                 |                                                  |
     *                                 |            "getAccountDateOfZ\n"                 |
     *                                 | -----------------------------------------------> |
     *                                 |            {POS number(int)}                     |
     *                                 | -----------------------------------------------> |
     *                                 |            {zSequence.intValue(int)}             |
     *                                 | -----------------------------------------------> |
     *                                 |           "accDateStr(String)\n"(null)mean fail  |
     *                                 | <----------------------------------------------- |
     *
     *                           updateHead                                      processUpdateHead()
     *                                 |                                                  |
     *                                 |    "[updateHead},[pos number],[tableName],[count]"   |
     *
     *                           needDown                                      processNeedDown()
     *                                 |                                                  |
     *                                 |         "[needDown],[pos number]"                |
     *
     * package: hyi.cream.inline
     *
     * +--------------+                   +-------------------------------------------------+
     * |<<singleton>> | 1            0..* |                                                 |
     * |    Server    |-------------------|     ServerThread                                |
     * |--------------|                   |-------------------------------------------------|
     * |--------------|                   |-------------------------------------------------|
     * |+startup()    |                   |+run()                                           |
     * |+static boolean serverExist()     |-getMessage()                                    |
     * |+static void log()                |-Object[] getObject()                            |
     * +--------------+                   |-putObject(Object)                               |
     *                                    |-putObject(Object[])                             |
     *                                    |+void addPOSStatusListener(POSStatusListener l)  |
     *                                    |-Iterator getPOSStatusListener()                 |
     *                                    |-void fireEvent(POSStatusEvent e)                |
     *                                    |-void processCountDoingInventory()               |
     *                                    |-void processTurnSingleUlTable()                 |
     *                                    |-void processGetAccountDateOfZ()                 |
     *                                    +-------------------------------------------------+
     *
     * +---------------------------------+
     * |        Client                   |
     * |---------------------------------|
     * |---------------------------------|
     * |+boolean isConnected()           |
     * |+connect()                       |
     * |+closeConnection()               |
     * |+boolean isConnected()           |
     * |+void setConnected(boolean c)    |
     * |+void sendRuok()                 |
     * |+sendMessage(String)             |
     * |+String getMessage()             |
     * |+sendObject(Object)              |
     * |+sendObject(Object[])            |
     * |+void enterCommandLineMode()     |
     * |+void processCommand(String line) throws ClientCommandException
     * |+Object[] requestObjects(String) |
     * |-uploadDac()                     |
     * |-downloadDac()                   |
     * |-int countDoingInventory()       |
     * |-String turnSingleUlTable()      |
     * |-String getAccountDateOfZ()      |
     * +---------------------------------+
     *
     *
     *
     * +------------------------+
     * |         DacBase        |
     * |------------------------|
     * |------------------------|
     * |+static Iterator getMultipleObjects(Class, String, Map) <-----+
     * |+deleteAll()            |                                     |
     * |+backupAll()            |                                     |
     * |+restoreAll()           |                                     |
     * |                        |                                     |
     * +------------------------+                                     |
     *                                                                |
     *                                                                |
     * *All DACs for download                                         |
     * +------------------------+                                     |
     * |          PLU           |                                     |
     * |------------------------|                                     |
     * |------------------------|                          <<invoke>> |
     * |+static Collection getAllObjectsForPOS() ---------------------+
     * |                        |
     * |                        |
     * +------------------------+
     *
     *
     * +--------------------------+
     * |       Transaction        |
     * |--------------------------|
     * |--------------------------|
     * |                          |
     * |+static int getMaxTransactionNumber()
     * |+static int[] getTransactionLost(int posNumber, int maxNumberShouldHave)
     * |+void addLineItemSimpleVersion(LineItem lineItem)
     * |+Transaction cloneForSC() |
     * |                          |
     * |                          |
     * +--------------------------+
     *
     *
     * +--------------------------+
     * |         ZReport          |
     * |--------------------------|
     * |--------------------------|
     * |+ZReport cloneForSC()     |
     * |                          |
     * |                          |
     * +--------------------------+
     *
     * +--------------------------+
     * |       ShiftReport        |
     * |--------------------------|
     * |--------------------------|
     * |+ShiftReport cloneForSC() |
     * |                          |
     * |                          |
     * +--------------------------+
     *
     * +--------------------------+        +--------------------------+
     * |  DacObjectOutputStream   |        |  DacObjectInputStream    |
     * |--------------------------|        |--------------------------|
     * |--------------------------|        |--------------------------|
     * |+void writeObject(DacBase obj)     |+DacBase readObject()     |
     * |                          |        |                          |
     * |                          |        |                          |
     * +--------------------------+        +--------------------------+
     *
     * ServerUI:
     * . hyi.cream.inline.ServerUI extends JFrame
     * . contains a Swing table and a text pane
     * //. contains a toolbar: getZList, getShiftList
     *
     *
     *  +-----+------+------+-------+--------------------------------------------+
     *  | POS |Status| Send |Receive|                 Message                    |
     *  +-----+------+------+-------+------------------------------------------+-+
     *  |  1  |  O   |  O   |   O   |                                          |^|
     *  +-----+------+------+-------+------------------------------------------| |
     *  |  2  |  O   |  O   |   O   |                                          | |
     *  +-----+------+------+-------+------------------------------------------| |
     *  |  3  |  O   |  O   |   O   |                                          | |
     *  +-----+------+------+-------+------------------------------------------| |
     *  |  4  |  O   |  O   |   O   |                                          | |
     *  +-----+------+------+-------+------------------------------------------| |
     *  |  5  |  O   |  O   |   O   |                                          |v|
     *  +-----+------+------+-------+------------------------------------------+-+
     *  Log:
     *  +----------------------------------------------------------------------+-+
     *  |YYYY/MM/DD HH:MM:SS XXXXXXXXXXXXXXXXXXXXXXXXXXX                       |^|
     *  |YYYY/MM/DD HH:MM:SS XXXXXXXXXXXXXXXXXXXXXXXXXXX                       | |
     *  |YYYY/MM/DD HH:MM:SS XXXXXXXXXXXXXXXXXXXXXXXXXXX                       | |
     *  |YYYY/MM/DD HH:MM:SS XXXXXXXXXXXXXXXXXXXXXXXXXXX                       | |
     *  |                                                                      | |
     *  |                                                                      | |
     *  |                                                                      | |
     *  |                                                                      | |
     *  |                                                                      | |
     *  |                                                                      | |
     *  |                                                                      |v|
     *  +----------------------------------------------------------------------+-+
     *
     *
     *
     *
     *                             +--------------------+
     *                             |                    |
     *                             |DisplayControlThread|
     *                             +--------------------+
     *                                       |
     *                                       |
     *  +--------------+           +-------------------+ <<forward  +--------------+              +----------------+
     *  |              |           |                   |   event>>  |              | 1       0..* |                |
     *  |  JTable      |-----------|POSStatusTableModel|<-----------|    Server    |--------------|  ServerThread  |
     *  +--------------+           +-------------------+            +--------------+              +----------------+
     *                                      |                       /       |                             |
     *                                      _                      /        _                             |<<instantiate>>
     *                                      V                     /         V                             v
     *                             +-------------------+         /  +-------------------+          +--------------+
     *                             |                   |        /   |                   |          |              |
     *                             | POSStatusListener |       /    | POSStatusListener |          |POSStatusEvent|
     *                             +-------------------+      /     +-------------------+          +--------------+
     *                                                       /
     *  +--------------+           +-------------------+    /
     *  |              |           |                   |<- /
     *  |  JList       |-----------|   LogListModel    |
     *  +--------------+           +-------------------+
     *                                      |
     *                                      _
     *                                      V
     *                             +-------------------+
     *                             |                   |
     *                             | POSStatusListener |
     *                             +-------------------+
     *
     */
    public void run() {
        String command;
        String className;
        String serverDir;
        int numberOfObjects;
        StringTokenizer t;
        String[] commandList = { "ruok", "pubobject", "getobject", "getfile", "movefile", "synctrans" };

        while (true) {
            command = getMessage();
            if (Server.debugLog)
            {
                if (!command.startsWith("ruok?")) // 不要log ruok, 太多了
                    log("got:" + command);
            }

            if (command.equals("quit"))
                break;

            // Filter out the garbage in the front.
            // 因为有时候会发生这种状况，不知何因？
            // e.g.,
            // 2002/12/26 15:01:07 [1] RX:^@^@^@^A^@^@^CBputobject hyi.cream.dac.Transaction 1
            for (int i = 0; i < commandList.length; i++) {
                int idx;
                if ((idx = command.indexOf(commandList[i])) > 0) {
                    command = command.substring(idx);
                    break;
                }
            }

            // "ruok?"
            if (command.startsWith("ruok?")) {
                processRuok(command);

            // "putobject2\n {String_object}"
            } else if (command.startsWith("putobject2")) {
                // Now there are only two kinds of DAC sent by this protocol: Transaction(
                // which contains LineItem objects) and Inventory.
                log("RX: " + command);
                String objectsInString = getObject2();
                if (objectsInString != null) {
                    //if (objectsInString.startsWith("hyi.cream.dac.Transaction")) {
                    //    Object[] dacObjects = Transaction.constructFromTabSeperatedData(
                    //        objectsInString);
                    //    processObjectGot(dacObjects);
                    if (objectsInString.startsWith("hyi.cream.dac.Inventory")) {
                        Inventory.constructFromTabSeperatedData(this, objectsInString);
                    }
                    putMessage("OK");
                    log("TX: OK");
                }

            // "putobject {class_name} [number_of_objects]"
            } else if (command.startsWith("putobject")) {
                t = new java.util.StringTokenizer(command, " ");
                t.nextToken(); // skip "pubobject"

                // get {class_name}
                if (!t.hasMoreTokens()) { // must give a object's class name
                    log("Command error: " + command);
                    continue;
                }
                className = t.nextToken();

                // get [number_of_object]
                if (t.hasMoreTokens()) {
                    try {
                        numberOfObjects = Integer.parseInt(t.nextToken());
                    } catch (NumberFormatException e) {
                        log("Command error: " + command);
                        continue;
                    }
                } else
                    numberOfObjects = 1; // default number of object is 1 if not given

                log("RX:" + command);
                Object[] object = getObject(className, numberOfObjects);
                if (object != null) {
                    String r = processObjectGot(object);
                    putMessage(r);
                    log("TX:" + r);
                }

            } else if (command.startsWith("getobject2")) {
                t = new java.util.StringTokenizer(command, " ");
                t.nextToken(); // skip "getobject"

                // get {class_name}
                if (!t.hasMoreTokens()) { // must give a object's class name
                    putObject(new Integer(-1));
                    log("Command error: " + command);
                    continue;
                }
                className = t.nextToken();

                log("RX:" + command);
                try {
                    if (!pp.beforeDownloadingMaster()) { // cannot download now
                        log("Cannot download master now. Date shift process may be running now or failed.");
                        out.writeInt(-1);
                        continue;
                    }

                    //Bruce/20030404
                    String cacheDF = GetProperty.getCacheDownloadFile("no");
                    if (cacheDF.equalsIgnoreCase("yes")) {
                        System.out.println("-----------------getDownloadSQLFile : " + className);
                        String[] sqlFilePath = getDownloadSQLFile(className);
                        if (sqlFilePath == null) {
                            out.writeInt(0);
                            if (!getMessage().equals("OK"))
                                log("Didn't get an OK on getobjects");
                        } else {
                            // send record count
                            InputStream recCountFile = new FileInputStream(sqlFilePath[1]);
                            byte[] recCnt = new byte[(int) recCountFile.available()];
                            recCountFile.read(recCnt);
                            recCountFile.close();
                            int recCount = Integer.parseInt(new String(recCnt));
                            out.writeInt(recCount);
                            log("Send record count: " + recCount);

                            // then send compressed file content
                            if (!putFile(sqlFilePath[0])) {
                                log("Command error: " + command);
                            } else {
                                //2010-07-18 setup long timeout even in master caching
                                socket.setSoTimeout(Server.inlineLongTimeout);
                                if (!getMessage().equals("OK"))
                                    log("Didn't get an OK on getobjects");
                                socket.setSoTimeout(DEFAULT_TIME_OUT);
                            }
                        }

                    } else {

                        // invoke the DAC's getAllObjectsForPOS()
                        Collection dacs =
                            (Collection) Class.forName(className).getDeclaredMethod(
                                "getAllObjectsForPOS",
                                new Class[0]).invoke(
                                null,
                                new Object[0]);
                        if (dacs == null || dacs.size() == 0) {
                            out.writeInt(0);
                            if (!getMessage().equals("OK"))
                                log("Didn't get an OK on getobjects");
                        } else {
                            Object dacArray[] = dacs.toArray();
                            String sqlFilePath = "insert" + getPOSNumber() + ".sql";

                            if (dacArray[0] instanceof File) {
                                File sqlDumpFile = (File)dacArray[0];
                                if (!sqlDumpFile.renameTo(new File(sqlFilePath)))
                                    Server.log("Failed when moving " + sqlDumpFile.getAbsolutePath() + " to " +
                                            sqlFilePath);
                                int recordCount = (Integer)dacArray[1];
                                out.writeInt(recordCount);

                            } else {
                                out.writeInt(dacArray.length);

                                OutputStream sqlFile = new BufferedOutputStream(new FileOutputStream(sqlFilePath));

                                for (int i = 0; i < dacArray.length; i++) {
                                    DacBase dac = (DacBase)dacArray[i];

                                    String insertString = dac.getInsertSqlStatement(true);
                                    //System.out.println(insertString);
                                    try {
                                        sqlFile.write(insertString.getBytes(FILE_ENCODING));
                                        sqlFile.write(';');
                                        sqlFile.write('\n');
                                        sqlFile.flush();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                sqlFile.close();
                            }
                            //log("Finished at make sqlFile.");
                            //log("Begin transfer the sqlFiel.");
                            //System.out.println("Send " + sqlFilePath);
                            if (!putFile(sqlFilePath)) {
                                log("Command error: " + command);
                            } else {
                                socket.setSoTimeout(Server.inlineLongTimeout);
                                if (!getMessage().equals("OK"))
                                    log("Didn't get an OK on getobjects");
                                socket.setSoTimeout(DEFAULT_TIME_OUT);
                            }
                        }
                    }

                } catch (Exception e) {
                    log("Failed on: " + command);
                    e.printStackTrace(Server.getLogger());
                    Server.getLogger().flush();
                    
                    //Bruce/20030717
                    //putObject(new Integer(-1));
                    try {
                        out.writeInt(-1);
                    } catch (IOException e1) {
                        e1.printStackTrace(Server.getLogger());
                    }
                }

                // "getobject {class_name}"
            } else if (command.startsWith("getobject")) {
                t = new java.util.StringTokenizer(command, " ");
                t.nextToken(); // skip "getobject"

                // get {class_name}
                if (!t.hasMoreTokens()) { // must give a object's class name
                    putObject(new Integer(-1));
                    log("Command error: " + command);
                    continue;
                }
                className = t.nextToken();

                ArrayList args = new ArrayList();
                while (t.hasMoreTokens()) {
                    String s = t.nextToken();
                    if (!s.equals("null"))
                        args.add(new String(s));
                }

                //System.out.println(args.size());

                Class argTypes[] = new Class[args.size()];
                for (int i = 0; i < args.size(); i++) {
                    try {
                        argTypes[i] = Class.forName("java.lang.String");
                    } catch (ClassNotFoundException e) {
                    }
                }
                // because there are too many log messages on "Message"...
                if (className.equals("hyi.cream.dac.Message"))
                    enableLog = false;

                log("RX:" + command);

                // invoke the DAC's getAllObjectsForPOS() or queryObject()
                Collection dacs = null;
                boolean isPutSuccess = true;
                try {
                    if (args.size() == 0) {
                        dacs = (Collection) Class.forName(className).getDeclaredMethod(
                            "getAllObjectsForPOS").invoke(null);
                    } else {
                        dacs = (Collection) Class.forName(className).getDeclaredMethod(
                            "queryObject", argTypes).invoke(null, args.toArray());
                    }
                    if (dacs == null || dacs.isEmpty()) {
                        isPutSuccess = putObject(new Integer(0));
                        isPutSuccess = isPutSuccess && putObject(new Object[0]);
                    } else  {
                        isPutSuccess = putObject(dacs.size());
                        isPutSuccess = isPutSuccess && putObject(dacs.toArray());
                    }
                } catch (Exception e) {
                    enableLog = true;
                    putObject(-1);
                    log("Failed on: " + command);
                    log(e.toString());
                    e.printStackTrace();
                    isPutSuccess = false;
                }

                /*
                 * Meyer / 2003-02-19 /
                 *         if meeting getPLUPriceChange, invoke afterDonePluPriceChange()
                 *
                 */
                if (className.equals("hyi.cream.dac.PLUPriceChange")) {
                    String pluPriceChangeID = "";
                    String maxID = "";
                    Iterator iter = dacs.iterator();
                    while (iter.hasNext()) {
                        pluPriceChangeID = ((PLU) iter.next()).getPluPriceChangeID();
                        if (pluPriceChangeID.compareToIgnoreCase(maxID) > 0) {
                            maxID = pluPriceChangeID;
                        }
                    }
                    if (dacs.size() > 0) {
                        pp.afterDonePluPriceChange(maxID, String.valueOf(this.getPOSNumber()), isPutSuccess);
                    }
                }

                enableLog = true;

                // "getfile {class_name}"
            } else if (command.startsWith("updateobject")) {
                t = new java.util.StringTokenizer(command, " ");
                t.nextToken(); // skip "getobject"

                // get {class_name}
                if (!t.hasMoreTokens()) { // must give a object's class name
                    putObject(new Integer(-1));
                    log("Command error: " + command);
                    continue;
                }
                className = t.nextToken();

                ArrayList args = new ArrayList();
                while (t.hasMoreTokens()) {
                    String s = t.nextToken();
                    if (!s.equals("null"))
                        args.add(new String(s));
                }

                //System.out.println(args.size());

                Class argTypes[] = new Class[args.size()];
                for (int i = 0; i < args.size(); i++) {
                    try {
                        argTypes[i] = Class.forName("java.lang.String");
                    } catch (ClassNotFoundException e) {
                    }
                }
                // because there are too many log messages on "Message"...
                if (className.equals("hyi.cream.dac.Message"))
                    enableLog = false;

                log("RX:" + command);

                // invoke the DAC's getAllObjectsForPOS() or queryObject()
                int dacs = 0;
                boolean isPutSuccess = true;
                try {
                    dacs = (Integer) Class.forName(className).getDeclaredMethod(
                        "updateObject", argTypes).invoke(null, args.toArray());
                    if (dacs == 0) {
                        isPutSuccess = putObject(new Integer(0));
                        isPutSuccess = isPutSuccess && putObject(new Object[0]);
                    } else  {
                        isPutSuccess = putObject(dacs);
                        isPutSuccess = isPutSuccess && putObject(dacs);
                    }
                } catch (Exception e) {
                    enableLog = true;
                    putObject(-1);
                    log("Failed on: " + command);
                    log(e.toString());
                    e.printStackTrace();
                    isPutSuccess = false;
                }

                enableLog = true;

                // "getfile {class_name}"
            } else if (command.startsWith("getfile") || command.startsWith("movefile")) {
                t = new java.util.StringTokenizer(command, " ");
                t.nextToken(); // skip "getobject"

                // get {class_name}
                if (!t.hasMoreTokens()) { // must give a object's class name
                    putObject(new Integer(-1));
                    log("Command error: " + command);
                    continue;
                }
                serverDir = t.nextToken();
                log(command);
                if (!putFiles(serverDir, command.startsWith("movefile"))) {
                    log("Command error: " + command);
                    continue;
                }

                // "synctrans"
            } else if (command.startsWith("synctrans")) {
                syncTransaction();

                // "countDoingInventory"
            } else if (command.startsWith("countDoingInventory")) {
                processCountDoingInventory();

                // "turnSingleUlTable"
            } else if (command.startsWith("turnSingleUlTable")) {
                synchronized(lockObject) {
                    processTurnSingleUlTable();
                }

                // "countPosul_inventoryRec"
            } else if (command.startsWith("countPosul_inventoryRec")) {
                processCountPosul_inventoryRec();

                // "getAccountDateOfZ"
            } else if (command.startsWith("getAccountDateOfZ")) {
                processGetAccountDateOfZ();

            // 更新commdl_head
            } else if (command.startsWith("updateHead")) {
                processUpdateHead(command);
            } else if (command.startsWith("needDown")) {
                processNeedDown(command);
            } else if (command.startsWith("getBookableSubIssns")) {
                processBookableSubIssns(command);
            } else if (command.startsWith("getBookedInfos")) {
                processBookedInfos(command);
            } else if (command.startsWith("getReturnBookInfos")) {
                processReturnBookInfos(command);
            } else if (command.startsWith("getWholesales")) {
                processWholesales(command);
            } else {
                log("RX:" + command);
                putMessage("Invalid command.");
            }
        }

        try {
            if (out != null)
                out.close();
            if (in != null)
                in.close();
            if (socket != null) {
                Server.log("socket close: ip=" + socket.getInetAddress().getHostAddress() + ", port=" + socket.getPort());
                socket.close();
            }
        } catch (IOException ex) {
        }
        if (posNumber != 0) {
            setPOSConnectionState(posNumber, false);
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_DISCONNECTED, false, false, ""));
        }
        log("POS " + posNumber + " is disconnected.");
        try {
            Server.getInstance().modifyThreadCount(-1);
            //log("---------- Thread Count : " + Server.getInstance().getThreadCount());
        } catch (Exception e) {}
//        Server.shrinkLogFiles();
    }

    /**
     * Get a command string from client.
     */
    private String getMessage() {
        try {
            int c;

            //System.out.println("Ready to read");

            // read one line
            buf.setLength(0);
            while ((c = in.read()) != '\n') {
                if (c == -1)
                    return "quit";
                buf.append((char) c);
            }

            fireEvent(
                new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, true, buf.toString()));
            //System.out.println("Received:" + buf.toString());
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));

            return buf.toString();
        } catch (IOException e) {
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
            e.printStackTrace(Server.getLogger());
            return "quit";
        }
    }

    public void putMessage(String str) {
        try {
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, true, false, str));
            out.write(str.getBytes(FILE_ENCODING));
            out.write('\n');
            out.flush();
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
        } catch (IOException e) {
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
            e.printStackTrace(Server.getLogger());
        }
    }

    /**
     * Get objects from client.
     */
    private Object[] getObject(String className, int numberOfObjects) {
        //log("putobject " + className + " " + numberOfObjects);

        try {
            Object[] object = new Object[numberOfObjects];

            if (numberOfObjects > 0)
                fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, true,
                    "RX:" + className));

            for (int i = 0; i < numberOfObjects; i++) {
                if (objectIn == null)
                    //objectIn = new ObjectInputStream(new BufferedInputStream(in)); don't use buffered!
                    objectIn = new ObjectInputStream(in);

                boolean isDAC = objectIn.readBoolean();
                if (isDAC)
                    object[i] = dacIn.readObject();
                else
                    object[i] = objectIn.readObject();

                if (object[i] != null)
                    log("RX:" + object[i].toString());
            }
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));

            return object;
            // NOTE: caller has to call the last putMessage("OK") after finishing its work for
        } catch (Exception e) {
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
            e.printStackTrace(Server.getLogger());
            return null;
        }
    }

    /**
     * Get objects from client by "putobject2" protocol in a String object.
     */
    private String getObject2() {
        try {
            if (objectIn == null)
                //objectIn = new ObjectInputStream(new BufferedInputStream(in)); don't use buffered!
                objectIn = new ObjectInputStream(in);
            String objects = (String)objectIn.readObject();

            if (objects != null && objects.length() != 0) {
                fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED,
                    false, true, "RX: " + objects.substring(0, 25) + "..."));
                log("TX: " + objects.substring(0, 25) + "...");
                fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED,
                    false, false, ""));
            }

            return objects;
            // NOTE: caller has to call the last putMessage("OK") after finishing its work for
        } catch (Exception e) {
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED,
                false, false, ""));
            e.printStackTrace(Server.getLogger());
            return null;
        }
    }


    private boolean putObject(Object object) {
        boolean success = true;
        try {
            if (objectOut == null)
                objectOut = new ObjectOutputStream(new BufferedOutputStream(out));

            fireEvent(
                new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, true, false, "TX:" + object));
            if (object instanceof DacBase) {
                dacOut.writeObject((DacBase) object);
                if (!getMessage().equals("OK"))
                    throw new IOException("putObject(Object): Didn't get an OK on sending object");
            } else {
                objectOut.writeObject(object);
                objectOut.flush();
            }
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
            log("TX:" + object.toString());

        } catch (Exception e) {
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
            e.printStackTrace(Server.getLogger());
            log(e.toString());

            success = false;
        }

        return success;
    }

    private boolean putObject(Object[] object) {
        boolean success = true;
        try {
            if (objectOut == null)
                objectOut = new ObjectOutputStream(out);

            if (object.length > 0)
                fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, true,
                        false, "TX:" + object[0].getClass().getName()));
            for (int i = 0; i < object.length; i++) {
                if (object[i] instanceof DacBase) {
                    // log("TX:" + i + ":" + object[i]);
                    objectOut.writeBoolean(true);        // means DAC
                    objectOut.flush();                   // you must be flush() here, or something gotta be wrong
                    dacOut.writeObject((DacBase) object[i]);
                } else {
                    objectOut.writeBoolean(false);       // means non-DAC
                    objectOut.writeObject(object[i]);
                    objectOut.flush();
                }
            }
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
            if (object.length > 0)
                log("TX:" + object.length + " " + object[0].getClass().getName() + " objects.");
            if (!getMessage().equals("OK")) {
                throw new IOException("putObject(Object[]): Didn't get an OK on sending object");
            } else {
                log("RX:OK");
            }
        } catch (Exception e) {
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
            e.printStackTrace(Server.getLogger());

            log(e.toString());
            success = false;
        }
        return success;
    }

    public String processObjectGot(Object[] object) {
        String result = "OK";
        if (object[0] instanceof ShiftReport) {
            if (!pp.afterReceivingShiftReport((ShiftReport) object[0])) {
                // error handling...
            }
        } else if (object[0] instanceof ShiftEx) {
            if (!pp.afterReceivingShiftEx(object)) {
                // error handling...
            }
        } else if (object[0] instanceof ZReport) {
            StringBuffer accdateStrBuf = new StringBuffer();
            if (!pp.afterReceivingZReport((ZReport) object[0], accdateStrBuf)) {
                // error handling...
            } else {
                result += accdateStrBuf.toString();
            }
        } else if (object[0] instanceof ZEx) {
            if (!pp.afterReceivingZEx(object)) {
                // error handling...
            }
        } else if (object[0] instanceof Transaction) {
            if (!pp.afterReceivingTransaction(object)) {
                result = "ERROR";
            }
        } else if (object[0] instanceof DepSales) {
            if (!pp.afterReceivingDepSales(object)) {
                // error handling...
            }
        } else if (object[0] instanceof DaishouSales) {
            if (!pp.afterReceivingDaishouSales(object)) {
                // error handling...
            }
        } else if (object[0] instanceof DaiShouSales2) {
            if (!pp.afterReceivingDaiShouSales2(object)) {
                // error handling...
            }
        } else if (object[0] instanceof CashForm) {
            if (!pp.afterReceivingCashForm(object)) {
                // error handling...
            }
        } else if (object[0] instanceof Inventory) {
            if (!pp.afterReceivingInventory(object)) {
                // error handling...
            }
        } else if (object[0] instanceof Attendance) {
            if (!pp.afterReceivingAttendance(object)) {
                // error handling...
            }
        } else if (object[0] instanceof Attendance1) {
            if (!pp.afterReceivingAttendance1(object)) {
                // error handling...
            }
        } else if (object[0] instanceof HistoryTrans) {
            if (!pp.afterReceivingHistoryTrans(object)) {
                // error handling...
            }
        } else if (object[0] instanceof Version) {
            if (!pp.afterReceivingPosVersion(object)) {
                // error handling...
            }
        } else if (object[0] instanceof SsmpLog) {
            StringBuilder id = new StringBuilder();
            if (!pp.afterReceivingSsmpLog((SsmpLog)object[0], id)) {
                // error handling...
            } else {
                result += id.toString();
            }
        }
        return result;
    }

    private boolean putFiles(String serverDir, boolean moveFiles) {
        try {
            File dir = new File(serverDir);
            if (!dir.exists() || !dir.isDirectory()) {
                log(dir.toString() + " doesn't exist or isn't a directory.");
                //putObject(new Integer(-1));
                //Bruce/20071031 如果目錄找不到，就當作找不到東西，不要視為失敗
                out.writeInt(0);
                return true;
            }
            //OutputStream buffOut = new BufferedOutputStream(out);
            File[] allFiles = dir.listFiles();
            out.writeInt(allFiles.length); // number of files
            //putObject(new Integer(allFiles.length));    // number of files
            for (int i = 0; i < allFiles.length; i++) {
                File file = allFiles[i];
                if (file.isFile()) {
                    try {
                        putMessage(file.getName()); // file name
                        InputStream read = new BufferedInputStream(new FileInputStream(file));
                        log("Send " + file.getName() + "(size=" + read.available() + ")");
                        out.writeInt(read.available()); // file size
                        //putObject(new Integer(read.available()));   // file size
                        if (read.available() == 0)
                            continue;
                        byte[] all = new byte[read.available()];
                        int remainLength = read.available();
                        read.read(all);
                        // file content
                        //buffOut.write(all);
                        //buffOut.flush();
                        for (int j = 0; j < (all.length - 1) / 4096 + 1; j++) {
                            out.write(all, j * 4096, Math.min(remainLength, 4096));
                            out.flush();
                            //System.out.println("sent " + Math.min(remainLength, 4096) + " bytes");
                            remainLength -= 4096;
                        }
                        read.close();
                        if (moveFiles)
                            file.delete();
                    } catch (IOException fe) {
                        log(fe.toString());
                        fe.printStackTrace();
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            log(e.toString());
            return false;
        }
        return true;
    }

    private boolean putFile(String fileName) {
        //OutputStream buffOut = new BufferedOutputStream(out);
        File file = new File(fileName);

        if (file.isFile()) {
            try {
                //out.writeInt(1);
                //putMessage(file.getName());         // file name
                log("Send " + file.getName());
                InputStream read = new BufferedInputStream(new FileInputStream(file));
                out.writeInt(read.available()); // file size
                byte[] all = new byte[read.available()];
                int remainLength = read.available();
                read.read(all);
                // file content
                //buffOut.write(all);
                //buffOut.flush();
                for (int j = 0; j < (all.length - 1) / 4096 + 1; j++) {
                    out.write(all, j * 4096, Math.min(remainLength, 4096));
                    out.flush();
                    //System.out.println("sent " + Math.min(remainLength, 4096) + " bytes");
                    remainLength -= 4096;
                }
                read.close();
            } catch (IOException fe) {
                log(fe.toString());
                fe.printStackTrace();
                return false;
            }
        }
        return true;
    }

    private void syncTransaction() {
        try {

//            //Bruce/2002-04-01
//            // If date shift is running, don't send any request
//            if (!pp.canSyncTransaction()) {
//                out.writeInt(0);
//                if (Server.debugLog)
//                    log("Cannot syncTransaction now, date shift is running.");
//                return;
//            }

            int posNumber = in.readInt();
            if (Server.debugLog)
                log("synctrans> posNumber=" + posNumber);

            int maxTransactionNumberInPOS = in.readInt();
            if (Server.debugLog)
                log("synctrans> maxTrans=" + maxTransactionNumberInPOS);

            int beginTransactionNumber = in.readInt();
            if (Server.debugLog)
                log("synctrans> beginTrans=" + beginTransactionNumber);
                
            int endTransactionNumber = in.readInt();
            if (Server.debugLog)
                log("synctrans> endTrans=" + endTransactionNumber);
          
            //int maxTransactionNumberInSC = Transaction.getMaxTransactionNumber(posNumber);
            int[] lostNumbers = Transaction.getTransactionLost(posNumber, maxTransactionNumberInPOS,
                    beginTransactionNumber, endTransactionNumber);
            if (lostNumbers == null) {
                out.writeInt(0);
                if (Server.debugLog)
                    log("synctrans> No lostNumber. Over.");
                return;
            }
            if (lostNumbers.length > 2000) {
                out.writeInt(2000);
                for (int i = (lostNumbers.length - 2000); i < lostNumbers.length; i++) {
                    if (lostNumbers[i] == 0)
                            continue;
                    out.writeInt(lostNumbers[i]);
                }
                // Bruce/20071031/ 只log起始和結束交易序號
                int beginNo = lostNumbers[lostNumbers.length - 2000];
                int endNo = lostNumbers[lostNumbers.length - 1];
                log("Request transaction: " + beginNo + " -> " + endNo);
            } else {
                out.writeInt(lostNumbers.length);
                StringBuffer logMessage = new StringBuffer(
                    "Request transaction(" + lostNumbers.length + "): ");
                for (int i = 0; i < lostNumbers.length; i++) {
                    // Bruce/20071031/ 只log前20個交易序號, 怕太冗長
                    if (i < 20) {
                        if (i > 0) logMessage.append(',');
                        logMessage.append(lostNumbers[i]);
                    } else if (i == 20) {
                        logMessage.append(",...");
                    }
                    out.writeInt(lostNumbers[i]);
                }
            }
            if (Server.debugLog)
                log("synctrans> over.");
        } catch (IOException e) {
            log(e.toString());
        }
    }

    private void processRuok(String command) {
        try {
            posNumber = in.readInt();

            if (!hasBeenWrittenConnectionStatusFile) {
                setPOSConnectionState(posNumber, true);
                hasBeenWrittenConnectionStatusFile = true;
            }

            /*
            if (objectIn == null)
                objectIn = new ObjectInputStream(in);
            posNumber = ((Integer)objectIn.readObject()).intValue();
            System.out.println(((String)objectIn.readObject()).toString());
            System.out.println(((java.util.Date)objectIn.readObject()).toString());
            //System.out.println(((java.math.HYIDouble)objectIn.readObject()).toString());
            */

            //log("pos=" + posNumber);
            //putMessage("yes");

            // Retrieve max priceChgID from posdl_deltaplu, if got something,
            // send back it to POS.
//            if ()
            String maxPriceChangeID = PLUPriceChange.getMaxPriceChangeVersion();
//            String maxPriceChangeID = null;

            String storeNo = command.substring(5);
            if (storeNo != null && storeNo.length() > 0 && !storeNo.equals(Store.getStoreID())
                && GetProperty.isCheckStoreID())
                putMessage("no");
            if (maxPriceChangeID == null)
                putMessage("yes");
            else
                putMessage("yes" + maxPriceChangeID);

        } catch (IOException e) {
            e.printStackTrace(Server.getLogger());
        }

        //if (posNumber != 0) {
        //    fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED,
        //        false, false, ""));
        //}
    }

    /**
     * Write POS connection status file. File name is property "ConnectionStatusFile".
     */
    static void setPOSConnectionState(int posNumber, boolean connect) {
        try {
            if (connectionStatusFile == null)
                return;

            File f0 = new File(connectionStatusFile);
            if (!f0.exists()) {
                FileWriter fw = new FileWriter(f0);
                fw.write("00000");
                fw.close();
            }

            RandomAccessFile f = new RandomAccessFile(connectionStatusFile, "rw");
            f.seek(posNumber - 1);
            f.writeBytes(connect ? "1" : "0");
            f.close();

        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

    /**
     * Query the count that doing Inventory
     *
     */
    private void processCountDoingInventory() {
        DbConnection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        int        result      = -1;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            String sqlString = "SELECT COUNT(*) AS count FROM inventoryhead "
                + "WHERE storeID='" + getStoreID() + "' "
                + "AND invStatus in ('" + INVENTORY_INIT_STATUS + "', '" + INVENTORY_LOADING_STATUS + "', '" + INVENTORY_LOADED_STATUS + "')";
            log("sqlString for processCountDoingInventory() : " + sqlString);
            log("query start ... : " + sqlString);
            resultSet = statement.executeQuery(sqlString);
            log(" query finish. ");
            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            try {
                log("result : " + result);
                out.writeInt(result);
                log("processCountDoingInventory() end.");
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            } catch (IOException e) {
                e.printStackTrace(Server.getLogger());
            }
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Query the count that doing Inventory
     *
     */
    private void processCountPosul_inventoryRec() {
        log("processCountPosul_inventoryRec() begin ...");
        DbConnection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        int        result      = -1;
        try {
            posNumber = in.readInt();
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            String sqlString = "SELECT COUNT(*) AS count FROM posul_inventory "
                + " WHERE posNumber='" + posNumber + "'";
            log("sqlString for processCountPosul_inventoryRec() : " + sqlString);
            resultSet = statement.executeQuery(sqlString);
            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }
        } catch (IOException e) {
            e.printStackTrace(Server.getLogger());
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            try {
                out.writeInt(result);
                log("result of processCountPosul_inventoryRec() : " + result);
                log("processCountPosul_inventoryRec() end.");
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (IOException e) {
                e.printStackTrace(Server.getLogger());
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * processTurnSingleUlTable by receive the table name from Client
     *
     * precondition: DateShift.jar must in the jdk/jre/lib/ext/
     */
    private void processTurnSingleUlTable() {
        log("processTurnSingleUlTable begin ... ");
        String result = "Fail";
        String tableName = getMessage();
        log("tableName = getMessage(): " + tableName);
        if (tableName != null && tableName.equalsIgnoreCase("posul_inventory")) {
            log("processTurnSingleUlTable DateShift.mainInitializeOut ... ");
            DateShift.mainInitializeOut();
            Pandian pandian = new Pandian();
            log("processTurnSingleUlTable pandian.dataImport ... ");
            if (pandian.dataImport(null))
                result = "OK";
            log("processTurnSingleUlTable DateShift.mainFinalizeOut() ... " + result);
             DateShift.mainFinalizeOut();
            log("turnSingleUlTable() table name " + tableName + " : "+ result);
        } else {
            log("Wrong table name : " + tableName);
        }
        putMessage(result);
        log("processTurnSingleUlTable end. ");
    }

//      /**
//       * processTurnSingleUlTable by receive the table name from Client
//       *
//       * precondition java program local in "/usr/java/jdk1.3/jre/bin"(Linux)
//       *                                    or "JAVA_HOME%\bin\"(Windows)
//       *              DateShift.jar local in "/home/hyi/"(Linux)
//       *                              or "%JAVA_HOME%\jre\lib\ext\" (Windows)
//       */
//    private void processTurnSingleUlTable() {
//        String tableName;
//        String result = "Fail";
//        String command = "";
//        String osName = CreamToolkit.getOsName();
//        Runtime currentApp =  Runtime.getRuntime();
//        tableName = getMessage();
//        try {
//            if (tableName != null && tableName.equalsIgnoreCase("posul_inventory")) {
//                if (osName.equals("Linux")){
//                    command = "/usr/java/jdk1.3/jre/bin/java -jar "
//                        + "/home/hyi/DateShift.jar -pandian 4";
////                    command = "/bin/sh ls /home/hyi/ > /home/hyi/a.out";
//                } else if (osName.equals("Windows")) {
//                    command = "cmd /c "
//                        + "%JAVA_HOME%\\bin\\java -jar "
//                        + "%JAVA_HOME%\\jre\\lib\\ext\\DateShift.jar -pandian 4";
////                    command = "cmd /c dir c:\\t\\ > c:\\t\\a.out";
//                } else {
//                    throw new Exception("Unknown OS");
//                }
//            }
//            Process c = currentApp.exec(command);
////            c.wait(timeOut);
//            c.waitFor();
//            if (c.exitValue() == 0)
//                result = "OK";
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            putMessage(result);
//        }
//    }

    private void processGetAccountDateOfZ() {
        int        posNo      = -1;
        int        zSeq       = -1;
        DbConnection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        String     accDateStr  = null;
        try {
            posNo = in.readInt();
            zSeq = in.readInt();
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            String sqlString = "SELECT accountDate FROM posul_z "
                + " WHERE zSequenceNumber='" + zSeq + "' AND posNumber='" + posNo + "'";
            resultSet = statement.executeQuery(sqlString);
            if (resultSet.next()) {
                java.sql.Date accDate = resultSet.getDate(1);
                DateFormat yyyy_MM_dd = CreamCache.getInstance().getDateFormate();
                accDateStr = yyyy_MM_dd.format(accDate);
                out.writeInt(1);
                putMessage(accDateStr);
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            CreamToolkit.releaseConnection(connection);
        }
    }
    
    private void processUpdateHead(String command) {
        try {
            StringTokenizer t = new StringTokenizer(command, " ");
            log("Command : " + command);
            t.nextToken(); // skip "getobject"
            String posNumber = "";
            String tableName = "";
            String count = "";
            if (t.hasMoreTokens()) {
                posNumber = t.nextToken();
            } else
                return ;
            if (t.hasMoreTokens()) {
                tableName = t.nextToken();
            } else
                return ;
            if (t.hasMoreTokens()) {
                count = t.nextToken();
            } else
                return ;
            String updateDate = (CreamCache.getInstance().getDateFormate()).format(new java.util.Date());
            String downloadDate = (new SimpleDateFormat("yyyyMMddhhmmss")).format(new java.util.Date());
            String where = " WHERE updateBeginDate= '" + updateDate + "' AND storeID = '" + getStoreID() +
                    "' AND tableName = 'pos" + posNumber + "'";

            String sequenceSql = "SELECT MAX(sequencenumber) AS sequence FROM commdl_head " + where;
            Map data = CreamToolkit.getResultData(sequenceSql);
            if (data == null || data.isEmpty() || data.get("sequence") == null) {
                String insert = "INSERT INTO commdl_head (updateBeginDate, storeID, sequenceNumber, " +
                        "tableName, downloadCount, insertCount, downloadDate, downloadResult) VALUES ('" +
                        updateDate + "', '" + getStoreID() + "', 1, '" +
                        tableName + posNumber + "', " + count + ", " + count +
                        ", '" + downloadDate + "', 2)";
                CreamToolkit.logMessage("UpdateHead : " + insert);
                CreamToolkit.execute(insert);
                return ;
            }

            String sequence = data.get("sequence").toString();
            String sql = "UPDATE commdl_head SET downloadCount = " + count +
                    ", insertCount = " + count + ", downloadDate = '" + downloadDate +
                    "', downloadResult = 2" + where + " AND sequencenumber = " + sequence ;
            CreamToolkit.logMessage("UpdateHead : " + sql);
            CreamToolkit.execute(sql);
//            putMessage("success");
        } catch (Exception e) {
//            putMessage("fail");
        }
    }
    
    static public String getStoreID() {
        if (storeID == null || storeID.trim().equals("")) {
            String sql = "SELECT storeID FROM store";
            Map data = CreamToolkit.getResultData(sql);
            storeID = (String) data.get("storeID");
        }
        return storeID;
    }
    
    private void processNeedDown(String command) {
        StringTokenizer t = new StringTokenizer(command, " ");
        log("Command : " + command);
        t.nextToken();
        String posNumber = "";
        if (t.hasMoreTokens()) {
            posNumber = t.nextToken();
        } else
            return ;
        String updateDate = (CreamCache.getInstance().getDateFormate()).format(new java.util.Date());
        String where = " WHERE updateBeginDate= '" + updateDate + "' AND storeID = '" + getStoreID() +
                "' AND tableName = 'pos" + posNumber + "'";
        String sequenceSql = "SELECT MAX(sequencenumber) AS sequence, downloadResult FROM commdl_head " +
                where + " GROUP BY tableName";
        log("processNeedDown : " + sequenceSql);
        Map data = CreamToolkit.getResultData(sequenceSql);
        log("processNeedDown | data " + data);
        if (data == null || data.isEmpty())
            putMessage("no");
        else if (data.containsKey("downloadResult") && data.get("downloadResult") != null &&
                !data.get("downloadResult").toString().equals("2"))
            putMessage("yes");
        else
            putMessage("no");
    }
    
    private void processBookableSubIssns(String command) {
        StringTokenizer t = new StringTokenizer(command, " ");
        log("Command : " + command);
        t.nextToken();
        String itemNo = "";
        if (t.hasMoreTokens()) {
            itemNo = t.nextToken();
        } else
            return ;
        String start = "";
        if (t.hasMoreTokens()) {
            start = t.nextToken();
        } else
            return ;
        String end = "";
        if (t.hasMoreTokens()) {
            end = t.nextToken();
        } else
            return ;

        List datas = PeriodicalUtils.processGetBookableSubIssns(itemNo, start, end);

        putObject(datas);
    }

    private void processBookedInfos(String command) {
        StringTokenizer t = new StringTokenizer(command, " ");
        log("Command : " + command);
        t.nextToken();
        String memberID = "";
        if (t.hasMoreTokens()) {
            memberID = t.nextToken();
        } else
            return ;
        List<BookedInfo> datas = PeriodicalUtils.processGetBookedInfos(memberID);
        //putObject(datas);

        // james/20080321 只有是当月或者之前月份的刊物才可以领刊
        final SimpleDateFormat df = new SimpleDateFormat("yyMM");
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, c.get(Calendar.YEAR) - 1);
        String lastYearYYMM = df.format(c.getTime());
        String nowYYMM = df.format(new java.util.Date());
        List<BookedInfo> filteredData= new ArrayList<BookedInfo>();
        for (BookedInfo bi : datas){
            System.out.println("bi.getItemNo(), bi.getYear() + bi.getDtlcode1()>" + bi.getItemNo() + " | " + bi.getYear() + bi.getDtlcode1());
            List<String> itemBookableSubIssns = PeriodicalUtils.processGetBookableSubIssns(bi.getItemNo(), lastYearYYMM, nowYYMM);
            if (itemBookableSubIssns.contains(bi.getYear() + bi.getDtlcode1())){
                //System.out.println("-----------");
                filteredData.add(bi);
            }
        }
        Server.log("filteredData size| " + filteredData.size());
        putObject(filteredData);
    }

    private void processReturnBookInfos(String command) {
        StringTokenizer t = new StringTokenizer(command, " ");
        log("Command : " + command);
        t.nextToken();
        String memberID = "";
        if (t.hasMoreTokens()) {
            memberID = t.nextToken();
        } else
            return ;
        List<BookedInfo> datas = PeriodicalUtils.processGetBookedInfos(memberID);
        //putObject(datas);

        // james/20080321 只有是当月或者之前月份的刊物才可以领刊
        //
        final SimpleDateFormat df = new SimpleDateFormat("yyMM");
        Calendar c = Calendar.getInstance();
        Calendar cc = Calendar.getInstance();
        c.set(Calendar.YEAR, c.get(Calendar.YEAR) - 1);
        cc.set(Calendar.YEAR,c.get(Calendar.YEAR) + 3);
        String lastYearYYMM = df.format(c.getTime());
        String nowYYMM = df.format(cc.getTime());
        List<BookedInfo> filteredData= new ArrayList<BookedInfo>();
        for (BookedInfo bi : datas){
            System.out.println("bi.getItemNo(), bi.getYear() + bi.getDtlcode1()>" + bi.getItemNo() + " | " + bi.getYear() + bi.getDtlcode1());
            List<String> itemBookableSubIssns = PeriodicalUtils.processGetBookableSubIssns(bi.getItemNo(), lastYearYYMM, nowYYMM);
            if (itemBookableSubIssns.contains(bi.getYear() + bi.getDtlcode1())){
                //System.out.println("-----------");
                filteredData.add(bi);
            }
        }
        Server.log("filteredData size| " + filteredData.size());
        putObject(filteredData);
    }

    private void processWholesales(String command) {
        StringTokenizer t = new StringTokenizer(command, " ");
        log("Command : " + command);
        t.nextToken();
        String storeID = "";
        if (t.hasMoreTokens()) {
            storeID = t.nextToken();
        } else
            return ;
        String billNo = "";
        if (t.hasMoreTokens()) {
            billNo = t.nextToken();
        } else
            return ;

        List datas = WholesaleUtils.processGetWholesales(storeID, billNo);

        putObject(datas);
    }
}
