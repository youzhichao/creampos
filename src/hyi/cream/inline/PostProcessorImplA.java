package hyi.cream.inline;

import hyi.cream.dac.*;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.util.UppercaseKeyMap;
import hyi.cream.groovydac.CardDetail;
import hyi.cream.groovydac.CardDetailChinaTrust;

import java.math.BigDecimal;
import hyi.cream.util.DbConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.IteratorUtils;

/**
 * Implement of PostProcessor for nitori accdayrpt account code.
 */
class PostProcessorImplA extends PostProcessorAdapter {
    transient public static final String VERSION = "20080704.1";

    static final String PAYID_CASH = "00";
    static final int DATE_ACCDATE = 200;
    static final int DATE_BIZDATE = DATE_ACCDATE + 1;
    static final int DATE_INFDATE = DATE_ACCDATE + 2;
    static final String POS_AMOUNT = "posamount";
    static final String STORE_AMOUNT = "storeamount";
    static final Integer integer0 = new Integer(0);

    static private DateFormat yyyy_MM_dd = CreamCache.getInstance().getDateFormate();
    // static Object lockObject = new Object();
    static private Map zSeqToAccountingDateMap = new HashMap();

//    /**
//     * 根据交易时间决定营业日期。
//     * 
//     * @param date 交易时间
//     * @return 返回营业日期
//     */
//    private Date getBusinessDate(Date date) {
//        String changeTime = "22:59:59";
//        Map value = query("SELECT changetime FROM calendar WHERE dateType = '0'");
//        if (value != null && value.containsKey("changetime")) {
//            changeTime = (String)value.get("changetime").toString();
//            if (changeTime.trim().length() == 0 || changeTime.equals("00:00:00")) {
//                changeTime = "22:59:59";        
//            }
//        }
//        String realTime = new SimpleDateFormat("HH:mm:ss").format(date);
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//        if (realTime.compareTo(changeTime) > 0) {
//            cal.add(Calendar.DATE, 1);
//        }
//        cal.set(Calendar.HOUR, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        return cal.getTime();
//    }

    /**
     * 由Z帐序号决定会计日期。
     * 
     * @param posNumber POS机号
     * @param zSeq Z帐序号
     * @return 返回会计日期
     */
    private Date getAccountingDate(DbConnection connection, int posNumber, int zSeq) throws SQLException {
        Integer key = new Integer(posNumber * 1000000 + zSeq);
        Date accountingDate = (Date)zSeqToAccountingDateMap.get(key);
        if (accountingDate != null)
            return accountingDate;
        Map value = query(connection, "SELECT accountdate FROM posul_z WHERE posNumber=" + posNumber 
            + " AND zSequenceNumber=" + zSeq);
        if (value != null && value.containsKey("accountdate")) {
            accountingDate = (Date)value.get("accountdate");
            zSeqToAccountingDateMap.put(key, accountingDate);
            return accountingDate;
        }
        return getAccountingDate(connection); // get accountDate from Table calendar
    }

    /**
     * 由Z帐序号或交易时间决定会计日期。
     * 
     * @param posNumber POS机号
     * @param zSeq Z帐序号
     * @param transactionDate 交易时间
     * @return 返回会计日期
     */
    private Date getAccountingDate(DbConnection connection, int posNumber, int zSeq,
            Date transactionDate) throws SQLException {

        Integer key = new Integer(posNumber * 1000000 + zSeq);
        Date accountingDate = (Date)zSeqToAccountingDateMap.get(key);
        if (accountingDate != null)
            return accountingDate;
        Map value = query(connection, "SELECT accountdate FROM posul_z WHERE posNumber="
            + posNumber + " AND zSequenceNumber=" + zSeq);

        if (value != null && value.containsKey("accountdate")) {
            accountingDate = (Date)value.get("accountdate");
            zSeqToAccountingDateMap.put(key, accountingDate);
            return accountingDate;
        }
        return getAccountingDate(connection); // get accountDate from Table calendar
    }
    
//    private Date getAccountingDate(Date date) {
//        String changeTime = "23:59:59";
//        Map value = query("SELECT changetime FROM calendar WHERE dateType = '1'");
//        if (value != null && value.containsKey("changetime")) {
//            changeTime = (String)value.get("changetime").toString();
//            if (changeTime.trim().length() == 0 || changeTime.equals("00:00:00")) {
//                changeTime = "23:59:59";        
//            }
//        }
//        String realTime = new SimpleDateFormat("HH:mm:ss").format(date);
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//
//        //Bruce/2003-12-01
//        //有两种决定会计日期的models:
//        //如果AccountingDateShiftTime="afternoon"(default)，则超过换日时间后为(d+1)日；
//        //否则(="morning")超过换日时间后仍为(d)日，在换日之前为(d-1)日。
//        boolean afternoon = CreamProperties.getInstance().getProperty(
//            "AccountingDateShiftTime","afternoon").equalsIgnoreCase("afternoon");
//        if (afternoon) {
//            if (realTime.compareTo(changeTime) > 0) {
//                cal.add(Calendar.DATE, 1);
//            }
//        } else {
//            if (realTime.compareTo(changeTime) <= 0) {
//                cal.add(Calendar.DATE, -1);
//            }
//        }
//        cal.set(Calendar.HOUR, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        return cal.getTime();
//    }
    
    public String getScEodResult(DbConnection connection, int dateType) throws SQLException {
        String type;
        switch (dateType) {
           case DATE_ACCDATE:
               type = "1";
               break;
           case DATE_BIZDATE:
               type = "0";
               break;
           case DATE_INFDATE:
               type = "4";
               break;
           default:
               return null;
        }
        String storeID = getStoreID(); //CreamToolkit.getConfiguration().getProperty("StoreID");
        Statement statement = null;
        ResultSet resultSet = null;
        String select = "SELECT scEodResult FROM calendar ";
        String where = " WHERE storeID = '" + storeID + "' AND dateType = '" + type + "'";

        statement = connection.createStatement();
        resultSet = statement.executeQuery(select + where);

        String scEodResult = null;
        if (resultSet.next())
            scEodResult = resultSet.getString(1);

        if (statement != null)
            statement.close();
        if (resultSet != null)
            resultSet.close();
        return scEodResult;
    }

    public boolean isDateShiftRunning(DbConnection connection, int dateType) throws SQLException {
        String result = getScEodResult(connection, dateType);
        return (result != null && result.equals("1"));
    }

    public boolean isDateShiftRunningOrFailed(DbConnection connection, int dateType) throws SQLException {
        String result = getScEodResult(connection, dateType);
        return (result != null && (result.equals("1") || result.equals("2")));
    }

    public synchronized boolean afterReceivingShiftReport(ShiftReport shift) {
        DbConnection connection = null;
        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();


            Date accountingDate = shift.getAccountingDate();
            if (accountingDate == null || ((yyyy_MM_dd.format(accountingDate)).startsWith("197"))) {
                // 重传z时如果没有设置accoutdate,就得看sc是否已存在,如果存在,accountdate使用原来的
                ShiftReport oldShift = ShiftReport.queryByKey(connection, shift.getTerminalNumber(), shift.getZSequenceNumber(), shift.getSequenceNumber());
                if (oldShift != null) {
                    accountingDate = oldShift.getAccountingDate();
                } else {
                    accountingDate = getAccountingDate(connection);  // get accountDate from Table calendar
                    shift.setAccountingDate(accountingDate);
                }
            }
            shift.setAccountingDate(accountingDate);

            if (shift.deleteByPrimaryKey(connection))
                Server.log("Old Shift deleted (date=" + yyyy_MM_dd.format(shift.getAccountingDate()) +
                    ",pos=" + shift.getTerminalNumber() +
                    ",z=" +shift.getZSequenceNumber() +
                    ",shift=" +shift.getSequenceNumber() + ").");

            shift.insert(connection, false);

            // commit transaction
            connection.commit();
            Server.log("Shift inserted (date=" + yyyy_MM_dd.format(shift.getAccountingDate()) +
                ",pos=" + shift.getTerminalNumber() +
                ",z=" +shift.getZSequenceNumber() +
                ",shift=" +shift.getSequenceNumber() + ").");
            return true;

        } catch (SQLException e) {
            Server.log("Shift inserted failed (pos=" + shift.getTerminalNumber() +
                ",z=" +shift.getZSequenceNumber() +
                ",shift=" +shift.getSequenceNumber() + ").");
            e.printStackTrace(Server.getLogger());
            return false;

        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Check if the given date's accdayrpt has no records, if yes, copy from
     * yesterday's record, or from commdl_accitem.
     * 
     * @author bruce
     */
    private boolean generateAccdayrptRecords(DbConnection connection, Date date) throws SQLException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        String yesterday = yyyy_MM_dd.format(cal.getTime());
        String today = yyyy_MM_dd.format(date);

        Map value = null;
        value = query(connection, "SELECT COUNT(*) AS cnt FROM accdayrpt " 
                + "WHERE accountDate='" + today + "'");
        if (value != null && value.containsKey("cnt")) {
            if (CreamToolkit.retrieveIntValue(value.get("cnt")) > 0)
                return true;    //表示今天的记录已经存在
        }

        value = query(connection, "SELECT COUNT(*) AS cnt FROM commdl_accitem WHERE updatebegindate = '" + today + "'");
        if (value != null && value.containsKey("cnt") && CreamToolkit.retrieveIntValue(value.get("cnt")) > 0) {
            //表示今天的记录已经存在
            String accitemDateString = today;

            value = query(connection, "SELECT MAX(sequenceNumber) dt FROM commdl_accitem WHERE updateBeginDate='"
                + accitemDateString + "'");
            Integer sequenceNumber = (Integer)value.get("dt");
            executeQuery(connection,
                "INSERT INTO accdayrpt ("
                    + "storeID, accountDate, accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate) "
                    + "SELECT storeID, '" + today + "', accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate "
                    + "FROM commdl_accitem "
                    + "WHERE updateBeginDate='" + accitemDateString 
                    + "' AND sequenceNumber=" + sequenceNumber);
            executeQuery(connection,
                "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                    + "WHERE accountDate='" + today + "'");
        } else {

            value = query(connection, "SELECT COUNT(*) AS cnt FROM accdayrpt "
                + "WHERE accountDate='" + yesterday + "'");
            Integer yesterdayCount = integer0;
            if (value != null && value.containsKey("cnt"))
                yesterdayCount = new Integer(CreamToolkit.retrieveIntValue(value.get("cnt")));

            if (yesterdayCount.compareTo(integer0) > 0) { // 表示有昨天的记录
                try {
                    executeQuery(connection,
                        "CREATE TEMPORARY TABLE tmp ("
                            + "storeID varchar(6) NOT NULL DEFAULT '' ,"
                            + "accountDate date NOT NULL DEFAULT '0000-00-00' ,"
                            + "accountNumber varchar(6) NOT NULL DEFAULT '' ,"
                            + "accountName varchar(30) ,"
                            + "posNumber tinyint(3) unsigned NOT NULL DEFAULT '0' ,"
                            + "displayType char(2) ,"
                            + "displaySequence varchar(6) ,"
                            + "posAmount decimal(12,2) ,"
                            + "storeAmount decimal(12,2) ,"
                            + "storeCount tinyint(3) unsigned ,"
                            + "updateType varchar(4) ,"
                            + "calType varchar(4) ,"
                            + "uploadType char(1) ,"
                            + "itemNumber varchar(10) ,"
                            + "payID char(2) ,"
                            + "dataSource varchar(50) ,"
                            + "updateUserID varchar(8) ,"
                            + "updateDate datetime ,"
                            + "turnMfg varchar(2) NOT NULL DEFAULT '0'"
                            + ")  ENGINE=InnoDB DEFAULT CHARSET=utf8;");
                    executeQuery(connection,
                        "INSERT INTO tmp ("
                            + "storeID, accountDate, accountNumber, accountName, posNumber,"
                            + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                            + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                            + "updateUserID, updateDate, turnMfg ) "
                            + "SELECT storeID, '" + today + "', accountNumber, accountName, posNumber,"
                            + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                            + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                            + "updateUserID, updateDate, turnMfg "
                            + "FROM accdayrpt "
                            + "WHERE accountDate='" + yesterday + "'");
                    executeQuery(connection, "INSERT INTO accdayrpt SELECT * FROM tmp");
                    executeQuery(connection,
                        "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                            + "WHERE accountDate='" + today + "'");
                } catch (SQLException e) {
                    e.printStackTrace(Server.getLogger());
                    throw e;
                } finally {
                    try {
                        executeQuery(connection, "DROP TABLE tmp");
                    } catch (Exception e) {}
                }

            } else {  // accdayrpt中没有昨天的记录，从commdl_accitem取

                value = query(connection, "SELECT MAX(updatebegindate) dt FROM commdl_accitem");
                if (value == null || !value.containsKey("dt")) {
                    Server.log("Cannot create accdayrpt records.");
                    return false;
                }
                Date accitemDate = (Date)value.get("dt");
                String accitemDateString = yyyy_MM_dd.format(accitemDate);

                value = query(connection, "SELECT MAX(sequenceNumber) dt FROM commdl_accitem WHERE updateBeginDate='"
                    + accitemDateString + "'");
                Integer sequenceNumber = (Integer)value.get("dt");

                executeQuery(connection,
                    "INSERT INTO accdayrpt ("
                        + "storeID, accountDate, accountNumber, accountName, posNumber,"
                        + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                        + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                        + "updateUserID, updateDate) "
                        + "SELECT storeID, '" + today + "', accountNumber, accountName, posNumber,"
                        + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                        + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                        + "updateUserID, updateDate "
                        + "FROM commdl_accitem "
                        + "WHERE updateBeginDate='" + accitemDateString
                        + "' AND sequenceNumber=" + sequenceNumber);
                executeQuery(connection,
                    "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                        + "WHERE accountDate='" + today + "'");
            }
        }

        // 避免posAmount和storeAmount有空值
        executeQuery(connection,
            "UPDATE accdayrpt SET storeAmount=0 "
                + "WHERE accountDate='" + today 
                + "' AND storeAmount IS NULL");
        executeQuery(connection,
            "UPDATE accdayrpt SET posAmount=0 "
                + "WHERE accountDate='" + today 
                + "' AND posAmount IS NULL");

        return true;
    }

    @Override
    synchronized public boolean afterReceivingShiftEx(Object[] shiftExes) {
        DbConnection connection = null;
        try {
            int zNumber = ((ShiftEx)shiftExes[0]).getZSequenceNumber();
            int shiftNumber = ((ShiftEx)shiftExes[0]).getShiftSequenceNumber();
            int posNumber = ((ShiftEx)shiftExes[0]).getTerminalNumber();

            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();
            Date accountingDateObj = null;

            //删除已经存在的
            ShiftEx.deleteBySequenceNumber(connection, posNumber, zNumber, shiftNumber);
            
            for (int i = 0; i < shiftExes.length; i++) {
                ShiftEx sx = (ShiftEx)shiftExes[i];
                if (i == 0) {
                    accountingDateObj = sx.getAccountingDate();
                    if (accountingDateObj == null || ((yyyy_MM_dd.format(accountingDateObj)).startsWith("197"))) {
                        // 重传shiftex时如果没有设置accoutdate,就得看sc是否已存在,如果存在,accountdate使用原来的
                        ShiftReport oldShift = ShiftReport.queryByKey(connection, sx.getTerminalNumber(), sx.getZSequenceNumber(), sx.getShiftSequenceNumber());
                        if (oldShift != null) {
                            accountingDateObj = oldShift.getAccountingDate();
                        } else {
                            accountingDateObj = getAccountingDate(connection);  // get accountDate from Table calendar
                        }
                    }
                }
                sx.setAccountingDate(accountingDateObj);
                sx.setUploadState("1");
                sx.insert(connection, false);
                Server.log("ShiftEx inserted (pos=" + posNumber 
                    + ", zNumber=" + zNumber
                    + ", shiftNumber=" + shiftNumber
                    + ", code=" + sx.getAccountCode() 
                    + ", amt=" + sx.getAmount() 
                    + ")."); 
            }
            // commit transaction
            connection.commit();
            Server.log("ShiftEx insertion commited.");
            return true;

        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    @Override
    synchronized public boolean afterReceivingZEx(Object[] zExes) {
        DbConnection connection = null;
        try {
            int zNumber = ((ZEx)zExes[0]).getZSequenceNumber();
            int posNumber = ((ZEx)zExes[0]).getTerminalNumber();

            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();

            Date accountingDateObj = null;

            // delete old zex
            ZEx.deleteBySequenceNumber(connection, posNumber, zNumber);

            for (int i = 0; i < zExes.length; i++) {
                // insert innocake.zex --
                ZEx zex = (ZEx)zExes[i];
                if (i == 0) {
                    accountingDateObj = zex.getAccountingDate();
                    if (accountingDateObj == null || ((yyyy_MM_dd.format(accountingDateObj)).startsWith("197"))) {
                        // 重传zex时如果没有设置accoutdate,就得看sc是否已存在,如果存在,accountdate使用原来的
                        ZReport oldZ = ZReport.queryByKey(connection, zex.getStoreNumber(), zex.getTerminalNumber(), zex.getZSequenceNumber());
                        if (oldZ != null) {
                            accountingDateObj = oldZ.getAccountingDate();
                        } else {
                            accountingDateObj = getAccountingDate(connection);  // get accountDate from Table calendar
                        }
                    }
                }
                zex.setAccountingDate(accountingDateObj);
                zex.insert(connection, false);
                zex.setUploadState("1");
                Server.log("ZEx inserted (pos=" + posNumber + ", zNumber=" + zNumber + ", code="
                    + zex.getAccountCode() + ", amt=" + zex.getAmount() + ").");
            }
            
            String accountingDate = yyyy_MM_dd.format(accountingDateObj);

            // 检查或生成该会计日期的accdayrpt记录 --
            generateAccdayrptRecords(connection, accountingDateObj);

            // 把storeAmount先计成与原posAmount的差额(为了保留用户原来对storeAmount的修改)
            String cond = "WHERE accountDate='" + accountingDate + "' AND ("
                + "LEFT(accountNumber,1) IN ('1','2','3','4','5') OR "
                + "accountNumber LIKE '710__' OR "   // 710 (71000 - 71019) (折扣si00 - si19) 
                + "accountNumber IN ('600','602','604','606','608','610','690','730','908'))";
            executeQuery(connection, "UPDATE accdayrpt SET storeAmount=storeAmount-posAmount "
                + cond);

            // 然後把posAmount一些欄位歸零
            executeQuery(connection, "UPDATE accdayrpt SET posAmount=0 " + cond);

            String[][] posCodeToScPrefix = new String[][] { 
                { "PAYCNT_", "1" },     // Nitori支付別銷售統計 - 銷售次數
                { "PAYAMT_", "2" },     // Nitori支付別銷售統計 - 銷售金額
                { "RTNCNT_", "3" },     // Nitori支付別銷售統計 - 退貨次數
                { "RTNAMT_", "4" },     // Nitori支付別銷售統計 - 退貨金額
                { "OVERAMT_", "5" },       // Nitori支付別銷售統計 - 溢收金額, 拆分成OVER_PAYAMT_ 和OVER_RTNAMT_ 以后就统计不到了
                { "OVER_PAYAMT_", "5" },       // Nitori支付別銷售統計 - 溢收金額, 拆分成OVER_PAYAMT_ 和OVER_RTNAMT_ 以后的 销售部分
                { "OVER_RTNAMT_", "5" },       // Nitori支付別銷售統計 - 溢收金額, 拆分成OVER_PAYAMT_ 和OVER_RTNAMT_ 以后的 退货部分
                { "CHGAMT_", "908" },      // Nitori支付別銷售統計 - 找零金合計 
            };

            String[][] posCodeToScCode = new String[][] { 
                { "DELIVERY_CNT", "600" },          // Nitori承票張數
                { "DELIVERY_AMT", "602" },          // Nitori承票金額
                { "RTN_DELIVERY_CNT", "604" },      // Nitori退貨傳票張數
                { "RTN_DELIVERY_AMT", "606" },      // Nitori退貨傳票金額
                { "RTN_NORMAL_CNT", "608" },        // Nitori退貨張數
                { "RTN_NORMAL_AMT", "610" },        // Nitori退貨金額
            };

            HYIDouble chgTotal = new HYIDouble(0);
            HYIDouble overTotal = new HYIDouble(0);
            HYIDouble salesTotal = new HYIDouble(0);
            HYIDouble cashTotal = new HYIDouble(0);
            HYIDouble cashReturnTotal = new HYIDouble(0);

            List<ZEx> allZExes = ZEx.queryByAccdate(connection, accountingDateObj);
            
            HYIDouble bagAmtTotal = new HYIDouble(0);
            HYIDouble bagRtnAmtTotal = new HYIDouble(0);
            Map<String, HYIDouble> bagAmt_hm = new HashMap<String, HYIDouble>();
            Map<String, HYIDouble> bagRtnAmt_hm = new HashMap<String, HYIDouble>();
            
            HYIDouble sendFeeAmtTotal = new HYIDouble(0);
            HYIDouble sendFeeRtnAmtTotal = new HYIDouble(0);
            Map<String, HYIDouble> sendFeeAmt_hm = new HashMap<String, HYIDouble>();
            Map<String, HYIDouble> rtnSendFeeAmt_hm = new HashMap<String, HYIDouble>();
            
            Map<String, HYIDouble> over_payamt_hm = new HashMap<String, HYIDouble>();
            Map<String, HYIDouble> over_rtnamt_hm = new HashMap<String, HYIDouble>();

            if (GetProperty.isOverAmtAddBackToPaymentInAccdayrpt()){ // TODO
                over_payamt_hm = fillCodeAmtHm(allZExes, "OVER_PAYAMT_");
                over_rtnamt_hm = fillCodeAmtHm(allZExes, "OVER_RTNAMT_");
            }
            
            if (GetProperty.isMoveOutBagAmountToOtherPayment()){
                bagAmt_hm = fillCodeAmtHm(allZExes, "BAG_PAYAMT_");
                bagRtnAmt_hm = fillCodeAmtHm(allZExes, "BAG_RTNAMT_");
            } 
            
            if (GetProperty.isMoveOutSendFeeToOtherPayment()){
                sendFeeAmt_hm = fillCodeAmtHm(allZExes, "SENDFEE_PAYAMT_");
                rtnSendFeeAmt_hm = fillCodeAmtHm(allZExes, "SENDFEE_RTNAMT_");
            }
            
            
            
            outLoop: for (ZEx sx : allZExes) {
                // update cake.accdayrpt --
                String zexAccCode = sx.getAccountCode();
                HYIDouble amount = sx.getAmount();
//                HYIDouble amount;
//                if (zexAccCode.contains("RTN") && zexAccCode.contains("AMT")) // 確認退貨金額為負 
//                    amount = sx.getAmount().abs().negateMe();// 不知道前台传过来是正是负数，所以先用绝对值
//                else
//                    amount = sx.getAmount();

                for (String[] map : posCodeToScPrefix) {
                    if (zexAccCode.startsWith(map[0])) {
                        String payId = zexAccCode.substring(map[0].length());

                        if (zexAccCode.startsWith("CHGAMT_")){ // 找零金額
                            if (zexAccCode.startsWith("CHGAMT_02")){ //只累計禮券引起的现金找零
                                chgTotal.addMe(amount); // 找零合計
                            }
                            continue outLoop; // 不更新每一项，累计完后才更新找零合計
                        }
                        
                        //if (zexAccCode.startsWith("OVERAMT_")) // 溢收合計, 如果有就用老办法
                        //if (zexAccCode.startsWith("OVERAMT_") || zexAccCode.startsWith("OVER_PAYAMT_") || zexAccCode.startsWith("OVER_RTNAMT_"))
                        if (zexAccCode.startsWith("OVER")) // 溢收合計, 新老办法记录的溢收都统计
                            overTotal.addMe(amount); 
                        if (zexAccCode.startsWith("PAYAMT_") && payId.equals(PAYID_CASH))
                            cashTotal.addMe(amount); // 現金合計, 不包含溢收，现金也不会溢收
                        if (zexAccCode.startsWith("RTNAMT_") && payId.equals(PAYID_CASH))
                            cashReturnTotal.addMe(amount); // 現金退貨合計, 不包含溢收，现金也不会溢收
                        if (zexAccCode.contains("AMT") 
                                && !zexAccCode.startsWith("CHGAMT_")
                                && !zexAccCode.startsWith("BAG_PAYAMT_") //购物袋支付金额
                                && !zexAccCode.startsWith("BAG_RTNAMT_") //购物袋支付金额
                                && !zexAccCode.startsWith("SENDFEE_PAYAMT_") //购物袋支付金额
                                && !zexAccCode.startsWith("SENDFEE_RTNAMT_") //购物袋支付金额
                                ){
                            if (!GetProperty.isOverAmtAddBackToPaymentInAccdayrpt()
                                     && !zexAccCode.startsWith("OVERAMT_")     //旧的溢收也算
                                     && !zexAccCode.startsWith("OVER_PAYAMT_") //现在溢收要算
                                     && !zexAccCode.startsWith("OVER_RTNAMT_") //现在溢收要算
                            ){
                                salesTotal.addMe(amount); // 全部支付合計, 不算溢收
                            } else {
                                salesTotal.addMe(amount); // 全部支付合計，溢收
                            }
                        }
                        
                        // 对指定pos， 指定payamt扣减
                        HYIDouble subAmt;
                        String s;
                        if (zexAccCode.startsWith("PAYAMT_")){ // 销售金额
                            if (GetProperty.isOverAmtAddBackToPaymentInAccdayrpt()){
                                    s = sx.getTerminalNumber() + "-" + sx.getZSequenceNumber() + "-"
                                        + zexAccCode.substring("PAYAMT_".length(),zexAccCode.length());
                                subAmt = over_payamt_hm.get(s);
                                if (subAmt != null) {
                                    amount.addMe(subAmt); //原先不包含溢收部分，现在从新加回来
                                }
                            }
                            if (GetProperty.isMoveOutBagAmountToOtherPayment()){
                                s = sx.getTerminalNumber() + "-" + sx.getZSequenceNumber() + "-"
                                    + zexAccCode.substring("PAYAMT_".length(),zexAccCode.length());
                                subAmt = bagAmt_hm.get(s);
                                if (subAmt != null) {
                                    amount.subtractMe(subAmt);
                                    bagAmtTotal.addMe(subAmt);
                                }
                            }
                            if (GetProperty.isMoveOutSendFeeToOtherPayment()){
                                s = sx.getTerminalNumber() + "-"
                                + sx.getZSequenceNumber() + "-"
                                + zexAccCode.substring("PAYAMT_".length(),zexAccCode.length());
                                subAmt = sendFeeAmt_hm.get(s);
                                if (subAmt != null) {
                                    amount.subtractMe(subAmt);
                                    sendFeeAmtTotal.addMe(subAmt);
                                }
                            }
                        } else if (zexAccCode.startsWith("RTNAMT_")){ // 退货金额
                            if (GetProperty.isOverAmtAddBackToPaymentInAccdayrpt()){
                                s = sx.getTerminalNumber() + "-" + sx.getZSequenceNumber() + "-"
                                    + zexAccCode.substring("PAYAMT_".length(),zexAccCode.length());
                                subAmt = over_rtnamt_hm.get(s);
                                System.out.println("payID = " + s);
                                System.out.println("over_rtnamt_hm.keySet = " + bagAmt_hm.keySet().iterator());
                                System.out.println("over_rtnamt_hm.values = " + bagAmt_hm.values().iterator());
                                if (subAmt != null) {
                                    amount.addMe(subAmt); //原先不包含溢收部分，现在从新加回来
                                }
                            }
                            if (GetProperty.isMoveOutBagAmountToOtherPayment()){
                                s = sx.getTerminalNumber() + "-"
                                + sx.getZSequenceNumber() + "-"
                                + zexAccCode.substring("RTNAMT_".length(),zexAccCode.length());
                                subAmt = bagRtnAmt_hm.get(s);
                                if (subAmt != null) {
                                    amount.subtractMe(subAmt);
                                    bagRtnAmtTotal.addMe(subAmt);
                                }
                            }
                            if (GetProperty.isMoveOutSendFeeToOtherPayment()){
                                s = sx.getTerminalNumber() + "-"
                                + sx.getZSequenceNumber() + "-"
                                + zexAccCode.substring("RTNAMT_".length(),zexAccCode.length());
                                subAmt = rtnSendFeeAmt_hm.get(s);
                                if (subAmt != null) {
                                    amount.subtractMe(subAmt);
                                    sendFeeRtnAmtTotal.addMe(subAmt);
                                }
                            }
                        }
                        
                        String update = "UPDATE accdayrpt SET posAmount=posAmount+" + amount
                            + ",storeAmount=storeAmount+" + amount + " WHERE accountDate='"
                            + accountingDate + "' AND LEFT(accountNumber,1)='" + map[1] + "' AND "
                            + "payID='" + payId + "'";
                        Server.log("Update accdayrtp: " + update);
                        executeQuery(connection, update);
                        continue outLoop;
                    }
                }

                for (String[] map : posCodeToScCode) {
                    if (zexAccCode.startsWith(map[0])) {
                        accumByAccountCode(connection, accountingDate, map[1], amount);
                        continue outLoop;
                    }
                }

                // 710 (71000 - 71019) (折扣si00 - si19)
                for (int i = 0; i < 20; i++){
                    String siId = (i< 10) ? ("0" + i) : ("" + i);
                    if (zexAccCode.equals("SIAMT_" + siId)) {
                        // 写含税的，不再按照固定 5% 反算未税金额
                        //int amt = amount.divide(new HYIDouble(1.05D), 0,
                        int amt = amount.setScale(0,
                                BigDecimal.ROUND_HALF_UP).negateMe().intValue();
                        accumByAccountCode(connection, accountingDate, "710" + siId, amt);
                        continue outLoop;
                    } else if (zexAccCode.equals("SITAX_" + siId)) {
                        // 分别扣除 SI对应税金, 得到未税金额
                        int tax = amount.setScale(0,
                                BigDecimal.ROUND_HALF_UP).intValue();
                        accumByAccountCode(connection, accountingDate, "710" + siId, tax);
                        continue outLoop;
                    }
                }
            }
            
            // 710__ 变动会影响到 710，711，712 栏位, 所以这里要重算一次
            recaleAfterReceivingDepSales(connection, accountingDateObj); 
            
            // TODO 购物袋金额 次数 归入"其他"金种似乎不妥, 所以目前还没有处理次数问题
            // 将剔除出来的购物袋金额bagAmtTotal合计，从加到"其他"金种中
            // 因为可能存在真正的"其他"金种，不可与完全update，而是累加
            if (GetProperty.isMoveOutBagAmountToOtherPayment()){
                accumByAccountCode(connection, accountingDate,
                        "2" + GetProperty.getBagAmtPaymentID(), bagAmtTotal);
                accumByAccountCode(connection, accountingDate,
                        "4" + GetProperty.getBagAmtPaymentID(), bagRtnAmtTotal);
            }
            if (GetProperty.isMoveOutSendFeeToOtherPayment()){
                accumByAccountCode(connection, accountingDate,
                        "2" + GetProperty.getSendFeeAmtPaymentID(), sendFeeAmtTotal);
                accumByAccountCode(connection, accountingDate,
                        "4" + GetProperty.getSendFeeAmtPaymentID(), sendFeeRtnAmtTotal);
            }
            
            // Nitori銷售收入合計690
            updateByAccountCode(connection, accountingDate, "690", salesTotal);

            // Nitori銷售現金收入801
            updateByAccountCode(connection, accountingDate, "801", cashTotal);

            // Nitori現金退货支出805
            updateByAccountCode(connection, accountingDate, "805", cashReturnTotal);

            // Nitori其他應收款730, 溢收放在这里
            updateByAccountCode(connection, accountingDate, "730", overTotal);

            // Nitori找零金額合計908, 只累計禮券引起的現金找零
            updateByAccountCode(connection, accountingDate, "908", chgTotal);
            
            // commit transaction
            connection.commit();
            Server.log("ZEx insertion commited.");
            return true;

        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    
    private HashMap<String, HYIDouble> fillCodeAmtHm(List<ZEx> allZExes, String codeSegment) {
        HashMap<String, HYIDouble> map = new HashMap<String, HYIDouble>();
        for (ZEx sx : allZExes) {
            String zexAccCode = sx.getAccountCode();
            HYIDouble amount = sx.getAmount();
            if (zexAccCode.startsWith(codeSegment)){
                map.put(sx.getTerminalNumber() + "-" + sx.getZSequenceNumber() + "-"
                        + zexAccCode.substring(codeSegment.length(), zexAccCode.length()),
                        amount);
            }
        }
        return map;
    }

    private void updateByAccountCode(DbConnection connection, String date,
            String code, HYIDouble amount) throws SQLException {
        String update = "UPDATE accdayrpt SET posAmount=" + amount
                + ",storeAmount=" + amount + " WHERE accountDate='" + date
                + "' AND accountNumber='" + code + "'";
        Server.log("Update accdayrtp: " + update);
        executeQuery(connection, update);
    }
    
    private void accumByAccountCode(DbConnection connection, String date,
            String code, HYIDouble amount) throws SQLException {
        String update = "UPDATE accdayrpt SET posAmount=posAmount+" + amount
                + ",storeAmount=storeAmount+" + amount + " WHERE accountDate='"
                + date + "' AND accountNumber='" + code + "'";
        Server.log("Update accdayrtp: " + update);
        executeQuery(connection, update);
    }

    private void accumByAccountCode(DbConnection connection, String date, String code, Integer amount)
        throws SQLException {
        String update = "UPDATE accdayrpt SET posAmount=posAmount+" + amount
            + ",storeAmount=storeAmount+" + amount + " WHERE accountDate='" + date
            + "' AND accountNumber='" + code + "'";
        Server.log("Update accdayrtp: " + update);
        executeQuery(connection, update);
    }
    
    private HYIDouble queryByAccountCode(DbConnection connection, String date, String code)
            throws SQLException {
        return queryAmount(connection, "SELECT storeAmount FROM accdayrpt WHERE accountDate='"
            + date + "' AND accountNumber='" + code + "'");
    }

    public boolean afterReceivingZReport(ZReport z) {
        return afterReceivingZReport(z, new StringBuffer());
    }

    public boolean afterReceivingZReport(ZReport z, StringBuffer accdateStrBuf) {
        boolean returnValue = true;
        DbConnection connection = null;

        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();

            Date accountingDate = z.getAccountingDate();
            if (accountingDate == null || ((yyyy_MM_dd.format(accountingDate)).startsWith("197"))) {
                // 重传z时如果没有设置accoutdate,就得看sc是否已存在,如果存在,accountdate使用原来的
                ZReport oldZ = ZReport.queryByKey(connection, z.getStoreNumber(), z.getTerminalNumber(), z.getSequenceNumber());
                if (oldZ != null) {
                    accountingDate = oldZ.getAccountingDate();
                } else {
                    accountingDate = getAccountingDate(connection);  // get accountDate from Table calendar
                }
            }
            z.setAccountingDate(accountingDate);
            String accountingDateString = yyyy_MM_dd.format(accountingDate);
            if (accdateStrBuf != null) {
                accdateStrBuf.append(accountingDateString);
            }
            
            Date yesterday = CreamToolkit.shiftDay(accountingDate, -1);
            String yesterdayString = yyyy_MM_dd.format(yesterday);
            
            // 1. insert/update z --
            if (z.deleteByPrimaryKey(connection))
                Server.log("Delete old Z (date=" + accountingDateString +
                    ",pos=" + z.getTerminalNumber() +
                    ",z=" + z.getSequenceNumber() + ").");

            z.insert(connection, false);
            Server.log("Z inserted (date=" + accountingDateString +
                ",pos=" + z.getTerminalNumber() +
                ",z=" + z.getSequenceNumber() + ").");

            // 2. update updateAccountDateByZNo --
            updateAccountDateByZNo(connection, z, accountingDateString);
            
            //String updateSQL = "UPDATE posul_tranhead SET accountDate='" + accountingDateString +
            //   "' WHERE zSequenceNumber='" + z.getSequenceNumber() + "' AND posNumber='" + z.getTerminalNumber() + "'";
            //executeQuery(connection, updateSQL);
            //Server.log("Success update posul_tranhead accountDate [SQL] " + updateSQL);
    
            // 3. 检查或生成该会计日期的accdayrpt记录 --
            generateAccdayrptRecords(connection, accountingDate);
    
            // 4. Update posaccountdate --

            HashMap newRec = new UppercaseKeyMap();
            executeQuery(connection, "DELETE FROM posaccountdate WHERE posNumber=" + z.getTerminalNumber() 
                +" AND zSequenceNumber=" + z.getSequenceNumber());
            newRec.put("storeID", z.getStoreNumber().trim());
            newRec.put("accountDate", z.getAccountingDate());
            newRec.put("posNumber", z.getTerminalNumber());
            newRec.put("zSequenceNumber", z.getSequenceNumber());
            insert(connection, newRec, "posaccountdate");

            // 5. update accdayrpt --

            Iterator zIter = ZReport.queryByDateTime(connection, accountingDate);
            if (zIter == null)
                return returnValue;

            // 把storeAmount先计成与原posAmount的差额(为了保留用户原来对storeAmount的修改)
            String cond = "WHERE accountDate='" + accountingDateString + "' AND "
                + "accountNumber IN ('800','802','804','808','904','905','906','907')";
//            + "accountNumber IN ('608','610','800','802','804','808','904','905','906','907')";
            executeQuery(connection, "UPDATE accdayrpt SET storeAmount=storeAmount-posAmount "
                + cond);

            // 然後把posAmount一些欄位歸零
            executeQuery(connection, "UPDATE accdayrpt SET posAmount=0 " + cond);

            // 昨日現金留存(800) <- 昨日現金留存(807)
            HYIDouble last807 = queryByAccountCode(connection, yesterdayString, "807");
            accumByAccountCode(connection, accountingDateString, "800", last807);

            // 开始针对每一个该日的Z帐进行过转到accdayrpt的计算
            while (zIter.hasNext()) {
                z = (ZReport)zIter.next();

                Object[][] posCodeToScCode = new Object[][] { 
//                    { "608", z.getReturnTransactionCount() },           // Nitori退貨張數
//                    { "610", z.getReturnAmount().absMe().negateMe() },  // Nitori退貨金額 // ? 不知道前台传过来是正是负数，所以先用绝对值
                    { "802", z.getPaidInAmount() },                     // Nitori其他現金收入
                    //{ "804", z.getPaidOutAmount().absMe().negateMe() }, // Nitori其他現金支出  // ?  不知道前台传过来是正是负数，所以先用绝对值
                    { "804", z.getPaidOutAmount().negateMe() }, // Nitori其他現金支出  // ?  不知道前台传过来是正是负数，所以先用绝对值
                    { "904", z.getCustomerCount() },                    // Nitori來客數
                };

                for (Object[] x : posCodeToScCode) {
                    if (x[1] instanceof Integer)
                        accumByAccountCode(connection, accountingDateString, (String)x[0],
                            (Integer)x[1]);
                    else if (x[1] instanceof HYIDouble)
                        accumByAccountCode(connection, accountingDateString, (String)x[0],
                            (HYIDouble)x[1]);
                }
            }

            // 本日應有現金(808=800+801+802-804-805)
            HYIDouble a800 = queryByAccountCode(connection, accountingDateString, "800");
            HYIDouble a801 = queryByAccountCode(connection, accountingDateString, "801");
            HYIDouble a802 = queryByAccountCode(connection, accountingDateString, "802");
            HYIDouble a804 = queryByAccountCode(connection, accountingDateString, "804");
            HYIDouble a805 = queryByAccountCode(connection, accountingDateString, "805");
//            accumByAccountCode(connection, accountingDateString, "808",
//                a800.addMe(a801).addMe(a802).subtractMe(a804.abs()).subtractMe(a805.abs())); // ?  不知道前台传过来是正是负数，所以先用绝对值
            accumByAccountCode(connection, accountingDateString, "808",
                    a800.addMe(a801).addMe(a802).addMe(a804).addMe(a805));

            // 累計來客數(905=本月一号到accountingDate所有904相加)
            HYIDouble a905 = queryAmount(connection,
                "SELECT SUM(posAmount) FROM accdayrpt WHERE accountDate>='"
                    + accountingDateString.substring(0, 8) + "01' " 
                    + " AND accountDate <= '" + accountingDateString 
                    + "' AND accountNumber='904'");
            accumByAccountCode(connection, accountingDateString, "905", a905);
            
            // 客單價(906=690/904)
            HYIDouble a690 = queryByAccountCode(connection, accountingDateString, "690");
            HYIDouble a904 = queryByAccountCode(connection, accountingDateString, "904");
            if (!a904.equals(new HYIDouble(0)))
                accumByAccountCode(connection, accountingDateString, "906", a690.divide(a904, 0,
                    BigDecimal.ROUND_HALF_UP).intValue());

            // 累計客單價(907=本月一号到accountingDate所有690相加/905)
            HYIDouble a690s = queryAmount(connection,
                "SELECT SUM(posAmount) FROM accdayrpt WHERE accountDate>='"
                    + accountingDateString.substring(0, 8) + "01'" 
                    + " AND accountDate <= '" + accountingDateString 
                    + "' AND accountNumber='690'");
            if (!a905.equals(new HYIDouble(0)))
                accumByAccountCode(connection, accountingDateString, "907", a690s.divide(a905, 0,
                    BigDecimal.ROUND_HALF_UP).intValue());

            Server.log("afterReceivingZReport() succ, transaction commited. (pos=" 
                + z.getTerminalNumber() + ",z=" + z.getSequenceNumber() + ").");

            Calendar accCal = Calendar.getInstance();
            accCal.setTime(accountingDate);
            for (int i = accCal.get(Calendar.DAY_OF_MONTH) + 1; i <= accCal.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
                Calendar nextCal = Calendar.getInstance();
                nextCal.setTime(accountingDate);
                nextCal.set(Calendar.DAY_OF_MONTH, i);
                String nextDay = yyyy_MM_dd.format(nextCal.getTime());
                if (hasAccountDate(connection, nextDay)) {
                    cond = "WHERE accountDate='" + nextDay + "' AND "
                            + "accountNumber IN ('905','907')";
                    executeQuery(connection, "UPDATE accdayrpt SET storeAmount=storeAmount-posAmount "
                            + cond);

                    // 累計來客數(905=本月一号到accountingDate所有904相加)
                    a905 = queryAmount(connection,
                        "SELECT SUM(posAmount) FROM accdayrpt WHERE accountDate>='"
                            + accountingDateString.substring(0, 8) + "01' "
                            + " AND accountDate <= '" + nextDay
                            + "' AND accountNumber='904'");
                    accumByAccountCode(connection, nextDay, "905", a905);

                    // 客單價(906=690/904)
//                    a690 = queryByAccountCode(connection, nextDay, "690");
//                    a904 = queryByAccountCode(connection, nextDay, "904");
//                    accumByAccountCode(connection, accountingDateString, "906", a690.divide(a904, 0,
//                        BigDecimal.ROUND_HALF_UP).intValue());

                    // 累計客單價(907=本月一号到accountingDate所有690相加/905)
                    a690s = queryAmount(connection,
                        "SELECT SUM(posAmount) FROM accdayrpt WHERE accountDate>='"
                            + accountingDateString.substring(0, 8) + "01'"
                            + " AND accountDate <= '" + nextDay
                            + "' AND accountNumber='690'");
                    
                    if (!a905.equals(new HYIDouble(0)))
                        accumByAccountCode(connection, nextDay, "907", a690s.divide(a905, 0,
                            BigDecimal.ROUND_HALF_UP).intValue());
                }
            }
            // commit transaction
            connection.commit();
            return returnValue;
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            Server.log("afterReceivingZReport() failed (pos=" + z.getTerminalNumber() +
                ",z=" + z.getSequenceNumber() + ").");
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    private boolean updateAccountDateByZNo(DbConnection connection, ZReport z, String accountingDateString) {
        boolean result = false;
        try {
            String updateSQL = "UPDATE posul_tranhead SET accountDate='" + accountingDateString
                + "' WHERE zSequenceNumber='"+ z.getSequenceNumber()
                + "' AND posNumber='"+ z.getTerminalNumber()
                + "' AND storeID='"+ z.getStoreNumber() + "'";
            Server.log(updateSQL);
            executeQuery(connection, updateSQL);

            updateSQL = "UPDATE posul_shift SET accountDate='" + accountingDateString
                + "' WHERE zSequenceNumber='"+ z.getSequenceNumber()
                + "' AND posNumber='"+ z.getTerminalNumber()
                + "' AND storeID='"+ z.getStoreNumber() + "'";
            Server.log(updateSQL);
            executeQuery(connection, updateSQL);

            updateSQL = "UPDATE posul_daishousales SET accountDate='" + accountingDateString
                + "' WHERE zSequenceNumber='"+ z.getSequenceNumber()
                + "' AND posNumber='"+ z.getTerminalNumber()
                + "' AND storeID='"+ z.getStoreNumber() + "'";
            Server.log(updateSQL);
            executeQuery(connection, updateSQL);

            updateSQL = "UPDATE posul_daishousales2 SET accountDate='" + accountingDateString
                + "' WHERE zSequenceNumber='"+ z.getSequenceNumber()
                + "' AND posNumber='"+ z.getTerminalNumber()
                + "' AND storeID='"+ z.getStoreNumber() + "'";
            Server.log(updateSQL);
            executeQuery(connection, updateSQL);
            result = true;
        } catch (SQLException e) {
            e.printStackTrace();
            e.printStackTrace(Server.getLogger());
        } finally {
            System.out.println("updateAccountDateByZNo result>" + result);
            Server.log("updateAccountDateByZNo result>" + result);
        }
        return result;
    }

/*
    public synchronized boolean afterReceivingTransaction(Object[] trans) {
        int i;
        Transaction tran = null;
        int lineItemShouldHave = 0;
        int lineItemActualHave = 0;
        int posNumber = 0;
        int transactionNumber = 0;
        Date accountingDate = null;

        for (i = 0; i < trans.length; i++) {
            if (i == 0) {
                Transaction curTran = (Transaction) trans[i];
                DbConnection connection = null;
                try {
                    // begin transaction
                    connection = CreamToolkit.getPooledConnection();
                    ZReport oldZ = ZReport.queryByKey(connection, curTran.getStoreNumber(), curTran.getTerminalNumber(), curTran.getZSequenceNumber());
                    if (oldZ != null)
                        accountingDate = oldZ.getAccountingDate();
//                    connection.commit();
                } catch (SQLException e) {
                    e.printStackTrace(Server.getLogger());
                    Server.log("afterReceivingTransaction() getAccountingDate failed!");
                } finally {
                    CreamToolkit.releaseConnection(connection);
                }
            }
            if (trans[i] instanceof Transaction) {
                tran = (Transaction)trans[i];
                if (accountingDate != null)
                    tran.setAccountingDate(accountingDate);
                lineItemShouldHave = tran.getDetailCount().intValue();
                posNumber = tran.getTerminalNumber().intValue();
                transactionNumber = tran.getTransactionNumber().intValue();
            } else if (trans[i] instanceof LineItem) {
                lineItemActualHave++;
                LineItem li = (LineItem)trans[i];
                if (tran != null) {
                    // Because there is no connection between Transaction and
                    // LineItem objects sent from POS
                    tran.addLineItemSimpleVersion(li);
                    li.setSystemDateTime(tran.getSystemDateTime());
                }
            }
        }

        Server.log("[" + posNumber + "] add transaction (no = " + transactionNumber + ") begin...");

        // integrity check, if has problem, discard it.
        if (lineItemShouldHave != lineItemActualHave) {
            Server.log("Transaction (pos=" + posNumber + ", no=" + transactionNumber
                + ") is incomplete. Detail count should be " + lineItemShouldHave
                + ", but actual detail count is " + lineItemActualHave);
            return false;
        }

        PostObjectData pod = new PostObjectData(posNumber,transactionNumber,tran.getSystemDateTime());
        pod.save(tran);
        
        updatePeiDaPayStatus(tran);
        Server.log("[" + posNumber + "] add transaction (no = " + transactionNumber + ") end");
        return true;
    }
*/
    public synchronized boolean afterReceivingTransaction(Object[] trans) {
        Transaction tran = null;
        int lineItemShouldHave = 0;
        int lineItemActualHave = 0;
        int posNumber = 0;
        int transactionNumber = 0;

        DbConnection connection = null;
        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();

            ZReport oldZ = null;
            for (int i = 0; i < trans.length; i++) {
                if (trans[i] instanceof Transaction) {
                    tran = (Transaction)trans[i];
                    oldZ = ZReport.queryAccountDateByKey(connection, tran.getStoreNumber(),
                        tran.getTerminalNumber(), tran.getZSequenceNumber());
                    if (oldZ != null)
                        tran.setAccountingDate(oldZ.getAccountingDate());

                    lineItemShouldHave = tran.getDetailCount();
                    posNumber = tran.getTerminalNumber();
                    transactionNumber = tran.getTransactionNumber();

                    // insert into posul_tranhead
                    tran.insert(connection, false);

                    ////Bruce/20090206/Fix defect: 修改可以跨機台重覆退貨的問題。現在交易上傳後，會將後端被作廢交易的dealtype1改為星號。
                    //Integer voidNo = tran.getVoidTransactionNumber();
                    //String saleman = tran.getSalesman();
                    //boolean otherStoreRefund = saleman != null && saleman.startsWith("S=");
                    //if (!otherStoreRefund && voidNo != null && voidNo > 0) {
                    //    int voidPosNo = voidNo % 100;
                    //    int voidTranNo = voidNo / 100;
                    //    if (executeQuery(connection, "UPDATE posul_tranhead SET dealtype1='*' WHERE posNumber="
                    //        + voidPosNo + " AND transactionNumber=" + voidTranNo) <= 0)
                    //       Server.log("Cannot mark void transaction.");
                    //}

                } else if (trans[i] instanceof LineItem) {
                    lineItemActualHave++;
                    LineItem lineItem = (LineItem)trans[i];
                    if (tran != null) {
                        // Because there is no connection between Transaction and
                        // LineItem objects sent from POS
                        tran.addLineItemSimpleVersion(lineItem);
                        lineItem.setSystemDateTime(tran.getSystemDateTime());
                    }

                    // insert into posul_trandtl
                    lineItem.insert(connection, false);

                } else if (trans[i] instanceof CardDetail) {
                    CardDetail cardDetail = (CardDetail)trans[i];
                    CardDetailChinaTrust cardDetailChinaTrust = (CardDetailChinaTrust)cardDetail.addendum;

                    if (oldZ != null) {
                        cardDetail.setAccountDate(oldZ.getAccountingDate());
                        cardDetailChinaTrust.setAccountDate(oldZ.getAccountingDate());
                    }

                    // insert into posl_carddetail
                    Object cardDetailId = cardDetail.insert(connection, true);

                    // insert into posul_carddetail_chinatrust
                    cardDetailChinaTrust.setCarddetailId((Integer)cardDetailId);
                    cardDetailChinaTrust.insert(connection);
                }
            }

            // integrity check, if has problem, discard it.
            if (lineItemShouldHave != lineItemActualHave) {
                Server.log("Transaction (pos=" + posNumber + ", no=" + transactionNumber
                    + ") is incomplete. Detail count should be " + lineItemShouldHave
                    + ", but actual detail count is " + lineItemActualHave);
                connection.rollback();
                return false;
            }

            updatePeiDaPayStatus(tran);
            Server.log("[" + posNumber + "] Transaction (no = " + transactionNumber + ") inserted");

            // commit transaction
            connection.commit();

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            e.printStackTrace(Server.getLogger());
            return false;

        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public synchronized boolean afterReceivingDepSales(Object[] depSales) {

        DbConnection connection = null;
        try {
            int zNumber = ((DepSales)depSales[0]).getSequenceNumber();
            int posNumber = ((DepSales)depSales[0]).getPOSNumber();

            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();

            Date accountingDateObj = null;
            
            //删除已经存在的
            DepSales.deleteBySequenceNumber(connection, zNumber, posNumber);

            for (int i = 0; i < depSales.length; i++) {
                DepSales ds = (DepSales)depSales[i];
                if (i == 0) {
                    accountingDateObj = ds.getAccountDate();
                    ZReport oldZ = ZReport.queryByKey(connection, ds.getStoreNumber(), ds.getPOSNumber(), ds.getSequenceNumber());
                    if (oldZ != null) {
                        accountingDateObj = oldZ.getAccountingDate();
                    } else if (accountingDateObj == null || ((yyyy_MM_dd.format(accountingDateObj)).startsWith("197"))) {
                           accountingDateObj = getAccountingDate(connection);  // get accountDate from Table calendar
                    }
                }
                
                ds.setAccountDate(accountingDateObj);
                ds.insert(connection, false);
                Server.log("DepSales inserted (pos=" + ds.getPOSNumber() 
                    + ", zNumber=" + ds.getSequenceNumber()
                    + ", id=" + ds.getDepID() 
                    + ")."); 
            }
            
            recaleAfterReceivingDepSales(connection, accountingDateObj);

            // commit transaction
            connection.commit();
            Server.log("DepSales insertion commited.");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            e.printStackTrace(Server.getLogger());
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * 没有包含 Conntction 的 事务开始和提交，请在方法外层实现！
     * @param connection
     * @param accountingDateObj
     * @return 
     * @throws SQLException
     */
    public boolean recaleAfterReceivingDepSales(DbConnection connection, Date accountingDateObj) throws SQLException  {
        boolean result = false;
        String accountingDate = yyyy_MM_dd.format(accountingDateObj);

        // 检查或生成该会计日期的accdayrpt记录 --
        generateAccdayrptRecords(connection, accountingDateObj);

        // update accdayrpt --

        // 把storeAmount先计成与原posAmount的差额(为了保留用户原来对storeAmount的修改)
        String cond = "WHERE accountDate='" + accountingDate + "' AND "
            + "LEFT(accountNumber,1) IN ('7') AND accountNumber<>'730'" // 730是其他应收款(溢收合計)，不能在這清零
            + " AND accountNumber NOT LIKE '710__'"; // 71000-71019 是SI折扣统计，在zEx 收到的时候更新
        executeQuery(connection, "UPDATE accdayrpt SET storeAmount=storeAmount-posAmount "
            + cond);

        // 然後把posAmount一些欄位歸零
        executeQuery(connection, "UPDATE accdayrpt SET posAmount=0 " + cond);

        
        // 注意：siPlusTotalAmount用于表示毛额的累计税金
        
        // 分類0(700)~分類9(709), 未税毛额
        for (int i = 0; i <= 9; i++) {
            HYIDouble amt = queryAmount(connection,
                //"SELECT SUM(grossSaleTotalAmount - taxAmount) FROM posul_depsales WHERE " + "accountDate='"
                //"SELECT SUM(grossSaleTotalAmount) FROM posul_depsales WHERE " + "accountDate='"
                "SELECT SUM(grossSaleTotalAmount - siPlusTotalAmount) FROM posul_depsales WHERE accountDate='"
                    + accountingDate + "' AND depID='" + i + "'");
            //int amtNoTax = amt.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
            //int amtNoTax = amt.divide(new HYIDouble(1.05D), 0, BigDecimal.ROUND_HALF_UP).intValue();
            int amtNoTax = amt.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
            accumByAccountCode(connection, accountingDate, (700 + i) + "", amtNoTax);
        }

        //  92类  折扣类商品(714), 未税毛额
        HYIDouble amt = queryAmount(connection,
            //"SELECT SUM(grossSaleTotalAmount - taxAmount) FROM posul_depsales WHERE " + "accountDate='"
            //"SELECT SUM(grossSaleTotalAmount) FROM posul_depsales WHERE " + "accountDate='"
            "SELECT SUM(grossSaleTotalAmount - siPlusTotalAmount) FROM posul_depsales WHERE " + "accountDate='"
                + accountingDate + "' AND depID='92'");
        //int amtNoTax = amt.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
        //int amtNoTax = amt.divide(new HYIDouble(1.05D), 0, BigDecimal.ROUND_HALF_UP).intValue();
        int amtNoTax = amt.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
        accumByAccountCode(connection, accountingDate, "714", amtNoTax);

        // 91类  配送費(791), 未税毛额    
        amt = queryAmount(connection,
                //"SELECT SUM(grossSaleTotalAmount - taxAmount) FROM posul_depsales WHERE " + "accountDate='"
                "SELECT SUM(grossSaleTotalAmount - siPlusTotalAmount) FROM posul_depsales WHERE " + "accountDate='"
                + accountingDate + "' AND depID='91'");
        //amtNoTax = amt.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
        //amtNoTax = amt.divide(new HYIDouble(1.05D), 0, BigDecimal.ROUND_HALF_UP).intValue();
        amtNoTax = amt.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
            accumByAccountCode(connection, accountingDate, "791", amtNoTax);

        // 93类  专柜(715), 未税毛额  (毛额)
        amt = queryAmount(connection,
                //"SELECT SUM(grossSaleTotalAmount - taxAmount) FROM posul_depsales WHERE " + "accountDate='"
                //"SELECT SUM(netSaleTotalAmount) FROM posul_depsales WHERE " + "accountDate='"  //净额
                "SELECT SUM(grossSaleTotalAmount - siPlusTotalAmount) FROM posul_depsales WHERE " + "accountDate='"  //毛额
                + accountingDate + "' AND depID='93'");
        //amtNoTax = amt.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
        //amtNoTax = amt.divide(new HYIDouble(1.05D), 0, BigDecimal.ROUND_HALF_UP).intValue();
        amtNoTax = amt.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
        accumByAccountCode(connection, accountingDate, "715", amtNoTax);
        
        // 合計(720)
        amt = queryAmount(connection,
            "SELECT SUM(netSaleTotalAmount) FROM posul_depsales WHERE accountDate='"
                + accountingDate
                + "' AND depID IN ('0','1','2','3','4','5','6','7','8','9','91','92','93')");
        accumByAccountCode(connection, accountingDate, "720", amt);
        
//        // 折扣(710)  SI:(sc折扣 + 员工折扣) 未税折扣
//        //amt = queryAmount(connection, "SELECT SUM(discountTotalAmount+mixAndMatchTotalAmount) "
//        amt = queryAmount(connection, "SELECT SUM((grossSaleTotalAmount-siPlusTotalAmount) - (netSaleTotalAmount-taxAmount)) "
//            + " FROM posul_depsales WHERE accountDate='" + accountingDate
//            + "' AND depID IN ('0','1','2','3','4','5','6','7','8','9','91','92')");
//        //amtNoTax = amt.divide(new HYIDouble(1.05D), 0, BigDecimal.ROUND_HALF_UP).negateMe().intValue();
//        amtNoTax = amt.setScale(0, BigDecimal.ROUND_HALF_UP).negateMe().intValue();
//        accumByAccountCode(connection, accountingDate, "710", amtNoTax);
        // (710 = 71000 - 71019) (折扣si00 - si19)
        amt = queryAmount(connection, "SELECT SUM(storeAmount) FROM accdayrpt WHERE accountDate='"
                + accountingDate + "' AND accountNumber LIKE '710__'");
        accumByAccountCode(connection, accountingDate, "710", amt);

        // 商品銷售合計(711=700+701..+ 709 + 710 + 714 + 791 + 715) 
        amt = new HYIDouble(0);
        for (int i = 700; i <= 710; i++){
            amt = amt.addMe(queryByAccountCode(connection, accountingDate, i + ""));
        }
        amt = amt.addMe(queryByAccountCode(connection, accountingDate, "714"));
        amt = amt.addMe(queryByAccountCode(connection, accountingDate, "791"));
        amt = amt.addMe(queryByAccountCode(connection, accountingDate, "715"));
        accumByAccountCode(connection, accountingDate, "711", amt);

        // 消費稅(712=720-711)
        HYIDouble a720 = queryByAccountCode(connection, accountingDate, "720");
        HYIDouble a711 = queryByAccountCode(connection, accountingDate, "711");
        //HYIDouble a791 = queryByAccountCode(connection, accountingDate, "791");
        //accumByAccountCode(connection, accountingDate, "712", a720.subtractMe(a711).subtractMe(a791));
        accumByAccountCode(connection, accountingDate, "712", a720.subtractMe(a711));

        result = true;
        return result;
    }

    public synchronized boolean afterReceivingDaishouSales(Object[] daishouSales) {
        DbConnection connection = null;
        try {
            int zNumber = ((DaishouSales)daishouSales[0]).getZSequenceNumber();
            int posNumber = ((DaishouSales)daishouSales[0]).getPosNumber();

            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();

            Date accountingDateObj = null;
            
            DaishouSales.deleteBySequenceNumber(connection, zNumber, posNumber);
            
            for (int i = 0; i < daishouSales.length; i++) {
                DaishouSales ds = (DaishouSales)daishouSales[i];
                if (i == 0) {
                    accountingDateObj = ds.getAccountDate();
                    if (accountingDateObj == null || ((yyyy_MM_dd.format(accountingDateObj)).startsWith("197"))) {
                        // 重传zex时如果没有设置accoutdate,就得看sc是否已存在,如果存在,accountdate使用原来的
                        ZReport oldZ = ZReport.queryByKey(connection, ds.getStoreID(), ds.getPosNumber(), ds.getZSequenceNumber());
                        if (oldZ != null) {
                            accountingDateObj = oldZ.getAccountingDate();
                        } else {
                            accountingDateObj = getAccountingDate(connection);  // get accountDate from Table calendar
                        }
                    }
                }
                
                ds.setAccountDate(accountingDateObj);
                ds.insert(connection, false);
                Server.log("DaishouSales inserted (pos=" + posNumber +
                    ",z=" + zNumber +
                    ",id1=" + ds.getFirstNumber() +
                    ",id2=" + ds.getSecondNumber() + ").");
            }

            // commit transaction
            connection.commit();
            Server.log("DaishouSales insertion commited.");
            return true;

        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            return false;

        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * afterReceivingDaiShouSales2
     *
     * @return 如果代收资料存入SC数据库中成功则返回 true，否则返回 false.
     */
    public synchronized boolean afterReceivingDaiShouSales2(Object[] daiShouSales2) {

        DbConnection connection = null;
        try {
            if (daiShouSales2.length == 0) 
                return true;

            int zNumber = ((DaiShouSales2)daiShouSales2[0]).getZSequenceNumber();
            int posNumber = ((DaiShouSales2)daiShouSales2[0]).getPosNumber();

            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();
            Date accountingDateObj = null;

            DaiShouSales2.deleteBySequenceNumber(connection, zNumber, posNumber);
            
            for (int i = 0; i < daiShouSales2.length; i++) {
                DaiShouSales2 ds = (DaiShouSales2)daiShouSales2[i];
                
                if (i == 0) {
                    accountingDateObj = ds.getAccountDate();
                    if (accountingDateObj == null || ((yyyy_MM_dd.format(accountingDateObj)).startsWith("197"))) {
                        // 重传zex时如果没有设置accoutdate,就得看sc是否已存在,如果存在,accountdate使用原来的
                        ZReport oldZ = ZReport.queryByKey(connection, ds.getStoreID(), ds.getPosNumber(), ds.getZSequenceNumber());
                        if (oldZ != null) {
                            accountingDateObj = oldZ.getAccountingDate();
                        } else {
                            accountingDateObj = getAccountingDate(connection);  // get accountDate from Table calendar
                        }
                    }
                }
                
                ds.setAccountDate(accountingDateObj);
                ds.insert(connection, false);
                Server.log("DaiShouSales2 inserted (StoreID = " + ds.getStoreID() 
                    + ", POS = " + posNumber
                    + ", Z = " + zNumber
                    + ", ID = " + ds.getID() 
                    + ")");
            }

            // commit transaction
            connection.commit();
            Server.log("DaishouSales2 insertion commited.");
            return true;

        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            return false;

        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Inventory processor.
     *
     * @param inventory Inventory objects.
     * @return true if success, otherwise false.
     */
    public boolean afterReceivingInventory(Object[] inventory) {
        int deleteCount = 0;
        int insertCount = 0;
        //int updateCount = 0;
        DbConnection connection = null;
        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();
            int len = inventory.length;
            for (int i = 0; i < len; i++) {
                Inventory ds = (Inventory) inventory[i];
//                try {
//                    ds.insert(connection, false);
//                    insertCount++;
//                } catch (SQLException e) {
//                    ds.deleteByPrimaryKey(connection);
//                    ds.insert(connection, false);
//                    updateCount++;
//                }
                if (ds.deleteByPrimaryKey(connection)){
                    deleteCount++;
                }
                ds.insert(connection, false);
                insertCount++;
            }

            // commit transaction
            connection.commit();
            Server.log("Inventory insertion commited. " + deleteCount + " clash record deleted"
                    + ", " + insertCount + " records inserted");
            return true;

        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            return false;

        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

     public boolean afterReceivingAttendance1(Object[] attendances) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();
            Date accountingDateObj = null;
            for (int i = 0; i < attendances.length; i++) {
                Attendance1 att = (Attendance1)attendances[i];
                if (i == 0) {
                    accountingDateObj = att.getBusiDate();
//                    ZReport oldZ = ZReport.queryByKey(connection, att.getStoreID(), att.getPosNumber(), att.getZnumber().intValue());
//                    if (oldZ != null) {
//                        accountingDateObj = oldZ.getAccountingDate();
//                        Server.log("Get existed Z's account date=" + yyyy_MM_dd.format(accountingDateObj));

                    //Bruce> 現在depsales上傳的時候會計日期已填成POS端的系統日期（原來的下面這個句子不會成立）。現在改成上傳的時候必須以後端的會計日期為準。
                    //} else if (accountingDateObj == null || ((yyyy_MM_dd.format(accountingDateObj)).startsWith("197"))) {
//                    } else {
                           accountingDateObj = getAccountingDate(connection);  // get accountDate from Table calendar
                            Server.log("Get today's account date=" + yyyy_MM_dd.format(accountingDateObj));
//                    }
                }
                att.setBusiDate(accountingDateObj);
                att.insert(connection, false);
                Server.log("posul_Attendance inserted (busidate=" + yyyy_MM_dd.format(accountingDateObj) + ", pos=" + att.getPosNumber()
                    + ", storeID=" + att.getStoreID()
                    + ", employeeID=" + att.getEmployeeID()
                    + ").");
            }
            connection.commit();
            Server.log("posul_Attendance insertion commited.");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            e.printStackTrace(Server.getLogger());
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Master download preprocessor.
     *
     * @return true if it allows master downloading now, false otherwise.
     */
    public boolean beforeDownloadingMaster(DbConnection connection) throws SQLException {
        return true;
        //李荣侨2013/05/10
        // Hi 游副总：
        // 昨天在公司内部召开教超维护会议，讨论教超近期维护的内容，发现有1个问题重复出现的频率比较高，希望POS程序能配合修改。
        // POS主档更新失败
        // POS主档下发会读取calendar中某个字段的值，当该值为1POS主档下发就会失败，而该问题产生的原因是由于 DataShift的某个
        // 原因导致，在没有找到DataShift原因之前，我希望修改POS主档下发不判断该标志，直接下发主档。请回复什么时候能修改好，谢谢。
        //return !isDateShiftRunningOrFailed(connection, DATE_BIZDATE);
    }
    
    /**
     * Meyer / 2002-02-11
     * Check before synchronize transaction
     * 
     * @return true if it allows synchronize transaction, false otherwise.
     */
    public boolean canSyncTransaction(DbConnection connection) throws SQLException {
        return !isDateShiftRunning(connection, DATE_BIZDATE);
    }
}
