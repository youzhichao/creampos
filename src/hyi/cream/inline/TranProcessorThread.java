package hyi.cream.inline;

import hyi.cream.dac.PLU;
import hyi.cream.dac.Transaction;
import hyi.cream.state.GetProperty;
import hyi.cream.util.*;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import java.io.File;
import java.io.FilenameFilter;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Transaction XML file eater.
 */
public class TranProcessorThread extends MutableThread {
    private String postObjectDir = "TmpTrans/"; // 上传上来的对象存放的目录
    private String postObjectErrDir = "TmpTrans/err/"; // 出错后存放的位置

    public TranProcessorThread(String s) {
        super(s);
    }

    
    public void run() {
        File dir = new File(postObjectDir);
        File errDir = new File(postObjectErrDir);
        while (true) {
            // 出现异常以后忽略，该线程继续运行

            try {
                // Object[] tranNumbersInSC = getAllTrans();
                if (dir.exists() && dir.isDirectory()) {
                    String[] fileNames = dir.list(new NameFilter());
                    for (int i = 0; i < fileNames.length; i++) {
                        // String tmp = fileNames[i].substring(0,fileNames[i].indexOf(".xml"));
                        // if (Arrays.binarySearch(tranNumbersInSC, tmp) < 0){
                        String xmlFile = postObjectDir + fileNames[i];
                        Server.log("------- xmlFile : " + xmlFile);
                        if (!processTransaction(xmlFile))
                        	org.apache.commons.io.FileUtils.copyFile(new File(xmlFile), 
                        			new File(postObjectErrDir + fileNames[i]));
                    	new File(xmlFile).delete();
                    }
                    if (fileNames.length > 0){
                    	//z上传成功更新没有更新的交易在此同步会计日（在途中的交易还未进入数据库）
                    	syncTranheadAccountDateFromZ();
                    }
                } else {
                	errDir.mkdirs();
                }
            } catch (Exception e) {
                e.printStackTrace(Server.getLogger());
            }

            try {
            	//Thread.sleep(1L * 60L * 1000L);// 每隔一分钟运行一次
            	Thread.sleep(GetProperty.getInlineInsertTranFromXmlTime("30") * 1000L);
            } catch (Exception e) {
                e.printStackTrace(Server.getLogger());
            }
            continue;
        }
    }
    
	/**
	 * 针对设定了会计日的z, 如果该z对应交易还未填会计日，则填写会计日
	 * @return count of record update
	 */
	public static int syncTranheadAccountDateFromZ() {
		int result = 0;
		DbConnection connection = null;
		Statement statement = null;
        
        //Bruce/20080202/ 這條語句在數據量大的時候，執行時間太長了，而且會把posul_z lock住，導致Z帳上傳失敗
//		String sql = " UPDATE " +
//			"  posul_tranhead a, " +
//			"  (SELECT storeID, posNumber, zSequenceNumber,accountDate FROM posul_z" +
//			"     WHERE accountDate <> '1970-01-01' AND accountDate <>'1971-01-01') b " +
//			" SET a.accountDate=b.accountDate " + 
//			" WHERE (a.accountDate='1970-01-01' OR a.accountDate='1971-01-01')" +
//			"   AND a.storeID=b.storeID" +
//			"   AND a.posNumber=b.posNumber" + 
//			"   AND a.zSequenceNumber=b.zSequenceNumber";
		// JAMES/20080626/
        // please confirm that it use together with index like:
        // [ alter table posul_tranhead ADD INDEX accdate_posno_zseq (accountDate,storeID,posNumber,zSequenceNumber); ]
        // if change this sql, please change the index together if no other use it;
        String sql = "UPDATE" +
            " posul_tranhead a, posul_z b" +
            " SET a.accountDate=b.accountDate" + 
            " WHERE (a.accountDate='1970-01-01' OR a.accountDate='1971-01-01')" +
            "  AND a.storeID=b.storeID" +
            "  AND a.posNumber=b.posNumber" + 
            "  AND a.zSequenceNumber=b.zSequenceNumber";

        //Server.log("syncTranheadAccountDateFromZ | " + sql);
		//System.out.println(new Date() + "syncTranheadAccountDateFromZ | " + sql);
		try {
			connection = CreamToolkit.getPooledConnection();
	        statement = connection.createStatement();
	        result = statement.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			e.printStackTrace(Server.getLogger());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
            CreamToolkit.releaseConnection(connection);
            Server.log("syncTranheadAccountDateFromZ | update count=" + result + "[" + sql + "]");
		}
		return result;
	}

    

    // private Object[] getAllTrans() {
    // String sql = "SELECT transactionNumber, posNumber, systemDate FROM posul_tranhead";
    // Object[] tmpArrays = new Object[]{};
    // DbConnection con = null ;
    // Statement stmt = null;
    // ResultSet rst = null;
    // try {
    // con = CreamToolkit.getPooledConnection();
    // stmt = con.createStatement();
    // rst = stmt.executeQuery(sql);
    // ArrayList list = new ArrayList();
    // while (rst.next()) {
    // String dt = new SimpleDateFormat("yyyyMMdd").format(rst.getDate(3));
    // list.add(rst.getInt(2) + "@" + rst.getInt(1) + "@" + dt);
    // }
    // tmpArrays= list.toArray();
    // Arrays.sort(tmpArrays);
    //            
    // } catch (SQLException e) {
    // e.printStackTrace(Server.getLogger());
    // } finally {
    // try {
    // if (rst != null)
    // rst.close();
    // if (stmt != null)
    // stmt.close();
    // } catch (SQLException e) {
    // }
    // if (con != null)
    // CreamToolkit.releaseConnection(con);
    // }
    // return tmpArrays;
    // }

    private boolean processTransaction(String fileName) {
    	boolean success = true;
        DbConnection connection = null;
        try {
            SAXBuilder builder = new SAXBuilder(true);
            builder.setValidation(false);
            Document doc = builder.build(new File(fileName));
            ArrayList lstSql = new ArrayList();

            Element root = doc.getRootElement();
            Iterator itr = root.getChildren().iterator();
            while (itr.hasNext()) {
                Element el = (Element)itr.next();
                String s = el.getChildText("sql");
                if (s != null && s.length() > 0) {
                    lstSql.add(s);
                }
            }

            // begin a transaction
            connection = CreamToolkit.getTransactionalConnection();

            executeQuery(connection, lstSql.toArray());

            if (Server.isUpdateInv())
                additionalProcess(connection, root);

            // commit transaction
            connection.commit();

        } catch (JDOMException e) {
        	success = false;
            e.printStackTrace(Server.getLogger());

        } catch (SQLException e) {
        	success = false;
            String errString = "Error occured when insert transaction into database."
                + "\n \t transaction file:[" + fileName + "]";
            Server.getLogger().write(errString);
            e.printStackTrace(Server.getLogger());

        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return success;
    }

    private void executeQuery(DbConnection connection, Object[] sqls) throws SQLException {
        Statement stmt = null;
        String tranSql = null;
    	try {
	        stmt = connection.createStatement();
	
	        // insert into posul_tranhead at last
	        for (int i = 0; i < sqls.length; i++) {
	            if (sqls[i].toString().toLowerCase().indexOf("into posul_tranhead") > 0)
	                tranSql = sqls[i].toString();
	            else
	                stmt.execute((String)sqls[i]);
	        }
	        if (tranSql != null)
	            stmt.execute(tranSql);
    	} catch (SQLException e) {
    		Server.log("TranProcess | sql error : " + tranSql);
    		e.printStackTrace(Server.getLogger());
    		throw new SQLException(e.getMessage());
    	} finally {
    		if (stmt != null)
    			stmt.close();
    	}
    }

    /**
     * 额外的处理，包括插入作废发票、退货交易、更新库存等
     * 
     * @throws SQLException
     */
    private void additionalProcess(DbConnection connection, Element elTran) throws SQLException {

        Element elTranHead = elTran.getChild("TranHead");
        if (elTranHead == null)
            return;

        String dealType1 = elTranHead.getChildText("dealType1");
        String dealType2 = elTranHead.getChildText("dealType2");
        String dealType3 = elTranHead.getChildText("dealType3");
        if ((dealType3 != null && !dealType3.equals("0"))
        		&& !(dealType1.equals("0") && dealType2.equals("0") && dealType3.equals("4"))) {
            String tmp = elTranHead.getChildText("sql");
            Map tranFieldMap = getFieldMap(tmp);

        	List<Map> details = new ArrayList<Map>();
            if (dealType3.equals("4")) {
                Iterator itrDetails = elTran.getChildren("TranDetail").iterator();
                while (itrDetails.hasNext()) {
                	Map detail = new HashMap();
                    Element elDetail = (Element)itrDetails.next();
                    detail.put("itemNumber", elDetail.getChildText("itemNumber"));
                    detail.put("quantity", new HYIDouble(elDetail.getChildText("quantity")));
                    detail.put("unitPrice", new HYIDouble(elDetail.getChildText("unitPrice")));
                    detail.put("lineItemSequence", new Integer(elDetail.getChildText("lineItemSequence")));
                    details.add(detail);
                }
            }
            additionalProcess(connection, tranFieldMap, details);
        }

        // /营业日，会计日，验收日合并以后，上传交易时不在更改库存
        // /如果要更改库存，那么更改哪一天呢？
        // Iterator itr = elTran.getChildren("TranDetail").iterator();
        // while (itr.hasNext()) {
        // updateInv(elTranHead, (Element)itr.next());
        // }
    }

    public void additionalProcess(DbConnection connection, Map tranFieldMap, List<Map> details) throws SQLException {

        if (tranFieldMap == null)
            return;
        String dealType1 = (String) tranFieldMap.get("dealType1");
        String dealType2 = (String) tranFieldMap.get("dealType2");
        String dealType3 = (String) tranFieldMap.get("dealType3");
        if ((dealType3 != null && !dealType3.equals("0"))
        		&& !(dealType1.equals("0") && dealType2.equals("0") && dealType3.equals("4"))) {

            // 部分退货第三笔也有voidTransaction
            if (!(dealType1.equals("0") && dealType2.equals("0") && dealType3.equals("4"))) {
            	insertVoidInvo(connection, tranFieldMap);
            }
            

            if (dealType3.equals("4"))
                insertSalertn(connection, tranFieldMap, details);
        }

        // /营业日，会计日，验收日合并以后，上传交易时不在更改库存
        // /如果要更改库存，那么更改哪一天呢？
        // Iterator itr = elTran.getChildren("TranDetail").iterator();
        // while (itr.hasNext()) {
        // updateInv(elTranHead, (Element)itr.next());
        // }
    }

    private void insertVoidInvo(DbConnection connection, Map fieldMap) throws SQLException {
    	// 由于voidTransactionNumber＝voidTransactionNumber＋voidPosNo
        Object o = fieldMap.get("voidTransactionNumber");
        if (o == null)
            return;

        String voidTranNo = o.toString();
//        int posNumber = Integer.parseInt((String)fieldMap.get("posNumber"));
        int voidPosNumber = Integer.parseInt(voidTranNo.substring(voidTranNo.length() - 2));
        String voidTranNumber = voidTranNo.substring(0, voidTranNo.length() - 2);
        Transaction voidT;
        try {
            voidT = Transaction.queryByTransactionNumber(connection,
                voidPosNumber, Integer.parseInt(voidTranNumber));
            if (voidT == null)
                return;
        } catch (Exception e) {
            return;
        }

        HashMap newMap = new UppercaseKeyMap();
        newMap.put("accountDate", getAccountingDate(connection, new java.util.Date()));
        newMap.put("storeID", (String)fieldMap.get("storeID"));
        if (fieldMap.get("systemDate") instanceof java.util.Date)
            newMap.put("voidDateTime", fieldMap.get("systemDate"));
        else 
        	newMap.put("voidDateTime", Timestamp.valueOf((String)fieldMap.get("systemDate")));
        newMap.put("posNumber", new Integer(voidPosNumber));
        newMap.put("cashierID", (String)fieldMap.get("cashier"));
        newMap.put("transactionNumber", new Integer(voidTranNumber));

        String dt1 = (String)fieldMap.get("dealType1");
        String dt2 = (String)fieldMap.get("dealType2");
        String dt3 = (String)fieldMap.get("dealType3");

        if (dt1.equals("0") && dt2.equals("0") && dt3.equals("1")) {
            newMap.put("voidType", "1");
        } else if (dt1.equals("0") && dt2.equals("3") && dt3.equals("4")) {
            newMap.put("voidType", "2");
        } else if (dt1.equals("0") && dt2.equals("0") && dt3.equals("2")) {
            newMap.put("voidType", "3");
        } else if (dt1.equals("0") && dt2.equals("0") && dt3.equals("3")) {
            newMap.put("voidType", "4");
        } else {
        }

        newMap.put("voidInvoiceHead", voidT.getInvoiceID());
        newMap.put("voidInvoiceNumber", voidT.getInvoiceNumber());
        newMap.put("invoiceCount", voidT.getInvoiceCount());
        newMap.put("netSaleTotalAmount", voidT.getNetSalesTotalAmount());
        newMap.put("netSaleTax0Amount", voidT.getNetSalesAmount0());
        newMap.put("netSaleTax1Amount", voidT.getNetSalesAmount1());
        newMap.put("netSaleTax2Amount", voidT.getNetSalesAmount2());
        newMap.put("netSaleTax3Amount", voidT.getNetSalesAmount3());
        newMap.put("netSaleTax4Amount", voidT.getNetSalesAmount4());
        try {
        	PostProcessorAdapter.insert(connection, newMap, "voidinvo");
        } catch (SQLException e) {
        	// 交易可能重传
        	if (e.getMessage().indexOf("Duplicate entry") < 0)
        		throw e;
        }
    }

    private void insertSalertn(DbConnection connection, Map tranMap, List<Map> details)
			throws SQLException {

		String storeID = (String) tranMap.get("storeID");
		String posNumber = tranMap.get("posNumber").toString();
		String tranNumber = tranMap.get("transactionNumber").toString();
		String cashier = (String) tranMap.get("cashier");
		java.util.Date systemDate = null;
        if (tranMap.get("systemDate") instanceof java.util.Date)
        	systemDate = (java.util.Date) tranMap.get("systemDate");
        else 
        	systemDate = Timestamp.valueOf((String)tranMap.get("systemDate"));
		// String dt1 = (String)fieldMap.get("dealType1");
		String dt2 = (String) tranMap.get("dealType2");
		// String dt3 = (String)fieldMap.get("dealType3");

		Iterator itrDetails = details.iterator();
		while (itrDetails.hasNext()) {
			Map detail = (Map) itrDetails.next();
			String itemNumber = (String) detail.get("itemNumber");
			HYIDouble quantity = (HYIDouble)detail
					.get("quantity");
			HYIDouble unitPrice = (HYIDouble) detail
					.get("unitPrice");
			int lineItemSequence = (Integer) detail
					.get("lineItemSequence");

			PLU plu = PLU.queryByItemNumber(itemNumber);
			if (plu == null) {
				Server.log("insertSalertn(): Cannot find: " + "itemNumber[ "
						+ itemNumber + "] in posdl_plu");
				continue;
			}

			HashMap newMap = new UppercaseKeyMap();
			ArrayList last = new ArrayList();
			if (dt2.equals("0")) {
				last = PostProcessorAdapter.getCollectionOfStatement(
						connection,
						"SELECT * FROM salertn WHERE transactionNumber = "
								+ (Integer.parseInt(tranNumber) - 1)
								+ " AND posNumber = '" + posNumber + "'");

				Iterator itr = last.iterator();
				while (itr.hasNext()) {
					Map next = (Map) itr.next();
					HYIDouble amt = (HYIDouble) next.get("salertnquantity");
					String number = (String) next.get("itemnumber");
					if (amt.abs().compareTo(quantity.abs()) == 0
							&& number.compareTo(plu.getInStoreCode()) == 0) {
						// storeID + busiDate + posNumber + transactionNumber +
						// lineItemSequence
						String delete = "DELETE FROM salertn"
								+ " WHERE storeID = '" + next.get("storeid")
								+ "' AND busiDate = '" + next.get("busidate")
								+ "' AND posNumber = '" + next.get("posnumber")
								+ "' AND transactionNumber = "
								+ next.get("transactionnumber")
								+ " AND lineItemSequence = "
								+ next.get("lineitemsequence");
						PostProcessorAdapter.executeQuery(connection, delete);

						// Bruce> Add "break" to fix a bug:
						// Buy (1) A x 1
						// (2) A x 1
						// (3) B x 2
						// (4) C x 3
						// if 部分退货 (1),(3),(4), then it'll delete all 'A' in
						// salertn
						break;
					}
				}
				continue;
			} // end if (dt2.equals("0"))

			newMap.put("busiDate", getBusinessDate(connection, systemDate));
			newMap.put("storeID", storeID);
			newMap.put("posNumber", new Integer(posNumber));
			newMap.put("transactionNumber", new Integer(tranNumber));
			newMap.put("lineItemSequence", lineItemSequence);
			newMap.put("itemNumber", plu.getInStoreCode());
			newMap.put("shipNumber", "0");
			newMap.put("updateUserID", cashier);
			newMap.put("updateDate", new java.util.Date());
			newMap.put("saleRtnQuantity", quantity.abs());
			newMap.put("storeUnitPrice", unitPrice);
			Map m = PostProcessorAdapter.query(connection,
					"SELECT unitCost FROM plu WHERE itemNumber = '"
							+ plu.getInStoreCode() + "'");
			HYIDouble unitCost = (HYIDouble) m.get("unitcost");
			newMap.put("unitCost", unitCost);
			try {
				PostProcessorAdapter.insert(connection, newMap, "salertn");
			} catch (SQLException e) {
				// 交易可能重传
				if (e.getMessage().indexOf("Duplicate entry") < 0)
					throw e;
			}
		}
}
// private boolean updateInv(Element elTranHead, Element elTranDetail) {
// String storeID = elTranHead.getChildText("storeID");
// String systemDate = elTranHead.getChildText("systemDate");
//        String dt1 = elTranHead.getChildText("dealType1");
//        String dt2 = elTranHead.getChildText("dealType2");
//        String dt3 = elTranHead.getChildText("dealType3");
//
//        String itemNo = elTranDetail.getChildText("itemNumber");
//        HYIDouble quantity = new HYIDouble(elTranDetail.getChildText("quantity"));
//        String detailCode = elTranDetail.getChildText("detailCode");
//
//        if (detailCode.equals("D") || // 折扣
//            detailCode.equals("I") || // 代售
//            detailCode.equals("M") || // 组合促销
//            detailCode.equals("O") || // 代收公共事业费
//            detailCode.equals("Q")) // 代付
//            return true;
//
//        // System.out.println("updateInv");
//
//        HashMap pluQtyMap = new HashMap();
//        DbConnection con = null;
//        Statement stmt = null;
//        ResultSet rst = null;
//        try {
//            con = CreamToolkit.getPooledConnection();
//            stmt = con.createStatement();
//
//            rst = stmt.executeQuery(" SELECT * FROM itempack WHERE StoreID='" + storeID
//                + "' AND packNo='" + itemNo + "'");
//            while (rst.next()) {
//                pluQtyMap.put(rst.getString("itemNo"), quantity.multiply(new HYIDouble(rst
//                    .getInt("packQty"))));
//            }
//            // get original itemNumber when no itempack record existed
//            if (pluQtyMap.size() == 0) {
//                pluQtyMap.put(itemNo, quantity);
//            }
//        } catch (SQLException e) {
//            // get original itemNumber when no itempack table existed
//            pluQtyMap.put(itemNo, quantity);
//
//        } finally {
//            try {
//                if (stmt != null)
//                    stmt.close();
//            } catch (SQLException e) {
//                e.printStackTrace(Server.getLogger());
//            }
//            if (con != null)
//                CreamToolkit.releaseConnection(con);
//        }
//
//        String shipNumber = "0";
//        SimpleDateFormat format = CreamCache.getInstance().getDateFormate();
//
//        java.util.Date d = Timestamp.valueOf(systemDate);
//        String busiDate = format.format(getBusinessDate(d)).toString();
//        String itemNumber;
//        HYIDouble itemQuantity;
//        Iterator itr = pluQtyMap.keySet().iterator();
//
//        while (itr.hasNext()) {
//            itemNumber = (String)itr.next();
//            itemQuantity = (HYIDouble)pluQtyMap.get(itemNumber);
//
//            String select = "SELECT * FROM inv ";
//            String updateString = "UPDATE inv SET ";
//            String where = " WHERE storeID = '" + storeID + "' AND busiDate = '" + busiDate
//                + "' AND itemNumber = '" + itemNumber + "' AND shipNumber = '" + shipNumber + "'";
//
//            Map map = PostProcessorAdapter.query(select + where);
//
//            if (map.isEmpty()) {
//                Server.log("Cannot find inv with number of: " + itemNumber);
//                Server.log(select + where);
//                return false;
//            }
//
//            HYIDouble accSaleQuantity = (HYIDouble)map.get("accsalequantity");
//            HYIDouble accSaleRtnQuantity = (HYIDouble)map.get("accsalertnquantity");
//            String fieldName1 = "accsalequantity";
//            String fieldName2 = "accsalertnquantity";
//
//            Server.log("dt1=" + dt1 + ",dt2=" + dt2 + ",dt3=" + dt3);
//            String invStr = "Sale qty of " + itemNumber + ":" + accSaleQuantity;
//            String rtnStr = "Return qty of " + itemNumber + ":" + accSaleRtnQuantity;
//
//            if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("1")) {// 前笔误打
//                accSaleQuantity = accSaleQuantity.addMe(itemQuantity);
//                updateString += fieldName1 + "=" + accSaleQuantity + "";
//                invStr += "->" + accSaleQuantity;
//                Server.log(invStr);
//            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("3") && dt3.equals("4")) {// 全部退货
//                accSaleRtnQuantity = accSaleRtnQuantity.addMe(itemQuantity.negate());
//                updateString += fieldName2 + "=" + accSaleRtnQuantity + "";
//                rtnStr += "->" + accSaleRtnQuantity;
//                Server.log(rtnStr);
//            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("4")) {// 部分退货
//                accSaleRtnQuantity = accSaleRtnQuantity.addMe(itemQuantity.negate());
//                updateString += fieldName2 + "=" + accSaleRtnQuantity + "";
//                rtnStr += "->" + accSaleRtnQuantity;
//                Server.log(rtnStr);
//            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("3")) {// 交易取消
//                accSaleQuantity = accSaleQuantity.addMe(itemQuantity);
//                updateString += fieldName1 + "=" + accSaleQuantity + "";
//                invStr += "->" + accSaleQuantity;
//                Server.log(invStr);
//            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("2")) {// 卡纸重印
//                accSaleQuantity = accSaleQuantity.addMe(itemQuantity);
//                updateString += fieldName1 + "=" + accSaleQuantity + "";
//                invStr += "->" + accSaleQuantity;
//                Server.log(invStr);
//            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("0")) {// 正常交易
//                accSaleQuantity = accSaleQuantity.addMe(itemQuantity);
//                updateString += fieldName1 + "=" + accSaleQuantity + "";
//                invStr += "->" + accSaleQuantity;
//                Server.log(invStr);
//            } else {
//                return true;
//            }
//
//            updateString += where;
//            PostProcessorAdapter.update(updateString);
//            Collection col = PostProcessorAdapter.getFieldList("inv");
//            if (col.contains("modifyDate".toLowerCase())) {
//                PostProcessorAdapter.update("UPDATE inv SET modifyDate = '"
//                    + getBusinessDate(new java.util.Date()) + "' " + where);
//            }
//
//            // 更新inv表中期初数量
//            if (!getBusinessDate(new java.util.Date()).equals(busiDate)) {
//                // 参数列表： 店号，交易日，品号，趟次，调整数量
//                if (!updateBeginInvQty(storeID, busiDate, itemNumber, shipNumber, quantity)) {
//                    Server.log("Error occured when update begin inv quantity: " + "storeID = "
//                        + storeID + ", " + "itemNumber = " + itemNumber + ", " + "busiDate = "
//                        + busiDate + ", " + "shipNumber = " + shipNumber);
//                }
//            }
//
//        } // end while
//
//        return true;
//    }

//    /**
//     * 更新busiDate 以后各天的期初数量 参数列表 String storeID : 店号 String busiDate : 业务日期 String itemNo : 品号
//     * String shipNo : 趟次 HYIDouble deltaQty : 调整数量（需要从期初数量中减去）
//     */
//    private boolean updateBeginInvQty(String storeID, String busiDate, String itemNo,
//        String shipNo, HYIDouble deltaQty) {
//
//        String updateString = "UPDATE inv SET befInvQuantity = befInvQuantity - " + deltaQty + " "
//            + "WHERE storeID = '" + storeID + "' AND " + "itemNumber = '" + itemNo + "' AND "
//            + "shipNumber = '" + shipNo + "' AND " + "busiDate > '" + busiDate + "'";
//
//        return PostProcessorAdapter.update(updateString);
//    }

    /**
     * sqlStatement 为完整的Insert语句
     */
    private Map getFieldMap(String sqlStatement) {
        HashMap newMap = new UppercaseKeyMap();
        String s1 = sqlStatement
            .substring(sqlStatement.indexOf("(") + 1, sqlStatement.indexOf(")"));
        String s2 = sqlStatement.substring(sqlStatement.lastIndexOf("(") + 1, sqlStatement
            .lastIndexOf(")"));

        StringTokenizer stk1 = new StringTokenizer(s1, ",");
        StringTokenizer stk2 = new StringTokenizer(s2, ",");

        while (stk1.hasMoreTokens()) {
            String fieldName = (String)stk1.nextToken();
            String tmp1 = (String)stk2.nextToken();
            String value = null;
            int i1 = tmp1.indexOf('\'');
            int i2 = tmp1.lastIndexOf('\'');
            if (!(i1 == i2 || i1 + 1 == i2))
                value = tmp1.substring(i1 + 1, i2);

            newMap.put(fieldName, value);
        }

        return newMap;
    }

    /**
     * 根据交易时间决定会计日期。
     * 
     * @param date
     *            交易时间
     * @return 返回会计日期
     */
    private java.util.Date getAccountingDate(DbConnection connection, java.util.Date date)
        throws SQLException {

        String changeTime = "23:59:59";
        Map value = PostProcessorAdapter
            .query(connection, "SELECT changetime FROM calendar WHERE dateType = '1'");
        if (value != null && value.containsKey("changetime")) {
            changeTime = (String)value.get("changetime").toString();
            if (changeTime.trim().length() == 0 || changeTime.equals("00:00:00")) {
                changeTime = "23:59:59";
            }
        }
        String realTime = new SimpleDateFormat("HH:mm:ss").format(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // 有两种决定会计日期的models:
        // 如果AccountingDateShiftTime="afternoon"(default)，则超过换日时间后为(d+1)日；
        // 否则(="morning")超过换日时间后仍为(d)日，在换日之前为(d-1)日。
        boolean afternoon = GetProperty.getAccountingDateShiftTime("afternoon").equalsIgnoreCase(
            "afternoon");
        if (afternoon) {
            if (realTime.compareTo(changeTime) > 0) {
                cal.add(Calendar.DATE, 1);
            }
        } else {
            if (realTime.compareTo(changeTime) <= 0) {
                cal.add(Calendar.DATE, -1);
            }
        }
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * 根据交易时间决定营业日期。
     * 
     * @param date
     *            交易时间
     * @return 返回营业日期
     */
    private java.util.Date getBusinessDate(DbConnection connection, java.util.Date date)
        throws SQLException {

        String changeTime = "22:59:59";
        Map value = PostProcessorAdapter
            .query(connection, "SELECT changetime FROM calendar WHERE dateType = '0'");
        if (value != null && value.containsKey("changetime")) {
            changeTime = (String)value.get("changetime").toString();
            if (changeTime.trim().length() == 0 || changeTime.equals("00:00:00")) {
                changeTime = "22:59:59";
            }
        }
        String realTime = new SimpleDateFormat("HH:mm:ss").format(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (realTime.compareTo(changeTime) > 0) {
            cal.add(Calendar.DATE, 1);
        }
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    class NameFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            if (name.endsWith(".xml"))
                return true;
            else
                return false;
        }
    }
}
