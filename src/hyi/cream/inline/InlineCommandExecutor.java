package hyi.cream.inline;

import hyi.cream.dac.Attendance;
import hyi.cream.state.GetProperty;
import hyi.cream.groovydac.Param;
import org.apache.commons.lang.StringUtils;

import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 * Inline command executor.
 *
 * @author Bruce You
 * @since 2007-10-28
 */
public class InlineCommandExecutor extends Thread
{
    // Singleton ---

    private static InlineCommandExecutor instance = new InlineCommandExecutor();

    private InlineCommandExecutor() {
        initialize();
    }

    public static InlineCommandExecutor getInstance() {
        return instance;
    }

    // Main body ---

    private LinkedList commandQueue = new LinkedList();

    private void initialize()
    {
        // Try to slow down the inline command executor thread, to make the foreground
        // process more responsive.
        setPriority(Thread.MIN_PRIORITY);

        start();
    }

    public void run()
    {
        String command;
        boolean thisTimePause = true;
        boolean nextTimePause;
        for (;;) {
            synchronized (commandQueue) {
                try {
                    if (commandQueue.size() == 0)
                        commandQueue.wait();

                    command = (String)commandQueue.removeFirst();
                    if (StringUtils.isEmpty(command)) // for safe
                        continue;

                    nextTimePause = commandQueue.isEmpty();
                    // 拿到空掉的時候，下次的第一個command執行之前要暫停3秒
                    // 因為可能command送過來的時候，database還沒commit，執行太快怕會拿不到數據
                    
                } catch (Exception e) {
                    e.printStackTrace(Client.getLogger());
                    continue;
                }
            }
            try {
                if (thisTimePause)
                    sleep(3000);
                thisTimePause = nextTimePause; 

                Client inlineClient = Client.getInstance();
                String lowerCaseCommand = command.toLowerCase();
                if (lowerCaseCommand.startsWith("putz")) {
                    sendZReport(inlineClient, command);
                } else if (lowerCaseCommand.startsWith("putshift")) {
                    sendShiftReport(inlineClient, command);
                } else {
                    inlineClient.processCommand(command);
                }
            } catch (Exception e) {
                // block for any exception for making it more robust
                e.printStackTrace(Client.getLogger());
            }
        }
    }

    public void execute(String command) {
        // put command into queue only when POS and SC are well-connected
        if (Client.getInstance().isConnected()) {
            synchronized (commandQueue) {
                if (!commandQueue.contains(command)) {
                    commandQueue.add(command);
                    commandQueue.notifyAll();
                }
            }
        }
    }

    private boolean sendZReport(Client client, String putZCommand) {
        try {
            StringTokenizer t = new StringTokenizer(putZCommand, " \t");
            t.nextToken();
            String zNumber = t.nextToken();

            client.processCommand("putDepSales " + zNumber);
            client.processCommand("putDaiShouSales " + zNumber);
            client.processCommand("putDaiShouSales2 " + zNumber);
            client.processCommand("putAttendance " + zNumber);
            client.processCommand("putZEx " + zNumber);
            client.processCommand("putZ " + zNumber); // putZ/putZEx at last

            //if (GetProperty.getIsGenHistoryTrans("no").equalsIgnoreCase("yes")) {
            //    HistoryTrans.genHistoryTrans(Integer.parseInt(zNumber.trim()));
            //    Client.getInstance().processCommand("putHistoryTrans " + zNumber);
            //}

            Client.log("ZReport upload success. (command = " + putZCommand + ")", false);
            return true;

        } catch (ClientCommandException e) {
            e.printStackTrace(Client.getLogger());
            Client.log("ZReport upload failed! (command = " + putZCommand + ")", false);
            return false;
        } catch (Exception e) {
            e.printStackTrace(Client.getLogger());
            Client.log("ZReport upload failed! (command = " + putZCommand + ")", false);
            return false;
        }
    }

    private boolean sendShiftReport(Client client, String putShiftCommand) {
        try {
            StringTokenizer t = new StringTokenizer(putShiftCommand, " \t");
            t.nextToken();
            String zNumber = t.nextToken();
            String shiftNumber = t.nextToken();

            client.processCommand(putShiftCommand);
            client.processCommand("putShiftEx " + zNumber + " " + shiftNumber);

            if (Param.getInstance().isSendPunchInOutDetail()) {
                Attendance.genAttendance(zNumber);
                Client.getInstance().processCommand("putWorkCheck");
            }
            if (Param.getInstance().isUploadVersion()) {
                hyi.cream.dac.Version.genVersion();
                Client.getInstance().processCommand("putPosVersion");
            }
            return true;

        } catch (ClientCommandException e) {
            e.printStackTrace(Client.getLogger());
            Client.log("ShiftReport upload failed! (command = " + putShiftCommand + ")", false);
            return false;
        } catch (Exception e) {
            e.printStackTrace(Client.getLogger());
            Client.log("ShiftReport upload failed! (command = " + putShiftCommand + ")", false);
            return false;
        }
    }

    public boolean isQueueEmpty() {
        return commandQueue.isEmpty();
    }

    public int getQueueSize() {
        return commandQueue.size();
    }
}
