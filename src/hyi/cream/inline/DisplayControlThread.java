package hyi.cream.inline;

class DisplayControlThread extends Thread {

    private int countDown = 6;
    private int posNumber;
    private POSStatusTableModel model;
    private boolean needSwitchOff;
    private Thread thisThread;

    DisplayControlThread(int posNumber, POSStatusTableModel model) {
        this.posNumber = posNumber;
        this.model = model;
    }

    synchronized public void resetCountDown() {
        needSwitchOff = false;
        countDown = 6;
        if (thisThread != null)
            thisThread.interrupt();
    }

    synchronized public int getCountDown() {
        return countDown;
    }

    synchronized public int countDown() {
        countDown--;
        return countDown;
    }

    public void prepareSwithOff() {
        if (getCountDown() <= 0) {
            needSwitchOff = true;
            switchOff();
        }
        needSwitchOff = true;
    }

    private void switchOff() {
        if (needSwitchOff) {
            model.clearSendingReceiving(posNumber);
            needSwitchOff = false;
        }
    }

    public void run() {
        thisThread = Thread.currentThread();
        while (true) {
            try {
                while (countDown() > 0) {
                    sleep(200);
                }
                switchOff();
                if (Thread.interrupted())
                    continue;
                sleep(999999999);
            } catch (InterruptedException e) {
                continue;
            }
        }
    }
}
