package hyi.cream.inline;

import hyi.cream.POSTerminalApplication;
import hyi.cream.groovydac.CardDetail;
import hyi.cream.groovydac.Param;
import hyi.cream.dac.*;
import hyi.cream.event.SystemInfoEvent;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamPropertyUtil;
import hyi.cream.util.CreamToolkit;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import hyi.cream.util.DbConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.*;

/**
 * Inline client.
 *
 * @author Bruce
 * @version 1.7
 */
public class ConnectionlessClient {

    private static boolean commandLineMode;

    private boolean connectOrig;

    private String accdateStr;

    /**
     * /Bruce/1.7/2002-04-27/ 增加上传代收明细帐.
     *
     * /Bruce/1.6/2002-04-13/ Modify socket timeout interval.
     *
     * /Bruce/1.5/2002-04-03/ socket.setSoTimeout(TIMEOUT_INTERVAL) on socket
     * creation.
     *
     * /Bruce/1.5/2002-04-01/ 1. Change isConnected() and setConnected() to
     * non-synchronized. 2. Log more detail.
     */

    public static final String FILE_ENCODING = "UTF-8";
    public static final String CLIENT_SQL_FILE = "client.sql";

    // Things could be changed.
    // 2003-02-10 Modified by Meyer: add 4 table to download
    private static Class[] dacForDownload = new Class[] { PLU.class, Cashier.class, Reason.class,
        MixAndMatch.class, Payment.class, TaxType.class, Dep.class, Category.class, Store.class,
        MMGroup.class, CombDiscountType.class, CombGiftDetail.class, CombPromotionHeader.class,
        CombSaleDetail.class, DaiShouDef.class, SI.class, Rebate.class, DiscountType.class,
        PosButton3.class, Member.class };

    // 2005-11-29 allan : 下传表由配置决定
    private List<Class> allDacs;

    private static final int SEND_RUOK_INTERVAL = 31680;  // "168" for Rika 2007-05-09, Bruce, Kaohsiung

    private static final int DEFAULT_TIMEOUT_INTERVAL = 60000;

    private static final int RUOK_TIMEOUT_INTERVAL = 10000;

    private static final int ACK_TIMEOUT_INTERVAL = 120000;

    private static final int LONG1_TIMEOUT_INTERVAL = 240000;

    private static final int SYNC_TIMEOUT_INTERVAL = 100000;

    // Other private fields.
    private Socket socket;

    private String server;

    private int port;

    private DataOutputStream out;

    private DataInputStream in;

    // private BufferedInputStream buffIn;
    private ObjectOutputStream objectOut;

    private ObjectInputStream objectIn;

    private DacObjectOutputStream dacOut;

    private DacObjectInputStream dacIn;

    private boolean connected; // connection state

    private StringBuffer buff = new StringBuffer();

    private Thread ruokThread;

    private boolean firstTimeConnect = true;

    // private OutputStream logFile = System.out;

    private ArrayList scriptFiles = new ArrayList();

    private boolean autoSyncTransaction;

    private DacBase returnObject;

    private Object returnObject2;

    private int numberOfFilesGotOrMoved;

    private boolean firstTimeConnectSucc = true;

    //static private ConnectionlessClient client;

    static private DateFormat dateFormatter = CreamCache.getInstance()
            .getDateTimeFormate();

    static private int posNumber;

    static private PrintWriter logger;

    //// 需要上传的数据除盘点外都先放到里面
    //private Cargo cargo = Cargo.getInstance();

    static {
        openLog();
    }

    public Integer getMaxZNoInSC() {
        return maxZNoInSC;
    }

    public static void openLog() {
        try {
            String logFilePath = "inline.log"; // GetProperty.getInlineClientLogFile("");
            if (!logFilePath.equals("")) {
                logger = new PrintWriter(new OutputStreamWriter(
                        new FileOutputStream(logFilePath, true), FILE_ENCODING));
            } else {
                logger = new PrintWriter(new OutputStreamWriter(
                        new FileOutputStream("/root/inline.log", true),
                        FILE_ENCODING));
            }
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    public static void closeLog() {
        if (logger != null) {
            logger.close();
            logger = null;
        }
    }

    /**
     * Meyer/2003-02-17 Get MixAndMatchVersion
     */
    private String mixAndMatchVersion;

    /**
     * ZhaoHong /2003-06-18 Get DaiShouVersion
     */
    // private String daiShouVersion;
    /*
     * 2003-02-10 / Added By meyer 资源
     */
    private static ResourceBundle res = CreamToolkit.GetResource();

    private Integer maxZNoInSC;

    public ConnectionlessClient() throws InstantiationException {
        //if (client != null)
        //    throw new InstantiationException();
        try {
            posNumber = Param.getInstance().getTerminalNumber();
            autoSyncTransaction = Param.getInstance().isAutoSyncTransaction();
            mixAndMatchVersion = Param.getInstance().getMixAndMatchVersion();
            // daiShouVersion =
            // GetProperty.getProperty("DaiShouVersion");
            // if (daiShouVersion == null)
            // daiShouVersion = "";
        } catch (NumberFormatException e) {
            log(e);
        }
    }

    public ConnectionlessClient(String server, int port) throws InstantiationException {
        this();
        this.server = server; // remember the server and port connected
        this.port = port;
        //ConnectionlessClient.client = this;
        connect(server, port);
//        if (!GetProperty.getUseRuok("yes").equalsIgnoreCase("no")) {
//            start(); // start a ruok thread
//        }
    }
/*
    public ConnectionlessClient(String server, int port, boolean commandLineMode) throws InstantiationException {
        this();
        this.commandLineMode = commandLineMode;
        this.server = server; // remember the server and port connected
        this.port = port;
        ConnectionlessClient.client = this;
        connect(server, port);
        if (!GetProperty.getUseRuok("yes").equalsIgnoreCase("no")) {
            start(); // start a ruok thread
        }
    }

    public static ConnectionlessClient getInstance() {
        try {
            if (client == null)
                client = new ConnectionlessClient();
            return client;
        } catch (InstantiationException e) {
            return null;
        }
    }
*/
    // synchronized public boolean isConnected() {
    public boolean isConnected() {
        return connected;
    }

    // synchronized public void setConnected(boolean c) {
    public void setConnected(boolean c) {
        connected = c;

        if (!commandLineMode) {
            if (connectOrig != connected) {
                connectOrig = connected;
                try {
                    POSTerminalApplication.getInstance().getSystemInfo()
                            .fireEvent(new SystemInfoEvent(this));
                } catch (NullPointerException e) {
                }
            }
        }
    }

    synchronized public Socket getSocket() {
        return socket;
    }

    synchronized public void setSocket(Socket s) {
        socket = s;
    }

    private void prompt(String s) {
        // if (commandLineMode)
        // System.out.println(s);
        log(s, true);
    }

    synchronized static public void log(String message, boolean displayToConsole) {
        // message = dateFormatter.format(new java.util.Date()) + message;\
        if (displayToConsole)
            System.out.println(message);
        if (posNumber == 0)
            message = "[ ] " + message;
        else
            message = "[" + posNumber + "] " + message;
        message = "[" + dateFormatter.format(new Date()) + "]"
                + message;
        // System.out.println(message);
        logger.println(message);
        logger.flush();
    }

    static public PrintWriter getLogger() {
        return logger;
    }

    /**
     * Thread for keep sending "ruok?" (Are you ok?) and "synctrans" message to
     * server.
     */
/*    public void run() {
        ruokThread = Thread.currentThread();
        int ruokSynctransSwitcher = 0;
        int autoSyncTransactionTimes = 50;
        try {
            autoSyncTransactionTimes = Integer.parseInt(GetProperty.getAutoSyncTransactionTimes("50"));
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        }
        while (true) {
            try {
                if (Thread.currentThread().isInterrupted())
                    break;

                if (!isConnected()) {
                    if (getSocket() != null)
                        closeConnection();
                    connect(server, port);
                    // sendRuok();
                    processCommand("ruok");
                    // Thread.sleep(SEND_RUOK_INTERVAL);
                }

//                DacBase dac =null;
//                DbConnection conn = null;
//                dac = dac.getValueOfStatement(conn,"selectStatement");
//                if(uploadDac(dac)){
//                    sendRuok();
//                }else{
//
//                };

                if (ruokSynctransSwitcher % autoSyncTransactionTimes != 0
                        || !autoSyncTransaction) {
                    sendRuok();
                    Trigger.getInstance().run();
                } else {
                    try { // synctrans
                        ruokSynctransSwitcher = 0;
                        processCommand("SyncTransaction");
                    } catch (ClientCommandException e) {
                        log(e);
                    }
                }
                ruokSynctransSwitcher++;
                //cargo.takeAction();

                // if connection is broken, try connecting again
                Thread.sleep(SEND_RUOK_INTERVAL);
            } catch (InterruptedException e) {
                prompt("ruok thread is terminated.");
                break;
            } catch (ClientCommandException e) {
                prompt("ruok thread is terminated.");
                break;
            }
        }
    }
*/
    public boolean connect(String server, int port) {
        try {
            setConnected(false);
            // prompt("Try connect to " + server + ":" + port + "...");
            socket = new Socket(server, port);
            socket.setSoTimeout(DEFAULT_TIMEOUT_INTERVAL);
            this.server = server; // remember the server and port connected
            this.port = port;
            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());
            dacOut = new DacObjectOutputStream(socket.getOutputStream());
            dacIn = new DacObjectInputStream(socket.getInputStream());
            // buffIn = new BufferedInputStream(in);
            objectOut = new ObjectOutputStream(new BufferedOutputStream(out));
            prompt("POS is connected to inline server.");
            setConnected(true);

            /*
            // if connected first time, get max transaction number from SC
            if (firstTimeConnectSucc) {
                if (!commandLineMode)
                    adjustNextTransactionNumber();
                firstTimeConnectSucc = false;
            }
            */
            return true;
        } catch (UnknownHostException e1) { // if the IP address of the host
            // could not be determined.
            if (isConnected() || firstTimeConnect) {
                e1.printStackTrace(getLogger());
                closeConnection();
                prompt("POS cannot connect to inline server.(" + e1 + ")");
            }
            setConnected(false);
            return false;
        } catch (IOException e2) { // if an I/O error occurs when creating the
            // socket.
            if (isConnected() || firstTimeConnect) {
                e2.printStackTrace(getLogger());
                closeConnection();
                prompt("POS cannot connect to inline server.(" + e2 + ")");
            }
            setConnected(false);
            return false;
        } catch (SecurityException e3) { // a security manager exists and its
            // checkConnect method doesn't allow
            // the operation
            if (isConnected() || firstTimeConnect) {
                e3.printStackTrace(getLogger());
                closeConnection();
                prompt("POS cannot connect to inline server.(" + e3 + ")");
            }
            setConnected(false);
            return false;
        } finally {
            firstTimeConnect = false;
        }
    }

    private void adjustNextTransactionNumber() {
        DbConnection connection = null;
        try {
            processCommand("queryMaxTransactionNumber");
            SequenceNumberGetter m = (SequenceNumberGetter)getReturnObject();
            if (m != null) {
                Integer maxTranNo = m.getMaxTransactionNumber();
                if (maxTranNo != null) {
                    int nextTranNoInSC = maxTranNo + 1;
                    int nextTranNoInPOS = Param.getInstance().getNextTransactionNumber();
                    if (nextTranNoInSC > nextTranNoInPOS) {
                        Param.getInstance().updateNextTransactionNumber(nextTranNoInSC);
                    }
                }
            }
            processCommand("queryMaxZNumber");
            m = (SequenceNumberGetter)getReturnObject();
            if (m != null)
                maxZNoInSC = m.getMaxZNumber();
        } catch (ClientCommandException e) {
            log(e);
        } catch (NumberFormatException e) {
            log(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    synchronized public void closeConnection() {
        try {
            if (out != null) {
                out.close();
                out = null;
            }
            if (in != null) {
                in.close();
                in = null;
            }
            if (socket != null) {
                socket.close();
                socket = null;
            }
            objectIn = null;
            objectOut = null;
            dacOut = null;
            dacIn = null;
            // System.out.println("socket closed.");
        } catch (IOException e) {
            prompt(e.toString());
            log(e);
        }
    }

    void log(Throwable e) {
        e.printStackTrace(getLogger());
    }

    synchronized private void sendRuok() {
        if (!isConnected())
            return;

        int oldSoTimeout;
        try {
            oldSoTimeout = socket.getSoTimeout();
            socket.setSoTimeout(RUOK_TIMEOUT_INTERVAL);
            // System.out.println("Client> Send ruok.");
            sendMessage("ruok?" + Store.getStoreID());

            // System.out.println("Client> Write pos number.");
            out.writeInt(posNumber);

            // System.out.println("Client> Get response...");
            String response = getMessage(); // return "yes" if normal
            // System.out.println("Client> Ruok get: " + yes);
            // System.out.println("Client> Set timeout back");
            socket.setSoTimeout(oldSoTimeout);

            boolean responseYes = response.startsWith("yes");
            // boolean connectedStatus = yes.equals("yes");
            if (response.startsWith("no")) {
                closeConnection();  // close connection silently
                setConnected(false);
                return;
            } else if (responseYes && !response.equals("yes")) { // it must be
                // priceChangeID
                String piggybackCmd = response.substring(3);
                String priceChangeVersion = Param.getInstance().getPriceChangeVersion();
                if (!priceChangeVersion.equalsIgnoreCase(piggybackCmd)) {
                    DbConnection connection = null;
                    try {
                        // sleep for a while in case SC has not yet completely
                        // insert record into posdl_deltaplu
                        Thread.sleep(5 * 1000);
                        processCommand("getPLUPriceChange");
                        Param.getInstance().updatePriceChangeVersion(piggybackCmd);
                    } catch (InterruptedException e) {
                        log(e);
                    } catch (ClientCommandException e) {
                        log(e);
                    } finally {
                        CreamToolkit.releaseConnection(connection);
                    }
                }
            }

            if (isConnected() && !responseYes)
                prompt("POS cannot connect to inline server.");

            setConnected(responseYes);
        } catch (SocketException e) {
            log(e);
            if (isConnected()) {
                prompt("POS cannot connect to inline server.(" + e + ")");
                closeConnection();
            }
            setConnected(false);
        } catch (IOException e) {
            log(e);
            if (isConnected()) {
                prompt("POS cannot connect to inline server.(" + e + ")");
                closeConnection();
            }
            setConnected(false);
        } catch (Exception e) {
            log(e);
            if (isConnected()) {
                prompt("POS cannot connect to inline server.(" + e + ")");
                closeConnection();
            }
            setConnected(false);
        }
    }

    synchronized public int countDoingInventory() {
        int result = -1; // mean fail
        int oldSoTimeout;
        if (!isConnected()) {
            return result;
        }
        try {
            oldSoTimeout = socket.getSoTimeout();
            socket.setSoTimeout(LONG1_TIMEOUT_INTERVAL);
            // log("bef sendMessage(countDoingInventory)" + new
            // java.util.Date());
            sendMessage("countDoingInventory");
            // log("after sendMessage(countDoingInventory)" + new
            // java.util.Date());
            result = in.readInt();
            // log("arter readInt result = " + result + " "
            // + new java.util.Date());
            socket.setSoTimeout(oldSoTimeout);
        } catch (IOException e) {
            e.printStackTrace();
            log(e);
        }
        return result;
    }

    synchronized public boolean compareInventoryRecCount() {
        boolean result = false;
        int a = getPosInventoryRecCount();
        // log("Rec of POS inventory(pandin) : " + a);
        int b = countPosul_inventoryRec();
        // log("Rec of SC posul_inventory(pandin) : " + b);
        if (a == -1 || b == -1) {
            // log("Error on compareInventoryRecCount!!!");
        } else if (a == b) {
            result = true;
        }
        // log("If it equals : " + result);
        return result;
    }

    synchronized private int countPosul_inventoryRec() { // Does it need
        // synchronized???
        // log("countPosul_inventoryRec() begin ...");
        int result = -1;
        if (!isConnected()) {
            return result;
        }
        try {
            sendMessage("countPosul_inventoryRec");
            out.writeInt(posNumber);
            result = in.readInt();
        } catch (IOException e) {
            e.printStackTrace();
            log(e);
        }
        return result;
    }

    synchronized public String turnSingleUlTable(String tableName) {
        // log("turnSingleUlTable(" + tableName + ") begin ... ");
        String result = "Fail";
        if (!isConnected()) {
            return result;
        }
        int oldSoTimeout;
        try {
            oldSoTimeout = socket.getSoTimeout();
            socket.setSoTimeout(LONG1_TIMEOUT_INTERVAL);
            sendMessage("turnSingleUlTable");
            // log("sent turnSingleUlTable ok");
            sendMessage(tableName);
            // log("sent tableName ok");
            result = getMessage();
            // log("turnSingleUlTable(" + tableName + ") end ... ");
            socket.setSoTimeout(oldSoTimeout);
        } catch (IOException e) {
            e.printStackTrace();
            log(e);
        }
        return result;
    }

    private void sendSyncTransaction() throws ClientCommandException {
        int[] beginAndEndTranNumber = Transaction.getBeginAndEndTranNumber();
        if (beginAndEndTranNumber != null)
            sendSyncTransaction(beginAndEndTranNumber);
    }

    private void sendSyncTransaction(int[] beginAndEndTranNumber)
            throws ClientCommandException {
        if (!isConnected())
            return;

        DbConnection connection = null;
        try {
            int i;
            int numOfTransactionRequested;
            int[] transactionNumbers = null;

            int maxTransactionNumber;
            connection = CreamToolkit.getPooledConnection();
            maxTransactionNumber = Transaction.getMaxTransactionNumber(connection);
            // pos无数据应该不必Sync
            if (maxTransactionNumber == -1) {
                log("Can't found transaction!", true);
                return;
            }
            // int[] beginAndEndTranNumber =
            // Transaction.getBeginAndEndTranNumber();
            int oldSoTimeout;
            synchronized (this) {
                if (out == null) // for safe
                    return;
                oldSoTimeout = socket.getSoTimeout();
                socket.setSoTimeout(SYNC_TIMEOUT_INTERVAL);
                sendMessage("synctrans");
                out.writeInt(posNumber);
                out.writeInt(maxTransactionNumber);
                out.writeInt(beginAndEndTranNumber[0]);
                out.writeInt(beginAndEndTranNumber[1]);
                CreamToolkit.logMessage("synctrans | posNumber : " + posNumber
                    + " | max : " + maxTransactionNumber + " | begin : "
                    + beginAndEndTranNumber[0] + " | end : "
                    + beginAndEndTranNumber[1]);
                numOfTransactionRequested = in.readInt();
                if (numOfTransactionRequested > 0) {
                    transactionNumbers = new int[numOfTransactionRequested];
                    for (i = 0; i < numOfTransactionRequested; i++)
                        transactionNumbers[i] = in.readInt();
                }

                socket.setSoTimeout(oldSoTimeout);
            }

            Thread currentThread = Thread.currentThread();
            InlineCommandExecutor inlineCommandExecutor = InlineCommandExecutor.getInstance();
            for (i = 0; i < numOfTransactionRequested; i++) {
                if (Transaction.existsTransaction(connection, posNumber, transactionNumbers[i])) {

                    //Bruce/20071030/ Process command by ourself instead of InlineCommandExecutor.
                    //Reason: 因为怕有可能一下子会放进去太多的command, 让重启动作无法进行(see DacTransfer.shutdown())
                    processCommand("putTransaction " + transactionNumbers[i]);
                    if (currentThread.isInterrupted())
                        return;

                    // Try to make foreground user interaction more responsive
                    Thread.yield();
                }
            }

            // 上传未上传的Z/Shift
            Iterator iter = ShiftReport.queryNotUploadReport(connection);
            if (iter != null) {
                while (iter.hasNext()) {
                    ShiftReport shift = (ShiftReport) iter.next();
                    inlineCommandExecutor.execute("putShift " + shift.getZSequenceNumber()
                        + " " + shift.getSequenceNumber());
                }
            }

            if (currentThread.isInterrupted())
                return;

            iter = ZReport.queryNotUploadReport(connection);
            if (iter != null) {
                while (iter.hasNext()) {
                    ZReport z = (ZReport) iter.next();
                    String zSeq = "" + z.getSequenceNumber();
                    inlineCommandExecutor.execute("putAttendance " + zSeq);
                    inlineCommandExecutor.execute("putZ " + zSeq);
                }
            }
        } catch (IOException e) {
            log(e);
            setConnected(false);
            throw new ClientCommandException(e.getMessage());
        } catch (SQLException e) {
            log(e);
            // CreamToolkit.haltSystemOnDatabaseFatalError();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    synchronized public void sendMessage(String str) throws IOException {
        if (!isConnected())
            return;

        out.write(str.getBytes(FILE_ENCODING));
        out.write('\n');
        out.flush();

    }

    synchronized public String getMessage() throws IOException {
        if (!isConnected())
            return "";

        buff.setLength(0);
        for (;;) {
            int c = in.read();
            if (c == '\n') {
                // prompt("Get " + buff.toString());
                // System.out.println(buff); System.out.flush();
                return buff.toString();
            }
            if (c == -1) {
                // prompt("Get " + buff.toString());
                // System.out.println(buff); System.out.flush();
                return buff.toString();
            }
            buff.append((char) c);
        }

    }

    synchronized public boolean sendObject(Object object) throws IOException {
        if (!isConnected())
            return false;

        String className = object.getClass().getName();
        sendMessage("putobject " + className + " 1");
        if (object instanceof DacBase) {
            objectOut.writeBoolean(true); // means DAC
            objectOut.flush();
            dacOut.writeObject((DacBase) object);
//          int oldSoTimeout = socket.getSoTimeout();
//          socket.setSoTimeout(ACK_TIMEOUT_INTERVAL);
            String message = getMessage();
            boolean gotOK = message.startsWith("OK");
            if (className.equalsIgnoreCase("hyi.cream.dac.ZReport")) {
                message = message.substring(2);
                if (message.length() > 0) {
                    this.accdateStr = message;
                }
            }
//          socket.setSoTimeout(oldSoTimeout);
            return gotOK;
        } else {
            objectOut.writeBoolean(false); // means not a DAC
            objectOut.writeObject(object);
            objectOut.flush();
            // objectOut.reset();
        }
        // Thread.sleep(3); // for advoid a problem when doing a local test
        return true;
    }

    synchronized public boolean sendObject(Object[] object) throws IOException {
        if (!isConnected())
            return false;

        if (object.length == 0)
            return true;

        String className = object[0].getClass().getName();

        boolean dacs = true;
        sendMessage("putobject " + className + " " + object.length);
        for (int i = 0; i < object.length; i++) {
            if (object[i] instanceof DacBase) {
                objectOut.writeBoolean(true); // means DAC
                objectOut.flush();
                dacOut.writeObject((DacBase)object[i]);
            } else {
                objectOut.writeBoolean(false); // means not a DAC
                dacs = false;
                objectOut.writeObject(object[i]);
                objectOut.flush();
            }
        }
        int oldSoTimeout = socket.getSoTimeout();
        socket.setSoTimeout(ACK_TIMEOUT_INTERVAL);
        String message = getMessage();
        boolean gotOK = message.startsWith("OK");
        if (className.equalsIgnoreCase("hyi.cream.dac.ZReport")) {
            message = message.substring(2);
            if (message.length() > 0) {
                this.accdateStr = message;
            }
        }
        socket.setSoTimeout(oldSoTimeout);
        if (dacs && !gotOK) {
            return false;
        }
        return true;
    }

    /**
     * "getobject" protocol starter.
     */
    synchronized public int requestObjectsAndGetCount(String className)
            throws IOException {
        if (!isConnected())
            return 0;

        try {
            socket.setSoTimeout(DEFAULT_TIMEOUT_INTERVAL);
            sendMessage("getobject " + className);
            if (objectIn == null) {
                objectIn = new ObjectInputStream(in);
            }
            Integer numOfObject = (Integer) objectIn.readObject();
            if (numOfObject == null || numOfObject.intValue() == -1) // get -1 if server failed
                return 0;
            return numOfObject.intValue();
        } catch (ClassNotFoundException e) {
            log(e);
            return 0;
        } catch (Exception e) {
            // Bruce/20030505
            if (isConnected()) {
                prompt("POS cannot connect to inline server.(" + e + ")");
                closeConnection();
                log(e);
            }
            setConnected(false);
            if (e instanceof IOException)
                throw (IOException) e;
            else
                throw new IOException(e.toString());
        }
    }

    /**
     * @ For Download Plu
     * @return
     */
    synchronized public int requestObjectsAndGetCount2(String className)
            throws IOException, ClientCommandException {
        if (!isConnected() || socket == null)
            return 0;
        try {
            sendMessage("getobject2 " + className);
            socket.setSoTimeout(LONG1_TIMEOUT_INTERVAL);
            int numOfObjects = in.readInt();
            log("requestObjectsAndGetCount2(): numofObjects=" + numOfObjects, false);
            socket.setSoTimeout(DEFAULT_TIMEOUT_INTERVAL);
            if (numOfObjects == -1) // get -1 if server failed
                throw new ClientCommandException("Get number of files failed");

            return numOfObjects;
        } catch (IOException e) {
            log(e);
            throw e;
        }
    }

    synchronized public Object[] requestObjects(String className) {
        if (!isConnected())
            return null;

        try {
            sendMessage("getobject " + className);
            if (objectIn == null) {
                objectIn = new ObjectInputStream(in);
            }
            Integer numOfObject = (Integer) objectIn.readObject();
            if (numOfObject == null || numOfObject.intValue() == -1) // get -1 if server failed
                return null;
            Object[] object = new Object[numOfObject.intValue()];
            for (int i = 0; i < numOfObject.intValue(); i++) {
                if (className.indexOf("hyi.cream.dac") != -1) {
                    object[i] = dacIn.readObject();
                    prompt("Get " + i + ":" + object[i]);
                } else {
                    object[i] = objectIn.readObject();
                }
            }
            return object;
            // NOTE: caller has to sendMessage("OK") after finishing its work
        } catch (InterruptedIOException e1) {
            e1.printStackTrace(getLogger());
            return null;
        } catch (Exception e2) {
            e2.printStackTrace(getLogger());
            return null;
        }
    }

    public ArrayList getScriptFiles() {
        return scriptFiles;
    }

    synchronized public void requestFiles(String serverDir, String clientDir,
            boolean moveFiles) throws ClientCommandException {
        if (!isConnected())
            throw new ClientCommandException("Not connected with server");

        if (!clientDir.endsWith(File.separator))
            clientDir += File.separator;

        scriptFiles.clear();
        try {
            if (moveFiles)
                sendMessage("movefile " + serverDir);
            else
                sendMessage("getfile " + serverDir);

            // Integer numOfFiles = (Integer)objectIn.readObject();
            numberOfFilesGotOrMoved = in.readInt();
            if (numberOfFilesGotOrMoved == -1) // get -1 if server failed
                throw new ClientCommandException("Get number of files failed");
            for (int i = 0; i < numberOfFilesGotOrMoved; i++) {
                String fileName = getMessage();
                // Integer fileSize = (Integer)objectIn.readObject();
                int fileSize = in.readInt();
                prompt("Get " + clientDir + fileName + "  size=" + fileSize);
                if (fileName.endsWith(".sh"))
                    scriptFiles.add(clientDir + fileName);
                if (fileSize == -1) // get -1 if server failed
                    throw new ClientCommandException("Get file size failed");
                byte[] fileContent = new byte[fileSize];
                // buffIn.read(fileContent);
                in.readFully(fileContent);
                OutputStream f = new BufferedOutputStream(new FileOutputStream(
                        clientDir + fileName));
                f.write(fileContent);
                f.flush();
                f.close();
            }
        } catch (IOException e) {
            throw new ClientCommandException(e.getMessage());
        }
    }

//    /**
//     * Download master tables from SC, then return.
//     *
//     * Caller has the resposibilites to send "OK"/"NG" back to server within
//     * synchronization block of Client.getInstance().
//     *
//     * @param className
//     *            Class name of a certain DAC.
//     */
//    synchronized public Object[] downloadDac0(String className)
//            throws ClientCommandException {
//        if (!isConnected())
//            throw new ClientCommandException("Not connected with server now.");
//
//        try {
//            ArrayList dacs = new ArrayList();
//            int numOfObjects = requestObjectsAndGetCount(className);
//            if (numOfObjects == 0) {
//                // Meyer/2003-02-19/ sendMessage outside
//                // sendMessage("OK");
//                return null;
//            }
//            // DacBase dac = (DacBase)Class.forName(className).newInstance();
//            for (int i = 0; i < numOfObjects; i++) {
//                DacBase object;
//                try {
//                    object = dacIn.readObject();
//                } catch (Exception e) {
//                    log(e);
//                    throw new ClientCommandException(className
//                            + " readObject() failed.");
//                }
//                dacs.add(object);
//            }
//            // sendMessage("OK");
//            return dacs.toArray();
//        } catch (IOException e) {
//            log(e);
//            throw new ClientCommandException(e.getMessage());
//        }
//    }

    /**
     * Query a DAC object from ServerThread. Given by a class name and a
     * argument, and it'll query by "getobject" protocol.
     *
     * @param className
     *            DAC class name.
     * @param arg
     *            Query argument
     * @return DacBase Return null if nothing found, otherwise return a DAC
     *         object.
     */
    synchronized public Object querySingleObject(String className, String... args) {
        try {
            String param = className;
            for (String arg : args)
                param += " " + arg;
            int numOfObjects = requestObjectsAndGetCount(param);
            if (numOfObjects <= 0) {
                sendMessage("OK");
                return null;
            }
            if (objectIn == null)
                objectIn = new ObjectInputStream(in);

            Object object;
            try {
                boolean isDac = objectIn.readBoolean();
                if (isDac)
                    object = dacIn.readObject();
                else
                    object = objectIn.readObject(); // non-DAC
                sendMessage("OK");
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return object;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    synchronized public Collection queryMultipleObjects(String className, String... args) {
        try {
            String param = className;
            for (String arg : args)
                param += " " + arg;
            int numOfObjects = requestObjectsAndGetCount(param);
            if (numOfObjects <= 0) {
                sendMessage("OK");
                return null;
            }
            if (objectIn == null) {
                objectIn = new ObjectInputStream(new BufferedInputStream(in));
            }

            Collection returnObjects = new ArrayList();
            for (int i = 0; i < numOfObjects; i++) {
                boolean isDac = objectIn.readBoolean();
                if (isDac) {
                    DacBase dac = dacIn.readObject();
                    returnObjects.add(dac);
                    log("RX: " + dac.toString(), true);
                }
                else
                    returnObjects.add(objectIn.readObject());
            }
            sendMessage("OK");
            return returnObjects;
        } catch (Exception e) {
            e.printStackTrace();
            log(e);
            try {
                sendMessage("NG");
            } catch (IOException e0) {
            }
            return null;
        }
    }

    private List loadAllDacConf(File confFile) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(confFile));
            String line = null;
            allDacs = new ArrayList<Class>();
            while ((line = br.readLine()) != null) {
                Class class1 = null;
                try {
                    class1 = Class.forName(line);
                } catch (ClassNotFoundException e) {
                    log(e);
                    allDacs.clear();
                    break;
                }
                allDacs.add(class1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null)
                try {
                    br.close();
                } catch (IOException e) {
                }
        }
        return allDacs;
    }

    synchronized private void downloadAllDac() throws ClientCommandException {
        if (allDacs == null) {
            File downDacConf = new File(Param.getInstance().getConfigDir(), "downloadlist.conf");
            if (downDacConf.exists()) {
                System.out.println("----------- found downDacConf file");
                System.out.println("------------ dacs list : " + loadAllDacConf(downDacConf));
            } else
                allDacs = new ArrayList();
        }
        if (allDacs.isEmpty()) {
            //Bruce/20080911/ must provide downloadlist.conf now
            //downloadDac(null); // 用旧版本下传
            return;
        }
        downloadDac2(allDacs);
    }

//    /**
//     * Download master tables from SC, then insert into database.
//     *
//     * Policy: If something wrong, just rollback the table being modified, and go home.
//     *
//     * @param className
//     *            null if download all, otherwise the class name.
//     */
//    synchronized private void downloadDac(String className) throws ClientCommandException {
//
//        if (!isConnected())
//            throw new ClientCommandException("Not connected with server now.");
//
//        boolean needSendOkLater = false;
//        Map downHead = new HashMap();
//        DbConnection connection = null;
//        try {
//            for (int k = 0; k < dacForDownload.length; k++) {
//                if (!isValidToDownload(dacForDownload[k]))
//                    continue;
//                if (className != null && !className.equals(dacForDownload[k].getName()))
//                    continue;
//
//                long timeStamp = System.currentTimeMillis();
//                int numOfObjects = requestObjectsAndGetCount(dacForDownload[k].getName());
//                if (numOfObjects == 0) {
//                    prompt("It takes 0 second for download 0 record into "
//                        + dacForDownload[k].getName().substring(
//                            dacForDownload[k].getName().lastIndexOf('.') + 1));
//                    sendMessage("OK");
//                    continue;
//                }
//                needSendOkLater = true;
//                DacBase dac = (DacBase)dacForDownload[k].newInstance();
//                downHead.put(dac.getInsertUpdateTableName(), String.valueOf(numOfObjects));
//
//                // begin a database transaction
//                connection = CreamToolkit.getTransactionalConnection()();
//
//                // delete all records first
//                dac.deleteAll(connection);
//
//                for (int i = 0; i < numOfObjects; i++) {
//                    DacBase object;
//                    try {
//                        object = dacIn.readObject();
//                    } catch (Exception e) {
//                        connection.rollback();
//                        connection.close();
//                        throw new ClientCommandException(dacForDownload[k].getName() + " readObject() failed.");
//                    }
//                    object.insert(connection, false);
//                    if (numOfObjects % 100 == 0)
//                        System.gc();
//                }
//                connection.commit();
//                connection.close();
//                connection = null;
//
//                needSendOkLater = false;
//                sendMessage("OK");
//
//                timeStamp = System.currentTimeMillis() - timeStamp;
//                prompt("It takes " + ((double)timeStamp / 1000.0) + " seconds for download "
//                    + numOfObjects + " records into " + dac.getInsertUpdateTableName());
//            }
//
//            if (GetProperty.getNeedUpdateHead("no").equalsIgnoreCase("yes")) {
//                // 全部下传成功发送'tablename','insertcount'到server
//                prompt("send message " + "updateHead " + String.valueOf(posNumber) + " pos 0");
//                sendMessage("updateHead " + String.valueOf(posNumber) + " pos 0");
//            }
//
//        } catch (SQLException e) {
//            if (needSendOkLater) {
//                try {
//                    sendMessage("OK");
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                }
//            }
//            log(e);
//            throw new ClientCommandException(e.getMessage());
//        } catch (Exception e) {
//            log(e);
//            throw new ClientCommandException(e.getMessage());
//        } finally {
//            CreamToolkit.releaseConnection(connection);
//        }
//    }

    private boolean importIntoDatabase(DbConnection connection) throws SQLException {
        Statement statement = null;
        BufferedReader fin = null;
        String line = "";
        try {
            statement = connection.createStatement();
            fin = new BufferedReader(new InputStreamReader(new FileInputStream(CLIENT_SQL_FILE),
                FILE_ENCODING));
            while ((line = fin.readLine()) != null)
                statement.execute(line);
            fin.close();
            statement.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return false;
        } catch (SQLException e) {
            CreamToolkit.logMessage("ERR SQL : " + line);
            e.printStackTrace(CreamToolkit.getLogger());
            throw e;
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (Exception e) {
            }
        }
    }

    private boolean isValidToDownload(Class dacClass) {
        if ("1".equals(mixAndMatchVersion)
            && (dacClass.equals(CombDiscountType.class)
                || dacClass.equals(CombGiftDetail.class)
                || dacClass.equals(CombPromotionHeader.class)
                || dacClass.equals(CombSaleDetail.class)))
            return false;
        if ("2".equals(mixAndMatchVersion)
            && (dacClass.equals(Rebate.class)
                || dacClass.equals(DiscountType.class)
                || dacClass.equals(MMGroup.class)))
            return false;
        return true;
    }

    /**
     * Download DAC version 2.
     *
     * @return
     */
    synchronized private void downloadDac2(List dacClasses) throws ClientCommandException {
        boolean needSendOkLater = false;

        if (!isConnected())
            throw new ClientCommandException("Not connected with server now.");

        DbConnection connection = null;
        try {
            for (int k = 0; k < dacForDownload.length; k++) {
                if (!isValidToDownload(dacForDownload[k]))
                    continue;
                if (dacClasses != null && !dacClasses.contains(dacForDownload[k]))
                    continue;

                long timeStamp = System.currentTimeMillis();
                int numOfObjects = requestObjectsAndGetCount2(dacForDownload[k].getName());
                if (numOfObjects == 0) {
                    prompt("It takes 0 second for download 0 record into "
                        + dacForDownload[k].getName().substring(
                            dacForDownload[k].getName().lastIndexOf('.') + 1));
                    sendMessage("OK");
                    continue;
                }
                needSendOkLater = true;

                // get the SQL statement file...

                socket.setSoTimeout(LONG1_TIMEOUT_INTERVAL);
                int fileSize = in.readInt();
                log("downloadDac2(): fileSize=" + fileSize, false);
                if (fileSize < 0) // get -1 if server failed
                    throw new ClientCommandException("Get file size failed");

                log("downloadDac2(): read/write file content...", false);
                int byteRead = 0;
                byte[] fileContent = new byte[1024 * 4];
                OutputStream f = new BufferedOutputStream(new FileOutputStream(CLIENT_SQL_FILE));
                while (byteRead < fileSize) {
                    int bytes = in.read(fileContent);
                    f.write(fileContent, 0, bytes);
                    byteRead += bytes;
                }
                f.flush();
                f.close();
                socket.setSoTimeout(DEFAULT_TIMEOUT_INTERVAL);

                DacBase dac = (DacBase)dacForDownload[k].newInstance();

                // begin a database transaction
                connection = CreamToolkit.getTransactionalConnection();

                // delete all records first
                log("downloadDac2(): deleteAll...", false);
                dac.deleteAll(connection);

                // import into database...
                log("downloadDac2(): import into database...(FreeMem="
                    + Runtime.getRuntime().freeMemory() + ", TotalMem="
                    + Runtime.getRuntime().totalMemory() + ")", false);

                boolean succ = importIntoDatabase(connection);
                if (!succ) {
                    connection.rollback();
                    connection.close();
                    needSendOkLater = false;
                    sendMessage("OK");
                    throw new ClientCommandException(dacForDownload[k].getName()
                        + " import into database failed.");
                }
                connection.commit();
                //connection.close();
                //connection = null;

                log("downloadDac2(): send ACK...", false);
                needSendOkLater = false;
                sendMessage("OK");

                timeStamp = System.currentTimeMillis() - timeStamp;
                prompt("It takes " + ((double)timeStamp / 1000.0) + " seconds for download "
                    + numOfObjects + " records into " + dac.getInsertUpdateTableName());
            }

            if (Param.getInstance().getNeedUpdateHead()) {
                // 全部下传成功发送'tablename','insertcount'到server
                prompt("send message " + "updateHead " + String.valueOf(posNumber) + " pos 0");
                sendMessage("updateHead " + String.valueOf(posNumber) + " pos 0");
            }
        } catch (SQLException e) {
            if (needSendOkLater) {
                try {
                    sendMessage("OK");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            log(e);
            throw new ClientCommandException(e.getMessage());
        } catch (Exception e) {
            log(e);
            throw new ClientCommandException(e.getMessage());
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Upload DAC object to SC.
     *
     * @return false if something wrong, true otherwise.
     */
    synchronized public boolean uploadDac(DacBase dac) {
        if (!isConnected())
            return false;

        try {
            // TODO: use reflection instead...
            if (dac instanceof ShiftReport) {
                ShiftReport shift = ((ShiftReport) dac).cloneForSC();
                return sendObject(shift);
            } else if (dac instanceof ZReport) {
                ZReport z = ((ZReport) dac).cloneForSC();
                return sendObject(z);
            } else if (dac instanceof CashForm) {
                CashForm cf = ((CashForm) dac).cloneForSC();
                return sendObject(cf);
            } else if (dac instanceof Transaction) {
                // Transaction tran = ((Transaction)dac).cloneForSC();
                Object[] tran = ((Transaction) dac).cloneForSC();
                // dac contains Transaction and LineItems
                return sendObject(tran);
            }

        } catch (IOException e) {
            e.printStackTrace();
            log(e);
        } catch (Exception e) {
            e.printStackTrace();
            log(e);
        }

        return false;
    }

    /**
     * Upload DAC object to SC.
     *
     * @return false if something wrong, true otherwise.
     */
    synchronized private boolean uploadDac(Object[] dac) {
        if (!isConnected())
            return false;
        try {
            if (dac == null || dac.length == 0)
                return true;
            //System.out.println("------------ start to send object ---------");
            // if (dac[0] instanceof DepSales
            // || dac[0] instanceof DaishouSales
            // || dac[0] instanceof DaiShouSales2) {
            return sendObject(dac);

        } catch (IOException e) {
            log(e);
            return false;
        }
    }

    /**
     * Upload DAC objects to SC. By "putobject2" protocol.
     *
     * @param dacs
     *            the format is:
     *            "[class_name]\t[first_field]\t[second_field]\t...\n" +
     *            "[class_name]\t[first_field]\t[second_field]\t...\n" + ...
     *
     * @return false if something wrong, true otherwise.
     */
    synchronized private boolean uploadDac2(String dacs) {
        if (!isConnected())
            return false;
        try {
            sendMessage("putobject2");
            objectOut.writeObject(dacs);
            objectOut.flush();
            // if (sendObjectCallBack != null)
            // sendObjectCallBack.callBack("数据传送完毕，等候后台回应，请稍侯...");

            int oldSoTimeout = socket.getSoTimeout();
            socket.setSoTimeout(ACK_TIMEOUT_INTERVAL);
            boolean ret = getMessage().equals("OK");
            socket.setSoTimeout(oldSoTimeout);
            return ret;

        } catch (IOException e) {
            prompt("uploadDac2: " + e.getMessage());
            return false;
        }
    }

    /**
     * Get inventory(pandian) Record Count on pos database cream
     *
     * @return (int) Recoud count, if some error occour -1 will be return.
     * @author James 2004-06-23
     */
    private int getPosInventoryRecCount() {
        DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        int result = -1;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            String sqlString = "SELECT COUNT(*) AS count FROM inventory ";
            // + " WHERE posNumber='" + posNumber + "'"; // It is on pos, so
            // needn't point the posNo
            // log("sqlString for getPosInventoryRecCount() : " + sqlString);
            resultSet = statement.executeQuery(sqlString);
            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            CreamToolkit.releaseConnection(connection);
            // log("result of getPosInventoryRecCount() : " + result);
        }
        return result;
    }

    private String getHelp() {
        return "Inline Client Version 1.7 (Copyright 2001-2003, Hongyuan Software Co.)\n"
                + "Available commands:\n"
                + res.getString("Help")
                + res.getString("QuitCommand")
                + res.getString("GetAll")
                + res.getString("GetPlu")
                + res.getString("GetPluPriceChange")
                + res.getString("GetCashier")
                + res.getString("GetReason")
                + res.getString("getPayment")
                + res.getString("getTaxType")
                + res.getString("getCategory")
                + res.getString("getDep")
                + res.getString("getStore")
                + res.getString("getDaiShouDef")
                + (mixAndMatchVersion.equals("2") ? res
                        .getString("getCombDiscountType")
                        + res.getString("getCombPromotionHeader")
                        + res.getString("getCombSaleDetail")
                        + res.getString("getCombGiftDetail") : res
                        .getString("getMixAndMatch")
                        + res.getString("getMMGroup"))
                + res.getString("getSI")
                + res.getString("getDiscountType")
                + res.getString("getRebate")
                + res.getString("getPosButton3")
                + res.getString("queryMember")
                + res.getString("queryLimitQtySold")
                + res.getString("queryMaxTransactionNumber")
                + res.getString("queryMaxZNumber")
                + res.getString("putShift")
                + res.getString("putShiftEx")
                + res.getString("putCashForm")
                + res.getString("putZ")
                + res.getString("putZEx")
                + res.getString("putDepSales")
                + res.getString("putDaishouSales")
                + res.getString("putDaiShouSales2")
                + res.getString("putWorkCheck")
                + res.getString("putAttendance")
                + res.getString("putPosVersion")
//              + res.getString("putHistoryTrans")
                + res.getString("putTransaction")
                + res.getString("countDoingInventory")
                + res.getString("putInventory")
                + res.getString("putAllInventory")
                + res.getString("compareInventoryRecCount")
                + res.getString("turnSingleUlTable")
                + res.getString("getFile")
                + res.getString("moveFile") + res.getString("syncTransaction");
    }

    private String[] getCommandAndArguments(String origLine) {
        List list = new ArrayList();

        StringTokenizer t = new StringTokenizer(origLine, " \t");
        while (t.hasMoreTokens()) {
            list.add(t.nextToken());
        }
        // String[] retStrings = new String[list.size() + 2]; // add another 2
        // null into it for checking with null value
        String[] retStrings = new String[list.size()];
        System.arraycopy(list.toArray(), 0, retStrings, 0, list.size());
        return retStrings;
    }

    /**
     * Process a inline client command.
     *
     * @param line
     *            Command string with arguments.
     * @throws ClientCommandException
     */
    synchronized public void processCommand(String line) throws ClientCommandException {
        if (StringUtils.isEmpty(line))
            return;

        log("TX:" + line, false);
        System.out.println("TX:" + line);
        String[] args = getCommandAndArguments(line);

        if (args == null || args.length == 0 || args[0] == null) {
            return;

        } else if (args[0].equals("?") || args[0].equalsIgnoreCase("help")) {
            throw new ClientCommandException(getHelp());

        } else if (args[0].equalsIgnoreCase("quit")) {
            try {
                sendMessage("quit");
            } catch (IOException e) {
                log(e);
            }
            closeConnection();
            ruokThread.interrupt();
            return;
        } else if (args[0].compareToIgnoreCase("ruok") == 0) {
            sendRuok();

        } else if (args[0].compareToIgnoreCase("putZEx") == 0) {
            if (args.length != 2) {
                throw new ClientCommandException("Please give a z number.");
            }
            putZEx(args);

        } else if (args[0].compareToIgnoreCase("putZ") == 0) {
            if (args.length < 2) {
                String s = "Wrong parameters! Please give me a Z number (and a accounting date), \n"
                        + "Example 1: putZ 168 (server will get the current AccountDate from calendar defaultly) \n"
                        + "Example 2: putZ 168 2004-08-25 (server will use your input date as AccountDate)";
                throw new ClientCommandException(s);
            }
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getPooledConnection();
                Integer zSequenceNumber = Integer.valueOf(args[1]);
                ZReport z = ZReport.queryBySequenceNumber(connection, zSequenceNumber);
                /**
                 * (2004-09-07)
                 * pos日结账时取，server端取日历当前会计日作为z帐的会计日，Client端根据返回的会计日填写pos机上的z,tranhead的会计日
                 * 重传z帐时，若不给定会计日，而且如果pos机z帐含会计日，则以pos机z帐原有会计日为准，
                 * 如果pos机z帐不含会计日，以日历当前会计日为准； 如果重传z帐时，给定了会计日，则以给定的为准
                 */
                boolean accountDateGivenByHand = false;
                if (args.length == 3) {
                    accountDateGivenByHand = true;
                    z.setAccountingDate(java.sql.Date.valueOf(args[2]));
                }
                accdateStr = null;
                if (z != null) {
                    if (uploadDac(z)) {
                        if (accountDateGivenByHand) {
                            accdateStr = args[2];
                        } else if (accdateStr != null) {
                            z.setAccountingDate(java.sql.Date.valueOf(accdateStr));
                        }
                        if (accdateStr != null) {
                            if (!updateAccountDateByZNo(zSequenceNumber, accdateStr))
                                log("Error on updateAccountDateByZNo according sc posul_z",
                                    true);
                        }
                        try {
                            z.setUploadState("1");
                            log("Z uploaded. no=" + z.getSequenceNumber()
                                    + " accountDate=" + accdateStr, true);
                            z.update(connection);
                        } catch (SQLException e) {
                            throw new ClientCommandException("Save z failed.");
                        }
                    } else {
                        try {
                            z.setUploadState("2");
                            log("Z uploaded failed. no=" + z.getSequenceNumber()
                                + " accountDate=" + accdateStr, true);
                            z.update(connection);
                        } catch (SQLException e) {
                            throw new ClientCommandException("Save z failed.");
                        }
                        throw new ClientCommandException("Send z failed.");
                    }
                } else
                    throw new ClientCommandException("Cannot find the given z.");
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Invalid z number.");
            } catch (IllegalArgumentException e) {
                throw new ClientCommandException("Invalid given accountDate format.");
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                throw new ClientCommandException();
            } finally {
                CreamToolkit.releaseConnection(connection);
            }

        } else if (args[0].compareToIgnoreCase("putShift") == 0) {
            if (args.length != 3) {
                throw new ClientCommandException(
                        "Please give a z and a shift number.");
            }
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getPooledConnection();
                ShiftReport shift = ShiftReport.queryByZNumberAndShiftNumber(connection,
                        Integer.valueOf(args[1]), Integer.valueOf(args[2]));
                if (shift != null) {
                    if (uploadDac(shift)) {
                        try {
                            shift.setUploadState("1");
                            log("Shift uploaded. z=" + shift.getZSequenceNumber()
                                    + ", shift=" + shift.getSequenceNumber(), true);
                            shift.update(connection);
                        } catch (EntityNotFoundException e) {
                            log(e);
                        } catch (SQLException e) {
                            log(e);
                        }
                    } else {
                        try {
                            shift.setUploadState("2");
                            log("Shift uploaded failed. z="
                                    + shift.getZSequenceNumber() + ", shift="
                                    + shift.getSequenceNumber(), true);
                            shift.update(connection);
                            throw new ClientCommandException("Send shift failed.");
                        } catch (EntityNotFoundException e) {
                            log(e);
                        } catch (SQLException e) {
                            log(e);
                        }
                    }
                } else
                    throw new ClientCommandException(
                            "Cannot find the given shift.");
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Invalid shift number.");
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                throw new ClientCommandException();
            } finally {
                CreamToolkit.releaseConnection(connection);
            }

        } else if (args[0].compareToIgnoreCase("putShiftEx") == 0) {
            if (args.length != 3) {
                throw new ClientCommandException("Please give a z and a shift number.");
            }
            putShiftEx(args);

        } else if (args[0].compareToIgnoreCase("putCashForm") == 0) {
            if (args.length != 3) {
                throw new ClientCommandException(
                        "Please give a z and a shift number.");
            }
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getPooledConnection();
                CashForm cf = CashForm.queryByZNumberAndShiftNumber(connection,
                    Integer.parseInt(args[1]), Integer.parseInt(args[2]));
                if (cf != null) {
                    connection = CreamToolkit.getPooledConnection();
                    if (uploadDac(cf)) {
                        cf.setUploadState("1");
                        log("CashForm uploaded. z=" + cf.getZSequenceNumber()
                                + ", shift=" + cf.getSequenceNumber(), true);
                        cf.update(connection);
                    } else {
                        cf.setUploadState("2");
                        log("CashForm uploaded failed. z="
                                + cf.getZSequenceNumber() + ", shift="
                                + cf.getSequenceNumber(), true);
                        cf.update(connection);
                        throw new ClientCommandException("Send CashForm failed.");
                    }
                } else
                    throw new ClientCommandException("Cannot find the given CashForm.");
            } catch (EntityNotFoundException e) {
                log(e);
                throw new ClientCommandException("Update CashForm failed.");
            } catch (SQLException e) {
                log(e);
                throw new ClientCommandException("Update CashForm failed.");
            } catch (NumberFormatException e) {
                log(e);
                throw new ClientCommandException("Invalid CashForm number.");
            } finally {
                CreamToolkit.releaseConnection(connection);
            }

        } else if (args[0].compareToIgnoreCase("putTransaction") == 0) {
            if (args.length != 2) {
                throw new ClientCommandException("Please give a transaction number.");
            }
            int transNumber;
            try {
                transNumber = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Please give a transaction number.");
            }

            DbConnection connection = null;
            try {
                connection = CreamToolkit.getPooledConnection();
                Transaction tran = Transaction.queryByTransactionNumber(connection, posNumber, transNumber);

                if (tran != null) {
                    if (!uploadDac(tran))
                        throw new ClientCommandException("Send transaction " + transNumber + " failed.");
                    if ("J".equals(tran.getState1())
                            || "K".equals(tran.getState1())
                            || "L".equals(tran.getState1())
                            || "T".equals(tran.getState1())
                    )
                    {
                        //如果是期刊交易,且上传成功,更改交易传输状态
                        if (!"1".equals(tran.getState2())) {
                            tran.setState2("1");
                            try {
                                tran.update(connection);
                            } catch (EntityNotFoundException e) {
                                CreamToolkit.logMessage(e);
                            }
                        }
                    }
                } else {
                    throw new ClientCommandException(
                            "Cannot find the given transaction " + transNumber + ".");
                }
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } finally {
                CreamToolkit.releaseConnection(connection);
            }

        } else if (args[0].compareToIgnoreCase("putDepSales") == 0) {
            if (args.length != 2)
                throw new ClientCommandException("Please give a z number.");

            DbConnection connection = null;
            try {
//                connection = CreamToolkit.getTransactionalConnection();
                connection = CreamToolkit.getPooledConnection();
                Iterator iter = DepSales.queryBySequenceNumber(connection, Integer.valueOf(args[1]));
                if (iter == null) {
                    CreamToolkit.logMessage("Cannot find the given DepSales.");
                    return;
                }

                Object[] depSales = DepSales.cloneForSC(iter);
                if (depSales != null) {
                    boolean success = uploadDac(depSales);
                    Iterator iter2 = DepSales.queryBySequenceNumber(connection, Integer.valueOf(args[1]));
                    if (iter2 != null) {
                        while (iter2.hasNext()) {
                            DepSales ds = (DepSales) iter2.next();
                            ds.setUploadState((success) ? "1" : "2");
                            ds.update(connection);
                        }
//                        connection.commit();
                    }
                    if (!success)
                        throw new ClientCommandException("Send DepSales failed.");
                } else
                    throw new ClientCommandException("Cannot find the given DepSales.");
            } catch (SQLException e) {
                log(e);
                throw new ClientCommandException("Update DepSales failed.");
            } catch (EntityNotFoundException e) {
                log(e);
                throw new ClientCommandException("Update DepSales failed.");
            } catch (NumberFormatException e) {
                log(e);
                throw new ClientCommandException("Invalid z number.");
            } finally {
                CreamToolkit.releaseConnection(connection);
            }

        } else if (args[0].compareToIgnoreCase("putDaishouSales") == 0) {
            if (args.length != 2)
                throw new ClientCommandException("Please give a z number.");

            DbConnection connection = null;
            try {
                connection = CreamToolkit.getTransactionalConnection();
                Iterator iter = DaishouSales.getCurrentDaishouSales(connection, Integer.parseInt(args[1]));
                if (iter == null) {
                    CreamToolkit.logMessage("Cannot find the given DaishouSales.");
                    return;
                }

                Object[] ds = DaishouSales.cloneForSC(iter);
                if (ds != null) {
                    boolean success = uploadDac(ds);
                    Iterator iter2 = DaishouSales.getCurrentDaishouSales(connection, Integer.parseInt(args[1]));
                    if (iter2 != null) {
                        while (iter2.hasNext()) {
                            DaishouSales d = (DaishouSales) iter2.next();
                            d.setTcpflg((success) ? "1" : "2");
                            d.update(connection);
                        }
                        connection.commit();
                    }
                    if (!success)
                        throw new ClientCommandException("Send DaishouSales failed.");
                } else
                    throw new ClientCommandException("Cannot find the given DaishouSales.");
            } catch (SQLException e) {
                log(e);
                throw new ClientCommandException("Update DaishouSales failed.");
            } catch (EntityNotFoundException e) {
                log(e);
                throw new ClientCommandException("Update DaishouSales failed.");
            } catch (NumberFormatException e) {
                log(e);
                throw new ClientCommandException("Invalid z number.");
            } finally {
                CreamToolkit.releaseConnection(connection);
            }

        } else if (args[0].compareToIgnoreCase("putWorkCheck") == 0) {
            // 考勤资料上传
            String zNumber = null;
            if (args.length == 1)
                ;
            else if (args.length == 2)
                zNumber = args[1];
            else
                throw new ClientCommandException("Please give a z number.");

            try {
                Iterator iter = Attendance.genAttendance(zNumber);
                if (iter == null) {
                    return; // 因为有可能没有资料，无资料视为正常
                    // throw new ClientCommandException("Cannot find the given
                    // Work_check.");
                }

                Object[] wc = Attendance.cloneForSC(iter);
                if (wc != null) {
                    boolean success = uploadDac(wc);

                    if (!success)
                        throw new ClientCommandException(
                                "Send WorkCheck failed.");
                } else
                    throw new ClientCommandException(
                            "Cannot find the given WorkCheck.");
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Invalid z number.");
            }

//        } else if (args[0].compareToIgnoreCase("putHistoryTrans") == 0) {
//            if (args.length < 2) {
//                String s = "Wrong parameters! please give zNo , \n"
//                        + "Example 1: putHistoryTrans 168 \n";
//                throw new ClientCommandException(s);
//            }
//            // 资料上传
//            boolean accountDateGivenByHand = false;
//            String zNumber = null;
//            java.util.Date accdate = null;
//            if (args.length == 1)
//                ;
//            else if (args.length == 2)
//                zNumber = args[1];
//            else
//                throw new ClientCommandException("Please give a z number.");
//
//            DbConnection connection = null;
//            try {
//                connection = CreamToolkit.getTransactionalConnection();
//                HistoryTrans.updateAccdate(connection, Integer.parseInt(zNumber));
//                connection.commit();
//
//                Iterator iter = HistoryTrans.getHistoryTrans(connection, Integer.parseInt(zNumber));
//                if (iter == null) {
//                    System.out.println("historytrans data not found!");
//                    return; // 因为有可能没有资料，无资料视为正常
//                    // throw new ClientCommandExcept ion("Cannot find the given
//                    // Work_check.");
//                }
//
//                Object[] hts = HistoryTrans.cloneForSC(iter);
//
//                if (hts != null) {
//                    int count = hts.length;
//                    int length = 0;
//                    int index = 0;
//                    int preCount = Integer.parseInt(GetProperty.getHistoryTransSendPreCount("200"));
//                    while (count > 0) {
//                        Object[] tmp = null;
//                        if (count >= preCount) {
//                            count -= preCount;
//                            length = preCount;
//                        } else  {
//                            length = count;
//                            count = 0;
//                        }
//System.out.println("--------- send historyTrans | startIndex : " + index + " | length : " + length);
//                        tmp = new Object[length];
//                        for (int i = 0; i < length; i++) {
//                            tmp[i] = hts[index];
//                            index++;
//                        }
//                        if (length != 0) {
//                            boolean success = uploadDac(tmp);
//                            if (!success)
//                                throw new ClientCommandException(
//                                        "Send HistoryTrans failed.");
//                        }
//                    }
//                } else
//                    throw new ClientCommandException(
//                            "Cannot find the given HistoryTrans.");
//            } catch (SQLException e) {
//                log(e);
//                throw new ClientCommandException("HistoryTrans.updateAccdate() failed.");
//            } catch (NumberFormatException e) {
//                throw new ClientCommandException("Invalid z number.");
//            } finally {
//                CreamToolkit.releaseConnection(connection);
//            }
//
        } else if (args[0].compareToIgnoreCase("putAttendance") == 0) {
            // 教超:考勤资料上传 attendance(pos表名) ,posul_attendance(sc表)
            DbConnection connection = null;
            String zNumber = null;
            if (args.length == 1)
                ;
            else if (args.length == 2)
                zNumber = args[1];
            else
                throw new ClientCommandException("Please give a z number.");

            try {
                connection = CreamToolkit.getPooledConnection();
                Iterator iter = Attendance1.getAttendance(zNumber);
                if (iter == null) {
                    return; // 因为有可能没有资料，无资料视为正常
                    // throw new ClientCommandException("Cannot find the given
                    // attendance.");
                }
                Object[] wc = Attendance1.cloneForSC(iter);
//                if (wc != null) {
//                    Attendance1 att = null;
//                    for (int i = 0; i < wc.length; i++) {
//                        att = (Attendance1) wc[i];
//                        if (att != null) {
//                            boolean success = uploadDac(att);
//                            if (success) {
//                                att.setTcpflag(1);
//                                att.update(connection);
//                            } else {
//                                CreamToolkit.logMessage("Send Attendance1 failed : " + att.toString());
//                            }
//                        }
//                    }
//                } else
//                    throw new ClientCommandException("Cannot find the given Attendance1.");
                if (wc != null) {
                    boolean success = uploadDac(wc);
                    if (success) {
                        Iterator iter1 = Attendance1.getAttendance(zNumber);
                        if (iter1 != null) {
                            Attendance1 att = null;
                            while (iter1.hasNext()) {
                                att = (Attendance1) iter1.next();
                                if (att != null) {
                                    att.setTcpflag(1);
                                    att.update(connection);
                                }
                            }
                        }
                    } else
                        throw new ClientCommandException("Send Attendance1 failed.");
                } else
                    throw new ClientCommandException("Cannot find the given Attendance1.");
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Invalid z number.");
            } catch (SQLException e) {
                log(e);
                throw new ClientCommandException("Update Attendance1 failed.");
            } catch (EntityNotFoundException e) {
                log(e);
                throw new ClientCommandException("Update Attendance1 failed.");
            }
        }else if (args[0].compareToIgnoreCase("putPosVersion") == 0) {
            // 版本信息上传

            try {
                Iterator iter = Version.getCurrentVersion();
                if (iter == null) {
                    return; // 因为有可能没有资料，无资料视为正常
                }

                Object[] vs = Version.cloneForSC(iter);
                if (vs != null) {
                    boolean success = uploadDac(vs);

                    if (!success)
                        throw new ClientCommandException("Send Version failed.");
                } else
                    throw new ClientCommandException(
                            "Cannot find the given PosVersion.");
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Cannot put Version.");
            }

        } else if (args[0].compareToIgnoreCase("putDaishouSales2") == 0) {
            // 代收资料上传
            if (args.length != 2)
                throw new ClientCommandException("Please give a z number.");
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getPooledConnection();
                Iterator iter = DaiShouSales2.getCurrentDaishouSales(connection, Integer.parseInt(args[1]));
                if (iter == null) {
                    return; // 因为有可能没有资料，无资料视为正常
                    // throw new ClientCommandException("Cannot find the given
                    // DaishouSales.");
                }

                Object[] ds = DaiShouSales2.cloneForSC(iter);
                if (ds != null) {
                    boolean success = uploadDac(ds);
                    // Iterator iter2 =
                    // DaiShouSales2.getCurrentDaishouSales(Integer.parseInt(args[1]));
                    // if (iter2 != null) {
                    // while (iter2.hasNext()) {
                    // DaiShouSales2 d = (DaiShouSales2) iter2.next();
                    // d.setUploadState((success) ? "1" : "2");
                    // d.update();
                    // }
                    // }
                    if (!success)
                        throw new ClientCommandException(
                                "Send DaiShouSales2 failed.");
                } else
                    throw new ClientCommandException(
                            "Cannot find the given DaiShouSales2.");
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Invalid z number.");
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                throw new ClientCommandException("Invalid z number.");
            } finally {
                CreamToolkit.releaseConnection(connection);
            }

        } else if (args[0].compareToIgnoreCase("putInventory") == 0) {

            String invData = Inventory.getUnsentTabSeperatedData();
            if (invData != null) {
                if (invData.trim().length() != 0) {
                    if (uploadDac2(invData)) {
                        Inventory.updateUploadState("1"); // success
                        // turnSingleUlTable("posul_inventory");
                    } else {
                        Inventory.updateUploadState("2"); // failed
                        throw new ClientCommandException(
                                "Send inventory data failed.");
                    }
                }
            } else {
                throw new ClientCommandException("Cannot find inventory data.");
            }

        } else if (args[0].compareToIgnoreCase("putAllInventory") == 0) {
            String invData = Inventory.getTabSeperatedData();
            if (invData != null && invData.trim().length() != 0) {
                if (uploadDac2(invData)) {
                    Inventory.updateUploadState("1"); // success
                } else {
                    Inventory.updateUploadState("2"); // failed
                    throw new ClientCommandException(
                            "Send inventory data failed.");
                }
            } else {
                throw new ClientCommandException("Cannot find inventory data.");
            }

        } else if (args[0].compareToIgnoreCase("turnSingleUlTable") == 0) { // (注：目前只支持转posul_inventory)
            if (args.length != 2) {
                throw new ClientCommandException("Please imput the table name");
            }
            turnSingleUlTable(args[1]);

        } else if (args[0].compareToIgnoreCase("getAll") == 0) {
            downloadAllDac();

        } else if (args[0].compareToIgnoreCase("getPLU") == 0) {
            downloadDac2(Collections.singletonList(PLU.class));

            //Bruce/20080613
            CreamCache.getInstance().rebuildPLUObjectDB();

        } else if (args[0].equalsIgnoreCase("getTransaction")) {
            if (args.length < 3)
                throw new ClientCommandException("Please give me a POS number then a transaction number.");

            Collection objs = queryMultipleObjects("hyi.cream.dac.Transaction", args[1], args[2]);
            if (objs == null || objs.isEmpty()) {
                prompt("Cannot find this transaction.");
                setReturnObject(null);
            } else {
                Transaction trans = new Transaction();
                trans.init();
                for (Object obj : objs) {
                    if (obj instanceof Transaction) {
                        trans.getFieldMap().putAll(((Transaction)obj).getFieldMap());
                        for (int i = 1; i <= 4; i++) {
                            String paymentId = trans.getPayNumberByID(i);
                            if (!StringUtils.isEmpty(paymentId))
                                trans.addPayment(Payment.queryByPaymentID(paymentId));
                        }
                    } else if (obj instanceof LineItem)
                        trans.addLineItemSimpleVersion((LineItem)obj);
                    else if (obj instanceof CardDetail)
                        trans.setCardDetail((CardDetail)obj);
                }

                if (commandLineMode) {
                    prompt("Transaction number: " + trans.getTransactionNumber());
                    prompt("Sales amount: " + trans.getNetSalesAmount());
                } else {
                    setReturnObject(trans);
                }
            }

        } else if (args[0].compareToIgnoreCase("getPLUPriceChange") == 0) {
            Object[] plus = queryMultipleObjects("hyi.cream.dac.PLUPriceChange").toArray();

            DbConnection connection = null;
            try {
                if (plus != null)
                    PLU.importSomePLUs(connection, plus);

//                // Meyer/2003-02-19/ send message
//                try {
//                    sendMessage("OK");
//                } catch (IOException e) {
//                    log(e);
//                }
            } catch (SQLException e) {
                log(e);
//                // Meyer/2003-02-19/ send message
//                try {
//                    sendMessage("NG");
//                } catch (IOException e2) {
//                    log(e2);
//                }
                throw new ClientCommandException("Some price change update failed.");
            }

        } else if (args[0].compareToIgnoreCase("getCashier") == 0) {
            downloadDac2(Collections.singletonList(Cashier.class));

        } else if (args[0].compareToIgnoreCase("getDaiShouDef") == 0) {
            downloadDac2(Collections.singletonList(DaiShouDef.class));

        } else if (args[0].compareToIgnoreCase("getReason") == 0) {
            downloadDac2(Collections.singletonList(Reason.class));

        } else if (args[0].compareToIgnoreCase("getMixAndMatch") == 0) {
            downloadDac2(Collections.singletonList(MixAndMatch.class));

        } else if (args[0].compareToIgnoreCase("getSI") == 0) {
            downloadDac2(Collections.singletonList(SI.class));

        } else if (args[0].compareToIgnoreCase("getPayment") == 0) {

            DbConnection connection = null;
            try {
                downloadDac2(Collections.singletonList(Payment.class));
                // Meyer/2003-02-17
                connection = CreamToolkit.getTransactionalConnection();
                Payment.insertCashIfNotExist(connection);
                connection.commit();
            } catch (SQLException e) {
                log(e);
            } finally {
                CreamToolkit.releaseConnection(connection);
            }

        } else if (args[0].compareToIgnoreCase("getTaxType") == 0) {
            downloadDac2(Collections.singletonList(TaxType.class));

        } else if (args[0].compareToIgnoreCase("getCategory") == 0) {
            downloadDac2(Collections.singletonList(Category.class));

        } else if (args[0].compareToIgnoreCase("getDep") == 0) {
            downloadDac2(Collections.singletonList(Dep.class));

        } else if (args[0].compareToIgnoreCase("getStore") == 0) {
            downloadDac2(Collections.singletonList(Store.class));

        } else if (args[0].compareToIgnoreCase("getPosButton3") == 0) {
            downloadDac2(Collections.singletonList(PosButton3.class));

        } else if (args[0].compareToIgnoreCase("getMember") == 0) {
            downloadDac2(Collections.singletonList(Member.class));

        } else if (args[0].compareToIgnoreCase("getMMGroup") == 0) {
            downloadDac2(Collections.singletonList(MMGroup.class));

        } else if (args[0].compareToIgnoreCase("getDiscountType") == 0) {
            downloadDac2(Collections.singletonList(DiscountType.class));

        } else if (args[0].compareToIgnoreCase("getRebate") == 0) {
            downloadDac2(Collections.singletonList(Rebate.class));

        } else if (args[0].compareToIgnoreCase("queryMember") == 0) {
            setReturnObject(null);
            if (args.length != 2)
                throw new ClientCommandException(
                        "Please give a member id, telphone number or an identity card ID.");

            Member m = (Member) querySingleObject("hyi.cream.dac.Member",
                    args[1]);
            if (m == null) {
                prompt("Cannot find this member.");
            } else {
                if (commandLineMode) {
                    prompt("Member ID: " + m.getID());
                    prompt("Member Card ID: " + m.getMemberCardID());
                    prompt("Member Name: " + m.getName());
                    prompt("Telphone Number: " + m.getTelephoneNumber());
                    prompt("Identity ID: " + m.getIdentityID());
                    prompt("Action Rebate: " + m.getMemberActionBonus());
                } else {
                    setReturnObject(m);
                }
            }
        } else if (args[0].compareToIgnoreCase("queryMember2") == 0) {
            setReturnObject(null);
            if (args.length != 2)
                throw new ClientCommandException(
                        "Please give a member id, telphone number or an identity card ID.");

            MemberCardBase m = (MemberCardBase) querySingleObject("hyi.cream.dac.MemberCardBase",
                    args[1]);
            if (m == null) {
                prompt("Cannot find this member.");
            } else {
                if (commandLineMode) {
//                    prompt("Member ID: " + m.getMemNo());
                    prompt("Member Card ID: " + m.getFcardnumber());
                    prompt("Member Name: " + m.getFmemname());
                    prompt("Telphone Number: " + m.getFtelno());
                } else {
                    setReturnObject(m);
                }
            }

        // zhaohong/2003-06-10 查询限量销售数据
        } else if (args[0].compareToIgnoreCase("queryLimitQtySold") == 0) {
            setReturnObject(null);
            String sArgs = "";
            if (args.length < 3 || args.length > 4)
                throw new ClientCommandException(
                        "Usage: queryLimitQtySold {Item Number} {1 or -1} [Member ID]");
            for (int i = 1; i < args.length; i++)
                sArgs += (args[i] + " ");
            // System.out.println(sArgs);
            LimitQtySold lqs = (LimitQtySold) querySingleObject(
                    "hyi.cream.dac.LimitQtySold", sArgs);

            if (commandLineMode) {
                if (lqs != null) {
                    prompt("LimitPrice: " + lqs.getLimitPrice());
                    prompt("LimitRebateRate: " + lqs.getLimitRebateRate());
                    prompt("QtySold: " + lqs.getQtySold());
                    prompt("Member Quantity Sold: " + lqs.getMemberQtySold());
                    prompt("IsSalable: " + lqs.isSaleable());
                }
            } else {
                setReturnObject(lqs);
            }

        } else if (args[0].compareToIgnoreCase("queryMaxTransactionNumber") == 0) {
            setReturnObject(null);
            SequenceNumberGetter m = (SequenceNumberGetter) querySingleObject(
                    "hyi.cream.dac.SequenceNumberGetter", posNumber + ",tran");
            if (m == null) {
                prompt("Cannot find.");
            } else {
                if (commandLineMode) {
                    prompt("Server max transaction number: " + m.getMaxTransactionNumber());
                } else {
                    setReturnObject(m);
                }
            }

        } else if (args[0].compareToIgnoreCase("queryMaxZNumber") == 0) {
            setReturnObject(null);
            SequenceNumberGetter m = (SequenceNumberGetter) querySingleObject(
                    "hyi.cream.dac.SequenceNumberGetter", posNumber + ",z");
            if (m == null) {
                prompt("Cannot find.");
            } else {
                if (commandLineMode) {
                    prompt("Server max Z number: " + m.getMaxZNumber());
                } else {
                    setReturnObject(m);
                }
            }

        // James/2004-06-18
        // 新增查询后台inventoryhead表中标记为“盘点初始化“或“盘点数据导入“的记录数
        } else if (args[0].compareToIgnoreCase("countDoingInventory") == 0) {
            prompt("Count of Doing Inventory (pandin): "
                    + countDoingInventory());

        // James/2004-06-23
        // 检查pos机inventory记录数和后台的posul_inventory对应pos机号对应记录数是否一致
        } else if (args[0].compareToIgnoreCase("compareInventoryRecCount") == 0) {
            compareInventoryRecCount();

        // Meyer/2003-02-10
        // 新增新版组合促销用表
        } else if (args[0].compareToIgnoreCase("getCombDiscountType") == 0) {
            downloadDac2(Collections.singletonList(CombDiscountType.class));

        } else if (args[0].compareToIgnoreCase("getCombGiftDetail") == 0) {
            downloadDac2(Collections.singletonList(CombGiftDetail.class));

        } else if (args[0].compareToIgnoreCase("getCombPromotionHeader") == 0) {
            downloadDac2(Collections.singletonList(CombPromotionHeader.class));

        } else if (args[0].compareToIgnoreCase("getCombSaleDetail") == 0) {
            downloadDac2(Collections.singletonList(CombSaleDetail.class));

        } else if (args[0].compareToIgnoreCase("getFile") == 0
                || args[0].compareToIgnoreCase("moveFile") == 0) {
            if (args.length < 3 || args[1] == null || args[2] == null)
                throw new ClientCommandException(
                        "Please give a server and client directory name.");
            else
                requestFiles(args[1], args[2], args[0]
                        .compareToIgnoreCase("moveFile") == 0);

        } else if (args[0].compareToIgnoreCase("syncTransaction") == 0) {
            try {
                if (args.length == 1) {
                    sendSyncTransaction();
                } else if (args.length == 2) {
                    int[] beginAndEndTranNumber = new int[2];
                    beginAndEndTranNumber[0] = Integer.parseInt(args[1]);
                    beginAndEndTranNumber[1] = beginAndEndTranNumber[0];
                    sendSyncTransaction(beginAndEndTranNumber);
                } else {
                    int[] beginAndEndTranNumber = new int[2];
                    beginAndEndTranNumber[0] = Integer.parseInt(args[1]);
                    beginAndEndTranNumber[1] = Integer.parseInt(args[2]);
                    sendSyncTransaction(beginAndEndTranNumber);
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new ClientCommandException("Invalid command.");
            }
        } else if (args[0].compareToIgnoreCase("getBookableSubIssns") == 0) {
            setReturnObject2(null);
            if (args.length != 4)
                throw new ClientCommandException(
                        "Usage: getBookableSubIssns {Item Number} {start month} {end month}");

            try {
                List subIssns = getBookableSubIssns(args[1], args[2], args[3]);
                setReturnObject2(subIssns);
            } catch (IOException e) {
                e.printStackTrace();
                throw new ClientCommandException("Invalid command.");
            }
        } else if (args[0].compareToIgnoreCase("getBookedInfos") == 0) {
            setReturnObject2(null);
            if (args.length != 2)
                throw new ClientCommandException(
                        "Usage: getBookedInfos {cardno}");

            try {
                List bookedInfos = getBookedInfos(args[1]);
                setReturnObject2(bookedInfos);
            } catch (IOException e) {
                e.printStackTrace();
                throw new ClientCommandException("Invalid command.");
            }
        } else if (args[0].compareToIgnoreCase("getWholesales") == 0) {
            setReturnObject2(null);
            if (args.length != 3)
                throw new ClientCommandException(
                    "Usage: getWholesales {storeID} {billNo}");

            try {
                List wholesales = getWholesales(args[1], args[2]);
                setReturnObject2(wholesales);
            } catch (IOException e) {
                e.printStackTrace();
                throw new ClientCommandException("Invalid command.");
            }

        } else if (args[0].equalsIgnoreCase("getDeliveryStatus")) {
            setReturnObject2(null);
            if (args.length != 4)
                throw new ClientCommandException(
                    "Usage: getDeliveryStatus {storeID} {deliveryNo} {version}");

            Integer status = (Integer)querySingleObject("hyi.cream.groovydac.DeliveryHead",
                "getDeliveryStatus", args[1], args[2], args[3]);
            if (status == null)
                prompt("Cannot find this delivery.");
            else
                setReturnObject2(status);

        } else if (args[0].equalsIgnoreCase("getOrigTranNoFromDeliveryIDForRefund")) {
            setReturnObject2(null);
            if (args.length != 3)
                throw new ClientCommandException(
                    "Usage: getOrigTranNoFromDeliveryIDForRefund {storeID} {deliveryNo}");

            Integer status = (Integer)querySingleObject("hyi.cream.groovydac.DeliveryHead",
                "getOrigTranNoFromDeliveryIDForRefund", args[1], args[2], "X");
            if (status == null)
                prompt("Cannot find this delivery.");
            else
                setReturnObject2(status);

        } else if (args[0].equalsIgnoreCase("getOrigTranNoFromDeliveryID")) {
            setReturnObject2(null);
            if (args.length != 3)
                throw new ClientCommandException(
                    "Usage: getOrigTranNoFromDeliveryID {storeID} {deliveryNo}");

            Integer status = (Integer)querySingleObject("hyi.cream.groovydac.DeliveryHead",
                "getOrigTranNoFromDeliveryID", args[1], args[2], "X");
            if (status == null)
                prompt("Cannot find this delivery.");
            else
                setReturnObject2(status);

        } else if (args[0].equalsIgnoreCase("getPosDeliveryDtl")) {
            setReturnObject2(null);
            if (args.length != 4)
                throw new ClientCommandException(
                    "Usage: getPosDeliveryDtl {storeID} {deliveryNo} {version}");

            Collection maps = queryMultipleObjects("hyi.cream.groovydac.DeliveryHead",
                "getPosDeliveryDtl", args[1], args[2], args[3]);
            if (maps == null)
                prompt("Cannot find this delivery.");
            else
                setReturnObject2(maps);

        } else if (args[0].equalsIgnoreCase("updateDeliveryStatus")) {
            setReturnObject2(null);
            if (args.length != 6)
                throw new ClientCommandException(
                    "Usage: updateDeliveryStatus {storeID} {deliveryNo} {posNumber} {transactionNumber} {updateUserId}");

            Integer status = (Integer)querySingleObject("hyi.cream.groovydac.DeliveryHead",
                "updateDeliveryStatus", args[1], args[2], args[3], args[4], args[5]);
            if (status == null)
                prompt("Cannot update this delivery.");
            else
                setReturnObject2(status);

        } else {
            throw new ClientCommandException("Invalid command.");
        }
    }

    private void putShiftEx(String[] args) throws ClientCommandException {
        DbConnection connection = null;
        try {
//            connection = CreamToolkit.getTransactionalConnection();
            connection = CreamToolkit.getPooledConnection();
            Iterator<ShiftEx> iter = ShiftEx.queryBySequenceNumber(connection, Integer.valueOf(args[1]),
                Integer.valueOf(args[2]));
            if (iter == null) {
                CreamToolkit.logMessage("Cannot find the given ShiftEx.");
                return;
            }

            Object[] shiftExes = ShiftEx.cloneForSC(iter);
            if (shiftExes != null) {
                boolean success = uploadDac(shiftExes);
                Iterator iter2 = ShiftEx.queryBySequenceNumber(connection, Integer.valueOf(args[1]),
                    Integer.valueOf(args[2]));
                if (iter2 != null) {
                    while (iter2.hasNext()) {
                        ShiftEx shx = (ShiftEx) iter2.next();
                        shx.setUploadState((success) ? "1" : "2");
                        shx.update(connection);
                    }
//                    connection.commit();
                }
                if (!success)
                    throw new ClientCommandException("Send ShiftEx failed.");
            } else
                throw new ClientCommandException("Cannot find the given ShiftEx.");
        } catch (SQLException e) {
            log(e);
            throw new ClientCommandException("Update ShiftEx failed.");
        } catch (EntityNotFoundException e) {
            log(e);
            throw new ClientCommandException("Update ShiftEx failed.");
        } catch (NumberFormatException e) {
            log(e);
            throw new ClientCommandException("Invalid z number.");
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    private void putZEx(String[] args) throws ClientCommandException {
        DbConnection connection = null;
        try {
//            connection = CreamToolkit.getTransactionalConnection();
            connection = CreamToolkit.getPooledConnection();
            Iterator<ZEx> iter = ZEx.queryBySequenceNumber(connection, Integer.valueOf(args[1]));
            if (iter == null) {
                CreamToolkit.logMessage("Cannot find the given ZEx.");
                return;
            }

            Object[] zExes = ZEx.cloneForSC(iter);
            if (zExes != null) {
                boolean success = uploadDac(zExes);
                Iterator iter2 = ZEx.queryBySequenceNumber(connection, Integer.valueOf(args[1]));
                if (iter2 != null) {
                    while (iter2.hasNext()) {
                        ZEx shx = (ZEx) iter2.next();
                        shx.setUploadState((success) ? "1" : "2");
                        shx.update(connection);
                    }
//                    connection.commit();
                }
                if (!success)
                    throw new ClientCommandException("Send ZEx failed.");
            } else
                throw new ClientCommandException("Cannot find the given ZEx.");
        } catch (SQLException e) {
            log(e);
            throw new ClientCommandException("Update ZEx failed.");
        } catch (EntityNotFoundException e) {
            log(e);
            throw new ClientCommandException("Update ZEx failed.");
        } catch (NumberFormatException e) {
            log(e);
            throw new ClientCommandException("Invalid z number.");
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Inline client command line mode.
     */
    public void enterCommandLineMode() {
        commandLineMode = true;
        prompt("Inline Client Version 1.3 (Copyright 2001-2003, Hongyuan Software Co.)");
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                System.in));
        try {
            while (true) {
                System.out.print("> ");
                try {
                    String line = reader.readLine();
                    processCommand(line);
                    if (line.equalsIgnoreCase("quit")) {
                        closeConnection();
                        ruokThread.interrupt();
                        return;
                    } else if (!line.equals("")) {
                        System.out.println("OK.");
                    }
                } catch (ClientCommandException e2) {
                    prompt(e2.getMessage());
                }
            }
        } catch (IOException e) {
            log(e);
        }
    }

    /**
     * 如果发现conf/downloadlist.conf 则用此配置里的表
     * 没有发现文件则用旧版的downloadMaster()
     * @since 2005-11-29
     * @throws ClientCommandException
     */
    public void downloadMaster2() throws ClientCommandException
    {
        if (allDacs == null) {
            File downDacConf = new File(Param.getInstance().getConfigDir(), "downloadlist.conf");
            if (downDacConf.exists())
                loadAllDacConf(downDacConf);
            else
                allDacs = new ArrayList();
        }
        if (allDacs.isEmpty()) { // 用旧版本下传
            downloadMaster();
            return;
        }

        hyi.cream.uibeans.Indicator indicator = POSTerminalApplication.getInstance()
            .getMessageIndicator();
        //ConnectionlessClient client = ConnectionlessClient.getInstance();

        for (int i = 0; i < allDacs.size(); i++) {
            Class curClass = (Class)allDacs.get(i);
            String name = curClass.getName().substring(curClass.getName().lastIndexOf(".") + 1,
                curClass.getName().length());
            indicator.setMessage(res.getString("MasterDownloading") + "(" + name + ")");
            processCommand("get" + name);
        }
    }

    /*
     * Download Master table
     */
    public void downloadMaster() throws ClientCommandException {
        hyi.cream.uibeans.Indicator indicator = POSTerminalApplication
                .getInstance().getMessageIndicator();
        //ConnectionlessClient client = ConnectionlessClient.getInstance();
        // Client.getInstance().processCommand("getAll");
        indicator.setMessage(res.getString("MasterDownloading") + "(Cashier)");
        processCommand("getCashier");
        indicator.setMessage(res.getString("MasterDownloading") + "(Reason)");
        processCommand("getReason");

        indicator.setMessage(res.getString("MasterDownloading") + "(DeltaPlu)");
        processCommand("getPLUPriceChange");
        if ("2".equals(mixAndMatchVersion)) {
            indicator.setMessage(res.getString("MasterDownloading")
                    + "(Combination_Discount_Type)");
            processCommand("getCombDiscountType");
            indicator.setMessage(res.getString("MasterDownloading")
                    + "(Combination_Gift_Detail)");
            processCommand("getCombGiftDetail");
            indicator.setMessage(res.getString("MasterDownloading")
                    + "(Combination_Promotion_Header)");
            processCommand("getCombPromotionHeader");
            indicator.setMessage(res.getString("MasterDownloading")
                    + "(Combination_Sale_Detail)");
            processCommand("getCombSaleDetail");
        } else {
            indicator.setMessage(res.getString("MasterDownloading")
                    + "(MixAndMatch)");
            processCommand("getMixAndMatch");
            indicator.setMessage(res.getString("MasterDownloading")
                    + "(MMGroup)");
            processCommand("getMMGroup");
        }

        indicator.setMessage(res.getString("MasterDownloading") + "(Payment)");
        processCommand("getPayment");
        indicator.setMessage(res.getString("MasterDownloading") + "(TaxType)");
        processCommand("getTaxType");
        indicator.setMessage(res.getString("MasterDownloading") + "(Category)");
        processCommand("getCategory");
        indicator.setMessage(res.getString("MasterDownloading") + "(Dep)");
        processCommand("getDep");
        indicator.setMessage(res.getString("MasterDownloading") + "(Store)");
        processCommand("getStore");
        indicator.setMessage(res.getString("MasterDownloading") + "(PLU)");
        processCommand("getPLU");
        indicator.setMessage(res.getString("MasterDownloading")
                + "(DaiShouDef)");
        processCommand("getDaiShouDef");
        indicator.setMessage(res.getString("MasterDownloading") + "(SI)");
        processCommand("getSI");
        indicator.setMessage(res.getString("MasterDownloading") + "(Rebate)");
        processCommand("getRebate");
        indicator.setMessage(res.getString("MasterDownloading")
                + "(DiscountType)");
        processCommand("getDiscountType");
        indicator.setMessage(res.getString("MasterDownloading")
                + "(PosButton3)");
        processCommand("getPosButton3");

        indicator.setMessage(res.getString("MasterDownloading")
                + "(Member)");
        processCommand("getMember");

        // re-cache
        Store.createCache();
        Cashier.createCache();
        Dep.createCache();
        Payment.createCache();
        Reason.createCache();
        TaxType.createCache();
    }

//    /**
//     * @param zSequence
//     * @return accountDate of posul_z in server
//     */
//    synchronized private String getAccountDateOfZ(Integer zSequence) {
//        String accountDateStr = null;
//        if (!isConnected()) {
//            return accountDateStr;
//        }
//        try {
//            sendMessage("getAccountDateOfZ");
//            out.writeInt(posNumber);
//            out.writeInt(zSequence.intValue());
//            accountDateStr = getMessage();
//        } catch (java.io.InterruptedIOException e1) {
//            e1.printStackTrace(getLogger());
//        } catch (Exception e2) {
//            e2.printStackTrace(getLogger());
//        }
//        return accountDateStr;
//    }

    /**
     * update tables accountDate of the z.eodcnt:
     *  tranhead, shift, depsales, daishousales, daishousales2
     * @param eodcnt:
     *            zSequence in db cream table tranhead
     * @param accdateStr:
     *            accountDate will be add to db cream table tranhead
     * @return true | false : if update table success or fail
     */
    private boolean updateAccountDateByZNo(Integer eodcnt, String accdateStr) {
        DbConnection connection = null;
        Statement statement = null;
        boolean result = false;
        int count = 0;
        String sqlString = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();

            sqlString = "UPDATE tranhead SET ACCDATE='" + accdateStr
                + "' WHERE EODCNT='" + eodcnt + "'";
            log("[sqlString]" + sqlString, true);
            count = statement.executeUpdate(sqlString);
            log(count + " records updated", true);

            sqlString = "UPDATE shift SET accdate='" + accdateStr
                + "' WHERE eodcnt='" + eodcnt + "'";
                log("[sqlString] " + sqlString, true);
            count = statement.executeUpdate(sqlString);
            log(count + " records updated", true);

            sqlString = "UPDATE depsales SET accountdate='" + accdateStr
                + "' WHERE sequencenumber='" + eodcnt + "'";
                log("[sqlString] " + sqlString, true);
            count = statement.executeUpdate(sqlString);
            log(count + " records updated", true);

            sqlString = "UPDATE daishousales SET accountdate='" + accdateStr
                + "' WHERE zsequencenumber='" + eodcnt + "'";
                log("[sqlString] " + sqlString, true);
            count = statement.executeUpdate(sqlString);
            log(count + " records updated", true);

            sqlString = "UPDATE daishousales2 SET accountdate='" + accdateStr
                + "' WHERE zsequencenumber='" + eodcnt + "'";
                log("[sqlString] " + sqlString, true);
            count = statement.executeUpdate(sqlString);
            log(count + " records updated", true);

            result = true;
        } catch (SQLException e) {
            e.printStackTrace();
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                e.printStackTrace(CreamToolkit.getLogger());
            }
            CreamToolkit.releaseConnection(connection);
            log("result of updateAccountDateByZNo : " + result, true);
        }
        return result;
    }

    /**
     * Command inline mode Inline Client.
     */
/*
    public static void main(String[] args) throws InterruptedException {
        if (args.length < 2) {
            System.out.println("Please give me a server address and a port number.");
            System.exit(1);
        }

        if (args[0].equals("auto")) {
            if (args.length >= 4) {
                try {
                    ConnectionlessClient.commandLineMode = true;
                    ConnectionlessClient c = new ConnectionlessClient(args[1], Integer.parseInt(args[2]));
                    String line = "";
                    for (int i = 3; i < args.length; i++)
                        line += " " + args[i];
                    c.processCommand(line);
                } catch (NumberFormatException e) {
                    System.out.println("Please give me a legal port number.");
                    System.exit(1);
                } catch (InstantiationException e) {
                    System.out.println("Cannot create inline client.");
                    System.exit(1);
                } catch (ClientCommandException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("must : auto ip port command");
            }
            System.exit(0);
        }

        try {
            ConnectionlessClient.commandLineMode = true;
            ConnectionlessClient c = new ConnectionlessClient(args[0], Integer.parseInt(args[1]), true);
            c.enterCommandLineMode();
            System.exit(0);
        } catch (NumberFormatException e) {
            System.out.println("Please give me a legal port number.");
            System.exit(1);
        } catch (InstantiationException e) {
            System.out.println("Cannot create inline client.");
            System.exit(1);
        }
    }
*/
    /**
     * Get a cached DAC object for caller.
     *
     * @return The cached DAC object.
     */
    public DacBase getReturnObject() {
        return returnObject;
    }

    /**
     * Set a cached DAC object for caller.
     *
     * @param returnObject
     *            The cached DAC object.
     */
    public void setReturnObject(DacBase returnObject) {
        this.returnObject = returnObject;
    }

    /**
     * Get a cached object for caller.
     *
     * @return The cached object.
     */
    public Object getReturnObject2() {
        return returnObject2;
    }

    /**
     * Set a cached object for caller.
     *
     * @param returnObject
     *            The cached object.
     */
    public void setReturnObject2(Object returnObject) {
        this.returnObject2 = returnObject;
    }
    /**
     * Return the number of files got or moved last time.
     *
     * @return Return the number of files got or moved last time.
     */
    public int getNumberOfFilesGotOrMoved() {
        return numberOfFilesGotOrMoved;
    }

    synchronized public boolean needDownMasterVersion() {
        boolean need = false;
        if (!isConnected())
            return need;
        try {
            sendMessage("needDown " + posNumber);
            need = getMessage().equalsIgnoreCase("yes");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return need;
    }

    public void test1() {
        for (int k = 0; k < dacForDownload.length; k++) {
            try {
                DacBase dac = (DacBase) dacForDownload[k].newInstance();
                String tableName = dac.getInsertUpdateTableName();
                sendMessage("updateHead 1 " + tableName + " 100");
                CreamToolkit.sleepInSecond(1);
            } catch (Exception e) {
            }
        }
    }

    synchronized public List getBookableSubIssns(String itemNo, String start, String end)
            throws IOException {
        if (!isConnected())
            return null;

        try {
            socket.setSoTimeout(DEFAULT_TIMEOUT_INTERVAL);
            sendMessage("getBookableSubIssns " + itemNo + " " + start + " " + end);
            if (objectIn == null) {
                objectIn = new ObjectInputStream(in);
            }
            List bookableSubIssns = (List) objectIn.readObject();
            return bookableSubIssns;
        } catch (Exception e) {
            if (isConnected()) {
                prompt("POS cannot connect to inline server.(" + e + ")");
                closeConnection();
                log(e);
            }
            setConnected(false);
            if (e instanceof IOException)
                throw (IOException) e;
            else
                throw new IOException(e.toString());
        }
    }

    synchronized public List getBookedInfos(String memberID)
            throws IOException {
        if (!isConnected())
            return null;

        try {
            socket.setSoTimeout(DEFAULT_TIMEOUT_INTERVAL);
            sendMessage("getBookedInfos " + memberID);
            if (objectIn == null) {
                objectIn = new ObjectInputStream(in);
            }
            List bookedInfos = (List) objectIn.readObject();
            return bookedInfos;
        } catch (Exception e) {
            if (isConnected()) {
                prompt("POS cannot connect to inline server.(" + e + ")");
                closeConnection();
                log(e);
            }
            setConnected(false);
            if (e instanceof IOException)
                throw (IOException) e;
            else
                throw new IOException(e.toString());
        }
    }

    synchronized public List getWholesales(String storeID, String billNo)
            throws IOException {
        if (!isConnected())
            return null;

        try {
            socket.setSoTimeout(DEFAULT_TIMEOUT_INTERVAL);
            sendMessage("getWholesales " + storeID + " " + billNo);
            if (objectIn == null) {
                objectIn = new ObjectInputStream(in);
            }
            List bookableSubIssns = (List) objectIn.readObject();
            return bookableSubIssns;
        } catch (Exception e) {
            if (isConnected()) {
                prompt("POS cannot connect to inline server.(" + e + ")");
                closeConnection();
                log(e);
            }
            setConnected(false);
            if (e instanceof IOException)
                throw (IOException) e;
            else
                throw new IOException(e.toString());
        }
    }

}