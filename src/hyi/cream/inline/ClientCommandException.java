package hyi.cream.inline;

public class ClientCommandException extends Exception {

    public ClientCommandException() {
        super();
    }

    public ClientCommandException(String s) {
        super(s);
    }
}

