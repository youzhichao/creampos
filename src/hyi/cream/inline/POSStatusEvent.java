package hyi.cream.inline;

import java.util.EventObject;

public class POSStatusEvent extends EventObject {
    private int posNumber;
    private int status;
    private boolean sending;
    private boolean receiving;
    private String message;

    // Constants
    public static final int STATUS_CONNECTED = 0;
    public static final int STATUS_DISCONNECTED = 1;
    public static final int STATUS_LOGGING = 2;

    /**
     * Constructor.
     *
     * @param posNumber
     * @param status
     * @param sending
     * @param receiving
     * @param message
     */
    public POSStatusEvent(Object object, int posNumber, int status, boolean sending, boolean receiving, String message) {
        super(object);
        this.posNumber = posNumber;
        this.status = status;
        this.sending = sending;
        this.receiving = receiving;
        this.message = message;
    }

    public int getPosNumber() {
        return posNumber;
    }

    public void setPosNumber(int posNumber) {
        this.posNumber = posNumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean getSending() {
        return sending;
    }

    public void setSending(boolean sending) {
        this.sending = sending;
    }

    public boolean getReceiving() {
        return receiving;
    }

    public void setReceiving(boolean receiving) {
        this.receiving = receiving;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
