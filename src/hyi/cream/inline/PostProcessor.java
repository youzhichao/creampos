package hyi.cream.inline;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.Date;

import hyi.cream.dac.*;
import hyi.cream.groovydac.SsmpLog;

/**
 * Data post-processor for inline server.
 *
 * @author Bruce
 * @version 1.7
 */
interface PostProcessor {
    /**
     * /Bruce/1.7/2002-04-27/
     *    增加上传代收明细帐.
     */

    /**
     * Shift report processor.
     *
     * @param shift Shift report object.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingShiftReport(ShiftReport shift);

    /**
     * ShiftEx report processor.
     *
     * @param shiftEx ShiftEx report object.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingShiftEx(Object[] shiftExes);
    
    /**
     * Z report processor.
     *
     * @param z Z report object
     * @return true if success, otherwise false.
     */
    boolean afterReceivingZReport(ZReport z);
	boolean afterReceivingZReport(ZReport z, StringBuffer accdateStrBuf);

    /**
     * ZEx report processor.
     *
     * @param zEx ZEx report object
     * @return true if success, otherwise false.
     */
    boolean afterReceivingZEx(Object[] zExes);

    /**
     * Transaction processor.
     *
     * @param trans Transaction and LineItem objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingTransaction(Object[] trans);

    /**
     * Department sales processor.
     *
     * @param depSales Department sales objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingDepSales(Object[] depSales);

    /**
     * Daishou sales processor.
     *
     * @param daishouSales Daishou sales objects.
     * @return true if success, otherwise false.
     */

    boolean afterReceivingDaishouSales(Object[] daishouSales);

	/**
	  * Daishou sales processor.
	  *
	  * @param DaiShouSales2 Daishou sales objects.
	  * @return true if success, otherwise false.
	  */

    boolean afterReceivingDaiShouSales2(Object[] daiShouSales2);
    
    /**
     * CashForm processor.
     *
     * @param cashForm CashForm objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingCashForm(Object[] cashForm);   

    /**
     * Inventory processor.
     *
     * @param inventory Inventory objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingInventory(Object[] inventory);   

    /**
     * Attendance processor.
     *
     * @param attendance Attendance objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingAttendance(Object[] attendance);

     /**
     * Attendance1 processor.
     *
     * @param attendance1 Attendance1 objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingAttendance1(Object[] attendance1);
    /**
     * HistoryTrans processor.
     *
     * @param hts HistoryTrans objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingHistoryTrans(Object[] hts);   
    /**
     * PosVersion processor.
     *
     * @param posVersion PosVersion objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingPosVersion(Object[] posVersion);
    
    /**
     * Master download preprocessor.
     *
     * @return true if it allows master downloading now, false otherwise.
     */
    boolean beforeDownloadingMaster();
    
    /**
     * 
     * 
     */
    boolean canSyncTransaction();
    
    /**
     * 变价
     */
    void afterDonePluPriceChange(String ID, String posID, boolean success);
    
    boolean recaleAfterReceivingDepSales(DbConnection connection, Date accountingDateObj) throws SQLException;

    /**
     * SsmpLog processor.
     *
     * @param ssmpLog SsmpLog object
     * @param generatedId the id of the newly-inserted commul_ssmp_log record
     * @return true if success, otherwise false
     */
    boolean afterReceivingSsmpLog(SsmpLog ssmpLog, StringBuilder generatedId);
}
