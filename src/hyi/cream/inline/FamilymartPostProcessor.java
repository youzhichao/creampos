package hyi.cream.inline;

/**
 * PostProcessor for Familymart. TO BE REMOVED.
 *
 * @author Meyer
 * @version 1.0
 */
public class FamilymartPostProcessor extends PostProcessorAdapter {

//    /**
//     * /Meyer/2003-02-10
//     *		1、在CStorePostProcess基础上建立新类
//     * 		2、增加处理"现金清点单"的处理方法	
//     * 
//     * 
//     */
//    transient public static final String VERSION = "1.0";
//
//    static final int DATE_ACCDATE         = 200;
//    static final int DATE_BIZDATE         = DATE_ACCDATE + 1;
//    static final int DATE_INFDATE         = DATE_ACCDATE + 2;
//    static private DateFormat dateFormatter = CreamCache.getInstance().getDateFormate();
//    static private DateFormat yyyy_MM_dd = CreamCache.getInstance().getDateFormate();
//
//    private static java.util.Date getAccountingDate() {
//        java.util.Date accountingDate = null;
//        DbConnection connection = null;
//        Statement statement = null;
//        ResultSet resultSet = null;
//        try {
//            connection = CreamToolkit.getPooledConnection();
//            statement = connection.createStatement();
//            resultSet = statement.executeQuery(
//            	"SELECT sp_value FROM system_parameters WHERE sp_id='0004'");
//            if (resultSet.next()) {
//        		accountingDate = resultSet.getDate("sp_value"); 		
//				System.out.println("############ found" + accountingDate);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace(Server.getLogger());
//        }finally {
//            try {
//                if (statement != null)
//                    statement.close();
//                if (connection != null)
//                    CreamToolkit.releaseConnection(connection);
//            } catch (SQLException e2) {
//                e2.printStackTrace(Server.getLogger());
//            }
//        }
//		System.out.println("############ 1" + accountingDate);
//        if (accountingDate == null) {
//        	accountingDate = new java.util.Date();
//        }
//        System.out.println("############# 2" + accountingDate);
//        return accountingDate;
//    }
//
//
//    public boolean afterReceivingShiftReport(ShiftReport shift) {
//		Date accountingDate = shift.getAccountingDate();
//        if (accountingDate == null || ((yyyy_MM_dd.format(accountingDate)).startsWith("197"))) {
//			// 根据Z的结束日期来决定会计日期
//			accountingDate = getAccountingDate();  // get accountDate from Table calendar         	
//	        shift.setAccountingDate(getAccountingDate());
//        }
//        shift.deleteByPrimaryKey();
//        
//        if (!shift.insert(false)) {
//            Server.log("Shift inserted failed (pos=" + shift.getTerminalNumber() +
//                ",z=" +shift.getZSequenceNumber() +
//                ",shift=" +shift.getSequenceNumber() + ").");
//            return false;
//        }
//        Server.log("Shift inserted (date=" + dateFormatter.format(shift.getAccountingDate()) +
//            ",pos=" + shift.getTerminalNumber() +
//            ",z=" +shift.getZSequenceNumber() +
//            ",shift=" +shift.getSequenceNumber() + ").");
//        return true;
//    }
//
//    public boolean afterReceivingZReport(ZReport z) {
//        boolean returnValue = true;
//        
//        Date accountingDate = (Date)DacBase.getValueOfStatement(
//                "SELECT accountdate FROM posul_z WHERE storeID = '" + z.getStoreNumber()
//        		+ "' AND posNumber = " + z.getTerminalNumber()
//        		+ " AND zSequenceNumber = " + z.getSequenceNumber());
//        if (accountingDate == null || ((yyyy_MM_dd.format(accountingDate)).startsWith("197"))) {
//			// 根据Z的结束日期来决定会计日期
//			accountingDate = getAccountingDate();  // get accountDate from Table calendar         	
//        }
//        
//		z.setAccountingDate(accountingDate);
//		
//        z.deleteByPrimaryKey();
//
//        if (!z.insert(false)) {
//            Server.log("Z inserted failed (pos=" + z.getTerminalNumber() +
//                    ",z=" + z.getSequenceNumber() + ").");
//            return false;
//            
//        }
//        Server.log("Z inserted (date=" + dateFormatter.format(z.getAccountingDate()) +
//            ",pos=" + z.getTerminalNumber() +
//            ",z=" + z.getSequenceNumber() + ").");
//
//        return returnValue;
//    }
//
//    private java.util.Date getBussinessDate() {
//        String storeID = getStoreID();//CreamToolkit.getConfiguration().getProperty("StoreID");
//        DbConnection connection = null;
//        Statement statement = null;
//        ResultSet resultSet = null;
//        String select = "SELECT * FROM calendar";
//        String where = " WHERE storeID = '" + storeID + "' AND dateType = '" + "0'";
//        try {
//            connection = CreamToolkit.getPooledConnection();
//            statement = connection.createStatement();
//            resultSet = statement.executeQuery(select + where);
//        } catch (SQLException e) {
//            e.printStackTrace(Server.getLogger());
//            try {
//                if (statement != null)
//                    statement.close();
//                if (connection != null)
//                    CreamToolkit.releaseConnection(connection);
//            } catch (SQLException e2) {
//                e2.printStackTrace(Server.getLogger());
//            }
//            return new java.util.Date();
//        }
//        HashMap newMap = null;
//        try {
//            if (resultSet.next()) {
//                newMap = new HashMap();
//                ResultSetMetaData metaData = resultSet.getMetaData();
//                int t = 0;
//                for (int i = 1; i <= metaData.getColumnCount(); i++) {
//					Object o = resultSet.getObject(i);
//					if (o != null) {
//						t = metaData.getColumnType(i);
//						if (t == Types.NUMERIC || t == Types.DECIMAL) {
//							o = new HYIDouble(((BigDecimal)o).doubleValue());
//						}
//					}
//					newMap.put(metaData.getColumnName(i), o);
//                }
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (statement != null)
//                    statement.close();
//                if (connection != null)
//                    CreamToolkit.releaseConnection(connection);
//            } catch (SQLException e) {
//                e.printStackTrace(Server.getLogger());
//            }
//        }
//        java.util.Date accDate = new java.util.Date();
//        if (newMap == null)
//            return accDate;
//        if (newMap.get("currentDate") instanceof java.util.Date)
//            accDate = (java.util.Date)newMap.get("currentDate");
//        return accDate;
//    }
//
//    public boolean afterReceivingTransaction(Object[] trans) {
//        int i;
//        boolean success = true;
//        int tranNo = 0;
//        
//        int len = trans.length;
//        if (len <= 0 || !(trans[0] instanceof Transaction))
//        	return false;
//        Transaction tran = (Transaction) trans[0];
//        Date tranTime = tran.getSystemDateTime();
//        tranNo = tran.getTransactionNumber().intValue();
//        Server.log("-------- start insert " + tran.getTerminalNumber() + "|" + tran.getTransactionNumber());
//        for (i = 1; i < len; i++) {
//        	if (trans[i] instanceof LineItem) {
//                LineItem li = (LineItem)trans[i];
//                if (tran != null) {
//                    tran.addLineItemSimpleVersion(li);
//                    li.setSystemDateTime(tranTime);
//                    Server.log("lineItem insert : " + li.getTransactionNumber() 
//                    		+ "|" + li.getLineItemSequence() + "|" + li.getSystemDateTime());
//                    if (!li.insert(false)) {
//                    	success = false;
//                    	break;
//                    }
//                }
//            } else {
//            	success = false;
//            	break;
//            }
//        }
//        
//        Server.log("tran insert : " + tran.getTerminalNumber() + "|" + tran.getTransactionNumber() 
//        		 + "|" + tran.getSystemDateTime());
//        if (success)
//        	success = tran.insert(false);
//        else 
//        	Server.log("insert detail error [trannumber]: " + tranNo + ". will del head & detail!");
//        
//        if (!success) {
//            for (i = 0; i < len; i++) {
//                if (trans[i] instanceof DacBase) {
//                	((DacBase) trans[i]).deleteByPrimaryKey();
//                }
//            }
//        }
//        return success;
//    }
//
//    public boolean afterReceivingDepSales(Object[] depSales) {
//        boolean success = true;
//        int zNumber = ((DepSales)depSales[0]).getSequenceNumber();
//        int posNumber = ((DepSales)depSales[0]).getPOSNumber();
//		
//        DepSales tmp = (DepSales) depSales[0];
//        Date accountingDate = (Date)DacBase.getValueOfStatement(
//                "SELECT top 1 accountdate FROM posul_depsales WHERE storeID = '" + tmp.getStoreNumber()
//        		+ "' AND posNumber = " + tmp.getPOSNumber() + " AND zSequenceNumber = "
//        		+ tmp.getSequenceNumber() + " AND depID = '" + tmp.getDepID() + "'");
//        
//        if (accountingDate == null || ((yyyy_MM_dd.format(accountingDate)).startsWith("197"))) {
//			// 根据Z的结束日期来决定会计日期
//			accountingDate = getAccountingDate();  // get accountDate from Table calendar         	
//        }
//        //删除已经存在的
//        DepSales.deleteBySequenceNumber(zNumber,posNumber);
//        for (int i = 0; i < depSales.length; i++) {
//            DepSales ds = (DepSales)depSales[i];
//   	        ds.setAccountDate(accountingDate);
//            if (!ds.insert(false)) {
//                Server.log("DepSales inserted failed (pos=" + ds.getPOSNumber() +
//                    ",z=" + ds.getSequenceNumber() +
//                    ",id=" + ds.getDepID() + ").");
//                success = false;
//            } else {
//                Server.log("DepSales inserted (pos=" + ds.getPOSNumber() +
//                    ",z=" + ds.getSequenceNumber() +
//                    ",id=" + ds.getDepID() + ").");
//            }
//        }
//        return success;
//    }
//
//    public boolean afterReceivingDaishouSales(Object[] daishouSales) {
//        boolean success = true;
//        
//        int zNumber = ((DaishouSales)daishouSales[0]).getZSequenceNumber();
//        int posNumber = ((DaishouSales)daishouSales[0]).getPosNumber();
//		
//		DaishouSales tmp = (DaishouSales) daishouSales[0];
//        Date accountingDate = (Date)DacBase.getValueOfStatement(
//                "SELECT top 1 accountdate FROM posul_daishousales WHERE storeID = '" 
//                + tmp.getStoreID()
//        		+ "' AND posNumber = " + tmp.getPosNumber() + " AND zSequenceNumber = "
//        		+ tmp.getZSequenceNumber());
//        
//        if (accountingDate == null || ((yyyy_MM_dd.format(accountingDate)).startsWith("197"))) {
//			// 根据Z的结束日期来决定会计日期
//			accountingDate = getAccountingDate();  // get accountDate from Table calendar         	
//        }
//        
//        DaishouSales.deleteBySequenceNumber(zNumber, posNumber);
//        for (int i = 0; i < daishouSales.length; i++) {
//            DaishouSales ds = (DaishouSales)daishouSales[i];
//   	        ds.setAccountDate(accountingDate);
//            if (!ds.insert(false)) {
//                Server.log("DaishouSales inserted failed (pos=" + ds.getPosNumber() +
//                    ",z=" + ds.getZSequenceNumber() +
//                    ",id1=" + ds.getFirstNumber() +
//                    ",id2=" + ds.getSecondNumber() + ").");
//                success = false;
//            } else {
//                Server.log("DaishouSales inserted (pos=" + ds.getPosNumber() +
//                    ",z=" + ds.getZSequenceNumber() +
//                    ",id1=" + ds.getFirstNumber() +
//                    ",id2=" + ds.getSecondNumber() + ").");
//            }
//        }
//        return success;
//    }
//    
//	/**
//	 * afterReceivingDaiShouSales2
//	 * 
//	 * @return 如果代收资料存入SC数据库中成功则返回 true，否则返回 false.
//	 */
//	public boolean afterReceivingDaiShouSales2(Object[] daiShouSales2) {
//		boolean success = true;
//		
//        int zNumber = ((DaiShouSales2)daiShouSales2[0]).getZSequenceNumber();
//        int posNumber = ((DaiShouSales2)daiShouSales2[0]).getPosNumber();
//		
//		DaiShouSales2 tmp = (DaiShouSales2) daiShouSales2[0];
//		String sql = "SELECT  top 1 accountdate  FROM posul_daishousales2 WHERE storeID = '" + tmp.getStoreID()
//		    + "' AND posNumber = " + tmp.getPosNumber() + " AND ZSequenceNumber = "  + tmp.getZSequenceNumber();
//        Date accountingDate = (Date)DacBase.getValueOfStatement(sql);
//
//        if (accountingDate == null || ((yyyy_MM_dd.format(accountingDate)).startsWith("197"))) {
//			// 根据Z的结束日期来决定会计日期
//			accountingDate = getAccountingDate();  // get accountDate from Table calendar         	
//        }
//        
//        DaiShouSales2.deleteBySequenceNumber(zNumber, posNumber);
//        for (int i = 0; i < daiShouSales2.length; i++) {
//			DaiShouSales2 ds = (DaiShouSales2)daiShouSales2[i];
//   	        ds.setAccountDate(accountingDate);
//			if (!ds.insert(false)) {
//				Server.log("DaiShouSales2 insert failed ( StoreID = " + ds.getStoreID() 
//				+ ", POS = " + ds.getPosNumber()
//				+ ", Z = " + ds.getZSequenceNumber()
//				+ ", ID = " + ds.getID() 
//				+ ")");
//				success = false;
//			} else {
//				Server.log("DaiShouSales2 inserted ( StoreID = " + ds.getStoreID() 
//				+ ", POS = " + ds.getPosNumber()
//				+ ", Z = " + ds.getZSequenceNumber()
//				+ ", ID = " + ds.getID() 
//				+ ")");
//			}
//		}
//		return success;
//	}
//
//    
//    /*
//     * 处理现金清点单
//     * 
//     */
//    public boolean afterReceivingCashForm(Object[] CashForm) {
//        boolean success = true;
//        for (int i = 0; i < CashForm.length; i++) {
//            CashForm cf = (CashForm)CashForm[i];
//
//            cf.deleteByPrimaryKey();
//            if (!cf.insert(false)) {
//                Server.log("CashForm inserted failed (storeID=" + cf.getStoreID() + 
//        		",pos=" + cf.getPOSNumber() +
//                ",z=" + cf.getZSequenceNumber() + 
//                ",shift=" + cf.getSequenceNumber() + ").");
//                success = false;
//            } else {
//                Server.log("CashForm inserted (storeID=" + cf.getStoreID() + 
//        		",pos=" + cf.getPOSNumber() +
//                ",z=" + cf.getZSequenceNumber() + 
//                ",shift=" + cf.getSequenceNumber() + ").");
//            }
//        }
//        return success;
//    }
//
//	/**
//	 * after done PluPriceChange, write flag into db
//	 * 
//	 */
//    public void afterDonePluPriceChange(String ID, String posID, boolean success) {
//        String status = success ? "0" : "1";
//        DbConnection connection = null;
//        Statement statement = null;
//        try {
//            connection = CreamToolkit.getPooledConnection();
//            statement = connection.createStatement();
//			/*
//			 * Meyer / 2003-02-21/just update, LogicChina has inserted the record 
//			 */
//            statement.executeUpdate(
//            	" UPDATE pricechage_dl_status SET pds_status='" + status + "'" + 
//            	" WHERE pds_serial_number='" + ID + "'" + 
//            	" AND pds_pos_id='" + posID + "'");
//        } catch(SQLException e) {
//        	e.printStackTrace();        	
//        } finally {
//            try {
//                if (statement != null)
//                    statement.close();
//                if (connection != null)
//                    CreamToolkit.releaseConnection(connection);
//            } catch (SQLException e2) {
//                e2.printStackTrace(Server.getLogger());  
//            }
//        }
//    }
//    
//    /*
//     * 
//     * 
//     */
//    public boolean afterReceivingAttendance(Object[] attendances) {
//        boolean success = true;
//        for (int i = 0; i < attendances.length; i++) {
//        	Attendance attendance = (Attendance)attendances[i];
//        	attendance.deleteByPrimaryKey();
//        	if (!attendance.insert(false)) {
//                Server.log("work_check inserted failed (storeID=" + attendance.getStoreID() + 
//        		",Employee=" + attendance.getEmployee() +
//                ",type=" + attendance.getShiftType() + 
//                ",SignOndttm=" + attendance.getSignOnDateTime() + ").");
//                success = false;
//            } else {
//                Server.log("work_check inserted (storeID=" + attendance.getStoreID() + 
//        		",Employee=" + attendance.getEmployee() +
//                ",type=" + attendance.getShiftType() + 
//                ",SignOndttm=" + attendance.getSignOnDateTime() + ").");
//            }
//        }
//        return success;
//    }
//
//    public boolean afterReceivingHistoryTrans(Object[] hts) {
//        boolean success = true;
//        java.util.Date accdate = getAccountingDate();
//
//        for (int i = 0; i < hts.length; i++) {
//        	HistoryTrans ht = (HistoryTrans)hts[i];
//    		
//        	Date accountingDate = ht.getAccdate();
//            if (accountingDate == null || ((yyyy_MM_dd.format(accountingDate)).startsWith("197"))) {
//    			accountingDate = accdate;         	
//    			ht.setAccdate(accountingDate);
//            }
//        	
//        	ht.deleteByPrimaryKey();
//        	if (!ht.insert(false)) {
//                Server.log("HistoryTrans inserted failed (storeID=" + ht.getStoreid() + 
//        		",Systemdatetime=" + ht.getSystemdatetime() +
//                ",Terminalnumber=" + ht.getTerminalnumber() + 
//                ",trannumber=" + ht.getPrinttrannumber() + 
//                ").");
//                success = false;
//            } else {
//                Server.log("HistoryTrans inserted (storeID=" + ht.getStoreid() + 
//                		",Systemdatetime=" + ht.getSystemdatetime() +
//                        ",Terminalnumber=" + ht.getTerminalnumber() + 
//                        ",trannumber=" + ht.getPrinttrannumber() + 
//                        ").");
//            }
//        }
//        return success;
//    }
//
//    /*
//     * 
//     * 
//     */
//    public boolean afterReceivingPosVersion(Object[] posVersions) {
//        boolean success = true;
//        for (int i = 0; i < posVersions.length; i++) {
//        	try{
//        	Version posVersion = (Version)posVersions[i];
//
//        	posVersion.deleteByPrimaryKey();
//            if (!posVersion.insert(false)) {
//                Server.log("posVersion inserted failed (version=" + posVersion.getVersion() + 
//        		 ").");
//                success = false;
//            } else {
//                Server.log("posVersion inserted (version=" + posVersion.getVersion() + 
//        		 ").");
//            }
//        	}catch(Exception e2){
//        		e2.printStackTrace(Server.getLogger());  
//        	};
//        }
//        return success;
//    }
//
//    
//	public static void main(String[] args) {
//		Server.setFakeExist();
//		FamilymartPostProcessor.getAccountingDate();
//	}
     
}
