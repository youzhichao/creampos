package hyi.cream.inline;


public interface POSStatusListener extends java.util.EventListener {

    /**
     * Invoked when POS's status has been changed.
     *
     * @param e an event object represents the changes.
     */
    public void posStatusChanged(POSStatusEvent e);
}
