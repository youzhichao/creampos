package hyi.cream.inline;

import hyi.cream.Version;
import hyi.cream.dac.DacBase;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.ThreadWatchDog;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Inline server(singleton).
 * <p/>
 * Sample cream.conf:
 * <pre>
 * Locale=zh_TW
 * MaxLineItems=50
 * ServerDatabaseConnectionIncrement=5
 * InlineServerLogFile=inline.log
 * InlineClientUseJVM=YES
 * LogFile=cream.log
 * DebugInlineServer=yes
 * PostProcessor=hyi.cream.inline.PostProcessorImplA
 * EnableMessageDownload=yes
 * DebugInlineServer=yes
 * CacheDownloadFile=yes
 * </pre>
 *
 * @author Bruce
 */
public class Server implements POSStatusListener {

    /**
     * /Bruce/1.5/2002-04-03/
     *    Add starting up log message.
     */
    transient public static final String VERSION = "2.0";

    public static final int INLINE_SERVER_TCP_PORT = 3000;
    public static final String INLINE_SERVER_DOMAIN_NAME = "sc";

    private ServerSocket serverSocket;
    private static Server server;
    private ArrayList listener = new ArrayList();

    static private PrintWriter logger;
    static private DateFormat dateFormatter;
    public static boolean debugLog = false;
    static private boolean fakeAtPosSide;
    static private boolean fakeExist = false;
    static private boolean updateInv = false;
    static private int threadCount = 0;
    static int inlineLongTimeout = 360;

    private static void init() {
        try {
            dateFormatter = CreamCache.getInstance().getDateTimeFormate();
            CreamToolkit.getLogLevel();// init db pools
            String logFilePath = GetProperty.getInlineServerLogFile();
            System.out.println("Log file url: " + logFilePath);
            logger = new PrintWriter(new OutputStreamWriter(new FileOutputStream(
                logFilePath, true), "UTF-8"));
            String debug = GetProperty.getDebugInlineServer("");
            if (debug.compareToIgnoreCase("yes") == 0) {
                debugLog = true;
            }

            String updateInvStr = GetProperty.getUpdateInventory("yes");
            updateInv = updateInvStr.equalsIgnoreCase("yes");
            inlineLongTimeout = GetProperty.getInlineLongTimeout() * 1000;
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    synchronized static public void log(String message) {
        if (logger != null) {
            message = "[" + dateFormatter.format(new java.util.Date()) + "]"
                    + message;
            System.out.println(message);
            logger.println(message);
            logger.flush();
        }  else {
            System.out.println(message);
        }
    }

    static public PrintWriter getLogger() {
        return logger;
    }

    public Server() throws InstantiationException {
        if (server != null)
            throw new InstantiationException();

        try {
            serverSocket = new ServerSocket(INLINE_SERVER_TCP_PORT);
            server = this;
            init();
        } catch (IOException e2) { // if an I/O error occurs when creating the socket.
            System.err.println(e2);
            throw new InstantiationException();
        } catch (SecurityException e3) { // a security manager exists and its checkConnect method doesn't allow the operation
            System.err.println(e3);
            throw new InstantiationException();
        }
    }

    /**
     * 为Server端准备主档文件所用
     * @param b no matter true or false;
     */
    Server(boolean b) {
        server = this;
        init();
    }

    public static void setFakeAtPosSide(boolean fakeAtPosSide) {
        Server.fakeAtPosSide = fakeAtPosSide;
    }

    public static void setFakeExist() {
        fakeExist = true;
    }

    public static boolean isAtServerSide() {
        return serverExist();
    }

    public static boolean serverExist() {
        if (fakeAtPosSide)
            return false;
        return (fakeExist) ? true : (server != null);
    }

    public static Server getInstance() throws InstantiationException {
        if (server == null)
            server = new Server();
        return server;
    }

    synchronized public void modifyThreadCount(int count) {
        threadCount += count;
    }
    
    public int getThreadCount() {
        return threadCount;
    }
    
    public void startup() {
        log("Inline server is starting up.");
        
        //the tpt thread is a mutable thread that is running to process transactions
        TranProcessorThread tpt = new TranProcessorThread("TranProcessorThread");
        tpt.start();

        //the dog will be watching on the threads 
        //if the thread is dead, then the dog will restart it. 
        ThreadWatchDog dog = ThreadWatchDog.getInstance();
        ThreadWatchDog.setLogger(getLogger());
        dog.put(tpt);
        dog.start();
        int maxCount = -1;
        try {
            maxCount = Integer.parseInt(GetProperty
                    .getMaxThreadCount("-1"));
            //log("MaxThreadCount : " + maxCount);
        } catch (Exception e) {}
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                log(socket.getInetAddress().getHostAddress() + ":" + socket.getPort() + " is connected.");
                ServerThread t = new ServerThread(socket);
                modifyThreadCount(1);
                if (maxCount == -1) {
                } else if (maxCount < threadCount) {
                    log("Inline connection over limit(" + maxCount + ")! Inline Server shutdown.");
                    System.exit(0);
                }
                log("Current inline connection count=" + threadCount);
                t.start();
            } catch (IOException e) {
                e.printStackTrace(getLogger());
            } catch (InstantiationException e) {
                e.printStackTrace(getLogger());
            } catch (OutOfMemoryError e) {
                log(e.getMessage());
                log("Inline server shutdown.");
                System.exit(0);
            }
        }
    }

    public void closeServerSocket() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace(getLogger());
        }
    }

    public void addPOSStatusListener(POSStatusListener l) {
        listener.add(l);
    }

    private Iterator getPOSStatusListener() {
        return listener.iterator();
    }

    /**
     * Fire event to every POSStatusListener
     */
    private void fireEvent(POSStatusEvent e) {
        Iterator iter = getPOSStatusListener();
        while (iter.hasNext()) {
            POSStatusListener l = (POSStatusListener)iter.next();
            l.posStatusChanged(e);
        }
    }

    synchronized public void posStatusChanged(POSStatusEvent e) {
        fireEvent(e);
    }

    public void createServerGUI() {
        boolean packFrame = false;

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(Exception e) {
            e.printStackTrace(getLogger());
        }
        final ServerGUIFrame frame = new ServerGUIFrame();
        //Center the window
        //frame.setUndecorated(true);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = new Dimension(600, 400);
        frame.setSize(frameSize);
        if (frameSize.height > screenSize.height) {
            frameSize.height = screenSize.height;
        }
        if (frameSize.width > screenSize.width) {
            frameSize.width = screenSize.width;
        }
        frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);

        //Validate frames that have preset sizes
        //Pack frames that have useful preferred size info, e.g. from their layout

        if (packFrame) {
            frame.pack();
        }
        else {
            frame.validate();
        }
        frame.myInit();
        frame.setVisible(true);
    }

    /**
     * Server main().
     */
    public static void main(String[] args) {
        Server c;
        Server.setFakeExist();
        try {
            //@pingping 2015-05-13 inlineServer端不检查表
//            HealthyChecker.checkForServer();

            if (args.length != 0 && args[0].equalsIgnoreCase("-getMaster")) {
                c = new Server(true);
                Server.prepareMasterFiles();
                return;  
            }
            if (args.length >= 3 && args[0].equalsIgnoreCase("-recaleAccdayrptFromDepsales")) {
                c = new Server(true);
                java.sql.Date beginDate = java.sql.Date.valueOf(args[1]);
                java.sql.Date endDate = java.sql.Date.valueOf(args[2]);
                if (beginDate.after(endDate)){
                    System.out.println("beginDate cant't after endDate!");
                    Server.log("beginDate cant't after endDate!");
                    return;
                }
                DbConnection connection = CreamToolkit.getTransactionalConnection();
                PostProcessor pp = ServerThread.getPostProcessor();
                java.util.Date accountingDateObj = beginDate;
                while (!accountingDateObj.after(endDate)) {
                    pp.recaleAfterReceivingDepSales(connection, accountingDateObj);
                    accountingDateObj = CreamToolkit.shiftDay(accountingDateObj, 1);
                }
                connection.commit();
                Server.log("recaleAccdayrptFromDepsales finish!");
                CreamToolkit.releaseConnection(connection);
                return;
            }

            c = Server.getInstance();
            log("Version: " + Version.getVersion() + " " + Version.getVersionDate()); 
             
            shrinkLogFiles();
            if (args.length != 0 ) {
                if(args[0].equals("-gui")) 
                    c.createServerGUI();
            }
            if (GetProperty.getTransactionCheckerThread("no").equalsIgnoreCase("yes"))
                new TransactionCheckerThread().start();
            c.startup();
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace(getLogger());
        }
    }

    private static void prepareMasterFiles() {
        String classNames[] = {
            "hyi.cream.dac.PLU",
            "hyi.cream.dac.Cashier",
            "hyi.cream.dac.Reason",
            "hyi.cream.dac.MixAndMatch",
            "hyi.cream.dac.Payment",
            "hyi.cream.dac.TaxType",
            "hyi.cream.dac.Dep",
            "hyi.cream.dac.Category",
            "hyi.cream.dac.Store",
            "hyi.cream.dac.MMGroup",
            "hyi.cream.dac.CombDiscountType",
            "hyi.cream.dac.CombGiftDetail",
            "hyi.cream.dac.CombPromotionHeader",
            "hyi.cream.dac.CombSaleDetail", 
            "hyi.cream.dac.DaiShouDef",
            "hyi.cream.dac.SI",
            "hyi.cream.dac.Rebate",           
            "hyi.cream.dac.DiscountType"
        }; 
        
        for (int i = 0; i < classNames.length; i++) {
            try {
                Server.log("Prepare download sql file for " + classNames[i] + " ... ");
                getDownloadSQLFile(classNames[i]);
            } catch (Exception e) {
                e.printStackTrace(getLogger());
            }
        }
    }
    
    private synchronized static void getDownloadSQLFile(String className) throws Exception{
        
        File sqlFilePath = new File(CreamToolkit.getConfigDir() + className); 
        if (sqlFilePath.exists()) 
            sqlFilePath.delete();

        File recCountFilePath = new File(sqlFilePath.getAbsolutePath() + ".cnt");
        if (recCountFilePath.exists())
            recCountFilePath.delete();
        
        //generate the download SQL file
        // Invoke the DAC's getAllObjectsForPOS()
        //System.out.println("Invoking hyi.cream.dac.PLU.getAllObjectsForPOS...");                
        Collection dacs =
            (Collection) Class.forName(className).getDeclaredMethod(
                "getAllObjectsForPOS",
                new Class[0]).invoke(
                null,
                new Object[0]);
        if (dacs == null || dacs.size() == 0)
            return;
            
        Object dacArray[] = dacs.toArray();
        
        //System.out.println("Creating download file: " + sqlFilePath + "...");
        OutputStream sqlFile = new BufferedOutputStream(new FileOutputStream(sqlFilePath));
        for (int i = 0; i < dacArray.length; i++) {
            DacBase dac = (DacBase) dacArray[i];
            try {
                String insertString = dac.getInsertSqlStatement(true);
                sqlFile.write(insertString.getBytes());
                sqlFile.write(';');
                sqlFile.write('\n');
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        sqlFile.close();

        // create record count file
        //System.out.println("Creating download file: " + recCountFilePath + "...");
        DataOutputStream recCountFile = new DataOutputStream(new FileOutputStream(recCountFilePath));
        recCountFile.write((dacArray.length + "").getBytes());
        recCountFile.close();

    }
    
    public static void shrinkLogFiles() {
        /*
        try {
            Runtime.getRuntime().exec("/usr/bin/shrinkfile -s 10m /root/cream.log /root/inline.log");
        } catch (IOException e) {
                e.printStackTrace(CreamToolkit.getLogger());
        }
        */
        logger.close();
        logger = null;
        CreamToolkit.closeLog();
        String f = GetProperty.getInlineServerLogFile();
        if (!f.equals(""))
            CreamToolkit.shrinkFile(new File(f), 10L * 1024 * 1024);
        f = GetProperty.getLogFile();
        if (!f.equals(""))
            CreamToolkit.shrinkFile(new File(f), 10L * 1024 * 1024);
        CreamToolkit.openLogger();
        init();
    }

    /**
     * 上传交易时是否更新库存表。
     * 
     * @return true if need update Inventory.
     */
    public static boolean isUpdateInv() {
        return updateInv;
    }
}
