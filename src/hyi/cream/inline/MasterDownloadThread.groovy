package hyi.cream.inline

import hyi.cream.POSTerminalApplication
import hyi.cream.dac.Alipay_detail
import hyi.cream.dac.Cmpay_detail
import hyi.cream.dac.LineItem
import hyi.cream.dac.Selfbuy_detail
import hyi.cream.dac.Selfbuy_head
import hyi.cream.dac.Transaction
import hyi.cream.dac.UnionPay_detail
import hyi.cream.dac.WeiXin_detail
import hyi.cream.groovydac.Param
import hyi.cream.groovydac.SsmpLog
import hyi.cream.state.GetProperty
import hyi.cream.state.StateMachine
import hyi.cream.util.CreamCache
import hyi.cream.util.CreamToolkit
import java.text.DateFormat
import java.util.regex.Matcher

/**
 * Thread for downloading masters, program files, shrinking log files, and
 * purging outdated POS transaction data.
 * 
 * @author Bruce You
 */
public class MasterDownloadThread extends Thread {

    // Singleton ------------------------------------------------------------
    private static MasterDownloadThread instance = new MasterDownloadThread()
    private MasterDownloadThread() { init() }
    static MasterDownloadThread getInstance() {
        if (instance == null)
            instance = new MasterDownloadThread() 
        return instance
    }
    //-----------------------------------------------------------------------

    private DateFormat dateFormatter
    private String inlineServerProgramDirectory
    private def masterVersion
    private def res = CreamToolkit.GetResource()
    private int posNo
    private Boolean versionControlled
    private Param param
    private boolean veryFirstTime = true

    def init() {
        param = Param.instance
        dateFormatter = CreamCache.instance.dateFormate
        inlineServerProgramDirectory = param.inlineServerProgramDirectory
        posNo = Param.instance.terminalNumber
        masterVersion = param.masterVersion
        if (!masterVersion) {
            masterVersion = dateFormatter.format(new Date())
            param.masterVersion = masterVersion
        }
        setDaemon true
        setName 'MasterDownloadThread'
        setPriority this.MIN_PRIORITY
        start()
    }

    /** Check to see if now is time for master or program download. */
    private Map isTimeToDownload() {
        try {
            if (veryFirstTime) {
                veryFirstTime = false
                return [timeToDownloadMaster: true,
                        timeToDownloadProgram: true]
            }
            // master and program download time is splitted by a slash
            def regexes = param.downloadMasterHour.split('/')
            def downloadMasterHourRE = regexes[0]
            def downloadProgramHourRE = regexes.length > 1 ? regexes[1] : ''
            def delayInMillis = param.downloadMasterDelayBetweenPos * 1000L
            def boundaryTimeForPos = new Date(new Date().getTime() - (delayInMillis * (posNo - 1)))
            def timeNow = CreamCache.instance.timeFormate.format(boundaryTimeForPos) // in hh:mm:ss
            //println "timeNow=$timeNow"
            def timeToDownloadMaster
            def timeToDownloadProgram
            if (downloadMasterHourRE ==~ /\d{1,2}/) {
                //println "MasterDownload> match ${timeNow} with simple hour RE: ${downloadMasterHourRE}"
                // 如果只簡寫一個數字h (\d{1,2} 一到兩位數字), 則意思是 >=h:00,>=h:00。
                // 例如，只寫“1”時，表示在超過(凌晨1點+delay)後同時下傳master和program。這樣和舊的設置兼容了。
                timeToDownloadProgram = timeToDownloadMaster =
                    timeNow >= String.format('%02d:00:00', downloadMasterHourRE.toInteger())
            } else {
                // 否則參考參數 Param.downloadMasterHour 說明
                timeNow = timeNow[0..-4]  // drop second
                //println "MasterDonwlaod> match ${timeNow} with ${downloadMasterHourRE}/${downloadProgramHourRE}"
                timeToDownloadMaster = timeNow ==~ downloadMasterHourRE
                timeToDownloadProgram = timeNow ==~ downloadProgramHourRE
            }
            //println "MasterDonwload> return ${timeToDownloadMaster} and ${timeToDownloadProgram}"
            return [timeToDownloadMaster: timeToDownloadMaster,
                    timeToDownloadProgram: timeToDownloadProgram]
        } catch (Exception e) {
            CreamToolkit.logMessage e
            return [timeToDownloadMaster: false, timeToDownloadProgram: false]
        }
    }

    /**
     * Run shell scripts downloaded from server-side.
     *
     * @return true if scripts had been runned.
     */
    private boolean runScriptFiles() {
        boolean runned = false
        Matcher matcher = System.getProperty('os.name') =~ /Linux|Mac/
        boolean unix = matcher.find()
        for (String script in Client.instance.scriptFiles) {
            try {
                // run as a shell script
                ((unix ? ['/bin/bash'] : ['cmd', '/c']) + script)
                    .execute()
                    .waitForOrKill(3 * 60 * 1000)
                runned = true
            } catch (Exception e) {
                CreamToolkit.logMessage e

                // try again as an executable
                try { script.execute().waitForOrKill(3 * 60 * 1000); runned = true }
                catch (Exception x) { CreamToolkit.logMessage x }
            }
        }
        runned
    }

    synchronized boolean downloadProgramFiles() throws ClientCommandException {
        String serverDirectory

        if (inlineServerProgramDirectory) {
            if (!inlineServerProgramDirectory.endsWith('/'))
                inlineServerProgramDirectory += '/'
            serverDirectory = inlineServerProgramDirectory + posNo
        } else {
            serverDirectory = '/home/hyi/inline/posdl/' + posNo
        }

        // movefile to client's current directory
        Client.instance.processCommand 'moveFile ' + serverDirectory + ' .'
        runScriptFiles()
    }

    void shrinkLogFiles() {
        def logFileMaxSize = 20L * 1024 * 1024
        CreamToolkit.logMessage('Shrinking log files...')

        CreamToolkit.closeLog()
        String logFile = GetProperty.getLogFile()
        if (logFile) CreamToolkit.shrinkFile(new File(logFile), logFileMaxSize)
        CreamToolkit.shrinkFile(new File('events.log'), logFileMaxSize)
        CreamToolkit.openLogger()

        Client.closeLog()
        String inlineLogFile = GetProperty.getInlineClientLogFile()
        if (inlineLogFile) CreamToolkit.shrinkFile(new File(inlineLogFile), logFileMaxSize)
        Client.openLog()

        CreamToolkit.shrinkFile(new File('debug.log'), logFileMaxSize)
        CreamToolkit.shrinkFile(new File('err.log'), logFileMaxSize)
        CreamToolkit.shrinkFile(new File('stdout.log'), logFileMaxSize)
        CreamToolkit.shrinkFile(new File('stderr.log'), logFileMaxSize)

        // remove transaction log two months ago
        File tranLogDir = new File('tranlog')
        if (tranLogDir.exists() && tranLogDir.isDirectory()) {
            def twoMonthsAgo = dateFormatter.format(new Date() - 62)
            tranLogDir.eachFile { File file ->
                if (file.name.compareTo(twoMonthsAgo) < 0) {
                    file.delete()
                    CreamToolkit.logMessage file.absolutePath + ' is deleted.'
                }
            }
        }
    }

    void deleteOutdatedData() {
        Cmpay_detail.deleteOutdatedData()
        Alipay_detail.deleteOutdatedData()
        WeiXin_detail.deleteOutdatedData()
        UnionPay_detail.deleteOutdatedData()
        Selfbuy_head.deleteOutdatedData()
        Selfbuy_detail.deleteOutdatedData()
        LineItem.deleteOutdatedData()
        Transaction.deleteOutdatedData()
        SsmpLog.deleteOutdatedData()
    }

    /**
     * Check to see if master is version-controlled.
     *
     * @return null if cannot be determined yet, otherwise true or false.
     */
    private Boolean isVersionControlled() {
        try {
            if (versionControlled == null) {
                def client = Client.instance
                if (client.isConnected()) {
                    client.processCommand 'getMasterVersion'
                    if (client.returnObject2)
                        versionControlled = true
                    else
                        versionControlled = false
                }
            }
        } catch (Exception e) {
            CreamToolkit.logMessage e
        }
        versionControlled
    }

    public void run() {
        def client = Client.instance
        def indicator = POSTerminalApplication.instance.messageIndicator
        def app = POSTerminalApplication.instance
        def gonnaDownloadMaster = false     // "即將下傳主檔" 旗標
        def gonnaDownloadProgram = false    // "即將下傳程序文件" 旗標

        // Main loop --

        while (true) {
        try {
            if (this.interrupted())
                return

            String origMessage = null
            def tableDownloaded = false
            def downloadSomeFiles = false

            sleep 5000

            if (!client.isConnected())
                continue

            def versionControlled = isVersionControlled()
            if (versionControlled == null)
                continue    // do nothing if cannot be determined yet

            // Check to see if now is time for master or program download
            def t2d = isTimeToDownload()
            // returns t2d.timeToDownloadMaster and t2d.timeToDownloadProgram

            if (!gonnaDownloadMaster && !gonnaDownloadProgram &&          // 如果之前沒發現到了下傳時間
                !t2d.timeToDownloadMaster && !t2d.timeToDownloadProgram)  // 而且現在也還不到下傳時間
                continue                                                  // 就do nothing

            //println "DownlaodMaster> gonnaDownloadMaster = ${gonnaDownloadMaster}"
            //println "DownlaodMaster> gonnaDownloadProgram = ${gonnaDownloadProgram}"
            //println "DownlaodMaster> t2d.timeToDownloadMaster = ${t2d.timeToDownloadMaster}"
            //println "DownlaodMaster> t2d.timeToDownloadProgram = ${t2d.timeToDownloadProgram}"

            if (t2d.timeToDownloadMaster) // 如果現在到了下傳主檔時間，則打開 "即將下傳主檔" 旗標
                gonnaDownloadMaster = t2d.timeToDownloadMaster
            if (t2d.timeToDownloadProgram) // 如果現在到了下傳程序文件時間，則打開 "即將下傳程序文件" 旗標
                gonnaDownloadProgram = t2d.timeToDownloadProgram

            if (!app.getTransactionEnd()) // 如果準備下傳東西，但在交易中
                continue                  // do nothing

            // Time is up...

            if (versionControlled) {
                if (gonnaDownloadMaster) {
                    try {
                        tableDownloaded = client.downloadMasters() > 0
                    } catch (Exception e) {
                        CreamToolkit.logMessage e
                        indicator.setMessage(res.getString("MasterDownloadFailed"))
                        SsmpLog.report10011(e)
                    }
                }

                if (gonnaDownloadProgram) {
                    origMessage = indicator.getMessage()
                    StateMachine.instance.setEventProcessEnabled false
                    indicator.setMessage res.getString('ProgramDownloading')
                    downloadProgramFiles()
                    downloadSomeFiles = client.getNumberOfFilesGotOrMoved() > 0
                }
                String versionShouldHave = dateFormatter.format(new Date())
                if (param.masterVersion != versionShouldHave) {
                    shrinkLogFiles()
                    deleteOutdatedData()
                    param.masterVersion = versionShouldHave
                }
            } else {
                String versionShouldHave = dateFormatter.format(new Date())
                if (param.masterVersion != versionShouldHave) {
                    if (gonnaDownloadMaster) {
                        try {
                            tableDownloaded = client.downloadAllMastersForcedly() > 0;
                        } catch (Exception e) {
                            CreamToolkit.logMessage e
                            indicator.setMessage(res.getString("MasterDownloadFailed"))
                            SsmpLog.report10011(e)
                        }
                    }

                    if (gonnaDownloadProgram) {
                        origMessage = indicator.getMessage()
                        StateMachine.instance.setEventProcessEnabled false
                        indicator.setMessage res.getString('ProgramDownloading')
                        downloadProgramFiles()
                        downloadSomeFiles = client.getNumberOfFilesGotOrMoved() > 0
                    }
                    shrinkLogFiles()
                    deleteOutdatedData()
                    param.masterVersion = versionShouldHave
                }
            }

            if (tableDownloaded)
                SsmpLog.report10012()
            if (downloadSomeFiles)
                SsmpLog.report10013(Client.instance.scriptFiles)

            if (tableDownloaded || downloadSomeFiles)
                indicator.setMessage res.getString('UpdateCompleted')
            else if (origMessage != null)
                indicator.setMessage(origMessage)

            afterDownloadingMaster(tableDownloaded, downloadSomeFiles)

            StateMachine.instance.setEventProcessEnabled true
            gonnaDownloadMaster = gonnaDownloadProgram = false

            if (!tableDownloaded && !downloadSomeFiles) // 如果沒有版本相異的主檔下傳，也沒有任何程序文件下傳，
                sleep 55000                             // 為了避免不斷頻繁檢查, 所以睡一會兒

        } catch (Exception e) {
            CreamToolkit.logMessage e
            SsmpLog.report10011(e)
        }}
    }

    public static void afterDownloadingMaster(boolean downloadSomeTables, boolean downloadSomeFiles) {
        def unix = System.getProperty('os.name') =~ /Linux|Mac/

        if (downloadSomeFiles) {
            //Bruce/20081212/ 如果有程序文件下傳，必須在下次啟動的時候重新做PLU object DB，
            // 否則有可能會由於class serialVerionUID不一樣導致class incompatible的問題
            CreamCache.instance.clearPLUObjectDB()

            if (unix)     // 如果有程序文件下傳，OS reboot on Linux
                CreamToolkit.reboot()
            else          // 如果有程序文件下傳，AP exit on Windows
                CreamToolkit.stopPos 0
        }

        if (!downloadSomeTables)
            return

        String action = Param.instance.actionAfterMasterDownload
        if (action.contains("Reboot") || action.contains("reboot")) {
            if (unix)     // 主檔下傳後，OS reboot on Linux
                CreamToolkit.reboot()
            else          // 主檔下傳後，AP exit on Windows
                CreamToolkit.stopPos 0
        } else if (action.contains("Restart") || action.contains("restart")) {
            CreamToolkit.stopPos 0
        }
    }

    public void close() {
        interrupt()
        instance = null
    }

//    public void repairAndOptimizeTables() {
//        try {
//            // Find mysqlcheck...
//            String mysqlCheckPath
//            if (new File("/usr/bin/mysqlcheck").exists())
//                mysqlCheckPath = "/usr/bin/mysqlcheck"
//            else if (new File("c:\\Program Files\\MySQL\\bin\\mysqlcheck.exe")
//                    .exists())
//                mysqlCheckPath = "c:\\Program Files\\MySQL\\bin\\mysqlcheck.exe"
//            else if (new File("c:\\MySQL\\bin\\mysqlcheck.exe").exists())
//                mysqlCheckPath = "c:\\MySQL\\bin\\mysqlcheck.exe"
//            else
//                mysqlCheckPath = "mysqlcheck"
//
//            // Run mysqlcheck and write log...
//            String[] cmdList = {
//                    " -v -r cream tranhead trandetail z shift depsales daishousales", // repair
//                    " -v -o cream tranhead trandetail z shift depsales daishousales" } // optimize
//            SimpleDateFormat dateFormatter = CreamCache.getInstance()
//                    .getDateTimeFormate()
//            for (int i = 0 i < cmdList.length i++) {
//                Process p = Runtime.getRuntime().exec(
//                        mysqlCheckPath + cmdList[i])
//                InputStream stdout = p.getInputStream()
//                String logFilePath = GetProperty.getLogFile("")
//                if (!logFilePath.equals("")) {
//                    OutputStream logFile = new BufferedOutputStream(
//                            new FileOutputStream(logFilePath, true))
//                    String dateString = dateFormatter
//                            .format(new java.util.Date())
//                    logFile.write(dateString.getBytes(CreamToolkit
//                            .getEncoding()))
//                    logFile.write(' ')
//                    logFile.write((mysqlCheckPath + cmdList[i])
//                            .getBytes(CreamToolkit.getEncoding()))
//                    logFile.write('\n')
//                    int c
//                    while ((c = stdout.read()) != -1)
//                        logFile.write(c)
//                    logFile.close()
//                    stdout.close()
//                }
//                p.waitFor()
//            }
//            CreamToolkit.logMessage("repairAndOptimizeTables success.........")
//        } catch (FileNotFoundException e) {
//        } catch (IOException e) {
//        } catch (InterruptedException e) {
//        }
//    }

//    /**
//     * 将数据库备份到指定的机器上。
//     *
//     * @return true if sucess.
//     */
//    public boolean backUp() {
//        boolean isOK = false
//
//        // 从"MySQLDataDirectory"和"DatabaseURL"取得资料存放的目录
//        String localDir = GetProperty.getMySQLDataDirectory("")
//        if (localDir.equals(""))
//            return true
//        if (!localDir.endsWith(File.separator))
//            localDir += File.separator
//        String url = GetProperty.getDatabaseURL("")
//        localDir += url.substring(url.lastIndexOf('/') + 1)
//        if (!localDir.endsWith(File.separator))
//            localDir += File.separator
//
//        // 从"MySQLRemoteBackupDirectory"取得远端资料备份的目录(rsync server directory)
//        String backDir = GetProperty.getMySQLRemoteBackupDirectory("")
//        if (backDir.equals(""))
//            return true
//        if (!backDir.endsWith(File.separator))
//            backDir += File.separator
//
//        String posNo = GetProperty.getTerminalNumber("")
//        try {
//            String commandLine = "sudo rsync -r " + localDir + " " + backDir + posNo
//                    + File.separator
//            Process p = Runtime.getRuntime().exec(commandLine)
//            int exitCode = p.waitFor()
//            isOK = (exitCode == 0)
//        } catch (IOException e) {
//            e.printStackTrace(CreamToolkit.getLogger())
//        } catch (InterruptedException e) {
//            e.printStackTrace(CreamToolkit.getLogger())
//        }
//        return isOK
//    }
//
//    /**
//     * 从备份恢复数据库。
//     *
//     * @return true if sucess.
//     */
//    public boolean recoverFromBackup() {
//        boolean isOK = false
//
//        // 从"MySQLDataDirectory"和"DatabaseURL"取得资料存放的目录
//        String localDir = GetProperty.getMySQLDataDirectory("")
//        if (localDir.equals(""))
//            return false
//        if (!localDir.endsWith(File.separator))
//            localDir += File.separator
//        String url = GetProperty.getDatabaseURL("")
//        localDir += url.substring(url.lastIndexOf('/') + 1)
//        if (!localDir.endsWith(File.separator))
//            localDir += File.separator
//
//        // 从"MySQLRemoteBackupDirectory"取得远端资料备份的目录(rsync server directory)
//        String backDir = GetProperty.getMySQLRemoteBackupDirectory("")
//        if (backDir.equals(""))
//            return false
//        if (!backDir.endsWith(File.separator))
//            backDir += File.separator
//
//        String posNo = GetProperty.getTerminalNumber("")
//
//        try {
//            String commandLine = "sudo rsync -r " + backDir + posNo + "/" + " "
//                    + localDir
//            Process p = Runtime.getRuntime().exec(commandLine)
//            int exitCode = p.waitFor()
//            isOK = (exitCode == 0)
//        } catch (IOException e) {
//            e.printStackTrace(CreamToolkit.getLogger())
//        } catch (InterruptedException e) {
//            e.printStackTrace(CreamToolkit.getLogger())
//        }
//        return isOK
//    }
}
