package hyi.cream.inline;

import hyi.cream.dac.*;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.periodical.PeriodicalUtils;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import hyi.cream.util.DbConnection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

class PostProcessorImplB extends PostProcessorAdapter {
    transient public static final String VERSION = "1.8";

    static final int DATE_ACCDATE = 200;
    static final int DATE_BIZDATE = DATE_ACCDATE + 1;
    static final int DATE_INFDATE = DATE_ACCDATE + 2;
    static private DateFormat yyyy_MM_dd = CreamCache.getInstance()
            .getDateFormate();
    static final String POS_AMOUNT = "posamount";
    static final String STORE_AMOUNT = "storeamount";
    static final Integer integer0 = new Integer(0);
    static Object lockObject = new Object();
    private static final NumberFormat seqFormat = new DecimalFormat("0000");

    static private Map zSeqToAccountingDateMap = new HashMap();
    private static SimpleDateFormat yMMDD = new SimpleDateFormat("yMMDD");
    private static final SimpleDateFormat dfyyyyMMddHHmmss = CreamCache
            .getInstance().getDateTimeFormate();

    // /**
    // * 根据交易时间决定营业日期。
    // *
    // * @param date 交易时间
    // * @return 返回营业日期
    // */
    // private Date getBusinessDate(Date date) {
    // String changeTime = "22:59:59";
    // Map value = query("SELECT changetime FROM calendar WHERE dateType =
    // '0'");
    // if (value != null && value.containsKey("changetime")) {
    // changeTime = (String)value.get("changetime").toString();
    // if (changeTime.trim().length() == 0 || changeTime.equals("00:00:00")) {
    // changeTime = "22:59:59";
    // }
    // }
    // String realTime = new SimpleDateFormat("HH:mm:ss").format(date);
    // Calendar cal = Calendar.getInstance();
    // cal.setTime(date);
    // if (realTime.compareTo(changeTime) > 0) {
    // cal.add(Calendar.DATE, 1);
    // }
    // cal.set(Calendar.HOUR, 0);
    // cal.set(Calendar.MINUTE, 0);
    // cal.set(Calendar.SECOND, 0);
    // return cal.getTime();
    // }

    /**
     * 由Z帐序号决定会计日期。
     *
     * @param posNumber
     *            POS机号
     * @param zSeq
     *            Z帐序号
     * @return 返回会计日期
     */
    private Date getAccountingDate(DbConnection connection, int posNumber,
            int zSeq) throws SQLException {
        Integer key = new Integer(posNumber * 1000000 + zSeq);
        Date accountingDate = (Date) zSeqToAccountingDateMap.get(key);
        if (accountingDate != null)
            return accountingDate;
        Map value = query(connection,
                "SELECT accountdate FROM posul_z WHERE posNumber=" + posNumber
                        + " AND zSequenceNumber=" + zSeq);
        if (value != null && value.containsKey("accountdate")) {
            accountingDate = (Date) value.get("accountdate");
            zSeqToAccountingDateMap.put(key, accountingDate);
            return accountingDate;
        }
        // return getAccountingDate(new Date());
        return getAccountingDate(); // get accountDate from Table calendar
    }

    /**
     * - 由Z帐序号或交易时间决定会计日期。
     *
     * @param posNumber
     *            POS机号
     * @param zSeq
     *            Z帐序号
     * @param transactionDate
     *            交易时间
     * @return 返回会计日期
     */
    private Date getAccountingDate(DbConnection connection, int posNumber,
            int zSeq, Date transactionDate) throws SQLException {
        Integer key = new Integer(posNumber * 1000000 + zSeq);
        Date accountingDate = (Date) zSeqToAccountingDateMap.get(key);
        if (accountingDate != null)
            return accountingDate;
        Map value = query(connection,
                "SELECT accountdate FROM posul_z WHERE posNumber=" + posNumber
                        + " AND zSequenceNumber=" + zSeq);

        if (value != null && value.containsKey("accountdate")) {
            accountingDate = (Date) value.get("accountdate");
            zSeqToAccountingDateMap.put(key, accountingDate);
            return accountingDate;
        }
        // return getAccountingDate(transactionDate);
        return getAccountingDate(); // get accountDate from Table calendar
    }

    /**
     * 取得系统业务当前的会计日
     *
     * @return Date 系统业务当前的会计日
     * @version 2004-08-25 by James
     */
    private Date getAccountingDate() {
        String storeID = getStoreID();
        DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        java.util.Date accDate = null;
        String select = "SELECT currentDate FROM calendar";
        String where = " WHERE storeID = '" + storeID + "' AND dateType = '1'";
        Server.log(select + where); // only for debug
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(select + where);
            if (resultSet.next()) {
                accDate = resultSet.getDate(1);
            }
            System.out.println(" getAccountingDate() = " + accDate);
        } catch (Exception e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace(Server.getLogger());
            }
        }
        if (accDate == null) {
            accDate = new java.util.Date();
            Server
                    .log("[Exception] Can not get account date in the table of calendar!");
        }
        return accDate;
    }

    public boolean isDateShiftRunning(int dateType) {
        String type = "";
        switch (dateType) {
        case DATE_ACCDATE:
            type = "1";
            break;
        case DATE_BIZDATE:
            type = "0";
            break;
        case DATE_INFDATE:
            type = "4";
            break;
        default:
            return false;
        }
        String storeID = getStoreID();// CreamToolkit.getConfiguration().getProperty("StoreID");
        DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String select = "SELECT * FROM calendar ";
        String where = " WHERE storeID = '" + storeID + "' AND dateType = '"
                + type + "'";
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(select + where);
            // System.out.println(select + where);
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            if (connection == null) {
                // BRUCE> THIS IS VERY SERIOUS PROBLEM, SHUT INLINE SERVER DOWN,
                // AND LET IT RESTART AGAIN!!!
                Server
                        .log("Cannot get database connection, server is stopped.");
                System.exit(1);
            }
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace(Server.getLogger());
            }
            return false;
        }
        HashMap newMap = null;
        try {
            if (resultSet.next()) {
                newMap = new HashMap();
                ResultSetMetaData metaData = resultSet.getMetaData();
                int t = 0;
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object o = resultSet.getObject(i);
                    if (o != null) {
                        t = metaData.getColumnType(i);
                        if (t == Types.NUMERIC || t == Types.DECIMAL) {
                            o = new HYIDouble(((BigDecimal) o).doubleValue());
                        }
                    }
                    newMap.put(metaData.getColumnName(i), o);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
        }
        String running = "";
        if (newMap == null)
            return false;
        if (newMap.get("scEodResult") instanceof String)
            running = (String) newMap.get("scEodResult");
        if (running.compareTo("1") == 0)
            return true;
        return false;
    }

    public boolean isDateShiftRunningOrFailed(int dateType) {
        String type = "";
        switch (dateType) {
        case DATE_ACCDATE:
            type = "1";
            break;
        case DATE_BIZDATE:
            type = "0";
            break;
        case DATE_INFDATE:
            type = "4";
            break;
        default:
            return false;
        }
        String storeID = getStoreID();// CreamToolkit.getConfiguration().getProperty("StoreID");
        DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String select = "SELECT * FROM calendar ";
        String where = " WHERE storeID = '" + storeID + "' AND dateType = '"
                + type + "'";
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(select + where);
            // System.out.println(select + where);
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            if (connection == null) {
                // BRUCE> THIS IS VERY SERIOUS PROBLEM, SHUT INLINE SERVER DOWN,
                // AND LET IT RESTART AGAIN!!!
                Server
                        .log("Cannot get database connection, server is stopped.");
                System.exit(1);
            }
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace(Server.getLogger());
            }
            return false;
        }
        HashMap newMap = null;
        try {
            if (resultSet.next()) {
                newMap = new HashMap();
                ResultSetMetaData metaData = resultSet.getMetaData();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object obj = metaData.getColumnName(i);
                    newMap.put(obj, resultSet.getObject(i));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
        }
        String running = "";
        if (newMap == null)
            return false;
        if (newMap.get("scEodResult") instanceof String)
            running = (String) newMap.get("scEodResult");
        if (running.equals("1") || running.equals("2"))
            return true;
        return false;
    }

    @Override
    public boolean afterReceivingZEx(Object[] zExex) {
        return super.afterReceivingZEx(zExex);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public synchronized boolean afterReceivingShiftReport(ShiftReport shift) {
        // 根据shift的结束日期来决定会计日期
        // Date accountingDate =
        // getAccountingDate(shift.getSignOffSystemDateTime());
        Date accountingDate = getAccountingDate(); // get accountDate from
        // Table calendar
        shift.setAccountingDate(accountingDate);

        DbConnection connection = null;
        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();
            shift.deleteByPrimaryKey(connection);

            shift.insert(connection, false);
            connection.commit();

            Server.log("Shift inserted (date="
                    + yyyy_MM_dd.format(shift.getAccountingDate()) + ",pos="
                    + shift.getTerminalNumber() + ",z="
                    + shift.getZSequenceNumber() + ",shift="
                    + shift.getSequenceNumber() + ").");
        } catch (SQLException e) {
            Server.log("Shift inserted failed (pos="
                    + shift.getTerminalNumber() + ",z="
                    + shift.getZSequenceNumber() + ",shift="
                    + shift.getSequenceNumber() + ").");
            e.printStackTrace(Server.getLogger());
            return false;

        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return true;
    }

    /**
     * Check if the given date's accdayrpt has no records, if yes, copy from
     * yesterday's record, or from commdl_accitem.
     *
     * @author bruce
     */
    private boolean generateAccdayrptRecords(DbConnection connection, Date date)
            throws SQLException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        String yesterday = yyyy_MM_dd.format(cal.getTime());
        String today = yyyy_MM_dd.format(date);

        Map value = null;
        value = query(connection, "SELECT COUNT(*) AS cnt FROM accdayrpt "
                + "WHERE accountDate='" + today + "'");
        if (value != null && value.containsKey("cnt")) {
            if (CreamToolkit.retrieveIntValue(value.get("cnt")) > 0)
                return true; // 表示今天的记录已经存在
        }

        value = query(connection,
                "SELECT COUNT(*) AS cnt FROM commdl_accitem WHERE updatebegindate = '"
                        + today + "'");
        if (value != null && value.containsKey("cnt")
                && CreamToolkit.retrieveIntValue(value.get("cnt")) > 0) {
            // 表示今天的记录已经存在
            String accitemDateString = today;

            value = query(connection,
                    "SELECT MAX(sequenceNumber) dt FROM commdl_accitem WHERE updateBeginDate='"
                            + accitemDateString + "'");
            Integer sequenceNumber = (Integer) value.get("dt");
            executeQuery(
                    connection,
                    "INSERT INTO accdayrpt ("
                            + "storeID, accountDate, accountNumber, accountName, posNumber,"
                            + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                            + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                            + "updateUserID, updateDate) "
                            + "SELECT storeID, '"
                            + today
                            + "', accountNumber, accountName, posNumber,"
                            + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                            + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                            + "updateUserID, updateDate "
                            + "FROM commdl_accitem "
                            + "WHERE updateBeginDate='" + accitemDateString
                            + "' AND sequenceNumber=" + sequenceNumber);
            executeQuery(connection,
                    "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                            + "WHERE accountDate='" + today + "'");
        } else {

            value = query(connection, "SELECT COUNT(*) AS cnt FROM accdayrpt "
                    + "WHERE accountDate='" + yesterday + "'");
            Integer yesterdayCount = integer0;
            if (value != null && value.containsKey("cnt"))
                yesterdayCount = new Integer(CreamToolkit
                        .retrieveIntValue(value.get("cnt")));

            if (yesterdayCount.compareTo(integer0) > 0) { // 表示有昨天的记录
                try {
                    executeQuery(
                            connection,
                            "CREATE TEMPORARY TABLE tmp ("
                                    + "storeID varchar(6) NOT NULL DEFAULT '' ,"
                                    + "accountDate date NOT NULL DEFAULT '0000-00-00' ,"
                                    + "accountNumber varchar(6) NOT NULL DEFAULT '' ,"
                                    + "accountName varchar(30) ,"
                                    + "posNumber tinyint(3) unsigned NOT NULL DEFAULT '0' ,"
                                    + "displayType char(2) ,"
                                    + "displaySequence varchar(6) ,"
                                    + "posAmount decimal(12,2) ,"
                                    + "storeAmount decimal(12,2) ,"
                                    + "storeCount tinyint(3) unsigned ,"
                                    + "updateType varchar(4) ,"
                                    + "calType varchar(4) ,"
                                    + "uploadType char(1) ,"
                                    + "itemNumber varchar(10) ,"
                                    + "payID char(2) ,"
                                    + "dataSource varchar(50) ,"
                                    + "updateUserID varchar(8) ,"
                                    + "updateDate datetime ,"
                                    + "turnMfg varchar(2) NOT NULL DEFAULT '0'"
                                    + ")  ENGINE=InnoDB DEFAULT CHARSET=utf8;");
                    executeQuery(
                            connection,
                            "INSERT INTO tmp ("
                                    + "storeID, accountDate, accountNumber, accountName, posNumber,"
                                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                                    + "updateUserID, updateDate, turnMfg ) "
                                    + "SELECT storeID, '"
                                    + today
                                    + "', accountNumber, accountName, posNumber,"
                                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                                    + "updateUserID, updateDate, turnMfg "
                                    + "FROM accdayrpt " + "WHERE accountDate='"
                                    + yesterday + "'");
                    executeQuery(connection,
                            "INSERT INTO accdayrpt SELECT * FROM tmp");
                    executeQuery(connection,
                            "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                                    + "WHERE accountDate='" + today + "'");
                } catch (SQLException e) {
                    e.printStackTrace(Server.getLogger());
                    throw e;
                } finally {
                    try {
                        executeQuery(connection, "DROP TABLE tmp");
                    } catch (Exception e) {
                    }
                }

            } else { // accdayrpt中没有昨天的记录，从commdl_accitem取

                value = query(connection,
                        "SELECT MAX(updatebegindate) dt FROM commdl_accitem");
                if (value == null || !value.containsKey("dt")) {
                    Server.log("Cannot create accdayrpt records.");
                    return false;
                }
                Date accitemDate = (Date) value.get("dt");
                String accitemDateString = yyyy_MM_dd.format(accitemDate);

                value = query(connection,
                        "SELECT MAX(sequenceNumber) dt FROM commdl_accitem WHERE updateBeginDate='"
                                + accitemDateString + "'");
                Integer sequenceNumber = (Integer) value.get("dt");

                executeQuery(
                        connection,
                        "INSERT INTO accdayrpt ("
                                + "storeID, accountDate, accountNumber, accountName, posNumber,"
                                + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                                + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                                + "updateUserID, updateDate) "
                                + "SELECT storeID, '"
                                + today
                                + "', accountNumber, accountName, posNumber,"
                                + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                                + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                                + "updateUserID, updateDate "
                                + "FROM commdl_accitem "
                                + "WHERE updateBeginDate='" + accitemDateString
                                + "' AND sequenceNumber=" + sequenceNumber);
                executeQuery(connection,
                        "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                                + "WHERE accountDate='" + today + "'");
            }
        }

        // 避免posAmount和storeAmount有空值
        executeQuery(connection, "UPDATE accdayrpt SET storeAmount=0 "
                + "WHERE accountDate='" + today + "' AND storeAmount IS NULL");
        executeQuery(connection, "UPDATE accdayrpt SET posAmount=0 "
                + "WHERE accountDate='" + today + "' AND posAmount IS NULL");

        return true;
    }

    public boolean afterReceivingZReport(ZReport z) {
        return afterReceivingZReport(z, new StringBuffer());
    }

    public boolean afterReceivingZReport(ZReport z, StringBuffer accdateStrBuf) {
        boolean returnValue = true;
        DbConnection connection = null;

        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();
            Date accountingDate = z.getAccountingDate();
            if (accountingDate == null
                    || ((yyyy_MM_dd.format(accountingDate)).startsWith("197"))) {
                // 重传z时如果没有设置accoutdate,就得看sc是否已存在,如果存在,accountdate使用原来的
                ZReport oldZ = ZReport.queryByKey(connection, z.getStoreNumber(), z.getTerminalNumber(), z.getSequenceNumber());
                if (oldZ != null) {
                    accountingDate = oldZ.getAccountingDate();
                } else {
                    accountingDate = getAccountingDate();  // get accountDate from Table calendar
                }
            }
            z.setAccountingDate(accountingDate);
            String accountingDateString = yyyy_MM_dd.format(accountingDate);
            if (accdateStrBuf != null) {
                accdateStrBuf.append(accountingDateString);
            }

            // 1. insert/update z --
            if (z.deleteByPrimaryKey(connection))
                Server.log("Delete old Z (date=" + accountingDateString +
                    ",pos=" + z.getTerminalNumber() +
                    ",z=" + z.getSequenceNumber() + ").");

            z.insert(connection, false);
            Server.log("Z inserted (date=" + accountingDateString + ",pos="
                    + z.getTerminalNumber() + ",z=" + z.getSequenceNumber()
                    + ").");

            // 2. update updateAccountDateByZNo --
            updateAccountDateByZNo(connection, z, accountingDateString);

            // 检查或生成该会计日期的accdayrpt记录
            generateAccdayrptRecords(connection, accountingDate);

            // Update posaccountdate
            HashMap newRec = new HashMap();
            newRec.put("storeID", z.getStoreNumber().trim());
            newRec.put("accountDate", z.getAccountingDate());
            newRec.put("posNumber", z.getTerminalNumber());
            newRec.put("zSequenceNumber", z.getSequenceNumber());
            try {
                insert(connection, newRec, "posaccountdate");
            } catch (Exception e) {
                e.printStackTrace();
                e.printStackTrace(Server.getLogger());
            }
            synchronized (lockObject) {
                Iterator iter = ZReport.queryByDateTime(connection,
                        accountingDate);
                if (iter == null)
                    return returnValue;
                // 先避免posAmount和storeAmount有空值
                executeQuery(connection, "UPDATE accdayrpt SET storeAmount=0 "
                        + "WHERE accountDate='" + accountingDateString
                        + "' AND storeAmount IS NULL");
                executeQuery(connection, "UPDATE accdayrpt SET posAmount=0 "
                        + "WHERE accountDate='" + accountingDateString
                        + "' AND posAmount IS NULL");
                // 把storeAmount先计成与原posAmount的差额(为了保留用户原来对storeAmount的修改)
                executeQuery(connection,
                        "UPDATE accdayrpt SET storeAmount=storeAmount-posAmount "
                                + "WHERE accountDate='" + accountingDateString
                                + "'");
                // 然后把posAmount归零
                executeQuery(connection, "UPDATE accdayrpt SET posAmount=0 "
                        + "WHERE accountDate='" + accountingDateString + "'");
                // 开始针对每一个该日的Z帐进行过转到accdayrpt的计算
                while (iter.hasNext()) {
                    z = (ZReport) iter.next();

                    // 更新现金日报表
                    // Primary Key: storeID + accountDate + accountNumber +
                    // posNumber
                    String storeID = z.getStoreNumber();
                    int posNumber = z.getTerminalNumber().intValue();
                    SimpleDateFormat format = CreamCache.getInstance()
                            .getDateFormate();
                    String accDate = format.format(z.getAccountingDate())
                            .toString();
                    Statement statement = null;
                    ResultSet resultSet = null;
                    ArrayList table = null;
                    String select = "SELECT * FROM accdayrpt ";
                    String updateString = "UPDATE accdayrpt SET ";
                    String where = " WHERE storeID = '" + storeID
                            + "' AND accountDate = '" + accDate + "'";// AND
                    // posNumber
                    // = '"
                    // +
                    // posNumber
                    // +
                    // "'";
                    // CreamToolkit.logMessage("" + select + where);
                    try {
                        statement = connection.createStatement();
                        resultSet = statement.executeQuery(select + where);
                        table = constructFromResultSet(resultSet);
                        if (table.isEmpty()) {
                            Server
                                    .log("afterReceivingZReport(): Cannot find record in accdayrpt");
                            Server.log("Query : " + select + where);
                            returnValue = false;
                        }
                    } catch (SQLException e) {
                        Server
                                .log("afterReceivingZReport(): Cannot find record in accdayrpt");
                        Server.log("SQLStatement: " + select + where);
                        e.printStackTrace(Server.getLogger());
                        returnValue = false;
                    } finally {
                        try {
                            if (statement != null)
                                statement.close();
                        } catch (SQLException e) {
                            e.printStackTrace(Server.getLogger());
                        }
                        if (!returnValue) {
                            return false;
                        }
                    }

                    Map zRecord = z.getValues();
                    HashMap map = new HashMap();

                    // Bruce/2002-04-10
                    // map.put("001", z.getTransactionCount());
                    map.put("001", z.getCustomerCount());

                    map.put("002", z.getGrossSales());
                    map.put("004", z.getMixAndMatchTotalAmount());
                    map.put("006", z.getNetSalesTotalAmount());
                    map.put("201", z.getVoucherAmount());
                    map.put("202", z.getPreRcvAmount()); // 期刊预订
                    map.put("203", z.getPreRcvDetailAmount1()); // 团购结算
                    map.put("300", z.getDaiShouAmount2()); // Bruce/20030819
                    // 公共事业费代收金额
                    map.put("601", z.getSpillAmount());
                    /*
                     * map.put("102A", z.getTaxAmount1()); map.put("102B",
                     * z.getTaxAmount2()); map.put("102C", z.getTaxAmount3());
                     * map.put("102D", z.getTaxAmount4());
                     */
                    // map.put("101A", z.getTaxedAmount());
                    // map.put("101B", z.getNoTaxedAmount());
                    //
                    Iterator itr = table.iterator();
                    while (itr.hasNext()) {
                        HashMap accdayrptRecord = (HashMap) itr.next();
                        String accountNumber = (String) accdayrptRecord
                                .get("accountnumber");
                        // CreamToolkit.logMessage("" + "accountnumber=" +
                        // accountNumber);
                        String payID = (String) accdayrptRecord.get("payid");
                        if (payID == null || payID.length() == 0)
                            payID = "-1";
                        // CreamToolkit.logMessage("" + "payid=" + payID);
                        int cposNumber = Integer.parseInt(accdayrptRecord.get(
                                "posnumber").toString());
                        // CreamToolkit.logMessage("" + "posNumber=" +
                        // cposNumber);
                        if (cposNumber == posNumber) {
                            if (map.containsKey(accountNumber)) {
                                HYIDouble amt = (HYIDouble) accdayrptRecord
                                        .get(POS_AMOUNT);
                                HYIDouble mapAmt = new HYIDouble(0);
                                if (map.get(accountNumber) instanceof Integer) {
                                    Integer in = (Integer) map
                                            .get(accountNumber);
                                    mapAmt = new HYIDouble(in.intValue());
                                } else if (map.get(accountNumber) instanceof HYIDouble) {
                                    mapAmt = (HYIDouble) map.get(accountNumber);
                                }
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Original amount = " + amt );
                                if (amt == null)
                                    amt = new HYIDouble(0);
                                amt = amt.addMe(mapAmt);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Additional amount = " + mapAmt);
                                accdayrptRecord.put(POS_AMOUNT, amt);
                                HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                        .get(STORE_AMOUNT);
                                if (sAmt == null) {
                                    accdayrptRecord.put(STORE_AMOUNT, amt);
                                } else {
                                    accdayrptRecord.put(STORE_AMOUNT, sAmt
                                            .addMe(mapAmt));
                                }
                            }
                        } else if (cposNumber == 0) {
                            if (map.containsKey(accountNumber)) {
                                HYIDouble amt = (HYIDouble) accdayrptRecord
                                        .get(POS_AMOUNT);
                                HYIDouble mapAmt = new HYIDouble(0);
                                if (map.get(accountNumber) instanceof Integer) {
                                    Integer in = (Integer) map
                                            .get(accountNumber);
                                    mapAmt = new HYIDouble(in.intValue());
                                } else if (map.get(accountNumber) instanceof HYIDouble) {
                                    mapAmt = (HYIDouble) map.get(accountNumber);
                                }
                                if (amt == null)
                                    amt = new HYIDouble(0);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Original amount = " + amt );
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Additional amount = " + mapAmt);
                                amt = amt.addMe(mapAmt);
                                accdayrptRecord.put(POS_AMOUNT, amt);
                                HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                        .get(STORE_AMOUNT);
                                if (sAmt == null) {
                                    accdayrptRecord.put(STORE_AMOUNT, amt);
                                } else {
                                    accdayrptRecord.put(STORE_AMOUNT, sAmt
                                            .addMe(mapAmt));
                                }
                            } else if (accountNumber.startsWith("3")) {// 代收

                                HYIDouble daiShouTotalAmount = (HYIDouble) accdayrptRecord
                                        .get(POS_AMOUNT);
                                if (daiShouTotalAmount == null)
                                    daiShouTotalAmount = new HYIDouble(0);

                                HYIDouble amt;
                                if (accountNumber.equals("300")) { // Bruce/20030819
                                    // 公共事业费代收金额
                                    amt = z.getDaiShouAmount2();
                                } else {
                                    if (payID.startsWith("0")
                                            && payID.length() > 1)
                                        payID = payID.substring(1);
                                    amt = (HYIDouble) zRecord
                                            .get("daiShouDetailAmount" + payID);
                                }

                                if (amt == null)
                                    amt = new HYIDouble(0);
                                daiShouTotalAmount = daiShouTotalAmount
                                        .addMe(amt);

                                accdayrptRecord.put(POS_AMOUNT,
                                        daiShouTotalAmount);
                                HYIDouble storeAmt = (HYIDouble) accdayrptRecord
                                        .get(STORE_AMOUNT);
                                if (storeAmt == null) {
                                    accdayrptRecord.put(STORE_AMOUNT,
                                            daiShouTotalAmount);
                                } else {
                                    accdayrptRecord.put(STORE_AMOUNT, storeAmt
                                            .addMe(amt));
                                }
                            } else if (accountNumber.startsWith("4")) {// 代付
                                if (payID.startsWith("0") && payID.length() > 1)
                                    payID = payID.substring(1);
                                HYIDouble amt = (HYIDouble) zRecord
                                        .get("daiFuDetailAmount" + payID);
                                if (amt == null)
                                    amt = new HYIDouble(0);
                                HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                        .get(POS_AMOUNT);
                                if (amt2 == null)
                                    amt2 = new HYIDouble(0);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Original amount = " + amt2);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Additional amount = " + amt);
                                amt2 = amt2.addMe(amt);
                                accdayrptRecord.put(POS_AMOUNT, amt2);
                                HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                        .get(STORE_AMOUNT);
                                if (sAmt == null) {
                                    accdayrptRecord.put(STORE_AMOUNT, amt2);
                                } else {
                                    accdayrptRecord.put(STORE_AMOUNT, sAmt
                                            .addMe(amt));
                                }
                            } else if (accountNumber.startsWith("5")) {// 支付
                                if (payID.length() < 2)
                                    payID += "0";
                                HYIDouble amt = (HYIDouble) zRecord.get("pay"
                                        + payID + "Amount");
                                if (amt == null)
                                    amt = new HYIDouble(0);
                                HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                        .get(POS_AMOUNT);
                                if (amt2 == null)
                                    amt2 = new HYIDouble(0);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Original amount = " + amt2);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Additional amount = " + amt);
                                amt2 = amt2.addMe(amt);
                                accdayrptRecord.put(POS_AMOUNT, amt2);
                                HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                        .get(STORE_AMOUNT);
                                if (sAmt == null) {
                                    accdayrptRecord.put(STORE_AMOUNT, amt2);
                                } else {
                                    accdayrptRecord.put(STORE_AMOUNT, sAmt
                                            .addMe(amt));
                                }
                            } else if (accountNumber.startsWith("7")) {// Paid-In
                                int id = Integer.parseInt(payID);
                                if (id < 0)
                                    continue;
                                HYIDouble amt = (HYIDouble) zRecord
                                        .get("paidInDetailAmount" + id);
                                if (amt == null)
                                    amt = new HYIDouble(0);
                                HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                        .get(POS_AMOUNT);
                                if (amt2 == null)
                                    amt2 = new HYIDouble(0);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Original amount = " + amt2);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Additional amount = " + amt);
                                amt2 = amt2.addMe(amt);
                                accdayrptRecord.put(POS_AMOUNT, amt2);
                                HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                        .get(STORE_AMOUNT);
                                if (sAmt == null) {
                                    accdayrptRecord.put(STORE_AMOUNT, amt2);
                                } else {
                                    accdayrptRecord.put(STORE_AMOUNT, sAmt
                                            .addMe(amt));
                                }
                            } else if (accountNumber.startsWith("8")) {// Paid-Out
                                int id = Integer.parseInt(payID);
                                if (id < 0)
                                    continue;
                                HYIDouble amt = (HYIDouble) zRecord
                                        .get("paidOutDetailAmount"
                                                + Integer.parseInt(payID));
                                if (amt == null)
                                    amt = new HYIDouble(0);
                                HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                        .get(POS_AMOUNT);
                                if (amt2 == null)
                                    amt2 = new HYIDouble(0);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Original amount = " + amt2);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Additional amount = " + amt);
                                amt2 = amt2.addMe(amt);
                                accdayrptRecord.put(POS_AMOUNT, amt2);
                                HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                        .get(STORE_AMOUNT);
                                if (sAmt == null) {
                                    accdayrptRecord.put(STORE_AMOUNT, amt2);
                                } else {
                                    accdayrptRecord.put(STORE_AMOUNT, sAmt
                                            .addMe(amt));
                                }
                            } else if (accountNumber.startsWith("9")) {// 门市费用
                                int id = Integer.parseInt(payID);
                                if (id < 0)
                                    continue;
                                HYIDouble amt = (HYIDouble) zRecord
                                        .get("paidOutDetailAmount"
                                                + Integer.parseInt(payID));
                                if (amt == null)
                                    amt = new HYIDouble(0);
                                HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                        .get(POS_AMOUNT);
                                if (amt2 == null)
                                    amt2 = new HYIDouble(0);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Original amount = " + amt2);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Additional amount = " + amt);
                                amt2 = amt2.addMe(amt);
                                accdayrptRecord.put(POS_AMOUNT, amt2);
                                HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                        .get(STORE_AMOUNT);
                                if (sAmt == null) {
                                    accdayrptRecord.put(STORE_AMOUNT, amt2);
                                }
                            } else if (accountNumber.startsWith("101")) {
                                int id = Integer.parseInt(payID);
                                if (id < 0)
                                    continue;
                                HYIDouble amt = (HYIDouble) zRecord
                                        .get("netSaleTax" + id + "Amount");
                                // CreamToolkit.logMessage("" + accountNumber +
                                // "zAmount = " + amt);
                                if (amt == null)
                                    amt = new HYIDouble(0);
                                HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                        .get(POS_AMOUNT);
                                if (amt2 == null)
                                    amt2 = new HYIDouble(0);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Original amount = " + amt2);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Additional amount = " + amt);
                                amt2 = amt2.addMe(amt);
                                accdayrptRecord.put(POS_AMOUNT, amt2);
                                HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                        .get(STORE_AMOUNT);
                                if (sAmt == null) {
                                    accdayrptRecord.put(STORE_AMOUNT, amt2);
                                } else {
                                    accdayrptRecord.put(STORE_AMOUNT, sAmt
                                            .addMe(amt));
                                }
                            } else if (accountNumber.startsWith("102")) {
                                int id = Integer.parseInt(payID);
                                if (id < 0)
                                    continue;
                                HYIDouble amt = (HYIDouble) zRecord
                                        .get("taxAmount" + id);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // "zAmount = " + amt);
                                if (amt == null)
                                    amt = new HYIDouble(0);
                                HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                        .get(POS_AMOUNT);
                                if (amt2 == null)
                                    amt2 = new HYIDouble(0);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Original amount = " + amt2);
                                // CreamToolkit.logMessage("" + accountNumber +
                                // ":Additional amount = " + amt);
                                amt2 = amt2.addMe(amt);
                                accdayrptRecord.put(POS_AMOUNT, amt2);
                                HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                        .get(STORE_AMOUNT);
                                if (sAmt == null) {
                                    accdayrptRecord.put(STORE_AMOUNT, amt2);
                                } else {
                                    accdayrptRecord.put(STORE_AMOUNT, sAmt
                                            .addMe(amt));
                                }
                            }
                        }
                        accdayrptRecord.put("updateUserID", z
                                .getCashierNumber());
                        accdayrptRecord.put("updateDate", new java.util.Date());
                        String uWhere = " WHERE storeID = '" + storeID
                                + "' AND accountDate = '" + accDate
                                + "' AND posNumber = '" + cposNumber
                                + "' AND accountNumber = '" + accountNumber
                                + "'";
                        String fieldName = "";
                        String nameParams = "";
                        Object fieldValue = new Object();
                        Iterator iterator = accdayrptRecord.keySet().iterator();
                        while (iterator.hasNext()) {
                            fieldName = (String) iterator.next();
                            if (accdayrptRecord.get(fieldName) == null) {
                                fieldValue = new Object();
                            } else {
                                fieldValue = accdayrptRecord.get(fieldName);
                                if (fieldValue instanceof java.util.Date) {
                                    SimpleDateFormat df = CreamCache
                                            .getInstance().getDateTimeFormate();
                                    java.util.Date d = (java.util.Date) fieldValue;
                                    nameParams += fieldName + "=" + "'"
                                            + df.format(d).toString() + "',";
                                } else if (fieldValue instanceof String) {
                                    nameParams += fieldName + " = " + "'"
                                            + fieldValue + "',";
                                } else {
                                    nameParams += fieldName + " = "
                                            + fieldValue + ",";
                                }
                            }
                        }
                        String finalStr = updateString
                                + nameParams.substring(0,
                                        nameParams.length() - 1) + "  "
                                + uWhere;//
                        try {
                            statement = connection.createStatement();
                            statement.execute(finalStr);
                            statement.close();
                        } catch (SQLException e) {
                            Server.log("SQL statement: " + finalStr);
                            e.printStackTrace(Server.getLogger());
                            returnValue = false;
                        } finally {
                            try {
                                if (statement != null)
                                    statement.close();
                            } catch (SQLException e) {
                                e.printStackTrace(Server.getLogger());
                            }
                        }
                    }
                } // Iterate ZReport loop
            } // synchronization block
            //将该日的团购销售总额和团购收款单的总额填入到现金日报表中
            //2013-07-17 @pingping 修改团购结算和赊账翻倍的问题
           /* String billHeadSqlStr = "select storeID, sum(salesAmount), updateUserID from billhead where " +
                    "accountdate = '"  + accountingDate + " ' and billtype='W' and billstatus='1' " +
                    "group by storeID";
            ArrayList billHead = getAllAmount(connection,billHeadSqlStr);//查询赊账
            String rpHeadSqlSre = "select storeID,sum(invcAmount) , updateUserID from RP_head where " +
                    "accountdate= '" + accountingDate + "' and billstatus='1'group by storeID";
            ArrayList rpHead = getAllAmount(connection,rpHeadSqlSre);//查询团购结算
            if (rpHead.size() != 0 && billHead.size() !=0)
                returnValue = updateAccountBillHeadOrRPHead(connection,billHead,rpHead,accountingDate);*/

            connection.commit();
            return returnValue;
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            Server.log("afterReceivingZReport() failed (pos="
                    + z.getTerminalNumber() + ",z=" + z.getSequenceNumber()
                    + ").");
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }

    }
    //将该日的团购销售总额和团购收款单的总额填入到现金日报表中
    private boolean updateAccountBillHeadOrRPHead (DbConnection connection, ArrayList billHead, ArrayList rpHead,
                                                   Date accountingDate) {
        boolean returnValue = true;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = format.format(accountingDate);
        String select = "select * from accdayrpt ";
        String storeId = "";
        String where = "";
        if ((billHead.size() != 0) && (rpHead.size() != 0)) {
            storeId = billHead.get(0).toString();
            where = where + "where storeId = '" + storeId + "' and accountdate = '" + dateStr
                    + "' and  (accountNumber = '502' or accountNumber = '203') ";
        } else if (billHead.size() != 0) {
            storeId = billHead.get(0).toString();
            where = where + "where storeId = '" + storeId + "' and accountdate = '" + dateStr
                    + "' and accountNumber = '502' ";
        } else {
            storeId = rpHead.get(0).toString();
            where = where + "where storeId = '" + storeId + "' and accountdate = '" + dateStr
                    + "' and accountNumber = '203' ";
        }
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList table = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(select + where);
            Server.log(select + where);
            table = constructFromResultSet(resultSet);
            if (table.isEmpty()) {
                Server.log("updateAccountBillHeadOrRPHead(): Cannot find record in accdayrpt");
                Server.log("Query : " + select + where);
                return false;
            }
        } catch (SQLException e) {
            Server.log("updateAccountBillHeadOrRPHead(): Cannot find record in accdayrpt");
            Server.log("SQLStatement: " + select + where);
            e.printStackTrace(Server.getLogger());
            return false;
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
            if (!returnValue)
                return false;
        }
        Iterator itr = table.iterator();
        while (itr.hasNext()) {
            HashMap accdayrptRecord = (HashMap)itr.next();
            String accountNumber = (String)accdayrptRecord.get("accountnumber");
            if ((accountNumber.startsWith("203")) && (rpHead.size() != 0)) {
                ArrayList rpdtl = null;
                String rpdtlSql = "select payment,payamount from RP_dtl where billnumber in (select " +
                        "billnumber from RP_head where accountdate='" + dateStr + "' and billstatus='1')";
                try {
                    statement = connection.createStatement();
                    resultSet = statement.executeQuery(rpdtlSql);
                    Server.log(rpdtlSql);
                    rpdtl = constructFromResultSet(resultSet);
                } catch (SQLException e) {
                    Server.log("SQLStatement: " + rpdtlSql);
                    e.printStackTrace(Server.getLogger());
                    return false;
                } finally {
                    try {
                        if (statement != null)
                            statement.close();
                    } catch (SQLException e) {
                        e.printStackTrace(Server.getLogger());
                    }
                    if (!returnValue)
                        return false;
                }
                if (rpdtl.size() != 0) {
                    Iterator rpItr = rpdtl.iterator();
                    while (rpItr.hasNext()) {
                        String paymentStr = "";
                        String updatePaymentStr = "";
                        HashMap rpdtlmap = (HashMap)rpItr.next();
                        String payment = (String)rpdtlmap.get("payment");
                        if (payment.equals("00"))
                            paymentStr = paymentStr + "500";
                        else if (payment.equals("01"))
                            paymentStr = paymentStr + "501";
                        else if (payment.equals("03"))
                            paymentStr = paymentStr + "503";
                        else if (payment.equals("04"))
                            paymentStr = paymentStr + "504";
                        else if (payment.equals("05"))
                            paymentStr = paymentStr + "505";
                        else if (payment.equals("07"))
                            paymentStr = paymentStr + "506";
                        else if (payment.equals("08"))
                            paymentStr = paymentStr + "507";
                        else if (payment.equals("09"))
                            paymentStr = paymentStr + "508";
                        else if (payment.equals("10"))
                            paymentStr = paymentStr + "509";
                        else if (payment.equals("11"))
                            paymentStr = paymentStr + "510";
                        else if (payment.equals("12"))
                            paymentStr = paymentStr + "511";
                        else if (payment.equals("13"))
                            paymentStr = paymentStr + "512";
                        else if (payment.equals("14"))
                            paymentStr = paymentStr + "513";
                        else if (payment.equals("15"))
                            paymentStr = paymentStr + "514";
                        else if (payment.equals("16"))
                            paymentStr = paymentStr + "515";

                        String rpSelect = "select posAmount,storeAmount from accdayrpt  ";
                        String rpWhere = " where accountDate = '" + dateStr + "' and  storeID = '" + rpHead.get(0)
                                + "' and accountNumber = '" + paymentStr + "'";

                        ArrayList accdayrptPayment = null;
                        try {
                            accdayrptPayment = getAccdayrptPayment(connection, rpSelect + rpWhere);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        if (accdayrptPayment.size() != 0) {
                            HYIDouble posamt = (HYIDouble)accdayrptPayment.get(0);
                            posamt.addMe((HYIDouble)rpdtlmap.get("payamount"));
                            HYIDouble storeamt = (HYIDouble)accdayrptPayment.get(1);
                            storeamt.addMe((HYIDouble)rpdtlmap.get("payamount"));
                            SimpleDateFormat updateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            updatePaymentStr = "update accdayrpt set posAmount = " + posamt + ",storeAmount = "
                                    + storeamt + ", updateDate = '" + updateFormat.format(new Date()) + "'  " + rpWhere;
                            try {
                                connection = CreamToolkit.getPooledConnection();
                                statement = connection.createStatement();
                                statement.execute(updatePaymentStr);
                                Server.log("SQl statement success:" + updatePaymentStr);
                                statement.close();
                            } catch (SQLException e) {
                                Server.log("SQL statement: " + updatePaymentStr);
                                e.printStackTrace(Server.getLogger());
                                returnValue = false;
                            } finally {
                                try {
                                    if (statement != null)
                                        statement.close();
                                } catch (SQLException e) {
                                    e.printStackTrace(Server.getLogger());
                                }
                                if (connection != null)
                                    CreamToolkit.releaseConnection(connection);
                            }
                        }
                    }
                }
                HYIDouble posAmt = (HYIDouble)rpHead.get(1);
                HYIDouble scAmt = (HYIDouble)rpHead.get(1);
                accdayrptRecord.put("posamount", posAmt);
                accdayrptRecord.put("storeamount", scAmt);
                accdayrptRecord.put("updateuserid", rpHead.get(2));
            } else if ((accountNumber.startsWith("502")) && (billHead.size() != 0)) {
                HYIDouble posAmt = new HYIDouble(0D);
                HYIDouble scAmt = new HYIDouble(0D);
                posAmt.addMe((HYIDouble)billHead.get(1));
                scAmt.addMe((HYIDouble)billHead.get(1));
                accdayrptRecord.put("posamount", posAmt);
                accdayrptRecord.put("storeamount", scAmt);
                accdayrptRecord.put("updateuserid", billHead.get(2));
            }
            accdayrptRecord.put("updatedate", new Date());
            String uWhere = " WHERE storeID = '" + storeId + "' AND accountDate = '" + dateStr
                    + "' AND accountNumber = '" + accountNumber + "'";

            String fieldName = "";
            String nameParams = "";
            Object fieldValue = new Object();
            Iterator iterator = accdayrptRecord.keySet().iterator();
            while (iterator.hasNext()) {
                fieldName = (String)iterator.next();
                if (accdayrptRecord.get(fieldName) == null) {
                    fieldValue = new Object();
                } else {
                    fieldValue = accdayrptRecord.get(fieldName);
                    if (fieldValue instanceof java.util.Date) {
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        java.util.Date d = (java.util.Date)fieldValue;
                        nameParams  += fieldName + "=" + "'" + df.format(d).toString() + "',";
                    } else if (fieldValue instanceof String) {
                        nameParams += fieldName + " = " + "'" + fieldValue + "',";
                    } else {
                        nameParams += fieldName + " = " +  fieldValue + ",";
                    }
                }
            }
            String updateString = "update accdayrpt set  ";
            String finalStr = updateString + nameParams.substring(0, nameParams.length() - 1) + "  " + uWhere;
            try {
                connection = CreamToolkit.getPooledConnection();
                statement = connection.createStatement();
                Server.log("SQl statement success:" + finalStr);
                statement.execute(finalStr);
                statement.close();
            } catch (SQLException e) {
                Server.log("SQL statement: " + finalStr);
                e.printStackTrace(Server.getLogger());
                returnValue = false;
            } finally {
                try {
                    if (statement != null)
                        statement.close();
                } catch (SQLException e) {
                    e.printStackTrace(Server.getLogger());
                }
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            }
        }
        return returnValue;
    }
    /**
     * tranhead, shift, depsales, daishousales, daishousales2
     * @param connection
     * @param z
     * @param accountingDateString
     * @return
     */
    private boolean updateAccountDateByZNo(DbConnection connection, ZReport z, String accountingDateString) {
        boolean result = false;
        try {
            String updateSQL = "UPDATE posul_tranhead SET accountDate='" + accountingDateString
            + "' WHERE zSequenceNumber='"+ z.getSequenceNumber()
            + "' AND posNumber='"+ z.getTerminalNumber()
            + "' AND storeID='"+ z.getStoreNumber() + "'";
            Server.log(updateSQL);
            executeQuery(connection, updateSQL);

            updateSQL = "UPDATE posul_shift SET accountDate='" + accountingDateString
            + "' WHERE zSequenceNumber='"+ z.getSequenceNumber()
            + "' AND posNumber='"+ z.getTerminalNumber()
            + "' AND storeID='"+ z.getStoreNumber() + "'";
            Server.log(updateSQL);
            executeQuery(connection, updateSQL);

            updateSQL = "UPDATE posul_daishousales SET accountDate='" + accountingDateString
            + "' WHERE zSequenceNumber='"+ z.getSequenceNumber()
            + "' AND posNumber='"+ z.getTerminalNumber()
            + "' AND storeID='"+ z.getStoreNumber() + "'";
            Server.log(updateSQL);
            executeQuery(connection, updateSQL);

            updateSQL = "UPDATE posul_daishousales2 SET accountDate='" + accountingDateString
            + "' WHERE zSequenceNumber='"+ z.getSequenceNumber()
            + "' AND posNumber='"+ z.getTerminalNumber()
            + "' AND storeID='"+ z.getStoreNumber() + "'";
            Server.log(updateSQL);
            executeQuery(connection, updateSQL);
            result = true;
        } catch (SQLException e) {
            e.printStackTrace();
            e.printStackTrace(Server.getLogger());
        } finally {
            System.out.println("updateAccountDateByZNo result>" + result);
            Server.log("updateAccountDateByZNo result>" + result);
        }
        return result;
    }

    public synchronized boolean afterReceivingTransaction(Object[] trans) {
        int i;
        Transaction tran = null;
        int lineItemShouldHave = 0;
        int lineItemActualHave = 0;
        int posNumber = 0;
        int transactionNumber = 0;
        List<Map> details = new ArrayList<Map>();
        Date accountingDate = null;
        DbConnection connection = null;

        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();
            for (i = 0; i < trans.length; i++) {
                if (i == 0) {
                    Transaction curTran = (Transaction)trans[i];
                    ZReport oldZ = ZReport.queryByKey(connection, curTran
                        .getStoreNumber(), curTran.getTerminalNumber(),
                        curTran.getZSequenceNumber());
                    if (oldZ != null)
                        accountingDate = oldZ.getAccountingDate();
                }
                if (trans[i] instanceof Transaction) {
                    tran = (Transaction)trans[i];
                    if (accountingDate != null)
                        tran.setAccountingDate(accountingDate);
                    lineItemShouldHave = tran.getDetailCount().intValue();
                    posNumber = tran.getTerminalNumber().intValue();
                    transactionNumber = tran.getTransactionNumber().intValue();
                    Server.log("afterReceivingTransaction " + transactionNumber);
                } else if (trans[i] instanceof LineItem) {
                    lineItemActualHave++;
                    LineItem li = (LineItem)trans[i];
                    details.add(li.getFieldMap());
                    if (tran != null) {
                        // Because there is no connection between Transaction and
                        // LineItem objects sent from POS
                        tran.addLineItemSimpleVersion(li);
                        li.setSystemDateTime(tran.getSystemDateTime());
                    }
                } else if (trans[i] instanceof Alipay_detail) {
                    Alipay_detail li = (Alipay_detail)trans[i];
                    details.add(li.getFieldMap());
                    if (tran != null) {
                        tran.addAlipayList(li);
                        li.setSYSTEMDATE(tran.getSystemDateTime());
                    }
                } else if (trans[i] instanceof Cmpay_detail) {
                    Cmpay_detail cMpay_detail = (Cmpay_detail)trans[i];
                    details.add(cMpay_detail.getFieldMap());
                    if (tran != null) {
                        tran.addCmPayList(cMpay_detail);
                        cMpay_detail.setSYSTEMDATE(tran.getSystemDateTime());
                    }
                } else if (trans[i] instanceof WeiXin_detail) {
                    WeiXin_detail cWeiXin_detail = (WeiXin_detail)trans[i];
                    details.add(cWeiXin_detail.getFieldMap());
                    if (tran != null) {
                        tran.addWeiXinList(cWeiXin_detail);
                        cWeiXin_detail.setSYSTEMDATE(tran.getSystemDateTime());
                    }
                } else if (trans[i] instanceof UnionPay_detail) {
                    UnionPay_detail cUnionpay_detail = (UnionPay_detail)trans[i];
                    details.add(cUnionpay_detail.getFieldMap());
                    if (tran != null) {
                        tran.addUnionPayList(cUnionpay_detail);
                        cUnionpay_detail.setSYSTEMDATE(tran.getSystemDateTime());
                    }
                } else if (trans[i] instanceof Selfbuy_head) {
                    Selfbuy_head cSelfbuy_head = (Selfbuy_head)trans[i];
                    details.add(cSelfbuy_head.getFieldMap());
                    if (tran != null) {
                        tran.addSelfbuyHeadList(cSelfbuy_head);
                        cSelfbuy_head.setSYSTEMDATE(tran.getSystemDateTime());
                    }
                } else if (trans[i] instanceof Selfbuy_detail) {
                    Selfbuy_detail cSelfbuy_detail = (Selfbuy_detail)trans[i];
                    details.add(cSelfbuy_detail.getFieldMap());
                    if (tran != null) {
                        tran.addSelfbuyDetailList(cSelfbuy_detail);
                        cSelfbuy_detail.setSYSTEMDATE(tran.getSystemDateTime());
                    }
                }
            }

            // integrity check, if has problem, discard it.
            if (lineItemShouldHave != lineItemActualHave) {
                Server.log("Transaction (pos=" + posNumber + ", no="
                    + transactionNumber
                    + ") is incomplete. Detail count should be "
                    + lineItemShouldHave + ", but actual detail count is "
                    + lineItemActualHave);
                connection.rollback();
                return false;
            }

            Server.log("[" + posNumber + "] add transaction (no = "
                + transactionNumber + ") begin...");

            int ret = 0; // continue
            String state1 = tran.getState1();
            if ("J".equals(tran.getState1())
                || "K".equals(state1)
                || "L".equals(state1)
                || "T".equals(state1)
                || "Q".equals(state1)
                || "P".equals(state1)
                || "D".equals(state1)
                || "B".equals(state1)
                || !tran.getState3().equals("0")) {
                ret = afterReceivingSpecialTran(connection, tran, details);
            }
            if (ret == 1) {
                // commit transaction
                connection.commit();

                return true;
            } else if (ret == -1) {
                // commit transaction
                connection.commit();

                return false;
            }

            for (i = 0; i < trans.length; i++) {
                if (trans[i] instanceof Transaction) {
                    tran = (Transaction)trans[i];

                    // insert into posul_tranhead
                    tran.deleteByPrimaryKey(connection);
                    tran.insert(connection, false);

                } else if (trans[i] instanceof LineItem) {
                    LineItem li = (LineItem)trans[i];

                    // insert into posul_trandtl
                    li.deleteByPrimaryKey(connection);
                    li.insert(connection, false);
                } else if (trans[i] instanceof Alipay_detail) {
                    Alipay_detail detail = (Alipay_detail) trans[i];
                    detail.deleteByPrimaryKey(connection);
                    detail.insert(connection, false);
                } else if (trans[i] instanceof Cmpay_detail) {
                    Cmpay_detail detail = (Cmpay_detail) trans[i];
                    detail.deleteByPrimaryKey(connection);
                    detail.insert(connection, false);
                } else if (trans[i] instanceof WeiXin_detail) {
                    WeiXin_detail detail = (WeiXin_detail) trans[i];
                    detail.deleteByPrimaryKey(connection);
                    detail.insert(connection, false);
                } else if (trans[i] instanceof UnionPay_detail) {
                    UnionPay_detail detail = (UnionPay_detail) trans[i];
                    detail.deleteByPrimaryKey(connection);
                    detail.insert(connection, false);
                } else if (trans[i] instanceof Selfbuy_head) {
                    Selfbuy_head detail = (Selfbuy_head) trans[i];
                    detail.deleteByPrimaryKey(connection);
                    detail.insert(connection, false);
                } else if (trans[i] instanceof Selfbuy_detail) {
                    Selfbuy_detail detail = (Selfbuy_detail) trans[i];
                    detail.deleteByPrimaryKey(connection);
                    detail.insert(connection, false);
                }
            }

            ////PostObjectData pod = new PostObjectData(posNumber, transactionNumber,
            ////    tran.getSystemDateTime());
            ////pod.save(tran);
            updatePeiDaPayStatus(tran);

            // commit transaction
            connection.commit();

            Server.log("[" + posNumber + "] add transaction (no = " + transactionNumber + ") end");
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            e.printStackTrace(Server.getLogger());
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * 转档流程 return
     * -1 false
     * 0 continue
     * 1 true
     */
    private int afterReceivingSpecialTran(DbConnection connection, Transaction tran, List<Map> details) {
        // 期刊,团购交易,tranhead直接insert到数据库,因为要更新交易状态
        String state1 = tran.getState1();
        Server.log("afterReceivingTransaction is periodical "
                + tran.getState1() + tran.getState2() + tran.getState3());
        try {
            boolean isTurned = isTurned(connection, tran);
            Server.log("afterReceivingTransaction isTurned " + isTurned);
            if (isTurned)
                // 已更新过ordbookhead,直接insert交易
                return 0;

            // 赊账交易需附加处理
            if (!tran.getState3().equals("0"))
                updateSheAmount2Member(connection, tran);

            if ("J".equals(state1))
                insertOrderPeriodical(connection, tran);
            else if ("K".equals(state1))
                updateDrawPeriodical(connection, tran);
            else if ("L".equals(state1))
                updateReturnPeriodical(connection, tran);
            else if ("T".equals(state1) || "P".equals(state1)
                    || "D".equals(state1) || "B".equals(state1))
                updateWholesale(connection, tran);
            else if ("Q".equals(state1))
                updateWholeClearingsale(connection, tran);
            else if (!tran.getState3().equals("0"))
                ;
            else
                return -1;
            tran.setState2("1");
            directInsertTran(connection, tran);
            TranProcessorThread tpt = new TranProcessorThread("s");
            tpt.additionalProcess(connection, tran.getFieldMap(), details);
            Server.log("afterReceivingSpecialTran insertion commited.");
            return 1;
        } catch (Exception e) {
            e.printStackTrace(Server.getLogger());
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace(Server.getLogger());
            }
            return -1;
        }
    }

    public synchronized boolean afterReceivingDepSales(Object[] depSales) {
        boolean success = true;
        DbConnection connection = null;
        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();
            int zNumber = ((DepSales) depSales[0]).getSequenceNumber();
            int posNumber = ((DepSales) depSales[0]).getPOSNumber();
            Date accDate = getAccountingDate(connection, posNumber, zNumber);

            // 删除已经存在的
            DepSales.deleteBySequenceNumber(connection, zNumber, posNumber);

            for (int i = 0; i < depSales.length; i++) {
                DepSales ds = (DepSales) depSales[i];
                ds.setAccountDate(accDate);
                ds.insert(connection, false);
                Server.log("DepSales inserted (pos=" + posNumber + ", zNumber="
                        + zNumber + ", id=" + ds.getDepID() + ").");
            }
            // commit transaction
            connection.commit();
            Server.log("DepSales insertion commited.");
            return true;

        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            return false;

        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public synchronized boolean afterReceivingDaishouSales(Object[] daishouSales) {
        boolean success = true;
        DbConnection connection = null;
        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();
            int zNumber = ((DaishouSales) daishouSales[0]).getZSequenceNumber();
            int posNumber = ((DaishouSales) daishouSales[0]).getPosNumber();
            Date accDate = getAccountingDate(connection, posNumber, zNumber);

            DaishouSales.deleteBySequenceNumber(connection, zNumber, posNumber);

            for (int i = 0; i < daishouSales.length; i++) {
                DaishouSales ds = (DaishouSales) daishouSales[i];
                ds.setAccountDate(accDate);
                ds.insert(connection, false);
                Server.log("DaishouSales inserted (pos=" + posNumber + ",z="
                        + zNumber + ",id1=" + ds.getFirstNumber() + ",id2="
                        + ds.getSecondNumber() + ").");
            }
            connection.commit();
            Server.log("DaishouSales insertion commited.");
            return true;
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * afterReceivingDaiShouSales2
     *
     * @return 如果代收资料存入SC数据库中成功则返回 true，否则返回 false.
     */
    public synchronized boolean afterReceivingDaiShouSales2(
            Object[] daiShouSales2) {
        boolean success = true;
        DbConnection connection = null;
        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();
            if (daiShouSales2.length == 0)
                return success;

            int zNumber = ((DaiShouSales2) daiShouSales2[0])
                    .getZSequenceNumber();
            int posNumber = ((DaiShouSales2) daiShouSales2[0]).getPosNumber();

            DaiShouSales2
                    .deleteBySequenceNumber(connection, zNumber, posNumber);

            for (int i = 0; i < daiShouSales2.length; i++) {
                DaiShouSales2 ds = (DaiShouSales2) daiShouSales2[i];
                ds.setAccountDate(getAccountingDate(connection, posNumber,
                        zNumber, ds.getPayTime()));
                ds.insert(connection, false);
                Server.log("DaiShouSales2 inserted (StoreID = "
                        + ds.getStoreID() + ", POS = " + posNumber + ", Z = "
                        + zNumber + ", ID = " + ds.getID() + ")");
            }
            // commit transaction
            connection.commit();
            Server.log("DaishouSales2 insertion commited.");
            return true;
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            return false;

        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Inventory processor.
     *
     * @param inventory
     *            Inventory objects.
     * @return true if success, otherwise false.
     */
    public boolean afterReceivingInventory(Object[] inventory) {
        int deleteCount = 0;
        int insertCount = 0;
        //int updateCount = 0;
        DbConnection connection = null;
        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();
            int len = inventory.length;
            for (int i = 0; i < len; i++) {
                Inventory ds = (Inventory) inventory[i];
//                try {
//                    ds.insert(connection, false);
//                    insertCount++;
//                } catch (SQLException e) {
//                    ds.deleteByPrimaryKey(connection);
//                    ds.insert(connection, false);
//                    updateCount++;
//                }
                if (ds.deleteByPrimaryKey(connection)){
                    deleteCount++;
                }
                ds.insert(connection, false);
                insertCount++;
            }

            // commit transaction
            connection.commit();
            Server.log("Inventory insertion commited. " + deleteCount + " clash record deleted"
                    + ", " + insertCount + " records inserted");
            return true;

        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            return false;

        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public boolean afterReceivingAttendance1(Object[] attendances) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();
            Date accountingDateObj = null;
            for (int i = 0; i < attendances.length; i++) {
                Attendance1 att = (Attendance1)attendances[i];
                if (i == 0) {
                    accountingDateObj = att.getBusiDate();
//                    ZReport oldZ = ZReport.queryByKey(connection, att.getStoreID(), att.getPosNumber(), att.getZnumber().intValue());
//                    if (oldZ != null) {
//                        accountingDateObj = oldZ.getAccountingDate();
//                        Server.log("Get existed Z's account date=" + yyyy_MM_dd.format(accountingDateObj));

                    //Bruce> 現在depsales上傳的時候會計日期已填成POS端的系統日期（原來的下面這個句子不會成立）。現在改成上傳的時候必須以後端的會計日期為準。
                    //} else if (accountingDateObj == null || ((yyyy_MM_dd.format(accountingDateObj)).startsWith("197"))) {
//                    } else {
                           accountingDateObj = getAccountingDate();  // get accountDate from Table calendar
                           Server.log("Get today's account date=" + yyyy_MM_dd.format(accountingDateObj));
//                    }
                }
                att.setBusiDate(accountingDateObj);
                att.insert(connection, false);
                Server.log("posul_Attendance inserted (busidate=" + yyyy_MM_dd.format(accountingDateObj) + ", pos=" + att.getPosNumber()
                    + ", storeID=" + att.getStoreID()
                    + ", employeeID=" + att.getEmployeeID()
                    + ", createDate=" + att.getCreateDate()
                    + ").");
            }
            connection.commit();
            Server.log("posul_Attendance insertion commited.");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            e.printStackTrace(Server.getLogger());
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Master download preprocessor.
     *
     * @return true if it allows master downloading now, false otherwise.
     */
    public boolean beforeDownloadingMaster() {
        return true;
        //李荣侨2013/05/10
        // Hi 游副总：
        // 昨天在公司内部召开教超维护会议，讨论教超近期维护的内容，发现有1个问题重复出现的频率比较高，希望POS程序能配合修改。
        // POS主档更新失败
        // POS主档下发会读取calendar中某个字段的值，当该值为1POS主档下发就会失败，而该问题产生的原因是由于 DataShift的某个
        // 原因导致，在没有找到DataShift原因之前，我希望修改POS主档下发不判断该标志，直接下发主档。请回复什么时候能修改好，谢谢。
        //return !isDateShiftRunningOrFailed(DATE_BIZDATE);
    }

    /**
     * Meyer / 2002-02-11 Check before synchronize transaction
     *
     * @return true if it allows synchronize transaction, false otherwise.
     */
    public boolean canSyncTransaction() {
        return !isDateShiftRunning(DATE_BIZDATE);
    }

    private void updateSheAmount2Member(DbConnection connection, Transaction tran)
            throws EntityNotFoundException, SQLException {
        // 赊账只有会员才能使用
        if (tran.getMemberID() == null || tran.getMemberID().trim().equals("")) {
            return;
        }

        HYIDouble sheAmount = tran.getShePaymentAmount();
        Server.log("updateSheAmount2Member : " + sheAmount);
        if (sheAmount.compareTo(new HYIDouble(0)) == 0)
            return;
////    James/20081014 the SheAmount include sign, needn't multiply the sign again.
//        HYIDouble sign = new HYIDouble(1);
//        if ("1".equals(tran.getState3()))
//            ;
//        else if ("2".equals(tran.getState3()))
//            sign = new HYIDouble(-1);
//        else if ("3".equals(tran.getState3()))
//            sign = new HYIDouble(0);
//        Server.log("updateSheAmount2Member : " + sign);

        MemberCardBase mcb = new MemberCardBase();
        mcb.setStoreID(tran.getStoreNumber());
        mcb.setFcardnumber(tran.getMemberID());
        mcb.load(connection);
        Server.log("updateSheAmount2Member : found");

        if (mcb.getEarningamt() == null)
            mcb.setEarningamt(new HYIDouble(0));

        if (mcb.getEarningamt() == null)
            mcb.setEarningamt(new HYIDouble(0));
        mcb.setEarningamt(mcb.getEarningamt().add(sheAmount));
//      mcb.setEarningamt(mcb.getEarningamt().add(sheAmount.multiply(sign)));
        Server.log("updateSheAmount2Member : " + mcb.getEarningamt());
        mcb.setUpdateuserid(tran.getCashierNumber());
        mcb.setUpdatedate(new Date());
        mcb.update(connection);
    }

    private void updateWholesale(DbConnection connection, Transaction tran)
            throws EntityNotFoundException, SQLException {
        String sql = "UPDATE wholesalehead SET billUpdate = '"
                + CreamCache.getInstance().getDateFormate().format(new Date())
                + "' , cashierId = '" + tran.getCashierNumber() + "' , "
                + " cashierDate = '"
                + dfyyyyMMddHHmmss.format(tran.getSystemDateTime())
                + "' , posNo = " + tran.getTerminalNumber() + " , "
                + " tranNo = " + tran.getTransactionNumber()
                + " , billState = 3 " + " WHERE storeID = '"
                + tran.getStoreNumber() + "' AND billNo = '"
                + tran.getAnnotatedId().trim() + "'";
        // TODO 如何考虑团购退货的，团购作废等？
        // TODO 方案 pos退货，sc自动新增团购退货单，在此步把团购退货单改为已经审核
        Server.log("executeUpdate : " + sql);
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();
    }

    private void updateWholeClearingsale(DbConnection connection, Transaction tran)
            throws EntityNotFoundException, SQLException {
        MemberCardBase mcb = new MemberCardBase();
        mcb.setStoreID(tran.getStoreNumber());
        mcb.setFcardnumber(tran.getMemberID());
        mcb.load(connection);

        if (mcb.getEarningamt() == null)
            mcb.setEarningamt(new HYIDouble(0));
        HYIDouble clearingAmt = tran.getPayAmount1().add(tran.getPayAmount2())
                .add(tran.getPayAmount3()).add(tran.getPayAmount4()).subtract(
                        tran.getChangeAmount()).subtract(tran.getSpillAmount());
        Server.log("WholeClearingsale | Earningamt : " + mcb.getEarningamt()
                + " | clearingAmt : " + clearingAmt);
        mcb.setEarningamt(mcb.getEarningamt().subtract(clearingAmt));
        Server.log("WholeClearingsale | Earningamt : " + mcb.getEarningamt());
        mcb.setUpdateuserid(tran.getCashierNumber());
        mcb.setUpdatedate(new Date());
        mcb.update(connection);
    }

    private void updateReturnPeriodical(DbConnection connection, Transaction tran)
            throws EntityNotFoundException, SQLException {
        Object[] items = tran.getLineItems();
        Date updateDate = new Date();
        for (int i = 0; i < items.length; i++) {
            LineItem li = (LineItem) items[i];
            if (li.getRemoved() || "V".equalsIgnoreCase(li.getDetailCode())
                    || "E".equalsIgnoreCase(li.getDetailCode()))
                continue;
            if (li.getQuantity().compareTo(new HYIDouble(0)) == 0
                    || li.getContent().trim().length() == 0)
                continue;
            String[] between = StringUtils.splitPreserveAllTokens(li
                    .getContent(), "."); // billno,itemseq,dtlseq
            OrdBookHead obh = new OrdBookHead();
            obh.setStoreID(tran.getStoreNumber());
            obh.setBillNo(between[0]);
            obh.setItemSeq(Integer.parseInt(between[1]));
            obh.setItemNo(li.getItemNumber());
            obh.load(connection);

            obh.setVoidPosNo(tran.getTerminalNumber());
            obh.setVoidTranNo(tran.getTransactionNumber());

            OrdBookDtl obd = new OrdBookDtl();
            obd.setStoreID(tran.getStoreNumber());
            obd.setBillNo(between[0]);
            obd.setItemSeq(Integer.parseInt(between[1]));
            obd.setDtlseq(Integer.parseInt(between[2]));
            obd.setItemNo(li.getItemNumber());

            obd.setPosNo(tran.getTerminalNumber());
            obd.setTranNo(tran.getTransactionNumber());
            obd.setState(2);
            obd.setUpdateUserId(tran.getCashierNumber());
            obd.setUpdateDate(updateDate);

            //obh.setBillState(4);
            if (obh.getRtnOrdAmt() == null)
                obh.setRtnOrdAmt(new HYIDouble(0));
            obh.setRtnOrdAmt(obh.getRtnOrdAmt().add(li.getAmount()));
            obh.setUpdateUserId(tran.getCashierNumber());
            obh.setUpdateDate(updateDate);

            obd.update(connection);
            obh.update(connection);
        }
    }

    private void updateDrawPeriodical(DbConnection connection, Transaction tran)
            throws EntityNotFoundException, SQLException {
        Object[] items = tran.getLineItems();
        Date updateDate = new Date();
        for (int i = 0; i < items.length; i++) {
            LineItem li = (LineItem) items[i];
            if (li.getRemoved() || "V".equalsIgnoreCase(li.getDetailCode())
                    || "E".equalsIgnoreCase(li.getDetailCode()))
                continue;
            if (li.getQuantity().compareTo(new HYIDouble(0)) == 0
                    || li.getContent().trim().length() == 0)
                continue;
            String[] between = StringUtils.splitPreserveAllTokens(li
                    .getContent(), "."); // billno,itemseq,dtlseq
            OrdBookHead obh = new OrdBookHead();
            obh.setStoreID(tran.getStoreNumber());
            obh.setBillNo(between[0]);
            obh.setItemSeq(Integer.parseInt(between[1]));
            obh.setItemNo(li.getItemNumber());

            obh.load(connection);

            OrdBookDtl obd = new OrdBookDtl();
            obd.setStoreID(tran.getStoreNumber());
            obd.setBillNo(between[0]);
            obd.setItemSeq(Integer.parseInt(between[1]));
            obd.setDtlseq(Integer.parseInt(between[2]));
            obd.setItemNo(li.getItemNumber());
            if (obd.getQtyBase() == null)
                obd.setQtyBase(0);
            obd.setQtyBase(0);
            obd.setPosNo(tran.getTerminalNumber());
            obd.setTranNo(tran.getTransactionNumber());
            obd.setUpdateUserId(tran.getCashierNumber());
            obd.setUpdateDate(updateDate);
            if (obd.getQtyBase().intValue() == 0) {
                obd.setState(1);
                if (obh.getBalCount() > 0)
                    obh.setBalCount(obh.getBalCount() - 1);
                else
                    Server.log("data abnormity, orderbookhead["
                            + obh.getBillNo() + "] balcount is less 0!");
                // throw new SQLException("data abnormity, orderbookhead[" +
                // obh.getBillNo() +"] balcount is less 0!");
            }
            if (obh.getBalCount() == 0)// 全领完
                obh.setBillState(3);
            obh.setDtlCodeCurr(li.getDtlcode1());
            obh.setUpdateUserId(tran.getCashierNumber());
            obh.setUpdateDate(updateDate);

            obd.update(connection);
            obh.update(connection);
        }
    }

    private boolean insertOrderPeriodical(DbConnection connection,
            Transaction tran) throws SQLException {
        // begin transaction
        OrdBookHead obh_common = new OrdBookHead();
        obh_common.setStoreID(tran.getStoreNumber());
        obh_common
                .setBillNo(genOrdBookBillNO(connection, tran.getStoreNumber()));
        obh_common.setCardNo(tran.getMemberID());
        obh_common.setSrc(1); //
        obh_common.setCustOrdDate(tran.getSystemDateTime());
        // TODO custOrdType
        // obh.setCustOrdType(custOrdType)
        obh_common.setPosNo(tran.getTerminalNumber());
        obh_common.setTranNo(tran.getTransactionNumber());
        // obh.setAccountDate(accountDate)
        obh_common.setBillState(1);
        obh_common.setUpdateUserId(tran.getCashierNumber());

        OrdBookDtl obd_common = new OrdBookDtl();
        obd_common.setStoreID(tran.getStoreNumber());
        obd_common.setBillNo(obh_common.getBillNo());
        obd_common.setPosNo(tran.getTerminalNumber());
        obd_common.setTranNo(tran.getTransactionNumber());
        obd_common.setUpdateUserId(tran.getCashierNumber());
        obd_common.setUpdateDate(obh_common.getUpdateDate());
        obd_common.setState(0);
        obd_common.setIsorder(0);
        Object[] lines = tran.getLineItems();
        int itemSeq = 0;
        for (Object l : lines) {
            LineItem li = (LineItem) l;
            if (li.getRemoved() || "V".equalsIgnoreCase(li.getDetailCode())
                    || "E".equalsIgnoreCase(li.getDetailCode()))
                continue;
            if (li.getQuantity().compareTo(new HYIDouble(0)) == 0
                    || li.getContent() == null
                    || li.getContent().trim().length() == 0)
                continue;
            itemSeq++;
            OrdBookHead obh = (OrdBookHead) obh_common.clone();

            String[] between = StringUtils.splitPreserveAllTokens(li
                    .getContent(), "."); // monthstart,monthend,issncount,qty
            obh.setItemSeq(itemSeq);
            obh.setMonthStart(between[0]);
            obh.setMonthEnd(between[1]);
            List<String> subIssns = PeriodicalUtils.processGetBookableSubIssns(
                    li.getItemNumber(), between[0], between[1]);
            obh.setDtlCodeStart(subIssns.get(0));
            obh.setDtlCodeEnd(subIssns.get(subIssns.size() - 1));
            obh.setTotCount(Integer.parseInt(between[2]));
            obh.setOrdQty(Integer.parseInt(between[3]));
            obh.setTotOrdQty(li.getQuantity().intValue());
            obh.setCustOrdPrice(li.getUnitPrice());
            obh.setAmt(li.getAfterDiscountAmount());
            obh.setBalCount(obh.getTotCount());
            obh.setUpdateDate(new Date());
            obh.setItemNo(li.getItemNumber());
            obh.insert(connection, false);

            obd_common.setItemSeq(itemSeq);
            obd_common.setItemNo(li.getItemNumber());
            obd_common.setPlanQty(obh.getOrdQty());
            obd_common.setQtyBase(obh.getOrdQty());
            obd_common.setUpdateDate(obh.getUpdateDate());

            int dtlSeq = 0;
            for (String dtlcode1 : subIssns) {
                OrdBookDtl obd = (OrdBookDtl) obd_common.clone();
                dtlSeq++;
                obd.setDtlseq(dtlSeq);
                obd.setDtlcode1(dtlcode1);
                obd.insert(connection, false);
            }

        }
        // Server.log("Inventory insertion commited. (insert:" + insertCount
        // + ", update:"
        // + updateCount + ").");
        return true;
    }

    private String genOrdBookBillNO(DbConnection connection, String storeID) {
        String result = null;
        Server.log("begin call proc_getnum ...");
        String procedure = "{call proc_getnum(?,?,?)}";
        try {
            CallableStatement cstmt = connection.prepareCall(procedure);
            cstmt.registerOutParameter(3, Types.VARCHAR);
            cstmt.setInt(1, 8);
            cstmt.setString(2, storeID);
            cstmt.execute();
            result = cstmt.getString(3);
            Server.log("proc_getnum billNO : " + result);
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    private void directInsertTran(DbConnection connection, Transaction tran)
            throws SQLException {
        Date tranTime = tran.getSystemDateTime();
        Object[] items = tran.getLineItems();
        for (int i = 0; i < items.length; i++) {
            LineItem li = (LineItem) items[i];
            li.setSystemDateTime(tranTime);
            li.insert(connection, false);
            Server.log("lineItem insert : " + li.getTransactionNumber() + "|"
                    + li.getLineItemSequence() + "|" + li.getSystemDateTime());
        }

        tran.insert(connection, false);
        Server.log("tran insert : " + tran.getTerminalNumber() + "|"
                + tran.getTransactionNumber() + "|" + tran.getSystemDateTime());
    }

    public static boolean isTurned(DbConnection connection, Transaction tran) {
        if ("1".equals(tran.getState2())) {
            return true;
        }
        Transaction scTran = Transaction.queryByTransactionNumber(connection,
            tran.getTerminalNumber(), tran.getTransactionNumber(), false, false);
        if (scTran != null && "1".equals(scTran.getState2())) {
            return true;
        }
        return false;
    }

    public synchronized boolean afterReceivingAttendance(Object[] attendances) {
        boolean success = true;
        DbConnection connection = null;
        try {
            // begin transaction
            connection = CreamToolkit.getTransactionalConnection();
            for (int i = 0; i < attendances.length; i++) {
                Attendance attendance = (Attendance) attendances[i];
                attendance.deleteByPrimaryKey(connection);
                attendance.insert(connection, false);
                connection.commit();

                Server.log("work_check inserted (storeID="
                        + attendance.getStoreID() + ",Employee="
                        + attendance.getEmployee() + ",type="
                        + attendance.getShiftType() + ",SignOndttm="
                        + attendance.getSignOnDateTime() + ").");
            }
        } catch (SQLException e) {
            Server.log("work_check error!");
            e.printStackTrace(Server.getLogger());
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return success;
    }

    public static void main(String[] args)  {
        if (args[0].equals("-recalc")) {
            SimpleDateFormat formatDatee = new SimpleDateFormat("yyyy-MM-dd");
            Date recalcDate = null;
            String recalcDateString = args[1];
            try {
                recalcDate = formatDatee.parse(recalcDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println("-recalc:"+recalcDateString);
            boolean returnValue = true;
            DbConnection connection = null;
            ZReport z = null;
            try {
                // begin transaction
                connection = CreamToolkit.getTransactionalConnection();
                Date accountingDate = recalcDate;
                // 检查或生成该会计日期的accdayrpt记录
                PostProcessorImplB impl = new PostProcessorImplB();
                impl.generateAccdayrptRecords(connection, accountingDate);
                synchronized (lockObject) {
                    Iterator iter = ZReport.queryByDateTime(connection,
                            accountingDate);
                    if (iter == null) {//pos未日结也要产生现金日报，将团购销售总额和团购收款单的总额填到现金日报中
                        System.out.println("ZReport NOT EXISTS");
                        // 先避免posAmount和storeAmount有空值
                        executeQuery(connection, "UPDATE accdayrpt SET storeAmount=0 "
                                + "WHERE accountDate='" + recalcDateString
                                + "' AND storeAmount IS NULL");
                        executeQuery(connection, "UPDATE accdayrpt SET posAmount=0 "
                                + "WHERE accountDate='" + recalcDateString
                                + "' AND posAmount IS NULL");
                        // 把storeAmount先计成与原posAmount的差额(为了保留用户原来对storeAmount的修改)
                        executeQuery(connection,
                                "UPDATE accdayrpt SET storeAmount=storeAmount-posAmount "
                                        + "WHERE accountDate='" + recalcDateString
                                        + "'");
                        // 然后把posAmount归零
                        executeQuery(connection, "UPDATE accdayrpt SET posAmount=0 "
                                + "WHERE accountDate='" + recalcDateString + "'");
                        //将该日的团购销售总额和团购收款单的总额填入到现金日报表中
                        String billHeadSqlStr = "select storeID, sum(salesAmount), updateUserID from billhead where " +
                                "accountdate = '"  + recalcDateString + " ' and billtype='W' and billstatus='1' " +
                                "group by storeID";
                        ArrayList billHead = getAllAmount(connection,billHeadSqlStr);//查询赊账
                        String rpHeadSqlSre = "select storeID,sum(invcAmount) , updateUserID from RP_head where " +
                                "accountdate= '" + recalcDateString + "' and billstatus='1'group by storeID";
                        ArrayList rpHead = getAllAmount(connection,rpHeadSqlSre);//查询团购结算
                        if (rpHead.size() != 0 || billHead.size() !=0)
                            impl.updateAccountBillHeadOrRPHead(connection, billHead, rpHead, accountingDate);

                        connection.commit();
                        return;
                    }

                    // 先避免posAmount和storeAmount有空值
                    executeQuery(connection, "UPDATE accdayrpt SET storeAmount=0 "
                            + "WHERE accountDate='" + recalcDateString
                            + "' AND storeAmount IS NULL");
                    executeQuery(connection, "UPDATE accdayrpt SET posAmount=0 "
                            + "WHERE accountDate='" + recalcDateString
                            + "' AND posAmount IS NULL");
                    // 把storeAmount先计成与原posAmount的差额(为了保留用户原来对storeAmount的修改)
                    executeQuery(connection,
                            "UPDATE accdayrpt SET storeAmount=storeAmount-posAmount "
                                    + "WHERE accountDate='" + recalcDateString
                                    + "'");
                    // 然后把posAmount归零
                    executeQuery(connection, "UPDATE accdayrpt SET posAmount=0 "
                            + "WHERE accountDate='" + recalcDateString + "'");
                    // 开始针对每一个该日的Z帐进行过转到accdayrpt的计算
                    while (iter.hasNext()) {
                        z = (ZReport) iter.next();

                        // 更新现金日报表
                        // Primary Key: storeID + accountDate + accountNumber +
                        // posNumber
                        String storeID = z.getStoreNumber();
                        int posNumber = z.getTerminalNumber().intValue();
                        SimpleDateFormat format = CreamCache.getInstance()
                                .getDateFormate();
                        String accDate = format.format(z.getAccountingDate())
                                .toString();
                        Statement statement = null;
                        ResultSet resultSet = null;
                        ArrayList table = null;
                        String select = "SELECT * FROM accdayrpt ";
                        String updateString = "UPDATE accdayrpt SET ";
                        String where = " WHERE storeID = '" + storeID
                                + "' AND accountDate = '" + accDate + "'";// AND
                        // posNumber
                        // = '"
                        // +
                        // posNumber
                        // +
                        // "'";
                        // CreamToolkit.logMessage("" + select + where);
                        try {
                            statement = connection.createStatement();
                            resultSet = statement.executeQuery(select + where);
                            table = constructFromResultSet(resultSet);
                            if (table.isEmpty()) {
                                Server
                                        .log("main(): Cannot find record in accdayrpt");
                                Server.log("Query : " + select + where);
                                returnValue = false;
                            }
                        } catch (SQLException e) {
                            Server
                                    .log("main(): Cannot find record in accdayrpt");
                            Server.log("SQLStatement: " + select + where);
                            e.printStackTrace(Server.getLogger());
                            returnValue = false;
                        } finally {
                            try {
                                if (statement != null)
                                    statement.close();
                            } catch (SQLException e) {
                                e.printStackTrace(Server.getLogger());
                            }
                            if (!returnValue) {
                                return ;
                            }
                        }

                        Map zRecord = z.getValues();
                        HashMap map = new HashMap();

                        // Bruce/2002-04-10
                        // map.put("001", z.getTransactionCount());
                        map.put("001", z.getCustomerCount());

                        map.put("002", z.getGrossSales());
                        map.put("004", z.getMixAndMatchTotalAmount());
                        map.put("006", z.getNetSalesTotalAmount());
                        map.put("201", z.getVoucherAmount());
                        map.put("202", z.getPreRcvAmount()); // 期刊预订
                        map.put("203", z.getPreRcvDetailAmount1()); // 团购结算
                        map.put("300", z.getDaiShouAmount2()); // Bruce/20030819
                        // 公共事业费代收金额
                        map.put("601", z.getSpillAmount());
                        Iterator itr = table.iterator();
                        while (itr.hasNext()) {
                            HashMap accdayrptRecord = (HashMap) itr.next();
                            String accountNumber = (String) accdayrptRecord
                                    .get("accountnumber");
                            String payID = (String) accdayrptRecord.get("payid");
                            if (payID == null || payID.length() == 0)
                                payID = "-1";
                            int cposNumber = Integer.parseInt(accdayrptRecord.get(
                                    "posnumber").toString());
                            if (cposNumber == posNumber) {
                                if (map.containsKey(accountNumber)) {
                                    HYIDouble amt = (HYIDouble) accdayrptRecord
                                            .get(POS_AMOUNT);
                                    HYIDouble mapAmt = new HYIDouble(0);
                                    if (map.get(accountNumber) instanceof Integer) {
                                        Integer in = (Integer) map
                                                .get(accountNumber);
                                        mapAmt = new HYIDouble(in.intValue());
                                    } else if (map.get(accountNumber) instanceof HYIDouble) {
                                        mapAmt = (HYIDouble) map.get(accountNumber);
                                    }
                                    if (amt == null)
                                        amt = new HYIDouble(0);
                                    amt = amt.addMe(mapAmt);
                                    accdayrptRecord.put(POS_AMOUNT, amt);
                                    HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                            .get(STORE_AMOUNT);
                                    if (sAmt == null) {
                                        accdayrptRecord.put(STORE_AMOUNT, amt);
                                    } else {
                                        accdayrptRecord.put(STORE_AMOUNT, sAmt
                                                .addMe(mapAmt));
                                    }
                                }
                            } else if (cposNumber == 0) {
                                if (map.containsKey(accountNumber)) {
                                    HYIDouble amt = (HYIDouble) accdayrptRecord
                                            .get(POS_AMOUNT);
                                    HYIDouble mapAmt = new HYIDouble(0);
                                    if (map.get(accountNumber) instanceof Integer) {
                                        Integer in = (Integer) map
                                                .get(accountNumber);
                                        mapAmt = new HYIDouble(in.intValue());
                                    } else if (map.get(accountNumber) instanceof HYIDouble) {
                                        mapAmt = (HYIDouble) map.get(accountNumber);
                                    }
                                    if (amt == null)
                                        amt = new HYIDouble(0);
                                    amt = amt.addMe(mapAmt);
                                    accdayrptRecord.put(POS_AMOUNT, amt);
                                    HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                            .get(STORE_AMOUNT);
                                    if (sAmt == null) {
                                        accdayrptRecord.put(STORE_AMOUNT, amt);
                                    } else {
                                        accdayrptRecord.put(STORE_AMOUNT, sAmt
                                                .addMe(mapAmt));
                                    }
                                } else if (accountNumber.startsWith("3")) {// 代收

                                    HYIDouble daiShouTotalAmount = (HYIDouble) accdayrptRecord
                                            .get(POS_AMOUNT);
                                    if (daiShouTotalAmount == null)
                                        daiShouTotalAmount = new HYIDouble(0);

                                    HYIDouble amt;
                                    if (accountNumber.equals("300")) { // Bruce/20030819
                                        // 公共事业费代收金额
                                        amt = z.getDaiShouAmount2();
                                    } else {
                                        if (payID.startsWith("0")
                                                && payID.length() > 1)
                                            payID = payID.substring(1);
                                        amt = (HYIDouble) zRecord
                                                .get("daiShouDetailAmount" + payID);
                                    }

                                    if (amt == null)
                                        amt = new HYIDouble(0);
                                    daiShouTotalAmount = daiShouTotalAmount
                                            .addMe(amt);

                                    accdayrptRecord.put(POS_AMOUNT,
                                            daiShouTotalAmount);
                                    HYIDouble storeAmt = (HYIDouble) accdayrptRecord
                                            .get(STORE_AMOUNT);
                                    if (storeAmt == null) {
                                        accdayrptRecord.put(STORE_AMOUNT,
                                                daiShouTotalAmount);
                                    } else {
                                        accdayrptRecord.put(STORE_AMOUNT, storeAmt
                                                .addMe(amt));
                                    }
                                } else if (accountNumber.startsWith("4")) {// 代付
                                    if (payID.startsWith("0") && payID.length() > 1)
                                        payID = payID.substring(1);
                                    HYIDouble amt = (HYIDouble) zRecord
                                            .get("daiFuDetailAmount" + payID);
                                    if (amt == null)
                                        amt = new HYIDouble(0);
                                    HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                            .get(POS_AMOUNT);
                                    if (amt2 == null)
                                        amt2 = new HYIDouble(0);
                                    amt2 = amt2.addMe(amt);
                                    accdayrptRecord.put(POS_AMOUNT, amt2);
                                    HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                            .get(STORE_AMOUNT);
                                    if (sAmt == null) {
                                        accdayrptRecord.put(STORE_AMOUNT, amt2);
                                    } else {
                                        accdayrptRecord.put(STORE_AMOUNT, sAmt
                                                .addMe(amt));
                                    }
                                } else if (accountNumber.startsWith("5")) {// 支付
                                    if (payID.length() < 2)
                                        payID += "0";
                                    HYIDouble amt = (HYIDouble) zRecord.get("pay"
                                            + payID + "Amount");
                                    if (amt == null)
                                        amt = new HYIDouble(0);
                                    HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                            .get(POS_AMOUNT);
                                    if (amt2 == null)
                                        amt2 = new HYIDouble(0);
                                    amt2 = amt2.addMe(amt);
                                    accdayrptRecord.put(POS_AMOUNT, amt2);
                                    HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                            .get(STORE_AMOUNT);
                                    if (sAmt == null) {
                                        accdayrptRecord.put(STORE_AMOUNT, amt2);
                                    } else {
                                        accdayrptRecord.put(STORE_AMOUNT, sAmt
                                                .addMe(amt));
                                    }
                                } else if (accountNumber.startsWith("7")) {// Paid-In
                                    int id = Integer.parseInt(payID);
                                    if (id < 0)
                                        continue;
                                    HYIDouble amt = (HYIDouble) zRecord
                                            .get("paidInDetailAmount" + id);
                                    if (amt == null)
                                        amt = new HYIDouble(0);
                                    HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                            .get(POS_AMOUNT);
                                    if (amt2 == null)
                                        amt2 = new HYIDouble(0);
                                    amt2 = amt2.addMe(amt);
                                    accdayrptRecord.put(POS_AMOUNT, amt2);
                                    HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                            .get(STORE_AMOUNT);
                                    if (sAmt == null) {
                                        accdayrptRecord.put(STORE_AMOUNT, amt2);
                                    } else {
                                        accdayrptRecord.put(STORE_AMOUNT, sAmt
                                                .addMe(amt));
                                    }
                                } else if (accountNumber.startsWith("8")) {// Paid-Out
                                    int id = Integer.parseInt(payID);
                                    if (id < 0)
                                        continue;
                                    HYIDouble amt = (HYIDouble) zRecord
                                            .get("paidOutDetailAmount"
                                                    + Integer.parseInt(payID));
                                    if (amt == null)
                                        amt = new HYIDouble(0);
                                    HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                            .get(POS_AMOUNT);
                                    if (amt2 == null)
                                        amt2 = new HYIDouble(0);
                                    amt2 = amt2.addMe(amt);
                                    accdayrptRecord.put(POS_AMOUNT, amt2);
                                    HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                            .get(STORE_AMOUNT);
                                    if (sAmt == null) {
                                        accdayrptRecord.put(STORE_AMOUNT, amt2);
                                    } else {
                                        accdayrptRecord.put(STORE_AMOUNT, sAmt
                                                .addMe(amt));
                                    }
                                } else if (accountNumber.startsWith("9")) {// 门市费用
                                    int id = Integer.parseInt(payID);
                                    if (id < 0)
                                        continue;
                                    HYIDouble amt = (HYIDouble) zRecord
                                            .get("paidOutDetailAmount"
                                                    + Integer.parseInt(payID));
                                    if (amt == null)
                                        amt = new HYIDouble(0);
                                    HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                            .get(POS_AMOUNT);
                                    if (amt2 == null)
                                        amt2 = new HYIDouble(0);
                                    amt2 = amt2.addMe(amt);
                                    accdayrptRecord.put(POS_AMOUNT, amt2);
                                    HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                            .get(STORE_AMOUNT);
                                    if (sAmt == null) {
                                        accdayrptRecord.put(STORE_AMOUNT, amt2);
                                    }
                                } else if (accountNumber.startsWith("101")) {
                                    int id = Integer.parseInt(payID);
                                    if (id < 0)
                                        continue;
                                    HYIDouble amt = (HYIDouble) zRecord
                                            .get("netSaleTax" + id + "Amount");
                                    if (amt == null)
                                        amt = new HYIDouble(0);
                                    HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                            .get(POS_AMOUNT);
                                    if (amt2 == null)
                                        amt2 = new HYIDouble(0);
                                    amt2 = amt2.addMe(amt);
                                    accdayrptRecord.put(POS_AMOUNT, amt2);
                                    HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                            .get(STORE_AMOUNT);
                                    if (sAmt == null) {
                                        accdayrptRecord.put(STORE_AMOUNT, amt2);
                                    } else {
                                        accdayrptRecord.put(STORE_AMOUNT, sAmt
                                                .addMe(amt));
                                    }
                                } else if (accountNumber.startsWith("102")) {
                                    int id = Integer.parseInt(payID);
                                    if (id < 0)
                                        continue;
                                    HYIDouble amt = (HYIDouble) zRecord
                                            .get("taxAmount" + id);
                                    if (amt == null)
                                        amt = new HYIDouble(0);
                                    HYIDouble amt2 = (HYIDouble) accdayrptRecord
                                            .get(POS_AMOUNT);
                                    if (amt2 == null)
                                        amt2 = new HYIDouble(0);
                                    // CreamToolkit.logMessage("" + accountNumber +
                                    // ":Original amount = " + amt2);
                                    // CreamToolkit.logMessage("" + accountNumber +
                                    // ":Additional amount = " + amt);
                                    amt2 = amt2.addMe(amt);
                                    accdayrptRecord.put(POS_AMOUNT, amt2);
                                    HYIDouble sAmt = (HYIDouble) accdayrptRecord
                                            .get(STORE_AMOUNT);
                                    if (sAmt == null) {
                                        accdayrptRecord.put(STORE_AMOUNT, amt2);
                                    } else {
                                        accdayrptRecord.put(STORE_AMOUNT, sAmt
                                                .addMe(amt));
                                    }
                                }
                            }
                            accdayrptRecord.put("updateUserID", z
                                    .getCashierNumber());
                            accdayrptRecord.put("updateDate", new java.util.Date());
                            String uWhere = " WHERE storeID = '" + storeID
                                    + "' AND accountDate = '" + accDate
                                    + "' AND posNumber = '" + cposNumber
                                    + "' AND accountNumber = '" + accountNumber
                                    + "'";
                            String fieldName = "";
                            String nameParams = "";
                            Object fieldValue = new Object();
                            Iterator iterator = accdayrptRecord.keySet().iterator();
                            while (iterator.hasNext()) {
                                fieldName = (String) iterator.next();
                                if (accdayrptRecord.get(fieldName) == null) {
                                    fieldValue = new Object();
                                } else {
                                    fieldValue = accdayrptRecord.get(fieldName);
                                    if (fieldValue instanceof java.util.Date) {
                                        SimpleDateFormat df = CreamCache
                                                .getInstance().getDateTimeFormate();
                                        java.util.Date d = (java.util.Date) fieldValue;
                                        nameParams += fieldName + "=" + "'"
                                                + df.format(d).toString() + "',";
                                    } else if (fieldValue instanceof String) {
                                        nameParams += fieldName + " = " + "'"
                                                + fieldValue + "',";
                                    } else {
                                        nameParams += fieldName + " = "
                                                + fieldValue + ",";
                                    }
                                }
                            }
                            String finalStr = updateString
                                    + nameParams.substring(0,
                                            nameParams.length() - 1) + "  "
                                    + uWhere;//
                            try {
                                statement = connection.createStatement();
                                statement.execute(finalStr);
                                statement.close();
                            } catch (SQLException e) {
                                Server.log("SQL statement: " + finalStr);
                                e.printStackTrace(Server.getLogger());
                                returnValue = false;
                            } finally {
                                try {
                                    if (statement != null)
                                        statement.close();
                                } catch (SQLException e) {
                                    e.printStackTrace(Server.getLogger());
                                }
                            }
                        }
                    } // Iterate ZReport loop
                } // synchronization block
                //将该日的团购销售总额和团购收款单的总额填入到现金日报表中
                String billHeadSqlStr = "select storeID, sum(salesAmount), updateUserID from billhead where " +
                        "accountdate = '"  + recalcDateString + " ' and billtype='W' and billstatus='1' " +
                        "group by storeID";
                ArrayList billHead = getAllAmount(connection,billHeadSqlStr);//查询赊账
                String rpHeadSqlSre = "select storeID,sum(invcAmount) , updateUserID from RP_head where " +
                        "accountdate= '" + recalcDateString + "' and billstatus='1'group by storeID";
                ArrayList rpHead = getAllAmount(connection,rpHeadSqlSre);//查询团购结算
                if (rpHead.size() != 0 || billHead.size() !=0)
                    impl.updateAccountBillHeadOrRPHead(connection, billHead, rpHead, accountingDate);

                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
                Server.log("main() failed ");
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
        }
    }

    //
    // public boolean afterReceivingHistoryTrans(Object[] hts) {
    // boolean success = true;
    // java.util.Date accdate = getAccountingDate();
    //
    // for (int i = 0; i < hts.length; i++) {
    // HistoryTrans ht = (HistoryTrans)hts[i];
    //
    // Date accountingDate = ht.getAccdate();
    // if (accountingDate == null ||
    // ((yyyy_MM_dd.format(accountingDate)).startsWith("197"))) {
    // accountingDate = accdate;
    // ht.setAccdate(accountingDate);
    // }
    //
    // ht.deleteByPrimaryKey();
    // if (!ht.insert(false)) {
    // Server.log("HistoryTrans inserted failed (storeID=" + ht.getStoreid() +
    // ",Systemdatetime=" + ht.getSystemdatetime() +
    // ",Terminalnumber=" + ht.getTerminalnumber() +
    // ",trannumber=" + ht.getPrinttrannumber() +
    // ").");
    // success = false;
    // } else {
    // Server.log("HistoryTrans inserted (storeID=" + ht.getStoreid() +
    // ",Systemdatetime=" + ht.getSystemdatetime() +
    // ",Terminalnumber=" + ht.getTerminalnumber() +
    // ",trannumber=" + ht.getPrinttrannumber() +
    // ").");
    // }
    // }
    // return success;
    // }
    //
    // /*
    // *
    // *
    // */
    // public boolean afterReceivingPosVersion(Object[] posVersions) {
    // boolean success = true;
    // for (int i = 0; i < posVersions.length; i++) {
    // try{
    // Version posVersion = (Version)posVersions[i];
    //
    // posVersion.deleteByPrimaryKey();
    // if (!posVersion.insert(false)) {
    // Server.log("posVersion inserted failed (version=" +
    // posVersion.getVersion() +
    // ").");
    // success = false;
    // } else {
    // Server.log("posVersion inserted (version=" + posVersion.getVersion() +
    // ").");
    // }
    // }catch(Exception e2){
    // e2.printStackTrace(Server.getLogger());
    // };
    // }
    // return success;
    // }

}
