package hyi.cream;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * POSTerminalApplication class preloader.
 * 
 * @author Bruce You
 */
public class Bootstrapper
{
    public static void main(String[] args)
    {
        ProgressBar progressBar = new ProgressBar();
        progressBar.setVisible(true);

        // Preload all the necessary classes, updating the progress bar as
        // they are loaded.
        loadClass(progressBar);

        progressBar.dispose();
        POSTerminalApplication.main(args);
    }

    private static void loadClass(ProgressBar progressBar)
    {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream("conf/preload_classes.conf")));

            // read classes count at first line
            String line = br.readLine();
            progressBar.setMax(Integer.parseInt(line));

            // loading classes
            while ((line = br.readLine()) != null) {
                try {
                    Class.forName(line.split("\\s")[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    progressBar.updateProgress();
                }
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
