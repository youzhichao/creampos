package hyi.cream.util;

import hyi.cream.POSTerminalApplication;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBToolkit {

    private static boolean isAllowedToUpdate(String sql) {
        // not allow to do any update in training mode
        return sql.startsWith("select") || sql.startsWith("SELECT") ||
            !POSTerminalApplication.getInstance().getTrainingMode();
    }

	public static boolean executeUpdate(String sql) {
        if (!isAllowedToUpdate(sql))
            return true;    // pretend to success

		boolean result = true;
		DbConnection connection = null;
		Statement statement = null;
		try {
			connection = CreamToolkit.getPooledConnection();
			statement = connection.createStatement();
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			result = false;
			CreamToolkit.logMessage("SQLException: " + sql);
			e.printStackTrace(CreamToolkit.getLogger());
		} finally {
    		try {
    			if (statement != null)
    				statement.close();
    		} catch (SQLException e) {
    			result = false;
    			e.printStackTrace(CreamToolkit.getLogger());
    		}
			CreamToolkit.releaseConnection(connection);
        }
		return result;
	}

    /**
     * Execute a SQL statement within a database transaction.
     */
    public static boolean savedToDB = false;
    public static void execute(Connection connection, String sql) throws SQLException {
        if (!isAllowedToUpdate(sql))
            return;    // pretend to success

        try {
            Statement statement = connection.createStatement();
            statement.execute(sql);
            statement.close();
            savedToDB = true;
        } catch (SQLException e) {
        	e.printStackTrace();
        	e.printStackTrace(CreamToolkit.getLogger());
            CreamToolkit.logMessage("--ERROR : " + sql);
            savedToDB = false;
            throw e;
        }
    }

    /**
     * Execute a SQL statement within a database transaction.
     */
    public static int executeUpdate(Connection connection, String sql) throws SQLException {
        if (!isAllowedToUpdate(sql))
            return 1;    // pretend to success

        Statement statement = connection.createStatement();
        //CreamToolkit.logMessage("DBToolkit.executeUpdate>" + sql);
        int recCount = statement.executeUpdate(sql);
        statement.close();
        return recCount;
    }

    public static boolean execute(String sql) {
        if (!isAllowedToUpdate(sql))
            return true;    // pretend to success

		boolean result = true;
		DbConnection connection = null;
		Statement statement = null;
		try {
			connection = CreamToolkit.getPooledConnection();
			statement = connection.createStatement();
			statement.execute(sql);
		} catch (SQLException e) {
			result = false;
			CreamToolkit.logMessage("SQLException: " + sql);
			e.printStackTrace(CreamToolkit.getLogger());
		} finally {
    		try {
    			if (statement != null)
    				statement.close();
    		} catch (SQLException e) {
    			result = false;
    			e.printStackTrace(CreamToolkit.getLogger());
    		}
			CreamToolkit.releaseConnection(connection);
        }
		return result;
	}
    
    public static Map query(Connection connection, String selectStatement) throws SQLException {
        ResultSet resultSet = null;
        HashMap map = new HashMap();

        Statement statement = connection.createStatement();
        resultSet = statement.executeQuery(selectStatement);
        ResultSetMetaData metaData = resultSet.getMetaData();
        if (resultSet.next()) {
			int t = 0;
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
				Object o = resultSet.getObject(i);
				if (o != null) {
					t = metaData.getColumnType(i);
					if (t == Types.NUMERIC || t == Types.DECIMAL) {
						o = new HYIDouble(((BigDecimal)o).doubleValue());
					}
                    map.put(metaData.getColumnName(i).toLowerCase(), o);
				}
            }
        }
        if (resultSet != null)
            resultSet.close();
        if (statement != null)
            statement.close();
        return map;
    }

    public static List<Map> query2(Connection connection, String selectStatement) throws SQLException {
    	List<Map> datas = new ArrayList<Map>();
        ResultSet resultSet = null;
        Statement statement = connection.createStatement();
        resultSet = statement.executeQuery(selectStatement);
        ResultSetMetaData metaData = resultSet.getMetaData();
        while (resultSet.next()) {
            HashMap map = new UppercaseKeyMap();
			int t = 0;
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
				Object o = resultSet.getObject(i);
				if (o != null) {
					t = metaData.getColumnType(i);
					if (t == Types.NUMERIC || t == Types.DECIMAL) {
						o = new HYIDouble(((BigDecimal)o).doubleValue());
					}
                    map.put(metaData.getColumnName(i).toLowerCase(), o);
				}
            }
            datas.add(map);
        }
        if (resultSet != null)
            resultSet.close();
        if (statement != null)
            statement.close();
        return datas;
    }
}
