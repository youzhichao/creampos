package hyi.cream.util;

//import java.math.*;
//import java.io.*;
//import jpos.*;
//import java.text.*;

import hyi.cream.dac.CashForm;
import hyi.cream.dac.Inventory;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.ShiftReport;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.ZReport;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.state.State;
import hyi.spos.JposException;

import java.sql.SQLException;
import java.util.ResourceBundle;

public abstract class CreamPrinter {

	protected static int ASCII_SEPARATOR = 128;
	private boolean headerPrinted;
	protected int currentLine = 1;
	protected int currentPage = 1;
	protected boolean printEnabled = true;
	protected ResourceBundle res;
	protected HYIDouble subTotal = new HYIDouble(0);
	protected HYIDouble total = new HYIDouble(0);
    public boolean isPrintToConsoleInsteadOfPrinter = false;

	public static CreamPrinter getInstance() {
        return GenericCreamPrinter.getInstance_zh_CN();
	}

    /** 印表機是否已定位. */
    abstract public boolean isPrinterAtRightStart();

    /** 印表機是否連接正常. */
    abstract public boolean isPrinterHealthy();

    abstract public void printStamp();
	abstract public void printHeader(DbConnection connection);
	abstract public void printHeader(DbConnection connection, Transaction p0);
	abstract public void printLineItem(DbConnection connection, LineItem p0) throws SQLException;
    abstract public void printLineItem(DbConnection connection, Transaction tran, LineItem lineItem) throws SQLException;
	abstract public void printSubtotal(DbConnection connection) throws SQLException;
    abstract public void printPayment(DbConnection connection, Transaction p0) throws SQLException;
    abstract public void printCancel(DbConnection connection, String cancelMsg) throws SQLException;
    
    abstract public void reprint(DbConnection connection, int number) throws SQLException;
    abstract public void reprint(DbConnection connection, Transaction p0) throws SQLException;

    abstract public void printCashForm(CashForm cf);
	abstract public void printZReport(ZReport p0, State p1); 
	abstract public void printXReport(ZReport p0, State p1);
	abstract public void printShiftReport(ShiftReport p0, State p1);

	abstract public void printFirstReceipt();
	abstract public void printSlip(State p0);

	//Properties
	public boolean getHeaderPrinted () {
		return headerPrinted;
	}

    public void setHeaderPrinted(boolean headerPrinted) {
		this.headerPrinted = headerPrinted;
	}

	public boolean getPrintEnabled() {
		return printEnabled;
	}

	public void setPrintEnabled(boolean printEnabled) {
		this.printEnabled = printEnabled;
	}

	public int getCurrentLine(){
		return currentLine;
	}

	public void setCurrentLine(int currentLine){
        this.currentLine = currentLine;
	}

	public int getCurrentPage(){
		return currentPage;
    }

    public void setCurrentPage(int currentPage){
        this.currentPage = currentPage;
	}
	
	public HYIDouble getSubTotal(){
        return subTotal;
    }

	public void setSubTotal (HYIDouble subTotal){
        this.subTotal = subTotal;
	}

	public HYIDouble getTotal(){
		return total;
    }

	public void setTotal (HYIDouble total){
		this.total = total;
	}

    public void printInventoryHeader() {
    }

    public void printInventoryItem(Inventory inv) {
    }

    public void printInventoryFooter() {
    }

	public void tryPrint()  throws NoSuchPOSDeviceException, JposException {
	}
}
