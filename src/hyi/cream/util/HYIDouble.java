package hyi.cream.util;

/*
 * Created on 2004-5-24
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

import java.io.Serializable;
import java.math.*;
import java.text.*;
import java.io.*;

/**
 * @author pyliu
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class HYIDouble implements Serializable,Comparable {

    /**
     * The value of the Double.
     *
     * @serial
     */
    private double value;

    /**
     * The scale of this HYIDouble. Only marked in construction.
     *
     * @serial
     * @see #scale
     */
    private int scale = 0;

    /**
     * The minmium scale of this HYIDouble.
     *
     */
    private final static double MIN_SCALE = 0.0000001;

    /**
     * Calculate scale of a double value.
     *
     * @param   value   the value needed to get scale.
     */
    private static int calScale(double val) {
        String s;
        if (val < MIN_SCALE && val > -MIN_SCALE) {
            s = "0";//otherwise, it will be "0.0", that is not what we want
        } else {
            s = String.valueOf(val);
        }
        return calScale(s);
    }

    private static int calScale(String val) {
        int pointPos = val.indexOf('.');
        int ascale = 0;
        if (pointPos == -1) {             /* e.g. "123" */
        //} else if (pointPos == val.length()-1) { /* e.g. "123." */
        } else {    /* Fraction part exists */
            ascale = val.length() - pointPos - 1;
        }
        return ascale;
    }

    /**
     * Constructs a newly allocated <code>HYIDouble</code> object that
     * clones the primitive HYIDouble.
     *
     * @param   value   the value to be represented by the <code>HYIDouble</code>.
     * @param   ascale  the scale to be represented by the <code>HYIDouble</code>.
     */
    private HYIDouble(double v, int ascale) {
        this.value = v;
        this.scale = ascale;
    }

    public Object clone() {
        return new HYIDouble(value, this.scale);
    }

    /**
     * Constructs a newly allocated <code>HYIDouble</code> object that
     * represents the primitive <code>double</code> argument.
     *
     * @param   value   the value to be represented by the <code>HYIDouble</code>.
     */
    public HYIDouble(double v) {
        this.value = v;
        this.scale = calScale(this.value);
    }

    /**
     * Constructs a newly allocated <code>HYIDouble</code> object that
     * represents the floating- point value of type <code>double</code>
     * represented by the string.
     *
     * @param      s   a string to be converted to a <code>HYIDouble</code>.
     * @exception  NumberFormatException  if the string does not contain a
     *               parsable number.
     */
    public HYIDouble(String s) throws NumberFormatException {
    // REMIND: this is inefficient
        this(Double.parseDouble(s));
        this.scale = calScale(s);
    }

    /**
     * Translates a BigInteger unscaled value and an int scale into a
     * HYIDouble.  The value of the HYIDouble is
     * <tt>(unscaledVal/10<sup>scale</sup>)</tt>.
     *
     * @param unscaledVal unscaled value of the HYIDouble.
     * @param ascale scale of the HYIDouble.
     * @throws NumberFormatException scale is negative
     */
    public HYIDouble(BigInteger unscaledVal, int ascale) {
        if (ascale < 0)
            throw new NumberFormatException("Negative scale");
        this.scale = ascale;
        value = unscaledVal.doubleValue();
        while (ascale-- > 0) {
            value /= 10;
        }
    }

    /**
     * Reset this HYIDouble.
     *
     * @param      v   a double to reset the vaule of this <code>HYIDouble</code>.
     * @return the old double value of this HYIDouble.
     */
    public double reset(double v) {
        double oldValue = this.value;
        this.value = v;
        this.scale = calScale(this.value);
        return oldValue;
    }

    /**
     * Converts this HYIDouble to an int.  Standard <i>narrowing primitive
     * conversion</i> as defined in <i>The Java Language Specification</i>:
     * any fractional part of this HYIDouble will be discarded, and if the
     * resulting "BigInteger" is too big to fit in an int, only the low-order
     * 32 bits are returned.
     *
     * @return this HYIDouble converted to an int.
     */
    public int intValue(){
        int result = (int)(value >= 0 ? Math.floor(value) : Math.ceil(value));
        return result;
    }

    /**
     * Returns the double value of this HYIDouble.
     *
     * @return  the <code>double</code> value represented by this object.
     */
    public double doubleValue() {
        double v = trimScale(this.value, this.scale, BigDecimal.ROUND_HALF_UP);
        return v;
    }

    /**
     * Returns a HYIDouble whose value is <tt>(this + val)</tt>
     *
     * @param  val value to be added to this HYIDouble.
     * @return <tt>this + val</tt>
     */
    public HYIDouble add(HYIDouble val){
        HYIDouble n = (HYIDouble)(this.clone());
        n.value += val.value;
        n.scale = Math.max(this.scale, val.scale);
        this.scale = n.scale;
        val.scale = n.scale;
        return n;
    }
//    public HYIDouble addMe(HYIDouble val) {
//        return add(val);
//    }

    public HYIDouble addMe(HYIDouble val){
        this.value += val.value;
        this.scale = Math.max(this.scale, val.scale);
        val.scale = this.scale;
        return this;
    }

    /**
     * Returns a HYIDouble whose value is <tt>(this - val)</tt>
     *
     * @param  val value to be subtracted to this HYIDouble.
     * @return <tt>this - val</tt>
     */
    public HYIDouble subtract(HYIDouble val){
        HYIDouble n = (HYIDouble)(this.clone());
        n.value -= val.value;
        n.scale = Math.max(this.scale, val.scale);
        this.scale = n.scale;
        val.scale = n.scale;
        return n;
    }

    public HYIDouble subtractMe(HYIDouble val){
        this.value -= val.value;
        this.scale = Math.max(this.scale, val.scale);
        val.scale = this.scale;
        return this;
    }

    /**
     * Returns a HYIDouble whose value is <tt>(this * val)</tt>
     *
     * @param  val value to be multiplied by this HYIDouble.
     * @return <tt>this * val</tt>
     */
    public HYIDouble multiply(HYIDouble val){
        HYIDouble n = (HYIDouble)(this.clone());
        n.value *= val.value;
        n.scale += val.scale;
        return n;
    }

    public HYIDouble multiplyMe(HYIDouble val){
        this.value *= val.value;
        this.scale += val.scale;
        return this;
    }

    /**
     * Returns a HYIDouble whose value is <tt>(this / val)</tt>
     *
     * @param  val value to be multiplied by this HYIDouble.
     * @return <tt>this / val</tt>
     */
    public HYIDouble divide(HYIDouble val, int ascale, int roundingMode){
        HYIDouble n = (HYIDouble)(this.clone());
        n.value /= val.value;
        return n.setScaleMe(ascale, roundingMode);
    }

    public HYIDouble divideMe(HYIDouble val, int ascale, int roundingMode) {
        this.value /= val.value;
        return setScaleMe(ascale, roundingMode);
    }

    /**
     * Returns a HYIDouble whose value is <tt>(this / val)</tt>
     *
     * @param  val value to be multiplied by this HYIDouble.
     * @return <tt>this / val</tt>
     */
    public HYIDouble divide(HYIDouble val, int roundingMode) {
        return divide(val, this.scale, roundingMode);
    }

    public HYIDouble divideMe(HYIDouble val, int roundingMode) {
        return divideMe(val, this.scale, roundingMode);
    }

    /**
     * Returns a new HYIDouble whose value is <tt>(-this)</tt>
     *
     * @return <tt>-this</tt>
     */
    public HYIDouble negate() {
        HYIDouble n = (HYIDouble)(this.clone());
        n.value = -value;
        return n;
    }

    public HYIDouble negateMe() {
        value = -value;
        return this;
    }

    /**
     * Returns the signum function of this HYIDouble.
     *
     * @return -1, 0 or 1 as the value of this HYIDouble is negative, zero or
     *           positive.
     */
    public int signum() {
        return (value > 0 ? 1 : (value == 0) ? 0 : -1);
    }

    /**
     * Returns a HYIDouble whose value is the absolute value of this
     * HYIDouble.
     *
     * @return <tt>abs(this)</tt>
     */
    public HYIDouble abs(){
        return (signum() < 0 ? negate() : (HYIDouble)this.clone());
    }

    public HYIDouble absMe(){
         return (signum() < 0 ? negateMe() : this);
    }

    /**
     * Returns the <i>scale</i> of this HYIDouble.  (The scale is the number
     * of digits to the right of the decimal point.)
     *
     * @return the scale of this HYIDouble.
     */
    public int scale() {
        return this.scale;
    }

    /**
     * Returns a HYIDouble which is equivalent to this one with the decimal
     * point moved n places to the left.  If n is non-negative, the call merely
     * adds n to the scale.  If n is negative, the call is equivalent to
     * movePointRight(-n).  (The HYIDouble returned by this call has value
     * <tt>(this * 10<sup>-n</sup>)</tt> and scale
     * <tt>max(this.scale()+n, 0)</tt>.)
     *
     * @param  n number of places to move the decimal point to the left.
     * @return a HYIDouble which is equivalent to this one with the decimal
     *           point moved <tt>n</tt> places to the left.
     */
    public HYIDouble movePointLeft(int n) {
        HYIDouble r = (HYIDouble)(this.clone());
        if (n > 0) {
            r.scale += n;
            while (n-- > 0) r.value /= 10;
            return r;
        } else if (n == 0) {
            return r;
        } else {
            return r.movePointRightMe(-n);
        }
    }

    public HYIDouble movePointLeftMe(int n) {
        if (n > 0) {
            this.scale += n;
            while (n-- > 0) value /= 10;
            return this;
        } else if (n == 0) {
            return this;
        } else {
            return movePointRightMe(-n);
        }
    }

    public HYIDouble movePointRight(int n) {
        HYIDouble r = (HYIDouble)(this.clone());
        if (n > 0) {
            r.scale -= n;
            r.scale = r.scale > 0 ? r.scale : 0;
            while (n-- > 0) r.value *= 10;
            return r;
        } else if (n == 0) {
            return r;
        } else {
            return r.movePointLeftMe(-n);
        }
    }

    public HYIDouble movePointRightMe(int n) {
        if (n > 0) {
            this.scale -= n;
            this.scale = this.scale > 0 ? this.scale : 0;
            while (n-- > 0) value *= 10;
            return this;
        } else if (n == 0) {
            return this;
        } else {
            return movePointLeftMe(-n);
        }
    }
    /**
     * Returns a HYIDouble whose scale is the specified value, and whose
     * unscaled value is determined by multiplying or dividing this
     * HYIDouble's unscaled value by the appropriate power of ten to maintain
     * its overall value.  If the scale is reduced by the operation, the
     * unscaled value must be divided (rather than multiplied), and the value
     * may be changed; in this case, the specified rounding mode is applied to
     * the division.
     *
     * @param  scale scale of the HYIDouble value to be returned.
     * @param  roundingMode The rounding mode to apply.
     * @return a HYIDouble whose scale is the specified value, and whose
     *           unscaled value is determined by multiplying or dividing this
     *            HYIDouble's unscaled value by the appropriate power of ten to
     *           maintain its overall value.
     * @see    BigDecimal#ROUND_UP
     * @see    BigDecimal#ROUND_DOWN
     * @see    BigDecimal#ROUND_CEILING
     * @see    BigDecimal#ROUND_FLOOR
     * @see    BigDecimal#ROUND_HALF_UP
     * @see    BigDecimal#ROUND_HALF_DOWN
     * @see    BigDecimal#ROUND_HALF_EVEN
     * @see    BigDecimal#ROUND_UNNECESSARY
     */
    public HYIDouble setScale(int ascale, int roundingMode) {
        if (ascale < 0)
            throw new NumberFormatException("Negative scale");
        HYIDouble n = (HYIDouble)this.clone();
        if (ascale < n.scale) {
            n.value = trimScale(n.value, ascale, roundingMode);
        }
        n.scale = ascale;
        return n;
    }

    public HYIDouble setScaleMe(int ascale, int roundingMode) {
        if (ascale < 0)
            throw new NumberFormatException("Negative scale");
        if (ascale < this.scale) {
            this.value = trimScale(value, ascale, roundingMode);
        }
        this.scale = ascale;
        return this;
    }

    private static double trimScale(double v, int ascale, int roundingMode) {
        int orgScale = calScale(v);
        double val = v;
        if (ascale < orgScale) {
            int factor = 1;
            while (ascale-- > 0) {
                factor *= 10;
            }
            val = v * factor;
            val = round(val, roundingMode);
            val /= factor;
        }
        return val;
    }

    /**
     * Returns the rounded double value of (X.0) of a double value.
     *
     * @return rounded double value.
     */
    private static double round(double d, int roundingMode) {
        double r;
        if (d == 0) {
            r = d;
        } else if (roundingMode == BigDecimal.ROUND_UNNECESSARY) {
//Rounding mode to assert that the requested operation has an exact result, hence no rounding is necessary. If this rounding mode is specified on an operation that yields an inexact result, an ArithmeticException is thrown.
            r = (int)d;
        } else if (roundingMode == BigDecimal.ROUND_UP) {
//Rounding mode to round away from zero. Always increments the digit prior to a non-zero discarded fraction. Note that this rounding mode never decreases the magnitude of the calculated value.
            r = (d > 0) ? Math.ceil(d) : Math.floor(d);
        } else if (roundingMode == BigDecimal.ROUND_DOWN) {
//Rounding mode to round towards zero. Never increments the digit prior to a discarded fraction (i.e., truncates). Note that this rounding mode never increases the magnitude of the calculated value.            //r =
            r = (d > 0) ? Math.floor(d) : Math.ceil(d);
        } else if (roundingMode == BigDecimal.ROUND_CEILING) {
//Rounding mode to round towards positive infinity. If the BigDecimal is positive, behaves as for ROUND_UP; if negative, behaves as for ROUND_DOWN. Note that this rounding mode never decreases the calculated value.
            r = Math.ceil(d);
        } else if (roundingMode == BigDecimal.ROUND_FLOOR) {
//Rounding mode to round towards negative infinity. If the BigDecimal is positive, behave as for ROUND_DOWN; if negative, behave as for ROUND_UP. Note that this rounding mode never increases the calculated value.
            r = Math.floor(d);
        } else if (roundingMode == BigDecimal.ROUND_HALF_UP) {
//Rounding mode to round towards "nearest neighbor" unless both neighbors are equidistant, in which case round up. Behaves as for ROUND_UP if the discarded fraction is >= .5; otherwise, behaves as for ROUND_DOWN. Note that this is the rounding mode that most of us were taught in grade school.
            r = (d > 0) ? Math.round(d) : -Math.round(0 - d);
        } else if (roundingMode == BigDecimal.ROUND_HALF_DOWN) {
//Rounding mode to round towards "nearest neighbor" unless both neighbors are equidistant, in which case round down. Behaves as for ROUND_UP if the discarded fraction is > .5; otherwise, behaves as for ROUND_DOWN.
            double frac = Math.abs(d);
            frac = frac - (int)frac;
            if (frac > 0.5) {
                r = (d > 0) ? Math.ceil(d) : Math.floor(d);
            } else {
                r = (d > 0) ? Math.floor(d) : Math.ceil(d);
            }
        } else if (roundingMode == BigDecimal.ROUND_HALF_EVEN) {
//Rounding mode to round towards the "nearest neighbor" unless both neighbors are equidistant, in which case, round towards the even neighbor. Behaves as for ROUND_HALF_UP if the digit to the left of the discarded fraction is odd; behaves as for ROUND_HALF_DOWN if it's even. Note that this is the rounding mode that minimizes cumulative error when applied repeatedly over a sequence of calculations.
            r = Math.rint(d);
        } else {
            throw new NumberFormatException("No such rounding mode");
        }
        return r;
    }

    /**
     * Returns a HYIDouble whose scale is the specified value, and whose
     * value is numerically equal to this HYIDouble's.  Throws an
     * ArithmeticException if this is not possible.  This call is typically
     * used to increase the scale, in which case it is guaranteed that there
     * exists a HYIDouble of the specified scale and the correct value.  The
     * call can also be used to reduce the scale if the caller knows that the
     * HYIDouble has sufficiently many zeros at the end of its fractional
     * part (i.e., factors of ten in its integer value) to allow for the
     * rescaling without loss of precision.
     * <p>
     * Note that this call returns the same result as the two argument version
     * of setScale, but saves the caller the trouble of specifying a rounding
     * mode in cases where it is irrelevant.
     *
     * @param  scale scale of the HYIDouble value to be returned.
     * @return a HYIDouble whose scale is the specified value, and whose
     *           unscaled value is determined by multiplying or dividing this
     *            HYIDouble's unscaled value by the appropriate power of ten to
     *           maintain its overall value.
     * @see    #setScale(int, int)
     */
    public HYIDouble setScale(int ascale) {
        return setScale(ascale, BigDecimal.ROUND_UNNECESSARY);
    }

    public HYIDouble setScaleMe(int ascale) {
        return setScaleMe(ascale, BigDecimal.ROUND_UNNECESSARY);
    }

    /**
     * Returns a hashcode for this <code>HYIDouble</code> object.
     *
     * @return  a <code>hash code</code> value for this object.
     */
    public int hashCode() {
        double v = trimScale(value, this.scale, BigDecimal.ROUND_HALF_UP);
        long bits = Double.doubleToLongBits(v);
        return (int)(bits ^ (bits >>> 32));
    }

    public BigDecimal toBigDecimal() {
        return new BigDecimal(doubleValue());
    }

    /**
     * Returns a String representation of this HYIDouble object.
     * The primitive <code>double</code> value represented by this
     * object is converted to a string exactly as if by the method
     * <code>toString</code> of one argument.
     *
     * @return  a <code>String</code> representation of this object.
     */
    public String toString() {
        String s;
        int ascale = calScale(value);
        if (this.scale > ascale) {
            s = addZero(this.scale, this.value);
        } else if (this.scale < ascale) {
            double vt = trimScale(value, this.scale, BigDecimal.ROUND_HALF_UP);
            if (this.scale == 0) {
                s = String.valueOf((int)vt);
            } else {
                int newascale = calScale(vt);
                if (this.scale > newascale) {
                    s = addZero(this.scale, vt);
                } else {
                    s = String.valueOf(vt);
                }
            }
        } else {
            if (this.scale == 0 && value < MIN_SCALE && value > -MIN_SCALE) {
                s = "0";
            } else {
                s = String.valueOf(value);
            }
        }
        return s;
    }

    private String addZero(int ascale, double val) {
        StringBuffer pattern = new StringBuffer("#0.");
        while (ascale-- > 0) {
            pattern.append('0');
        }
        DecimalFormat df = new DecimalFormat();
        StringBuffer result = new StringBuffer();
        FieldPosition fieldPosition = new FieldPosition(0);
        df.applyPattern(pattern.toString());
        df.format(val, result, fieldPosition);
        return result.toString();
    }
    /**
     * Compares two HYIDoubles numerically.
     *
     * @param   anotherDouble   the <code>HYIDouble</code> to be compared.
     * @return  the value <code>0</code> if <code>anotherHYIDouble</code> is
     *        numerically equal to this HYIDouble; a value less than
     *          <code>0</code> if this HYIDouble is numerically less than
     *        <code>anotherHYIDouble</code>; and a value greater than
     *        <code>0</code> if this HYIDouble is numerically greater than
     *        <code>anotherHYIDouble</code>.
     *
     * @see     Comparable#compareTo(Object)
     */
    public int compareTo(HYIDouble anotherHYIDouble) {
        double thisVal = trimScale(value, this.scale, BigDecimal.ROUND_HALF_UP);
        double anotherVal = trimScale(anotherHYIDouble.value, anotherHYIDouble.scale, BigDecimal.ROUND_HALF_UP);

        if (thisVal < anotherVal)
            return -1;         // Neither val is NaN, thisVal is smaller
        else if (thisVal > anotherVal)
            return 1;         // Neither val is NaN, thisVal is larger
        else
            return 0;       // (0.0, -0.0) or (NaN, !NaN)

        /*
        long thisBits = Double.doubleToLongBits(thisVal);
        long anotherBits = Double.doubleToLongBits(anotherVal);

        return (thisBits == anotherBits ?  0 : // Values are equal
                (thisBits < anotherBits ? -1 : // (-0.0, 0.0) or (!NaN, NaN)
                 1));                          // (0.0, -0.0) or (NaN, !NaN)
        */
    }

    /**
     * Compares this HYIDouble to another Object.  If the Object is a HYIDouble,
     * this function behaves like <code>compareTo(HYIDouble)</code>.  Otherwise,
     * it throws a <code>ClassCastException</code> (as HYIDoubles are comparable
     * only to other HYIDoubles).
     *
     * @param   o the <code>Object</code> to be compared.
     * @return  the value <code>0</code> if the argument is a HYIDouble
     *        numerically equal to this HYIDouble; a value less than
     *        <code>0</code> if the argument is a HYIDouble numerically
     *        greater than this HYIDouble; and a value greater than
     *        <code>0</code> if the argument is a HYIDouble numerically
     *        less than this HYIDouble.
     * @exception <code>ClassCastException</code> if the argument is not a
     *          <code>Double</code>.
     * @see     java.lang.Comparable
     */
    public int compareTo(Object o) {
        return compareTo((HYIDouble)o);
    }

    /**
     * Compares this HYIDouble with the specified Object for equality.
     * Unlike compareTo, this method considers two HYIDouble equal only
     * if they are equal in value and scale (thus 2.0 is not equal to 2.00
     * when compared by this method).
     *
     * @param  x Object to which this HYIDouble is to be compared.
     * @return <tt>true</tt> if and only if the specified Object is a
     *           HYIDouble whose value and scale are equal to this HYIDouble's.
     * @see    #compareTo(HYIDouble)
     */
    public boolean equals(Object x){
        if (!(x instanceof HYIDouble))
            return false;
        HYIDouble xDec = (HYIDouble) x;
        //if (this.scale != xDec.scale) return false;
        int r = this.compareTo(xDec);
        return (r == 0);
    }

    /**
     * Save the state of the <tt>HYIDouble</tt> instance to a stream (i.e.,
     * serialize it).
     *
     */
    private void writeObject(java.io.ObjectOutputStream s)
        throws IOException {
        // Write out the threshold, loadfactor, and any hidden stuff
        s.defaultWriteObject();

        // Write out scale
        s.writeInt(scale);

        // Write out value
        s.writeDouble(value);
    }

    private static final long serialVersionUID = 1;

    /**
     * Reconstitute the <tt>HYIDouble</tt> instance from a stream (i.e.,
     * deserialize it).
     */
    private void readObject(java.io.ObjectInputStream s)
         throws IOException, ClassNotFoundException {
        // Read in the threshold, loadfactor, and any hidden stuff
        s.defaultReadObject();

        // Read scale;
        scale = s.readInt();

        // Read value;
        value = s.readDouble();
    }

    public static HYIDouble zero() {
        return new HYIDouble(0.00);
    }

//    public static void main(String[] args) {
//        HYIDouble h1 = new HYIDouble(0.00);
//        HYIDouble h2 = new HYIDouble(0.00);
//        BigDecimal b = new BigDecimal(0.00);
//        //h1.divide(h2, BigDecimal.ROUND_HALF_UP);
//        //h1.multiply(h2);
//        //h1.negate();
//        //h1.abs();
//        //h1.movePointLeft(-1);
//        System.out.println(h1);
//        System.out.println(h2);
//        System.out.println(8.99*3.678);
//    }
}
