package hyi.cream.util;


/**
 * @author CStore
 */
public class NumberConfuser {

	public NumberConfuser() {
	}

	/**
	 * 把传过来的字符串顺序打乱，如果输入的字符串长度不足8位
     * 前面用"0"补足
	 * 
	 * @param 需要打乱的字符串
	 * @return 打乱以后的字符串
	 */
	public static String confuse(String inputStr) {
		int sum = 0;
		StringBuffer target = new StringBuffer();

        if(inputStr.length() < 8) {
            inputStr = "0000000" + inputStr;
        }
        
        inputStr = inputStr.substring(inputStr.length()-8);
          
		for (int i = 0; i < inputStr.length(); i++) {
			sum += Integer.parseInt(inputStr.substring(i, i + 1));
		}
		
        sum = sum % 10;

		switch (sum) {
			case 0 :
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(3));
                  
        		break;
			case 1 :
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(5));
				break;
			case 2 :
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
				break;
			case 3 :
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
				break;
			case 4 :
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(3));
				break;
			case 5 :
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
				break;
			case 6 :
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(3));
				break;
			case 7 :
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(5));
				break;
			case 8 :
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
				break;
			case 9 :
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
				break;
			default :
				target = null;
				break;
		}
		return target.toString();
	}

	/**
	 * 把传过来的打乱过的字符串顺序还原
	 * 
	 * @param 打乱的字符串
	 * @return 还原以后的字符串
	 */
	public static String defuse(String inputStr) {
		int sum = 0;
        StringBuffer target = new StringBuffer();
        
		for (int i = 0; i < inputStr.length(); i++) {
			sum += Integer.parseInt(inputStr.substring(i, i + 1));
		}
		sum = sum % 10;
		System.out.println(sum);
		switch (sum) {
			case 0 :
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(6));
				break;

			case 1 :
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(4));
				break;

			case 2 :
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(2));
                break;
                
			case 3 :
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
                break;

			case 4 :
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(6));
				break;
                
			case 5 :
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(2));
                break;
                
			case 6 :
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(6));
				break;

			case 7 :
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(4));
				break;
                
			case 8 :
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(0));
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(2));
				break;

			case 9 :
                target.append(inputStr.charAt(5));
                target.append(inputStr.charAt(7));
                target.append(inputStr.charAt(4));
                target.append(inputStr.charAt(1));
                target.append(inputStr.charAt(6));
                target.append(inputStr.charAt(3));
                target.append(inputStr.charAt(2));
                target.append(inputStr.charAt(0));
				break;
		}
		return target.toString();
	}
    
//    public static void main(String[] args) {
//        System.out.println(NumberConfuser.confuse("15228"));
//        Random rand = new Random();
//        for (int i = 0; i < 20; i++) {
//            Integer ig = new Integer(Math.abs(rand.nextInt()));
//            String tmp = ig.toString();
//            if(tmp.length() < 8) {
//                tmp = "0000000" + tmp;
//            }
//            tmp = tmp.substring(tmp.length()-8);
//            System.out.println("input: " + tmp);
//            String tmp1 = NumberConfuser.confuse(tmp);
//            String tmp2 = NumberConfuser.defuse(tmp1);
//            System.out.println("tmp2: " + tmp2 + " --> tmp1:  " + tmp1);
//            
//        }
//    }
}
