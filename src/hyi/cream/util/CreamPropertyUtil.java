package hyi.cream.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * Class for accessing properties in cream.conf for inline server.
 */
public class CreamPropertyUtil extends Properties {

    private static final long serialVersionUID = 1L;

    private static CreamPropertyUtil instance = null;

//  private static Properties properties = new Properties();
//  private static Properties origProperties = new Properties();
//  private static Set modifiedPropertySet = Collections.synchronizedSet(new HashSet());
//  private static Set newPropertySet = Collections.synchronizedSet(new HashSet());
//  private Boolean mutex = Boolean.FALSE;

    private CreamPropertyUtil() {
        initProperties(); // call init method to initialize it
        instance = this;
    }

    public static CreamPropertyUtil getInstance() {
        if (instance == null)
            instance = new CreamPropertyUtil();
        return instance;
    }

    private void initProperties() {
        if (hyi.cream.inline.Server.serverExist()) {
            try {
                // get cream.conf in current directory on server side
                File propFile = new File("cream.conf");
                if (!propFile.exists()) {
                    put("Locale", "zh_CN");
                    put("ServerDatabaseDriver", "com.mysql.jdbc.Driver");
                    put("ServerDatabaseURL", "jdbc:mysql://localhost/cake");
                    put("ServerDatabaseUser", "root");
                    put("ServerDatabaseUserPassword", "");
                    put("ServerDatabaseInitialConnections", "5");
                    put("ServerDatabaseConnectionIncrement", "5");
                    put("LogFile", "cream.log");
                    put("InlineClientUseJVM", "YES");
                    put("InlineServerLogFile", "/root/inline.log");
                    put("MaxLineItems", "50");

                    FileOutputStream outProp = new FileOutputStream(propFile);
                    store(outProp, "Cream Configuration");
                    outProp.close();
                }
                load(new FileInputStream(propFile));
            } catch (IOException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        }

        /*else {
            ResultSet resultSet = null;
            Connection connection = null;
            Statement statement = null;
            try {
                connection = CreamToolkit.getPooledConnection();
                statement = connection.createStatement();
                resultSet = statement.executeQuery("SELECT * FROM property");
                while (resultSet.next()) {
                    properties.put(resultSet.getObject(1), resultSet
                            .getObject(2));
                    origProperties.put(resultSet.getObject(1), resultSet
                            .getObject(2));
                }
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            } finally {
                try {
                    if (resultSet != null)
                        resultSet.close();
                    if (statement != null)
                        statement.close();
                } catch (SQLException e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }
                CreamToolkit.releaseConnection(connection);
            }
        }*/
    }

    /**
     * Store the properties into table "property" in a seperated transaction.
     * Think twice to sure if you really need this!! 請三思是否要用此方法.
     */
    // synchronized public void depositInIndependentTransaction() {
    // Connection connection = null;
    // try {
    // connection = CreamToolkit.getPooledConnection();
    // deposit(connection);
    // } catch (SQLException e) {
    // CreamToolkit.logMessage(e);
    // // Do I need to halt system here?
    // } finally {
    // CreamToolkit.releaseConnection(connection);
    // }
    // }

//    /**
//     * Store the properties into table "property" in a seperated transaction.
//     * Think twice to sure if you really need this!! 請三思是否要用此方法.
//     * 仅提交需要立刻更新property的数据
//     */
//    synchronized public void depositInIndependentTransaction(final String[] str) {
//        //mutex = false;
//
//        new Thread(new Runnable() {
//            public void run() {
//
//                List key = Arrays.asList(str);
//                System.out.println("--------------- direct : " + key + " | " + modifiedPropertySet + " | " + newPropertySet);
//                Connection connection = null;
//                if (hyi.cream.inline.Server.serverExist()) {
//                    String fileName = "cream.conf";
//                    File propFile = new File(fileName);
//                    try {
//                        FileOutputStream outProp = new FileOutputStream(
//                                propFile);
//                        store(outProp, "Cream Configuration");
//                    } catch (IOException e) {
//                        e.printStackTrace(CreamToolkit.getLogger());
//                        return;
//                    }
//                } else {
//                    ResultSet resultSet = null;
//                    Statement statement = null;
//                    try {
//                        connection = CreamToolkit.getPooledConnection();
//                        statement = connection.createStatement();
//                        Iterator iter;
//                        synchronized (newPropertySet) {
//                            iter = newPropertySet.iterator();
//                            while (iter.hasNext()) {
//                                String name = (String) iter.next();
//                                if (key.contains(name)) {
//System.out.println("----------------- direct insert property : " + name);
//                                    statement
//                                            .executeUpdate("INSERT INTO property (name,value) values ('"
//                                                    + name
//                                                    + "','"
//                                                    + properties
//                                                            .getProperty(name)
//                                                    + "')");
//                                    origProperties.setProperty(name, properties
//                                            .getProperty(name));
//                                }
//                            }
//                        }
//                        synchronized (modifiedPropertySet) {
//                            iter = modifiedPropertySet.iterator();
//                            while (iter.hasNext()) {
//                                String name = (String) iter.next();
//                                if (key.contains(name)) {
//System.out.println("----------------- direct update property : " + name + " | " + modifiedPropertySet + " | value " + properties
//        .getProperty(name));
//                                    statement
//                                            .executeUpdate("UPDATE property SET value='"
//                                                    + properties
//                                                            .getProperty(name)
//                                                    + "' WHERE name='"
//                                                    + name
//                                                    + "'");
//                                }
//                            }
//                        }
//                        modifiedPropertySet.remove(key);
//                        newPropertySet.remove(key);
//                    } catch (ConcurrentModificationException e) {
//                        CreamToolkit.logMessage(e);
//                    } catch (Exception e) {
//                    } finally {
////                        synchronized (CreamPropertyUtil.this.mutex) {
////                            CreamPropertyUtil.this.mutex = true;
////                            CreamPropertyUtil.this.mutex.notifyAll();
////                        }
//
//                        try {
//                            if (resultSet != null)
//                                resultSet.close();
//                            if (statement != null)
//                                statement.close();
//                        } catch (SQLException e) {
//                            CreamToolkit.logMessage(e);
//                        }
//                        CreamToolkit.releaseConnection(connection);
//                        System.out.println("-----------------------------------end");
//                    }
//                }
//            }
//        }).start();
//        try {
//            Thread.currentThread().sleep(100);
//            System.out.println("-----------------------------------2");
//        } catch (Exception e) {}
////        synchronized (mutex)
////        {
////            if (this.mutex == Boolean.FALSE)
////            {
////                try
////                {
////                    this.mutex.wait();
////                }
////                catch (InterruptedException e)
////                {
////                    e.printStackTrace();
////                }
////            }
////        }
//    }

//    /**
//     * Store the properties into table "property".
//     */
//    synchronized public void deposit(Connection connection) throws SQLException {
//        if (connection == null)
//            return;
//
//        if (hyi.cream.inline.Server.serverExist()) {
//            String fileName = "cream.conf";
//            File propFile = new File(fileName);
//            try {
//                FileOutputStream outProp = new FileOutputStream(propFile);
//                store(outProp, "Cream Configuration");
//            } catch (IOException e) {
//                e.printStackTrace(CreamToolkit.getLogger());
//                return;
//            }
//        } else {
//            ResultSet resultSet = null;
//            Statement statement = null;
//            try {
//                statement = connection.createStatement();
//                Iterator iter;
//                synchronized (newPropertySet) {
//                    iter = newPropertySet.iterator();
//                    while (iter.hasNext()) {
//                        String name = (String) iter.next();
//                        statement
//                                .executeUpdate("INSERT INTO property (name,value) values ('"
//                                        + name
//                                        + "','"
//                                        + properties.getProperty(name) + "')");
//                        origProperties.setProperty(name, properties
//                                .getProperty(name));
//                    }
//                }
//                synchronized (modifiedPropertySet) {
//                    iter = modifiedPropertySet.iterator();
//                    while (iter.hasNext()) {
//                        String name = (String) iter.next();
//                        statement.executeUpdate("UPDATE property SET value='"
//                                + properties.getProperty(name)
//                                + "' WHERE name='" + name + "'");
//                    }
//                }
//                modifiedPropertySet.clear();
//                newPropertySet.clear();
//            } catch (ConcurrentModificationException e) {
//                CreamToolkit.logMessage(e);
//            } finally {
//                try {
//                    if (resultSet != null)
//                        resultSet.close();
//                    if (statement != null)
//                        statement.close();
//                } catch (SQLException e) {
//                    CreamToolkit.logMessage(e);
//                }
//            }
//        }
//    }

    public String getProperty(String name, String defaultValue) {
        String ret = getProperty(name);
        return (ret == null) ? defaultValue : ret;
    }

    public String getProperty(String name) {
        if (hyi.cream.inline.Server.serverExist()) {
            return super.getProperty(name);
        } else {
            return null;
        }
    }

//    public Object setProperty(String name, String value) {
//        if (hyi.cream.inline.Server.serverExist()) {
//            return super.setProperty(name, value);
//        } else {
//            properties.setProperty(name, value);
//            if (origProperties.containsKey(name)) {
//                modifiedPropertySet.add(name);
//            } else {
//                newPropertySet.add(name);
//            }
//            return value;
//        }
//    }

//    public boolean containsKey(String s) {
//        if (hyi.cream.inline.Server.serverExist()) {
//            return super.containsKey(s);
//        } else {
//            return properties.containsKey(s);
//        }
//    }

//    public void put(String name, String value) {
//        if (hyi.cream.inline.Server.serverExist()) {
//            super.put(name, value);
//        } else {
//            properties.put(name, value);
//            if (origProperties.containsKey(name)) {
//                modifiedPropertySet.add(name);
//            } else {
//                newPropertySet.add(name);
//            }
//        }
//    }

    // public static void main(String[] args) {
    // try {
    // CreamProperties2 c = new CreamProperties2();
    // System.out.println("containsKey = " + c.containsKey("log"));
    // System.out.println("get'log' = " + c.getProperty("log"));
    // c.put("x", "x");
    // c.setProperty("x", "xxx");
    // c.deposit();
    // } catch (InstantiationException e) {
    // }
    // }
}
