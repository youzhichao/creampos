package hyi.cream.util

import hyi.cream.groovydac.Param
import org.apache.commons.io.FilenameUtils
import org.apache.commons.io.FileUtils

/**
 * Reader for .conf file.
 * <p/>
 * 如果.conf文件存在，會將其內容寫到property表，然後將該文件移至trash目錄下。
 * <p/>
 * 所以之後.conf文件的內容皆從property.value來。
 *
 * @author Bruce You
 * @since Jun 4, 2009 1:37:14 PM
 */
class ConfReader {
    def currentLine
    def content

    ConfReader(File confFile) {
        def param = Param.instance
        def paramName = FilenameUtils.getBaseName(confFile.name)
        if (confFile.exists()) {
            // 將conf文件內容寫到property --
            //   Param name = conf file name without extension, e.g., 'screenbanner'
            //   Param value = 過濾掉comment和空行，然後用 \n join起來
            param."$paramName" = confFile.readLines().grep(~/^[^#\s].+/).join(/\n/)

            // move conf file to subdirectory 'trash'
            def trashDir = [confFile.parent, 'trash'] as File
            FileUtils.copyFileToDirectory confFile, trashDir
            confFile.delete()
        }
        content = param."$paramName".split(/\\n/)
        currentLine = 0
    }

    String readLine() {
        if (currentLine >= content.size())
            return null
        content[currentLine++]
    }

    void close() {
        currentLine = 0
        content = null
    }
}