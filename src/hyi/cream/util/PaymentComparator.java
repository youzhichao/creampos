package hyi.cream.util;

import hyi.cream.dac.Payment;

import java.util.Comparator;

public class PaymentComparator implements Comparator<Payment> {
    public int compare(Payment o1, Payment o2) {
        try {
            int seqno1;
            int seqno2;
            seqno1 = Integer.parseInt(o1.getSeqno());
            seqno2 = Integer.parseInt(o2.getSeqno());
            int comp = new Integer(seqno2).compareTo(new Integer(seqno1));
            if (comp == 0)
                return o1.getPaymentID().compareTo(o2.getPaymentID());
            else
                return comp;
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean equals(Payment obj) {
        return obj.equals(this);
    }
}