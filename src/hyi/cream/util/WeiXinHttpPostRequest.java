package hyi.cream.util;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Store;
import hyi.cream.groovydac.Param;
import hyi.cream.state.GetProperty;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.io.*;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author : liupingping
 *
 * 微信接口
 */
public class WeiXinHttpPostRequest {

    public static String appid = /*"wx9647cd142fb512e1";*/Store.getWeixinAppId();//微信分配的公众账号ID
    public static String mch_id = /*"10000008";*/Store.getWeixinMchId();//微信支付分配的商户号
    public static String sub_mch_id = "123";//微信支付分配的子商户号，受理模式下必填
    public static String device_info = Store.getStoreID();//终端设备号(商户自定义)
    public static String version = "1.0";//版本号
    public static String appSecret	= "c8952a4fbbe62bb1a90602e6f6274787";
    public static String appKey = "SEYNn8ClFmMYEPNhG226Y6Nl2qpD5KeJb2jlVKuVp6bScMtF8h6UcF31pgsLk2OiPXrAuKNdCLS9RbDNfvuJZm5dWjj50U0hEpErYgrpl0lx8LCb6rfoA7oQewcbeGhH";
    public static String partnerId = "1214002601";
    public static String partnerKey =/*"134acc6ada2e13ab39038d257f47c117";*/Store.getweixinPartnerKey();//签名密钥
    public static String weiXinUrl = Param.getInstance().getWeiXinUrl();
    public static String weiXinGoodsTag = Param.getInstance().getWeiXinGoodsTag();
    public static String weiXinKeyStore = /*"apiclient_cert.p12";*/Store.getWeiXinKeyStore();//证书名
    public static String weiXinKeyPassword = /*"10010489";*/Store.getWeiXinKeyPassword();//证书密码

    public static String  getPropery() {

        if (appid == null || appid.equals(""))
            return null;
        System.out.println("appid：" + appid);
        CreamToolkit.logMessage("appid："+appid);
        if (mch_id == null || mch_id.equals(""))
            return null;
        System.out.println("mch_id：" + mch_id);
        CreamToolkit.logMessage("mch_id: " + mch_id);
        if (partnerKey == null || partnerKey.equals(""))
            return null;
        System.out.println("partnerKey：" + partnerKey);
        CreamToolkit.logMessage("partherKey: " + partnerKey);

        return "";
    }


    //https通讯
    public static String  getReposnes(String str, String urlStr) {
        try {
            System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date()) + "---------------------------------------");
            CreamToolkit.logMessage("---------------------------------------");
            KeyStore keyStore  = KeyStore.getInstance("PKCS12");
            FileInputStream instream = new FileInputStream(new File(weiXinKeyStore));
            try {
                keyStore.load(instream, weiXinKeyPassword.toCharArray());
            } finally {
                instream.close();
            }

            // Trust own CA and all self-signed certs
            SSLContext sslcontext = SSLContexts.custom()
                    .loadKeyMaterial(keyStore, weiXinKeyPassword.toCharArray())
                    .build();
            // Allow TLSv1 protocol only
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    sslcontext,
                    new String[] { "TLSv1" },
                    null,
                    SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
            int timeout = 3000;
            RequestConfig defaultRequestConfig = RequestConfig.custom()
                    .setSocketTimeout(timeout)
                    .setConnectTimeout(timeout)
                    .setConnectionRequestTimeout(timeout)
                    .setStaleConnectionCheckEnabled(true)
                    .build();
            CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig)
                    .setSSLSocketFactory(sslsf)
                    .build();

            HttpPost httppost = new HttpPost(urlStr);
            System.out.println(str);
            CreamToolkit.logMessage(str);
            StringEntity stringEntity = new StringEntity(str,"UTF-8");
            httppost.setEntity(stringEntity);
            System.out.println(new SimpleDateFormat("\nyyyy/MM/dd HH:mm:ss:SSS ").format(new Date())
                    +" Https POST Request：" + httppost.getRequestLine());
            CreamToolkit.logMessage(" Https POST Request：" + httppost.getRequestLine());

            CloseableHttpResponse response = httpclient.execute(httppost);
            // 获取服务端的反馈
            String strLine = "";
            String strResponse = "";
            HttpEntity entity = response.getEntity();
            try {
                System.out.println("----------------------------------------");
                CreamToolkit.logMessage("----------------------------------------");
                System.out.println(response.getStatusLine());
                if (entity != null) {
                    System.out.println();
                    System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date()) +
                            "Response content length: " + entity.getContentLength());
                    CreamToolkit.logMessage("Response content length: " + entity.getContentLength());
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent(),"UTF-8"));
                    while ((strLine = bufferedReader.readLine()) != null) {
                        strResponse += strLine + "\n";
                    }
                }
                EntityUtils.consume(entity);
            }finally {
                response.close();
                httpclient.close();
            }

            System.out.print(strResponse);
            CreamToolkit.logMessage(strResponse);
            System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date()) + " -----------------------------------------");
            CreamToolkit.logMessage(" -----------------------------------------");
            return strResponse;
        }catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("WanOfflineCanNotTrade......");
        CreamToolkit.logMessage("WanOfflineCanNotTrade......");
        return "";
    }

    /**
     * 提交被扫支付接口
     * auth_code：扫码支付授权码，设备读取用户微信中的条码或者二维码信息
     * out_trade_no：商户订单号
     * body：商品描述
     * total_fee：总金额 以分为单位
     * spbill_create_ip：订单生成的机器IP
     * */
    public static String micropay(String auth_code, String out_trade_no, String body, String total_fee, String detail) {
        String url = weiXinUrl+"pay/micropay";
        Map map = new HashMap();
        map.put("auth_code", auth_code);
        map.put("out_trade_no", out_trade_no);
        map.put("body", body);
        map.put("total_fee", total_fee);
        map.put("appid", appid);
        map.put("mch_id", mch_id);
        map.put("detail", detail);
        if (!device_info.equals(""))
            map.put("device_info", device_info);
        map.put("nonce_str", getRandom());//随机字符串 32位
        map.put("spbill_create_ip", "192.168.0.1");
        map.put("version", version);
        String str = getSortedHashMapByKey(map);
        String md5Str = getMD5Str(str);
        String goods = "";
        if (map.containsKey("goods_tag")) {
            goods = weiXinGoodsTag;//"<goods_tag>"+map.get("goods_tag")+"</goods_tag>" ;
        }
        String xml = "<xml>" +
                "<appid>"+map.get("appid")+"</appid>" +
                "<auth_code>"+map.get("auth_code")+"</auth_code>" +
                "<body><![CDATA["+map.get("body")+"]]></body>" +
                "<device_info>"+map.get("device_info")+"</device_info>" +
                "<mch_id>"+map.get("mch_id")+"</mch_id>" +
                "<nonce_str>"+map.get("nonce_str")+"</nonce_str>" +
                "<out_trade_no>"+map.get("out_trade_no")+"</out_trade_no>" +
                "<spbill_create_ip>"+map.get("spbill_create_ip")+"</spbill_create_ip>" +
                "<total_fee>"+map.get("total_fee")+"</total_fee>" + goods +
                "<detail>"+map.get("detail")+"</detail>"+
                "<version>"+map.get("version")+"</version>"+
                "<sign><![CDATA["+md5Str+"]]></sign>" +
                "</xml>";
        String res = getReposnes(xml,url);
        return res;
    }

    /**
     * 被扫订单查询接口
     * out_trade_no：商户订单号
     * transaction_id：微信订单号
     */

    public static String orderquery(String out_trade_no) {
        String url = weiXinUrl+"pay/orderquery";
        Map map = new HashMap();
        map.put("out_trade_no", out_trade_no);
        map.put("appid", appid);
        map.put("mch_id", mch_id);
        map.put("nonce_str", getRandom());//随机字符串 32位
        map.put("version", version);
        String str = getSortedHashMapByKey(map);
        String md5Str = getMD5Str(str);
        String xml = "<xml>" +
                "<appid>"+map.get("appid")+"</appid>" +
                "<mch_id>"+map.get("mch_id")+"</mch_id>" +
                "<nonce_str>"+map.get("nonce_str")+"</nonce_str>" +
                "<out_trade_no>"+map.get("out_trade_no")+"</out_trade_no>" +
                "<version>"+map.get("version")+"</version>"+
                "<sign><![CDATA["+md5Str+"]]></sign>" +
                "</xml>";
        String res = getReposnes(xml,url);
        return res;
    }

    /**关闭订单接口*
     *
     * out_trade_no：商户订单号
     */
    public static String closeorder(String out_trade_no) {
        String url = weiXinUrl+"pay/closeorder";
        Map map = new HashMap();
        map.put("out_trade_no", out_trade_no);
        map.put("appid", appid);
        map.put("mch_id", mch_id);
        map.put("nonce_str", getRandom());//随机字符串 32位
        String str = getSortedHashMapByKey(map);
        String md5Str = getMD5Str(str);
        String xml = "<xml>" +
                "<appid>"+map.get("appid")+"</appid>" +
                "<mch_id>"+map.get("mch_id")+"</mch_id>" +
                "<nonce_str>"+map.get("nonce_str")+"</nonce_str>" +
                "<out_trade_no>"+map.get("out_trade_no")+"</out_trade_no>" +
                "<sign><![CDATA["+md5Str+"]]></sign>" +
                "</xml>";
        String res = getReposnes(xml,url);
        return res;
    }

    /**退款申请接口*
     *
     * out_trade_no：商户订单号
     * transaction_id：微信订单号
     * out_refund_no：商户退款单号
     * total_fee：总金额
     * refund_fee：退款金额
     * op_user_id：操作员
     */
    public static String refund(String out_trade_no, String transaction_id, String out_refund_no, String total_fee, String refund_fee, String op_user_id) {
        String url = weiXinUrl+"secapi/pay/refund";
        Map map = new HashMap();
        map.put("out_trade_no", out_trade_no);
        map.put("transaction_id", transaction_id);
        map.put("out_refund_no", out_refund_no);
        map.put("refund_fee", refund_fee);
        map.put("op_user_id", op_user_id);
        map.put("total_fee", total_fee);
        map.put("appid", appid);
        map.put("mch_id", mch_id);
        if (!device_info.equals(""))
            map.put("device_info", device_info);
        map.put("nonce_str", getRandom());//随机字符串 32位
        map.put("spbill_create_ip", "192.168.0.1");
        String str = getSortedHashMapByKey(map);
        String md5Str = getMD5Str(str);
        String xml = "<xml>" +
                "<appid>"+map.get("appid")+"</appid>" +
                "<device_info>"+map.get("device_info")+"</device_info>" +
                "<mch_id>"+map.get("mch_id")+"</mch_id>" +
                "<nonce_str>"+map.get("nonce_str")+"</nonce_str>" +
                "<op_user_id>"+map.get("op_user_id")+"</op_user_id>" +
                "<out_refund_no>"+map.get("out_refund_no")+"</out_refund_no>" +
                "<out_trade_no>"+map.get("out_trade_no")+"</out_trade_no>" +
                "<refund_fee>"+map.get("refund_fee")+"</refund_fee>" +
                "<spbill_create_ip>"+map.get("spbill_create_ip")+"</spbill_create_ip>" +
                "<total_fee>"+map.get("total_fee")+"</total_fee>" +
                "<transaction_id>"+map.get("transaction_id")+"</transaction_id>" +
                "<sign><![CDATA["+md5Str+"]]></sign>" +
                "</xml>";
        String res = getReposnes(xml,url);
        return res;
    }

    /**退款查询接口
     * transaction_id：微信订单号
     * out_trade_no：商户订单号
     * out_refund_no：商户退款单号
     * refund_id：微信退款单号
     */
    public static String refundquery(String transaction_id, String out_trade_no, String out_refund_no) {
        String url = weiXinUrl+"pay/refundquery";
        Map map = new HashMap();
        map.put("transaction_id", transaction_id);
        map.put("out_trade_no", out_trade_no);
        map.put("out_refund_no", out_refund_no);
        map.put("appid", appid);
        map.put("mch_id", mch_id);
        if (!device_info.equals(""))
            map.put("device_info", device_info);
        map.put("nonce_str", getRandom());//随机字符串 32位
        String str = getSortedHashMapByKey(map);
        String md5Str = getMD5Str(str);
        String xml = "<xml>" +
                "<appid>"+map.get("appid")+"</appid>" +
                "<device_info>"+map.get("device_info")+"</device_info>" +
                "<mch_id>"+map.get("mch_id")+"</mch_id>" +
                "<nonce_str>"+map.get("nonce_str")+"</nonce_str>" +
                "<out_refund_no>"+map.get("out_refund_no")+"</out_refund_no>" +
                "<out_trade_no>"+map.get("out_trade_no")+"</out_trade_no>" +
                "<transaction_id>"+map.get("transaction_id")+"</transaction_id>" +
                "<sign><![CDATA["+md5Str+"]]></sign>" +
                "</xml>";
        String res = getReposnes(xml,url);
        return res;
    }

    /**冲正接口
     * out_trade_no：商户订单号
     *
     */
    public static String reverse(String out_trade_no) {
        String url = weiXinUrl+"secapi/pay/reverse";
        Map map = new HashMap();
        map.put("appid", appid);
        map.put("mch_id", mch_id);
        map.put("out_trade_no", out_trade_no);
        map.put("nonce_str", getRandom());//随机字符串 32位
        String str = getSortedHashMapByKey(map);
        String md5Str = getMD5Str(str);
        String xml = "<xml>" +
                "<appid>"+map.get("appid")+"</appid>" +
                "<mch_id>"+map.get("mch_id")+"</mch_id>" +
                "<nonce_str>"+map.get("nonce_str")+"</nonce_str>" +
                "<out_trade_no>"+map.get("out_trade_no")+"</out_trade_no>" +
                "<sign><![CDATA["+md5Str+"]]></sign>" +
                "</xml>";
        String res = getReposnes(xml,url);
        return res;
    }

    /**对账单接口
     *
     * bill_date：对账单日起
     * bill_type：账单类型  ALL:返回当日所有订单信息,默认值;  SUCCESS:返回当日成功支付的订单;REFUND:返回当日退款订单
     */
    public static String downloadbill(String bill_date, String bill_type) {
        String url = weiXinUrl+"pay/downloadbill";
        Map map = new HashMap();
        map.put("appid", appid);
        map.put("mch_id", mch_id);
        if (!device_info.equals(""))
            map.put("device_info", device_info);
        map.put("nonce_str", getRandom());//随机字符串 32位
        map.put("bill_date", bill_date);
        map.put("bill_type", bill_type);
        String str = getSortedHashMapByKey(map);
        String md5Str = getMD5Str(str);
        String xml = "<xml>" +
                "<appid>"+map.get("appid")+"</appid>" +
                "<bill_date>"+map.get("bill_date")+"</bill_date>" +
                "<bill_type>"+map.get("bill_type")+"</bill_type>" +
                "<device_info>"+map.get("device_info")+"</device_info>" +
                "<mch_id>"+map.get("mch_id")+"</mch_id>" +
                "<nonce_str>"+map.get("nonce_str")+"</nonce_str>" +
                "<sign><![CDATA["+md5Str+"]]></sign>" +
                "</xml>";
        String res = getReposnes(xml,url);
        return res;
    }


    //随机字符串
    public static String getRandom() {
        String radStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuffer generateRandStr = new StringBuffer();
        Random rand = new Random();
        int length = 32;
        for(int i=0;i<length;i++)
        {
            int randNum = rand.nextInt(36);
            generateRandStr.append(radStr.substring(randNum,randNum+1));
        }
        return generateRandStr+"";
    }

    //对map的key进行升序，并转成key1=value1&key2=value2..的形式
    public static String getSortedHashMapByKey(Map h) {

        String str = "";
        TreeMap treemap = new TreeMap(h);

        Iterator it = treemap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String key = entry.getKey().toString();
            String value = entry.getValue().toString();
            str += key + "="+value+"&";
        }
        return str;
    }

    //MD5加密计算
    public static String getMD5Str(String str) {
        MessageDigest messageDigest = null;
        try {
            str +="key="+partnerKey;
            System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())
                    + " --------------------------- process WeiXin ---------------------------");
            CreamToolkit.logMessage(" --------------------------- process WeiXin ---------------------------");

            System.out.println("MD5加密数据：" + str);
            CreamToolkit.logMessage("MD5加密数据：" + str);
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(str.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException caught!");
            CreamToolkit.logMessage("NoSuchAlgorithmException caught!");
            System.exit(-1);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        byte[] byteArray = messageDigest.digest();
        StringBuffer md5StrBuff = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
                md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
            else
                md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
        }
        String res = md5StrBuff.toString().toUpperCase();
        System.out.println("sign = "+res);
        CreamToolkit.logMessage("sign = "+res);
        return res;
    }

    public static  String getPosIP() {
//        String posId = Param.getInstance().getTerminalNumber();
        String number ;
        int id = Param.getInstance().getTerminalNumber();

        Map map = new HashMap();
        File propFile = new File("/etc/hosts");
        try {
            FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = null;
            if (Param.getInstance().getLocale().equals("zh_TW")) {
                inst = new InputStreamReader(filein, "BIG5");
            } else if (Param.getInstance().getLocale().equals("zh_CN")) {
                inst = new InputStreamReader(filein, "GBK");
            }
            BufferedReader in = new BufferedReader(inst);
            String line = null;
            while ((line = in.readLine()) != null) {
                if (!line.trim().startsWith("#")) {
                    if (line.indexOf("pos1") != -1) {
                        map.put("pos1",line.substring(0,line.indexOf("pos1")).trim());
                    } else if (line.indexOf("pos2") != -1) {
                        map.put("pos2",line.substring(0,line.indexOf("pos2")).trim());
                    } else if (line.indexOf("pos3") != -1) {
                        map.put("pos3",line.substring(0,line.indexOf("pos3")).trim());
                    } else if (line.indexOf("pos4") != -1) {
                        map.put("pos4",line.substring(0,line.indexOf("pos4")).trim());
                    } else if (line.indexOf("pos5") != -1) {
                        map.put("pos5",line.substring(0,line.indexOf("pos5")).trim());
                    }
                }
            }
        } catch (FileNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("File is not found: " + propFile.toString());
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        number = (String)map.get("pos"+id) ;
        if (number == null || number.equals(""))
            return "192.168.0.1";
        return number;
    }
    public static void main(String [] args) {
        try {
            SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmsss");
            String str = f.format(new Date());
            appid = "wxcdd85339688ad174";
            mch_id = "1488944102";
            partnerKey = "JYCSMLIwgSWfrIn4l3XkBfFUi31vroiD";
            weiXinUrl = "https://api.mch.weixin.qq.com/";
            weiXinKeyStore = "apiclient_cert.p12";
            weiXinKeyPassword = "1488944102";
//            micropay("111882508230425441",str,"商品","1","");//支付
//            orderquery("201409041349059");//支付查询
//            closeorder("201408181236");//关闭订单
//            refund("10017813337101","4200000013201709264362158725","10017813337115","770","770","00001");//退款
//            refundquery("1006470648201408220005064605","111113258750901","   111113258751101");//退款查询
//            reverse("201409041349059");//冲正订单
//            downloadbill("20140813","ALL");//对账订单
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}