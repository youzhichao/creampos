package hyi.cream.util;

import hyi.cream.groovydac.Param;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.http.params.CoreConnectionPNames;

import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 移动支付接口类
 */
public class HttpHepler {

    public static String urlStr = Param.getInstance().getCMPAYAddress(); //"http://211.136.119.81:5906/CMPay/CMPAYProcess";//"http://219.233.194.165:5906/CMPay/CMPAYProcess";
    public static int timeOut = 60000;
    public static String REQSYS = Param.getInstance().getCMPAYMerId();
    public static String PIKFLG = Param.getInstance().getCMPAYPIKFLG();
    public static SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
    public static SimpleDateFormat hhmmss = new SimpleDateFormat("HHmmss");
    public static SimpleDateFormat hhmmssSSS = new SimpleDateFormat("HHmmssSSS");

    public static String post( String xmlData)
            throws Exception {

        HttpClient client = new HttpClient();

        // 设置连接超时时间(单位毫秒)
        client.setTimeout(timeOut);
        // 设置连接超时时间(单位毫秒)
        client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeOut);
        client.getHttpConnectionManager().getParams().setConnectionTimeout(timeOut);
        HttpConnectionManagerParams managerParams = client
                .getHttpConnectionManager().getParams();
        managerParams.setConnectionTimeout(timeOut);

        System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new java.util.Date()) + "---------------------------------------");
        System.out.println(urlStr);
        PostMethod postMethod = new PostMethod(urlStr);
        postMethod.setDoAuthentication(true);
//        postMethod.addRequestHeader("Content-Type", "text/xml");
//        postMethod.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "GBK");//设置字符编码
        System.out.println(xmlData);
        if (xmlData != null) {
            postMethod.setRequestEntity(new StringRequestEntity(xmlData,"text/xml","GBK"));
        }
        try {
            int status = client.executeMethod(postMethod);
            if (status == 200) {
                System.out.println(postMethod.getResponseBodyAsString());
                String result = URLDecoder.decode(postMethod.getResponseBodyAsString(), "GBK");
                System.out.println(status + "\n" + result);
                System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new java.util.Date()) + "---------------------------------------");
                return result;
            }
            System.out.println(status + "\n" + postMethod.getResponseBodyAsString());
            System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new java.util.Date()) + "---------------------------------------");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            postMethod.releaseConnection();
        }
        return "";
    }

    /**
     *
     * @param mcode
     * 云支付下单	101435
     * 云支付短信验证码支付	101436
     * 云支付短信验证码重下发	101437
     * 云支付用户账户可用余额查询	101438
     * 云极速支付	101439
     * 云支付动态密码下发	101431
     * 云极速动态密码支付	101432
     * 云支付冲正	101440
     * NFC下单	11452
     * NFC查询用户余额	101451
     * 商户撤销/退款	101456
     * 订单状态查询	101455
     * 用户手机支付业务鉴权	101710
     * @return
     */
    public static String getHead(String mcode, String mid) {
        Date date = new Date();
        Random random = new Random();
        int a = random.nextInt(10);
        if (mid.equals("")) {
            mid = a + hhmmssSSS.format(new Date());
        }
        String str = "<HEAD>" +
                "<MCODE>"+mcode+"</MCODE>" +
                "<MID>0007"+ mid +"</MID>" +
                "<DATE>"+yyyyMMdd.format(date)+"</DATE>" +
                "<TIME>"+hhmmss.format(date)+"</TIME>" +
                "<PAYDAY>"+yyyyMMdd.format(date)+"</PAYDAY>" +
                "<MSGPRO>SF</MSGPRO>" +
                "<VER>0001</VER>" +
                "<REQSYS>"+REQSYS+"</REQSYS>" +
                "<SCH>rq</SCH>" +
                "<MSGATR>00</MSGATR>" +
                "<SAFEFLG>00</SAFEFLG>" +
                "<MAC></MAC>" +
                "</HEAD>";
        return str;
    }

    public static String getBody(String mcode, String orderid,
                                 String usertoken,String usertokentp, String amt, String oprid, String smscd,
                                 String dynpwd) {
        Date date = new Date();
        String str = "<BODY>" +
                "<MERID>" + REQSYS + "</MERID>" ;
        if (mcode.equals("101436")) {//云支付短信验证码支付
            str += "<ORDERID>"+orderid+"</ORDERID>" +
                    "<SMSCD>"+smscd+"</SMSCD>" +
                    "<OPRID>"+oprid+"</OPRID>";
        } else if (mcode.equals("101437")) {//云支付短信验证码重下发
            str += "<ORDERID>"+orderid+"</ORDERID>";
        } else if (mcode.equals("101431")) {//云支付动态密码下发
            str +=  "<USERTOKEN>" + usertoken + "</USERTOKEN>" +
                    "<USERTOKENTP>" + usertokentp + "</USERTOKENTP>";
        } else if (mcode.equals("101432")) {//云极速动态密码支付
            str += "<ORDERID>"+orderid+"</ORDERID>" +
                    "<AMT>"+amt+"</AMT>" +
                    "<CURRENCY>CMY</CURRENCY>" +
                    "<ORDERDATE>"+yyyyMMdd.format(date)+"</ORDERDATE>" +
                    "<PERIOD>30</PERIOD>" +
                    "<PERIODUNIT>00</PERIODUNIT>" +
                    "<PRODUCTNAME></PRODUCTNAME>" +
//                "<RESERVER1></RESERVER1>" +
//                "<RESERVER2></RESERVER2>" +
                    "<COUFLAG>0</COUFLAG>" +
                    "<VCHFLAG>0</VCHFLAG>" +
                    "<CASHFLAG>0</CASHFLAG>" +
                    "<USERTOKEN>"+usertoken+"</USERTOKEN>" +
                    "<DYNPWD>"+dynpwd+"</DYNPWD>" +
                    "<OPRID>"+oprid+"</OPRID>";
        } else if (mcode.equals("11452")) {//NFC下单
            str += "<ORDERID>"+orderid+"</ORDERID>" +
                    "<AMT>"+amt+"</AMT>" +
                    "<CURRENCY>CMY</CURRENCY>" +
                    "<ORDERDATE>"+yyyyMMdd.format(date)+"</ORDERDATE>" +
                    "<PERIOD>30</PERIOD>" +
                    "<PERIODUNIT>00</PERIODUNIT>" +
                    "<PRODUCTNAME></PRODUCTNAME>" +
//                "<RESERVER1></RESERVER1>" +
//                "<RESERVER2></RESERVER2>" +
                    "<COUFLAG>0</COUFLAG>" +
                    "<VCHFLAG>0</VCHFLAG>" +
                    "<CASHFLAG>0</CASHFLAG>" +
                    "<USRID></USRID>" +
                    "<USRTYP>UCID</USRTYP>" +
                    "<VERTYP>3</VERTYP>" +
                    "<VERVAL>3</VERVAL>" +
                    "<VERDESC>3</VERDESC>" +
                    "<OPRID>"+oprid+"</OPRID>";
        } else if (mcode.equals("101451")) {//NFC查询用户余额接口
            str += "<USRID></USRID>" +
                    "<USRTYP>UCID</USRTYP>" +
                    "<VERTYP>3</VERTYP>" +
                    "<VERVAL>3</VERVAL>" +
                    "<VERDESC>3</VERDESC>";
        }
        return str + "</BODY>";
    }

    public static String getSign(String str) {
//        String datastring = str;
//        try {
//            datastring = new String(str.getBytes("GBK"));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//
//        //进行GBK解码
//        byte[] reqMsg_DI = null;
//        try {
//            String reqMsg = URLDecoder.decode(datastring, "UTF8");
//            reqMsg_DI = reqMsg.getBytes("GBK");
//        } catch (UnsupportedEncodingException e2) {
//            e2.printStackTrace();
//        }
//        //获得验签的原数据
//        String vvv = new String(reqMsg_DI);
//
//        System.out.println("要签名的字符串(已GBK转换)：" + vvv);
        String signStr = SignUtil.cmpaysign(str, REQSYS); //生产签名
        System.out.println("签名后的字符串：" + signStr);
        return signStr;
    }

    //用户鉴权
    public static String authentication(String userToken) {
        String head = "<MPAY>" + getHead("101710", "");
        String detail = "<BODY>" +
                "<MOBILEID>"+userToken+"</MOBILEID>";
        String last = "</BODY></MPAY>";
        String signStr = "<SIGN>" + getSign(head + detail + last) + "</SIGN>";
        String xml = head + detail + signStr + last;
        try {
            return post(xml);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    //余额查询
    public static String query(String userToken, String usertokentp) {
        String head = "<MPAY>" + getHead("101438", "");
        String detail = "<BODY><MERID>" + REQSYS + "</MERID>" +
                "<USERTOKEN>" + userToken + "</USERTOKEN>" +
                "<USERTOKENTP>" + usertokentp + "</USERTOKENTP>";
        String last = "</BODY></MPAY>";
        String signStr = "<SIGN>" + getSign(head + detail + last) + "</SIGN>";
        String xml = head + detail + signStr + last;
//        String detail = getBody("101438","",userToken,usertokentp,"","","","","","","") + "</MPAY>";
//        String xml = head + detail;
        try {
            return post(xml);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    //云支付下单
    public static String order(String orderId, String userToken, String usertokentp, String amt, String oprId) {
        String head = "<MPAY>" + getHead("101435", "");
        String detail ="<BODY><MERID>" + REQSYS + "</MERID>" +
                "<ORDERID>" + orderId + "</ORDERID>" +
                "<USERTOKEN>" + userToken + "</USERTOKEN>" +
                "<USERTOKENTP>" + usertokentp + "</USERTOKENTP>" +
                "<AMT>" + amt + "</AMT>" +
                "<AUTHORIZEMODE>CAS</AUTHORIZEMODE>" +
                "<CURRENCY>CMY</CURRENCY>" +
                "<ORDERDATE>" + yyyyMMdd.format(new Date()) + "</ORDERDATE>" +
                "<MERACDATE>" + yyyyMMdd.format(new Date()) + "</MERACDATE>" +
                "<PERIOD>30</PERIOD>" +
                "<PERIODUNIT>00</PERIODUNIT>" +
                "<MERCHANTABBR>商品展示名称</MERCHANTABBR>" +
                "<PRODUCTDESC>产品描述</PRODUCTDESC>" +
                "<PRODUCTID>产品编号</PRODUCTID>" +
                "<PRODUCTNAME>测试</PRODUCTNAME>" +
                "<PRODUCTNUM>产品数量</PRODUCTNUM>" +
                "<TXNTYP>S</TXNTYP>" +
                "<CALLBACKURL>www.baidu.com</CALLBACKURL>" +
                "<SHOWURL>www.baidu.com</SHOWURL>" +
                "<RESERVER1></RESERVER1>" +
                "<RESERVER2></RESERVER2>" +
                "<COUFLAG>0</COUFLAG>" +
                "<VCHFLAG>0</VCHFLAG>" +
                "<CASHFLAG>0</CASHFLAG>" +
                "<OPRID>" + oprId + "</OPRID>";
        String last = "</BODY></MPAY>";
        String signStr = "<SIGN>" + getSign(head + detail + last) + "</SIGN>";
        String xml = head + detail + signStr + last;
//        String detail = getBody("101435",orderId,userToken,usertokentp,amt,oprId,"","","","","") + "</MPAY>";
//        String xml = head + detail;
        try {
            return post(xml);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    //云极速支付
    public static String pay(String orderId, String userToken, String amt, String oprId, String productName, String mid) {
        String head = "<MPAY>" + getHead("101439", mid);
        String detail = "<BODY><MERID>" + REQSYS + "</MERID>"
                +"<ORDERID>"+orderId+"</ORDERID>" +
                "<AMT>"+amt+"</AMT>" +
                "<CURRENCY>CNY</CURRENCY>" +
                "<ORDERDATE>"+yyyyMMdd.format(new Date())+"</ORDERDATE>" +
                "<PERIOD>30</PERIOD>" +
                "<PERIODUNIT>00</PERIODUNIT>" +
                "<PRODUCTNAME>" + productName + "</PRODUCTNAME>" +
                "<RESERVER1></RESERVER1>" +
                "<RESERVER2></RESERVER2>" +
                "<COUFLAG>0</COUFLAG>" +
                "<VCHFLAG>0</VCHFLAG>" +
                "<CASHFLAG>0</CASHFLAG>" +
                "<USERTOKEN>"+userToken+"</USERTOKEN>" +
                "<OPRID>"+oprId+"</OPRID>" +
                "<PIKFLG>"+PIKFLG+"</PIKFLG>";
        String last = "</BODY></MPAY>";
        String signStr = "<SIGN>" + getSign(head + detail + last) + "</SIGN>";
        String xml = head + detail + signStr + last;
//        String detail = getBody("101439",orderId,userToken,usertokentp,amt,oprId,"","","","","") + "</MPAY>";
//        String xml = head + detail;
        try {
            System.out.println("发送报文：" + xml);
            return post(xml);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    //云支付冲正
    public static String cancel(String OMID, String ODATE) {
        String head = "<MPAY>" + getHead("101440", "");
        String detail = "<BODY><MERID>" + REQSYS + "</MERID>" +
                "<OMID>"+OMID+"</OMID>" +
                "<ODATE>"+ODATE+"</ODATE>";
        String last = "</BODY></MPAY>";
        String signStr = "<SIGN>" + getSign(head + detail + last) + "</SIGN>";
        String xml = head + detail + signStr + last;
//        String detail = getBody("101440","","","","","","","",OMID,ODATE,"") + "</MPAY>";
//        String xml = head + detail;
        try {
            return post(xml);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    //订单状态查询
    public static String orderQuery(String orderId) {
        String head = "<MPAY>" + getHead("101455", "");
        String detail = "<BODY><MERID>" + REQSYS + "</MERID><ORDERID>"+orderId+"</ORDERID>";
        String last = "</BODY></MPAY>";
        String signStr = "<SIGN>" + getSign(head + detail + last) + "</SIGN>";
        String xml = head + detail + signStr + last;
        try {
            return post(xml);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    //商品撤销/退款
    public static String refund(String orderId, String oDate, String amt, String requestid, String mid) {
        String head = "<MPAY>" + getHead("101456", mid);
        String detail = "<BODY><MERID>" + REQSYS + "</MERID>" +
                "<ORDERID>"+orderId+"</ORDERID>" +
                "<ORDERDATE>" +oDate+"</ORDERDATE>" +
                "<AMT>"+amt+"</AMT>" +
                "<REQUESTID>"+requestid+"</REQUESTID>";
        String last = "</BODY></MPAY>";
        String signStr = "<SIGN>" + getSign(head + detail + last) + "</SIGN>";
        String xml = head + detail + signStr + last;
        try {
            return post(xml);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String unicodeToChinese(String unicodeStr) {
        String unicodes[] = unicodeStr.split(";");
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < unicodes.length; i++) {
            String u = unicodes[i].substring(2);
            str.append((char)(Integer.valueOf(u).intValue()));
        }
        return str.toString();
    }

    public static void main(String [] args) {
//        System.out.println(unicodeToChinese(""));

//        int count = 0;
//        while(true) {
//            try {
//                Thread.sleep(5000);
//
//                count++;
//                if (count % 2 == 0) {
//                    urlStr = "http://219.233.194.165:5906/CMPay/CMPAYProcess";
//                } else {
//                    urlStr = "http://211.136.119.81:5906/CMPay/CMPAYProcess";
//
//                }
//                authentication("15821472754");
//                Thread.sleep(5000);
//                query("15821472754", "0");
//                Thread.sleep(5000);
//                String result = pay("111111111", "15821472754", "1", "11111111", "测试", "");
//                String result = refund("10010311594311", "20150626", "1980", "10010311594312","");
//                String desc = CreamToolkit.getValueFromXml(result, "<DESC>", "</DESC>");
//                System.out.println(HttpHepler.unicodeToChinese(desc));
//                Thread.sleep(5000);
        orderQuery("10010311594311");
//                Thread.sleep(5000);
//                cancel("111111111", "");
//                Thread.sleep(5000);
//                refund("111111111", "", "1", "111111112","");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

    }
}
