package hyi.cream.util

import hyi.cream.groovydac.Transaction
import static hyi.cream.util.CreamToolkit.getPooledConnection
import hyi.cream.groovydac.LineItem
import org.apache.commons.lang.StringUtils
import java.sql.Connection

/**
 * Generator of transaction sheet with barcode image in Postscript format.
 *
 * @author Bruce You
 * @since Oct 11, 2008 9:36:38 PM
 */
class TransactionReport {

    // 4 columns and 10 rows in a page (-e EAN) 
    static final GNU_BARCODE = '/usr/local/bin/barcode -t 4x10+10+80-10-80'

    static void main(args) {
        Connection connection = getPooledConnection()
        def posno = 6
        for (i in 44948..45076)
            generate(connection, posno, i)
        connection.close()
    }
    
    static generate(Connection connection, int posNumber, int transNumber) {
        File dir = new File("." + File.separator + "transheet")
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                CreamToolkit.logMessage("transheet:" + dir.getAbsolutePath() + " cannot be created.");
                return;
            }
        }

        Transaction trans = Transaction.queryByTransactionNumber(connection, posNumber, transNumber)
        File sheetFile = [dir, "trans_${posNumber}_${transNumber}.ps"] as File

        // run GNU's 'barcode'

        def process
        //if (trans.tmtranseq==40714)
        //    process = '/usr/local/bin/barcode -t 4x12+10+80-10-80'.execute()
        //else
            process = GNU_BARCODE.execute()

        // Input barcodes --

        def printedLineItems = []
        def discountType = ''
        if (trans.detailcnt == 0) {
            process << "${trans.dealtype1}-${trans.dealtype2}-${trans.dealtype3}\n" 
        } else {
            for (lineItem in trans.lineItems) {
                if (lineItem.codetx == 'S' ||lineItem.codetx == 'E' || lineItem.codetx == 'V' || lineItem.codetx == 'R') {
                    if (StringUtils.isEmpty(lineItem.pluno))
                        process << 'DEP:('+ lineItem.catno + '-' + lineItem.midcatno + '-' + lineItem.microcatno + ')'
                    else
                        process << lineItem.pluno
                    process << '\n\n'
                    printedLineItems << lineItem
                } else if (lineItem.codetx == 'H') {
                    process << 'CASH-IN-'
                    process << '\n\n'
                    printedLineItems << lineItem
                } else if (lineItem.codetx == 'G') {
                    process << 'CASH-OUT'
                    process << '\n\n'
                    printedLineItems << lineItem
                } else if (lineItem.codetx == 'D') {
                    discountType = lineItem.pluno == '00' ? 'Employee' : lineItem.pluno == '01' ? 'SC' : 'Unknown'
                }
            }
        }
        process.out.close()

        // Read postscript output and add some other transaction information --

        def buffer = new StringBuilder()
        def itemIdx = 0
        process.in.withReader { Reader reader ->
            def line
            def xpos
            def ypos

            while ((line = reader.readLine()) != null) {
                //println line
                if (line == 'showpage') {
                    // Print transaction information --
                    def annotatedid = (StringUtils.isEmpty(trans.annotatedid)) ? '' : ", Delivery No: ${trans.annotatedid}"
                    def voidSeq = (trans.voidseq == null) ? '' : ", Void Seq: ${trans.voidseq}"
                    def lineSpace = 16

                    buffer << """
/Simsun findfont
14 scalefont
setfont
newpath
20 ${ypos -= 30} moveto
([${trans.dealtype1}${trans.dealtype2}${trans.dealtype3}] POS: ${trans.posno}, Trans No: ${trans.tmtranseq}${annotatedid}${voidSeq}) show
20 ${ypos -= lineSpace} moveto
(Date: ${trans.sysdate}, Z=${trans.eodcnt}, Shift=${trans.signonid}) show
"""
                    if (trans.detailcnt > 0) buffer << """
20 ${ypos -= lineSpace} moveto
(Gross sales: ${trans.grosalamt}) show
20 ${ypos -= lineSpace} moveto
(   Discount: ${discountType} ${new HYIDouble(0).addMe(trans.sipamt0).addMe(trans.sipamt1).addMe(trans.sipamt2).addMe(trans.sipamt3).addMe(trans.sipamt4).negate()}) show
20 ${ypos -= lineSpace} moveto
(  Net sales:  ${trans.netsalamt}) show

20 ${ypos -= lineSpace} moveto
(Payment ----------------------------) show
20 ${ypos -= lineSpace} moveto
(${trans.getNitoriPaymentEnglishName(0)}: ${trans.payamt1}, ${trans.getNitoriPaymentEnglishName(1)}: ${trans.payamt2}) show
20 ${ypos -= lineSpace} moveto
(${trans.getNitoriPaymentEnglishName(2)}: ${trans.payamt3}, ${trans.getNitoriPaymentEnglishName(3)}: ${trans.payamt4}) show
20 ${ypos -= lineSpace} moveto
(     Change:  ${trans.changeamt}, Over Amount:  ${trans.overamt}) show

"""
                    buffer << "showpage\n"
                    continue
                }

                buffer << line
                buffer << '\n'

                if (line.startsWith('    [(')) {
                    def w = line.trim().split('\\s+')
                    xpos = w[1].toDouble() + 26
                    ypos = w[2].toDouble()
                    continue
                }
                if (line.startsWith('% End barcode') && trans.detailcnt > 0) {
                    LineItem lineItem = printedLineItems[itemIdx]
                    buffer << """
/Simsun findfont
16 scalefont
setfont
newpath
${xpos} ${ypos} moveto
(${lineItem.codetx} @${lineItem.unitprice.toString()[0..-4]} x ${lineItem.qty.toString()[0..-4]}) show
"""
                    itemIdx++
                }
            }
        }

        sheetFile.text = buffer.toString()
        println "${sheetFile} is created."
     }
}