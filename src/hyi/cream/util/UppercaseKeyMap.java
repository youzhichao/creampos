package hyi.cream.util;

import java.util.HashMap;
import org.apache.commons.lang.StringUtils;
/**
 * A map always uses upper-case string as key
 * 
 * @author Bruce You
 */
public class UppercaseKeyMap extends HashMap {

    private static final long serialVersionUID = 1L;
    
    public Object put(Object key, Object value) {
        return super.put(((String)key).toUpperCase(), value);
    }

    public Object get(Object key) {
         Object o = super.get(((String)key).toUpperCase());
         
         if (o instanceof String) { // 截掉后面的空格
        	 o = StringUtils.stripEnd((String) o, null);
         }
         return o;
    }
}