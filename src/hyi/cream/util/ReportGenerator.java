package hyi.cream.util;

import hyi.cream.dac.*;
import hyi.cream.groovydac.Param;
import hyi.cream.state.GetProperty;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;

public class ReportGenerator {

	private static final int RIGHT_ALIGN = 0;
	private static final int LEFT_ALIGN = 1;
	private DacBase dac = null;

//	private String jkdPayAmountFormatter = "#############0.00";
//	private String jkdPayCountFormatter = "######0";

	/**
	 * Constructor.
	 * 
	 * @param dac
	 *            the associated DAC object.
	 */
	public ReportGenerator(DacBase dac) {
		this.dac = dac;
	}

	/**
	 * According to the report definition file of the DAC object, generate
	 * report and output to the given Writer.
	 * 
	 * @param writer
	 *            the output Writer.
	 */
	public Writer generate(Writer writer) {
		return generate(writer, false);
	}

	public Writer generate(Writer writer, boolean x) {
		boolean noText = true;
		boolean inOne = false;
		String line = "";
		Class c = dac.getClass();
		File defFile = CreamToolkit.getConfigurationFile(c);
		CreamFormatter cr = CreamFormatter.getInstance();

		// read report define file(z or shift)

		// output to the writer

		try {
			FileInputStream filein = new FileInputStream(defFile);
			InputStreamReader inst = null;

			inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
			BufferedReader in = new BufferedReader(inst);

			line = in.readLine();
			boolean isFirst = true;

			do {
				String oneLine = "";
				StringTokenizer t = new StringTokenizer(line, "[],", true);

				do {

					// check if blank line
					if (t.countTokens() == 0) {
						noText = true;
						break;
					}

					// get header
					String head = t.nextToken();

					// check if no content
					if (!t.hasMoreElements()) {
						if (x && head.indexOf("Z") != -1) {
							head = head.replace('Z', 'X');
						}
						if (inOne) {
							oneLine = oneLine + head;
						} else {
							oneLine = head;
							noText = false;
						}
						break;
					}

					// check if no header
					if (head.equals("[")) {
						head = "";
					} else {
						t.nextToken();
					}

					// get property name
					String propName = t.nextToken();
					Object valueObject = null;

					// do for payment of jiekuandan
					if (dac != null && dac instanceof CashForm) {
						if (propName.equalsIgnoreCase("PayXX")) {
							CashForm cashForm = (CashForm) dac;
							if (cashForm == null)
								break;

							Iterator itr = Payment.getAllPayment();
							if (itr == null)
								break;

							while (itr.hasNext()) {
								Payment p = (Payment) itr.next();
								head = p.getScreenName();
								String payID = p.getPaymentID();
								if (payID.equals("00")) // skip cash
									continue;
								String key1 = "pay" + payID + "Amount";
								String key2 = "pay" + payID + "Count";
								if (cashForm.getFieldValue(key1) == null) {
									continue;
								}
								HYIDouble value1 = (HYIDouble) cashForm
										.getFieldValue(key1);
								if (value1 == null)
									continue;
								if (value1.compareTo(new HYIDouble(0)) == 0) {
									continue;
								}
								Integer value2 = (Integer) cashForm
										.getFieldValue(key2);
								if (value2 == null)
									continue;
								String payAmount = "";
								String payCount = "";
								// payAmount = cr.format(value1.doubleValue(),
								// jkdPayAmountFormatter);
								payAmount = padBlank(value1, RIGHT_ALIGN, 17);
								// payCount = cr.format(value2.intValue(),
								// jkdPayCountFormatter);
								payCount = padBlank(value2.intValue(),
										RIGHT_ALIGN, 7);
								oneLine = "  "
										+ padBlank(head, LEFT_ALIGN, 10)
										+ payCount
										+ CreamToolkit.GetResource().getString(
												"aSheetOf") + "\n";
								writer.write(oneLine);

								oneLine = "  "
										+ payAmount
										+ CreamToolkit.GetResource().getString(
												"Dollar") + "\n";
								writer.write(oneLine);
							}
							noText = true;
							break;
						}
					}

					// get property value by name

					if (propName.equalsIgnoreCase("new Date")) {
						valueObject = new Date();
					} else if (isFirst && dac != null && dac instanceof ZReport
							&& propName.equalsIgnoreCase("ZPayXX")) {
						isFirst = false;

						ZReport zReport = (ZReport) dac;
						if (zReport == null)
							break;

						Iterator itr = Payment.getAllPayment();
						if (itr == null)
							break;

						while (itr.hasNext()) {
							Payment p = (Payment) itr.next();
							head = p.getScreenName();
							String payID = p.getPaymentID();
							if (payID.equals("00")) // skip cash
								continue;
							String key1 = "PAY" + payID + "AMT";
							String key2 = "PAY" + payID + "CNT";

							if (zReport.getFieldValue(key1) == null) {
								continue;
							}
							HYIDouble value1 = (HYIDouble) zReport
									.getFieldValue(key1);
							if (value1 == null)
								continue;
							if (value1.compareTo(new HYIDouble(0)) == 0) {
								continue;
							}

							Integer value2 = (Integer) zReport
									.getFieldValue(key2);
							if (value2 == null)
								continue;

							String payAmount = "";
							String payCount = "";
							// payAmount = cr.format(value1.doubleValue(),
							// jkdPayAmountFormatter);
							payAmount = padBlank(value1, RIGHT_ALIGN, 12);
							// payCount = cr.format(value2.intValue(),
							// jkdPayCountFormatter);
							payCount = padBlank(value2.intValue(), RIGHT_ALIGN,
									22);
							oneLine = padBlank(head, LEFT_ALIGN, 12)
									+ payAmount + "\n";
							writer.write(oneLine);
							oneLine = payCount
									+ CreamToolkit.GetResource()
											.getString("Ci") + "\n";
							writer.write(oneLine);

						}
						noText = true;
						break;
					} else if (isFirst && dac != null
							&& dac instanceof ShiftReport
							&& propName.equalsIgnoreCase("SHIFTPayXX")) {
						isFirst = false;

						ShiftReport shiftReport = (ShiftReport) dac;
						if (shiftReport == null)
							break;

						Iterator itr = Payment.getAllPayment();
						if (itr == null)
							break;

						while (itr.hasNext()) {
							Payment p = (Payment) itr.next();
							head = p.getScreenName();
							String payID = p.getPaymentID();
							if (payID.equals("00")) // skip cash
								continue;
							String key1 = "PAY" + payID + "AMT";
							String key2 = "PAY" + payID + "CNT";

							if (shiftReport.getFieldValue(key1) == null) {
								continue;
							}
							HYIDouble value1 = (HYIDouble) shiftReport
									.getFieldValue(key1);
							if (value1 == null)
								continue;
							if (value1.compareTo(new HYIDouble(0)) == 0) {
								continue;
							}

							Integer value2 = (Integer) shiftReport
									.getFieldValue(key2);
							if (value2 == null)
								continue;

							String payAmount = "";
							String payCount = "";
							// payAmount = cr.format(value1.doubleValue(),
							// jkdPayAmountFormatter);
							payAmount = padBlank(value1, RIGHT_ALIGN, 12);
							// payCount = cr.format(value2.intValue(),
							// jkdPayCountFormatter);
							payCount = padBlank(value2.intValue(), RIGHT_ALIGN,
									22);
							oneLine = padBlank(head, LEFT_ALIGN, 12)
									+ payAmount + "\n";
							writer.write(oneLine);
							oneLine = payCount
									+ CreamToolkit.GetResource()
											.getString("Ci") + "\n";
							writer.write(oneLine);

						}
						noText = true;
						break;
					} else {
						Method method = dac.getClass().getDeclaredMethod(
								"get" + propName, new Class[0]);
						valueObject = method.invoke(dac, new Object[0]);
					}

					// overleap ','
					t.nextToken();

					// get property's formatter
					String formatter = t.nextToken();

					// check if no formatter ( for String )
					if (formatter.equals(",")) {
						formatter = "";
					} else {
						t.nextToken();
					}

					// get property's String by formatter
					String formated = "";
					if (valueObject instanceof Date) {
						formated = cr.format((Date) valueObject, formatter);
					} else if (valueObject instanceof HYIDouble) {
						formated = cr.format(((HYIDouble) valueObject)
								.doubleValue(), formatter);
					} else if (valueObject instanceof Integer) {
						formated = cr.format(
								((Integer) valueObject).intValue(), formatter);
					} else if (valueObject instanceof String) {
						formated = (String) valueObject;
					}

					// get align mode
					String al = t.nextToken();

					// get direction : 'L' means left; 'R' means right
					String align = al.substring(0, 1);

					// get string's length
					int length = Integer.parseInt(al.substring(1))
							- formated.length();

					// do for string's length
					for (int i = 0; i < length; i++) {
						if (align.equalsIgnoreCase("R")) {
							formated = " " + formated;
						} else {
							formated = formated + " ";
						}
					}
					noText = false;

					// get one property string
					oneLine = oneLine + head + formated;

					// overleap ']'
					t.nextToken();
					inOne = true;
				} while (t.hasMoreElements());

				// do for blank line
				if (noText) {
					oneLine = " ";
				}

				// add enter to per line's end
				oneLine = oneLine + "\n";
				writer.write(oneLine);
				// System.out.println(oneLine);

				// do for next line
				line = in.readLine();
				inOne = false;
			} while (line != null);

//			String blank = "";
//			if (!GetProperty.getDepSalesPrintBlank("").trim().equals("")) {
//				try {
//					int l = Integer.parseInt(GetProperty.getDepSalesPrintBlank(""));
//					for (int i = 0; i < l; i++)
//						blank += " ";
//				} catch (Exception e) {
//				}
//			}

			if (Param.getInstance().isDepSalesPrint()
					&& dac != null && dac instanceof ZReport) {

                DbConnection connection = null;
                try {
                    connection = CreamToolkit.getPooledConnection();
                    ZReport z = (ZReport) dac;
    				Iterator ite = DepSales.queryBySequenceNumber(connection, z.getSequenceNumber());
    				if (ite != null) {
    					while (ite.hasNext()) {
    						DepSales d = (DepSales) ite.next();
    						String id = d.getDepID();
    						Dep dep = Dep.queryByDepID(id);
    						String name;
    						if (dep == null)
    							name = id;
    						else
    							name = Dep.queryByDepID(id).getDepName();
    						line = id + "   " + name + "\n";
    						writer.write(line);
    						line = "      " + d.getGrossSaleTotalAmount() + "\n";
    						writer.write(line);
    					}
    					writer.write("\n\n\n\n\n\n\n\n");
    				} else {
    					CreamToolkit.logMessage("ReportGenerator: DepSales is null");
                    }
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                } finally {
                    CreamToolkit.releaseConnection(connection);
                }
			}

		} catch (FileNotFoundException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("Configuration file of " + c.toString()
					+ " is not found.");
		} catch (IOException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("IOException at " + this);
		} catch (InvocationTargetException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("invocation target exception at " + this);
		} catch (IllegalAccessException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("Illegal access exception at " + this);
		} catch (NoSuchMethodException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("No such method at " + this);
			System.out.println(e);
		}
		return writer;
	}

	private String padBlank(String value, int align, int digits) {
		StringBuffer fld = new StringBuffer(value);
		try {
			while (fld.toString().getBytes(GetProperty.getConfFileLocale()).length < digits) { // pad space
				if (align == RIGHT_ALIGN)
					fld.insert(0, ' ');
				else
					fld.append(' ');
			}
		} catch (UnsupportedEncodingException e) {
		}
		return fld.toString();
	}

	private String padBlank(int value, int align, int digits) {
		StringBuffer fld = new StringBuffer("" + value);
		while (fld.length() < digits) { // pad space
			if (align == RIGHT_ALIGN)
				fld.insert(0, ' ');
			else
				fld.append(' ');
		}
		return fld.toString();
	}

	private String padBlank(HYIDouble value, int align, int digits) {
		value = value.setScale(2, BigDecimal.ROUND_HALF_UP);
		StringBuffer fld = new StringBuffer(value.toString());
		while (fld.length() < digits) { // pad space
			if (align == RIGHT_ALIGN)
				fld.insert(0, ' ');
			else
				fld.append(' ');
		}
		return fld.toString();
	}

	// public static void main(String[] args) {
	// ZReport z = ZReport.getOrCreateCurrentZReport();
	// //z.setSystemDateTime(new Date());
	// z.setTerminalNumber(new Integer(001001));
	// z.setStoreNumber("999");
	// z.setSequenceNumber(new Integer(987654321));
	// //z.setGrandTotal(new HYIDouble(78));
	// //z.setItemCountGrandTotal(new HYIDouble(321));
	// //z.setTransactionCountGrandTotal(new Integer(567));
	// ReportGenerator rg = new ReportGenerator(z);
	// //rg.generate(new BufferedWriter(new OutputStreamWriter(System.out)));
	// StringWriter sw = (StringWriter)rg.generate(new StringWriter(), true);
	// StringBuffer sb = sw.getBuffer();
	// System.out.println(sw.toString());
	// }
}
