package hyi.cream.util;

//Decompiled by DJ v3.7.7.81 Copyright 2004 Atanas Neshkov  Date: 2006-08-09 16:21:57
//Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
//Decompiler options: packimports(3) 
//Source File Name:   BMPImageProcessor.java

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.MemoryImageSource;
import java.io.FileInputStream;

//Referenced classes of package com.starmicronics.utility:
//         BMPImageProcessorException

public class BMPImageProcessor {

	public BMPImageProcessor() {
	}

	public static Image getImageFromBMPFile(String bmpFilePath)
			throws Exception {
		Image image = null;
		try {
			FileInputStream fs = new FileInputStream(bmpFilePath);
			int bflen = 14;
			byte bf[] = new byte[bflen];
			fs.read(bf, 0, bflen);
			int bilen = 40;
			byte bi[] = new byte[bilen];
			fs.read(bi, 0, bilen);
			int nsize = (bf[5] & 0xff) << 24 | (bf[4] & 0xff) << 16
					| (bf[3] & 0xff) << 8 | bf[2] & 0xff;
			int nbisize = (bi[3] & 0xff) << 24 | (bi[2] & 0xff) << 16
					| (bi[1] & 0xff) << 8 | bi[0] & 0xff;
			int nwidth = (bi[7] & 0xff) << 24 | (bi[6] & 0xff) << 16
					| (bi[5] & 0xff) << 8 | bi[4] & 0xff;
			int nheight = (bi[11] & 0xff) << 24 | (bi[10] & 0xff) << 16
					| (bi[9] & 0xff) << 8 | bi[8] & 0xff;
			int nplanes = (bi[13] & 0xff) << 8 | bi[12] & 0xff;
			int nbitcount = (bi[15] & 0xff) << 8 | bi[14] & 0xff;
			int ncompression = bi[19] << 24 | bi[18] << 16 | bi[17] << 8
					| bi[16];
			int nsizeimage = (bi[23] & 0xff) << 24 | (bi[22] & 0xff) << 16
					| (bi[21] & 0xff) << 8 | bi[20] & 0xff;
			int nxpm = (bi[27] & 0xff) << 24 | (bi[26] & 0xff) << 16
					| (bi[25] & 0xff) << 8 | bi[24] & 0xff;
			int nypm = (bi[31] & 0xff) << 24 | (bi[30] & 0xff) << 16
					| (bi[29] & 0xff) << 8 | bi[28] & 0xff;
			int nclrused = (bi[35] & 0xff) << 24 | (bi[34] & 0xff) << 16
					| (bi[33] & 0xff) << 8 | bi[32] & 0xff;
			int nclrimp = (bi[39] & 0xff) << 24 | (bi[38] & 0xff) << 16
					| (bi[37] & 0xff) << 8 | bi[36] & 0xff;
			if (nbitcount == 24) {
				int npad = nsizeimage / nheight - nwidth * 3;
				int ndata[] = new int[nheight * nwidth];
				byte brgb[] = new byte[(nwidth + npad) * 3 * nheight];
				fs.read(brgb, 0, (nwidth + npad) * 3 * nheight);
				int nindex = 0;
				for (int j = 0; j < nheight; j++) {
					for (int i = 0; i < nwidth; i++) {
						ndata[nwidth * (nheight - j - 1) + i] = 0xff000000
								| (brgb[nindex + 2] & 0xff) << 16
								| (brgb[nindex + 1] & 0xff) << 8 | brgb[nindex]
								& 0xff;
						nindex += 3;
					}

					nindex += npad;
				}

				image = Toolkit.getDefaultToolkit()
						.createImage(
								new MemoryImageSource(nwidth, nheight, ndata,
										0, nwidth));
			} else if (nbitcount == 8) {
				int nNumColors = 0;
				if (nclrused > 0)
					nNumColors = nclrused;
				else
					nNumColors = 1 << nbitcount;
				if (nsizeimage == 0) {
					nsizeimage = (nwidth * nbitcount + 31 & 0xffffffe0) >> 3;
					nsizeimage *= nheight;
				}
				int npalette[] = new int[nNumColors];
				byte bpalette[] = new byte[nNumColors * 4];
				fs.read(bpalette, 0, nNumColors * 4);
				int nindex8 = 0;
				for (int n = 0; n < nNumColors; n++) {
					npalette[n] = 0xff000000
							| (bpalette[nindex8 + 2] & 0xff) << 16
							| (bpalette[nindex8 + 1] & 0xff) << 8
							| bpalette[nindex8] & 0xff;
					nindex8 += 4;
				}

				int npad8 = nsizeimage / nheight - nwidth;
				int ndata8[] = new int[nwidth * nheight];
				byte bdata[] = new byte[(nwidth + npad8) * nheight];
				fs.read(bdata, 0, (nwidth + npad8) * nheight);
				nindex8 = 0;
				for (int j8 = 0; j8 < nheight; j8++) {
					for (int i8 = 0; i8 < nwidth; i8++) {
						ndata8[nwidth * (nheight - j8 - 1) + i8] = npalette[bdata[nindex8] & 0xff];
						nindex8++;
					}

					nindex8 += npad8;
				}

				image = Toolkit.getDefaultToolkit().createImage(
						new MemoryImageSource(nwidth, nheight, ndata8, 0,
								nwidth));
			} else if (nbitcount == 1) {
				int nNumColors = 0;
				if (nclrused > 0)
					nNumColors = nclrused;
				else
					nNumColors = 1 << nbitcount;
				if (nsizeimage == 0)
					nsizeimage = 0;
				int npalette[] = new int[nNumColors];
				byte bpalette[] = new byte[nNumColors * 4];
				fs.read(bpalette, 0, nNumColors * 4);
				int nindex8 = 0;
				for (int n = 0; n < nNumColors; n++) {
					npalette[n] = 0xff000000
							| (bpalette[nindex8 + 2] & 0xff) << 16
							| (bpalette[nindex8 + 1] & 0xff) << 8
							| bpalette[nindex8] & 0xff;
					nindex8 += 4;
				}

				int npad8 = 0;
				int byteWidth = nwidth / 8;
				if (nwidth % 8 != 0)
					byteWidth++;
				if ((byteWidth + 3) % 4 == 0)
					npad8 = 3;
				else if ((byteWidth + 2) % 4 == 0)
					npad8 = 2;
				else if ((byteWidth + 1) % 4 == 0)
					npad8 = 1;
				else
					npad8 = 0;
				int ndata8[] = new int[nwidth * nheight];
				byte bdata[] = new byte[(byteWidth + npad8) * nheight];
				fs.read(bdata, 0, (byteWidth + npad8) * nheight);
				nindex8 = 0;
				int pixelIndex = 1;
				int paddedWidth = byteWidth + npad8;
				for (int y = 0; y < nheight; y++) {
					for (int x = 0; x < nwidth; x++) {
						int sourceHorizontalPos = x / 8;
						int pixelShift = x % 8;
						int sourceIndex = (nheight - (y + 1)) * paddedWidth
								+ sourceHorizontalPos;
						byte sourcePixel = (byte) (bdata[sourceIndex] & 128 >> pixelShift);
						int destinationIndex = y * nwidth + x;
						ndata8[destinationIndex] = npalette[sourcePixel != 0 ? 1
								: 0];
					}

				}

				image = Toolkit.getDefaultToolkit().createImage(
						new MemoryImageSource(nwidth, nheight, ndata8, 0,
								nwidth));
			} else {
				image = (Image) null;
			}
			fs.close();
		} catch (Exception e) {
			throw e;
		}
		return image;
	}

	public static void createBMPFileFromImage(Image image, String s)
			throws Exception {
	}
}