package hyi.cream.util;

import hyi.cream.groovydac.Param;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;

public final class SignUtil
{

    public SignUtil()
    {
    }

    public static String cmpaysign(String dataString, String merId)
    {
        System.out.println("要签名的数据：" + dataString);

        byte signeddata[] = (byte[])null;
        String prkpath = "";
        if(merId == null || "".equals(merId))
            return "";
        prkpath = (new StringBuilder(String.valueOf(KEYS_PATH_PREFIX))).append("/")/*.append(merId).append("/")*/.append(merId).append("_prikey.dat").toString();
        try {
            byte data[] = dataString.getBytes("utf-8");
            FileInputStream f = new FileInputStream(prkpath);
            ObjectInputStream b = new ObjectInputStream(f);
            RSAPrivateKey prk = (RSAPrivateKey)b.readObject();
            Signature s = Signature.getInstance("MD5WithRSA");
            s.initSign(prk);
            s.update(data);
            signeddata = s.sign();
        }
        catch(Exception e){}
        return bytesToHexStr(signeddata);
    }

    public static final String bytesToHexStr(byte bcd[])
    {

        StringBuffer s = new StringBuffer(bcd.length * 2);
        for(int i = 0; i < bcd.length; i++)
        {
            s.append(bcdLookup[bcd[i] >>> 4 & 0xf]);
            s.append(bcdLookup[bcd[i] & 0xf]);
        }
        return s.toString();
    }

    private static final char bcdLookup[] = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f'
    };

    private static final String KEYS_PATH_PREFIX = Param.getInstance().getConfigDir();

}
