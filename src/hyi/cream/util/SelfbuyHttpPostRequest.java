package hyi.cream.util;


import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.*;
import hyi.cream.groovydac.Param;
import hyi.cream.groovydac.Property;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.mortbay.util.ajax.JSON;

/**
 * @author : liupingping
 *
 * 自助收银接口
 */
public class SelfbuyHttpPostRequest {

    public static String appid = Param.getInstance().getSelfAppid();//身份标识
    public static String shopid = Store.getSelfbuyShopid();//erp门店号
    public static String selfbuySign =Param.getInstance().getSelfSign();//签名密钥
    public static String selfbuyUrl = Param.getInstance().getSelfUrl();
    private static POSTerminalApplication app = POSTerminalApplication.getInstance();
    transient protected static final Param PARAM = Param.getInstance();
    public static int ii = 1;
    public static String classesDate = "";

    public static String  getPropery() {

        if (appid == null || appid.equals(""))
            return null;
        System.out.println("appid：" + appid);
        CreamToolkit.logMessage("appid："+appid);
        if (shopid == null || shopid.equals(""))
            return null;
        System.out.println("shopid：" + shopid);
        CreamToolkit.logMessage("shopid："+shopid);
        if (selfbuySign == null || selfbuySign.equals(""))
            return null;
        System.out.println("sign：" + selfbuySign);
        CreamToolkit.logMessage("sign: " + selfbuySign);

        return "";
    }



    public static HashMap postHttpSelfbuy (String openId) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String format = sdf.format(date);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int selfstartdate = Param.getInstance().getSelfstartdate() * -1;
        c.add(Calendar.DAY_OF_MONTH, selfstartdate);// 今天-n天
        Date tomorrow = c.getTime();
        String format1 = sdf1.format(date);
        String format2 = sdf1.format(tomorrow);
//        String url = "order/query";
        String orderId = Param.getInstance().getMaxorderid();
        String sign = DigestUtils.shaHex("appid="+appid+"&body={\"orderid\":"+orderId+",\"shopid\":\""+shopid+"\",\"startdate\":\""+format2+"\",\"enddate\":\""+format1+"\"}&nonce_str="+format+"&secret="+selfbuySign).toLowerCase();
        try {
            openId = new String(("{\n" +
                    "    \"appid\": \""+appid+"\",\n" +
                    "    \"body\": \"{\\\"orderid\\\":"+orderId+",\\\"shopid\\\":\\\""+shopid+"\\\",\\\"startdate\\\":\\\""+format2+"\\\",\\\"enddate\\\":\\\""+format1+"\\\"}\",\n" +
                    "    \"nonce_str\": \""+format+"\",\n" +
                    "    \"sign\": \""+sign+"\"\n" +
                    "}").getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return getResult(openId);
    }

    //https通讯
    private static HashMap getResult(String openId) {
        CreamToolkit.logMessage(openId);
        System.out.println("openId："+openId);

        String ss = selfbuyUrl;
        CreamToolkit.logMessage("url: "+ss);
        System.out.println("url: "+ss);

        Object rstObject = null;

        try {
            // 4.执行postMethod,调用http接口
            URL url = new URL(ss);
            int timeout = 30;
            timeout = timeout * 1000;
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            conn.setConnectTimeout(timeout);
//            conn.setReadTimeout(10000)
            conn.setReadTimeout(timeout);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("content-type","application/json");

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(openId);
            wr.flush();

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            String result = "";
            while ((line = rd.readLine()) != null) {
                result += line;
            }

           /* int s1 = (ii+1);
            int s2 = (ii+2);
            result = "{\n" +
                    "    \"body\": \"{\\\"total\\\":14,\\\"maxorderid\\\":31,\\\"orders\\\":[{\\\"orderid\\\":5,\\\"orderno\\\":\\\"25253336844628498\\\",\\\"tradetype\\\":1,\\\"shopid\\\":\\\"1001\\\",\\\"orderedat\\\":\\\"2017-10-26T16:50:02\\\",\\\"total\\\":3.80,\\\"discount\\\":3.79,\\\"payment\\\":\\\"支付宝\\\",\\\"paid\\\":0.01,\\\"paidat\\\":\\\"2017-10-27T17:15:33\\\",\\\"goods\\\":[{\\\"goodsno\\\":\\\"100001\\\",\\\"num\\\":1.000,\\\"price\\\":3.80,\\\"paid\\\":0.01,\\\"discount\\\":3.79}]},{\\\"orderid\\\":31,\\\"orderno\\\":\\\"25325273620676649\\\",\\\"tradetype\\\":1,\\\"shopid\\\":\\\"1001\\\",\\\"orderedat\\\":\\\"2017-11-16T15:25:38\\\",\\\"total\\\":2.00,\\\"discount\\\":0.32,\\\"payment\\\":\\\"支付宝\\\",\\\"paid\\\":1.68,\\\"paidat\\\":\\\"2017-11-21T15:15:49\\\",\\\"goods\\\":[{\\\"barcode\\\":\\\"6922266440571\\\",\\\"goodsno\\\":\\\"2000847\\\",\\\"num\\\":1.000,\\\"price\\\":1.00,\\\"paid\\\":0.68,\\\"discount\\\":0.32},{\\\"barcode\\\":\\\"6940490790408\\\",\\\"goodsno\\\":\\\"2000956\\\",\\\"num\\\":1.000,\\\"price\\\":1.00,\\\"paid\\\":1.00,\\\"discount\\\":0.00}]}]}\",\n" +
                    "    \"message\": \"成功获取了[2]个订单\",\n" +
                    "    \"status\": 0\n" +
                    "}";
            ii = s2;*/
            CreamToolkit.logMessage("kargo-result:" + result);
            System.out.println("kargo-result:" + result);


            //result=null ,!=null?
            if (result == null || "".equals(result)) {
                return null;
            } else {
                rstObject = JSON.parse(result);   //!=null
            }

            wr.close();
            rd.close();

            return (HashMap) rstObject;
        } catch (Exception e) {
            e.printStackTrace();
            CreamToolkit.logMessage(e);

        }
        return null;
    }


    public static void main(String [] args) {

        try {
            DbConnection connection =  CreamToolkit.getTransactionalConnection();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void selfbuyPost() {
        DbConnection connection =  null;
        boolean autoCommit = false;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            HashMap lm = SelfbuyHttpPostRequest.postHttpSelfbuy("");
            HashMap lm1 = (HashMap) JSON.parse(lm.get("body").toString());
            Object[] head = (Object[]) lm1.get("orders");
            Object[] detail;
            connection =  CreamToolkit.getTransactionalConnection();
            autoCommit = connection.getAutoCommit();
            connection.setAutoCommit(false);
            POSTerminalApplication app = POSTerminalApplication.getInstance();
            Transaction trans =  app.getCurrentTransaction();


            for(int i = 0; i < head.length; i++){
                Selfbuy_head sh = new Selfbuy_head();
                Alipay_detail ad = new Alipay_detail();
                HYIDouble grosalamt = new HYIDouble(0);
                HYIDouble saleamt = new HYIDouble(0);
                HYIDouble payamt1 = new HYIDouble(0);
                HYIDouble netsalamt = new HYIDouble(0);
                trans.getSelfbuyHeadList().clear();
                trans.getSelfbuyDetailList().clear();
                trans.getAlipayList().clear();
                trans.init(connection);
                detail = (Object[]) ((HashMap)(((Object[])lm1.get("orders"))[i])).get("goods");
                CreamToolkit.logMessage("Selfbuy_head_length：" + head.length+"-----"+i);
                System.out.println("Selfbuy_head_length:" + head.length+"-----"+i);
                sh.setStoreId(Store.getStoreID());
                sh.setOrderId(Integer.valueOf((((HashMap)(head[i])).get("orderid").toString())));
                sh.setOrderNo((((HashMap)(head[i])).get("orderno")) == null ? "" : ((HashMap)(head[i])).get("orderno").toString());
                sh.setTradetype(Integer.valueOf((((HashMap)(head[i])).get("tradetype").toString())));
                sh.setShopid((((HashMap)(head[i])).get("shopid")) == null ? "" : ((HashMap)(head[i])).get("shopid").toString());
                sh.setOrderedat((((HashMap)(head[i])).get("orderedat")) == null ? new Date() : format.parse((((HashMap) (head[i])).get("orderedat").toString())));
                sh.setTotal(new HYIDouble((Double) ((HashMap)(head[i])).get("total")));
                sh.setDiscount(new HYIDouble((Double) ((HashMap)(head[i])).get("discount")));
                sh.setPayment((((HashMap)(head[i])).get("payment")) != null && (((HashMap)(head[i])).get("payment")).toString().equals("支付宝") ? "16" : "");
                sh.setPaid(new HYIDouble((Double) ((HashMap)(head[i])).get("paid")));
                sh.setPaidat((((HashMap)(head[i])).get("paidat")) == null ? new Date() : format.parse((((HashMap) (head[i])).get("paidat").toString())));
                sh.setTmtranseq(trans.getTransactionNumber());
                sh.setSYSTEMDATE(new Date());
                sh.setBuyerLoginId((((HashMap)(head[i])).get("buyer_id")) == null ? "" : ((HashMap)(head[i])).get("buyer_id").toString());
                sh.setTradeNo((((HashMap)(head[i])).get("transaction_id")) == null ? "" : ((HashMap)(head[i])).get("transaction_id").toString());
//                sh.insert(connection);
                trans.addSelfbuyHeadList(sh);
                CreamToolkit.logMessage(" sh.insert(connection);");
                ad.setSTORENUMBER(Store.getStoreID());
                ad.setPOSNUMBER(99);
                ad.setTRANSACTIONNUMBER(trans.getTransactionNumber());
                ad.setZSeq(trans.getZSequenceNumber());
                ad.setTOTAL_FEE(new HYIDouble((Double) ((HashMap)(head[i])).get("paid")));
                ad.setTRADE_NO((((HashMap)(head[i])).get("transaction_id")) == null ? "" : ((HashMap)(head[i])).get("transaction_id").toString());
                ad.setSYSTEMDATE(new Date());
                ad.setSEQ(i < 10 ? "0"+i : i+"");
                ad.setBUYER_LOGON_ID((((HashMap)(head[i])).get("buyer_id")) == null ? "" : ((HashMap)(head[i])).get("buyer_id").toString());
                trans.addAlipayList(ad);
                trans.setTransactionNumber(trans.getTransactionNumber());
                trans.setSystemDateTime((((HashMap)(head[i])).get("orderedat")) == null ? new Date() : format.parse((((HashMap) (head[i])).get("orderedat").toString())));
                for(int j = 0; j < detail.length; j++){
                    Selfbuy_detail sd = new Selfbuy_detail();
                    CreamToolkit.logMessage("Selfbuy_detail");
                    sd.setStoreId(Store.getStoreID());
                    sd.setOrderId(Integer.valueOf((((HashMap)(head[i])).get("orderid")).toString()));
                    sd.setOrderNo((((HashMap)(head[i])).get("orderno")) == null ? "" : ((HashMap)(head[i])).get("orderno").toString());
                    sd.setSEQ((j+1)+"");
                    sd.setBarcode((((HashMap)detail[j]).get("barcode")) == null ? "" : ((HashMap)detail[j]).get("barcode").toString());
                    sd.setGoodsno((((HashMap)detail[j]).get("goodsno")) == null ? "" : ((HashMap)detail[j]).get("goodsno").toString());
                    sd.setPromotionno((((HashMap)detail[j]).get("promotionno")) == null ? "" : ((HashMap)detail[j]).get("promotionno").toString());
                    sd.setNum(Integer.valueOf((int) Double.parseDouble(((HashMap)detail[j]).get("num").toString())));
                    sd.setPrice(new HYIDouble((Double) ((HashMap)detail[j]).get("price")));
                    sd.setPaid(new HYIDouble((Double) ((HashMap)detail[j]).get("paid")));
                    sd.setDiscount(new HYIDouble((Double) ((HashMap)detail[j]).get("discount")));
                    sd.setTmtranseq(trans.getTransactionNumber());
                    sd.setSYSTEMDATE(new Date());
//                    sd.insert(connection);
                    trans.addSelfbuyDetailList(sd);
                    int orderid = Integer.valueOf((((HashMap)(head[i])).get("orderid")).toString());
                    String orderno = (((HashMap)(head[i])).get("orderno")) == null ? "" : ((HashMap)(head[i])).get("orderno").toString();
                    if(LineItem.getOrderIdAndOrderNo(connection,orderid,orderno) > 0){
                        continue;
                    }
                    LineItem li = new LineItem();
                    String number = (((HashMap)detail[j]).get("barcode")) == null ? "" : ((HashMap)detail[j]).get("barcode").toString();
                    PLU plu = PLU.queryBarCode(number);
                    if (plu == null) {
                        number = (((HashMap) detail[j]).get("goodsno")) == null ? "" : ((HashMap) detail[j]).get("goodsno").toString();
                        plu = PLU.queryByInStoreCode(number);
                        if(plu == null) {
                            CreamToolkit.logMessage("该商品不存在:" + number);
                            System.out.println("该商品不存在:" + number);
                        }
                    }
                    li.setTransactionNumber(trans.getTransactionNumber());
                    li.setLineItemSequence((j+1));
                    li.setPluNumber((((HashMap)detail[j]).get("barcode")) == null ? "" : ((HashMap)detail[j]).get("barcode").toString());
                    li.setItemNumber((((HashMap)detail[j]).get("goodsno")) == null ? "" : ((HashMap)detail[j]).get("goodsno").toString());
                    li.setQuantity(new HYIDouble((Double)  ((HashMap)detail[j]).get("num")));
                    li.setAmount(new HYIDouble((Double) ((HashMap)detail[j]).get("price")));
                    li.setUnitPrice(new HYIDouble((Double) ((HashMap)detail[j]).get("price")));
                    li.setAfterDiscountAmount(new HYIDouble((Double) ((HashMap)detail[j]).get("paid")));
                    li.setRemoved(false);
                    li.setTerminalNumber(trans.getTerminalNumber());
                    li.setDetailCode("S");
                    li.setCategoryNumber(plu != null ? plu.getCategoryNumber() : "");
                    li.setMidCategoryNumber(plu != null ? plu.getMidCategoryNumber() : "");
                    li.setMicroCategoryNumber(plu != null ? plu.getMicroCategoryNumber() : "");
                    li.setThinCategoryNumber(plu != null ? plu.getThinCategoryNumber() : "");
                    li.setTaxType(plu != null ? plu.getTaxType() : "");
                    li.setSelfbuyOrderid(Integer.valueOf((((HashMap)(head[i])).get("orderid")).toString()));
                    li.setSelfbuyOrderno((((HashMap)(head[i])).get("orderno")) == null ? "" : ((HashMap)(head[i])).get("orderno").toString());
//                    li.insert(connection);
                    trans.addLineItem(li);
                    HYIDouble num = new HYIDouble((Double)  ((HashMap)detail[j]).get("num"));
                    grosalamt = grosalamt.add(new HYIDouble((Double) ((HashMap)detail[j]).get("price")).multiply(num));
                    saleamt = saleamt.add(new HYIDouble((Double) ((HashMap)detail[j]).get("paid")).multiply(num));
                    payamt1 = payamt1.add(new HYIDouble((Double) ((HashMap)detail[j]).get("paid")).multiply(num));
                    netsalamt = netsalamt.add(new HYIDouble((Double) ((HashMap)detail[j]).get("paid")).multiply(num));

                }
                int orderid = Integer.valueOf((((HashMap)(head[i])).get("orderid")).toString());
                String orderno = (((HashMap)(head[i])).get("orderno")) == null ? "" : ((HashMap)(head[i])).get("orderno").toString();
                if(Transaction.getOrderIdAndOrderNo(connection,orderid,orderno) > 0){
                    continue;
                }
                trans.setGrossSalesAmount(grosalamt);//没有优惠的总金额
                trans.setSalesAmount(saleamt);//支付总金额
                trans.setPayNumber1("16");//支付宝
                trans.setPayAmount1(payamt1);//支付总金额
                trans.setNetSalesAmount(netsalamt);//优惠后总金额
                trans.setSelfbuyOrderid(Integer.valueOf((((HashMap)(head[i])).get("orderid")).toString()));
                trans.setSelfbuyOrderno((((HashMap)(head[i])).get("orderno")) == null ? "" : ((HashMap)(head[i])).get("orderno").toString());
                trans.accumulate();
                trans.store(connection);
                CreamToolkit.logMessage("trans.store(connection);");
            }
            int maxorderid = Integer.valueOf(lm1.get("maxorderid").toString());
            if(maxorderid > Integer.valueOf(Param.getInstance().getMaxorderid())){
                Param.getInstance().updateMaxorderid(lm1.get("maxorderid").toString());
            }
            connection.commit();
            connection.setAutoCommit(autoCommit);
            trans.getSelfbuyHeadList().clear();
            trans.getSelfbuyDetailList().clear();
            trans.getAlipayList().clear();
        } catch (Exception e) {
            e.printStackTrace();
            CreamToolkit.logMessage(e);
            try {
                connection.commit();
                connection.setAutoCommit(autoCommit);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static void selfbuyClasses() {
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            if (StringUtils.isEmpty(PARAM.getCashierNumber())
                    || ShiftReport.getCurrentShift(connection) == null) {
                PARAM.updateCashierNumber("99999990");
                PARAM.setCashierNumber("99999990");
                //TODO Refactoring to ZReport.isNotClosedYet()/ShiftReport.isNotClosedYet()
                Date initDate = CreamToolkit.getInitialDate();
                ZReport z = ZReport.getOrCreateCurrentZReport(connection);
                if (CreamToolkit.compareDate(z.getAccountingDate(), initDate) != 0) {
                    z = ZReport.createZReport(connection);
                }
                ShiftReport.createShiftReport(connection);
                // 如果sc,pos数据被清空,下面开班交易会获得－1的z序号,所以先把z save
                connection.commit();

                Transaction trans = app.getCurrentTransaction();
                trans.clear();
                trans.init();
                trans.setDealType2("8");
                trans.setInvoiceCount(0);
                trans.setInvoiceID("");
                trans.setInvoiceNumber("");
                trans.store(connection);

                String logMsg = CreamToolkit.GetResource().getString("CashierloginI") + " " + app.getCurrentCashierNumber() + " " + CreamToolkit.GetResource().getString("CashierloginII");
//                app.getMessageIndicator().setMessage(logMsg);
//                app.getWarningIndicator().setMessage("");
                CreamToolkit.logMessage(logMsg);
                connection.commit();
            }

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static void selfbuyShift() {
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        app.setEnableKeylock(false);
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();
            Date date1 = format.parse(format.format(new Date()));
            Date date2 = format.parse(ShiftReport.getMaxgnofdttm(connection));
            if(date1.after(date2)){

                final ShiftReport shift = ShiftReport.getCurrentShift(connection);
                //if (GetProperty.getShiftWithHoldTrn(null) != null) {
                if (!PARAM.getShiftWithHoldTrn().equalsIgnoreCase("allow")) {
                    TransactionHold.deleteByPosNO(connection, shift.getTerminalNumber());
                }

                //作交班
                app.getCurrentTransaction().clear();
                app.getCurrentTransaction().init();
                app.getCurrentTransaction().setDealType2("9");
//                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("shift"));
                app.getCurrentTransaction().setInvoiceCount(0);
                app.getCurrentTransaction().setInvoiceID("");
                app.getCurrentTransaction().setInvoiceNumber("");
                app.getCurrentTransaction().store(connection);
                CreamToolkit.logMessage("ShiftState> Signoff record created.");
                connection.commit();
                CreamToolkit.releaseConnection(connection);

                CreamToolkit.logMessage("ZState | entry | start ");

                // 作日结
//                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("z"));
                connection = CreamToolkit.getTransactionalConnection();
                Transaction currTrans = app.getCurrentTransaction();
                currTrans.clear();
                currTrans.init();
                currTrans.setDealType2("D");
                currTrans.setInvoiceCount(0);
                currTrans.setInvoiceID("");
                currTrans.setInvoiceNumber("");
                currTrans.store(connection);
                connection.commit();
            }

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static void selfbuyShift2() {
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        app.setEnableKeylock(false);
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();

                final ShiftReport shift = ShiftReport.getCurrentShift(connection);
                //if (GetProperty.getShiftWithHoldTrn(null) != null) {
                if (!PARAM.getShiftWithHoldTrn().equalsIgnoreCase("allow")) {
                    TransactionHold.deleteByPosNO(connection, shift.getTerminalNumber());
                }

                //作交班
                app.getCurrentTransaction().clear();
                app.getCurrentTransaction().init();
                app.getCurrentTransaction().setDealType2("9");
//                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("shift"));
                app.getCurrentTransaction().setInvoiceCount(0);
                app.getCurrentTransaction().setInvoiceID("");
                app.getCurrentTransaction().setInvoiceNumber("");
                app.getCurrentTransaction().store(connection);
                CreamToolkit.logMessage("ShiftState> Signoff record created.");
                connection.commit();
                CreamToolkit.releaseConnection(connection);

                CreamToolkit.logMessage("ZState | entry | start ");

                // 作日结
//                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("z"));
                connection = CreamToolkit.getTransactionalConnection();
                Transaction currTrans = app.getCurrentTransaction();
                currTrans.clear();
                currTrans.init();
                currTrans.setDealType2("D");
                currTrans.setInvoiceCount(0);
                currTrans.setInvoiceID("");
                currTrans.setInvoiceNumber("");
                currTrans.store(connection);
                connection.commit();

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

}