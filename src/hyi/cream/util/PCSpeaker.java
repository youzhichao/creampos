/*
 * Created on 2004-3-18
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package hyi.cream.util;

import hyi.cream.*;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.spos.JposException;
import hyi.spos.ToneIndicator;
//import jpos.*;
/**
 * @author Administrator
 *
 */
public class PCSpeaker {
	public final static int MESSAGE_BEEP = 1000;
	private static PCSpeaker pcSpeaker = null;
	
	public static PCSpeaker getInstance() {
		try {
			if (pcSpeaker == null) {
				pcSpeaker = new PCSpeaker();
			}
		} catch (Exception e) {
			e.printStackTrace(CreamToolkit.getLogger());
		}
		return pcSpeaker;
	}
	
	private void beep(int numberOfCycles, int interSoundWait) {
		POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
		//System.out.println("Waringing");
		try {
			ToneIndicator tone = posHome.getToneIndicator();
			if (!tone.getDeviceEnabled())
				tone.setDeviceEnabled(true);
			//int sound = 99999;
			tone.setAsyncMode(true);
			System.out.println("will sound ....");
			tone.sound(numberOfCycles, interSoundWait);
		} catch (Exception ne) {
			CreamToolkit.logMessage(ne);
		}
	}
	
	public void speeker(int type) {
		if (type == MESSAGE_BEEP) {
			beep(20, 1500);
			new Thread() {
				public void run(){
					try {
						Thread.sleep(20 * 1500 + 1000);
						POSPeripheralHome3.getInstance()
							.getToneIndicator().clearOutput();
					} catch (Exception e) {
					}
				}
			}.start();
		}
	}
}
