package hyi.cream.util;

import hyi.cream.dac.DacBase;
import hyi.cream.dac.Store;
import hyi.cream.groovydac.Param;
import hyi.cream.state.GetProperty;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Receipt generator.
 *
 * @author Bruce
 */
public class ReceiptGenerator {

    // singleton instance
    private static ReceiptGenerator receiptGenerator = new ReceiptGenerator();
//	private static ReceiptGenerator receiptGenerator;

    private boolean workable;

    private List tLines = new ArrayList(); // "T" line
    private List hLines = new ArrayList(); // "H" line
    private List dLines = new ArrayList(); // "D" line
    private List bLines = new ArrayList(); // "B" line
    private List pLines = new ArrayList(); // "P" line
    private List fLines = new ArrayList(); // "F" line
    private int lineItemPerPage = 6;
    private int linesPerPage = 30;
    private int linesBeforePayment = 18;
    private int linesPrinted;
    private int linesBeforePaymentPrinted;
    private int blankLinesBeforePayment;
    private int pageNumber = 1;
    private int lineItemsPrintedInThisPage; //已经打印了几笔单品
    private StringBuffer formFeedByBlankLines = new StringBuffer();
    private StringBuffer feedToPayment = new StringBuffer();

    private int firstColumnSpaceCount;

    /**
     * Constructor of ReceiptGenerator() will read ~/receipt.conf
     * and create "lines" collection.
     * <p>
     * Definition sample file:
     * <p>
     * #
     * # 1. 以 "$" 打头者为变量，目前的内定变量有：
     * #    $LINE_ITEM_PER_PAGE : 每页打印的line items数
     * #    $LINES_PER_PAGE : 每页打印的行数
     * #    $PAGE_NO : 页数
     * #    $FORM_FEED : 跳页符
     * #    $FORM_FEED_BY_BLANK_LINE : 跳页 by line-feed
     * #    $CUT_PAPER : 立即切纸
     * #    $CUT_PAPER_AT_MARK : 卷到黑点切纸
     * #    $STORE_ADDRESS : 门市地址
     * #    $STORE_TELEPHONE : 门市电话
     * #    $DATE : 当前系统日期与时间 (not implemented)
     * #    $FIRST_COLUMN_SPACE_COUNT :
     * # 2. 每行若以 "$" 打头，则为变量赋值。目前只有 LINE_ITEM_PER_PAGE 可赋值。
     * # 3. 以 "%" 打头者则为 property, 从property表中取值。(not implemented)
     * # 4. 每行若以 "D" 打头，则表示为LineItem段，其他则为Transaction段。
     * # 5. 打印顺序：T -> (H -> D -> B)* -> F
     * #                   ^----P----/
     * # 6. 每行前面若有用小括号括的东西，则会判断该条件是否成立，如果返回true，才打印此行。
     * # 7. 判断条件中可以比较大小（仅对于Integer和BigDecimal有效）例如（xxxGTaa）表示xxx > aa。
     * #    比较运算符  GT -- (大于)        GE -- (大于或等于)
     * #              LT -- (小于)        LE -- (小于或等于)
     * #              EQ -- (等于)        GL -- (不等于)
     * #
     * #                    2003-4-9 21:36 By Bruce You at Hongyuan Software(Shanghai)
     * $LINE_ITEM_PER_PAGE=10
     * T[%TopLogo1]
     * T[%TopLogo2]
     * T
     * H[SystemDateTime,yyyy/MM/dd HH:mm,L16]    序:[TransactionNumber,00000000,L8]    机:[TerminalNumber,##,L2]
     * H店:[StoreNumber,,L6]  收银员:[CashierNumber,,L7]  配达:[PeiDaNumber,,L15] 页:[$PAGE_NO,##,L2]
     * H
     * H---------------------------------------------------------------
     * D[PluNumber,,L13]     [PrintName,,L20]
     * D     @[UnitPrice,####0.00,R8] x [Quantity,###,R3]   [TotalPrice,#####0.00,R9]
     * B---------------------------------------------------------------
     * F(TotalMMAmountIsNotZero)小计:[GrossSalesAmount,#####0.00,R9]       组合促销:[TotalMMAmount,####0.00,R8]
     * F合计:[NetSalesAmount,#####0.00,R9]             [Pay1Name,,]:[PayAmount1,#####0.00,R9]
     * F(PayAmount2IsNotZero)                       [Pay2Name,,]:[PayAmount2,#####0.00,R9]
     * F(PayAmount3IsNotZero)                       [Pay3Name,,]:[PayAmount3,#####0.00,R9]
     * F(PayAmount4IsNotZero)                       [Pay4Name,,]:[PayAmount4,#####0.00,R9]
     * F(ChangeAmountIsNotZero)                        找零:[ChangeAmount,####0.00,R8]
     * F(SpillAmountIsNotZero)                         溢收:[SpillAmount,###0.00,R7]
     * F(SalesmanIsNotNull)售货员:[Salesman,,L10]
     * F(WeiXiuNumberIsNotNull)维修编号:[WeiXiuNumber,,L13]
     * F[%BottomLogo1]
     * F[%BottomLogo2]
     * F[$FORM_FEED]
     */
    private ReceiptGenerator() {
        //String confFile = GetProperty.getReceiptConfName("receipt.conf");
        File definitionFile = new File(CreamToolkit.getConfigDir() + "receipt.conf");
        if (!definitionFile.exists())
            return;

        setWorkable(true);
        try {
            BufferedReader reader =
                    new BufferedReader(
                            new InputStreamReader(new FileInputStream(definitionFile), GetProperty.getConfFileLocale()));
            String line;
            while ((line = reader.readLine()) != null) {
                switch (line.charAt(0)) {
                    case '#': // skip comment
                        break;
                    case '$':
                        if (line.startsWith("$LINE_ITEM_PER_PAGE")) {
                            try {
                                StringTokenizer s = new StringTokenizer(line, "=");
                                s.nextToken();
                                setLineItemPerPage(Integer.parseInt(s.nextToken()));
                            } catch (NumberFormatException e) {
                                CreamToolkit.logMessage("receipt file syntax error: " + line);
                            }
                        } else if (line.startsWith("$LINES_PER_PAGE")) {
                            try {
                                StringTokenizer s = new StringTokenizer(line, "=");
                                s.nextToken();
                                setLinesPerPage(Integer.parseInt(s.nextToken()));
                            } catch (NumberFormatException e) {
                                CreamToolkit.logMessage("receipt file syntax error: " + line);
                            }
                            // $LINES_BEFORE_PAYMENT 从第一行到打印Payment 信息的行数
                            // 更新日期 2003-06-05 By ZhaoHong                   
                        } else if (line.startsWith("$LINES_BEFORE_PAYMENT")) {
                            try {
                                StringTokenizer s = new StringTokenizer(line, "=");
                                s.nextToken();
                                setLinesBeforePayment(Integer.parseInt(s.nextToken()));
                            } catch (NumberFormatException e) {
                                CreamToolkit.logMessage("receipt file syntax error: " + line);
                            }
                        } else if (line.startsWith("$FIRST_COLUMN_SPACE_COUNT")) {
                            try {
                                StringTokenizer s = new StringTokenizer(line, "=");
                                s.nextToken();
                                setFirstColumnSpaceCount(Integer.parseInt(s.nextToken()));
                            } catch (NumberFormatException e) {
                                CreamToolkit.logMessage("receipt file syntax error: " + line);
                            }
                        }

                        break;
                    case 'T':
                        tLines.add(parseEscapeCharacters(line.substring(1)));
                        break;
                    case 'H':
                        hLines.add(parseEscapeCharacters(line.substring(1)));
                        break;
                    case 'D':
                        dLines.add(parseEscapeCharacters(line.substring(1)));
                        break;
                    case 'B':
                        bLines.add(parseEscapeCharacters(line.substring(1)));
                        break;
                    case 'P':
                        pLines.add(parseEscapeCharacters(line.substring(1)));
                        break;
                    case 'F':
                        fLines.add(parseEscapeCharacters(line.substring(1)));
                        break;
                }
            }
            reader.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (FileNotFoundException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    /**
     * Escape something like:
     *
     * \x1b\x21\x20\x1b\x61\x01NITORI\x1b\x21\x00\x1b\x61\x00
     *
     * 其中:
     * # \x1b\x21\x20: 設定雙倍寬度, \x1b\x21\x00: 正常寬度
     * # \x1b\x61\x01: 居中列印, \x1b\x61\x00: 靠左列印
     */
    private String parseEscapeCharacters(String input) {
        StringBuilder buffer = new StringBuilder(input.length());
        int lastPos = 0;
        int pos = 0;
        while ((pos = input.indexOf("\\x", pos)) != -1) {
            buffer.append(input.substring(lastPos, pos));
            buffer.append((char) Integer.parseInt(input.substring(pos + 2, pos + 4), 16));
            pos = pos + 4;
            lastPos = pos;
        }
        buffer.append(input.substring(lastPos));
        return buffer.toString();
    }

    public static ReceiptGenerator getInstance() {
        return receiptGenerator;
    }
//    public static ReceiptGenerator getInstance() {
//    	if (receiptGenerator == null){
//    		receiptGenerator = new ReceiptGenerator();
//    	}
//        return receiptGenerator;
//    }

    public boolean isWorkable() {
        return workable;
    }

    public void setWorkable(boolean workable) {
        this.workable = workable;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    private String checkCondition(String line, DacBase dac) {
        final int NullZeroEmpty = 0;
        final int GT = 1; //大于
        final int LT = 2; //小于
        final int EQ = 3; //等于
        final int GL = 4; //不等于（大于或小于）
        final int GE = 5; //大于等于
        final int LE = 6; //小于等于
        final int IsZero = 7;

        // Process conditional lines
        int condition = -1;

        if (line.charAt(0) == '(') {
            int e = line.indexOf(')');
            String cond = line.substring(1, e);
            String prop, base = "0";

            if (cond.endsWith("IsNotEmpty")) {
                prop = cond.substring(0, cond.indexOf("IsNotEmpty"));
                condition = NullZeroEmpty;
            } else if (cond.endsWith("IsNotZero")) {
                prop = cond.substring(0, cond.indexOf("IsNotZero"));
                condition = NullZeroEmpty;
            } else if (cond.endsWith("IsZero")) {
                prop = cond.substring(0, cond.indexOf("IsZero"));
                condition = IsZero;
            } else if (cond.endsWith("IsNotNull")) {
                prop = cond.substring(0, cond.indexOf("IsNotNull"));
                condition = NullZeroEmpty;
            } else if (cond.indexOf("GT") != -1) {
                prop = cond.substring(0, cond.indexOf("GT"));
                base = cond.substring(cond.indexOf("GT") + 2);
                condition = GT;
            } else if (cond.indexOf("LT") != -1) {
                prop = cond.substring(0, cond.indexOf("LT"));
                base = cond.substring(cond.indexOf("LT") + 2);
                condition = LT;
            } else if (cond.indexOf("EQ") != -1) {
                prop = cond.substring(0, cond.indexOf("EQ"));
                base = cond.substring(cond.indexOf("EQ") + 2);
                condition = EQ;
            } else if (cond.indexOf("GL") != -1) {
                prop = cond.substring(0, cond.indexOf("GL"));
                base = cond.substring(cond.indexOf("GL") + 2);
                condition = GL;
            } else if (cond.indexOf("GE") != -1) {
                prop = cond.substring(0, cond.indexOf("GE"));
                base = cond.substring(cond.indexOf("GE") + 2);
                condition = GE;
            } else if (cond.indexOf("LE") != -1) {
                prop = cond.substring(0, cond.indexOf("LE"));
                base = cond.substring(cond.indexOf("LE") + 2);
                condition = LE;
            } else
                return null;

            try {
                Object valueObject;
                Method method = dac.getClass().getDeclaredMethod("get" + prop, new Class[0]);
                valueObject = method.invoke(dac, new Object[0]);
                if (valueObject == null)
                    return null;

                switch (condition) {
                    case NullZeroEmpty:
                        if (valueObject.toString().trim().length() == 0
                                || valueObject.toString().trim().equals("0")
                                || valueObject.toString().trim().equals("0.0")
                                || valueObject.toString().trim().equals("0.00")
                                || valueObject.toString().trim().equals("-0")
                                || valueObject.toString().trim().equals("-0.0")
                                || valueObject.toString().trim().equals("-0.00"))
                            return null;

                        if (valueObject instanceof Map) {
                            if (((Map) valueObject).size() == 0)
                                return null;
                        }

                        break;
                    case GT:
                        //只支持Integer 和 BigDecimal比较 否则忽略条件
                        if (valueObject instanceof Integer) {
                            Integer ir = new Integer(base);
                            if (ir.compareTo((Integer) valueObject) >= 0)
                                return null;
                        } else if (valueObject instanceof HYIDouble) {
                            HYIDouble bd = new HYIDouble(base);
                            if (bd.compareTo(valueObject) >= 0)
                                return null;
                        }

                        break;
                    case LT:
                        //只支持Integer 和 BigDecimal比较 否则忽略条件
                        if (valueObject instanceof Integer) {
                            Integer ir = new Integer(base);
                            if (ir.compareTo((Integer) valueObject) <= 0)
                                return null;
                        } else if (valueObject instanceof HYIDouble) {
                            HYIDouble bd = new HYIDouble(base);
                            if (bd.compareTo(valueObject) <= 0)
                                return null;
                        }

                        break;
                    case EQ:
                        //只支持Integer 和 BigDecimal比较 否则忽略条件
                        if (valueObject instanceof Integer) {
                            Integer ir = new Integer(base);
                            if (ir.compareTo((Integer) valueObject) != 0)
                                return null;
                        } else if (valueObject instanceof HYIDouble) {
                            HYIDouble bd = new HYIDouble(base);
                            if (bd.compareTo(valueObject) != 0)
                                return null;
                        }

                        break;
                    case GL:
                        //只支持Integer 和 BigDecimal比较 否则忽略条件
                        if (valueObject instanceof Integer) {
                            Integer ir = new Integer(base);
                            if (ir.compareTo((Integer) valueObject) == 0)
                                return null;
                        } else if (valueObject instanceof HYIDouble) {
                            HYIDouble bd = new HYIDouble(base);
                            if (bd.compareTo(valueObject) == 0)
                                return null;
                        }

                        break;

                    case GE:
                        //只支持Integer 和 BigDecimal比较 否则忽略条件
                        if (valueObject instanceof Integer) {
                            Integer ir = new Integer(base);
                            if (ir.compareTo((Integer) valueObject) > 0)
                                return null;
                        } else if (valueObject instanceof HYIDouble) {
                            HYIDouble bd = new HYIDouble(base);
                            if (bd.compareTo(valueObject) > 0)
                                return null;
                        }

                        break;
                    case LE:
                        //只支持Integer 和 BigDecimal比较 否则忽略条件
                        if (valueObject instanceof Integer) {
                            Integer ir = new Integer(base);
                            if (ir.compareTo((Integer) valueObject) < 0)
                                return null;
                        } else if (valueObject instanceof HYIDouble) {
                            HYIDouble bd = new HYIDouble(base);
                            if (bd.compareTo(valueObject) < 0)
                                return null;
                        }

                        break;
                    case IsZero:
                        if (valueObject.toString().trim().length() == 0
                                || valueObject.toString().trim().equals("0")
                                || valueObject.toString().trim().equals("0.0")
                                || valueObject.toString().trim().equals("0.00")
                                || valueObject.toString().trim().equals("-0")
                                || valueObject.toString().trim().equals("-0.0")
                                || valueObject.toString().trim().equals("-0.00"))
                            ;
                        else
                            return null;
                    default:
                }

            } catch (Exception e1) {
                e1.printStackTrace(CreamToolkit.getLogger());
                return null;
            }

            line = line.substring(e + 1);
        }

        return line;
    }

    private Object fillValues(String line, DacBase dac) {
        if (line.length() == 0)
            return line;
        String retLine = "";
        if (line.charAt(0) == '(') {
            StringTokenizer tokener = new StringTokenizer(line, "(", true);
            boolean isFirst = true;
            boolean firstIsNull = false;
            while (tokener.hasMoreTokens()) {
                String subLine = tokener.nextToken().toString();
                subLine = "(" + tokener.nextToken().toString();
                Object tmp = fillValues2(subLine, dac);
                if (tmp != null) {
                    if (firstIsNull) {
                        for (int i = 0; i < this.firstColumnSpaceCount; i++)
                            retLine += " ";
                    }
                    retLine += tmp;
                } else {
                    if (isFirst)
                        firstIsNull = true;
                }
                isFirst = false;
            }
            if (retLine.trim().equals(""))
                retLine = null;
            return retLine;
        } else
            return fillValues2(line, dac);
    }

    private Object fillValues2(String line, DacBase dac) {
        if (line.length() == 0)
            return line;

        line = checkCondition(line, dac);
        if (line == null) {
            return line;
        }

        CreamFormatter formatter = CreamFormatter.getInstance();
        StringBuffer retLine = new StringBuffer();
        StringTokenizer tokener = new StringTokenizer(line, "[],", true);

        try {
            String text = tokener.nextToken();
            while (true) {
                if (!text.equals("[")) {
                    retLine.append(text);
                    text = tokener.nextToken();
                }
                if (!text.equals("["))
                    continue;
                String property = tokener.nextToken();
                tokener.nextToken(); // ','
                text = tokener.nextToken(); // maybe a ',' or a pattern
                String pattern = null;
                if (!text.equals(",")) {
                    pattern = text;
                    tokener.nextToken(); // ','
                }
                text = tokener.nextToken(); // maybe a ']' or a align&length
                String alignAndLength = null;
                if (!text.equals("]")) {
                    alignAndLength = text;
                    tokener.nextToken(); // ']'
                }

                try {
                    String formatedText = "";
                    if (property.equalsIgnoreCase("$PAGE_NO")) {
                        formatedText = this.getPageNumber() + "";
                    } else if (property.equalsIgnoreCase("$STORE_ADDRESS")) {
                        formatedText = Store.getAddress();
                    } else if (property.equalsIgnoreCase("$STORE_TELEPHONE")) {
                        if (Param.getInstance().getPrintAreaNumber()) {
                            formatedText = Store.getAreaNumber() + "-" + Store.getTelephoneNumber();
                        } else {
                            formatedText = Store.getTelephoneNumber();
                        }
                    } else if (property.equalsIgnoreCase("$FORM_FEED")) {
                        formatedText = "\u000C"; // form feed char
                    } else if (property.equalsIgnoreCase("$FORM_FEED_BY_BLANK_LINE")) {
                        // return by a StringBuffer object
                        return formFeedByBlankLines;
                    } else if (property.equalsIgnoreCase("$FEED_TO_PAYMENT")) { //zhaohong 2003-06-05
                        // return by a StringBuffer object
                        return feedToPayment;
                    } else if (property.equalsIgnoreCase("$CUT_PAPER_AT_MARK")) {
                        formatedText = "$CUT_PAPER_AT_MARK";
                    } else if (property.equalsIgnoreCase("$CUT_PAPER")) {
                        formatedText = "$CUT_PAPER";
                    } else {
                        Object valueObject;
                        Method method = dac.getClass().getDeclaredMethod("get" + property, new Class[0]);
                        valueObject = method.invoke(dac, new Object[0]);

                        if (valueObject == null) { //Bruce/20030505/ prevent a NullPointerException problem
                            formatedText = "";
                        } else if (pattern != null) {
                            if (valueObject instanceof Date || valueObject instanceof Timestamp) {
                                formatedText = formatter.format((Date) valueObject, pattern);
                            } else if (valueObject instanceof HYIDouble) {
                                formatedText =
                                        formatter.format(((HYIDouble) valueObject).doubleValue(), pattern);
                            } else if (valueObject instanceof Integer) {
                                formatedText = formatter.format(((Integer) valueObject).intValue(), pattern);
                            } else if (valueObject instanceof String) {
                                formatedText = (String) valueObject;
                            }
                        } else {
                            formatedText = valueObject.toString();
                        }
                    }

                    if (alignAndLength != null) {
                        // get direction : 'L' means left; 'R' means right; 'J' means left but not fill
                        String align = alignAndLength.substring(0, 1);

                        // get string's length
                        int needLen = 0;
                        int len = 0;
                        if (align.equalsIgnoreCase("J")) {
                            needLen = getPrintLength(formatedText);
                        } else {
                            needLen = Integer.parseInt(alignAndLength.substring(1));
                            len = needLen - getPrintLength(formatedText);
                        }
                        //Bruce/2003-12-05
                        // 因为灿坤的receipt.conf中交易日期给的长度不够，造成如果要trim length的话会把后面砍掉，
                        // 但是灿坤的receipt.conf格式文件种类众多，不想更新receipt.conf，所以在遇到日期pattern
                        // 的时候，就不要right trim了。 蛮tricky!
                        if (len < 0 && (pattern == null || pattern.indexOf("yyyy") == -1)) {
                            // right trim length
                            for (int i = len; i < 0; ) {
                                char c = formatedText.charAt(formatedText.length() - 1);
                                if ((int) c > 255) { // Chinese char
                                    i += 2;
                                } else {
                                    i++;
                                }
                                formatedText = formatedText.substring(0, formatedText.length() - 1);
                            }
                            if (getPrintLength(formatedText) < needLen)
                                formatedText += " ";
                        } else {
                            // do for string's length
                            for (int i = 0; i < len; i++) {
                                if (align.equalsIgnoreCase("R")) {
                                    formatedText = " " + formatedText;

                                } else {
                                    formatedText = formatedText + " ";
                                }
                            }
                        }
                    }
                    retLine.append(formatedText);

                } catch (Exception e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }

                text = tokener.nextToken();
            }
        } catch (NoSuchElementException e) {
            //e.printStackTrace();
        }

        return retLine.toString();
    }

    static private int getPrintLength(String data) {
        int len = 0;
        for (int i = 0; i < data.length(); i++) {
            char c = data.charAt(i);
            if ((int) c > 255) // Chinese char
                len += 2;
            else
                len++;
        }
        return len;
    }

    public Collection generateLines(char tag, DacBase dac) {
        Collection retLines = new ArrayList();

        Iterator iter;
        switch (tag) {
            case 'T':
                iter = tLines.iterator();
                break;
            case 'H':
                iter = hLines.iterator();
                break;
            case 'D':
                iter = dLines.iterator();
                break;
            case 'B':
                iter = bLines.iterator();
                break;
            case 'P':
                iter = pLines.iterator();
                break;
            case 'F':
                iter = fLines.iterator();
                break;
            default:
                return retLines; // impossible
        }
        while (iter.hasNext()) {
            String line = (String) iter.next();
            Object obj = fillValues(line, dac);
            if (obj != null)
                retLines.add(obj);
        }
        return retLines;
    }

    public int getLineItemPerPage() {
        return lineItemPerPage;
    }

    public void setLineItemPerPage(int lineItemPerPage) {
        this.lineItemPerPage = lineItemPerPage;
    }

    public int getLinesPerPage() {
        return linesPerPage;
    }

    public void setLinesPerPage(int linesPerPage) {
        this.linesPerPage = linesPerPage;
    }

    /**
     * 从第一行到打印Payment 信息的行数
     */
    public int getLinesBeforePayment() {
        return linesBeforePayment;
    }

    public void setLinesBeforePayment(int linesBeforePayment) {
        this.linesBeforePayment = linesBeforePayment;
    }

    public void setFirstColumnSpaceCount(int firstColumnSpaceCount) {
        this.firstColumnSpaceCount = firstColumnSpaceCount;
    }

    public int getLinesPrinted() {
        return linesPrinted;
    }

    public void setLinesPrinted(int linesPrinted) {
        this.linesPrinted = linesPrinted;

        // 将formFeedByBlankLines设置成n个回车，其中
        // n=(每页行数)-(此页已打印行数)-1，也就是换页需要打印的空白行数
        formFeedByBlankLines.setLength(0);
        int blankLines = getLinesPerPage() - getLinesPrinted() - 1;
        for (int i = 0; i < blankLines; i++)
            formFeedByBlankLines.append('\n');
    }

    public int getLinesBeforePaymentPrinted() {
        return linesBeforePaymentPrinted;
    }

    public void setLinesBeforPaymentPrinted(int linesBeforePaymentPrinted) {
        this.linesBeforePaymentPrinted = linesBeforePaymentPrinted;
        feedToPayment.setLength(0);
        int blankLines = getLinesBeforePayment() - getLinesBeforePaymentPrinted() - 1;
        for (int i = 0; i < blankLines; i++)
            feedToPayment.append('\n');
        this.blankLinesBeforePayment = blankLines;
    }

    public int getBlankLinesBeforePayment() {
        return blankLinesBeforePayment;
    }

    public int getLineItemsPrintedInThisPage() {
        return lineItemsPrintedInThisPage;
    }

    public void setLineItemsPrintedInThisPage(int lineItemsPrintedInThisPage) {
        this.lineItemsPrintedInThisPage = lineItemsPrintedInThisPage;
    }

//    static public void main(String[] arg) {
//        String formatedText = "中文中文中文12345678";
//        System.out.println("formatedTextlen=" + getPrintLength(formatedText));
//        int needLen = 17;
//        int len = needLen - getPrintLength(formatedText);
//        for (int i = len; i < 0;) {
//            char c = formatedText.charAt(formatedText.length() - 1);
//            System.out.println("c=" + (int)c);
//            if ((int)c > 255) { // Chinese char
//                System.out.println("> 255, c=" + (int)c);
//                i += 2;
//            } else {
//                i++;
//            }
//            System.out.println("i=" +i);
//            formatedText = formatedText.substring(0, formatedText.length() - 1);
//        }
//        if (getPrintLength(formatedText) < needLen)
//            formatedText += " ";
//        System.out.println(formatedText);
//    }

}
