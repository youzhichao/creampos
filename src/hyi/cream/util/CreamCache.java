package hyi.cream.util;

import hyi.cream.dac.PLU;
import hyi.cream.state.GetProperty;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

public class CreamCache {
    public final static String BY_ITEMNUMBER = "itemNumber";

    public final static String BY_BARCODE = "barCode";
    public final static String FILE_PLU_ODB = "plu.odb";
    public final static String FILE_PLUNO_INDEX = "plu_pluno.idx";
    public final static String FILE_ITEMNO_INDEX = "plu_itemno.idx";

    private static CreamCache cache;

    private Map pluCacheByItemNumberMap;

    private Map pluCacheByBarCodeMap;
    
    private HYIDouble compare0 = new HYIDouble(0);

    private static SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");

    private static SimpleDateFormat yyyyMMddHHmmsss = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:sss");

    private static SimpleDateFormat yyyyMMddHHmmssSSS = new SimpleDateFormat(
    "yyyy-MM-dd HH:mm:ss.SSS");
    
    private static SimpleDateFormat yyyyMMdd = new SimpleDateFormat(
            "yyyy-MM-dd");

    private static SimpleDateFormat yyyyMMdd2 = new SimpleDateFormat("yyyyMMdd");

    private static SimpleDateFormat yyyyMMddHHmm = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm");

    private static SimpleDateFormat yyyyMMddHHmm2 = new SimpleDateFormat(
            "yyyyMMddHHmm");

    private static SimpleDateFormat HHmmss = new SimpleDateFormat("HH:mm:ss");

    private boolean usePLUCache = true;

    private static class IndexEntry implements Comparable<IndexEntry>, Serializable {
        private static final long serialVersionUID = 1L;

        String number;
        int position;

        public IndexEntry(String number, int position) {
            this.number = number;
            this.position = position;
        }

        public int compareTo(IndexEntry another) {
            return number.compareTo(another.number);
        }
    }

    private List<IndexEntry> plunoIndex;
    private List<IndexEntry> itemnoIndex;

    private CreamCache() {
        pluCacheByItemNumberMap = new HashMap();
        pluCacheByBarCodeMap = new HashMap();
    }

    public static CreamCache getInstance() {
        if (cache == null)
            cache = new CreamCache();
        return cache;
    }

    public void clearCache() {
        if (pluCacheByItemNumberMap.size() > 300) {
            pluCacheByItemNumberMap.clear();
            pluCacheByBarCodeMap.clear();
        }
    }

    public PLU getPLUByItemNumberOld(String itemNumber) {
        if (pluCacheByItemNumberMap.containsKey(itemNumber))
            return (PLU) pluCacheByItemNumberMap.get(itemNumber);
        return null;
    }

    public PLU getPLUByItemNumber(String itemNumber) {
        try {
            if (itemnoIndex == null)
                loadPLUObjectDBIndex();

            int point = Collections.binarySearch(itemnoIndex, new IndexEntry(itemNumber, -1));
            if (point >= 0 && itemnoIndex.get(point).number.equals(itemNumber)) {
                int position = itemnoIndex.get(point).position;
                RandomAccessFile pludb = new RandomAccessFile(FILE_PLU_ODB, "r");
                pludb.seek(position);
                int objectSize = pludb.read() | (pludb.read() << 8);
                byte[] bytes = new byte[objectSize];
                pludb.readFully(bytes);
                pludb.close();

                FastByteArrayInputStream fbos = new FastByteArrayInputStream(bytes, objectSize);
                ObjectInputStream objectIn = new ObjectInputStream(fbos);
                PLU plu = (PLU)objectIn.readObject();
                objectIn.close();
                return plu;
            }
            else
                return null;
        }
        catch (Exception e)
        {
            CreamToolkit.logMessage(e);
            return null;
        }
    }
    
    public PLU getPLUByBarCodeOld(String barCode) {
        if (pluCacheByBarCodeMap.containsKey(barCode))
            return (PLU) pluCacheByBarCodeMap.get(barCode);
        return null;
    }

    public PLU getPLUByBarCode(String barCode) {
        try {
            if (plunoIndex == null)
                loadPLUObjectDBIndex();

            int point = Collections.binarySearch(plunoIndex, new IndexEntry(barCode, -1));
            if (point >= 0 && plunoIndex.get(point).number.equals(barCode)) {
                int position = plunoIndex.get(point).position;
                RandomAccessFile pludb = new RandomAccessFile(FILE_PLU_ODB, "r");
                pludb.seek(position);
                int objectSize = pludb.read() | (pludb.read() << 8);
                byte[] bytes = new byte[objectSize];
                pludb.readFully(bytes);
                pludb.close();

                FastByteArrayInputStream fbos = new FastByteArrayInputStream(bytes, objectSize);
                ObjectInputStream objectIn = new ObjectInputStream(fbos);
                PLU plu = (PLU)objectIn.readObject();
                objectIn.close();
                return plu;
            }
            else
                return null;
        }
        catch (Exception e)
        {
            CreamToolkit.logMessage(e);
            return null;
        }
    }
    
    public PLU addPLU(String type, String key, PLU plu) {
//        if (plu == null)
//            return plu;
//
//        if (type.equals(BY_ITEMNUMBER)) {
//            pluCacheByItemNumberMap.put(key, plu);
//            pluCacheByBarCodeMap.put(plu.getPluNumber(), plu);
//        } else if (type.equals(BY_BARCODE)) {
//            pluCacheByBarCodeMap.put(key, plu);
//            pluCacheByItemNumberMap.put(plu.getItemNumber(), plu);
//        }
        return plu;
    }

    public HYIDouble getHYIDouble0() {
        return compare0;
    }

    // 是否采用快的促销方式
    public boolean isMMQuick() {
        return GetProperty.getMixAndMatchQuick().equalsIgnoreCase("yes");
    }

    /**
     * yyyy-MM-dd HH:mm:ss
     * 
     * @return
     */
    public SimpleDateFormat getDateTimeFormate() {
        return yyyyMMddHHmmss;
    }

    /**
     * yyyyMMddHHmm
     * 
     * @return
     */
    public SimpleDateFormat getDateTimeFormate4() {
        return yyyyMMddHHmm2;
    }

    /**
     * yyyyMMddHHmmss.SSS
     * 
     * @return
     */
    public SimpleDateFormat getDateTimeFormate5() {
    	return yyyyMMddHHmmssSSS;
    }
    
    /**
     * yyyy-MM-dd HH:mm:sss
     * 
     * @return
     */
    public SimpleDateFormat getDateTimeFormate3() {
        return yyyyMMddHHmmsss;
    }

    /**
     * yyyy-MM-dd HH:mm
     * 
     * @return
     */
    public SimpleDateFormat getDateTimeFormate2() {
        return yyyyMMddHHmm;
    }

    /**
     * yyyy-MM-dd
     * 
     * @return
     */
    public SimpleDateFormat getDateFormate() {
        return yyyyMMdd;
    }

    /**
     * yyyyMMdd
     * 
     * @return
     */
    public SimpleDateFormat getDateFormate2() {
        return yyyyMMdd2;
    }

    /**
     * HH:mm:ss
     * 
     * @return
     */
    public SimpleDateFormat getTimeFormate() {
        return HHmmss;
    }

    public SimpleDateFormat getSpecialTimeFormate() {
        return HHmmss;
    }
    
    public void setUsePLUCache(boolean flag) {
        usePLUCache = flag;
    }

    public boolean isUsePLUCache() {
        return usePLUCache;
    }

    public void loadPLUObjectDBIndex() {
        try {
            InputStream plu_pluno = new BufferedInputStream(new FileInputStream(FILE_PLUNO_INDEX));
            ObjectInputStream objectIn = new ObjectInputStream(plu_pluno);
            plunoIndex = (List<IndexEntry>)objectIn.readObject();

            FileInputStream plu_itemno = new FileInputStream(FILE_ITEMNO_INDEX);
            objectIn = new ObjectInputStream(plu_itemno);
            itemnoIndex = (List<IndexEntry>)objectIn.readObject();
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        }
    }

    public void clearPLUObjectDB() {
        try {
            new File(FILE_PLU_ODB).delete();
            new File(FILE_PLUNO_INDEX).delete();
            new File(FILE_ITEMNO_INDEX).delete();
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        }
    }

    public void rebuildPLUObjectDB() {
        try {
            new File(FILE_PLU_ODB).delete();
            new File(FILE_PLUNO_INDEX).delete();
            new File(FILE_ITEMNO_INDEX).delete();
            buildPLUObjectDB();
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        }
    }

    public void buildPLUObjectDB() {
        long time = System.currentTimeMillis();
        DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            File pluOdb = new File(FILE_PLU_ODB);
            File pluPlunoIdx = new File(FILE_PLUNO_INDEX);
            File pluItemnoIdx = new File(FILE_ITEMNO_INDEX);
            if (pluOdb.exists() && pluPlunoIdx.exists() && pluItemnoIdx.exists()) {
                loadPLUObjectDBIndex();
                return;
            }

            int position = 0;
            int count = 0;
            FileOutputStream pludb = new FileOutputStream(pluOdb);
            plunoIndex = new ArrayList<IndexEntry>();
            itemnoIndex = new ArrayList<IndexEntry>();
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM plu");
            while (resultSet.next()) {
                PLU plu = new PLU();
                plu.constructPojoFromResultSet(resultSet);
                FastByteArrayOutputStream fbos = new FastByteArrayOutputStream();
                //GZIPOutputStream gzos = new GZIPOutputStream(fbos); // will be slower
                ObjectOutputStream objectOut = new ObjectOutputStream(fbos);
                objectOut.writeObject(plu);
                objectOut.flush();
                objectOut.close();
                int pluObjectSize = fbos.getSize();
                byte[] pluObjectBytes = fbos.getByteArray();
                pludb.write(pluObjectSize & 0xFF);
                pludb.write((pluObjectSize & 0xFF00) >> 8);
                pludb.write(pluObjectBytes, 0, pluObjectSize);
                if (count++ % 500 == 0)
                    System.out.println("buildPLUObjectDB()> count=" + count
                        + ",size=" + pluObjectSize);
                plunoIndex.add(new IndexEntry(plu.getPluNumber(), position));
                itemnoIndex.add(new IndexEntry(plu.getItemNumber(), position));
                position += 2 + pluObjectSize;
            }
            pludb.flush();
            pludb.close();

            FileOutputStream plu_pluno = new FileOutputStream(pluPlunoIdx);
            ObjectOutputStream objectOut = new ObjectOutputStream(plu_pluno);
            Collections.sort(plunoIndex);
            objectOut.writeObject(plunoIndex);
            objectOut.flush();
            objectOut.close();
            
            FileOutputStream plu_itemno = new FileOutputStream(pluItemnoIdx);
            objectOut = new ObjectOutputStream(plu_itemno);
            Collections.sort(itemnoIndex);
            objectOut.writeObject(itemnoIndex);
            objectOut.flush();
            objectOut.close();
        } catch (IOException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
            System.out.println("CreamCache.buildPLUObjectDB(): " 
                + (System.currentTimeMillis() - time) + "ms");
        }
    }

//    public static void main(String[] args) {
//        List mixAndMatchIDArray = new ArrayList();
//        mixAndMatchIDArray.add("301");
//        mixAndMatchIDArray.add("401");
//        mixAndMatchIDArray.add("201");
//        Collections.sort(mixAndMatchIDArray, new Comparator() {
//            public int compare(Object o1, Object o2) {
//                String s1 = (String) o1;
//                String s2 = (String) o2;
//                return -s1.compareTo(s2); // reverse order
//            }
//
//            public boolean equals(Object obj) {
//                return ((String) obj).equals(this);
//            }
//        });
//        System.out.println("----- " + mixAndMatchIDArray);
//    }
}
