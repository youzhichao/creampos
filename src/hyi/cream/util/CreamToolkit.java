package hyi.cream.util;

import hyi.cream.*;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.event.SystemInfoEvent;
import hyi.cream.inline.Server;
import hyi.cream.groovydac.Param;
import hyi.cream.groovydac.SsmpLog;
import hyi.cream.dac.*;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.state.*;
import hyi.cream.uibeans.ItemList;
import hyi.cream.uibeans.PayingPane;
import hyi.cream.uibeans.ScreenBanner;
import hyi.cream.uibeans.touch.TouchPane;
import hyi.spos.JposException;
import hyi.spos.KeylockConst;
import hyi.spos.LineDisplay;

import java.awt.*;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.*;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * CreamToolkit class.
 * 
 * @author Dai, Slackware, Bruce
 * @version 1.6
 */
public class CreamToolkit {

    /**
     * Meyer/1.6/2003-01-22 增加2组get,set反射方法： getAmountMethod,setAmountMethod
     * getCountMethod,setCountMethod
     * 
     * /Bruce/1.6/2002-04-26/ Fix a problem when property
     * "DatabaseConnectionIncrement" does not exist.
     * 
     * /Bruce/1.5/2002-04-01/ Fix a problem when conn.setAutoCommit(true) is
     * failed. (On inline server side)
     */
    transient public static final String VERSION = "1.6";

    private static Image image;

    /** Database connection property file name. */
    private static final String DB_CONNECTION_CONF_FILE = "dbcp.conf";
    private static final String INLINEDB_CONNECTION_CONF_FILE = "dbcp.inline.conf";

    public static final int LOG_NONE = -1;
    public static final int LOG_NORMAL = 0;
    public static final int LOG_VERBOSE = 1;

    private static PrintWriter logger;
    private static PrintWriter eventLogger;
    private static PrintWriter printerRecorder;
    private static int logLevel = LOG_NORMAL;
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private static DateFormat yyyyMMdd = CreamCache.getInstance().getDateFormate();
    static Properties dbProp = new Properties();

    private static boolean databaseMySQL;
    private static boolean databasePostgreSQL;
    private static String databaseName;
    private static String databaseHost;

    static {
        openLogger();
        setupDatabaseConnectionPool();
    }

    private static void setupDatabaseConnectionPool() {
        try {
            String dbcpFile = "";
            if (hyi.cream.inline.Server.serverExist()) {
                dbcpFile = INLINEDB_CONNECTION_CONF_FILE;
            } else {
                dbcpFile = DB_CONNECTION_CONF_FILE;
            }
            File dbcpConfigFile = new File(dbcpFile);
            if (!dbcpConfigFile.exists())
                throw new IllegalArgumentException("Cannot find: " + dbcpFile);

            Properties allProperties = new Properties();
            FileInputStream inputStream = new FileInputStream(dbcpConfigFile);
            allProperties.load(inputStream);
            inputStream.close();

            // Try loading JDBC driver
            String jdbcClassName = allProperties.getProperty("dbcp.driverClassName");
            databaseMySQL = jdbcClassName.contains("mysql");
            databasePostgreSQL = jdbcClassName.contains("postgres"); 
            Class.forName(jdbcClassName);

            // Get database name
            String databaseURL = allProperties.getProperty("dbcp.url");
            int lastSlash = databaseURL.lastIndexOf("/");
            int firstSlashSlash = databaseURL.indexOf("//");
            databaseName = databaseURL.substring(lastSlash + 1);
            databaseHost = databaseURL.substring(firstSlashSlash + 2, lastSlash);
            // not yet consider ':port'
            
            Properties dbcpProperties = new Properties();
            Iterator iter = allProperties.keySet().iterator();
            while (iter.hasNext()) {
                String key = (String)iter.next();
                if (key.startsWith("dbcp.")) {
                    dbcpProperties.put(key.substring(key.indexOf(".") + 1), allProperties
                        .getProperty(key));
                }
            }
            DBPoolManager.init(dbcpProperties);
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    public static String getDatabaseName() {
        return databaseName;
    }

    public static String getDatabaseHost() {
        return databaseHost;
    }

    public static Connection getDatabaseConnectionByStoreNo(String storeNo) {
        try {
            String ipAddress = Reason.queryStoreIPAddress(storeNo);
            return DriverManager.getConnection(
                "dbcp.url=jdbc:postgresql://" + ipAddress + "/cream", "postgres", "");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    //private static void determineLineDisplayType() {
    //    POSPeripheralHome3 posPeripheralHome = POSPeripheralHome3.getInstance();
    //    if (posPeripheralHome != null && posPeripheralHome.getLineDisplayLogicalName() != null) {
    //        //ibm4840LineDisplay = posPeripheralHome.getLineDisplayLogicalName().equals("IBM4840LineDisplay");
    //        cd7110LineDisplay = posPeripheralHome.getLineDisplayLogicalName().equals("CD7110");
    //    }
    //}

    /**
     * Get OS name string
     * 
     * @return Linux, Windows or Unknown;
     */
    public static String getOsName() {
        String osName = System.getProperties().getProperty("os.name");
        if (osName.indexOf("Linux") != -1)
            return "Linux";
        else if (osName.indexOf("Windows") != -1)
            return "Windows";
        else if (osName.indexOf("Mac") != -1)
            return "MacOS";
        else
            return "Unknown";
    }

    /**
     * Get image by a name.
     */
    public static Image getImage(String s) {
        image = Toolkit.getDefaultToolkit().getImage(getConfigDir() + s);
        return image;
    }

    public static Image getNextImage(String s) {
        image = Toolkit.getDefaultToolkit().getImage(getConfigDir() + "2" + s);
        return image;
    }

    /**
     * 取得configuration file(*.conf)所在目录。
     * 
     * @return String Configuration file directory.
     */
    public static String getConfigDir() {
        String configDir = Param.getInstance().getConfigDir();
        if (!configDir.endsWith(File.separator))
            configDir += File.separator;
        return configDir;
    }

    /**
     * Get a configuration "File" by a String.
     */
    public static File getConfigurationFile(String c) {
        String baseDir = getConfigDir();
        String fileName = "";
        if (c.equals("connectMsgBanner"))
            fileName = "connmsg";
        else if (c.equals("pageMsgBanner"))
            fileName = "pagemsg";
        else if (c.equals("productInfoBanner"))
            fileName = "productinfo";
        else if (c.equals("totalAmountBanner"))
            fileName = "totalmsg";
        else if (c.equals("screenBanner2"))
            fileName = "screenbanner2";
        else if (c.equals("payingPane1"))
            fileName = "payingpane1";
        else if (c.equals("payingPane2"))
            fileName = "payingpane2";
        else if (c.equals("agelevel"))
            fileName = "agelevel";
        /*
        else if (c.equals("timeBanner"))
            fileName = "custtimebanner";
        else if (c.equals("custPayingPane"))
            fileName = "custpayingpane";
        else if (c.equals("custProductInfo"))
            fileName = "custproductinfo";
        else if (c.equals("custAdvertWord"))
            fileName = "custadvertword";
        else if (c.equals("custWelcome"))
            fileName = "custwelcome";
        */
        else if (c.equals("dynaPayingPane"))
            fileName = "dynapayingpane";
        else if (c.startsWith("POSPeripheralHome")) {
            fileName = "jpos";
            File f = getLocaleFile(baseDir, fileName, ".xml");
            if (f == null)
                f = getLocaleFile(baseDir + ".." + File.pathSeparator, fileName, ".xml");
            return f;
        }

        return getLocaleFile(baseDir, fileName, ".conf");
    }

    /**
     * Get a configuration "File" by a "Class" object.
     */
    public static File getConfigurationFile(Class c) {
        String baseDir = getConfigDir();
        String fileName = "";
        if (c.equals(ItemList.class))
            fileName = "itemlist";
        else if (c.equals(PayingPane.class))
            fileName = "payingpane";
        else if (c.equals(ScreenBanner.class))
            fileName = "screenbanner";
        else if (c.equals(POSButtonHome2.class)) {
            // 2005.11.09 不同pos机读不同配置文件，有利于版本更新
            // KeyboardType不存在，就读默认的posbutton3.conf
            fileName = "posbutton3" + Param.getInstance().getKeyboardType();
            File buttonConf = getLocaleFile(baseDir, fileName, ".conf");
            if (buttonConf != null && buttonConf.exists())
                return buttonConf;
            fileName = "posbutton3";
            return getLocaleFile(baseDir, fileName, ".conf");
        } else if (c.equals(POSPeripheralHome3.class) || c.equals(POSPeripheralHome3.class))
            fileName = "posdevices3";
        else if (c.equals(StateMachine.class))
            fileName = "statechart3";
        else if (c.equals(ShiftReport.class))
            fileName = "shiftreport";
        else if (c.equals(ZReport.class))
            fileName = "zreport";
        else if (c.equals(KeyLock1State.class))
            fileName = "keylock1";
        else if (c.equals(KeyLock2State.class))
            fileName = "keylock2";
        else if (c.equals(KeyLock3State.class))
            fileName = "keylock3";
        else if (c.equals(ConfigState.class))
            fileName = "config";
        else if (c.equals(CashForm.class))
            fileName = "cashform";
        else if (c.equals(Integer.class))
            fileName = "agelevel";
        else if (GetProperty.isEnableTouchPanel() && c.equals(TouchPane.class))
            fileName = "touchallowbutton";

        return getLocaleFile(baseDir, fileName, ".conf");
    }
    
    public static File getLocaleFile(String baseDir, String fileName, String postfix) {
        return new File(baseDir + fileName + postfix);
        /*String locale = Param.getInstance().getLocale();
        File file = new File(baseDir + fileName + "_" + locale + postfix);
        if (!file.exists()) {
            file = new File(baseDir + fileName + postfix);
            if (!file.exists())
                return null;
        }
        return file;*/
    }

    public static ResourceBundle GetResource() {
        String locale;
        if (Server.isAtServerSide())
            locale = GetProperty.getLocale();
        else
            locale = Param.getInstance().getLocale();
        if (locale.equals("en_US"))
            return ResourceBundle.getBundle("hyi.cream.util.CreamResource_en_US");
        else
            return ResourceBundle.getBundle("hyi.cream.util.CreamResource");
    }

    public static java.util.Date getInitialDate() {
        java.util.Date initDate = new java.util.Date(0);
        int timeOffset = 0;
        TimeZone timeZone = Calendar.getInstance().getTimeZone();
        timeOffset = timeOffset - timeZone.getRawOffset();
        initDate.setTime(timeOffset);
        return initDate;
    }

    public static void openLogger() {
        try {
            logger = new PrintWriter(new OutputStreamWriter(new FileOutputStream(GetProperty
                .getLogFile(), true), "UTF-8"));
            eventLogger = new PrintWriter(new OutputStreamWriter(new FileOutputStream(
                "events.log", true), "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static PrintWriter getLogger() {
        try {
            if (logger == null) {
                logger = new PrintWriter(new OutputStreamWriter(
                    new FileOutputStream(GetProperty.getLogFile(), true),
                    "UTF-8"));
            }
            return logger;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PrintWriter getEventLogger() {
        try {
            if (eventLogger == null) {
                eventLogger = new PrintWriter(new OutputStreamWriter(
                    new FileOutputStream("events.log", true),
                    "UTF-8"));
            }
            return eventLogger;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void recordPrint(String line) {
        java.util.Date currentTime = new java.util.Date();
        getPrinterRecorder().println("[" + dateFormatter.format(currentTime) + "] " + line);
        getPrinterRecorder().flush();
    }

    private static PrintWriter getPrinterRecorder() {
        try {
            if (printerRecorder == null) {
                printerRecorder = new PrintWriter(new OutputStreamWriter(
                    new FileOutputStream(GetProperty.getPrinterRecordFile(), true),
                    "UTF-8"));
            }
            return printerRecorder;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static int getLogLevel() {
        return logLevel;
    }

    public static void setLogLevel(int level) {
        logLevel = level;
    }

    /**
     * Log exception message.
     *
     * @since Bruce, 20070314 
     */
    public static void logMessage(Throwable e) {
        if (e.getMessage() != null)
            logMessage(LOG_NORMAL, e.getMessage());
        e.printStackTrace(getLogger());
        e.printStackTrace();
    }

    public static void logMessage(String msg) {
        logMessage(LOG_NORMAL, msg);
    }

    public static void logVerboseMessage(String msg) {
        logMessage(LOG_VERBOSE, msg);
    }

    public static void logMessage(int logLevel, String msg) {
        if (logLevel == LOG_VERBOSE || logLevel == LOG_NORMAL) {
            java.util.Date currentTime = new java.util.Date();
            String dateString = "[" + dateFormatter.format(currentTime) + "] ";
            if (logger != null) {
                logger.print(dateString);
                logger.println(msg);
                logger.flush();
            }
            //Bruce/20070314/ Also output to standard out
            System.out.print(dateString);
            System.out.println(msg);
        } else {
            if (logger != null) {
                logger.println(msg);
                logger.flush();
            }
        }
    }

    public static void logEvent(String msg) {
        if (eventLogger != null) {
            eventLogger.println(msg);
            eventLogger.flush();
        }
    }

    public static void closeLog() {
        if (logger != null) {
            logger.close();
            logger = null;
        }
        if (eventLogger != null) {
            eventLogger.close();
            eventLogger = null;
        }
    }

    /**
     * Write transaction log into directory "tranlog/[yyyy-MM-dd].sql" under
     * current working directory.
     * 
     * @param insertStatement
     *            INSERT statement string.
     */
    public static void writeTransactionLog(String insertStatement) {
        File tranLogDir = new File("tranlog");
        if (!tranLogDir.exists()) {
            if (!tranLogDir.mkdir()) {
                logMessage("Err> Create tranlog directory failed.");
                return;
            }
        }

        try {
            Writer tranLog = new FileWriter(tranLogDir + File.separator
                    + yyyyMMdd.format(new Date()) + ".sql", true);
            tranLog.write(insertStatement);
            if (!insertStatement.endsWith(";")
                    && (insertStatement.startsWith("INSERT")
                            || insertStatement.startsWith("insert")
                            || insertStatement.startsWith("DELETE")
                            || insertStatement.startsWith("delete")
                            || insertStatement.startsWith("UPDATE") || insertStatement
                            .startsWith("update")))
                tranLog.write(';');
            tranLog.write('\n');
            tranLog.close();
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    /**
     * Get non-auto-committed transactional DB connection. You must commit() yourself
     * to save changes.
     * 如果DbConnection.isNeedClose() is true, 則caller有責任在用完後把他close掉。
     */
    public static DbConnection getTransactionalConnection() throws SQLException {
        return getDbConnection(false);
    }

    /**
     * Get auto-committed DB connection.
     * 如果DbConnection.isNeedClose() is true, 則caller有責任在用完後把他close掉。
     */
    public static DbConnection getPooledConnection() throws SQLException {
        return getDbConnection(true);
    }

    /**
     * Get auto-committed DB connection.
     * 如果DbConnection.isNeedClose() is true, 則caller有責任在用完後把他close掉。
     */
    public static DbConnection getNonTransactionalConnection() throws SQLException {
        return getDbConnection(true);
    }

    /**
     * Get DB connection.
     * <p/>
     * 同一個scope中只有發起人才有權決定是否auto-commit，中間人不能修改；也就是說，如果發起人
     * 是用getPooledConnection()拿到opening connection，在該connection還沒關閉的同一個
     * scope中，即使你是用getTransactionalConnection()拿到的connection，它仍然是auto-commit.
     */
    private static DbConnection getDbConnection(boolean autoCommit) throws SQLException {
        Connection conn = DbConnection.getOpeningConnection();

        // 如果thread-local沒有一個opening conection, 那麼caller有責任在用完後把他close掉。
        boolean needClose = (conn == null);

        if (conn == null) {
            conn = DBPoolManager.getConnection();

            // see method comment
            conn.setAutoCommit(autoCommit);
        }
        return new DbConnection(conn, needClose);
    }

    public static void releaseConnection(DbConnection connection) {
        try {
            if (connection != null)
                connection.closeIfNeeds();
        } catch (SQLException e) {
            logMessage(e);
        }
        return;
    }

    public static boolean checkInput(String number, int i) {
        try {
            Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean checkInput(String number, HYIDouble max) {
        try {
            HYIDouble value = new HYIDouble(number);
            if (max != null && !max.equals(HYIDouble.zero()) && value.compareTo(max) > 0)
                return false;
            else
                return true;
        } catch (NumberFormatException e) {
            CreamToolkit.logMessage("" + e);
            return false;
        } catch (StringIndexOutOfBoundsException e) {
            CreamToolkit.logMessage("" + e);
            return false;
        }
    }

    /**
     * 将交易显示在相关资料显示在客显上
     */
    public static void showText(Transaction transaction, int mode) {
        try {
            //if (cd7110LineDisplay) {
            //    cd7110ShowText(transaction, mode);
            //    return;
            //}

            LineDisplay lineDisplay = POSPeripheralHome3.getInstance().getLineDisplay();

            if (mode == 1) { // 结账模式
                lineDisplay.showTenderInfo(transaction);

            } else {
                LineItem lineItem = transaction.getCurrentLineItem();
                if (lineItem == null)
                    return;

                lineDisplay.showSalesInfo(transaction, lineItem);
            }
        } catch (Exception e) {
        }
    }

    /**
     * @param t 当前交易
     * @param mode 显示模式：
     * 1=结账模式
     * 3=找零模式
     * 其他=销售模式
     */
    public static void cd7110ShowText(Transaction t, int mode) {
        if (mode == 1) {// 结账模式
            HYIDouble salesAmount = t.getSalesAmount();
            HYIDouble change = t.getChangeAmount();
            if (change == null) {
                change = new HYIDouble(0);
            }

            String zeros = "";

            // check upString
            String saString = salesAmount.toString();
            int len = salesAmount.scale();
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            // if (saString.endsWith(zeros)) {
            // salesAmount = salesAmount.setScale(0, BigDecimal.ROUND_HALF_UP);
            // } else {
            salesAmount = salesAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
            // }
            saString = salesAmount.toString();

            // check upString
            String pcString = change.toString();
            len = change.scale();
            zeros = "";
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            if (pcString.endsWith(zeros)) {
                change = change.setScale(0, BigDecimal.ROUND_HALF_UP);
            } else {
                change = change.setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            pcString = change.toString();
//          FunPanel adPanel = POSTerminalApplication.getInstance()
//                  .getAdPanel();
//          if (adPanel != null) {
//              adPanel.showText(pcString, saString);
//          }

            // create linedisplay string
            String line1 = "";
            String line2 = "";
            line1 = line1 + "CHANGE     ";
            while (pcString.length() < 9) {
                pcString = " " + pcString;
            }
            line1 = line1 + pcString;
            // twoString = "SUBTOTAL:";
            while (saString.length() < 11) {
                saString = " " + saString;
            }
            line2 = line2 + saString;

            // show text
            try {
                LineDisplay lineDisplay = POSPeripheralHome3.getInstance().getLineDisplay();
                lineDisplay.clearText();
                lineDisplay.displayText(line2, 3);
            } catch (JposException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit
                        .logMessage("Jpos exception at show text on LineDisplay");
            } catch (Exception e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit
                        .logMessage("No such POSDevice: LineDisplay, at show text on LineDisplay");
            }
        } else if (mode == 3) {// 找零模式
            HYIDouble salesAmount = t.getSalesAmount();
            HYIDouble change = t.getChangeAmount();
            if (change == null) {
                change = new HYIDouble(0);
            }

            String zeros = "";

            // check upString
            String saString = salesAmount.toString();
            int len = salesAmount.scale();
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            if (saString.endsWith(zeros)) {
                salesAmount = salesAmount.setScale(0, BigDecimal.ROUND_HALF_UP);
            } else {
                salesAmount = salesAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            saString = salesAmount.toString();

            // check upString
            String pcString = change.toString();
            len = change.scale();
            zeros = "";
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            // if (pcString.endsWith(zeros)) {
            // change = change.setScale(0, BigDecimal.ROUND_HALF_UP);
            // } else {
            change = change.setScale(2, BigDecimal.ROUND_HALF_UP);
            // }
            pcString = change.toString();
//          FunPanel adPanel = POSTerminalApplication.getInstance()
//                  .getAdPanel();
//          if (adPanel != null) {
//              adPanel.showText(pcString, saString);
//          }

            // create linedisplay string
            String oneString = "";
            String twoString = "";
            // oneString = oneString + "CHANGE ";
            while (pcString.length() < 11) {
                pcString = " " + pcString;
            }
            oneString = oneString + pcString;
            // twoString = "SUBTOTAL:";
            while (saString.length() < 11) {
                saString = " " + saString;
            }
            twoString = twoString + saString;
            // show text
            try {
                LineDisplay lineDisplay = POSPeripheralHome3.getInstance().getLineDisplay();
                lineDisplay.clearText();
                lineDisplay.displayText(oneString, 5);// change
            } catch (JposException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit
                        .logMessage("Jpos exception at show text on LineDisplay");
            } catch (Exception e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit
                        .logMessage("No such POSDevice: LineDisplay, at show text on LineDisplay");
            }
        } else {// 销售模式
            LineItem l = t.getCurrentLineItem();
            if (l == null) {
                return;
            }

            HYIDouble unitPrice = l.getUnitPrice();
            HYIDouble quantity = l.getQuantity();
            HYIDouble amount = l.getAmount();
            HYIDouble salesAmount = t.getSalesAmount();
            // System.out.println("CreamToolkit showText() --> sales amount = "
            // + salesAmount);

            String zeros = "";

            // check upString
            String upString = unitPrice.toString();
            int len = unitPrice.scale();
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            if (upString.endsWith(zeros)) {
                unitPrice = unitPrice.setScale(0, BigDecimal.ROUND_HALF_UP);
            } else {
                unitPrice = unitPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            upString = unitPrice.toString();

            // check upString
            String qtString = quantity.toString();
            len = quantity.scale();
            zeros = "";
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            if (qtString.endsWith(zeros)) {
                quantity = quantity.setScale(0, BigDecimal.ROUND_HALF_UP);
            } else {
                quantity = quantity.setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            qtString = quantity.toString();

            // check upString
            zeros = "";
            String atString = amount.toString();
            len = amount.scale();
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            // if (atString.endsWith(zeros)) {
            // amount = amount.setScale(0, BigDecimal.ROUND_HALF_UP);
            // } else {
            amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
            // }
            atString = amount.toString();

            // check upString
            zeros = "";
            String saString = salesAmount.toString();
            len = salesAmount.scale();
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            if (saString.endsWith(zeros)) {
                salesAmount = salesAmount.setScale(0, BigDecimal.ROUND_HALF_UP);
            } else {
                salesAmount = salesAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            saString = salesAmount.toString();

            // create linedisplay string
            String oneString = "";
            String twoString = "";
            while (upString.length() < 6) {
                upString = upString + " ";
            }
            oneString = oneString + upString + "X";
            while (qtString.length() < 3) {
                qtString = " " + qtString;
            }
            oneString = oneString + qtString + "=";
            while (atString.length() < 11) {
                atString = " " + atString;
            }
            // oneString = oneString + atString;
            oneString = atString;
            twoString = "SUBTOTAL:";
            while (saString.length() < 11) {
                saString = " " + saString;
            }
            twoString = twoString + saString;

            // show text
            try {
                LineDisplay lineDisplay = POSPeripheralHome3.getInstance().getLineDisplay();
                lineDisplay.clearText();
                lineDisplay.displayText(oneString, 2);
            } catch (JposException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit
                        .logMessage("Jpos exception at show text on LineDisplay");
            } catch (Exception e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit
                        .logMessage("No such POSDevice: LineDisplay, at show text on LineDisplay");
            }
        }
    }

    public static Class getSinkStateFromKeyLockCode(int key) {
        switch (key) {
        case KeylockConst.LOCK_KP_LOCK:
            return null;

        case KeylockConst.LOCK_KP_NORM:
            return InitialState.class;

        case KeylockConst.LOCK_KP_SUPR:
            return KeyLock2State.class;

        case KeylockConst.LOCK_KP_SUPR + 1:
            return KeyLock3State.class;

        case KeylockConst.LOCK_KP_SUPR + 2:
        case KeylockConst.LOCK_KP_SUPR + 3:
        case KeylockConst.LOCK_KP_SUPR + 4:
        case KeylockConst.LOCK_KP_SUPR + 5:
            return ConfigState.class;
        }
        return null;
    }

    public static void shrinkFile(File file, long maxSize) {
        try {
            if (!file.exists())
                return;
            if (file.length() <= maxSize)
                return;

            File tempFile = File.createTempFile("smf", "log", file
                    .getParentFile());
            BufferedInputStream inf = new BufferedInputStream(
                    new FileInputStream(file));
            if (inf.skip(file.length() - maxSize) < 0) {
                inf.close();
                return;
            }

            BufferedOutputStream outf = new BufferedOutputStream(
                    new java.io.FileOutputStream(tempFile));
            int c;
            while ((c = inf.read()) != -1) {
                outf.write(c);
            }
            inf.close();
            outf.close();

            file.delete();
            tempFile.renameTo(file);

        } catch (IOException e) {
        }
    }

    /**
     * getAmount的反射方法
     * 
     * @author Meyer
     * @param invokeObj -
     *            Object, 调用方法的对象
     * @param methodName -
     *            String, 调用的方法名
     * @return result - HYIDouble 返回的amount
     */
    public static HYIDouble getAmountMethod(Object invokeObj, String methodName) {
        HYIDouble result = null;
        try {
            Method method = invokeObj.getClass().getDeclaredMethod(methodName,
                    new Class[0]);
            result = (HYIDouble) method.invoke(invokeObj, new Object[0]);
        } catch (NoSuchMethodException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InvocationTargetException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (ClassCastException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return result;
    }

    /**
     * setAmount的反射方法
     * 
     * @author Meyer
     * @param invokeObj -
     *            Object, 调用方法的对象
     * @param methodName -
     *            String, 调用的方法名
     * @param amt -
     *            HYIDouble, 要设置的新值
     */
    public static void setAmountMethod(Object invokeObj, String methodName,
            HYIDouble amt) {
        Class[] classArray = { HYIDouble.class };
        Object[] paramObj = { amt };
        try {
            Method method = invokeObj.getClass().getDeclaredMethod(methodName,
                    classArray);
            method.invoke(invokeObj, paramObj);
        } catch (NoSuchMethodException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InvocationTargetException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    /**
     * getCount的反射方法
     * 
     * @author Meyer
     * @param invokeObj -
     *            Object, 调用方法的对象
     * @param methodName -
     *            String, 调用的方法名
     * @return result - int, 返回的count
     */
    public static int getCountMethod(Object invokeObj, String methodName) {
        int result = 0;
        try {
            Method method = invokeObj.getClass().getDeclaredMethod(methodName,
                    new Class[0]);
            result = ((Integer) method.invoke(invokeObj, new Object[0]))
                    .intValue();
        } catch (NoSuchMethodException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InvocationTargetException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (ClassCastException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return result;
    }

    /**
     * setCount的反射方法
     * 
     * @author Meyer
     * @param invokeObj -
     *            Object, 调用方法的对象
     * @param methodName -
     *            String, 调用的方法名
     * @param count -
     *            int, 要设置的新值
     */
    public static void setCountMethod(Object invokeObj, String methodName,
            int count) {
        Class[] classArray = { Integer.class };
        Object[] paramObj = { new Integer(count) };
        try {
            Method method = invokeObj.getClass().getDeclaredMethod(methodName,
                    classArray);
            method.invoke(invokeObj, paramObj);
        } catch (NoSuchMethodException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InvocationTargetException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    /**
     * Get language encoding.
     */
    public static String getEncoding() {
        String locale = Param.getInstance().getLocale();
        if (locale.equals("zh_TW"))
            return "ms950";
        else if (locale.equals("zh_CN"))
            return "GBK";
        else if (locale.equals("UTF-8"))
            return "UTF-8";
        else
            return "GBK";
    }

    /**
     * Get printer character encoding.
     */
    public static String getPrinterCharacterEncoding() {
        String locale = Param.getInstance().getLocale();
        if (locale.equals("zh_TW"))
            return "ms950";
        else if (locale.equals("zh_CN"))
            return "GBK";
        else if (locale.equals("UTF-8"))
            return "UTF-8";
        else
            return "GBK";
    }

    /**
     * Get the check digit of EAN13 bar code.
     * 
     * @param barCode
     *            barCode number string
     * @return String the C/D.
     */
    public static String getEAN13CheckDigit(String barCode) {
        if (barCode.length() < 12)
            return null;
        int decode = 0;
        for (int i = 0; i < 12; i++) {
            int k = (int) barCode.charAt(11 - i) - 48;
            if (i % 2 == 0)
                k = (k * 3) % 10;
            else
                k %= 10;
            decode += k;
        }
        if (decode % 10 != 0)
            decode = 10 - decode % 10;
        else
            decode = 0;
        return String.valueOf(decode);
    }

    /**
     * Check if the EAN bar code's check digit is correct.
     * 
     * @param barCode
     *            EAN13 bar code number string.
     * @return boolean true if correct, false otherwise.
     */
    public static boolean checkEAN13(String barCode) {
        String cd = CreamToolkit.getEAN13CheckDigit(barCode);
        return (cd == null) ? false : barCode.endsWith(cd);
    }

    public static boolean execute(String sql) {
        boolean success = false;
        if (sql == null || sql.trim().equals(""))
            return success;
        DbConnection conn = null;
        Statement st = null;
        try {
            conn = getPooledConnection();
            st = conn.createStatement();
            success = st.execute(sql);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("sql : " + sql);
            e.printStackTrace(getLogger());
        } finally {
            try {
                st.close();
            } catch (Exception e) {
            }
            releaseConnection(conn);
        }
        return success;
    }

    public static Map getResultData(String sql) {
        Map data = new HashMap();
        if (sql == null || sql.trim().equals(""))
            return data;
        DbConnection conn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            conn = getPooledConnection();
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            if (rs.next()) {
                int count = rs.getMetaData().getColumnCount();
                for (int i = 1; i <= count; i++)
                    data.put(rs.getMetaData().getColumnName(i), rs.getObject(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace(getLogger());
        } finally {
            try {
                rs.close();
                st.close();
            } catch (Exception e) {
            }
            releaseConnection(conn);
        }
        return data;
    }

    public static java.util.List getResultData2(String sql) {
        java.util.List data = new ArrayList();
        if (sql == null || sql.trim().equals(""))
            return data;
        DbConnection conn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            conn = getPooledConnection();
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Map record = new HashMap();
                int count = rs.getMetaData().getColumnCount();
                for (int i = 1; i <= count; i++)
                    record.put(rs.getMetaData().getColumnName(i), rs
                            .getObject(i));
                data.add(record);
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace(getLogger());
        } finally {
            try {
                rs.close();
                st.close();
            } catch (Exception e) {
            }
            releaseConnection(conn);
        }
        return data;
    }

    public static HYIDouble getPluPrice(Transaction trans, PLU curPLU) {

        HYIDouble pluPrice = curPLU.getUnitPrice();
        HYIDouble pluPmPrice = curPLU.getSpecialPrice();

        // check discount price
        Date currentDate = new Date();
        java.sql.Time currentTime = new java.sql.Time(currentDate.getTime());
        if (pluPmPrice != null && pluPmPrice.compareTo(new HYIDouble(0)) == 1) {
            java.sql.Date discountStartDate = curPLU.getDiscountStartDate();
            java.sql.Date discountEndDate = curPLU.getDiscountEndDate();
            if (discountStartDate != null && discountEndDate != null
                    && currentDate.after(discountStartDate)
                    && currentDate.before(discountEndDate)) {
                java.sql.Time discountStart = curPLU.getDiscountStartTime();
                java.sql.Time discountEnd = curPLU.getDiscountEndTime();
                int discountStartHour = Integer.parseInt(discountStart
                        .toString().substring(0, 2));
                int discountStartMinute = Integer.parseInt(discountStart
                        .toString().substring(3, 5));
                int discountStartSecond = Integer.parseInt(discountStart
                        .toString().substring(6));
                int discountEndHour = Integer.parseInt(discountEnd.toString()
                        .substring(0, 2));
                int discountEndMinute = Integer.parseInt(discountEnd.toString()
                        .substring(3, 5));
                int discountEndSecond = Integer.parseInt(discountEnd.toString()
                        .substring(6));
                int currentHour = currentTime.getHours();
                int currentMinute = currentTime.getMinutes();
                int currentSecond = currentTime.getSeconds();
                if (currentHour < discountEndHour
                        && currentHour > discountStartHour) {
                    pluPrice = pluPmPrice;
                } else if (currentHour == discountEndHour
                        || currentHour == discountStartHour) {
                    if (currentMinute < discountEndMinute
                            && currentMinute > discountStartMinute) {
                        pluPrice = pluPmPrice;
                    } else if (currentMinute == discountEndMinute
                            && currentMinute == discountStartMinute) {
                        if (currentSecond <= discountEndSecond
                                && currentSecond >= discountEndSecond) {
                            pluPrice = pluPmPrice;
                        }
                    }
                }
            }
        }

        // check member price
        if (trans.getMemberID() != null) {
            HYIDouble memberPrice = curPLU.getMemberPrice();
            if (memberPrice != null) {
                if (pluPrice.compareTo(memberPrice) == 1) {
                    pluPrice = memberPrice;
                }
            }
        }
        return pluPrice;
    }

    public static boolean isInterzone(Date checkDate, Date bDate, Date eDate,
            Date bTime, Date eTime) {
        Calendar tmp = Calendar.getInstance();
        tmp.setTime(checkDate);
        tmp.set(Calendar.HOUR_OF_DAY, 0);
        tmp.set(Calendar.MINUTE, 0);
        tmp.set(Calendar.SECOND, 0);
        tmp.set(Calendar.MILLISECOND, 0);
        Date date = tmp.getTime();

        if ((date.compareTo(bDate) < 0) || (date.compareTo(eDate) > 0))
            return false;

        if (bTime == null)
            return true;
        tmp.setTime(checkDate);
        Calendar tmp2 = Calendar.getInstance();
        tmp2.setTime(bTime);
        tmp.set(Calendar.DAY_OF_MONTH, tmp2.get(Calendar.DAY_OF_MONTH));
        tmp.set(Calendar.MONTH, tmp2.get(Calendar.MONTH));
        tmp.set(Calendar.YEAR, tmp2.get(Calendar.YEAR));
        date = tmp.getTime();
        // System.out.println("date : " + date);
        tmp = null;
        tmp2 = null;
        return ((date.compareTo(bTime) >= 0) && (date.compareTo(eTime) <= 0)) ? true
                : false;
    }

    public static void drawInfoFrame(Graphics og, int x, int y, int w, int h, int arc) {
        og.fillRoundRect(x, y, w, h, arc, arc);
    }

    public static String[] getPlayList(String playDir) {
        String[] all = null;
        try {
            File dir = new File(playDir);
            File[] allFiles = dir.listFiles(new FileSelector(dir, "mpg"));
            all = new String[allFiles.length];
            for (int i = 0; i < allFiles.length; i++) {
                all[i] = allFiles[i].getAbsolutePath();
                System.out.println("found media : " + all[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return all;
    }

    /**
     * Get application resource string.
     */
    public static String getString(String resourceId, Object ... arguments) {
        try {
            if (arguments == null || arguments.length == 0)
                return GetResource().getString(resourceId);
            else
                return MessageFormat.format(GetResource().getString(resourceId), arguments);
        } catch (Exception e) {
            // if cannot be found, return the resourceId itself
            return resourceId;
        }
    }

    /**
     * 轉鑰匙訊息至鎖定或OFF位置的文字來自resource string "TurnKeyToOff(XXXX)",
     * 其中XXXX可能是:
     * <p/>
     * 1. Keylock device logical name.<br/>
     * 2. If not exist Keylock device, use "ibm".<br/>
     * <p/>
     * 如果resource string找不到，就使用Keylock.getKeyPositionName(KeylockConst.LOCK_KP_LOCK)
     * 获得转钥匙信息。通常这些信息存在jpos.xml的Keylock entry中的property "NameOfLOCK"
     *
     * @since 2008-8-18 Bruce You
     */
    public static void showTurnKeyToOffPositionMessage() {
        String message = "LOCK";
        try {
            String keylockName = POSPeripheralHome3.getInstance().getKeylockLogicalName();
            if (keylockName == null)
                keylockName = "ibm";
            message = GetResource().getString("TurnKeyToOff(" + keylockName + ")");
        } catch (MissingResourceException e) {
            try {
                String posName = POSPeripheralHome3.getInstance().getKeylock()
                    .getKeyPositionName(KeylockConst.LOCK_KP_LOCK);
                message = getString("TurnKeyToOff", posName);
            } catch (Exception e1) {
                logMessage(e1);
            }
        } finally {
            POSTerminalApplication.getInstance().getMessageIndicator().setMessage(
                message);
        }
    }

    /**
     * 轉鑰匙訊息至鎖定或OFF位置的文字來自resource string "TurnKeyToSalesPosition(XXXX)",
     * 其中XXXX可能是:
     * <p/>
     * 1. Keylock device logical name.<br/>
     * 2. If not exist Keylock device, use "ibm".<br/>
     * <p/>
     * 如果resource string找不到，就使用Keylock.getKeyPositionName(KeylockConst.LOCK_KP_NORM)
     * 获得转钥匙信息。通常这些信息存在jpos.xml的Keylock entry中的property "NameOfNORM"
     *
     * @since 2008-8-18 Bruce You
     */
    public static void showTurnKeyToSalesPositionMessage() {
        String message = "SALE";
        try {
            String keylockName = POSPeripheralHome3.getInstance().getKeylockLogicalName();
            if (keylockName == null)
                keylockName = "ibm";
            message = GetResource().getString("TurnKeyToSalesPosition(" + keylockName + ")");
        } catch (MissingResourceException e) {
            try {
                String posName = POSPeripheralHome3.getInstance().getKeylock()
                    .getKeyPositionName(KeylockConst.LOCK_KP_NORM);
                message = getString("TurnKeyToSalesPosition", posName);
            } catch (Exception ex) {
                logMessage(ex);
            }
        } finally {
            POSTerminalApplication.getInstance().getMessageIndicator().setMessage(
                message);
        }
    }

    public static int compareDate(Date date1, Date date2)
    {
        return convertDateToStringInYYYYMMDDFormat(date1).compareTo(
            convertDateToStringInYYYYMMDDFormat(date2));
    }

    private static String convertDateToStringInYYYYMMDDFormat(Date date) {
        return yyyyMMdd.format(date);
    }

    /**
     * Try retrieving integer value from an unknown object.
     */
    public static int retrieveIntValue(Object obj) {
        if (obj == null)
            return 0;
        else if (obj instanceof Integer)
            return ((Integer)obj).intValue();
        else if (obj instanceof Long)
            return ((Long)obj).intValue();
        else if (obj instanceof Short)
            return ((Short)obj).intValue();
        else if (obj instanceof Byte)
            return ((Byte)obj).intValue();
        else if (obj instanceof Boolean)
            return ((Boolean)obj).booleanValue() ? 1 : 0;
        else
            return 0;
    }

    public static void sleepInSecond(int seconds) {
        try {
            Thread.sleep(seconds * 1000L);
        } catch (InterruptedException e) {
        }
    }

    public static void haltSystemOnDatabaseFatalError(Exception e) {
        SsmpLog.report10002(e);
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        app.getWarningIndicator().setMessage(getString("DatabaseFatalError"));
        StateMachine.getInstance().setEventProcessEnabled(false);
    }

    public static Date shiftDay(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, days);
        return cal.getTime();
    }

    public static Date shiftMinutes(Date date, int minutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        return cal.getTime();
    }

    public static String replace(String source, String target, int startpos) {
        source = source.substring(0, startpos)
                + target + source.substring(startpos + 1, source.length());
        return source;
    }
    
    public static HYIDouble convertBigDecimal2HYIDouble(BigDecimal value) {
        if (value == null)
            return null;
        return new HYIDouble(value.doubleValue());
    }

    public static boolean isDatabaseMySQL()
    {
        return databaseMySQL;
    }

    public static boolean isDatabasePostgreSQL()
    {
        return databasePostgreSQL;
    }

    /**
     * Returns a copy of the object, or null if the object cannot be serialized.
     */
    public static Object deepClone(Object orig)
    {
        Object obj = null;
        try
        {
            // Write the object out to a byte array
            FastByteArrayOutputStream fbos = new FastByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(fbos);
            out.writeObject(orig);
            out.flush();
            out.close();

            // Retrieve an input stream from the byte array and read
            // a copy of the object back in.
            ObjectInputStream in = new ObjectInputStream(fbos.getInputStream());
            obj = in.readObject();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException cnfe)
        {
            cnfe.printStackTrace();
        }
        catch (Exception e2){
            e2.printStackTrace();
        }
        return obj;
    }

    public static int getPrintLength(String data) {
        int len = 0;
        for (int i = 0; i < data.length(); i++) {
            char c = data.charAt(i);
            if ((int)c > 255) // Chinese char
                len += 2;
            else
                len++;
        }
        return len;
    }

    public static String dacToString(String line, DacBase dac) {
        CreamFormatter formatter = CreamFormatter.getInstance();
        StringBuilder retLine = new StringBuilder();
        StringTokenizer tokener = new StringTokenizer(line, "[],", true);

        try {
            String text = tokener.nextToken();
            while (true) {
                if (!text.equals("[")) {
                    retLine.append(text);
                    text = tokener.nextToken();
                }
                if (!text.equals("["))
                    continue;
                String property = tokener.nextToken();
                tokener.nextToken(); // ','
                text = tokener.nextToken(); // maybe a ',' or a pattern
                String pattern = null;
                if (!text.equals(",")) {
                    pattern = text;
                    tokener.nextToken(); // ','
                }
                text = tokener.nextToken(); // maybe a ']' or a align&length
                String alignAndLength = null;
                if (!text.equals("]")) {
                    alignAndLength = text;
                    tokener.nextToken(); // ']'
                }

                try {
                    String formatedText = "";
                    Object valueObject;
                    Method method = dac.getClass().getDeclaredMethod("get" + property);
                    valueObject = method.invoke(dac);

                    if (valueObject == null) { //Bruce/20030505/ prevent a NullPointerException problem
                        formatedText = "";
                    } else if (pattern != null) {
                        if (valueObject instanceof Date || valueObject instanceof Timestamp) {
                            formatedText = formatter.format((Date)valueObject, pattern);
                        } else if (valueObject instanceof HYIDouble) {
                            formatedText =
                                formatter.format(((HYIDouble)valueObject).doubleValue(), pattern);
                        } else if (valueObject instanceof Integer) {
                            formatedText = formatter.format(((Integer)valueObject).intValue(), pattern);
                        } else if (valueObject instanceof String) {
                            formatedText = (String)valueObject;
                        }
                    } else {
                        formatedText = valueObject.toString();
                    }

                    if (alignAndLength != null) {
                        // get direction : 'L' means left; 'R' means right
                        String align = alignAndLength.substring(0, 1);

                        // get string's length
                        int needLen = Integer.parseInt(alignAndLength.substring(1));
                        int len = needLen - getPrintLength(formatedText);

                        //Bruce/2003-12-05
                        // 因为灿坤的receipt.conf中交易日期给的长度不够，造成如果要trim length的话会把后面砍掉，
                        // 但是灿坤的receipt.conf格式文件种类众多，不想更新receipt.conf，所以在遇到日期pattern
                        // 的时候，就不要right trim了。 蛮tricky!
                        if (len < 0 && (pattern == null || pattern.indexOf("yyyy") == -1)) {
                            // right trim length
                            for (int i = len; i < 0;) {
                                char c = formatedText.charAt(formatedText.length() - 1);
                                if ((int)c > 255) { // Chinese char
                                    i += 2;
                                } else {
                                    i++;
                                }
                                formatedText = formatedText.substring(0, formatedText.length() - 1);
                            }
                            if (getPrintLength(formatedText) < needLen)
                                formatedText += " ";
                        } else {
                            // do for string's length
                            for (int i = 0; i < len; i++) {
                                if (align.equalsIgnoreCase("R")) {
                                    formatedText = " " + formatedText;
                                } else {
                                    formatedText = formatedText + " ";
                                }
                            }
                        }
                    }
                    retLine.append(formatedText);

                } catch (Exception e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }

                text = tokener.nextToken();
            }
        } catch (NoSuchElementException e) {
        }

        return retLine.toString();
    }

    public static void reboot() {
        try {
            logMessage("start to reboot ..............");
        } catch (Exception e) {
        } finally {
            // {SYSTEM-DEPENDENT}
            try {
                String[] cmd = { "sudo", "/sbin/reboot" };
                Runtime.getRuntime().exec(cmd).waitFor();
                System.exit(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关机
     */
    public static void halt() {
        try {
            hyi.cream.inline.Client.getInstance().processCommand("quit");
        } catch (Exception e) {
        } finally {
            // {SYSTEM-DEPENDENT}
            try {
                String[] cmd = { "sudo", "/sbin/poweroff" };
                Runtime.getRuntime().exec(cmd).waitFor();
                System.exit(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关机
     */
    public static void halt2() {
        try {
            hyi.cream.inline.Client.getInstance().processCommand("quit");
        } catch (Exception e) {
        } finally {
            // {SYSTEM-DEPENDENT}
            try {
                String[] cmd = { "/sbin/shutdown", "now" };
                Runtime.getRuntime().exec(cmd).waitFor();
                System.exit(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void stopPos(final int s) {
        logMessage("The system is going to stop in " + s + " seconds!");
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(s * 1000);
                    hyi.cream.inline.Client.getInstance().processCommand("quit");
                    //releaseAll();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    System.exit(0);
                }
            }
        }).start();
    }

    public static String fillZero(String str, int len) {
        if (str == null)
            str = "";
        while (str.length() < len) {
            str = "0" + str;
        }
        return str.substring(0, len);
    }
    public static String getValueFromXml(String xml, String token1, String token2) {
        String result = "";
        int a = xml.indexOf(token1);
        int b = xml.indexOf(token2);

        if (a >= 0 && b >= 0) {
            result = xml.substring(a+token1.length(), b);
        }
        return result;
    }

    public static void wanConnected() {
        String ip = Param.getInstance().getPingURL();
        String ips[] = ip.split(";");
        boolean connected = false;
        for (int i = 0; i < ips.length; i++) {
            connected = CreamToolkit.callPing(ips[i]);
            if (connected)
                break;
        }
        SystemInfo.setWanConnected(connected);
    }

    //Call OS Ping Command. gllg
    public static boolean callPing(String ip) {
        String str = "";
        //Windows
        if (System.getProperty("os.name").toString().indexOf("Windows") != -1) {
            str = "ping " + ip + " -w 5000 -n 1";
        } else { //Linux
            str = "/bin/ping " + ip + " -w 2 -c 1";
        }


//        System.out.println("ping command:" + str);
        Runtime rt = Runtime.getRuntime();
        String log = "";
        Process process = null;
        try {
            process = rt.exec(str);
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream(),"gbk"));
            String line = br.readLine();
            while (line != null) {
                log = log + line + "\n";
                line = br.readLine();
            }
            br.close();
            try {
                process.waitFor();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (process != null) {
                process.destroy();
            }
        }
//        System.out.println("ping " + ip);
//        System.out.println("result " + log);
        if (log.indexOf("Reply from") != -1 || log.indexOf("bytes from") != -1 || log.indexOf("来自") != -1) {
            return true;
        } else
            return false;

    }


}

class FileSelector implements FilenameFilter {
    private File dir = null;

    private String name = null;

    public FileSelector(File dir, String name) {
        this.dir = dir;
        this.name = name;
    }

    public boolean accept(File dir, String name) {
        if (name.trim().endsWith(this.name.trim()) && dir.equals(this.dir))
            return true;
        else
            return false;
    }


//    public static void main(String[] args) {
//        //test1();
//        test2();
//        //test3();
//    }
//
//    private static void test3() {
//        int sum =0;
//        boolean isValid = false;
//        String barcode = "12345678901234123454";
//        String chechStr = "4";
//        for (int i = 0; i < 19; i++) {
//            if ((i + 1) % 2 == 0)
//                sum += barcode.charAt(i) - 48; // '0'的ASCII码为48
//            else
//                sum += 3 * (barcode.charAt(i) - 48);
//        }
//        sum += Integer.parseInt(chechStr);
//        if (sum % 10 == 0)
//            isValid = true;
//        System.out.println("" + sum % 10);
//    }
//
//    private static void test2() {
//        System.out.println(CreamToolkit.getEAN13CheckDigit("1234567009605"));
//        System.out.println(CreamToolkit.getEAN13CheckDigit("2234567009604"));
//        System.out.println(CreamToolkit.getEAN13CheckDigit("123456701000"));
//    }
//
//    private static void test1() {
////        System.out.println(CreamToolkit.getInitialDate());
//        System.out.println(CreamToolkit.replace("000000", "1", 0));
//        System.out.println(CreamToolkit.replace("000000", "1", 1));
//        System.out.println(CreamToolkit.replace("000000", "1", 5));
//        System.out.println(CreamToolkit.getInitialDate());
//    }
}