package hyi.cream.util;

//import hyi.cream.POSPeripheralHome;

import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.*;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.groovydac.Param;
import hyi.cream.groovydac.SsmpLog;
import hyi.cream.state.InventoryState;
import hyi.cream.state.InventoryStockState;
import hyi.cream.state.State;
import hyi.cream.state.StateToolkit;
import hyi.spos.JposException;
import hyi.spos.POSPrinter;
import hyi.spos.POSPrinterConst;
import org.apache.commons.lang.StringUtils;

import java.io.StringWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

//import jpos.JposException;
//import jpos.POSPrinter;
//import jpos.POSPrinterConst;

//import org.apache.log4j.Logger;

public class GenericCreamPrinter extends CreamPrinter {

    //private static Logger logger = Logger.getLogger(GenericCreamPrinter.class);
    private static GenericCreamPrinter instance = null;

    POSTerminalApplication app = POSTerminalApplication.getInstance();
    private boolean firstPage = true;
    private int lineItemsPrintedInThisPage;
    private int linesPrintedInThisPage;
    public static final int MAX_CHAR_LINE = 24;
    public static final int MAX_ITEM_LINES = 15;
    private HYIDouble tax0Amount = new HYIDouble(0);
    private HYIDouble tax1Amount = new HYIDouble(0);
    private HYIDouble tax2Amount = new HYIDouble(0);
    private HYIDouble tax3Amount = new HYIDouble(0);
    private HYIDouble tax4Amount = new HYIDouble(0);
    private HYIDouble zero = new HYIDouble(0);
    private boolean printDaiFu;
    private ReceiptGenerator invoiceGen;
    
    private GenericCreamPrinter() {
        invoiceGen = ReceiptGenerator.getInstance();
        res = CreamToolkit.GetResource();
        //isPrintToConsoleInsteadOfPrinter = GetProperty.getIsPrint2Console("no").equals("yes");
        printDaiFu = Param.getInstance().isPrintDaiFu();

        // // 交易需要需要打乱
        // needConfuseTranNumber = GetProperty.getConfuseTranNumber("");
    }

    public static GenericCreamPrinter getInstance_zh_CN() {
        if (instance == null)
            instance = new GenericCreamPrinter();
        return instance;
    }

    /**
     * Get POS printer and enable it.
     */
    private POSPrinter getPOSPrinter() throws NoSuchPOSDeviceException, JposException {
        POSPrinter printer = POSPeripheralHome3.getInstance().getPOSPrinter();
        if (!printer.getClaimed())
            printer.claim(2000);
        if (!printer.getDeviceEnabled() )
            printer.setDeviceEnabled(true);
        return printer;
    }

    /**
     * Print to printer(receipt and journal) or console, but does't count up
     * ReceiptGenerator.LinesBeforPaymentPrinted.
     */
    private void printToPrinter(DbConnection connection, POSPrinter printer, String data) throws JposException {
        String line = data.toString();

        printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, line, line);

        if (line.endsWith("\f")) {  // "\f" == "\u000C"
            Param.getInstance().updateInvoicePrintingUnfinished(false);
        }

        linesPrintedInThisPage++;
        invoiceGen.setLinesPrinted(linesPrintedInThisPage);

        // write invoice log
        if (line.charAt(line.length() - 1) == '\n') // 把最后一个换行符滤掉，下面的loger会自己换行的
            line = line.substring(0, line.length() - 1);
        CreamToolkit.recordPrint(line);
    }

    /**
     * The same as printSummary(), but also count up ReceiptGenerator.LinesBeforPaymentPrinted.
     */
    private void print(DbConnection connection, POSPrinter printer, String data) throws JposException {
        printToPrinter(connection, printer, data);
        invoiceGen.setLinesBeforPaymentPrinted(linesPrintedInThisPage);
    }

    /**
     * Do four things:<br/> 1. Really cut paper.<br/> 2. Write invoice printing log.<br/> 3. Set
     * "InvoicePrintingUnfinished" to false.<br/> 4. Count up next invoice number in property if
     * needed.<p>
     * 
     * 因為可能會更新property中的"InvoiceNumber"，所以可能會throw SQLException.
     */
    private void cutPaper(DbConnection connection, POSPrinter printer, int cutPercentage,
        boolean needCountUpInvoiceNumber) throws JposException, SQLException {
        if (app.getTrainingMode())
            return;

        try {
            printer.cutPaper(cutPercentage);
            CreamToolkit.recordPrint("--------PAPER CUT-------");
            //logger.info("--------PAPER CUT-------");

            if (needCountUpInvoiceNumber) {
            	System.out.println("--------------  needCountUpInvoiceNumber");
            	app.setLastInvoiceID(Param.getInstance().getInvoiceID());
            	app.setLastInvoiceNumber(Param.getInstance().getInvoiceNumber());
				StateToolkit.countUpInvoiceNumber(connection);
				System.out.println("--------------  needCountUpInvoiceNumber end");            	
            }

            Param.getInstance().updateInvoicePrintingUnfinished(false);

        } catch (EntityNotFoundException e) {
            throw new SQLException(e.toString());
        } catch (SQLException e) {
            throw e;
        }
    }

    /**
     * Cut paper fully.
     */
    private void cutPaper(DbConnection connection, POSPrinter printer,
        boolean needCountUpInvoiceNumber) throws JposException, SQLException {
        cutPaper(connection, printer, 100, needCountUpInvoiceNumber);
    }

    /**
     * Cut paper nearly fully.
     */
    private void cutPaperPartially(DbConnection connection, POSPrinter printer,
        boolean needCountUpInvoiceNumber) throws JposException, SQLException {
        cutPaper(connection, printer, 99, needCountUpInvoiceNumber);
    }

    public boolean isPrinterAtRightStart() {
        try {
            if (!getPrintEnabled())
                return true;
            if (!Param.getInstance().isNeedCheckPrinterAtRightStart())
            	return true;
            if (app.getTrainingMode())
                return true;

            String status = getPOSPrinter().getCheckHealthText();
            System.out.println("[Printer status]=>[" + status + "]");
            // n0: "S"
            // n1: 收執聯位置 "0":定位 "1":未定位
            // n2: 存根聯位置 "0":定位 "1":未定位
            // n3: 紙張是否用完 "0":否 "1":是
            // n4: 印證紙張定位 "0":無紙 "1":有紙
            // n5: 發票印表機發生錯誤 "0":沒發生 "1":發生
            // n6: 是否列印中 "0":否 "1":是
            // n7: 列印超過40行 "0":否 "1":是
            // n8: 資料緩衝區是否已滿 "0":否 "1":是
            // n9: 資料緩衝區是否為空 "0":是 "1":否
            boolean ok = status != null && (
                status.charAt(1) == '0' || status.startsWith("Well now")
                /*&& status.charAt(3) == '0' && status.charAt(5) == '0' && status.charAt(8) == '0'*/ );
            if (!ok)
                SsmpLog.report11001();
            return ok;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return false;
        } catch (NoSuchPOSDeviceException e) {
            e.printStackTrace();
            return false;
        } catch (JposException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isPrinterHealthy() {
        return true; // don't check any printer status in-between transaction

//        try {
//            if (!getPrintEnabled())
//                return true;
//
//            String status = getPOSPrinter().getCheckHealthText();
//            // n0: "S"
//            // n1: 收執聯位置 "0":定位 "1":未定位
//            // n2: 存根聯位置 "0":定位 "1":未定位
//            // n3: 紙張是否用完 "0":否 "1":是
//            // n4: 印證紙張定位 "0":無紙 "1":有紙
//            // n5: 發票印表機發生錯誤 "0":沒發生 "1":發生
//            // n6: 是否列印中 "0":否 "1":是
//            // n7: 列印超過40行 "0":否 "1":是
//            // n8: 資料緩衝區是否已滿 "0":否 "1":是
//            // n9: 資料緩衝區是否為空 "0":是 "1":否
//            return status.charAt(3) == '0' && status.charAt(5) == '0' && status.charAt(8) == '0';
//        } catch (IndexOutOfBoundsException e) {
//            return false;
//        } catch (NoSuchPOSDeviceException e) {
//            return false;
//        } catch (JposException e) {
//            return false;
//        }
    }

    public void printCashForm(CashForm j) {
        if (!getPrintEnabled())
            return;
        if (app.getTrainingMode())
            return;
        StringWriter data = new StringWriter();
        ReportGenerator r = new ReportGenerator(j);
        data = (StringWriter)r.generate(data, false);
        try {
            POSPrinter printer = getPOSPrinter();
            print(null, printer, data.toString());
            cutPaperPartially(null, printer, false); // 让它不要卷太多
            printFirstReceipt();
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        } catch (SQLException e) {
            // never happened, because it won't update any property
        }
    }
//    public void printCashForm(CashForm j) {
//    	if (!getPrintEnabled())
//    		return;
//    	StringWriter data = new StringWriter();
//    	ReportGenerator r = new ReportGenerator(j);
//    	data = (StringWriter)r.generate(data, false);
//    	try {
//    		POSPrinter printer = getPOSPrinter();
//    		
//    		String printerType = GetProperty.getPrinterType("");
//    		if (printerType != null && printerType.equalsIgnoreCase("OneStation")) {
//    			print(printer, data.toString());
//    			return;
//    		}
//    		
//    		if (GetProperty.getPrinterCutPaperType("").equalsIgnoreCase("star")) {
//    			print(printer, data.toString());
//    			cutPaper(null, printer, false);
//    			return;
//    		} else if (GetProperty.getPosType("").toLowerCase().startsWith("tec")) {
//    			print(printer, "\u001B|1C");
//    			print(printer, "\u001B|N");
//    		}
//    		
//    		if (invoiceGen.isWorkable()) {
//    			print(printer, data.toString());
//    			pageUp(null, false);
//    			if (GetProperty.getPosType("").toLowerCase().startsWith("tec")) {
//    				cutPaper(null, printer, false);
//    			}
//    			return;
//    		}
//    		
//    		print(printer, "\u001B|4lF");
//    		print(printer, data.toString());
//    		print(printer, "\u001B|10lF");
//    		// pageUp();
//    		if (GetProperty.getPosType("").toLowerCase().startsWith("tec")) {
//    			cutPaperPartially(null, printer, false); // 让它不要卷太多
//    		}
//    		
//    		printFirstReceipt();
//    	} catch (NoSuchPOSDeviceException e) {
//    		CreamToolkit.logMessage(e);
//    	} catch (JposException e) {
//    		CreamToolkit.logMessage(e);
//    	} catch (SQLException e) {
//    		// never happened, because it won't update any property
//    	}
//    }

    public void printXReport(ZReport z, State sourceState) {
        if (!getPrintEnabled())
            return;
        if (app.getTrainingMode())
            return;
        StringWriter data = new StringWriter();
        ReportGenerator r = new ReportGenerator(z);
        data = (StringWriter)r.generate(data, true);
        try {
            POSPrinter printer = getPOSPrinter();
            print(null, printer, data.toString());
            cutPaperPartially(null, printer, false);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        } catch (SQLException e) {
            // never happened, because it won't update any property
        }
    }
//    public void printXReport(ZReport z, State sourceState) {
//    	if (!getPrintEnabled())
//    		return;
//    	StringWriter data = new StringWriter();
//    	ReportGenerator r = new ReportGenerator(z);
//    	data = (StringWriter)r.generate(data, true);
//    	try {
//    		POSPrinter printer = getPOSPrinter();
//    		
//    		if (GetProperty.getPrinterCutPaperType("").equalsIgnoreCase("star")) {
//    			print(printer, data.toString());
//    			cutPaperPartially(null, printer, false);
//    			return;
//    		} else if (GetProperty.getPosType("").toLowerCase().startsWith("tec")) {
//    			print(printer, "\u001B|1C");
//    			print(printer, "\u001B|N");
//    		}
//    		
//    		if (invoiceGen.isWorkable()) {
//    			print(printer, data.toString());
//    			pageUp(null, false);
//    			if (GetProperty.getPosType("").equalsIgnoreCase("tec")) {
//    				cutPaperPartially(null, printer, false);
//    			}
//    			;
//    			return;
//    		}
//    		
//    		print(printer, "\u001B|4lF");
//    		print(printer, data.toString());
//    		print(printer, "\u001B|10lF");
//    		pageUp(null, false);
//    		if (GetProperty.getPosType("").toLowerCase().startsWith("tec")) {
//    			cutPaperPartially(null, printer, false);
//    		}
//    		
//    		printFirstReceipt();
//    	} catch (NoSuchPOSDeviceException e) {
//    		CreamToolkit.logMessage(e);
//    	} catch (JposException e) {
//    		CreamToolkit.logMessage(e);
//    	} catch (SQLException e) {
//    		// never happened, because it won't update any property
//    	}
//    }

    public void printZReport(ZReport z, State sourceState) {
        if (!getPrintEnabled())
            return;
        if (app.getTrainingMode())
            return;
        StringWriter data = new StringWriter();
        ReportGenerator r = new ReportGenerator(z);
        data = (StringWriter)r.generate(data);
        try {
            POSPrinter printer = getPOSPrinter();
            print(null, printer, data.toString());
            cutPaperPartially(null, printer, false);
            //printFirstReceipt();
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        } catch (SQLException e) {
            // never happened, because it won't update any property
        }
    }
//    public void printZReport(ZReport z, State sourceState) {
//    	if (!getPrintEnabled())
//    		return;
//    	StringWriter data = new StringWriter();
//    	ReportGenerator r = new ReportGenerator(z);
//    	data = (StringWriter)r.generate(data);
//    	try {
//    		POSPrinter printer = getPOSPrinter();
//    		
//    		if (GetProperty.getPrinterCutPaperType("").equalsIgnoreCase("star")) {
//    			print(printer, data.toString());
//    			cutPaperPartially(null, printer, false);
//    			return;
//    		} else if (GetProperty.getPosType("").toLowerCase().startsWith("tec")) {
//    			print(printer, "\u001B|1C");
//    			print(printer, "\u001B|N");
//    		}
//    		
//    		if (invoiceGen.isWorkable()) {
//    			print(printer, data.toString());
//    			pageUp(null, false);
//    			if (GetProperty.getPosType("").equalsIgnoreCase("tec")) {
//    				cutPaperPartially(null, printer, false);
//    			}
//    			return;
//    		}
//    		
//    		print(printer, "\u001B|4lF");
//    		print(printer, data.toString());
//    		print(printer, "\u001B|10lF");
//    		pageUp(null, false);
//    		if (GetProperty.getPosType("").toLowerCase().startsWith("tec")) {
//    			cutPaperPartially(null, printer, false);
//    		}
//    		
//    		printFirstReceipt();
//    	} catch (NoSuchPOSDeviceException e) {
//    		CreamToolkit.logMessage(e);
//    	} catch (JposException e) {
//    		CreamToolkit.logMessage(e);
//    	} catch (SQLException e) {
//    		// never happened, because it won't update any property
//    	}
//    }

    private void pageUp(DbConnection connection, boolean needCountUpInvoiceNumber) throws SQLException {
        try {
            if (app.getTrainingMode())
                return;
            POSPrinter printer = getPOSPrinter();
            if (invoiceGen.isWorkable()) {
                // Print "P" section in receipt format definition file
                Collection lines = invoiceGen.generateLines('P', new hyi.cream.dac.LineItem());
                Iterator iter = lines.iterator();
                while (iter.hasNext()) {
                    String line = iter.next().toString();
                    if (line.indexOf("\u000C") != -1) {
                        print(connection, printer, line);
                    } else if (line.indexOf("$CUT_PAPER_AT_MARK") != -1) {
                        cutPaper(connection, printer, needCountUpInvoiceNumber); // 遇黑点切纸
                    } else if (line.indexOf("$CUT_PAPER") != -1) {
                        cutPaperPartially(connection, printer, needCountUpInvoiceNumber); // 立即切纸
                    } else {
                        print(connection, printer, line + "\n");
                    }
                }
            } else {
                print(connection, printer, "\u001B|V");
            }
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        }
    }

    public void printShiftReport(ShiftReport shift, State sourceState) {
    	if (!getPrintEnabled())
    		return;
        if (app.getTrainingMode())
            return;
    	StringWriter data = new StringWriter();
    	ReportGenerator r = new ReportGenerator(shift);
    	data = (StringWriter)r.generate(data);
    	try {
    		POSPrinter printer = getPOSPrinter();
    		print(null, printer, data.toString());
    		cutPaperPartially(null, printer, false);
    	} catch (NoSuchPOSDeviceException e) {
    		CreamToolkit.logMessage(e);
    	} catch (JposException e) {
    		CreamToolkit.logMessage(e);
    	} catch (SQLException e) {
    		// never happened, because it won't update any property
    	}
    }
//    public void printShiftReport(ShiftReport shift, State sourceState) {
//        if (!getPrintEnabled())
//            return;
//        StringWriter data = new StringWriter();
//        ReportGenerator r = new ReportGenerator(shift);
//        data = (StringWriter)r.generate(data);
//        try {
//            POSPrinter printer = getPOSPrinter();
//            if (GetProperty.getPrinterCutPaperType("").equalsIgnoreCase("star")) {
//                print(printer, data.toString());
//                cutPaperPartially(null, printer, false);
//                return;
//            } else if (GetProperty.getPosType("").toLowerCase().startsWith("tec")) {
//                print(printer, "\u001B|1C");
//                print(printer, "\u001B|N");
//            }
//
//            if (invoiceGen.isWorkable()) {
//                print(printer, data.toString());
//                pageUp(null, false);
//                if (GetProperty.getPosType("").toLowerCase().startsWith("tec")) {
//                    cutPaperPartially(null, printer, false);
//                }
//                return;
//            }
//            print(printer, "\u001B|4lF");
//            print(printer, data.toString());
//            print(printer, "\u001B|10lF");
//            pageUp(null, false);
//            if (GetProperty.getPosType("").toLowerCase().startsWith("tec")) {
//                cutPaperPartially(null, printer, false);
//            }
//        } catch (NoSuchPOSDeviceException e) {
//            CreamToolkit.logMessage(e);
//        } catch (JposException e) {
//            CreamToolkit.logMessage(e);
//        } catch (SQLException e) {
//            // never happened, because it won't update any property
//        }
//    }

    /**
     * Slip format:
     * 
     * <pre>
     *    1234567890123456789012345678901234567890123456789012
     *    TTT YYMMDD HH:MM TT-NNNNNN SSSSSS CCCCCCC $999999.99
     *         
     *    TTT: 交易别
     *         Zxx: 支付类（xx为支付代号）
     *         Gyy: 折扣类（yy为折扣代号）
     *    YYMMDD: 公元日期
     *    HH:MM: 时间
     *    TT: 收银机机号
     *    NNNNNN: 交易序号
     *    SSSSSS: 店号
     *    CCCCCCC: 收银员
     *    $999999.99: 支付或折扣金额
     * </pre>
     */
    private String getSlipData(Transaction tran, State sourceState) {
        boolean discount = false;
        boolean returnItem = false;
        String space = " ";
        if (sourceState.getClass().getName().endsWith("Slipping2State")
            || sourceState.getClass().getName().endsWith("Slipping3State"))
            discount = true;
        if (sourceState.getClass().getName().endsWith("ReturnSlippingState"))
            returnItem = true;
        String slip = "";
        if (discount && tran.getLastSI() != null)
            slip += "G" + tran.getLastSI().getSIID() + space;
        else if (!returnItem)
            slip += "Z" + tran.getLastestPayment().getPaymentID() + space;
        else if (returnItem)
            slip += "RTN" + space;
        SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd HH:MM");
        Date currentTime_1 = new Date();
        slip += formatter.format(currentTime_1) + space;
        slip += Param.getInstance().getTerminalNumber() + "-";
        String number = Param.getInstance().getNextTransactionNumber() + "";
        slip += getFormattedTransactionNumber(Integer.parseInt(number)) + space;
        slip += Store.getStoreID() + space;
        slip += Param.getInstance().getCashierNumber() + space;
        if (discount && tran.getLastSI() != null)
            slip += "$" + tran.getAppliedSIs().get(tran.getLastSI());
        else if (!returnItem) {
            String payID = tran.getLastestPayment().getPaymentID();
            slip += "$"
                + tran.getPayAmountByID(tran.getCorrespondingID(payID)).setScale(2, 4);
        } else if (returnItem) {
            slip += "$" + tran.getSalesAmount();
        }
        slip += "\n";
        return slip;
    }

    private String getFormattedTransactionNumber(int number) {
        String NextTransactionSequenceNumber = String.valueOf(number);
        String zero = "";
        if (NextTransactionSequenceNumber.length() < 8) {
            int addBits = 8 - NextTransactionSequenceNumber.length();
            for (int i = 0; i < addBits; i++) {
                zero += "0";
            }
            NextTransactionSequenceNumber = zero + NextTransactionSequenceNumber;
        } else if (NextTransactionSequenceNumber.length() > 8) {
            int beginIndex = NextTransactionSequenceNumber.length() - 8;
            NextTransactionSequenceNumber = NextTransactionSequenceNumber.substring(beginIndex,
                NextTransactionSequenceNumber.length());
        }
        return NextTransactionSequenceNumber;
    }

    public void printSlip(State sourceState) {
        if (app.getTrainingMode())
            return;
        try {
        	String data = getSlipData(app.getCurrentTransaction(), sourceState);
            POSPrinter printer = getPOSPrinter();
            printer.printNormal(POSPrinterConst.PTR_S_SLIP, data);
            CreamToolkit.recordPrint(data);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        }
    }

    public void reprint(DbConnection connection, Transaction tran) throws SQLException {
        if (!getPrintEnabled())
            return;
        if (app.getTrainingMode())
            return;
        printHeader(connection, tran);
        Object[] lines = tran.getLineItems();
        int length = tran.getLineItems().length;
        for (int i = 0; i < length; i++) {
            LineItem li = (LineItem)lines[i];
            char detailCode = ' ';
            try {
                detailCode = li.getDetailCode().charAt(0);
            } catch (Exception e) {
            }

            if (li.getPrinted())
                li.setPrinted(false);

            switch (detailCode) {
            case 'M':
            case 'D':
            case 'E':
            case 'A':
                continue;
            }
            if (li.getDetailCode().compareTo("S") == 0 && li.getRemoved() && i < length - 1) {
                LineItem lineItem = (LineItem)lines[i + 1];
                if (lineItem.getDetailCode().compareTo("E") == 0)
                    continue;
            }
            printLineItem(connection, tran, li);
        }
        printPayment(connection, tran);
        SsmpLog.report10007(tran.getTransactionNumber());
    }

    public void reprint(DbConnection connection, int transNumber) throws SQLException {
        if (!getPrintEnabled())
            return;
        Transaction previousTransaction = Transaction.queryByTransactionNumber(connection,
            Param.getInstance().getTerminalNumber(), transNumber);
        if (previousTransaction == null)
            return;
        reprint(connection, previousTransaction);
    }

    public void printLineItem(DbConnection connection, Transaction tran, LineItem lineItem) throws SQLException {
        if (!getPrintEnabled() || lineItem.getPrinted() || !invoiceGen.isWorkable())
            return;
        if (app.getTrainingMode())
            return;

        printLineItemByReceiptGenerator(connection, tran, lineItem);
    }

    public void printLineItem(DbConnection connection, LineItem lineItem) throws SQLException {
        if (!getPrintEnabled() || lineItem.getPrinted() || !invoiceGen.isWorkable())
            return;
        if (app.getTrainingMode())
            return;

        Transaction tran = app.getCurrentTransaction();
        printLineItemByReceiptGenerator(connection, tran, lineItem);
    }

    private void printLineItemByReceiptGenerator(DbConnection connection, Transaction trans,
        LineItem lineItem) throws SQLException {
    	boolean isCutPaper = false;
        if (!getPrintEnabled()) {
            return;
        }
        if (lineItem.getPrinted()) {
            return;
        }
        if (app.getTrainingMode())
            return;

        // 如果不需印代付，而且此交易为代付交易的话，直接返回
        if (!printDaiFu && lineItem.getDetailCode().equals("Q")) {
            return;
        }
        try {
            Collection lines;
            Iterator iter;

            POSPrinter printer = getPOSPrinter();
            if (linesPrintedInThisPage >= invoiceGen.getLinesPerPage()
                || lineItemsPrintedInThisPage >= invoiceGen.getLineItemPerPage()) {
                lines = invoiceGen.generateLines('B', lineItem);
                iter = lines.iterator();
                while (iter.hasNext()) {
                    print(connection, printer, iter.next().toString() + "\n");
                }
                lines = invoiceGen.generateLines('P', lineItem);
                iter = lines.iterator();
                while (iter.hasNext()) {
                    String line = iter.next().toString();
                    if (line.indexOf("\u000C") != -1) {
                        print(connection, printer, line);
                    } else if (line.indexOf("$CUT_PAPER_AT_MARK") != -1) {
                        line = res.getString("Subtotal") + ":"
                            + getSubTotal().setScale(2, 4) + "\n\n";
                        print(connection, printer, line);
                        cutPaper(connection, printer, true); // 遇黑点切纸
                        isCutPaper = true;
                    } else if (line.indexOf("$CUT_PAPER") != -1) {
                        cutPaperPartially(connection, printer, true); // 立即切纸
                        isCutPaper = true;
                    } else if (line.indexOf("$CUT_PAPER") == -1) {
                        print(connection, printer, line + "\n");
                    }
                }
                invoiceGen.setPageNumber(invoiceGen.getPageNumber() + 1);
                if (StringUtils.isEmpty(Param.getInstance().getInvoiceID()) && isCutPaper) {
                	// 最后一张打印结束後，不继续
                	;
                } else {
                	printHeaderByReceiptGenerator(connection, trans);
                }
                lineItemsPrintedInThisPage = 0;
                invoiceGen.setLineItemsPrintedInThisPage(0);
            }
            if (StringUtils.isEmpty(Param.getInstance().getInvoiceID()) && isCutPaper) {
            	// 最后一张打印结束後，不继续
            	;
            } else {
	            subTotal = subTotal.add(lineItem.getAmount());
	            lines = invoiceGen.generateLines('D', lineItem);
	            iter = lines.iterator();
	            while (iter.hasNext()) {
	                String line = iter.next().toString() + "\n";
	                print(connection, printer, line);
	            }
	            lineItemsPrintedInThisPage++;
	            invoiceGen.setLineItemsPrintedInThisPage(lineItemsPrintedInThisPage);
	            lineItem.setPrinted(true);
            }

        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        }
    }

    public void printHeader(DbConnection connection, Transaction tran) {
        if (!getPrintEnabled() || !invoiceGen.isWorkable())
            return;
        if (app.getTrainingMode())
            return;

        printHeaderByReceiptGenerator(connection, tran);
    }

    /**
     * 打印盘点表头（包括：门市编号名称、盘点时间、仓位编号、盘点人员编号）
     */
    public void printInventoryHeader() {
        try {
            if (app.getTrainingMode())
                return;
            POSPrinter printer = getPOSPrinter();

            String content = CreamToolkit.GetResource().getString("InventoryData") + "\n";
            content += res.getString("Store") + ":"
                + app.getSystemInfo().getStoreNameAndNumber()
                + "\n";
            content += new SimpleDateFormat(CreamToolkit.GetResource().getString("TimeFormat"))
                .format(new Date())
                + "\n";
            content += CreamToolkit.GetResource().getString("CashierNo")
                + InventoryState.getCashierNumber();
            content += CreamToolkit.GetResource().getString("StoreHouseNo")
                + InventoryStockState.getStockNumber() + "\n\n";

            print(null, printer, content);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        }
    }

    /**
     * 打印盘点记录
     */
    public void printInventoryItem(Inventory inv) {
        try {
            if (app.getTrainingMode())
                return;
            POSPrinter printer = getPOSPrinter();

            StringBuffer content = new StringBuffer(inv.getItemSequence().toString());
            content.append("  " + inv.getItemNumber());
            content.append(" " + inv.getDescription() + "\n");
            content.append("    @" + inv.getUnitPrice());
            content.append(" x " + inv.getActStockQty());
            content.append(" = " + inv.getAmount() + "\n");

            print(null, printer, content.toString());
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        }
    }

    /**
     * 打印盘点小计
     */
    public void printInventoryFooter() {
        try {
            if (app.getTrainingMode())
                return;
            POSPrinter printer = getPOSPrinter();

            HYIDouble total = new HYIDouble(0);
            List dacList = app.getDacViewer().getDacList();
            if (dacList != null && dacList.size() > 0) {
                Iterator iter = dacList.iterator();
                while (iter.hasNext()) {
                    total = total.addMe(((Inventory)iter.next()).getAmount());
                }
            }
            String content = "\n\n" + CreamToolkit.GetResource().getString("InventoryTotalAmount")
                + total.toString() + "\n\n\n\n\n\n\n\n";
            print(null, printer, content);
            printer.cutPaper(99);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        }
    }

    /**
     * Print current transaction header.
     */
    public void printHeaderByReceiptGenerator(DbConnection connection) {
        if (app.getTrainingMode())
            return;
        Transaction trans = app.getCurrentTransaction();

        // 如果不需印代付，而且此交易为代付交易的话，直接返回
        if (!printDaiFu && trans.getDaiFuAmount() != null
            && trans.getDaiFuAmount().compareTo(zero) != 0)
            return;

        printHeaderByReceiptGenerator(connection, trans);
    }

    /**
     * Print a given transaction's header.
     * 
     * @param trans
     *            Transaction DAC.
     */
    public void printHeaderByReceiptGenerator(DbConnection connection, Transaction trans) {
    	boolean firstLine = true;
        if (!getPrintEnabled())
            return;
        if (app.getTrainingMode())
            return;

        // 如果不需印代付，而且此交易为代付交易的话，直接返回
        if (!printDaiFu && trans.getDaiFuAmount() != null
            && trans.getDaiFuAmount().compareTo(zero) != 0) {
            return;
        }

        linesPrintedInThisPage = 0;
        invoiceGen.setLinesPrinted(0);

        // ZhaoHong 2003-06-05
        invoiceGen.setLinesBeforPaymentPrinted(0);
        List lines = new ArrayList();
        try {
            if (firstPage) {
                lines.addAll(invoiceGen.generateLines('T', trans));
                invoiceGen.setPageNumber(1);
            }
            lines.addAll(invoiceGen.generateLines('H', trans));
            setSubTotal(new HYIDouble(0));

            POSPrinter printer = getPOSPrinter();

            // added by zhaohong 2003-12-10 tec 的打印机打印出来字体变大了，需要重新设置
            lineItemsPrintedInThisPage = 0;

            try {
                //if (GetProperty.getPosType("").toLowerCase().startsWith("tec")) {
                if (POSPeripheralHome3.getInstance().getPOSPrinter().getClass().equals("POSPrinterDRJST51")) {
                    //&& !GetProperty.getPrinterCutPaperType("").equalsIgnoreCase("star")) {
                    print(connection, printer, "\u001B|1C");
                    print(connection, printer, "\u001B|N");
                }
            } catch (Exception e) {}

//            try {
//                if (!GetProperty.getTopBitmapName("").equals("")){
//                	String data = GetProperty.getTopBitmapName("");
//                	if (justPrintToConsole)
//                		System.out.println(data);
//                	else
//                		printer.printBitmap(POSPrinterConst.PTR_S_RECEIPT, data, POSPrinterConst.PTR_BM_ASIS,
//                				POSPrinterConst.PTR_BM_CENTER);
//                }
//            } catch (JposException e) {
//                CreamToolkit.logMessage(e);
//            }
            Iterator iter = lines.iterator();
            while (iter.hasNext()) {
            	print(connection, printer, iter.next().toString() + "\n");
                if (firstPage) {
                    // if print header successfuly, set property "InvoicePrintingUnfinished" to true
                	if (firstLine) {
                		try {
	                        Param.getInstance().updateInvoicePrintingUnfinished(true);
                		} catch (Exception e) {
                			e.printStackTrace();
                		}
                	}
                	firstLine = false;
                }
            }
            if (firstPage)
                firstPage = false;

        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
//        } catch (SQLException e) {
//            CreamToolkit.logMessage(e);
//            CreamToolkit.haltSystemOnDatabaseFatalError();
        }
    }

    public void tryPrint() throws NoSuchPOSDeviceException, JposException {
    	if (!getPrintEnabled() || !invoiceGen.isWorkable())
    		return;
    	
    	String line = "--- Try print ...";
        String lineOk = line + "[OK]\n";
        POSPrinter printer = getPOSPrinter();
        printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, lineOk, lineOk);
        CreamToolkit.recordPrint(lineOk);
    }
    
    public void printHeader(DbConnection connection) {
        if (!getPrintEnabled() || !invoiceGen.isWorkable())
            return;
        if (app.getTrainingMode())
            return;

        printHeaderByReceiptGenerator(connection);
    }

    public void printCancel(DbConnection connection, String cancelMsg) throws SQLException {
        // Otherwise, print "cancel" mark on receipt, feed and cut paper,
        if (!getPrintEnabled())
            return;
        if (app.getTrainingMode())
            return;
        try {
            POSPrinter printer = getPOSPrinter();
            String line = cancelMsg + "\n\n\n";
            print(connection, printer, line);

            if (invoiceGen.isWorkable()) {
                pageUp(connection, true);
            } else {
                // 因为如果是用receipt.conf的话，已经打印'P' section了,里面已经有包含切纸了
                if (Param.getInstance().getPosType().toLowerCase().startsWith("tec"))
                    cutPaper(connection, printer, true);
            }
            setCurrentLine(1);
            setCurrentPage(1);
            firstPage = true;
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        }
    }

    public void printStamp() {
        if (!getPrintEnabled())
            return;
        if (app.getTrainingMode())
            return;
        try {
        	String data = "\u001B|sL";
            POSPrinter printer = getPOSPrinter();
            printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, data);
            CreamToolkit.recordPrint(data);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        }
    }

    public void printStampI() {
        if (!getPrintEnabled())
            return;
        if (app.getTrainingMode())
            return;
        try {
        	POSPrinter printer = getPOSPrinter();
        	print(null, printer, "\u001B|2C Welcome to\n\u001B|2C  C-Store");
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        }
    }

    public void printPayment(DbConnection connection, Transaction tran) throws SQLException {
        if (!getPrintEnabled() || !invoiceGen.isWorkable())
            return;
        if (app.getTrainingMode())
            return;

        printPaymentByReceiptGenerator(connection, tran);
    }

    private void printPaymentByReceiptGenerator(DbConnection connection, Transaction trans) throws SQLException {
        try {
            if (!getPrintEnabled())
                return;
            if (app.getTrainingMode())
                return;

            // 如果不需印代付，而且此交易为代付交易的话，直接返回
            if (!printDaiFu && trans.getDaiFuAmount() != null
                && trans.getDaiFuAmount().compareTo(zero) != 0) {
                System.out.println("PrintPaymentByReceiptGenerator(): Didn't print this trans with daifu amount="
                    + trans.getDaiFuAmount());
                return;
            }

            POSPrinter printer = getPOSPrinter();
            // linesPrintedInThisPage += invoiceGen.getBlankLinesBeforePayment();
            Collection lines = invoiceGen.generateLines('B', trans);
            Iterator iter = lines.iterator();
            while (iter.hasNext()) {
                String line = iter.next().toString();
                printToPrinter(connection, printer, line + "\n");
            }

            lines = invoiceGen.generateLines('F', trans);
            iter = lines.iterator();
            while (iter.hasNext()) {
                String line = iter.next().toString();
                if (line.indexOf("\u000C") != -1) {
                    printToPrinter(connection, printer, line);
                } else if (line.indexOf("$CUT_PAPER_AT_MARK") != -1) {
//                    try {
//                        if (!GetProperty.getBottomBitmapName("").equals("")) {
//                        	String data = GetProperty.getBottomBitmapName("");
//                        	if (justPrintToConsole)
//                        		System.out.println(data);
//                        	else
//                        		printer.printBitmap(POSPrinterConst.PTR_S_RECEIPT, data, POSPrinterConst.PTR_BM_ASIS,
//                        				POSPrinterConst.PTR_BM_CENTER);
//		                    CreamToolkit.recordPrint(data);
//                        }
//                    } catch (JposException e) {
//                        CreamToolkit.logMessage(e);
//                    }
                    cutPaper(connection, printer, true); // 遇黑点切纸
                } else if (line.indexOf("$CUT_PAPER") != -1) {
//                    try {
//                        if (!GetProperty.getBottomBitmapName("").equals("")) {
//                        	String data = GetProperty.getBottomBitmapName("");
//                        	if (justPrintToConsole)
//                        		System.out.println(data);
//                        	else
//                        		printer.printBitmap(POSPrinterConst.PTR_S_RECEIPT, data, POSPrinterConst.PTR_BM_ASIS,
//                        				POSPrinterConst.PTR_BM_CENTER);
//		                	CreamToolkit.recordPrint(data);
//                        }
//                    } catch (JposException e) {
//                        CreamToolkit.logMessage(e);
//                    }
                    cutPaperPartially(connection, printer, true); // 立即切纸
                } else if (line.indexOf("$CUT_PAPER") == -1) {
                    printToPrinter(connection, printer, line + "\n");
                }
            }
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } finally {
            lineItemsPrintedInThisPage = 0;
            firstPage = true;
        }
    }

    public String replaceLineFeed(String data) {
        while (true) {
            int i = data.indexOf("\\n");
            if (i == -1)
                break;
            data = data.substring(0, i) + "\n" + data.substring(i + 2);
        }
        return data;
    }

    public void printSubtotal(DbConnection connection) throws SQLException {
        if (!getPrintEnabled())
            return;
        if (getSubTotal().doubleValue() == 0)
            return;
        if (app.getTrainingMode())
            return;
        try {
            POSPrinter printer = getPOSPrinter();
            String subtotalContent = "\n" + res.getString("Subtotal") + ":"
                + getSubTotal().setScale(2, 4) + "\n\n";

            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            printToPrinter(connection, printer, subtotalContent);
            if (Param.getInstance().getPosType().toLowerCase().startsWith("tec"))
                cutPaper(connection, printer, true);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e);
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        }
    }

    public void printFirstReceipt() {
        // if (!getPrintEnabled())
        // return;
        // String command = "";
        // //cut paper at the point;//feed 36 lines
        // //command = "\u001B|2lF";
        // //print the stammp
        // command += "\u001B|V";
        // command = "\u001B|8lF";
        // command += "\u001B|P";
        // try {
        // POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        // POSPrinter printer = posHome.getPOSPrinter();
        // if (!printer.getClaimed())
        // printer.claim(1000);
        // if (!printer.getDeviceEnabled())
        // printer.setDeviceEnabled(true);
        // //command = "\u001B|2lF";
        // //printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, command);
        // //printer.cutPaper(100);
        // } catch (NoSuchPOSDeviceException e) {
        // CreamToolkit.logMessage(e);
        // } catch (JposException e) {
        // CreamToolkit.logMessage(e);
        // }
    }

    public HYIDouble getTax0Amount() {
        return tax0Amount;
    }

    public void setTax0Amount(HYIDouble amt) {
        tax0Amount = amt;
    }

    public HYIDouble getTax1Amount() {
        return tax1Amount;
    }

    public void setTax1Amount(HYIDouble amt) {
        tax1Amount = amt;
    }

    public HYIDouble getTax2Amount() {
        return tax2Amount;
    }

    public void setTax2Amount(HYIDouble amt) {
        tax2Amount = amt;
    }

    public HYIDouble getTax3Amount() {
        return tax3Amount;
    }

    public void setTax3Amount(HYIDouble amt) {
        tax3Amount = amt;
    }

    public HYIDouble getTax4Amount() {
        return tax4Amount;
    }

    public void setTax4Amount(HYIDouble amt) {
        tax4Amount = amt;
    }

    public static void main(String[] args) {
        // -z [z_seq]
        // -s [z_seq] [shift_seq]
        if (args.length == 0 || args[0].equals("-z") && args.length < 2 || args[0].equals("-s")
            && args.length < 3) {
            System.exit(0);
        }
        DbConnection connection = null;
        try {
            if (args[0].equals("-z")) {
                connection = CreamToolkit.getPooledConnection();
                ZReport z = ZReport.queryBySequenceNumber(connection, new Integer(args[1]));
                CreamPrinter.getInstance().printZReport(z, null);
                System.exit(0);
            }
            if (args[0].equals("-s")) {
                connection = CreamToolkit.getPooledConnection();
                ShiftReport shift = ShiftReport.queryByZNumberAndShiftNumber(connection,
                    new Integer(args[1]), new Integer(args[2]));
                CreamPrinter.getInstance().printShiftReport(shift, null);
                System.exit(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}
