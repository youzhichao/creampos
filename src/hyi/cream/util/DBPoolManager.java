package hyi.cream.util;

import org.apache.commons.dbcp.BasicDataSourceFactory;
import org.apache.commons.dbcp.BasicDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DBPoolManager {
    // static private Vector drivers = new Vector();
    // static private String defaultPool;
    // static private Hashtable pools = new Hashtable();
    // static private Properties prop;

    static private DataSource dataSource;
    static private DBPoolManager pool;

    /**
     * @param prop -- the Properties object
     */
    protected DBPoolManager(Properties dbcpProperties) {
        try {
            if (dbcpProperties != null) {
                dataSource = BasicDataSourceFactory.createDataSource(dbcpProperties);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param prop -- the Properties object
     */
    static public void init(Properties prop) {
        if (pool == null)
            pool = new DBPoolManager(prop);
    }

    // static public void release() {
    // if (ds != null)
    // try {
    // ds.close();
    // } catch (SQLException e) {
    // e.printStackTrace();
    // }
    // ds = null;
    //        pool = null;
    //    }

    /**
     * @return PoolManager
     */
    static public DBPoolManager getPool() {
        if (pool == null) {
            pool = new DBPoolManager(null);
            CreamToolkit.logMessage("dbpool not create, use default dbpool!");
        }
        return pool;
    }


    // /**
    // * @param props -- the Properties object
    // */
    // static protected void loadDrivers(Properties dbcpProperties) throws Exception {
    // String poolName = props.getProperty("db.default");
    // String driverClassName = props.getProperty("db.pools." + poolName + ".driver");
    // ds = new BasicDataSource();
    // int max = 100;
    // try {
    // max = Integer.parseInt(props.getProperty("db.pools." + poolName + ".maxconns", "100"));
    // } catch (NumberFormatException e) {}
    // String longinTimes = props.getProperty("db.pools." + poolName + ".logintimeout");
    // try {
    // Integer.parseInt(longinTimes);
    // } catch (Exception e) {
    // longinTimes = "5";
    // }
    // try {
    // Class.forName(driverClassName);
    // ds.setDriverClassName(driverClassName);
    // // ds.setLoginTimeout(Integer.parseInt(longinTimes));
    // ds.setMaxActive(max);
    // // ds.setMaxActive(5);
    // ds.setUsername(props.getProperty("db.pools." + poolName + ".user"));
    // ds.setPassword(props.getProperty("db.pools." + poolName + ".password"));
    // ds.setUrl(props.getProperty("db.pools." + poolName + ".url"));
    // } catch (Exception e) {
    // CreamToolkit.logMessage("load db driver! " + e.getMessage());
    // }
    // }

    /**
     * Get a connection based on default pool (defined by default in property file)
     * 
     * @return connection -- the Connection object
     * @exception java.util.NoSuchElementException
     *                default Pool name not found
     * @exception java.sql.SQLException
     *                DB error while getting DB connection from default pool
     */
    static public Connection getConnection() throws SQLException {
        Connection connection = null;
        int retry = 1;

        // PostgreSQL server可能在application啟動時尚未啟動，so we do a retry here
        while (retry <= 18) {
            try {
                connection = dataSource.getConnection();
                break;
            } catch (SQLException e) {
                e.printStackTrace();
                CreamToolkit.sleepInSecond(5);
                retry++;
                CreamToolkit.logMessage("Retry connecting to database(" + retry + ")...");
            }
        }

        // conn.setTransactionIsolation(0);
        if (((BasicDataSource)dataSource).getNumActive() > 3) {
            System.out.println("DBPoolManager> maxActive=" + ((BasicDataSource)dataSource).getMaxActive()
                + ", maxIdle=" + ((BasicDataSource)dataSource).getMaxIdle() + ", numActive="
                + ((BasicDataSource)dataSource).getNumActive() + ", numIdle="
                + ((BasicDataSource)dataSource).getNumIdle());
        }

        connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        return connection;
    }
}