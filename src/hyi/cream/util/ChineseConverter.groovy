package hyi.cream.util

import hyi.cream.util.CreamToolkit
import hyi.cream.groovydac.Param

/**
 * 繁體轉简体，和英語術語轉中文術語的工具。
 * <p/>
 * 簡繁體對照表在chinese_map.txt，術語對照表在chinese_map.txt。
 * <p/>
 * <pre>
 * 例如convert()方法可以將：
 *   "請你開{screen}和{printer}!"
 *
 * 在繁體環境轉成：
 *   "請你開螢幕和印表機!"
 *
 * 在简体环境转成：
 *   "请你开屏幕和打印机!"
 * </pre>
 *
 * @author Bruce You
 */
class ChineseConverter {

    // Singleton ----------------------------------------------------
    private static ChineseConverter instance = new ChineseConverter()
    private ChineseConverter() { init() }
    static ChineseConverter getInstance() { return instance }
    //---------------------------------------------------------------

    private charMap = [:]   // map of 繁 to 簡
    private termMap = [:]   // map of 英文term to [簡體term, 繁體term]

    boolean traditional     // may be changed by Param.switchBetweenTraditionalAndSimplifiedChinese()

    private void init() {
        try {
            URL url = this.class.getResource('/hyi/cream/util/chinese_map.txt')
            def reader = url.newReader('utf-8')
            reader.eachLine { line ->
                charMap[line.charAt(1)] = line.charAt(0)
            }
            reader.close()

            url = this.class.getResource('/hyi/cream/util/chinese_term.txt')
            reader = url.newReader('utf-8')
            reader.eachLine { line ->
                def term = line.split(/,/)
                termMap[term[0]] = [term[1], term[2]] as String[]
            }
            reader.close()

            traditional = Param.instance.tranditionalChinese()

        } catch (Exception e) {
            CreamToolkit.logMessage e
        }
    }

    String convert(String string) {
        
        // First, convert terms, for example:
        // convert 'xxx{os}yyy' to 'xxx作業系統yyy'
        // convert 'xxx{os}yyy' to 'xxx操作系统yyy'
        // convert 'xxx{printer}yyy' to 'xxx印表機yyy'
        // convert 'xxx{printer}yyy' to 'xxx打印机yyy'

        def replMap = [:]
        def matcher = (string =~ /(?:\{)(.*?)(?:\})/) // 只capture大括弧中間的term
        matcher.each { whole, term ->
            def replacement = termMap[term]
            if (replacement != null)
                replMap[term] = replacement
        }
        replMap.each { term, replacement ->
            string = string.replaceAll("\\{$term\\}", traditional ? replacement[1] : replacement[0])
        }

        // Then convert 繁 to 简 if needed

        if (traditional) return string

        def buffer = new StringBuilder(string.length());
        for (i in 0..<string.length()) {
            def 繁 = string.charAt(i)
            def 简 = charMap.get(繁)
            buffer << (简 != null ? 简 : 繁)
        }
        return buffer.toString();
    }

    /*static void main(args) {
        println(instance.convert('{screen}x{screen}中華{os}民{printer}國萬{software}歲！{thought}'));
    }*/
}