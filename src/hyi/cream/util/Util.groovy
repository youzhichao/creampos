package hyi.cream.util

/**
 * Utility class in Groovy.
 *
 * @author Bruce You on 8/5/15.
 */
class Util {

    static boolean isPostgreSQLDumpFile(file) {
        def firstLine
        (file as File).withReader { firstLine = it.readLine() }
        if (firstLine)
            return firstLine.contains('PostgreSQL')
        else
            return false
    }

    static int importPostgreSQLDumpFile(file) {
        ['/usr/bin/psql', '-d', 'cream', '-U', 'postgres', '-q', '-f', file]
            .execute().waitFor()
    }
}
