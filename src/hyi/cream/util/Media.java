package hyi.cream.util;

import javax.swing.JPanel;

public class Media extends JPanel { 
	/*implements ControllerListener {
}

	// media Player
	Player player = null;

	// component in which video is playing
	Component visualComponent = null;

	// controls gain, position, start, stop
	Component controlComponent = null;

	// displays progress during download
	Component progressBar = null;

	boolean firstTime = true;

	long CachingSize = 0L;

	Panel panel = null;

	int controlPanelHeight = 0;

	int videoWidth = 0;

	int videoHeight = 0;
	Color backgroundColor;
	
	public Media(Color color) {
		this.backgroundColor = color;
	}
	public void init(String[] file) {
		// $ System.out.println("Applet.init() is called");
		setLayout(null);
		setBackground(backgroundColor);
		panel = new Panel();
		panel.setLayout(null);
		add(panel);
		panel.setSize(this.getWidth(), this.getHeight());

		// input file name from html param
//		String mediaFile = "file:///d:\\backup\\video\\bb.mpg";
		String mediaFile = "file:///" + file[0];
		// URL for our media file
		MediaLocator mrl = null;
		URL url = null;

		// Get the media filename info.
		// The applet tag should contain the path to the
		// source media file, relative to the html page.

		try {
			// Create a media locator from the file name
			if ((mrl = new MediaLocator(mediaFile)) == null)
				Fatal("Can't build URL for " + mediaFile);


			// Create an instance of a player for this media
			try {
				player = Manager.createPlayer(mrl);
			} catch (NoPlayerException e) {
				System.out.println(e);
				Fatal("Could not create player for " + mrl);
			}

			// Add ourselves as a listener for a player's events
			player.addControllerListener(this);
		} catch (MalformedURLException e) {
			Fatal("Invalid media file URL!");
		} catch (IOException e) {
			Fatal("IO exception creating player for " + mrl);
		}

		// This applet assumes that its start() calls
		// player.start(). This causes the player to become
		// realized. Once realized, the applet will get
		// the visual and control panel components and add
		// them to the Applet. These components are not added
		// during init() because they are long operations that
		// would make us appear unresposive to the user.
	}

	public void start() {
		// $ System.out.println("Applet.start() is called");
		// Call start() to prefetch and start the player.
		System.out.println("--------------- Media start : " + player);
		if (player != null) {
			player.start();
		}
	}

	public void stop() {
		// $ System.out.println("Applet.stop() is called");
		if (player != null) {
			player.stop();
//			player.deallocate();
		}
	}

	public void pause() {
//		if (player != null) {
//			player.
//		}
	}
	public void destroy() {
		// $ System.out.println("Applet.destroy() is called");
		player.close();
	}

	public synchronized void controllerUpdate(ControllerEvent event) {
		// If we're getting messages from a dead player,
		// just leave
		if (player == null)
			return;

		// When the player is Realized, get the visual
		// and control components and add them to the Applet
		if (event instanceof RealizeCompleteEvent) {
			if (progressBar != null) {
				panel.remove(progressBar);
				progressBar = null;
			}

			int width = this.WIDTH;
			int height = this.HEIGHT;
//			if (controlComponent == null)
//				if ((controlComponent = player.getControlPanelComponent()) != null) {
//
//					controlPanelHeight = controlComponent.getPreferredSize().height;
//					panel.add(controlComponent);
//					height += controlPanelHeight;
//				}
			if (visualComponent == null)
				if ((visualComponent = player.getVisualComponent()) != null) {
					panel.add(visualComponent);
					Dimension videoSize = visualComponent.getPreferredSize();
					videoWidth = videoSize.width;
					videoHeight = videoSize.height;
					width = videoWidth;
					height += videoHeight;
					System.out.println("------- Media | WIDTH : " + this.WIDTH + 
							" | HEIGHT : " + this.HEIGHT + " | videoWidth : " + videoWidth +
							" | videoHeight : " + videoHeight + 
							" | height : " + this.getHeight() + " | width : " + this.getWidth());
					visualComponent.setBounds((this.getWidth() - videoWidth) / 2, 
							(this.getHeight() - videoHeight) / 2, videoWidth, videoHeight);	
//					visualComponent.setBounds(0, 0, videoWidth, videoHeight);	
				}

			panel.setBounds(0, 0, this.getWidth(), this.getHeight());
			if (controlComponent != null) {
				controlComponent.setBounds(0, videoHeight, width,
						controlPanelHeight);
				controlComponent.invalidate();
			}

		} else if (event instanceof CachingControlEvent) {
			if (player.getState() > Controller.Realizing)
				return;
			// Put a progress bar up when downloading starts,
			// take it down when downloading ends.
			CachingControlEvent e = (CachingControlEvent) event;
			CachingControl cc = e.getCachingControl();

			// Add the bar if not already there ...
			if (progressBar == null) {
				if ((progressBar = cc.getControlComponent()) != null) {
					panel.add(progressBar);
					panel.setSize(progressBar.getPreferredSize());
					validate();
				}
			}
		} else if (event instanceof EndOfMediaEvent) {
			// We've reached the end of the media; rewind and
			// start over
			player.setMediaTime(new Time(0));
			player.start();
		} else if (event instanceof ControllerErrorEvent) {
			// Tell TypicalPlayerApplet.start() to call it a day
			player = null;
			Fatal(((ControllerErrorEvent) event).getMessage());
		} else if (event instanceof ControllerClosedEvent) {
			panel.removeAll();
		}
	}

	void Fatal(String s) {
		// Applications will make various choices about what
		// to do here. We print a message
		System.err.println("FATAL ERROR: " + s);
		throw new Error(s); // Invoke the uncaught exception
		// handler System.exit() is another
		// choice.

	}
	
	public static void main(String[] args) {
		Frame test = new Frame(); 
        test.setLayout(new BorderLayout());
        Media media = new Media(Color.green);
        media.init(new String[] {"/windows/D/resource/video/bb.mpg"});
		test.add(media, BorderLayout.CENTER);
		test.addWindowListener(new WindowAdapter(){            
			public void windowClosing(WindowEvent e){                
					System.exit(1);            
			}        
		});    
        test.setSize(640,480);
        media.start();
        test.show();
		
	}
	*/
}