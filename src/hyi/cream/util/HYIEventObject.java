package hyi.cream.util;

public class HYIEventObject implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	protected Class eventClass;
	
	protected Object  source;
    
    protected int status;

    protected long sleepTimes;
    
    public HYIEventObject() {
    }

    public Object getSource() {
		return source;
	}

	public void setSource(Object source) {
		this.source = source;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

    public String toString() {
        return getClass().getName() + "[className=" + eventClass.getName() + ", source=" + source + "]";
    }

	public Class getEventClass() {
		return eventClass;
	}

	public void setEventClass(Class eventClass) {
		this.eventClass = eventClass;
	}

	public long getSleepTimes() {
		return sleepTimes;
	}

	public void setSleepTimes(long sleepTimes) {
		this.sleepTimes = sleepTimes;
	}
}
