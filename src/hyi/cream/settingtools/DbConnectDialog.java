package hyi.cream.settingtools;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;

public class DbConnectDialog {

    private static Display display;
    private Shell sShell = null;  //  @jve:decl-index=0:visual-constraint="10,10"
    //private CLabel posIpLabel = null;
    //private Text posIpText = null;
    private Label label = null;
    private Text userText = null;
    private Text passwordText = null;
    private Label label1 = null;
    private Button okButton = null;
    private Button cancelButton = null;
    private CLabel cLabel = null;
    private static boolean connectToPosOK;
    private Label errorHint = null;
    private boolean waitingThreadIsRunning;
    private Cursor waitCursor = new Cursor(Display.getCurrent(), SWT.CURSOR_WAIT);


    public static boolean open() {
        display = Display.getDefault();
        DbConnectDialog thisClass = new DbConnectDialog();
        thisClass.createSShell();
        Util.centerWindow(display, thisClass.sShell);
        thisClass.sShell.open();
        while (!thisClass.sShell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        return connectToPosOK;
    }

    /**
     * This method initializes sShell
     */
    private void createSShell() {
        GridData gridData9 = new org.eclipse.swt.layout.GridData();
        gridData9.horizontalSpan = 2;
        gridData9.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData9.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData9.horizontalIndent = 80;
        gridData9.grabExcessVerticalSpace = true;
        gridData9.grabExcessHorizontalSpace = true;
        GridData gridData8 = new org.eclipse.swt.layout.GridData();
        gridData8.widthHint = 100;
        gridData8.horizontalIndent = 0;
        GridData gridData7 = new org.eclipse.swt.layout.GridData();
        gridData7.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        gridData7.widthHint = 100;
        gridData7.grabExcessVerticalSpace = false;
        gridData7.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData6 = new org.eclipse.swt.layout.GridData();
        gridData6.widthHint = 100;
        gridData6.grabExcessHorizontalSpace = true;
        GridData gridData5 = new org.eclipse.swt.layout.GridData();
        gridData5.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        gridData5.grabExcessHorizontalSpace = true;
        gridData5.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData4 = new org.eclipse.swt.layout.GridData();
        gridData4.widthHint = 80;
        gridData4.grabExcessHorizontalSpace = true;
        GridData gridData3 = new org.eclipse.swt.layout.GridData();
        gridData3.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        gridData3.grabExcessHorizontalSpace = true;
        gridData3.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData2 = new org.eclipse.swt.layout.GridData();
        gridData2.grabExcessHorizontalSpace = true;
        gridData2.widthHint = 140;
        GridData gridData1 = new org.eclipse.swt.layout.GridData();
        gridData1.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        gridData1.grabExcessHorizontalSpace = true;
        gridData1.widthHint = -1;
        gridData1.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData = new org.eclipse.swt.layout.GridData();
        gridData.horizontalSpan = 2;
        gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData.horizontalIndent = 30;
        gridData.heightHint = 60;
        gridData.grabExcessHorizontalSpace = true;
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 2;
        gridLayout.verticalSpacing = 8;
        gridLayout.horizontalSpacing = 8;
        gridLayout.makeColumnsEqualWidth = true;
        sShell = new Shell(
            PosSettingApplication.getInstance().sShell, // !!!comment out this to enable VE editing
            SWT.APPLICATION_MODAL | SWT.DIALOG_TRIM);
        sShell.setText("Connecting to POS");
        sShell.setImage(new Image(Display.getCurrent(), getClass().getResourceAsStream(
            "/hyi/cream/settingtools/InlineIcon.ico")));
        sShell.setLayout(gridLayout);
        sShell.setSize(new org.eclipse.swt.graphics.Point(378,226));
        cLabel = new CLabel(sShell, SWT.NONE);
        //posIpLabel = new CLabel(sShell, SWT.NONE);
        //posIpLabel.setText("&POS地址:");  // Generated
        //posIpLabel.setLayoutData(gridData1);
        //posIpText = new Text(sShell, SWT.BORDER);
        //posIpText.setLayoutData(gridData2);
        label = new Label(sShell, SWT.NONE);
        label.setText("数据库用户名(&U):");
        label.setLayoutData(gridData3);
        userText = new Text(sShell, SWT.BORDER);
        userText.setLayoutData(gridData4);
        label1 = new Label(sShell, SWT.NONE);
        label1.setText("数据库密码(&W):");
        label1.setLayoutData(gridData5);
        passwordText = new Text(sShell, SWT.BORDER | SWT.PASSWORD);
        passwordText.setLayoutData(gridData6);
        errorHint = new Label(sShell, SWT.NONE);
        okButton = new Button(sShell, SWT.NONE);
        okButton.setText("连接");
        okButton.setLayoutData(gridData7);
        okButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                errorHint.setText("数据库连线中，请稍后……");
                sShell.setCursor(waitCursor);
                connectToPosOK = Util.openDbConnection(/*posIpText.getText(),*/ userText.getText(),
                    passwordText.getText());
                sShell.setCursor(null);
                if (connectToPosOK) {
                    errorHint.setText("");
                    sShell.dispose();
                    return;
                }
                
                errorHint.setText("无法连接数据库，请重新输入！");
                //posIpText.selectAll();
                //posIpText.setFocus();
                userText.selectAll();
                userText.setFocus();
                if (waitingThreadIsRunning)
                    return;

                // 让errorHint显示3秒种就消失
                new Thread() {
                    public void run() {
                        waitingThreadIsRunning = true;
                        try {
                            Thread.sleep(3000);
                            display.asyncExec(new Runnable() {
                                public void run() {
                                    if (!errorHint.isDisposed())
                                        errorHint.setText("");
                                }
                            });
                        } catch (InterruptedException e) {
                        }
                        waitingThreadIsRunning = false;
                    }
                }.start();
            }
        });
        sShell.setDefaultButton(okButton);
        sShell.addShellListener(new org.eclipse.swt.events.ShellAdapter() {
            public void shellClosed(org.eclipse.swt.events.ShellEvent e) {
                connectToPosOK = false;
                sShell.dispose();
            }
        });
        cancelButton = new Button(sShell, SWT.NONE);
        cancelButton.setText("取消");
        cancelButton.setLayoutData(gridData8);
        cancelButton
                .addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
                    public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                        connectToPosOK = false;
                        sShell.dispose();
                    }
                });
        cLabel.setImage(new Image(Display.getCurrent(), getClass().getResourceAsStream("/hyi/cream/settingtools/InlineIcon.gif")));
        cLabel.setLayoutData(gridData);
        cLabel.setText("数据库连线\n\n请输入数据库用户名密码");
        errorHint.setText("");
        errorHint.setLayoutData(gridData9);
        errorHint.setForeground(new Color(Display.getCurrent(), 255, 0, 0));
    }

    public static boolean isConnectToPosOK() {
        return connectToPosOK;
    }

    public static void setConnectToPosOK(boolean connectToPosOK) {
        DbConnectDialog.connectToPosOK = connectToPosOK;
    }
}
