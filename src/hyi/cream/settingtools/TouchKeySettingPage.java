package hyi.cream.settingtools;

import hyi.cream.settingtools.entity.ButtonLevel;
import hyi.cream.settingtools.entity.Category;
import hyi.cream.settingtools.entity.CategoryButton;
import hyi.cream.settingtools.entity.CategoryPreviewButton;
import hyi.cream.settingtools.entity.Item;
import hyi.cream.settingtools.entity.ItemButton;
import hyi.cream.settingtools.entity.Organization;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;


public class TouchKeySettingPage extends Composite {
    static private PosSettingApplication app = PosSettingApplication.getInstance(); 
    static private Cursor waitCursor = new Cursor(Display.getCurrent(), SWT.CURSOR_WAIT);
    static private Cursor handCursor = new Cursor(Display.getCurrent(), SWT.CURSOR_HAND);

    private SashForm sashForm = null;
    private Tree categoryButtonTree = null;
    private Composite ItemButtonComposite = null;
    private Composite itemListComposite = null;
    private Tree itemTree = null;
    private Composite categoryButtonComposite = null;
    private Group group1 = null;
    private Text itemListNoteTextArea = null;
    private Group group2 = null;
    private Label previewLabel = null;
    //private List<CategoryTouchButton> categoryButtonList;
    private Composite categoryButtonAddRemoveComposite = null;
    private Button addCategoryButton = null;
    private Button removeCategoryButton = null;
    private Group categoryEditAreaGroup = null;
    private CLabel categoryButtonTextLabel = null;
    private Text categoryButtonText = null;
    private Button categoryColorButton = null;
    private CLabel colorValueLabel = null;
    private CategoryPreviewButton categoryButtonPreviewLabel = null;
    protected CategoryButton currentCategoryButton;
    protected TreeItem currentCategoryButtonTreeItem;
    private Composite saveButtonComposite = null;
    private Button saveButton = null;
    private Button reloadButton = null;
    private Group itemButtonGroup = null;
    private Composite itemButtonComposite = null;
    private Composite pageUpDownComposite = null;
    private ItemButton[][] itemButtons = new ItemButton[4][4];
    private Group itemButtonEditAreaGroup = null;
    private CLabel itemButtonDescLabel = null;
    private Text itemButtonText = null;
    private Label itemNumberDescLabel = null;
    private Text itemNumberText = null;
    private Button itemButtonColorButton = null;
    private CLabel itemButtonColorLabel = null;
    private Button pageUpButton = null;
    private Button pageDownButton = null;
    private Button emptyItemButton = null;
    private Button removeItemButton = null;
    private Organization organization = new Organization(0, "", "");
    private Composite organizationComposite = null;
    private CLabel organizationLabel = null;
    private CLabel organizationInfoLabel = null;
    private Button changeOrganizationButton = null;
    private Button copyDataButton = null;

    private Transfer[] transferTypes = new Transfer[] {TextTransfer.getInstance()};
    private int dndOperations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK;
    private TreeItem[] draggedItemOrCategory = new TreeItem[1];
    private TreeItem[] draggedCategoryButton = new TreeItem[1];
    private ItemButton[] draggedItemButton = new ItemButton[1];
    private ItemButton currentItemButton;
    private boolean dirty;
    private boolean enableModifyDirty = true;;
    private int deleteRecordCount;
    private int insertRecordCount;
    private int updateFailedCount;

    /**
     * This method initializes sashForm 
     *
     */
    private void createSashForm() {
        GridData gridData14 = new org.eclipse.swt.layout.GridData();
        gridData14.grabExcessHorizontalSpace = true;
        gridData14.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData14.grabExcessVerticalSpace = true;
        gridData14.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        sashForm = new SashForm(this, SWT.NONE);
        sashForm.setBackground(new Color(Display.getCurrent(), 255, 255, 206));
        createItemListComposite();
        createComposite();
        createItemButtonComposite();
        sashForm.setLayoutData(gridData14);
        sashForm.setWeights(new int[] {30, 25, 45});
    }

    /**
     * This method initializes categoryButtonTree   
     *
     */
    private void createCategoryButtonTree() {
        GridData gridData2 = new org.eclipse.swt.layout.GridData();
        gridData2.horizontalSpan = 2;
        gridData2.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData2.grabExcessHorizontalSpace = true;
        gridData2.grabExcessVerticalSpace = true;
        gridData2.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        categoryButtonTree = new Tree(group2, SWT.NONE);
        categoryButtonTree.setFont(new Font(Display.getDefault(), "\u5b8b\u4f53", 11, SWT.NORMAL));
        categoryButtonTree.setLayoutData(gridData2);
        categoryButtonTree.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent e) {
                setEnableModifyDirty(false);   // don't change dirty when select category
                updateCurrentCategoryButtonTreeItem((TreeItem)e.item);
                setEnableModifyDirty(true);    
            }
            public void widgetDefaultSelected(SelectionEvent arg0) {
            }
        });
    }

    private void updateCurrentCategoryButtonTreeItem(TreeItem item) {
        currentCategoryButtonTreeItem = item;
        if (item != null)
            currentCategoryButton = (CategoryButton)item.getData();
        else
            currentCategoryButton = null;
        if (currentCategoryButton != null) {
            categoryButtonText.setText(currentCategoryButton.getName());
            colorValueLabel.setText(currentCategoryButton.getColorText());
            categoryButtonPreviewLabel.setText(formatToButtonText(currentCategoryButton.getName()));
            categoryButtonPreviewLabel.setBackground(new Color(null, currentCategoryButton.getColor()));
        } else {
            categoryButtonText.setText("");
            colorValueLabel.setText("");
            categoryButtonPreviewLabel.setText("");
            categoryButtonPreviewLabel.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));
        }
        updateItemButtonsFromCurrentCategoryButton();
        updateCurrentItemButton(null);
    }

    private void updateItemButtonsFromCurrentCategoryButton() {
        if (currentCategoryButton != null) {
            ButtonLevel currentButtonLevel = currentCategoryButton.getCurrentButtonLevel();
            if (currentButtonLevel != null) {
                Item[][] items = currentButtonLevel.getItems();
                for (int i = 0; i < items.length; i++)
                    for (int j = 0; j < items[i].length; j++) {
                        if (items[i][j] != null) {
                            itemButtons[i][j].setText(formatToButtonText(items[i][j].getName()));
                            itemButtons[i][j].setBackground(new Color(null, items[i][j].getColor()));
                            itemButtons[i][j].setData(items[i][j]);
                        } else {
                            itemButtons[i][j].setText("");
                            itemButtons[i][j].setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));
                            itemButtons[i][j].setData(null);
                        }
                    }
                return;
            } else {
                ButtonLevel bl = currentCategoryButton.createNewButtonLevel();
                currentCategoryButton.setCurrentButtonLevel(bl);
            }
        }
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++) {
                itemButtons[i][j].setText("");
                itemButtons[i][j].setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));
                itemButtons[i][j].setData(null);
            }
        
    }

    protected String formatToButtonText(String name) {
        return name.replaceAll("\\^", "\n");
    }

    /**
     * This method initializes ItemButtonComposite  
     *
     */
    private void createItemButtonComposite() {
        GridLayout gridLayout5 = new GridLayout();
        gridLayout5.numColumns = 1;
        ItemButtonComposite = new Composite(sashForm, SWT.NONE);
        ItemButtonComposite.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
        createItemButtonGroup();
        ItemButtonComposite.setLayout(gridLayout5);
    }

    /**
     * This method initializes itemListComposite    
     *
     */
    private void createItemListComposite() {
        itemListComposite = new Composite(sashForm, SWT.NONE);
        itemListComposite.setLayout(new GridLayout());
        createGroup1();
    }

    /**
     * This method initializes itemTree 
     *
     */
    private void createItemTree() {
        GridData gridData1 = new org.eclipse.swt.layout.GridData();
        gridData1.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData1.grabExcessHorizontalSpace = true;
        gridData1.grabExcessVerticalSpace = true;
        gridData1.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
        itemTree = new Tree(group1, SWT.NONE);
        itemTree.setLayoutData(gridData1);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        /* Before this is run, be sure to set up the launch configuration (Arguments->VM Arguments)
         * for the correct SWT library path in order to run with the SWT dlls. 
         * The dlls are located in the SWT plugin jar.  
         * For example, on Windows the Eclipse SWT 3.1 plugin jar is:
         *       installation_directory\plugins\org.eclipse.swt.win32_3.1.0.jar
         */
        Display display = Display.getDefault();
        Shell shell = new Shell(display);
        shell.setLayout(new FillLayout());
        shell.setSize(new Point(300, 200));
        TouchKeySettingPage thisClass = new TouchKeySettingPage(shell, SWT.NONE);
        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        display.dispose();
    }

    public TouchKeySettingPage(Composite parent, int style) {
        super(parent, style);
        initialize();
        furtherInitialize();
    }

    private void initialize() {
        this.setLayout(new GridLayout());
        createOrganizationComposite();
        createSashForm();
        createSaveButtonComposite();
        setSize(new org.eclipse.swt.graphics.Point(796,386));
    }

    private void furtherInitialize() {
        setupDragAndDrop();

        if (NamedQuery.isRunAtStore()) {
            organizationComposite.setVisible(false);
            organizationComposite.dispose();
            copyDataButton.setVisible(false);
        }
    }

    public void loadButtonData() {
        fillDataIntoItemTree();
        fillDataIntoCategoryButtonTree();
        setDirty(false);
    }

    private void setupDragAndDrop() {
        setupItemTreeAsDragSource();
        setupCategoryButtonTreeAsDragSource();
        setupCategoryButtonTreeAsDropTarget();
        setupItemButtonAsDragSource();
        setupItemButtonAsDropTarget();
    }

    private void setupItemButtonAsDragSource() {
        for (int i = 0; i < this.itemButtons.length; i++) {
            for (int j = 0; j < this.itemButtons[i].length; j++) {
                final ItemButton itemButton = itemButtons[i][j];
                final DragSource source = new DragSource(itemButton, dndOperations);
                source.setTransfer(transferTypes);
                source.addDragListener (new DragSourceListener () {
                    public void dragStart(DragSourceEvent event) {
                        Item item = (Item)itemButton.getData();
                        if (item == null || 
                            (item.getId().trim().length() == 0 && item.getName().trim().length() == 0)) {
                            event.doit = false;
                        } else {
                            event.doit = true;
                            draggedItemButton[0] = itemButton;
                        }
                    }
                    public void dragSetData (DragSourceEvent event) {
                        event.data = draggedItemButton[0].getText(); // no use
                    }
                    public void dragFinished(DragSourceEvent event) {
                        draggedItemButton[0] = null;
                    }
                });
            }
        }
    }

    private void setupItemButtonAsDropTarget() {
        for (int i = 0; i < this.itemButtons.length; i++) {
            for (int j = 0; j < this.itemButtons[i].length; j++) {
                final ItemButton targetItemButton = itemButtons[i][j];
                DropTarget target = new DropTarget(targetItemButton, dndOperations);
                target.setTransfer(transferTypes);
                target.addDropListener (new DropTargetAdapter() {
                    public void drop(DropTargetEvent event) {
                        if (event.data == null) {
                            event.detail = DND.DROP_NONE;
                            return;
                        }
                        if (currentCategoryButton == null)
                            return;

                        // Determine dragSource...
                        if (draggedCategoryButton[0] != null)
                            return;
                        else if (draggedItemOrCategory[0] != null) {
                            Object obj = draggedItemOrCategory[0].getData();
                            if (obj instanceof Category)
                                return;
                            else if (obj instanceof Item) {
                                Item draggedItem = (Item)obj;

                                // use category button's color 
                                draggedItem.setColor(currentCategoryButton.getColor());
                                targetItemButton.setBackground(new Color(null, currentCategoryButton.getColor()));

                                currentCategoryButton.insertItemToCurrentButtonLevel(
                                    targetItemButton.getRow(), targetItemButton.getCol(), draggedItem);
                                setDirty(true);
                                updateItemButtonsFromCurrentCategoryButton();
                                updateCurrentItemButton(targetItemButton);
                                return;
                            }
                        } else if (draggedItemButton[0] != null) {
                            // targetItemButton <- draggedItemButton[0]
                            ItemButton fromButton = draggedItemButton[0];
                            currentCategoryButton.moveItemButton(fromButton.getRow(),
                                fromButton.getCol(),
                                targetItemButton.getRow(),
                                targetItemButton.getCol());
                            setDirty(true);
                            updateItemButtonsFromCurrentCategoryButton();
                            updateCurrentItemButton(targetItemButton);
                        }
                    }
                });
            }
        }
    }

    private void setupCategoryButtonTreeAsDropTarget() {
        DropTarget target = new DropTarget(categoryButtonTree, dndOperations);
        target.setTransfer(transferTypes);
        target.addDropListener (new DropTargetAdapter() {
            public void dragOver(DropTargetEvent event) {
                event.feedback = DND.FEEDBACK_EXPAND | DND.FEEDBACK_SCROLL;
                if (event.item != null) {
                    TreeItem item = (TreeItem)event.item;
                    Point pt = Display.getCurrent().map(null, categoryButtonTree, event.x, event.y);
                    Rectangle bounds = item.getBounds();
                    if (pt.y < bounds.y + bounds.height/3) {
                        event.feedback |= DND.FEEDBACK_INSERT_BEFORE;
                    } else if (pt.y > bounds.y + 2*bounds.height/3) {
                        event.feedback |= DND.FEEDBACK_INSERT_AFTER;
                    } else {
                        if (draggedItemOrCategory[0] != null)
                            event.feedback |= DND.FEEDBACK_SELECT;
                        else
                            event.feedback |= DND.FEEDBACK_NONE;
                    }
                } else {
                    event.feedback |= DND.FEEDBACK_INSERT_AFTER;
                }
            }

            public void drop(DropTargetEvent event) {
                if (event.data == null) {
                    event.detail = DND.DROP_NONE;
                    return;
                }

                // Determine dragSource...
                CategoryButton draggedCatButton = null;
                Category draggedCategory = null;
                Item draggedItem = null;
                if (draggedCategoryButton[0] != null)
                    draggedCatButton = (CategoryButton)draggedCategoryButton[0].getData();
                else if (draggedItemOrCategory[0] != null) {
                    Object obj = draggedItemOrCategory[0].getData();
                    if (obj instanceof Category)
                        draggedCategory = (Category)obj;
                    else if (obj instanceof Item)
                        draggedItem = (Item)obj;
                }

                TreeItem droppedCategoryButtonTreeItem = (TreeItem)event.item;
                if (droppedCategoryButtonTreeItem == null) {  // if drop to an empty area

                    if (draggedCatButton != null) {     // if dragging a category button
                        TreeItem newItem = new TreeItem(categoryButtonTree, SWT.NONE);
                        associateTreeItemAndCategoryButton(newItem, draggedCatButton);
                        draggedCategoryButton[0].dispose();
                        setDirty(true);
                    } else if (draggedCategory != null) {   // if dragging a category
                        buildCategoryButtonFromCategory(draggedCategory, -1);
                        setDirty(true);
                    } else if (draggedItem != null) {       // if dragging a item
                        // not allowed, ignore...
                    }

                } else {    // if drop onto a category button

                    Point pt = Display.getCurrent().map(null, categoryButtonTree, event.x, event.y);
                    Rectangle bounds = droppedCategoryButtonTreeItem.getBounds();
                    TreeItem parent = droppedCategoryButtonTreeItem.getParentItem();

                    if (parent != null) {       // impossible in our case, just for safe
                        TreeItem[] items = parent.getItems();
                        int index = 0;
                        for (int i = 0; i < items.length; i++) {
                            if (items[i] == droppedCategoryButtonTreeItem) {
                                index = i;
                                break;
                            }
                        }
                        if (pt.y < bounds.y + bounds.height/3) {
                            TreeItem newItem = new TreeItem(parent, SWT.NONE, index);
                            associateTreeItemAndCategoryButton(newItem, draggedCatButton);
                            draggedCategoryButton[0].dispose();
                            setDirty(true);
                        } else if (pt.y > bounds.y + 2*bounds.height/3) {
                            TreeItem newItem = new TreeItem(parent, SWT.NONE, index+1);
                            associateTreeItemAndCategoryButton(newItem, draggedCatButton);
                            draggedCategoryButton[0].dispose();
                            setDirty(true);
                        }

                    } else {

                        // Find the dropped TreeItem's position in Tree
                        TreeItem[] items = categoryButtonTree.getItems();
                        int index = 0;
                        for (int i = 0; i < items.length; i++) {
                            if (items[i] == droppedCategoryButtonTreeItem) {
                                index = i;
                                break;
                            }
                        }

                        if (pt.y < bounds.y + bounds.height/3) {    // insert before

                            if (draggedCatButton != null) {         // if dragging a category button
                                TreeItem newItem = new TreeItem(categoryButtonTree, SWT.NONE, index);
                                associateTreeItemAndCategoryButton(newItem, draggedCatButton);
                                draggedCategoryButton[0].dispose();
                                setDirty(true);
                            } else if (draggedCategory != null) {   // if dragging a category
                                buildCategoryButtonFromCategory(draggedCategory, index);
                                setDirty(true);
                            } else if (draggedItem != null) {       // if dragging a item
                                // not allowed, ignore...
                            }

                        } else if (pt.y > bounds.y + 2*bounds.height/3) {   // insert after

                            if (draggedCatButton != null) {         // if dragging a category button
                                TreeItem newItem = new TreeItem(categoryButtonTree, SWT.NONE, index + 1);
                                associateTreeItemAndCategoryButton(newItem, draggedCatButton);
                                draggedCategoryButton[0].dispose();
                                setDirty(true);
                            } else if (draggedCategory != null) {   // if dragging a category
                                buildCategoryButtonFromCategory(draggedCategory, index + 1);
                                setDirty(true);
                            } else if (draggedItem != null) {       // if dragging a item
                                // not allowed, ignore...
                            }
                            
                        } else {  // if drop right onto a category button  
                            
                            if (draggedCatButton != null) {         // if dragging a category button
                                // do nothing...
                            } else if (draggedCategory != null) {   // if dragging a category
                                MessageBox mb = new MessageBox(getShell(), SWT.ICON_INFORMATION
                                    | SWT.YES | SWT.NO | SWT.CANCEL);
                                mb.setText("变更分类按键 “" + droppedCategoryButtonTreeItem.getText() + "”");
                                mb.setMessage("选择 “是” 将覆盖原分类按键成为 “" + draggedCategory.getName() 
                                    + "”。选择 “否” 将 “" + draggedCategory.getName() + "” 中的商品添加到原分类按键。");
                                int ret = mb.open();
                                if (ret == SWT.YES) {
                                    overwriteCategoryButtonFromCategory(droppedCategoryButtonTreeItem,
                                        draggedCategory);
                                    setDirty(true);
                                } else if (ret == SWT.NO) {
                                    appendCategoryButtonFromCategory(droppedCategoryButtonTreeItem,
                                        draggedCategory);
                                    setDirty(true);
                                }

                            } else if (draggedItem != null) {       // if dragging a item
                                // not allowed, ignore...
                            }
                        }
                    }
                }
            }
        });
    }

    private void buildCategoryButtonFromCategory(Category category, int posInCategoryButtonTree) {
        TreeItem newItem;
        if (posInCategoryButtonTree >= 0)
            newItem = new TreeItem(categoryButtonTree, SWT.NONE, posInCategoryButtonTree);
        else
            newItem = new TreeItem(categoryButtonTree, SWT.NONE);
        CategoryButton newCatButton = new CategoryButton();
        newCatButton.setId(CategoryButton.getCurrentMaxId() + 1);
        newCatButton.setName(category.getName());
        newCatButton.setColor(getRandomColor());    // give it a random color
        associateTreeItemAndCategoryButton(newItem, newCatButton);
        updateCurrentCategoryButtonTreeItem(newItem);

        newCatButton.populateItems(category.getItemList());
        updateItemButtonsFromCurrentCategoryButton();
        updateCurrentItemButton(null);
    }

    private void overwriteCategoryButtonFromCategory(TreeItem categoryTreeItem, Category category) {
        CategoryButton catButton = (CategoryButton)categoryTreeItem.getData();
        //catButton.setId(CategoryButton.getCurrentMaxId() + 1);
        catButton.setName(category.getName());
        //catButton.setColor(getRandomColor());    // don't change original color
        associateTreeItemAndCategoryButton(categoryTreeItem, catButton);
        updateCurrentCategoryButtonTreeItem(categoryTreeItem);

        catButton.populateItems(category.getItemList());
        updateItemButtonsFromCurrentCategoryButton();
        updateCurrentItemButton(null);
    }

    private void appendCategoryButtonFromCategory(TreeItem categoryTreeItem, Category category) {
        CategoryButton catButton = (CategoryButton)categoryTreeItem.getData();
        associateTreeItemAndCategoryButton(categoryTreeItem, catButton);
        updateCurrentCategoryButtonTreeItem(categoryTreeItem);

        catButton.populateItemsByAppending(category.getItemList());
        updateItemButtonsFromCurrentCategoryButton();
        updateCurrentItemButton(null);
    }

    private void setupCategoryButtonTreeAsDragSource() {
        final DragSource source = new DragSource(categoryButtonTree, dndOperations);
        source.setTransfer(transferTypes);
        source.addDragListener (new DragSourceListener () {
            public void dragStart(DragSourceEvent event) {
                TreeItem[] selection = categoryButtonTree.getSelection();
                if (selection.length > 0 && selection[0].getItemCount() == 0) {
                    event.doit = true;
                    draggedCategoryButton[0] = selection[0];
                    // So, if draggedCategories[0] is not null, it means that we're
                    // now dragging a category button from the categoryButtonTree. 
                } else {
                    event.doit = false;
                }
            };
            public void dragSetData (DragSourceEvent event) {
                event.data = draggedCategoryButton[0].getText();
            }
            public void dragFinished(DragSourceEvent event) {
                //if (event.detail == DND.DROP_MOVE)
                //    dragSourceItem[0].dispose();
                draggedCategoryButton[0] = null;
            }
        });
    }

    private void setupItemTreeAsDragSource() {
        final DragSource source = new DragSource(itemTree, dndOperations);
        source.setTransfer(transferTypes);
        source.addDragListener (new DragSourceListener () {
            public void dragStart(DragSourceEvent event) {
                TreeItem[] selection = itemTree.getSelection();
                if (selection.length > 0) {
                    event.doit = true;
                    draggedItemOrCategory[0] = selection[0];
                    // So, if draggedItems[0] is not null, it means that we're
                    // now dragging a category or item from the leftmost itemTree. 
                } else {
                    event.doit = false;
                }
            };
            public void dragSetData (DragSourceEvent event) {
                event.data = draggedItemOrCategory[0].getText();
            }
            public void dragFinished(DragSourceEvent event) {
                draggedItemOrCategory[0] = null;
            }
        });
    }

    private void associateTreeItemAndCategoryButton(TreeItem treeItem, CategoryButton catButton) {
        treeItem.setData(catButton);
        treeItem.setText(catButton.toString());
        treeItem.setBackground(new Color(null, catButton.getColor()));
        currentCategoryButton = catButton;
        currentCategoryButtonTreeItem = treeItem;
        categoryButtonTree.setSelection(new TreeItem[] {treeItem});
    }
    
    private boolean fillDataIntoCategoryButtonTree() {
        try {
            //categoryButtonList = new ArrayList<CategoryTouchButton>();
            //ResultSet rs = Util.query("SELECT row,label,red,green,blue,arg1 FROM buttonsetting"
            //    + " WHERE level=500 AND row!=100 ORDER BY row");
            ResultSet rs;
            if (NamedQuery.isRunAtStore())
                rs = Util.queryByNamedQuery("buttonsetting.AllCategoryButtons");
            else
                rs = Util.queryByNamedQuery("buttonsetting.AllCategoryButtons", organization.getOrgID());
            if (rs == null)
                return false;
            while (rs.next()) {
                CategoryButton catButton = new CategoryButton();
                catButton.setId(rs.getInt(1));
                catButton.setName(rs.getString(2));
                catButton.setColor(new RGB(rs.getInt(3), rs.getInt(4), rs.getInt(5)));
                catButton.setItemPageStartIndex(rs.getInt(6));
                //categoryButtonList.add(catButton);
                catButton.loadItemButtonData(organization.getOrgID());

                TreeItem catButtonTreeItem = new TreeItem(categoryButtonTree, SWT.NONE);
                catButtonTreeItem.setText(catButton.toString());
                catButtonTreeItem.setData(catButton);
                catButtonTreeItem.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_BLUE));
                catButtonTreeItem.setBackground(new Color(null, catButton.getColor()));
            }
            rs.close();
            //rs.getStatement().close();
            return true;
        } catch (SQLException e) {
            app.showExceptionInfo("数据读取错误", "buttonsetting表读取错误", e);
            return false;
        }
    }

    private boolean fillDataIntoItemTree() {
        try {
            List<Category> categoryList = new ArrayList<Category>();
            //ResultSet rs = Util.query("SELECT SUBSTRING(GrpDepNo,1,2),GrpDepName FROM category");
            ResultSet rs = Util.queryByNamedQuery("category.AllCategoryIDs");
            if (rs == null)
                return false;
            while (rs.next()) {
                Category cat = new Category();
                cat.setId(rs.getString(1));
                cat.setName(rs.getString(2));
                categoryList.add(cat);
            }
            rs.close();
            //rs.getStatement().close();
            
            for (Category cat : categoryList) {
                TreeItem catTreeItem = new TreeItem(itemTree, SWT.NONE);
                catTreeItem.setText(cat.toString());
                catTreeItem.setData(cat);

                //rs = Util.query("SELECT ItemNo,ScreenName FROM plu WHERE GrpNo='" + cat.getId() + "'");
                //Bruce/20060518
                if (NamedQuery.isRunAtStore())
                    rs = Util.queryByNamedQuery("plu.AllItemsInGivenCategory", cat.getId());
                else
                    rs = Util.queryByNamedQuery("plu.AllItemsInGivenCategory", cat.getId(), organization.getOrgID());

                while (rs != null && rs.next()) {
                    Item item = new Item(rs.getString(1),   // id
                        rs.getString(2),                    // name
                        new RGB(0, 0, 0));                  // color

                    TreeItem itemTreeItem = new TreeItem(catTreeItem, SWT.NONE);
                    itemTreeItem.setText(item.toString());
                    itemTreeItem.setData(item);
                    cat.addItem(item);
                }
                rs.close();
                //rs.getStatement().close();
            }
            return true;
        } catch (SQLException e) {
            app.showExceptionInfo("数据读取错误", "category或plu表读取错误", e);
            return false;
        }
    }

    /**
     * This method initializes composite    
     *
     */
    private void createComposite() {
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 1;
        categoryButtonComposite = new Composite(sashForm, SWT.NONE);
        createGroup2();
        categoryButtonComposite.setLayout(gridLayout);
    }

    /**
     * This method initializes group1   
     *
     */
    private void createGroup1() {
        GridData gridData8 = new org.eclipse.swt.layout.GridData();
        gridData8.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData8.grabExcessHorizontalSpace = true;
        gridData8.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData = new org.eclipse.swt.layout.GridData();
        gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData.grabExcessHorizontalSpace = true;
        gridData.grabExcessVerticalSpace = true;
        gridData.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
        group1 = new Group(itemListComposite, SWT.NONE);
        group1.setText("分类与商品清单");
        group1.setLayout(new GridLayout());
        group1.setLayoutData(gridData);
        createItemTree();
        itemListNoteTextArea = new Text(group1, SWT.MULTI | SWT.WRAP);
        itemListNoteTextArea.setText("操作说明:\n"
            + "1.可点击并拖拉一个分类到右边的分类按键中。\n"
            + "2.可点击并拖拉一个商品到最右边的单品按键的任意位置。");
        itemListNoteTextArea.setBackground(new Color(Display.getCurrent(), 255, 255, 228));
        itemListNoteTextArea.setLayoutData(gridData8);
    }

    /**
     * This method initializes group2   
     *
     */
    private void createGroup2() {
        GridData gridData13 = new org.eclipse.swt.layout.GridData();
        gridData13.grabExcessHorizontalSpace = true;
        gridData13.heightHint = 56;
        gridData13.widthHint = 56;
        GridData gridData10 = new org.eclipse.swt.layout.GridData();
        gridData10.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        gridData10.grabExcessHorizontalSpace = true;
        gridData10.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData3 = new org.eclipse.swt.layout.GridData();
        gridData3.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData3.grabExcessHorizontalSpace = true;
        gridData3.grabExcessVerticalSpace = true;
        gridData3.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
        GridLayout gridLayout1 = new GridLayout();
        gridLayout1.numColumns = 2;
        group2 = new Group(categoryButtonComposite, SWT.NONE);
        group2.setText("分类按键列");
        group2.setLayout(gridLayout1);
        group2.setLayoutData(gridData3);
        createCategoryButtonTree();
        createComposite2();
        previewLabel = new Label(group2, SWT.NONE);
        previewLabel.setText("按键预览:");
        previewLabel.setLayoutData(gridData10);
        categoryButtonPreviewLabel = new CategoryPreviewButton(group2, SWT.CENTER);
        categoryButtonPreviewLabel.setText("");
        categoryButtonPreviewLabel.setBackground(new Color(Display.getCurrent(), 51, 102, 240));
        categoryButtonPreviewLabel.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_BLUE));
        categoryButtonPreviewLabel.setFont(new Font(Display.getDefault(), "\u5b8b\u4f53", 11, SWT.NORMAL));
        categoryButtonPreviewLabel.setLayoutData(gridData13);
        createGroup();
    }

    /**
     * This method initializes composite    
     *
     */
    private void createComposite2() {
        GridData gridData9 = new org.eclipse.swt.layout.GridData();
        gridData9.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData9.grabExcessHorizontalSpace = true;
        gridData9.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData7 = new org.eclipse.swt.layout.GridData();
        gridData7.grabExcessHorizontalSpace = true;
        gridData7.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData7.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        GridLayout gridLayout2 = new GridLayout();
        gridLayout2.numColumns = 2;
        GridData gridData11 = new org.eclipse.swt.layout.GridData();
        gridData11.horizontalSpan = 2;
        gridData11.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData11.grabExcessHorizontalSpace = true;
        gridData11.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        categoryButtonAddRemoveComposite = new Composite(group2, SWT.NONE);
        categoryButtonAddRemoveComposite.setLayoutData(gridData11);
        categoryButtonAddRemoveComposite.setLayout(gridLayout2);
        addCategoryButton = new Button(categoryButtonAddRemoveComposite, SWT.NONE);
        addCategoryButton.setText("新增");
        addCategoryButton.setLayoutData(gridData9);
        addCategoryButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                CategoryButton newCatButton = new CategoryButton();
                newCatButton.setId(CategoryButton.getCurrentMaxId() + 1);
                newCatButton.setName("新增分类钮");
                newCatButton.setColor(getRandomColor());    // give it a random color
                TreeItem newItem = new TreeItem(categoryButtonTree, SWT.NONE);
                associateTreeItemAndCategoryButton(newItem, newCatButton);
                updateCurrentCategoryButtonTreeItem(newItem);
                setDirty(true);
            }
        });
        removeCategoryButton = new Button(categoryButtonAddRemoveComposite, SWT.NONE);
        removeCategoryButton.setText("删除");
        removeCategoryButton.setLayoutData(gridData7);
        removeCategoryButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                if (currentCategoryButtonTreeItem != null) {
                    MessageBox mb = new MessageBox(getShell(), SWT.ICON_WARNING
                        | SWT.OK | SWT.CANCEL);
                    mb.setText("删除分类");
                    mb.setMessage("确定删除分类按键: " + currentCategoryButton.toString() + "?");
                    if (mb.open() == SWT.OK) {
                        currentCategoryButtonTreeItem.dispose();
                        currentCategoryButtonTreeItem = null;
                        currentCategoryButton = null;
                        setDirty(true);
                    }
                }
            }
        });
    }

    /**
     * This method initializes group    
     *
     */
    private void createGroup() {
        GridData gridData6 = new org.eclipse.swt.layout.GridData();
        gridData6.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData6.grabExcessHorizontalSpace = true;
        gridData6.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData5 = new org.eclipse.swt.layout.GridData();
        gridData5.horizontalSpan = 2;
        GridLayout gridLayout3 = new GridLayout();
        gridLayout3.numColumns = 2;
        GridData gridData4 = new org.eclipse.swt.layout.GridData();
        gridData4.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData4.grabExcessHorizontalSpace = true;
        gridData4.horizontalSpan = 2;
        gridData4.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData12 = new org.eclipse.swt.layout.GridData();
        gridData12.horizontalSpan = 2;
        gridData12.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData12.grabExcessHorizontalSpace = true;
        gridData12.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        categoryEditAreaGroup = new Group(group2, SWT.NONE);
        categoryEditAreaGroup.setText("分类按键编辑区");
        categoryEditAreaGroup.setLayout(gridLayout3);
        categoryEditAreaGroup.setLayoutData(gridData12);
        categoryButtonTextLabel = new CLabel(categoryEditAreaGroup, SWT.NONE);
        categoryButtonTextLabel.setText("分类按键显示文字:\n(可以用“^”符号折行)");
        categoryButtonTextLabel.setLayoutData(gridData5);
        categoryButtonText = new Text(categoryEditAreaGroup, SWT.BORDER);
        categoryButtonText.setLayoutData(gridData4);
        categoryButtonText.setTextLimit(30);
        categoryButtonText.addModifyListener(new org.eclipse.swt.events.ModifyListener() {
            public void modifyText(org.eclipse.swt.events.ModifyEvent e) {
                if (currentCategoryButton != null) {
                    String s = ((Text)e.widget).getText();
                    currentCategoryButton.setName(s);
                    currentCategoryButtonTreeItem.setText(s);
                    categoryButtonPreviewLabel.setText(formatToButtonText(s));
                    setDirty(true);
                }
            }
        });
        categoryColorButton = new Button(categoryEditAreaGroup, SWT.NONE);
        categoryColorButton.setText("颜色...");
        colorValueLabel = new CLabel(categoryEditAreaGroup, SWT.NONE);
        colorValueLabel.setText("");
        colorValueLabel.setLayoutData(gridData6);
        categoryColorButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                
                CenteringColorDialog colorDg = new CenteringColorDialog(getShell());
                RGB colorChose = colorDg.open();
                if (colorChose != null && currentCategoryButton != null) {
                    currentCategoryButton.setColor(colorChose);
                    Color c = new Color(null, currentCategoryButton.getColor());
                    currentCategoryButtonTreeItem.setBackground(c);
                    categoryButtonPreviewLabel.setBackground(c);
                    setDirty(true);
                }
            }
        });
    }

    private RGB getRandomColor() {
        return new RGB(new Random().nextInt(256 - 130) + 130,
            new Random().nextInt(256 - 130) + 130,
            new Random().nextInt(256 - 130) + 130);   
    }

    /**
     * This method initializes saveButtonComposite  
     *
     */
    private void createSaveButtonComposite() {
        GridData gridData17 = new org.eclipse.swt.layout.GridData();
        gridData17.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        gridData17.grabExcessHorizontalSpace = true;
        gridData17.widthHint = 180;
        gridData17.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData16 = new org.eclipse.swt.layout.GridData();
        gridData16.horizontalAlignment = org.eclipse.swt.layout.GridData.BEGINNING;
        gridData16.grabExcessHorizontalSpace = true;
        gridData16.horizontalIndent = 10;
        gridData16.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridLayout gridLayout4 = new GridLayout();
        gridLayout4.numColumns = 3;
        GridData gridData15 = new org.eclipse.swt.layout.GridData();
        gridData15.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData15.grabExcessHorizontalSpace = true;
        gridData15.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        saveButtonComposite = new Composite(this, SWT.NONE);
        saveButtonComposite.setLayoutData(gridData15);
        saveButtonComposite.setLayout(gridLayout4);
        copyDataButton = new Button(saveButtonComposite, SWT.NONE);
        copyDataButton.setText("复制其他门市按键配置...");
        copyDataButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                Organization origOrg = organization;
                Organization selectedOrg = StoreSelectionDialog.open("选择要复制其按键配置的组织或门市");
                if (selectedOrg != null && !origOrg.equals(selectedOrg)) {
                    clearAllButtonData();   
                    setOrganization(selectedOrg);
                    loadButtonData();   // load selected organization's data first
                    setOrganization(origOrg);   // set back to original organization
                    setDirty(true);
                }
            }
        });
        saveButton = new Button(saveButtonComposite, SWT.NONE);
        saveButton.setText("保存变更后的按键配置");
        saveButton.setLayoutData(gridData17);
        saveButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                setCursor(waitCursor);
                saveButtonSettings();
                setCursor(null);
                MessageBox mb = new MessageBox(getShell(), SWT.ICON_INFORMATION | SWT.YES | SWT.NO);
                mb.setText("按键更新信息");
                mb.setMessage("删除记录数: " + deleteRecordCount + "\n"
                    + "插入记录数: " + insertRecordCount + "\n"
                    + "更新失败记录数: " + updateFailedCount + "              \n\n"
                    + "您要查看详细更新日志吗？"); 
                if (mb.open() == SWT.YES)
                    UpdateLogDialog.open();
                setDirty(false);
            }
        });
        reloadButton = new Button(saveButtonComposite, SWT.NONE);
        reloadButton.setText("放弃变更配置，重新读入原有配置");
        reloadButton.setLayoutData(gridData16);
        reloadButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                MessageBox mb = new MessageBox(getShell(), SWT.ICON_INFORMATION
                    | SWT.OK | SWT.CANCEL);
                mb.setText("放弃变更配置");
                mb.setMessage("是否放弃 “" + organization.toString() + "” 的按键变更配置，并重新从数据库读入？"); 
                int ret = mb.open();
                if (ret == SWT.OK) {
                    setCursor(waitCursor);
                    clearAllButtonData();
                    loadButtonData();
                    setDirty(false);
                    setCursor(null);
                }
            }
        });
    }

    private void resetRecordCounter() {
        deleteRecordCount = insertRecordCount = updateFailedCount = 0;
    }

    private void countDelete(int updateCount) {
        if (updateCount == -1)
            updateFailedCount++;
        else
            deleteRecordCount += updateCount;
    }

    private void countInsert(int updateCount) {
        if (updateCount == -1)
            updateFailedCount++;
        else
            insertRecordCount += updateCount;
    }

    protected void saveButtonSettings() {
        try {
            resetRecordCounter();
            UpdateLogDialog.logMessage.setLength(0);

            // First delete all button data of the selected organization
            if (NamedQuery.isRunAtStore())
                countDelete(Util.execUpdateByNamedQuery("buttonsetting.DeleteTouchButtonData"));
            else
                countDelete(Util.execUpdateByNamedQuery("buttonsetting.DeleteTouchButtonData",
                    organization.getOrgID()));
    
            PreparedStatement ps = Util.getPreparedStatement("buttonsetting.InsertTouchButtonData");
            if (ps == null)
                return;

            if (NamedQuery.isRunAtStore())
                countInsert(Util.execUpdate(ps, 0, 500, 100, 100, "MENU", "", "",
                    0, 0, 0, "", "", "", "", "", "", "", "", "", "", 5));
            else
                countInsert(Util.execUpdate(ps, organization.getOrgID(), 0, 500, 100, 100, "MENU", "", "",
                    0, 0, 0, "", "", "", "", "", "", "", "", "", "", 5));
    
            TreeItem[] categoryButtonTreeItems = categoryButtonTree.getItems();
            int currentCatButtonIdx = 0;
            int currentLevelIdx = 140;
            for (TreeItem categoryButtonTreeItem : categoryButtonTreeItems) {
                CategoryButton catButton = (CategoryButton)categoryButtonTreeItem.getData();
                if (catButton == null)
                    continue;   // for safe
    
                if (NamedQuery.isRunAtStore())
                    countInsert(Util.execUpdate(ps, 0, 500, 0, currentCatButtonIdx++,
                        "PluMenuButton", "s", catButton.getName().trim(), catButton.getColor().red,
                        catButton.getColor().green, catButton.getColor().blue, currentLevelIdx,
                        "", "", "", "", "", "", "", "", "", 11));
                else
                    countInsert(Util.execUpdate(ps, organization.getOrgID(), 0, 500, 0, currentCatButtonIdx++,
                        "PluMenuButton", "s", catButton.getName().trim(), catButton.getColor().red,
                        catButton.getColor().green, catButton.getColor().blue, currentLevelIdx,
                        "", "", "", "", "", "", "", "", "", 11));
                
                for (ButtonLevel buttonLevel : catButton.getButtonLevels()) {
    
                    if (NamedQuery.isRunAtStore())
                        countInsert(Util.execUpdate(ps, 0, currentLevelIdx, 100, 100,
                            catButton.getName().trim().replaceAll("\\^", ""),
                            "", "", 0, 0, 0, "", "", "", "", "", "", "", "", "", "", 5));
                    else
                        countInsert(Util.execUpdate(ps, organization.getOrgID(), 0, currentLevelIdx, 100, 100,
                            catButton.getName().trim().replaceAll("\\^", ""),
                            "", "", 0, 0, 0, "", "", "", "", "", "", "", "", "", "", 5));
    
                    Item[][] items = buttonLevel.getItems();
                    for (int r = 0; r < items.length; r++) {
                        for (int c = 0; c < items[r].length; c++) {
                            Item item = items[r][c];
                            if (item != null && !item.isEmpty()) {
                                if (NamedQuery.isRunAtStore())
                                    countInsert(Util.execUpdate(ps, 0, currentLevelIdx,
                                        c, r, "PluButton", "s", item.getName(), item.getColor().red,
                                        item.getColor().green, item.getColor().blue, item.getId(),
                                        "", "", "", "", "", "", "", "", "", 11));
                                else
                                    countInsert(Util.execUpdate(ps, organization.getOrgID(), 0, currentLevelIdx,
                                        c, r, "PluButton", "s", item.getName(), item.getColor().red,
                                        item.getColor().green, item.getColor().blue, item.getId(),
                                        "", "", "", "", "", "", "", "", "", 11));
                            }
                        }
                    }
                    currentLevelIdx++;
                }
            }
            ps.close();
        } catch (SQLException e) {
        } finally {
            saveButton.setEnabled(false);
            reloadButton.setEnabled(false);
            UpdateLogDialog.logMessage.append("更新完毕。");
        }
    }

    /**
     * This method initializes itemButtonGroup  
     *
     */
    private void createItemButtonGroup() {
        GridData gridData18 = new org.eclipse.swt.layout.GridData();
        gridData18.grabExcessHorizontalSpace = true;
        gridData18.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData18.grabExcessVerticalSpace = true;
        gridData18.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        itemButtonGroup = new Group(ItemButtonComposite, SWT.NONE);
        itemButtonGroup.setText("商品按键区");
        itemButtonGroup.setLayout(new GridLayout());
        createItemButtonComposite2();
        itemButtonGroup.setLayoutData(gridData18);
        createPageUpDownComposite();
        createItemButtonEditAreaGroup();
    }

    /**
     * This method initializes itemButtonComposite  
     *
     */
    private void createItemButtonComposite2() {
        GridData gridData21 = new org.eclipse.swt.layout.GridData();
        gridData21.heightHint = -1;
        gridData21.grabExcessHorizontalSpace = true;
        gridData21.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData21.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData21.grabExcessVerticalSpace = true;
        gridData21.widthHint = -1;
        GridLayout gridLayout6 = new GridLayout();
        gridLayout6.numColumns = 4;
        gridLayout6.verticalSpacing = 7;
        gridLayout6.horizontalSpacing = 18;
        GridData gridData19 = new org.eclipse.swt.layout.GridData();
        gridData19.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData19.grabExcessHorizontalSpace = true;
        gridData19.grabExcessVerticalSpace = true;
        gridData19.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
        itemButtonComposite = new Composite(itemButtonGroup, SWT.NONE);
        itemButtonComposite.setLayoutData(gridData19);
        itemButtonComposite.setLayout(gridLayout6);

        MouseListener itemButtonMouseListener = new MouseAdapter() {
            public void mouseDown(MouseEvent e) {
                ItemButton itemButton = (ItemButton)e.widget;
                setEnableModifyDirty(false);
                updateCurrentItemButton(itemButton);
                setEnableModifyDirty(true);
            }
        };
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                itemButtons[i][j] = new ItemButton(itemButtonComposite, SWT.CENTER | SWT.SHADOW_OUT, i, j);
                itemButtons[i][j].setFont(new Font(Display.getDefault(), "宋体", 13, SWT.NORMAL));
                itemButtons[i][j].setBackground(new Color(Display.getCurrent(), 174, 255, 255));
                itemButtons[i][j].setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_BLUE));
                itemButtons[i][j].setLayoutData(gridData21);
                itemButtons[i][j].setCursor(handCursor);
                itemButtons[i][j].addMouseListener(itemButtonMouseListener);
            }
        }
    }

    private void updateCurrentItemButton(ItemButton itemButton) {
        if (currentItemButton != null && currentItemButton != itemButton) {
            currentItemButton.setFont(new Font(Display.getDefault(), "宋体", 12, SWT.NORMAL));
            currentItemButton.setSelected(false);
        }
        currentItemButton = itemButton;

        if (itemButton != null) {
            itemButton.setFont(new Font(Display.getDefault(), "宋体", 12, SWT.BOLD));
            itemButton.setSelected(true);
        }
        Item item;
        if (itemButton != null && (item = (Item)itemButton.getData()) != null) {
            itemButtonText.setText(item.getName());
            itemNumberText.setText(item.getId());
            itemButtonColorLabel.setText(item.getColor().toString());
        } else {
            itemButtonText.setText("");
            itemNumberText.setText("");
            itemButtonColorLabel.setText("");
        }
    }

    /**
     * This method initializes pageUpDownComposite  
     *
     */
    private void createPageUpDownComposite() {
        GridData gridData30 = new org.eclipse.swt.layout.GridData();
        gridData30.grabExcessHorizontalSpace = true;
        gridData30.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData30.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        GridData gridData29 = new org.eclipse.swt.layout.GridData();
        gridData29.horizontalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData29.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridLayout gridLayout8 = new GridLayout();
        gridLayout8.numColumns = 4;
        GridData gridData20 = new org.eclipse.swt.layout.GridData();
        gridData20.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData20.grabExcessHorizontalSpace = true;
        gridData20.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        pageUpDownComposite = new Composite(itemButtonGroup, SWT.NONE);
        pageUpDownComposite.setLayoutData(gridData20);
        pageUpDownComposite.setLayout(gridLayout8);
        emptyItemButton = new Button(pageUpDownComposite, SWT.NONE);
        emptyItemButton.setText("变为空白按键");
        emptyItemButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                if (currentItemButton != null && currentCategoryButton != null) {
                    currentItemButton.setData(null);
                    currentItemButton.setText("");
                    currentItemButton.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));

                    updateCurrentItemButton(currentItemButton);
                    // Why will the Item inside current ButtonLevel also be changed?
                    // 'cause this will update itemButtonText,..etc., and it'll tigger their
                    // ModifyText event, then update the related Item inside current ButtonLevel
                    setDirty(true);
                }
            }
        });
        removeItemButton = new Button(pageUpDownComposite, SWT.NONE);
        removeItemButton.setText("删除商品按键");
        removeItemButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                if (currentItemButton != null && currentCategoryButton != null) {
                    currentCategoryButton.removeItemFromCurrentButtonLevel(
                        currentItemButton.getRow(), currentItemButton.getCol());
                    updateItemButtonsFromCurrentCategoryButton();
                    updateCurrentItemButton(null);
                    setDirty(true);
                }
            }
        });
        pageUpButton = new Button(pageUpDownComposite, SWT.NONE);
        pageUpButton.setText("上页");
        pageUpButton.setLayoutData(gridData30);
        pageUpButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                if (currentCategoryButton != null) {
                    currentCategoryButton.previousButtonLevel();
                    updateItemButtonsFromCurrentCategoryButton();
                    if (currentItemButton != null)
                        updateCurrentItemButton(currentItemButton);
                }
            }
        });
        pageDownButton = new Button(pageUpDownComposite, SWT.NONE);
        pageDownButton.setText("下页");
        pageDownButton.setLayoutData(gridData29);
        pageDownButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                if (currentCategoryButton != null) {
                    currentCategoryButton.nextButtonLevel();
                    updateItemButtonsFromCurrentCategoryButton();
                    if (currentItemButton != null)
                        updateCurrentItemButton(currentItemButton);
                }
            }
        });
    }

    /**
     * This method initializes itemButtonEditAreaGroup  
     *
     */
    private void createItemButtonEditAreaGroup() {
        GridData gridData28 = new org.eclipse.swt.layout.GridData();
        gridData28.grabExcessHorizontalSpace = true;
        gridData28.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData28.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        GridData gridData27 = new org.eclipse.swt.layout.GridData();
        gridData27.grabExcessHorizontalSpace = false;
        gridData27.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData27.widthHint = 110;
        gridData27.horizontalAlignment = org.eclipse.swt.layout.GridData.BEGINNING;
        GridData gridData26 = new org.eclipse.swt.layout.GridData();
        gridData26.grabExcessHorizontalSpace = true;
        gridData26.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData26.widthHint = 150;
        gridData26.horizontalSpan = 3;
        gridData26.horizontalAlignment = org.eclipse.swt.layout.GridData.BEGINNING;
        GridData gridData25 = new org.eclipse.swt.layout.GridData();
        gridData25.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        gridData25.grabExcessHorizontalSpace = false;
        gridData25.horizontalIndent = 6;
        gridData25.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData24 = new org.eclipse.swt.layout.GridData();
        gridData24.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        gridData24.grabExcessHorizontalSpace = false;
        gridData24.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData23 = new org.eclipse.swt.layout.GridData();
        gridData23.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        gridData23.grabExcessHorizontalSpace = false;
        gridData23.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridLayout gridLayout7 = new GridLayout();
        gridLayout7.numColumns = 4;
        GridData gridData22 = new org.eclipse.swt.layout.GridData();
        gridData22.grabExcessHorizontalSpace = true;
        gridData22.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData22.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        itemButtonEditAreaGroup = new Group(itemButtonGroup, SWT.NONE);
        itemButtonEditAreaGroup.setText("商品按键编辑区");
        itemButtonEditAreaGroup.setLayout(gridLayout7);
        itemButtonEditAreaGroup.setLayoutData(gridData22);
        itemButtonDescLabel = new CLabel(itemButtonEditAreaGroup, SWT.NONE);
        itemButtonDescLabel.setText("商品按键显示文字:\n(可以用“^”符号折行)");
        itemButtonDescLabel.setLayoutData(gridData24);
        itemButtonText = new Text(itemButtonEditAreaGroup, SWT.BORDER);
        itemButtonText.setLayoutData(gridData26);
        itemButtonText.setTextLimit(30);
        itemButtonText.addModifyListener(new org.eclipse.swt.events.ModifyListener() {
            public void modifyText(org.eclipse.swt.events.ModifyEvent e) {
                if (currentItemButton != null) {
                    String s = ((Text)e.widget).getText();
                    currentItemButton.setText(formatToButtonText(s));
                    Item item = (Item)currentItemButton.getData();
                    if (item != null) {
                        item.setName(s);
                    } else {
                        Item newItem = new Item("", s, currentItemButton.getBackground().getRGB());
                        associateNewItemToCurrentItemButton(newItem);
                    }
                    setDirty(true);
                }
            }
        });
        itemNumberDescLabel = new Label(itemButtonEditAreaGroup, SWT.NONE);
        itemNumberDescLabel.setText("商品编号:");
        itemNumberDescLabel.setLayoutData(gridData23);
        itemNumberText = new Text(itemButtonEditAreaGroup, SWT.BORDER);
        itemNumberText.setLayoutData(gridData27);
        itemNumberText.setTextLimit(18);
        itemNumberText.addModifyListener(new org.eclipse.swt.events.ModifyListener() {
            public void modifyText(org.eclipse.swt.events.ModifyEvent e) {
                if (currentItemButton != null) {
                    String s = ((Text)e.widget).getText();
                    Item item = (Item)currentItemButton.getData();
                    if (item != null) {
                        item.setId(s);
                    } else {
                        Item newItem = new Item(s, "", currentItemButton.getBackground().getRGB());
                        associateNewItemToCurrentItemButton(newItem);
                    }
                    setDirty(true);
                }
            }
        });
        itemButtonColorButton = new Button(itemButtonEditAreaGroup, SWT.NONE);
        itemButtonColorButton.setText("颜色...");
        itemButtonColorButton.setLayoutData(gridData25);
        itemButtonColorButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                CenteringColorDialog colorDg = new CenteringColorDialog(getShell());
                RGB colorChose = colorDg.open();
                if (colorChose != null && currentItemButton != null) {
                    currentItemButton.setBackground(new Color(null, colorChose));
                    itemButtonColorLabel.setText(colorChose.toString());
                    Item item = (Item)currentItemButton.getData();
                    if (item != null) {
                        item.setColor(colorChose);
                    } else {
                        Item newItem = new Item("", "", colorChose);
                        associateNewItemToCurrentItemButton(newItem);
                    }
                    setDirty(true);
                }
            }
        });
        itemButtonColorLabel = new CLabel(itemButtonEditAreaGroup, SWT.NONE);
        itemButtonColorLabel.setText("");
        itemButtonColorLabel.setLayoutData(gridData28);
    }

    private void associateNewItemToCurrentItemButton(Item newItem) {
        if (currentItemButton != null)
            currentItemButton.setData(newItem);

        // also add into currentCategoryButton's repository
        if (currentCategoryButton != null && currentItemButton != null) {
            ButtonLevel currentButtonLevel = currentCategoryButton.getCurrentButtonLevel();
            if (currentButtonLevel != null) {
                currentButtonLevel.setItem(currentItemButton.getRow(),
                    currentItemButton.getCol(), newItem);
            } else {
                // new a new button panel
                ButtonLevel newButtonLevel = currentCategoryButton.createNewButtonLevel();
                currentCategoryButton.setCurrentButtonLevel(newButtonLevel);
                newButtonLevel.setItem(currentItemButton.getRow(),
                    currentItemButton.getCol(), newItem);
            }
        }
    }

    public void setOrganization(Organization org) {
        organization = org;
        organizationInfoLabel.setText(org.toString());
    }

    public void clearAllButtonData() {
        itemTree.removeAll();
        categoryButtonTree.removeAll();
        updateCurrentCategoryButtonTreeItem(null);
    }

    /**
     * This method initializes organizationComposite    
     *
     */
    private void createOrganizationComposite() {
        GridData gridData33 = new org.eclipse.swt.layout.GridData();
        gridData33.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData33.grabExcessHorizontalSpace = true;
        gridData33.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData32 = new org.eclipse.swt.layout.GridData();
        gridData32.grabExcessHorizontalSpace = false;
        gridData32.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData32.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        GridLayout gridLayout9 = new GridLayout();
        gridLayout9.numColumns = 3;
        gridLayout9.marginHeight = 1;
        gridLayout9.marginWidth = 5;
        GridData gridData31 = new org.eclipse.swt.layout.GridData();
        gridData31.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData31.grabExcessHorizontalSpace = true;
        gridData31.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        organizationComposite = new Composite(this, SWT.BORDER);
        organizationComposite.setLayoutData(gridData31);
        organizationComposite.setLayout(gridLayout9);
        organizationLabel = new CLabel(organizationComposite, SWT.NONE);
        organizationLabel.setText("组织或门市: ");
        organizationInfoLabel = new CLabel(organizationComposite, SWT.NONE);
        organizationInfoLabel.setText("0001");
        organizationInfoLabel.setLayoutData(gridData33);
        changeOrganizationButton = new Button(organizationComposite, SWT.NONE);
        changeOrganizationButton.setText("选择其他组织或门市...");
        changeOrganizationButton.setLayoutData(gridData32);
        changeOrganizationButton.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent e) {
                Organization org = StoreSelectionDialog.open();
                if (org != null) {
                    setOrganization(org);
                    clearAllButtonData();
                    loadButtonData();
                }
            }
            public void widgetDefaultSelected(SelectionEvent arg0) {
            }
        });
    }

    protected void setEnableModifyDirty(boolean b) {
        enableModifyDirty = b;
    }

    protected boolean getEnableModifyDirty() {
        return enableModifyDirty;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        if (getEnableModifyDirty()) {
            this.dirty = dirty;
            saveButton.setEnabled(dirty);
            reloadButton.setEnabled(dirty);
        }
    }
}  //  @jve:decl-index=0:visual-constraint="10,10"
