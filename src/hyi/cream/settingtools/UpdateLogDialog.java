package hyi.cream.settingtools;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.graphics.Image;

public class UpdateLogDialog {

    static StringBuffer logMessage = new StringBuffer();

    private Shell sShell = null;  //  @jve:decl-index=0:visual-constraint="10,10"
    private Text textArea = null;
    private Button closeButton = null;

    public static void open() {
        Display display = Display.getDefault();
        UpdateLogDialog thisClass = new UpdateLogDialog();
        thisClass.createSShell();
        Util.centerWindow(display, thisClass.sShell);
        thisClass.sShell.open();
        while (!thisClass.sShell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
    }

    /**
     * This method initializes sShell
     */
    private void createSShell() {
        GridLayout gridLayout = new GridLayout();
        gridLayout.verticalSpacing = 10;
        GridData gridData1 = new org.eclipse.swt.layout.GridData();
        gridData1.horizontalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData1.widthHint = 100;
        gridData1.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        GridData gridData = new org.eclipse.swt.layout.GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData.grabExcessVerticalSpace = true;
        gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        sShell = new Shell(
            PosSettingApplication.getInstance().sShell, // !!!comment out this to enable VE editing
            SWT.APPLICATION_MODAL | SWT.SHELL_TRIM);
        sShell.setText("更新記錄");
        sShell.setImage(new Image(Display.getCurrent(), getClass().getResourceAsStream("/hyi/cream/settingtools/InlineIcon.ico")));
        sShell.setLayout(gridLayout);
        sShell.setSize(new org.eclipse.swt.graphics.Point(684,275));
        textArea = new Text(sShell, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL | SWT.BORDER);
        textArea.setLayoutData(gridData);
        closeButton = new Button(sShell, SWT.NONE);
        closeButton.setText("关闭");
        closeButton.setLayoutData(gridData1);
        closeButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                sShell.dispose();
            }
        });
        sShell.setDefaultButton(closeButton);

        textArea.setText(UpdateLogDialog.logMessage.toString());
    }

    public static void print(String string) {
        logMessage.append(string);
    }

    public static void println(String string) {
        logMessage.append(string);
        logMessage.append('\n');
    }

}
