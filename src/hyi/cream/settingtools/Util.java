package hyi.cream.settingtools;

import java.awt.Toolkit;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class Util {

    private static Connection conn;
    private static PosSettingApplication app = PosSettingApplication.getInstance();
    private static String os = System.getProperty("os.name");
    
    public static boolean openDbConnection(/*String host,*/ String user, String password) {
        try {
            /*
            Class.forName("com.mysql.jdbc.Driver");
            Properties dbProp = new Properties();
            dbProp.put("user", user);
            dbProp.put("password", password);
            dbProp.put("useUnicode", "TRUE");
            dbProp.put("characterEncoding", "GBK");
            //dbProp.put("autoReconnect", "true");
            dbProp.put("connectTimeout", "5");
            conn = DriverManager.getConnection("jdbc:mysql://" + host + "/cacao", dbProp);

            Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
            conn = DriverManager.getConnection(
                //"jdbc:microsoft:sqlserver://localhost:1433;User=sa;Password=;DatabaseName=HQ_DATA");
                "jdbc:microsoft:sqlserver://localhost:1433;DatabaseName=HQ_DATA",
                "sa", "");
            */

            Class.forName(NamedQuery.getJDBCDriverClass());
            conn = DriverManager.getConnection(NamedQuery.getDatabaseURL(), user, password);
            return true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            app.showErrorMessageBox("连接数据库失败", "无法连接到数据库。");
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            app.showErrorMessageBox("连接数据库失败", "无法连接到数据库。");
            return false;
        }
    }

    public static void closeDbConnection() {
        try {
            if (conn != null) {
                conn.close();
                conn = null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            app.showExceptionInfo("Close Database Failed", "Cannot close database.", e);
        }
    }
    
    /**
     * Query data from database and fill the result into SWT Table.
     * 
     * @param table SWT Table.
     * @param sql Qeury SQL statement.
     * @return record count or -1 if error occurred.
     */
    public static ResultSet query(String sql) {
        try {
            Statement statement;
            ResultSet resultSet;
            statement = conn.createStatement();
            resultSet = statement.executeQuery(sql);
            //statement.close();
            return resultSet;
        } catch (SQLException e) {
            StringWriter stackTraceString = new StringWriter();
            e.printStackTrace(new PrintWriter(stackTraceString));
            PosSettingApplication.getInstance().showErrorMessageBox("查询信息",
                "查询失败！可能是查询语句语法错误。\n\n" + stackTraceString.toString());
            return null;
        }
    }

    public static ResultSet queryByNamedQuery(String namedQueryName, Object ... args) {
        try {
            String queryString = NamedQuery.getQuery(namedQueryName);
            PreparedStatement preparedStatement;
            ResultSet resultSet;
            preparedStatement = conn.prepareStatement(queryString);
            if (args != null && args.length > 0) {
                int i = 1;
                for (Object arg : args) {
                    preparedStatement.setObject(i++, arg);
                }
            }
            resultSet = preparedStatement.executeQuery();
            return resultSet;
        } catch (SQLException e) {
            StringWriter stackTraceString = new StringWriter();
            e.printStackTrace(new PrintWriter(stackTraceString));
            PosSettingApplication.getInstance().showErrorMessageBox("查询信息",
                "查询失败(" + namedQueryName + ")！可能是查询语句语法错误。\n\n" + stackTraceString.toString());
            return null;
        }
    }

    public static int execUpdate(PreparedStatement ps, Object ... args) {
        try {
            int i = 0;
            //System.out.print(sql + ", ");
            UpdateLogDialog.print("> ");
            if (args != null) {
                for (Object arg : args) {
                    ps.setObject(i + 1, arg);
                    i++;
                    //System.out.print("arg" + i + "=" + arg.toString()+ ", ");
                    UpdateLogDialog.print("arg");
                    UpdateLogDialog.print(i + "");
                    UpdateLogDialog.print("=");
                    UpdateLogDialog.print(arg.toString());
                    UpdateLogDialog.print(", ");
                }
            }
            int recCount = ps.executeUpdate();
            //System.out.println(", Cnt=" + recCount);
            UpdateLogDialog.print(" Update Count=");
            UpdateLogDialog.println(recCount + "");
            return recCount;
        } catch (SQLException e) {
            UpdateLogDialog.print("-> Failed!");
            StringWriter stackTraceString = new StringWriter();
            e.printStackTrace(new PrintWriter(stackTraceString));
            PosSettingApplication.getInstance().showErrorMessageBox("更新信息",
                "更新失败！可能是更新语句语法错误。\n\n" + stackTraceString.toString());
            return -1;
        }
    }

    public static PreparedStatement getPreparedStatement(String namedQueryName) {
        try {
            String updateString = NamedQuery.getQuery(namedQueryName);
            UpdateLogDialog.print("PreparedStatement> " + updateString + "\n");
            PreparedStatement ps;
            ps = conn.prepareStatement(updateString);
            return ps;
        } catch (SQLException e) {
            UpdateLogDialog.print("-> Failed!");
            StringWriter stackTraceString = new StringWriter();
            e.printStackTrace(new PrintWriter(stackTraceString));
            PosSettingApplication.getInstance().showErrorMessageBox("Get PreparedStatement",
                "PreparedStatement语法错误。\n\n" + stackTraceString.toString());
            return null;
        }
    }

    public static int execUpdateByNamedQuery(String namedQueryName, Object ... args) {
        String updateString = NamedQuery.getQuery(namedQueryName);
        return execUpdate(updateString, args);
    }

    /**
     * Update database and return update record count.
     * 
     * @param sql Update SQL statement.
     * @return record count or -1 if error occurred.
     */
    public static int execUpdate(String sql, Object ... args) {
        try {
            int i = 0;
            //System.out.print(sql + ", ");
            UpdateLogDialog.print(sql);
            UpdateLogDialog.print(", ");
            
            PreparedStatement ps;
            ps = conn.prepareStatement(sql);
            if (args != null) {
                for (Object arg : args) {
                    ps.setObject(i + 1, arg);
                    i++;
                    System.out.print("arg" + i + "=" + arg.toString()+ ", ");
                    UpdateLogDialog.print("arg");
                    UpdateLogDialog.print(i + "");
                    UpdateLogDialog.print("=");
                    UpdateLogDialog.print(arg.toString());
                    UpdateLogDialog.print(", ");
                }
            }
            int recCount = ps.executeUpdate();
            ps.close();
            //System.out.println(", Cnt=" + recCount);
            UpdateLogDialog.print(" Update Count=");
            UpdateLogDialog.println("" + recCount);
            return recCount;
        } catch (SQLException e) {
            UpdateLogDialog.print("-> Failed!");
            StringWriter stackTraceString = new StringWriter();
            e.printStackTrace(new PrintWriter(stackTraceString));
            PosSettingApplication.getInstance().showErrorMessageBox("更新信息",
                "更新失败！可能是更新语句语法错误。\n\n" + stackTraceString.toString());
            return -1;
        }
    }

    public static void centerWindow(Display display, Shell sShell) {
        Rectangle screenBounds = display.getBounds();
        if (screenBounds.width > 1024)
            screenBounds.width = 1024;
        if (screenBounds.height > 768)
            screenBounds.height = 768;
        
        Rectangle shellBounds = sShell.getBounds();
        sShell.setLocation((screenBounds.width - shellBounds.width) / 2,
            (screenBounds.height - shellBounds.height) / 2);
    }

    public static void playExclamationSound() {
        if (os.contains("Mac"))
            Toolkit.getDefaultToolkit().beep();
        else
        {
            Runnable r = (Runnable)Toolkit.getDefaultToolkit().getDesktopProperty(
                "win.sound.exclamation");
            r.run();
        }
    }

    public static void playAsteriskSound() {
        if (os.contains("Mac"))
            Toolkit.getDefaultToolkit().beep();
        else
        {
            Runnable r = (Runnable)Toolkit.getDefaultToolkit().getDesktopProperty(
                "win.sound.asterisk");
            r.run();
        }
    }
    
    public static void main(String[] args)
    {
        try
        {
            URL url = new URL("http://localhost:8080/ans?wicket:bookmarkablePage=:cricket.web.EmptyPage");
            URLConnection connection = url.openConnection();
//            connection.setDoOutput(true);

//            PrintWriter out = new PrintWriter(connection.getOutputStream());
//            out.print("wicket:bookmarkablePage");
//            out.print('=');
//            out.print(URLEncoder.encode(":cricket.web.master.item.ItemDataTablePage", "UTF-8"));
//            out.close();
            
            InputStream ins = connection.getInputStream();
            Scanner inr = new Scanner(ins, "UTF-8");
            while (inr.hasNextLine())
                 System.out.println(inr.nextLine());
            inr.close();
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
