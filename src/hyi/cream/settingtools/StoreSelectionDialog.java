package hyi.cream.settingtools;

import hyi.cream.settingtools.entity.Organization;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;


public class StoreSelectionDialog {

    private Shell sShell = null;  //  @jve:decl-index=0:visual-constraint="10,10"
    private CLabel titleLabel = null;
    private Tree tree = null;
    private Composite composite = null;
    private Button okButton = null;
    private Button cancelButton = null;
    private TreeItem firstTreeItem;
    private TreeItem firstSubTreeItem;
    private Organization selectedOrganization;
    private String titleText;

    /**
     * This method initializes tree 
     */
    private void createTree() {
        GridData gridData1 = new org.eclipse.swt.layout.GridData();
        gridData1.grabExcessHorizontalSpace = true;
        gridData1.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData1.grabExcessVerticalSpace = true;
        gridData1.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        tree = new Tree(sShell, SWT.NONE);
        tree.setLayoutData(gridData1);
        tree.addMouseListener(new org.eclipse.swt.events.MouseAdapter() {
            public void mouseDoubleClick(org.eclipse.swt.events.MouseEvent e) {
                TreeItem[] items = tree.getSelection();
                if (items.length > 0) {
                    selectedOrganization = (Organization)items[0].getData();
                    sShell.dispose();
                }
                return;
            }
        });
    }

    /**
     * This method initializes composite    
     */
    private void createComposite() {
        GridData gridData4 = new org.eclipse.swt.layout.GridData();
        gridData4.grabExcessHorizontalSpace = true;
        gridData4.horizontalIndent = 20;
        gridData4.widthHint = 110;
        GridData gridData3 = new org.eclipse.swt.layout.GridData();
        gridData3.grabExcessHorizontalSpace = true;
        gridData3.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData3.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        gridData3.widthHint = 110;
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 2;
        GridData gridData2 = new org.eclipse.swt.layout.GridData();
        gridData2.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData2.grabExcessHorizontalSpace = true;
        gridData2.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        composite = new Composite(sShell, SWT.NONE);
        composite.setLayoutData(gridData2);
        composite.setLayout(gridLayout);
        okButton = new Button(composite, SWT.NONE);
        okButton.setText("  选择组织或门市  ");
        okButton.setLayoutData(gridData3);
        okButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                TreeItem[] items = tree.getSelection();
                if (items.length > 0) {
                    selectedOrganization = (Organization)items[0].getData();
                    sShell.dispose();
                }
                return;
            }
        });
        cancelButton = new Button(composite, SWT.NONE);
        cancelButton.setText("取消");
        cancelButton.setLayoutData(gridData4);
        cancelButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                selectedOrganization = null;
                sShell.dispose();
            }
        });

        sShell.setDefaultButton(okButton);
    }

//    /**
//     * @param args
//     */
//    public static void main(String[] args) {
//        Util.openDbConnection(null, null, null);
//
//        /* Before this is run, be sure to set up the launch configuration (Arguments->VM Arguments)
//         * for the correct SWT library path in order to run with the SWT dlls. 
//         * The dlls are located in the SWT plugin jar.  
//         * For example, on Windows the Eclipse SWT 3.1 plugin jar is:
//         *       installation_directory\plugins\org.eclipse.swt.win32_3.1.0.jar
//         */
//        Display display = Display.getDefault();
//        StoreSelectionDialog thisClass = new StoreSelectionDialog();
//        thisClass.createSShell();
//        thisClass.furtherInitialize();
//        Util.centerWindow(display, thisClass.sShell);
//        thisClass.sShell.open();
//        while (!thisClass.sShell.isDisposed()) {
//            if (!display.readAndDispatch())
//                display.sleep();
//        }
//        display.dispose();
//    }

    public static Organization open() {
        return open("选择要编辑按键配置的组织或门市");
    }

    public static Organization open(String titleText) {
        Display display = Display.getDefault();
        StoreSelectionDialog dialog = new StoreSelectionDialog();
        dialog.titleText = titleText; 
        dialog.createSShell();
        dialog.furtherInitialize();
        Util.centerWindow(display, dialog.sShell);
        dialog.sShell.open();
        while (!dialog.sShell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        return dialog.selectedOrganization;
    }

    private void furtherInitialize() {
        tree.setHeaderVisible(true);
        TreeColumn column1 = new TreeColumn(tree, SWT.LEFT);
        column1.setText("组织或门市");
        column1.setWidth(200);
        TreeColumn column2 = new TreeColumn(tree, SWT.RIGHT);
        column2.setText("按键记录数");
        column2.setWidth(80);
        TreeColumn column3 = new TreeColumn(tree, SWT.LEFT);
        column3.setText("门市电话");
        column3.setWidth(100);
        TreeColumn column4 = new TreeColumn(tree, SWT.LEFT);
        column4.setText("联系人");
        column4.setWidth(90);
        TreeColumn column5 = new TreeColumn(tree, SWT.LEFT);
        column5.setText("IP地址");
        column5.setWidth(100);

        firstTreeItem = null;
        firstSubTreeItem = null;
        buildAllOrganizationTreeItems(null, 0);

        // for expanding something
        tree.showItem(firstSubTreeItem);
        tree.showItem(firstTreeItem);
    }

    private void buildAllOrganizationTreeItems(TreeItem parentTree, int parentId) {
        try {
            //ResultSet rs = Util.query(
            //    "select o.fOrgID,o.fOrgNo,o.fOrgName,o.fIsLast,fTelNo,fLineNO,fIpAdd,isnull(b.reccnt, 0)"
            //    + " from Ts_organize AS o left join"
            //    + "   (select fOrgID,count(*) as reccnt from Ts_pluButton group by fOrgID) b"
            //    + "   on o.fOrgID=b.fOrgID "
            //    + " where o.fParentID=" + parentId
            //    + " order by o.fOrgID");
            ResultSet rs = Util.queryByNamedQuery("organization.CertainLevelOfOrganizationsAndTheirButtonCount",
                parentId);
            if (rs == null)
                return;
            while (rs.next()) {
                Organization organization = new Organization();
                organization.setOrgID(rs.getInt(1));
                organization.setOrgNo(rs.getString(2));
                organization.setOrgName(rs.getString(3));
                boolean isLeaf = rs.getBoolean(4);
                organization.setTelNo(rs.getString(5));
                organization.setLineNo(rs.getString(6));
                organization.setIpAddr(rs.getString(7));
                organization.setButtonCount(rs.getInt(8));
                TreeItem treeItem;
                if (parentTree == null) {
                    treeItem = new TreeItem(tree, SWT.NONE);
                    treeItem.setData(organization);
                    if (firstTreeItem == null)
                        firstTreeItem = treeItem;
                } else {
                    treeItem = new TreeItem(parentTree, SWT.NONE);
                    treeItem.setData(organization);
                    if (firstSubTreeItem == null) {
                        firstSubTreeItem = treeItem;
                    }
                }
                treeItem.setText(new String[] {
                    organization.toString(),
                    organization.getButtonCount() + "  ",
                    organization.getTelNo(),
                    organization.getLineNo(),
                    organization.getIpAddr()});
                if (!isLeaf)
                    buildAllOrganizationTreeItems(treeItem, organization.getOrgID());
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method initializes sShell
     */
    private void createSShell() {
        GridData gridData = new org.eclipse.swt.layout.GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.BEGINNING;
//        sShell = new Shell();
        sShell = new Shell(
            PosSettingApplication.getInstance().sShell, // !!!comment out this to enable VE editing
            SWT.APPLICATION_MODAL | SWT.DIALOG_TRIM | SWT.MAX | SWT.RESIZE);
        sShell.setText("组织或门市选择");
        sShell.setLayout(new GridLayout());
        sShell.setSize(new org.eclipse.swt.graphics.Point(632,468));
        sShell.setImage(new Image(Display.getCurrent(), getClass().getResourceAsStream(
            "/hyi/cream/settingtools/InlineIcon.ico")));
        titleLabel = new CLabel(sShell, SWT.NONE);
        titleLabel.setText(titleText);
        titleLabel.setLayoutData(gridData);
        createTree();
        createComposite();
    }
}
