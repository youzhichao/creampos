package hyi.cream.settingtools;

import hyi.cream.settingtools.entity.Organization;
import hyi.cream.settingtools.entity.Property;
import hyi.cream.settingtools.entity.Section;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;


public class PosSettingApplication {

    private LinkedList<CTabItem> pageTravellingList = new LinkedList<CTabItem>();
    private Cursor waitCursor = new Cursor(Display.getCurrent(), SWT.CURSOR_WAIT);

    //Bruce> 为了避开Visual Editor的问题，将currentPage放在一个inner class里面
    //private CTabItem currentPage;
    static private class G {
        static public CTabItem currentPage;
    }

    static private PosSettingApplication instance;

    public Shell sShell = null; // @jve:decl-index=0:visual-constraint="10,10"

    private Menu menuBar = null;

    private Menu adjustSubmenu = null;

    private CTabFolder cTabFolder = null;

    private Composite welcomePageComposite = null;

    private ToolBar toolBar = null;

    private CLabel logoLabel = null;
    public PosSettingApplication() {
        instance = this;
    }

    static public PosSettingApplication getInstance() {
        return instance;
    }
    
    /**
     * This method initializes cTabFolder
     * 
     */
    private void createCTabFolder() {
        GridData gridData1 = new org.eclipse.swt.layout.GridData();
        gridData1.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL; // Generated
        gridData1.grabExcessHorizontalSpace = true; // Generated
        gridData1.grabExcessVerticalSpace = true; // Generated
        gridData1.horizontalIndent = 0; // Generated
        gridData1.verticalAlignment = org.eclipse.swt.layout.GridData.FILL; // Generated
        cTabFolder = new CTabFolder(sShell, SWT.NONE);
        createWelcomePageComposite();
        cTabFolder.setLayoutData(gridData1); // Generated
        cTabFolder.addCTabFolder2Listener(new org.eclipse.swt.custom.CTabFolder2Adapter() {
            public void close(org.eclipse.swt.custom.CTabFolderEvent e) {
                removePageFromTravellingList((CTabItem)e.item);
                if (((CTabItem)e.item).getControl().getClass() == PropertySettingPage.class)
                    Util.closeDbConnection();
                if (G.currentPage != null)
                    cTabFolder.setSelection(G.currentPage);
            }
        });
        cTabFolder.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                CTabItem ti = (CTabItem)e.item;
                recordCurrentTravellingPage(ti);                
            }
        });
        CTabItem cTabItem = new CTabItem(cTabFolder, SWT.CLOSE);
        String tabString = " 歡迎首頁 ";
        cTabItem.setText(tabString); // Generated
        cTabItem.setControl(welcomePageComposite); // Generated
        recordCurrentTravellingPage(cTabItem);
    }

    /**
     * This method initializes composite
     * 
     */
    private void createWelcomePageComposite() {
        GridData gridData2 = new org.eclipse.swt.layout.GridData();
        gridData2.horizontalAlignment = org.eclipse.swt.layout.GridData.CENTER;  // Generated
        gridData2.grabExcessHorizontalSpace = true;  // Generated
        gridData2.grabExcessVerticalSpace = true;  // Generated
        gridData2.horizontalIndent = 0;  // Generated
        gridData2.heightHint = 200;  // Generated
        gridData2.verticalAlignment = org.eclipse.swt.layout.GridData.BEGINNING;  // Generated
        welcomePageComposite = new Composite(cTabFolder, SWT.NONE);
        welcomePageComposite.setLayout(new GridLayout()); // Generated
        logoLabel = new CLabel(welcomePageComposite, SWT.SHADOW_NONE);
        logoLabel.setText("泓遠資訊（台北）、泓远软件（上海）ePOS後台維護程式\nVersion 1.0.0\n\n" +
            "設計 - 泓远软件（上海）\nCopyright(c) Hongyuan Software 2006-2007. All rights reserved."); // Generated
        logoLabel.setImage(new Image(Display.getCurrent(), getClass().getResourceAsStream(
                "/hyi/cream/settingtools/ePOSLogo.gif"))); // Generated
        logoLabel.setLayoutData(gridData2);  // Generated
    }

    /**
     * This method initializes toolBar
     * 
     */
    private void createToolBar() {
        GridData gridData = new org.eclipse.swt.layout.GridData();
        gridData.grabExcessHorizontalSpace = true; // Generated
        gridData.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER; // Generated
        gridData.heightHint = 25; // Generated
        gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL; // Generated
        toolBar = new ToolBar(sShell, SWT.NONE);
        toolBar.setLayoutData(gridData); // Generated
        ToolItem backToolItem = new ToolItem(toolBar, SWT.PUSH);
        backToolItem.setImage(new Image(Display.getCurrent(), getClass().getResourceAsStream("/hyi/cream/settingtools/back.gif")));  // Generated
        backToolItem.addSelectionListener(new org.eclipse.swt.events.SelectionListener() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                pageBackward();
            }
            public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent e) {
                widgetSelected(e);
            }
        });
        ToolItem forwardToolItem = new ToolItem(toolBar, SWT.PUSH);
        forwardToolItem.setImage(new Image(Display.getCurrent(), getClass().getResourceAsStream("/hyi/cream/settingtools/forward.gif")));  // Generated
        forwardToolItem.addSelectionListener(new org.eclipse.swt.events.SelectionListener() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                pageForeward();
            }
            public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent e) {
                widgetSelected(e);
            }
        });
    }

    private void adjustWindowSize(Display display) {
        Rectangle screenBounds = display.getBounds();
        if (screenBounds.width > 1024)
            screenBounds.width = 1024;
        if (screenBounds.height > 768)
            screenBounds.height = 768;
        sShell.setSize(new org.eclipse.swt.graphics.Point(screenBounds.width - 40,
            screenBounds.height - 80));
        Rectangle shellBounds = sShell.getBounds();
        sShell.setLocation((screenBounds.width - shellBounds.width) / 2,
            (screenBounds.height - shellBounds.height) / 2 - 10);
    }

    public static void main(String[] args) {
        if (args.length > 0 && args[0].equals("-store"))
            NamedQuery.setRunAtStore();
        NamedQuery.load();
        
        Display display = Display.getDefault();
        PosSettingApplication thisClass = new PosSettingApplication();
        thisClass.createSShell();

        //>>Bruce
        //thisClass.sShell.setMaximized(true);
        thisClass.adjustWindowSize(display);

        //GraphicsProvider.setMode(GraphicsProvider.MODE_SWT);            
        //SwtGraphicsProvider.setDefaultDisplay(display);

        thisClass.sShell.open();
        while (!thisClass.sShell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        display.dispose();
    }

    /**
     * This method initializes sShell
     */
    private void createSShell() {
        GridLayout gridLayout = new GridLayout();
        gridLayout.makeColumnsEqualWidth = false; // Generated
        gridLayout.marginWidth = 0; // Generated
        gridLayout.marginHeight = 0; // Generated
        gridLayout.horizontalSpacing = 0; // Generated
        gridLayout.numColumns = 1; // Generated
        gridLayout.verticalSpacing = 3; // Generated
        sShell = new Shell();
        sShell.setText("泓遠資訊ePOS後台維護程式");
        sShell.setImage(new Image(Display.getCurrent(), getClass().getResourceAsStream("/hyi/cream/settingtools/InlineIcon.gif"))); // Generated
        createToolBar();
        createCTabFolder();
        sShell.setLayout(gridLayout); // Generated
        sShell.setSize(new org.eclipse.swt.graphics.Point(541,360));
        menuBar = new Menu(sShell, SWT.BAR);
        MenuItem adjustMenu = new MenuItem(menuBar, SWT.CASCADE);
        adjustMenu.setText("系統設定(&F)"); // Generated
        adjustSubmenu = new Menu(adjustMenu);
        MenuItem posSettingMenuItem = new MenuItem(adjustSubmenu, SWT.PUSH);
        posSettingMenuItem.setText("&POS參數設定...");  // Generated
        MenuItem touchButtonSettingMenuItem = new MenuItem(adjustSubmenu, SWT.PUSH);
        touchButtonSettingMenuItem.setText("觸控螢幕按鍵設定(&T)...");
        touchButtonSettingMenuItem
            .addSelectionListener(new org.eclipse.swt.events.SelectionListener() {
                public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                    CTabItem cTabItem = findTabItem(" 觸控螢幕按鍵設定 ");
                    if (cTabItem != null)
                        openTabItem(cTabItem);
                    else {
                        if (loginIfDatabaseIsNotOpened()) {
                            if (NamedQuery.isRunAtStore()) {
                                sShell.setCursor(waitCursor);
                                cTabItem = createTabItem(TouchKeySettingPage.class, " 觸控螢幕按鍵設定 ");
                                TouchKeySettingPage page = (TouchKeySettingPage)cTabItem.getControl();
                                page.loadButtonData();
                                openTabItem(cTabItem);
                                sShell.setCursor(null);
                            } else {
                                Organization org = StoreSelectionDialog.open();
                                if (org != null) {
                                    sShell.setCursor(waitCursor);
                                    cTabItem = createTabItem(TouchKeySettingPage.class, " 触屏按键配置 ");
                                    TouchKeySettingPage page = (TouchKeySettingPage)cTabItem.getControl();
                                    page.setOrganization(org);
                                    page.loadButtonData();
                                    openTabItem(cTabItem);
                                    sShell.setCursor(null);
                                }
                            }
                        }
                    }
                }
                public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent e) {
                    widgetSelected(e);
                }
            });
        MenuItem separator = new MenuItem(adjustSubmenu, SWT.SEPARATOR);

        posSettingMenuItem.addSelectionListener(new org.eclipse.swt.events.SelectionListener() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                CTabItem cTabItem = findTabItem(" POS參數設定 ");
                if (cTabItem != null)
                    openTabItem(cTabItem);
                else {
                    if (loginIfDatabaseIsNotOpened()) {
                        sShell.setCursor(waitCursor);
                        Map<Section, List<Property>> propMap = SettingsLoader.load();
                        sShell.setCursor(null);
                        if (propMap == null) {
                            return;
                        }
                        // if connect to POS ok, then open property setting page
                        cTabItem = createTabItem(PropertySettingPage.class, " POS參數設定 ");
                        
                        // Inject property map into the Composite object
                        ((PropertySettingPage)cTabItem.getControl()).setPropMap(propMap);
                        
                        openTabItem(cTabItem);
                    }
                }
            }
            public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent e) {
                widgetSelected(e);
            }
        });

        
        MenuItem quitMenuItem = new MenuItem(adjustSubmenu, SWT.PUSH);
        quitMenuItem.setText("退出(&X)"); // Generated
        quitMenuItem.addSelectionListener(new org.eclipse.swt.events.SelectionListener() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                System.exit(0);
            }

            public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent e) {
                widgetSelected(e);
            }
        });
        adjustMenu.setMenu(adjustSubmenu); // Generated
        sShell.setMenuBar(menuBar); // Generated
    }

    public <T extends Composite> CTabItem createTabItem(Class<T> compositeClass,
        String tabString) {
        try {
            T page = null;
            CTabItem cTabItem;
            Constructor<T> ctr = compositeClass.getConstructor(new Class[] {
                Composite.class, int.class });
            page = ctr.newInstance(new Object[] { cTabFolder, SWT.NONE });
            cTabItem = new CTabItem(cTabFolder, SWT.CLOSE);
            cTabItem.setText(tabString);
            cTabItem.setControl(page);
            return cTabItem;
        } catch (Exception e) {
            showExceptionInfo("無法開啟", "無法開啟 '" + tabString, e);
            return null;
        }
    }

    public void openTabItem(CTabItem cTabItem) {
        cTabFolder.setSelection(cTabItem);
        recordCurrentTravellingPage(cTabItem);
        return;
    }

    private CTabItem findTabItem(String text) {
        CTabItem[] tabItems = cTabFolder.getItems();
        for (CTabItem ti : tabItems) {
            if (ti.getText().equals(text))
                return ti;
        }
        return null;
    }
    
    private void recordCurrentTravellingPage(CTabItem cTabItem) {
        if (G.currentPage == cTabItem)
            return;
        if (G.currentPage == null) {
            pageTravellingList.add(cTabItem);
            G.currentPage = cTabItem;
            return;
        }
        pageTravellingList.remove(cTabItem);
        int currentPagePos = pageTravellingList.indexOf(G.currentPage);
        if (currentPagePos >= pageTravellingList.size() - 1)
            pageTravellingList.add(cTabItem);
        else
            pageTravellingList.add(currentPagePos + 1, cTabItem);
        G.currentPage = cTabItem;
    }
    
    private void removePageFromTravellingList(CTabItem cTabItem) {
        if (G.currentPage != cTabItem) {
            pageTravellingList.remove(cTabItem);
            return;
        }
        int currentPagePos = pageTravellingList.indexOf(cTabItem); 
        pageTravellingList.remove(cTabItem);
        if (currentPagePos >= pageTravellingList.size())
            currentPagePos = pageTravellingList.size() - 1;
        if (currentPagePos != -1)
            G.currentPage = pageTravellingList.get(currentPagePos);
        else
            G.currentPage = null;
    }

    private void pageBackward() {
        if (G.currentPage == null)
            return;
        int currentPagePos = pageTravellingList.indexOf(G.currentPage);
        if (currentPagePos <= 0)
            return;
        currentPagePos--;
        G.currentPage = pageTravellingList.get(currentPagePos);
        cTabFolder.setSelection(G.currentPage);
    }
    
    private void pageForeward() {
        if (G.currentPage == null)
            return;
        int currentPagePos = pageTravellingList.indexOf(G.currentPage);
        if (currentPagePos == -1 || currentPagePos >= pageTravellingList.size() - 1)
            return;
        currentPagePos++;
        G.currentPage = pageTravellingList.get(currentPagePos);
        cTabFolder.setSelection(G.currentPage);
    }
    
    public void showExceptionInfo(String title, String msg, Exception e) {
        StringWriter stackTraceString = new StringWriter();
        e.printStackTrace(new PrintWriter(stackTraceString));
        showErrorMessageBox(title, msg + ":\n\n"
            + stackTraceString.toString() + "\n請記錄上述訊息，提供軟體維護人員。");
    }
    
    public void showErrorMessageBox(String title, String msg) {
        MessageBox mb = new MessageBox(sShell, SWT.ICON_ERROR | SWT.OK);
        mb.setText(title);
        mb.setMessage(msg);
        mb.open();
    }

    public void showInfoMessageBox(String title, String msg) {
        MessageBox mb = new MessageBox(sShell, SWT.ICON_INFORMATION | SWT.OK);
        mb.setText(title);
        mb.setMessage(msg);
        mb.open();
    }
    
    private boolean loginIfDatabaseIsNotOpened() {
        if (!DbConnectDialog.isConnectToPosOK())
            return DbConnectDialog.open();
        return true;
    }
}
