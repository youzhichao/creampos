package hyi.cream.settingtools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class NamedQuery {
    static public enum Site { Store, HQ };
    static private Site runAt = Site.HQ;

    static private PosSettingApplication app = PosSettingApplication.getInstance(); 
    static private Properties properties = new Properties();

    static public void load() {
        try {
            InputStream namedQueryDefinitionFile = null;
            if (new File(getNamedQueryFile()).exists())
                namedQueryDefinitionFile = new FileInputStream(getNamedQueryFile());
            if (namedQueryDefinitionFile == null)
                namedQueryDefinitionFile = new FileInputStream("./conf/" + getNamedQueryFile());
            if (namedQueryDefinitionFile == null)
                namedQueryDefinitionFile = app.getClass().getResourceAsStream(getNamedQueryFile());
            properties.loadFromXML(namedQueryDefinitionFile);
            namedQueryDefinitionFile.close();
        } catch (FileNotFoundException e) {
            app.showErrorMessageBox("配置文件错误", "配置文件" + getNamedQueryFile() + "找不到。");
        } catch (IOException e) {
            app.showErrorMessageBox("配置文件错误", "配置文件" + getNamedQueryFile() + "读取错误。");
        }
    }

    static public String getJDBCDriverClass() {
        return properties.getProperty("jdbc.DriverClass").trim();
    }

    static public String getDatabaseURL() {
        return properties.getProperty("jdbc.DatabaseURL").trim();
    }

    static public String getQuery(String name) {
        return properties.getProperty(name).trim();
    }

    static public String getQuery(String name, String defaultValue) {
        return properties.getProperty(name, defaultValue).trim();
    }

    static public boolean isRunAtStore() {
        return (runAt == Site.Store);
    }

    static public void setRunAtStore() {
        runAt = Site.Store;
    }
    
    static String getNamedQueryFile() {
        return isRunAtStore() ? "namedquery_store.xml" : "namedquery_hq.xml";
    }
}
