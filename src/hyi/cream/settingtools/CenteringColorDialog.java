package hyi.cream.settingtools;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Shell;

public class CenteringColorDialog {

    // With this we can basically set the location of the ColorDialog
    // centering, it will require a little bit of guessing since the width and
    // height of the dialog are unknown.
    static private final int COLORDIALOG_WIDTH = 222;
    static private final int COLORDIALOG_HEIGHT = 306;

    private final Shell parentShell;
    
    public CenteringColorDialog (Shell parentShell) {
        this.parentShell = parentShell;
    }
    
    public RGB open () {
        final Shell centerShell = new Shell(parentShell, SWT.NO_TRIM);
        centerShell.setLocation((parentShell.getSize().x - COLORDIALOG_WIDTH) / 2,
            (parentShell.getSize().y - COLORDIALOG_HEIGHT) / 2);
        ColorDialog colorDg = new ColorDialog(centerShell, SWT.APPLICATION_MODAL);
        final RGB colorChose = colorDg.open();
        centerShell.dispose();
        return colorChose;
    }
}
