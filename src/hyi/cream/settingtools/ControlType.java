package hyi.cream.settingtools;

public enum ControlType {
    EditBox, CheckBox, ComboBox;
}
