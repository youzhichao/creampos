package hyi.cream.settingtools;

import hyi.cream.settingtools.entity.Property;
import hyi.cream.settingtools.entity.Section;

import java.util.List;
import java.util.Map;

import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Button;

import static hyi.cream.settingtools.ControlType.*;

import org.eclipse.swt.widgets.Tree;


public class PropertySettingPage extends Composite {

    private static final String NOT_SET = "<not set>";
    private Tree tree = null;
    private Composite parentComposite = null;
    private Composite[] composites;
    Map<Section, List<Property>> propMap;
    private Composite buttonComposite = null;
    private Button saveButton = null;
    private Button undoButton = null;
    private Cursor waitCursor = new Cursor(Display.getCurrent(), SWT.CURSOR_WAIT);
    private ContentChangeListener contentChangeListener = new ContentChangeListener(); 
    
    private final class ContentChangeListener implements KeyListener, SelectionListener {
        Color changedColor = new Color(Display.getCurrent(), 255, 255, 128);
        
        /**
         * When widget's content is changed, change its background color and enable
         * saveButton and undoButton.
         * 
         * @param w
         *            SWT widget.
         */
        public void checkIfContentChanged(Widget w) {
            boolean contentChanged = false;
            if (w instanceof Button) {
                contentChanged = !((((Button)w).getSelection()) ? "yes" : "no")
                    .equalsIgnoreCase(((Property)((Button)w).getData()).originalValue);
                if (contentChanged)
                    ((Button)w).setBackground(changedColor);
            } else if (w instanceof Text) {
                String newValue = ((Text)w).getText();
                newValue = newValue.equals(NOT_SET) ? null : newValue;
                String origValue = ((Property)((Text)w).getData()).originalValue;
                contentChanged = !(
                    (newValue == origValue) || (newValue != null && newValue.equals(origValue)));
                if (contentChanged)
                    ((Text)w).setBackground(changedColor);
            } else if (w instanceof Combo) {
                String newValue = ((Combo)w).getText();
                newValue = newValue.equals(NOT_SET) ? null : newValue;
                String origValue = ((Property)((Combo)w).getData()).originalValue;
                contentChanged = !(
                    (newValue == origValue) || (newValue != null && newValue.equals(origValue)));
                if (contentChanged)
                    ((Combo)w).setBackground(changedColor);
            }
            if (contentChanged) {
                saveButton.setEnabled(true);
                undoButton.setEnabled(true);
            }
        }

        public void keyPressed(KeyEvent e) {
        }

        public void keyReleased(KeyEvent e) {
            checkIfContentChanged(e.widget);
        }

        public void widgetSelected(SelectionEvent e) {
            checkIfContentChanged(e.widget);
        }

        public void widgetDefaultSelected(SelectionEvent e) {
            checkIfContentChanged(e.widget);
        }
    }

    /**
     * This method initializes tree 
     */
    private void createTree() {
        GridData gridData = new org.eclipse.swt.layout.GridData();
        gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.BEGINNING;  // Generated
        gridData.grabExcessVerticalSpace = true;  // Generated
        gridData.heightHint = -1;  // Generated
        gridData.widthHint = 150;  // Generated
        gridData.verticalSpan = 1;
        gridData.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;  // Generated
        tree = new Tree(this, SWT.BORDER);
        tree.setLayoutData(gridData);  // Generated
    }

    /**
     * This method initializes scrolledComposite    
     */
    private void createScrolledComposite() {
        GridData gridData1 = new org.eclipse.swt.layout.GridData();
        gridData1.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;  // Generated
        gridData1.grabExcessVerticalSpace = true;  // Generated
        gridData1.grabExcessHorizontalSpace = true;  // Generated
        gridData1.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;  // Generated
        parentComposite = new Composite(this, SWT.BORDER);
        parentComposite.setLayoutData(gridData1);  // Generated
    }


    public PropertySettingPage(Composite parent, int style) {
        super(parent, style);
        initialize();
    }

    private void initialize() {
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 2;  // Generated
        createTree();
        this.setLayout(gridLayout);  // Generated
        createScrolledComposite();
        createButtonComposite();
        setSize(new org.eclipse.swt.graphics.Point(492,331));
    }

    /**
     * Create correspoding composite pages and controls.
     */
    private void createPropertyContorls() {
        setCursor(waitCursor);

        // Create array of section pages
        composites = new Composite[propMap.keySet().size()];

        int i = 0;
        for (Section sec : propMap.keySet()) {

            // Create tree node at left panel
            TreeItem treeItem = new TreeItem(tree, SWT.NONE);
            treeItem.setText(sec.description);
            treeItem.setData(i);

            // Select first item
            if (i == 0)
                tree.setSelection(new TreeItem[] {treeItem});

            // create section page composite
            Composite page = composites[i] = new Composite(parentComposite, SWT.NONE);
            GridLayout gridLayout = new GridLayout();
            gridLayout.numColumns = 2; //4;
            gridLayout.verticalSpacing = 3;
            page.setLayout(gridLayout);

            /*
             * Layout:
             * 
             * Col1               Col2 
             * -----------------------------Title Text---------------------------------
             * ------Description: [TextBox---------] 
             *                    Comment---------------(default:xxx)
             * ------Description: [ComboBox-------V]
             *                    Comment---------------(default:xxx)
             * [x] Description---------------------- 
             *                    Comment---------------(default:xxx)
             */            

            // Title Text
            CLabel cLabel = new CLabel(page, SWT.SHADOW_OUT);
            cLabel.setText(sec.description);
            cLabel.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_BACKGROUND));
            cLabel.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_FOREGROUND));
            GridData gridData = new org.eclipse.swt.layout.GridData();
            gridData.horizontalSpan = 2;
            gridData.heightHint = 23;
            gridData.verticalSpan = 8;
            gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
            gridData.grabExcessHorizontalSpace = true;
            gridData.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
            cLabel.setLayoutData(gridData);

            // Get the proproties of this section
            CLabel emptyLabel, descriptionLabel;
            Button descriptionCheckbox;
            Text propertyText;
            Combo propertyCombo;
            CLabel commentLabel;
            List<Property> props = propMap.get(sec);
            for (Property prop : props) {
                switch (prop.controlType) {
                case EditBox:
                case ComboBox:
                    // Description
                    descriptionLabel = new CLabel(page, SWT.NONE);
                    descriptionLabel.setText(prop.description + ": ");
                    gridData = new org.eclipse.swt.layout.GridData();
                    gridData.horizontalSpan = 1;
                    gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
                    gridData.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
                    descriptionLabel.setLayoutData(gridData);

                    // Text or Combo
                    gridData = new org.eclipse.swt.layout.GridData();
                    gridData.horizontalSpan = 1;
                    //gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
                    //gridData.grabExcessHorizontalSpace = true;
                    gridData.widthHint = prop.width;
                    gridData.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
                    if (prop.controlType == EditBox) {
                        prop.control = propertyText = new Text(page, SWT.BORDER);
                        propertyText.setText(prop.originalValue == null ? NOT_SET : prop.originalValue);
                        propertyText.setLayoutData(gridData);
                        propertyText.setData(prop);
                        propertyText.addKeyListener(contentChangeListener); // for tracking changes
                    } else {
                        prop.control = propertyCombo = new Combo(page, SWT.READ_ONLY);
                        propertyCombo.add(NOT_SET);
                        for (String pv : prop.possibleValues) {
                            propertyCombo.add(pv);
                        }
                        propertyCombo.setText(prop.originalValue == null ? NOT_SET : prop.originalValue);
                        propertyCombo.setLayoutData(gridData);
                        propertyCombo.setData(prop);
                        propertyCombo.addSelectionListener(contentChangeListener); // for tracking changes
                    }
                    break;
                    
                case CheckBox:
                    // Description
                    prop.control = descriptionCheckbox = new Button(page, SWT.CHECK);
                    descriptionCheckbox.setText(prop.description);
                    
                    boolean checkState;
                    if (prop.originalValue != null)
                        checkState = prop.originalValue.equalsIgnoreCase("yes");
                    else if (prop.defaultValue != null)
                        checkState = prop.defaultValue.equalsIgnoreCase("yes");
                    else
                        // 如果没有defaultValue，而且在property表也不存在的话，只好把checkState default设成false
                        checkState = false;

                    descriptionCheckbox.setSelection(checkState);
                    gridData = new org.eclipse.swt.layout.GridData();
                    gridData.horizontalSpan = 2;
                    gridData.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
                    descriptionCheckbox.setLayoutData(gridData);
                    descriptionCheckbox.setData(prop);
                    descriptionCheckbox.addSelectionListener(contentChangeListener); // for tracking changes
                    break;
                }

                // Comment
                
                if (prop.getComment().length() > 0 || prop.getDefaultValue().length() > 0) {
                    emptyLabel = new CLabel(page, SWT.NONE);
                    gridData = new org.eclipse.swt.layout.GridData();
                    gridData.horizontalSpan = 1;
                    gridData.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
                    emptyLabel.setLayoutData(gridData);
    
                    commentLabel = new CLabel(page, SWT.NONE);
                    commentLabel.setText(prop.getComment() +
                        ((prop.getDefaultValue().length() != 0) ? " (default = " + prop.getDefaultValue() + ")" : ""));
                    commentLabel.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
                    gridData = new org.eclipse.swt.layout.GridData();
                    gridData.horizontalSpan = 1;
                    //gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.HORIZONTAL_ALIGN_BEGINNING;
                    //gridData.grabExcessHorizontalSpace = true;
                    gridData.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
                    commentLabel.setLayoutData(gridData);
                }
            }

            page.setVisible(false);
            i++;
        }
        composites[0].setVisible(true);
        
        parentComposite.addControlListener(new ControlListener() {
            public void controlMoved(ControlEvent arg0) {
            }

            public void controlResized(ControlEvent e) {
                Composite parent = (Composite)e.getSource();
                for (Composite child : composites)
                    child.setSize(parent.getSize());
            }
        });

        tree.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent e) {
                int pageIdx = (Integer)e.item.getData(); // the associated Section
                for (int i = 0; i < composites.length; i++)
                    composites[i].setVisible(i == pageIdx);
            }
            public void widgetDefaultSelected(SelectionEvent arg0) {
            }
        });
        
        setCursor(null);
    }

    /**
     * This method initializes buttonComposite  
     *
     */
    private void createButtonComposite() {
        GridData gridData4 = new org.eclipse.swt.layout.GridData();
        gridData4.widthHint = 130;
        gridData4.grabExcessHorizontalSpace = true;
        gridData4.horizontalIndent = 25;
        GridLayout gridLayout1 = new GridLayout();
        gridLayout1.numColumns = 2;
        GridData gridData3 = new org.eclipse.swt.layout.GridData();
        gridData3.widthHint = 130;
        gridData3.grabExcessHorizontalSpace = true;
        gridData3.horizontalAlignment = org.eclipse.swt.layout.GridData.END;
        gridData3.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        gridData3.horizontalIndent = 0;
        GridData gridData2 = new org.eclipse.swt.layout.GridData();
        gridData2.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
        gridData2.grabExcessHorizontalSpace = true;
        gridData2.horizontalSpan = 2;
        gridData2.verticalAlignment = org.eclipse.swt.layout.GridData.CENTER;
        buttonComposite = new Composite(this, SWT.NONE);
        buttonComposite.setLayoutData(gridData2);
        buttonComposite.setLayout(gridLayout1);
        saveButton = new Button(buttonComposite, SWT.NONE);
        saveButton.setText("保存更动内容(&S)");
        saveButton.setLayoutData(gridData3);
        saveButton.setEnabled(false);
        saveButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                saveToDatabase();
                UpdateLogDialog.open();
            }
        });

        undoButton = new Button(buttonComposite, SWT.NONE);
        undoButton.setText("还原原有内容(&U)");
        undoButton.setLayoutData(gridData4);
        undoButton.setEnabled(false);
        undoButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
                for (Section sec : propMap.keySet()) {
                    List<Property> props = propMap.get(sec);
                    for (Property prop : props) {
                        switch (prop.controlType) {
                        case EditBox:
                            ((Text)prop.control)
                                .setText(prop.originalValue == null ? NOT_SET
                                    : prop.originalValue);
                            break;

                        case ComboBox:
                            ((Combo)prop.control)
                                .setText(prop.originalValue == null ? NOT_SET
                                    : prop.originalValue);
                            break;

                        case CheckBox:
                            ((Button)prop.control)
                                .setSelection(prop.originalValue != null
                                    && prop.originalValue.equalsIgnoreCase("yes"));
                            break;
                        }
                        // 还原背景色
                        prop.control.setBackground(null);
                    }
                }
                saveButton.setEnabled(false);
                undoButton.setEnabled(false);
            }
        });
    }

    private void saveToDatabase() {
        try {
            UpdateLogDialog.logMessage.setLength(0);

            for (Section sec : propMap.keySet()) {
                List<Property> props = propMap.get(sec);
                for (Property prop : props) {
                    switch (prop.controlType) {
                    case EditBox:
                        prop.updatedValue = ((Text)prop.control).getText();
                        break;

                    case ComboBox:
                        prop.updatedValue = ((Combo)prop.control).getText();
                        break;

                    case CheckBox:
                        prop.updatedValue = ((Button)prop.control).getSelection() ? "yes"
                            : "no";
                        break;
                    }

                    if (prop.originalValue == null) { // 表示原来数据库中不存在这个property
                        if (!prop.updatedValue.equals(NOT_SET)) {
                            if (prop.defaultValue == null
                                || !prop.updatedValue.equals(prop.defaultValue)) {
                                if (Util.execUpdate(
                                    "INSERT INTO property (name,value) VALUES (?,?)",
                                    prop.name, prop.updatedValue) <= 0)
                                    return;
                                else {
                                    prop.originalValue = prop.updatedValue;
                                    // 还原背景色
                                    prop.control.setBackground(null);
                                }
                            }
                        }
                    } else { // 表示原来数据库中存在这个property
                        if (prop.updatedValue.equals(NOT_SET)) {
                            // 因为checkbox的updatedValue永远不会等于NOT_SET，所以一旦checkbox类型的property
                            // 存在数据库，就不可能被删除掉。
                            if (Util.execUpdate("DELETE FROM property WHERE name=?",
                                prop.name) <= 0)
                                return;
                            else {
                                prop.originalValue = null;
                                // 还原背景色
                                prop.control.setBackground(null);
                            }
                        } else {
                            if (!prop.originalValue.equals(prop.updatedValue)) {
                                if (Util.execUpdate(
                                    "UPDATE property SET value=? WHERE name=?",
                                    prop.updatedValue, prop.name) <= 0)
                                    return;
                                else {
                                    prop.originalValue = prop.updatedValue;
                                    // 还原背景色
                                    prop.control.setBackground(null);
                                }
                            }
                        }
                    }
                }
            }
        } finally {
            saveButton.setEnabled(false);
            undoButton.setEnabled(false);
            UpdateLogDialog.logMessage.append("更新完毕。");
        }
    }

    public Map<Section, List<Property>> getPropMap() {
        return propMap;
    }

    public void setPropMap(Map<Section, List<Property>> propMap) {
        this.propMap = propMap;
        createPropertyContorls();
    }
    
}  // @jve:decl-index=0:visual-constraint="10,10"
