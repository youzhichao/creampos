package hyi.cream.settingtools;

import hyi.cream.settingtools.entity.Property;
import hyi.cream.settingtools.entity.Section;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


/**
 * This is the property definition file loader.
 * The property definition file is always named "property.xml", SettingLoader
 * will first try to find this file under current working directory, if 
 * cannot be found, then it will look up by class loader.
 *  
 * @author Bruce
 * @version 1.0
 */
public class SettingsLoader {
    static PosSettingApplication app = PosSettingApplication.getInstance(); 
    static Map<String, String> propertiesInDb = new HashMap<String, String>();

    /**
     * The property definition file is always named "property.xml", SettingLoader
     * will first find to find this file under current working directory, if 
     * cannot be found, then it will look up by class loader.
     * 
     * @return A Map<Section List<Property>> object which contains the property definitions.
     * @version 1.0
     * @hides load
     */
    static public Map<Section, List<Property>> load() {
        if (!loadPOSPropertiesInDb())
            return null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputStream propertyDefinitionFile = null;
            if (new File("property.xml").exists())
                propertyDefinitionFile = new FileInputStream("property.xml");
            if (new File("conf/property.xml").exists())
                propertyDefinitionFile = new FileInputStream("conf/property.xml");
            if (propertyDefinitionFile == null)
                propertyDefinitionFile = app.getClass().getResourceAsStream("/property.xml");
            Document document = builder.parse(propertyDefinitionFile);
            Node configurationNode = document.getChildNodes().item(0);
            NodeList sectionNodeList = configurationNode.getChildNodes();

            Map<Section, List<Property>> propMap = new TreeMap<Section, List<Property>>();
            // -> Don't use HashMap, because HashMap cannot keep the right order

            for (int i = 0; i < sectionNodeList.getLength(); i++) {
                Node sectionNode = sectionNodeList.item(i);
                String sectionName = sectionNode.getNodeName();
                if (sectionName.startsWith("#"))
                    continue;

                System.out.print(sectionName);
                if (!sectionName.equals("Section"))
                    throw new SAXException("看不懂的Section:" + sectionName);

                NamedNodeMap attrMap = sectionNode.getAttributes();
                Section section = new Section(attrMap.getNamedItem("name").getNodeValue(),
                    attrMap.getNamedItem("description").getNodeValue());
                System.out.print(": name=" + section.name);
                System.out.println(", description=" + section.description);

                List<Property> propList = new ArrayList<Property>();
                propMap.put(section, propList);
                
                NodeList propertyNodeList = sectionNode.getChildNodes();
                for (int j = 0; j < propertyNodeList.getLength(); j++) {
                    Node propertyNode = propertyNodeList.item(j);
                    String propertyType = propertyNode.getNodeName();
                    if (propertyType.startsWith("#"))
                        continue;

                    Property prop = new Property();
                    System.out.print("    " + propertyType); // CheckBox, EditBox, or ComboBox
                    if (propertyType.equals("CheckBox"))
                        prop.controlType = ControlType.CheckBox;
                    else if (propertyType.equals("EditBox"))
                        prop.controlType = ControlType.EditBox;
                    else if (propertyType.equals("ComboBox"))
                        prop.controlType = ControlType.ComboBox;
                    else
                        throw new SAXException("Control type不正確"); 

                    NamedNodeMap propertyAttrMap = propertyNode.getAttributes();

                    prop.name = propertyAttrMap.getNamedItem("propertyName").getNodeValue();
                    System.out.print(": propertyName=" + prop.name);

                    Node defaultNode = propertyAttrMap.getNamedItem("default");
                    if (defaultNode == null) {
                        System.out.print(", default=null");
                        prop.defaultValue = null;
                    } else {
                        prop.defaultValue = defaultNode.getNodeValue();
                        System.out.print(", default=" + prop.defaultValue);
                    }

                    Node widthNode = propertyAttrMap.getNamedItem("width");
                    if (widthNode == null) {
                        System.out.print(", width=null");
                        prop.width = -1;
                    } else {
                        prop.width = Integer.parseInt(widthNode.getNodeValue());
                        System.out.print(", width=" + prop.width);
                    }

                    Node possibleValuesNode = propertyAttrMap.getNamedItem("possibleValues");
                    if (possibleValuesNode == null) {
                        System.out.print(", possibleValues=null");
                    } else {
                        prop.possibleValues = possibleValuesNode.getNodeValue().split(",");
                        System.out.print(", possibleValues=" + possibleValuesNode.getNodeValue());
                    }

                    prop.description = propertyAttrMap.getNamedItem("description").getNodeValue(); 
                    System.out.print(", description=" + prop.description);

                    Node commentNode = propertyAttrMap.getNamedItem("comment");
                    if (commentNode == null) {
                        System.out.println(", comment=null");
                    } else {
                        prop.comment = commentNode.getNodeValue();
                        System.out.println(", comment=" + prop.comment);
                    }

                    prop.originalValue = propertiesInDb.get(prop.name);

                    propList.add(prop);
                }
            }
            return propMap;

        } catch (SAXParseException e) {
            // Use the contained exception, if any
            Exception x = e;
            if (e.getException() != null)
                x = e.getException();
            app.showExceptionInfo("解析錯誤", "檔案\"property.xml\"的第"
                + e.getLineNumber() + "行有問題 ", x);
            // "uri: " + spe.getSystemId(), spe);

        } catch (SAXException e) {
            // Error generated during parsing
            Exception x = e;
            if (e.getException() != null)
                x = e.getException();
            app.showExceptionInfo("解析錯誤", "檔案\"property.xml\"解析錯誤", x);

        } catch (ParserConfigurationException e) {
            // Parser with specified options can't be built
            app.showExceptionInfo("解析錯誤", "檔案\"property.xml\"解析錯誤", e);

        } catch (IOException e) {
            // I/O error
            app.showExceptionInfo("解析錯誤", "檔案\"property.xml\"讀取錯誤", e);

        } catch (NumberFormatException e) {
            // Parser with specified options can't be built
            app.showExceptionInfo("解析錯誤", "檔案\"property.xml\"解析錯誤", e);

        } catch (NullPointerException e) {
            // Parser with specified options can't be built
            app.showExceptionInfo("解析錯誤", "檔案\"property.xml\"解析錯誤", e);
        }
       
        return null;
    }
    
    private static boolean loadPOSPropertiesInDb() {
        try {
            //ResultSet rs = Util.query("SELECT name, value FROM property");
            ResultSet rs = Util.queryByNamedQuery("appProperties.ApplicationProperties");

            if (rs == null)
                return false;
            while (rs.next())
                propertiesInDb.put(rs.getString(1), rs.getString(2));
            rs.close();
            // rs.getStatement().close();
            return true;
        } catch (SQLException e) {
            app.showExceptionInfo("数据读取错误", "property表读取错误", e);
            return false;
        }
    }
}
