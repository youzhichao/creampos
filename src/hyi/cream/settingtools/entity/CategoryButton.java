package hyi.cream.settingtools.entity;

import hyi.cream.settingtools.NamedQuery;
import hyi.cream.settingtools.PosSettingApplication;
import hyi.cream.settingtools.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.RGB;


public class CategoryButton {
    static private int currentMaxId;
    
    private int id;
    private String name;
    private RGB color;
    private int itemPageStartIndex;
    private ButtonLevel currentButtonLevel;

    private List<ButtonLevel> buttonLevels = new ArrayList<ButtonLevel>();

    public RGB getColor() {
        return color;
    }

    public void setColor(RGB color) {
        this.color = color;
    }

    public String getColorText() {
        return getColor().toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        currentMaxId = Math.max(currentMaxId, id);
    }

    static public int getCurrentMaxId() {
        return currentMaxId;
    }
    
    public int getItemPageStartIndex() {
        return itemPageStartIndex;
    }

    public void setItemPageStartIndex(int itemPageStartIndex) {
        this.itemPageStartIndex = itemPageStartIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        return getName();
    }

    public List<ButtonLevel> getButtonLevels() {
        return buttonLevels;
    }

    public ButtonLevel createNewButtonLevel() {
        ButtonLevel buttonLevel = new ButtonLevel();
        getButtonLevels().add(buttonLevel);
        return buttonLevel;
    }

    public boolean loadItemButtonData(int orgID) {
        try {
            int startLevel = getItemPageStartIndex();
            
            for (;; startLevel++) {
                //ResultSet rs = Util.query("SELECT className FROM buttonsetting"
                //    + " WHERE level=" + startLevel + " AND row=100 AND col=100");
                ResultSet rs;
                if (NamedQuery.isRunAtStore())
                    rs = Util.queryByNamedQuery("buttonsetting.CategoryNameInGivenLevel", startLevel);
                else
                    rs = Util.queryByNamedQuery("buttonsetting.CategoryNameInGivenLevel", orgID, startLevel);
                
                if (rs == null || !rs.next())
                    break;
                if (compareCategroyName(getName(), rs.getString(1)) != 0)
                    break;
                rs.close();

                //rs = Util.query("SELECT row,col,label,red,green,blue,arg1"
                //    + " FROM buttonsetting"
                //    + " WHERE level=" + startLevel + " AND row!=100 ORDER BY row,col");
                if (NamedQuery.isRunAtStore())
                    rs = Util.queryByNamedQuery("buttonsetting.ItemButtonsInGivenLevel", startLevel);
                else
                    rs = Util.queryByNamedQuery("buttonsetting.ItemButtonsInGivenLevel", orgID, startLevel);
                ButtonLevel buttonLevel = null;
                while (rs.next()) {
                    if (buttonLevel == null)
                        buttonLevel = createNewButtonLevel();
                    Item item = new Item(rs.getString(7),   // id
                        rs.getString(3),                    // name
                        new RGB(rs.getInt(4), rs.getInt(5), rs.getInt(6))); // color
                    int row = rs.getInt(1);
                    int col = rs.getInt(2);
                    buttonLevel.setItem(row, col, item);
                }
                rs.close();
                if (getCurrentButtonLevel() == null)
                    setCurrentButtonLevel(buttonLevel);
            }
            return true;
        } catch (SQLException e) {
            PosSettingApplication.getInstance().showExceptionInfo(
                "数据读取错误", "buttonsetting表读取错误", e);
            return false;
        }
    }

    private int compareCategroyName(String string1, String string2) {
        // drop out "^" before comparing them
        return string1.trim().replaceAll("\\^", "").compareTo(
            string2.trim().replaceAll("\\^", ""));
    }

    public ButtonLevel getCurrentButtonLevel() {
        return currentButtonLevel;
    }

    public void setCurrentButtonLevel(ButtonLevel currentButtonLevel) {
        this.currentButtonLevel = currentButtonLevel;
    }

    /** Try to move to next button level. */
    public ButtonLevel nextButtonLevel() {
        if (currentButtonLevel == null || buttonLevels.size() == 0)
            return null;
        int idx = buttonLevels.indexOf(currentButtonLevel);
        if (idx >= buttonLevels.size() - 1) {
            if (!currentButtonLevel.isPageFull()) {
                Util.playExclamationSound();
                return null;   // reach last page
            } else {
                // create a new page
                ButtonLevel newButtonLevel = createNewButtonLevel();
                setCurrentButtonLevel(newButtonLevel);
                return newButtonLevel;
            }
        }
        ButtonLevel nextButtonLevel = buttonLevels.get(idx + 1);
        setCurrentButtonLevel(nextButtonLevel);
        return nextButtonLevel;
    }

    /** Try to move to previous button level. */
    public ButtonLevel previousButtonLevel() {
        if (currentButtonLevel == null || buttonLevels.size() == 0)
            return null;
        int idx = buttonLevels.indexOf(currentButtonLevel);
        if (idx == 0) {
            Util.playExclamationSound();
            return null;   // reach last page
        }
        ButtonLevel c = buttonLevels.get(idx - 1);
        setCurrentButtonLevel(c);
        
        return c;
    }

    public void populateItems(List<Item> itemList) {
        if (itemList == null)
            return;
        buttonLevels.clear();
        int r = 0;
        int c = 0;
        ButtonLevel buttonLevel = createNewButtonLevel();
        setCurrentButtonLevel(buttonLevel);
        for (Item item : itemList) {
            if (r >= 4) {
                buttonLevel = createNewButtonLevel();
                r = c = 0;
            }
            item.setColor(getColor());  // use category button's color initially
            buttonLevel.setItem(r, c, item);
            c++;
            if (c >= 4) {
                r++;
                c = 0;
            }
        }
    }

    public void populateItemsByAppending(List<Item> itemList) {
        if (itemList == null || itemList.size() == 0)
            return;

        ButtonLevel buttonLevel;
        int r = 0;
        int c = 0;
        if (buttonLevels.size() == 0) {
            buttonLevel = createNewButtonLevel();
        } else {
            buttonLevel = buttonLevels.get(buttonLevels.size() - 1);    // last page
            int[] rowCol = new int[2];
            if (buttonLevel.findLastEmptyCell(rowCol)) {
                r = rowCol[0];
                c = rowCol[1];
            } else {
                buttonLevel = createNewButtonLevel();
            }
        }
        setCurrentButtonLevel(buttonLevel);
        for (Item item : itemList) {
            if (r >= 4) {
                buttonLevel = createNewButtonLevel();
                r = c = 0;
            }
            item.setColor(getColor());  // use category button's color initially
            buttonLevel.setItem(r, c, item);
            c++;
            if (c >= 4) {
                r++;
                c = 0;
            }
        }
    }

    public void removeItemFromCurrentButtonLevel(int row, int col) {
        ButtonLevel buttonLevel = getCurrentButtonLevel();
        if (buttonLevel == null)
            return;
        int idx = buttonLevels.indexOf(buttonLevel);
        buttonLevel.removeItem(row, col);
        while (true) {
            if (idx >= buttonLevels.size() - 1) {
                buttonLevel.getItems()[3][3] = null; // remove last one
                break;
            }
            ButtonLevel nextButtonLevel = buttonLevels.get(++idx);
            buttonLevel.getItems()[3][3] = nextButtonLevel.getItems()[0][0]; // move next-first to prev-last
            nextButtonLevel.removeItem(0, 0);
            buttonLevel = nextButtonLevel;
        }
    }

    public void insertItemToCurrentButtonLevel(int row, int col, Item item) {
        ButtonLevel buttonLevel = getCurrentButtonLevel();
        if (buttonLevel == null)
            return;

        boolean isTargetButtonEmpty = buttonLevel.isItemEmpty(row, col);
        if (isTargetButtonEmpty) {
            buttonLevel.getItems()[row][col] = item;
            return;
        }
        
        int idx = buttonLevels.indexOf(buttonLevel);
        Item lastItem = buttonLevel.rightShift(row, col);
        buttonLevel.getItems()[row][col] = item;
        while (true) {
            if (idx >= buttonLevels.size() - 1) {
                if (lastItem != null) {
                    ButtonLevel newButtonLevel = createNewButtonLevel();
                    newButtonLevel.getItems()[0][0] = lastItem;
                }
                return;
            }
            buttonLevel = buttonLevels.get(++idx);
            Item lastLastItem = buttonLevel.rightShift(0, 0);
            buttonLevel.getItems()[0][0] = lastItem;
            lastItem = lastLastItem;
        }
    }
    
    public void moveItemButton(int fromRow, int fromCol, int toRow, int toCol) {
        ButtonLevel buttonLevel = getCurrentButtonLevel();
        if (buttonLevel == null)
            return;
        buttonLevel.moveItemButton(fromRow, fromCol, toRow, toCol);
    }
}
