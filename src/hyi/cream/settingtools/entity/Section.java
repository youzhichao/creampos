package hyi.cream.settingtools.entity;

public class Section implements Comparable<Section> {
    public String name;
    public String description;

    public Section(String name, String description) {
        this.name = name;
        this.description = description;
    }
    
    @Override
    public String toString() {
        return "Section:" + name + "(" + description + ")";
    }

    public int compareTo(Section o) {
        return name.compareTo(o.name);
    }
}
