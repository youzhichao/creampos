package hyi.cream.settingtools.entity;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

public class ItemButton extends CLabel {

    private boolean selected;
    private int row;
    private int col;
    
    public ItemButton(Composite parent, int style, int row, int col) {
        super(parent, style);
        this.row = row;
        this.col = col;
        addPaintListener(new PaintListener() {
            public void paintControl(PaintEvent event) {
                onPaintTouchButton(event);
            }
        });
    }
    
    @Override
    public void setText(String text) {
        int lineLen = 0;
        StringBuffer sb = new StringBuffer(text.length() + 3);
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c == '^' || c == '\n' || c == '\r')
                lineLen = -1;
            if ((int)c > 255)   // Chinese word
                lineLen += 2;
            else
                lineLen++;
            sb.append(c);
            if (lineLen >= 8 && i != text.length() - 1) {
                sb.append('\n');
                lineLen = 0;
            }
        }
        super.setText(sb.toString());
    }

    void onPaintTouchButton(PaintEvent event) {
        if (!selected)
            return;

        Rectangle r = getClientArea();
        if (r.width == 0 || r.height == 0) return;

        GC gc = event.gc;
        Display disp= getDisplay();

        Color c1 = null;
        Color c2 = null;
        c1 = disp.getSystemColor(SWT.COLOR_BLACK);
        c2 = disp.getSystemColor(SWT.COLOR_BLACK);
        if (c1 != null && c2 != null) {
            gc.setLineWidth(3);
            drawBevelRect(gc, r.x, r.y, r.width-1, r.height-1, c1, c2);
        }
    }

    private void drawBevelRect(GC gc, int x, int y, int w, int h, Color topleft, Color bottomright) {
        gc.setForeground(bottomright);
        gc.drawLine(x+w, y,   x+w, y+h);
        gc.drawLine(x,   y+h, x+w, y+h);
        
        gc.setForeground(topleft);
        gc.drawLine(x, y, x+w-1, y);
        gc.drawLine(x, y, x,     y+h-1);
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        redraw();
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }
}
