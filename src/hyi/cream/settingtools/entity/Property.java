package hyi.cream.settingtools.entity;

import hyi.cream.settingtools.ControlType;

import org.eclipse.swt.widgets.Control;


public class Property {
    public ControlType controlType;
    public String name;
    public String defaultValue;
    public int width;
    public String description;
    public String comment;
    public String[] possibleValues;    // for ControlType.ComboBox
    
    public String originalValue;       // null means <default>
    public String updatedValue;

    public Control control;            // associated SWT control

    public String getComment() {
        return (comment == null) ? "" : comment;
    }

    public String getDefaultValue() {
        return (defaultValue == null) ? "" : defaultValue;
    }
}
