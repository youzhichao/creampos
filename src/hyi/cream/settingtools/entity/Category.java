package hyi.cream.settingtools.entity;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private String id;
    private String name;
    private List<Item> itemList = new ArrayList<Item>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getId() + " " + getName();
    }

    public void addItem(Item item) {
        itemList.add(item);
    }
    
    public List<Item> getItemList() {
        return itemList;
    }
}
