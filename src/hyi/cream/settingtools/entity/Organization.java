package hyi.cream.settingtools.entity;

public class Organization {
    int orgID;
    String orgNo;
    String orgName;
    String telNo;
    String lineNo;
    String ipAddr;
    int buttonCount;

    public Organization() {
    }

    public Organization(int i, String no, String string) {
        orgID = i;
        orgNo = no;
        orgName = string;
    }

    public int getButtonCount() {
        return buttonCount;
    }

    public void setButtonCount(int buttonCount) {
        this.buttonCount = buttonCount;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public String getLineNo() {
        return lineNo;
    }

    public void setLineNo(String lineNo) {
        this.lineNo = lineNo;
    }

    public int getOrgID() {
        return orgID;
    }

    public void setOrgID(int orgID) {
        this.orgID = orgID;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }
    
    @Override
    public String toString() {
        return getOrgNo() + " " + getOrgName();
    }

    @Override
    public boolean equals(Object obj) {
        return (obj != null && obj instanceof Organization 
            && getOrgID() == ((Organization)obj).getOrgID());
    }
    
    @Override
    public int hashCode() {
        return getOrgID();
    }
}
