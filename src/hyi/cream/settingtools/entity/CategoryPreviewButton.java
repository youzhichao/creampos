package hyi.cream.settingtools.entity;

import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.widgets.Composite;

public class CategoryPreviewButton extends CLabel {

    public CategoryPreviewButton(Composite parent, int style) {
        super(parent, style);
    }

    @Override
    public void setText(String text) {
        int lineLen = 0;
        StringBuffer sb = new StringBuffer(text.length() + 3);
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c == '^' || c == '\n' || c == '\r')
                lineLen = -1;
            if ((int)c > 255)   // Chinese word
                lineLen += 2;
            else
                lineLen++;
            sb.append(c);
            if (lineLen >= 6 && i != text.length() - 1) {
                sb.append('\n');
                lineLen = 0;
            }
        }
        super.setText(sb.toString());
    }
}
