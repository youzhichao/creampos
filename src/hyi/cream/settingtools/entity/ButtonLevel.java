package hyi.cream.settingtools.entity;

public class ButtonLevel {
    private int levelNumber;
    private Item[][] items = new Item[4][4];

    public Item[][] getItems() {
        return items;
    }

    public void setItem(int row, int col, Item item) {
        if (row >= 0 && row < 4 && col >= 0 && col < 4) {
            items[row][col] = item;
        }
    }

    public int getLevelNumber() {
        return levelNumber;
    }

    public void setLevelNumber(int levelNumber) {
        this.levelNumber = levelNumber;
    }

    public boolean isPageFull() {
        for (int r = 3; r >= 0; r--)
            for (int c = 3; c >= 0; c--) {
                if (items[r][c] == null)
                    return false;
            }
        return true;
    }
    
    public boolean findLastEmptyCell(int[] returnedRowCol) {
        boolean found = false;
        returnedRowCol[0] = -1;     // row
        returnedRowCol[1] = -1;     // col
        for (int r = 3; r >= 0; r--)
            for (int c = 3; c >= 0; c--) {
                if (items[r][c] == null) {
                    returnedRowCol[0] = r;
                    returnedRowCol[1] = c;
                    found = true;
                } else
                    return found;
            }
        return found;
    }

    public void removeItem(int row, int col) {
        int lr = row;
        int lc = col;
        int c = col + 1;
        for (int r = row; r < 4; r++) {
            for (; c < 4; c++) {
                items[lr][lc] = items[r][c];
                lc = c;
                lr = r;
            }
            c = 0;
        }
    }

    public boolean isItemEmpty(int row, int col) {
        if (items[row][col] == null)
            return true;
        String id = items[row][col].getId();
        String name = items[row][col].getName();
        if ((id == null || id.trim().length() == 0)
            && (name == null || name.trim().length() == 0))
            return true;
        return false;
    }

    public void moveItemButton(int fromRow, int fromCol, int toRow, int toCol) {
        if (fromRow == toRow && fromCol == toCol)
            return;
        boolean isTargetButtonEmpty = isItemEmpty(toRow, toCol);
        if (isTargetButtonEmpty) {
            items[toRow][toCol] = items[fromRow][fromCol];
            //removeItem(fromRow, fromCol);
            items[fromRow][fromCol] = null;
        } else {
            Item fromItem = items[fromRow][fromCol];
            int lr = fromRow;
            int lc = fromCol;
            if (isToBeforeFrom(fromRow, fromCol, toRow, toCol)) {
                int c = fromCol - 1;
                for (int r = fromRow; r >= toRow; r--) {
                    for (; ((r == toRow) ? c >= toCol : c >= 0); c--) {
                        items[lr][lc] = items[r][c];
                        lr = r;
                        lc = c;
                    }
                    c = 3;
                }
            } else {
                int c = fromCol + 1;
                for (int r = fromRow; r <= toRow; r++) {
                    for (; ((r == toRow) ? c <= toCol : c < 4); c++) {
                        items[lr][lc] = items[r][c];
                        lr = r;
                        lc = c;
                    }
                    c = 0;
                }
            }
            items[toRow][toCol] = fromItem;
        }
    }

    private boolean isToBeforeFrom(int fromRow, int fromCol, int toRow, int toCol) {
        if (fromRow > toRow)
            return true;
        if (fromRow == toRow && fromCol > toCol)
            return true;
        return false;
    }

    public Item rightShift(int row, int col) {
        Item lastItem = items[3][3];
        int lr = 3;
        int lc = 3;
        int c = 2;
        for (int r = 3; r >= row; r--) {
            for (; ((r == row) ? c >= col : c >= 0); c--) {
                items[lr][lc] = items[r][c];
                lr = r;
                lc = c;
            }
            c = 3;
        }
        return lastItem;
    }
}
