package hyi.cream.settingtools.entity;

import org.eclipse.swt.graphics.RGB;

public class Item {
    private String id;
    private String name;
    private RGB color;

    public Item(String id, String name, RGB color) {
        this.id = id;
        this.name = name;
        this.color = color;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getId() + " " + getName();
    }

    public RGB getColor() {
        return color;
    }

    public void setColor(RGB color) {
        this.color = color;
    }

    public boolean isEmpty() {
        return (id == null || id.trim().length() == 0 ||
            name == null || name.trim().length() == 0);
    }
}
