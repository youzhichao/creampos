package hyi.cream;

import hyi.cream.autotest.AutoTester;
import hyi.cream.dac.*;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.event.SystemInfoEvent;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.groovydac.Param;
import hyi.cream.groovydac.SsmpLog;
import hyi.cream.gwt.JettyStarter;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.inline.MasterDownloadThread;
import hyi.cream.inline.Server;
import hyi.cream.state.GetProperty;
import hyi.cream.state.StateMachine;
import hyi.cream.uibeans.*;
import hyi.cream.uibeans.customer.CustPayingPaneBanner;
import hyi.cream.uibeans.duo2.SecondScreen;
import hyi.cream.uibeans.touch.TouchPane;
import hyi.cream.util.*;
import hyi.spos.*;
import hyi.spos.Scanner;
import hyi.spos.events.StatusUpdateEvent;
import protector.Protector;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.*;
import java.util.List;
import java.util.Timer;

/**
 * POSTerminalApplication. Main class of this POS application.
 *
 * @author Bruce
 */
public class POSTerminalApplication {

    private static final long serialVersionUID = 1L;

    private static boolean fullScreen;
    private static Dimension screenSize;
    private static final boolean DEBUG = false;

    public static Object waitForTransactionStored = new Object();
    private static POSTerminalApplication posTerminalApplication = new POSTerminalApplication();
    private static String newCashierID; // 纪录高权限用户 add by lxf 2003.02.12
    private static java.util.Map scaleBarCodesFormatMap;

    // 2005-07-01 把版本信息移入Version.java中
    //private static ThreadWatchDog dog = ThreadWatchDog.getInstance();
    private ThreadWatchDog dog = null; // delay create instance

    private Frame mainFrame;
    private Container contentPane;
    private final Param param = Param.getInstance();
    private StateMachine stateMachine;
    private POSPeripheralHome3 posPeripheralHome;
    private Transaction currentTransaction;
    private Transaction blankTransaction;
    private Transaction clonedBlankTransaction;
    private List transactionListener;
    private SystemInfo systemInfo;
    private POSButtonHome2 posButtonHome;
    private ScreenBanner screenBanner;
    private Indicator messageIndicator;
    private Indicator warningIndicator;
    private Indicator informationIndicator;
    private ItemList itemList;
    private DacViewer dacViewer;
    private PopupMenuPane popupMenuPane;
    private SignOffForm signOffForm;
    private CreditCardForm creditCardForm;
    private boolean buyerNumberPrinted;
    private boolean trainingMode = false;
    private boolean keyState;
    private boolean returnItemState;
    private java.util.ResourceBundle res = CreamToolkit.GetResource();
    private String cashierNumber = "";
    private boolean transactionEnd = true;
    private int keyPosition = 0;
    private boolean beginState = true;
    private boolean isAllReturn = false;
    private boolean checked = false;
    private boolean salemanChecked = false;
    private boolean scanCashierNumber = false;
    private boolean scanSalemanNumber = false;
    private HYIDouble returnQty = new HYIDouble(0);
    // private Panel backgroudPane;
    private EPOSBackground posBackgroundPanel;
    // private boolean firstPaint = true;

    private Banner connectMsgBanner;
    private Banner pageMsgBanner;
    // private Banner totalMsgBanner;
    Panel totalMsgPanel = new Panel();
    private ProductInfoBanner productInfoBanner;
    private TotalAmountBanner totalAmountBanner;
    private Banner screenBanner2;
    private boolean payingPaneVisible = false;
    private PayingPane payingPane;
    private PayingPaneBanner payingPane1;
    private PayingPaneBanner payingPane2;

    // private CustomerPanel customerPanel;
//    private Panel customerPanel;
//    private CustomerBanner timeBanner;
//    private CustPayingPaneBanner custPayingPane;
//    private CustProductInfoBanner custProductInfo;
//    private CustomerBanner custAdvertWord;
//    private CustomerTextBanner custWelcome;
//    private CustAnimatedPane animatedPane;
//    // private CustIndicator custInfoIndicator;
//    //private DigPrize digPrize;
//    // private TextField tf;
//    private Media media;
//    private TouchButton touchButton1;
//    private TouchButton touchButton2;

    private boolean isReturnFromSC = false; //退货数据是否从sc来
    // private FunPanel adPanel;
    private boolean crdPaymentMenuVisible = false; // 信用卡支付菜单是否可以显示
    private boolean enableKeylock = true; // keylock失效
    private String lastInvoiceID = "";
    private String lastInvoiceNumber = "";
    private boolean isAlipay = false;//是否是支付宝
    private boolean isWeiXin = false;//是否是微信
    private boolean isUnionPay = false;//是否是银联
    private boolean shift2 = true;//是否扫描日结了
    private String selfBuy = "";
    private JPanel buttonPanel = new JPanel();
    private CardLayout buttonCardLayout = new CardLayout();

    private JPanel buttonPanel1 = new JPanel();

    private JPanel buttonPanel2 = new JPanel();

    private DynaPayingPaneBanner dynaPayingPane;

    /**
     * Singleton getter.
     */
    public static POSTerminalApplication getInstance() {
        if (posTerminalApplication == null)
            posTerminalApplication = new POSTerminalApplication();
        return posTerminalApplication;
    }

    /**
     * Constructor of POSTerminalApplication.
     */
    private POSTerminalApplication() {
        setScanCashierNumber(param.getScanCashierNumber());
        setScanSalemanNumber(param.getScanSalemanNumber());
        posTerminalApplication = this;
    }

    public static String getVersion() {
        return Version.getVersion();
    }

    /**
     * 纪录高权限用户. Added by lxf 2003-02-12
     */
    public static String getNewCashierID() {
        return newCashierID;
    }

    public static void setNewCashierID(String id) {
        newCashierID = id;
    }

    public static boolean isRecalcMode = false;

    private void recalc() {
        Object[] rtn = getRecalParameter();
        if (!((Boolean)rtn[0]).booleanValue())
            System.exit(0);
        isRecalcMode = true;
        recalcFromTransactions((String)rtn[1], (String)rtn[2],
            (Date)rtn[3]);
    }

    private void createPeripherals() {
        // MSR
        try {
            MSR msr = posPeripheralHome.getMSR();
            msr.claim(JposConst.JPOS_FOREVER);
            msr.setDeviceEnabled(true);
            msr.setDataEventEnabled(true);
            msr.addDataListener(posPeripheralHome);
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }

        // Scanner
        try {
            Scanner scanner = posPeripheralHome.getScanner();
            scanner.claim(JposConst.JPOS_FOREVER);
            scanner.setDeviceEnabled(true);
            scanner.setDataEventEnabled(true);
            //scanner.setDecodeData(true);
            scanner.addDataListener(posPeripheralHome);
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }

        // print
        try {
            POSPrinter print = posPeripheralHome.getPOSPrinter();
            print.claim(JposConst.JPOS_FOREVER);
            print.setDeviceEnabled(true);
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }

        // Keylock
        try {
            Keylock keylock = posPeripheralHome.getKeylock();
            keylock.setDeviceEnabled(true);
            keylock.addStatusUpdateListener(posPeripheralHome);
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }

        // LineDisplay
        try {
            LineDisplay lineDisplay = posPeripheralHome.getLineDisplay();
            lineDisplay.claim(JposConst.JPOS_FOREVER);
            lineDisplay.setDeviceEnabled(true);
            lineDisplay.clearText();
            String str = param.getWelcomeMessage();
            if (!str.trim().equals(""))
                lineDisplay.displayText(str, 1);
        } catch (Exception ex) {
            CreamToolkit.logMessage(ex.toString());
        }
    }

    public void layoutUIBeans() {
        EPOSBackground ePosBackground = EPOSBackground.getInstance();
        if (screenBanner != null) {
            screenBanner.setBounds(
                ePosBackground.screenBannerX + 2,
                ePosBackground.screenBannerY, EPOSBackground
                    .getInstance().screenBannerW - 2, EPOSBackground
                    .getInstance().screenBannerH - 2);
            screenBanner.repaint();
        }
        if (payingPane != null) {
            payingPane.setBounds(
                ePosBackground.payingBannerX + 2,
                ePosBackground.payingBannerY, EPOSBackground
                    .getInstance().payingBannerW - 2, EPOSBackground
                    .getInstance().payingBannerH);
            payingPane.repaint();
        }
        if (payingPane1 != null) {
            payingPane1.setBounds(
                ePosBackground.payingBanner1X + 2,
                ePosBackground.payingBanner1Y, EPOSBackground
                    .getInstance().payingBanner1W - 2, EPOSBackground
                    .getInstance().payingBanner1H);
            payingPane1.repaint();
        }

        if (dynaPayingPane != null) {
            dynaPayingPane.setBounds(
                ePosBackground.payingBanner1X + 2,
                ePosBackground.payingBanner1Y, EPOSBackground
                    .getInstance().payingBanner1W - 2, EPOSBackground
                    .getInstance().payingBanner1H);
            dynaPayingPane.repaint();
        }

        if (payingPane2 != null) {
            payingPane2.setBounds(
                ePosBackground.payingBanner2X + 2,
                ePosBackground.payingBanner2Y, EPOSBackground
                    .getInstance().payingBanner2W - 2, EPOSBackground
                    .getInstance().payingBanner2H);
            payingPane2.repaint();
        }
        if (itemList != null) {
            itemList.setBounds(ePosBackground.itemListX,
                ePosBackground.itemListY, EPOSBackground
                    .getInstance().itemListW, EPOSBackground
                    .getInstance().itemListH);
            dacViewer.setBounds(ePosBackground.itemListX,
                ePosBackground.itemListY, EPOSBackground
                    .getInstance().itemListW, EPOSBackground
                    .getInstance().itemListH);

            if (GetProperty.isEnableTouchPanel()) {
                TouchPane.getInstance().setBounds(
                    ePosBackground.itemListX,
                    ePosBackground.itemListY,
                    EPOSBackground.getInstance().itemListW,
                    ePosBackground.productInfoY + ePosBackground.productInfoH
                        - ePosBackground.itemListY);
                TouchPane.getInstance().setBounds(ePosBackground.itemListX + 20,
                    ePosBackground.itemListY + 30, EPOSBackground.getInstance().itemListW,
                    EPOSBackground.getInstance().itemListH);
            }
            itemList.repaint();
        }

        if (informationIndicator != null) {
            informationIndicator.setBounds(
                ePosBackground.informationIndicatorX,
                ePosBackground.informationIndicatorY,
                ePosBackground.informationIndicatorW,
                ePosBackground.informationIndicatorH);
            informationIndicator.repaint();
        }
        if (warningIndicator != null) {
            warningIndicator.setBounds(
                ePosBackground.warningIndicatorX,
                ePosBackground.warningIndicatorY,
                ePosBackground.warningIndicatorW,
                ePosBackground.warningIndicatorH);
            warningIndicator.repaint();
        }
        if (messageIndicator != null) {
            messageIndicator.setBounds(
                ePosBackground.messageIndicatorX,
                ePosBackground.messageIndicatorY,
                ePosBackground.messageIndicatorW,
                ePosBackground.messageIndicatorH);
            messageIndicator.repaint();
        }
        if (connectMsgBanner != null) {
            connectMsgBanner.setBounds(
                ePosBackground.connectMessageX,
                ePosBackground.connectMessageY,
                ePosBackground.connectMessageW,
                ePosBackground.connectMessageH);
            connectMsgBanner.repaint();
        }
        if (pageMsgBanner != null) {
            pageMsgBanner.setBounds(
                EPOSBackground.getInstance().pageMessageX,
                EPOSBackground.getInstance().pageMessageY,
                EPOSBackground.getInstance().pageMessageW,
                EPOSBackground.getInstance().pageMessageH);
            pageMsgBanner.repaint();
        }

//        if (touchButton1 != null) {
//            touchButton1.setBounds(ePosBackground.pageMessageX +
//                ePosBackground.pageMessageW / 4 + 6,
//                ePosBackground.pageMessageY, (3 * EPOSBackground
//                    .getInstance().pageMessageW / 4 - 8) / 2, EPOSBackground
//                    .getInstance().pageMessageH + 6);
//            touchButton1.repaint();
//            touchButton2.setBounds(ePosBackground.pageMessageX +
//                ePosBackground.pageMessageW / 4 + 6
//                + (3 * EPOSBackground
//                .getInstance().pageMessageW / 4 - 6) / 2,
//                ePosBackground.pageMessageY, (3 * EPOSBackground
//                    .getInstance().pageMessageW / 4 - 6) / 2, EPOSBackground
//                    .getInstance().pageMessageH + 6);
//            touchButton2.repaint();
//        }

        if (productInfoBanner != null) {
            productInfoBanner.setBounds(
                ePosBackground.productInfoX + 2,
                ePosBackground.productInfoY, EPOSBackground
                    .getInstance().productInfoW - 2, EPOSBackground
                    .getInstance().productInfoH - 2);
            productInfoBanner.repaint();
        }
        if (totalAmountBanner != null) {
            totalAmountBanner.setBounds(
                ePosBackground.subtotalX + 3, EPOSBackground
                    .getInstance().subtotalY, EPOSBackground
                    .getInstance().subtotalW - 9, EPOSBackground
                    .getInstance().subtotalH - 6);
            totalAmountBanner.repaint();
        }
        if (screenBanner2 != null) {
            screenBanner2.setBounds(
                ePosBackground.screenBanner2X, EPOSBackground
                    .getInstance().screenBanner2Y, EPOSBackground
                    .getInstance().screenBanner2W, EPOSBackground
                    .getInstance().screenBanner2H);
            screenBanner2.repaint();
        }

//        if (GetProperty.getCreateAdPanel("no").equalsIgnoreCase("yes")) {
//            // animatedPane customerPanel.setBounds(384, 20, 640, 768);
//            int marginH = 5;
//            int marginW = 10;
//            int startx = marginW;
//            int starty = marginH;
//            int compW = customerPanel.getWidth() / 2;
//            int compH = customerPanel.getHeight() - 2 * marginH;
//            int averageH = (compH - 5 * marginH) / 14;
//
//            starty = customerPanel.getHeight() - 2 * averageH;
//            compH = 2 * averageH;
//            if (custAdvertWord != null) {
//                custAdvertWord.setBounds(startx, starty, customerPanel.getWidth(), compH);
//                custAdvertWord.repaint();
//            }
//
//            starty = marginH;
//            compH = customerPanel.getHeight() - 2 * marginH - 2 * averageH;
////			if (digPrize != null) {
////				digPrize.setBounds(startx, starty, compW, compH);
////				digPrize.setVisible(false);
////				digPrize.validate();
////				digPrize.repaint();
////			}
//
//            if (media != null) {
//                media.setBounds(startx, starty, compW, compH);
////				media.start();
//                // media.setVisible(true);
//                media.validate();
//            }
//
//            if (animatedPane != null) {
//                // animatedPane.setBackground(Color.black);
//                // animatedPane.setBounds(startx, starty, compW, compH);
//                // animatedPane.repaint();
//            }
//
//            // timebanne
//            startx += compW + marginW;
//            starty += marginH;
//            compW = customerPanel.getWidth() - compW - 3 * marginW;
//            compH = averageH;
//            if (timeBanner != null) {
//                timeBanner.setBounds(startx, starty, compW, compH);
//                timeBanner.repaint();
//            }
//
//            starty += compH + marginH;
//            compH = 4 * averageH;
//            if (custProductInfo != null) {
//                custProductInfo.setBounds(startx, starty, compW, compH);
//                custProductInfo.repaint();
//            }
//
//            starty += compH + marginH;
//            compH = 3 * averageH;
//            if (custWelcome != null) {
//                custWelcome.setBounds(startx, starty, compW, compH);
//                custWelcome.repaint();
//            }
//
//            starty += compH + marginH;
//            compH = 4 * averageH;
//            if (custPayingPane != null) {
//                custPayingPane.setBounds(startx, starty, compW, compH);
//                custPayingPane.repaint();
//            }
//
////			starty += compH + marginH;
////			compH = 2 * averageH;
////			if (custAdvertWord != null) {
////				custAdvertWord.setBounds(startx, starty, compW, compH);
////				custAdvertWord.repaint();
////			}
//
//        }
    }

    private Cursor createCursor() {
        boolean showCursor = param.getShowMouseCursor();
        Cursor cursor;
        if (showCursor)
            cursor = new Cursor(Cursor.DEFAULT_CURSOR);
        else
            cursor = Toolkit.getDefaultToolkit().createCustomCursor(
                Toolkit.getDefaultToolkit().createImage(new byte[0]),
                new Point(), "");
        return cursor;
    }

    /**
     * 在獨立運行的POS中，mainFrame就是最外層的Frame。如果是embedded in SC client，
     * mainFrame是SC client的MF (ref. SC's POSTerminalViewer)。
     */
    public Frame getMainFrame() {
        if (mainFrame == null) {
            POSTerminalApplication posTerminalApplication = POSTerminalApplication.getInstance();
            if(!posTerminalApplication.selfBuy.equals("S")) {
                mainFrame = new Frame("Hongyuan ePOS") {
                    @Override
                    public void update(Graphics g) {
                        paint(g);
                    }
                };
            }

            // determine frame size
            resizeFrame(mainFrame);
        }
        mainFrame.setTitle(Param.getInstance().getApplicationTitle());
        //mainFrame.setCursor(app.createCursor());
        //mainFrame.setLayout(new BorderLayout());
        return mainFrame;
    }

    public void setMainFrame(Frame mainFrame) {
        this.mainFrame = mainFrame;
    }

    /**
     * 在獨立運行的POS中，contentPane就是最外層的Frame。如果是embedded in SC client，
     * contentPane是POSTerminalViewer (ref. SC's POSTerminalViewer)。
     */
    public Container getContentPane() {
        if (contentPane == null)
            contentPane = getMainFrame();

        return contentPane;
    }

    public void setContentPane(Container contentPane) {
        this.contentPane = contentPane;
    }

    private void showSecondScreen() {
        //客戶顯示螢幕.
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        System.out.println("device count:" + gs.length);
        for (int j = 0; j < gs.length; j++) {
            GraphicsDevice gd = gs[j];
            System.out.println("device id:" + gd.getIDstring());
            if (gd.getIDstring().equals(":0.1")) {
                //CustDispFrame secondScreen = new CustDispFrame("HYI_CUST_FRAME", gd.getDefaultConfiguration());
                SecondScreen secondScreen = new SecondScreen("HYI_CUST_FRAME", gd.getDefaultConfiguration());
                System.out.println("secondScreen = " + secondScreen);
                secondScreen.setDefaultLookAndFeelDecorated(false);
                secondScreen.setUndecorated(true);
                if (gd.isFullScreenSupported())
                    gd.setFullScreenWindow(secondScreen);

                secondScreen.setVisible(true);
                secondScreen.playVideo();
                System.out.println("addTransactionListener()");
                //secondScreen.playFlash();
                //設定TransactionListener.
                POSTerminalApplication.getInstance().getCurrentTransaction().addTransactionListener(secondScreen);
            }
        }
    }

    private void startup() {

        // Startup a inline client
        new Client(Param.getInstance().getScIpAddress(), Server.INLINE_SERVER_TCP_PORT);

        if (Param.getInstance().isUseRuok()) {
            // The dog will be watching on the threads,
            // if the thread is dead, then the dog will restart it.
            dog = ThreadWatchDog.getInstance();
            ThreadWatchDog.setLogger(CreamToolkit.getLogger());
            dog.put(Client.getInstance());
            dog.start();
        }

        contentPane = getContentPane();

        CreamCache.getInstance().buildPLUObjectDB();

        // Create POSPeripheralHome
        posPeripheralHome = POSPeripheralHome3.getInstance();

        // Preparing peripherals...
        createPeripherals();

        // Create POSButtonHome
        posButtonHome = POSButtonHome2.getInstance();

        Panel posPanel = new Panel();

        try {
            Cursor cursor = createCursor();

            contentPane.setLayout(new BorderLayout());

            contentPane.add(posPanel, BorderLayout.CENTER);

            posBackgroundPanel = EPOSBackground.getInstance();
            posBackgroundPanel.setEnablePainting(true);
            posBackgroundPanel.setLayout(null);
            posPanel.setLayout(new BorderLayout());
            posPanel.add(posBackgroundPanel, BorderLayout.CENTER);

            if (param.getCreateButtonPanel()) {
                //getContentPane().add(createButtonPanel(), BorderLayout.SOUTH);
                contentPane.add(createButtonPanel(), BorderLayout.SOUTH);
            }

            contentPane.setVisible(true);
            waitForEPOSBackgroundPaint();

            if (DEBUG)
                System.out.println("PAINT> EPOSBackround painted.");

            // Creating some UIBeans...
            popupMenuPane = PopupMenuPane.getInstance();
            popupMenuPane.setCursor(cursor);
            popupMenuPane.centerPopupMenu();
            // lineItemIndicator = new LineItemIndicator();
            // lineItemIndicator.setCursor(cursor);
            messageIndicator = new Indicator();
            messageIndicator.setCursor(cursor);
            warningIndicator = new Indicator(Color.red);
            warningIndicator.setCursor(cursor);
            informationIndicator = new Indicator(Indicator.SCROLL_MARQUEE2);
            informationIndicator.setCursor(cursor);
            itemList = new ItemList();
            itemList.setCursor(cursor);
            dacViewer = new DacViewer();
            dacViewer.setCursor(cursor);

            if (param.isUseCashForm()) {
                signOffForm = SignOffForm.getInstance();
                signOffForm.setCursor(cursor);
            }
            creditCardForm = CreditCardForm.getInstance();
            creditCardForm.setCursor(cursor);

            systemInfo = new SystemInfo();

            // Create current Transaction object
            createCurrentTransaction();

//			TouchPane.getInstance();
            // ScreenBanner
            systemInfo.setTransaction(currentTransaction);
            currentTransaction.addTransactionListener(systemInfo);
            screenBanner = new ScreenBanner(EPOSBackground.getInstance()
                .getTextBackgroundColor(), Color.blue.darker().darker(),
                Color.black);
            screenBanner.setCursor(cursor);
            screenBanner.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(screenBanner);
            posBackgroundPanel.add(screenBanner);

            // ItemList
            itemList.setSelectionMode(ItemList.SINGLE_SELECTION);
            currentTransaction.addTransactionListener(itemList);
            itemList.setTransaction(currentTransaction);
            posBackgroundPanel.add(itemList);
            posBackgroundPanel.add(dacViewer);

            // Indicators
            posBackgroundPanel.add(messageIndicator);
            posBackgroundPanel.add(warningIndicator);
            posBackgroundPanel.add(informationIndicator);

            // Bruce/20030317/ For Familymart: add "DontShowLicenseInfo"
            if (!param.getShowLicenseInfo())
                informationIndicator.setMessage("");
            else
                informationIndicator.setMessage(res.getString("CompanyTitle"));

//			new Thread(messageIndicator).start();
//			new Thread(warningIndicator).start();
//			new Thread(informationIndicator).start();

            // Area 6 connect message
            connectMsgBanner = new Banner(CreamToolkit
                .getConfigurationFile("connectMsgBanner"), EPOSBackground
                .getInstance().getColor6(), Color.black, Color.black);
            connectMsgBanner.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(connectMsgBanner);
            connectMsgBanner.setCursor(cursor);
            posBackgroundPanel.add(connectMsgBanner);

            // Area 11 page & time messages
            pageMsgBanner = new Banner(CreamToolkit
                .getConfigurationFile("pageMsgBanner"), EPOSBackground
                .getInstance().getColor4(), Color.black, Color.black);
            pageMsgBanner.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(pageMsgBanner);
            pageMsgBanner.setCursor(cursor);
            posBackgroundPanel.add(pageMsgBanner);

//          if (GetProperty.getUseTouchButtonInScreen("no").equalsIgnoreCase("yes")) {
//              touchButton1 = new TouchButton(new TouchMenuButton(0, 0, 1, "触屏一", 116, "t1"));
//              touchButton1.setCursor(cursor);
//              posBackgroundPanel.add(touchButton1);
//
//              touchButton2 = new TouchButton(new TouchMenuButton(0, 0, 2, "触屏二", 2, "t2"));
//              touchButton2.setCursor(cursor);
//              posBackgroundPanel.add(touchButton2);
//          }

            // productinfo message
            productInfoBanner = new ProductInfoBanner(CreamToolkit
                .getConfigurationFile("productInfoBanner"), EPOSBackground
                .getInstance().getTextBackgroundColor(), Color.black,
                Color.black);
            productInfoBanner.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(productInfoBanner);
            productInfoBanner.setCursor(cursor);
            posBackgroundPanel.add(productInfoBanner);

            // Area 8 total message
            totalAmountBanner = new TotalAmountBanner(CreamToolkit
                .getConfigurationFile("totalAmountBanner"), EPOSBackground
                .getInstance().getColor8(), Color.black, Color.black);
            totalAmountBanner.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(totalAmountBanner);
            totalAmountBanner.setCursor(cursor);
            posBackgroundPanel.add(totalAmountBanner);

            // screenbanner2
            screenBanner2 = new Banner(CreamToolkit
                .getConfigurationFile("screenBanner2"), EPOSBackground
                .getInstance().getColor8(), Color.blue.darker().darker(),
                Color.black);
            screenBanner2.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(screenBanner2);
            screenBanner2.setCursor(cursor);
            posBackgroundPanel.add(screenBanner2);

            // PayingPanes
            payingPane = new PayingPane();
            payingPane.setCursor(cursor);
            payingPane.setTransaction(currentTransaction);
            posBackgroundPanel.add(payingPane);
            systemInfo.addSystemInfoListener(payingPane);

            payingPane1 = new PayingPaneBanner(CreamToolkit
                .getConfigurationFile("payingPane1"));
            payingPane1.setCursor(cursor);
            payingPane1.setTransaction(currentTransaction);
            posBackgroundPanel.add(payingPane1);

            dynaPayingPane = new DynaPayingPaneBanner(CreamToolkit
                .getConfigurationFile("dynaPayingPane"));
            dynaPayingPane.setCursor(cursor);
            dynaPayingPane.setTransaction(currentTransaction);
            posBackgroundPanel.add(dynaPayingPane);

            payingPane2 = new PayingPaneBanner(CreamToolkit
                .getConfigurationFile("payingPane2"));
            payingPane2.setCursor(cursor);
            payingPane2.setTransaction(currentTransaction);
            posBackgroundPanel.add(payingPane2);
            payingPane1.setVisible(false);
            payingPane2.setVisible(false);
            dynaPayingPane.setVisible(false);

            // currentTransaction.addTransactionListener(lineItemIndicator);
            // lineItemIndicator.repaint();
            // posBackgroundPanel
            // customerPanel

//			if (GetProperty.getCreateAdPanel("no").equalsIgnoreCase("yes")) {
//				// custInfoIndicator = new CustIndicator();
//				// custInfoIndicator.setMessage("This is
//				// test......................");
//				// customerPanel.add(custInfoIndicator);
//
//				digPrize = new DigPrize();
//				digPrize.init();
//				digPrize.validate();
//				digPrize.setVisible(true);
//				customerPanel.add(digPrize);
//
//				if (GetProperty.getVideoEnable("yes").equals("yes")) {
////					media = new Media(new Color(0xff384779));
////					media.init(CreamToolkit.getPlayList(GetProperty
////							.getVideoPath("/root")));
////					media.setVisible(true);
//					customerPanel.add(media);
//				}
//
//				// timebanner
//				timeBanner = new CustomerBanner(CreamToolkit
//						.getConfigurationFile("timeBanner"),
//				// EPOSBackground.getInstance().getColor8(),
//						Color.black, Color.white, Color.white);
//				timeBanner.setSystemInfo(systemInfo);
//				systemInfo.addSystemInfoListener(timeBanner);
//				timeBanner.setCursor(cursor);
//				customerPanel.add(timeBanner);
//
//				custPayingPane = new CustPayingPaneBanner(CreamToolkit
//						.getConfigurationFile("custPayingPane"));
//				custPayingPane.setCursor(cursor);
//				custPayingPane.setTransaction(currentTransaction);
//				systemInfo.addSystemInfoListener(custPayingPane);
//				customerPanel.add(custPayingPane);
//
//				custProductInfo = new CustProductInfoBanner(CreamToolkit
//						.getConfigurationFile("custProductInfo"),
//						EPOSBackground.getInstance().getTextBackgroundColor(),
//						Color.black, Color.black);
//				custProductInfo.setCursor(cursor);
//				custProductInfo.setSystemInfo(systemInfo);
//				systemInfo.addSystemInfoListener(custProductInfo);
//				customerPanel.add(custProductInfo);
//
//				custAdvertWord = new CustomerBanner(CreamToolkit
//						.getConfigurationFile("custAdvertWord"), Color.black,
//						Color.white, Color.white);
//				custAdvertWord.setCursor(cursor);
//				custAdvertWord.setSystemInfo(systemInfo);
//				systemInfo.addSystemInfoListener(custAdvertWord);
//				customerPanel.add(custAdvertWord);
//
//				custWelcome = new CustomerTextBanner(CreamToolkit
//						.getConfigurationFile("custWelcome"), Color.white,
//						Color.black, Color.black);
//				custWelcome.setCursor(cursor);
//				custWelcome.setSystemInfo(systemInfo);
//				systemInfo.addSystemInfoListener(custWelcome);
//				customerPanel.add(custWelcome);
//			}

            // Create StateMachine
            setBeginState(true);
            stateMachine = StateMachine.getInstance();
            stateMachine.initStateMachine();

            // Enable event forward
            posButtonHome.setEventForwardEnabled(true);
            posPeripheralHome.setEventForwardEnabled(true);
            stateMachine.setEventProcessEnabled(true);

            layoutUIBeans();

            screenBanner.setVisible(true);
            itemList.setVisible(true);
            dacViewer.setVisible(false);
            payingPane1.setVisible(false);
            payingPane2.setVisible(false);
            dynaPayingPane.setVisible(false);
            productInfoBanner.setVisible(true);
            totalAmountBanner.setVisible(true);
            messageIndicator.setVisible(true);
            warningIndicator.setVisible(true);
            informationIndicator.setVisible(true);
            informationIndicator.repaint();

            if (GetProperty.isEnableTouchPanel())
                TouchPane.getInstance().setVisible(false);

            CreamPrinter.getInstance().setPrintEnabled(param.isReceiptPrint());
            CreamPrinter.getInstance().printFirstReceipt();
            setKeyPosition(5);
        } catch (ConfigurationNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage(e);
        }
        try{
            showSecondScreen();
        }catch (Exception e){
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage(e);
        }
        // setting AppFrame
        if (contentPane instanceof Window) {
            ((Window)contentPane).addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    try {
                        close();
                        CreamToolkit.closeLog();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    System.exit(0);
                }
            });
        }

		//final Thread thread = new ShowTime(screenBanner2);
		//thread.start();
        //James/2008-06-29/ 旧的方式刷新时间不准时，改为继承TimerTask，由Timer来定时执行
        Calendar cl = java.util.Calendar.getInstance();
        cl.set(Calendar.MINUTE, cl.get(Calendar.MINUTE) + 1);
        cl.clear(Calendar.SECOND);
        cl.clear(Calendar.MILLISECOND);
        new java.util.Timer().schedule(new ShowTime(screenBanner2), cl.getTime(), 60 * 1000L);

        //Bruce/2003-08-10
        boolean pass = true;
        try {
            Class.forName("protector.Protector");
            pass = Protector.checkUserLicense();
            CreamToolkit.logMessage("protector : " + pass);
        } catch (Exception e) {
            CreamToolkit.logMessage("This is non-protected version.");
        }
        if (pass) {
            if (!param.getNeedTurnKeyAtStart()) {
                stateMachine.processEvent(new StatusUpdateEvent(new FakeKeylock(1), 0));
                stateMachine.processEvent(new StatusUpdateEvent(new FakeKeylock(2), 0));
            }
        }

        // if (adPanel != null) {
        // adPanel.run();
        // }
        // boolean two = GetProperty.getTwoScreen("no").equalsIgnoreCase("yes");
        // if (two) {
        // try {
        // Robot rb = new Robot();
        // //rb.mouseMove(0, 0);
        // rb.mouseMove((int)(getLocation().getX() + 1),
        // (int)(getLocation().getY() + 1));
        // } catch(AWTException e){
        // e.printStackTrace();
        // }
        // }

        // Make it faster when loading first PLU
        PLU.queryBarCode("000000");

        // start master downloading thread
        MasterDownloadThread.getInstance();

        SsmpLog.report10010();

        CreamToolkit.wanConnected();
        POSTerminalApplication.getInstance().getSystemInfo().fireEvent(new SystemInfoEvent(this));
    }


    private void createCurrentTransaction() {
        System.out.println("POSTerminalApplication.createCurrentTransaction invoked.");
        blankTransaction = Transaction.createCurrentTransaction();
        currentTransaction = (Transaction)CreamToolkit.deepClone(blankTransaction);
    }
    /**
     * Create a new clonedBlankTransaction by cloning from blankTransaction.
     */
    public void createClonedBlankTransaction() {
        clonedBlankTransaction = (Transaction)CreamToolkit.deepClone(blankTransaction);
    }

    /**
     * Create a new currentTransaction by cloning from blankTransaction.
     */
    public Transaction cloneFromBlankTransaction() {
        if (clonedBlankTransaction == null)
            createClonedBlankTransaction();
        setCurrentTransaction(clonedBlankTransaction);
        clonedBlankTransaction = null;
        return getCurrentTransaction();
        //Transaction trans = (Transaction)CreamToolkit.deepClone(blankTransaction);
        //setCurrentTransaction(trans);
        //CreamSession.getInstance().clearTransactionSessionData();
        //return trans;
    }

    public void setAlipay(boolean t) {
        isAlipay = t;
    }
    public boolean getAlipay() {
        return isAlipay;
    }
    public void setWeiXin(boolean t) {
        isWeiXin = t;
    }
    public boolean getWeiXin() {
        return isWeiXin;
    }
    public void setUnionPay(boolean t) {
        isUnionPay = t;
    }
    public boolean getUnionPay() {
        return isUnionPay;
    }
    public void setShift2(boolean shift2) {
        this.shift2 = shift2;
    }
    public boolean getShift2() {
        return shift2;
    }
    private JPanel createButtonPanel() {
        buttonPanel.setLayout(buttonCardLayout);

        buttonPanel1.setLayout(new GridLayout(1, 8));

        createCommandButton(buttonPanel1, CreamToolkit.GetResource().getString("ReSweep"),
                new QingKongButton(0, 0, 0, ""));
        createCommandButton(buttonPanel1, CreamToolkit.GetResource().getString("Eliminate"),
                new ClearButton(0, 0, 0, "Clear"));
        /* createCommandButton(buttonPanel1, CreamToolkit.GetResource().getString("confirm"),
                new EnterButton(0, 0, 0, "Enter"));
        createCommandButton(buttonPanel1, "",
                new POSButton());*/
        createCommandButton(buttonPanel1, "",
                new POSButton());
        createCommandButton(buttonPanel1, "",
                new POSButton());
        createCommandButton(buttonPanel1, CreamToolkit.GetResource().getString("SettleAccounts"),
                new AgeLevelButton(0, 0, 0, "","1"));

        buttonPanel.add(buttonPanel1, "BUTTON_PANEL1");

        buttonPanel2.setLayout(new GridLayout(1, 8));

        createCommandButton(buttonPanel2, CreamToolkit.GetResource().getString("Return"),
                new ClearButton(0, 0, 0, "Clear"));
        createCommandButton(buttonPanel2, CreamToolkit.GetResource().getString("alipayButton"),
                new AlipayButton(0, 0, 0, "","16"));
        createCommandButton(buttonPanel2, CreamToolkit.GetResource().getString("weixinButton"),
                new WeiXinButton(0, 0, 0, "","20"));
        createCommandButton(buttonPanel2, CreamToolkit.GetResource().getString("unionPayButton"),
                new UnionPayButton(0, 0, 0, "","21"));
        createCommandButton(buttonPanel2, CreamToolkit.GetResource().getString("cmpayButton"),
                new CmpayButton(0, 0, 0, "","17"));

        buttonPanel.add(buttonPanel2, "BUTTON_PANEL2");

        buttonCardLayout.show(buttonPanel, "BUTTON_PANEL1");

        /*createCommandButton(buttonPanel, "",
                new POSButton());
        createCommandButton(buttonPanel, "",
                new POSButton());
        createCommandButton(buttonPanel, "",
                new POSButton());
        createCommandButton(buttonPanel, "",
                new POSButton());
        createCommandButton(buttonPanel, "",
                new POSButton());*/
        /*createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelESC"),
            new ClearButton(0, 0, 0, "Clear"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelF1"),
            new EmptyButton(0, 0, 0, "F1"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelF2"),
            new EmptyButton(0, 0, 0, "F2"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelF3"),
            new EmptyButton(0, 0, 0, "F3"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelF4"),
            new EmptyButton(0, 0, 0, "F4"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelF5"),
            new EmptyButton(0, 0, 0, "F6")); // 系统设置的keylock
        // pos is 6
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelS"),
            new SalesmanButton(0, 0, 0, "Salesman"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelK"),
            new PaymentButton(0, 0, 0, "Check", "01"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelC"),
            new CheckInOutButton(0, 0, 0, "Cashier"));

        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelQ"),
            new InquiryButton(0, 0, 0, "Inquiry"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelO"),
            new OverrideAmountButton(0, 0, 0, "Override"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelP"),
            new DiscountRateButton(0, 0, 0, "Disc"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelN"),
            new DiscountRateIDButton(0, 0, 0, "DiscID"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panel／"),
            new RemoveButton(0, 0, 0, "Remove"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panel＊"),
            new QuantityButton(0, 0, 0, "Qty"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panel＋"),
            new AgeLevelButton(0, 0, 0, "Nan", "1"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelR"),
            new DailyReportButton(0, 0, 0, "SalesReport"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("panelEnter"),
            new EnterButton(0, 0, 0, "Enter"));*/
        return buttonPanel;
    }
    private JPanel createButtonPanel2() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1, 8));

        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("Return"),
                new ClearButton(0, 0, 0, "Clear"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("alipayButton"),
                new AlipayButton(0, 0, 0, "","16"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("weixinButton"),
                new WeiXinButton(0, 0, 0, "","20"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("unionPayButton"),
                new UnionPayButton(0, 0, 0, "","21"));
        createCommandButton(buttonPanel, CreamToolkit.GetResource().getString("cmpayButton"),
                new CmpayButton(0, 0, 0, "","17"));
        return buttonPanel;
    }

    private class FakeKeylock extends Keylock {
        int keyPos;

        FakeKeylock(int keyPos) {
            this.keyPos = keyPos;
        }

        public int getKeyPosition() {
            return keyPos;
        }

        @Override
        public void claim(int timeout) throws JposException {
        }

        @Override
        public void close() throws JposException {
        }

        @Override
        public void open(String logicalName) throws JposException {
        }

        @Override
        public void release() throws JposException {
        }
    }

    public void createCommandButton(JPanel buttonPanel, String label,
        final POSButton button) {
        final Color bgColor = new Color(195, 208, 255);
        final Color bgPressedColor = new Color(195, 108, 255);
        final JButton cmdButton = new JButton(label) {
            public Dimension getPreferredSize() {
                return new Dimension(80, Param.getInstance().getButtonSize());
            };
        };
        //设置按钮按下去的颜色
        UIManager.put("Button.select", bgPressedColor);
//        cmdButton.setFont(new Font(
//            CreamToolkit.GetResource().getString("Song"), Font.PLAIN, 14));
        cmdButton.setFont(new Font("SimHei", Font.PLAIN, 40));
        cmdButton.setBackground(bgColor);
        cmdButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                StateMachine stateMachine = StateMachine.getInstance();
                if (button.getLabel().startsWith("F")) {
                    int keyPos = Integer.parseInt(button.getLabel()
                        .substring(1));
                    stateMachine.processEvent(new StatusUpdateEvent(
                        new FakeKeylock(keyPos), 0));
                } else {
                    stateMachine.processEvent(new POSButtonEvent(button));
                }
            }
        });
        buttonPanel.add(cmdButton);
    }

    class ShowTime extends TimerTask {
        Banner screenBanner = null;

        public ShowTime(Banner screenBanner) {
            this.screenBanner = screenBanner;
        }

        public void run() {
            // while (true) {
            // System.out.println("ShowTime a" + CreamCache.getInstance().getDateTimeFormate5().format(new java.util.Date()));
            if (screenBanner != null)
                screenBanner.repaint();
            // System.out.println("ShowTime b" + CreamCache.getInstance().getDateTimeFormate5().format(new java.util.Date()));
            // }
        }
    }
//	 use sleep is not accurate at
//	class ShowTime extends Thread {
//		Banner screenBanner = null;
//
//		public ShowTime(Banner screenBanner) {
//			this.screenBanner = screenBanner;
//		}
//
//		public void run() {
//			while (true) {
//				if (screenBanner != null)
//					screenBanner.repaint();
//				try {
//					sleep(59000); // is not accurate
//				} catch (InterruptedException ie) {
//				}
//			}
//		}
//	}

    /**
     * Wait for EPOSBackground finished its first paint().
     */
    private void waitForEPOSBackgroundPaint() {
        synchronized (EPOSBackground.getInstance()) {
            try {
                EPOSBackground.getInstance().wait(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setCurrentCashierNumber(String number) {
        cashierNumber = number;
    }

    public String getCurrentCashierNumber() {
        return cashierNumber;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public String getSelfBuy() {
        return selfBuy;
    }

    public void setSelfBuy(String ceKeys) {
        selfBuy = ceKeys;
    }

    public void setCurrentTransaction(Transaction trans) {
        if (transactionListener == null) // backup transient transactionListener
            transactionListener = currentTransaction.getTransactionListener();
        if (transactionListener != null) // restore transient transactionListener
            trans.setTransactionListener(transactionListener);

        systemInfo.setTransaction(trans);
        itemList.setTransaction(trans);
        payingPane.setTransaction(trans);
        payingPane1.setTransaction(trans);
        dynaPayingPane.setTransaction(trans);
        payingPane2.setTransaction(trans);
        currentTransaction = trans;
    }

    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    public POSButtonHome2 getPOSButtonHome() {
        return posButtonHome;
    }

    public POSPeripheralHome3 getPOSPeripheralHome() {
        return posPeripheralHome;
    }

    /*
      * public ButtonPanel getButtonPanel() { return buttonPanel; }
      */

    public ScreenBanner getScreenBanner() {
        return screenBanner;
    }

    public void repaintScreenBanner2() {
        screenBanner2.repaint();
    }

    // public LineItemIndicator getLineItemIndicator() {
    // return lineItemIndicator;
    // }

    public Indicator getMessageIndicator() {
        return messageIndicator;
    }

    public Indicator getWarningIndicator() {
        return warningIndicator;
    }

    public Indicator getInformationIndicator() {
        return informationIndicator;
    }

    public ItemList getItemList() {
        return itemList;
    }

    public DacViewer getDacViewer() {
        return dacViewer;
    }

    public PayingPane getPayingPane() {
        return payingPane;
    }

    public Frame getAppFrame() {
        return mainFrame;
    }

    public PopupMenuPane getPopupMenuPane() {
        return popupMenuPane;
    }

    public SignOffForm getSignOffForm() {
        return signOffForm;
    }

    public CreditCardForm getCreditCardForm() {
        return creditCardForm;
    }

    public void setBuyerNumberPrinted(boolean buyerNumberPrinted) {
        this.buyerNumberPrinted = buyerNumberPrinted;
    }

    public boolean getBuyerNumberPrinted() {
        return buyerNumberPrinted;
    }

    public boolean getTrainingMode() {
        return trainingMode;
    }

    public void setTrainingMode(boolean training) {
        trainingMode = training;
        CreamPrinter.getInstance().setPrintEnabled(!training);
        if (trainingMode) {
            this.getItemList().setBackLabel(res.getString("TrainingMode"));
        } else {
            this.getItemList().setBackLabel(null);
        }
    }

    public Panel getTotalMsgPanel() {
        return totalMsgPanel;
    }

    private void setupFullScreen(Window frame) {
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice dev = env.getDefaultScreenDevice();
        boolean isSupported = dev.isFullScreenSupported();
        if (isSupported)
            dev.setFullScreenWindow(frame);
    }

    private void resizeFrame(Container mainFrame) {
        if (fullScreen) {
            if (mainFrame instanceof Frame) {
                Frame frame = (Frame)mainFrame;
                frame.setUndecorated(true);
                frame.setExtendedState(Frame.MAXIMIZED_BOTH);
                frame.setLocation(0,0);
                setupFullScreen(frame);
            }
        } else {
            int width, height;
            if (screenSize == null)
                screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            width = (int)screenSize.getWidth();
            height = (int)screenSize.getHeight();
            boolean two = false; //GetProperty.getTwoScreen("no").equalsIgnoreCase("yes");
            if (CreamToolkit.getOsName().contains("Windows")) {
                width -= 20;
                height -= 60;
                if (two) {
                    String posX = "0"; // GetProperty.getposX("0");
                    String posY = "0"; // GetProperty.getposY("0");
                    int x = 0, y = 0;
                    try {
                        x = Integer.parseInt(posX);
                        y = Integer.parseInt(posY);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mainFrame.setSize(width / 2, height);
                    mainFrame.setLocation(x, y);
                } else {
                    mainFrame.setSize(width, height);
                    mainFrame.setLocation((screenSize.width - width) / 2,
                        (screenSize.height - height) / 2 - 20);
                }
            } else {
                if (two) {
                    String posX = "0"; // GetProperty.getposX("0");
                    String posY = "0"; // GetProperty.getposY("0");
                    int x = 0, y = 0;
                    try {
                        x = Integer.parseInt(posX);
                        y = Integer.parseInt(posY);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mainFrame.setSize(width / 2, height);
                    mainFrame.setLocation(x, y);
                } else {
                    mainFrame.setSize(width, height);
                    mainFrame.setLocation((screenSize.width - width) / 2,
                        (screenSize.height - height) / 2);
                }
            }
        }
    }

    public void setKeyState(boolean keyState) {
        this.keyState = keyState;
    }

    public boolean getkeyState() {
        return keyState;
    }

    public boolean getReturnItemState() {
        return returnItemState;
    }

    public void setReturnItemState(boolean returnItemState) {
        this.returnItemState = returnItemState;
    }

    public boolean getTransactionEnd() {
        return transactionEnd;
    }

    public void setTransactionEnd(boolean transactionEnd) {
        // System.out.println("setTransactionEnd(" + transactionEnd + ")");
        this.transactionEnd = transactionEnd;
        // if (transactionEnd)
        // setReturnItemState(false);
    }

    public int getKeyPosition() {
        return keyPosition;
    }

    public void setKeyPosition(int keyPosition) {
        stateMachine.setTurnable(0);
        this.keyPosition = keyPosition;
    }

    public boolean getBeginState() {
        return beginState;
    }

    public void setBeginState(boolean beginState) {
        this.beginState = beginState;
    }

    private boolean enabledPopupMenu = false;

    private boolean dynaPayingPaneVisible;

    public boolean getEnabledPopupMenu() {
        return enabledPopupMenu;
    }

    public void setEnabledPopupMenu(boolean enabledPopupMenu) {
        this.enabledPopupMenu = enabledPopupMenu;
    }

    public boolean getIsAllReturn() {
        return isAllReturn;
    }

    public void setIsAllReturn(boolean isAllReturn) {
        this.isAllReturn = isAllReturn;
    }

    public boolean getChecked() {
        return checked;
    }

    public boolean getSalemanChecked() {
        return salemanChecked;
    }

    public boolean getPayingPaneVisible() {
        return payingPaneVisible;
    }

    public void setPayingPaneVisible(boolean visible) {
        payingPaneVisible = visible;

        if (visible) {
            screenBanner.setVisible(false);
            itemList.setVisible(false);
            productInfoBanner.setVisible(false);
            totalAmountBanner.setVisible(false);
            if (dynaPayingPaneVisible)
                dynaPayingPane.setVisible(true);
            else
                payingPane1.setVisible(true);

            payingPane2.setVisible(true);
        } else {
            payingPane1.setVisible(false);
            payingPane2.setVisible(false);
            dynaPayingPane.setVisible(false);
            dynaPayingPaneVisible = false;
            screenBanner.setVisible(true);
            itemList.setVisible(true);
            productInfoBanner.setVisible(true);
            totalAmountBanner.setVisible(true);
//			repaint();
//			posBackgroundPanel.repaint();
        }

        mainFrame.repaint();
        posBackgroundPanel.repaint();
    }

    public void setDynaPayingPaneVisible(boolean visible) {
        dynaPayingPaneVisible = visible;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void setSalemanChecked(boolean salemanChecked) {
        this.salemanChecked = salemanChecked;
    }

    public boolean getScanCashierNumber() {
        return scanCashierNumber;
    }

    public boolean getScanSalemanNumber() {
        return scanSalemanNumber;
    }

    public void setScanCashierNumber(boolean scanCashierNumber) {
        this.scanCashierNumber = scanCashierNumber;
    }

    public void setScanSalemanNumber(boolean scanSalemanNumber) {
        this.scanSalemanNumber = scanSalemanNumber;
    }

    public HYIDouble getReturnQty() {
        return returnQty;
    }

    public void setReturnQty(HYIDouble returnQty) {
        this.returnQty = returnQty;
    }

    private static boolean matchOptions(String option, List<String> matchers) {
        for (String matcher : matchers)
            if (matcher.equalsIgnoreCase(option))
                return true;
        return false;
    }

    /**
     * ePOS Entry point.
     * <p/>
     * Parameters:
     *  -help: show this help info, then exit
     *  -version: print version and machine unique id, then exit
     *  true | -recalc: recalculate the summary report from transaction, then exit
     *  -fullscreen: enter full screen mode
     *  -screensize {width}x{height}: specify frame size"
     *  -noserver: do not start the Jetty servlet engine for web-based console
     * 参数说明:
     *  -help: 显示本说明，然后程序结束
     *  -version: 显示版本号和机器唯一编码，然后程序结束
     *  true | -recalc: 从交易明细重算统计报表，然后程序结束
     *  -fullscreen: 进入全屏模式
     *  -screensize {寬}x{高}: 指定POS窗口大小
     *  -noserver: 不启动Jetty servlet引擎
     */
    public static void main(String[] args) {
        System.setProperty("file.encoding", "UTF-8");

        // load log4j configuration
        // DOMConfigurator.configure("log4j.xml");

        boolean acceptNextScreenSize = false;
        boolean runJettyServer = true;
        POSTerminalApplication.args = args;


        for (String arg : args) {
            CreamToolkit.logMessage("------------"+arg+"=");
            if (acceptNextScreenSize) {
                acceptNextScreenSize = false;
                String[] dimension = arg.split("x");
                screenSize = new Dimension(Integer.parseInt(dimension[0]), Integer.parseInt(dimension[1]));
            } else if (matchOptions(arg, Arrays.asList("-h", "-help", "--help"))) {
                showHelpInfo();
                System.exit(0);
            } else if (matchOptions(arg, Arrays.asList("-v", "-version", "--version"))) {
                showVersionInfo();
                System.exit(0);
            } else if (matchOptions(arg, Arrays.asList("true", "-recalc", "--recalc"))) {
                POSTerminalApplication.getInstance().recalc();
                System.exit(0);
            } else if (matchOptions(arg, Arrays.asList("selfbuy"))) {
                CreamToolkit.logMessage("posTerminalApplication.selfBuy3"+posTerminalApplication.selfBuy+"...");
                POSTerminalApplication posTerminalApplication = POSTerminalApplication.getInstance();
                posTerminalApplication.currentTransaction = Transaction.createCurrentTransaction();
                posTerminalApplication.selfBuy = "S";
                CreamToolkit.logMessage("posTerminalApplication.selfBuy1"+posTerminalApplication.selfBuy+"...");
                new Client(Param.getInstance().getScIpAddress(), Server.INLINE_SERVER_TCP_PORT);
                MasterDownloadThread.getInstance();
//                posTerminalApplication.setCurrentTransaction(Transaction.createCurrentTransaction());
                new Thread(new Runnable() {
                    public void run() {
                        while(true){
                            try {

                                int ss = Integer.parseInt(Param.getInstance().getSelfbuySleep());
                                Thread.sleep(ss * 1000);
                                SelfbuyHttpPostRequest.selfbuyClasses();
                                SelfbuyHttpPostRequest.selfbuyShift();
                                SelfbuyHttpPostRequest.selfbuyPost();

                                //releaseAll();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();
            }  else if (matchOptions(arg, Arrays.asList("-fullscreen", "--fullscreen"))) {
                fullScreen = true;
            } else if (matchOptions(arg, Arrays.asList("-noserver", "--noserver"))) {
                runJettyServer = false;
            } else if (matchOptions(arg, Arrays.asList("-screensize", "--screensize"))) {
                acceptNextScreenSize = true;
            }
        }

        if (runJettyServer) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    JettyStarter.start();

                }
            }, "Jetty").start();
        }

        CreamToolkit.logMessage("------------------------------------------------");
        CreamToolkit.logMessage("POS startups, version: " + getVersion() + " " + Version.getVersionDate());
        CreamToolkit.logMessage("------------------------------------------------");

        HealthyChecker.checkForPOS();

        POSTerminalApplication posTerminalApplication = POSTerminalApplication.getInstance();
        CreamToolkit.logMessage("posTerminalApplication.selfBuy2"+posTerminalApplication.selfBuy+"...");
        if(!posTerminalApplication.selfBuy.equals("S")){
            try {
                POSTerminalApplication app = POSTerminalApplication.getInstance();
                app.startup();
//                MasterDownloadThread.getInstance();
            } catch (Exception e) {
                CreamToolkit.logMessage(e);
            }
        }
    }

    private static void showVersionInfo() {
        System.out.println("Version: " + getVersion() + " " + Version.getVersionDate());
        try {
            Class.forName("protector.Protector");
            System.out.println("Machine ID: " + Protector.getMachineUniqueID());
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private static void showHelpInfo() {
        System.out.println("Hongyuan ePOS Version " + getVersion());
        System.out.println("\nOptions:");
        System.out.println("  -help: show this help info, then exit");
        System.out.println("  -version: print version and machine unique id, then exit");
        System.out.println("  true | -recalc: recalculate the summary report from transaction, then exit");
        System.out.println("  -fullscreen: enter full screen mode");
        System.out.println("  -screensize {width}x{height}: specify frame size");
        System.out.println("  -noserver: do not start the Jetty servlet engine for web-based console");
        System.out.println("参数说明:");
        System.out.println("  -help: 显示本说明，然后程序结束");
        System.out.println("  -version: 显示版本号和机器唯一编号，然后程序结束");
        System.out.println("  true | -recalc: 从交易明细重算统计报表，然后程序结束");
        System.out.println("  -fullscreen: 进入全屏模式");
        System.out.println("  -screensize {寬}x{高}: 指定POS窗口大小");
        System.out.println("  -noserver: 不启动Jetty servlet引擎");
    }

    // ZhaoHong 2003-08-15
    // 最近一段时间交易的分段信息 按照EODCNT分段
    private static ArrayList lastestTrans = new ArrayList();

    private static String shiftbnumber = "";
    private static String shiftenumber = "";
    public static String args[];
    private ShiftReport shift = null;
    private ZReport z = null;

    private Object[] getRecalParameter() {
        boolean rtnValue = false;
        String bnumber = "";
        String enumber = "";
        java.util.Date busiDate = null;
        String message = CreamToolkit.GetResource().getString(
            "PleaseSelectDateBetweenQ");
        String message2 = CreamToolkit.GetResource().getString(
            "TransactionNumberSample");

        BufferedReader reader = new BufferedReader(new InputStreamReader(
            System.in));
        String cmd = "";
        if (getLastestTransactions()) {
            HashMap mp = null;
            String msg = "";
            System.out.println("\n" + message2 + "\n");
            for (int i = 0; i < lastestTrans.size(); i++) {
                mp = (HashMap)lastestTrans.get(i);
                msg = "" + (i + 1) + ".  " + mp.get("BTranNo") + "("
                    + mp.get("BTranTime") + ")" + "  --  "
                    + mp.get("ETranNo") + "(" + mp.get("ETranTime") + ")"
                    + "  Z = " + mp.get("ZNumber");
                System.out.println(msg);
            }

            while (true) {
                System.out.println("\n" + message + "\n");

                System.out.print(">");

                try {
                    cmd = reader.readLine();

                    if (cmd.equalsIgnoreCase("q")) {
                        System.out.println("Bye");
                        break;
                    } else if (Integer.parseInt(cmd) - 1 < lastestTrans.size()) {
                        mp = (HashMap)lastestTrans
                            .get(Integer.parseInt(cmd) - 1);
                        bnumber = mp.get("BTranNo").toString();
                        enumber = mp.get("ETranNo").toString();

                        // 录入会计日期
                        while (true) {
                            String message3 = CreamToolkit.GetResource()
                                .getString("PleaseInputAccountDate");

                            System.out.println(message3);
                            System.out.print(">");

                            try {
                                cmd = reader.readLine();
                                if (cmd.equalsIgnoreCase("q")) {
                                    System.out.println("Bye");
                                    break;
                                } else if (cmd == null) {
                                    busiDate = new java.util.Date();
                                } else
                                    busiDate = java.sql.Date.valueOf(cmd);
                                break;
                            } catch (IllegalArgumentException e) {
                                continue;
                            } catch (IOException e) {
                                e.printStackTrace();
                                break;
                            }
                        }

                        rtnValue = true;
                        break;
                    } else {
                        continue;
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                } catch (NumberFormatException e) {
                    continue;
                }
            }

        }
        Object[] rtn = new Object[4];
        rtn[0] = new Boolean(rtnValue);
        rtn[1] = bnumber;
        rtn[2] = enumber;
        rtn[3] = busiDate;
        return rtn;
    }

    // 取最近一段时间的交易情况
    private boolean getLastestTransactions() {
        boolean rtnValue = false;
        if (lastestTrans.size() > 0)
            return true;

        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Statement stmt = connection.createStatement();
            // 取最近15个交易日
            int baseZNumber = ZReport.getCurrentZNumber(connection) - 40;
            if (baseZNumber < 0)
                baseZNumber = 0;
            String sql = "select TMTRANSEQ, EODCNT, SYSDATE from tranhead "
                + "where EODCNT >= " + baseZNumber + " "
                + "order by EODCNT desc, SYSDATE";

            ResultSet rst = stmt.executeQuery(sql);

            int a = -1;
            HashMap hm = null;
            while (rst.next()) {
                int minTranNo = 0, maxTranNo = 0;
                if (a == -1 || a != rst.getInt("EODCNT")) {
                    hm = new HashMap();
                    lastestTrans.add(hm);

                    a = rst.getInt("EODCNT");
                    minTranNo = rst.getInt("TMTRANSEQ");
                    maxTranNo = minTranNo;
                    hm.put("ZNumber", new Integer(a));
                    hm.put("BTranNo", new Integer(minTranNo));
                    hm.put("ETranNo", new Integer(maxTranNo));
                    hm.put("BTranTime", rst.getObject("SYSDATE"));
                    hm.put("ETranTime", rst.getObject("SYSDATE"));
                } else {
                    // if (rst.getInt("TMTRANSEQ") < minTranNo) {
                    // minTranNo = rst.getInt("TMTRANSEQ");
                    // hm.put("BTranNo", new Integer(minTranNo));
                    // hm.put("BTranTime", rst.getObject("SYSDATE"));
                    // }

                    // if (rst.getInt("TMTRANSEQ") > maxTranNo) {
                    maxTranNo = rst.getInt("TMTRANSEQ");
                    hm.put("ETranNo", new Integer(maxTranNo));
                    hm.put("ETranTime", rst.getObject("SYSDATE"));
                    // }
                }

            } // end while
            rst.close();
            rst = null;
            // con.close();
            // con = null;
            rtnValue = true;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }

        return rtnValue;
    }

    public void recalcFromTransactions(String bnumber, String enumber, Date busiDate) {
        recalcFromTransactions(bnumber, enumber, busiDate, true);
    }

    public void recalcFromTransactions(String bnumber, String enumber, java.util.Date busiDate,
        boolean exit) {

        HYIDouble zero = new HYIDouble(0);
        // initial
        DbConnection connection = null;
        try {
            if (exit) {
                screenBanner = new ScreenBanner();
                messageIndicator = new Indicator();
                warningIndicator = new Indicator();
                informationIndicator = new Indicator(2);
                itemList = new ItemList();
                popupMenuPane = PopupMenuPane.getInstance();
                systemInfo = new SystemInfo();
            }
            // payingPane = new PayingPane();

            int posNumber = param.getTerminalNumber();
            connection = CreamToolkit.getPooledConnection();//.getTransactionalConnection();
            CreamToolkit.logMessage("Recalc report: " + bnumber + " - " + enumber);

            currentTransaction = Transaction.createCurrentTransaction();
            //posPeripheralHome = POSPeripheralHome3.getInstance();
            //posButtonHome = POSButtonHome2.getInstance();
            //stateMachine = StateMachine.getInstance();

            System.out.println();
            System.out.println("#       this is information of parameters :");
            System.out.println("        enclosing   : " + bnumber + " - " + enumber);

            // begin calculate
            System.out.println();
            System.out.println("# begin ......");
            // d = CreamToolkit.getInitialDate();
            java.util.Date bd = new java.util.Date();
            java.util.Date ed = new java.util.Date();
            java.util.Date shiftbd = new java.util.Date();
            java.util.Date shifted = new java.util.Date();
            DateFormat yyyyMMdd = CreamCache.getInstance().getDateFormate();
            String cnumber = "";
            int zbnumber = 0;
            int zenumber = 0;

            // property 表中设置的系统最大交易序号
            int maxTranNumber = Transaction.getMaxTranNumberInProperty();
            boolean bn = Integer.parseInt(bnumber) <= Integer.parseInt(enumber);
            boolean check = POSTerminalApplication.isRecalcMode;
            POSTerminalApplication.isRecalcMode = true;
            for (int i = Integer.parseInt(bnumber);
                 i <= (bn ? Integer.parseInt(enumber) : Integer.parseInt(enumber) + maxTranNumber); i++) {

                int transNumber = (i <= maxTranNumber) ? i : (i - maxTranNumber);

                // get current transaction
                System.out.println("query " + transNumber);
                currentTransaction = Transaction.queryByTransactionNumber(connection, posNumber, transNumber);

                // check dealtype2 = '8' (sign on)
                // = '9' (sign off)
                // = 'D' (eod)
                // 处理跳号
                if (currentTransaction == null)
                    continue;

                String dealtype2 = currentTransaction.getDealType2();

                if (i == Integer.parseInt(bnumber)) {
                    bd = currentTransaction.getSystemDateTime();
                    zbnumber = transNumber;

                    // Create new ZReport
                    z = new ZReport();
                    // z.setSequenceNumber(currentTransaction.getSignOnNumber());
                    z.setSequenceNumber(currentTransaction.getZSequenceNumber());
                    z.setBeginSystemDateTime(new java.util.Date());
                    z.setEndSystemDateTime(CreamToolkit.getInitialDate());
                    z.setBeginTransactionNumber(connection, 100);
                    z.setEndTransactionNumber(connection, 100);
                    z.setBeginInvoiceNumber(connection, "0");
                    // prop.getProperty("NextInvoiceNumber"));
                    z.setEndInvoiceNumber(connection, "0");
                    // prop.getProperty("NextInvoiceNumber"));
                    z.setMachineNumber(param.getTerminalPhysicalNumber());

                    if (!z.exists(connection)) {
                        z.insert(connection);
                    } else {
                        z.deleteByPrimaryKey(connection);
                        z.insert(connection);
                    }

                    // System.out.println(b + " = " + shift.getSequenceNumber()
                    // + " <<<>>> " + shift.getZSequenceNumber());
                    // z = (ZReport) z.queryByPrimaryKey();

                    // delete existed DepSales
                    Iterator itr = DepSales.queryBySequenceNumber(connection, currentTransaction
                        .getZSequenceNumber());

                    if (itr != null) {
                        while (itr.hasNext()) {
                            ((DepSales)itr.next()).deleteByPrimaryKey(connection);
                        }
                    }

                    // Create DepSales
                    DepSales.createDepSales(connection, currentTransaction.getZSequenceNumber());

                    // delete existed DaishouSales 代售
                    Iterator iter = DaishouSales.getCurrentDaishouSales(connection,
                        currentTransaction.getZSequenceNumber());
                    while (iter != null && iter.hasNext()) {
                        ((DaishouSales)iter.next()).deleteByPrimaryKey(connection);
                    }

                    // create new DaishouSales
                    DaishouSales.createDaishouSales(connection, currentTransaction.getZSequenceNumber());
                    Iterator iterator = DaishouSales.getCurrentDaishouSales(connection,
                        currentTransaction.getZSequenceNumber());
                    while (iterator != null && iterator.hasNext()) {
                        DaishouSales dss = (DaishouSales)iterator.next();
                        dss.setAccountDate(busiDate);
                        dss.update(connection);
                    }

                    // delete existed DaishouSales2 代售2
                    DaiShouSales2.deleteBySequenceNumber(connection,
                        currentTransaction.getZSequenceNumber(), currentTransaction.getTerminalNumber());

                    ZEx.deleteBySequenceNumber(connection, currentTransaction.getTerminalNumber(),
                        currentTransaction.getZSequenceNumber());

                    ShiftEx.deleteBySequenceNumber(connection, currentTransaction.getTerminalNumber(),
                        currentTransaction.getZSequenceNumber(),
                        currentTransaction.getSignOnNumber());

                } else if (i == (bn ? Integer.parseInt(enumber) : Integer.parseInt(enumber) + maxTranNumber)
                    || dealtype2.equalsIgnoreCase("D")) {
                    ed = currentTransaction.getSystemDateTime();
                    zenumber = (i <= maxTranNumber) ? i : (i - maxTranNumber);
                }

                if (shift != null) {
                    // continue
                    try {
                        currentTransaction.calcHardTotals(connection);
                        System.out.println("# " + ((i <= maxTranNumber) ? i : (i - maxTranNumber))
                            + " (" + currentTransaction.getDealType1() + ", "
                            + currentTransaction.getDealType2() + ", "
                            + currentTransaction.getDealType3() + ") --- OK");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (dealtype2.equalsIgnoreCase("8")) {
                    shiftbd = currentTransaction.getSystemDateTime();
                    try {
                        shift = new ShiftReport();
                        shift.setSequenceNumber(currentTransaction.getSignOnNumber());
                        shift.setZSequenceNumber(currentTransaction.getZSequenceNumber());
                        shift.setSignOnSystemDateTime(new java.util.Date());
                        shift.setSignOffSystemDateTime(CreamToolkit.getInitialDate());
                        shift.setBeginTransactionNumber(connection, 100);
                        shift.setEndTransactionNumber(connection, 100);
                        shift.setBeginInvoiceNumber(connection, "0");
                        // prop.getProperty("NextInvoiceNumber"));
                        shift.setEndInvoiceNumber(connection, "0");
                        // prop.getProperty("NextInvoiceNumber"));
                        shift.setMachineNumber(param.getTerminalPhysicalNumber());

                        // Bruce/20030417
                        // 借零金额初始值 for 灿坤
                        HYIDouble initialCashInAmount = param.getInitialCashInAmount();
                        if (!initialCashInAmount.equals(zero)) {
                            shift.setCashInAmount(initialCashInAmount);
                            shift.setCashInCount(1);
                        }
                        try {
                            shift.insert(connection);
                        } catch (SQLException e) {
                            // if exists, delete it first
                            shift.deleteByPrimaryKey(connection);
                            shift.insert(connection);
                        }

                        // System.out.println(b + " = " + shift.getSequenceNumber()
                        // + " <<<>>> " + shift.getZSequenceNumber());
                        // shift.queryByPrimaryKey();

                    } catch (Exception e) {
                        System.out.println(e);
                        if (exit)
                            System.exit(0);
                    }
                    shiftbnumber = (i <= maxTranNumber) ? i + "" : (i - maxTranNumber) + "";
                    cnumber = currentTransaction.getCashierNumber();
                }

                if (dealtype2.equalsIgnoreCase("9")) {
                    shiftenumber = (i <= maxTranNumber) ? i + "" : (i - maxTranNumber) + "";
                    shifted = currentTransaction.getSystemDateTime();
                    shift.setBeginTransactionNumber(connection, Integer.valueOf(shiftbnumber));
                    shift.setEndTransactionNumber(connection, Integer.valueOf(shiftenumber));
                    shift.setAccountingDate(busiDate);
                    shift.setSignOnSystemDateTime(shiftbd);
                    shift.setSignOffSystemDateTime(shifted);
                    shift.setCashierNumber(cnumber);
                    // shift.
                    // System.out.println(shift.getSequenceNumber() + " <<<>>> " +
                    // shift.getZSequenceNumber());
                    shift.setUploadState("2");
                    shift.update(connection);
                    System.out.println();
                    System.out.println("#     this is some information of shift :");
                    System.out.println("# shift accdate = "
                        + yyyyMMdd.format(shift.getAccountingDate()));
                    System.out.println("# shift number = " + shift.getSequenceNumber());
                    System.out.println("# shift eodcnt = " + shift.getZSequenceNumber());
                    System.out.println("# shift bnumber = " + shift.getBeginTransactionNumber());
                    System.out.println("# shift enumber = " + shift.getEndTransactionNumber());
                    System.out.println("# shift upload = " + shift.getUploadState());
                    System.out.println("# shift sale amount = "
                        + new HYIDouble(0).addMe(shift.getNetSalesAmount1()).addMe(
                        shift.getNetSalesAmount2()).addMe(shift.getNetSalesAmount3()).addMe(
                        shift.getNetSalesAmount4()));

                } else if (dealtype2.equalsIgnoreCase("D")
                    || i == (bn ? Integer.parseInt(enumber) : Integer.parseInt(enumber)
                    + maxTranNumber)) {

                    z.setBeginTransactionNumber(connection, zbnumber);
                    System.out.println("zenumber=" + zenumber);
                    z.setEndTransactionNumber(connection, zenumber);
                    z.setBeginSystemDateTime(bd);
                    z.setEndSystemDateTime(ed);

                    // 根据用户输入的日期设置会计日期
                    z.setAccountingDate(busiDate);

                    z.setCashierNumber(cnumber);
                    z.setTransactionCount(zenumber - zbnumber + 1);
                    z.setUploadState("2");
                    // z.update();
                    z.update(connection);
                    System.out.println();
                    System.out.println("#     this is some information of z :");
                    System.out.println("# z accdate = " + yyyyMMdd.format(z.getAccountingDate()));
                    System.out.println("# z eodcnt = " + z.getSequenceNumber());
                    System.out.println("# z bnumber = " + z.getBeginTransactionNumber());
                    System.out.println("# z enumber = " + z.getEndTransactionNumber());
                    System.out.println("# z bdate = " + z.getBeginSystemDateTime());
                    System.out.println("# z edate = " + z.getEndSystemDateTime());
                    System.out.println("# z upload = " + z.getUploadState());
                    System.out.println("# z sale amount = "
                        + new HYIDouble(0).addMe(z.getNetSalesAmount1()).addMe(
                        z.getNetSalesAmount2()).addMe(z.getNetSalesAmount3()).addMe(
                        z.getNetSalesAmount4()));
                    // Save DepSales
                    DepSales.updateAll(connection, busiDate);
                }
            }
            //connection.commit();

            POSTerminalApplication.isRecalcMode = check;
            System.out.println("# finished.");

        } catch (ConfigurationNotFoundException e) {
            CreamToolkit.logMessage(e);
        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        if (exit)
            System.exit(0);
    }

    public ShiftReport getNewShift() {
        return shift;
    }

    public ZReport getNewZ() {
        return z;
    }

    public PayingPaneBanner getPayingPane1() {
        return payingPane1;
    }

    public PayingPaneBanner getPayingPane2() {
        return payingPane2;
    }

    public JPanel getButtonPanel() {
        return buttonPanel;
    }

    public CardLayout getButtonCardLayout() {
        return buttonCardLayout;
    }

    public JPanel getButtonPanel1() {
        return buttonPanel1;
    }

    public JPanel getButtonPanel2() {
        return buttonPanel2;
    }

    public DynaPayingPaneBanner getDyanPayingPane() {
        return dynaPayingPane;
    }

    public CustPayingPaneBanner getCustPayingPane() {
        return null; //custPayingPane;
    }

    /**
     * @author ll 2003.04.17 input : property.ScaleBarCodesFormat :
     * 2IIIIIIWW.WWWPPP.PPC 开头的数值:前置码 I:plu编号 W:商品重量 P:商品金额 C:检核码
     * .:是小数点(在实际条码中不存在)
     * <p/>
     * output : java.util.Map key : 条码总长(不包括小数点) value : 格式如下 java.util.List
     * list.get(0) String : 前置码 list.get(1) Integer : plu编号开始位置 list.get(2)
     * Integer : plu编号结束位置 list.get(3) Integer : 商品重量开始位置 list.get(4) Integer :
     * 商品重量结束位置 list.get(5) Integer : 商品重量的小数点位置 list.get(6) Integer : 商品金额开始位置
     * list.get(7) Integer : 商品金额结束位置 list.get(8) Integer : 商品金额的小数点位置
     */
    public java.util.Map getScaleBarCodeFormat() {
        if (scaleBarCodesFormatMap != null && !scaleBarCodesFormatMap.isEmpty())
            return scaleBarCodesFormatMap;
        scaleBarCodesFormatMap = new HashMap();
        String scaleBarCodesFormat = param.getScaleBarCodeFormat();
        if (scaleBarCodesFormat.trim().equals(""))
            return null; // Bruce/20030505/ prevent a NullPointerException problem
        StringTokenizer st = new StringTokenizer(scaleBarCodesFormat, ",");
        while (st.hasMoreTokens()) {
            try {
                String scaleBarCodeFormat = st.nextToken();
                String pref = "";
                int length = scaleBarCodeFormat.length();
                int totalLength = 0;

                char flag = scaleBarCodeFormat.charAt(totalLength);
                while (flag <= '9' && flag >= '0' && totalLength < length) {
                    pref += String.valueOf(flag);
                    totalLength++;
                    flag = scaleBarCodeFormat.charAt(totalLength);
                }

                totalLength = 0;
                for (int i = 0; i < length; i++) {
                    flag = scaleBarCodeFormat.charAt(i);
                    if (flag != '.')
                        totalLength++;
                }

                java.util.List iList = getPosition(scaleBarCodeFormat, 'I');
                java.util.List wList = getPosition(scaleBarCodeFormat, 'W');
                java.util.List pList = getPosition(scaleBarCodeFormat, 'P');

                java.util.List scaleBarCodeFormatList = new ArrayList();
                scaleBarCodeFormatList.add(pref);
                scaleBarCodeFormatList.add(iList.get(1));
                scaleBarCodeFormatList.add(iList.get(2));
                scaleBarCodeFormatList.add(wList.get(1));
                scaleBarCodeFormatList.add(wList.get(2));
                scaleBarCodeFormatList.add(wList.get(3));
                scaleBarCodeFormatList.add(pList.get(1));
                scaleBarCodeFormatList.add(pList.get(2));
                scaleBarCodeFormatList.add(pList.get(3));

                scaleBarCodesFormatMap.put(String.valueOf(totalLength),
                    scaleBarCodeFormatList);
            } catch (Exception e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        }

        return scaleBarCodesFormatMap;
    }

    /**
     * @param source
     * @param flag
     * @return java.util.List list(0) : flag list(1) : start pos list(2) : end
     *         pos list(3) : comma pos
     */
    private java.util.List getPosition(String source, char flag) {
        java.util.List list = new Vector();
        int index = source.indexOf(flag);
        if (index < 0) {
            list.add(String.valueOf(flag));
            list.add(new Integer(0));
            list.add(new Integer(0));
            list.add(new Integer(0));
            return list;
        }

        int index1 = source.indexOf(".");
        int prefCount = 0;
        if (index > index1) {
            while (index1 >= 0) {
                prefCount++;
                index1 = source.substring(index1 + 1, index).indexOf(".");
            }
        }

        boolean has = true;
        int length = 0;
        int i = index;
        int pos = 0;
        while (i < source.length() && has) {
            char tmp = source.charAt(i);
            if (tmp == flag) {
                length++;
            } else if (tmp == '.') {
                pos = length + 1;
            } else {
                has = false;
            }
            i++;
        }
        list.add(String.valueOf(flag));
        list.add(new Integer(index - prefCount));
        list.add(new Integer(index - prefCount + length));
        list.add(new Integer(pos - 1));
        return list;
    }

    public TotalAmountBanner getTotalAmountBanner() {
        return totalAmountBanner;
    }

    public void setCrdPaymentMenuVisible(boolean visible) {
        crdPaymentMenuVisible = visible;
    }

    public boolean getCrdPaymentMenuVisible() {
        return crdPaymentMenuVisible;
    }

//    public void setAnimatedVisible(boolean visible, String mmID) {
//		try {
//			if (!visible) {
//				digPrize.setVisible(false);
//				if (media != null) {
//					media.setVisible(true);
//				}
//			} else {
//				if (media != null) {
//					media.setVisible(false);
//				}
//				digPrize.initData(mmID);
//				digPrize.initButtPane();
//				digPrize.setVisible(true);
//				digPrize.validate();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//    }

//    public void setDigPrizeMessage(String msg) {
//		digPrize.setMessage(msg);
//    }

//    public GiftGroup fireDigPrizeEvent(int index) {
//		return digPrize.fireButtonEvent(index);
//        return null;
//    }

    /**
     * 退货数据是否从sc来
     * 从sc来:不需要对源交易设置退货标志
     *
     * @return
     */
    public boolean isReturnFromSC() {
        return isReturnFromSC;
    }

    public void setReturnFromSC(boolean isReturnFromSC) {
        this.isReturnFromSC = isReturnFromSC;
    }

    public boolean isEnableKeylock() {
        return enableKeylock;
    }

    public void setEnableKeylock(boolean enableKeylock) {
        this.enableKeylock = enableKeylock;
    }

    public String getLastInvoiceID() {
        return lastInvoiceID;
    }

    public void setLastInvoiceID(String lastInvoiceID) {
        this.lastInvoiceID = lastInvoiceID;
    }

    public String getLastInvoiceNumber() {
        return lastInvoiceNumber;
    }

    public void setLastInvoiceNumber(String lastInvoiceNumber) {
		this.lastInvoiceNumber = lastInvoiceNumber;
	}

    public EPOSBackground getPosBackgroundPanel() {
        return posBackgroundPanel;
    }

    public ProductInfoBanner getProductInfoBanner() {
        return productInfoBanner;
    }

    public void repaintEverything() {
        getPosBackgroundPanel().setOffScreenImageInvalid(true);
        getProductInfoBanner().setInvalid(true);
        getTotalAmountBanner().setInvalid(true);
        getPosBackgroundPanel().repaint();
        if (getPopupMenuPane().isVisible())
            getPopupMenuPane().repaint();
    }

    public void close() {
        POSPeripheralHome3 posPeripheralHome = getPOSPeripheralHome();
        if (posPeripheralHome != null && posPeripheralHome.getLineDisplay() != null) {
            try {
                posPeripheralHome.getLineDisplay().clearText();
            } catch (JposException e) {
            }
        }
        try {
            Client.getInstance().processCommand("quit");
        } catch (ClientCommandException e) {
        }
        getMessageIndicator().setMessage("");
        getInformationIndicator().setMessage("");
        getWarningIndicator().setMessage("");
        ThreadWatchDog.close();
        MasterDownloadThread.getInstance().close();
        JettyStarter.stop();
        EPOSBackground.close();
        PopupMenuPane.close();
        SignOffForm.close();
        CreditCardForm.close();

        posTerminalApplication = null;
        contentPane = null;
        mainFrame = null;
    }
}
