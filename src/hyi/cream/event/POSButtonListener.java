package hyi.cream.event;


public interface POSButtonListener extends java.util.EventListener {

    /**
     * Invoked when a POS button has been pressed.
     * @param e an event object.
     */
    public void buttonPressed(POSButtonEvent e);
}

 