package hyi.cream.event;

import java.util.EventObject;


public class TransactionEvent extends EventObject {
    int StateChanged;
    Object o;

    /** This id indicates that an line item has been added into the transaction.
     */
    public static final int ITEM_ADDED = 0;

    /** This id indicates that an line item has been removed from the transaction.
     */
    public static final int ITEM_REMOVED = 1;

    /** This id indicates that an line item has been modified in the transaction.
     */
    public static final int ITEM_CHANGED = 2;
 
    /** This id indicates that some transaction information is changed. */
    public static final int TRANS_INFO_CHANGED = 3;

    /** This id indicates that it begins with a new transaction now.
     */
    public static final int TRANS_RENEW = 4;

    /**
     * Constructor.
     * @param source the object that originated the event
     * @param stateChanged an integer that indicates the changes of transaction,
     *        it could be ITEM_ADDED, ITEM_REMOVED, ITEM_CHANGED, or
     *        TRANS_RENEW.
     */
    public TransactionEvent(Object source, int StateChanged) {
        super(source);
        this.StateChanged = StateChanged;
    }
    
    public TransactionEvent(Object source, int StateChanged, Object o) {
        super(source);
        this.StateChanged = StateChanged;
        this.o = o;
    }

    /**
     * Returns the type of changes of transaction.
     * @return an integer that indicates the changes of transactions, it could
     *           be ITEM_ADDED, ITEM_REMOVED, ITEM_CHANGED or TRANS_RENEW.
     */
    public int getStateChanged() {
        return StateChanged;
    }

    public Object getObject() {
        return o;
    }
}

