package hyi.cream.event;

import java.io.IOException;

public class POSButtonEvent extends java.util.EventObject {

    public POSButtonEvent(Object source) {
        super(source);
    }
    
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.writeObject(source);
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException,
            ClassNotFoundException {
        source =  in.readObject();
    }
}

 