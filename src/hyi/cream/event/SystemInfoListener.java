package hyi.cream.event;


public interface SystemInfoListener extends java.util.EventListener {

    /**
     * Invoked when some sytem informations have been changed.
    * @param e an event object represents the changes.
     */
    public void systemInfoChanged(SystemInfoEvent e);
}
