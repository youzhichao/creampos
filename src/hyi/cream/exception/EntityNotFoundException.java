package hyi.cream.exception;

public class EntityNotFoundException extends Exception{
    private static final long serialVersionUID = 1L;

    public EntityNotFoundException() {
        super();
    }

    public EntityNotFoundException(String s) {
        super(s);
    }
}