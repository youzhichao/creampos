package hyi.cream.exception;

public class NoSuchPOSDeviceException extends Exception{
    private static final long serialVersionUID = 1L;

    public NoSuchPOSDeviceException() {
        super();
    }

    public NoSuchPOSDeviceException(String s) {
        super(s);
    }
}