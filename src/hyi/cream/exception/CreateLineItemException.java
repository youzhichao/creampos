package hyi.cream.exception;

public class CreateLineItemException extends Exception{
    private static final long serialVersionUID = 1L;

    public CreateLineItemException() {
        super();
    }

    public CreateLineItemException(String s) {
        super(s);
    }
}