package hyi.cream.exception;

public class LineItemNotFoundException extends Exception{
    private static final long serialVersionUID = 1L;

    public LineItemNotFoundException() {
        super();
    }

    public LineItemNotFoundException(String s) {
        super(s);
    }
}