package hyi.cream.exception;

public class TooManyLineItemsException extends Exception{
    private static final long serialVersionUID = 1L;

    public TooManyLineItemsException() {
        super();
    }

    public TooManyLineItemsException(String s) {
        super(s);
    }
}

