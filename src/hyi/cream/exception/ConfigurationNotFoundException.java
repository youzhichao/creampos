package hyi.cream.exception;

public class ConfigurationNotFoundException extends Exception{
    private static final long serialVersionUID = 1L;

    public ConfigurationNotFoundException() {
        super();
    }

    public ConfigurationNotFoundException(String s) {
        super(s);
    }
}