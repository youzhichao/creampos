package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;

import java.math.BigDecimal;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Providing query method for sequence number from inline client. Including
 * transaction number, and Z number.
 * 
 * <P>
 * POS换机后第一次使用，必须连线，以便取得上次的最大交易序号和Z号。
 * 
 * @author Bruce
 */
public class SequenceNumberGetter extends DacBase {
    private static final long serialVersionUID = 1L;

	public SequenceNumberGetter() {
	}

	public List getPrimaryKeyList() {
		return null;
	}

	public String getInsertUpdateTableName() {
		return null;
	}

	/**
	 * Query object method. This method will be invoked by ServerThread when
	 * receiving "getObject hyi.cream.dac.SequenceNumberGetter
	 * [{pos_no},{tran|z}]" command.
	 * 
	 * @param data
	 * @return Collection Return a Collection contains a SequenceNumberGetter
	 *         object for sequence number.
	 */
	public static Collection queryObject(String data) {
	    List retList = new ArrayList();
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
			StringTokenizer tk = new StringTokenizer(data, ",");
			String posNumber = tk.nextToken();
			String type = tk.nextToken();
			String sql = "";
			if (type.equals("tran")) {
				SequenceNumberGetter obj = null;
				//if (GetProperty.getDBType("MySql").equalsIgnoreCase("MySql")) { // PostgreSQL is also ok
                sql = "select transactionNumber tranno from posul_tranhead"
                    + " where posNumber=" + posNumber
                    + " order by systemDate desc limit 1";
				//} else {
				//	sql = "select transactionNumber tranno from posul_tranhead where posNumber="
				//		+ posNumber
				//		+ " AND systemdate = (select max(systemdate) from posul_tranhead where posNumber="
				//		+ posNumber	+ ")";
				//}
				obj = DacBase.getSingleObject(connection, SequenceNumberGetter.class, sql);
				retList.add(obj);
				CreamToolkit.logMessage("---- queryMaxTran | sql : " + sql + " | getMaxTransactionNumber : " + obj.getMaxTransactionNumber());
			} else if (type.equals("z")) {
				SequenceNumberGetter obj = DacBase.getSingleObject(connection, 
                    SequenceNumberGetter.class, "select max(zSequenceNumber) zno from posul_z"
					+ " where posNumber=" + posNumber);
				retList.add(obj);
			} else if (type.equals("book")) {
                SequenceNumberGetter obj = DacBase.getSingleObject(connection,
                        SequenceNumberGetter.class, "select billNo, totCount from ordbookhead "
                                + " where tranNo='" + posNumber +"'");
                if (obj != null){
                    SequenceNumberGetter obj2 = DacBase.getSingleObject(connection,
                            SequenceNumberGetter.class, "select count(*) as count from ordbookdtl "
                                    + " where tranNo='" + posNumber + "' and billNo = '"+obj.getBillNo()+"'");
                    if (obj.getTotCount().intValue() != obj2.getCount()) {
                        retList.add("false");
                    } else {
                        retList.add("true");
                    }
                }
            }
		} catch (NoSuchElementException e) {
			return null;
		} catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } catch (EntityNotFoundException e) {
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return retList;
	}

	public Integer getMaxTransactionNumber() {
		Object obj = getFieldValue("tranno");
		if (obj == null)
			return null;
		else {
			try {
				return new Integer(obj.toString());
			} catch (Exception e) {
				return new Integer((new BigDecimal(obj.toString())).intValue());
			}
		}
	}

	public Integer getMaxZNumber() {
		Object obj = getFieldValue("zno");
		if (obj == null)
			return null;
		else {
			try {
				return new Integer(obj.toString());
			} catch (Exception e) {
				return new Integer((new BigDecimal(obj.toString())).intValue());
			}
		}
	}

    public String getBillNo() {
        Object obj = getFieldValue("billNo");
        if (obj == null)
            return null;
        else {
            try {
                return obj.toString();
            } catch (Exception e) {
                return obj.toString();
            }
        }
    }

    public Integer getCount() {
        Object obj = getFieldValue("count");
        if (obj == null)
            return null;
        else {
            try {
                return new Integer(obj.toString());
            } catch (Exception e) {
                return new Integer((new BigDecimal(obj.toString())).intValue());
            }
        }
    }

    public Integer getTotCount() {
        Object obj = getFieldValue("totcount");
        if (obj == null)
            return null;
        else {
            try {
                return new Integer(obj.toString());
            } catch (Exception e) {
                return new Integer((new BigDecimal(obj.toString())).intValue());
            }
        }
    }
}
