package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * 自助点单单头unionpay_detail
 * @author Administrator
 *
 */
public class Selfbuy_detail extends DacBase implements Serializable {

	static final String tableName = "selfbuy_detail";
	private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("storeid");
		primaryKeys.add("orderid");
		primaryKeys.add("seq");
		primaryKeys.add("orderno");
	}

	public Selfbuy_detail() throws InstantiationException {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (Server.serverExist()) {
			return "posul_selfbuy_detail";
		}
		return tableName;
	}

	public static String getInsertUpdateTableNameStaic() {
		if (Server.serverExist()) {
			return "posul_selfbuy_detail";
		}
		return "selfbuy_detail";
	}

	public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
				{"storeid", "storeid"},
				{"orderid", "orderid"},
				{"orderno", "orderno"},
				{"seq","seq"},
				{"barcode", "barcode"},
				{"goodsno","goodsno"},
				{"promotionno","promotionno"},
				{"num","num"},
				{"price","price"},
				{"paid","paid"},
				{"discount","discount"},
				{"tmtranseq","tmtranseq"},
				{"systemdate","systemdate"},
		};
	}

	//STOREID,店号
	public String getStoreId() {
		return (String) getFieldValue("storeid");
	}
	public void setStoreId(String storeNumber) {
		 setFieldValue("storeid",storeNumber);
	}

	//orderid, 订单id
	public Integer getOrderId() {
		return (Integer) getFieldValue("orderid");
	}
	public void setOrderId(Integer posNumber) {
		setFieldValue("orderid",posNumber);
	}

	//orderno, 订单号
	public String getOrderNo() {
		return (String) getFieldValue("orderno");
	}
	public void setOrderNo(String employeeId) {
		setFieldValue("orderno",employeeId);
	}

	//流水号
	public String getSEQ(){
		return (String)getFieldValue("SEQ");
	}
	public void setSEQ(String s){
		setFieldValue("SEQ",s);
	}

	//barcode,商品条码
	public String getBarcode() {
		return (String) getFieldValue("barcode");
	}
	public void setBarcode(String temperature) {
		setFieldValue("barcode",temperature);
	}

	//goodsno,商品货号
	public String getGoodsno() {
		return (String) getFieldValue("goodsno");
	}
	public void setGoodsno(String temperature) {
		setFieldValue("goodsno",temperature);
	}

	//promotionno,促销活动号
	public String getPromotionno() {
		return (String) getFieldValue("promotionno");
	}
	public void setPromotionno(String temperature) {
		setFieldValue("promotionno",temperature);
	}

	//num,数量
	public Integer getNum() {
		return (Integer)getFieldValue("num");
	}

	public void setNum(Integer s){
		setFieldValue("num",s);
	}

    //price,原价
    public void setPrice(HYIDouble date) {
        setFieldValue("price",date);
    }
    public HYIDouble getPrice() {
        return (HYIDouble)getFieldValue("price");
    }

	//paid,付款金额
	public HYIDouble getpaid() {
		return (HYIDouble)getFieldValue("paid");
	}

	public void setPaid(HYIDouble s){
		setFieldValue("paid",s);
	}

    //discount,优惠金额
    public HYIDouble getDiscount(){
        return (HYIDouble)getFieldValue("discount");
    }
    public void setDiscount(HYIDouble s){
        setFieldValue("discount",s);
    }

	//tmtranseq,交易号
	public Integer getTmtranseq() {
		return (Integer)getFieldValue("tmtranseq");
	}

	public void setTmtranseq(Integer s){
		setFieldValue("tmtranseq",s);
	}

	//SYSTEMDATE,时间
	public void setSYSTEMDATE(Date date) {
		setFieldValue("systemDate",date);
	}
	public Date getSYSTEMDATE() {
		return (Date)getFieldValue("systemDate");
	}

	public static Iterator queryByTranNo(DbConnection connection, int tranNo) {
		try {
			return getMultipleObjects(connection, Selfbuy_detail.class,"select * from " + getInsertUpdateTableNameStaic() + " where  tmtranseq = " + tranNo);
		} catch (EntityNotFoundException e) {
//            CreamToolkit.logMessage(e);
		} catch (SQLException e) {
			CreamToolkit.logMessage(e);
		}
		return null;
	}

	public Selfbuy_detail cloneForSC() {
		String[][] fieldNameMap = getPosToScFieldNameArray();
		try {
			Selfbuy_detail clonedObj = new Selfbuy_detail();
			for (int i = 0; i < fieldNameMap.length; i++) {
				Object value = this.getFieldValue(fieldNameMap[i][0]);
				if (value == null) {
					try {
						value = this.getClass().getDeclaredMethod(
								"get" + fieldNameMap[i][0], new Class[0]).invoke(this, new Object[0]);
					} catch (Exception e) {
						value = null;
					}
				}
				clonedObj.setFieldValue(fieldNameMap[i][1], value);
			}
			return clonedObj;
		} catch (InstantiationException e) {
			return null;
		}
	}

	static public void deleteOutdatedData() {
		DbConnection connection = null;
		Statement statement = null;
		try {
			connection = CreamToolkit.getPooledConnection();
			statement = connection.createStatement();
			if (Server.serverExist()) {
				// 需要保留的天数
				int tranReserved = PARAM.getTransactionReserved();
				long l = new Date().getTime() - tranReserved * 1000L * 3600 * 24;
				String baseTime = CreamCache.getInstance().getDateTimeFormate().format(new java.sql.Date(l));
				statement.executeUpdate("DELETE FROM posul_selfbuy_detail WHERE systemDate < '" + baseTime + "'");
			} else {
				Iterator<String> itr = Transaction.getOutdateTranNumbers();
				while (itr != null && itr.hasNext())
					statement.executeUpdate("DELETE FROM selfbuy_detail WHERE tmtranseq=" + itr.next());
			}
		} catch (SQLException e) {
			e.printStackTrace(CreamToolkit.getLogger());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				e.printStackTrace(CreamToolkit.getLogger());
			}
			if (connection != null)
				CreamToolkit.releaseConnection(connection);
		}
	}

}
