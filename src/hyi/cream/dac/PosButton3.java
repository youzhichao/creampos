package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Store DAC.
 * 
 * @author Bruce
 */
public class PosButton3 extends DacBase {
    private static final long serialVersionUID = 1L;

    transient public static final String VERSION = "1.0";
    private static ArrayList primaryKeys = new ArrayList();
    static {
        primaryKeys.add("fID");
    }

    /**
     * Constructor
     */
    public PosButton3() {
    }

    /**
     * @see hyi.cream.dac.DacBase#getPrimaryKeyList()
     */
    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public String getInsertUpdateTableName() {
        return "posbutton3";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        return "posbutton3";
    }

    public String getfID() {
        return (String) getFieldValue("fID");
    }

    public String getfValue() {
        return (String) getFieldValue("fValue");
    }

   public String getfFileName() {
        return (String) getFieldValue("fFileName");
    }

   

	
	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("fID", "fID");
        fieldNameMap.put("fValue", "fValue");
        fieldNameMap.put("fFileName", "fFileName");
        
        return fieldNameMap;	
	}
	
    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.PosButton3.class,
                "SELECT * FROM posdl_posbutton3", getScToPosFieldNameMap());
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static String read() throws IOException {
        StringBuffer sb = new StringBuffer();
        BufferedReader r = new BufferedReader(new FileReader("posbutton3.conf"));
        String s;
        while ((s = r.readLine()) != null) {
            sb.append(s);
            sb.append("\n");
        }
        r.close();
        return sb.toString();
    }

    public static void createPosButton3File() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
        	BufferedWriter w = new BufferedWriter(new FileWriter(CreamToolkit.getConfigDir() + "posbutton3.conf"));
            List list = DacBase.getMultipleObjects(connection, hyi.cream.dac.PosButton3.class,
                "SELECT * FROM posbutton3", null);
            for(int i = 0; i < list.size(); i++){
                w.write(((PosButton3)list.get(i)).getfValue());
                w.newLine();
            }
            w.close();
        } catch (IOException e) {
            CreamToolkit.logMessage(e);
        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage("Cannot find records in postbutton3.");
        } catch (SQLException e) {
            // silent on loss of posbutton3 table
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}
