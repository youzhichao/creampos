package hyi.cream.dac;

//import hyi.cream.exception.EntityNotFoundException;
//import hyi.cream.util.CreamCache;
//import hyi.cream.util.HYIDouble;
//
//import java.io.Serializable;
//import hyi.cream.util.DbConnection;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Iterator;
//import java.util.List;
//import hyi.cream.util.CreamToolkit;

/**
 * TO BE REMOVED.
 */
public class HistoryTrans { /* extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    transient public static final String VERSION = "1.0";

    static final String tableName = "historytrans";
    
    private static ArrayList primaryKeys = new ArrayList();

    private static final Collection existedFieldList = getExistedFieldList(getInsertUpdateTableNameStaticVersion());

    static {
        primaryKeys.add("storeid");
        primaryKeys.add("systemdatetime");
        primaryKeys.add("terminalnumber");
        primaryKeys.add("itemseq");
        primaryKeys.add("printtrannumber");
    }
    
    public HistoryTrans() {
		super();
	}

    public String getInsertUpdateTableName() {
        if (getUseClientSideTableName())
            return tableName;
        else if (hyi.cream.inline.Server.serverExist())
            return "posul_" + tableName;
        else
            return tableName;
    }

    public static String getInsertUpdateTableNameStaticVersion( ) {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_" + tableName;
        else
            return tableName;
    }

    public HYIDouble getAmount() {
        return (HYIDouble)this.getFieldValue("amount");
    }

    public void setAmount(HYIDouble amount) {
        setFieldValue("amount", amount);
    }

    public String getCashierName() {
        return (String)this.getFieldValue("cashiername");
    }

    public void setCashierName(String cashiername) {
        setFieldValue("cashiername", cashiername);
    }

    public String getCashierNO() {
        return (String)this.getFieldValue("cashierno");
    }

    public void setCashierNO(String cashierno) {
        setFieldValue("cashierno", cashierno);
    }
    
    public HYIDouble getChangeamount() {
        return (HYIDouble)this.getFieldValue("changeamount");
    }

    public void setChangeamount(HYIDouble changeamount) {
        setFieldValue("changeamount", changeamount);
    }

    public String getCodetx() {
        return (String)this.getFieldValue("codetx");
    }

    public void setCodetx(String codetx) {
        setFieldValue("codetx", codetx);
    }
    
    public HYIDouble getDaifuamount() {
        return (HYIDouble)this.getFieldValue("daifuamount");
    }

    public void setDaifuamount(HYIDouble daifuamount) {
        setFieldValue("daifuamount", daifuamount);
    }

    public HYIDouble getDaishouamount() {
        return (HYIDouble)this.getFieldValue("daishouamount");
    }

    public void setDaishouamount(HYIDouble daishouamount) {
        setFieldValue("daishouamount", daishouamount);
    }

    public HYIDouble getDaishouamount2() {
        return (HYIDouble)this.getFieldValue("daishouamount2");
    }

    public void setDaishouamount2(HYIDouble daishouamount2) {
        setFieldValue("daishouamount2", daishouamount2);
    }

    public String getDealtype1() {
        return (String)this.getFieldValue("dealtype1");
    }

    public void setDealtype1(String dealtype1) {
        setFieldValue("dealtype1", dealtype1);
    }

    public String getDealtype2() {
        return (String)this.getFieldValue("dealtype2");
    }

    public void setDealtype2(String dealtype2) {
        setFieldValue("dealtype2", dealtype2);
    }
    
    public String getDealtype3() {
        return (String)this.getFieldValue("dealtype3");
    }

    public void setDealtype3(String dealtype3) {
        setFieldValue("dealtype3", dealtype3);
    }
    
    public String getDisctype() {
        return (String)this.getFieldValue("disctype");
    }

    public void setDisctype(String disctype) {
        setFieldValue("disctype", disctype);
    }
    
    public HYIDouble getDtlaftdscamt() {
        return (HYIDouble)this.getFieldValue("dtlaftdscamt");
    }

    public void setDtlaftdscamt(HYIDouble dtlaftdscamt) {
        setFieldValue("dtlaftdscamt", dtlaftdscamt);
    }

    public HYIDouble getDtltaxamt() {
        return (HYIDouble)this.getFieldValue("dtltaxamt");
    }

    public void setDtltaxamt(HYIDouble dtltaxamt) {
        setFieldValue("dtltaxamt", dtltaxamt);
    }

    public String getDtltaxtype() {
        return (String)this.getFieldValue("dtltaxtype");
    }

    public void setDtltaxtype(String dtltaxtype) {
        setFieldValue("dtltaxtype", dtltaxtype);
    }

    public String getEmpno() {
        return (String)this.getFieldValue("empno");
    }

    public void setEmpno(String empno) {
        setFieldValue("empno", empno);
    }

    public Integer getEodcnt() {
        return (Integer)this.getFieldValue("eodcnt");
    }

    public void setEodcnt(Integer eodcnt) {
        setFieldValue("eodcnt", eodcnt);
    }

    public HYIDouble getGrosssalesamount() {
        return (HYIDouble)this.getFieldValue("grosssalesamount");
    }

    public void setGrosssalesamount(HYIDouble grosssalesamount) {
        setFieldValue("grosssalesamount", grosssalesamount);
    }

    public String getItemnumber() {
        return (String)this.getFieldValue("itemnumber");
    }

    public void setItemnumber(String itemnumber) {
        setFieldValue("itemnumber", itemnumber);
    }

    public Integer getItemseq() {
        return (Integer)this.getFieldValue("itemseq");
    }

    public void setItemseq(Integer itemseq) {
        setFieldValue("itemseq", itemseq);
    }
    
    public String getItemvoid() {
        return (String)this.getFieldValue("itemvoid");
    }

    public void setItemvoid(String itemvoid) {
        setFieldValue("itemvoid", itemvoid);
    }

    public String getMicrocatno() {
        return (String)this.getFieldValue("microcatno");
    }

    public void setMicrocatno(String microcatno) {
        setFieldValue("microcatno", microcatno);
    }

    public String getMidcatno() {
        return (String)this.getFieldValue("midcatno");
    }

    public void setMidcatno(String midcatno) {
        setFieldValue("midcatno", midcatno);
    }
    
    public HYIDouble getMnmtotamt() {
        return (HYIDouble)this.getFieldValue("mnmtotamt");
    }

    public void setMnmtotamt(HYIDouble mnmtotamt) {
        setFieldValue("mnmtotamt", mnmtotamt);
    }

//    public HYIDouble getNetsalamt() {
//        return (HYIDouble)this.getFieldValue("netsalamt");
//    }
//
//    public void setNetsalamt(HYIDouble netsalamt) {
//        setFieldValue("netsalamt", netsalamt);
//    }
//
    public HYIDouble getOrigprice() {
        return (HYIDouble)this.getFieldValue("origprice");
    }

    public void setOrigprice(HYIDouble origprice) {
        setFieldValue("origprice", origprice);
    }

    public HYIDouble getSpillamount() {
        return (HYIDouble)this.getFieldValue("spillamount");
    }

    public void setSpillamount(HYIDouble spillamount) {
        setFieldValue("spillamount", spillamount);
    }

    public HYIDouble getPayamount1() {
        return (HYIDouble)this.getFieldValue("payamount1");
    }

    public void setPayamount1(HYIDouble payamount1) {
        setFieldValue("payamount1", payamount1);
    }

    public HYIDouble getPayamount2() {
        return (HYIDouble)this.getFieldValue("payamount2");
    }

    public void setPayamount2(HYIDouble payamount2) {
        setFieldValue("payamount2", payamount2);
    }

    public HYIDouble getPayamount3() {
        return (HYIDouble)this.getFieldValue("payamount3");
    }

    public void setPayamount3(HYIDouble payamount3) {
        setFieldValue("payamount3", payamount3);
    }

    public HYIDouble getPayamount4() {
        return (HYIDouble)this.getFieldValue("payamount4");
    }

    public void setPayamount4(HYIDouble payamount4) {
        setFieldValue("payamount4", payamount4);
    }

    public String getPayname1() {
        return (String)this.getFieldValue("payname1");
    }

    public void setPayname1(String payname1) {
        setFieldValue("payname1", payname1);
    }

    public String getPayname2() {
        return (String)this.getFieldValue("payname2");
    }

    public void setPayname2(String payname2) {
        setFieldValue("payname2", payname2);
    }

    public String getPayname3() {
        return (String)this.getFieldValue("payname3");
    }

    public void setPayname3(String payname3) {
        setFieldValue("payname3", payname3);
    }

    public String getPayname4() {
        return (String)this.getFieldValue("payname4");
    }

    public void setPayname4(String payname4) {
        setFieldValue("payname4", payname4);
    }

    public String getPayno1() {
        return (String)this.getFieldValue("payno1");
    }

    public void setPayno1(String payno1) {
        setFieldValue("payno1", payno1);
    }

    public String getPayno2() {
        return (String)this.getFieldValue("payno2");
    }

    public void setPayno2(String payno2) {
        setFieldValue("payno2", payno2);
    }

    public String getPayno3() {
        return (String)this.getFieldValue("payno3");
    }

    public void setPayno3(String payno3) {
        setFieldValue("payno3", payno3);
    }

    public String getPayno4() {
        return (String)this.getFieldValue("payno4");
    }

    public void setPayno4(String payno4) {
        setFieldValue("payno4", payno4);
    }

    public String getPaytype1() {
        return (String)this.getFieldValue("paytype1");
    }

    public void setPaytype1(String paytype1) {
        setFieldValue("paytype1", paytype1);
    }

    public String getPaytype2() {
        return (String)this.getFieldValue("paytype2");
    }

    public void setPaytype2(String paytype2) {
        setFieldValue("paytype2", paytype2);
    }

    public String getPaytype3() {
        return (String)this.getFieldValue("paytype3");
    }

    public void setPaytype3(String paytype3) {
        setFieldValue("paytype3", paytype3);
    }

    public String getPaytype4() {
        return (String)this.getFieldValue("paytype4");
    }

    public void setPaytype4(String paytype4) {
        setFieldValue("paytype4", paytype4);
    }

    public String getPluno() {
        return (String)this.getFieldValue("pluno");
    }

    public void setPluno(String pluno) {
        setFieldValue("pluno", pluno);
    }

    public String getDescription() {
        return (String)this.getFieldValue("description");
    }

    public void setDescription(String description) {
        setFieldValue("description", description);
    }
    				  
    public Integer getTerminalnumber() {
        return (Integer)this.getFieldValue("terminalnumber");
    }

    public void setTerminalnumber(Integer terminalnumber) {
        setFieldValue("terminalnumber", terminalnumber);
    }
    
    public HYIDouble getQuantity() {
        return (HYIDouble)this.getFieldValue("quantity");
    }

    public void setQuantity(HYIDouble quantity) {
        setFieldValue("quantity", quantity);
    }
    
    public HYIDouble getSalesamount() {
        return (HYIDouble)this.getFieldValue("salesamount");
    }

    public void setSalesamount(HYIDouble salesamount) {
        setFieldValue("salesamount", salesamount);
    }

    public String getStoreaddress() {
        return (String)this.getFieldValue("storeaddress");
    }

    public void setStoreaddress(String storeaddress) {
        setFieldValue("storeaddress", storeaddress);
    }

    public String getStoreid() {
        return (String)this.getFieldValue("storeid");
    }

    public void setStoreid(String storeid) {
        setFieldValue("storeid", storeid);
    }

    public String getStorename() {
        return (String)this.getFieldValue("storename");
    }

    public void setStorename(String storename) {
        setFieldValue("storename", storename);
    }

    public java.util.Date getAccdate() {
        return (java.util.Date)this.getFieldValue("accdate");
    }

    public void setAccdate(java.util.Date accdate) {
        setFieldValue("accdate", accdate);
    }

    public java.util.Date getSystemdatetime() {
        return (java.util.Date)this.getFieldValue("systemdatetime");
    }

    public void setSystemdatetime(java.util.Date systemdatetime) {
        setFieldValue("systemdatetime", systemdatetime);
    }

    public HYIDouble getTotmamount() {
        return (HYIDouble)this.getFieldValue("totmamount");
    }

    public void setTotmamount(HYIDouble totmamount) {
        setFieldValue("totmamount", totmamount);
    }

    public Integer getPrinttrannumber() {
        return (Integer)this.getFieldValue("printtrannumber");
    }

    public void setPrinttrannumber(Integer printtrannumber) {
        setFieldValue("printtrannumber", printtrannumber);
    }

    public String getTrantype() {
        return (String)this.getFieldValue("trantype");
    }

    public void setTrantype(String trantype) {
        setFieldValue("trantype", trantype);
    }

    public HYIDouble getUnitprice() {
        return (HYIDouble)this.getFieldValue("unitprice");
    }

    public void setUnitprice(HYIDouble unitprice) {
        setFieldValue("unitprice", unitprice);
    }

	public Collection getExistedFieldList() {
		return existedFieldList;
	}

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

	public static String[][] getPosToScFieldNameArray() {
		String[][] names = new String[][] { 
				{ "accdate", "accdate" },
				{ "amount", "amount" },
				{ "cashiername", "cashiername" }, 
				{ "cashierno", "cashierno" },
				{ "changeamt", "changeamt" },
				{ "codetx", "codetx" }, 
				{ "daifuamt", "daifuamt" },
				{ "daishouamt", "daishouamt" }, 
				{ "daishouamt2", "daishouamt2" },
				{ "dealtype1", "dealtype1" },
				{ "dealtype2", "dealtype2" },
				{ "dealtype3", "dealtype3" },
				{ "disctype", "disctype" }, 
				{ "dtlaftdscamt", "dtlaftdscamt" },
				{ "dtltaxamt", "dtltaxamt" },
				{ "dtltaxtype", "dtltaxtype" },
				{ "empno", "empno" }, 
				{ "eodcnt", "eodcnt" },
				{ "grosssalesamount", "grosssalesamount" }, 
				{ "itemnumber", "itemnumber" },
				{ "itemscreenname", "itemscreenname" }, 
				{ "itemseq", "itemseq" },
				{ "itemvoid", "itemvoid" },
				{ "microcatno", "microcatno" },
				{ "midcatno", "midcatno" },
				{ "mnmtotamt", "mnmtotamt" },
				{ "netsalamt", "netsalamt" },
				{ "origprice", "origprice" },
				{ "spillamount", "spillamount" },
				{ "payamount1", "payamount1" }, 
				{ "payamount2", "payamount2" },
				{ "payamount3", "payamount3" }, 
				{ "payamount4", "payamount4" },
				{ "payname1", "payname1" },
				{ "payname2", "payname2" },
				{ "payname3", "payname3" },
				{ "payname4", "payname4" },
				{ "payno1", "payno1" },
				{ "payno2", "payno2" },
				{ "payno3", "payno3" },
				{ "payno4", "payno4" },
				{ "paytype1", "paytype1" },
				{ "paytype2", "paytype2" },
				{ "paytype3", "paytype3" },
				{ "paytype4", "paytype4" },
				{ "pluname", "pluname" },
				{ "pluno", "pluno" },
				{ "terminalnumber", "terminalnumber" },
				{ "quantity", "quantity" },
				{ "salesamount", "salesamount"},
				{ "storeaddress", "storeaddress" },//
				{ "storeid", "storeid" },//
				{ "storename", "storename" },//
				{ "systemdatetime", "systemdatetime" },
				{ "tmcode", "tmcode" },
				{ "totmamount", "totmamount" },
				{ "printtrannumber", "printtrannumber" },
				{ "trantype", "trantype" },
				{ "unitprice", "unitprice" },
				};
		return names;
	}
	
	public static void updateAccdate(DbConnection connection, int eodcnt) throws SQLException {
		ZReport z = ZReport.queryBySequenceNumber(connection, new Integer(eodcnt));
		String accdate = CreamCache.getInstance().getDateFormate().format(z.getAccountingDate());
		String sql = "UPDATE " + getInsertUpdateTableNameStaticVersion() + 
				" SET accdate = '" + accdate + "' WHERE eodcnt = " + eodcnt;
		executeQuery(connection, sql);
	}
	
	public static Iterator getHistoryTrans(DbConnection connection, int curZNum) {
		try {
		    ZReport z = ZReport.queryBySequenceNumber(connection, new Integer(curZNum));
		    int beginSeq = z.getBeginTransactionNumber().intValue();
		    int endSeq = z.getEndTransactionNumber().intValue();
		    System.out.println("---- getHistoryTrans | znum : " + curZNum
		        + " | beginseq : " + beginSeq + " | endSeq : " + endSeq);
		    
            return getMultipleObjects(connection, HistoryTrans.class, "SELECT * FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE printtrannumber BETWEEN "
                + beginSeq + " AND " + endSeq);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}
	
	public static Iterator genHistoryTrans(DbConnection connection, int curZNum) throws SQLException,
        EntityNotFoundException {
        ZReport z = ZReport.queryBySequenceNumber(connection, new Integer(curZNum));
        return genHistoryTrans(connection, curZNum, z.getBeginTransactionNumber().intValue(), z
            .getEndTransactionNumber().intValue());
    }

	public static Iterator genHistoryTrans(DbConnection connection, int zNum, int beginSeq, int endSeq)
        throws SQLException, EntityNotFoundException {
        System.out.println("---- genHistoryTrans | znum : " + zNum + " | beginseq : " + beginSeq
            + " | endSeq : " + endSeq);
        // from store
        String storeID = Store.getStoreID();
        String storeAddress = Store.getAddress();
        String storeName = Store.getStoreChineseName();

        executeQuery(connection, "DELETE FROM historytrans WHERE eodcnt = " + zNum);
        Iterator it = getMultipleObjects(connection, Transaction.class,
            "SELECT * FROM tranhead WHERE TMTRANSEQ BETWEEN " + beginSeq + " AND " + endSeq);

        while (it.hasNext()) {
            Transaction tran = (Transaction)it.next();
            Iterator it2 = LineItem.queryByTransactionNumber(connection,
                tran.getTransactionNumber().toString());
            while (it2.hasNext()) {
                LineItem li = (LineItem)it2.next();
                HistoryTrans ht = new HistoryTrans();
                ht.setStoreid(storeID);
                ht.setStoreaddress(storeAddress);
                ht.setStorename(storeName);
                ht.setCashierNO(tran.getCashierNumber());
                ht.setCashierName(tran.getCashierName());
                ht.setChangeamount(tran.getChangeAmount());
                ht.setDaifuamount(tran.getDaiFuAmount());
                ht.setDaishouamount(tran.getDaiShouAmount());
                ht.setDaishouamount2(tran.getDaiShouAmount2());
                ht.setDealtype1(tran.getDealType1());
                ht.setDealtype2(tran.getDealType2());
                ht.setDealtype3(tran.getDealType3());
                ht.setEmpno(tran.getEmployeeNumber());
                ht.setEodcnt(new Integer(zNum));
                ht.setGrosssalesamount(tran.getGrossSalesAmount());
                ht.setMnmtotamt(tran.getMMTotalAmt());
                // ht.setNetsalamt(tran.getNetSalesAmount());
                ht.setSpillamount(tran.getSpillAmount());
                ht.setPayamount1(tran.getPayAmount1());
                ht.setPayamount2(tran.getPayAmount2());
                ht.setPayamount3(tran.getPayAmount3());
                ht.setPayamount4(tran.getPayAmount4());
                ht.setPayname1(tran.getPayName1());
                ht.setPayname2(tran.getPayName2());
                ht.setPayname3(tran.getPayName3());
                ht.setPayname4(tran.getPayName4());
                ht.setPayno1(tran.getPayNumber1());
                ht.setPayno2(tran.getPayNumber2());
                ht.setPayno3(tran.getPayNumber3());
                ht.setPayno4(tran.getPayNumber4());
                ht.setTerminalnumber(tran.getTerminalNumber());
                ht.setSalesamount(tran.getSalesAmount());
                ht.setSystemdatetime(tran.getSystemDateTime());
                ht.setTotmamount(tran.getTotalMMAmount());
                ht.setPrinttrannumber(tran.getTransactionNumber());
                ht.setTrantype(tran.getTransactionType());
                // from tranhead
                // from payment from cache
                ht.setPaytype1(Payment.queryByPaymentID(tran.getPayNumber1()).getPaymentType());
                if (tran.getPayNumber2() != null && !tran.getPayNumber2().trim().equals(""))
                    ht.setPaytype2(Payment.queryByPaymentID(tran.getPayNumber2()).getPaymentType());
                if (tran.getPayNumber3() != null && !tran.getPayNumber3().trim().equals(""))
                    ht.setPaytype3(Payment.queryByPaymentID(tran.getPayNumber3()).getPaymentType());
                if (tran.getPayNumber4() != null && !tran.getPayNumber4().trim().equals(""))
                    ht.setPaytype4(Payment.queryByPaymentID(tran.getPayNumber4()).getPaymentType());
                // from trandtl
                ht.setAmount(li.getAfterDiscountAmount());
                ht.setCodetx(li.getDetailCode());
                ht.setDisctype(li.getDiscountType());
                ht.setItemnumber(li.getItemNumber());
                ht.setItemseq(new Integer(li.getLineItemSequence()));
                ht.setItemvoid(li.getRemoved() ? "1" : "0");
                ht.setMicrocatno(li.getMicroCategoryNumber());
                ht.setMidcatno(li.getMidCategoryNumber());
                ht.setOrigprice(li.getOriginalPrice());
                ht.setDescription(li.getDescription());
                ht.setPluno(li.getPluNumber());
                ht.setQuantity(li.getQuantity());
                ht.setUnitprice(li.getUnitPrice());
                // from plu

                // ??
                //dtlaftdscamt
                //dtltaxamt
                //dtltaxtype
                ht.insert(connection);
            }
        }
        return it;
    }
	
	public static Object[] cloneForSC(Iterator iter) {
		String[][] fieldNameMap = getPosToScFieldNameArray();

		try {
			ArrayList objArray = new ArrayList();
			while (iter.hasNext()) {
				HistoryTrans ht = (HistoryTrans) iter.next();
				HistoryTrans clonedht = new HistoryTrans();
				for (int i = 0; i < fieldNameMap.length; i++) {
					Object value = ht.getFieldValue(fieldNameMap[i][0]);
					if (value == null) {
						try {
							value = ht.getClass().getDeclaredMethod(
									"get" + fieldNameMap[i][0], new Class[0])
									.invoke(ht, new Object[0]);
						} catch (Exception e) {
							value = null;
						}
					}
					clonedht.setFieldValue(fieldNameMap[i][1], value);
				}
				objArray.add(clonedht);
			}
			return objArray.toArray();
		} catch (Exception e) {
			return null;
		}
	}
	
//	public static void main(String[] args) {
//		HistoryTrans.genHistoryTrans(Integer.parseInt(args[0]));
//	}
*/
}