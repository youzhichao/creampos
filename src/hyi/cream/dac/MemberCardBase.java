/*
 * Created on 2005-3-15
 * for quanjia WorkCheck(考勤系统)
 * 
 */
package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 */
public class MemberCardBase extends DacBase implements Serializable {

    private static final long serialVersionUID = 1L;

	private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("storeID");
		primaryKeys.add("fcardnumber");
	}

	private boolean needConfirm;

	public MemberCardBase() {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (hyi.cream.inline.Server.serverExist())
			return "memcardbase";
		return "";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (hyi.cream.inline.Server.serverExist())
			return "memcardbase";
		return "";
	}

	public void setStoreID(String storeID) {
		setFieldValue("storeID", storeID);
	}

	public String getStoreID() {
		return ((String) getFieldValue("storeID"));
	}

	public void setFcardnumber(String fcardnumber) {
		setFieldValue("fcardnumber", fcardnumber);
	}

	public String getFcardnumber() {
		return ((String) getFieldValue("fcardnumber"));
	}

	public void setFtypeno(String ftypeno) {
		setFieldValue("ftypeno", ftypeno);
	}

	public String getFtypeNo() {
		return ((String) getFieldValue("ftypeno"));
	}

//	public void setFtypename(String ftypename) {
//		setFieldValue("ftypename", ftypename);
//	}
//
//	public String getFtypename() {
//		return ((String) getFieldValue("ftypename"));
//	}
//	
//	public void setMemNo(String memNo) {
//		setFieldValue("memNo", memNo);
//	}
//
//	public String getMemNo() {
//		return ((String) getFieldValue("memNo"));
//	}
//	
	public void setFmemberemcid(String fmemberemcid) {
		setFieldValue("fmemberemcid", fmemberemcid);
	}

	public String getFmemberemcid() {
		return ((String) getFieldValue("fmemberemcid"));
	}
	
	public void setFtelno(String ftelno) {
		setFieldValue("ftelno", ftelno);
	}

	public String getFtelno() {
		return ((String) getFieldValue("ftelno"));
	}
	
	public void setFmemname(String fmemname) {
		setFieldValue("fmemname", fmemname);
	}

	public String getFmemname() {
		return ((String) getFieldValue("fmemname"));
	}
		
	public void setFenglishname(String fenglishname) {
		setFieldValue("fenglishname", fenglishname);
	}

	public String getFenglishname() {
		return ((String) getFieldValue("fenglishname"));
	}
	
	public void setFmakestoreno(String fmakestoreno) {
		setFieldValue("fmakestoreno", fmakestoreno);
	}

	public String getFmakestoreno() {
		return ((String) getFieldValue("fmakestoreno"));
	}
	
	public void setUpdateuserid(String updateuserid) {
		setFieldValue("updateuserid", updateuserid);
	}

	public String getUpdateuserid() {
		return ((String) getFieldValue("updateuserid"));
	}
//	
//	public void setAreaNo(String areaNo) {
//		setFieldValue("areaNo", areaNo);
//	}
//
//	public String getAreaNo() {
//		return ((String) getFieldValue("areaNo"));
//	}
//	
//	public void setAreaName(String areaName) {
//		setFieldValue("areaName", areaName);
//	}
//
//	public String getAreaName() {
//		return ((String) getFieldValue("areaName"));
//	}
//	
	public void setFstatus(Integer fstatus) {
		setFieldValue("fstatus", fstatus);
	}

	public Integer getFstatus() {
		return (Integer) getFieldValue("fstatus");
	}

	public Date getFbegdate() {
		return (Date) getFieldValue("fbegdate");
	}

	public void setFbegdate(Date fbegdate) {
		setFieldValue("fbegdate", fbegdate);
	}

	public Date getFenddate() {
		return (Date) getFieldValue("fenddate");
	}

	public void setFenddate(Date fenddate) {
		setFieldValue("fenddate", fenddate);
	}

	public Date getFbirthday() {
		return (Date) getFieldValue("fbirthday");
	}

	public void setFbirthday(Date fbirthday) {
		setFieldValue("fbirthday", fbirthday);
	}

	public Date getUpdatedate() {
		return (Date) getFieldValue("updatedate");
	}

	public void setUpdatedate(Date updatedate) {
		setFieldValue("updatedate", updatedate);
	}

//	public HYIDouble getDisRate() {
//		return (HYIDouble) getFieldValue("disRate");
//	}
//
//	public void setDisRate(HYIDouble disRate) {
//		setFieldValue("disRate", disRate);
//	}
//	
//	public HYIDouble getDisBirRate() {
//		return (HYIDouble) getFieldValue("disBirRate");
//	}
//
//	public void setDisBirRate(HYIDouble disBirRate) {
//		setFieldValue("disBirRate", disBirRate);
//	}
//
//	public HYIDouble getBounsRate() {
//		return (HYIDouble) getFieldValue("bounsRate");
//	}
//
//	public void setBounsRate(HYIDouble bounsRate) {
//		setFieldValue("bounsRate", bounsRate);
//	}
//
	public HYIDouble getBuycreditamt() {
		return (HYIDouble) getFieldValue("buycreditamt");
	}

	public void setBuycreditamt(HYIDouble buycreditamt) {
		setFieldValue("buycreditamt", buycreditamt);
	}

	public HYIDouble getBuycreditperiod() {
		return (HYIDouble) getFieldValue("buycreditperiod");
	}

	public void setBuycreditperiod(HYIDouble buycreditperiod) {
		setFieldValue("buycreditperiod", buycreditperiod);
	}

	public HYIDouble getSellcreditamt() {
		return (HYIDouble) getFieldValue("sellcreditamt");
	}

	public void setSellcreditamt(HYIDouble sellcreditamt) {
		setFieldValue("sellcreditamt", sellcreditamt);
	}

	public HYIDouble getSellcreditperiod() {
		return (HYIDouble) getFieldValue("sellcreditperiod");
	}

	public void setSellcreditperiod(HYIDouble sellcreditperiod) {
		setFieldValue("sellcreditperiod", sellcreditperiod);
	}

	public HYIDouble getEarningamt() {
		return (HYIDouble) getFieldValue("earningamt");
	}

	public void setEarningamt(HYIDouble earningamt) {
		setFieldValue("earningamt", earningamt);
	}

	public HYIDouble getDealAmt() {
		return (HYIDouble) getFieldValue("dealAmt");
	}

	public void setDealAmt(HYIDouble dealAmt) {
		setFieldValue("dealAmt", dealAmt);
	}
	/**
     * Query object method. This method will be invoked by ServerThread when
     * receiving "getObject hyi.cream.dac.Member [memberID]" command.
     * 
     * @param date Can be member ID, member card ID, telephone number, or identity ID.
     * @return Collection Return a Collection contains a Member DAC object.
     */
    public static Collection queryObject(String data) {
    	Server.log("membercardbase queryObject");
        List list = new ArrayList();
        MemberCardBase member;

        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();

            if (data.length() == 18) { // maybe it is a first 12 digits of idCardNumber
                // Try treat it as a idCardNumber...
                try {
                    member = DacBase.getSingleObject(connection, MemberCardBase.class,
                        "SELECT * FROM memcardbase WHERE memberemcid = '" + data
                            + CreamToolkit.getEAN13CheckDigit(data) + "'");
                    list.add(member);
                    return list;
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                } catch (EntityNotFoundException e) {
                }

                try {
                    member = (MemberCardBase)DacBase.getSingleObject(connection, MemberCardBase.class,
                        "SELECT * FROM memcardbase WHERE memberemcid='" + data
                            + CreamToolkit.getEAN13CheckDigit(data) + "'");
                    list.add(member);
                    return list;
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                } catch (EntityNotFoundException e) {
                }
            }

            // check as member ID first
            try {
                member = (MemberCardBase)DacBase.getSingleObject(connection, MemberCardBase.class,
                    "SELECT * FROM memcardbase WHERE fcardnumber = '" + data + "'");
            	Server.log("memcardbase | sql : " + "SELECT * FROM memcardbase WHERE fcardnumber = '" + data + "'");
                list.add(member);
            	Server.log("memcardbase | member : " + member.getFcardnumber());
                return list;
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } catch (EntityNotFoundException e) {
            }

            // then check it as either id card number, member ID, or tel number
            try {
                member = (MemberCardBase)DacBase.getSingleObject(connection, MemberCardBase.class,
                    "SELECT * FROM memcardbase WHERE memberemcid = '" + data + "' OR "
                        + "fcardnumber = '" + data + "' OR " + "telno = '" + data + "'");
                list.add(member);
                return list;
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } catch (EntityNotFoundException e) {
            }

            // Try treat it as a memberID...
            // member = (Member)DacBase.getSingleObject(Member.class,
            // "SELECT * FROM member WHERE memberID='" + data + "'");
            // if (member != null) {
            // list.add(member);
            // return list;
            // }
            //
            //
            // // Try treat it as telNumber...
            // member = (Member)DacBase.getSingleObject(Member.class,
            // "SELECT * FROM member WHERE telNumber='" + data + "'");
            // if (member != null) {
            // list.add(member);
            // return list;
            // }
            //
            // // Try treat it as memberemcid()...
            //        member = (Member)DacBase.getSingleObject(Member.class,
            //            "SELECT * FROM member WHERE memberemcid='" + data + "'");
            //        if (member != null) {
            //            list.add(member);
            //            return list;
            //        }
            return list;

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
	
    /**
     * Query member data by a given ID.
     */
    public static MemberCardBase queryByMemberID(String id) {
        if (!Client.getInstance().isConnected()) {
//            return tryCreateAnUnkownMember(id);
        	return null;
        } else {
            // Send command "queryMember" to inline server.
            try {
                Client client = Client.getInstance();
                System.out.println("------------------------");
                client.processCommand("queryMember2 " + id);
                System.out.println("------------------------");
                MemberCardBase member = (MemberCardBase)client.getReturnObject();
                if (member != null) 
//                {
                    member.setNeedConfirm(false);  // No need user confirmation
                return member;
//                } else {                     
//                    return tryCreateAnUnkownMember(id);
//                }
            } catch (ClientCommandException e) {
                return null;
            }
        }
    }
    
   /**
     * 在离线状态下，若前台只输入前12码会员卡号，则必需由用户确认是否接受此会员卡号，
     * 在这种情况下，needConfirm会被设置为true。
     * 
     * @return boolean 是否需要让用户确认接受此会员卡号
     */
    public boolean isNeedConfirm() {
        return needConfirm;
    }

    /**
     * 在离线状态下，若前台只输入前12码会员卡号，则必需由用户确认是否接受此会员卡号，
     * 在这种情况下，needConfirm会被设置为true。
     * 
     * @param needConfirm 是否需要让用户确认接受此会员卡号
     */
    public void setNeedConfirm(boolean needConfirm) {
        this.needConfirm = needConfirm;
    }

}
