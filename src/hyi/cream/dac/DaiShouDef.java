package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.*;

public class DaiShouDef extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

	transient public static final String VERSION = "1.5";
	private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("ID");
	}

	public DaiShouDef() {

	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (getUseClientSideTableName())
			return "daishoudef";
		else if (hyi.cream.inline.Server.serverExist())
			return "posdl_daishoudef";
		else
			return "daishoudef";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (hyi.cream.inline.Server.serverExist())
			return "posdl_daishoudef";
		else
			return "daishoudef";
	}

	public static DaiShouDef queryByID(DbConnection connection, String id) {
        try {
            String sel = "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                + " WHERE ID = '" + id + "'";
            return getSingleObject(connection, DaiShouDef.class, sel);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

	public boolean equals(Object obj) {
		if (!(obj instanceof DaiShouDef))
			return false;
		DaiShouDef objCp = (DaiShouDef) obj;
		return this.getID().equals(objCp.getID());
	}

	/**
	 * 取出所有代收定义项目
	 * 
	 * @return Collection
	 * @throws SQLException 
	 * @throws EntityNotFoundException 
	 * @throws SQLException 
	 */
	public static Collection<DaiShouDef> getAllDaiShouDefs(DbConnection connection) throws SQLException, EntityNotFoundException{
		String sql = "SELECT * FROM " + getInsertUpdateTableNameStaticVersion();
		return DacBase.getMultipleObjects(connection, DaiShouDef.class, sql, null);
    }

	/**
	 * 从SC 中取出数据为下载到POS系统做准备
	 * 
	 * @return Collection
	 */
	public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            String sql = "SELECT * FROM posdl_daishoudef";
            return DacBase.getMultipleObjects(connection, DaiShouDef.class, sql, null);
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
	}

	/**
	 * 检查 prx 是否是该代收类型的前缀
	 * 
	 * @param prx
	 * @return boolean
	 */
	public boolean containsPrefix(String prx) {
		String prefix = getPrefix();
		String tmpString, startPrx, endPrx;
		// prefix == null && prefix == "" 得到的结果应该一致
		if (prefix == null || prefix.trim().equals(""))
			return true;

		StringTokenizer token1 = new StringTokenizer(prefix, ",");
		while (token1.hasMoreTokens()) {
			tmpString = token1.nextToken();
			if (tmpString.equals(prx))
				return true;
			int i = tmpString.indexOf("-");
			if (i != -1) {
				startPrx = tmpString.substring(0, i);
				endPrx = tmpString.substring(i + 1);
				if (prx.compareTo(startPrx) >= 0 && prx.compareTo(endPrx) <= 0)
					return true;
			}
		}
		return false;
	}

	/**
	 * To check if the barcode is valid.
	 * 
	 * @since 2003-06-23
	 * @param barcode
	 * @return boolean
	 */
	public boolean isBarcodeValid(String barcode) {
		String chechStr;
		int beginIndex, sum = 0;
		boolean isValid = false;

		String chk = getCDMethod();
		if (chk == null)
			return true;

		beginIndex = getCDStartPos().intValue() - 1;
		if (beginIndex < 0)
			return true;

		try {
			switch (chk.charAt(0)) {
			case '1':
				/* 校验位前的奇数位数字×3 ＋ 偶数位数字×1 ＋ 校验位之和的个位数为0 */
				chechStr = barcode.substring(beginIndex, beginIndex + 1);
				for (int i = 0; i < beginIndex; i++) {
					if ((i + 1) % 2 == 0)
						sum += barcode.charAt(i) - 48; // '0'的ASCII码为48
					else
						sum += 3 * (barcode.charAt(i) - 48);
				}
				sum += Integer.parseInt(chechStr);
				if (sum % 10 == 0)
					isValid = true;
				break;

			/* 校验位前的数字和的最后一位等于校验位 */
			case '2':
				chechStr = barcode.substring(beginIndex, beginIndex + 1);
				for (int i = 0; i < beginIndex; i++)
					sum += barcode.charAt(i) - 48;
				if (sum % 10 == Integer.parseInt(chechStr))
					isValid = true;
				break;

			/* 校验位前的数字之和的最后两位等于校验位 */
			case '3':
				chechStr = barcode.substring(beginIndex, beginIndex + 2);
				for (int i = 0; i < beginIndex; i++)
					sum += barcode.charAt(i) - 48;
				if (sum % 100 == Integer.parseInt(chechStr))
					isValid = true;
				break;
			case '4': // 铁通公司通信费
				// 最后两位为校验位,即33,34位
				chechStr = barcode.substring(beginIndex, beginIndex + 2);
				// 前32位的权值，计算方式为w[i] = MOD(10*w[i+1], 97) 第32位w[31] = 3.
				int[] w = { 28, 61, 74, 85, 57, 93, 19, 31, 71, 75, 56, 25, 51,
						73, 17, 89, 38, 62, 45, 53, 15, 50, 5, 49, 34, 81, 76,
						27, 90, 9, 30, 3 };
				// 计算每一位与相应的加权因子的乘积然后求和
				for (int i = 0; i < 32; i++) {
					sum += (barcode.charAt(i) - 48) * w[i];
				}
				// 取模数 用97除上一步计算出来的和
				int mod = sum % 97;
				// 用98减去模数
				int check = 98 - mod;
				// 余数不足两位前面补0即为校验码
				String s = "0" + check;
				// 如果算出的校验码跟barcode中取出来的相同就是正确的barcode
				if (s.endsWith(chechStr))
					isValid = true;
				break;
			case '5': // EAN13条码校验法
				isValid = CreamToolkit.checkEAN13(barcode);
				break;
			default:
			}

		} catch (StringIndexOutOfBoundsException e) {
			e.printStackTrace(CreamToolkit.getLogger());
		} catch (NumberFormatException e) {
			e.printStackTrace(CreamToolkit.getLogger());
			// System.out.println(" Amount = -1" + barcode) ;
		}

		return isValid;

	}

	/** ****** getters and setters ******************** */

	/**
	 * 取出输入条码的金额
	 */
	public HYIDouble getAmount(String barcode) {
		int beginIndex, endIndex, amountDecimal;
		String amountString;

		// 如果不是以数字开头，去掉这一位
		if (barcode.charAt(0) < '0' || barcode.charAt(0) > '9')
			barcode = barcode.substring(1);

		beginIndex = getAmountStartPos().intValue() - 1;
		endIndex = getAmountLen().intValue() + beginIndex;
		amountDecimal = getAmountDecimal().intValue();
		try {
			amountString = barcode.substring(beginIndex, endIndex);
			return new HYIDouble(amountString).movePointLeft(amountDecimal);
		} catch (Exception e) {
			return new HYIDouble(-1);
		}
	}

	/**
	 * 取出输入条码的时间
	 */
	public Date getYearMonth(String barcode) {
		Date date = null;
		try {
			if (this.getYearLen().intValue() < 1 || this.getYearLen().intValue() > 4|| this.getMonthLen().intValue() != 2)
				return null;
			int beginIndex = this.getMonthStartPos().intValue() - 1;
			int endIndex = this.getMonthLen().intValue() + beginIndex;
			int month = Integer.parseInt(barcode.substring(beginIndex, endIndex));
			// java中如果月>12,会在年份上＋1，故要判断是否在12以内
			if (month <= 0 || month > 12)
				return null;
			beginIndex = this.getYearStartPos().intValue() - 1;
			endIndex = this.getYearLen().intValue() + beginIndex;
			int year = 0;
			Calendar cal = Calendar.getInstance();
			int y = cal.get(Calendar.YEAR);
			if (this.getYearLen().intValue() == 1) {
				year = Integer.parseInt(String.valueOf(y).substring(0, 3) + barcode.substring(beginIndex, endIndex)); 
			} else if (this.getYearLen().intValue() == 2) {
				year = Integer.parseInt(String.valueOf(y).substring(0, 2) + barcode.substring(beginIndex, endIndex)); 
			} else if (this.getYearLen().intValue() == 3) {
				year = Integer.parseInt(String.valueOf(y).substring(0, 1) + barcode.substring(beginIndex, endIndex)); 
			} else if (this.getYearLen().intValue() == 4) {
				year = Integer.parseInt(barcode.substring(beginIndex, endIndex)); 
			}
			cal.set(Calendar.YEAR,  year);
			cal.set(Calendar.MONTH, month - 1);
			date = cal.getTime();
		} catch (Exception e) {
		}
		return date;
	}

	public String getID() {
		return (String) getFieldValue("ID");
	}

	public void setID(String id) {
		setFieldValue("ID", id);
	}

	public String getName() {
		return (String) getFieldValue("Name");
	}

	public void setName(String name) {
		setFieldValue("Name", name);
	}

	public String getItemNumber() {
		return (String) getFieldValue("ItemNumber");
	}

	public void setItemNumber(String itemNumber) {
		setFieldValue("ItemNumber", itemNumber);
	}

	public Integer getBarcodeLen() {
		return (Integer) getFieldValue("BarcodeLen");
	}

	public void setBarcodeLen(Integer barcodeLen) {
		setFieldValue("BarcodeLen", barcodeLen);
	}

	public Integer getAmountStartPos() {
		return (Integer) getFieldValue("AmountStartPos");
	}

	public void setAmountStartPos(Integer amountStartPos) {
		setFieldValue("AmountStartPos", amountStartPos);
	}

	public Integer getAmountLen() {
		return (Integer) getFieldValue("AmountLen");
	}

	public void setAmountLen(Integer amountLen) {
		setFieldValue("AmountLen", amountLen);
	}

	public Integer getAmountDecimal() {
		return (Integer) getFieldValue("AmountDecimal");
	}

	public void setAmountDecimal(Integer amountDecimal) {
		setFieldValue("AmountDecimal", amountDecimal);
	}

	public Integer getCDStartPos() {
		return (Integer) getFieldValue("CDStartPos");
	}

	public void setCDStartPos(Integer cdStartPos) {
		setFieldValue("CDStartPos", cdStartPos);
	}

	public String getCDMethod() {
		return (String) getFieldValue("CDMethod");
	}

	public void setCDMethod(Character method) {
		setFieldValue("CDMethod", method);
	}

	public String getPrefix() {
		return (String) getFieldValue("Prefix");
	}

	public void setPrefix(String prefix) {
		setFieldValue("Prefix", prefix);
	}

	public Integer getPrefixLen() {
		return (Integer) getFieldValue("PrefixLen");
	}

	public void setPrefixLen(Integer prefixLen) {
		setFieldValue("PrefixLen", prefixLen);
	}

	public Integer getYearStartPos() {
		return (Integer) getFieldValue("YearStartPos");
	}

	public void setYearStartPos(Integer yearStartPos) {
		setFieldValue("YearStartPos", yearStartPos);
	}

	public Integer getYearLen() {
		return (Integer) getFieldValue("YearLen");
	}

	public void setYearLen(Integer yearLen) {
		setFieldValue("YearLen", yearLen);
	}

	public Integer getMonthStartPos() {
		return (Integer) getFieldValue("MonthStartPos");
	}

	public void setMonthStartPos(Integer monthStartPos) {
		setFieldValue("MonthStartPos", monthStartPos);
	}

	public Integer getMonthLen() {
		return (Integer) getFieldValue("MonthLen");
	}

	public void setMonthLen(Integer monthLen) {
		setFieldValue("MonthLen", monthLen);
	}

	/**
	 * @param daiShouBarcode
	 * @return
	 */
	public static ArrayList<DaiShouDef> matchDaiShouDefs(final String daiShouBarcode) {
		ArrayList<DaiShouDef> daiShouDefs = new ArrayList<DaiShouDef>();
		DaiShouDef dsf;
		Iterator itr = null;
		DbConnection connection = null;
		try {
		    connection = CreamToolkit.getPooledConnection();
		    itr = getAllDaiShouDefs(connection).iterator();
		} catch (Exception e) {
			CreamToolkit.logMessage(e);
		    // 不应因为代收错误而让系统停止!
		    //CreamToolkit.haltSystemOnDatabaseFatalError();
		} finally {
		    CreamToolkit.releaseConnection(connection);
		}
	
	    // 根据barcode.length等区分
	    while (itr != null && itr.hasNext()) {
	        dsf = (DaiShouDef)itr.next();
	        if (dsf != null && dsf.getBarcodeLen().intValue() == daiShouBarcode.length()
	            // && dsf.containsPrefix(daiShouBarcode.substring(0, 2))
	            && dsf.isBarcodeValid(daiShouBarcode)
	            && dsf.getAmount(daiShouBarcode).intValue() > 0) {
	            int prefixLen = 0;
	            if (dsf.getPrefixLen() != null)
	                prefixLen = dsf.getPrefixLen().intValue();
	            if ((prefixLen == 0)
	                || (prefixLen > 0 && dsf.containsPrefix(daiShouBarcode.substring(0,
	                    prefixLen)))) {
	                daiShouDefs.add(dsf);
	                // System.out.println("---------- fit dsf : " + dsf.getName());
	            }
	        }
	    }
	
	    List notFit = new ArrayList();
	    List fit = new ArrayList();
	    if (daiShouDefs.size() > 1) {
	        // 根据年，月是否符合逻辑来判断
	        for (int i = 0; i < daiShouDefs.size(); i++) {
	            dsf = (DaiShouDef)daiShouDefs.get(i);
	            Date defDate = dsf.getYearMonth(daiShouBarcode);
	            System.out.println("---------  defCal : " + defDate);
	            if (defDate == null)
	                notFit.add(dsf);
	            else {
	                Calendar defCal = Calendar.getInstance();
	                defCal.setTime(defDate);
	                defCal.set(Calendar.DAY_OF_MONTH, 1);
	                Calendar maxCal = Calendar.getInstance();
	                maxCal.set(Calendar.DAY_OF_MONTH, 28);
	                Calendar minCal = Calendar.getInstance();
	                minCal.set(Calendar.MONTH, minCal.get(Calendar.MONTH) - 3);
	                // System.out.println("------- maxCal : "
	                // + maxCal.get(Calendar.YEAR)
	                // + maxCal.get(Calendar.MONTH) + " | minCal : "
	                // + minCal.get(Calendar.YEAR)
	                // + minCal.get(Calendar.MONTH) + " | defCal : "
	                // + defCal.get(Calendar.YEAR)
	                // + defCal.get(Calendar.MONTH));
	                if (maxCal.after(defCal) && minCal.before(defCal)) {
	                    // System.out.println("------- fit : "
	                    // + defCal.get(Calendar.YEAR)
	                    // + defCal.get(Calendar.MONTH));
	                    fit.add(dsf);
	                } else {
	                    // System.out.println("------- notfit : "
	                    // + defCal.get(Calendar.YEAR)
	                    // + defCal.get(Calendar.MONTH));
	                    notFit.add(dsf);
	                }
	            }
	        }
	        // if (fit.isEmpty()) {
	        // // 如果没有匹配的，仍从之前的选择
	        // } else {
	        // 如果有匹配的，则从匹配中选择
	        daiShouDefs.clear();
	        daiShouDefs.addAll(fit);
	        // }/
	    }
		return daiShouDefs;
	}
}
