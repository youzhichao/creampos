package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * 移动云支付cmpay_detail
 * @author Administrator
 *
 */
public class Cmpay_detail extends DacBase implements Serializable {

	static final String tableName = "cmpay_detail";
	private static ArrayList primaryKeys = new ArrayList();

	static {
        primaryKeys.add("storeID");
        primaryKeys.add("posNumber");
        primaryKeys.add("transactionNumber");
	}

	public Cmpay_detail() throws InstantiationException {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}


	public String getInsertUpdateTableName() {
		if (Server.serverExist()) {
            return "posul_cmpay_detail";
        }
		return tableName;
	}

	public static String getInsertUpdateTableNameStaic() {
        if (Server.serverExist()) {
            return "posul_cmpay_detail";
        }
        return "cmpay_detail";
    }

	public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
            {"storeID", "storeID"},
            {"posNumber", "posNumber"},
            {"posNumber", "posNumber"},
            {"transactionNumber", "transactionNumber"},
            {"orderId", "orderId"},
            {"mid", "mid"},
            {"zseq", "zseq"},
            {"amt", "amt"},
            {"coupamt", "coupamt"},
            {"vchamt", "vchamt"},
            {"cashamt", "cashamt"},
            {"systemDate", "systemDate"},
            {"phonenumber", "phonenumber"},
            {"mark", "mark"},
		};
	}

	//STORENUMBER,店号
	public void setSTORENUMBER(String storeNumber) {
		 setFieldValue("storeID",storeNumber);
	}
    public String getSTORENUMBER() {
        return (String)getFieldValue("storeID");
    }
	//POSNUMBER, 机号
	public void setPOSNUMBER(Integer posNumber) {
		setFieldValue("posNumber",posNumber);
	}
    public Integer getPOSNUMBER() {
        return (Integer)getFieldValue("posNumber");
    }
	//TRANSACTIONNUMBER, 交易序号
	public Integer getTRANSACTIONNUMBER() {
		return (Integer) getFieldValue("transactionNumber");
	}
	public void setTRANSACTIONNUMBER(Integer employeeId) {
		setFieldValue("transactionNumber",employeeId);
	}
	//zseq, z帐序号
	public Integer getZseq() {
		return (Integer) getFieldValue("zseq");
	}
	public void setZseq(Integer employeeId) {
		setFieldValue("zseq",employeeId);
	}
    //orderId, 移动商户订单号
	public String getOrderId() {
		return (String) getFieldValue("orderId");
	}
	public void setOrderId(String employeeId) {
		setFieldValue("orderId",employeeId);
	}
    //mid, 系统跟踪号
	public String getmid() {
		return (String) getFieldValue("mid");
	}
	public void setmid(String mid) {
		setFieldValue("mid",mid);
	}

	//amt,交易金额
	public HYIDouble getAmt() {
		return (HYIDouble) getFieldValue("amt");
	}
	public void setAmt(HYIDouble temperature) {
		setFieldValue("amt",temperature);
	}

	//coupamt,电子券消费金额
	public HYIDouble getCoupamt() {
		return (HYIDouble) getFieldValue("coupamt");
	}
	public void setCoupamt(HYIDouble updateDateTime) {
		setFieldValue("coupamt",updateDateTime);
	}

	//vchamt,代金券消费金额
	public HYIDouble getVchamt() {
		return (HYIDouble) getFieldValue("vchamt");
	}
	public void setVchamt(HYIDouble updateDateTime) {
		setFieldValue("vchamt",updateDateTime);
	}

	//cashamt,现金消费金额
	public HYIDouble getCashamt() {
		return (HYIDouble) getFieldValue("cashamt");
	}
	public void setCashamt(HYIDouble updateDateTime) {
		setFieldValue("cashamt",updateDateTime);
	}

    //SYSTEMDATE,时间
    public void setSYSTEMDATE(Date date) {
        setFieldValue("systemDate",date);
    }
    public Date getSYSTEMDATE() {
        return (Date)getFieldValue("systemDate");
    }

    //phonenumber, 手机号
    public String getphonenumber() {
        return (String) getFieldValue("phonenumber");
    }
    public void setphonenumber(String phonenumber) {
        setFieldValue("phonenumber",phonenumber);
    }

    //mark, 预留字段
    public String getmark() {
        return (String) getFieldValue("mark");
    }
    public void setmark(String mark) {
        setFieldValue("mark",mark);
    }


    public static Iterator queryByTranNo(DbConnection connection, int tranNo) {
        try {
            return getMultipleObjects(connection, Cmpay_detail.class,"select * from " + getInsertUpdateTableNameStaic() + " where  transactionNumber = " + tranNo);
        } catch (EntityNotFoundException e) {
//            CreamToolkit.logMessage(e);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        }
        return null;
    }

    public static Cmpay_detail queryByStoreIdTranNoPosNo(DbConnection connection,String storeId,String tranNo,String posId) {
        try {
            return getSingleObject(connection, Cmpay_detail.class,"select * from " + getInsertUpdateTableNameStaic()
                    + " where storeID = '" + storeId + "' and transactionNumber = " + tranNo + " and posNumber = " + posId);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } catch (EntityNotFoundException e) {
//            CreamToolkit.logMessage(e);
        }
        return null;
    }

    public Cmpay_detail cloneForSC() {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            Cmpay_detail clonedObj = new Cmpay_detail();
            for (String[] aFieldNameMap : fieldNameMap) {
                Object value = this.getFieldValue(aFieldNameMap[0]);
                if (value == null) {
                    try {
                        value = this.getClass().getDeclaredMethod(
                                "get" + aFieldNameMap[0], new Class[0]).invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedObj.setFieldValue(aFieldNameMap[1], value);
            }
            return clonedObj;
        } catch (InstantiationException e) {
            return null;
        }
    }

    static public void deleteOutdatedData() {
        DbConnection connection = null;
        Statement statement = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            if (Server.serverExist()) {
                // 需要保留的天数
                int tranReserved = PARAM.getTransactionReserved();
                long l = new Date().getTime() - tranReserved * 1000L * 3600 * 24;
                String baseTime = CreamCache.getInstance().getDateTimeFormate().format(new java.sql.Date(l));
                statement.executeUpdate("DELETE FROM posul_cmpay_detail WHERE systemDate < '" + baseTime + "'");
            } else {
                Iterator<String> itr = Transaction.getOutdateTranNumbers();
                while (itr != null && itr.hasNext())
                    statement.executeUpdate("DELETE FROM posul_cmpay_detail WHERE transactionNumber=" + itr.next());
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
    }
}
