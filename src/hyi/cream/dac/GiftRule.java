package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author allan 客显屏的游戏及赠品选择规则定义档
 */
public class GiftRule extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;
					
	transient public static final String VERSION = "1.0";
	private static ArrayList primaryKeys = new ArrayList();
	static final String tableName = "giftrule";

	static {
		primaryKeys.add("ID");
	}

	public GiftRule() {
		super();
	}

	/**
	 * @return
	 */
	public String getID() {
		return (String) getFieldValue("ID");
	}

	/**
	 * giftrule.mmid 来源于 mixandmatch.id
	 * 
	 * @return
	 */
	public String getMMID() {
		return (String) getFieldValue("mmID");
	}

	/**
	 * X维的个数
	 * 
	 * @return
	 */
	public Integer getXDimension() {
		return (Integer) getFieldValue("xDimension");
	}

	/**
	 * Y维的个数
	 * 
	 * @return
	 */
	public Integer getYDimension() {
		return (Integer) getFieldValue("yDimension");
	}

	/**
	 * 无礼品所占的比例
	 * 
	 * @return
	 */
	public Integer getGroup0Scale() {
		return (Integer) getFieldValue("group0Scale");
	}

	/**
	 * 群一礼品所占的比例
	 * 
	 * @return
	 */
	public Integer getGroup1Scale() {
		return (Integer) getFieldValue("group1Scale");
	}

	/**
	 * 群二礼品所占的比例
	 * 
	 * @return
	 */
	public Integer getGroup2Scale() {
		return (Integer) getFieldValue("group2Scale");
	}

	/**
	 * 群三礼品所占的比例
	 * 
	 * @return
	 */
	public Integer getGroup3Scale() {
		return (Integer) getFieldValue("group3Scale");
	}

	/**
	 * 群四礼品所占的比例
	 * 
	 * @return
	 */
	public Integer getGroup4Scale() {
		return (Integer) getFieldValue("group4Scale");
	}

	/**
	 * 群五礼品所占的比例
	 * 
	 * @return
	 */
	public Integer getGroup5Scale() {
		return (Integer) getFieldValue("group5Scale");
	}

	/**
	 * 群六礼品所占的比例
	 * 
	 * @return
	 */
	public Integer getGroup6Scale() {
		return (Integer) getFieldValue("group6Scale");
	}

	/**
	 * 如果没赢得礼品可重试的次数
	 * 
	 * @return
	 */
	public Integer getRetryTimes() {
		return (Integer) getFieldValue("retryTimes");
	}

	/**
	 * 每笔交易最多可获得礼品的次数 0==表示不限制
	 * 
	 * @return
	 */
	public Integer getTranMaxTimes() {
		return (Integer) getFieldValue("tranMaxTimes");
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		return tableName;
	}

	/**
	 * return fieldName map of PosToSc as Map
	 */
	public static Map getScToPosFieldNameMap() {
		Map fieldNameMap = new HashMap();
		fieldNameMap.put("ID", "ID");
		fieldNameMap.put("mmID", "mmID");
		fieldNameMap.put("dimension", "dimension");
		fieldNameMap.put("group0Scale", "group0Scale");
		fieldNameMap.put("group1Scale", "group1Scale");
		fieldNameMap.put("group2Scale", "group2Scale");
		fieldNameMap.put("group3Scale", "group3Scale");
		fieldNameMap.put("group4Scale", "group4Scale");
		fieldNameMap.put("group5Scale", "group5Scale");
		fieldNameMap.put("group6Scale", "group6Scale");
		fieldNameMap.put("retryTimes", "retryTimes");
		fieldNameMap.put("tranMaxTimes", "tranMaxTimes");
		return fieldNameMap;
	}

	public static GiftRule queryByID(DbConnection connection, String ID) {
		try {
            return getSingleObject(connection, GiftRule.class, "SELECT * FROM "
            		+ tableName + " WHERE ID='" + ID + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}

	public static GiftRule queryByMMID(DbConnection connection, String mmID) {
		try {
            return getSingleObject(connection, GiftRule.class, "SELECT * FROM "
            		+ tableName + " WHERE mmID='" + mmID + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}
}
