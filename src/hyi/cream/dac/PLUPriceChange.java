package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 即时变价DAC.
 * 
 * @author Bruce You
 */
public class PLUPriceChange extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    public transient static final String VERSION = "1.5";

    private static List primaryKeys = new ArrayList();

    static {
        primaryKeys.add("ID");
        primaryKeys.add("PLUNO");
    }
    
    /**
     * Constructor for PLUPriceChange.
     * @throws InstantiationException
     */
    public PLUPriceChange() {
        super();
    }

    /**
     * @see hyi.cream.dac.DacBase#getPrimaryKeyList()
     */
    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    /**
     * @see hyi.cream.dac.DacBase#getInsertUpdateTableName()
     */
    public String getInsertUpdateTableName() {
        if (getUseClientSideTableName())
            return "plu";
        else if (hyi.cream.inline.Server.serverExist())
            return "posdl_deltaplu";
        else
            return "plu";
    }

    public String getPriceChangeVersion() {
        return (String)getFieldValue("ID");
    }

    public static String getMaxPriceChangeVersion() {
        if (hyi.cream.inline.Server.serverExist()) {
            DbConnection connection = null;
            Statement statement = null;
            ResultSet resultSet = null;
            String priceChangeID = null;
            try {
                connection = CreamToolkit.getPooledConnection();
                statement = connection.createStatement();
                resultSet = statement.executeQuery("SELECT MAX(ID) from posdl_deltaplu");
                if (resultSet.next())
                    priceChangeID = resultSet.getString(1);
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
            return priceChangeID;
        } else
            return null;
    }

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        Collection dacs;
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            dacs = DacBase.getMultipleObjects(connection, hyi.cream.dac.PLU.class, // return "PLU" instead
                "SELECT * FROM posdl_deltaplu order by ID", getScToPosFieldNameMap());

            String kindType;
            String signType;
            String openPrice;
            // String weightType;
            Iterator iter = dacs.iterator();
            while (iter.hasNext()) {
                PLU plu = (PLU)iter.next();
                // 1. Determine values of PLUSIGN2, PLUSIGN3 from __KINDTYPE, __SIGNTYPE,
                //    and __OPENPRICE, then add them into fieldMap.
                kindType = (String)plu.getFieldValue("__KINDTYPE");
                signType = (String)plu.getFieldValue("__SIGNTYPE");
                openPrice = (String)plu.getFieldValue("__OPENPRICE");
                if (kindType != null && signType != null && openPrice != null) {
                    Integer[] attr = PLU.getPluSpecialValue(kindType, signType, openPrice);
                    plu.setFieldValue("PLUSIGN2", attr[0]);
                    plu.setFieldValue("PLUSIGN3", attr[1]);
    
                   // 2. Remove __KINDTYPE, __SIGNTYPE, and __OPENPRICE.
                    plu.fieldMap.remove("__KINDTYPE");
                    plu.fieldMap.remove("__SIGNTYPE");
                    plu.fieldMap.remove("__OPENPRICE");
                }
            }
            return dacs;

        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
    
	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
		//Meyer/2003-02-19/ Add field "ID" for PostProcessor.afterDonePluPriceChange()
        fieldNameMap.put("ID", "pluPriceChangeID");
        
        fieldNameMap.put("pluNumber", "PLUNO");
        fieldNameMap.put("pluName", "PLUNAME");
        fieldNameMap.put("pluEnglishName", "PRINTNAME");
        fieldNameMap.put("itemNumber", "ITEMNO");
        fieldNameMap.put("shipNumber", "SHIPNO");
        fieldNameMap.put("taxID", "PLUTAX");
        fieldNameMap.put("storeUnitPrice", "PLUPRICE");
        fieldNameMap.put("openPrice", "__OPENPRICE");
        fieldNameMap.put("specialPrice", "PLUPMPRICE");
        fieldNameMap.put("specialBeginDate", "PLUPMDATES");
        fieldNameMap.put("specialEndDate", "PLUPMDATEE");
        fieldNameMap.put("specialBeginTime", "PLUPMTIMES");
        fieldNameMap.put("specialEndTime", "PLUPMTIMEE");
        fieldNameMap.put("memberPrice", "PLUMBPRICE");
        fieldNameMap.put("couponPrice1", "PLUOPPRICE1");
        fieldNameMap.put("couponPrice2", "PLUOPPRICE2");
        fieldNameMap.put("discountBeginDate", "DISCBEGDT");
        fieldNameMap.put("discountEndDate", "DISCENDDT");
        fieldNameMap.put("siGroup", "PLUSIGN1");
        fieldNameMap.put("categoryNumber", "CATNO");
        fieldNameMap.put("midCategoryNumber", "MIDCATNO");
        fieldNameMap.put("microCategoryNumber", "MICROCATNO");
        fieldNameMap.put("kindType", "__KINDTYPE");
        fieldNameMap.put("signType", "__SIGNTYPE");
        fieldNameMap.put("weightType", "__WEIGHTTYPE");
        fieldNameMap.put("mixAndMatchNumber", "MAMNO");
        fieldNameMap.put("depID", "depID");
        fieldNameMap.put("cost", "COST");
        return fieldNameMap;	
	}
}
