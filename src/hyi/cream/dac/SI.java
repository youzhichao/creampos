package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class SI extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 折扣形态: 折扣（百分比） */
    public static final char DISC_TYPE_DISCOUNT_PERCENTAGE = '0';

    /** 折扣形态: 折让 */
    public static final char DISC_TYPE_DISCOUNT_AMOUNT = '1';

    /** 折扣形态: 加成（百分比） */
    public static final char DISC_TYPE_ADDON_AMOUNT = '2';

    /** 折扣形态: 全折 */
    public static final char DISC_TYPE_FREE = '3';

    /** 折扣形态: 開放折扣百分比输入 */
    public static final char DISC_TYPE_INPUT_PERCENTAGE = '4';

    /** 折扣形态: 信用卡红利折抵 */
    public static final char DISC_TYPE_BONUS = '5';

    /** 折扣形态: 開放折讓金額输入 */
    public static final char DISC_TYPE_INPUT_DISCOUNT_AMOUNT = '6';

    private static ArrayList primaryKeys = new ArrayList();

    private HYIDouble discountAmount;

    static {
        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("storeID");
            primaryKeys.add("siID");
        } else
            primaryKeys.add("SIID");
    }

    public SI() {
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SI that = (SI)o;
        return getSIID().equals(that.getSIID());
    }

    public int hashCode() {
        return getSIID().hashCode();
    }

    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public String getInsertUpdateTableName() {
        if (getUseClientSideTableName())
            return "si";
        else if (hyi.cream.inline.Server.serverExist())
            return "posdl_si";
        else
            return "si";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posdl_si";
        else
            return "si";
    }

    /**
     * 找紅利折抵對應的SI.
     */
    public static SI queryBonusDiscount() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            if (hyi.cream.inline.Server.serverExist()) {
                return getSingleObject(connection,
                    SI.class,
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                        + " WHERE sitype='" + DISC_TYPE_BONUS + "'"
                        + " AND storeID = '" + Store.getStoreID() + "'");
            } else {
                return getSingleObject(connection, SI.class,
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                        + " WHERE sitype='" + DISC_TYPE_BONUS + "' ORDER BY siid LIMIT 1");
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static SI queryBySIID(String SIID) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            if (hyi.cream.inline.Server.serverExist()) {
                return getSingleObject(connection,
                    SI.class,
                    "SELECT * FROM "
                        + getInsertUpdateTableNameStaticVersion()
                        + " WHERE SIID='" + SIID + "'"
                        + " AND storeID = '"
                        + Store.getStoreID()
                        + "'");
            } else {
                return (SI) getSingleObject(connection,
                    SI.class,
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE SIID='" + SIID + "'");
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * 判断商品 plu 是否包含在 SI 中。
     * 判断方法：
     *     如果getCATNO()（大类）为空就判断 SIGroup
     *     大类不为空就看是否和商品所属大类相同，不相同给isContains 赋值 false 否则
     *        如果getMIDCATNO()（中类）为空给isContains 赋值 true 否则判断中类是否相符；
     *          如果中类不符就赋值给isContains 赋值 false
     *          如果小类为空就给isContains 赋值 true 否则判断小类是否相符
     *              相符就给isContains 赋值true 否则给isContains 赋值 false;
     * 　　然后，如果isContains值为true,就判断周循环中，当日是否有效，
     *         如果无效则给isContains 赋值 false
     *     返回　isContains
     * @param plu
     * @return boolean
     */
    public boolean contains(PLU plu, LineItem lineItem) {
        boolean isContains = false;

        if ((lineItem.getPluNumber() == null || lineItem.getPluNumber().trim().equals(""))) {
            if (lineItem.getCategoryNumber().equals("9"))
                return false;
        } else if (plu == null || plu.getCategoryNumber().equals("9"))
            return false;

        // 33=不能使用si折扣的商品
        if (plu != null) {
            Iterator it = Reason.queryByreasonCategory("33");
            while (it != null && it.hasNext()) {
                Reason reason = (Reason) it.next();
                if (reason.getreasonName() != null &&
                        reason.getreasonName().equals(plu.getPluNumber()))
                    return false;
            }
        }

        if (getCATNO() != null) {
            if (getCATNO().trim().equals("00"))
                isContains = true; // 全部
            else if (getCATNO().equals(plu.getCategoryNumber())) {
                if (this.getMIDCATNO() != null) {
                    if (getMIDCATNO().equals(plu.getMidCategoryNumber())) {
                        if (getMICROCATNO() != null) {
                            isContains = (getMICROCATNO().equals(plu.getMicroCategoryNumber()));
                        } else {
                            isContains = true; //大类中类均相符但小类为空
                        }
                    } else {
                        isContains = false; //大类相符但中类不符
                    }
                } else {
                    isContains = true; // 大类相符 中类为空
                }
            } else {
                isContains = false; // 大类不为空但中类不符
            }
        } else {
            if (plu != null && plu.getSIGroup() != null) {
                BigInteger pluSI = new BigInteger(plu.getSIGroup().toString()); //
                String siDiscount = getDiscountSI();
                for (int i = 1; i <= siDiscount.length(); i++) {
                    if (siDiscount.charAt(i - 1) == '1' && pluSI.testBit(siDiscount.length() - i)) {
                        Date now = new Date();
                        if (now.after(plu.getDiscountEnd2()) || now.before(plu.getDiscountBeginDate()))
                            isContains = false;
                        else
                            isContains = true;

                        break;
                    }
                }
            } // end of "if (plu.getSIGroup() != null)"
        }

        if (!isContains)
            return isContains;

        //  check day of week
        Calendar c = Calendar.getInstance();
//        c.setTime(new Date());
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        String weekCycle = getCycle();
        if (weekCycle == null) {
            weekCycle = "1111111";
        } else if (weekCycle.length() < 7) {
            weekCycle = weekCycle + "11111111";
        }

        if (weekCycle.charAt(dayOfWeek - 1) == '0') {
            isContains = false;
        }

        return isContains;
    }

    public boolean contains(LineItem lineItem) {
        if (lineItem.getDetailCode().equals("D"))
            return false;
        PLU plu = PLU.queryBarCode(lineItem.getPluNumber());
        return contains(plu, lineItem);
    }

    /*
    CHAR[7]:小计前、后折扣 ④
    ‘A’ – 小计后折扣
    ‘B’ - 小计前折扣
    */
    public boolean isAfterSI() {
        if (this.getAttribute().charAt(7) == 'A')
            return true;
        return false;
    }

    /**
     * @return boolean  (if the SI is valid then return true, else return false)
     */
    public boolean isValid() {
        Date now = new Date();
        java.sql.Date curDate = java.sql.Date.valueOf(CreamCache.getInstance().getDateFormate().format(now));
        java.sql.Time curTime = java.sql.Time.valueOf(CreamCache.getInstance().getTimeFormate().format(now));

        java.sql.Date startDate = getBDate();
        java.sql.Date endDate = getEDate();

        if (startDate != null
            && endDate != null
            && curDate.compareTo(startDate) >= 0
            && curDate.compareTo(endDate) <= 0) {

            java.sql.Time startTime = getBTime();
            java.sql.Time endTime = getETime();
            if (startTime == null
                || endTime == null
                || (curTime.compareTo(startTime) >= 0 && curTime.compareTo(endTime) <= 0)) {
                return true;
            }
        }
        return false;
    }

    //public HYIDouble computeDiscntValue(HYIDouble base, HYIDouble itemCount) {
    public HYIDouble computeDiscntValue(HYIDouble base) {
        HYIDouble discountAmount = null;

        ////////////////////////////
        // CHAR[0]: 四舍五入flag
        //    ‘R’ - 四舍五入
        //    ‘C’ - 进位
        //    ‘D’ - 舍去
        // CHAR[1]:小数位数
        // CHAR[2]:SI相容
        // CHAR[3]:浮动%
        // CHAR[4]:认证
        // CHAR[5]:M&M相容
        // CHAR[6]:
        // CHAR[7]
        ////////////////////////////

        int round;
        String attribute = getAttribute();
        switch (attribute.charAt(0)) {
            case 'R': round = BigDecimal.ROUND_HALF_UP; break;
            case 'C': round = BigDecimal.ROUND_UP; break;
            case 'D': round = BigDecimal.ROUND_DOWN; break;
            default: round = BigDecimal.ROUND_HALF_UP;
        }

        switch (getType()) {
        case DISC_TYPE_DISCOUNT_PERCENTAGE: // 折扣（百分比）
        case DISC_TYPE_INPUT_PERCENTAGE:   // 开放折扣
            discountAmount = base.multiply(getValue()).negate();
            break;

        case DISC_TYPE_DISCOUNT_AMOUNT:         // 折讓
        case DISC_TYPE_INPUT_DISCOUNT_AMOUNT:   // 手輸折讓金額
            //discountAmount = getValue().negate().multiply(itemCount);
            if (getValue().compareTo(base) > 0)
                discountAmount = base.negate();
            else
                discountAmount = getValue().negate();
            break;

        case DISC_TYPE_ADDON_AMOUNT:        // 加成（百分比）
            discountAmount = base.multiply(getValue());
            break;

        case DISC_TYPE_FREE:                //全折
            discountAmount = base.negate();
        }

        return discountAmount != null ? discountAmount.setScale(attribute.charAt(1) - '0', round) :
            new HYIDouble(0);

    }

    /**
     * 从SC 中取出数据为下载到POS系统做准备
     *
     * @return Collection
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            String sql = "SELECT * FROM posdl_si";
            return DacBase.getMultipleObjects(connection, SI.class, sql, getScToPosFieldNameMap());
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("siID", "SIID");
        fieldNameMap.put("siScreenName", "SICNAMES");
        fieldNameMap.put("siPrintName", "SINAMEP");
        fieldNameMap.put("beginDate", "BDATE");
        fieldNameMap.put("endDate", "EDATE");
        fieldNameMap.put("beginTime", "BTIME");
        fieldNameMap.put("endTime", "ETIME");
        fieldNameMap.put("weekCycle", "CYCLE");
        fieldNameMap.put("siGroupNumber", "SINO");
        fieldNameMap.put("catNo", "CATNO");
        fieldNameMap.put("midCatNo", "MIDCATNO");
        fieldNameMap.put("microCatNo", "MICROCATNO");
        fieldNameMap.put("siType", "SITYPE");
        fieldNameMap.put("discountValue", "DVALUE");
        fieldNameMap.put("discountBase", "DBASE");
        fieldNameMap.put("discountAttribute", "ATTR");

        return fieldNameMap;
    }

    //Get properties value:
    public String getSIID() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("siID");
        else
            return (String) getFieldValue("SIID");
    }

    public String getScreenName() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("siScreenName");
        else
            return (String) getFieldValue("SICNAMES");
    }

    public String getPrintName() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("siPrintName");
        else
            return (String) getFieldValue("SINAMEP");
    }

    public java.sql.Date getBDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.sql.Date) getFieldValue("beginDate");
        else
            return (java.sql.Date) getFieldValue("BDATE");
    }

    public java.sql.Date getEDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.sql.Date) getFieldValue("endDate");
        else
            return (java.sql.Date) getFieldValue("EDATE");
    }

    public java.sql.Time getBTime() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.sql.Time) getFieldValue("beginTime");
        else
            return (java.sql.Time) getFieldValue("BTIME");
    }

    public java.sql.Time getETime() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.sql.Time) getFieldValue("endTime");
        else
            return (java.sql.Time) getFieldValue("ETIME");
    }

    public String getCycle() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("cycle");
        else
            return (String) getFieldValue("CYCLE");
    }

    public String getDiscountSI() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("siGroupNumber");
        else
            return (String) getFieldValue("SINO");
    }

    public String getCATNO() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("catNo");
        else
            return (String) getFieldValue("CATNO");
    }

    public String getMIDCATNO() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("midCatNo");
        else
            return (String) getFieldValue("MIDCATNO");
    }

    public String getMICROCATNO() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("microCatNo");
        else
            return (String) getFieldValue("MICROCATNO");
    }

    /**
     * @return String
     * 折扣形态
     *       '0'：折扣（百分比）
     *       '1'：折让
     *       '2'：加成（百分比）
     *       '3'：全折
     *       '4'：开放折扣数输入
     */
    public char getType() {
        String discType;
        if (hyi.cream.inline.Server.serverExist())
            discType = (String) getFieldValue("siType");
        else
            discType = (String) getFieldValue("SITYPE");

        return (StringUtils.isEmpty(discType)) ?
            '?' :
            discType.charAt(0);
    }

    public HYIDouble getValue() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountValue");
        else
            return (HYIDouble) getFieldValue("DVALUE");
    }

    /*
     * 开放折扣需要输入折扣比率
     */
    public void setValue(HYIDouble value) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountValue", value);
        else
            setFieldValue("DVALUE", value);
    }

    /**
     * @return String
     * 折扣基准
     *           ‘R’：依折扣前单价计算
     *           ‘N’：依折扣后单价计算
     */
    public String getBase() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("discountBase");
        else
            return (String) getFieldValue("DBASE");
    }

    /**
     * @return String
     *           CHAR[0]: 四舍五入flag(‘R’:四舍五入 ‘C’:进位   ‘D’:舍去)
     *           CHAR[1]:小数位数
     *           CHAR[2]:SI相容（‘0’不相容）
     *           CHAR[3]:No use
     *           CHAR[4]:认证
     *           CHAR[5]:M&M相容（‘0’不相容）
     *           CHAR[6]:No use
     *           CHAR[7]:No use
     */
    public String getAttribute() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("discountAttribute");
        else
            return (String) getFieldValue("ATTR");
    }

    /**
     * 此折扣是否容不下和別人一起折扣（是否只能單獨折扣）. 
     */
    public boolean isOnlyAllowStandalone() {
        try {
            return getAttribute().charAt(2) == '0'; // SI相容（‘0’不相容）
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 用来设置固定或已知金额的折扣。现在用在信用卡红利折抵金额。
     */
    public HYIDouble getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(HYIDouble discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * 取得計算的小數位數.
     */
    public int getScale() {
        return getAttribute().charAt(1) - '0';
    }
}

