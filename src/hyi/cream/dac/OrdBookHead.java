/*
 * Created on 2005-3-15
 * for quanjia WorkCheck(考勤系统)
 * 
 */
package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DBToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public class OrdBookHead extends DacBase implements Serializable {

    private static final long serialVersionUID = 1L;

	private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("storeID");
		primaryKeys.add("billNo");
		primaryKeys.add("itemNo");
		primaryKeys.add("itemSeq");
	}

	public OrdBookHead() {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (hyi.cream.inline.Server.serverExist())
			return "ordbookhead";
		return "";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (hyi.cream.inline.Server.serverExist())
			return "ordbookhead";
		return "";
	}

	public void setStoreID(String storeID) {
		setFieldValue("storeID", storeID);
	}

	public String getStoreID() {
		return ((String) getFieldValue("storeID"));
	}

	public void setBillNo(String billNo) {
		setFieldValue("billNo", billNo);
	}

	public String getBillNo() {
		return ((String) getFieldValue("billNo"));
	}

	public void setCardNo(String cardNo) {
		setFieldValue("cardNo", cardNo);
	}

	public String getCardNo() {
		return ((String) getFieldValue("cardNo"));
	}
	
	public void setItemNo(String itemNo) {
		setFieldValue("itemNo", itemNo);
	}

	public String getItemNo() {
		return ((String) getFieldValue("itemNo"));
	}
	
	public void setMonthStart(String monthStart) {
		setFieldValue("monthStart", monthStart);
	}

	public String getMonthStart() {
		return ((String) getFieldValue("monthStart"));
	}
	
	public void setMonthEnd(String monthEnd) {
		setFieldValue("monthEnd", monthEnd);
	}

	public String getMonthEnd() {
		return ((String) getFieldValue("monthEnd"));
	}
	
	public void setDtlCodeStart(String dtlCodeStart) {
		setFieldValue("dtlCodeStart", dtlCodeStart);
	}

	public String getDtlCodeStart() {
		return ((String) getFieldValue("dtlCodeStart"));
	}
		
	public void setDtlCodeEnd(String dtlCodeEnd) {
		setFieldValue("dtlCodeEnd", dtlCodeEnd);
	}

	public String getDtlCodeEnd() {
		return ((String) getFieldValue("dtlCodeEnd"));
	}
	
	public void setDtlCodeCurr(String dtlCodeCurr) {
		setFieldValue("dtlCodeCurr", dtlCodeCurr);
	}

	public String getDtlCodeCurr() {
		return ((String) getFieldValue("dtlCodeCurr"));
	}
	
	public void setUpdateUserId(String updateUserId) {
		setFieldValue("updateUserId", updateUserId);
	}

	public String getUpdateUserId() {
		return ((String) getFieldValue("updateUserId"));
	}
	
	public void setFfu(String ffu) {
		setFieldValue("ffu", ffu);
	}

	public String getFfu() {
		return ((String) getFieldValue("ffu"));
	}
	
	public void setItemSeq(Integer itemSeq) {
		setFieldValue("itemSeq", itemSeq);
	}

	public Integer getItemSeq() {
		return (Integer) getFieldValue("itemSeq");
	}

	public void setSrc(Integer src) {
		setFieldValue("src", src);
	}

	public Integer getSrc() {
		return (Integer) getFieldValue("src");
	}

	public void setCustOrdType(Integer custOrdType) {
		setFieldValue("custOrdType", custOrdType);
	}

	public Integer getCustOrdType() {
		return (Integer) getFieldValue("custOrdType");
	}
	
	public void setTotCount(Integer totCount) {
		setFieldValue("totCount", totCount);
	}

	public Integer getTotCount() {
		return (Integer) getFieldValue("totCount");
	}
	
	public void setOrdQty(Integer ordQty) {
		setFieldValue("ordQty", ordQty);
	}

	public Integer getOrdQty() {
		return (Integer) getFieldValue("ordQty");
	}
	
	public void setTotOrdQty(Integer totOrdQty) {
		setFieldValue("totOrdQty", totOrdQty);
	}

	public Integer getTotOrdQty() {
		return (Integer) getFieldValue("totOrdQty");
	}

	public void setPosNo(Integer posNo) {
		setFieldValue("posNo", posNo);
	}

	public Integer getPosNo() {
		return (Integer) getFieldValue("posNo");
	}
	
	public void setTranNo(Integer tranNo) {
		setFieldValue("tranNo", tranNo);
	}

	public Integer getTranNo() {
		return (Integer) getFieldValue("tranNo");
	}
	
	public void setVoidPosNo(Integer voidPosNo) {
		setFieldValue("voidPosNo", voidPosNo);
	}

	public Integer getVoidPosNo() {
		return (Integer) getFieldValue("voidPosNo");
	}
	
	public void setVoidTranNo(Integer voidTranNo) {
		setFieldValue("voidTranNo", voidTranNo);
	}

	public Integer getVoidTranNo() {
		return (Integer) getFieldValue("voidTranNo");
	}

	public void setBalCount(Integer balCount) {
		setFieldValue("balCount", balCount);
	}

	public Integer getBalCount() {
		return (Integer) getFieldValue("balCount");
	}
	
	public void setBillState(Integer billState) {
		setFieldValue("billState", billState);
	}

	public Integer getBillState() {
		return (Integer) getFieldValue("billState");
	}
	
	public Date getCustOrdDate() {
		return (Date) getFieldValue("custOrdDate");
	}

	public void setCustOrdDate(Date custOrdDate) {
		setFieldValue("custOrdDate", custOrdDate);
	}

	public Date getAccountDate() {
		return (Date) getFieldValue("accountDate");
	}

	public void setAccountDate(Date accountDate) {
		setFieldValue("accountDate", accountDate);
	}

	public Date getUpdateDate() {
		return (Date) getFieldValue("updateDate");
	}

	public void setUpdateDate(Date updateDate) {
		setFieldValue("updateDate", updateDate);
	}

	public HYIDouble getCustOrdPrice() {
		return (HYIDouble) getFieldValue("custOrdPrice");
	}

	public void setCustOrdPrice(HYIDouble custOrdPrice) {
		setFieldValue("custOrdPrice", custOrdPrice);
	}

	public HYIDouble getAmt() {
		return (HYIDouble) getFieldValue("amt");
	}

	public void setAmt(HYIDouble amt) {
		setFieldValue("amt", amt);
	}

	public HYIDouble getRtnOrdAmt() {
		return (HYIDouble) getFieldValue("rtnOrdAmt");
	}

	public void setRtnOrdAmt(HYIDouble rtnOrdAmt) {
		setFieldValue("rtnOrdAmt", rtnOrdAmt);
	}
}
