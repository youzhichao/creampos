package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Store DAC.
 * 
 * @author Bruce
 */
public class Store extends DacBase {
    private static final long serialVersionUID = 1L;

    transient public static final String VERSION = "1.0";
    transient static private Store store;
    private static ArrayList primaryKeys = new ArrayList();
    static {
        primaryKeys.add("storeID");
        createCache();
    }

    /**
     * Constructor
     */
    public Store() {
    }

    /**
     * @see hyi.cream.dac.DacBase#getPrimaryKeyList()
     */
    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public static void createCache() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            store = getSingleObject(connection,
                    Store.class,
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion());
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } catch (EntityNotFoundException e) {
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public String getInsertUpdateTableName() {
        return "store";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        return "store";
    }

    public static String getStoreID() {
        if (store != null)
            return (String)store.getFieldValue("storeID");
        else
            return "";
    }

    public static String getStoreChineseName() {
        if (store != null)
            return (String)store.getFieldValue("storeChineseName");
        else
            return "";
    }

   public static String getStoreEnglishName() {
        if (store != null)
            return (String)store.getFieldValue("storeEnglishName");
        else
            return "";
    }

   public static String getAreaNumber() {
        if (store != null)
            return (String)store.getFieldValue("areaNumber");
        else
            return "";
    }
   public static String getTelephoneNumber() {
        if (store != null)
            return (String)store.getFieldValue("telephoneNumber");
        else
            return "";
    }

   public static String getFaxNumber() {
        if (store != null)
            return (String)store.getFieldValue("faxNumber");
        else
            return "";
    }
   public static String getZip() {
        if (store != null)
            return (String)store.getFieldValue("zip");
        else
            return "";
    }

   public static String getAddress() {
        if (store != null)
            return (String)store.getFieldValue("address");
        else
            return "";
    }

    //alipay_partner_id
    public static String getAlipay_partner_id () {
        if (store != null)
            return (String)store.getFieldValue("alipay_partner_id");
        else
            return "";
    }

    // alipay_security_code
    public static String getAlipay_security_code() {
        if (store != null)
            return (String)store.getFieldValue("alipay_security_code") ;
        else
            return "";
    }

    // weixinAppId微信分配的公众账号ID
    public static String getWeixinAppId() {
        if (store != null)
            return (String)store.getFieldValue("weixinAppId") ;
        else
            return "";
    }

    // weixinMchId微信支付分配的商户号
    public static String getWeixinMchId() {
        if (store != null)
            return (String)store.getFieldValue("weixinMchId") ;
        else
            return "";
    }

    // weixinPartnerKeyvarchar签名密钥
    public static String getweixinPartnerKey() {
        if (store != null)
            return (String)store.getFieldValue("weixinPartnerKey") ;
        else
            return "";
    }

    // weiXinKeyStore证书名
    public static String getWeiXinKeyStore() {
        if (store != null)
            return (String)store.getFieldValue("weiXinKeyStore") ;
        else
            return "";
    }

    // weiXinKeyPassword证书密码
    public static String getWeiXinKeyPassword() {
        if (store != null)
            return (String)store.getFieldValue("weiXinKeyPassword") ;
        else
            return "";
    }

    // sdkSingcertPath 签名证书
    public static String getSdkSingcertPath() {
        if (store != null)
            return (String)store.getFieldValue("sdkSingcertPath") ;
        else
            return "";
    }

    // sdkSingcertPwd 证书密码
    public static String getSdkSingcertPwd() {
        if (store != null)
            return (String)store.getFieldValue("sdkSingcertPwd") ;
        else
            return "";
    }

    // sdkEencyptcertPath 密码加密证书
    public static String getSdkEencyptcertPath() {
        if (store != null)
            return (String)store.getFieldValue("sdkEencyptcertPath") ;
        else
            return "";
    }

    // sdkRootcertPath 根证书
    public static String getSdkRootcertPath() {
        if (store != null)
            return (String)store.getFieldValue("sdkRootcertPath") ;
        else
            return "";
    }

    // sdkMiddlecertPath 根证书
    public static String getSdkMiddlecertPath() {
        if (store != null)
            return (String)store.getFieldValue("sdkMiddlecertPath") ;
        else
            return "";
    }

    // sdkMerId 商户号码
    public static String getSdkMerId() {
        if (store != null)
            return (String)store.getFieldValue("sdkMerId") ;
        else
            return "";
    }

    // selfbuyShopid 口碑门店号
    public static String getSelfbuyShopid() {
        if (store != null)
            return (String)store.getFieldValue("selfbuyShopid") ;
        else
            return "";
    }
	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("storeID", "storeID");
        fieldNameMap.put("storeChineseName", "storeChineseName");
        fieldNameMap.put("storeEnglishName", "storeEnglishName");
        fieldNameMap.put("areaNumber", "areaNumber");
        fieldNameMap.put("telephoneNumber", "telephoneNumber");
        fieldNameMap.put("faxNumber", "faxNumber");
        fieldNameMap.put("zip", "zip");
        fieldNameMap.put("address", "address");
        fieldNameMap.put("alipay_partner_id", "alipay_partner_id");
        fieldNameMap.put("alipay_security_code", "alipay_security_code");
        fieldNameMap.put("weixinAppId","weixinAppId");
        fieldNameMap.put("weixinMchId","weixinMchId");
        fieldNameMap.put("weixinPartnerKey","weixinPartnerKey");
        fieldNameMap.put("weiXinKeyStore","weiXinKeyStore");
        fieldNameMap.put("weiXinKeyPassword","weiXinKeyPassword");
        fieldNameMap.put("sdkSingcertPath","sdkSingcertPath");
        fieldNameMap.put("sdkSingcertPwd","sdkSingcertPwd");
        fieldNameMap.put("sdkEencyptcertPath","sdkEencyptcertPath");
        fieldNameMap.put("sdkRootcertPath","sdkRootcertPath");
        fieldNameMap.put("sdkMiddlecertPath","sdkMiddlecertPath");
        fieldNameMap.put("sdkMerId","sdkMerId");
        fieldNameMap.put("selfbuyShopid","selfbuyShopid");
        return fieldNameMap;
	}
	
    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.Store.class,
                "SELECT * FROM store", getScToPosFieldNameMap());
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}
