package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamPropertyUtil;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Shift report class.
 *
 * @author Dai, Slackware, Bruce
 * @version 1.5
 */
public class ShiftReport extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public ShiftReport cloneForSC()
     */
    transient public static final String VERSION = "1.5";

    static ShiftReport currentShift = null;
    private static ArrayList primaryKeys = new ArrayList();

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    static {
        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("StoreID");
            primaryKeys.add("PosNumber");
            primaryKeys.add("ZSequenceNumber");
            primaryKeys.add("ShiftSequenceNumber");
        } else {
            primaryKeys.add("STORENO");
            primaryKeys.add("POSNO");
            primaryKeys.add("EODCNT");
            primaryKeys.add("SHIFTCNT");
        }
    }

    public ShiftReport() {
        setAccountingDate(CreamToolkit.getInitialDate());//new Date()
        //setSequenceNumber(Integer.decode(GetProperty.getProperty("ShiftNumber")));
        setStoreNumber(Store.getStoreID());
        setTerminalNumber(PARAM.getTerminalNumber());
        setCashierNumber(PARAM.getCashierNumber());
        setMachineNumber(PARAM.getTerminalPhysicalNumber());
        setUploadState("0");
    }

    public ShiftReport(int i) throws InstantiationException {
        // Another constructor for doing nothing
    }

    public static Iterator queryByDateTime(DbConnection connection, java.util.Date date) {
        //System.out.println("SELECT * FROM " + tableName + " WHERE  ACCDATE='" + df.format(date).toString() + "'");
        try {
            SimpleDateFormat df = CreamCache.getInstance().getDateFormate();
            return getMultipleObjects(connection, ShiftReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE ACCDATE='" + df.format(date).toString() + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static Iterator queryByZNumber(DbConnection connection, Integer zNumber) {
        try {
            return getMultipleObjects(connection, ShiftReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE EODCNT='" + zNumber + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    //Bruce/2002-01-31
    public static ShiftReport queryByZNumberAndShiftNumber(DbConnection connection, Integer zNumber,
    		Integer shiftNumber) {
        try {
            return getSingleObject(connection, ShiftReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
                " WHERE EODCNT='" + zNumber + "' AND SHIFTCNT='" + shiftNumber + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static ShiftReport queryByKey(DbConnection connection, Integer posNo, Integer zNumber,
    		Integer shiftNumber) {
        try {
            if (hyi.cream.inline.Server.serverExist()) {
            	return getSingleObject(connection, ShiftReport.class,
		                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
		                " WHERE ZSequenceNumber=" + zNumber + " AND ShiftSequenceNumber=" + shiftNumber + " AND PosNumber = " + posNo);
            } else {
            	return getSingleObject(connection, ShiftReport.class,
		                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
		                " WHERE EODCNT=" + zNumber + " AND SHIFTCNT=" + shiftNumber + " AND POSNO = " + posNo);
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }
    /**
     * Query last 20 shift reports.
     * 
     * @return Iterator Last 20 shift reports.
     */
    public static Iterator queryRecentReports() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return getMultipleObjects(connection, ShiftReport.class,
                "SELECT SGNONDTTM, SGNOFDTTM, SHIFTCNT, EODCNT FROM " 
                + getInsertUpdateTableNameStaticVersion() + " ORDER BY SGNONDTTM DESC LIMIT 20");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static Iterator getUploadFailedList(DbConnection connection) {
        try {
            String failFlag = "2";
            return getMultipleObjects(connection, ShiftReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE TCPFLG='" + failFlag + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static Iterator getNotUploadedList(DbConnection connection) {
        try {
            String notUploadFlag = "0";
            return getMultipleObjects(connection, ShiftReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE TCPFLG='" + notUploadFlag + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static Iterator queryNotUploadReport(DbConnection connection) {
        try {
            return getMultipleObjects(connection, ShiftReport.class,
                "SELECT EODCNT,SHIFTCNT FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE TCPFLG<>'1' AND ACCDATE<>'1970-01-01'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static ShiftReport getCurrentShift(DbConnection connection) {
        if (currentShift == null) {
            //Iterator iter = ShiftReport.queryByDateTime(connection, CreamToolkit.getInitialDate());
            //if (iter != null) {
            //    if (iter.hasNext())
            //        currentShift = (ShiftReport)iter.next();
            //}

            ZReport currentZ = ZReport.getCurrentZReport(connection);
            if (currentZ == null) // 如果没有current Z，就不可能有current Shift
                return null;

            try {
                // 看同一个Z序號中最大的那筆是否為197X年，如果是則返回他作為當前Shift
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                currentShift = DacBase.getSingleObject(connection, hyi.cream.dac.ShiftReport.class,
                        "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                        + " WHERE eodcnt=" + currentZ.getSequenceNumber() + " ORDER BY shiftcnt DESC LIMIT 1");
                if (currentShift != null && df.format(currentShift.getAccountingDate()).startsWith("197"))
                    return currentShift;
                else
                    currentShift = null;
            } catch (Exception e) {
                currentShift = null;
            }
        }
        return currentShift;
    }

    public static String getMaxgnofdttm(DbConnection connection) {
        String maxSgnofdttm = "";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String select = "select max(sgnondttm) from " + getInsertUpdateTableNameStaticVersion();
            maxSgnofdttm = df.format(getValueOfStatement(connection, select));
            return maxSgnofdttm != "" ? maxSgnofdttm : "1970-01-01";
        } catch (Exception e) {
            maxSgnofdttm = df.format(new Date());
        }
        return maxSgnofdttm != "" ? maxSgnofdttm : "1970-01-01";
    }

    public static int getNextShiftNumber(DbConnection connection) {
        int z = 0;
        if (ZReport.queryByDateTime(connection, CreamToolkit.getInitialDate()) == null)
            return 0;//z = currentZ.getCurrentZNumber();
        else
            z = ZReport.getCurrentZNumber(connection);
        String select = "SELECT MAX(SHIFTCNT) AS SHIFTNUMBER FROM " + getInsertUpdateTableNameStaticVersion() 
            + " WHERE EODCNT = " + z;
        Object shiftCount = getValueOfStatement(connection, select);
        return (shiftCount == null) ? 0 : CreamToolkit.retrieveIntValue(shiftCount) + 1;
    }

    synchronized public static ShiftReport createShiftReport(DbConnection connection)
        throws SQLException {

        //Bruce/20030117> Advoid two 1970-01-01 shift records
        Iterator iter = ShiftReport.queryByDateTime(connection, CreamToolkit.getInitialDate());
        if (iter != null && iter.hasNext()) {
            CreamToolkit.logMessage("ShiftReport.createShiftReport(): A current shift report exists! Why?");
            currentShift = (ShiftReport)iter.next();
            return currentShift;
        }

        currentShift = new ShiftReport();
        int z = 0 ;
        if (ZReport.getCurrentZNumber(connection) > 0)
            z = ZReport.getCurrentZNumber(connection);
        if (ZReport.getCurrentZNumber(connection) >= 0 && ZReport.queryByDateTime(connection,
            CreamToolkit.getInitialDate()) == null)
            z++;
        currentShift.setSequenceNumber(new Integer(getNextShiftNumber(connection)));
        currentShift.setZSequenceNumber(new Integer(z));
        currentShift.setSignOnSystemDateTime(new Date());
        currentShift.setSignOffSystemDateTime(CreamToolkit.getInitialDate());
        currentShift.setBeginTransactionNumber(connection, new Integer(Transaction.getNextTransactionNumber()));
        currentShift.setEndTransactionNumber(connection, new Integer(Transaction.getNextTransactionNumber()));
        String nextInvoiceNumber = PARAM.getNextInvoiceNumber();
        currentShift.setBeginInvoiceNumber(connection, nextInvoiceNumber);
        currentShift.setEndInvoiceNumber(connection, nextInvoiceNumber);
        currentShift.setMachineNumber(PARAM.getTerminalPhysicalNumber());

        //Bruce/20030417
        //借零金额初始值 for 灿坤
        HYIDouble initialCashInAmount = PARAM.getInitialCashInAmount();
        if (!initialCashInAmount.equals(new HYIDouble(0))) {
            currentShift.setCashInAmount(initialCashInAmount);
            currentShift.setCashInCount(1);
        }

        try {
            if (currentShift.exists(connection))
                currentShift.load(connection);
            else
                currentShift.insert(connection);
        } catch (EntityNotFoundException e) {
            throw new SQLException(e.toString());
        }

//        CreamToolkit.logMessage("Create shift failed! (z=" +
//            currentShift.getZSequenceNumber() + ", shift=" +
//            currentShift.getSequenceNumber() + ")");
        return currentShift;
    }

    // Get&Set properties value:
    public HYIDouble getGrossSales() {
         HYIDouble total = new HYIDouble(0);
         for (int i = 0; i < 11; i++) {
             HYIDouble sub = (HYIDouble)getFieldValue("GROSALTX" + i +"AM");
             if (sub != null)
                 total = total.addMe(sub);
         }
         return total;
         //return (HYIDouble)getFieldValue("GROSSSALES");
    }

   /* public void updateTaxMM() {
        Hashtable taxMMAmount = tran.getTaxMMAmountHashtable();
        Hashtable taxMMCount = tran.getTaxMMCountHashtable();
        String key = "";

        //  set to M&M amount
        Iterator iter = (taxMMAmount.keySet()).iterator();
        String fieldName = "";
        HYIDouble fieldValue = null;
        while (iter.hasNext()) {
            key = (String)iter.next();
            fieldName = "MNMAMT" + key;
            fieldValue = (HYIDouble)taxMMAmount.get(key);
            setFieldValue(fieldName, fieldValue);
        }

        //  set to M&M count
        iter = (taxMMAmount.keySet()).iterator();
        String fieldName2 = "";
        Integer fieldValue2 = null;
        while (iter.hasNext()) {
            key = (String)iter.next();
            fieldName2 = "MNMCNT" + key;
            fieldValue2 = (Integer)taxMMCount.get(key);
            setFieldValue(fieldName2, fieldValue2);
        }
    }          */

    public HYIDouble getMixAndMatchTotalAmount() {
         HYIDouble total = new HYIDouble(0);
         for (int i = 0; i < 10; i++) {
             HYIDouble sub = (HYIDouble)getFieldValue("MNMAMT" + i);
             if (sub != null) {
                total = total.addMe(sub);
             }
         }
         return total;
    }

    public int getMixAndMatchTotalCount() {
         int total = 0;
         for (int i = 0; i < 10; i++) {
             Integer sub = (Integer)getFieldValue("MNMCNT" + i);
             if (sub != null) {
                total = total + sub.intValue();
             }
         }
         return total;
    }

    public HYIDouble getNetSalesTotalAmount() {
         HYIDouble total = new HYIDouble(0);
         for (int i = 0; i < 10; i++) {
             HYIDouble sub = (HYIDouble)getFieldValue("NETSALAMT" + i);
             if (sub != null) {
                total = total.addMe(sub);
             }
         }
         return total;
    }

    public HYIDouble getTotalAmount() {
        return new HYIDouble(0)
            .addMe(getNetSalesTotalAmount())
            .addMe(getDaiShouAmount())
            .addMe(getDaiShouAmount2())
            .addMe(getDaiFuAmount().negate())
            .addMe(getSpillAmount())
            .addMe(getPaidInAmount())
            .addMe(getPaidOutAmount().negate());
    }
    
    /**
     * 钱箱总金额 = 现金支付 - 找零 + PaidIn - PaidOut + 借零 - 投库
     * 注意: shift.Pay00Amount = (现金支付 - 找零)
     */
    public HYIDouble getTotalCashAmount() {
    	return new HYIDouble(0)
	    	.addMe(getPay00Amount() == null ? new HYIDouble(0) : getPay00Amount())
	    	.addMe(getCashInAmount() == null ? new HYIDouble(0) : getCashInAmount())
	    	.addMe(getCashOutAmount() == null ? new HYIDouble(0) : getCashOutAmount().negate())
	    	.addMe(getPaidInAmount() == null ? new HYIDouble(0) : getPaidInAmount())
	    	.addMe(getPaidOutAmount() == null ? new HYIDouble(0) : getPaidOutAmount().negate());
    }

    public HYIDouble getTotalPayAmount() {
         HYIDouble total = new HYIDouble(0);
         for (int i = 0; i < 30; i++) {
             String j = i + "";
             if (i < 10)
                j = "0" + i;
             HYIDouble sub = (HYIDouble)getFieldValue("PAY" + j + "AMT");
             if (sub != null) {
                total = total.addMe(sub);
             }
         }
         return total;
    }



//表头

//StoreNumber	STORENO	CHAR(6)	N
    public String getStoreNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("StoreID");
        else
            return (String)getFieldValue("STORENO");
    }

    public void setStoreNumber(String storeNumber) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("StoreID", storeNumber);
        else
            setFieldValue("STORENO", storeNumber);
    }

//TerminalNumber	TMCODE	TINYINT UNSIGNED	N
    public Integer getTerminalNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("PosNumber");
        else
            return (Integer)getFieldValue("POSNO");
    }

    public void setTerminalNumber(Integer posno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("PosNumber", posno);
        else
            setFieldValue("POSNO", posno);
    }

//MachineNumber
    public String getMachineNumber() {
        return (String)getFieldValue("MACHINENO");
    }

    public void setMachineNumber(String machineno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("MachineNumber", machineno);
        else
            setFieldValue("MACHINENO", machineno);
    }

//SequenceNumber	SHIFTCNT	TINYINT UNSIGNED	N
    public Integer getSequenceNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("ShiftSequenceNumber");
        else
            return (Integer)getFieldValue("SHIFTCNT");
    }

    public void setSequenceNumber(Integer SQNumber) {
        setFieldValue("SHIFTCNT", SQNumber);
    }

//ZSequenceNumber EODCNT	TINYINT UNSIGNED	N
    public Integer getZSequenceNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("ZSequenceNumber");
        else
            return (Integer)getFieldValue("EODCNT");
    }

    public void setZSequenceNumber(Integer ZNO) {
        setFieldValue("EODCNT", ZNO);
    }

//AccountingDate
    public Date getAccountingDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (Date)getFieldValue("AccountDate");
        else
            return (Date)getFieldValue("ACCDATE");
    }

    public void setAccountingDate(Date accdate) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("AccountDate", accdate);
        else
            setFieldValue("ACCDATE", accdate);
    }

//SignOnSystemDateTime
    public Date getSignOnSystemDateTime() {
        if (hyi.cream.inline.Server.serverExist())
            return (Date)getFieldValue("SignOnSystemDateTime");
        else
            return (Date)getFieldValue("SGNONDTTM");
    }

    public void setSignOnSystemDateTime(Date sgnondttm) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("SignOnSystemDateTime", sgnondttm);
        else
            setFieldValue("SGNONDTTM", sgnondttm);
    }

//SignOffSystemDateTime
    public Date getSignOffSystemDateTime() {
        if (hyi.cream.inline.Server.serverExist())
            return (Date)getFieldValue("SignOffSystemDateTime");
        else
            return (Date)getFieldValue("SGNOFDTTM");
    }

    public void setSignOffSystemDateTime(Date sgnoffdttm) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("SignOffSystemDateTime", sgnoffdttm);
        else
            setFieldValue("SGNOFDTTM", sgnoffdttm);
    }

//BeginTransactionNumber	SGNONSEQ	INT UNSIGNED	N
    public Integer getBeginTransactionNumber() {
        return (Integer)getFieldValue("SGNONSEQ");
    }

    public void setBeginTransactionNumber(DbConnection connection, Integer no) throws SQLException {
        setFieldValue("SGNONSEQ", no);
        PARAM.updateBeginTransactionNumberOfCashier(no);
    }

//EndTransactionNumber	SGNOFFSEQ	INT UNSIGNED	N
    public Integer getEndTransactionNumber() {
        return (Integer)getFieldValue("SGNOFFSEQ");
    }

    public void setEndTransactionNumber(DbConnection connection, Integer no) throws SQLException {
        setFieldValue("SGNOFFSEQ", no);
        PARAM.updateEndTransactionNumberOfCashier(no);
    }

//BeginInvoiceNumber	SGNONINV	CHAR(10)	N
    public String getBeginInvoiceNumber() {
        return (String)getFieldValue("SGNONINV");
    }

    public void setBeginInvoiceNumber(DbConnection connection, String invNo) throws SQLException {
        setFieldValue("SGNONINV", invNo);
        PARAM.updateBeginInvoiceNumberOfCashier(invNo);
    }

//EndInvoiceNumber	SGNOFFINV	CHAR(10)	N
    public String getEndInvoiceNumber() {
        return (String)getFieldValue("SGNOFFINV");
    }

    public void setEndInvoiceNumber(DbConnection connection, String no) throws SQLException {
        setFieldValue("SGNOFFINV", no);
        PARAM.updateEndInvoiceNumberOfCashier(no);
    }

//UploadState	TCPFLG	ENUM("0","1","2")	N
//"0":未上传 "1":上传成功 "2"：上传失败
    public String getUploadState() {
        return (String)getFieldValue("TCPFLG");
    }

    public void setUploadState(String TcpFlag) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("UploadState", TcpFlag);
        else
            setFieldValue("TCPFLG", TcpFlag);
    }

//CashierNumber	CASHIER	CHAR(7)	N
    public String getCashierNumber() {
        return (String)getFieldValue("CASHIER");
    }

    public void setCashierNumber(String cashierNo) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("CashierID", cashierNo);
        else
            setFieldValue("CASHIER", cashierNo);
    }

//InitialCash
    /*public HYIDouble getInitialCash() {
        return (HYIDouble)getFieldValue("INITCASH");
    }

    public void setInitialCash(HYIDouble initcash) {
        setFieldValue("INITCASH", initcash);
    }*/


//交易明细

//TaxType0GrossSales	GROSALTX0AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType0GrossSales() {
        return (HYIDouble) getFieldValue("GROSALTX0AM");
    }

    public void setTaxType0GrossSales(HYIDouble TaxType0GrossSales) {
        setFieldValue("GROSALTX0AM", TaxType0GrossSales);
    }

//TaxType1GrossSales	GROSALTX1AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType1GrossSales() {
        return (HYIDouble) getFieldValue("GROSALTX1AM");
    }

    public void setTaxType1GrossSales(HYIDouble TaxType1GrossSales) {
        setFieldValue("GROSALTX1AM", TaxType1GrossSales);
    }

//TaxType2GrossSales	GROSALTX2AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType2GrossSales() {
        return (HYIDouble) getFieldValue("GROSALTX2AM");
    }

    public void setTaxType2GrossSales(HYIDouble TaxType2GrossSales) {
        setFieldValue("GROSALTX2AM", TaxType2GrossSales);
    }

//TaxType3GrossSales	GROSALTX3AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType3GrossSales() {
        return (HYIDouble) getFieldValue("GROSALTX3AM");
    }

    public void setTaxType3GrossSales(HYIDouble TaxType3GrossSales) {
        setFieldValue("GROSALTX3AM", TaxType3GrossSales);
    }

//TaxType4GrossSales	GROSALTX4AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType4GrossSales() {
        return (HYIDouble) getFieldValue("GROSALTX4AM");
    }

    public void setTaxType4GrossSales(HYIDouble TaxType4GrossSales) {
        setFieldValue("GROSALTX4AM", TaxType4GrossSales);
    }
//TaxType5GrossSales	GROSALTX5AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType5GrossSales() {
        return (HYIDouble) getFieldValue("GROSALTX5AM");
    }

    public void setTaxType5GrossSales(HYIDouble TaxType5GrossSales) {
        setFieldValue("GROSALTX5AM", TaxType5GrossSales);
    }
//TaxType6GrossSales	GROSALTX6AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType6GrossSales() {
        return (HYIDouble) getFieldValue("GROSALTX6AM");
    }

    public void setTaxType6GrossSales(HYIDouble TaxType6GrossSales) {
        setFieldValue("GROSALTX6AM", TaxType6GrossSales);
    }
//TaxType7GrossSales	GROSALTX7AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType7GrossSales() {
        return (HYIDouble) getFieldValue("GROSALTX7AM");
    }

    public void setTaxType7GrossSales(HYIDouble TaxType7GrossSales) {
        setFieldValue("GROSALTX7AM", TaxType7GrossSales);
    }
//TaxType8GrossSales	GROSALTX8AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType8GrossSales() {
        return (HYIDouble) getFieldValue("GROSALTX8AM");
    }

    public void setTaxType8GrossSales(HYIDouble TaxType8GrossSales) {
        setFieldValue("GROSALTX8AM", TaxType8GrossSales);
    }
//TaxType9GrossSales	GROSALTX9AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType9GrossSales() {
        return (HYIDouble) getFieldValue("GROSALTX9AM");
    }

    public void setTaxType9GrossSales(HYIDouble TaxType9GrossSales) {
        setFieldValue("GROSALTX9AM", TaxType9GrossSales);
    }
//TaxType10GrossSales	GROSALTX10AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType10GrossSales() {
        return (HYIDouble) getFieldValue("GROSALTX10AM");
    }

    public void setTaxType10GrossSales(HYIDouble TaxType10GrossSales) {
        setFieldValue("GROSALTX10AM", TaxType10GrossSales);
    }

    //  TaxAmount0	TAXAMT0	DECIMAL(12,2)	N	税别0税额
    public HYIDouble getTaxAmount0() {
        return (HYIDouble)getFieldValue("TAXAMT0");
    }

    public void setTaxAmount0(HYIDouble taxamt0) {
        setFieldValue("TAXAMT0", taxamt0);
    }

    //  TaxAmount1	TAXAMT1	DECIMAL(12,2)	N	税别1税额
    public HYIDouble getTaxAmount1() {
        return (HYIDouble)getFieldValue("TAXAMT1");
    }

    public void setTaxAmount1(HYIDouble taxamt1) {
        setFieldValue("TAXAMT1", taxamt1);
    }

    //  TaxAmount2	TAXAMT2	DECIMAL(12,2)	N	税别2税额
    public HYIDouble getTaxAmount2() {
        return (HYIDouble)getFieldValue("TAXAMT2");
    }

    public void setTaxAmount2(HYIDouble taxamt2) {
        setFieldValue("TAXAMT2", taxamt2);
    }

    //  TaxAmount3	TAXAMT3	DECIMAL(12,2)	N	税别3税额
    public HYIDouble getTaxAmount3() {
        return (HYIDouble)getFieldValue("TAXAMT3");
    }

    public void setTaxAmount3(HYIDouble taxamt3) {
        setFieldValue("TAXAMT3", taxamt3);
    }

    //  TaxAmount4	TAXAMT4	DECIMAL(12,2)	N	税别4税额
    public HYIDouble getTaxAmount4() {
        return (HYIDouble)getFieldValue("TAXAMT4");
    }

    public void setTaxAmount4(HYIDouble taxamt4) {
        setFieldValue("TAXAMT4", taxamt4);
    }

    //  TaxAmount5	TAXAMT5	DECIMAL(12,2)	N	税别5税额
    public HYIDouble getTaxAmount5() {
        return (HYIDouble)getFieldValue("TAXAMT5");
    }

    public void setTaxAmount5(HYIDouble taxamt5) {
        setFieldValue("TAXAMT5", taxamt5);
    }

    //  TaxAmount6	TAXAMT6	DECIMAL(12,2)	N	税别6税额
    public HYIDouble getTaxAmount6() {
        return (HYIDouble)getFieldValue("TAXAMT6");
    }

    public void setTaxAmount6(HYIDouble taxamt6) {
        setFieldValue("TAXAMT6", taxamt6);
    }

    //  TaxAmount7	TAXAMT7	DECIMAL(12,2)	N	税别7税额
    public HYIDouble getTaxAmount7() {
        return (HYIDouble)getFieldValue("TAXAMT7");
    }

    public void setTaxAmount7(HYIDouble taxamt7) {
        setFieldValue("TAXAMT7", taxamt7);
    }

    //  TaxAmount8	TAXAMT8	DECIMAL(12,2)	N	税别8税额
    public HYIDouble getTaxAmount8() {
        return (HYIDouble)getFieldValue("TAXAMT8");
    }

    public void setTaxAmount8(HYIDouble taxamt8) {
        setFieldValue("TAXAMT8", taxamt8);
    }

    //  TaxAmount9	TAXAMT9	DECIMAL(12,2)	N	税别9税额
    public HYIDouble getTaxAmount9() {
        return (HYIDouble)getFieldValue("TAXAMT9");
    }

    public void setTaxAmount9(HYIDouble taxamt9) {
        setFieldValue("TAXAMT9", taxamt9);
    }

    //  TaxAmount10	TAXAMT10	DECIMAL(12,2)	N	税别10税额
    public HYIDouble getTaxAmount10() {
        return (HYIDouble)getFieldValue("TAXAMT10");
    }

    public void setTaxAmount10(HYIDouble taxamt10) {
        setFieldValue("TAXAMT10", taxamt10);
    }

    //Bruce/20030328
    public HYIDouble getTotalTaxAmount() {
        return new HYIDouble(0).addMe(getTaxAmount0()).addMe(getTaxAmount1()).addMe(getTaxAmount2()).addMe(getTaxAmount3()).addMe(getTaxAmount4());
    }

    //  TaxAmount   税别合计
    private HYIDouble taxAmount = new HYIDouble(0);
    public HYIDouble getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(HYIDouble taxamt) {
        taxAmount = taxamt;
    }

//ItemCount	ITEMSALEQTY	INT UNSIGNED	N
    public HYIDouble getItemCount() {
        return (HYIDouble)getFieldValue("ITEMSALEQTY");
    }

    public void setItemCount(HYIDouble saleCount){
        setFieldValue("ITEMSALEQTY",saleCount);
    }

//TransactionCount	TRANCNT	INT UNSIGNED	N
    public Integer getTransactionCount() {
        Object o = getFieldValue("TRANCNT");
        int n = -1;
        if (o instanceof Integer)
            n = ((Integer)o).intValue();
        else if (o instanceof Long)
            n = ((Long)o).intValue();
        return (new Integer(n)) ;

    }

    public void setTransactionCount(Integer tranCount) {
        setFieldValue("TRANCNT", tranCount);
    }

//CustomerCount
    public Integer getCustomerCount() {
        return (Integer)getFieldValue("CUSTCNT");
    }

    public void setCustomerCount(Integer custcnt) {
        setFieldValue("CUSTCNT", custcnt);
    }

   /*
    public Integer getInCustomerCount() {
        return (Integer)getFieldValue("INCUSTCNT");
    }

    public void setInCustomerCount(Integer custcnt) {
        setFieldValue("INCUSTCNT", custcnt);
    }

    public Integer getOutCustomerCount() {
        return (Integer)getFieldValue("OUTCUSTCNT");
    }

    public void setOutCustomerCount(Integer custcnt) {
        setFieldValue("OUTCUSTCNT", custcnt);
    }*/

//SIPercentPlusTotalAmount		HYIDouble	N	各种税别SI总加成金额
    public HYIDouble getSIPercentPlusTotalAmount() {
        HYIDouble total = new HYIDouble(0);
        total = total.addMe(getSIPercentPlusAmount0());
        total = total.addMe(getSIPercentPlusAmount1());
        total = total.addMe(getSIPercentPlusAmount2());
        total = total.addMe(getSIPercentPlusAmount3());
        total = total.addMe(getSIPercentPlusAmount4());
        total = total.addMe(getSIPercentPlusAmount6());
        total = total.addMe(getSIPercentPlusAmount7());
        total = total.addMe(getSIPercentPlusAmount8());
        total = total.addMe(getSIPercentPlusAmount9());
        total = total.addMe(getSIPercentPlusAmount10());
        return total;
    }

//SIPercentPlusTotalCount		int	N	各种税别 SI总加成次数
    public Integer getSIPercentPlusTotalCount() {
        int total = getSIPercentPlusCount0().intValue();
        total += getSIPercentPlusCount1().intValue();
        total += getSIPercentPlusCount2().intValue();
        total += getSIPercentPlusCount3().intValue();
        total += getSIPercentPlusCount4().intValue();
        total += getSIPercentPlusCount5().intValue();
        total += getSIPercentPlusCount6().intValue();
        total += getSIPercentPlusCount7().intValue();
        total += getSIPercentPlusCount8().intValue();
        total += getSIPercentPlusCount9().intValue();
        total += getSIPercentPlusCount10().intValue();
        return new Integer(total);
    }

//SIPercentOffTotalAmount		HYIDouble	N	各种税别 SI总折扣金额
    public HYIDouble getSIPercentOffTotalAmount() {
        HYIDouble total = new HYIDouble(0);
        total = total.addMe(getSIPercentOffAmount0());
        total = total.addMe(getSIPercentOffAmount1());
        total = total.addMe(getSIPercentOffAmount2());
        total = total.addMe(getSIPercentOffAmount3());
        total = total.addMe(getSIPercentOffAmount4());
        total = total.addMe(getSIPercentOffAmount5());
        total = total.addMe(getSIPercentOffAmount6());
        total = total.addMe(getSIPercentOffAmount7());
        total = total.addMe(getSIPercentOffAmount8());
        total = total.addMe(getSIPercentOffAmount9());
        total = total.addMe(getSIPercentOffAmount10());
        return total;
    }

//SIPercentOffTotalCount		int	N	各种税别 SI总折扣次数
    public Integer getSIPercentOffTotalCount() {
        int total = getSIPercentOffCount0().intValue();
        total += getSIPercentOffCount1().intValue();
        total += getSIPercentOffCount2().intValue();
        total += getSIPercentOffCount3().intValue();
        total += getSIPercentOffCount4().intValue();
        total += getSIPercentOffCount5().intValue();
        total += getSIPercentOffCount6().intValue();
        total += getSIPercentOffCount7().intValue();
        total += getSIPercentOffCount8().intValue();
        total += getSIPercentOffCount9().intValue();
        total += getSIPercentOffCount10().intValue();
        return new Integer(total);
    }

//SIDiscountTotalAmount		HYIDouble	N	各种税别总折让金额
    public HYIDouble getSIDiscountTotalAmount() {
        HYIDouble total = new HYIDouble(0);
        total = total.addMe(getSIDiscountAmount0());
        total = total.addMe(getSIDiscountAmount1());
        total = total.addMe(getSIDiscountAmount2());
        total = total.addMe(getSIDiscountAmount3());
        total = total.addMe(getSIDiscountAmount4());
        total = total.addMe(getSIDiscountAmount5());
        total = total.addMe(getSIDiscountAmount6());
        total = total.addMe(getSIDiscountAmount7());
        total = total.addMe(getSIDiscountAmount8());
        total = total.addMe(getSIDiscountAmount9());
        total = total.addMe(getSIDiscountAmount10());
        return total;
    }

//SIDiscountTotalCount		int	N	各种税别总折让次数

    public Integer getSIDiscountTotalCount() {
        int total = getSIDiscountCount0().intValue();
        total += getSIDiscountCount1().intValue();
        total += getSIDiscountCount2().intValue();
        total += getSIDiscountCount3().intValue();
        total += getSIDiscountCount4().intValue();
        total += getSIDiscountCount5().intValue();
        total += getSIDiscountCount6().intValue();
        total += getSIDiscountCount7().intValue();
        total += getSIDiscountCount8().intValue();
        total += getSIDiscountCount9().intValue();
        total += getSIDiscountCount10().intValue();
        return new Integer(total);
    }

//MixAndMatchTotalAmount		HYIDouble	N	各种税别 M&M折让金额
    /*public HYIDouble getMixAndMatchTotalAmount() {
        HYIDouble total = getMixAndMatchAmount0();
        total = total.add(getMixAndMatchAmount1());
        total = total.add(getMixAndMatchAmount2());
        total = total.add(getMixAndMatchAmount3());
        total = total.add(getMixAndMatchAmount4());
        return total;
    }*/

//MixAndMatchTotalCount		int	N	各种税别 M&M折让次数
    /*public Integer getMixAndMatchTotalCount() {
        int total = getMixAndMatchCount0().intValue();
        total += getMixAndMatchCount1().intValue();
        total += getMixAndMatchCount2().intValue();
        total += getMixAndMatchCount3().intValue();
        total += getMixAndMatchCount4().intValue();
        return new Integer(total);
    }*/

//NotIncludedTotalSales		HYIDouble	N	各种税别已开发票支付金额
    public HYIDouble getNotIncludedTotalSales() {
        HYIDouble total = new HYIDouble(0);
        total = total.addMe(getNotIncludedSales0());
        total = total.addMe(getNotIncludedSales1());
        total = total.addMe(getNotIncludedSales2());
        total = total.addMe(getNotIncludedSales3());
        total = total.addMe(getNotIncludedSales4());
        total = total.addMe(getNotIncludedSales5());
        total = total.addMe(getNotIncludedSales6());
        total = total.addMe(getNotIncludedSales7());
        total = total.addMe(getNotIncludedSales8());
        total = total.addMe(getNotIncludedSales9());
        total = total.addMe(getNotIncludedSales10());
        return total;
    }
//NetSalesTotalAmount		HYIDouble	N	各种税别发票金额

    /*
    public HYIDouble getNetSalesTotalAmount() {
        HYIDouble total = getNetSalesAmount0();
        total = total.add(getNetSalesAmount1());
        total = total.add(getNetSalesAmount2());
        total = total.add(getNetSalesAmount3());
        total = total.add(getNetSalesAmount4());
        return total;
   }*/

//
    public Integer getTotalPayCount() {
        int total = 0;
        for (int i = 0; i < 30; i++) {
            String j = String.valueOf(i);
            if (i < 10)
                j = "0" + i;
            if (getFieldValue("PAY" + j + "CNT") != null) {
                Integer t = (Integer)getFieldValue("PAY" + j + "CNT");
                total += t.intValue();
            }
        }
        return new Integer(total);
    }
/*****************************************************************************/

//0
//SIPercentPlusAmount0	SIPLUSAMT0	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount0() {
        return (HYIDouble)getFieldValue("SIPLUSAMT0");
    }

    public void setSIPercentPlusAmount0(HYIDouble SIPercentPlusAmount0) {
        setFieldValue("SIPLUSAMT0", SIPercentPlusAmount0);
    }

//SIPercentPlusCount0	SIPLUSCNT0	INT UNSIGNED	N
    public Integer getSIPercentPlusCount0() {
        return (Integer)getFieldValue("SIPLUSCNT0");
    }

    public void setSIPercentPlusCount0(Integer SIPercentPlusCount0) {
        setFieldValue("SIPLUSCNT0", SIPercentPlusCount0);
    }

//SIPercentOffAmount0	SIPAMT0	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount0() {
        return (HYIDouble)getFieldValue("SIPAMT0");
    }

    public void setSIPercentOffAmount0(HYIDouble SIPercentOffAmount0) {
        setFieldValue("SIPAMT0", SIPercentOffAmount0);
    }

//SIPercentOffCount0	SIPCNT0	INT UNSIGNED	N
    public Integer getSIPercentOffCount0() {
        return (Integer)getFieldValue("SIPCNT0");
    }

    public void setSIPercentOffCount0(Integer SIPercentOffCount0) {
        setFieldValue("SIPCNT0", SIPercentOffCount0);
    }

//SIDiscountAmount0	SIMAMT0	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount0() {
        return (HYIDouble)getFieldValue("SIMAMT0");
    }

    public void setSIDiscountAmount0(HYIDouble SIDiscountAmount0) {
        setFieldValue("SIMAMT0", SIDiscountAmount0);
    }

//SIDiscountCount0	SIMCNT0	INT UNSIGNED	N
    public Integer getSIDiscountCount0() {
        return (Integer)getFieldValue("SIMCNT0");
    }

    public void setSIDiscountCount0(Integer SIDiscountCount0) {
        setFieldValue("SIMCNT0", SIDiscountCount0);
    }

//MixAndMatchAmount0
    public HYIDouble getMixAndMatchAmount0() {
        return (HYIDouble)getFieldValue("MNMAMT0");
    }

    public void setMixAndMatchAmount0(HYIDouble mnmamt0) {
        setFieldValue("MNMAMT0", mnmamt0);
    }

//MixAndMatchCount0	MNMCNT0	INT UNSIGNED	N
    public Integer getMixAndMatchCount0() {
        return (Integer)getFieldValue("MNMCNT0");
    }

    public void setMixAndMatchCount0(Integer MixAndMatchCount0) {
        setFieldValue("MNMCNT0", MixAndMatchCount0);
    }

//NotIncludedSales0
    public HYIDouble getNotIncludedSales0() {
        return (HYIDouble)getFieldValue("RCPGIFAMT0");
    }

    public void setNotIncludedSales0(HYIDouble rcpgifamt0) {
        setFieldValue("RCPGIFAMT0", rcpgifamt0);
    }

//NetSalesAmount0
    public HYIDouble getNetSalesAmount0() {
        return (HYIDouble)getFieldValue("NETSALAMT0");
    }

    public void setNetSalesAmount0(HYIDouble netsalamt0) {
        setFieldValue("NETSALAMT0", netsalamt0);
    }

//1
//SIPercentPlusAmount1	SIPLUSAMT1	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount1() {
        return (HYIDouble)getFieldValue("SIPLUSAMT1");
    }

    public void setSIPercentPlusAmount1(HYIDouble SIPercentPlusAmount1) {
        setFieldValue("SIPLUSAMT1", SIPercentPlusAmount1);
    }

//SIPercentPlusCount1	SIPLUSCNT1	INT UNSIGNED	N
    public Integer getSIPercentPlusCount1() {
        return (Integer)getFieldValue("SIPLUSCNT1");
    }

    public void setSIPercentPlusCount1(Integer SIPercentPlusCount1) {
        setFieldValue("SIPLUSCNT1", SIPercentPlusCount1);
    }

//SIPercentOffAmount1	SIPAMT1	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount1() {
        return (HYIDouble)getFieldValue("SIPAMT1");
    }

    public void setSIPercentOffAmount1(HYIDouble SIPercentOffAmount1) {
        setFieldValue("SIPAMT1", SIPercentOffAmount1);
    }

//SIPercentOffCount1	SIPCNT1	INT UNSIGNED	N
    public Integer getSIPercentOffCount1() {
        return (Integer)getFieldValue("SIPCNT1");
    }

    public void setSIPercentOffCount1(Integer SIPercentOffCount1) {
        setFieldValue("SIPCNT1", SIPercentOffCount1);
    }

//SIDiscountAmount1	SIMAMT1	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount1() {
        return (HYIDouble)getFieldValue("SIMAMT1");
    }

    public void setSIDiscountAmount1(HYIDouble SIDiscountAmount1) {
        setFieldValue("SIMAMT1", SIDiscountAmount1);
    }

//SIDiscountCount1	SIMCNT1	INT UNSIGNED	N
    public Integer getSIDiscountCount1() {
        return (Integer)getFieldValue("SIMCNT1");
    }

    public void setSIDiscountCount1(Integer SIDiscountCount1) {
        setFieldValue("SIMCNT1", SIDiscountCount1);
    }

//MixAndMatchAmount1
    public HYIDouble getMixAndMatchAmount1() {
        return (HYIDouble)getFieldValue("MNMAMT1");
    }

    public void setMixAndMatchAmount1(HYIDouble mnmamt1) {
        setFieldValue("MNMAMT1", mnmamt1);
    }

//MixAndMatchCount1	MNMCNT1	INT UNSIGNED	N
    public Integer getMixAndMatchCount1() {
        return (Integer)getFieldValue("MNMCNT1");
    }

    public void setMixAndMatchCount1(Integer MixAndMatchCount1) {
        setFieldValue("MNMCNT1", MixAndMatchCount1);
    }

//NotIncludedSales1
    public HYIDouble getNotIncludedSales1() {
        return (HYIDouble)getFieldValue("RCPGIFAMT1");
    }

    public void setNotIncludedSales1(HYIDouble rcpgifamt1) {
        setFieldValue("RCPGIFAMT1", rcpgifamt1);
    }

//NetSalesAmount1
    public HYIDouble getNetSalesAmount1() {
        return (HYIDouble)getFieldValue("NETSALAMT1");
    }

    public void setNetSalesAmount1(HYIDouble netsalamt1) {
        setFieldValue("NETSALAMT1", netsalamt1);
    }

//2
//SIPercentPlusAmount2	SIPLUSAMT2	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount2() {
        return (HYIDouble)getFieldValue("SIPLUSAMT2");
    }

    public void setSIPercentPlusAmount2(HYIDouble SIPercentPlusAmount2) {
        setFieldValue("SIPLUSAMT2", SIPercentPlusAmount2);
    }

//SIPercentPlusCount2	SIPLUSCNT2	INT UNSIGNED	N
    public Integer getSIPercentPlusCount2() {
        return (Integer)getFieldValue("SIPLUSCNT2");
    }

    public void setSIPercentPlusCount2(Integer SIPercentPlusCount2) {
        setFieldValue("SIPLUSCNT2", SIPercentPlusCount2);
    }

//SIPercentOffAmount2	SIPAMT2	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount2() {
        return (HYIDouble)getFieldValue("SIPAMT2");
    }

    public void setSIPercentOffAmount2(HYIDouble SIPercentOffAmount2) {
        setFieldValue("SIPAMT2", SIPercentOffAmount2);
    }

//SIPercentOffCount2	SIPCNT2	INT UNSIGNED	N
    public Integer getSIPercentOffCount2() {
        return (Integer)getFieldValue("SIPCNT2");
    }

    public void setSIPercentOffCount2(Integer SIPercentOffCount2) {
        setFieldValue("SIPCNT2", SIPercentOffCount2);
    }

//SIDiscountAmount2	SIMAMT2	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount2() {
        return (HYIDouble)getFieldValue("SIMAMT2");
    }

    public void setSIDiscountAmount2(HYIDouble SIDiscountAmount2) {
        setFieldValue("SIMAMT2", SIDiscountAmount2);
    }

//SIDiscountCount2	SIMCNT2	INT UNSIGNED	N
    public Integer getSIDiscountCount2() {
        return (Integer)getFieldValue("SIMCNT2");
    }

    public void setSIDiscountCount2(Integer SIDiscountCount2) {
        setFieldValue("SIMCNT2", SIDiscountCount2);
    }

//MixAndMatchAmount2
    public HYIDouble getMixAndMatchAmount2() {
        return (HYIDouble)getFieldValue("MNMAMT2");
    }

    public void setMixAndMatchAmount2(HYIDouble mnmamt2) {
        setFieldValue("MNMAMT2", mnmamt2);
    }

//MixAndMatchCount2	MNMCNT2	INT UNSIGNED	N
    public Integer getMixAndMatchCount2() {
        return (Integer)getFieldValue("MNMCNT2");
    }

    public void setMixAndMatchCount2(Integer MixAndMatchCount2) {
        setFieldValue("MNMCNT2", MixAndMatchCount2);
    }

//NotIncludedSales2
    public HYIDouble getNotIncludedSales2() {
        return (HYIDouble)getFieldValue("RCPGIFAMT2");
    }

    public void setNotIncludedSales2(HYIDouble rcpgifamt2) {
        setFieldValue("RCPGIFAMT2", rcpgifamt2);
    }

//NetSalesAmount2
    public HYIDouble getNetSalesAmount2() {
        return (HYIDouble)getFieldValue("NETSALAMT2");
    }

    public void setNetSalesAmount2(HYIDouble netsalamt2) {
        setFieldValue("NETSALAMT2", netsalamt2);
    }

//3
//SIPercentPlusAmount3	SIPLUSAMT3	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount3() {
        return (HYIDouble)getFieldValue("SIPLUSAMT3");
    }

    public void setSIPercentPlusAmount3(HYIDouble SIPercentPlusAmount3) {
        setFieldValue("SIPLUSAMT3", SIPercentPlusAmount3);
    }

//SIPercentPlusCount3	SIPLUSCNT3	INT UNSIGNED	N
    public Integer getSIPercentPlusCount3() {
        return (Integer)getFieldValue("SIPLUSCNT3");
    }

    public void setSIPercentPlusCount3(Integer SIPercentPlusCount3) {
        setFieldValue("SIPLUSCNT3", SIPercentPlusCount3);
    }

//SIPercentOffAmount3	SIPAMT3	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount3() {
        return (HYIDouble)getFieldValue("SIPAMT3");
    }

    public void setSIPercentOffAmount3(HYIDouble SIPercentOffAmount3) {
        setFieldValue("SIPAMT3", SIPercentOffAmount3);
    }

//SIPercentOffCount3	SIPCNT3	INT UNSIGNED	N
    public Integer getSIPercentOffCount3() {
        return (Integer)getFieldValue("SIPCNT3");
    }

    public void setSIPercentOffCount3(Integer SIPercentOffCount3) {
        setFieldValue("SIPCNT3", SIPercentOffCount3);
    }

//SIDiscountAmount3	SIMAMT3	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount3() {
        return (HYIDouble)getFieldValue("SIMAMT3");
    }

    public void setSIDiscountAmount3(HYIDouble SIDiscountAmount3) {
        setFieldValue("SIMAMT3", SIDiscountAmount3);
    }

//SIDiscountCount3	SIMCNT3	INT UNSIGNED	N
    public Integer getSIDiscountCount3() {
        return (Integer)getFieldValue("SIMCNT3");
    }

    public void setSIDiscountCount3(Integer SIDiscountCount3) {
        setFieldValue("SIMCNT3", SIDiscountCount3);
    }

//MixAndMatchAmount3
    public HYIDouble getMixAndMatchAmount3() {
        return (HYIDouble)getFieldValue("MNMAMT3");
    }

    public void setMixAndMatchAmount3(HYIDouble mnmamt3) {
        setFieldValue("MNMAMT3", mnmamt3);
    }

//MixAndMatchCount3	MNMCNT3	INT UNSIGNED	N
    public Integer getMixAndMatchCount3() {
        return (Integer)getFieldValue("MNMCNT3");
    }

    public void setMixAndMatchCount3(Integer MixAndMatchCount3) {
        setFieldValue("MNMCNT3", MixAndMatchCount3);
    }

//NotIncludedSales3
    public HYIDouble getNotIncludedSales3() {
        return (HYIDouble)getFieldValue("RCPGIFAMT3");
    }

    public void setNotIncludedSales3(HYIDouble rcpgifamt3) {
        setFieldValue("RCPGIFAMT3", rcpgifamt3);
    }

//NetSalesAmount3
    public HYIDouble getNetSalesAmount3() {
        return (HYIDouble)getFieldValue("NETSALAMT3");
    }

    public void setNetSalesAmount3(HYIDouble netsalamt3) {
        setFieldValue("NETSALAMT3", netsalamt3);
    }

//4
//SIPercentPlusAmount4	SIPLUSAMT4	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount4() {
        return (HYIDouble)getFieldValue("SIPLUSAMT4");
    }

    public void setSIPercentPlusAmount4(HYIDouble SIPercentPlusAmount4) {
        setFieldValue("SIPLUSAMT4", SIPercentPlusAmount4);
    }

//SIPercentPlusCount4	SIPLUSCNT4	INT UNSIGNED	N
    public Integer getSIPercentPlusCount4() {
        return (Integer)getFieldValue("SIPLUSCNT4");
    }

    public void setSIPercentPlusCount4(Integer SIPercentPlusCount4) {
        setFieldValue("SIPLUSCNT4", SIPercentPlusCount4);
    }

//SIPercentOffAmount4	SIPAMT4	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount4() {
        return (HYIDouble)getFieldValue("SIPAMT4");
    }

    public void setSIPercentOffAmount4(HYIDouble SIPercentOffAmount4) {
        setFieldValue("SIPAMT4", SIPercentOffAmount4);
    }

//SIPercentOffCount4	SIPCNT4	INT UNSIGNED	N
    public Integer getSIPercentOffCount4() {
        return (Integer)getFieldValue("SIPCNT4");
    }

    public void setSIPercentOffCount4(Integer SIPercentOffCount4) {
        setFieldValue("SIPCNT4", SIPercentOffCount4);
    }

//SIDiscountAmount4	SIMAMT4	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount4() {
        return (HYIDouble)getFieldValue("SIMAMT4");
    }

    public void setSIDiscountAmount4(HYIDouble SIDiscountAmount4) {
        setFieldValue("SIMAMT4", SIDiscountAmount4);
    }

//SIDiscountCount4	SIMCNT4	INT UNSIGNED	N
    public Integer getSIDiscountCount4() {
        return (Integer)getFieldValue("SIMCNT4");
    }

    public void setSIDiscountCount4(Integer SIDiscountCount4) {
        setFieldValue("SIMCNT4", SIDiscountCount4);
    }

//MixAndMatchAmount4
    public HYIDouble getMixAndMatchAmount4() {
        return (HYIDouble)getFieldValue("MNMAMT4");
    }

    public void setMixAndMatchAmount4(HYIDouble mnmamt4) {
        setFieldValue("MNMAMT4", mnmamt4);
    }

//MixAndMatchCount4	MNMCNT4	INT UNSIGNED	N
    public Integer getMixAndMatchCount4() {
        return (Integer)getFieldValue("MNMCNT4");
    }

    public void setMixAndMatchCount4(Integer MixAndMatchCount4) {
        setFieldValue("MNMCNT4", MixAndMatchCount4);
    }

//NotIncludedSales4
    public HYIDouble getNotIncludedSales4() {
        return (HYIDouble)getFieldValue("RCPGIFAMT4");
    }

    public void setNotIncludedSales4(HYIDouble rcpgifamt4) {
        setFieldValue("RCPGIFAMT4", rcpgifamt4);
    }

//NetSalesAmount4
    public HYIDouble getNetSalesAmount4() {
        return (HYIDouble)getFieldValue("NETSALAMT4");
    }

    public void setNetSalesAmount4(HYIDouble netsalamt4) {
        setFieldValue("NETSALAMT4", netsalamt4);
    }
//5
//SIPercentPlusAmount5	SIPLUSAMT5	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount5() {
        return (HYIDouble)getFieldValue("SIPLUSAMT5");
    }

    public void setSIPercentPlusAmount5(HYIDouble SIPercentPlusAmount5) {
        setFieldValue("SIPLUSAMT5", SIPercentPlusAmount5);
    }

//SIPercentPlusCount5	SIPLUSCNT5	INT UNSIGNED	N
    public Integer getSIPercentPlusCount5() {
        return (Integer)getFieldValue("SIPLUSCNT5");
    }

    public void setSIPercentPlusCount5(Integer SIPercentPlusCount5) {
        setFieldValue("SIPLUSCNT5", SIPercentPlusCount5);
    }

//SIPercentOffAmount5	SIPAMT5	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount5() {
        return (HYIDouble)getFieldValue("SIPAMT5");
    }

    public void setSIPercentOffAmount5(HYIDouble SIPercentOffAmount5) {
        setFieldValue("SIPAMT5", SIPercentOffAmount5);
    }

//SIPercentOffCount5	SIPCNT5	INT UNSIGNED	N
    public Integer getSIPercentOffCount5() {
        return (Integer)getFieldValue("SIPCNT5");
    }

    public void setSIPercentOffCount5(Integer SIPercentOffCount5) {
        setFieldValue("SIPCNT5", SIPercentOffCount5);
    }

//SIDiscountAmount5	SIMAMT5	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount5() {
        return (HYIDouble)getFieldValue("SIMAMT5");
    }

    public void setSIDiscountAmount5(HYIDouble SIDiscountAmount5) {
        setFieldValue("SIMAMT5", SIDiscountAmount5);
    }

//SIDiscountCount5	SIMCNT5	INT UNSIGNED	N
    public Integer getSIDiscountCount5() {
        return (Integer)getFieldValue("SIMCNT5");
    }

    public void setSIDiscountCount5(Integer SIDiscountCount5) {
        setFieldValue("SIMCNT5", SIDiscountCount5);
    }

//MixAndMatchAmount5
    public HYIDouble getMixAndMatchAmount5() {
        return (HYIDouble)getFieldValue("MNMAMT5");
    }

    public void setMixAndMatchAmount5(HYIDouble mnmamt5) {
        setFieldValue("MNMAMT5", mnmamt5);
    }

//MixAndMatchCount5	MNMCNT5	INT UNSIGNED	N
    public Integer getMixAndMatchCount5() {
        return (Integer)getFieldValue("MNMCNT5");
    }

    public void setMixAndMatchCount5(Integer MixAndMatchCount5) {
        setFieldValue("MNMCNT5", MixAndMatchCount5);
    }

//NotIncludedSales5
    public HYIDouble getNotIncludedSales5() {
        return (HYIDouble)getFieldValue("RCPGIFAMT5");
    }

    public void setNotIncludedSales5(HYIDouble rcpgifamt5) {
        setFieldValue("RCPGIFAMT5", rcpgifamt5);
    }

//NetSalesAmount5
    public HYIDouble getNetSalesAmount5() {
        return (HYIDouble)getFieldValue("NETSALAMT5");
    }

    public void setNetSalesAmount5(HYIDouble netsalamt5) {
        setFieldValue("NETSALAMT5", netsalamt5);
    }
//6
//SIPercentPlusAmount6	SIPLUSAMT6	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount6() {
        return (HYIDouble)getFieldValue("SIPLUSAMT6");
    }

    public void setSIPercentPlusAmount6(HYIDouble SIPercentPlusAmount6) {
        setFieldValue("SIPLUSAMT6", SIPercentPlusAmount6);
    }

//SIPercentPlusCount6	SIPLUSCNT6	INT UNSIGNED	N
    public Integer getSIPercentPlusCount6() {
        return (Integer)getFieldValue("SIPLUSCNT6");
    }

    public void setSIPercentPlusCount6(Integer SIPercentPlusCount6) {
        setFieldValue("SIPLUSCNT6", SIPercentPlusCount6);
    }

//SIPercentOffAmount6	SIPAMT6	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount6() {
        return (HYIDouble)getFieldValue("SIPAMT6");
    }

    public void setSIPercentOffAmount6(HYIDouble SIPercentOffAmount6) {
        setFieldValue("SIPAMT6", SIPercentOffAmount6);
    }

//SIPercentOffCount6	SIPCNT6	INT UNSIGNED	N
    public Integer getSIPercentOffCount6() {
        return (Integer)getFieldValue("SIPCNT6");
    }

    public void setSIPercentOffCount6(Integer SIPercentOffCount6) {
        setFieldValue("SIPCNT6", SIPercentOffCount6);
    }

//SIDiscountAmount6	SIMAMT6	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount6() {
        return (HYIDouble)getFieldValue("SIMAMT6");
    }

    public void setSIDiscountAmount6(HYIDouble SIDiscountAmount6) {
        setFieldValue("SIMAMT6", SIDiscountAmount6);
    }

//SIDiscountCount6	SIMCNT6	INT UNSIGNED	N
    public Integer getSIDiscountCount6() {
        return (Integer)getFieldValue("SIMCNT6");
    }

    public void setSIDiscountCount6(Integer SIDiscountCount6) {
        setFieldValue("SIMCNT6", SIDiscountCount6);
    }

//MixAndMatchAmount6
    public HYIDouble getMixAndMatchAmount6() {
        return (HYIDouble)getFieldValue("MNMAMT6");
    }

    public void setMixAndMatchAmount6(HYIDouble mnmamt6) {
        setFieldValue("MNMAMT6", mnmamt6);
    }

//MixAndMatchCount6	MNMCNT6	INT UNSIGNED	N
    public Integer getMixAndMatchCount6() {
        return (Integer)getFieldValue("MNMCNT6");
    }

    public void setMixAndMatchCount6(Integer MixAndMatchCount6) {
        setFieldValue("MNMCNT6", MixAndMatchCount6);
    }

//NotIncludedSales6
    public HYIDouble getNotIncludedSales6() {
        return (HYIDouble)getFieldValue("RCPGIFAMT6");
    }

    public void setNotIncludedSales6(HYIDouble rcpgifamt6) {
        setFieldValue("RCPGIFAMT6", rcpgifamt6);
    }

//NetSalesAmount6
    public HYIDouble getNetSalesAmount6() {
        return (HYIDouble)getFieldValue("NETSALAMT6");
    }

    public void setNetSalesAmount6(HYIDouble netsalamt6) {
        setFieldValue("NETSALAMT6", netsalamt6);
    }
//7
//SIPercentPlusAmount7	SIPLUSAMT7	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount7() {
        return (HYIDouble)getFieldValue("SIPLUSAMT7");
    }

    public void setSIPercentPlusAmount7(HYIDouble SIPercentPlusAmount7) {
        setFieldValue("SIPLUSAMT7", SIPercentPlusAmount7);
    }

//SIPercentPlusCount7	SIPLUSCNT7	INT UNSIGNED	N
    public Integer getSIPercentPlusCount7() {
        return (Integer)getFieldValue("SIPLUSCNT7");
    }

    public void setSIPercentPlusCount7(Integer SIPercentPlusCount7) {
        setFieldValue("SIPLUSCNT7", SIPercentPlusCount7);
    }

//SIPercentOffAmount7	SIPAMT7	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount7() {
        return (HYIDouble)getFieldValue("SIPAMT7");
    }

    public void setSIPercentOffAmount7(HYIDouble SIPercentOffAmount7) {
        setFieldValue("SIPAMT7", SIPercentOffAmount7);
    }

//SIPercentOffCount7	SIPCNT7	INT UNSIGNED	N
    public Integer getSIPercentOffCount7() {
        return (Integer)getFieldValue("SIPCNT7");
    }

    public void setSIPercentOffCount7(Integer SIPercentOffCount7) {
        setFieldValue("SIPCNT7", SIPercentOffCount7);
    }

//SIDiscountAmount7	SIMAMT7	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount7() {
        return (HYIDouble)getFieldValue("SIMAMT7");
    }

    public void setSIDiscountAmount7(HYIDouble SIDiscountAmount7) {
        setFieldValue("SIMAMT7", SIDiscountAmount7);
    }

//SIDiscountCount7	SIMCNT7	INT UNSIGNED	N
    public Integer getSIDiscountCount7() {
        return (Integer)getFieldValue("SIMCNT7");
    }

    public void setSIDiscountCount7(Integer SIDiscountCount7) {
        setFieldValue("SIMCNT7", SIDiscountCount7);
    }

//MixAndMatchAmount7
    public HYIDouble getMixAndMatchAmount7() {
        return (HYIDouble)getFieldValue("MNMAMT7");
    }

    public void setMixAndMatchAmount7(HYIDouble mnmamt7) {
        setFieldValue("MNMAMT7", mnmamt7);
    }

//MixAndMatchCount7	MNMCNT7	INT UNSIGNED	N
    public Integer getMixAndMatchCount7() {
        return (Integer)getFieldValue("MNMCNT7");
    }

    public void setMixAndMatchCount7(Integer MixAndMatchCount7) {
        setFieldValue("MNMCNT7", MixAndMatchCount7);
    }

//NotIncludedSales7
    public HYIDouble getNotIncludedSales7() {
        return (HYIDouble)getFieldValue("RCPGIFAMT7");
    }

    public void setNotIncludedSales7(HYIDouble rcpgifamt7) {
        setFieldValue("RCPGIFAMT7", rcpgifamt7);
    }

//NetSalesAmount7
    public HYIDouble getNetSalesAmount7() {
        return (HYIDouble)getFieldValue("NETSALAMT7");
    }

    public void setNetSalesAmount7(HYIDouble netsalamt7) {
        setFieldValue("NETSALAMT7", netsalamt7);
    }
//8
//SIPercentPlusAmount8	SIPLUSAMT8	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount8() {
        return (HYIDouble)getFieldValue("SIPLUSAMT8");
    }

    public void setSIPercentPlusAmount8(HYIDouble SIPercentPlusAmount8) {
        setFieldValue("SIPLUSAMT8", SIPercentPlusAmount8);
    }

//SIPercentPlusCount8	SIPLUSCNT8	INT UNSIGNED	N
    public Integer getSIPercentPlusCount8() {
        return (Integer)getFieldValue("SIPLUSCNT8");
    }

    public void setSIPercentPlusCount8(Integer SIPercentPlusCount8) {
        setFieldValue("SIPLUSCNT8", SIPercentPlusCount8);
    }

//SIPercentOffAmount8	SIPAMT8	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount8() {
        return (HYIDouble)getFieldValue("SIPAMT8");
    }

    public void setSIPercentOffAmount8(HYIDouble SIPercentOffAmount8) {
        setFieldValue("SIPAMT8", SIPercentOffAmount8);
    }

//SIPercentOffCount8	SIPCNT8	INT UNSIGNED	N
    public Integer getSIPercentOffCount8() {
        return (Integer)getFieldValue("SIPCNT8");
    }

    public void setSIPercentOffCount8(Integer SIPercentOffCount8) {
        setFieldValue("SIPCNT8", SIPercentOffCount8);
    }

//SIDiscountAmount8	SIMAMT8	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount8() {
        return (HYIDouble)getFieldValue("SIMAMT8");
    }

    public void setSIDiscountAmount8(HYIDouble SIDiscountAmount8) {
        setFieldValue("SIMAMT8", SIDiscountAmount8);
    }

//SIDiscountCount8	SIMCNT8	INT UNSIGNED	N
    public Integer getSIDiscountCount8() {
        return (Integer)getFieldValue("SIMCNT8");
    }

    public void setSIDiscountCount8(Integer SIDiscountCount8) {
        setFieldValue("SIMCNT8", SIDiscountCount8);
    }

//MixAndMatchAmount8
    public HYIDouble getMixAndMatchAmount8() {
        return (HYIDouble)getFieldValue("MNMAMT8");
    }

    public void setMixAndMatchAmount8(HYIDouble mnmamt8) {
        setFieldValue("MNMAMT8", mnmamt8);
    }

//MixAndMatchCount8	MNMCNT8	INT UNSIGNED	N
    public Integer getMixAndMatchCount8() {
        return (Integer)getFieldValue("MNMCNT8");
    }

    public void setMixAndMatchCount8(Integer MixAndMatchCount8) {
        setFieldValue("MNMCNT8", MixAndMatchCount8);
    }

//NotIncludedSales8
    public HYIDouble getNotIncludedSales8() {
        return (HYIDouble)getFieldValue("RCPGIFAMT8");
    }

    public void setNotIncludedSales8(HYIDouble rcpgifamt8) {
        setFieldValue("RCPGIFAMT8", rcpgifamt8);
    }

//NetSalesAmount8
    public HYIDouble getNetSalesAmount8() {
        return (HYIDouble)getFieldValue("NETSALAMT8");
    }

    public void setNetSalesAmount8(HYIDouble netsalamt8) {
        setFieldValue("NETSALAMT8", netsalamt8);
    }
//9
//SIPercentPlusAmount9	SIPLUSAMT9	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount9() {
        return (HYIDouble)getFieldValue("SIPLUSAMT9");
    }

    public void setSIPercentPlusAmount9(HYIDouble SIPercentPlusAmount9) {
        setFieldValue("SIPLUSAMT9", SIPercentPlusAmount9);
    }

//SIPercentPlusCount9	SIPLUSCNT9	INT UNSIGNED	N
    public Integer getSIPercentPlusCount9() {
        return (Integer)getFieldValue("SIPLUSCNT9");
    }

    public void setSIPercentPlusCount9(Integer SIPercentPlusCount9) {
        setFieldValue("SIPLUSCNT9", SIPercentPlusCount9);
    }

//SIPercentOffAmount9	SIPAMT9	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount9() {
        return (HYIDouble)getFieldValue("SIPAMT9");
    }

    public void setSIPercentOffAmount9(HYIDouble SIPercentOffAmount9) {
        setFieldValue("SIPAMT9", SIPercentOffAmount9);
    }

//SIPercentOffCount9	SIPCNT9	INT UNSIGNED	N
    public Integer getSIPercentOffCount9() {
        return (Integer)getFieldValue("SIPCNT9");
    }

    public void setSIPercentOffCount9(Integer SIPercentOffCount9) {
        setFieldValue("SIPCNT9", SIPercentOffCount9);
    }

//SIDiscountAmount9	SIMAMT9	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount9() {
        return (HYIDouble)getFieldValue("SIMAMT9");
    }

    public void setSIDiscountAmount9(HYIDouble SIDiscountAmount9) {
        setFieldValue("SIMAMT9", SIDiscountAmount9);
    }

//SIDiscountCount9	SIMCNT9	INT UNSIGNED	N
    public Integer getSIDiscountCount9() {
        return (Integer)getFieldValue("SIMCNT9");
    }

    public void setSIDiscountCount9(Integer SIDiscountCount9) {
        setFieldValue("SIMCNT9", SIDiscountCount9);
    }

//MixAndMatchAmount9
    public HYIDouble getMixAndMatchAmount9() {
        return (HYIDouble)getFieldValue("MNMAMT9");
    }

    public void setMixAndMatchAmount9(HYIDouble mnmamt9) {
        setFieldValue("MNMAMT9", mnmamt9);
    }

//MixAndMatchCount9	MNMCNT9	INT UNSIGNED	N
    public Integer getMixAndMatchCount9() {
        return (Integer)getFieldValue("MNMCNT9");
    }

    public void setMixAndMatchCount9(Integer MixAndMatchCount9) {
        setFieldValue("MNMCNT9", MixAndMatchCount9);
    }

//NotIncludedSales9
    public HYIDouble getNotIncludedSales9() {
        return (HYIDouble)getFieldValue("RCPGIFAMT9");
    }

    public void setNotIncludedSales9(HYIDouble rcpgifamt9) {
        setFieldValue("RCPGIFAMT9", rcpgifamt9);
    }

//NetSalesAmount9
    public HYIDouble getNetSalesAmount9() {
        return (HYIDouble)getFieldValue("NETSALAMT9");
    }

    public void setNetSalesAmount9(HYIDouble netsalamt9) {
        setFieldValue("NETSALAMT9", netsalamt9);
    }
//10
//SIPercentPlusAmount10	SIPLUSAMT10	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount10() {
        return (HYIDouble)getFieldValue("SIPLUSAMT10");
    }

    public void setSIPercentPlusAmount10(HYIDouble SIPercentPlusAmount10) {
        setFieldValue("SIPLUSAMT10", SIPercentPlusAmount10);
    }

//SIPercentPlusCount10	SIPLUSCNT10	INT UNSIGNED	N
    public Integer getSIPercentPlusCount10() {
        return (Integer)getFieldValue("SIPLUSCNT10");
    }

    public void setSIPercentPlusCount10(Integer SIPercentPlusCount10) {
        setFieldValue("SIPLUSCNT10", SIPercentPlusCount10);
    }

//SIPercentOffAmount10	SIPAMT10	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount10() {
        return (HYIDouble)getFieldValue("SIPAMT10");
    }

    public void setSIPercentOffAmount10(HYIDouble SIPercentOffAmount10) {
        setFieldValue("SIPAMT10", SIPercentOffAmount10);
    }

//SIPercentOffCount10	SIPCNT10	INT UNSIGNED	N
    public Integer getSIPercentOffCount10() {
        return (Integer)getFieldValue("SIPCNT10");
    }

    public void setSIPercentOffCount10(Integer SIPercentOffCount10) {
        setFieldValue("SIPCNT10", SIPercentOffCount10);
    }

//SIDiscountAmount10	SIMAMT10	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount10() {
        return (HYIDouble)getFieldValue("SIMAMT10");
    }

    public void setSIDiscountAmount10(HYIDouble SIDiscountAmount10) {
        setFieldValue("SIMAMT10", SIDiscountAmount10);
    }

//SIDiscountCount10	SIMCNT10	INT UNSIGNED	N
    public Integer getSIDiscountCount10() {
        return (Integer)getFieldValue("SIMCNT10");
    }

    public void setSIDiscountCount10(Integer SIDiscountCount10) {
        setFieldValue("SIMCNT10", SIDiscountCount10);
    }

//MixAndMatchAmount10
    public HYIDouble getMixAndMatchAmount10() {
        return (HYIDouble)getFieldValue("MNMAMT10");
    }

    public void setMixAndMatchAmount10(HYIDouble mnmamt10) {
        setFieldValue("MNMAMT10", mnmamt10);
    }

//MixAndMatchCount10	MNMCNT10	INT UNSIGNED	N
    public Integer getMixAndMatchCount10() {
        return (Integer)getFieldValue("MNMCNT10");
    }

    public void setMixAndMatchCount10(Integer MixAndMatchCount10) {
        setFieldValue("MNMCNT10", MixAndMatchCount10);
    }

//NotIncludedSales10
    public HYIDouble getNotIncludedSales10() {
        return (HYIDouble)getFieldValue("RCPGIFAMT10");
    }

    public void setNotIncludedSales10(HYIDouble rcpgifamt10) {
        setFieldValue("RCPGIFAMT10", rcpgifamt10);
    }

//NetSalesAmount10
    public HYIDouble getNetSalesAmount10() {
        return (HYIDouble)getFieldValue("NETSALAMT10");
    }

    public void setNetSalesAmount10(HYIDouble netsalamt10) {
        setFieldValue("NETSALAMT10", netsalamt10);
    }

//礼卷销售

//VoucherAmount	VALPAMT	DECIMAL(12,2)	N
    public HYIDouble getVoucherAmount() {
        return (HYIDouble)getFieldValue("VALPAMT");
    }

    public void setVoucherAmount(HYIDouble VoucherAmount) {
        setFieldValue("VALPAMT", VoucherAmount);
    }

//VoucherCount	VALPCNT	INT UNSIGNED	N
    public Integer getVoucherCount() {
        return (Integer)getFieldValue("VALPCNT");
    }

    public void setVoucherCount(Integer VoucherCount) {
        setFieldValue("VALPCNT", VoucherCount);
    }

    //预收
    public HYIDouble getPreRcvAmount() {
        return (HYIDouble)getFieldValue("PRCVAMT");
    }

    public void setPreRcvAmount(HYIDouble amt) {
        setFieldValue("PRCVAMT", amt);
    }

    public Integer getPreRcvCount() {
        return (Integer)getFieldValue("PRCVCNT");
    }

    public void setPreRcvCount(Integer cnt) {
        setFieldValue("PRCVCNT", cnt);
    }

    //1
    public HYIDouble getPreRcvDetailAmount1() {
        return (HYIDouble)getFieldValue("PRCVPLUAMT1");
    }

    public void setPreRcvDetailAmount1(HYIDouble amt) {
        setFieldValue("PRCVPLUAMT1", amt);
    }

    public Integer getPreRcvDetailCount1() {
        return (Integer)getFieldValue("PRCVPLUCNT1");
    }

    public void setPreRcvDetailCount1(Integer cnt) {
        setFieldValue("PRCVPLUCNT1", cnt);
    }

    //2
    public HYIDouble getPreRcvDetailAmount2() {
        return (HYIDouble)getFieldValue("PRCVPLUAMT2");
    }

    public void setPreRcvDetailAmount2(HYIDouble amt) {
        setFieldValue("PRCVPLUAMT2", amt);
    }

    public Integer getPreRcvDetailCount2() {
        return (Integer)getFieldValue("PRCVPLUCNT2");
    }

    public void setPreRcvDetailCount2(Integer cnt) {
        setFieldValue("PRCVPLUCNT2", cnt);
    }

    //3
    public HYIDouble getPreRcvDetailAmount3() {
        return (HYIDouble)getFieldValue("PRCVPLUAMT3");
    }

    public void setPreRcvDetailAmount3(HYIDouble amt) {
        setFieldValue("PRCVPLUAMT3", amt);
    }

    public Integer getPreRcvDetailCount3() {
        return (Integer)getFieldValue("PRCVPLUCNT3");
    }

    public void setPreRcvDetailCount3(Integer cnt) {
        setFieldValue("PRCVPLUCNT3", cnt);
    }

    //4
    public HYIDouble getPreRcvDetailAmount4() {
        return (HYIDouble)getFieldValue("PRCVPLUAMT4");
    }

    public void setPreRcvDetailAmount4(HYIDouble amt) {
        setFieldValue("PRCVPLUAMT4", amt);
    }

    public Integer getPreRcvDetailCount4() {
        return (Integer)getFieldValue("PRCVPLUCNT4");
    }

    public void setPreRcvDetailCount4(Integer cnt) {
        setFieldValue("PRCVPLUCNT4", cnt);
    }

    //5
    public HYIDouble getPreRcvDetailAmount5() {
        return (HYIDouble)getFieldValue("PRCVPLUAMT5");
    }

    public void setPreRcvDetailAmount5(HYIDouble amt) {
        setFieldValue("PRCVPLUAMT5", amt);
    }

    public Integer getPreRcvDetailCount5() {
        return (Integer)getFieldValue("PRCVPLUCNT5");
    }

    public void setPreRcvDetailCount5(Integer cnt) {
        setFieldValue("PRCVPLUCNT5", cnt);
    }

//代售
    public HYIDouble getDaiShouAmount() {
        return (HYIDouble)getFieldValue("DAISHOUAMT");
    }

    public void setDaiShouAmount(HYIDouble daishouamt) {
        setFieldValue("DAISHOUAMT", daishouamt);
    }

    public Integer getDaiShouCount() {
        return (Integer)getFieldValue("DAISHOUCNT");
    }

    public void setDaiShouCount(Integer daishoucnt) {
        setFieldValue("DAISHOUCNT", daishoucnt);
    }

    //1
    public HYIDouble getDaiShouDetailAmount1() {
        return (HYIDouble)getFieldValue("DAISHOUPLUAMT1");
    }

    public void setDaiShouDetailAmount1(HYIDouble daishoupluamt) {
        setFieldValue("DAISHOUPLUAMT1", daishoupluamt);
    }

    public Integer getDaiShouDetailCount1() {
        return (Integer)getFieldValue("DAISHOUPLUCNT1");
    }

    public void setDaiShouDetailCount1(Integer daishouplucnt) {
        setFieldValue("DAISHOUPLUCNT1", daishouplucnt);
    }

    //2
    public HYIDouble getDaiShouDetailAmount2() {
        return (HYIDouble)getFieldValue("DAISHOUPLUAMT2");
    }

    public void setDaiShouDetailAmount2(HYIDouble daishoupluamt) {
        setFieldValue("DAISHOUPLUAMT2", daishoupluamt);
    }

    public Integer getDaiShouDetailCount2() {
        return (Integer)getFieldValue("DAISHOUPLUCNT2");
    }

    public void setDaiShouDetailCount2(Integer daishouplucnt) {
        setFieldValue("DAISHOUPLUCNT2", daishouplucnt);
    }

    //3
    public HYIDouble getDaiShouDetailAmount3() {
        return (HYIDouble)getFieldValue("DAISHOUPLUAMT3");
    }

    public void setDaiShouDetailAmount3(HYIDouble daishoupluamt) {
        setFieldValue("DAISHOUPLUAMT3", daishoupluamt);
    }

    public Integer getDaiShouDetailCount3() {
        return (Integer)getFieldValue("DAISHOUPLUCNT3");
    }

    public void setDaiShouDetailCount3(Integer daishouplucnt) {
        setFieldValue("DAISHOUPLUCNT3", daishouplucnt);
    }

    //4
    public HYIDouble getDaiShouDetailAmount4() {
        return (HYIDouble)getFieldValue("DAISHOUPLUAMT4");
    }

    public void setDaiShouDetailAmount4(HYIDouble daishoupluamt) {
        setFieldValue("DAISHOUPLUAMT4", daishoupluamt);
    }

    public Integer getDaiShouDetailCount4() {
        return (Integer)getFieldValue("DAISHOUPLUCNT4");
    }

    public void setDaiShouDetailCount4(Integer daishouplucnt) {
        setFieldValue("DAISHOUPLUCNT4", daishouplucnt);
    }

    //5
    public HYIDouble getDaiShouDetailAmount5() {
        return (HYIDouble)getFieldValue("DAISHOUPLUAMT5");
    }

    public void setDaiShouDetailAmount5(HYIDouble daishoupluamt) {
        setFieldValue("DAISHOUPLUAMT5", daishoupluamt);
    }

    public Integer getDaiShouDetailCount5() {
        return (Integer)getFieldValue("DAISHOUPLUCNT5");
    }

    public void setDaiShouDetailCount5(Integer daishouplucnt) {
        setFieldValue("DAISHOUPLUCNT5", daishouplucnt);
    }

//代收公共事业费
    public HYIDouble getDaiShouAmount2() {
        return (HYIDouble)getFieldValue("DAISHOUAMT2");
    }

    public void setDaiShouAmount2(HYIDouble daishouamt) {
        setFieldValue("DAISHOUAMT2", daishouamt);
    }

    public Integer getDaiShouCount2() {
        return (Integer)getFieldValue("DAISHOUCNT2");
    }

    public void setDaiShouCount2(Integer daishoucnt) {
        setFieldValue("DAISHOUCNT2", daishoucnt);
    }


//代付
    public HYIDouble getDaiFuAmount() {
        return (HYIDouble)getFieldValue("DAIFUAMT");
    }

    public void setDaiFuAmount(HYIDouble daifuamt) {
        setFieldValue("DAIFUAMT", daifuamt);
    }

    public Integer getDaiFuCount() {
        return (Integer)getFieldValue("DAIFUCNT");
    }

    public void setDaiFuCount(Integer daifucnt) {
        setFieldValue("DAIFUCNT", daifucnt);
    }

    //1
    public HYIDouble getDaiFuDetailAmount1() {
        return (HYIDouble)getFieldValue("DAIFUPLUAMT1");
    }

    public void setDaiFuDetailAmount1(HYIDouble daifupluamt) {
        setFieldValue("DAIFUPLUAMT1", daifupluamt);
    }

    public Integer getDaiFuDetailCount1() {
        return (Integer)getFieldValue("DAIFUPLUCNT1");
    }

    public void setDaiFuDetailCount1(Integer daifuplucnt) {
        setFieldValue("DAIFUPLUCNT1", daifuplucnt);
    }

    //2
    public HYIDouble getDaiFuDetailAmount2() {
        return (HYIDouble)getFieldValue("DAIFUPLUAMT2");
    }

    public void setDaiFuDetailAmount2(HYIDouble daifupluamt) {
        setFieldValue("DAIFUPLUAMT2", daifupluamt);
    }

    public Integer getDaiFuDetailCount2() {
        return (Integer)getFieldValue("DAIFUPLUCNT2");
    }

    public void setDaiFuDetailCount2(Integer daifuplucnt) {
        setFieldValue("DAIFUPLUCNT2", daifuplucnt);
    }

    //3
    public HYIDouble getDaiFuDetailAmount3() {
        return (HYIDouble)getFieldValue("DAIFUPLUAMT3");
    }

    public void setDaiFuDetailAmount3(HYIDouble daifupluamt) {
        setFieldValue("DAIFUPLUAMT3", daifupluamt);
    }

    public Integer getDaiFuDetailCount3() {
        return (Integer)getFieldValue("DAIFUPLUCNT3");
    }

    public void setDaiFuDetailCount3(Integer daifuplucnt) {
        setFieldValue("DAIFUPLUCNT3", daifuplucnt);
    }

    //4
    public HYIDouble getDaiFuDetailAmount4() {
        return (HYIDouble)getFieldValue("DAIFUPLUAMT4");
    }

    public void setDaiFuDetailAmount4(HYIDouble daifupluamt) {
        setFieldValue("DAIFUPLUAMT4", daifupluamt);
    }

    public Integer getDaiFuDetailCount4() {
        return (Integer)getFieldValue("DAIFUPLUCNT4");
    }

    public void setDaiFuDetailCount4(Integer daifuplucnt) {
        setFieldValue("DAIFUPLUCNT4", daifuplucnt);
    }

    //5
    public HYIDouble getDaiFuDetailAmount5() {
        return (HYIDouble)getFieldValue("DAIFUPLUAMT5");
    }

    public void setDaiFuDetailAmount5(HYIDouble daifupluamt) {
        setFieldValue("DAIFUPLUAMT5", daifupluamt);
    }

    public Integer getDaiFuDetailCount5() {
        return (Integer)getFieldValue("DAIFUPLUCNT5");
    }

    public void setDaiFuDetailCount5(Integer daifuplucnt) {
        setFieldValue("DAIFUPLUCNT5", daifuplucnt);
    }

    // PaidIn
//PaidinAmount
    public HYIDouble getPaidInAmount() {
        return (HYIDouble)getFieldValue("PINAMT");
    }

    public void setPaidInAmount(HYIDouble pinamt) {
        setFieldValue("PINAMT", pinamt);
    }

//PaidInCount
    public Integer getPaidInCount() {
        return (Integer)getFieldValue("PINCNT");
    }

    public void setPaidInCount(Integer pincnt) {
        setFieldValue("PINCNT", pincnt);
    }

//PaidInDetailAmount1
    public HYIDouble getPaidInDetailAmount1() {
        return (HYIDouble)getFieldValue("PINPLUAMT1");
    }

    public void setPaidInDetailAmount1(HYIDouble pinpluamt1) {
        setFieldValue("PINPLUAMT1", pinpluamt1);
    }

//PaidInDetailCount1
    public Integer getPaidInDetailCount1() {
        return (Integer)getFieldValue("PINPLUCNT1");
    }

    public void setPaidInDetailCount1(Integer pinplucnt1) {
        setFieldValue("PINPLUCNT1", pinplucnt1);
    }

//PaidInDetailAmount2
    public HYIDouble getPaidInDetailAmount2() {
        return (HYIDouble)getFieldValue("PINPLUAMT2");
    }

    public void setPaidInDetailAmount2(HYIDouble pinpluamt2) {
        setFieldValue("PINPLUAMT2", pinpluamt2);
    }

//PaidInDetailCount2
    public Integer getPaidInDetailCount2() {
        return (Integer)getFieldValue("PINPLUCNT2");
    }

    public void setPaidInDetailCount2(Integer pinplucnt2) {
        setFieldValue("PINPLUCNT2", pinplucnt2);
    }

//PaidInDetailAmount3
    public HYIDouble getPaidInDetailAmount3() {
        return (HYIDouble)getFieldValue("PINPLUAMT3");
    }

    public void setPaidInDetailAmount3(HYIDouble pinpluamt3) {
        setFieldValue("PINPLUAMT3", pinpluamt3);
    }

//PaidInDetailCount3
    public Integer getPaidInDetailCount3() {
        return (Integer)getFieldValue("PINPLUCNT3");
    }

    public void setPaidInDetailCount3(Integer pinplucnt3) {
        setFieldValue("PINPLUCNT3", pinplucnt3);
    }

//PaidInDetailAmount4
    public HYIDouble getPaidInDetailAmount4() {
        return (HYIDouble)getFieldValue("PINPLUAMT4");
    }

    public void setPaidInDetailAmount4(HYIDouble pinpluamt4) {
        setFieldValue("PINPLUAMT4", pinpluamt4);
    }

//PaidInDetailCount4
    public Integer getPaidInDetailCount4() {
        return (Integer)getFieldValue("PINPLUCNT4");
    }

    public void setPaidInDetailCount4(Integer pinplucnt4) {
        setFieldValue("PINPLUCNT4", pinplucnt4);
    }

//PaidInDetailAmount5
    public HYIDouble getPaidInDetailAmount5() {
        return (HYIDouble)getFieldValue("PINPLUAMT5");
    }

    public void setPaidInDetailAmount5(HYIDouble pinpluamt5) {
        setFieldValue("PINPLUAMT5", pinpluamt5);
    }

//PaidInDetailCount5
    public Integer getPaidInDetailCount5() {
        return (Integer)getFieldValue("PINPLUCNT5");
    }

    public void setPaidInDetailCount5(Integer pinplucnt5) {
        setFieldValue("PINPLUCNT5", pinplucnt5);
    }

// PaidOut

//PaidOutAmount
    public HYIDouble getPaidOutAmount() {
        return (HYIDouble)getFieldValue("POUTAMT");
    }

    public void setPaidOutAmount(HYIDouble poutamt) {
        setFieldValue("POUTAMT", poutamt);
    }

//PaidOutCount
    public Integer getPaidOutCount() {
        return (Integer)getFieldValue("POUTCNT");
    }

    public void setPaidOutCount(Integer poutcnt) {
        setFieldValue("POUTCNT", poutcnt);
    }

//PaidOutDetailAmount1
    public HYIDouble getPaidOutDetailAmount1() {
        return (HYIDouble)getFieldValue("POUTPLUAMT1");
    }

    public void setPaidOutDetailAmount1(HYIDouble poutpluamt1) {
        setFieldValue("POUTPLUAMT1", poutpluamt1);
    }

//PaidOutDetailCount1
    public Integer getPaidOutDetailCount1() {
        return (Integer)getFieldValue("POUTPLUCNT1");
    }

    public void setPaidOutDetailCount1(Integer poutplucnt1) {
        setFieldValue("POUTPLUCNT1", poutplucnt1);
    }

//PaidOutDetailAmount2
    public HYIDouble getPaidOutDetailAmount2() {
        return (HYIDouble)getFieldValue("POUTPLUAMT2");
    }

    public void setPaidOutDetailAmount2(HYIDouble poutpluamt2) {
        setFieldValue("POUTPLUAMT2", poutpluamt2);
    }

//PaidOutDetailCount2
    public Integer getPaidOutDetailCount2() {
        return (Integer)getFieldValue("POUTPLUCNT2");
    }

    public void setPaidOutDetailCount2(Integer poutplucnt2) {
        setFieldValue("POUTPLUCNT2", poutplucnt2);
    }

//PaidOutDetailAmount3
    public HYIDouble getPaidOutDetailAmount3() {
        return (HYIDouble)getFieldValue("POUTPLUAMT3");
    }

    public void setPaidOutDetailAmount3(HYIDouble poutpluamt3) {
        setFieldValue("POUTPLUAMT3", poutpluamt3);
    }

//PaidOutDetailCount3
    public Integer getPaidOutDetailCount3() {
        return (Integer)getFieldValue("POUTPLUCNT3");
    }

    public void setPaidOutDetailCount3(Integer poutplucnt3) {
        setFieldValue("POUTPLUCNT3", poutplucnt3);
    }

//PaidOutDetailAmount4
    public HYIDouble getPaidOutDetailAmount4() {
        return (HYIDouble)getFieldValue("POUTPLUAMT4");
    }

    public void setPaidOutDetailAmount4(HYIDouble poutpluamt4) {
        setFieldValue("POUTPLUAMT4", poutpluamt4);
    }

//PaidOutDetailCount4
    public Integer getPaidOutDetailCount4() {
        return (Integer)getFieldValue("POUTPLUCNT4");
    }

    public void setPaidOutDetailCount4(Integer poutplucnt4) {
        setFieldValue("POUTPLUCNT4", poutplucnt4);
    }

//PaidOutDetailAmount5
    public HYIDouble getPaidOutDetailAmount5() {
        return (HYIDouble)getFieldValue("POUTPLUAMT5");
    }

    public void setPaidOutDetailAmount5(HYIDouble poutpluamt5) {
        setFieldValue("POUTPLUAMT5", poutpluamt5);
    }

//PaidOutDetailCount5
    public Integer getPaidOutDetailCount5() {
        return (Integer)getFieldValue("POUTPLUCNT5");
    }

    public void setPaidOutDetailCount5(Integer poutplucnt5) {
        setFieldValue("POUTPLUCNT5", poutplucnt5);
    }

//操作明细

//ExchangeAmount
    public HYIDouble getExchangeAmount() {
        return (HYIDouble)getFieldValue("EXGAMT");
    }

    public void setExchangeAmount(HYIDouble exgamt) {
        setFieldValue("EXGAMT", exgamt);
    }

//ExchangeItemQuantity
    public HYIDouble getExchangeItemQuantity() {
        return (HYIDouble)getFieldValue("EXGITEMQTY");
    }

    public void setExchangeItemQuantity(HYIDouble exgitemqty) {
        setFieldValue("EXGITEMQTY", exgitemqty);
    }

//ReturnAmount	RTNAMT	DECIMAL(12,2)	N
    public HYIDouble getReturnAmount() {
        return (HYIDouble)getFieldValue("RTNAMT");
    }

    public void setReturnAmount(HYIDouble returnAmount) {
        setFieldValue("RTNAMT", returnAmount);
    }

//ReturnItemQuantity
    public HYIDouble getReturnItemQuantity() {
        return (HYIDouble)getFieldValue("RTNITEMQTY");
    }

    public void setReturnItemQuantity(HYIDouble rtnitemqty) {
        setFieldValue("RTNITEMQTY", rtnitemqty);
    }

//ReturnLineItemCount
    /*public HYIDouble getReturnLineItemCount() {
        return (HYIDouble)getFieldValue("RTNLINECNT");
    }

    public void setReturnLineItemCount(HYIDouble rtnlinecnt) {
        setFieldValue("RTNLINECNT", rtnlinecnt);
    }*/

//ReturnTransactionCount RTNTRANCNT INT UNSIGNED	N
    public Integer getReturnTransactionCount() {
        return (Integer)getFieldValue("RTNTRANCNT");
    }

    public void setReturnTransactionCount(Integer returnCount) {
        setFieldValue("RTNTRANCNT", returnCount);
    }

//TransactionVoidAmount	VOIDINVAMT	DECIMAL(12,2)	N
    public HYIDouble getTransactionVoidAmount() {
        return (HYIDouble)getFieldValue("VOIDINVAMT");
    }

    public void setTransactionVoidAmount(HYIDouble VoidAmount) {
        setFieldValue("VOIDINVAMT", VoidAmount);
    }


//TransactionVoidCount	VOIDINVCNT	INT UNSIGNED	N	发票作废次数
    public Integer getTransactionVoidCount() {
        return (Integer)getFieldValue("VOIDINVCNT");
    }

    public void setTransactionVoidCount(Integer VoidCount) {
        setFieldValue("VOIDINVCNT", VoidCount);
    }

//TransactionCancelAmount	CANCELAMT	DECIMAL(12,2)	N	交易取消金额
    public HYIDouble getTransactionCancelAmount() {
        return (HYIDouble)getFieldValue("CANCELAMT");
    }

    public void setTransactionCancelAmount(HYIDouble transactionCancelAmount) {
        setFieldValue("CANCELAMT", transactionCancelAmount);
    }

//TransactionCancelCount	CANCELCNT	INT UNSIGNED	N	交易取消次数
    public Integer getTransactionCancelCount() {
        return (Integer)getFieldValue("CANCELCNT");
    }

    public void setTransactionCancelCount(Integer transactionCancelCount) {
        setFieldValue("CANCELCNT", transactionCancelCount);
    }

//LineVoidAmount	UPDAMT	DECIMAL(12,2)	N
    public HYIDouble getLineVoidAmount() {
        return (HYIDouble)getFieldValue("UPDAMT");
    }

    public void setLineVoidAmount(HYIDouble LineVoidAmount) {
        setFieldValue("UPDAMT", LineVoidAmount);
    }

//LineVoidCount	UPDCNT	INT UNSIGNED	N
    public Integer getLineVoidCount() {
        return (Integer) getFieldValue("UPDCNT");
    }

    public void setLineVoidCount(Integer LineVoidCount) {
        setFieldValue("UPDCNT",LineVoidCount);
    }

//VoidAmount	NOWUPDAMT	DECIMAL(12,2)	N
    public HYIDouble getVoidAmount() {
        return (HYIDouble)getFieldValue("NOWUPDAMT");
    }

    public void setVoidAmount(HYIDouble VoidAmount) {
        setFieldValue("NOWUPDAMT", VoidAmount);
    }

//VoidCount	NOWUPDCNT	INT UNSIGNED	N
    public Integer getVoidCount() {
        return (Integer) getFieldValue("NOWUPDCNT");
    }

    public void setVoidCount(Integer VoidCount) {
        setFieldValue("NOWUPDCNT",VoidCount);
    }

//ManualEntryAmount
    public HYIDouble getManualEntryAmount() {
        return (HYIDouble)getFieldValue("ENTRYAMT");
    }

    public void setManualEntryAmount(HYIDouble entryamt) {
        setFieldValue("ENTRYAMT", entryamt);
    }

//ManualEntryCount
    public Integer getManualEntryCount() {
        return (Integer)getFieldValue("ENTRYCNT");
    }

    public void setManualEntryCount(Integer entrycnt) {
        setFieldValue("ENTRYCNT", entrycnt);
    }

//PriceOpenEntryAmount
    public HYIDouble getPriceOpenEntryAmount() {
        return (HYIDouble)getFieldValue("POENTRYAMT");
    }

    public void setPriceOpenEntryAmount(HYIDouble poentryamt) {
        setFieldValue("POENTRYAMT", poentryamt);
    }

//PriceOpenEntryCount
    public Integer getPriceOpenEntryCount() {
        return (Integer)getFieldValue("POENTRYCNT");
    }

    public void setPriceOpenEntryCount(Integer poentrycnt) {
        setFieldValue("POENTRYCNT", poentrycnt);
    }

//CashReturnAmount
    public HYIDouble getCashReturnAmount() {
        return (HYIDouble)getFieldValue("RTNCSHAMT");
    }

    public void setCashReturnAmount(HYIDouble rtncshamt) {
        setFieldValue("RTNCSHAMT", rtncshamt);
    }

//CashReturnCount
    public Integer getCashReturnCount() {
        return (Integer)getFieldValue("RTNCSHCNT");
    }

    public void setCashReturnCount(Integer rtncshcnt) {
        setFieldValue("RTNCSHCNT", rtncshcnt);
    }

//DrawerOpenCount	DROPENCNT	INT UNSIGNED	N
    public Integer getDrawerOpenCount() {
        return (Integer)getFieldValue("DROPENCNT");
    }

    public void setDrawerOpenCount(Integer DrawerOpenCount) {
        setFieldValue("DROPENCNT", DrawerOpenCount);
    }

//ReprintAmount
    public HYIDouble getReprintAmount() {
        return (HYIDouble)getFieldValue("REPRTAMT");
    }

    public void setReprintAmount(HYIDouble reprtamt) {
        setFieldValue("REPRTAMT", reprtamt);
    }

//ReprintCount	REPRTCNT	INT UNSIGNED	N	重印次数	③
    public Integer getReprintCount() {
        return (Integer)getFieldValue("REPRTCNT");
    }

    public void setReprintCount(Integer reprintCount) {
        setFieldValue("REPRTCNT", reprintCount);
    }

//CashInAmount
    public HYIDouble getCashInAmount() {
        return (HYIDouble)getFieldValue("CASHINAMT");
    }

    public void setCashInAmount(HYIDouble cashinamt) {
        setFieldValue("CASHINAMT", cashinamt);
    }

//CashInCount
    public Integer getCashInCount() {
        return (Integer)getFieldValue("CASHINCNT");
    }

    public void setCashInCount(Integer cashincnt) {
        setFieldValue("CASHINCNT", cashincnt);
    }

//CashOutAmount
    public HYIDouble getCashOutAmount() {
        return (HYIDouble)getFieldValue("CASHOUTAMT");
    }

    public void setCashOutAmount(HYIDouble cashoutamt) {
        setFieldValue("CASHOUTAMT", cashoutamt);
    }

//CashOutCount
    public Integer getCashOutCount() {
        return (Integer)getFieldValue("CASHOUTCNT");
    }

    public void setCashOutCount(Integer cashoutcnt) {
        setFieldValue("CASHOUTCNT", cashoutcnt);
    }

//支付明细项目

//SpillAmount	OVERAMT	DECIMAL(12,2)	N
   public HYIDouble getSpillAmount() {
       return (HYIDouble)getFieldValue("OVERAMT");
   }

   public void setSpillAmount(HYIDouble SpillAmount ) {
       setFieldValue("OVERAMT", SpillAmount);
   }

//SpillCount	OVERCNT	INT UNSIGNED	N
    public Integer getSpillCount() {
        return (Integer)getFieldValue("OVERCNT");
    }

    public void setSpillCount(Integer SpillCount) {
        setFieldValue("OVERCNT", SpillCount);
    }

//ExchangeRate1	RATE1	DECIMAL(8,2)	Y
    public HYIDouble getExchangeRate1() {
        return (HYIDouble) getFieldValue("RATE1");
    }

    public void setExchangeRate1(HYIDouble ExchangeRate1) {
        setFieldValue("RATE1", ExchangeRate1);
    }

//ExchangeRate2	RATE2	DECIMAL(8,2)	Y
    public HYIDouble getExchangeRate2() {
        return (HYIDouble) getFieldValue("RATE2");
    }

    public void setExchangeRate2(HYIDouble ExchangeRate2) {
        setFieldValue("RATE2", ExchangeRate2);
    }

//ExchangeRate3	RATE3	DECIMAL(8,2)	Y
    public HYIDouble getExchangeRate3() {
        return (HYIDouble) getFieldValue("RATE3");
    }

    public void setExchangeRate3(HYIDouble ExchangeRate3) {
        setFieldValue("RATE3", ExchangeRate3);
    }

//ExchangeRate4	RATE4	DECIMAL(8,2)	Y
    public HYIDouble getExchangeRate4() {
        return (HYIDouble) getFieldValue("RATE4");
    }

    public void setExchangeRate4(HYIDouble ExchangeRate4) {
        setFieldValue("RATE4", ExchangeRate4);
    }

//Pay00Amount	PAY00AMT	DECIMAL(12,2)	N
    public HYIDouble getPay00Amount() {
        return (HYIDouble)getFieldValue("PAY00AMT");
    }
    public void setPay00Amount(HYIDouble Pay00Amount) {
        setFieldValue("PAY00AMT", Pay00Amount);
    }

//Pay00Count	PAY00CNT	INT UNSIGNED	N
    public Integer getPay00Count() {
        return (Integer)getFieldValue("PAY00CNT");
    }
    public void setPay00Count(Integer Pay00Count) {
        setFieldValue("PAY00CNT", Pay00Count);
    }

//Pay01Amount	PAY01AMT	DECIMAL(12,2)	N
    public HYIDouble getPay01Amount() {
        return (HYIDouble)getFieldValue("PAY01AMT");
    }
    public void setPay01Amount(HYIDouble Pay01Amount) {
        setFieldValue("PAY01AMT", Pay01Amount);
    }
//Pay01Count	PAY01CNT	INT UNSIGNED	N
    public Integer getPay01Count() {
        return (Integer)getFieldValue("PAY01CNT");
    }
    public void setPay01Count(Integer Pay01Count) {
        setFieldValue("PAY01CNT", Pay01Count);
    }

//Pay02Amount	PAY02AMT	DECIMAL(12,2)	N
    public HYIDouble getPay02Amount() {
        return (HYIDouble)getFieldValue("PAY02AMT");
    }
    public void setPay02Amount(HYIDouble Pay02Amount) {
        setFieldValue("PAY02AMT", Pay02Amount);
    }
//Pay02Count	PAY02CNT	INT UNSIGNED	N
    public Integer getPay02Count() {
        return (Integer)getFieldValue("PAY02CNT");
    }
    public void setPay02Count(Integer Pay02Count) {
        setFieldValue("PAY02CNT", Pay02Count);
    }

//Pay03Amount	PAY03AMT	DECIMAL(12,2)	N
    public HYIDouble getPay03Amount() {
        return (HYIDouble)getFieldValue("PAY03AMT");
    }
    public void setPay03Amount(HYIDouble Pay03Amount) {
        setFieldValue("PAY03AMT", Pay03Amount);
    }
//Pay03Count	PAY03CNT	INT UNSIGNED	N
    public Integer getPay03Count() {
        return (Integer)getFieldValue("PAY03CNT");
    }
    public void setPay03Count(Integer Pay03Count) {
        setFieldValue("PAY03CNT", Pay03Count);
    }

//Pay04Amount	PAY04AMT	DECIMAL(12,2)	N
    public HYIDouble getPay04Amount() {
        return (HYIDouble)getFieldValue("PAY04AMT");
    }
    public void setPay04Amount(HYIDouble Pay04Amount) {
        setFieldValue("PAY04AMT", Pay04Amount);
    }
//Pay04Count	PAY04CNT	INT UNSIGNED	N
    public Integer getPay04Count() {
        return (Integer)getFieldValue("PAY04CNT");
    }
    public void setPay04Count(Integer Pay04Count) {
        setFieldValue("PAY04CNT", Pay04Count);
    }

//Pay05Amount	PAY05AMT	DECIMAL(12,2)	N
    public HYIDouble getPay05Amount() {
        return (HYIDouble)getFieldValue("PAY05AMT");
    }
    public void setPay05Amount(HYIDouble Pay05Amount) {
        setFieldValue("PAY05AMT", Pay05Amount);
    }
//Pay05Count	PAY05CNT	INT UNSIGNED	N
    public Integer getPay05Count() {
        return (Integer)getFieldValue("PAY05CNT");
    }
    public void setPay05Count(Integer Pay05Count) {
        setFieldValue("PAY05CNT", Pay05Count);
    }

//Pay06Amount	PAY06AMT	DECIMAL(12,2)	N
    public HYIDouble getPay06Amount() {
        return (HYIDouble)getFieldValue("PAY06AMT");
    }
    public void setPay06Amount(HYIDouble Pay06Amount) {
        setFieldValue("PAY06AMT", Pay06Amount);
    }
//Pay06Count	PAY06CNT	INT UNSIGNED	N
    public Integer getPay06Count() {
        return (Integer)getFieldValue("PAY06CNT");
    }
    public void setPay06Count(Integer Pay06Count) {
        setFieldValue("PAY06CNT", Pay06Count);
    }

//Pay07Amount	PAY07AMT	DECIMAL(12,2)	N
    public HYIDouble getPay07Amount() {
        return (HYIDouble)getFieldValue("PAY07AMT");
    }
    public void setPay07Amount(HYIDouble Pay07Amount) {
        setFieldValue("PAY07AMT", Pay07Amount);
    }
//Pay07Count	PAY07CNT	INT UNSIGNED	N
    public Integer getPay07Count() {
        return (Integer)getFieldValue("PAY07CNT");
    }
    public void setPay07Count(Integer Pay07Count) {
        setFieldValue("PAY07CNT", Pay07Count);
    }

//Pay08Amount	PAY08AMT	DECIMAL(12,2)	N
    public HYIDouble getPay08Amount() {
        return (HYIDouble)getFieldValue("PAY08AMT");
    }
    public void setPay08Amount(HYIDouble Pay08Amount) {
        setFieldValue("PAY08AMT", Pay08Amount);
    }
//Pay08Count	PAY08CNT	INT UNSIGNED	N
    public Integer getPay08Count() {
        return (Integer)getFieldValue("PAY08CNT");
    }
    public void setPay08Count(Integer Pay08Count) {
        setFieldValue("PAY08CNT", Pay08Count);
    }

//Pay09Amount	PAY09AMT	DECIMAL(12,2)	N
    public HYIDouble getPay09Amount() {
        return (HYIDouble)getFieldValue("PAY09AMT");
    }
    public void setPay09Amount(HYIDouble Pay09Amount) {
        setFieldValue("PAY09AMT", Pay09Amount);
    }
//Pay09Count	PAY09CNT	INT UNSIGNED	N
    public Integer getPay09Count() {
        return (Integer)getFieldValue("PAY09CNT");
    }
    public void setPay09Count(Integer Pay09Count) {
        setFieldValue("PAY09CNT", Pay09Count);
    }

//Pay10Amount	PAY10AMT	DECIMAL(12,2)	N
    public HYIDouble getPay10Amount() {
        return (HYIDouble)getFieldValue("PAY10AMT");
    }
    public void setPay10Amount(HYIDouble Pay10Amount) {
        setFieldValue("PAY10AMT", Pay10Amount);
    }
//Pay10Count	PAY10CNT	INT UNSIGNED	N
    public Integer getPay10Count() {
        return (Integer)getFieldValue("PAY10CNT");
    }
    public void setPay10Count(Integer Pay10Count) {
        setFieldValue("PAY10CNT", Pay10Count);
    }

//Pay11Amount	PAY11AMT	DECIMAL(12,2)	N
    public HYIDouble getPay11Amount() {
        return (HYIDouble)getFieldValue("PAY11AMT");
    }
    public void setPay11Amount(HYIDouble Pay11Amount) {
        setFieldValue("PAY11AMT", Pay11Amount);
    }
//Pay11Count	PAY11CNT	INT UNSIGNED	N
    public Integer getPay11Count() {
        return (Integer)getFieldValue("PAY11CNT");
    }
    public void setPay11Count(Integer Pay11Count) {
        setFieldValue("PAY11CNT", Pay11Count);
    }

//Pay12Amount	PAY12AMT	DECIMAL(12,2)	N
    public HYIDouble getPay12Amount() {
        return (HYIDouble)getFieldValue("PAY12AMT");
    }
    public void setPay12Amount(HYIDouble Pay12Amount) {
        setFieldValue("PAY12AMT", Pay12Amount);
    }
//Pay12Count	PAY12CNT	INT UNSIGNED	N
    public Integer getPay12Count() {
        return (Integer)getFieldValue("PAY12CNT");
    }
    public void setPay12Count(Integer Pay12Count) {
        setFieldValue("PAY12CNT", Pay12Count);
    }

//Pay13Amount	PAY13AMT	DECIMAL(12,2)	N
    public HYIDouble getPay13Amount() {
        return (HYIDouble)getFieldValue("PAY13AMT");
    }
    public void setPay13Amount(HYIDouble Pay13Amount) {
        setFieldValue("PAY13AMT", Pay13Amount);
    }
//Pay13Count	PAY13CNT	INT UNSIGNED	N
    public Integer getPay13Count() {
        return (Integer)getFieldValue("PAY13CNT");
    }
    public void setPay13Count(Integer Pay13Count) {
        setFieldValue("PAY13CNT", Pay13Count);
    }

//Pay14Amount	PAY14AMT	DECIMAL(12,2)	N
    public HYIDouble getPay14Amount() {
        return (HYIDouble)getFieldValue("PAY14AMT");
    }
    public void setPay14Amount(HYIDouble Pay14Amount) {
        setFieldValue("PAY14AMT", Pay14Amount);
    }
//Pay14Count	PAY14CNT	INT UNSIGNED	N
    public Integer getPay14Count() {
        return (Integer)getFieldValue("PAY14CNT");
    }
    public void setPay14Count(Integer Pay14Count) {
        setFieldValue("PAY14CNT", Pay14Count);
    }

//Pay15Amount	PAY15AMT	DECIMAL(12,2)	N
    public HYIDouble getPay15Amount() {
        return (HYIDouble)getFieldValue("PAY15AMT");
    }
    public void setPay15Amount(HYIDouble Pay15Amount) {
        setFieldValue("PAY15AMT", Pay15Amount);
    }
//Pay15Count	PAY15CNT	INT UNSIGNED	N
    public Integer getPay15Count() {
        return (Integer)getFieldValue("PAY15CNT");
    }
    public void setPay15Count(Integer Pay15Count) {
        setFieldValue("PAY15CNT", Pay15Count);
    }

//Pay16Amount	PAY16AMT	DECIMAL(12,2)	N
    public HYIDouble getPay16Amount() {
        return (HYIDouble)getFieldValue("PAY16AMT");
    }
    public void setPay16Amount(HYIDouble Pay16Amount) {
        setFieldValue("PAY16AMT", Pay16Amount);
    }
//Pay16Count	PAY16CNT	INT UNSIGNED	N
    public Integer getPay16Count() {
        return (Integer)getFieldValue("PAY16CNT");
    }
    public void setPay16Count(Integer Pay16Count) {
        setFieldValue("PAY16CNT", Pay16Count);
    }

//Pay17Amount	PAY17AMT	DECIMAL(12,2)	N
    public HYIDouble getPay17Amount() {
        return (HYIDouble)getFieldValue("PAY17AMT");
    }
    public void setPay17Amount(HYIDouble Pay17Amount) {
        setFieldValue("PAY17AMT", Pay17Amount);
    }
//Pay17Count	PAY17CNT	INT UNSIGNED	N
    public Integer getPay17Count() {
        return (Integer)getFieldValue("PAY17CNT");
    }
    public void setPay17Count(Integer Pay17Count) {
        setFieldValue("PAY17CNT", Pay17Count);
    }

//Pay18Amount	PAY18AMT	DECIMAL(12,2)	N
    public HYIDouble getPay18Amount() {
        return (HYIDouble)getFieldValue("PAY18AMT");
    }
    public void setPay18Amount(HYIDouble Pay18Amount) {
        setFieldValue("PAY18AMT", Pay18Amount);
    }
//Pay18Count	PAY18CNT	INT UNSIGNED	N
    public Integer getPay18Count() {
        return (Integer)getFieldValue("PAY18CNT");
    }
    public void setPay18Count(Integer Pay18Count) {
        setFieldValue("PAY18CNT", Pay18Count);
    }

//Pay19Amount	PAY19AMT	DECIMAL(12,2)	N
    public HYIDouble getPay19Amount() {
        return (HYIDouble)getFieldValue("PAY19AMT");
    }
    public void setPay19Amount(HYIDouble Pay19Amount) {
        setFieldValue("PAY19AMT", Pay19Amount);
    }
//Pay19Count	PAY19CNT	INT UNSIGNED	N
    public Integer getPay19Count() {
        return (Integer)getFieldValue("PAY19CNT");
    }
    public void setPay19Count(Integer Pay19Count) {
        setFieldValue("PAY19CNT", Pay19Count);
    }

//Pay20Amount	PAY20AMT	DECIMAL(12,2)	N
    public HYIDouble getPay20Amount() {
        return (HYIDouble)getFieldValue("PAY20AMT");
    }
    public void setPay20Amount(HYIDouble Pay20Amount) {
        setFieldValue("PAY20AMT", Pay20Amount);
    }
//Pay20Count	PAY20CNT	INT UNSIGNED	N
    public Integer getPay20Count() {
        return (Integer)getFieldValue("PAY20CNT");
    }
    public void setPay20Count(Integer Pay20Count) {
        setFieldValue("PAY20CNT", Pay20Count);
    }

//Pay21Amount	PAY21AMT	DECIMAL(12,2)	N
    public HYIDouble getPay21Amount() {
        return (HYIDouble)getFieldValue("PAY21AMT");
    }
    public void setPay21Amount(HYIDouble Pay21Amount) {
        setFieldValue("PAY21AMT", Pay21Amount);
    }
//Pay21Count	PAY21CNT	INT UNSIGNED	N
    public Integer getPay21Count() {
        return (Integer)getFieldValue("PAY21CNT");
    }
    public void setPay21Count(Integer Pay21Count) {
        setFieldValue("PAY21CNT", Pay21Count);
    }

//Pay22Amount	PAY22AMT	DECIMAL(12,2)	N
    public HYIDouble getPay22Amount() {
        return (HYIDouble)getFieldValue("PAY22AMT");
    }
    public void setPay22Amount(HYIDouble Pay22Amount) {
        setFieldValue("PAY22AMT", Pay22Amount);
    }
//Pay22Count	PAY22CNT	INT UNSIGNED	N
    public Integer getPay22Count() {
        return (Integer)getFieldValue("PAY22CNT");
    }
    public void setPay22Count(Integer Pay22Count) {
        setFieldValue("PAY22CNT", Pay22Count);
    }

//Pay23Amount	PAY23AMT	DECIMAL(12,2)	N
    public HYIDouble getPay23Amount() {
        return (HYIDouble)getFieldValue("PAY23AMT");
    }
    public void setPay23Amount(HYIDouble Pay23Amount) {
        setFieldValue("PAY23AMT", Pay23Amount);
    }
//Pay23Count	PAY23CNT	INT UNSIGNED	N
    public Integer getPay23Count() {
        return (Integer)getFieldValue("PAY23CNT");
    }
    public void setPay23Count(Integer Pay23Count) {
        setFieldValue("PAY23CNT", Pay23Count);
    }

//Pay24Amount	PAY24AMT	DECIMAL(12,2)	N
    public HYIDouble getPay24Amount() {
        return (HYIDouble)getFieldValue("PAY24AMT");
    }
    public void setPay24Amount(HYIDouble Pay24Amount) {
        setFieldValue("PAY24AMT", Pay24Amount);
    }
//Pay24Count	PAY24CNT	INT UNSIGNED	N
    public Integer getPay24Count() {
        return (Integer)getFieldValue("PAY24CNT");
    }
    public void setPay24Count(Integer Pay24Count) {
        setFieldValue("PAY24CNT", Pay24Count);
    }

//Pay25Amount	PAY25AMT	DECIMAL(12,2)	N
    public HYIDouble getPay25Amount() {
        return (HYIDouble)getFieldValue("PAY25AMT");
    }
    public void setPay25Amount(HYIDouble Pay25Amount) {
        setFieldValue("PAY25AMT", Pay25Amount);
    }
//Pay25Count	PAY25CNT	INT UNSIGNED	N
    public Integer getPay25Count() {
        return (Integer)getFieldValue("PAY25CNT");
    }
    public void setPay25Count(Integer Pay25Count) {
        setFieldValue("PAY25CNT", Pay25Count);
    }

//Pay26Amount	PAY26AMT	DECIMAL(12,2)	N
    public HYIDouble getPay26Amount() {
        return (HYIDouble)getFieldValue("PAY26AMT");
    }
    public void setPay26Amount(HYIDouble Pay26Amount) {
        setFieldValue("PAY26AMT", Pay26Amount);
    }
//Pay26Count	PAY26CNT	INT UNSIGNED	N
    public Integer getPay26Count() {
        return (Integer)getFieldValue("PAY26CNT");
    }
    public void setPay26Count(Integer Pay26Count) {
        setFieldValue("PAY26CNT", Pay26Count);
    }

//Pay27Amount	PAY27AMT	DECIMAL(12,2)	N
    public HYIDouble getPay27Amount() {
        return (HYIDouble)getFieldValue("PAY27AMT");
    }
    public void setPay27Amount(HYIDouble Pay27Amount) {
        setFieldValue("PAY27AMT", Pay27Amount);
    }
//Pay27Count	PAY27CNT	INT UNSIGNED	N
    public Integer getPay27Count() {
        return (Integer)getFieldValue("PAY27CNT");
    }
    public void setPay27Count(Integer Pay27Count) {
        setFieldValue("PAY27CNT", Pay27Count);
    }

//Pay28Amount	PAY28AMT	DECIMAL(12,2)	N
    public HYIDouble getPay28Amount() {
        return (HYIDouble)getFieldValue("PAY28AMT");
    }
    public void setPay28Amount(HYIDouble Pay28Amount) {
        setFieldValue("PAY28AMT", Pay28Amount);
    }
//Pay28Count	PAY28CNT	INT UNSIGNED	N
    public Integer getPay28Count() {
        return (Integer)getFieldValue("PAY28CNT");
    }
    public void setPay28Count(Integer Pay28Count) {
        setFieldValue("PAY28CNT", Pay28Count);
    }

//Pay29Amount	PAY29AMT	DECIMAL(12,2)	N
    public HYIDouble getPay29Amount() {
        return (HYIDouble)getFieldValue("PAY29AMT");
    }
    public void setPay29Amount(HYIDouble Pay29Amount) {
        setFieldValue("PAY29AMT", Pay29Amount);
    }
//Pay29Count	PAY29CNT	INT UNSIGNED	N
    public Integer getPay29Count() {
        return (Integer)getFieldValue("PAY29CNT");
    }
    public void setPay29Count(Integer Pay29Count) {
        setFieldValue("PAY29CNT", Pay29Count);
    }

    //******************************************************************************
    //Payment Category related properties | start
    //******************************************************************************
    public HYIDouble getPayAmountByID (String id) {
        String innerID = null;
        if (id.length()==1 && Integer.parseInt(id)<10)
            innerID = "0" + id;
        else
            innerID = id;
        return (HYIDouble)getFieldValue("PAY" + innerID + "AMT");
    }

    public void setPayAmountByID (String id, HYIDouble payment) {
        String innerID = null;
        if (id.length()==1 && Integer.parseInt(id)<10)
            innerID = "0" + id;
        else
            innerID = id;
        setFieldValue("PAY" + innerID + "AMT", payment);
    }

    public Integer getPayCountByID(String id) {
        String innerID = null;
        if (id.length()==1 && Integer.parseInt(id)<10)
            innerID = "0" + id;
        else
            innerID = id;
        return (Integer)getFieldValue("PAY" + innerID + "CNT");
    }

    public void setPayCountByID(String id, Integer paycount) {
        String innerID = null;
        if (id.length()==1 && Integer.parseInt(id)<10)
            innerID = "0" + id;
        else
            innerID = id;
        setFieldValue("PAY" + innerID + "CNT", paycount);
    }

    public Integer getHoldtrancount() {
        return (Integer)getFieldValue("holdtrancount");
    }
    
    public void setHoldtrancount(Integer holdtrancount) {
        setFieldValue("holdtrancount", holdtrancount);
    }


    //------------------------------------------------------------------------------
    //Payment Category related properties | end
    //------------------------------------------------------------------------------

    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_shift";
        else
            return "shift";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_shift";
        else
            return "shift";
    }


	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as String[][] 
	 */
	public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
            {"STORENO", "StoreID"},
            {"POSNO", "PosNumber"},
            {"EODCNT", "ZSequenceNumber"},
            {"SHIFTCNT", "ShiftSequenceNumber"},
            {"ACCDATE", "AccountDate"},
            {"SGNONSEQ", "SignOnTransactionNumber"},
            {"SGNOFFSEQ", "SignOffTransactionNumber"},
            {"SGNONINV", "SignOnInvoiceNumber"},
            {"SGNOFFINV", "SignOffInvoiceNumber"},
            {"SGNONDTTM", "SignOnSystemDateTime"},
            {"SGNOFDTTM", "SignOffSystemDateTime"},
            {"MACHINENO", "MachineNumber"},
            {"CASHIER", "CashierID"},
            {"TCPFLG", "UploadState"},
            // 交易明细=交易��
            {"TRANCNT", "TransactionCount"},
            {"CUSTCNT", "CustomerCount"},
            {"ITEMSALEQTY", "ItemSaleQuantity"},
            {"GrossSales", "GrossSaleTotalAmount"},
            {"GROSALTX0AM", "GrossSaleTax0Amount"},
            {"GROSALTX1AM", "GrossSaleTax1Amount"},
            {"GROSALTX2AM", "GrossSaleTax2Amount"},
            {"GROSALTX3AM", "GrossSaleTax3Amount"},
            {"GROSALTX4AM", "GrossSaleTax4Amount"},
            {"GROSALTX5AM", "GrossSaleTax5Amount"},
            {"GROSALTX6AM", "GrossSaleTax6Amount"},
            {"GROSALTX7AM", "GrossSaleTax7Amount"},
            {"GROSALTX8AM", "GrossSaleTax8Amount"},
            {"GROSALTX9AM", "GrossSaleTax9Amount"},
            {"GROSALTX10AM", "GrossSaleTax10Amount"},
            {"SIPercentPlusTotalAmount", "SIPlusTotalAmount"},
            {"SIPLUSAMT0", "SIPlusTax0Amount"},
            {"SIPLUSAMT1", "SIPlusTax1Amount"},
            {"SIPLUSAMT2", "SIPlusTax2Amount"},
            {"SIPLUSAMT3", "SIPlusTax3Amount"},
            {"SIPLUSAMT4", "SIPlusTax4Amount"},
            {"SIPLUSAMT5", "SIPlusTax5Amount"},
            {"SIPLUSAMT6", "SIPlusTax6Amount"},
            {"SIPLUSAMT7", "SIPlusTax7Amount"},
            {"SIPLUSAMT8", "SIPlusTax8Amount"},
            {"SIPLUSAMT9", "SIPlusTax9Amount"},
            {"SIPLUSAMT10", "SIPlusTax10Amount"},
            {"SIPercentPlusTotalCount", "SIPlusTotalCount"},
            {"SIPLUSCNT0", "SIPlusTax0Count"},
            {"SIPLUSCNT1", "SIPlusTax1Count"},
            {"SIPLUSCNT2", "SIPlusTax2Count"},
            {"SIPLUSCNT3", "SIPlusTax3Count"},
            {"SIPLUSCNT4", "SIPlusTax4Count"},
            {"SIPLUSCNT5", "SIPlusTax5Count"},
            {"SIPLUSCNT6", "SIPlusTax6Count"},
            {"SIPLUSCNT7", "SIPlusTax7Count"},
            {"SIPLUSCNT8", "SIPlusTax8Count"},
            {"SIPLUSCNT9", "SIPlusTax9Count"},
            {"SIPLUSCNT10", "SIPlusTax10Count"},
            {"SIPercentOffTotalAmount", "DiscountTotalAmount"},
            {"SIPAMT0", "DiscountTax0Amount"},
            {"SIPAMT1", "DiscountTax1Amount"},
            {"SIPAMT2", "DiscountTax2Amount"},
            {"SIPAMT3", "DiscountTax3Amount"},
            {"SIPAMT4", "DiscountTax4Amount"},
            {"SIPAMT5", "DiscountTax5Amount"},
            {"SIPAMT6", "DiscountTax6Amount"},
            {"SIPAMT7", "DiscountTax7Amount"},
            {"SIPAMT8", "DiscountTax8Amount"},
            {"SIPAMT9", "DiscountTax9Amount"},
            {"SIPAMT10", "DiscountTax10Amount"},
            {"SIPercentOffTotalCount", "DiscountTotalCount"},
            {"SIPCNT0", "DiscountTax0Count"},
            {"SIPCNT1", "DiscountTax1Count"},
            {"SIPCNT2", "DiscountTax2Count"},
            {"SIPCNT3", "DiscountTax3Count"},
            {"SIPCNT4", "DiscountTax4Count"},
            {"SIPCNT5", "DiscountTax5Count"},
            {"SIPCNT6", "DiscountTax6Count"},
            {"SIPCNT7", "DiscountTax7Count"},
            {"SIPCNT8", "DiscountTax8Count"},
            {"SIPCNT9", "DiscountTax9Count"},
            {"SIPCNT10", "DiscountTax10Count"},
            {"MixAndMatchTotalAmount", "MixAndMatchTotalAmount"},
            {"MNMAMT0", "MixAndMatchTax0Amount"},
            {"MNMAMT1", "MixAndMatchTax1Amount"},
            {"MNMAMT2", "MixAndMatchTax2Amount"},
            {"MNMAMT3", "MixAndMatchTax3Amount"},
            {"MNMAMT4", "MixAndMatchTax4Amount"},
            {"MNMAMT5", "MixAndMatchTax5Amount"},
            {"MNMAMT6", "MixAndMatchTax6Amount"},
            {"MNMAMT7", "MixAndMatchTax7Amount"},
            {"MNMAMT8", "MixAndMatchTax8Amount"},
            {"MNMAMT9", "MixAndMatchTax9Amount"},
            {"MNMAMT10", "MixAndMatchTax10Amount"},
            {"MixAndMatchTotalCount", "MixAndMatchTotalCount"},
            {"MNMCNT0", "MixAndMatchTax0Count"},
            {"MNMCNT1", "MixAndMatchTax1Count"},
            {"MNMCNT2", "MixAndMatchTax2Count"},
            {"MNMCNT3", "MixAndMatchTax3Count"},
            {"MNMCNT4", "MixAndMatchTax4Count"},
            {"MNMCNT5", "MixAndMatchTax5Count"},
            {"MNMCNT6", "MixAndMatchTax6Count"},
            {"MNMCNT7", "MixAndMatchTax7Count"},
            {"MNMCNT8", "MixAndMatchTax8Count"},
            {"MNMCNT9", "MixAndMatchTax9Count"},
            {"MNMCNT10", "MixAndMatchTax10Count"},
            {"NotIncludedTotalSales", "NotIncludedTotalSale"},
            {"RCPGIFAMT0", "NotIncludedTax0Sale"},
            {"RCPGIFAMT1", "NotIncludedTax1Sale"},
            {"RCPGIFAMT2", "NotIncludedTax2Sale"},
            {"RCPGIFAMT3", "NotIncludedTax3Sale"},
            {"RCPGIFAMT4", "NotIncludedTax4Sale"},
            {"RCPGIFAMT5", "NotIncludedTax5Sale"},
            {"RCPGIFAMT6", "NotIncludedTax6Sale"},
            {"RCPGIFAMT7", "NotIncludedTax7Sale"},
            {"RCPGIFAMT8", "NotIncludedTax8Sale"},
            {"RCPGIFAMT9", "NotIncludedTax9Sale"},
            {"RCPGIFAMT10", "NotIncludedTax10Sale"},
            {"NetSalesTotalAmount", "NetSaleTotalAmount"},
            {"NETSALAMT0", "NetSaleTax0Amount"},
            {"NETSALAMT1", "NetSaleTax1Amount"},
            {"NETSALAMT2", "NetSaleTax2Amount"},
            {"NETSALAMT3", "NetSaleTax3Amount"},
            {"NETSALAMT4", "NetSaleTax4Amount"},
            {"NETSALAMT5", "NetSaleTax5Amount"},
            {"NETSALAMT6", "NetSaleTax6Amount"},
            {"NETSALAMT7", "NetSaleTax7Amount"},
            {"NETSALAMT8", "NetSaleTax8Amount"},
            {"NETSALAMT9", "NetSaleTax9Amount"},
            {"NETSALAMT10", "NetSaleTax10Amount"},
            {"TAXAMT0", "TaxAmount0"},
            {"TAXAMT1", "TaxAmount1"},
            {"TAXAMT2", "TaxAmount2"},
            {"TAXAMT3", "TaxAmount3"},
            {"TAXAMT4", "TaxAmount4"},
            {"TAXAMT5", "TaxAmount5"},
            {"TAXAMT6", "TaxAmount6"},
            {"TAXAMT7", "TaxAmount7"},
            {"TAXAMT8", "TaxAmount8"},
            {"TAXAMT9", "TaxAmount9"},
            {"TAXAMT10", "TaxAmount10"},
            {"TotalTaxAmount", "taxTotalAmount"},       //Bruce/20030328
            // 礼卷销售=�券�售��
            {"VALPAMT", "TotalVoucherAmount"},
            {"VALPCNT", "TotalVoucherCount"},
            // 预收=�收��
            {"PRCVAMT", "PreRcvAmount"},
            {"PRCVCNT", "PreRcvCount"},
            {"PRCVPLUAMT1", "PreRcvDetailAmount1"},
            {"PRCVPLUCNT1", "PreRcvDetailCount1"},
            {"PRCVPLUAMT2", "PreRcvDetailAmount2"},
            {"PRCVPLUCNT2", "PreRcvDetailCount2"},
            {"PRCVPLUAMT3", "PreRcvDetailAmount3"},
            {"PRCVPLUCNT3", "PreRcvDetailCount3"},
            {"PRCVPLUAMT4", "PreRcvDetailAmount4"},
            {"PRCVPLUCNT4", "PreRcvDetailCount4"},
            {"PRCVPLUAMT5", "PreRcvDetailAmount5"},
            {"PRCVPLUCNT5", "PreRcvDetailCount5"},
            // 代售=代售��
            {"DAISHOUAMT", "TotalDaiShouAmount"},
            {"DAISHOUCNT", "TotalDaiShouCount"},
            {"DAISHOUPLUAMT1", "DaiShouDetailAmount1"},
            {"DAISHOUPLUCNT1", "DaiShouDetailCount1"},
            {"DAISHOUPLUAMT2", "DaiShouDetailAmount2"},
            {"DAISHOUPLUCNT2", "DaiShouDetailCount2"},
            {"DAISHOUPLUAMT3", "DaiShouDetailAmount3"},
            {"DAISHOUPLUCNT3", "DaiShouDetailCount3"},
            {"DAISHOUPLUAMT4", "DaiShouDetailAmount4"},
            {"DAISHOUPLUCNT4", "DaiShouDetailCount4"},
            {"DAISHOUPLUAMT5", "DaiShouDetailAmount5"},
            {"DAISHOUPLUCNT5", "DaiShouDetailCount5"},
            //代收
            {"DAISHOUAMT2", "TotalDaiShouAmount2"},
            {"DAISHOUCNT2", "TotalDaiShouCount2"},
            // 代付=代付��
            {"DAIFUAMT", "TotalDaiFuAmount"},
            {"DAIFUCNT", "TotalDaiFuCount"},
            {"DAIFUPLUAMT1", "DaiFuDetailAmount1"},
            {"DAIFUPLUCNT1", "DaiFuDetailCount1"},
            {"DAIFUPLUAMT2", "DaiFuDetailAmount2"},
            {"DAIFUPLUCNT2", "DaiFuDetailCount2"},
            {"DAIFUPLUAMT3", "DaiFuDetailAmount3"},
            {"DAIFUPLUCNT3", "DaiFuDetailCount3"},
            {"DAIFUPLUAMT4", "DaiFuDetailAmount4"},
            {"DAIFUPLUCNT4", "DaiFuDetailCount4"},
            {"DAIFUPLUAMT5", "DaiFuDetailAmount5"},
            {"DAIFUPLUCNT5", "DaiFuDetailCount5"},
            // Paid-In=PaidIn��
            {"PINAMT", "TotalPaidinAmount"},
            {"PINCNT", "TotalPaidinCount"},
            {"PINPLUAMT1", "PaidInDetailAmount1"},
            {"PINPLUCNT1", "PaidInDetailCount1"},
            {"PINPLUAMT2", "PaidInDetailAmount2"},
            {"PINPLUCNT2", "PaidInDetailCount2"},
            {"PINPLUAMT3", "PaidInDetailAmount3"},
            {"PINPLUCNT3", "PaidInDetailCount3"},
            {"PINPLUAMT4", "PaidInDetailAmount4"},
            {"PINPLUCNT4", "PaidInDetailCount4"},
            {"PINPLUAMT5", "PaidInDetailAmount5"},
            {"PINPLUCNT5", "PaidInDetailCount5"},
            // Paid-Out=PaidOut��
            {"POUTAMT", "TotalPaidoutAmount"},
            {"POUTCNT", "TotalPaidoutCount"},
            {"POUTPLUAMT1", "PaidOutDetailAmount1"},
            {"POUTPLUCNT1", "PaidOutDetailCount1"},
            {"POUTPLUAMT2", "PaidOutDetailAmount2"},
            {"POUTPLUCNT2", "PaidOutDetailCount2"},
            {"POUTPLUAMT3", "PaidOutDetailAmount3"},
            {"POUTPLUCNT3", "PaidOutDetailCount3"},
            {"POUTPLUAMT4", "PaidOutDetailAmount4"},
            {"POUTPLUCNT4", "PaidOutDetailCount4"},
            {"POUTPLUAMT5", "PaidOutDetailAmount5"},
            {"POUTPLUCNT5", "PaidOutDetailCount5"},
            // 操作明细=操作明�
            {"EXGAMT", "exchangeItemAmount"},
            {"EXGITEMQTY", "exchangeItemQuantity"},
            {"EXGTRANCNT", "exchangeTransactionCount"},
            {"RTNAMT", "returnItemAmount"},
            {"RTNITEMQTY", "returnItemQuantity"},
            {"RTNTRANCNT", "returnTransactionCount"},
            {"VOIDINVAMT", "TransactionVoidAmount"},
            {"VOIDINVCNT", "TransactionVoidCount"},
            {"CANCELAMT", "TransactionCancelAmount"},
            {"CANCELCNT", "TransactionCancelCount"},
            {"UPDAMT", "LineVoidAmount"},
            {"UPDCNT", "LineVoidCount"},
            {"NOWUPDAMT", "VoidAmount"},
            {"NOWUPDCNT", "VoidCount"},
            {"DROPENCNT", "DrawerOpenCount"},
            {"RTNCSHCNT", "ReprintCount"},
            {"RTNCSHAMT", "ReprintAmount"},
            {"ENTRYAMT", "ManualEntryAmount"},
            {"ENTRYCNT", "ManualEntryCount"},
            {"POENTRYAMT", "PriceOpenEntryAmount"},
            {"POENTRYCNT", "PriceOpenEntryCount"},
            {"REPRTAMT", "cashReturnAmount"},
            {"REPRTCNT", "cashReturnCount"},
            {"CASHINAMT", "CashInAmount"},
            {"CASHINCNT", "CashInCount"},
            {"CASHOUTAMT", "CashOutAmount"},
            {"CASHOUTCNT", "CashOutCount"},
            // 支付明细项目=支付明�
            {"OVERAMT", "SpillAmount"},
            {"OVERCNT", "SpillCount"},
            {"RATE1", "ExchangeRate1"},
            {"RATE2", "ExchangeRate2"},
            {"RATE3", "ExchangeRate3"},
            {"RATE4", "ExchangeRate4"},
            {"PAY00AMT", "Pay00Amount"},
            {"PAY00CNT", "Pay00Count"},
            {"PAY01AMT", "Pay01Amount"},
            {"PAY01CNT", "Pay01Count"},
            {"PAY02AMT", "Pay02Amount"},
            {"PAY02CNT", "Pay02Count"},
            {"PAY03AMT", "Pay03Amount"},
            {"PAY03CNT", "Pay03Count"},
            {"PAY04AMT", "Pay04Amount"},
            {"PAY04CNT", "Pay04Count"},
            {"PAY05AMT", "Pay05Amount"},
            {"PAY05CNT", "Pay05Count"},
            {"PAY06AMT", "Pay06Amount"},
            {"PAY06CNT", "Pay06Count"},
            {"PAY07AMT", "Pay07Amount"},
            {"PAY07CNT", "Pay07Count"},
            {"PAY08AMT", "Pay08Amount"},
            {"PAY08CNT", "Pay08Count"},
            {"PAY09AMT", "Pay09Amount"},
            {"PAY09CNT", "Pay09Count"},
            {"PAY10AMT", "Pay10Amount"},
            {"PAY10CNT", "Pay10Count"},
            {"PAY11AMT", "Pay11Amount"},
            {"PAY11CNT", "Pay11Count"},
            {"PAY12AMT", "Pay12Amount"},
            {"PAY12CNT", "Pay12Count"},
            {"PAY13AMT", "Pay13Amount"},
            {"PAY13CNT", "Pay13Count"},
            {"PAY14AMT", "Pay14Amount"},
            {"PAY14CNT", "Pay14Count"},
            {"PAY15AMT", "Pay15Amount"},
            {"PAY15CNT", "Pay15Count"},
            {"PAY16AMT", "Pay16Amount"},
            {"PAY16CNT", "Pay16Count"},
            {"PAY17AMT", "Pay17Amount"},
            {"PAY17CNT", "Pay17Count"},
            {"PAY18AMT", "Pay18Amount"},
            {"PAY18CNT", "Pay18Count"},
            {"PAY19AMT", "Pay19Amount"},
            {"PAY19CNT", "Pay19Count"},
            {"PAY20AMT", "Pay20Amount"},
            {"PAY20CNT", "Pay20Count"},
            {"PAY21AMT", "Pay21Amount"},
            {"PAY21CNT", "Pay21Count"},
            {"PAY22AMT", "Pay22Amount"},
            {"PAY22CNT", "Pay22Count"},
            {"PAY23AMT", "Pay23Amount"},
            {"PAY23CNT", "Pay23Count"},
            {"PAY24AMT", "Pay24Amount"},
            {"PAY24CNT", "Pay24Count"},
            {"PAY25AMT", "Pay25Amount"},
            {"PAY25CNT", "Pay25Count"},
            {"PAY26AMT", "Pay26Amount"},
            {"PAY26CNT", "Pay26Count"},
            {"PAY27AMT", "Pay27Amount"},
            {"PAY27CNT", "Pay27Count"},
            {"PAY28AMT", "Pay28Amount"},
            {"PAY28CNT", "Pay28Count"},
            {"PAY29AMT", "Pay29Amount"},
            {"PAY29CNT", "Pay29Count"},
            {"CHANGAMT", "ExchangeDifference"},
            {"SI00AMT", "SI00Amount"},
            {"SI00CNT", "SI00Count"},
            {"SI01AMT", "SI01Amount"},
            {"SI01CNT", "SI01Count"},
            {"SI02AMT", "SI02Amount"},
            {"SI02CNT", "SI02Count"},
            {"SI03AMT", "SI03Amount"},
            {"SI03CNT", "SI03Count"},
            {"SI04AMT", "SI04Amount"},
            {"SI04CNT", "SI04Count"},
            {"SI05AMT", "SI05Amount"},
            {"SI05CNT", "SI05Count"},
            {"SI06AMT", "SI06Amount"},
            {"SI06CNT", "SI06Count"},
            {"SI07AMT", "SI07Amount"},
            {"SI07CNT", "SI07Count"},
            {"SI08AMT", "SI08Amount"},
            {"SI08CNT", "SI08Count"},
            {"SI09AMT", "SI09Amount"},
            {"SI09CNT", "SI09Count"},
            {"SI10AMT", "SI10Amount"},
            {"SI10CNT", "SI10Count"},
            {"SI11AMT", "SI11Amount"},
            {"SI11CNT", "SI11Count"},
            {"SI12AMT", "SI12Amount"},
            {"SI12CNT", "SI12Count"},
            {"SI13AMT", "SI13Amount"},
            {"SI13CNT", "SI13Count"},
            {"SI14AMT", "SI14Amount"},
            {"SI14CNT", "SI14Count"},
            {"SI15AMT", "SI15Amount"},
            {"SI15CNT", "SI15Count"},
            {"SI16AMT", "SI16Amount"},
            {"SI16CNT", "SI16Count"},
            {"SI17AMT", "SI17Amount"},
            {"SI17CNT", "SI17Count"},
            {"SI18AMT", "SI18Amount"},
            {"SI18CNT", "SI18Count"},
            {"SI19AMT", "SI19Amount"},
            {"SI19CNT", "SI19Count"},
            {"holdtrancount", "holdtrancount"},
		};
	}
	
	
    public ShiftReport cloneForSC() {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            ShiftReport clonedShift = new ShiftReport(0);
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = this.getFieldValue(fieldNameMap[i][0]);
                if (value == null) {
                    try {
                        value = this.getClass().getDeclaredMethod(
                            "get" + fieldNameMap[i][0], new Class[0]).invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedShift.setFieldValue(fieldNameMap[i][1], value);
            }
            return clonedShift;
        } catch (InstantiationException e) {
            return null;
        }
    }

    public void update(DbConnection connection) throws SQLException, EntityNotFoundException {
        setAllFieldsDirty(true);
        super.update(connection);
    }
}
