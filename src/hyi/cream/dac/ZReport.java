package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamPropertyUtil;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Z report class.
 *
 * @author Dai, Slackware, Bruce
 * @version 1.6
 */
public class ZReport extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * /Bruce/1.6/2002-04-15/
     *    Modify cloneForSC(): 代收、代付、PaidIn、PaidOut少了id 6到10的部分，
     *    造成代收等第6项以后的数据没有写入后台
     *
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public ZReport cloneForSC()
     */
    transient public static final String VERSION = "1.6";

    transient static ZReport currentZ = null;
    transient static int currentZNumber = -1;

    //private static final String daiShouVersion = 
    //    GetProperty.getProperty("DaiShouVersion") == null?
    //    "" : GetProperty.getProperty("DaiShouVersion");

    private static ArrayList primaryKeys = new ArrayList();

    static 	{
        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("storeID");
            primaryKeys.add("posNumber");
            primaryKeys.add("zSequenceNumber");
        } else {
            primaryKeys.add("POSNO");
            primaryKeys.add("EODCNT");
        }
    }

    public java.util.List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public ZReport() {
        setAccountingDate(CreamToolkit.getInitialDate());
        setStoreNumber(Store.getStoreID());
        setTerminalNumber(PARAM.getTerminalNumber());
        setMachineNumber(PARAM.getTerminalPhysicalNumber());
        setUploadState("0");
    }

    public ZReport(int i) {
        // constructor for doing nothing, for inlnie client cloneForSC()
    }
    
    public static ZReport queryByKey(DbConnection connection, String storeID, int posNumber, int zNo) {
    	try {
            if (hyi.cream.inline.Server.serverExist())
                return  getSingleObject(connection, ZReport.class, "SELECT * FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE storeID='"
                    + storeID + "' AND posNumber = " + posNumber + " AND zSequenceNumber = " + zNo);
            else
                return getSingleObject(connection, ZReport.class, "SELECT * FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE POSNO = "
                    + posNumber + " AND EODCNT = " + zNo);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }
    
    public static ZReport queryAccountDateByKey(DbConnection connection, String storeID, int posNumber, int zNo) {
    	try {
            if (hyi.cream.inline.Server.serverExist())
                return getSingleObject(connection, ZReport.class, "SELECT accountdate FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE storeID='"
                    + storeID + "' AND posNumber = " + posNumber + " AND zSequenceNumber = " + zNo);
            else
                return getSingleObject(connection, ZReport.class, "SELECT accdate FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE POSNO = "
                    + posNumber + " AND EODCNT = " + zNo);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static Iterator queryByDateTime(DbConnection connection, java.util.Date date) {
        try {
            SimpleDateFormat df = CreamCache.getInstance().getDateFormate();
            if (hyi.cream.inline.Server.serverExist())
                return getMultipleObjects(connection, ZReport.class, "SELECT * FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE accountDate='"
                    + df.format(date).toString() + "'");
            else
                return getMultipleObjects(connection, ZReport.class, "SELECT * FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE ACCDATE='"
                    + df.format(date).toString() + "' ORDER BY EODCNT DESC");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static ZReport queryBySequenceNumber(DbConnection connection, Integer number) {
        try {
            return getSingleObject(connection, ZReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE EODCNT = " + number);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    /**
     * Query last 20 Z reports.
     * 
     * @return Iterator Last 20 Z reports.
     */
    public static Iterator queryRecentReports() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return getMultipleObjects(connection, ZReport.class,
                "SELECT ZBEGDTTM, ZENDDTTM, EODCNT FROM " 
                + getInsertUpdateTableNameStaticVersion() + " ORDER BY ZBEGDTTM DESC LIMIT 20");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static Iterator getUploadFailedList(DbConnection connection) {
        try {
            String failFlag = "2";
            return getMultipleObjects(connection, ZReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE TCPFLG='" + failFlag + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

//    public static Iterator getNotUploadedList(DbConnection connection) {
//        try {
//            String notUploadFlag = "0";
//            return getMultipleObjects(connection, ZReport.class,
//                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE TCPFLG='" + notUploadFlag + "'");
//        } catch (SQLException e) {
//            CreamToolkit.logMessage(e);
//            return null;
//        } catch (EntityNotFoundException e) {
//            return null;
//        }
//    }

    public static Iterator queryNotUploadReport(DbConnection connection) {
        try {
            return getMultipleObjects(connection, ZReport.class,
                "SELECT EODCNT FROM " + getInsertUpdateTableNameStaticVersion()
                + " WHERE ACCDATE<>'1970-01-01' AND TCPFLG<>'1'");
            //Bruce/20080606/ It'd be better to have index like this:
            // CREATE INDEX z_idx_accdate_tcpflg ON z (accdate, tcpflg);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static int getCurrentZNumber() {
        //Bruce/20080414/ Cache currentZNumber for improving performance
        if (currentZNumber == -1) {
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getPooledConnection();
                currentZNumber = queryCurrentZNumber(connection);
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                CreamToolkit.haltSystemOnDatabaseFatalError(e);
                return -1;
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
        }
        return currentZNumber;
    }

    public static int getCurrentZNumber(DbConnection connection) {
        //Bruce/20080414/ Cache currentZNumber for improving performance
        if (currentZNumber == -1)
            currentZNumber = queryCurrentZNumber(connection);
        return currentZNumber;
    }

    private static int queryCurrentZNumber(DbConnection connection) {
        Object eodCount = getValueOfStatement(connection, "SELECT MAX(EODCNT) FROM z");
        if (eodCount == null) {
            return -1;
        }
        int n = -1;
        if (eodCount instanceof Integer)
            n = ((Integer)eodCount).intValue();
        else if (eodCount instanceof Long)
            n = ((Long)eodCount).intValue();
        if (n == -1) {
            // try retrieving from SC instead here
            Integer maxZNoInSC = Client.getInstance().getMaxZNoInSC();
            if (maxZNoInSC == null)
                return -1;
            else
                return maxZNoInSC.intValue();
        }
        return n;
    }
    
    private static ZReport getOrCreateLastZReport(DbConnection connection) throws SQLException {
        int zNumber = getCurrentZNumber(connection);
        if (zNumber == -1) {
            ZReport z1 = createZReport(connection);
            CreamToolkit
                .logMessage("getOrCreateLastZReport | Z report creating detected! new Z is: "
                    + z1.getValues());
            return z1;
        }
        ZReport z;
        try {
            z = getSingleObject(connection, ZReport.class, "SELECT * FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE EODCNT='" + zNumber + "'");
        } catch (EntityNotFoundException e) {
            z = createZReport(connection);
            CreamToolkit.logMessage("getOrCreateLastZReport | new z.number : "
                + z.getSequenceNumber());
        }
        return z;
    }

    /**
     * 取得会计日为1970-01-01的Z记录。
     *
     * @return ZReport The current ZReport.
     */
    public static ZReport getCurrentZReport(DbConnection connection) {
        if (currentZ == null) {
            try {
                // 看序號最大的那筆是否為197X年，如果是則返回他作為當前Z
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                currentZ = (ZReport) DacBase.getSingleObject(connection,
                    ZReport.class,"SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                        + " ORDER BY eodcnt DESC LIMIT 1");
                if (df.format(currentZ.getAccountingDate()).startsWith("197"))
                    return currentZ;
                else {
                    currentZ = null;
                    return null;
                }
            } catch (Exception e) {
                CreamToolkit.logMessage(e);
                currentZ = null;
            }
        }
        return currentZ;
    }

    public static int queryCount(DbConnection connection) {
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Object count = getValueOfStatement(connection,
                "select count(*) from z where zenddttm between '"+date+" 00:00:00' and '"+date+" 23:59:59'");
        if (count == null) {
            return 0;
        }
        int n = 0;
        if (count instanceof Integer)
            n = ((Integer)count).intValue();
        else if (count instanceof Long)
            n = ((Long)count).intValue();
        return n;
    }

    /**
     * 取得会计日为1970-01-01的Z记录，若找不到，则取得最后一笔Z记录，若没有任何Z记录，
     * 则生成一笔新的Z记录。
     *
     * @return ZReport The current ZReport.
     * @throws SQLException 
     */
    public static ZReport getOrCreateCurrentZReport(DbConnection connection) throws SQLException {
        if (currentZ == null) {
            currentZ = getCurrentZReport(connection);
            if (currentZ == null)
                currentZ = getOrCreateLastZReport(connection);
        }
        return currentZ;
    }

    public static ZReport createZReport(DbConnection connection) throws SQLException {

        // 在create Z report的时候，如果发现后台的Z帐序号比前台还大，就用后台的序号加1
        Client client = Client.getInstance();
        int currZNumber;
        if (client.isConnected()) {
            int maxZNoInSC = -1;
            try {
                client.processCommand("queryMaxZNumber");
                Object returnObj = client.getReturnObject();
                if (returnObj != null)  {
                    SequenceNumberGetter seq = (SequenceNumberGetter)returnObj;
                    Integer maxZNoInSCObj = seq.getMaxZNumber();
                    if (maxZNoInSCObj != null)
                        maxZNoInSC = maxZNoInSCObj.intValue();
                }
            } catch (ClientCommandException e) {
            }
            
            currZNumber = ZReport.getCurrentZNumber(connection);
            if (maxZNoInSC > currZNumber)
                currZNumber = maxZNoInSC;
        } else {
            currZNumber = ZReport.getCurrentZNumber(connection);
        }

        currentZ = newZReport(connection);
        currentZ.setSequenceNumber(new Integer(currZNumber + 1));
        currentZ.setBeginSystemDateTime(new Date());
        currentZ.setEndSystemDateTime(CreamToolkit.getInitialDate());
        currentZ.setBeginTransactionNumber(connection, new Integer(Transaction.getNextTransactionNumber()));
        currentZ.setEndTransactionNumber(connection, new Integer(Transaction.getNextTransactionNumber()));
        String nextInvoiceNumber = PARAM.getNextInvoiceNumber();
        currentZ.setBeginInvoiceNumber(connection, nextInvoiceNumber);
        currentZ.setEndInvoiceNumber(connection, nextInvoiceNumber);
        currentZ.setMachineNumber(PARAM.getTerminalPhysicalNumber());

        if (!currentZ.exists(connection))
            currentZ.insert(connection);

        DepSales.createDepSales(connection, currentZ.getSequenceNumber().intValue());
        DaishouSales.createDaishouSales(connection, currentZ.getSequenceNumber().intValue());

        return currentZ;

//        DbConnection connection = null;
//        try {
//            connection = CreamToolkit.getTransactionalConnection();
//            ZReport z = (ZReport)currentZ.queryByPrimaryKey();
//            if (z == null)
//                currentZ.insert(connection);
//            CreamToolkit.logMessage("createZReport | z.number : " + currentZ.getSequenceNumber());
//        } catch (SQLException e) {
//            CreamToolkit.releaseConnection(connection);
//            currentZ = (ZReport)currentZ.queryByPrimaryKey();
//            try {
//                connection = CreamToolkit.getTransactionalConnection();
//            } catch (SQLException e1) {
//            }
//        }
//        try {
//            if (currentZ != null) {
//                DepSales.createDepSales(connection, currentZ.getSequenceNumber().intValue());
//                // if (!daiShouVersion.equals("2"))
//                DaishouSales.createDaishouSales(connection, currentZ.getSequenceNumber().intValue());
//            }
//            connection.commit();
//        } catch (SQLException e) {
//            CreamToolkit.logMessage(e);
//        } finally {
//            CreamToolkit.releaseConnection(connection);
//        }
    }

    private static ZReport newZReport(DbConnection connection) {
        currentZ = new ZReport();
        try {
            CreamToolkit.logMessage("---------- debug | newZReport() | posNo : "
                + PARAM.getTerminalNumber());
            int lastZ = ZReport.getCurrentZNumber(connection);
            if (lastZ == -1 || lastZ == 0) {
                currentZ.setGrandTotal(new HYIDouble(0).setScale(2, 4));
                currentZ.setTransactionCountGrandTotal(new Integer(0));
                currentZ.setItemCountGrandTotal(new HYIDouble(0).setScale(2, 4));
                return currentZ;
            }
            // lastZ--;
            ZReport dataZ = (ZReport)getSingleObject(connection, ZReport.class, "SELECT * FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE EODCNT=" + lastZ
                + " AND POSNO=" + PARAM.getTerminalNumber());
            currentZ.setGrandTotal(new HYIDouble(0).addMe(dataZ.getGrandTotal()));
            currentZ.setTransactionCountGrandTotal(dataZ.getTransactionCountGrandTotal());
            currentZ.setItemCountGrandTotal(dataZ.getItemCountGrandTotal());
            CreamToolkit.logMessage("newZReport | old z.number : " + dataZ.getSequenceNumber());
            return currentZ;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e);
            currentZ.setGrandTotal(new HYIDouble(0).setScale(2, 4));
            currentZ.setTransactionCountGrandTotal(new Integer(0));
            currentZ.setItemCountGrandTotal(new HYIDouble(0).setScale(2, 4));
            CreamToolkit.logMessage("newZReport | new z.number : "
                + currentZ.getSequenceNumber());
            return currentZ;
        }
    }

    @Override
    public void insert(DbConnection connection, boolean needQueryAgain) throws SQLException
    {
        super.insert(connection, needQueryAgain);
        if (!hyi.cream.inline.Server.serverExist())
            currentZNumber = queryCurrentZNumber(connection); // only do this at POS side
    }

    @Override
    public void update(DbConnection connection) throws SQLException {
    	try {
            DepSales.updateAll(connection, getAccountingDate());
    	} catch (SQLException e) {
			CreamToolkit.logMessage("ZReport.update()> update depsales failed.");
            throw e;
    	}
    	try {
            this.setAllFieldsDirty(true);
            super.update(connection);
            //currentZNumber = queryCurrentZNumber(connection);
    	} catch (SQLException e) {
			CreamToolkit.logMessage("ZReport.update()> update z failed.");
            throw e;
    	} catch (EntityNotFoundException e) {
            CreamToolkit.logMessage("ZReport.update()> update z failed.");
            throw new SQLException(e.toString());
        }
    }

    /*
    public void updateTaxMM() {
        Hashtable taxMMAmount = tran.getTaxMMAmountHashtable();
        Hashtable taxMMCount = tran.getTaxMMCountHashtable();
        String key = "";

        //  set to M&M amount
        Iterator iter = (taxMMAmount.keySet()).iterator();
        String fieldName = "";
        HYIDouble fieldValue = null;
        while (iter.hasNext()) {
            key = (String)iter.next();
            fieldName = "MNMAMT" + key;
            fieldValue = (HYIDouble)taxMMAmount.get(key);
            setFieldValue(fieldName, fieldValue);
        }

        //  set to M&M count
        iter = (taxMMAmount.keySet()).iterator();
        String fieldName2 = "";
        Integer fieldValue2 = null;
        while (iter.hasNext()) {
            key = (String)iter.next();
            fieldName2 = "MNMCNT" + key;
            fieldValue2 = (Integer)taxMMCount.get(key);
            setFieldValue(fieldName2, fieldValue2);
        }
    }*/


    //Get&Set properties value:
//表头
//StoreNumber	STORENO	CHAR(6)	N
    public String getStoreNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("storeID");
        else
            return (String)getFieldValue("STORENO");
    }

    public void setStoreNumber(String storeNumber) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("storeID", storeNumber);
        else
            setFieldValue("STORENO", storeNumber);
    }

//TerminalNumber	POSNO	TINYINT UNSIGNED	N
    public Integer getTerminalNumber() {
        if (hyi.cream.inline.Server.serverExist()) {
            if (getFieldValue("posNumber") instanceof Byte)
                return new Integer(((Byte)getFieldValue("posNumber")).intValue());
            else
                return (Integer)getFieldValue("posNumber");
        } else {
            if (getFieldValue("POSNO") instanceof Byte)
                return new Integer(((Byte)getFieldValue("POSNO")).intValue());
            else
                return (Integer)getFieldValue("POSNO");
        }
    }

    public void setTerminalNumber(Integer posno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("posNumber", posno);
        else
            setFieldValue("POSNO", posno);
    }
    public void setTerminalNumber(Byte posno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("posNumber", posno);
        else
            setFieldValue("POSNO", posno);
    }

//MachineNumber
    public String getMachineNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("machineNumber");
        else
            return (String)getFieldValue("MACHINENO");
    }

    public void setMachineNumber(String machineno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("machineNumber", machineno);
        else
            setFieldValue("MACHINENO", machineno);
    }

//SequenceNumber	EODCNT	TINYINT UNSIGNED	N
    public Integer getSequenceNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("zSequenceNumber");
        else
            return (Integer)getFieldValue("EODCNT");
    }

    public void setSequenceNumber(Integer SQNumber) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("zSequenceNumber", SQNumber);
        else
            setFieldValue("EODCNT", SQNumber);
    }

//AccountingDate
    public Date getAccountingDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (Date)getFieldValue("accountDate");
        else
            return (Date)getFieldValue("ACCDATE");
    }

    public void setAccountingDate(Date accdate) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("accountDate", accdate);
        else
            setFieldValue("ACCDATE", accdate);
    }

//BeginSystemDateTime
    public Date getBeginSystemDateTime() {
        if (hyi.cream.inline.Server.serverExist())
            return (Date)getFieldValue("zBeginDateTime");
        else
            return (Date)getFieldValue("ZBEGDTTM");
    }

    public void setBeginSystemDateTime(Date zbegdttm) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("zBeginDateTime", zbegdttm);
        else
            setFieldValue("ZBEGDTTM", zbegdttm);
    }

//EndSystemDateTime
    public Date getEndSystemDateTime() {
        if (hyi.cream.inline.Server.serverExist())
            return (Date)getFieldValue("zEndDateTime");
        else
            return (Date)getFieldValue("ZENDDTTM");
    }

    public void setEndSystemDateTime(Date zenddttm) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("zEndDateTime", zenddttm);
        else
            setFieldValue("ZENDDTTM", zenddttm);
    }

//BeginTransactionNumber	SGNONSEQ	INT UNSIGNED	N
    public Integer getBeginTransactionNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("zBeginTransactionNumber");
        else
            return (Integer)getFieldValue("SGNONSEQ");
    }

    public void setBeginTransactionNumber(DbConnection connection, Integer no) throws SQLException {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("zBeginTransactionNumber", no);
        else {
            setFieldValue("SGNONSEQ", no);
            PARAM.updateBeginTransactionNumberOfZ(no);
        }
    }

//EndTransactionNumber	SGNOFFSEQ	INT UNSIGNED	N
    public Integer getEndTransactionNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("zEndTransactionNumber");
        else
            return (Integer)getFieldValue("SGNOFFSEQ");
    }

    public void setEndTransactionNumber(DbConnection connection, Integer no) throws SQLException {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("zEndTransactionNumber", no);
        else {
            setFieldValue("SGNOFFSEQ", no);
            PARAM.updateEndTransactionNumberOfZ(no);
        }
    }

//BeginInvoiceNumber	SGNONINV	CHAR(10)	N
    public String getBeginInvoiceNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("zBeginInvoiceNumber");
        else
            return (String)getFieldValue("SGNONINV");
    }

    public void setBeginInvoiceNumber(DbConnection connection, String no) throws SQLException {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("zBeginInvoiceNumber", no);
        else {
            setFieldValue("SGNONINV", no);
            PARAM.updateBeginInvoiceNumberOfZ(no);
        }
    }

//EndInvoiceNumber	SGNOFFINV	CHAR(10)	N
    public String getEndInvoiceNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("zEndInvoiceNumber");
        else
            return (String)getFieldValue("SGNOFFINV");
    }

    public void setEndInvoiceNumber(DbConnection connection, String eno) throws SQLException {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("zEndInvoiceNumber", eno);
        else {
            setFieldValue("SGNOFFINV", eno);
            PARAM.updateEndInvoiceNumberOfZ(eno);
        }
    }

//UploadState	TCPFLG	ENUM("0","1")	N
    public String getUploadState() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("uploadState");
        else
            return (String)getFieldValue("TCPFLG");
    }

    public void setUploadState(String TcpFlag) {
         if (hyi.cream.inline.Server.serverExist())
             setFieldValue("uploadState", TcpFlag);
         else
             setFieldValue("TCPFLG", TcpFlag);
    }

//CashierNumber	CASHIER	CHAR(7)	N
    public String getCashierNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("cashierID");
        else
            return (String)getFieldValue("CASHIER");
    }

    public void setCashierNumber(String cashierNo) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("cashierID", cashierNo);
        else
            setFieldValue("CASHIER", cashierNo);
    }

//GrandTotal
    public HYIDouble getGrandTotal() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("GT");
    }

    public void setGrandTotal(HYIDouble gt) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("GT", gt);
        else
            setFieldValue("GT", gt);
    }

//TransactionCountGrandTotal
    public Integer getTransactionCountGrandTotal() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("GTCUSCNT");
    }

    public void setTransactionCountGrandTotal(Integer gtcuscnt) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("ZEndInvoiceNumber", gtcuscnt);
        else
            setFieldValue("GTCUSCNT", gtcuscnt);
    }

//ItemCountGrandTotal
    public HYIDouble getItemCountGrandTotal() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("GTSALECNT");
    }

    public void setItemCountGrandTotal(HYIDouble gtsalecnt) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("GTSALECNT", ");
        else
            setFieldValue("GTSALECNT", gtsalecnt);
    }


//交易明细

//TaxType0GrossSales	SALAMTTAX0AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType0GrossSales() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("grossSaleTax0Amount");
        else
            return (HYIDouble)getFieldValue("SALAMTTAX0");
    }

    public void setTaxType0GrossSales(HYIDouble TaxType0GrossSales) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax0Amount", TaxType0GrossSales);
        else
            setFieldValue("SALAMTTAX0", TaxType0GrossSales);
    }

//TaxType1GrossSales	SALAMTTAX1AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType1GrossSales() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("grossSaleTax1Amount");
        else
            return (HYIDouble)getFieldValue("SALAMTTAX1");
    }

    public void setTaxType1GrossSales(HYIDouble TaxType1GrossSales) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax1Amount", TaxType1GrossSales);
        else
            setFieldValue("SALAMTTAX1", TaxType1GrossSales);
    }

//TaxType2GrossSales	SALAMTTAX2AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType2GrossSales() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("grossSaleTax2Amount");
        else
            return (HYIDouble)getFieldValue("SALAMTTAX2");
    }

    public void setTaxType2GrossSales(HYIDouble TaxType2GrossSales) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax2Amount", TaxType2GrossSales);
        else
            setFieldValue("SALAMTTAX2", TaxType2GrossSales);
    }

//TaxType3GrossSales	SALAMTTAX3AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType3GrossSales() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("grossSaleTax3Amount");
        else
            return (HYIDouble)getFieldValue("SALAMTTAX3");
    }

    public void setTaxType3GrossSales(HYIDouble TaxType3GrossSales) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax3Amount", TaxType3GrossSales);
        else
            setFieldValue("SALAMTTAX3", TaxType3GrossSales);
    }

//TaxType4GrossSales	SALAMTTAX4AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType4GrossSales() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("grossSaleTax4Amount");
        else
            return (HYIDouble)getFieldValue("SALAMTTAX4");
    }

    public void setTaxType4GrossSales(HYIDouble TaxType4GrossSales) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax4Amount", TaxType4GrossSales);
        else
            setFieldValue("SALAMTTAX4", TaxType4GrossSales);
    }
//TaxType5GrossSales	SALAMTTAX5AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType5GrossSales() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("grossSaleTax5Amount");
        else
            return (HYIDouble)getFieldValue("SALAMTTAX5");
    }

    public void setTaxType5GrossSales(HYIDouble TaxType5GrossSales) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax5Amount", TaxType5GrossSales);
        else
            setFieldValue("SALAMTTAX5", TaxType5GrossSales);
    }
//TaxType6GrossSales	SALAMTTAX6AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType6GrossSales() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("grossSaleTax6Amount");
        else
            return (HYIDouble)getFieldValue("SALAMTTAX6");
    }

    public void setTaxType6GrossSales(HYIDouble TaxType6GrossSales) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax6Amount", TaxType6GrossSales);
        else
            setFieldValue("SALAMTTAX6", TaxType6GrossSales);
    }
//TaxType7GrossSales	SALAMTTAX7AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType7GrossSales() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("grossSaleTax7Amount");
        else
            return (HYIDouble)getFieldValue("SALAMTTAX7");
    }

    public void setTaxType7GrossSales(HYIDouble TaxType7GrossSales) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax7Amount", TaxType7GrossSales);
        else
            setFieldValue("SALAMTTAX7", TaxType7GrossSales);
    }
//TaxType8GrossSales	SALAMTTAX8AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType8GrossSales() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("grossSaleTax8Amount");
        else
            return (HYIDouble)getFieldValue("SALAMTTAX8");
    }

    public void setTaxType8GrossSales(HYIDouble TaxType8GrossSales) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax8Amount", TaxType8GrossSales);
        else
            setFieldValue("SALAMTTAX8", TaxType8GrossSales);
    }
//TaxType9GrossSales	SALAMTTAX9AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType9GrossSales() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("grossSaleTax9Amount");
        else
            return (HYIDouble)getFieldValue("SALAMTTAX9");
    }

    public void setTaxType9GrossSales(HYIDouble TaxType9GrossSales) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax9Amount", TaxType9GrossSales);
        else
            setFieldValue("SALAMTTAX9", TaxType9GrossSales);
    }
//TaxType10GrossSales	SALAMTTAX10AM	DECIMAL(12,2)	N
    public HYIDouble getTaxType10GrossSales() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("grossSaleTax10Amount");
        else
            return (HYIDouble)getFieldValue("SALAMTTAX10");
    }

    public void setTaxType10GrossSales(HYIDouble TaxType10GrossSales) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax10Amount", TaxType10GrossSales);
        else
            setFieldValue("SALAMTTAX10", TaxType10GrossSales);
    }

    //  TaxAmount0	TAXAMT0	DECIMAL(12,2)	N	税别0税额
    public HYIDouble getTaxAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("taxAmount0");
        else
            return (HYIDouble)getFieldValue("TAXAMT0");
    }

    public void setTaxAmount0(HYIDouble taxamt0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount0", taxamt0);
        else
            setFieldValue("TAXAMT0", taxamt0);
    }

    //  TaxAmount1	TAXAMT1	DECIMAL(12,2)	N	税别1税额
    public HYIDouble getTaxAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("TaxAmount1");
        else
            return (HYIDouble)getFieldValue("TAXAMT1");
    }

    public void setTaxAmount1(HYIDouble taxamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount1", taxamt1);
        else
            setFieldValue("TAXAMT1", taxamt1);
    }

    //  TaxAmount2	TAXAMT2	DECIMAL(12,2)	N	税别2税额
    public HYIDouble getTaxAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("taxAmount2");
        else
            return (HYIDouble)getFieldValue("TAXAMT2");
    }

    public void setTaxAmount2(HYIDouble taxamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount2", taxamt2);
        else
            setFieldValue("TAXAMT2", taxamt2);
    }

    //  TaxAmount3	TAXAMT3	DECIMAL(12,2)	N	税别3税额
    public HYIDouble getTaxAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("taxAmount3");
        else
            return (HYIDouble)getFieldValue("TAXAMT3");
    }

    public void setTaxAmount3(HYIDouble taxamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount3", taxamt3);
        else
            setFieldValue("TAXAMT3", taxamt3);
    }

    //  TaxAmount4	TAXAMT4	DECIMAL(12,2)	N	税别4税额
    public HYIDouble getTaxAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("taxAmount4");
        else
            return (HYIDouble)getFieldValue("TAXAMT4");
    }

    public void setTaxAmount4(HYIDouble taxamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount4", taxamt4);
        else
            setFieldValue("TAXAMT4", taxamt4);
    }
    //  TaxAmount5	TAXAMT5	DECIMAL(12,2)	N	税别5税额
    public HYIDouble getTaxAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("taxAmount5");
        else
            return (HYIDouble)getFieldValue("TAXAMT5");
    }

    public void setTaxAmount5(HYIDouble taxamt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount5", taxamt5);
        else
            setFieldValue("TAXAMT5", taxamt5);
    }

    //  TaxAmount6	TAXAMT6	DECIMAL(12,2)	N	税别6税额
    public HYIDouble getTaxAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("taxAmount6");
        else
            return (HYIDouble)getFieldValue("TAXAMT6");
    }

    public void setTaxAmount6(HYIDouble taxamt6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount6", taxamt6);
        else
            setFieldValue("TAXAMT6", taxamt6);
    }

    //  TaxAmount7	TAXAMT7	DECIMAL(12,2)	N	税别7税额
    public HYIDouble getTaxAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("taxAmount7");
        else
            return (HYIDouble)getFieldValue("TAXAMT7");
    }

    public void setTaxAmount7(HYIDouble taxamt7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount7", taxamt7);
        else
            setFieldValue("TAXAMT7", taxamt7);
    }

    //  TaxAmount8	TAXAMT8	DECIMAL(12,2)	N	税别8税额
    public HYIDouble getTaxAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("taxAmount8");
        else
            return (HYIDouble)getFieldValue("TAXAMT8");
    }

    public void setTaxAmount8(HYIDouble taxamt8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount8", taxamt8);
        else
            setFieldValue("TAXAMT8", taxamt8);
    }

    //  TaxAmount9	TAXAMT9	DECIMAL(12,2)	N	税别9税额
    public HYIDouble getTaxAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("taxAmount9");
        else
            return (HYIDouble)getFieldValue("TAXAMT9");
    }

    public void setTaxAmount9(HYIDouble taxamt9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount9", taxamt9);
        else
            setFieldValue("TAXAMT9", taxamt9);
    }

    //  TaxAmount10	TAXAMT10	DECIMAL(12,2)	N	税别10税额
    public HYIDouble getTaxAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("taxAmount10");
        else
            return (HYIDouble)getFieldValue("TAXAMT10");
    }

    public void setTaxAmount10(HYIDouble taxamt10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount10", taxamt10);
        else
            setFieldValue("TAXAMT10", taxamt10);
    }

    //Bruce/20030328
    public HYIDouble getTotalTaxAmount() {
        return new HYIDouble(0).addMe(getTaxAmount0()).addMe(getTaxAmount1()).addMe(getTaxAmount2()).addMe(getTaxAmount3()).addMe(getTaxAmount4());
    }

    //  TaxAmount   税别合计
    public HYIDouble getTaxAmount() {
        HYIDouble taxAmount = new HYIDouble(0);
        if (getTaxAmount0() != null)
            taxAmount.addMe(getTaxAmount0());
        if (getTaxAmount1() != null)
            taxAmount.addMe(getTaxAmount1());
        if (getTaxAmount2() != null)
            taxAmount.addMe(getTaxAmount2());
        if (getTaxAmount3() != null)
            taxAmount.addMe(getTaxAmount3());
        if (getTaxAmount4() != null)
            taxAmount.addMe(getTaxAmount4());
        if (getTaxAmount5() != null)
            taxAmount.addMe(getTaxAmount5());
        if (getTaxAmount6() != null)
            taxAmount.addMe(getTaxAmount6());
        if (getTaxAmount7() != null)
            taxAmount.addMe(getTaxAmount7());
        if (getTaxAmount8() != null)
            taxAmount.addMe(getTaxAmount8());
        if (getTaxAmount9() != null)
            taxAmount.addMe(getTaxAmount9());
        if (getTaxAmount10() != null)
            taxAmount.addMe(getTaxAmount10());
        return taxAmount;
    }

    public void setTaxAmount(HYIDouble taxamt) {
        //taxAmount = taxamt;
    }

//ItemCount	ITEMSALEQTY	INT UNSIGNED	N
    public HYIDouble getItemCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("itemSaleQuantity");
        else
            return (HYIDouble)getFieldValue("SALECNT");
    }

    public void setItemCount(HYIDouble saleCount){
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("itemSaleQuantity",saleCount);
        else
            setFieldValue("SALECNT",saleCount);
    }

//TransactionCount	TRANCNT	INT UNSIGNED	N
    public Integer getTransactionCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("transactionCount");
        else
            return (Integer)getFieldValue("TRANCNT");
    }

    public void setTransactionCount(Integer tranCount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("transactionCount", tranCount);
        else
            setFieldValue("TRANCNT", tranCount);
    }

//CustomerCount
    public Integer getCustomerCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("customerCount");
        else
            return (Integer)getFieldValue("CUSTCNT");
    }

    public void setCustomerCount(Integer custcnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("customerCount", custcnt);
        else
            setFieldValue("CUSTCNT", custcnt);
    }

/******************************************************************************
    public Integer getInCustomerCount() {
        return (Integer)getFieldValue("INCUSTCNT");
    }

    public void setInCustomerCount(Integer custcnt) {
        setFieldValue("INCUSTCNT", custcnt);
    }

    public Integer getOutCustomerCount() {
        return (Integer)getFieldValue("OUTCUSTCNT");
    }

    public void setOutCustomerCount(Integer custcnt) {
        setFieldValue("OUTCUSTCNT", custcnt);
    }
/******************************************************************************/

    public HYIDouble getTaxedAmount() {//应税金额
       HYIDouble amt = new HYIDouble(0);
       String taxID = "0";
       if (TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount0());
       taxID = "1";
       if (TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount1());
       taxID = "2";
       if (TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount2());
       taxID = "3";
       if (TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount3());
       taxID = "4";
       if (TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount4());
       taxID = "5";
       if (TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount5());
       taxID = "6";
       if (TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount6());
       taxID = "7";
       if (TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount7());
       taxID = "8";
       if (TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount8());
       taxID = "9";
       if (TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount9());
       taxID = "10";
       if (TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount10());
       return amt;
    }

    public HYIDouble getNoTaxedAmount() {//免税金额
       HYIDouble amt = new HYIDouble(0);
       String taxID = "0";
       if (!TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount0());
       taxID = "1";
       if (!TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount1());
       taxID = "2";
       if (!TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount2());
       taxID = "3";
       if (!TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount3());
       taxID = "4";
       if (!TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount4());
       taxID = "5";
       if (!TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount5());
       taxID = "6";
       if (!TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount6());
       taxID = "7";
       if (!TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount7());
       taxID = "8";
       if (!TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount8());
       taxID = "9";
       if (!TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount9());
       taxID = "10";
       if (!TaxType.getTaxedType(taxID))
          amt = amt.addMe(getNetSalesAmount10());
       return amt;
    }

//SIPercentPlusTotalAmount		HYIDouble	N	各种税别SI总加成金额
    public HYIDouble getSIPercentPlusTotalAmount() {
        HYIDouble total = new HYIDouble(0); 
        total = total.addMe(getSIPercentPlusAmount0());
        total = total.addMe(getSIPercentPlusAmount1());
        total = total.addMe(getSIPercentPlusAmount2());
        total = total.addMe(getSIPercentPlusAmount3());
        total = total.addMe(getSIPercentPlusAmount4());
        total = total.addMe(getSIPercentPlusAmount5());
        total = total.addMe(getSIPercentPlusAmount6());
        total = total.addMe(getSIPercentPlusAmount7());
        total = total.addMe(getSIPercentPlusAmount8());
        total = total.addMe(getSIPercentPlusAmount9());
        total = total.addMe(getSIPercentPlusAmount10());
        return total;
    }

//SIPercentPlusTotalCount		int	N	各种税别 SI总加成次数
    public Integer getSIPercentPlusTotalCount() {
        int total = getSIPercentPlusCount0().intValue();
        total += getSIPercentPlusCount1().intValue();
        total += getSIPercentPlusCount2().intValue();
        total += getSIPercentPlusCount3().intValue();
        total += getSIPercentPlusCount4().intValue();
        total += getSIPercentPlusCount5().intValue();
        total += getSIPercentPlusCount6().intValue();
        total += getSIPercentPlusCount7().intValue();
        total += getSIPercentPlusCount8().intValue();
        total += getSIPercentPlusCount9().intValue();
        total += getSIPercentPlusCount10().intValue();
        return new Integer(total);
    }

//SIPercentOffTotalAmount		HYIDouble	N	各种税别 SI总折扣金额
    public HYIDouble getSIPercentOffTotalAmount() {
        HYIDouble total = new HYIDouble(0); 
        total = total.addMe(getSIPercentOffAmount0());
        total = total.addMe(getSIPercentOffAmount1());
        total = total.addMe(getSIPercentOffAmount2());
        total = total.addMe(getSIPercentOffAmount3());
        total = total.addMe(getSIPercentOffAmount4());
        total = total.addMe(getSIPercentOffAmount5());
        total = total.addMe(getSIPercentOffAmount6());
        total = total.addMe(getSIPercentOffAmount7());
        total = total.addMe(getSIPercentOffAmount8());
        total = total.addMe(getSIPercentOffAmount9());
        total = total.addMe(getSIPercentOffAmount10());
        return total;
    }

//SIPercentOffTotalCount		int	N	各种税别 SI总折扣次数
    public Integer getSIPercentOffTotalCount() {
        int total = getSIPercentOffCount0().intValue();
        total += getSIPercentOffCount1().intValue();
        total += getSIPercentOffCount2().intValue();
        total += getSIPercentOffCount3().intValue();
        total += getSIPercentOffCount4().intValue();
        total += getSIPercentOffCount5().intValue();
        total += getSIPercentOffCount6().intValue();
        total += getSIPercentOffCount7().intValue();
        total += getSIPercentOffCount8().intValue();
        total += getSIPercentOffCount9().intValue();
        total += getSIPercentOffCount10().intValue();
        return new Integer(total);
    }

//SIDiscountTotalAmount		HYIDouble	N	各种税别总折让金额
    public HYIDouble getSIDiscountTotalAmount() {
        HYIDouble total = new HYIDouble(0); 
        total = total.addMe(getSIDiscountAmount0());
        total = total.addMe(getSIDiscountAmount1());
        total = total.addMe(getSIDiscountAmount2());
        total = total.addMe(getSIDiscountAmount3());
        total = total.addMe(getSIDiscountAmount4());
        total = total.addMe(getSIDiscountAmount5());
        total = total.addMe(getSIDiscountAmount6());
        total = total.addMe(getSIDiscountAmount7());
        total = total.addMe(getSIDiscountAmount8());
        total = total.addMe(getSIDiscountAmount9());
        total = total.addMe(getSIDiscountAmount10());
        return total;
    }

//SIDiscountTotalCount		int	N	各种税别总折让次数

    public Integer getSIDiscountTotalCount() {
        int total = getSIDiscountCount0().intValue();
        total += getSIDiscountCount1().intValue();
        total += getSIDiscountCount2().intValue();
        total += getSIDiscountCount3().intValue();
        total += getSIDiscountCount4().intValue();
        total += getSIDiscountCount5().intValue();
        total += getSIDiscountCount6().intValue();
        total += getSIDiscountCount7().intValue();
        total += getSIDiscountCount8().intValue();
        total += getSIDiscountCount9().intValue();
        total += getSIDiscountCount10().intValue();
        return new Integer(total);
    }

//MixAndMatchTotalAmount		HYIDouble	N	各种税别 M&M折让金额
    public HYIDouble getMixAndMatchTotalAmount() {
        HYIDouble total = new HYIDouble(0); 
        total = total.addMe(getMixAndMatchAmount0());
        total = total.addMe(getMixAndMatchAmount1());
        total = total.addMe(getMixAndMatchAmount2());
        total = total.addMe(getMixAndMatchAmount3());
        total = total.addMe(getMixAndMatchAmount4());
        total = total.addMe(getMixAndMatchAmount5());
        total = total.addMe(getMixAndMatchAmount6());
        total = total.addMe(getMixAndMatchAmount7());
        total = total.addMe(getMixAndMatchAmount8());
        total = total.addMe(getMixAndMatchAmount9());
        total = total.addMe(getMixAndMatchAmount10());
        return total;
    }

//MixAndMatchTotalCount		int	N	各种税别 M&M折让次数
    public Integer getMixAndMatchTotalCount() {
        int total = getMixAndMatchCount0().intValue();
        total += getMixAndMatchCount1().intValue();
        total += getMixAndMatchCount2().intValue();
        total += getMixAndMatchCount3().intValue();
        total += getMixAndMatchCount4().intValue();
        total += getMixAndMatchCount5().intValue();
        total += getMixAndMatchCount6().intValue();
        total += getMixAndMatchCount7().intValue();
        total += getMixAndMatchCount8().intValue();
        total += getMixAndMatchCount9().intValue();
        total += getMixAndMatchCount10().intValue();
        return new Integer(total);
    }

//NotIncludedTotalSales		HYIDouble	N	各种税别已开发票支付金额
    public HYIDouble getNotIncludedTotalSales() {
        HYIDouble total = new HYIDouble(0); 
        total = total.addMe(getNotIncludedSales0());
        total = total.addMe(getNotIncludedSales1());
        total = total.addMe(getNotIncludedSales2());
        total = total.addMe(getNotIncludedSales3());
        total = total.addMe(getNotIncludedSales4());
        total = total.addMe(getNotIncludedSales5());
        total = total.addMe(getNotIncludedSales6());
        total = total.addMe(getNotIncludedSales7());
        total = total.addMe(getNotIncludedSales8());
        total = total.addMe(getNotIncludedSales9());
        total = total.addMe(getNotIncludedSales10());
        return total;
    }
//NetSalesTotalAmount		HYIDouble	N	各种税别发票金额

    public HYIDouble getNetSalesTotalAmount() {
        HYIDouble total = new HYIDouble(0); 
        total = total.addMe(getNetSalesAmount0());
        total = total.addMe(getNetSalesAmount1());
        total = total.addMe(getNetSalesAmount2());
        total = total.addMe(getNetSalesAmount3());
        total = total.addMe(getNetSalesAmount4());
        total = total.addMe(getNetSalesAmount5());
        total = total.addMe(getNetSalesAmount6());
        total = total.addMe(getNetSalesAmount7());
        total = total.addMe(getNetSalesAmount8());
        total = total.addMe(getNetSalesAmount9());
        total = total.addMe(getNetSalesAmount10());
        return total;
    }

    public Integer getTotalPayCount() {
        int total = 0;
        for (int i = 0; i < 30; i++) {
            String j = String.valueOf(i);
            if (i < 10)
                j = "0" + i;
            try {
                Integer value = (Integer)this.getClass().getDeclaredMethod(
                    "getPay" + j + "Count", new Class[0]).invoke(this, new Object[0]);
                if (value != null) {
                    total += value.intValue();
                }
            } catch (Exception e) {}
        }
        return new Integer(total);
    }
//0
//SIPercentPlusAmount0	SIPLUSAMT0	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("siPlusTax0Amount");
        else
            return (HYIDouble)getFieldValue("SIPLUSAMT0");
    }

    public void setSIPercentPlusAmount0(HYIDouble SIPercentPlusAmount0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax0Amount", SIPercentPlusAmount0);
        else
            setFieldValue("SIPLUSAMT0", SIPercentPlusAmount0);
    }

//SIPercentPlusCount0	SIPLUSCNT0	INT UNSIGNED	N
    public Integer getSIPercentPlusCount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siPlusTax0Count");
        else
            return (Integer)getFieldValue("SIPLUSCNT0");
    }

    public void setSIPercentPlusCount0(Integer SIPercentPlusCount0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax0Count", SIPercentPlusCount0);
        else
            setFieldValue("SIPLUSCNT0", SIPercentPlusCount0);
    }

//SIPercentOffAmount0	SIPAMT0	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("discountTax0Amount");
        else
            return (HYIDouble)getFieldValue("SIPAMT0");
    }

    public void setSIPercentOffAmount0(HYIDouble SIPercentOffAmount0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax0Amount", SIPercentOffAmount0);
        else
            setFieldValue("SIPAMT0", SIPercentOffAmount0);
    }

//SIPercentOffCount0	SIPCNT0	INT UNSIGNED	N
    public Integer getSIPercentOffCount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("discountTax0Count");
        else
            return (Integer)getFieldValue("SIPCNT0");
    }

    public void setSIPercentOffCount0(Integer SIPercentOffCount0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax0Count", SIPercentOffCount0);
        else
            setFieldValue("SIPCNT0", SIPercentOffCount0);
    }

//SIDiscountAmount0	SIMAMT0	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("SIMAMT0");
    }

    public void setSIDiscountAmount0(HYIDouble SIDiscountAmount0) {
        if (hyi.cream.inline.Server.serverExist())
            ;//
        else
            setFieldValue("SIMAMT0", SIDiscountAmount0);
    }

//SIDiscountCount0	SIMCNT0	INT UNSIGNED	N
    public Integer getSIDiscountCount0() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("SIMCNT0");
    }

    public void setSIDiscountCount0(Integer SIDiscountCount0) {
        if (hyi.cream.inline.Server.serverExist())
            ;//
        else
            setFieldValue("SIMCNT0", SIDiscountCount0);
    }

//MixAndMatchAmount0
    public HYIDouble getMixAndMatchAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("mixAndMatchTax0Amount");
        else
            return (HYIDouble)getFieldValue("MNMAMT0");
    }

    public void setMixAndMatchAmount0(HYIDouble mnmamt0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax0Count", mnmamt0);
        else
            setFieldValue("MNMAMT0", mnmamt0);
    }

//MixAndMatchCount0	MNMCNT0	INT UNSIGNED	N
    public Integer getMixAndMatchCount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("mixAndMatchTax0Count");
        else
            return (Integer)getFieldValue("MNMCNT0");
    }

    public void setMixAndMatchCount0(Integer MixAndMatchCount0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax0Count", MixAndMatchCount0);
        else
            setFieldValue("MNMCNT0", MixAndMatchCount0);
    }

//NotIncludedSales0
    public HYIDouble getNotIncludedSales0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("notIncludedTax0Sale");
        else
            return (HYIDouble)getFieldValue("RCPGIFAMT0");
    }

    public void setNotIncludedSales0(HYIDouble rcpgifamt0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax0Sale", rcpgifamt0);
        else
            setFieldValue("RCPGIFAMT0", rcpgifamt0);
    }

//NetSalesAmount0
    public HYIDouble getNetSalesAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("netSaleTax0Amount");
        else
            return (HYIDouble)getFieldValue("NETSALAMT0");
    }

    public void setNetSalesAmount0(HYIDouble netsalamt0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax0Amount", netsalamt0);
        else
            setFieldValue("NETSALAMT0", netsalamt0);
    }

//1
//SIPercentPlusAmount1	SIPLUSAMT1	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("siPlusTax1Amount");
        else
            return (HYIDouble)getFieldValue("SIPLUSAMT1");
    }

    public void setSIPercentPlusAmount1(HYIDouble SIPercentPlusAmount1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax1Amount", SIPercentPlusAmount1);
        else
            setFieldValue("SIPLUSAMT1", SIPercentPlusAmount1);
    }

//SIPercentPlusCount1	SIPLUSCNT1	INT UNSIGNED	N
    public Integer getSIPercentPlusCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siPlusTax1Count");
        else
            return (Integer)getFieldValue("SIPLUSCNT1");
    }

    public void setSIPercentPlusCount1(Integer SIPercentPlusCount1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax1Count", SIPercentPlusCount1);
        else
            setFieldValue("SIPLUSCNT1", SIPercentPlusCount1);
    }

//SIPercentOffAmount1	SIPAMT1	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("discountTax1Amount");
        else
            return (HYIDouble)getFieldValue("SIPAMT1");
    }

    public void setSIPercentOffAmount1(HYIDouble SIPercentOffAmount1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax1Amount", SIPercentOffAmount1);
        else
            setFieldValue("SIPAMT1", SIPercentOffAmount1);
    }

//SIPercentOffCount1	SIPCNT1	INT UNSIGNED	N
    public Integer getSIPercentOffCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("discountTax1Count");
        else
            return (Integer)getFieldValue("SIPCNT1");
    }

    public void setSIPercentOffCount1(Integer SIPercentOffCount1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax1Count", SIPercentOffCount1);
        else
            setFieldValue("SIPCNT1", SIPercentOffCount1);
    }

//SIDiscountAmount1	SIMAMT1	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("SIMAMT1");
    }

    public void setSIDiscountAmount1(HYIDouble SIDiscountAmount1) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMAMT1", SIDiscountAmount1);
        else
            setFieldValue("SIMAMT1", SIDiscountAmount1);
    }

//SIDiscountCount1	SIMCNT1	INT UNSIGNED	N
    public Integer getSIDiscountCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("SIMCNT1");
    }

    public void setSIDiscountCount1(Integer SIDiscountCount1) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMCNT1", SIDiscountCount1);
        else
            setFieldValue("SIMCNT1", SIDiscountCount1);
    }

//MixAndMatchAmount1
    public HYIDouble getMixAndMatchAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("mixAndMatchTax1Amount");
        else
            return (HYIDouble)getFieldValue("MNMAMT1");
    }

    public void setMixAndMatchAmount1(HYIDouble mnmamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax1Amount", mnmamt1);
        else
            setFieldValue("MNMAMT1", mnmamt1);
    }

//MixAndMatchCount1	MNMCNT1	INT UNSIGNED	N
    public Integer getMixAndMatchCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("mixAndMatchTax1Count");
        else
            return (Integer)getFieldValue("MNMCNT1");
    }

    public void setMixAndMatchCount1(Integer MixAndMatchCount1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax1Count", MixAndMatchCount1);
        else
            setFieldValue("MNMCNT1", MixAndMatchCount1);
    }

//NotIncludedSales1
    public HYIDouble getNotIncludedSales1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("notIncludedTax1Sale");
        else
            return (HYIDouble)getFieldValue("RCPGIFAMT1");
    }

    public void setNotIncludedSales1(HYIDouble rcpgifamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax1Sale", rcpgifamt1);
        else
            setFieldValue("RCPGIFAMT1", rcpgifamt1);
    }

//NetSalesAmount1
    public HYIDouble getNetSalesAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("netSaleTax1Amount");
        else
            return (HYIDouble)getFieldValue("NETSALAMT1");
    }

    public void setNetSalesAmount1(HYIDouble netsalamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax1Amount", netsalamt1);
        else
            setFieldValue("NETSALAMT1", netsalamt1);
    }

//2
//SIPercentPlusAmount2	SIPLUSAMT2	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("siPlusTax2Amount");
        else
            return (HYIDouble)getFieldValue("SIPLUSAMT2");
    }

    public void setSIPercentPlusAmount2(HYIDouble SIPercentPlusAmount2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax2Amount", SIPercentPlusAmount2);
        else
            setFieldValue("SIPLUSAMT2", SIPercentPlusAmount2);
    }

//SIPercentPlusCount2	SIPLUSCNT2	INT UNSIGNED	N
    public Integer getSIPercentPlusCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siPlusTax2Count");
        else
            return (Integer)getFieldValue("SIPLUSCNT2");
    }

    public void setSIPercentPlusCount2(Integer SIPercentPlusCount2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax2Count", SIPercentPlusCount2);
        else
            setFieldValue("SIPLUSCNT2", SIPercentPlusCount2);
    }

//SIPercentOffAmount2	SIPAMT2	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("discountTax2Amount");
        else
            return (HYIDouble)getFieldValue("SIPAMT2");
    }

    public void setSIPercentOffAmount2(HYIDouble SIPercentOffAmount2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax2Amount", SIPercentOffAmount2);
        else
            setFieldValue("SIPAMT2", SIPercentOffAmount2);
    }

//SIPercentOffCount2	SIPCNT2	INT UNSIGNED	N
    public Integer getSIPercentOffCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("discountTax2Count");
        else
            return (Integer)getFieldValue("SIPCNT2");
    }

    public void setSIPercentOffCount2(Integer SIPercentOffCount2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax2Count", SIPercentOffCount2);
        else
            setFieldValue("SIPCNT2", SIPercentOffCount2);
    }

//SIDiscountAmount2	SIMAMT2	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("SIMAMT2");
    }

    public void setSIDiscountAmount2(HYIDouble SIDiscountAmount2) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMAMT2", SIDiscountAmount2);
        else
            setFieldValue("SIMAMT2", SIDiscountAmount2);
    }

//SIDiscountCount2	SIMCNT2	INT UNSIGNED	N
    public Integer getSIDiscountCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("SIMCNT2");
    }

    public void setSIDiscountCount2(Integer SIDiscountCount2) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMCNT2", SIDiscountCount2);
        else
            setFieldValue("SIMCNT2", SIDiscountCount2);
    }

//MixAndMatchAmount2
    public HYIDouble getMixAndMatchAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("mixAndMatchTax2Amount");
        else
            return (HYIDouble)getFieldValue("MNMAMT2");
    }

    public void setMixAndMatchAmount2(HYIDouble mnmamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax2Amount", mnmamt2);
        else
            setFieldValue("MNMAMT2", mnmamt2);
    }

//MixAndMatchCount2	MNMCNT2	INT UNSIGNED	N
    public Integer getMixAndMatchCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("mixAndMatchTax2Count");
        else
            return (Integer)getFieldValue("MNMCNT2");
    }

    public void setMixAndMatchCount2(Integer MixAndMatchCount2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax2Count", MixAndMatchCount2);
        else
            setFieldValue("MNMCNT2", MixAndMatchCount2);
    }

//NotIncludedSales2
    public HYIDouble getNotIncludedSales2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("notIncludedTax2Sale");
        else
            return (HYIDouble)getFieldValue("RCPGIFAMT2");
    }

    public void setNotIncludedSales2(HYIDouble rcpgifamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax2Sale", rcpgifamt2);
        else
            setFieldValue("RCPGIFAMT2", rcpgifamt2);
    }

//NetSalesAmount2
    public HYIDouble getNetSalesAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("netSaleTax2Amount");
        else
            return (HYIDouble)getFieldValue("NETSALAMT2");
    }

    public void setNetSalesAmount2(HYIDouble netsalamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax2Amount", netsalamt2);
        else
            setFieldValue("NETSALAMT2", netsalamt2);
    }

//3
//SIPercentPlusAmount3	SIPLUSAMT3	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("siPlusTax3Amount");
        else
            return (HYIDouble)getFieldValue("SIPLUSAMT3");
    }

    public void setSIPercentPlusAmount3(HYIDouble SIPercentPlusAmount3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax3Amount", SIPercentPlusAmount3);
        else
            setFieldValue("SIPLUSAMT3", SIPercentPlusAmount3);
    }

//SIPercentPlusCount3	SIPLUSCNT3	INT UNSIGNED	N
    public Integer getSIPercentPlusCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siPlusTax3Count");
        else
            return (Integer)getFieldValue("SIPLUSCNT3");
    }

    public void setSIPercentPlusCount3(Integer SIPercentPlusCount3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax3Count", SIPercentPlusCount3);
        else
            setFieldValue("SIPLUSCNT3", SIPercentPlusCount3);
    }

//SIPercentOffAmount3	SIPAMT3	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("discountTax3Amount");
        else
            return (HYIDouble)getFieldValue("SIPAMT3");
    }

    public void setSIPercentOffAmount3(HYIDouble SIPercentOffAmount3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax3Amount", SIPercentOffAmount3);
        else
            setFieldValue("SIPAMT3", SIPercentOffAmount3);
    }

//SIPercentOffCount3	SIPCNT3	INT UNSIGNED	N
    public Integer getSIPercentOffCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("discountTax3Count");
        else
            return (Integer)getFieldValue("SIPCNT3");
    }

    public void setSIPercentOffCount3(Integer SIPercentOffCount3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax3Count", SIPercentOffCount3);
        else
            setFieldValue("SIPCNT3", SIPercentOffCount3);
    }

//SIDiscountAmount3	SIMAMT3	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("SIMAMT3");
    }

    public void setSIDiscountAmount3(HYIDouble SIDiscountAmount3) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMAMT3", SIDiscountAmount3);
        else
            setFieldValue("SIMAMT3", SIDiscountAmount3);
    }

//SIDiscountCount3	SIMCNT3	INT UNSIGNED	N
    public Integer getSIDiscountCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("SIMCNT3");
    }

    public void setSIDiscountCount3(Integer SIDiscountCount3) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMCNT3", SIDiscountCount3);
        else
            setFieldValue("SIMCNT3", SIDiscountCount3);
    }

//MixAndMatchAmount3
    public HYIDouble getMixAndMatchAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("mixAndMatchTax3Amount");
        else
            return (HYIDouble)getFieldValue("MNMAMT3");
    }

    public void setMixAndMatchAmount3(HYIDouble mnmamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax3Amount", mnmamt3);
        else
            setFieldValue("MNMAMT3", mnmamt3);
    }

//MixAndMatchCount3	MNMCNT3	INT UNSIGNED	N
    public Integer getMixAndMatchCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("mixAndMatchTax3Count");
        else
            return (Integer)getFieldValue("MNMCNT3");
    }

    public void setMixAndMatchCount3(Integer MixAndMatchCount3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax3Count", MixAndMatchCount3);
        else
            setFieldValue("MNMCNT3", MixAndMatchCount3);
    }

//NotIncludedSales3
    public HYIDouble getNotIncludedSales3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("notIncludedTax3Sale");
        else
            return (HYIDouble)getFieldValue("RCPGIFAMT3");
    }

    public void setNotIncludedSales3(HYIDouble rcpgifamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax3Sale", rcpgifamt3);
        else
            setFieldValue("RCPGIFAMT3", rcpgifamt3);
    }

//NetSalesAmount3
    public HYIDouble getNetSalesAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("netSaleTax3Amount");
        else
            return (HYIDouble)getFieldValue("NETSALAMT3");
    }

    public void setNetSalesAmount3(HYIDouble netsalamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax3Amount", netsalamt3);
        else
            setFieldValue("NETSALAMT3", netsalamt3);
    }

//4
//SIPercentPlusAmount4	SIPLUSAMT4	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("siPlusTax4Amount");
        else
            return (HYIDouble)getFieldValue("SIPLUSAMT4");
    }

    public void setSIPercentPlusAmount4(HYIDouble SIPercentPlusAmount4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax4Amount", SIPercentPlusAmount4);
        else
            setFieldValue("SIPLUSAMT4", SIPercentPlusAmount4);
    }

//SIPercentPlusCount4	SIPLUSCNT4	INT UNSIGNED	N
    public Integer getSIPercentPlusCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siPlusTax4Count");
        else
            return (Integer)getFieldValue("SIPLUSCNT4");
    }

    public void setSIPercentPlusCount4(Integer SIPercentPlusCount4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax4Count", SIPercentPlusCount4);
        else
            setFieldValue("SIPLUSCNT4", SIPercentPlusCount4);
    }

//SIPercentOffAmount4	SIPAMT4	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("discountTax4Amount");
        else
            return (HYIDouble)getFieldValue("SIPAMT4");
    }

    public void setSIPercentOffAmount4(HYIDouble SIPercentOffAmount4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax4Amount", SIPercentOffAmount4);
        else
            setFieldValue("SIPAMT4", SIPercentOffAmount4);
    }

//SIPercentOffCount4	SIPCNT4	INT UNSIGNED	N
    public Integer getSIPercentOffCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("discountTax4Count");
        else
            return (Integer)getFieldValue("SIPCNT4");
    }

    public void setSIPercentOffCount4(Integer SIPercentOffCount4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax4Count", SIPercentOffCount4);
        else
            setFieldValue("SIPCNT4", SIPercentOffCount4);
    }

//SIDiscountAmount4	SIMAMT4	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("SIMAMT4");
    }

    public void setSIDiscountAmount4(HYIDouble SIDiscountAmount4) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMAMT4", SIDiscountAmount4);
        else
            setFieldValue("SIMAMT4", SIDiscountAmount4);
    }

//SIDiscountCount4	SIMCNT4	INT UNSIGNED	N
    public Integer getSIDiscountCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("SIMCNT4");
    }

    public void setSIDiscountCount4(Integer SIDiscountCount4) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMCNT4", SIDiscountCount4);
        else
            setFieldValue("SIMCNT4", SIDiscountCount4);
    }

//MixAndMatchAmount4
    public HYIDouble getMixAndMatchAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("mixAndMatchTax4Amount");
        else
            return (HYIDouble)getFieldValue("MNMAMT4");
    }

    public void setMixAndMatchAmount4(HYIDouble mnmamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax4Amount", mnmamt4);
        else
            setFieldValue("MNMAMT4", mnmamt4);
    }

//MixAndMatchCount4	MNMCNT4	INT UNSIGNED	N
    public Integer getMixAndMatchCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("mixAndMatchTax4Count");
        else
            return (Integer)getFieldValue("MNMCNT4");
    }

    public void setMixAndMatchCount4(Integer MixAndMatchCount4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax4Count", MixAndMatchCount4);
        else
            setFieldValue("MNMCNT4", MixAndMatchCount4);
    }

//NotIncludedSales4
    public HYIDouble getNotIncludedSales4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("notIncludedTax4Sale");
        else
            return (HYIDouble)getFieldValue("RCPGIFAMT4");
    }

    public void setNotIncludedSales4(HYIDouble rcpgifamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax4Sale", rcpgifamt4);
        else
            setFieldValue("RCPGIFAMT4", rcpgifamt4);
    }

//NetSalesAmount4
    public HYIDouble getNetSalesAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("netSaleTax4Amount");
        else
            return (HYIDouble)getFieldValue("NETSALAMT4");
    }

    public void setNetSalesAmount4(HYIDouble netsalamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax4Amount", netsalamt4);
        else
            setFieldValue("NETSALAMT4", netsalamt4);
    }
//5
//SIPercentPlusAmount5	SIPLUSAMT5	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("siPlusTax5Amount");
        else
            return (HYIDouble)getFieldValue("SIPLUSAMT5");
    }

    public void setSIPercentPlusAmount5(HYIDouble SIPercentPlusAmount5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax5Amount", SIPercentPlusAmount5);
        else
            setFieldValue("SIPLUSAMT5", SIPercentPlusAmount5);
    }

//SIPercentPlusCount5	SIPLUSCNT5	INT UNSIGNED	N
    public Integer getSIPercentPlusCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siPlusTax5Count");
        else
            return (Integer)getFieldValue("SIPLUSCNT5");
    }

    public void setSIPercentPlusCount5(Integer SIPercentPlusCount5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax5Count", SIPercentPlusCount5);
        else
            setFieldValue("SIPLUSCNT5", SIPercentPlusCount5);
    }

//SIPercentOffAmount5	SIPAMT5	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("discountTax5Amount");
        else
            return (HYIDouble)getFieldValue("SIPAMT5");
    }

    public void setSIPercentOffAmount5(HYIDouble SIPercentOffAmount5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax5Amount", SIPercentOffAmount5);
        else
            setFieldValue("SIPAMT5", SIPercentOffAmount5);
    }

//SIPercentOffCount5	SIPCNT5	INT UNSIGNED	N
    public Integer getSIPercentOffCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("discountTax5Count");
        else
            return (Integer)getFieldValue("SIPCNT5");
    }

    public void setSIPercentOffCount5(Integer SIPercentOffCount5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax5Count", SIPercentOffCount5);
        else
            setFieldValue("SIPCNT5", SIPercentOffCount5);
    }

//SIDiscountAmount5	SIMAMT5	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("SIMAMT5");
    }

    public void setSIDiscountAmount5(HYIDouble SIDiscountAmount5) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMAMT5", SIDiscountAmount5);
        else
            setFieldValue("SIMAMT5", SIDiscountAmount5);
    }

//SIDiscountCount5	SIMCNT5	INT UNSIGNED	N
    public Integer getSIDiscountCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("SIMCNT5");
    }

    public void setSIDiscountCount5(Integer SIDiscountCount5) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMCNT5", SIDiscountCount5);
        else
            setFieldValue("SIMCNT5", SIDiscountCount5);
    }

//MixAndMatchAmount5
    public HYIDouble getMixAndMatchAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("mixAndMatchTax5Amount");
        else
            return (HYIDouble)getFieldValue("MNMAMT5");
    }

    public void setMixAndMatchAmount5(HYIDouble mnmamt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax5Amount", mnmamt5);
        else
            setFieldValue("MNMAMT5", mnmamt5);
    }

//MixAndMatchCount5	MNMCNT5	INT UNSIGNED	N
    public Integer getMixAndMatchCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("mixAndMatchTax5Count");
        else
            return (Integer)getFieldValue("MNMCNT5");
    }

    public void setMixAndMatchCount5(Integer MixAndMatchCount5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax5Count", MixAndMatchCount5);
        else
            setFieldValue("MNMCNT5", MixAndMatchCount5);
    }

//NotIncludedSales5
    public HYIDouble getNotIncludedSales5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("notIncludedTax5Sale");
        else
            return (HYIDouble)getFieldValue("RCPGIFAMT5");
    }

    public void setNotIncludedSales5(HYIDouble rcpgifamt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax5Sale", rcpgifamt5);
        else
            setFieldValue("RCPGIFAMT5", rcpgifamt5);
    }

//NetSalesAmount5
    public HYIDouble getNetSalesAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("netSaleTax5Amount");
        else
            return (HYIDouble)getFieldValue("NETSALAMT5");
    }

    public void setNetSalesAmount5(HYIDouble netsalamt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax5Amount", netsalamt5);
        else
            setFieldValue("NETSALAMT5", netsalamt5);
    }
//6
//SIPercentPlusAmount6	SIPLUSAMT6	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("siPlusTax6Amount");
        else
            return (HYIDouble)getFieldValue("SIPLUSAMT6");
    }

    public void setSIPercentPlusAmount6(HYIDouble SIPercentPlusAmount6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax6Amount", SIPercentPlusAmount6);
        else
            setFieldValue("SIPLUSAMT6", SIPercentPlusAmount6);
    }

//SIPercentPlusCount6	SIPLUSCNT6	INT UNSIGNED	N
    public Integer getSIPercentPlusCount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siPlusTax6Count");
        else
            return (Integer)getFieldValue("SIPLUSCNT6");
    }

    public void setSIPercentPlusCount6(Integer SIPercentPlusCount6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax6Count", SIPercentPlusCount6);
        else
            setFieldValue("SIPLUSCNT6", SIPercentPlusCount6);
    }

//SIPercentOffAmount6	SIPAMT6	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("discountTax6Amount");
        else
            return (HYIDouble)getFieldValue("SIPAMT6");
    }

    public void setSIPercentOffAmount6(HYIDouble SIPercentOffAmount6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax6Amount", SIPercentOffAmount6);
        else
            setFieldValue("SIPAMT6", SIPercentOffAmount6);
    }

//SIPercentOffCount6	SIPCNT6	INT UNSIGNED	N
    public Integer getSIPercentOffCount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("discountTax6Count");
        else
            return (Integer)getFieldValue("SIPCNT6");
    }

    public void setSIPercentOffCount6(Integer SIPercentOffCount6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax6Count", SIPercentOffCount6);
        else
            setFieldValue("SIPCNT6", SIPercentOffCount6);
    }

//SIDiscountAmount6	SIMAMT6	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("SIMAMT6");
    }

    public void setSIDiscountAmount6(HYIDouble SIDiscountAmount6) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMAMT6", SIDiscountAmount6);
        else
            setFieldValue("SIMAMT6", SIDiscountAmount6);
    }

//SIDiscountCount6	SIMCNT6	INT UNSIGNED	N
    public Integer getSIDiscountCount6() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("SIMCNT6");
    }

    public void setSIDiscountCount6(Integer SIDiscountCount6) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMCNT6", SIDiscountCount6);
        else
            setFieldValue("SIMCNT6", SIDiscountCount6);
    }

//MixAndMatchAmount6
    public HYIDouble getMixAndMatchAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("mixAndMatchTax6Amount");
        else
            return (HYIDouble)getFieldValue("MNMAMT6");
    }

    public void setMixAndMatchAmount6(HYIDouble mnmamt6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax6Amount", mnmamt6);
        else
            setFieldValue("MNMAMT6", mnmamt6);
    }

//MixAndMatchCount6	MNMCNT6	INT UNSIGNED	N
    public Integer getMixAndMatchCount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("mixAndMatchTax6Count");
        else
            return (Integer)getFieldValue("MNMCNT6");
    }

    public void setMixAndMatchCount6(Integer MixAndMatchCount6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax6Count", MixAndMatchCount6);
        else
            setFieldValue("MNMCNT6", MixAndMatchCount6);
    }

//NotIncludedSales6
    public HYIDouble getNotIncludedSales6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("notIncludedTax6Sale");
        else
            return (HYIDouble)getFieldValue("RCPGIFAMT6");
    }

    public void setNotIncludedSales6(HYIDouble rcpgifamt6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax6Sale", rcpgifamt6);
        else
            setFieldValue("RCPGIFAMT6", rcpgifamt6);
    }

//NetSalesAmount6
    public HYIDouble getNetSalesAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("netSaleTax6Amount");
        else
            return (HYIDouble)getFieldValue("NETSALAMT6");
    }

    public void setNetSalesAmount6(HYIDouble netsalamt6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax6Amount", netsalamt6);
        else
            setFieldValue("NETSALAMT6", netsalamt6);
    }
//7
//SIPercentPlusAmount7	SIPLUSAMT7	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("siPlusTax7Amount");
        else
            return (HYIDouble)getFieldValue("SIPLUSAMT7");
    }

    public void setSIPercentPlusAmount7(HYIDouble SIPercentPlusAmount7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax7Amount", SIPercentPlusAmount7);
        else
            setFieldValue("SIPLUSAMT7", SIPercentPlusAmount7);
    }

//SIPercentPlusCount7	SIPLUSCNT7	INT UNSIGNED	N
    public Integer getSIPercentPlusCount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siPlusTax7Count");
        else
            return (Integer)getFieldValue("SIPLUSCNT7");
    }

    public void setSIPercentPlusCount7(Integer SIPercentPlusCount7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax7Count", SIPercentPlusCount7);
        else
            setFieldValue("SIPLUSCNT7", SIPercentPlusCount7);
    }

//SIPercentOffAmount7	SIPAMT7	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("discountTax7Amount");
        else
            return (HYIDouble)getFieldValue("SIPAMT7");
    }

    public void setSIPercentOffAmount7(HYIDouble SIPercentOffAmount7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax7Amount", SIPercentOffAmount7);
        else
            setFieldValue("SIPAMT7", SIPercentOffAmount7);
    }

//SIPercentOffCount7	SIPCNT7	INT UNSIGNED	N
    public Integer getSIPercentOffCount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("discountTax7Count");
        else
            return (Integer)getFieldValue("SIPCNT7");
    }

    public void setSIPercentOffCount7(Integer SIPercentOffCount7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax7Count", SIPercentOffCount7);
        else
            setFieldValue("SIPCNT7", SIPercentOffCount7);
    }

//SIDiscountAmount7	SIMAMT7	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("SIMAMT7");
    }

    public void setSIDiscountAmount7(HYIDouble SIDiscountAmount7) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMAMT7", SIDiscountAmount7);
        else
            setFieldValue("SIMAMT7", SIDiscountAmount7);
    }

//SIDiscountCount7	SIMCNT7	INT UNSIGNED	N
    public Integer getSIDiscountCount7() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("SIMCNT7");
    }

    public void setSIDiscountCount7(Integer SIDiscountCount7) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMCNT7", SIDiscountCount7);
        else
            setFieldValue("SIMCNT7", SIDiscountCount7);
    }

//MixAndMatchAmount7
    public HYIDouble getMixAndMatchAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("mixAndMatchTax7Amount");
        else
            return (HYIDouble)getFieldValue("MNMAMT7");
    }

    public void setMixAndMatchAmount7(HYIDouble mnmamt7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax7Amount", mnmamt7);
        else
            setFieldValue("MNMAMT7", mnmamt7);
    }

//MixAndMatchCount7	MNMCNT7	INT UNSIGNED	N
    public Integer getMixAndMatchCount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("mixAndMatchTax7Count");
        else
            return (Integer)getFieldValue("MNMCNT7");
    }

    public void setMixAndMatchCount7(Integer MixAndMatchCount7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax7Count", MixAndMatchCount7);
        else
            setFieldValue("MNMCNT7", MixAndMatchCount7);
    }

//NotIncludedSales7
    public HYIDouble getNotIncludedSales7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("notIncludedTax7Sale");
        else
            return (HYIDouble)getFieldValue("RCPGIFAMT7");
    }

    public void setNotIncludedSales7(HYIDouble rcpgifamt7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax7Sale", rcpgifamt7);
        else
            setFieldValue("RCPGIFAMT7", rcpgifamt7);
    }

//NetSalesAmount7
    public HYIDouble getNetSalesAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("netSaleTax7Amount");
        else
            return (HYIDouble)getFieldValue("NETSALAMT7");
    }

    public void setNetSalesAmount7(HYIDouble netsalamt7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax7Amount", netsalamt7);
        else
            setFieldValue("NETSALAMT7", netsalamt7);
    }
//8
//SIPercentPlusAmount8	SIPLUSAMT8	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("siPlusTax8Amount");
        else
            return (HYIDouble)getFieldValue("SIPLUSAMT8");
    }

    public void setSIPercentPlusAmount8(HYIDouble SIPercentPlusAmount8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax8Amount", SIPercentPlusAmount8);
        else
            setFieldValue("SIPLUSAMT8", SIPercentPlusAmount8);
    }

//SIPercentPlusCount8	SIPLUSCNT8	INT UNSIGNED	N
    public Integer getSIPercentPlusCount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siPlusTax8Count");
        else
            return (Integer)getFieldValue("SIPLUSCNT8");
    }

    public void setSIPercentPlusCount8(Integer SIPercentPlusCount8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax8Count", SIPercentPlusCount8);
        else
            setFieldValue("SIPLUSCNT8", SIPercentPlusCount8);
    }

//SIPercentOffAmount8	SIPAMT8	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("discountTax8Amount");
        else
            return (HYIDouble)getFieldValue("SIPAMT8");
    }

    public void setSIPercentOffAmount8(HYIDouble SIPercentOffAmount8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax8Amount", SIPercentOffAmount8);
        else
            setFieldValue("SIPAMT8", SIPercentOffAmount8);
    }

//SIPercentOffCount8	SIPCNT8	INT UNSIGNED	N
    public Integer getSIPercentOffCount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("discountTax8Count");
        else
            return (Integer)getFieldValue("SIPCNT8");
    }

    public void setSIPercentOffCount8(Integer SIPercentOffCount8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax8Count", SIPercentOffCount8);
        else
            setFieldValue("SIPCNT8", SIPercentOffCount8);
    }

//SIDiscountAmount8	SIMAMT8	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("SIMAMT8");
    }

    public void setSIDiscountAmount8(HYIDouble SIDiscountAmount8) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMAMT8", SIDiscountAmount8);
        else
            setFieldValue("SIMAMT8", SIDiscountAmount8);
    }

//SIDiscountCount8	SIMCNT8	INT UNSIGNED	N
    public Integer getSIDiscountCount8() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("SIMCNT8");
    }

    public void setSIDiscountCount8(Integer SIDiscountCount8) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMCNT8", SIDiscountCount8);
        else
            setFieldValue("SIMCNT8", SIDiscountCount8);
    }

//MixAndMatchAmount8
    public HYIDouble getMixAndMatchAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("mixAndMatchTax8Amount");
        else
            return (HYIDouble)getFieldValue("MNMAMT8");
    }

    public void setMixAndMatchAmount8(HYIDouble mnmamt8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax8Amount", mnmamt8);
        else
            setFieldValue("MNMAMT8", mnmamt8);
    }

//MixAndMatchCount8	MNMCNT8	INT UNSIGNED	N
    public Integer getMixAndMatchCount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("mixAndMatchTax8Count");
        else
            return (Integer)getFieldValue("MNMCNT8");
    }

    public void setMixAndMatchCount8(Integer MixAndMatchCount8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax8Count", MixAndMatchCount8);
        else
            setFieldValue("MNMCNT8", MixAndMatchCount8);
    }

//NotIncludedSales8
    public HYIDouble getNotIncludedSales8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("notIncludedTax8Sale");
        else
            return (HYIDouble)getFieldValue("RCPGIFAMT8");
    }

    public void setNotIncludedSales8(HYIDouble rcpgifamt8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax8Sale", rcpgifamt8);
        else
            setFieldValue("RCPGIFAMT8", rcpgifamt8);
    }

//NetSalesAmount8
    public HYIDouble getNetSalesAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("netSaleTax8Amount");
        else
            return (HYIDouble)getFieldValue("NETSALAMT8");
    }

    public void setNetSalesAmount8(HYIDouble netsalamt8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax8Amount", netsalamt8);
        else
            setFieldValue("NETSALAMT8", netsalamt8);
    }
//9
//SIPercentPlusAmount9	SIPLUSAMT9	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("siPlusTax9Amount");
        else
            return (HYIDouble)getFieldValue("SIPLUSAMT9");
    }

    public void setSIPercentPlusAmount9(HYIDouble SIPercentPlusAmount9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax9Amount", SIPercentPlusAmount9);
        else
            setFieldValue("SIPLUSAMT9", SIPercentPlusAmount9);
    }

//SIPercentPlusCount9	SIPLUSCNT9	INT UNSIGNED	N
    public Integer getSIPercentPlusCount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siPlusTax9Count");
        else
            return (Integer)getFieldValue("SIPLUSCNT9");
    }

    public void setSIPercentPlusCount9(Integer SIPercentPlusCount9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax9Count", SIPercentPlusCount9);
        else
            setFieldValue("SIPLUSCNT9", SIPercentPlusCount9);
    }

//SIPercentOffAmount9	SIPAMT9	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("discountTax9Amount");
        else
            return (HYIDouble)getFieldValue("SIPAMT9");
    }

    public void setSIPercentOffAmount9(HYIDouble SIPercentOffAmount9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax9Amount", SIPercentOffAmount9);
        else
            setFieldValue("SIPAMT9", SIPercentOffAmount9);
    }

//SIPercentOffCount9	SIPCNT9	INT UNSIGNED	N
    public Integer getSIPercentOffCount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("discountTax9Count");
        else
            return (Integer)getFieldValue("SIPCNT9");
    }

    public void setSIPercentOffCount9(Integer SIPercentOffCount9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax9Count", SIPercentOffCount9);
        else
            setFieldValue("SIPCNT9", SIPercentOffCount9);
    }

//SIDiscountAmount9	SIMAMT9	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("SIMAMT9");
    }

    public void setSIDiscountAmount9(HYIDouble SIDiscountAmount9) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMAMT9", SIDiscountAmount9);
        else
            setFieldValue("SIMAMT9", SIDiscountAmount9);
    }

//SIDiscountCount9	SIMCNT9	INT UNSIGNED	N
    public Integer getSIDiscountCount9() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("SIMCNT9");
    }

    public void setSIDiscountCount9(Integer SIDiscountCount9) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMCNT9", SIDiscountCount9);
        else
            setFieldValue("SIMCNT9", SIDiscountCount9);
    }

//MixAndMatchAmount9
    public HYIDouble getMixAndMatchAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("mixAndMatchTax9Amount");
        else
            return (HYIDouble)getFieldValue("MNMAMT9");
    }

    public void setMixAndMatchAmount9(HYIDouble mnmamt9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax9Amount", mnmamt9);
        else
            setFieldValue("MNMAMT9", mnmamt9);
    }

//MixAndMatchCount9	MNMCNT9	INT UNSIGNED	N
    public Integer getMixAndMatchCount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("mixAndMatchTax9Count");
        else
            return (Integer)getFieldValue("MNMCNT9");
    }

    public void setMixAndMatchCount9(Integer MixAndMatchCount9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax9Count", MixAndMatchCount9);
        else
            setFieldValue("MNMCNT9", MixAndMatchCount9);
    }

//NotIncludedSales9
    public HYIDouble getNotIncludedSales9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("notIncludedTax9Sale");
        else
            return (HYIDouble)getFieldValue("RCPGIFAMT9");
    }

    public void setNotIncludedSales9(HYIDouble rcpgifamt9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax9Sale", rcpgifamt9);
        else
            setFieldValue("RCPGIFAMT9", rcpgifamt9);
    }

//NetSalesAmount9
    public HYIDouble getNetSalesAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("netSaleTax9Amount");
        else
            return (HYIDouble)getFieldValue("NETSALAMT9");
    }

    public void setNetSalesAmount9(HYIDouble netsalamt9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax9Amount", netsalamt9);
        else
            setFieldValue("NETSALAMT9", netsalamt9);
    }
//10
//SIPercentPlusAmount10	SIPLUSAMT10	DECIMAL(12,2)	N
    public HYIDouble getSIPercentPlusAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("siPlusTax10Amount");
        else
            return (HYIDouble)getFieldValue("SIPLUSAMT10");
    }

    public void setSIPercentPlusAmount10(HYIDouble SIPercentPlusAmount10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax10Amount", SIPercentPlusAmount10);
        else
            setFieldValue("SIPLUSAMT10", SIPercentPlusAmount10);
    }

//SIPercentPlusCount10	SIPLUSCNT10	INT UNSIGNED	N
    public Integer getSIPercentPlusCount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siPlusTax10Count");
        else
            return (Integer)getFieldValue("SIPLUSCNT10");
    }

    public void setSIPercentPlusCount10(Integer SIPercentPlusCount10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax10Count", SIPercentPlusCount10);
        else
            setFieldValue("SIPLUSCNT10", SIPercentPlusCount10);
    }

//SIPercentOffAmount10	SIPAMT10	DECIMAL(12,2)	N
    public HYIDouble getSIPercentOffAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("discountTax10Amount");
        else
            return (HYIDouble)getFieldValue("SIPAMT10");
    }

    public void setSIPercentOffAmount10(HYIDouble SIPercentOffAmount10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax10Amount", SIPercentOffAmount10);
        else
            setFieldValue("SIPAMT10", SIPercentOffAmount10);
    }

//SIPercentOffCount10	SIPCNT10	INT UNSIGNED	N
    public Integer getSIPercentOffCount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("discountTax10Count");
        else
            return (Integer)getFieldValue("SIPCNT10");
    }

    public void setSIPercentOffCount10(Integer SIPercentOffCount10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax10Count", SIPercentOffCount10);
        else
            setFieldValue("SIPCNT10", SIPercentOffCount10);
    }

//SIDiscountAmount10	SIMAMT10	DECIMAL(12,2)	N
    public HYIDouble getSIDiscountAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return new HYIDouble(0);
        else
            return (HYIDouble)getFieldValue("SIMAMT10");
    }

    public void setSIDiscountAmount10(HYIDouble SIDiscountAmount10) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMAMT10", SIDiscountAmount10);
        else
            setFieldValue("SIMAMT10", SIDiscountAmount10);
    }

//SIDiscountCount10	SIMCNT10	INT UNSIGNED	N
    public Integer getSIDiscountCount10() {
        if (hyi.cream.inline.Server.serverExist())
            return new Integer(0);
        else
            return (Integer)getFieldValue("SIMCNT10");
    }

    public void setSIDiscountCount10(Integer SIDiscountCount10) {
        if (hyi.cream.inline.Server.serverExist())
            ;//setFieldValue("SIMCNT10", SIDiscountCount10);
        else
            setFieldValue("SIMCNT10", SIDiscountCount10);
    }

//MixAndMatchAmount10
    public HYIDouble getMixAndMatchAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("mixAndMatchTax10Amount");
        else
            return (HYIDouble)getFieldValue("MNMAMT10");
    }

    public void setMixAndMatchAmount10(HYIDouble mnmamt10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax10Amount", mnmamt10);
        else
            setFieldValue("MNMAMT10", mnmamt10);
    }

//MixAndMatchCount10	MNMCNT10	INT UNSIGNED	N
    public Integer getMixAndMatchCount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("mixAndMatchTax10Count");
        else
            return (Integer)getFieldValue("MNMCNT10");
    }

    public void setMixAndMatchCount10(Integer MixAndMatchCount10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax10Count", MixAndMatchCount10);
        else
            setFieldValue("MNMCNT10", MixAndMatchCount10);
    }

//NotIncludedSales10
    public HYIDouble getNotIncludedSales10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("notIncludedTax10Sale");
        else
            return (HYIDouble)getFieldValue("RCPGIFAMT10");
    }

    public void setNotIncludedSales10(HYIDouble rcpgifamt10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax10Sale", rcpgifamt10);
        else
            setFieldValue("RCPGIFAMT10", rcpgifamt10);
    }

//NetSalesAmount10
    public HYIDouble getNetSalesAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("netSaleTax10Amount");
        else
            return (HYIDouble)getFieldValue("NETSALAMT10");
    }

    public void setNetSalesAmount10(HYIDouble netsalamt10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax10Amount", netsalamt10);
        else
            setFieldValue("NETSALAMT10", netsalamt10);
    }

//礼卷销售

//VoucherAmount	VALPAMT	DECIMAL(12,2)	N
    public HYIDouble getVoucherAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("totalVoucherAmount");
        else
            return (HYIDouble)getFieldValue("VALPAMT");
    }

    public void setVoucherAmount(HYIDouble VoucherAmount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("totalVoucherAmount", VoucherAmount);
        else
            setFieldValue("VALPAMT", VoucherAmount);
    }

//VoucherCount	VALPCNT	INT UNSIGNED	N
    public Integer getVoucherCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("totalVoucherCount");
        else
            return (Integer)getFieldValue("VALPCNT");
    }

    public void setVoucherCount(Integer VoucherCount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("totalVoucherCount", VoucherCount);
        else
            setFieldValue("VALPCNT", VoucherCount);
    }

    //预收
    public HYIDouble getPreRcvAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("preRcvAmount");
        else
            return (HYIDouble)getFieldValue("PRCVAMT");
    }

    public void setPreRcvAmount(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvAmount", amt);
        else
            setFieldValue("PRCVAMT", amt);
    }

    public Integer getPreRcvCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("preRcvCount");
        else
            return (Integer)getFieldValue("PRCVCNT");
    }

    public void setPreRcvCount(Integer cnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvCount", cnt);
        else
            setFieldValue("PRCVCNT", cnt);
    }

    //1
    public HYIDouble getPreRcvDetailAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("preRcvDetailAmount1");
        else
            return (HYIDouble)getFieldValue("PRCVPLUAMT1");
    }

    public void setPreRcvDetailAmount1(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvDetailAmount1", amt);
        else
            setFieldValue("PRCVPLUAMT1", amt);
    }

    public Integer getPreRcvDetailCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("preRcvDetailCount1");
        else
            return (Integer)getFieldValue("PRCVPLUCNT1");
    }

    public void setPreRcvDetailCount1(Integer cnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvDetailCount1", cnt);
        else
            setFieldValue("PRCVPLUCNT1", cnt);
    }

    //2
    public HYIDouble getPreRcvDetailAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("preRcvDetailAmount2");
        else
            return (HYIDouble)getFieldValue("PRCVPLUAMT2");
    }

    public void setPreRcvDetailAmount2(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvDetailAmount2", amt);
        else
            setFieldValue("PRCVPLUAMT2", amt);
    }

    public Integer getPreRcvDetailCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("preRcvDetailCount2");
        else
            return (Integer)getFieldValue("PRCVPLUCNT2");
    }

    public void setPreRcvDetailCount2(Integer cnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvDetailCount2", cnt);
        else
            setFieldValue("PRCVPLUCNT2", cnt);
    }

    //3
    public HYIDouble getPreRcvDetailAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("preRcvDetailAmount3");
        else
            return (HYIDouble)getFieldValue("PRCVPLUAMT3");
    }

    public void setPreRcvDetailAmount3(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvDetailAmount3", amt);
        else
            setFieldValue("PRCVPLUAMT3", amt);
    }

    public Integer getPreRcvDetailCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("preRcvDetailCount3");
        else
            return (Integer)getFieldValue("PRCVPLUCNT3");
    }

    public void setPreRcvDetailCount3(Integer cnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvDetailCount3", cnt);
        else
            setFieldValue("PRCVPLUCNT3", cnt);
    }

    //4
    public HYIDouble getPreRcvDetailAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("preRcvDetailAmount4");
        else
            return (HYIDouble)getFieldValue("PRCVPLUAMT4");
    }

    public void setPreRcvDetailAmount4(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvDetailAmount4", amt);
        else
            setFieldValue("PRCVPLUAMT4", amt);
    }

    public Integer getPreRcvDetailCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("preRcvDetailCount4");
        else
            return (Integer)getFieldValue("PRCVPLUCNT4");
    }

    public void setPreRcvDetailCount4(Integer cnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvDetailCount4", cnt);
        else
            setFieldValue("PRCVPLUCNT4", cnt);
    }

    //5
    public HYIDouble getPreRcvDetailAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("preRcvDetailAmount5");
        else
            return (HYIDouble)getFieldValue("PRCVPLUAMT5");
    }

    public void setPreRcvDetailAmount5(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvDetailAmount5", amt);
        else
            setFieldValue("PRCVPLUAMT5", amt);
    }

    public Integer getPreRcvDetailCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("preRcvDetailCount5");
        else
            return (Integer)getFieldValue("PRCVPLUCNT5");
    }

    public void setPreRcvDetailCount5(Integer cnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvDetailCount5", cnt);
        else
            setFieldValue("PRCVPLUCNT5", cnt);
    }

//代售　－　如电话卡等
    public HYIDouble getDaiShouAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("totalDaiShouAmount");
        else
            return (HYIDouble)getFieldValue("DAISHOUAMT");
    }

    public void setDaiShouAmount(HYIDouble daishouamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("totalDaiShouAmount", daishouamt);
        else
            setFieldValue("DAISHOUAMT", daishouamt);
    }

    public Integer getDaiShouCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("totalDaiShouCount");
        else
            return (Integer)getFieldValue("DAISHOUCNT");
    }

    public void setDaiShouCount(Integer daishoucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("totalDaiShouCount", daishoucnt);
        else
            setFieldValue("DAISHOUCNT", daishoucnt);
    }

    //1
    public HYIDouble getDaiShouDetailAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("daiShouDetailAmount1");
        else
            return (HYIDouble)getFieldValue("DAISHOUPLUAMT1");
    }

    public void setDaiShouDetailAmount1(HYIDouble daishoupluamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouDetailAmount1", daishoupluamt);
        else
            setFieldValue("DAISHOUPLUAMT1", daishoupluamt);
    }

    public Integer getDaiShouDetailCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("daiShouDetailCount1");
        else
            return (Integer)getFieldValue("DAISHOUPLUCNT1");
    }

    public void setDaiShouDetailCount1(Integer daishouplucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouDetailCount1", daishouplucnt);
        else
            setFieldValue("DAISHOUPLUCNT1", daishouplucnt);
    }

    //2
    public HYIDouble getDaiShouDetailAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("daiShouDetailAmount2");
        else
            return (HYIDouble)getFieldValue("DAISHOUPLUAMT2");
    }

    public void setDaiShouDetailAmount2(HYIDouble daishoupluamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouDetailAmount2", daishoupluamt);
        else
            setFieldValue("DAISHOUPLUAMT2", daishoupluamt);
    }

    public Integer getDaiShouDetailCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("daiShouDetailCount2");
        else
            return (Integer)getFieldValue("DAISHOUPLUCNT2");
    }

    public void setDaiShouDetailCount2(Integer daishouplucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouDetailCount2", daishouplucnt);
        else
            setFieldValue("DAISHOUPLUCNT2", daishouplucnt);
    }

    //3
    public HYIDouble getDaiShouDetailAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("daiShouDetailAmount3");
        else
            return (HYIDouble)getFieldValue("DAISHOUPLUAMT3");
    }

    public void setDaiShouDetailAmount3(HYIDouble daishoupluamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouDetailAmount3", daishoupluamt);
        else
            setFieldValue("DAISHOUPLUAMT3", daishoupluamt);
    }

    public Integer getDaiShouDetailCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("daiShouDetailCount3");
        else
            return (Integer)getFieldValue("DAISHOUPLUCNT3");
    }

    public void setDaiShouDetailCount3(Integer daishouplucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouDetailCount3", daishouplucnt);
        else
            setFieldValue("DAISHOUPLUCNT3", daishouplucnt);
    }

    //4
    public HYIDouble getDaiShouDetailAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("daiShouDetailAmount4");
        else
            return (HYIDouble)getFieldValue("DAISHOUPLUAMT4");
    }

    public void setDaiShouDetailAmount4(HYIDouble daishoupluamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouDetailAmount4", daishoupluamt);
        else
            setFieldValue("DAISHOUPLUAMT4", daishoupluamt);
    }

    public Integer getDaiShouDetailCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("daiShouDetailCount4");
        else
            return (Integer)getFieldValue("DAISHOUPLUCNT4");
    }

    public void setDaiShouDetailCount4(Integer daishouplucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouDetailCount4", daishouplucnt);
        else
            setFieldValue("DAISHOUPLUCNT4", daishouplucnt);
    }

    //5
    public HYIDouble getDaiShouDetailAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("daiShouDetailAmount5");
        else
            return (HYIDouble)getFieldValue("DAISHOUPLUAMT5");
    }

    public void setDaiShouDetailAmount5(HYIDouble daishoupluamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouDetailAmount5", daishoupluamt);
        else
            setFieldValue("DAISHOUPLUAMT5", daishoupluamt);
    }

    public Integer getDaiShouDetailCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("daiShouDetailCount5");
        else
            return (Integer)getFieldValue("DAISHOUPLUCNT5");
    }

    public void setDaiShouDetailCount5(Integer daishouplucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouDetailCount5", daishouplucnt);
        else
            setFieldValue("DAISHOUPLUCNT5", daishouplucnt);
    }

//  代收公共事业费
      public HYIDouble getDaiShouAmount2() {
          if (hyi.cream.inline.Server.serverExist())
              return (HYIDouble)getFieldValue("totalDaiShouAmount2");
          else
              return (HYIDouble)getFieldValue("DAISHOUAMT2");
      }

      public void setDaiShouAmount2(HYIDouble daishouamt) {
          if (hyi.cream.inline.Server.serverExist())
              setFieldValue("totalDaiShouAmount2", daishouamt);
          else
              setFieldValue("DAISHOUAMT2", daishouamt);
      }

      public Integer getDaiShouCount2() {
          if (hyi.cream.inline.Server.serverExist())
              return (Integer)getFieldValue("totalDaiShouCount2");
          else
              return (Integer)getFieldValue("DAISHOUCNT2");
      }

      public void setDaiShouCount2(Integer daishoucnt) {
          if (hyi.cream.inline.Server.serverExist())
              setFieldValue("totalDaiShouCount2", daishoucnt);
          else
              setFieldValue("DAISHOUCNT2", daishoucnt);
      }


//代付
    public HYIDouble getDaiFuAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("totalDaiFuAmount");
        else
            return (HYIDouble)getFieldValue("DAIFUAMT");
    }

    public void setDaiFuAmount(HYIDouble daifuamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("totalDaiFuAmount", daifuamt);
        else
            setFieldValue("DAIFUAMT", daifuamt);
    }

    public Integer getDaiFuCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("totalDaiFuCount");
        else
            return (Integer)getFieldValue("DAIFUCNT");
    }

    public void setDaiFuCount(Integer daifucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("totalDaiFuCount", daifucnt);
        else
            setFieldValue("DAIFUCNT", daifucnt);
    }

    //1
    public HYIDouble getDaiFuDetailAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("daiFuDetailAmount1");
        else
            return (HYIDouble)getFieldValue("DAIFUPLUAMT1");
    }
    public void setDaiFuDetailAmount1(HYIDouble daifupluamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiFuDetailAmount1", daifupluamt);
        else
            setFieldValue("DAIFUPLUAMT1", daifupluamt);
    }
    public Integer getDaiFuDetailCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("daiFuDetailCount1");
        else
            return (Integer)getFieldValue("DAIFUPLUCNT1");
    }
    public void setDaiFuDetailCount1(Integer daifuplucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiFuDetailCount1", daifuplucnt);
        else
            setFieldValue("DAIFUPLUCNT1", daifuplucnt);
    }

    //2
    public HYIDouble getDaiFuDetailAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("daiFuDetailAmount2");
        else
            return (HYIDouble)getFieldValue("DAIFUPLUAMT2");
    }
    public void setDaiFuDetailAmount2(HYIDouble daifupluamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiFuDetailAmount2", daifupluamt);
        else
            setFieldValue("DAIFUPLUAMT2", daifupluamt);
    }
    public Integer getDaiFuDetailCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("daiFuDetailCount2");
        else
            return (Integer)getFieldValue("DAIFUPLUCNT2");
    }
    public void setDaiFuDetailCount2(Integer daifuplucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiFuDetailCount2", daifuplucnt);
        else
            setFieldValue("DAIFUPLUCNT2", daifuplucnt);
    }
    //3
    public HYIDouble getDaiFuDetailAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("daiFuDetailAmount3");
        else
            return (HYIDouble)getFieldValue("DAIFUPLUAMT3");
    }
    public void setDaiFuDetailAmount3(HYIDouble daifupluamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiFuDetailAmount3", daifupluamt);
        else
            setFieldValue("DAIFUPLUAMT3", daifupluamt);
    }
    public Integer getDaiFuDetailCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("daiFuDetailCount3");
        else
            return (Integer)getFieldValue("DAIFUPLUCNT3");
    }
    public void setDaiFuDetailCount3(Integer daifuplucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiFuDetailCount3", daifuplucnt);
        else
            setFieldValue("DAIFUPLUCNT3", daifuplucnt);
    }
    //4
    public HYIDouble getDaiFuDetailAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("daiFuDetailAmount4");
        else
            return (HYIDouble)getFieldValue("DAIFUPLUAMT4");
    }
    public void setDaiFuDetailAmount4(HYIDouble daifupluamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiFuDetailAmount4", daifupluamt);
        else
            setFieldValue("DAIFUPLUAMT4", daifupluamt);
    }
    public Integer getDaiFuDetailCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("daiFuDetailCount4");
        else
            return (Integer)getFieldValue("DAIFUPLUCNT4");
    }
    public void setDaiFuDetailCount4(Integer daifuplucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiFuDetailCount4", daifuplucnt);
        else
            setFieldValue("DAIFUPLUCNT4", daifuplucnt);
    }
    //5
    public HYIDouble getDaiFuDetailAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("daiFuDetailAmount5");
        else
            return (HYIDouble)getFieldValue("DAIFUPLUAMT5");
    }
    public void setDaiFuDetailAmount5(HYIDouble daifupluamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiFuDetailAmount5", daifupluamt);
        else
            setFieldValue("DAIFUPLUAMT5", daifupluamt);
    }
    public Integer getDaiFuDetailCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("daiFuDetailCount5");
        else
            return (Integer)getFieldValue("DAIFUPLUCNT5");
    }
    public void setDaiFuDetailCount5(Integer daifuplucnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiFuDetailCount5", daifuplucnt);
        else
            setFieldValue("DAIFUPLUCNT5", daifuplucnt);
    }

//PainIn

//PaidinAmount
    public HYIDouble getPaidInAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("totalPaidinAmount");
        else
            return (HYIDouble)getFieldValue("PINAMT");
    }

    public void setPaidInAmount(HYIDouble pinamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("totalPaidinAmount", pinamt);
        else
            setFieldValue("PINAMT", pinamt);
    }

//PaidInCount
    public Integer getPaidInCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("totalPaidinCount");
        else
            return (Integer)getFieldValue("PINCNT");
    }

    public void setPaidInCount(Integer pincnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("totalPaidinCount", pincnt);
        else
            setFieldValue("PINCNT", pincnt);
    }

//PaidInDetailAmount1
    public HYIDouble getPaidInDetailAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("paidInDetailAmount1");
        else
            return (HYIDouble)getFieldValue("PINPLUAMT1");
    }
    public void setPaidInDetailAmount1(HYIDouble pinpluamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidInDetailAmount1", pinpluamt1);
        else
            setFieldValue("PINPLUAMT1", pinpluamt1);
    }
//PaidInDetailCount1
    public Integer getPaidInDetailCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("paidInDetailCount1");
        else
            return (Integer)getFieldValue("PINPLUCNT1");
    }
    public void setPaidInDetailCount1(Integer pinplucnt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidInDetailCount1", pinplucnt1);
        else
            setFieldValue("PINPLUCNT1", pinplucnt1);
    }

//PaidInDetailAmount2
    public HYIDouble getPaidInDetailAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("paidInDetailAmount2");
        else
            return (HYIDouble)getFieldValue("PINPLUAMT2");
    }
    public void setPaidInDetailAmount2(HYIDouble pinpluamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidInDetailAmount2", pinpluamt2);
        else
            setFieldValue("PINPLUAMT2", pinpluamt2);
    }
//PaidInDetailCount2
    public Integer getPaidInDetailCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("paidInDetailCount2");
        else
            return (Integer)getFieldValue("PINPLUCNT2");
    }
    public void setPaidInDetailCount2(Integer pinplucnt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidInDetailCount2", pinplucnt2);
        else
            setFieldValue("PINPLUCNT2", pinplucnt2);
    }

//PaidInDetailAmount3
    public HYIDouble getPaidInDetailAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("paidInDetailAmount3");
        else
            return (HYIDouble)getFieldValue("PINPLUAMT3");
    }
    public void setPaidInDetailAmount3(HYIDouble pinpluamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidInDetailAmount3", pinpluamt3);
        else
            setFieldValue("PINPLUAMT3", pinpluamt3);
    }
//PaidInDetailCount3
    public Integer getPaidInDetailCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("paidInDetailCount3");
        else
            return (Integer)getFieldValue("PINPLUCNT3");
    }
    public void setPaidInDetailCount3(Integer pinplucnt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidInDetailCount3", pinplucnt3);
        else
            setFieldValue("PINPLUCNT3", pinplucnt3);
    }

//PaidInDetailAmount4
    public HYIDouble getPaidInDetailAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("paidInDetailAmount4");
        else
            return (HYIDouble)getFieldValue("PINPLUAMT4");
    }
    public void setPaidInDetailAmount4(HYIDouble pinpluamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidInDetailAmount4", pinpluamt4);
        else
            setFieldValue("PINPLUAMT4", pinpluamt4);
    }
//PaidInDetailCount4
    public Integer getPaidInDetailCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("paidInDetailCount4");
        else
            return (Integer)getFieldValue("PINPLUCNT4");
    }
    public void setPaidInDetailCount4(Integer pinplucnt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidInDetailCount4", pinplucnt4);
        else
            setFieldValue("PINPLUCNT4", pinplucnt4);
    }

//PaidInDetailAmount5
    public HYIDouble getPaidInDetailAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("paidInDetailAmount5");
        else
            return (HYIDouble)getFieldValue("PINPLUAMT5");
    }
    public void setPaidInDetailAmount5(HYIDouble pinpluamt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidInDetailAmount5", pinpluamt5);
        else
            setFieldValue("PINPLUAMT5", pinpluamt5);
    }
//PaidInDetailCount5
    public Integer getPaidInDetailCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("paidInDetailCount5");
        else
            return (Integer)getFieldValue("PINPLUCNT5");
    }
    public void setPaidInDetailCount5(Integer pinplucnt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidInDetailCount5", pinplucnt5);
        else
            setFieldValue("PINPLUCNT5", pinplucnt5);
    }

//PaidOut

//PaidOutAmount
    public HYIDouble getPaidOutAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("totalPaidoutAmount");
        else
            return (HYIDouble)getFieldValue("POUTAMT");
    }

    public void setPaidOutAmount(HYIDouble poutamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("totalPaidoutAmount", poutamt);
        else
            setFieldValue("POUTAMT", poutamt);
    }

//PaidOutCount
    public Integer getPaidOutCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("totalPaidoutCount");
        else
            return (Integer)getFieldValue("POUTCNT");
    }

    public void setPaidOutCount(Integer poutcnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("totalPaidoutCount", poutcnt);
        else
            setFieldValue("POUTCNT", poutcnt);
    }

//PaidOutDetailAmount1
    public HYIDouble getPaidOutDetailAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("paidOutDetailAmount1");
        else
            return (HYIDouble)getFieldValue("POUTPLUAMT1");
    }
    public void setPaidOutDetailAmount1(HYIDouble poutpluamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidOutDetailAmount1", poutpluamt1);
        else
            setFieldValue("POUTPLUAMT1", poutpluamt1);
    }
//PaidOutDetailCount1
    public Integer getPaidOutDetailCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("paidOutDetailCount1");
        else
            return (Integer)getFieldValue("POUTPLUCNT1");
    }
    public void setPaidOutDetailCount1(Integer poutplucnt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidOutDetailCount1", poutplucnt1);
        else
            setFieldValue("POUTPLUCNT1", poutplucnt1);
    }

//PaidOutDetailAmount2
    public HYIDouble getPaidOutDetailAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("paidOutDetailAmount2");
        else
            return (HYIDouble)getFieldValue("POUTPLUAMT2");
    }
    public void setPaidOutDetailAmount2(HYIDouble poutpluamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidOutDetailAmount2", poutpluamt2);
        else
            setFieldValue("POUTPLUAMT2", poutpluamt2);
    }
//PaidOutDetailCount2
    public Integer getPaidOutDetailCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("paidOutDetailCount2");
        else
            return (Integer)getFieldValue("POUTPLUCNT2");
    }
    public void setPaidOutDetailCount2(Integer poutplucnt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidOutDetailCount2", poutplucnt2);
        else
            setFieldValue("POUTPLUCNT2", poutplucnt2);
    }

//PaidOutDetailAmount3
    public HYIDouble getPaidOutDetailAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("paidOutDetailAmount3");
        else
            return (HYIDouble)getFieldValue("POUTPLUAMT3");
    }
    public void setPaidOutDetailAmount3(HYIDouble poutpluamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidOutDetailAmount3", poutpluamt3);
        else
            setFieldValue("POUTPLUAMT3", poutpluamt3);
    }
//PaidOutDetailCount3
    public Integer getPaidOutDetailCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("paidOutDetailCount3");
        else
            return (Integer)getFieldValue("POUTPLUCNT3");
    }
    public void setPaidOutDetailCount3(Integer poutplucnt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidOutDetailCount3", poutplucnt3);
        else
            setFieldValue("POUTPLUCNT3", poutplucnt3);
    }

//PaidOutDetailAmount4
    public HYIDouble getPaidOutDetailAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("paidOutDetailAmount4");
        else
            return (HYIDouble)getFieldValue("POUTPLUAMT4");
    }
    public void setPaidOutDetailAmount4(HYIDouble poutpluamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidOutDetailAmount4", poutpluamt4);
        else
            setFieldValue("POUTPLUAMT4", poutpluamt4);
    }
//PaidOutDetailCount4
    public Integer getPaidOutDetailCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("paidOutDetailCount4");
        else
            return (Integer)getFieldValue("POUTPLUCNT4");
    }
    public void setPaidOutDetailCount4(Integer poutplucnt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidOutDetailCount4", poutplucnt4);
        else
            setFieldValue("POUTPLUCNT4", poutplucnt4);
    }

//PaidOutDetailAmount5
    public HYIDouble getPaidOutDetailAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("paidOutDetailAmount5");
        else
            return (HYIDouble)getFieldValue("POUTPLUAMT5");
    }
    public void setPaidOutDetailAmount5(HYIDouble poutpluamt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidOutDetailAmount5", poutpluamt5);
        else
            setFieldValue("POUTPLUAMT5", poutpluamt5);
    }
//PaidOutDetailCount5
    public Integer getPaidOutDetailCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("paidOutDetailCount5");
        else
            return (Integer)getFieldValue("POUTPLUCNT5");
    }
    public void setPaidOutDetailCount5(Integer poutplucnt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("paidOutDetailCount5", poutplucnt5);
        else
            setFieldValue("POUTPLUCNT5", poutplucnt5);
    }

//操作明细

//ExchangeAmount
    public HYIDouble getExchangeAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("exchangeItemAmount");
        else
            return (HYIDouble)getFieldValue("EXGAMT");
    }

    public void setExchangeAmount(HYIDouble exgamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("exchangeItemAmount", exgamt);
        else
            setFieldValue("EXGAMT", exgamt);
    }

//ExchangeItemQuantity
    public HYIDouble getExchangeItemQuantity() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("exchangeItemQuantity");
        else
            return (HYIDouble)getFieldValue("EXGITEMQTY");
    }

    public void setExchangeItemQuantity(HYIDouble exgitemqty) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("exchangeItemQuantity", exgitemqty);
        else
            setFieldValue("EXGITEMQTY", exgitemqty);
    }

//ReturnAmount	RTNAMT	DECIMAL(12,2)	N
    public HYIDouble getReturnAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("returnItemAmount");
        else
            return (HYIDouble)getFieldValue("RTNAMT");
    }

    public void setReturnAmount(HYIDouble returnAmount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("returnItemAmount", returnAmount);
        else
            setFieldValue("RTNAMT", returnAmount);
    }

//ReturnItemQuantity
    public HYIDouble getReturnItemQuantity() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("returnItemQuantity");
        else
            return (HYIDouble)getFieldValue("RTNITEMQTY");
    }

    public void setReturnItemQuantity(HYIDouble rtnitemqty) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("returnItemQuantity", rtnitemqty);
        else
            setFieldValue("RTNITEMQTY", rtnitemqty);
    }

//ReturnLineItemCount
    /*public HYIDouble getReturnLineItemCount() {
        return (HYIDouble)getFieldValue("RTNLINECNT");
    }

    public void setReturnLineItemCount(HYIDouble rtnlinecnt) {
        setFieldValue("RTNLINECNT", rtnlinecnt);
    }*/

//ReturnTransactionCount RTNTRANCNT INT UNSIGNED	N
    public Integer getReturnTransactionCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("returnTransactionCount");
        else
            return (Integer)getFieldValue("RTNTRANCNT");
    }

    public void setReturnTransactionCount(Integer returnCount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("returnTransactionCount", returnCount);
        else
            setFieldValue("RTNTRANCNT", returnCount);
    }

//TransactionVoidAmount	VOIDINVAMT	DECIMAL(12,2)	N
    public HYIDouble getTransactionVoidAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("transactionVoidAmount");
        else
            return (HYIDouble)getFieldValue("VOIDINVAMT");
    }

    public void setTransactionVoidAmount(HYIDouble VoidAmount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("transactionVoidAmount", VoidAmount);
        else
            setFieldValue("VOIDINVAMT", VoidAmount);
    }


//TransactionVoidCount	VOIDINVCNT	INT UNSIGNED	N	发票作废次数
    public Integer getTransactionVoidCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("transactionVoidCount");
        else
            return (Integer)getFieldValue("VOIDINVCNT");
    }

    public void setTransactionVoidCount(Integer VoidCount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("transactionVoidCount", VoidCount);
        else
            setFieldValue("VOIDINVCNT", VoidCount);
    }

//TransactionCancelAmount	CANCELAMT	DECIMAL(12,2)	N	交易取消金额
    public HYIDouble getTransactionCancelAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("transactionCancelAmount");
        else
            return (HYIDouble)getFieldValue("CANCELAMT");
    }

    public void setTransactionCancelAmount(HYIDouble transactionCancelAmount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("transactionCancelAmount", transactionCancelAmount);
        else
            setFieldValue("CANCELAMT", transactionCancelAmount);
    }

//TransactionCancelCount	CANCELCNT	INT UNSIGNED	N	交易取消次数
    public Integer getTransactionCancelCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("transactionCancelCount");
        else
            return (Integer)getFieldValue("CANCELCNT");
    }

    public void setTransactionCancelCount(Integer transactionCancelCount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("transactionCancelCount", transactionCancelCount);
        else
            setFieldValue("CANCELCNT", transactionCancelCount);
    }

//LineVoidAmount	UPDAMT	DECIMAL(12,2)	N
    public HYIDouble getLineVoidAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("lineVoidAmount");
        else
            return (HYIDouble)getFieldValue("UPDAMT");
    }

    public void setLineVoidAmount(HYIDouble LineVoidAmount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("lineVoidAmount", LineVoidAmount);
        else
            setFieldValue("UPDAMT", LineVoidAmount);
    }

//LineVoidCount	UPDCNT	INT UNSIGNED	N
    public Integer getLineVoidCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("lineVoidCount");
        else
            return (Integer) getFieldValue("UPDCNT");
    }

    public void setLineVoidCount(Integer LineVoidCount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("lineVoidCount",LineVoidCount);
        else
            setFieldValue("UPDCNT",LineVoidCount);
    }

//VoidAmount	NOWUPDAMT	DECIMAL(12,2)	N
    public HYIDouble getVoidAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("voidAmount");
        else
            return (HYIDouble)getFieldValue("NOWUPDAMT");
    }

    public void setVoidAmount(HYIDouble VoidAmount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("voidAmount", VoidAmount);
        else
            setFieldValue("NOWUPDAMT", VoidAmount);
    }

//VoidCount	NOWUPDCNT	INT UNSIGNED	N
    public Integer getVoidCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("voidCount");
        else
            return (Integer) getFieldValue("NOWUPDCNT");
    }

    public void setVoidCount(Integer VoidCount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("voidCount",VoidCount);
        else
            setFieldValue("NOWUPDCNT",VoidCount);
    }

//ManualEntryAmount
    public HYIDouble getManualEntryAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("manualEntryAmount");
        else
            return (HYIDouble)getFieldValue("ENTRYAMT");
    }

    public void setManualEntryAmount(HYIDouble entryamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("manualEntryAmount", entryamt);
        else
            setFieldValue("ENTRYAMT", entryamt);
    }

//ManualEntryCount
    public Integer getManualEntryCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("manualEntryCount");
        else
            return (Integer)getFieldValue("ENTRYCNT");
    }

    public void setManualEntryCount(Integer entrycnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("manualEntryCount", entrycnt);
        else
            setFieldValue("ENTRYCNT", entrycnt);
    }

//PriceOpenEntryAmount
    public HYIDouble getPriceOpenEntryAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("priceOpenEntryAmount");
        else
            return (HYIDouble)getFieldValue("POENTRYAMT");
    }

    public void setPriceOpenEntryAmount(HYIDouble poentryamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("priceOpenEntryAmount", poentryamt);
        else
            setFieldValue("POENTRYAMT", poentryamt);
    }

//PriceOpenEntryCount
    public Integer getPriceOpenEntryCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("priceOpenEntryCount");
        else
            return (Integer)getFieldValue("POENTRYCNT");
    }

    public void setPriceOpenEntryCount(Integer poentrycnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("priceOpenEntryCount", poentrycnt);
        else
            setFieldValue("POENTRYCNT", poentrycnt);
    }

//CashReturnAmount
    public HYIDouble getCashReturnAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("cashReturnAmount");
        else
            return (HYIDouble)getFieldValue("RTNCSHAMT");
    }

    public void setCashReturnAmount(HYIDouble rtncshamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("cashReturnAmount", rtncshamt);
        else
            setFieldValue("RTNCSHAMT", rtncshamt);
    }

//CashReturnCount
    public Integer getCashReturnCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("cashReturnCount");
        else
            return (Integer)getFieldValue("RTNCSHCNT");
    }

    public void setCashReturnCount(Integer rtncshcnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("cashReturnCount", rtncshcnt);
        else
            setFieldValue("RTNCSHCNT", rtncshcnt);
    }

//DrawerOpenCount	DROPENCNT	INT UNSIGNED	N
    public Integer getDrawerOpenCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("drawerOpenCount");
        else
            return (Integer)getFieldValue("DROPENCNT");
    }

    public void setDrawerOpenCount(Integer DrawerOpenCount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("drawerOpenCount", DrawerOpenCount);
        else
            setFieldValue("DROPENCNT", DrawerOpenCount);
    }

//ReprintAmount
    public HYIDouble getReprintAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("reprintAmount");
        else
            return (HYIDouble)getFieldValue("REPRTAMT");
    }

    public void setReprintAmount(HYIDouble reprtamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("reprintAmount", reprtamt);
        else
            setFieldValue("REPRTAMT", reprtamt);
    }

//ReprintCount	REPRTCNT	INT UNSIGNED	N	重印次数	③
    public Integer getReprintCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("reprintCount");
        else
            return (Integer)getFieldValue("REPRTCNT");
    }

    public void setReprintCount(Integer reprintCount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("reprintCount", reprintCount);
        else
            setFieldValue("REPRTCNT", reprintCount);
    }

//CashInAmount
    public HYIDouble getCashInAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("cashInAmount");
        else
            return (HYIDouble)getFieldValue("CASHINAMT");
    }

    public void setCashInAmount(HYIDouble cashinamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("cashInAmount", cashinamt);
        else
            setFieldValue("CASHINAMT", cashinamt);
    }

//CashInCount
    public Integer getCashInCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("cashInCount");
        else
            return (Integer)getFieldValue("CASHINCNT");
    }

    public void setCashInCount(Integer cashincnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("cashInCount", cashincnt);
        else
            setFieldValue("CASHINCNT", cashincnt);
    }

//CashOutAmount
    public HYIDouble getCashOutAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("cashOutAmount");
        else
            return (HYIDouble)getFieldValue("CASHOUTAMT");
    }

    public void setCashOutAmount(HYIDouble cashoutamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("cashOutAmount", cashoutamt);
        else
            setFieldValue("CASHOUTAMT", cashoutamt);
    }

//CashOutCount
    public Integer getCashOutCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("cashOutCount");
        else
            return (Integer)getFieldValue("CASHOUTCNT");
    }

    public void setCashOutCount(Integer cashoutcnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("cashOutCount", cashoutcnt);
        else
            setFieldValue("CASHOUTCNT", cashoutcnt);
    }

//支付明细项目

//SpillAmount	OVERAMT	DECIMAL(12,2)	N
   public HYIDouble getSpillAmount() {
       if (hyi.cream.inline.Server.serverExist())
           return (HYIDouble)getFieldValue("spillAmount");
       else
           return (HYIDouble)getFieldValue("OVERAMT");
   }

   public void setSpillAmount(HYIDouble SpillAmount ) {
       if (hyi.cream.inline.Server.serverExist())
           setFieldValue("spillAmount", SpillAmount);
       else
           setFieldValue("OVERAMT", SpillAmount);
   }

//SpillCount	OVERCNT	INT UNSIGNED	N
    public Integer getSpillCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("spillCount");
        else
            return (Integer)getFieldValue("OVERCNT");
    }

    public void setSpillCount(Integer SpillCount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("spillCount", SpillCount);
        else
            setFieldValue("OVERCNT", SpillCount);
    }

//ExchangeRate1	RATE1	DECIMAL(8,2)	Y
    public HYIDouble getExchangeRate1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("exchangeRate1");
        else
            return (HYIDouble) getFieldValue("RATE1");
    }
    public void setExchangeRate1(HYIDouble ExchangeRate1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("exchangeRate1", ExchangeRate1);
        else
            setFieldValue("RATE1", ExchangeRate1);
    }

//ExchangeRate2	RATE2	DECIMAL(8,2)	Y
    public HYIDouble getExchangeRate2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("exchangeRate2");
        else
            return (HYIDouble) getFieldValue("RATE2");
    }
    public void setExchangeRate2(HYIDouble ExchangeRate2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("exchangeRate2", ExchangeRate2);
        else
            setFieldValue("RATE2", ExchangeRate2);
    }

//ExchangeRate3	RATE3	DECIMAL(8,2)	Y
    public HYIDouble getExchangeRate3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("exchangeRate3");
        else
            return (HYIDouble) getFieldValue("RATE3");
    }
    public void setExchangeRate3(HYIDouble ExchangeRate3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("exchangeRate3", ExchangeRate3);
        else
            setFieldValue("RATE3", ExchangeRate3);
    }

//ExchangeRate4	RATE4	DECIMAL(8,2)	Y
    public HYIDouble getExchangeRate4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("exchangeRate4");
        else
            return (HYIDouble) getFieldValue("RATE4");
    }
    public void setExchangeRate4(HYIDouble ExchangeRate4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("exchangeRate4", ExchangeRate4);
        else
            setFieldValue("RATE4", ExchangeRate4);
    }

//Pay00Amount	PAY00AMT	DECIMAL(12,2)	N
    public HYIDouble getPay00Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay00Amount");
        else
            return (HYIDouble)getFieldValue("PAY00AMT");
    }
    public void setPay00Amount(HYIDouble Pay00Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay00Amount", Pay00Amount);
        else
            setFieldValue("PAY00AMT", Pay00Amount);
    }
//Pay00Count	PAY00CNT	INT UNSIGNED	N
    public Integer getPay00Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay00Count");
        else
            return (Integer)getFieldValue("PAY00CNT");
    }
    public void setPay00Count(Integer Pay00Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay00Count", Pay00Count);
        else
            setFieldValue("PAY00CNT", Pay00Count);
    }

//Pay01Amount	PAY01AMT	DECIMAL(12,2)	N
    public HYIDouble getPay01Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay01Amount");
        else
            return (HYIDouble)getFieldValue("PAY01AMT");
    }
    public void setPay01Amount(HYIDouble Pay01Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay01Amount", Pay01Amount);
        else
            setFieldValue("PAY01AMT", Pay01Amount);
    }
//Pay01Count	PAY01CNT	INT UNSIGNED	N
    public Integer getPay01Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay01Count");
        else
            return (Integer)getFieldValue("PAY01CNT");
    }
    public void setPay01Count(Integer Pay01Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay01Count", Pay01Count);
        else
            setFieldValue("PAY01CNT", Pay01Count);
    }

//Pay02Amount	PAY02AMT	DECIMAL(12,2)	N
    public HYIDouble getPay02Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay02Amount");
        else
            return (HYIDouble)getFieldValue("PAY02AMT");
    }
    public void setPay02Amount(HYIDouble Pay02Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay02Amount", Pay02Amount);
        else
            setFieldValue("PAY02AMT", Pay02Amount);
    }
//Pay02Count	PAY02CNT	INT UNSIGNED	N
    public Integer getPay02Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay02Count");
        else
            return (Integer)getFieldValue("PAY02CNT");
    }
    public void setPay02Count(Integer Pay02Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay02Count", Pay02Count);
        else
            setFieldValue("PAY02CNT", Pay02Count);
    }

//Pay03Amount	PAY03AMT	DECIMAL(12,2)	N
    public HYIDouble getPay03Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay03Amount");
        else
            return (HYIDouble)getFieldValue("PAY03AMT");
    }
    public void setPay03Amount(HYIDouble Pay03Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay03Amount", Pay03Amount);
        else
            setFieldValue("PAY03AMT", Pay03Amount);
    }
//Pay03Count	PAY03CNT	INT UNSIGNED	N
    public Integer getPay03Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay03Count");
        else
            return (Integer)getFieldValue("PAY03CNT");
    }
    public void setPay03Count(Integer Pay03Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay03Count", Pay03Count);
        else
            setFieldValue("PAY03CNT", Pay03Count);
    }

//Pay04Amount	PAY04AMT	DECIMAL(12,2)	N
    public HYIDouble getPay04Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay04Amount");
        else
            return (HYIDouble)getFieldValue("PAY04AMT");
    }
    public void setPay04Amount(HYIDouble Pay04Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay04Amount", Pay04Amount);
        else
            setFieldValue("PAY04AMT", Pay04Amount);
    }
//Pay04Count	PAY04CNT	INT UNSIGNED	N
    public Integer getPay04Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay04Count");
        else
            return (Integer)getFieldValue("PAY04CNT");
    }
    public void setPay04Count(Integer Pay04Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay04Count", Pay04Count);
        else
            setFieldValue("PAY04CNT", Pay04Count);
    }

//Pay05Amount	PAY05AMT	DECIMAL(12,2)	N
    public HYIDouble getPay05Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay05Amount");
        else
            return (HYIDouble)getFieldValue("PAY05AMT");
    }
    public void setPay05Amount(HYIDouble Pay05Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay05Amount", Pay05Amount);
        else
            setFieldValue("PAY05AMT", Pay05Amount);
    }
//Pay05Count	PAY05CNT	INT UNSIGNED	N
    public Integer getPay05Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay05Count");
        else
            return (Integer)getFieldValue("PAY05CNT");
    }
    public void setPay05Count(Integer Pay05Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay05Count", Pay05Count);
        else
            setFieldValue("PAY05CNT", Pay05Count);
    }

//Pay06Amount	PAY06AMT	DECIMAL(12,2)	N
    public HYIDouble getPay06Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay06Amount");
        else
            return (HYIDouble)getFieldValue("PAY06AMT");
    }
    public void setPay06Amount(HYIDouble Pay06Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay06Amount", Pay06Amount);
        else
            setFieldValue("PAY06AMT", Pay06Amount);
    }
//Pay06Count	PAY06CNT	INT UNSIGNED	N
    public Integer getPay06Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay06Count");
        else
            return (Integer)getFieldValue("PAY06CNT");
    }
    public void setPay06Count(Integer Pay06Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay06Count", Pay06Count);
        else
            setFieldValue("PAY06CNT", Pay06Count);
    }

//Pay07Amount	PAY07AMT	DECIMAL(12,2)	N
    public HYIDouble getPay07Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay07Amount");
        else
            return (HYIDouble)getFieldValue("PAY07AMT");
    }
    public void setPay07Amount(HYIDouble Pay07Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay07Amount", Pay07Amount);
        else
            setFieldValue("PAY07AMT", Pay07Amount);
    }
//Pay07Count	PAY07CNT	INT UNSIGNED	N
    public Integer getPay07Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay07Count");
        else
            return (Integer)getFieldValue("PAY07CNT");
    }
    public void setPay07Count(Integer Pay07Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay07Count", Pay07Count);
        else
            setFieldValue("PAY07CNT", Pay07Count);
    }

//Pay08Amount	PAY08AMT	DECIMAL(12,2)	N
    public HYIDouble getPay08Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay08Amount");
        else
            return (HYIDouble)getFieldValue("PAY08AMT");
    }
    public void setPay08Amount(HYIDouble Pay08Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay08Amount", Pay08Amount);
        else
            setFieldValue("PAY08AMT", Pay08Amount);
    }
//Pay08Count	PAY08CNT	INT UNSIGNED	N
    public Integer getPay08Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay08Count");
        else
            return (Integer)getFieldValue("PAY08CNT");
    }
    public void setPay08Count(Integer Pay08Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay08Count", Pay08Count);
        else
            setFieldValue("PAY08CNT", Pay08Count);
    }

//Pay09Amount	PAY09AMT	DECIMAL(12,2)	N
    public HYIDouble getPay09Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay09Amount");
        else
            return (HYIDouble)getFieldValue("PAY09AMT");
    }
    public void setPay09Amount(HYIDouble Pay09Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay09Amount", Pay09Amount);
        else
            setFieldValue("PAY09AMT", Pay09Amount);
    }
//Pay09Count	PAY09CNT	INT UNSIGNED	N
    public Integer getPay09Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay09Count");
        else
            return (Integer)getFieldValue("PAY09CNT");
    }
    public void setPay09Count(Integer Pay09Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay09Count", Pay09Count);
        else
            setFieldValue("PAY09CNT", Pay09Count);
    }

//Pay10Amount	PAY10AMT	DECIMAL(12,2)	N
    public HYIDouble getPay10Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay10Amount");
        else
            return (HYIDouble)getFieldValue("PAY10AMT");
    }
    public void setPay10Amount(HYIDouble Pay10Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay10Amount", Pay10Amount);
        else
            setFieldValue("PAY10AMT", Pay10Amount);
    }
//Pay10Count	PAY10CNT	INT UNSIGNED	N
    public Integer getPay10Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay10Count");
        else
            return (Integer)getFieldValue("PAY10CNT");
    }
    public void setPay10Count(Integer Pay10Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay10Count", Pay10Count);
        else
            setFieldValue("PAY10CNT", Pay10Count);
    }

//Pay11Amount	PAY11AMT	DECIMAL(12,2)	N
    public HYIDouble getPay11Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay11Amount");
        else
            return (HYIDouble)getFieldValue("PAY11AMT");
    }
    public void setPay11Amount(HYIDouble Pay11Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay11Amount", Pay11Amount);
        else
            setFieldValue("PAY11AMT", Pay11Amount);
    }
//Pay11Count	PAY11CNT	INT UNSIGNED	N
    public Integer getPay11Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay11Count");
        else
            return (Integer)getFieldValue("PAY11CNT");
    }
    public void setPay11Count(Integer Pay11Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay11Count", Pay11Count);
        else
            setFieldValue("PAY11CNT", Pay11Count);
    }

//Pay12Amount	PAY12AMT	DECIMAL(12,2)	N
    public HYIDouble getPay12Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay12Amount");
        else
            return (HYIDouble)getFieldValue("PAY12AMT");
    }
    public void setPay12Amount(HYIDouble Pay12Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay12Amount", Pay12Amount);
        else
            setFieldValue("PAY12AMT", Pay12Amount);
    }
//Pay12Count	PAY12CNT	INT UNSIGNED	N
    public Integer getPay12Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay12Count");
        else
            return (Integer)getFieldValue("PAY12CNT");
    }
    public void setPay12Count(Integer Pay12Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay12Count", Pay12Count);
        else
            setFieldValue("PAY12CNT", Pay12Count);
    }

//Pay13Amount	PAY13AMT	DECIMAL(12,2)	N
    public HYIDouble getPay13Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay13Amount");
        else
            return (HYIDouble)getFieldValue("PAY13AMT");
    }
    public void setPay13Amount(HYIDouble Pay13Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay13Amount", Pay13Amount);
        else
            setFieldValue("PAY13AMT", Pay13Amount);
    }
//Pay13Count	PAY13CNT	INT UNSIGNED	N
    public Integer getPay13Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay13Count");
        else
            return (Integer)getFieldValue("PAY13CNT");
    }
    public void setPay13Count(Integer Pay13Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay13Count", Pay13Count);
        else
            setFieldValue("PAY13CNT", Pay13Count);
    }

//Pay14Amount	PAY14AMT	DECIMAL(12,2)	N
    public HYIDouble getPay14Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay14Amount");
        else
            return (HYIDouble)getFieldValue("PAY14AMT");
    }
    public void setPay14Amount(HYIDouble Pay14Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay14Amount", Pay14Amount);
        else
            setFieldValue("PAY14AMT", Pay14Amount);
    }
//Pay14Count	PAY14CNT	INT UNSIGNED	N
    public Integer getPay14Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay14Count");
        else
            return (Integer)getFieldValue("PAY14CNT");
    }
    public void setPay14Count(Integer Pay14Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay14Count", Pay14Count);
        else
            setFieldValue("PAY14CNT", Pay14Count);
    }

//Pay15Amount	PAY15AMT	DECIMAL(12,2)	N
    public HYIDouble getPay15Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay15Amount");
        else
            return (HYIDouble)getFieldValue("PAY15AMT");
    }
    public void setPay15Amount(HYIDouble Pay15Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay15Amount", Pay15Amount);
        else
            setFieldValue("PAY15AMT", Pay15Amount);
    }
//Pay15Count	PAY15CNT	INT UNSIGNED	N
    public Integer getPay15Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay15Count");
        else
            return (Integer)getFieldValue("PAY15CNT");
    }
    public void setPay15Count(Integer Pay15Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay15Count", Pay15Count);
        else
            setFieldValue("PAY15CNT", Pay15Count);
    }

//Pay16Amount	PAY16AMT	DECIMAL(12,2)	N
    public HYIDouble getPay16Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay16Amount");
        else
            return (HYIDouble)getFieldValue("PAY16AMT");
    }
    public void setPay16Amount(HYIDouble Pay16Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay16Amount", Pay16Amount);
        else
            setFieldValue("PAY16AMT", Pay16Amount);
    }
//Pay16Count	PAY16CNT	INT UNSIGNED	N
    public Integer getPay16Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay16Count");
        else
            return (Integer)getFieldValue("PAY16CNT");
    }
    public void setPay16Count(Integer Pay16Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay16Count", Pay16Count);
        else
            setFieldValue("PAY16CNT", Pay16Count);
    }

//Pay17Amount	PAY17AMT	DECIMAL(12,2)	N
    public HYIDouble getPay17Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay17Amount");
        else
            return (HYIDouble)getFieldValue("PAY17AMT");
    }
    public void setPay17Amount(HYIDouble Pay17Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay17Amount", Pay17Amount);
        else
            setFieldValue("PAY17AMT", Pay17Amount);
    }
//Pay17Count	PAY17CNT	INT UNSIGNED	N
    public Integer getPay17Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay17Count");
        else
            return (Integer)getFieldValue("PAY17CNT");
    }
    public void setPay17Count(Integer Pay17Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay17Count", Pay17Count);
        else
            setFieldValue("PAY17CNT", Pay17Count);
    }

//Pay18Amount	PAY18AMT	DECIMAL(12,2)	N
    public HYIDouble getPay18Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay18Amount");
        else
            return (HYIDouble)getFieldValue("PAY18AMT");
    }
    public void setPay18Amount(HYIDouble Pay18Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay18Amount", Pay18Amount);
        else
            setFieldValue("PAY18AMT", Pay18Amount);
    }
//Pay18Count	PAY18CNT	INT UNSIGNED	N
    public Integer getPay18Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay18Count");
        else
            return (Integer)getFieldValue("PAY18CNT");
    }
    public void setPay18Count(Integer Pay18Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay18Count", Pay18Count);
        else
            setFieldValue("PAY18CNT", Pay18Count);
    }

//Pay19Amount	PAY19AMT	DECIMAL(12,2)	N
    public HYIDouble getPay19Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay19Amount");
        else
            return (HYIDouble)getFieldValue("PAY19AMT");
    }
    public void setPay19Amount(HYIDouble Pay19Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay19Amount", Pay19Amount);
        else
            setFieldValue("PAY19AMT", Pay19Amount);
    }
//Pay19Count	PAY19CNT	INT UNSIGNED	N
    public Integer getPay19Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay19Count");
        else
            return (Integer)getFieldValue("PAY19CNT");
    }
    public void setPay19Count(Integer Pay19Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay19Count", Pay19Count);
        else
            setFieldValue("PAY19CNT", Pay19Count);
    }

//Pay20Amount	PAY20AMT	DECIMAL(12,2)	N
    public HYIDouble getPay20Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay20Amount");
        else
            return (HYIDouble)getFieldValue("PAY20AMT");
    }
    public void setPay20Amount(HYIDouble Pay20Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay20Amount", Pay20Amount);
        else
            setFieldValue("PAY20AMT", Pay20Amount);
    }
//Pay20Count	PAY20CNT	INT UNSIGNED	N
    public Integer getPay20Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay20Count");
        else
            return (Integer)getFieldValue("PAY20CNT");
    }
    public void setPay20Count(Integer Pay20Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay20Count", Pay20Count);
        else
            setFieldValue("PAY20CNT", Pay20Count);
    }

//Pay21Amount	PAY21AMT	DECIMAL(12,2)	N
    public HYIDouble getPay21Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay21Amount");
        else
            return (HYIDouble)getFieldValue("PAY21AMT");
    }
    public void setPay21Amount(HYIDouble Pay21Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay21Amount", Pay21Amount);
        else
            setFieldValue("PAY21AMT", Pay21Amount);
    }
//Pay21Count	PAY21CNT	INT UNSIGNED	N
    public Integer getPay21Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay21Count");
        else
            return (Integer)getFieldValue("PAY21CNT");
    }
    public void setPay21Count(Integer Pay21Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay21Count", Pay21Count);
        else
            setFieldValue("PAY21CNT", Pay21Count);
    }

//Pay22Amount	PAY22AMT	DECIMAL(12,2)	N
    public HYIDouble getPay22Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay22Amount");
        else
            return (HYIDouble)getFieldValue("PAY22AMT");
    }
    public void setPay22Amount(HYIDouble Pay22Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay22Amount", Pay22Amount);
        else
            setFieldValue("PAY22AMT", Pay22Amount);
    }
//Pay22Count	PAY22CNT	INT UNSIGNED	N
    public Integer getPay22Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay22Count");
        else
            return (Integer)getFieldValue("PAY22CNT");
    }
    public void setPay22Count(Integer Pay22Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay22Count", Pay22Count);
        else
            setFieldValue("PAY22CNT", Pay22Count);
    }

//Pay23Amount	PAY23AMT	DECIMAL(12,2)	N
    public HYIDouble getPay23Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay23Amount");
        else
            return (HYIDouble)getFieldValue("PAY23AMT");
    }
    public void setPay23Amount(HYIDouble Pay23Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay23Amount", Pay23Amount);
        else
            setFieldValue("PAY23AMT", Pay23Amount);
    }
//Pay23Count	PAY23CNT	INT UNSIGNED	N
    public Integer getPay23Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay23Count");
        else
            return (Integer)getFieldValue("PAY23CNT");
    }
    public void setPay23Count(Integer Pay23Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay23Count", Pay23Count);
        else
            setFieldValue("PAY23CNT", Pay23Count);
    }

//Pay24Amount	PAY24AMT	DECIMAL(12,2)	N
    public HYIDouble getPay24Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay24Amount");
        else
            return (HYIDouble)getFieldValue("PAY24AMT");
    }
    public void setPay24Amount(HYIDouble Pay24Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay24Amount", Pay24Amount);
        else
            setFieldValue("PAY24AMT", Pay24Amount);
    }
//Pay24Count	PAY24CNT	INT UNSIGNED	N
    public Integer getPay24Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay24Count");
        else
            return (Integer)getFieldValue("PAY24CNT");
    }
    public void setPay24Count(Integer Pay24Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay24Count", Pay24Count);
        else
            setFieldValue("PAY24CNT", Pay24Count);
    }

//Pay25Amount	PAY25AMT	DECIMAL(12,2)	N
    public HYIDouble getPay25Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay25Amount");
        else
            return (HYIDouble)getFieldValue("PAY25AMT");
    }
    public void setPay25Amount(HYIDouble Pay25Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay25Amount", Pay25Amount);
        else
            setFieldValue("PAY25AMT", Pay25Amount);
    }
//Pay25Count	PAY25CNT	INT UNSIGNED	N
    public Integer getPay25Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay25Count");
        else
            return (Integer)getFieldValue("PAY25CNT");
    }
    public void setPay25Count(Integer Pay25Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay25Count", Pay25Count);
        else
            setFieldValue("PAY25CNT", Pay25Count);
    }

//Pay26Amount	PAY26AMT	DECIMAL(12,2)	N
    public HYIDouble getPay26Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay26Amount");
        else
            return (HYIDouble)getFieldValue("PAY26AMT");
    }
    public void setPay26Amount(HYIDouble Pay26Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay26Amount", Pay26Amount);
        else
            setFieldValue("PAY26AMT", Pay26Amount);
    }
//Pay26Count	PAY26CNT	INT UNSIGNED	N
    public Integer getPay26Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay26Count");
        else
            return (Integer)getFieldValue("PAY26CNT");
    }
    public void setPay26Count(Integer Pay26Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay26Count", Pay26Count);
        else
            setFieldValue("PAY26CNT", Pay26Count);
    }

//Pay27Amount	PAY27AMT	DECIMAL(12,2)	N
    public HYIDouble getPay27Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay27Amount");
        else
            return (HYIDouble)getFieldValue("PAY27AMT");
    }
    public void setPay27Amount(HYIDouble Pay27Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay27Amount", Pay27Amount);
        else
            setFieldValue("PAY27AMT", Pay27Amount);
    }
//Pay27Count	PAY27CNT	INT UNSIGNED	N
    public Integer getPay27Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay27Count");
        else
            return (Integer)getFieldValue("PAY27CNT");
    }
    public void setPay27Count(Integer Pay27Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay27Count", Pay27Count);
        else
            setFieldValue("PAY27CNT", Pay27Count);
    }

//Pay28Amount	PAY28AMT	DECIMAL(12,2)	N
    public HYIDouble getPay28Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay28Amount");
        else
            return (HYIDouble)getFieldValue("PAY28AMT");
    }
    public void setPay28Amount(HYIDouble Pay28Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay28Amount", Pay28Amount);
        else
            setFieldValue("PAY28AMT", Pay28Amount);
    }
//Pay28Count	PAY28CNT	INT UNSIGNED	N
    public Integer getPay28Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay28Count");
        else
            return (Integer)getFieldValue("PAY28CNT");
    }
    public void setPay28Count(Integer Pay28Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay28Count", Pay28Count);
        else
            setFieldValue("PAY28CNT", Pay28Count);
    }

//Pay29Amount	PAY29AMT	DECIMAL(12,2)	N
    public HYIDouble getPay29Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay29Amount");
        else
            return (HYIDouble)getFieldValue("PAY29AMT");
    }
    public void setPay29Amount(HYIDouble Pay29Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay29Amount", Pay29Amount);
        else
            setFieldValue("PAY29AMT", Pay29Amount);
    }
//Pay29Count	PAY29CNT	INT UNSIGNED	N
    public Integer getPay29Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay29Count");
        else
            return (Integer)getFieldValue("PAY29CNT");
    }
    public void setPay29Count(Integer Pay29Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay29Count", Pay29Count);
        else
            setFieldValue("PAY29CNT", Pay29Count);
    }

//Pay30Amount	PAY30AMT	DECIMAL(12,2)	N
    public HYIDouble getPay30Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("pay30Amount");
        else
            return (HYIDouble)getFieldValue("PAY30AMT");
    }
    public void setPay30Amount(HYIDouble Pay30Amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay30Amount", Pay30Amount);
        else
            setFieldValue("PAY30AMT", Pay30Amount);
    }
//Pay30Count	PAY30CNT	INT UNSIGNED	N
    public Integer getPay30Count() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("pay30Count");
        else
            return (Integer)getFieldValue("PAY30CNT");
    }
    public void setPay30Count(Integer Pay30Count) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pay30Count", Pay30Count);
        else
            setFieldValue("PAY30CNT", Pay30Count);
    }
    
    public Integer getHoldtrancount() {
        return (Integer)getFieldValue("holdtrancount");
    }
    
    public void setHoldtrancount(Integer holdtrancount) {
        setFieldValue("holdtrancount", holdtrancount);
    }

//******************************************************************************
//Payment Category related properties | start
//******************************************************************************
//Utility methods
    public HYIDouble getPayAmountByID (String id) {
        String innerID = null;
        if (id.length()==1 && Integer.parseInt(id)<10)
            innerID = "0" + id;
        else
            innerID = id;
        //return (HYIDouble)getFieldValue("PAY" + innerID + "AMT");
        try {
            return (HYIDouble)this.getClass().getDeclaredMethod(
                "getPay" + innerID + "Amount", new Class[0]).invoke(this, new Object[0]);
        } catch (Exception e) {
            return new HYIDouble(99999999);
        }
    }

    public void setPayAmountByID (String id, HYIDouble payment) {
        String innerID = null;
        if (id.length()==1 && Integer.parseInt(id)<10)
            innerID = "0" + id;
        else
            innerID = id;
        //setFieldValue("PAY" + innerID + "AMT", payment);
        try {
            this.getClass().getDeclaredMethod(
                "setPay" + innerID + "Amount", new Class[] {HYIDouble.class}).invoke(
                this, new Object[] {payment});
        } catch (Exception e) {
       }
    }

    public Integer getPayCountByID(String id) {
        String innerID = null;
        if (id.length()==1 && Integer.parseInt(id)<10)
            innerID = "0" + id;
        else
            innerID = id;
        //return (Integer)getFieldValue("PAY" + innerID + "CNT");
        try {
            return (Integer)this.getClass().getDeclaredMethod(
                "getPay" + innerID + "Count", new Class[0]).invoke(this, new Object[0]);
        } catch (Exception e) {
            return new Integer(0);
        }
    }

    public void setPayCountByID(String id, Integer paycount) {
        String innerID = null;
        if (id.length()==1 && Integer.parseInt(id)<10)
            innerID = "0" + id;
        else
            innerID = id;
        //setFieldValue("PAY" + innerID + "CNT", paycount);
        try {
            this.getClass().getDeclaredMethod(
                "setPay" + innerID + "Count", new Class[] {Integer.class}).invoke(
                this, new Object[] {paycount});
        } catch (Exception e) {
        }
    }

    /**
     * 合计的营业金额
     */
    public HYIDouble getTotalAmount() {
         return new HYIDouble(0)
            .addMe(getNetSalesTotalAmount())
            .addMe(getDaiShouAmount())
            .addMe(getDaiShouAmount2())
            .addMe(getDaiFuAmount().negate())
            .addMe(getSpillAmount())
            .addMe(getPaidInAmount())
            .addMe(getPaidOutAmount().negate());
    }
    
    
    /**
     * 钱箱总金额 = 现金支付 - 找零 + PaidIn - PaidOut + 借零 - 投库
     * 注意: shift.Pay00Amount = (现金支付 - 找零)
     */
    public HYIDouble getTotalCashAmount() {
    	return new HYIDouble(0)
	    	.addMe(getPay00Amount())
	    	.addMe(getCashInAmount())
	    	.addMe(getCashOutAmount().negate())
	    	.addMe(getPaidInAmount())
	    	.addMe(getPaidOutAmount().negate());
    }
    

    public HYIDouble getTotalPayAmount() {
        HYIDouble total = new HYIDouble(0);
        for (int i = 0; i < 30; i++) {
            String j = i + "";
            if (i < 10)
                j = "0" + i;
            //HYIDouble sub = (HYIDouble)getFieldValue("PAY" + j + "AMT");
            try {
                HYIDouble sub = (HYIDouble)this.getClass().getDeclaredMethod(
                    "getPay" + j + "Amount", new Class[0]).invoke(this, new Object[0]);
                if (sub != null) {
                    total = total.addMe(sub);
                }
            } catch (Exception e) {
            }
        }
        return total;
    }

    public HYIDouble getGrossSales() {//SALAMTTAX4'//SALAMTTAX4
        HYIDouble total = new HYIDouble(0);
        for (int i = 0; i < 10; i++) {
            //HYIDouble sub = (HYIDouble)getFieldValue("SALAMTTAX" + i );
            try {
                HYIDouble sub = (HYIDouble)this.getClass().getDeclaredMethod(
                    "getTaxType" + i + "GrossSales", new Class[0]).invoke(this, new Object[0]);
                if (sub != null)
                    total = total.addMe(sub);
            } catch (Exception e) {
            }
        }
        return total;
    }

    /**
     * 回传当日的配达交易笔数。For灿坤POS的Z帐表之用。
     *
     * @return 当日的配达交易笔数
     */
    public Integer getPeiDaCount(DbConnection connection) {
        //Map m = getValueOfStatement("SELECT COUNT(*) FROM tranhead "
        //    + "WHERE DATE_FORMAT(SYSDATE,'%Y-%m-%d')='" 
        //    + new SimpleDateFormat("yyyy-MM-dd").format(this.getAccountingDate())
        //    + "' AND VOIDSEQ IS NULL AND DEALTYPE1<>'*' AND ANNOTATEDTYPE='P'");
        Object count = getValueOfStatement(connection, "SELECT COUNT(*) FROM tranhead "
            + "WHERE EODCNT=" + this.getSequenceNumber() 
            + " AND VOIDSEQ IS NULL AND DEALTYPE1<>'*' AND ANNOTATEDTYPE='P'");
        return new Integer(CreamToolkit.retrieveIntValue(count));
    }

    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_z";
        else
            return "z";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_z";
        else
            return "z";
    }


	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as String[][] 
	 */
	public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
            {"STORENO", "storeID"},
            {"POSNO", "posNumber"},
            {"EODCNT", "zSequenceNumber"},
            {"ACCDATE", "accountDate"},
            {"ZBEGDTTM", "zBeginDateTime"},
            {"ZENDDTTM", "zEndDateTime"},
            {"SGNONSEQ", "zBeginTransactionNumber"},
            {"SGNOFFSEQ", "zEndTransactionNumber"},
            {"SGNONINV", "zBeginInvoiceNumber"},
            {"SGNOFFINV", "zEndInvoiceNumber"},
            {"MACHINENO", "machineNumber"},
            {"CASHIER", "cashierID"},
            {"TCPFLG", "uploadState"},
            // GT
            // GTCUSCNT
            // GTSALECNT
            // 交易明细=交易��
            {"TRANCNT", "transactionCount"},
            {"CUSTCNT", "customerCount"},
            {"SALECNT", "itemSaleQuantity"},
            {"GrossSales", "grossSaleTotalAmount"},
            {"SALAMTTAX0", "grossSaleTax0Amount"},
            {"SALAMTTAX1", "grossSaleTax1Amount"},
            {"SALAMTTAX2", "grossSaleTax2Amount"},
            {"SALAMTTAX3", "grossSaleTax3Amount"},
            {"SALAMTTAX4", "grossSaleTax4Amount"},
            {"SALAMTTAX5", "grossSaleTax5Amount"},
            {"SALAMTTAX6", "grossSaleTax6Amount"},
            {"SALAMTTAX7", "grossSaleTax7Amount"},
            {"SALAMTTAX8", "grossSaleTax8Amount"},
            {"SALAMTTAX9", "grossSaleTax9Amount"},
            {"SALAMTTAX10", "grossSaleTax10Amount"},
            {"SIPercentPlusTotalAmount", "siPlusTotalAmount"},
            {"SIPLUSAMT0", "siPlusTax0Amount"},
            {"SIPLUSAMT1", "siPlusTax1Amount"},
            {"SIPLUSAMT2", "siPlusTax2Amount"},
            {"SIPLUSAMT3", "siPlusTax3Amount"},
            {"SIPLUSAMT4", "siPlusTax4Amount"},
            {"SIPLUSAMT5", "siPlusTax5Amount"},
            {"SIPLUSAMT6", "siPlusTax6Amount"},
            {"SIPLUSAMT7", "siPlusTax7Amount"},
            {"SIPLUSAMT8", "siPlusTax8Amount"},
            {"SIPLUSAMT9", "siPlusTax9Amount"},
            {"SIPLUSAMT10", "siPlusTax10Amount"},
            {"SIPercentPlusTotalCount", "siPlusTotalCount"},
            {"SIPLUSCNT0", "siPlusTax0Count"},
            {"SIPLUSCNT1", "siPlusTax1Count"},
            {"SIPLUSCNT2", "siPlusTax2Count"},
            {"SIPLUSCNT3", "siPlusTax3Count"},
            {"SIPLUSCNT4", "siPlusTax4Count"},
            {"SIPLUSCNT5", "siPlusTax5Count"},
            {"SIPLUSCNT6", "siPlusTax6Count"},
            {"SIPLUSCNT7", "siPlusTax7Count"},
            {"SIPLUSCNT8", "siPlusTax8Count"},
            {"SIPLUSCNT9", "siPlusTax9Count"},
            {"SIPLUSCNT10", "siPlusTax10Count"},
            {"SIPercentOffTotalAmount", "discountTotalAmount"},
            {"SIPAMT0", "discountTax0Amount"},
            {"SIPAMT1", "discountTax1Amount"},
            {"SIPAMT2", "discountTax2Amount"},
            {"SIPAMT3", "discountTax3Amount"},
            {"SIPAMT4", "discountTax4Amount"},
            {"SIPAMT5", "discountTax5Amount"},
            {"SIPAMT6", "discountTax6Amount"},
            {"SIPAMT7", "discountTax7Amount"},
            {"SIPAMT8", "discountTax8Amount"},
            {"SIPAMT9", "discountTax9Amount"},
            {"SIPAMT10", "discountTax10Amount"},
            {"SIPercentOffTotalCount", "discountTotalCount"},
            {"SIPCNT0", "discountTax0Count"},
            {"SIPCNT1", "discountTax1Count"},
            {"SIPCNT2", "discountTax2Count"},
            {"SIPCNT3", "discountTax3Count"},
            {"SIPCNT4", "discountTax4Count"},
            {"SIPCNT5", "discountTax5Count"},
            {"SIPCNT6", "discountTax6Count"},
            {"SIPCNT7", "discountTax7Count"},
            {"SIPCNT8", "discountTax8Count"},
            {"SIPCNT9", "discountTax9Count"},
            {"SIPCNT10", "discountTax10Count"},
            {"MixAndMatchTotalAmount", "mixAndMatchTotalAmount"},
            {"MNMAMT0", "mixAndMatchTax0Amount"},
            {"MNMAMT1", "mixAndMatchTax1Amount"},
            {"MNMAMT2", "mixAndMatchTax2Amount"},
            {"MNMAMT3", "mixAndMatchTax3Amount"},
            {"MNMAMT4", "mixAndMatchTax4Amount"},
            {"MNMAMT5", "mixAndMatchTax5Amount"},
            {"MNMAMT6", "mixAndMatchTax6Amount"},
            {"MNMAMT7", "mixAndMatchTax7Amount"},
            {"MNMAMT8", "mixAndMatchTax8Amount"},
            {"MNMAMT9", "mixAndMatchTax9Amount"},
            {"MNMAMT10", "mixAndMatchTax10Amount"},
            {"MixAndMatchTotalCount", "mixAndMatchTotalCount"},
            {"MNMCNT0", "mixAndMatchTax0Count"},
            {"MNMCNT1", "mixAndMatchTax1Count"},
            {"MNMCNT2", "mixAndMatchTax2Count"},
            {"MNMCNT3", "mixAndMatchTax3Count"},
            {"MNMCNT4", "mixAndMatchTax4Count"},
            {"MNMCNT5", "mixAndMatchTax5Count"},
            {"MNMCNT6", "mixAndMatchTax6Count"},
            {"MNMCNT7", "mixAndMatchTax7Count"},
            {"MNMCNT8", "mixAndMatchTax8Count"},
            {"MNMCNT9", "mixAndMatchTax9Count"},
            {"MNMCNT10", "mixAndMatchTax10Count"},
            {"NotIncludedTotalSales", "notIncludedTotalSale"},
            {"RCPGIFAMT0", "notIncludedTax0Sale"},
            {"RCPGIFAMT1", "notIncludedTax1Sale"},
            {"RCPGIFAMT2", "notIncludedTax2Sale"},
            {"RCPGIFAMT3", "notIncludedTax3Sale"},
            {"RCPGIFAMT4", "notIncludedTax4Sale"},
            {"RCPGIFAMT5", "notIncludedTax5Sale"},
            {"RCPGIFAMT6", "notIncludedTax6Sale"},
            {"RCPGIFAMT7", "notIncludedTax7Sale"},
            {"RCPGIFAMT8", "notIncludedTax8Sale"},
            {"RCPGIFAMT9", "notIncludedTax9Sale"},
            {"RCPGIFAMT10", "notIncludedTax10Sale"},
            {"NetSalesTotalAmount", "netSaleTotalAmount"},
            {"NETSALAMT0", "netSaleTax0Amount"},
            {"NETSALAMT1", "netSaleTax1Amount"},
            {"NETSALAMT2", "netSaleTax2Amount"},
            {"NETSALAMT3", "netSaleTax3Amount"},
            {"NETSALAMT4", "netSaleTax4Amount"},
            {"NETSALAMT5", "netSaleTax5Amount"},
            {"NETSALAMT6", "netSaleTax6Amount"},
            {"NETSALAMT7", "netSaleTax7Amount"},
            {"NETSALAMT8", "netSaleTax8Amount"},
            {"NETSALAMT9", "netSaleTax9Amount"},
            {"NETSALAMT10", "netSaleTax10Amount"},
            {"TAXAMT0", "taxAmount0"},
            {"TAXAMT1", "taxAmount1"},
            {"TAXAMT2", "taxAmount2"},
            {"TAXAMT3", "taxAmount3"},
            {"TAXAMT4", "taxAmount4"},
            {"TAXAMT5", "taxAmount5"},
            {"TAXAMT6", "taxAmount6"},
            {"TAXAMT7", "taxAmount7"},
            {"TAXAMT8", "taxAmount8"},
            {"TAXAMT9", "taxAmount9"},
            {"TAXAMT10", "taxAmount10"},
            {"TotalTaxAmount", "taxTotalAmount"},       //Bruce/20030328
            // �卷销售=�券�售��
            {"VALPAMT", "totalVoucherAmount"},
            {"VALPCNT", "totalVoucherCount"},
            // �收=�收��
            {"PRCVAMT", "preRcvAmount"},
            {"PRCVCNT", "preRcvCount"},
            {"PRCVPLUAMT1", "preRcvDetailAmount1"},
            {"PRCVPLUCNT1", "preRcvDetailCount1"},
            {"PRCVPLUAMT2", "preRcvDetailAmount2"},
            {"PRCVPLUCNT2", "preRcvDetailCount2"},
            {"PRCVPLUAMT3", "preRcvDetailAmount3"},
            {"PRCVPLUCNT3", "preRcvDetailCount3"},
            {"PRCVPLUAMT4", "preRcvDetailAmount4"},
            {"PRCVPLUCNT4", "preRcvDetailCount4"},
            {"PRCVPLUAMT5", "preRcvDetailAmount5"},
            {"PRCVPLUCNT5", "preRcvDetailCount5"},

            {"PRCVPLUAMT6", "preRcvDetailAmount6"},
            {"PRCVPLUCNT6", "preRcvDetailCount6"},
            {"PRCVPLUAMT7", "preRcvDetailAmount7"},
            {"PRCVPLUCNT7", "preRcvDetailCount7"},
            {"PRCVPLUAMT8", "preRcvDetailAmount8"},
            {"PRCVPLUCNT8", "preRcvDetailCount8"},
            {"PRCVPLUAMT9", "preRcvDetailAmount9"},
            {"PRCVPLUCNT9", "preRcvDetailCount9"},
            {"PRCVPLUAMT10", "preRcvDetailAmount10"},
            {"PRCVPLUCNT10", "preRcvDetailCount10"},

            // 代售=代售��
            {"DAISHOUAMT", "totalDaiShouAmount"},
            {"DAISHOUCNT", "totalDaiShouCount"},
            {"DAISHOUPLUAMT1", "daiShouDetailAmount1"},
            {"DAISHOUPLUCNT1", "daiShouDetailCount1"},
            {"DAISHOUPLUAMT2", "daiShouDetailAmount2"},
            {"DAISHOUPLUCNT2", "daiShouDetailCount2"},
            {"DAISHOUPLUAMT3", "daiShouDetailAmount3"},
            {"DAISHOUPLUCNT3", "daiShouDetailCount3"},
            {"DAISHOUPLUAMT4", "daiShouDetailAmount4"},
            {"DAISHOUPLUCNT4", "daiShouDetailCount4"},
            {"DAISHOUPLUAMT5", "daiShouDetailAmount5"},
            {"DAISHOUPLUCNT5", "daiShouDetailCount5"},

            {"DAISHOUPLUAMT6", "daiShouDetailAmount6"},
            {"DAISHOUPLUCNT6", "daiShouDetailCount6"},
            {"DAISHOUPLUAMT7", "daiShouDetailAmount7"},
            {"DAISHOUPLUCNT7", "daiShouDetailCount7"},
            {"DAISHOUPLUAMT8", "daiShouDetailAmount8"},
            {"DAISHOUPLUCNT8", "daiShouDetailCount8"},
            {"DAISHOUPLUAMT9", "daiShouDetailAmount9"},
            {"DAISHOUPLUCNT9", "daiShouDetailCount9"},
            {"DAISHOUPLUAMT10", "daiShouDetailAmount10"},
            {"DAISHOUPLUCNT10", "daiShouDetailCount10"},

            //代收公共事业费
            {"DAISHOUAMT2", "TotalDaiShouAmount2"},
            {"DAISHOUCNT2", "TotalDaiShouCount2"},
            
            // 代付=代付��
            {"DAIFUAMT", "totalDaiFuAmount"},
            {"DAIFUCNT", "totalDaiFuCount"},
            {"DAIFUPLUAMT1", "daiFuDetailAmount1"},
            {"DAIFUPLUCNT1", "daiFuDetailCount1"},
            {"DAIFUPLUAMT2", "daiFuDetailAmount2"},
            {"DAIFUPLUCNT2", "daiFuDetailCount2"},
            {"DAIFUPLUAMT3", "daiFuDetailAmount3"},
            {"DAIFUPLUCNT3", "daiFuDetailCount3"},
            {"DAIFUPLUAMT4", "daiFuDetailAmount4"},
            {"DAIFUPLUCNT4", "daiFuDetailCount4"},
            {"DAIFUPLUAMT5", "daiFuDetailAmount5"},
            {"DAIFUPLUCNT5", "daiFuDetailCount5"},

            {"DAIFUPLUAMT6", "daiFuDetailAmount6"},
            {"DAIFUPLUCNT6", "daiFuDetailCount6"},
            {"DAIFUPLUAMT7", "daiFuDetailAmount7"},
            {"DAIFUPLUCNT7", "daiFuDetailCount7"},
            {"DAIFUPLUAMT8", "daiFuDetailAmount8"},
            {"DAIFUPLUCNT8", "daiFuDetailCount8"},
            {"DAIFUPLUAMT9", "daiFuDetailAmount9"},
            {"DAIFUPLUCNT9", "daiFuDetailCount9"},
            {"DAIFUPLUAMT10", "daiFuDetailAmount10"},
            {"DAIFUPLUCNT10", "daiFuDetailCount10"},

            //Paid-In=PaidIn��
            {"PINAMT", "totalPaidinAmount"},
            {"PINCNT", "totalPaidinCount"},
            {"PINPLUAMT1", "paidInDetailAmount1"},
            {"PINPLUCNT1", "paidInDetailCount1"},
            {"PINPLUAMT2", "paidInDetailAmount2"},
            {"PINPLUCNT2", "paidInDetailCount2"},
            {"PINPLUAMT3", "paidInDetailAmount3"},
            {"PINPLUCNT3", "paidInDetailCount3"},
            {"PINPLUAMT4", "paidInDetailAmount4"},
            {"PINPLUCNT4", "paidInDetailCount4"},
            {"PINPLUAMT5", "paidInDetailAmount5"},
            {"PINPLUCNT5", "paidInDetailCount5"},

            {"PINPLUAMT6", "paidInDetailAmount6"},
            {"PINPLUCNT6", "paidInDetailCount6"},
            {"PINPLUAMT7", "paidInDetailAmount7"},
            {"PINPLUCNT7", "paidInDetailCount7"},
            {"PINPLUAMT8", "paidInDetailAmount8"},
            {"PINPLUCNT8", "paidInDetailCount8"},
            {"PINPLUAMT9", "paidInDetailAmount9"},
            {"PINPLUCNT9", "paidInDetailCount9"},
            {"PINPLUAMT10", "paidInDetailAmount10"},
            {"PINPLUCNT10", "paidInDetailCount10"},

            // Paid-Out=PaidOut��
            {"POUTAMT", "totalPaidoutAmount"},
            {"POUTCNT", "totalPaidoutCount"},
            {"POUTPLUAMT1", "paidOutDetailAmount1"},
            {"POUTPLUCNT1", "paidOutDetailCount1"},
            {"POUTPLUAMT2", "paidOutDetailAmount2"},
            {"POUTPLUCNT2", "paidOutDetailCount2"},
            {"POUTPLUAMT3", "paidOutDetailAmount3"},
            {"POUTPLUCNT3", "paidOutDetailCount3"},
            {"POUTPLUAMT4", "paidOutDetailAmount4"},
            {"POUTPLUCNT4", "paidOutDetailCount4"},
            {"POUTPLUAMT5", "paidOutDetailAmount5"},
            {"POUTPLUCNT5", "paidOutDetailCount5"},

            {"POUTPLUAMT6", "paidOutDetailAmount6"},
            {"POUTPLUCNT6", "paidOutDetailCount6"},
            {"POUTPLUAMT7", "paidOutDetailAmount7"},
            {"POUTPLUCNT7", "paidOutDetailCount7"},
            {"POUTPLUAMT8", "paidOutDetailAmount8"},
            {"POUTPLUCNT8", "paidOutDetailCount8"},
            {"POUTPLUAMT9", "paidOutDetailAmount9"},
            {"POUTPLUCNT9", "paidOutDetailCount9"},
            {"POUTPLUAMT10", "paidOutDetailAmount10"},
            {"POUTPLUCNT10", "paidOutDetailCount10"},

            // 操作明细=操作明�
            {"EXGAMT", "exchangeItemAmount"},
            {"EXGITEMQTY", "exchangeItemQuantity"},
            {"EXGTRANCNT", "exchangeTransactionCount"},
            {"RTNAMT", "returnItemAmount"},
            {"RTNITEMQTY", "returnItemQuantity"},
            {"RTNTRANCNT", "returnTransactionCount"},
            {"VOIDINVAMT", "transactionVoidAmount"},
            {"VOIDINVCNT", "transactionVoidCount"},
            {"CANCELAMT", "transactionCancelAmount"},
            {"CANCELCNT", "transactionCancelCount"},
            {"UPDAMT", "lineVoidAmount"},
            {"UPDCNT", "lineVoidCount"},
            {"NOWUPDAMT", "voidAmount"},
            {"NOWUPDCNT", "voidCount"},
            {"DROPENCNT", "drawerOpenCount"},
            {"RTNCSHCNT", "cashReturnCount"},
            {"RTNCSHAMT", "cashReturnAmount"},
            {"ENTRYAMT", "manualEntryAmount"},
            {"ENTRYCNT", "manualEntryCount"},
            {"POENTRYAMT", "priceOpenEntryAmount"},
            {"POENTRYCNT", "priceOpenEntryCount"},
            {"REPRTAMT", "reprintAmount"},
            {"REPRTCNT", "reprintCount"},
            {"CASHINAMT", "cashInAmount"},
            {"CASHINCNT", "cashInCount"},
            {"CASHOUTAMT", "cashOutAmount"},
            {"CASHOUTCNT", "cashOutCount"},
            // 支付明细项目=支付明�
            {"OVERAMT", "spillAmount"},
            {"OVERCNT", "spillCount"},
            {"RATE1", "exchangeRate1"},
            {"RATE2", "exchangeRate2"},
            {"RATE3", "exchangeRate3"},
            {"RATE4", "exchangeRate4"},
            {"PAY00AMT", "pay00Amount"},
            {"PAY00CNT", "pay00Count"},
            {"PAY01AMT", "pay01Amount"},
            {"PAY01CNT", "pay01Count"},
            {"PAY02AMT", "pay02Amount"},
            {"PAY02CNT", "pay02Count"},
            {"PAY03AMT", "pay03Amount"},
            {"PAY03CNT", "pay03Count"},
            {"PAY04AMT", "pay04Amount"},
            {"PAY04CNT", "pay04Count"},
            {"PAY05AMT", "pay05Amount"},
            {"PAY05CNT", "pay05Count"},
            {"PAY06AMT", "pay06Amount"},
            {"PAY06CNT", "pay06Count"},
            {"PAY07AMT", "pay07Amount"},
            {"PAY07CNT", "pay07Count"},
            {"PAY08AMT", "pay08Amount"},
            {"PAY08CNT", "pay08Count"},
            {"PAY09AMT", "pay09Amount"},
            {"PAY09CNT", "pay09Count"},
            {"PAY10AMT", "pay10Amount"},
            {"PAY10CNT", "pay10Count"},
            {"PAY11AMT", "pay11Amount"},
            {"PAY11CNT", "pay11Count"},
            {"PAY12AMT", "pay12Amount"},
            {"PAY12CNT", "pay12Count"},
            {"PAY13AMT", "pay13Amount"},
            {"PAY13CNT", "pay13Count"},
            {"PAY14AMT", "pay14Amount"},
            {"PAY14CNT", "pay14Count"},
            {"PAY15AMT", "pay15Amount"},
            {"PAY15CNT", "pay15Count"},
            {"PAY16AMT", "pay16Amount"},
            {"PAY16CNT", "pay16Count"},
            {"PAY17AMT", "pay17Amount"},
            {"PAY17CNT", "pay17Count"},
            {"PAY18AMT", "pay18Amount"},
            {"PAY18CNT", "pay18Count"},
            {"PAY19AMT", "pay19Amount"},
            {"PAY19CNT", "pay19Count"},
            {"PAY20AMT", "pay20Amount"},
            {"PAY20CNT", "pay20Count"},
            {"PAY21AMT", "pay21Amount"},
            {"PAY21CNT", "pay21Count"},
            {"PAY22AMT", "pay22Amount"},
            {"PAY22CNT", "pay22Count"},
            {"PAY23AMT", "pay23Amount"},
            {"PAY23CNT", "pay23Count"},
            {"PAY24AMT", "pay24Amount"},
            {"PAY24CNT", "pay24Count"},
            {"PAY25AMT", "pay25Amount"},
            {"PAY25CNT", "pay25Count"},
            {"PAY26AMT", "pay26Amount"},
            {"PAY26CNT", "pay26Count"},
            {"PAY27AMT", "pay27Amount"},
            {"PAY27CNT", "pay27Count"},
            {"PAY28AMT", "pay28Amount"},
            {"PAY28CNT", "pay28Count"},
            {"PAY29AMT", "pay29Amount"},
            {"PAY29CNT", "pay29Count"},
            {"CHANGAMT", "exchangeDifference"},
            {"SI00AMT", "si00Amount"},
            {"SI00CNT", "si00Count"},
            {"SI01AMT", "si01Amount"},
            {"SI01CNT", "si01Count"},
            {"SI02AMT", "si02Amount"},
            {"SI02CNT", "si02Count"},
            {"SI03AMT", "si03Amount"},
            {"SI03CNT", "si03Count"},
            {"SI04AMT", "si04Amount"},
            {"SI04CNT", "si04Count"},
            {"SI05AMT", "si05Amount"},
            {"SI05CNT", "si05Count"},
            {"SI06AMT", "si06Amount"},
            {"SI06CNT", "si06Count"},
            {"SI07AMT", "si07Amount"},
            {"SI07CNT", "si07Count"},
            {"SI08AMT", "si08Amount"},
            {"SI08CNT", "si08Count"},
            {"SI09AMT", "si09Amount"},
            {"SI09CNT", "si09Count"},
            {"SI10AMT", "si10Amount"},
            {"SI10CNT", "si10Count"},
            {"SI11AMT", "si11Amount"},
            {"SI11CNT", "si11Count"},
            {"SI12AMT", "si12Amount"},
            {"SI12CNT", "si12Count"},
            {"SI13AMT", "si13Amount"},
            {"SI13CNT", "si13Count"},
            {"SI14AMT", "si14Amount"},
            {"SI14CNT", "si14Count"},
            {"SI15AMT", "si15Amount"},
            {"SI15CNT", "si15Count"},
            {"SI16AMT", "si16Amount"},
            {"SI16CNT", "si16Count"},
            {"SI17AMT", "si17Amount"},
            {"SI17CNT", "si17Count"},
            {"SI18AMT", "si18Amount"},
            {"SI18CNT", "si18Count"},
            {"SI19AMT", "si19Amount"},
            {"SI19CNT", "si19Count"},
            {"holdtrancount", "holdtrancount"},
		};
	}

    /**
     * Clone a ZReport object for SC with converted field names.
     *
     * This method is only used at POS side.
     */
    public ZReport cloneForSC() {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        ZReport clonedZ = new ZReport(0);
        for (int i = 0; i < fieldNameMap.length; i++) {
            Object value = this.getFieldValue(fieldNameMap[i][0]);
            if (value == null) {
                try {
                    value = this.getClass().getDeclaredMethod(
                        "get" + fieldNameMap[i][0], new Class[0]).invoke(this, new Object[0]);
                } catch (Exception e) {
                    value = null;
                }
            }

            // 因为SC没有区分SI折扣与折让金额，所以将其相加
            if (fieldNameMap[i][0].startsWith("SIPAMT")) {
                ((HYIDouble)value).addMe((HYIDouble)this.getFieldValue(
                    "SIMAMT" + fieldNameMap[i][0].substring(fieldNameMap[i][0].length() - 1)));
            }
            if (fieldNameMap[i][0].startsWith("SIPCNT")) {
                //((Integer)value).add((Integer)this.getFieldValue(
                //    "SIMCNT" + fieldNameMap[i][0].substring(fieldNameMap[i][0].length() - 1)));
                value = new Integer(((Integer)value).intValue() + ((Integer)this.getFieldValue(
                    "SIMCNT" + fieldNameMap[i][0].substring(fieldNameMap[i][0].length() - 1))).intValue());
            }

            clonedZ.setFieldValue(fieldNameMap[i][1], value);
        }
        return clonedZ;
    }
    
    
}

