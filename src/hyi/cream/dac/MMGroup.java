package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Wissly
 * @version 1.0
 */
public class MMGroup extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    transient public static final String VERSION = "1.5";

	static final String tableName = "mmgroup";

	private static ArrayList primaryKeys = new ArrayList();
	static {
		primaryKeys.add("Mmid");
	}

	public String getInsertUpdateTableName() {
		return tableName;
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public static Iterator queryByID(DbConnection connection, String Mmid) {
        try {
            return getMultipleObjects(connection, MMGroup.class, "SELECT * FROM " + tableName
                + " WHERE Mmid='" + Mmid + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

	public static Iterator queryByITEMNO(String Item) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return getMultipleObjects(connection, MMGroup.class, "SELECT * FROM " + tableName
                + " WHERE Item ='" + Item + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

	public static DacBase queryByIDAndItemNo(String mmid, String item) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return getSingleObject(connection, MMGroup.class, "SELECT * FROM " + tableName
                + " WHERE Item ='" + item + "' AND Mmid='" + mmid + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

	public String getID() {
		return (String) getFieldValue("Mmid");
	}

	public String getITEMNO() {
		return (String) getFieldValue("Item");
	}

	public String getGROUPNUM() {
		return (String) getFieldValue("Groupno");
	}

	/**
	 * for cankun 指定售价
	 * 
	 * @return
	 */
	public HYIDouble getDiscountPrice() {
		return (HYIDouble) getFieldValue("discountPrice");
	}

	/**
	 * for cankun 指定还原金比例
	 * 
	 * @return
	 */
	public HYIDouble getRebateRate() {
		return (HYIDouble) getFieldValue("rebateRate");
	}

	public MMGroup() throws InstantiationException {

	}

	/**
	 * Meyer/2003-02-20 return fieldName map of PosToSc as Map
	 */
	public static Map getScToPosFieldNameMap() {
		Map fieldNameMap = new HashMap();
		fieldNameMap.put("MmID", "Mmid");
		fieldNameMap.put("Groupno", "Groupno");
		fieldNameMap.put("Item", "Item");
		fieldNameMap.put("discountPrice", "discountPrice");
		fieldNameMap.put("rebateRate", "rebateRate");
		return fieldNameMap;
	}

	/**
	 * Get all data for downloading to POS. This methid is used by inline
	 * server.
	 */
	public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.MMGroup.class,
                "SELECT * FROM posdl_mmgroup", getScToPosFieldNameMap());
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}