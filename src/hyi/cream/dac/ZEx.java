package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DBToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * DAC for cream.zex and innocake.posul_zex. 
 * 
 * @author Bruce You
 */
public class ZEx extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private static ArrayList primaryKeys = new ArrayList();

    static {
        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("PosNumber");
            primaryKeys.add("ZSequenceNumber");
            primaryKeys.add("AccountCode");
        } else {
            primaryKeys.add("POSNO");
            primaryKeys.add("EODCNT");
            primaryKeys.add("CODE");
        }
    }

    public ZEx() {
        setStoreNumber(Store.getStoreID());
        setTerminalNumber(PARAM.getTerminalNumber());
        setUploadState("0");
    }

    public ZEx(int i) {
        // do-nothing constructor
    }

    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public static List<ZEx> queryByAccdate(DbConnection connection, Date date) {
        try {
            SimpleDateFormat df = CreamCache.getInstance().getDateFormate();
            if (hyi.cream.inline.Server.serverExist())
                return getMultipleObjects2(connection, ZEx.class, "SELECT * FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE accountDate='"
                    + df.format(date).toString() + "' ORDER BY AccountDate, AccountCode");
            else
                return getMultipleObjects2(connection, ZEx.class, "SELECT * FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE ACCDATE='"
                    + df.format(date).toString() + "' ORDER BY accdate, code");
            
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static Iterator<ZEx> queryBySequenceNumber(DbConnection connection, Integer eodcnt) {
        try {
            if (hyi.cream.inline.Server.serverExist())
                return getMultipleObjects(connection, ZEx.class, "SELECT * FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE zSequenceNumber=" + eodcnt
                    + " ORDER BY AccountCode");
            else
                return getMultipleObjects(connection, ZEx.class, "SELECT * FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE eodcnt=" + eodcnt
                    + " ORDER BY code");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }
    
    public String getStoreNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("StoreID");
        else
            return (String)getFieldValue("STORENO");
    }

    public void setStoreNumber(String storeNumber) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("StoreID", storeNumber);
        else
            setFieldValue("STORENO", storeNumber);
    }

    public Integer getTerminalNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("PosNumber");
        else
            return (Integer)getFieldValue("POSNO");
    }

    public void setTerminalNumber(Integer posno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("PosNumber", posno);
        else
            setFieldValue("POSNO", posno);
    }

    public Integer getZSequenceNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("ZSequenceNumber");
        else
            return (Integer)getFieldValue("EODCNT");
    }

    public void setZSequenceNumber(Integer ZNO) {
        setFieldValue("EODCNT", ZNO);
    }

    // UploadState TCPFLG ENUM("0","1","2") N
    // "0":未上传 "1":上传成功 "2"：上传失败
    public String getUploadState() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("UploadState");
        else
            return (String)getFieldValue("TCPFLG");
    }

    public void setUploadState(String TcpFlag) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("UploadState", TcpFlag);
        else
            setFieldValue("TCPFLG", TcpFlag);
    }

    public String getAccountCode() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("AccountCode");
        else
            return (String)getFieldValue("CODE");
    }

    public void setAccountCode(String accountCode) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("AccountCode", accountCode);
        else
            setFieldValue("CODE", accountCode);
    }

    public HYIDouble getAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("Amount");
        else
            return (HYIDouble)getFieldValue("AMOUNT");
    }

    public void setAmount(HYIDouble amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("Amount", amount);
        else
            setFieldValue("AMOUNT", amount);
    }

    public Date getAccountingDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (Date)getFieldValue("AccountDate");
        else
            return (Date)getFieldValue("ACCDATE");
    }

    public void setAccountingDate(Date accdate) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("AccountDate", accdate);
        else
            setFieldValue("ACCDATE", accdate);
    }


    @Override
    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_zex";
        else
            return "zex";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_zex";
        else
            return "zex";
    }

    public static String[][] getPosToScFieldNameArray() {
        return new String[][] { { "STORENO", "StoreID" }, { "POSNO", "PosNumber" },
            { "EODCNT", "ZSequenceNumber" }, { "CODE", "AccountCode" }, { "AMOUNT", "Amount" },
            { "TCPFLG", "UploadState" }, 
            {"ACCDATE", "AccountDate"},
            };
    }

    /**
     * Clone ZEx objects for SC with converted field names.
     * 
     * This method is only used at POS side.
     */
    public static Object[] cloneForSC(Iterator<ZEx> iter) {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        ArrayList<ZEx> objArray = new ArrayList<ZEx>();
        while (iter.hasNext()) {
            ZEx ds = iter.next();
            ZEx clonedDS = new ZEx(0);
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = ds.getFieldValue(fieldNameMap[i][0]);
                if (value == null) {
                    try {
                        value = ds.getClass().getDeclaredMethod("get" + fieldNameMap[i][0],
                            new Class[0]).invoke(ds, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedDS.setFieldValue(fieldNameMap[i][1], value);
            }
            objArray.add(clonedDS);
        }
        return objArray.toArray();
    }

    public static void deleteBySequenceNumber(DbConnection connection, int posNumber, int zNumber)
            throws SQLException {
        String deleteSql = "DELETE FROM " + getInsertUpdateTableNameStaticVersion();
        if (hyi.cream.inline.Server.serverExist())
            deleteSql += " WHERE posNumber=" + posNumber + " AND zSequenceNumber=" + zNumber;
        else
            deleteSql += " WHERE posno=" + posNumber + " AND eodcnt=" + zNumber;
        DBToolkit.executeUpdate(connection, deleteSql);
    }
    
    /**
     * not replaces! add the amount base on the old amount in database.
     * @param connection
     * @throws SQLException
     */
    public void updateZEx(DbConnection connection) throws SQLException {
        String selectStatement = "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                + " WHERE  POSNO = " + this.getTerminalNumber()
                + " AND EODCNT = " + this.getZSequenceNumber()
                + " AND CODE = '" + this.getAccountCode() + "'";

        ZEx oldZEx = null;
        try {
            oldZEx = DacBase.getSingleObject(connection, ZEx.class, selectStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (EntityNotFoundException e) {
            // do nothing
        }
        if (this.getAmount() == null){
            this.setAmount(new HYIDouble(0));
        }
        if (oldZEx == null) {
            this.insert(connection);
        } else {
            this.setUploadState("0");
            this.setAmount(this.getAmount().add(oldZEx.getAmount()));
            try {
                this.update(connection);
            } catch (EntityNotFoundException e) {
                throw new SQLException(e.toString());
            }
        }
    }

    public void update(DbConnection connection) throws SQLException, EntityNotFoundException {
        setAllFieldsDirty(true);
        super.update(connection);
    }
}
