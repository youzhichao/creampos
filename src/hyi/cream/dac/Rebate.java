/*
 * Created on 2003-10-31
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * For 還圓金.
 */
public class Rebate extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

	private static ArrayList primaryKeys = new ArrayList();
	static {
		primaryKeys.add("storeID");
		primaryKeys.add("itemNumber");
		primaryKeys.add("beginDate");
		primaryKeys.add("discountBeginDate");
	}

	/**
	 * @throws InstantiationException
	 */
	public Rebate() {
		super();

	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (getUseClientSideTableName())
			return "rebate";
		else if (hyi.cream.inline.Server.serverExist())
			return "posdl_rebate";
		else
			return "rebate";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (hyi.cream.inline.Server.serverExist())
			return "posdl_rebate";
		else
			return "rebate";
	}

    /**
     * 从SC 中取出数据为下载到POS系统做准备
     * 
     * @return Collection
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            String sql = "SELECT * FROM posdl_rebate";
            return DacBase.getMultipleObjects(connection, Rebate.class, sql, null);
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * 根据商品编号得到对应的还元金比率
     * 如果是在促销期间，得到促销期间的还元金比率
     *  
     * @param itemNumber 商品编号
     * @return 还元金比率
     */
    public static HYIDouble getRebateRate(String itemNumber) {
        HYIDouble rate = new HYIDouble(0.00);
        Date dt = new Date();
        String today = CreamCache.getInstance().getDateFormate().format(dt);
        String table = getInsertUpdateTableNameStaticVersion();
        DacBase dacBase;
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            String sql = "SELECT * FROM " + table + " "
                + "WHERE itemNumber = '" + itemNumber + "' "
                + "AND beginDate <= '" + today + "' "
                + "AND endDate >= '" + today + "' " 
                + "ORDER BY beginDate DESC LIMIT 1";
            dacBase = getSingleObject(connection, Rebate.class, sql);
            Rebate rebate = (Rebate)dacBase;
            Date discBDate = rebate.getDiscountBeginDate();
            Date discEDate = rebate.getDiscountEndDate();
            dt = java.sql.Date.valueOf(today);
            if (dt.compareTo(discBDate) >=0 && dt.compareTo(discEDate) <= 0) {
                rate = rebate.getDiscountRebateRate();
            } else {
                rate = rebate.getRebateRate();
            }
            return rate;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.releaseConnection(connection);
            return rate;
        } catch (EntityNotFoundException e) {
        }

        try {
            String sql = "SELECT * FROM " + table + " "
                + "WHERE itemNumber = '" + itemNumber + "' "
                + "AND discountBeginDate <= '" + today + "' "
                + "AND discountEndDate >= '" + today + "' " 
                + "ORDER BY discountBeginDate DESC LIMIT 1";
            dacBase = getSingleObject(connection, Rebate.class, sql);
            Rebate rebate = (Rebate)dacBase;
            Date discBDate = rebate.getDiscountBeginDate();
            Date discEDate = rebate.getDiscountEndDate();
            dt = java.sql.Date.valueOf(today);
            if (dt.compareTo(discBDate) >=0 && dt.compareTo(discEDate) <= 0) {
                rate = rebate.getDiscountRebateRate();
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } catch (EntityNotFoundException e1) {
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return rate;    
    }
    

    public String getStoreID() {
		return (String) getFieldValue("storeID");
	}

	public String getItemNumber() {
		return (String) getFieldValue("itemNumber");
	}

	public java.sql.Date getBeginDate() {
		return (java.sql.Date) getFieldValue("beginDate");
	}

	public java.sql.Date getEndDate() {
		return (java.sql.Date) getFieldValue("endDate");
	}

    public HYIDouble getRebateRate() {
        return (HYIDouble) getFieldValue("rebateRate");
    }

    public java.sql.Date getDiscountBeginDate() {
        return (java.sql.Date) getFieldValue("discountBeginDate");
    }

    public java.sql.Date getDiscountEndDate() {
        return (java.sql.Date) getFieldValue("discountEndDate");
    }

    public HYIDouble getDiscountRebateRate() {
        return (HYIDouble)getFieldValue("discountRebateRate");
    }

}
