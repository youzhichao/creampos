package hyi.cream.dac;

import hyi.cream.inline.Server;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.groovydac.Param;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import hyi.cream.util.DbConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
/**
 * @author ll
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class LineOffTransfer {

    private static LineOffTransfer instance = null;
    private static Properties upload_tables = null;
    private final static String path = CreamToolkit.getConfigDir() + "serialized" + File.separator;
    private static Map keyMap = null;
    transient static final SimpleDateFormat dfyyyyMMdd = CreamCache.getInstance().getDateFormate();
    transient static final SimpleDateFormat dfyyyyMMddHHmmss = CreamCache.getInstance()
        .getDateTimeFormate();

    static {
        upload_tables = new Properties();
        //Bruce/20030113> Discard upload_tables.conf
        upload_tables.setProperty("tranhead", "posul_tranhead");
        upload_tables.setProperty("trandetail", "posul_trandtl");
        upload_tables.setProperty("shift", "posul_shift");
        upload_tables.setProperty("z", "posul_z");
        upload_tables.setProperty("depsales", "posul_depsales");
        upload_tables.setProperty("daishousales", "posul_daishousales");
        upload_tables.setProperty("cashform", "posul_cashform");
//        upload_tables.setProperty("attendance", "posul_attendance");

    }

    synchronized public static LineOffTransfer getInstance() {
        try {
            if (instance == null)
                instance = new LineOffTransfer();
        } catch (Exception ie){
            ie.printStackTrace(CreamToolkit.getLogger());
            return instance;
        }
        return instance;
    }

    private static String nextToken(StringTokenizer st) {
        String token = st.nextToken();
        if (token.equals("\t")) {
            return "";
        } else {
            try {
                st.nextToken();     // get the delimiter
            } catch (NoSuchElementException e) {
                // the last token don't have delimiter at end
            }
            return token;
        }
    }
	
    /*
     *Common utility methods for uploading...
     */
    //get the relevant field name mapping between local and remote machine
    private static Map getPosToScFieldNameMap(DacBase dac) throws Exception {
        if (!upload_tables.containsKey(dac.getInsertUpdateTableName()))
            throw new Exception ("Invalid upload table:" + dac.getInsertUpdateTableName());
		String[][] name = null;   
//		CreamToolkit.logMessage("LineOfTransfer | getMapByType |  start...");
        if (dac instanceof CashForm) {
	        name = CashForm.getPosToScFieldNameArray();
        } else if (dac instanceof DepSales) {
	        name = DepSales.getPosToScFieldNameArray();
        } else if (dac instanceof LineItem) {
	        name = LineItem.getPosToScFieldNameArray();
        } else if (dac instanceof ShiftReport) {
	        name = ShiftReport.getPosToScFieldNameArray();
        } else if (dac instanceof Transaction) {
	        name = Transaction.getPosToScFieldNameArray();
        } else if (dac instanceof ZReport) {
	        name = ZReport.getPosToScFieldNameArray();
        } else if (dac instanceof DaishouSales) {
	        name = DaishouSales.getPosToScFieldNameArray();
        } else if (dac instanceof DaiShouSales2) {
            name = DaiShouSales2.getPosToScFieldNameArray();
        }/*  else if (dac instanceof Attendance1) {
            name = Attendance1.getPosToScFieldNameArray();
        }*/
//        if (name.length > 0)
//        	CreamToolkit.logMessage("LineOfTransfer | getMapByType | found map !");
        Map map = new HashMap();
        for (int i = 0; i < name.length; i++) {
        	map.put(name[i][0], name[i][1]);
        }
        return map;

    	/*
        if (!upload_tables.containsKey(dac.getInsertUpdateTableName()))
            throw new Exception ("Invalid upload table:" + dac.getInsertUpdateTableName());
        Properties prop = new Properties();
        String str = dac.getClass().getName();
        str = str.substring(str.lastIndexOf(".") + 1);
        str = str.toLowerCase();
        prop.load(new FileInputStream(CreamToolkit.getConfigDir() + str + ".dac"));
        return prop;
        */
    }

	public static Map getDataType(DacBase dac) {
		Map type = new HashMap();
		DbConnection connection = null;
        PreparedStatement ps = null;
        ResultSet  resultSet   = null;
		String sql = "";
		String tableName = dac.getInsertUpdateTableName();
		sql = "select * from " + tableName;
		List keyList = dac.getPrimaryKeyList();
        if (keyList != null && !keyList.isEmpty()) {
        	for (int i = 0; i < keyList.size(); i++) {
        		String key = (String) keyList.get(i);
        		if (i == 0)
        			sql += " where ";
        		else if (i > 0)
        			sql += " and ";
    			sql += key + " = ?";
        	}
        }
        try {
        	Map prop = getPosToScFieldNameMap(dac);
            connection = CreamToolkit.getPooledConnection();
            ps = connection.prepareStatement(sql);
            for (int i = 0; i < keyList.size(); i++) 
            	ps.setObject(i + 1, dac.getFieldValue((String) keyList.get(i)));
            resultSet = ps.executeQuery();
            ResultSetMetaData metaData = resultSet.getMetaData();
            if (resultSet.next()) {
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                	String name = (String) prop.get(metaData.getColumnName(i));
                	if (name != null && !name.trim().equals(""))
		            	type.put(name, String.valueOf(metaData.getColumnType(i)));
                }
            }
        } catch (SQLException e) {
			e.printStackTrace();
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (Exception e) {
			e.printStackTrace();
            CreamToolkit.logMessage("Exception: " + e.getMessage());
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (ps != null)
                    ps.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                e.printStackTrace(CreamToolkit.getLogger());
            } 
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
        return type;
	}

    //Transit the DAC object's map to the new map value
    private static HashMap constructFromDac(DacBase dac) {
        HashMap newMap = new HashMap();
        Map oldMap = dac.getValues();
        try {
            keyMap = getPosToScFieldNameMap(dac);
        } catch (Exception e) { e.printStackTrace(); return null;}
        Iterator itr = keyMap.keySet().iterator();
        while (itr.hasNext()) {
            Object keyObj = itr.next();
            if (oldMap.containsKey(keyObj))
                newMap.put(keyMap.get(keyObj),oldMap.get(keyObj));
            else {
                Object value = null;
                try {
                    //System.out.println("get" + keyObj);
                    Method getMethod = dac.getClass().getDeclaredMethod("get" + keyObj,
                        new Class[0]);
                    //System.out.println(getMethod);
                    value = getMethod.invoke(dac, new Object[0]);
                    newMap.put(keyMap.get(keyObj), value);
                }
                catch (NoSuchMethodException e) {
                    //e.printStackTrace(CreamToolkit.getLogger());
                    //CreamToolkit.logMessage("No such method at " + this);
                    continue;
                } catch (InvocationTargetException e) {
                    e.printStackTrace(CreamToolkit.getLogger());
//                    CreamToolkit.logMessage("Invocation target exception at " + this);
                    continue;
                } catch (IllegalAccessException e) {
                    CreamToolkit.logMessage("LineOffTransfer | " + e.toString());
//                    CreamToolkit.logMessage("Illegal access exception at " + this);
                    continue;
                }

            }
        }
        return newMap;
    }

    /**
     * Retrieve the record data by the format:
     *  "field_name:[first_fieldname]\t....\n" +
     *  "field_type:[first_fieldname]\t....\n" +
     *  "[first_field]\t[second_field]\t...\n" +
     *  "[first_field]\t[second_field]\t...\n" + 
     *  ...
     *  '\b' == null
     */
	public static void dac2File(DacBase dac) throws Exception {
		String tableName = dac.getInsertUpdateTableName();
        if (!upload_tables.containsKey(tableName))
            throw new Exception ("Invalid upload table:" + tableName);
        HashMap newtable = constructFromDac(dac);
		BufferedWriter bw = null;
		String filePath = null;
		try {
            File serilizedDir = new File(path);
            if (!serilizedDir.exists())
                serilizedDir.mkdir();
            String fileName = upload_tables.getProperty(tableName);
            filePath = path + fileName + ".dat";
            File serializedFile = new File (filePath);
            if (serializedFile.exists())
                serializedFile.delete();

            //bw = new BufferedWriter(
            //  new FileWriter(serializedFile)); 
            bw = new BufferedWriter(
                new OutputStreamWriter(
                    new FileOutputStream(serializedFile), "UTF-8"));

			StringBuffer nameBuffer = new StringBuffer();
			StringBuffer valueBuffer = new StringBuffer();
			StringBuffer typeBuffer = new StringBuffer();
			Map type = getDataType(dac);
			int count = 0;
			for (Iterator it = newtable.keySet().iterator(); it.hasNext(); ) {
				String name = (String) it.next();
				String value = "";
				if (newtable.get(name) != null)
					value = newtable.get(name).toString();
				else
					value = String.valueOf('\b');
				if (count == 0) {
            		nameBuffer.append("field_name:");
            		typeBuffer.append("field_type:");
				} else if (count > 0) {
					typeBuffer.append('\t');
					nameBuffer.append('\t');
					valueBuffer.append('\t');
				}
				typeBuffer.append((String) type.get(name));
				nameBuffer.append(name);
				valueBuffer.append(value);					
				count++;
			}           
			typeBuffer.append('\n');
			nameBuffer.append('\n');
			valueBuffer.append('\n');
			bw.write(nameBuffer.toString());			
			bw.write(typeBuffer.toString());
            bw.write(valueBuffer.toString());

            bw.close();
            if (count == 0)
            	serializedFile.delete();
            else
	            packaging(path, fileName);
		} catch (IOException e) {
			e.printStackTrace();
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
            	if (bw != null)
            		bw.close();
			} catch (IOException e) {
	            e.printStackTrace(CreamToolkit.getLogger());
	        }
        }
	}
	
    /**
     * Retrieve the record data by the format:
     *  "field_name:[first_fieldname]\t....\n" +
     *  "field_type:[first_fieldname]\t....\n" +
     *  "[first_field]\t[second_field]\t...\n" +
     *  "[first_field]\t[second_field]\t...\n" + 
     *  ...
     * 	'\b' == null
     */
	public static void dac2File(DacBase[] dac) throws Exception {
		int length = dac.length;
		if (length <= 0)
			return;
		
		String tableName = "";
		BufferedWriter bw = null;
		String filePath = null;
		File serializedFile = null;
		Map type = new HashMap();
		String fileName = "";
		try {
			int count = 0;
			for (int i = 0; i < length; i++) {
				count++;
				if (count == 1) {
					type = getDataType(dac[i]);
					tableName = dac[i].getInsertUpdateTableName();
			        if (!upload_tables.containsKey(tableName))
			            throw new Exception ("Invalid upload table:" + tableName);
		            File serilizedDir = new File(path);
		            if (!serilizedDir.exists())
		                serilizedDir.mkdir();
		            fileName = upload_tables.getProperty(tableName);
		            filePath = path + fileName + ".dat";
		            serializedFile = new File (filePath);
		            if (serializedFile.exists())
		                serializedFile.delete();

		            //bw = new BufferedWriter(new FileWriter(serializedFile)); 
                    bw = new BufferedWriter(
                        new OutputStreamWriter(
                            new FileOutputStream(serializedFile), "UTF-8"));
				}
		        HashMap newtable = constructFromDac(dac[i]);
			
				StringBuffer nameBuffer = new StringBuffer();
				StringBuffer valueBuffer = new StringBuffer();
				StringBuffer typeBuffer = new StringBuffer();
				int j = 0;
				for (Iterator it = newtable.keySet().iterator(); it.hasNext(); ) {
					String name = (String) it.next();
					String value = "";
					if (newtable.get(name) != null)
						value = newtable.get(name).toString();
					else
						value = String.valueOf('\b');
					if (j > 0) {
						valueBuffer.append('\t');
					}
					if (count == 1) {
						if (j == 0) {
		            		nameBuffer.append("field_name:");
		            		typeBuffer.append("field_type:");
						} else if (j > 0) {
							nameBuffer.append('\t');
							typeBuffer.append('\t');
						}
						nameBuffer.append(name);
						typeBuffer.append((String) type.get(name));
						if (tableName.equalsIgnoreCase("tranhead"))
						System.out.println("Column name : " + name + " | type : " + type.get(name) + " | value : " + value);
					}
					valueBuffer.append(value);					
					j++;
				}            
				if (count == 1) {
					nameBuffer.append('\n');
					typeBuffer.append('\n');
					bw.write(nameBuffer.toString());
					bw.write(typeBuffer.toString());
				}
	            bw.write(valueBuffer.toString());
			}
            bw.close();
            if (count == 0)
            	serializedFile.delete();
            else
	            packaging(path, fileName);
		} catch (IOException e) {
			e.printStackTrace();
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
            	if (bw != null)
            		bw.close();
			} catch (IOException e) {
	            e.printStackTrace(CreamToolkit.getLogger());
	        }
        }
	}
	
	public static void db2File(DacBase dac) throws Exception {
		List keyList = dac.getPrimaryKeyList();
		String tableName = dac.getInsertUpdateTableName();
		Map queryMap = new HashMap();
		//String sql = "";
		for (int i = 0; i < keyList.size(); i++) {
			String key = (String) keyList.get(i);
			queryMap.put(key, dac.getFieldValue(key));
		}
		
		List queryList = new Vector();
		queryList.add(queryMap);
		Map prop = getPosToScFieldNameMap(dac);
		db2File(tableName, null, queryList, prop);
	}
	
	public static void db2File(Object[] dac) throws Exception {
		List keyList = new Vector();
		List queryList = new Vector();
		String tableName = "";
		for (int i = 0; i < dac.length; i++) {
			DacBase dacbase = (DacBase) dac[i];
			if (i == 0) {
				keyList = dacbase.getPrimaryKeyList();
				tableName = dacbase.getInsertUpdateTableName();
			}
			Map queryMap = new HashMap();
			//String sql = "";
			for (int j = 0; j < keyList.size(); j++) {
				String key = (String) keyList.get(j);
				queryMap.put(key, dacbase.getFieldValue(key));
			}
			queryList.add(queryMap);
		}
		Map prop = getPosToScFieldNameMap((DacBase) dac[0]);
		db2File(tableName, null, queryList, prop);
	}
	
	public void shiftDb2Disk(DacBase dac) throws Exception {
		List keyList = dac.getPrimaryKeyList();
		int startSeq = ((Integer) dac.getFieldValue("SGNONSEQ")).intValue();
		int endSeq = ((Integer) dac.getFieldValue("SGNOFFSEQ")).intValue();
		Map queryMap = new HashMap();
		String sql = "";
		for (int i = 0; i < keyList.size(); i++) {
			String key = (String) keyList.get(i);
			queryMap.put(key, dac.getFieldValue(key));
		}
		
		List queryList = new Vector();
		queryList.add(queryMap);
		Map prop = getPosToScFieldNameMap(dac);
		db2File("shift", null, queryList, prop);

		// get shift.BeginTransactionNumber and shift.EndTransactionNumber
		
		sql = "select * from tranhead where tmtranseq >= " + startSeq
			+ " and tmtranseq <= " + endSeq;
		prop.clear();
		prop.putAll(getPosToScFieldNameMap(new Transaction()));
		db2File("tranhead", sql, null, prop);
		
		prop.clear();
		prop.putAll(getPosToScFieldNameMap(new LineItem()));
		sql = "select * from trandetail where tmtranseq >= " + startSeq
			+ " and tmtranseq <= " + endSeq;
		db2File("trandetail", sql, null, prop);
	}
	
    /**
     * Retrieve the record data by the format:
     *  "field_name:[first_fieldname]\t....\n" +
     *  "field_type:[first_fieldname]\t....\n" +
     *  "[first_field]\t[second_field]\t...\n" +
     *  "[first_field]\t[second_field]\t...\n" + 
     *  ...
     */
	public static void db2File(String tableName, String sql, List queryList, Map prop) throws Exception {
        if (!upload_tables.containsKey(tableName))
            throw new Exception ("Invalid upload table:" + tableName);
//		CreamToolkit.logMessage("LineOffTransfer | db2file | tablename : " + tableName); 
//		CreamToolkit.logMessage("LineOffTransfer | db2file | queryList : " + queryList); 
//		CreamToolkit.logMessage("LineOffTransfer | db2file | prop : " + prop);
        DbConnection connection  = null;
        PreparedStatement ps = null;
        ResultSet  resultSet   = null;
		BufferedWriter bw = null;
		String filePath = null;
		try {
            File serilizedDir = new File(path);
            if (!serilizedDir.exists())
                serilizedDir.mkdir();
            String fileName = upload_tables.getProperty(tableName);
            filePath = path + fileName + ".dat";
            File serializedFile = new File (filePath);
            if (serializedFile.exists())
                serializedFile.delete();
            bw = new BufferedWriter(new FileWriter(serializedFile)); 
            
            if (sql == null || sql.trim().equals(""))
            	sql = "select * from " + tableName;
        	int count = 0;
        	List keyList = new Vector();
        	if (queryList != null)
	        	for (int i = 0; i < queryList.size(); i++) {
					Map queryMap = (Map) queryList.get(i);
					int count1 = 0;		
					boolean added = false;
		            if (queryMap != null && !queryMap.isEmpty()) {
		            	for (Iterator it = queryMap.keySet().iterator(); it.hasNext(); ) {
			        		if (count == 0 && !added) {
			        			sql += " where (";
			        			added = true;
			        		} else if (count > 0 && !added) {
			        			added = true;
			        			sql += " or (";
			        		}
		            		String key = (String) it.next();
		            		if (count1 > 0)
		            			sql += " and ";
		        			sql += key + " = ?";
		        			keyList.add(queryMap.get(key));
		        			count1++;
							if (!it.hasNext())
								sql += " ) ";
		            	}
		            }
		            count++;
	        	}
            connection = CreamToolkit.getPooledConnection();
            ps = connection.prepareStatement(sql);
            for (int i = 0; i < keyList.size(); i++) 
            	ps.setObject(i + 1, keyList.get(i));
            resultSet = ps.executeQuery();
            ResultSetMetaData metaData = resultSet.getMetaData();
	        StringBuffer titleBuffer = new StringBuffer();
	        StringBuffer typeBuffer = new StringBuffer();
			//List columnName = new Vector();

            count = 0;
            //boolean success = false;
            List column1List = new Vector();
            List column2List = new Vector();
            List typeList = new Vector();
            //List valueList = new Vector();
            while (resultSet.next()) {
            	count++;
		        StringBuffer valueBuffer = new StringBuffer();
            	if (count == 1) {
            		titleBuffer.append("field_name:");
            		typeBuffer.append("field_type:");
            	}
				int usedCount = 0;
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                	String name = metaData.getColumnName(i).toString();
                	if (prop.containsKey(name) || prop.containsKey(name.toLowerCase()) ||
                        prop.containsKey(name.toUpperCase())) {
                		usedCount++;
	                    if (usedCount > 1)
    	                    valueBuffer.append('\t');
    	                if (resultSet.getObject(i) != null) {
    	                	String tmp = resultSet.getString(i);
    	                	if (tmp.equals(""))
			                    valueBuffer.append('\b');
			                else
			                    valueBuffer.append(resultSet.getString(i));
    	                } else if (name.equalsIgnoreCase("accdate"))
		                	valueBuffer.append("1970-01-01");
		                else
		                    valueBuffer.append('\b');
	                    if (count == 1) {
		                    if (usedCount > 1) {
	    	                	titleBuffer.append('\t');
	        	            	typeBuffer.append('\t');
	            	        }
	                    	titleBuffer.append(prop.get(name));
	                    	column1List.add(name);
	                    	column2List.add(prop.get(name));
	                    	typeBuffer.append(String.valueOf(metaData.getColumnType(i)));
	                    	typeList.add(new Integer(metaData.getColumnType(i)));
	                    }
                	}
                }
                valueBuffer.append('\n');
                if (count == 1) {
                	titleBuffer.append('\n');
                	typeBuffer.append('\n');
                	bw.write(titleBuffer.toString());
                	bw.write(typeBuffer.toString());
                } 
                bw.write(valueBuffer.toString());
            }
            bw.close();
            if (count == 0)
            	serializedFile.delete();
            else
	            packaging(path, fileName);
		} catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
            	if (bw != null)
            		bw.close();
                if (resultSet != null)
                    resultSet.close();
                if (ps != null)
                    ps.close();
			} catch (IOException e) {
	            e.printStackTrace(CreamToolkit.getLogger());
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            } 
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
	}
	
	public static boolean floppy2Db(String path) {
		File file = new File(path);
//		if (!file.exists() || !file.isDirectory())
//			return false;
		File child[] = file.listFiles();		
		int length = child.length;
		DbConnection connection = null;
		for (int i = 0; i < length; i++) {
			try {
				connection = CreamToolkit.getPooledConnection();
				file2Db(child[i].getPath(), connection);
			} catch (SQLException e) {
				e.printStackTrace();
	            CreamToolkit.logMessage("SQLException: " + e.getMessage());
	            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
	            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
	            e.printStackTrace(CreamToolkit.getLogger());
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
	            CreamToolkit.releaseConnection(connection);
			}
		}
		return true;
	}
	
	public static boolean floppy2Db(String path, DbConnection connection)
		throws SQLException, IOException
	{
		File file = new File(path);
		File child[] = file.listFiles(new FilenameFilter() {
	        public boolean accept(File dir, String name) {
	        	return name.endsWith(".zip");
	        }
		});
		int length = child.length;
		System.out.println("found " + length + " files");
		for (int i = 0; i < length; i++) {
			try {
				file2Db(child[i].getPath(), connection);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public static boolean floppy2Db2(String path, DbConnection connection)
		throws SQLException, IOException
	{
		File file = new File(path);
		File child[] = file.listFiles(new FilenameFilter() {
	        public boolean accept(File dir, String name) {
	        	return name.endsWith(".zip");
	        }
		});
		int length = child.length;
		System.out.println("found " + length + " files");
		for (int i = 0; i < length; i++) {
			System.out.println("file name : " + child[i].getPath());
			try {
				file2Db2(child[i].getPath(), connection);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public static boolean file2Db(String fileName, DbConnection connection) {
		boolean ret = false;
		BufferedReader br = null;
        PreparedStatement ps = null;
        String sql = "";
		String tableName = "";
		try {
			Server.setFakeExist();
			tableName = fileName.substring(fileName.lastIndexOf(File.separator) + 1
				, fileName.lastIndexOf("."));
System.out.println("Start to insert into  " + tableName + " from " + fileName);				
			if (tableName == null || tableName.trim().equals(""))
				return false;
			ZipInputStream in = new ZipInputStream(new FileInputStream(fileName));
			in.getNextEntry();
			br = new BufferedReader(new InputStreamReader(in));
			StringBuffer sqlTitle = new StringBuffer();			
			String data = "";
			List columnList = new Vector();
			List fieldList = new Vector();
			List typeList = new Vector();
			sqlTitle.append("insert into ").append(tableName).append(" (");
	        int count = 0;
	        while ((data = br.readLine()) != null) {
//System.out.println("readLine() : " + data);              

				if (data.startsWith("field_name")) {
			        StringTokenizer tokenizer = new StringTokenizer(
			        	data.substring(data.indexOf(':') + 1, data.length()), "\t", true);
			        if (!tokenizer.hasMoreTokens())
			            return false;
			        count = 0;
			        while (tokenizer.hasMoreTokens()) {
			        	columnList.add(nextToken(tokenizer));
			        	count++;
			        }
				} else if (data.startsWith("field_type")) {
					typeList.clear();
			        StringTokenizer tokenizer = new StringTokenizer(
			        	data.substring(data.indexOf(':') + 1, data.length()), "\t", true);
			        if (!tokenizer.hasMoreTokens())
			            return false;
			        count = 0;
			        while (tokenizer.hasMoreTokens()) {
			        	typeList.add(nextToken(tokenizer));
			        	count++;
			        }
				} else {
					fieldList.clear();
			        StringTokenizer tokenizer = new StringTokenizer(data, "\t", true);
			        StringBuffer sqlValue = new StringBuffer();
			        StringBuffer sqlColumn = new StringBuffer();
			        count = 0;
			        int usedCount = 0;
			        while (tokenizer.hasMoreTokens()) {
			        	String value = nextToken(tokenizer); 
			        	int type = Integer.parseInt((String) typeList.get(count));
			        	if (!value.equalsIgnoreCase(String.valueOf('\b'))) {
				        	if (usedCount > 0) {
				        		sqlColumn.append(",");
				        		sqlValue.append(",");
				        	}
				        	sqlColumn.append(columnList.get(count));
				        	sqlValue.append("?");
				        	fieldList.add(convertType(value, type));
				        	usedCount++;
			        	} 
			        	count++;
			        }	        
			        sqlColumn.append(") values (");
					sqlValue.append(")");
					sql = sqlTitle.toString() + sqlColumn.toString() + sqlValue.toString();
		            ps = connection.prepareStatement(sql.toString());
					for (int i = 0; i < fieldList.size(); i++) {
						ps.setObject(i + 1, fieldList.get(i));
					}
					try {
						ps.executeUpdate();
					} catch (SQLException e) {
						e.printStackTrace();
			            CreamToolkit.logMessage("SQLException: " + e.getMessage());
			            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
			            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
			            e.printStackTrace(CreamToolkit.getLogger());
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (SQLException e) {
			e.printStackTrace();
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
            	if (br != null)
            		br.close();
                if (ps != null)
                    ps.close();
			} catch (Exception e) {
            } 
        }
System.out.println("Processing " + fileName +  " finished.");				
        return ret;
	}
	
	public static boolean file2Db2(String fileName, DbConnection connection)
		throws IOException, SQLException
	{
		boolean ret = false;
		BufferedReader br = null;
        PreparedStatement ps = null;
        String sql = "";
		String tableName = "";
		tableName = fileName.substring(fileName.lastIndexOf(File.separator) + 1
			, fileName.lastIndexOf("."));
		if (tableName == null || tableName.trim().equals(""))
			return false;
		System.out.println("file2db2 | tablename : " + tableName);
		ZipInputStream in = new ZipInputStream(new FileInputStream(fileName));
		/*ZipEntry entry =*/in.getNextEntry();
		System.out.println("zip entry");
		br = new BufferedReader(new InputStreamReader(in));
		StringBuffer sqlTitle = new StringBuffer();			
		String data = "";
		List fieldList = new Vector();
		List titleList = new Vector();
		List typeList = new Vector();
		sqlTitle.append("insert into ").append(tableName).append(" (");
        int count = 0;
        while ((data = br.readLine()) != null) {
			if (data.startsWith("field_name")) {
				System.out.println("get field name");
		        StringTokenizer tokenizer = new StringTokenizer(
		        	data.substring(data.indexOf(':') + 1, data.length()), "\t", true);
		        if (!tokenizer.hasMoreTokens())
		            return false;
		        count = 0;
		        while (tokenizer.hasMoreTokens()) {
		            String name = nextToken(tokenizer);
		        	if (count > 0)
		        		sqlTitle.append(",");
		        	sqlTitle.append(name);
		        	titleList.add(name);
//		        	System.out.println(count + " | column name : " + name);
		        	count++;
		        }
		        sqlTitle.append(") values (");
			} else if (data.startsWith("field_type")) {
				System.out.println("get field type");
				typeList.clear();
		        StringTokenizer tokenizer = new StringTokenizer(
		        	data.substring(data.indexOf(':') + 1, data.length()), "\t", true);
		        if (!tokenizer.hasMoreTokens())
		            return false;
		        count = 0;
		        while (tokenizer.hasMoreTokens()) {
		        	String type = nextToken(tokenizer);
		        	typeList.add(type);
//System.out.println(count + " | type : " + type);
		        	count++;
		        }
			} else {
				System.out.println("get data");
				fieldList.clear();
		        StringTokenizer tokenizer = new StringTokenizer(data, "\t", true);
		        count = 0;
		        while (tokenizer.hasMoreTokens()) {
			        StringBuffer sqlValue = new StringBuffer();
		        	String value = nextToken(tokenizer);
//		        	System.out.print(count + " | value : " + value); 
		        	sqlValue.append("?");
		        	int type = Integer.parseInt((String) typeList.get(count));
//		        	System.out.println(" | type : " + type);
		        	if (!value.equalsIgnoreCase(String.valueOf('\b')))
			        	fieldList.add(convertType(value, type));
			        else	
			        	continue;
					sql = "update " + tableName + " set " + (String) titleList.get(count)
						+ " = ?";
					try {
						System.out.println(count + " | " + titleList.get(count) + " | " + fieldList.get(count));
			            ps = connection.prepareStatement(sql.toString());
						ps.setObject(1, fieldList.get(count));
						ps.executeUpdate();
						ps.close();
//System.out.println(count + " | sql : "  + sql);						
					} catch (Exception e) {
						System.out.println("Error | sql : " + sql + " | value : " + fieldList.get(count));
						System.out.println("Error | value : " + fieldList.get(count));
						e.printStackTrace();
					}
		        	count++;
		        }	        
			}
		}
    	if (br != null)
    		br.close();
        return ret;
	}
	
	public static void packaging(String path, String fileName) throws IOException {
		File file = new File(path + File.separator + fileName + ".dat");
		String zipFileName = fileName + ".zip";
System.out.println("packaging ......." + zipFileName);
		File zipFile = new File(path + File.separator + zipFileName);
		if (zipFile.exists())
			zipFile.delete();
		ZipOutputStream zop = new ZipOutputStream(new FileOutputStream
			(new File(path + File.separator + zipFileName)));
		FileInputStream in = new FileInputStream(path + File.separator + fileName + ".dat");
		byte[] buf = new byte[1024];		
		zop.putNextEntry(new ZipEntry(fileName + ".dat"));
        int len;
        while ((len = in.read(buf)) > 0) {
            zop.write(buf, 0, len);
        }
        // Complete the entry
        zop.closeEntry();
        zop.close();
        in.close();
        file.delete();
System.out.println("packaging | file delete : " + file.getPath());
	}
	
	public static void unPackaging(String fileName) throws IOException {
		ZipInputStream in = new ZipInputStream(new FileInputStream(fileName));
		ZipEntry entry = in.getNextEntry();
		OutputStream out = new FileOutputStream(entry.getName());
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        out.close();
        in.close();
	}
	
	public static void testDb(String tableName1, String tableName2, List keyList
		, String propFile , DbConnection conn1, DbConnection conn2, String posWhere, String scWhere)
		throws IOException, SQLException
	{
		Statement statement = null;
		ResultSet resultSet = null;
        PreparedStatement ps = null;
        statement = conn1.createStatement();
        resultSet = statement.executeQuery("select * from " + tableName1 + " " + posWhere);
        // ResultSetMetaData metaData = resultSet.getMetaData();
        HashMap map = new HashMap();
        DacBase.fillResultMap(resultSet, map);
		
        Properties prop = new Properties();
        if (propFile == null || propFile.trim().equals("")) 
			for (Iterator it = map.keySet().iterator(); it.hasNext();) {
				Object key = it.next();
				prop.put(key, key);
			}
        else
        	prop.load(new FileInputStream(propFile));
        
        if (map.isEmpty()) {
        	System.out.println(tableName1 + " no data");
        	return ;
        }
        String sql = "";
        StringBuffer columnLine = new StringBuffer();
        StringBuffer valueLine = new StringBuffer();
        List valueList = new Vector();
        int keyLength = keyList.size();
		for (int i = 0; i < keyLength; i++) {
			if (i > 0) {
				columnLine.append(",");
				valueLine.append(",");
			}
			String columnName = (String) keyList.get(i);
			columnLine.append(prop.getProperty(columnName));
			valueLine.append("?");
			valueList.add(map.get(columnName));
		}       
		
		if (keyLength > 0) {
			sql = "insert into " + tableName2 + " (" + columnLine.toString() + ") values ("
				+ valueLine.toString() + ")";
            ps = conn2.prepareStatement(sql);
            for (int i = 0; i < keyLength; i++)
            	ps.setObject(i + 1, valueList.get(i));
			System.out.println("start insert key | sql : " + sql + " | value : " + valueList);
           	ps.executeUpdate();
           	ps.close();
           	System.out.println("key insert success!");
		}
		List usedColumn = new Vector(keyList);
		for (Iterator it = map.keySet().iterator(); it.hasNext();) {
			String columnName = (String) it.next();
			if (usedColumn.contains(columnName) || prop.getProperty(columnName) == null) 
				continue;
			usedColumn.add(columnName);			
			try {
				sql = "update " + tableName2 + " set " + prop.getProperty(columnName) + " = ?"
					 + " " + scWhere;
	            ps = conn2.prepareStatement(sql);
            	ps.setObject(1, map.get(columnName));
            	ps.execute();
            	ps.close();
			} catch (Exception e) {
				System.out.println("Error | columnName : " + columnName + " | value : " + map.get(columnName));
				System.out.println("sql=" + sql);
				System.out.println("sc fieldName=" + prop.getProperty(columnName));

				e.printStackTrace();
			}	
		}			
	}

    public String toString(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ? "": str);
    }

    private static HYIDouble toBigDecimal(String str) {
        try {
            if (str == null || str.length() == 0 || str.equalsIgnoreCase("null"))
                return new HYIDouble(0);
            else {
                return new HYIDouble(str).setScale(2);
            }
        } catch (NumberFormatException e) {
            return new HYIDouble(0);
        }
    }

    private static Integer toInteger(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ?
            new Integer(0) : new Integer(str));
    }

    private static Long toLong(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ?
            new Long(0) : new Long(str));
    }

	private static Double toDouble(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ?
            new Double(0) : new Double(str));
	}
	
	private static Float toFloat(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ?
            new Float(0) : new Float(str));
	}
	
    private static Boolean toBoolean(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ? Boolean.FALSE :
            (str.equalsIgnoreCase("Y") || str.equals("true")) ? Boolean.TRUE : Boolean.FALSE);
    }

    private static java.sql.Date toDate(String str) {
        if (str == null || str.length() == 0 || str.equalsIgnoreCase("null"))
            return null;
        try {
            java.util.Date d = null;
            if (str.length() == 10) {
                d = dfyyyyMMdd.parse(str);
                //if (!useJVM)
                //    d = new java.util.Date(d.getTime() + 16 * 60 * 60 * 1000);
            } else if (str.length() == 19) {
                d = dfyyyyMMddHHmmss.parse(str);
            }
            if (d == null) 
                return null;
            else
                return new java.sql.Date(d.getTime());
        } catch (ParseException e) {
            return null;
        }
    }

    private static java.sql.Time toTime(String str) {
        if (str == null || str.length() == 0 || str.equalsIgnoreCase("null"))
            return null;
        java.sql.Time t = new java.sql.Time(0);
        if (str.lastIndexOf(":") != -1) {
            try {
                StringTokenizer st0 = new StringTokenizer(str, ":");
                String hour = st0.nextToken().trim();
                String minute = st0.nextToken().trim();
                String second = "0";
                if (hour.equals("24")){
                    hour="23";
                    minute="59";
                }   
                if (!st0.hasMoreTokens()) {
                    second="0";   
                } else {
                    second = st0.nextToken().trim();
                    if (second == null || second.length() == 0)
                        second = "0";
                }
                java.util.Date d = new java.util.Date(
                    Integer.parseInt(hour) * 60 * 60 * 1000 +
                    Integer.parseInt(minute) * 60 * 1000 +
                    Integer.parseInt(second) * 1000);
                t = new java.sql.Time(d.getTime());
                return t;
            } catch (NoSuchElementException e) {
                CreamToolkit.logMessage(" nosunchelement "+e);
            } catch(NumberFormatException e2){
                CreamToolkit.logMessage(" numberformatexception "+e2);
            }
            return t;
        } else {
            try {
                if (str.length() >= 4) {
                    String hour = str.substring(0, 2);
                    String minute = str.substring(2, 4);
                    String second = str.substring(4,5);
                    if (hour.equals("24")) {
                        hour="23";
                        minute="59";
                    }
                    if (second == null || second.length() == 0)
                        second = "00";
                    java.util.Date d = new java.util.Date(
                        Integer.parseInt(hour) * 60 * 60 * 1000 +
                        Integer.parseInt(minute) * 60 * 1000 +
                        Integer.parseInt(second) * 1000);
                    t = new java.sql.Time(d.getTime());
                    return t;
                }        
            } catch (NumberFormatException e) {
            }
            return t;
        }
    }
	
	private static Object convertType(String value, int type) {
		Object ret = null;
		try {
			if (type == java.sql.Types.ARRAY) {
                ret = value;
				// return java.sql.Array
			} else if (type == java.sql.Types.LONGVARBINARY) {
                ret = value;
				// return InputStream
			} else if (type == java.sql.Types.REF) {
                ret = value;
				// return java.sql.Ref
			} else if (type == java.sql.Types.BLOB) {
                ret = value;
				// return java.sql.Blob
			} else if (type == java.sql.Types.CLOB) {
                ret = value;
				// return java.sql.Clob
			} else if (type == java.sql.Types.DATE) {
				ret = toDate(value);
			} else if (type == java.sql.Types.TIME) {
				ret = toTime(value);
			} else if (type == java.sql.Types.TIMESTAMP) {
				ret = toTime(value);
			} else if (type == java.sql.Types.BIGINT) {
				ret = toLong(value);
			} else if (type == java.sql.Types.BIT) {
				ret = toBoolean(value);
			} else if (type == java.sql.Types.TINYINT
				|| type == java.sql.Types.INTEGER
				|| type == java.sql.Types.SMALLINT
				|| type == java.sql.Types.NUMERIC)
			{
				ret = toInteger(value);
			} else if (type == java.sql.Types.REAL) {
				ret = toFloat(value);
			} else if (type == java.sql.Types.FLOAT 
				|| type == java.sql.Types.DOUBLE) 
			{
				ret = toDouble(value);
			} else if (type == java.sql.Types.DECIMAL) {
				ret = toBigDecimal(value);
			} else if (type == java.sql.Types.VARCHAR
				|| type == java.sql.Types.CHAR) 
			{
				ret = value;
			} else {
				ret = value;
			}
		} catch (Exception e) {
			ret = value;
		}
		return ret;
	}

    // Offline status: two steps available
    // firstly, transfer the DAC to a map and serialize it to the hard disk
    // secondly, prompt user to insert a floppy disk and copy the serialized data to floppy.
    public boolean saveToDisk(DacBase dac) {
        HashMap newtable = constructFromDac(dac);
        try {
            //String filePath = path + dac.getClass().getName().substring(dac.getClass().getName().lastIndexOf(".") + 1) + ".dat";
            File serilizedDir = new File(path);
            if (!serilizedDir.exists())
                serilizedDir.mkdir();
            String fileName = upload_tables.getProperty(dac.getInsertUpdateTableName());
            String filePath = path + fileName + ".dat";
            File serializedFile = new File (filePath);
            if (serializedFile.exists())
                serializedFile.delete();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(serializedFile));
            out.writeObject(newtable);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return false;
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return false;
        }
        return true;
    }

    //Bruce/2002-1-28
    // This method will not translate field name for you.
    public boolean saveToDisk(Object[] dacs) {
        for (int i = 0; i < dacs.length; i++) {
            DacBase dac = (DacBase)dacs[i];
            Map newtable = dac.getFieldMap();
            try {
                //String filePath = path + dac.getClass().getName().substring(dac.getClass().getName().lastIndexOf(".") + 1) + ".dat";
                File serilizedDir = new File(path);
                if (!serilizedDir.exists())
                    serilizedDir.mkdir();
                String fileName = upload_tables.getProperty(dac.getInsertUpdateTableName());
                //Bruce/2002-03-29
                // To fix a problem SC cannot correctly read depsales into database from floppy disk
                //String filePath = path + fileName + "." + i + ".dat";
                String filePath = path + fileName + "." + i;
                File serializedFile = new File (filePath);
                if (serializedFile.exists())
                    serializedFile.delete();
                ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(serializedFile));
                out.writeObject(newtable);
                out.flush();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace(CreamToolkit.getLogger());
                return false;
            } catch (IOException e) {
                e.printStackTrace(CreamToolkit.getLogger());
                return false;
            }
        }
        return true;
    }
	
    //modify by lxf 2003.01.23
    public boolean saveToDisk2(DacBase dac) {
        try {
        	String tableName = dac.getInsertUpdateTableName();
        	if (tableName.equalsIgnoreCase("shift"))
        		shiftDb2Disk(dac);
        	else
        		db2File(dac);
        } catch (FileNotFoundException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return false;
        } catch (Exception e) {
        	e.printStackTrace();
            e.printStackTrace(CreamToolkit.getLogger());
            return false;
        }
        return true;
    }//*/

    //modify by lxf 2003.01.23
    public static boolean saveToDisk2(Object[] dac) {
        try {
    		db2File(dac);
        } catch (FileNotFoundException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return false;
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return false;
        }
        return true;
    }//*/

    //modify by lxf 2003.01.23
    public static boolean saveToFloppyDisk(boolean cleanFilesOnHardDisk) {
        Runtime currentApp = Runtime.getRuntime();
        String comd = Param.getInstance().getSaveCommand();
        StringTokenizer st = new StringTokenizer(comd, ";");
        int result = -1;
        while (st.hasMoreTokens()) {
        	String command = st.nextToken();
	        StringTokenizer token = new StringTokenizer(command, " ");
	        ArrayList commandLine = new ArrayList();
	        while (token.hasMoreTokens()) {
	            commandLine.add(token.nextToken());
	        }
	        String [] com = new String [commandLine.size()];
	        for(int i = 0; i < commandLine.size(); i++) {
	            com[i] = (String)commandLine.get(i);
	        }
	        try {
	            CreamToolkit.logMessage("Save data to floppy disk 1 ." + commandLine);
	            Process c = currentApp.exec(com);
	            c.waitFor();
	            result = c.exitValue();
	            CreamToolkit.logMessage("Save data to floppy disk 1 | result " + result);
	        } catch (Exception e) {
	            e.printStackTrace(CreamToolkit.getLogger());
	            return false;
	        }
        }
        
        File serilizedDir = new File(CreamToolkit.getConfigDir() + "serialized" + File.separator);
        if (!serilizedDir.exists())
            serilizedDir.mkdir();
        if (cleanFilesOnHardDisk) {
            File [] all = serilizedDir.listFiles();
            for (int i = 0; i < all.length; i++) {
                File currentFile = all[i];
                if (currentFile.isFile() &&
                    (currentFile.getName().endsWith("dat") || currentFile.getName().startsWith("posul")))
                    currentFile.delete();
            }
        }
		if (result == 0)
	        return true;
	    else 
	    	return false;
    }

    //modify by lxf 2003.01.23
    public boolean saveToFloppyDisk2(boolean cleanFilesOnHardDisk) {
        Runtime currentApp = Runtime.getRuntime();
        String comd = Param.getInstance().getSaveCommand();
        StringTokenizer st = new StringTokenizer(comd, ";");
        int result = -1;
        while (st.hasMoreTokens()) {
        	String command = st.nextToken();
	        StringTokenizer token = new StringTokenizer(command, " ");
	        ArrayList commandLine = new ArrayList();
	        while (token.hasMoreTokens()) {
	            commandLine.add(token.nextToken());
	        }
	        String [] com = new String [commandLine.size()];
	        for(int i = 0; i < commandLine.size(); i++) {
	            com[i] = (String)commandLine.get(i);//{"edit.com", "d:\\e.log"};//"c:\\err.log", "a:\\"
	        }
	        try {
	            CreamToolkit.logMessage("Save data to floppy disk 2 ." + commandLine);
	            Process c = currentApp.exec(com);
	            c.waitFor();
	            result = c.exitValue();
	            CreamToolkit.logMessage("Save data to floppy disk 2 | result : " + result);
	        } catch (Exception e) {
	        	e.printStackTrace();
	            e.printStackTrace(CreamToolkit.getLogger());
	            return false;
	        }

	        if (result != 0)
	            return false;
        }
        File serilizedDir = new File(CreamToolkit.getConfigDir() + "serialized" + File.separator);
        if (!serilizedDir.exists())
            serilizedDir.mkdir();
        if (cleanFilesOnHardDisk) {
            File [] all = serilizedDir.listFiles();
            for (int i = 0; i < all.length; i++) {
                File currentFile = all[i];
                if (currentFile.isFile() &&
                    (currentFile.getName().endsWith("dat") || currentFile.getName().startsWith("posul")))
                    currentFile.delete();
            }
        }
		if (result == 0)
	        return true;
	    else 
	    	return false;
    }
	
	
    public static void main(String[] args) {
    	System.out.println("2003.02.19 15:40");
		try {
			//String ip = "192.168.1.36";
			String path = "a:";
			boolean test = false;
			//Map para = new HashMap();
			if (args.length == 1) {
				if (args[0].equals("-help")) {
					System.out.println("usage: java hyi.cream.dac.LineOffTransfer [path] [ip] [-test]");
					return;
				} else if (args[0].equals("-test")) {
					test = true;
				} else {
					path = args[0];
				}
			} else if (args.length == 2) {
				path = args[0];
				//ip = args[1];
			}
			System.out.println("Read data from: " + path); // + " | ip : " + ip);
			/*
			Server.setFakeExist();
			Properties dbProp = new Properties();
                Class.forName("com.sybase.jdbc2.jdbc.SybDriver").newInstance();
                dbProp.put("user","dba");
                dbProp.put("password", "sql");
                dbProp.put("useUnicode", "TRUE");
                dbProp.put("characterEncoding", "GBK");                
			DbConnection scConn = DriverManager.getConnection("jdbc:sybase:Tds:" + ip + ":2638/cake", dbProp);
			*/
            Server.setFakeExist();
			DbConnection scConn = CreamToolkit.getPooledConnection();
			if (test) {
				System.out.println("use test");
				LineOffTransfer.floppy2Db2(path, scConn);	
			} else
				LineOffTransfer.floppy2Db(path, scConn);	
			CreamToolkit.releaseConnection(scConn);		
		} catch (Exception e) {
			e.printStackTrace();
		} 
    }
}
