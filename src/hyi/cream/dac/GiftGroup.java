package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GiftGroup extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

	transient public static final String VERSION = "1.0";

	static final String tableName = "giftgroup";

	private static ArrayList primaryKeys = new ArrayList();
	static {
		primaryKeys.add("ID");
	}

	public GiftGroup() {
		super();
	}

	public String getInsertUpdateTableName() {
		return tableName;
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getID() {
		return (String) getFieldValue("ID");
	}

	public String getItemNO() {
		return (String) getFieldValue("itemNO");
	}

	public String getGroupNO() {
		return (String) getFieldValue("groupNO");
	}
	
	public String getCategory() {
		return (String) getFieldValue("category");
	}
	
	public HYIDouble getPrice() {
		return (HYIDouble) getFieldValue("price");
	}
	
	public static Iterator getGiftGroups(DbConnection connection, String id) {
		try {
            return getMultipleObjects(connection, GiftGroup.class, "SELECT * FROM "
            		+ tableName + " WHERE ID = '" + id + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}
}
