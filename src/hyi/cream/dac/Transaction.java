package hyi.cream.dac;

import hyi.cream.POSTerminalApplication;
import hyi.cream.SystemInfo;
import hyi.cream.event.TransactionEvent;
import hyi.cream.event.TransactionListener;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.groovydac.CardDetail;
import hyi.cream.groovydac.Param;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.inline.InlineCommandExecutor;
import hyi.cream.inline.Server;
import hyi.cream.state.GetProperty;
import hyi.cream.state.StateToolkit;
import hyi.cream.state.cat.CATAuthSalesFailedState;
import hyi.cream.state.cat.CATAuthSalesState;
import hyi.cream.uibeans.ItemList;
import hyi.cream.util.*;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Transaction DAC class.
 * 
 * @author Dai, Slackware, Bruce
 * @version 1.6
 */
public class Transaction extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ZhaoHong /2003-07-21 增加方法 deepClone() 深度克隆 * /Meyer/1.6/2003-01-22
     * 增加方法store2(), 与新的组合促销方案配套，将数据更新到数据库 增加方法getMMTotalAmount() 计算组合促销总计金额
     * /Burce/1.6/2002-04-28 修正若后台有重复两笔交易序号相同的交易，会导致后台会不停的和前台要交易数据 的问题.
     * /Bruce/1.5/2002-03-10/ Add/Modify some methods for preparing to use in
     * new inline:
     */
    public static final String VERSION = "1.6";

    private static List<String> primaryKeys = new ArrayList<String>();
    private List<LineItem> lineItems = new ArrayList<LineItem>();
    private List<Alipay_detail> alipayList = new ArrayList<Alipay_detail>();
    private List<WeiXin_detail> weiXinList = new ArrayList<WeiXin_detail>();
    private List<UnionPay_detail> unionpayList = new ArrayList<UnionPay_detail>();
    private List<Selfbuy_head> selfbuyHeadList = new ArrayList<Selfbuy_head>();
    private List<Selfbuy_detail> selfbuyDetailList = new ArrayList<Selfbuy_detail>();
    private List<EcDeliveryHead> ecDeliveryHeadList = new ArrayList<EcDeliveryHead>();
    private List<Cmpay_detail> cmpayList = new ArrayList<Cmpay_detail>();
    //protected ArrayList lineItemArrayLast = new ArrayList(); // transient
    private transient List transactionListener = new ArrayList();
    private ArrayList<Payment> paymentArray = new ArrayList<Payment>();
    private Map<Payment, HYIDouble> netPayments = new HashMap<Payment, HYIDouble>();
    protected LineItem currentLineItem = null; //currentLineItem is buffer lineItem
    private Payment lastestPayment = null;
    private HYIDouble itemDiscount = new HYIDouble(0);
    private Map<SI, HYIDouble> appliedSIs = new HashMap<SI, HYIDouble>(); // for memorize the SI involved in this transaction
    private Map<String, HYIDouble> appliedMMs = new HashMap<String, HYIDouble>(); // for memorize the SI involved in this transaction
    private Hashtable taxMMAmount = new Hashtable();
    //private Hashtable taxMMCount = new Hashtable();
    private SI lastSI;
    static int i = 0;
    //static public Transaction currentTransaction = null;
    // private static Set cache;
    private static int mmCount = 0;
    private HYIDouble mmAmount = new HYIDouble(0);
    private static HYIDouble originEarnAmount = new HYIDouble(0);
    private HYIDouble totalMMAmount = null;
    private HYIDouble taxAmount = new HYIDouble(0);
    protected HashMap mmMap = new HashMap();
    private LineItem roundDownItem = null;

    static private DateFormat dateFormatter = CreamCache.getInstance().getDateTimeFormate();
    public static final int maxLineItems = PARAM.getMaxLineItems();
    private static int rebateAmountScale = 1;
    private static final String mixAndMatchVersion = PARAM.getMixAndMatchVersion();
    private transient static final Collection existedFieldList = getExistedFieldList(getInsertUpdateTableNameStaticVersion());
    // private static final boolean excudeRebateInSum = GetProperty.getExcludeRebateInSum("no").equalsIgnoreCase("yes");
    // private HYIDouble roundDownAmount = new HYIDouble(0);

    private static final String rebateID = PARAM.getRebatePaymentID();

    // private List daiShouSalesCommitted;

    // 保存交易前设置为false 结束后设置为true
    private boolean storeComplete = false;
    private final Integer integer0 = 0;
    private static final String MIX_AND_MATCH_VERSION = PARAM.getMixAndMatchVersion();
    private static final java.util.Date DEFAULT_END_ACCDATE = getDefaultEndAccDate();

    static final int TRANSACTION_SAVE_BEGIN = 0;
    static final int TRANSACTION_SAVE_LINEITEM = 1;
    static final int TRANSACTION_SAVE_TRAN = 2;
    static final int TRANSACTION_SAVE_TRANEND = 3;
    static final int TRANSACTION_SAVE_SHIFT = 4;
    static final int TRANSACTION_SAVE_Z = 5;
    static final int TRANSACTION_SAVE_DEPSALES = 6;
    static final int TRANSACTION_SAVE_DAISHOUSALES = 7;
    static final int TRANSACTION_SAVE_DAISHOUSALES2 = 8;
    static final int TRANSACTION_SAVE_SHIFT2 = 9;
    static final int TRANSACTION_SAVE_Z2 = 10;
    static final int TRANSACTION_SAVE_SHIFT3 = 11;
    static final int TRANSACTION_SAVE_Z3 = 12;
    static final int TRANSACTION_SAVE_SHIFT4 = 13;
    static final int TRANSACTION_SAVE_Z4 = 14;
    static final int TRANSACTION_SAVE_END = 15;

    private Map diyOverrideAmountMap = new HashMap();
    protected int step = 0;
    private Integer holdTranNo = null;
    // 前日累计还元金
    private HYIDouble beforeTotalRebate = new HYIDouble(0.0);
    // 商品累计的还圆金
    private HYIDouble itemRebateAmt = new HYIDouble(0.0);
    // 额外的还圆金
    private HYIDouble specialRebateAmt = new HYIDouble(0.0);
    // 本次抵扣还元金
    private HYIDouble useRebateAmt = new HYIDouble(0.0);
    private static boolean undoFlag;
    private boolean salesCanNotEnd = false;
    public String printType = "";

    /**
     * 本次交易购物袋金额合计
     */
    private HYIDouble shoppingBagTotalAmount = new HYIDouble(0);
    private HYIDouble homeSendFeeTotalAmount = new HYIDouble(0);

    private String otherStoreID; // 他店退貨時，暫存他店店號

    static {
        try {
            rebateAmountScale = PARAM.getRebateAmountScale();
        } catch (NumberFormatException e) {
            CreamToolkit.logMessage(e);
        }

        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("storeID");
            primaryKeys.add("systemDate");
            primaryKeys.add("posNumber");
            primaryKeys.add("transactionNumber");
        } else {
            primaryKeys.add("TMTRANSEQ");
            // primaryKeys.add("POSNO");
        }
    }

    public Transaction() {
        // nextTransactionNumber = -1;
    }

    private static java.util.Date getDefaultEndAccDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(1971, 0, 1, 0, 0, 0); // = 1971-01-01 00:00:00
        return cal.getTime();
    }

    public static Transaction generateABrandNewTransaction() {
        long time = System.currentTimeMillis();

        POSTerminalApplication pos = POSTerminalApplication.getInstance();
        if (!pos.getMessageIndicator().getMessage().equals(""))
            pos.getMessageIndicator().setMessage("");

        Transaction trans = pos.cloneFromBlankTransaction();

        trans.setTransactionNumber(Transaction.getNextTransactionNumber());
        trans.setInvoiceID(PARAM.getInvoiceID());
        trans.setInvoiceNumber(PARAM.getInvoiceNumber());
        trans.setSignOnNumber(PARAM.getShiftNumber());
        trans.setZSequenceNumber(ZReport.getCurrentZNumber());
        trans.setCashierNumber(PARAM.getCashierNumber());
        SystemInfo systemInfo = pos.getSystemInfo();
        if (!pos.getScanSalemanNumber()) {
            trans.setSalesman(POSTerminalApplication.getNewCashierID());
            if (systemInfo != null)
                systemInfo.setSalesManNumber(POSTerminalApplication.getNewCashierID());
        }
        if (systemInfo != null)
            systemInfo.setIsDaiFu(false);

        // Bruce/2003-12-17
        if (pos.getReturnItemState()) {
            trans.setDealType2("3");
            trans.setDealType3("4");
        } else {
            trans.setDealType2("0");
        }

        // setInvoiceNumber(GetProperty.getNextInvoiceNumber("").substring(2));
        trans.setSystemDateTime(new java.util.Date());

        // clear ItemList
        ItemList itemList = pos.getItemList();
        //itemList.setTransaction(trans);
        itemList.setItemIndex(0);
        itemList.selectFinished();
        //posTerminal.getItemList().repaint();

        pos.getItemList().setVisible(true);
        pos.getPayingPane().setVisible(false);
        trans.fireEvent(new TransactionEvent(trans, TransactionEvent.TRANS_RENEW));

        System.out.println("Transaction.generateABrandNewTransaction(): "
            + (System.currentTimeMillis() - time) + "ms");
        return trans;
    }

//    public static Transaction generateABrandNewTransaction0() {
//        long time = System.currentTimeMillis();
//        // Clear currentTransaction.
//        POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
//        if (!posTerminal.getMessageIndicator().getMessage().equals("")) {
//            posTerminal.getMessageIndicator().setMessage("");
//        }
//        Transaction transaction = posTerminal.getCurrentTransaction();
//        transaction.clear();
//
//        // Bruce/2003-12-17
//        if (posTerminal.getReturnItemState()) {
//            transaction.setDealType2("3");
//            transaction.setDealType3("4");
//        } else {
//            transaction.setDealType2("0");
//        }
//
////      setInvoiceNumber(GetProperty.getNextInvoiceNumber("").substring(2));
//        // trans.setTransactionNumber(Integer.parseInt(GetProperty.getProperty("NextTransactionSequenceNumber")));
//        transaction.setSystemDateTime(new java.util.Date());
//
//        // clear ItemList
//        posTerminal.getItemList().selectFinished();
//        posTerminal.getItemList().setItemIndex(0);
//        posTerminal.getItemList().repaint();
//
//        posTerminal.getItemList().setVisible(true);
//        posTerminal.getPayingPane().setVisible(false);
//
//        // Bruce/20021030/
//        // //posTerminal.setTransactionEnd(false);
//        System.out.println("Transaction.generateABrandNewTransaction(): " 
//            + (System.currentTimeMillis() - time) + "ms");
//        return transaction;
//    }

    private HYIDouble nHYIDouble0() {
        return new HYIDouble(0);
    }

    public Object deepClone() {
        return CreamToolkit.deepClone(this);
    }

//    /**
//     * 由于DacBase覆盖clone的方法，无法把tran的lineitem clone 而且也要clone [transient private
//     * HashSet dirtyFields]
//     * 此 deepClone 不充分, 会有疏漏!
//     * @deprecated
//     * @return
//     */
//    private Object deepClone_have_error() {
//        int len = this.getLineItems().length;
//        Object[] obj = new Object[len];
//        for (int i = 0; i < len; i++)
//            obj[i] = ((LineItem) (this.getLineItems()[i])).clone();
//        Transaction tran = (Transaction) this.clone();
//        tran.clearLineItem();
//        for (int i = 0; i < len; i++) {
//            try {
//                tran.addLineItem((LineItem) obj[i], false);
//            } catch (Exception e) {
//            }
//        }
//        return tran;
//    }

    /**
     * 深度克隆
     * 
     * @author ZhaoHong
     * @since 2003-07-21
     * @return Object
     */
    /*
     * public Object deepClone() { // throws
     * IOException,OptionalDataException,ClassNotFoundException{ try {
     * ByteArrayOutputStream bo = new ByteArrayOutputStream();
     * ObjectOutputStream oo = new ObjectOutputStream(bo); //如果currentLineItem
     * 不为空 就把它加进去 this.addLineItem(null); oo.writeObject(this); //read from
     * stream ByteArrayInputStream bi = new
     * ByteArrayInputStream(bo.toByteArray()); ObjectInputStream oi = new
     * ObjectInputStream(bi); return (oi.readObject()); } catch (Exception e) {
     * e.printStackTrace(); return null; } }
     */

    public Collection getExistedFieldList() {
        return existedFieldList;
    }

    public java.util.List getPrimaryKeyList() {
        return primaryKeys;
    }

    public String toString() {
        return "Transaction (pos=" + getTerminalNumber() + ", no="
            + getTransactionNumber()
            + ", z=" + getZSequenceNumber() 
            + ", shift=" + this.getSignOnNumber() 
            + ", netsalamt=" + getNetSalesAmount()
            + ", state=" + getState() + ")";
    }

    // 为全家打印作废收银条增加的查询方法 2004-11-22
    public static Iterator queryRecentVoidReports() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            java.util.Date now = new java.util.Date();
            // String today = CreamCache.getInstance().getDateTimeFormate().format(now);
            long l = now.getTime() - 1000 * 3600 * 24L;
            String yesterdayTime = CreamCache.getInstance().getDateTimeFormate().format(
                new java.sql.Date(l));

            return getMultipleObjects(connection, Transaction.class,
                "SELECT SYSDATE, TMTRANSEQ, DEALTYPE1 FROM "
                    + getInsertUpdateTableNameStaticVersion()
                    + " WHERE DEALTYPE1 = '*' AND SYSDATE > '" + yesterdayTime + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static Transaction queryBySequenceNumber(DbConnection connection, Integer number) {
        try {
            return getSingleObject(connection, Transaction.class,
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                            + " WHERE TMTRANSEQ = " + number);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e);
            return null;
        }
    }

    public Integer getSequenceNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("transactionNumber");
        else
            return (Integer) getFieldValue("TMTRANSEQ");
    }

    public static boolean existsTransaction(DbConnection connection, int posNumber, int transNumber) {
        try {
            getSingleObject(connection, Transaction.class, Server.isAtServerSide() ?
                "SELECT transactionNumber FROM posul_tranhead WHERE posNumber=" + posNumber
                    + " AND transactionNumber=" + transNumber :
                "SELECT tmtranseq FROM tranhead WHERE tmtranseq=" + transNumber
            );
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static Transaction queryByTransactionNumber(DbConnection connection, int posNumber, int transNumber) {
        return queryByTransactionNumber(connection, posNumber, transNumber, false);
    }

    public static Transaction queryByTransactionNumber(DbConnection connection, int posNumber, int transNumber,
        boolean displayInItemList) {
        return queryByTransactionNumber(connection, posNumber, transNumber, displayInItemList, true);
    }

    private CardDetail cardDetail;

    public static Transaction queryByTransactionNumber(DbConnection connection, int posNumber, int transNumber,
        boolean displayInItemList, boolean isLogForEntityNotFoundException) {
        try {
            Transaction trans = getSingleObject(connection, Transaction.class, Server.isAtServerSide() ?
                "SELECT * FROM posul_tranhead WHERE posNumber=" + posNumber
                    + " AND transactionNumber=" + transNumber :
                "SELECT * FROM tranhead WHERE tmtranseq=" + transNumber
            );
            if (displayInItemList)
                POSTerminalApplication.getInstance().getItemList().setTransaction(trans);

            // Query associated LineItems
            Iterator itr = LineItem.queryByTransactionNumber(connection, posNumber, transNumber);
            if (itr != null) {
                while (itr.hasNext()) {
                    //t.addLineItem((LineItem)itr.next());
                    trans.addLineItemSimpleVersion((LineItem)itr.next());
                }
            }

            //query alipay_detail
            Iterator it = Alipay_detail.queryByTranNo(connection, transNumber);
            if (it != null) {
                while (it.hasNext())
                    trans.addAlipayList((Alipay_detail)it.next());
            }
            it = WeiXin_detail.queryByTranNo(connection, transNumber);
            if (it != null) {
                while (it.hasNext())
                    trans.addWeiXinList((WeiXin_detail)it.next());
            }
            it = UnionPay_detail.queryByTranNo(connection, transNumber);
            if (it != null) {
                while (it.hasNext())
                    trans.addUnionPayList((UnionPay_detail)it.next());
            }
            it = Selfbuy_head.queryByTranNo(connection, transNumber);
            if (it != null) {
                while (it.hasNext())
                    trans.addSelfbuyHeadList((Selfbuy_head)it.next());
            }
            it = Selfbuy_detail.queryByTranNo(connection, transNumber);
            if (it != null) {
                while (it.hasNext())
                    trans.addSelfbuyDetailList((Selfbuy_detail)it.next());
            }
            it = EcDeliveryHead.queryByTranNo(connection, transNumber);
            if (it != null) {
                while (it.hasNext())
                    trans.addEcDeliveryHeadList((EcDeliveryHead)it.next());
            }
            it = Cmpay_detail.queryByTranNo(connection, transNumber);
            if (it != null) {
                while(it.hasNext())
                    trans.addCmPayList((Cmpay_detail)it.next());
            }
            // Query associated CardDetail
            trans.setCardDetail(CardDetail.queryByTransactionNumber(connection, posNumber, transNumber));

            return trans;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            if (isLogForEntityNotFoundException)
                CreamToolkit.logMessage(e);
            return null;
        }
    }

    public CardDetail getCardDetail() {
        return cardDetail;
    }

    public void setCardDetail(CardDetail cardDetail) {
        this.cardDetail = cardDetail;
    }

    //    public static Transaction queryByTransactionNumber(DbConnection connection, int posNumber, String number) {
//        return queryByTransactionNumber(true, connection, posNumber, number);
//    }
//
//    public static Transaction queryByTransactionNumber(boolean isLogForEntityNotFoundException, DbConnection connection, int posNumber, int number) {
//        try {
//            Transaction t;
//            if (hyi.cream.inline.Server.serverExist()) {
//                t = getSingleObject(connection, Transaction.class, "SELECT * FROM "
//                    + getInsertUpdateTableNameStaticVersion() + " WHERE PosNumber=" + posNumber
//                    + " AND TransactionNumber=" + number);
//            } else {
//                t = getSingleObject(connection, Transaction.class, "SELECT * FROM "
//                    + getInsertUpdateTableNameStaticVersion() + " WHERE TMTRANSEQ='" + number + "'");
//                // +"' AND POSNO='" +
//                // GetProperty.getProperty("TerminalNumber") + "'"
//            }
//            Iterator itr = LineItem.queryByTransactionNumber(connection, posNumber, number);
//            if (itr != null) {
//                // try {
//                while (itr.hasNext()) {
//                    t.setLockEnable(true);
//                    // t.addLineItem((LineItem)itr.next());
//                    t.addLineItemSimpleVersion((LineItem)itr.next());
//                }
//                // } catch (TooManyLineItemsException te)
//                // {te.printStackTrace(CreamToolkit.getLogger()); }
//            }
//            t.setLockEnable(true);
//            return t;
//        } catch (SQLException e) {
//            CreamToolkit.logMessage(e);
//            return null;
//        } catch (EntityNotFoundException e) {
//            if (isLogForEntityNotFoundException){
//                CreamToolkit.logMessage(e);
//            }
//            return null;
//        }
//    }

    public static List getTransactionsByTime(String startTime, String endTime) {
        String sql = "SELECT tmtranseq FROM tranhead WHERE sysdate BETWEEN '" 
                + startTime + "' AND '" + endTime + "' ORDER BY tmtranseq";
        DbConnection connection = null;
        Statement statement;
        ResultSet resultSet;
        List ret = new ArrayList();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                ret.add(resultSet.getObject(1));
            }
        } catch (Exception e) {
            CreamToolkit.logMessage("SQLException: " + sql);
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return ret;
    }

    /**
     * 取得下个交易序号。规则：比较(tranhead中的最近交易序号加1)和(property表中的NextTransactionNumber)，
     * 取较大者为下一个交易序号，若该序号大于最大交易序号限制，返回1。
     * 
     * @return Next transaction number.
     */
    public static int getNextTransactionNumber() {
        int nextTransactionNumber;
        int maxSeqInTranhead = 0;

        DbConnection connection = null;
        Statement statement = null;
        ResultSet rst = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            if (Param.getInstance().maxTransactionNumberIsNoLimit()) {
                //Bruce/2009-05-12/ 如果最大交易序號置頂，則直接取最大號，這樣可以解決如果POS的系統
                // 時間亂跳導致取出的交易號不合理.
                int max = Transaction.getMaxTransactionNumber(connection);
                maxSeqInTranhead = (max > 0) ?  max : 0;
            } else {
                statement = connection.createStatement();
                rst = statement.executeQuery(
                    "SELECT tmtranseq FROM tranhead ORDER BY sysdate DESC,tmtranseq DESC LIMIT 1");
                if (rst.next())
                    maxSeqInTranhead = rst.getInt(1);
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (rst != null)
                    rst.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }
            CreamToolkit.releaseConnection(connection);
        }

        nextTransactionNumber = maxSeqInTranhead + 1;

        String nextTransactionNumberInPropertyStr = PARAM.getNextTransactionNumber() + "";
        int nextTransactionNumberInProperty;
        try {
            if (nextTransactionNumberInPropertyStr.equals(""))
                nextTransactionNumberInProperty = 1;
            else
                nextTransactionNumberInProperty = Integer
                    .parseInt(nextTransactionNumberInPropertyStr);
        } catch (NumberFormatException e) {
            nextTransactionNumberInProperty = 1;
        }

        // 取property表中NexTransactionNumber 与Tranhead表中最近交易序号＋１中的较大者
        nextTransactionNumber = Math.max(nextTransactionNumber, nextTransactionNumberInProperty);

        // 如果交易需要超过最大交易序号，交易序号置 1
        if (nextTransactionNumber > getMaxTranNumberInProperty())
            nextTransactionNumber = 1;

        return nextTransactionNumber;
    }

//    /**
//     * Get max transaction number of POS. This method is also used by inline
//     * ServerThread.
//     */
//    public static int getMaxTransactionNumber(int posNumber) {
//        Object maxSeqNumber;
//        if (hyi.cream.inline.Server.serverExist()) {
//            maxSeqNumber = getValueOfStatement("SELECT MAX(transactionNumber) FROM "
//                    + getInsertUpdateTableNameStaticVersion()
//                    + " WHERE PosNumber='" + posNumber + "'");
//        } else {
//            maxSeqNumber = getValueOfStatement("SELECT MAX(tmtranseq) FROM "
//                    + getInsertUpdateTableNameStaticVersion());
//        }
//
//        if (maxSeqNumber == null)
//            return -1;
//        return CreamToolkit.retrieveIntValue(maxSeqNumber);
//    }

    public static int getMaxTransactionNumber(DbConnection connection) {
        Object maxSeqNumber = getValueOfStatement(connection, "SELECT MAX(tmtranseq) FROM "
            + getInsertUpdateTableNameStaticVersion() + " WHERE posno="
            + PARAM.getTerminalNumber());

        if (maxSeqNumber == null)
            return -1;
        return CreamToolkit.retrieveIntValue(maxSeqNumber);
    }

    public static int getOrderIdAndOrderNo(DbConnection connection,int orderid,String orderno) {
        Object maxSeqNumber = getValueOfStatement(connection, "SELECT count(*) FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE selfbuyOrderid="
                + orderid + " and selfbuyOrderno='"+orderno+"'");

        if (maxSeqNumber == null)
            return -1;
        return CreamToolkit.retrieveIntValue(maxSeqNumber);
    }

    /**
     * 取得property表中设置的最大交易序号 "MaxTransactionNumber"
     * 
     * @return int
     */
    public static int getMaxTranNumberInProperty() {
        int d = 99999999;
        String maxTranNumber = PARAM.getMaxTransactionNumber();
        if (!maxTranNumber.equals("")) {
            try {
                d = Integer.parseInt(maxTranNumber);
            } catch (NumberFormatException e) {
                d = 99999999;
            }
        }
        return d;
    }

    /**
     * 返回一段时间内起始和结束交易序号 a[0] begin transaction number a[1] end transaction
     * number
     * 
     * @return int[] a
     */
    public static int[] getBeginAndEndTranNumber() {
        int[] seqNos = new int[2];

        /* 需要保留的天数
        int tranReserved = Transaction.getTransactionReserved();
        Date ldate = CreamToolkit.shiftDay(new Date(), -tranReserved);
        String baseTime = CreamCache.getInstance().getDateTimeFormate().format(ldate);
        String sql = "SELECT tmtranseq FROM tranhead WHERE "
            + " posno=" + GetProperty.getTerminalNumber("") + " AND "
            + " sysdate > '" + baseTime
            + "' ORDER BY sysdate DESC LIMIT 2000";
        */

        // 只取{SyncTransactionRange}天的数据 (default=7)
        int dayRange = PARAM.getSyncTransactionRange();
        Date now = new Date();
        Date baseDate = CreamToolkit.shiftDay(now, -dayRange);
        SimpleDateFormat df = CreamCache.getInstance().getDateTimeFormate();
        String baseTime = df.format(baseDate);

        //Bruce/20091207/ 也要排除最近一分鐘的交易，因為有可能此交易尚未完全結束，可能還會更新，所以此時需避免上傳，
        // 以免上傳partial result.
        Date upperDate = CreamToolkit.shiftMinutes(now, -1);
        String upperTime = df.format(upperDate);

        String sql = "SELECT tmtranseq FROM tranhead WHERE"
            + " posno=" + PARAM.getTerminalNumber() + " AND"
            + " sysdate BETWEEN '" + baseTime + "' AND '" + upperTime
            + "' ORDER BY sysdate DESC";
        //System.out.println("sql=" + sql);

        //Bruce/20080606/
        // It'd be better to have a index like this:
        // CREATE INDEX tranhead_idx_posno_sysdate ON tranhead (posno, sysdate);

        DbConnection connection = null;
        Statement statement = null;
        ResultSet rst = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            rst = statement.executeQuery(sql);
            int i = 0;
            while (rst.next()) {
                if (i == 0)
                    seqNos[1] = rst.getInt("tmtranseq");
                seqNos[0] = rst.getInt("tmtranseq");
                i++;
            }

            //Bruce/2009-05-12/ 如果最大交易序號置頂，則end transaction number直接取最大號，
            // 這樣可以解決如果POS的系統時間亂跳導致取出的交易號範圍不合理，使得有可能漏傳
            // 交易永遠上傳不了的問題。
            if (Param.getInstance().maxTransactionNumberIsNoLimit()) {
                int max = Transaction.getMaxTransactionNumber(connection);
                if (max != -1)
                    seqNos[1] = max;
            }

        } catch (SQLException e) {
            CreamToolkit.logMessage("Error SQL : " + sql);
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (rst != null)
                    rst.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }




        return seqNos;
    }

    public static int[] getTransactionLost(int posNumber, int maxTranNumber,
            int beginTranNumber, int endTranNumber) {
        String selectStatement;
        // Bruce/20030425/
        // 最小request序号改成1:
        // 1. 对于喜士多旧门市可能会造成序号0号的交易会永远上不去，但是机会非常非常小
        // 2. 但是只要是新装机的POS，序号都会从1号开始，所以不会有问题

       // int startNumber = Math.max(0, maxNumberShouldHave - 2000);

        String tranNumberCondition;
        boolean b = beginTranNumber <= endTranNumber;

        if (hyi.cream.inline.Server.serverExist()) {
            if (b) {
                tranNumberCondition = " AND transactionNumber BETWEEN "
                        + beginTranNumber + " AND " + endTranNumber;
            } else {
                tranNumberCondition = " AND ((transactionNumber BETWEEN "
                        + beginTranNumber + " AND " + maxTranNumber + ") "
                        + "OR (transactionNumber BETWEEN 1 " + "AND "
                        + endTranNumber + ")) ";
            }

            selectStatement = "SELECT transactionNumber FROM "
                    + getInsertUpdateTableNameStaticVersion()
                    + " WHERE PosNumber=" + posNumber + tranNumberCondition
                    + " ORDER BY systemDate";
        } else {
            if (b) {
                tranNumberCondition = " AND tmtranseq BETWEEN "
                        + beginTranNumber + " AND " + endTranNumber;
            } else {
                tranNumberCondition = " AND ((tmtranseq BETWEEN "
                        + beginTranNumber + " AND " + maxTranNumber + ") "
                        + "OR (tmtranseq BETWEEN 1 " + "AND " + endTranNumber
                        + ")) ";
            }

            selectStatement = "SELECT tmtranseq FROM "
                    + getInsertUpdateTableNameStaticVersion()
                    + " WHERE POSNO = " + posNumber + tranNumberCondition
                    + " ORDER BY SYSDATE";
        }

        DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<Integer> collection = new ArrayList<Integer>();

        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            CreamToolkit.logMessage("syns transaction sql : " + selectStatement);
            // int preSCTranNo, curSCTranNo;
            // boolean isMaxTranNumberReached = false;

            List<Integer> tranNoInSC = new ArrayList<Integer>();

            while (resultSet.next()) {
                tranNoInSC.add(resultSet.getInt(1));
            }

            Object[] tmpArrays = tranNoInSC.toArray();
            Arrays.sort(tmpArrays);

            // SC 中没有的交易号为丢失的交易号
            for (int i = beginTranNumber; i <= (b ? endTranNumber
                    : maxTranNumber + endTranNumber); i++) {
                int k = (i <= maxTranNumber) ? i : i - maxTranNumber;
                if (tmpArrays.length == 0
                        || Arrays.binarySearch(tmpArrays, k) < 0) {
                    collection.add(k);
                }
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage("Error Sql : " + selectStatement);
            CreamToolkit.logMessage(e);
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }
            CreamToolkit.releaseConnection(connection);
        }
        if (collection != null) {
            int[] ret = new int[collection.size()];
            for (i = 0; i < ret.length; i++) {
                ret[i] = collection.get(i);
            }
            return ret;
        } else
            return null;
    }

    public static Iterator getUploadFailedShift(DbConnection connection) {
        try {
            // String successFlag = "1";
            String failFlag = "2";
            String notUploadFlag = "0";
            return getMultipleObjects(connection, ShiftReport.class, "SELECT * FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE TCPFLG='"
                    + failFlag + "' OR TCPFLG='" + notUploadFlag + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static Transaction createCurrentTransaction() {
        Transaction transaction = new Transaction();
        transaction.init();
        return transaction;
    }

    synchronized public void init() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            init(connection);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * @param connection: is for getCurrentZNumber(connection) in this method.
     */
    synchronized public void init(DbConnection connection) {
        setStoreNumber(Store.getStoreID());
        setTerminalNumber(PARAM.getTerminalNumber());
        // setTransactionNumber(Integer.parseInt(GetProperty.getProperty("NextTransactionSequenceNumber")));
        // CreamToolkit.logMessage("" + Transaction.getNextTransactionNumber());
        setTransactionNumber(Transaction.getNextTransactionNumber());
        setDealType1("0");
        setDealType3("0");
        setDealType2("0");
        setDefaultState();
        setDayDateForAtt(null);
        setBeginTimeForAtt(null);
        setTerminalPhysicalNumber(PARAM.getTerminalPhysicalNumber());
        setInvoiceID(PARAM.getInvoiceID());
        setInvoiceNumber(PARAM.getInvoiceNumber());
        setInvoiceCount(0);
        setSignOnNumber(PARAM.getShiftNumber());
        setZSequenceNumber(ZReport.getCurrentZNumber(connection));
        setCashierNumber(PARAM.getCashierNumber());
        setBuyerNumber("");
        setTransactionType("00");
        setSpillAmount(nHYIDouble0());
        setDetailCount(0); // DETAILCNT SMALLINT UNSIGNED N 交易项数
        setSalesAmount(nHYIDouble0()); // SALEAMT DECIMAL(12,2) N 小计金额
        setInvoiceAmount(nHYIDouble0());

        setGrossSalesAmount(nHYIDouble0());
        setDaiFuAmount(nHYIDouble0());
        setDaiShouAmount(nHYIDouble0()); // 代售金额
        setDaiShouAmount2(nHYIDouble0()); // 代收金额
        setPreRcvAmount(nHYIDouble0()); // 期刊金额 or 会员赊账，预收
        setItemsRebateAmount(nHYIDouble0()); // 商品累计还圆金
        setSpecialRebateAmount(nHYIDouble0());// 额外还圆金
        setRebateAmount(nHYIDouble0()); // 本次新增还元金 = 商品累计还圆金 + 额外还圆金
        setRebateChangeAmount(nHYIDouble0()); // 本次结余还元金
        setTotalRebateAmount(nHYIDouble0()); // 还元金累计
        setUseRebateAmt(nHYIDouble0());
        setBeforeTotalRebateAmt(nHYIDouble0());
   
        setCustomerCount(1);
        setCustomerAgeLevel(1);
        roundDownItem = null;
        if (!POSTerminalApplication.getInstance().getScanSalemanNumber()) {
            setSalesman(POSTerminalApplication.getNewCashierID());
            SystemInfo systemInfo = POSTerminalApplication.getInstance().getSystemInfo();
            if (systemInfo != null)
                systemInfo.setSalesManNumber(POSTerminalApplication.getNewCashierID());
        }

        setGrossSalesTax0Amount(nHYIDouble0());
        setGrossSalesTax1Amount(nHYIDouble0());
        setGrossSalesTax2Amount(nHYIDouble0());
        setGrossSalesTax3Amount(nHYIDouble0());
        setGrossSalesTax4Amount(nHYIDouble0());
        setGrossSalesTax5Amount(nHYIDouble0());
        setGrossSalesTax6Amount(nHYIDouble0());
        setGrossSalesTax7Amount(nHYIDouble0());
        setGrossSalesTax8Amount(nHYIDouble0());
        setGrossSalesTax9Amount(nHYIDouble0());
        setGrossSalesTax10Amount(nHYIDouble0());
        // CreamToolkit.logMessage("" + getGrossSalesTax3Amount());
   
        initSIInfo();

        setNotIncludedSales0(nHYIDouble0());
        setNotIncludedSales1(nHYIDouble0());
        setNotIncludedSales2(nHYIDouble0());
        setNotIncludedSales3(nHYIDouble0());
        setNotIncludedSales4(nHYIDouble0());
        setNotIncludedSales5(nHYIDouble0());
        setNotIncludedSales6(nHYIDouble0());
        setNotIncludedSales7(nHYIDouble0());
        setNotIncludedSales8(nHYIDouble0());
        setNotIncludedSales9(nHYIDouble0());
        setNotIncludedSales10(nHYIDouble0());

        setNetSalesAmount(nHYIDouble0());
        setNetSalesAmount0(nHYIDouble0());
        setNetSalesAmount1(nHYIDouble0());
        setNetSalesAmount2(nHYIDouble0());
        setNetSalesAmount3(nHYIDouble0());
        setNetSalesAmount4(nHYIDouble0());
        setNetSalesAmount5(nHYIDouble0());
        setNetSalesAmount6(nHYIDouble0());
        setNetSalesAmount7(nHYIDouble0());
        setNetSalesAmount8(nHYIDouble0());
        setNetSalesAmount9(nHYIDouble0());
        setNetSalesAmount10(nHYIDouble0());
        //
        setMixAndMatchAmount0(nHYIDouble0().setScale(2, 4));
        setMixAndMatchAmount1(nHYIDouble0().setScale(2, 4));
        setMixAndMatchAmount2(nHYIDouble0().setScale(2, 4));
        setMixAndMatchAmount3(nHYIDouble0().setScale(2, 4));
        setMixAndMatchAmount4(nHYIDouble0().setScale(2, 4));
        setMixAndMatchAmount5(nHYIDouble0().setScale(2, 4));
        setMixAndMatchAmount6(nHYIDouble0().setScale(2, 4));
        setMixAndMatchAmount7(nHYIDouble0().setScale(2, 4));
        setMixAndMatchAmount8(nHYIDouble0().setScale(2, 4));
        setMixAndMatchAmount9(nHYIDouble0().setScale(2, 4));
        setMixAndMatchAmount10(nHYIDouble0().setScale(2, 4));
        setMixAndMatchCount0(0);
        setMixAndMatchCount1(0);
        setMixAndMatchCount2(0);
        setMixAndMatchCount3(0);
        setMixAndMatchCount4(0);
        setMixAndMatchCount5(0);
        setMixAndMatchCount6(0);
        setMixAndMatchCount7(0);
        setMixAndMatchCount8(0);
        setMixAndMatchCount9(0);
        setMixAndMatchCount10(0);
        setCreditCardExpireDate(CreamToolkit.getInitialDate());
        setPrintType("");
        currentLineItem = null;
        lineItems = new ArrayList<LineItem>();
        //lineItemArrayLast = new ArrayList();
        this.clearAppliedSIs();
        paymentArray.clear();
        itemDiscount = nHYIDouble0();
   
        mmAmount = nHYIDouble0();
        mmCount = 0;
        clearMMDetail();
        totalMMAmount = null;
        diyOverrideAmountMap.clear();
        if (POSTerminalApplication.getInstance().getSystemInfo() != null) {
            POSTerminalApplication.getInstance().getSystemInfo().setIsDaiFu(false);
        }
    }

    // clear current transaction
    // initialize it for next transaction
    public void clear(boolean fireEvent) {
        System.out.println("Transaction.clear() invoked.");
        super.clear();
        init();
        if (fireEvent)
            fireEvent(new TransactionEvent(this, TransactionEvent.TRANS_RENEW));
    }

    public void clear() {
        clear(true);
    }

    public void makeNegativeValue() {
        Iterator itr = this.getValues().keySet().iterator();
        Map values = getValues();
        while (itr.hasNext()) {
            Object key = itr.next();
            if (values.get(key) instanceof HYIDouble) {
                HYIDouble big = (HYIDouble) values.get(key);
                big = big.negate();
                setFieldValue((String) key, big);
            } else if (values.get(key) instanceof Integer) {
                String k = (String) key;
                Integer in = (Integer) values.get(k);
                if (k.toUpperCase().startsWith("MNMCNT")
                        || k.toUpperCase().startsWith("SIPCNT")
                        || k.toUpperCase().startsWith("SIPLUSCNT")
                        || k.toUpperCase().startsWith("SIMCNT")) {
                    setFieldValue((String) key, 0 - in);
                }
            }
        }

        if (getLineItems() != null) {
            Iterator iter = this.getLineItemsIterator();
            while (iter.hasNext()) {
                LineItem li = (LineItem) iter.next();
                li.makeNegativeValue();
            }
        }

        // (for Ver1.0) MixAndMatch 中折扣金额取相反数 ZhaoHong 2003-07-30
        setTotalMMAmount(getTotalMMAmount().negate());

        // SIAmtList 中折扣金额取相反数
        if (getAppliedSIs().size() > 0) {
            for (HYIDouble siAmount : getAppliedSIs().values()) {
                siAmount.negateMe();
            }
        }
    }

    public void makeCopy(Transaction t) {
        fieldMap = t.getValues();
    }

    public Integer getZSequenceNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("zSequenceNumber");
        else
            return (Integer) getFieldValue("EODCNT");
    }

    public void setZSequenceNumber(Integer number) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("zSequenceNumber", number);
        else
            setFieldValue("EODCNT", number);
    }

    public String getCorrespondingID(String payID) {
        if (getPayNumber1() != null)
            if (getPayNumber1().equals(payID))
                return "1";
        if (getPayNumber2() != null)
            if (getPayNumber2().equals(payID))
                return "2";
        if (getPayNumber3() != null)
            if (getPayNumber3().equals(payID))
                return "3";
        if (getPayNumber4() != null)
            if (getPayNumber4().equals(payID))
                return "4";
        return "";
    }

    public void calcHardTotals(DbConnection connection) throws SQLException {
        try {
            calcHardTotals(connection, TRANSACTION_SAVE_SHIFT);
        } catch (EntityNotFoundException e) {
            throw new SQLException(e.toString());
        }
    }

    /**
     * Deal with shift z shiftex zex depsales daishousales for this transaction
     * @param connection
     * @param step2
     * @throws SQLException
     * @throws EntityNotFoundException
     */
    public void calcHardTotals(DbConnection connection, int step2) throws SQLException, EntityNotFoundException {
        ShiftReport shift;
        ZReport z;
        boolean check = false;

        if (POSTerminalApplication.isRecalcMode) { // for recalculating
            shift = POSTerminalApplication.getInstance().getNewShift();
            z = POSTerminalApplication.getInstance().getNewZ();
            check = true;
        } else {
            shift = ShiftReport.getCurrentShift(connection);
            z = ZReport.getOrCreateCurrentZReport(connection);
        }

        // 交班交易--有开抽屉
        CreamToolkit.logMessage("calcHardTotals | dealType2 " + getDealType2()
                + "| dealType3 : " + getDealType3() + " | tranno : "
                + this.getTransactionNumber());
        if (getDealType2().equals("9")) {
            shift.setSignOffSystemDateTime(new java.util.Date());
            shift.setEndTransactionNumber(connection, getTransactionNumber());
            shift.setTransactionCount(shift.getTransactionCount() + 1);
            if (z != null) {
                z.setCashierNumber(shift.getCashierNumber());
            }
            if (!check) {
                // shift.setAccountingDate(DacTransfer.getInstance().getAccountingDate());
                // shift.setAccountingDate(new java.util.Date());
                shift.setAccountingDate(DEFAULT_END_ACCDATE);
                if (step2 <= TRANSACTION_SAVE_SHIFT) {
                    shift.update(connection);
                    this.step = TRANSACTION_SAVE_Z;
                    saveTo(TRANSACTION_SAVE_Z);
                }
            } else {
                // shift.setAccountingDate(POSTerminalApplication.getInstance().getNewDate());
                shift.setAccountingDate(DEFAULT_END_ACCDATE);
            }
            CreamToolkit.logMessage("Shift report closed: z="
                    + shift.getZSequenceNumber() + ",shift="
                    + shift.getSequenceNumber() + ",time="
                    + dateFormatter.format(shift.getSignOffSystemDateTime())
                    + ",amt=" + shift.getTotalAmount());
            //POSTerminalApplication.getInstance().getSystemInfo().setShiftTotalCashAmount(shift.getTotalAmount());
            CreamToolkit.logMessage("calcHardTotals | return");
            return;

        } else if (getDealType2().equals("D")) { // 日结交易
            // 梅林日结死机,出现在这一段
            z.setEndSystemDateTime(new java.util.Date());
            z.setEndTransactionNumber(connection, getTransactionNumber());
            //in = z.getTransactionCount() + 1;
            //z.setTransactionCount(in);

            z.setTransactionCount(z.getEndTransactionNumber() < z.getBeginTransactionNumber() ?
                getMaxTranNumberInProperty() - z.getBeginTransactionNumber() + z.getEndTransactionNumber() + 1 :
                z.getEndTransactionNumber() - z.getBeginTransactionNumber() + 1
            );

            CreamToolkit.logMessage("calcHardTotals | date shift | z.transactioncount : "
                + z.getTransactionCount());
            // z.setCashierNumber(shift.getCashierNumber());
            if (!check) {
                // z.setAccountingDate(DacTransfer.getInstance().getAccountingDate());
                // z.setAccountingDate(new java.util.Date());
                z.setAccountingDate(DEFAULT_END_ACCDATE);
                System.out.println("z.setAccountingDate(DEFAULT_END_ACCDATE) = "
                    + z.getAccountingDate());

                if (step2 <= TRANSACTION_SAVE_Z) {
                    z.update(connection);
                    this.step = TRANSACTION_SAVE_DEPSALES;
                    saveTo(TRANSACTION_SAVE_DEPSALES);
                }
                // Bruce/2002-01-29
                if (step2 <= TRANSACTION_SAVE_DEPSALES) {
                    Object[] d = DepSales.getCurrentDepSales(connection);
                    for (Object o : d) {
                        DepSales dep = (DepSales) o;
                        // dep.setAccountDate(DacTransfer.getInstance().getAccountingDate());
                        dep.setAccountDate(new Date());
                        dep.update(connection);
                    }
                    this.step = TRANSACTION_SAVE_DAISHOUSALES;
                    saveTo(TRANSACTION_SAVE_DAISHOUSALES);
                }

                // set daishousales 代售项目
                if (step2 <= TRANSACTION_SAVE_DAISHOUSALES) {
                    Iterator ite = DaishouSales.getCurrentDaishouSales(connection, 
                         getZSequenceNumber());
                    if (ite != null) {
                        while (ite.hasNext()) {
                            DaishouSales daishouSales = (DaishouSales) ite
                                    .next();
                            daishouSales.setAccountDate(new java.util.Date());
                            daishouSales.update(connection);
                        }
                    }
                    this.step = TRANSACTION_SAVE_DAISHOUSALES2;
                    saveTo(TRANSACTION_SAVE_DAISHOUSALES2);
                }

            } else {
                z.setAccountingDate(DEFAULT_END_ACCDATE);
                System.out
                        .println("z.setAccountingDate(DEFAULT_END_ACCDATE) = "
                                + z.getAccountingDate());
            }
            CreamToolkit.logMessage("Z report closed: z="
                    + z.getSequenceNumber() + ",time="
                    + dateFormatter.format(z.getEndSystemDateTime()) + ",amt="
                    + z.getTotalAmount());
            // System.out.println(z.getBeginSystemDateTime());
            // System.out.println(z.getCashierNumber());
            CreamToolkit.logMessage("calcHardTotals | return");
            return;
        } else if (getDealType2().equals("P")
                || getDealType2().equals("D")|| getDealType2().equals("B") || getDealType2().equals("C")) {
            return;// 仅打印的交易
        } else if (getDealType2().equals("Q")) { // 团购结算
            //shift.setPreRcvDetailAmount1(shift.getPreRcvDetailAmount1().add(this.getPreRcvAmount()));
            shift.setPreRcvDetailAmount1(this.getPreRcvAmount());
            shift.setPreRcvDetailCount1(shift.getPreRcvDetailCount1() + 1);
            //z.setPreRcvDetailAmount1(z.getPreRcvDetailAmount1().add(this.getPreRcvAmount()));
            z.setPreRcvDetailAmount1(this.getPreRcvAmount());
            z.setPreRcvDetailCount1(z.getPreRcvDetailCount1() + 1);
//            return;
        }

        // 前笔误打
        if (getDealType3().compareTo("1") == 0) {
            shift.setTransactionVoidAmount(shift.getTransactionVoidAmount()
                    .addMe(getSalesAmount().negate()));

            shift.setTransactionVoidCount(shift.getTransactionVoidCount() + 1);
            z.setTransactionVoidAmount(z.getTransactionVoidAmount().addMe(
                    getSalesAmount().negate()));
            z.setTransactionVoidCount(z.getTransactionVoidCount() + 1);
        }

        // ReprintAmount ④ REPRTAMT DECIMAL(12,2) N 重印金额
        // ReprintCount REPRTCNT INT UNSIGNED N 重印次数
        if (getDealType3().compareTo("2") == 0) {
            shift.setReprintAmount(shift.getReprintAmount().addMe(
                    getSalesAmount().negate()));
            shift.setReprintCount(shift.getReprintCount() + 1);

            z.setReprintAmount(z.getReprintAmount().addMe(
                    getSalesAmount().negate()));
            z.setReprintCount(z.getReprintCount() + 1);

        } else if (getDealType3().compareTo("3") == 0) { // 交易取消
            shift.setTransactionCancelAmount(shift.getTransactionCancelAmount()
                    .subtract(getSalesAmount()).subtract(getPayAmount1())
                    .subtract(getPayAmount2()).subtract(getPayAmount3())
                    .subtract(getPayAmount4()));
            shift.setTransactionCancelCount(shift.getTransactionCancelCount() + 1);
            z.setTransactionCancelAmount(z.getTransactionCancelAmount()
                    .subtract(getSalesAmount()).subtract(getPayAmount1())
                    .subtract(getPayAmount2()).subtract(getPayAmount3())
                    .subtract(getPayAmount4()));
            z.setTransactionCancelCount(z.getTransactionCancelCount() + 1);

        } else if (getDealType3().compareTo("4") == 0) { // 退货--有开抽屉
            shift.setReturnAmount(shift.getReturnAmount().subtract(
                    getNetSalesAmount()));
            z
                    .setReturnAmount(z.getReturnAmount().subtract(
                            getNetSalesAmount()));
            shift.setReturnItemQuantity(shift.getReturnItemQuantity().addMe(
                    POSTerminalApplication.getInstance().getReturnQty()));
            z.setReturnItemQuantity(z.getReturnItemQuantity().addMe(
                    POSTerminalApplication.getInstance().getReturnQty()));
            POSTerminalApplication.getInstance().setReturnQty(nHYIDouble0());

            if (getDealType2().equals("3")) {
                shift.setReturnTransactionCount(shift.getReturnTransactionCount() + 1);
                z.setReturnTransactionCount(z.getReturnTransactionCount() + 1);
            }
        }

        CreamToolkit.logMessage("calcHardTotals | start shift/z");
        // GrandToatal
        z.setGrandTotal(new HYIDouble(0).addMe(z.getGrandTotal().addMe(getGrossSalesAmount())));

        z.setTransactionCountGrandTotal(z.getTransactionCountGrandTotal() + 1);

        // 结束交易序号
        shift.setEndTransactionNumber(connection, getTransactionNumber());
        z.setEndTransactionNumber(connection, getTransactionNumber());

        // 营业额
        shift.getTaxType0GrossSales().addMe(getGrossSalesTax0Amount());
        shift.getTaxType1GrossSales().addMe(getGrossSalesTax1Amount());
        shift.getTaxType2GrossSales().addMe(getGrossSalesTax2Amount());
        shift.getTaxType3GrossSales().addMe(getGrossSalesTax3Amount());
        shift.getTaxType4GrossSales().addMe(getGrossSalesTax4Amount());
        shift.getTaxType5GrossSales().addMe(getGrossSalesTax5Amount());
        shift.getTaxType6GrossSales().addMe(getGrossSalesTax6Amount());
        shift.getTaxType7GrossSales().addMe(getGrossSalesTax7Amount());
        shift.getTaxType8GrossSales().addMe(getGrossSalesTax8Amount());
        shift.getTaxType9GrossSales().addMe(getGrossSalesTax9Amount());
        shift.getTaxType10GrossSales().addMe(getGrossSalesTax10Amount());

        if (z.getTaxType0GrossSales() == null) {
            z.setTaxType0GrossSales(nHYIDouble0());
        }
        z.getTaxType0GrossSales().addMe(getGrossSalesTax0Amount());
        if (z.getTaxType1GrossSales() == null)
            z.setTaxType1GrossSales(nHYIDouble0());

        z.getTaxType1GrossSales().addMe(getGrossSalesTax1Amount());
        if (z.getTaxType2GrossSales() == null)
            z.setTaxType2GrossSales(nHYIDouble0());

        z.getTaxType2GrossSales().addMe(getGrossSalesTax2Amount());
        if (z.getTaxType3GrossSales() == null)
            z.setTaxType3GrossSales(nHYIDouble0());

        z.getTaxType3GrossSales().addMe(getGrossSalesTax3Amount());
        if (z.getTaxType4GrossSales() == null)
            z.setTaxType4GrossSales(nHYIDouble0());

        z.getTaxType4GrossSales().addMe(getGrossSalesTax4Amount());
        if (z.getTaxType5GrossSales() == null)
            z.setTaxType5GrossSales(nHYIDouble0());

        z.getTaxType5GrossSales().addMe(getGrossSalesTax5Amount());
        if (z.getTaxType6GrossSales() == null)
            z.setTaxType6GrossSales(nHYIDouble0());

        z.getTaxType6GrossSales().addMe(getGrossSalesTax6Amount());
        if (z.getTaxType7GrossSales() == null)
            z.setTaxType7GrossSales(nHYIDouble0());

        z.getTaxType7GrossSales().addMe(getGrossSalesTax7Amount());
        if (z.getTaxType8GrossSales() == null)
            z.setTaxType8GrossSales(nHYIDouble0());

        z.getTaxType8GrossSales().addMe(getGrossSalesTax8Amount());
        if (z.getTaxType9GrossSales() == null)
            z.setTaxType9GrossSales(nHYIDouble0());

        z.getTaxType9GrossSales().addMe(getGrossSalesTax9Amount());
        if (z.getTaxType10GrossSales() == null)
            z.setTaxType10GrossSales(nHYIDouble0());

        z.getTaxType10GrossSales().addMe(getGrossSalesTax10Amount());

        // 营业净额
        shift.getNetSalesAmount0().addMe(getNetSalesAmount0());
        shift.getNetSalesAmount1().addMe(getNetSalesAmount1());
        shift.getNetSalesAmount2().addMe(getNetSalesAmount2());
        shift.getNetSalesAmount3().addMe(getNetSalesAmount3());
        shift.getNetSalesAmount4().addMe(getNetSalesAmount4());
        shift.getNetSalesAmount5().addMe(getNetSalesAmount5());
        shift.getNetSalesAmount6().addMe(getNetSalesAmount6());
        shift.getNetSalesAmount7().addMe(getNetSalesAmount7());
        shift.getNetSalesAmount8().addMe(getNetSalesAmount8());
        shift.getNetSalesAmount9().addMe(getNetSalesAmount9());
        shift.getNetSalesAmount10().addMe(getNetSalesAmount10());
        if (z.getNetSalesAmount0() == null)
            z.setNetSalesAmount0(nHYIDouble0());

        z.getNetSalesAmount0().addMe(getNetSalesAmount0());
        if (z.getNetSalesAmount1() == null)
            z.setNetSalesAmount1(nHYIDouble0());

        z.getNetSalesAmount1().addMe(getNetSalesAmount1());
        if (z.getNetSalesAmount2() == null)
            z.setNetSalesAmount2(nHYIDouble0());

        z.getNetSalesAmount2().addMe(getNetSalesAmount2());
        if (z.getNetSalesAmount3() == null)
            z.setNetSalesAmount3(nHYIDouble0());

        z.getNetSalesAmount3().addMe(getNetSalesAmount3());
        if (z.getNetSalesAmount4() == null)
            z.setNetSalesAmount4(nHYIDouble0());
        
        z.getNetSalesAmount4().addMe(getNetSalesAmount4());
        if (z.getNetSalesAmount5() == null)
            z.setNetSalesAmount5(nHYIDouble0());
        
        z.getNetSalesAmount5().addMe(getNetSalesAmount5());
        if (z.getNetSalesAmount6() == null)
            z.setNetSalesAmount6(nHYIDouble0());
        
        z.getNetSalesAmount6().addMe(getNetSalesAmount6());
        if (z.getNetSalesAmount7() == null)
            z.setNetSalesAmount7(nHYIDouble0());

        z.getNetSalesAmount7().addMe(getNetSalesAmount7());
        if (z.getNetSalesAmount8() == null)
            z.setNetSalesAmount8(nHYIDouble0());

        z.getNetSalesAmount8().addMe(getNetSalesAmount8());
        if (z.getNetSalesAmount9() == null)
            z.setNetSalesAmount9(nHYIDouble0());

        z.getNetSalesAmount9().addMe(getNetSalesAmount9());
        if (z.getNetSalesAmount10() == null)
            z.setNetSalesAmount10(nHYIDouble0());

        z.getNetSalesAmount10().addMe(getNetSalesAmount10());
        // 税额
        // init transaction
        if (getTaxAmount() == null)
            setTaxAmount(nHYIDouble0());

        if (getTaxAmount0() == null)
            setTaxAmount0(nHYIDouble0());

        if (getTaxAmount1() == null)
            setTaxAmount1(nHYIDouble0());

        if (getTaxAmount2() == null)
            setTaxAmount2(nHYIDouble0());

        if (getTaxAmount3() == null)
            setTaxAmount3(nHYIDouble0());

        if (getTaxAmount4() == null)
            setTaxAmount4(nHYIDouble0());

        if (getTaxAmount5() == null)
            setTaxAmount5(nHYIDouble0());

        if (getTaxAmount6() == null)
            setTaxAmount6(nHYIDouble0());

        if (getTaxAmount7() == null)
            setTaxAmount7(nHYIDouble0());

        if (getTaxAmount8() == null)
            setTaxAmount8(nHYIDouble0());

        if (getTaxAmount9() == null)
            setTaxAmount9(nHYIDouble0());

        if (getTaxAmount10() == null)
            setTaxAmount10(nHYIDouble0());

        // init shift
        if (shift.getTaxAmount() == null)
            shift.setTaxAmount(nHYIDouble0());

        if (shift.getTaxAmount0() == null)
            shift.setTaxAmount0(nHYIDouble0());

        if (shift.getTaxAmount1() == null)
            shift.setTaxAmount1(nHYIDouble0());

        if (shift.getTaxAmount2() == null)
            shift.setTaxAmount2(nHYIDouble0());

        if (shift.getTaxAmount3() == null)
            shift.setTaxAmount3(nHYIDouble0());

        if (shift.getTaxAmount4() == null)
            shift.setTaxAmount4(nHYIDouble0());

        if (shift.getTaxAmount5() == null)
            shift.setTaxAmount5(nHYIDouble0());

        if (shift.getTaxAmount6() == null)
            shift.setTaxAmount6(nHYIDouble0());

        if (shift.getTaxAmount7() == null)
            shift.setTaxAmount7(nHYIDouble0());

        if (shift.getTaxAmount8() == null)
            shift.setTaxAmount8(nHYIDouble0());

        if (shift.getTaxAmount9() == null)
            shift.setTaxAmount9(nHYIDouble0());

        if (shift.getTaxAmount10() == null)
            shift.setTaxAmount10(nHYIDouble0());

        // init z
        if (z.getTaxAmount() == null)
            z.setTaxAmount(nHYIDouble0());

        if (z.getTaxAmount0() == null)
            z.setTaxAmount0(nHYIDouble0());

        if (z.getTaxAmount1() == null)
            z.setTaxAmount1(nHYIDouble0());

        if (z.getTaxAmount2() == null)
            z.setTaxAmount2(nHYIDouble0());

        if (z.getTaxAmount3() == null)
            z.setTaxAmount3(nHYIDouble0());

        if (z.getTaxAmount4() == null)
            z.setTaxAmount4(nHYIDouble0());

        if (z.getTaxAmount5() == null)
            z.setTaxAmount5(nHYIDouble0());

        if (z.getTaxAmount6() == null)
            z.setTaxAmount6(nHYIDouble0());

        if (z.getTaxAmount7() == null)
            z.setTaxAmount7(nHYIDouble0());

        if (z.getTaxAmount8() == null)
            z.setTaxAmount8(nHYIDouble0());

        if (z.getTaxAmount9() == null)
            z.setTaxAmount9(nHYIDouble0());

        if (z.getTaxAmount10() == null)
            z.setTaxAmount10(nHYIDouble0());

        // set tax amount into shift
        shift.getTaxAmount().addMe(getTaxAmount());
        shift.getTaxAmount0().addMe(getTaxAmount0());
        shift.getTaxAmount1().addMe(getTaxAmount1());
        shift.getTaxAmount2().addMe(getTaxAmount2());
        shift.getTaxAmount3().addMe(getTaxAmount3());
        shift.getTaxAmount4().addMe(getTaxAmount4());
        shift.getTaxAmount5().addMe(getTaxAmount5());
        shift.getTaxAmount6().addMe(getTaxAmount6());
        shift.getTaxAmount7().addMe(getTaxAmount7());
        shift.getTaxAmount8().addMe(getTaxAmount8());
        shift.getTaxAmount9().addMe(getTaxAmount9());
        shift.getTaxAmount10().addMe(getTaxAmount10());

        // set tax amount of z
        z.getTaxAmount().addMe(getTaxAmount());
        z.getTaxAmount0().addMe(getTaxAmount0());
        z.getTaxAmount1().addMe(getTaxAmount1());
        z.getTaxAmount2().addMe(getTaxAmount2());
        z.getTaxAmount3().addMe(getTaxAmount3());
        z.getTaxAmount4().addMe(getTaxAmount4());
        z.getTaxAmount5().addMe(getTaxAmount5());
        z.getTaxAmount6().addMe(getTaxAmount6());
        z.getTaxAmount7().addMe(getTaxAmount7());
        z.getTaxAmount8().addMe(getTaxAmount8());
        z.getTaxAmount9().addMe(getTaxAmount9());
        z.getTaxAmount10().addMe(getTaxAmount10());

        // 组合促销
        shift.getMixAndMatchAmount0().addMe(getMixAndMatchAmount0());
        shift.getMixAndMatchAmount1().addMe(getMixAndMatchAmount1());
        shift.getMixAndMatchAmount2().addMe(getMixAndMatchAmount2());
        shift.getMixAndMatchAmount3().addMe(getMixAndMatchAmount3());
        shift.getMixAndMatchAmount4().addMe(getMixAndMatchAmount4());
        shift.getMixAndMatchAmount5().addMe(getMixAndMatchAmount5());
        shift.getMixAndMatchAmount6().addMe(getMixAndMatchAmount6());
        shift.getMixAndMatchAmount7().addMe(getMixAndMatchAmount7());
        shift.getMixAndMatchAmount8().addMe(getMixAndMatchAmount8());
        shift.getMixAndMatchAmount9().addMe(getMixAndMatchAmount9());
        shift.getMixAndMatchAmount10().addMe(getMixAndMatchAmount10());
        z.getMixAndMatchAmount0().addMe(getMixAndMatchAmount0());
        z.getMixAndMatchAmount1().addMe(getMixAndMatchAmount1());
        z.getMixAndMatchAmount2().addMe(getMixAndMatchAmount2());
        z.getMixAndMatchAmount3().addMe(getMixAndMatchAmount3());
        z.getMixAndMatchAmount4().addMe(getMixAndMatchAmount4());
        z.getMixAndMatchAmount5().addMe(getMixAndMatchAmount5());
        z.getMixAndMatchAmount6().addMe(getMixAndMatchAmount6());
        z.getMixAndMatchAmount7().addMe(getMixAndMatchAmount7());
        z.getMixAndMatchAmount8().addMe(getMixAndMatchAmount8());
        z.getMixAndMatchAmount9().addMe(getMixAndMatchAmount9());
        z.getMixAndMatchAmount10().addMe(getMixAndMatchAmount10());
        shift.setMixAndMatchCount0(shift.getMixAndMatchCount0() + getMixAndMatchCount0());
        shift.setMixAndMatchCount1(shift.getMixAndMatchCount1() + getMixAndMatchCount1());
        shift.setMixAndMatchCount2(shift.getMixAndMatchCount2() + getMixAndMatchCount2());
        shift.setMixAndMatchCount3(shift.getMixAndMatchCount3() + getMixAndMatchCount3());
        shift.setMixAndMatchCount4(shift.getMixAndMatchCount4() + getMixAndMatchCount4());
        shift.setMixAndMatchCount5(shift.getMixAndMatchCount5() + getMixAndMatchCount5());
        shift.setMixAndMatchCount6(shift.getMixAndMatchCount6() + getMixAndMatchCount6());
        shift.setMixAndMatchCount7(shift.getMixAndMatchCount7() + getMixAndMatchCount7());
        shift.setMixAndMatchCount8(shift.getMixAndMatchCount8() + getMixAndMatchCount8());
        shift.setMixAndMatchCount9(shift.getMixAndMatchCount9() + getMixAndMatchCount9());
        shift.setMixAndMatchCount10(shift.getMixAndMatchCount10() + getMixAndMatchCount10());
        z.setMixAndMatchCount0(z.getMixAndMatchCount0() + getMixAndMatchCount0());
        z.setMixAndMatchCount1(z.getMixAndMatchCount1() + getMixAndMatchCount1());
        z.setMixAndMatchCount2(z.getMixAndMatchCount2() + getMixAndMatchCount2());
        z.setMixAndMatchCount3(z.getMixAndMatchCount3() + getMixAndMatchCount3());
        z.setMixAndMatchCount4(z.getMixAndMatchCount4() + getMixAndMatchCount4());
        z.setMixAndMatchCount5(z.getMixAndMatchCount5() + getMixAndMatchCount5());
        z.setMixAndMatchCount6(z.getMixAndMatchCount6() + getMixAndMatchCount6());
        z.setMixAndMatchCount7(z.getMixAndMatchCount7() + getMixAndMatchCount7());
        z.setMixAndMatchCount8(z.getMixAndMatchCount8() + getMixAndMatchCount8());
        z.setMixAndMatchCount9(z.getMixAndMatchCount9() + getMixAndMatchCount9());
        z.setMixAndMatchCount10(z.getMixAndMatchCount10() + getMixAndMatchCount10());

        // SI折扣
        /* １.折让 全折 */
        shift.getSIDiscountAmount0().addMe(getSIDiscountAmount0());
        shift.getSIDiscountAmount1().addMe(getSIDiscountAmount1());
        shift.getSIDiscountAmount2().addMe(getSIDiscountAmount2());
        shift.getSIDiscountAmount3().addMe(getSIDiscountAmount3());
        shift.getSIDiscountAmount4().addMe(getSIDiscountAmount4());
        shift.getSIDiscountAmount5().addMe(getSIDiscountAmount5());
        shift.getSIDiscountAmount6().addMe(getSIDiscountAmount6());
        shift.getSIDiscountAmount7().addMe(getSIDiscountAmount7());
        shift.getSIDiscountAmount8().addMe(getSIDiscountAmount8());
        shift.getSIDiscountAmount9().addMe(getSIDiscountAmount9());
        shift.getSIDiscountAmount10().addMe(getSIDiscountAmount10());

        z.getSIDiscountAmount0().addMe(getSIDiscountAmount0());
        z.getSIDiscountAmount1().addMe(getSIDiscountAmount1());
        z.getSIDiscountAmount2().addMe(getSIDiscountAmount2());
        z.getSIDiscountAmount3().addMe(getSIDiscountAmount3());
        z.getSIDiscountAmount4().addMe(getSIDiscountAmount4());
        z.getSIDiscountAmount5().addMe(getSIDiscountAmount5());
        z.getSIDiscountAmount6().addMe(getSIDiscountAmount6());
        z.getSIDiscountAmount7().addMe(getSIDiscountAmount7());
        z.getSIDiscountAmount8().addMe(getSIDiscountAmount8());
        z.getSIDiscountAmount9().addMe(getSIDiscountAmount9());
        z.getSIDiscountAmount10().addMe(getSIDiscountAmount10());

        shift.setSIDiscountCount0(shift.getSIDiscountCount0() + getSIDiscountCount0());
        shift.setSIDiscountCount1(shift.getSIDiscountCount1() + getSIDiscountCount1());
        shift.setSIDiscountCount2(shift.getSIDiscountCount2() + getSIDiscountCount2());
        shift.setSIDiscountCount3(shift.getSIDiscountCount3() + getSIDiscountCount3());
        shift.setSIDiscountCount4(shift.getSIDiscountCount4() + getSIDiscountCount4());
        shift.setSIDiscountCount5(shift.getSIDiscountCount5() + getSIDiscountCount5());
        shift.setSIDiscountCount6(shift.getSIDiscountCount6() + getSIDiscountCount6());
        shift.setSIDiscountCount7(shift.getSIDiscountCount7() + getSIDiscountCount7());
        shift.setSIDiscountCount8(shift.getSIDiscountCount8() + getSIDiscountCount8());
        shift.setSIDiscountCount9(shift.getSIDiscountCount9() + getSIDiscountCount9());
        shift.setSIDiscountCount10(shift.getSIDiscountCount10() + getSIDiscountCount10());

        z.setSIDiscountCount0(z.getSIDiscountCount0() + getSIDiscountCount0());
        z.setSIDiscountCount1(z.getSIDiscountCount1() + getSIDiscountCount1());
        z.setSIDiscountCount2(z.getSIDiscountCount2() + getSIDiscountCount2());
        z.setSIDiscountCount3(z.getSIDiscountCount3() + getSIDiscountCount3());
        z.setSIDiscountCount4(z.getSIDiscountCount4() + getSIDiscountCount4());
        z.setSIDiscountCount5(z.getSIDiscountCount5() + getSIDiscountCount5());
        z.setSIDiscountCount6(z.getSIDiscountCount6() + getSIDiscountCount6());
        z.setSIDiscountCount7(z.getSIDiscountCount7() + getSIDiscountCount7());
        z.setSIDiscountCount8(z.getSIDiscountCount8() + getSIDiscountCount8());
        z.setSIDiscountCount9(z.getSIDiscountCount9() + getSIDiscountCount9());
        z.setSIDiscountCount10(z.getSIDiscountCount10() + getSIDiscountCount10());

        /* 2.折扣 开放折扣 */
        shift.getSIPercentOffAmount0().addMe(getSIPercentOffAmount0());
        shift.getSIPercentOffAmount1().addMe(getSIPercentOffAmount1());
        shift.getSIPercentOffAmount2().addMe(getSIPercentOffAmount2());
        shift.getSIPercentOffAmount3().addMe(getSIPercentOffAmount3());
        shift.getSIPercentOffAmount4().addMe(getSIPercentOffAmount4());
        shift.getSIPercentOffAmount5().addMe(getSIPercentOffAmount5());
        shift.getSIPercentOffAmount6().addMe(getSIPercentOffAmount6());
        shift.getSIPercentOffAmount7().addMe(getSIPercentOffAmount7());
        shift.getSIPercentOffAmount8().addMe(getSIPercentOffAmount8());
        shift.getSIPercentOffAmount9().addMe(getSIPercentOffAmount9());
        shift.getSIPercentOffAmount10().addMe(getSIPercentOffAmount10());

        z.getSIPercentOffAmount0().addMe(getSIPercentOffAmount0());
        z.getSIPercentOffAmount1().addMe(getSIPercentOffAmount1());
        z.getSIPercentOffAmount2().addMe(getSIPercentOffAmount2());
        z.getSIPercentOffAmount3().addMe(getSIPercentOffAmount3());
        z.getSIPercentOffAmount4().addMe(getSIPercentOffAmount4());
        z.getSIPercentOffAmount5().addMe(getSIPercentOffAmount5());
        z.getSIPercentOffAmount6().addMe(getSIPercentOffAmount6());
        z.getSIPercentOffAmount7().addMe(getSIPercentOffAmount7());
        z.getSIPercentOffAmount8().addMe(getSIPercentOffAmount8());
        z.getSIPercentOffAmount9().addMe(getSIPercentOffAmount9());
        z.getSIPercentOffAmount10().addMe(getSIPercentOffAmount10());

        shift.setSIPercentOffCount0(shift.getSIPercentOffCount0() + getSIPercentOffCount0());
        shift.setSIPercentOffCount1(shift.getSIPercentOffCount1() + getSIPercentOffCount1());
        shift.setSIPercentOffCount2(shift.getSIPercentOffCount2() + getSIPercentOffCount2());
        shift.setSIPercentOffCount3(shift.getSIPercentOffCount3() + getSIPercentOffCount3());
        shift.setSIPercentOffCount4(shift.getSIPercentOffCount4() + getSIPercentOffCount4());
        shift.setSIPercentOffCount5(shift.getSIPercentOffCount5() + getSIPercentOffCount5());
        shift.setSIPercentOffCount6(shift.getSIPercentOffCount6() + getSIPercentOffCount6());
        shift.setSIPercentOffCount7(shift.getSIPercentOffCount7() + getSIPercentOffCount7());
        shift.setSIPercentOffCount8(shift.getSIPercentOffCount8() + getSIPercentOffCount8());
        shift.setSIPercentOffCount9(shift.getSIPercentOffCount9() + getSIPercentOffCount9());
        shift.setSIPercentOffCount10(shift.getSIPercentOffCount10() + getSIPercentOffCount10());

        z.setSIPercentOffCount0(z.getSIPercentOffCount0() + getSIPercentOffCount0());
        z.setSIPercentOffCount1(z.getSIPercentOffCount1() + getSIPercentOffCount1());
        z.setSIPercentOffCount2(z.getSIPercentOffCount2() + getSIPercentOffCount2());
        z.setSIPercentOffCount3(z.getSIPercentOffCount3() + getSIPercentOffCount3());
        z.setSIPercentOffCount4(z.getSIPercentOffCount4() + getSIPercentOffCount4());
        z.setSIPercentOffCount5(z.getSIPercentOffCount5() + getSIPercentOffCount5());
        z.setSIPercentOffCount6(z.getSIPercentOffCount6() + getSIPercentOffCount6());
        z.setSIPercentOffCount7(z.getSIPercentOffCount7() + getSIPercentOffCount7());
        z.setSIPercentOffCount8(z.getSIPercentOffCount8() + getSIPercentOffCount8());
        z.setSIPercentOffCount9(z.getSIPercentOffCount9() + getSIPercentOffCount9());
        z.setSIPercentOffCount10(z.getSIPercentOffCount10() + getSIPercentOffCount10());

        /* 3. 加成 */
        shift.getSIPercentPlusAmount0().addMe(getSIPercentPlusAmount0());
        shift.getSIPercentPlusAmount1().addMe(getSIPercentPlusAmount1());
        shift.getSIPercentPlusAmount2().addMe(getSIPercentPlusAmount2());
        shift.getSIPercentPlusAmount3().addMe(getSIPercentPlusAmount3());
        shift.getSIPercentPlusAmount4().addMe(getSIPercentPlusAmount4());
        shift.getSIPercentPlusAmount5().addMe(getSIPercentPlusAmount5());
        shift.getSIPercentPlusAmount6().addMe(getSIPercentPlusAmount6());
        shift.getSIPercentPlusAmount7().addMe(getSIPercentPlusAmount7());
        shift.getSIPercentPlusAmount8().addMe(getSIPercentPlusAmount8());
        shift.getSIPercentPlusAmount9().addMe(getSIPercentPlusAmount9());
        shift.getSIPercentPlusAmount10().addMe(getSIPercentPlusAmount10());

        z.getSIPercentPlusAmount0().addMe(getSIPercentPlusAmount0());
        z.getSIPercentPlusAmount1().addMe(getSIPercentPlusAmount1());
        z.getSIPercentPlusAmount2().addMe(getSIPercentPlusAmount2());
        z.getSIPercentPlusAmount3().addMe(getSIPercentPlusAmount3());
        z.getSIPercentPlusAmount4().addMe(getSIPercentPlusAmount4());
        z.getSIPercentPlusAmount5().addMe(getSIPercentPlusAmount5());
        z.getSIPercentPlusAmount6().addMe(getSIPercentPlusAmount6());
        z.getSIPercentPlusAmount7().addMe(getSIPercentPlusAmount7());
        z.getSIPercentPlusAmount8().addMe(getSIPercentPlusAmount8());
        z.getSIPercentPlusAmount9().addMe(getSIPercentPlusAmount9());
        z.getSIPercentPlusAmount10().addMe(getSIPercentPlusAmount10());

        shift.setSIPercentPlusCount0(shift.getSIPercentPlusCount0() + getSIPercentPlusCount0());
        shift.setSIPercentPlusCount1(shift.getSIPercentPlusCount1() + getSIPercentPlusCount1());
        shift.setSIPercentPlusCount2(shift.getSIPercentPlusCount2() + getSIPercentPlusCount2());
        shift.setSIPercentPlusCount3(shift.getSIPercentPlusCount3() + getSIPercentPlusCount3());
        shift.setSIPercentPlusCount4(shift.getSIPercentPlusCount4() + getSIPercentPlusCount4());
        shift.setSIPercentPlusCount5(shift.getSIPercentPlusCount5() + getSIPercentPlusCount5());
        shift.setSIPercentPlusCount6(shift.getSIPercentPlusCount6() + getSIPercentPlusCount6());
        shift.setSIPercentPlusCount7(shift.getSIPercentPlusCount7() + getSIPercentPlusCount7());
        shift.setSIPercentPlusCount8(shift.getSIPercentPlusCount8() + getSIPercentPlusCount8());
        shift.setSIPercentPlusCount9(shift.getSIPercentPlusCount9() + getSIPercentPlusCount9());
        shift.setSIPercentPlusCount10(shift.getSIPercentPlusCount10() + getSIPercentPlusCount10());

        z.setSIPercentPlusCount0(z.getSIPercentPlusCount0() + getSIPercentPlusCount0());
        z.setSIPercentPlusCount1(z.getSIPercentPlusCount1() + getSIPercentPlusCount1());
        z.setSIPercentPlusCount2(z.getSIPercentPlusCount2() + getSIPercentPlusCount2());
        z.setSIPercentPlusCount3(z.getSIPercentPlusCount3() + getSIPercentPlusCount3());
        z.setSIPercentPlusCount4(z.getSIPercentPlusCount4() + getSIPercentPlusCount4());
        z.setSIPercentPlusCount5(z.getSIPercentPlusCount5() + getSIPercentPlusCount5());
        z.setSIPercentPlusCount6(z.getSIPercentPlusCount6() + getSIPercentPlusCount6());
        z.setSIPercentPlusCount7(z.getSIPercentPlusCount7() + getSIPercentPlusCount7());
        z.setSIPercentPlusCount8(z.getSIPercentPlusCount8() + getSIPercentPlusCount8());
        z.setSIPercentPlusCount9(z.getSIPercentPlusCount9() + getSIPercentPlusCount9());
        z.setSIPercentPlusCount10(z.getSIPercentPlusCount10() + getSIPercentPlusCount10());

        // 来客数
        if (shift.getCustomerCount() == null) {
            shift.setCustomerCount(integer0);
        }
        if (z.getCustomerCount() == null) {
            z.setCustomerCount(integer0);
        }
        int in = shift.getCustomerCount() + getCustomerCount();
        shift.setCustomerCount(in);
        in = z.getCustomerCount() + getCustomerCount();
        z.setCustomerCount(in);

        // 交易数
        in = shift.getTransactionCount() + 1;
        shift.setTransactionCount(in);
        in = z.getTransactionCount() + 1;
        z.setTransactionCount(in);

        // 礼券销售
        if (getDealType2().equals("F")) {
            shift.setVoucherAmount(shift.getVoucherAmount().addMe(
                    getSalesAmount()));
            shift.setVoucherCount(shift.getVoucherCount() + 1);
            z.setVoucherAmount(z.getVoucherAmount().addMe(getSalesAmount()));
            z.setVoucherCount(z.getVoucherCount() + 1);
        }

        // Start line Item
        getLineItems(); // 這行是要把最後一筆line item放到lineItemArrayLast裡面去，以便下一行可以拿到全部
        Iterator lineItems = getLineItemsIterator();
        LineItem lineItem;
        shoppingBagTotalAmount = new HYIDouble(0);
        homeSendFeeTotalAmount = new HYIDouble(0);

        // 本交易的Z 序号
        Integer zNumber = getZSequenceNumber();
        CreamToolkit.logMessage("calcHardTotals | circulation | zNumber : "
                + getZSequenceNumber());
        while (lineItems.hasNext()) { // Start the per lineItem parsing
            
            lineItem = (LineItem) lineItems.next();
            // 计算 分类帐
            DepSales dps = null;
            String pluno = lineItem.getPluNumber();
            if (pluno.equals(PARAM.getRoundItemNo()) && PARAM.isRoundDown()) {
                String depID = lineItem.getCategoryNumber();
                dps = DepSales.queryByDepID(connection, zNumber, depID);
            } else if (pluno != null && !pluno.equals("")) {
                //PLU plu = PLU.queryBarCode(pluno);
                //if (plu != null) {
                //    String depID = plu.getDepID();
                //    if (depID != null && !depID.equals("")) {
                //        dps = DepSales.queryByDepID(connection, zNumber, depID);
                //    }
                //}
                //Bruce/20080606/ Replace with faster code.
                String depID = lineItem.getDepID();
                if (depID != null && !depID.trim().equals(""))
                    dps = DepSales.queryByDepID(connection, zNumber, depID);
            } else if (pluno != null && pluno.equals("")) {
                String depID;

                ////Bruce/2009-03-18> Remove it.
                //// 对非正常plu，需要根据分类去猜测所属depID并填写depsales,
                //// 1. 按照小分类
                //// 2. 按照大分类
                //// 3. 依次安装大中小，大中，大 查找depsales中可用的depID
                ////String strategy = GetProperty.getCatNoToDepIdStrategy();
                ////if ("1".equals(strategy)){
                ////    depID = lineItem.getMicroCategoryNumber(); // 使用最小部门做为depID
                ////} else if ("2".equals(strategy)){
                ////    if (dps == null){
                ////        depID  = lineItem.getCategoryNumber();     // 大
                ////        if (depID != null && !depID.equals("")) {
                ////            dps = DepSales.queryByDepID(connection, zNumber, depID);
                ////        }
                ////    }
                ////} else if ("3".equals(strategy)) {

                // 对非正常plu，需要根据分类去猜测所属depID并填写depsales,
                // 依次安装大中小，大中，大 查找depsales中可用的depID

                // 对部门输入, 因为不知道对应的plu.depID, 所以考虑连接分类(大中小)(大中)(大)在依次构成depID
                // 依次猜测查找相应存在的depsales记录，直到找到为止
                if (dps == null) {
                    depID  = lineItem.getCategoryNumber()
                        + lineItem.getMidCategoryNumber()
                        + lineItem.getMicroCategoryNumber();   //  大中小
                    if (depID != null && !depID.equals("")) {
                        dps = DepSales.queryByDepID(connection, zNumber, depID);
                    }
                }
                if (dps == null) {
                    depID  = lineItem.getCategoryNumber()
                        + lineItem.getMidCategoryNumber();     // 大中
                    if (depID != null && !depID.equals("")) {
                        dps = DepSales.queryByDepID(connection, zNumber, depID);
                    }
                }
                if (dps == null) {
                    depID  = lineItem.getCategoryNumber();     // 大
                    if (depID != null && !depID.equals("")) {
                        dps = DepSales.queryByDepID(connection, zNumber, depID);
                    }
                }
            }

            // 指定更正 立即更正
            if (lineItem.getDetailCode().equals("V")) { // 指定更正   
                
                //      } else if (lineItem.getDetailCode().equalsIgnoreCase("O")) { // 代收公共事业费
                // LineVoidAmount Sum of all line items' amount with DetailCode
                // 'V'.
                // LineVoidCount Add the number of all line items with
                // DetailCode 'V'.

                if (lineItem.getQuantity() == null
                        || lineItem.getQuantity().compareTo(nHYIDouble0()) == 0) {
                    if ((getDealType1().equals("0")
                            && getDealType2().equals("0") && !(getDealType3()
                            .equals("4") || getDealType3().equals("0")))
                            || (getDealType1().equals("0")
                                    && getDealType2().equals("3") && getDealType3()
                                    .equals("4"))) {
                        lineItem.setQuantity(new HYIDouble(1));
                    } else {
                        lineItem.setQuantity(new HYIDouble(-1));
                    }
                }

                z.setLineVoidAmount(z.getLineVoidAmount().subtract(
                        lineItem.getAmount()));
                z.setLineVoidCount(z.getLineVoidCount() - lineItem.getQuantity().signum());
                shift.setLineVoidAmount(shift.getLineVoidAmount().subtract(
                        lineItem.getAmount()));
                shift.setLineVoidCount(shift.getLineVoidCount() - lineItem.getQuantity().signum());

            } else if (lineItem.getDetailCode().equals("E")) { // 立即更正
                // VoidAmount Sum of all line items' amount with DetailCode 'E'.
                // VoidCount Add the number of all line items with DetailCode
                // 'E'.

                if (lineItem.getQuantity() == null
                        || lineItem.getQuantity().compareTo(nHYIDouble0()) == 0) {
                    if ((getDealType1().equals("0")
                            && getDealType2().equals("0") && !(getDealType3()
                            .equals("4") || getDealType3().equals("0")))
                            || (getDealType1().equals("0")
                                    && getDealType2().equals("3") && getDealType3()
                                    .equals("4"))) {
                        lineItem.setQuantity(new HYIDouble(1));
                    } else {
                        lineItem.setQuantity(new HYIDouble(-1));
                    }
                }

                z.setVoidAmount(z.getVoidAmount()
                        .subtract(lineItem.getAmount()));
                z.setVoidCount(z.getVoidCount() - lineItem.getQuantity().signum());
                shift.setVoidAmount(shift.getVoidAmount().subtract(
                        lineItem.getAmount()));
                shift.setVoidCount(shift.getVoidCount() - lineItem.getQuantity().signum());
            }

            if (lineItem.getRemoved())
                continue;

            if (!lineItem.getDetailCode().equals("M")
                    && !lineItem.getDetailCode().equals("D")
                    && !lineItem.getDetailCode().equals("I") // 代售
                    && !lineItem.getDetailCode().equals("O") // 代收
                    && !lineItem.getDetailCode().equals("K") // Bruce/20090513/ 期刊領刊也不用記帳  
                    && !lineItem.getDetailCode().equals("Q")
                    && !lineItem.getDetailCode().equals("N")
                    && !lineItem.getDetailCode().equals("T")) {
                /**
                 * ZReport related processing
                 */
                // ItemCountGrandTotal
                z.setItemCountGrandTotal(new HYIDouble(0)
                        .addMe(z.getItemCountGrandTotal().addMe(
                                lineItem.getQuantity())));
                /**
                 * ZReport and ShiftReport related processing
                 */
                // ItemCount
                shift.setItemCount(shift.getItemCount().addMe(
                        lineItem.getQuantity()));
                z.setItemCount(z.getItemCount().addMe(lineItem.getQuantity()));

                // 计算 分类帐
                if (dps != null) {

                    if (pluno.equals(PARAM.getRoundItemNo()) && PARAM.isRoundDown()){
                        ;  // skip and do nothing
                    } else {
                        dps.setGrossSaleTotalAmount(dps
                                .getGrossSaleTotalAmount().addMe(
                                        lineItem.getAmount()));
                        dps.setSiPlusTotalAmount(dps.getSiPlusTotalAmount()
                                .addMe(lineItem.getGrossSaleTaxAmount()));
                    }
                    
                    dps.setMixAndMatchTotalAmount(dps.getMixAndMatchTotalAmount()
                            .addMe(lineItem.getAmount().subtract(
                            lineItem.getAfterDiscountAmount())));
                    
                    dps.setNetSaleTotalAmount(dps.getNetSaleTotalAmount()
                            .addMe(lineItem.getAfterDiscountAmount()));
                    
                    if (PARAM.isGrossTrandtlTaxAmt()) {
                        ; // 毛额计税+折扣计税，净额暂时不填，如果要填必须做折扣对应的税金分摊到各个单品中
                    } else {
                        if (dps.getTaxAmount() == null)
                            dps.setTaxAmount(new HYIDouble(0));
                        dps.setTaxAmount(dps.getTaxAmount().addMe(lineItem.getTaxAmount()));
                    }

                    // 顺便登记属于购物袋商品depID的lineItem
                    if (PARAM.isGatherShoppingBagInfo() 
                            && dps.getDepID().equals(PARAM.getShoppingBagDepID())){
                        shoppingBagTotalAmount.addMe(lineItem.getAfterDiscountAmount());
                    }
                    // 顺便登记属于配送费商品depID的lineItem
                    if (PARAM.isGatherSendFeeInfo() 
                            && dps.getDepID().equals(PARAM.getSendFeeDepID())){
                        homeSendFeeTotalAmount.addMe(lineItem.getAfterDiscountAmount());
                    }
                }
            }

            if (lineItem.getPluNumber() == null) {
                lineItem.setPluNumber("");
                lineItem.setItemNumber("");
            }

            // 手输应免税
            if (lineItem.getPluNumber().equals("")
                    && (lineItem.getDetailCode().equals("S") || lineItem
                            .getDetailCode().equals("R"))) {
                shift.setManualEntryAmount(shift.getManualEntryAmount().addMe(
                        lineItem.getAmount()));
                z.setManualEntryAmount(z.getManualEntryAmount().addMe(
                        lineItem.getAmount()));
                if ((getDealType1().equals("0") && getDealType2().equals("0") && !(getDealType3()
                        .equals("4") || getDealType3().equals("0")))
                        || (getDealType1().equals("0")
                                && getDealType2().equals("3") && getDealType3()
                                .equals("4"))) {
                    shift.setManualEntryCount(shift.getManualEntryCount() - 1);
                    z.setManualEntryCount(z.getManualEntryCount() - 1);

                } else {
                    shift.setManualEntryCount(shift.getManualEntryCount() + 1);
                    z.setManualEntryCount(z.getManualEntryCount() + 1);
                }
            }

            // Open Price
            if (lineItem.isOpenPrice()) {
                z.setPriceOpenEntryAmount(z.getPriceOpenEntryAmount().addMe(
                        lineItem.getAmount()));
                shift.setPriceOpenEntryAmount(shift.getPriceOpenEntryAmount()
                        .addMe(lineItem.getAmount()));
                z.setPriceOpenEntryCount(z.getPriceOpenEntryCount() + 1);
                shift.setPriceOpenEntryCount(shift.getPriceOpenEntryCount() + 1);
            }

            // 代售 如电话卡等
            if (lineItem.getDetailCode().equals("I")) {
                Iterator iter;
                // 计算代售明细
                String firstNumber;
                String secondNumber;
                if (StringUtils.isEmpty(PARAM.getPreventScannerCatNo())) {
//                  String tt = 
                    firstNumber = lineItem.getDiscountNumber();
                    if (firstNumber.indexOf("-") > 0) 
                        firstNumber = firstNumber.substring(0, firstNumber.indexOf("-"));
                    secondNumber = "";
    
                    String fileName = "daishou" + Integer.valueOf(firstNumber) + ".conf";
                    fileName = CreamToolkit.getConfigDir() + fileName;
    
                    File propFile = new File(fileName);
                    ArrayList menuArrayList = new ArrayList();
                    try {
                        FileInputStream filein = new FileInputStream(propFile);
                        InputStreamReader inst;
                        inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
                        BufferedReader inn = new BufferedReader(inst);
                        String line;
                        char ch ;
                        int i;
    
                        do {
                            do {
                                line = inn.readLine();
                                if (line == null) {
                                    break;
                                }
                                while (line.equals("")) {
                                    line = inn.readLine();
                                }
                                i = 0;
                                do {
                                    ch = line.charAt(i);
                                    i++;
                                } while ((ch == ' ' || ch == '\t')
                                        && i < line.length());
                            } while (ch == '#' || ch == ' ' || ch == '\t');
    
                            if (line == null) {
                                break;
                            }
                            menuArrayList.add(line);
                        } while (true);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                        continue;
                    } catch (IOException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                        continue;
                    }
    
                    for (int j = 0; j < menuArrayList.size(); j++) {
                        String line = (String) menuArrayList.get(j);
                        StringTokenizer t = new StringTokenizer(line, ",.");
                        String key2 = t.nextToken();
                        t.nextToken();
                        String plunumber = t.nextToken();
                        if (plunumber.equalsIgnoreCase(lineItem.getPluNumber())) {
                            secondNumber = key2;
                            break;
                        }
                    }
                } else {
                    String line = lineItem.getDiscountNumber();
                    firstNumber = line.substring(0, line.indexOf('-'));
                    secondNumber = line.substring(line.indexOf('-') + 1, line.length());
                }
                DaishouSales daishouSales = DaishouSales.queryByFirstNumberAndSecondNumber(
                    connection, getZSequenceNumber().toString(), firstNumber, secondNumber);

                if (step2 <= TRANSACTION_SAVE_DAISHOUSALES2) {
                    if (daishouSales != null) {
                        // check 前笔误打 and 重印 and 退货
                        daishouSales.getPosAmount().addMe(lineItem.getAmount());
                        daishouSales.setPosCount(daishouSales.getPosCount() + lineItem.getQuantity().intValue());
                        daishouSales.update(connection);
//                      CreamToolkit.logMessage("processTransaction | daishouSales.update succ : " + lineItem.getAmount());
                        this.step = TRANSACTION_SAVE_SHIFT2;
                        saveTo(TRANSACTION_SAVE_SHIFT2);
                    }
                }

                iter = Reason.queryByreasonCategory("08");

                String id;
                Reason r;
                int index = 1;
                HYIDouble amount;
                String fieldName1 = "DAISHOUPLUAMT";
                String fieldName2 = "DAISHOUPLUCNT";
                String fieldName3 = "DAISHOUAMT";
                String fieldName4 = "DAISHOUCNT";
                if (iter != null) {
                    if (shift.getFieldValue(fieldName3) == null) {
                        shift.setFieldValue(fieldName3, nHYIDouble0());
                    }
                    if (shift.getFieldValue(fieldName4) == null) {
                        shift.setFieldValue(fieldName4, integer0);
                    }
                    if (z.getFieldValue(fieldName3) == null) {
                        z.setFieldValue(fieldName3, nHYIDouble0());
                    }
                    if (z.getFieldValue(fieldName4) == null) {
                        z.setFieldValue(fieldName4, integer0);
                    }

                    while (iter.hasNext()) {
                        // if (!daiShouVersion.equals("2")) {
                        r = (Reason) iter.next();
                        id = r.getreasonNumber();
                        // } else {
                        // id = ((DaiShouDef)iter.next()).getID();
                        // }

                        // if (lineItem.getPluNumber().equals(id)) {
                        if (Integer.parseInt(firstNumber) == Integer
                                .parseInt(id)) {
                            amount = lineItem.getAmount();
                            // System.out.println("(((((((((((((( = " + amount);
                            fieldName1 = fieldName1 + index;
                            fieldName2 = fieldName2 + index;

                            if (shift.getFieldValue(fieldName1) == null) {
                                shift.setFieldValue(fieldName1, nHYIDouble0());
                            }
                            if (shift.getFieldValue(fieldName2) == null) {
                                shift.setFieldValue(fieldName2, integer0);
                            }
                            if (z.getFieldValue(fieldName1) == null) {
                                z.setFieldValue(fieldName1, nHYIDouble0());
                            }
                            if (z.getFieldValue(fieldName2) == null) {
                                z.setFieldValue(fieldName2, integer0);
                            }

                            // check 前笔误打 and 重印 and 退货
                            shift.setFieldValue(fieldName1, ((HYIDouble) shift.getFieldValue(fieldName1)).addMe(amount));
                            shift.setFieldValue(fieldName3, ((HYIDouble) shift.getFieldValue(fieldName3)).addMe(amount));
                            z.setFieldValue(fieldName1, ((HYIDouble) z.getFieldValue(fieldName1)).addMe(amount));
                            z.setFieldValue(fieldName3, ((HYIDouble) z.getFieldValue(fieldName3)).addMe(amount));

                            if ((getDealType1().equals("0") && getDealType2().equals("0") && (getDealType3().equals("1")
                                || getDealType3().equals("2") || getDealType3().equals("3")))
                                || (getDealType1().equals("0") && getDealType2().equals("3") && getDealType3().equals("4"))) {
                                shift.setFieldValue(fieldName2, ((Integer)shift.getFieldValue(fieldName2)) - 1);
                                shift.setFieldValue(fieldName4, ((Integer)shift.getFieldValue(fieldName4)) - 1);
                                z.setFieldValue(fieldName2, ((Integer)z.getFieldValue(fieldName2)) - 1);
                                z.setFieldValue(fieldName4, ((Integer) z.getFieldValue(fieldName4)) - 1);
                            } else {
                                shift.setFieldValue(fieldName2, ((Integer) shift.getFieldValue(fieldName2)) + 1);
                                shift.setFieldValue(fieldName4, ((Integer) shift.getFieldValue(fieldName4)) + 1);
                                z.setFieldValue(fieldName2, ((Integer) z.getFieldValue(fieldName2)) + 1);
                                z.setFieldValue(fieldName4, ((Integer) z.getFieldValue(fieldName4)) + 1);
                            }
                            break;
                        }
                        index++;
                    } // End While((iter.hasNext()))
                }

            } else if (lineItem.getDetailCode().equalsIgnoreCase("O")) { // 代收公共事业费
                writeDaiShouSales2(connection, lineItem, false);

                // ZhaoHong
                // 2003-07-25
                if (shift.getDaiShouAmount2() == null)
                    shift.setDaiShouAmount2(new HYIDouble(0.00));
                if (shift.getDaiShouCount2() == null)
                    shift.setDaiShouCount2(0);
                if (z.getDaiShouAmount2() == null)
                    z.setDaiShouAmount2(new HYIDouble(0.00));
                if (z.getDaiShouCount2() == null)
                    z.setDaiShouCount2(0);
                HYIDouble tmpAmount = lineItem.getAmount();
                shift.setDaiShouAmount2(shift.getDaiShouAmount2().addMe(
                        tmpAmount));
                z.setDaiShouAmount2(z.getDaiShouAmount2().addMe(tmpAmount));
                //tmpAmount = null;

                if ((getDealType1().equals("0") && getDealType2().equals("0") && (getDealType3()
                        .equals("1")
                        || getDealType3().equals("2") || getDealType3().equals(
                        "3")))
                        || (getDealType1().equals("0")
                                && getDealType2().equals("3") && getDealType3()
                                .equals("4"))) {

                    shift.setDaiShouCount2(shift.getDaiShouCount2() - 1);
                    z.setDaiShouCount2(z.getDaiShouCount2() - 1);
                } else {
                    shift.setDaiShouCount2(shift.getDaiShouCount2() + 1);
                    z.setDaiShouCount2(z.getDaiShouCount2() + 1);
                }

            } else if (lineItem.getDetailCode().equalsIgnoreCase("Q")) { // 代付
                Iterator iter = Reason.queryByreasonCategory("09");
                Reason r;
                String daifuID;
                int daifuIndex = 1;
                HYIDouble daifuAmount;
                // HYIDouble totalAmount = nHYIDouble0();
                String fieldName1 = "DAIFUPLUAMT";
                String fieldName2 = "DAIFUPLUCNT";
                String fieldName3 = "DAIFUAMT";
                String fieldName4 = "DAIFUCNT";
                if (iter != null) {
                    while (iter.hasNext()) {
                        r = (Reason) iter.next();
                        daifuID = r.getreasonNumber();

                        if (lineItem.getPluNumber().equals(daifuID)) {
                            daifuAmount = lineItem.getUnitPrice().negate();
                            fieldName1 = fieldName1 + daifuIndex;
                            fieldName2 = fieldName2 + daifuIndex;

                            // check 前笔误打 and 重印
                            shift.setFieldValue(fieldName1, ((HYIDouble) shift
                                    .getFieldValue(fieldName1))
                                    .addMe(daifuAmount));
                            shift.setFieldValue(fieldName3, ((HYIDouble) shift
                                    .getFieldValue(fieldName3))
                                    .addMe(daifuAmount));
                            z.setFieldValue(fieldName1, ((HYIDouble) z
                                    .getFieldValue(fieldName1))
                                    .addMe(daifuAmount));
                            z.setFieldValue(fieldName3, ((HYIDouble) z
                                    .getFieldValue(fieldName3))
                                    .addMe(daifuAmount));

                            if (getDealType1().equals("0")
                                    && getDealType2().equals("0")
                                    && (getDealType3().equals("1")
                                            || getDealType3().equals("2") || getDealType3()
                                            .equals("3"))) {
                                shift.setFieldValue(fieldName2, ((Integer) shift.getFieldValue(fieldName2)) - 1);
                                shift.setFieldValue(fieldName4, ((Integer) shift.getFieldValue(fieldName4)) - 1);
                                z.setFieldValue(fieldName2, ((Integer) z.getFieldValue(fieldName2)) - 1);
                                z.setFieldValue(fieldName4, ((Integer) z.getFieldValue(fieldName4)) - 1);
                            } else {
                                shift.setFieldValue(fieldName2, ((Integer) shift.getFieldValue(fieldName2)) + 1);
                                shift.setFieldValue(fieldName4, ((Integer) shift.getFieldValue(fieldName4)) + 1);
                                z.setFieldValue(fieldName2, ((Integer) z.getFieldValue(fieldName2)) + 1);
                                z.setFieldValue(fieldName4, ((Integer) z.getFieldValue(fieldName4)) + 1);
                            }
                            break;
                        }
                        daifuIndex++;
                    }
                }

            } else if (getDealType2().equals("H")) { // 'H':借零
                shift.getCashInAmount().addMe(lineItem.getUnitPrice());
                shift.setCashInCount(shift.getCashInCount() + 1);
                z.getCashInAmount().addMe(lineItem.getUnitPrice());
                z.setCashInCount(z.getCashInCount() + 1);
                if (!check) {
                    if (step2 <= TRANSACTION_SAVE_SHIFT2) {
                        shift.update(connection);
                        this.step = TRANSACTION_SAVE_Z2;
                        saveTo(TRANSACTION_SAVE_Z2);
                    }
                    if (step2 <= TRANSACTION_SAVE_Z2) {
                        z.update(connection);
                        this.step = TRANSACTION_SAVE_SHIFT3;
                        saveTo(TRANSACTION_SAVE_SHIFT3);
                    }
                }
                return;

            } else if (getDealType2().equals("G")) { // 'G':投库
                shift.getCashOutAmount().addMe(lineItem.getUnitPrice());
                shift.setCashOutCount(shift.getCashOutCount() + 1);
                z.getCashOutAmount().addMe(lineItem.getUnitPrice());
                z.setCashOutCount(z.getCashOutCount() + 1);
                if (!check) {
                    if (step2 <= TRANSACTION_SAVE_SHIFT3) {
                        shift.update(connection);
                        this.step = TRANSACTION_SAVE_Z3;
                        saveTo(TRANSACTION_SAVE_Z3);
                    }
                    if (step2 <= TRANSACTION_SAVE_Z3) {
                        z.update(connection);
                        this.step = TRANSACTION_SAVE_SHIFT4;
                        saveTo(TRANSACTION_SAVE_SHIFT4);
                    }
                }
                return;

            } else if (lineItem.getDetailCode().equalsIgnoreCase("T")) { // "T":PaidOut
                Iterator iter = Reason.queryByreasonCategory("11");
                Reason r;
                String id;
                int index = 1;
                HYIDouble amount = null;
                // HYIDouble totalAmount = nHYIDouble0();
                String fieldName1 = "POUTPLUAMT";
                String fieldName2 = "POUTPLUCNT";
                String fieldName3 = "POUTAMT";
                String fieldName4 = "POUTCNT";
                if (iter != null) {

                    if (shift.getFieldValue(fieldName3) == null) {
                        shift.setFieldValue(fieldName3, nHYIDouble0());
                    }
                    if (shift.getFieldValue(fieldName4) == null) {
                        shift.setFieldValue(fieldName4, integer0);
                    }
                    if (z.getFieldValue(fieldName3) == null) {
                        z.setFieldValue(fieldName3, nHYIDouble0());
                    }
                    if (z.getFieldValue(fieldName4) == null) {
                        z.setFieldValue(fieldName4, integer0);
                    }

                    while (iter.hasNext()) {
                        r = (Reason) iter.next();
                        id = r.getreasonNumber();
                        if (lineItem.getPluNumber().equals(id)) {
                            amount = lineItem.getUnitPrice().negate();
                            fieldName1 = fieldName1 + index;
                            fieldName2 = fieldName2 + index;

                            if (shift.getFieldValue(fieldName1) == null) {
                                shift.setFieldValue(fieldName1, nHYIDouble0());
                            }
                            if (shift.getFieldValue(fieldName2) == null) {
                                shift.setFieldValue(fieldName2, integer0);
                            }
                            if (z.getFieldValue(fieldName1) == null) {
                                z.setFieldValue(fieldName1, nHYIDouble0());
                            }
                            if (z.getFieldValue(fieldName2) == null) {
                                z.setFieldValue(fieldName2, integer0);
                            }

                            if (getDealType1().equals("0") && getDealType2().equals("0") && (getDealType3().equals("1")
                                || getDealType3().equals("2") || getDealType3().equals("3"))) {
                                shift.setFieldValue(fieldName2, ((Integer) shift.getFieldValue(fieldName2)) - 1);
                                shift.setFieldValue(fieldName4, ((Integer) shift.getFieldValue(fieldName4)) - 1);
                                z.setFieldValue(fieldName2, ((Integer) z.getFieldValue(fieldName2)) - 1);
                                z.setFieldValue(fieldName4, ((Integer) z.getFieldValue(fieldName4)) - 1);
                                shift.setFieldValue(fieldName1, ((HYIDouble) shift.getFieldValue(fieldName1)).subtractMe(amount));
                                shift.setFieldValue(fieldName3, ((HYIDouble) shift.getFieldValue(fieldName3)).subtractMe(amount));
                                z.setFieldValue(fieldName1, ((HYIDouble) z.getFieldValue(fieldName1)).subtractMe(amount));
                                z.setFieldValue(fieldName3, ((HYIDouble) z.getFieldValue(fieldName3)).subtractMe(amount));
                            } else {
                                shift.setFieldValue(fieldName2, ((Integer) shift.getFieldValue(fieldName2)) + 1);
                                shift.setFieldValue(fieldName4, ((Integer) shift.getFieldValue(fieldName4)) + 1);
                                z.setFieldValue(fieldName2, ((Integer) z.getFieldValue(fieldName2)) + 1);
                                z.setFieldValue(fieldName4, ((Integer) z.getFieldValue(fieldName4)) + 1);
                                shift.setFieldValue(fieldName1, ((HYIDouble) shift.getFieldValue(fieldName1)).addMe(amount));
                                shift.setFieldValue(fieldName3, ((HYIDouble) shift.getFieldValue(fieldName3)).addMe(amount));
                                z.setFieldValue(fieldName1, ((HYIDouble) z.getFieldValue(fieldName1)).addMe(amount));
                                z.setFieldValue(fieldName3, ((HYIDouble) z.getFieldValue(fieldName3)).addMe(amount));
                            }
                            break;
                        }
                        index++;
                    }
                }

            } else if (lineItem.getDetailCode().equalsIgnoreCase("N")) { // "N":PaidIn
                Iterator iter = Reason.queryByreasonCategory("10");
                Reason r;
                String id;
                int index = 1;
                HYIDouble amount;
                // HYIDouble totalAmount = nHYIDouble0();
                String fieldName1 = "PINPLUAMT";
                String fieldName2 = "PINPLUCNT";
                String fieldName3 = "PINAMT";
                String fieldName4 = "PINCNT";
                if (iter != null) {

                    if (shift.getFieldValue(fieldName3) == null) {
                        shift.setFieldValue(fieldName3, nHYIDouble0());
                    }
                    if (shift.getFieldValue(fieldName4) == null) {
                        shift.setFieldValue(fieldName4, integer0);
                    }
                    if (z.getFieldValue(fieldName3) == null) {
                        z.setFieldValue(fieldName3, nHYIDouble0());
                    }
                    if (z.getFieldValue(fieldName4) == null) {
                        z.setFieldValue(fieldName4, integer0);
                    }

                    while (iter.hasNext()) {
                        r = (Reason) iter.next();
                        id = r.getreasonNumber();
                        if (lineItem.getPluNumber().equals(id)) {
                            amount = lineItem.getUnitPrice();
                            fieldName1 = fieldName1 + index;
                            fieldName2 = fieldName2 + index;

                            if (shift.getFieldValue(fieldName1) == null) {
                                shift.setFieldValue(fieldName1, nHYIDouble0());
                            }
                            if (shift.getFieldValue(fieldName2) == null) {
                                shift.setFieldValue(fieldName2, integer0);
                            }
                            if (z.getFieldValue(fieldName1) == null) {
                                z.setFieldValue(fieldName1, nHYIDouble0());
                            }
                            if (z.getFieldValue(fieldName2) == null) {
                                z.setFieldValue(fieldName2, integer0);
                            }

                            if (getDealType1().equals("0") && getDealType2().equals("0") && (getDealType3().equals("1")
                                || getDealType3().equals("2") || getDealType3().equals("3"))) {
                                shift.setFieldValue(fieldName2, ((Integer) shift.getFieldValue(fieldName2)) - 1);
                                shift.setFieldValue(fieldName4, ((Integer) shift.getFieldValue(fieldName4)) - 1);
                                z.setFieldValue(fieldName2, ((Integer) z.getFieldValue(fieldName2)) - 1);
                                z.setFieldValue(fieldName4, ((Integer) z.getFieldValue(fieldName4)) - 1);
                                shift.setFieldValue(fieldName1, ((HYIDouble) shift.getFieldValue(fieldName1)).subtractMe(amount));
                                shift.setFieldValue(fieldName3, ((HYIDouble) shift.getFieldValue(fieldName3)).subtractMe(amount));
                                z.setFieldValue(fieldName1, ((HYIDouble) z.getFieldValue(fieldName1)).subtractMe(amount));
                                z.setFieldValue(fieldName3, ((HYIDouble) z.getFieldValue(fieldName3)).subtractMe(amount));
                            } else {
                                shift.setFieldValue(fieldName2, ((Integer) shift.getFieldValue(fieldName2)) + 1);
                                shift.setFieldValue(fieldName4, ((Integer) shift.getFieldValue(fieldName4)) + 1);
                                z.setFieldValue(fieldName2, ((Integer) z.getFieldValue(fieldName2)) + 1);
                                z.setFieldValue(fieldName4, ((Integer) z.getFieldValue(fieldName4)) + 1);
                                shift.setFieldValue(fieldName1, ((HYIDouble) shift.getFieldValue(fieldName1)).addMe(amount));
                                shift.setFieldValue(fieldName3, ((HYIDouble) shift.getFieldValue(fieldName3)).addMe(amount));
                                z.setFieldValue(fieldName1, ((HYIDouble) z.getFieldValue(fieldName1)).addMe(amount));
                                z.setFieldValue(fieldName3, ((HYIDouble) z.getFieldValue(fieldName3)).addMe(amount));
                            }
                            break;
                        }
                        index++;
                    }
                }
            } else if (lineItem.getDetailCode().equalsIgnoreCase("J")
                    || lineItem.getDetailCode().equalsIgnoreCase("L")) { // 期刊

                if (shift.getPreRcvAmount() == null)
                    shift.setPreRcvAmount(new HYIDouble(0.00));
                if (shift.getPreRcvCount() == null)
                    shift.setPreRcvCount(0);
                if (z.getPreRcvAmount() == null)
                    z.setPreRcvAmount(new HYIDouble(0.00));
                if (z.getPreRcvCount() == null)
                    z.setPreRcvCount(0);
                HYIDouble tmpAmount = lineItem.getAmount();
                shift.getPreRcvAmount().addMe(tmpAmount);
                z.getPreRcvAmount().addMe(tmpAmount);
                if (lineItem.getDetailCode().equalsIgnoreCase("J")) {
                    shift.setPreRcvCount(shift.getPreRcvCount() + 1);
                    z.setPreRcvCount(z.getPreRcvCount() + 1);
                } else if (lineItem.getDetailCode().equalsIgnoreCase("L")) {
                    shift.setPreRcvCount(shift.getPreRcvCount() - 1);
                    z.setPreRcvCount(z.getPreRcvCount() - 1);
                }
            }
        } // end of for circulation

        // 支付相关.............................................................
        // 溢收
        // SpillAmount, SpillCount
        if (getSpillAmount().doubleValue() != 0) {
            int integer = 0;
            shift
                    .setSpillAmount(shift.getSpillAmount().addMe(
                            getSpillAmount()));
            if (getSpillAmount().compareTo(nHYIDouble0()) == 1) {
                integer = shift.getSpillCount() + 1;
            } else {
                integer = shift.getSpillCount() - 1;
            }
            shift.setSpillCount(integer);
            z.setSpillAmount(z.getSpillAmount().addMe(getSpillAmount()));
            if (getSpillAmount().compareTo(nHYIDouble0()) == 1) {
                integer = z.getSpillCount() + 1;
            } else {
                integer = z.getSpillCount() - 1;
            }
            z.setSpillCount(integer);
        }


        // Start payment realted data accumulation
        // Pay00Amount ~ Pay29Amount Add the corresponding payment amount.
        // Pay00Count ~ Pay29 CountCount up the corresponding payment count.
        // (Count down if it is a作废交易or退货交易)
        
        udpateShiftZExData(connection);
        
        HYIDouble currentAmount = getPayAmount1();
        if (currentAmount != null) { //
            String payID = getPayNumber1();
            if (!payID.trim().equals("")) {
                currentAmount = shift.getPayAmountByID(payID).addMe(
                        currentAmount);
                shift.setPayAmountByID(payID, currentAmount);
                currentAmount = getPayAmount1();
                currentAmount = z.getPayAmountByID(payID).addMe(currentAmount);
                z.setPayAmountByID(payID, currentAmount);
                int currentPayCount = shift.getPayCountByID(payID);
                int currentPayCount2 = z.getPayCountByID(payID);
                currentAmount = getPayAmount1();
                if ((currentAmount.compareTo(nHYIDouble0()) == 1 && getDaiFuAmount()
                        .compareTo(nHYIDouble0()) == 0)
                        || (currentAmount.compareTo(nHYIDouble0()) == -1 && getDaiFuAmount()
                                .compareTo(nHYIDouble0()) != 0)) {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount + 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 + 1));

                } else {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount - 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 - 1));

                }
            }
        }
        currentAmount = getPayAmount2();
        if (currentAmount != null) {
            String payID = getPayNumber2();
            if (!payID.trim().equals("")) {
                currentAmount = shift.getPayAmountByID(payID).addMe(
                        currentAmount);
                shift.setPayAmountByID(payID, currentAmount);
                currentAmount = getPayAmount2();
                currentAmount = z.getPayAmountByID(payID).addMe(currentAmount);
                z.setPayAmountByID(payID, currentAmount);
                int currentPayCount = shift.getPayCountByID(payID);
                int currentPayCount2 = z.getPayCountByID(payID);
                currentAmount = getPayAmount1();
                if ((currentAmount.compareTo(nHYIDouble0()) == 1 && getDaiFuAmount()
                        .compareTo(nHYIDouble0()) == 0)
                        || (currentAmount.compareTo(nHYIDouble0()) == -1 && getDaiFuAmount()
                                .compareTo(nHYIDouble0()) != 0)) {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount + 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 + 1));

                } else {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount - 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 - 1));

                }
            }
        }
        
        currentAmount = getPayAmount3();
        if (currentAmount != null) {
            String payID = getPayNumber3();
            if (!payID.trim().equals("")) {
                currentAmount = shift.getPayAmountByID(payID).addMe(
                        currentAmount);
                shift.setPayAmountByID(payID, currentAmount);
                currentAmount = getPayAmount3();
                currentAmount = z.getPayAmountByID(payID).addMe(currentAmount);
                z.setPayAmountByID(payID, currentAmount);
                int currentPayCount = shift.getPayCountByID(payID);
                int currentPayCount2 = z.getPayCountByID(payID);
                currentAmount = getPayAmount1();
                if ((currentAmount.compareTo(nHYIDouble0()) == 1 && getDaiFuAmount()
                        .compareTo(nHYIDouble0()) == 0)
                        || (currentAmount.compareTo(nHYIDouble0()) == -1 && getDaiFuAmount()
                                .compareTo(nHYIDouble0()) == -1)) {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount + 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 + 1));

                } else {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount - 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 - 1));

                }
            }
        }
        currentAmount = getPayAmount4();
        if (currentAmount != null) {
            String payID = getPayNumber4();
            if (!payID.trim().equals("")) {
                currentAmount = shift.getPayAmountByID(payID).addMe(
                        currentAmount);
                shift.setPayAmountByID(payID, currentAmount);
                currentAmount = getPayAmount4();
                currentAmount = z.getPayAmountByID(payID).addMe(currentAmount);
                z.setPayAmountByID(payID, currentAmount);
                int currentPayCount = shift.getPayCountByID(payID);
                int currentPayCount2 = z.getPayCountByID(payID);
                currentAmount = getPayAmount1();
                if ((currentAmount.compareTo(nHYIDouble0()) == 1 && getDaiFuAmount()
                        .compareTo(nHYIDouble0()) == 0)
                        || (currentAmount.compareTo(nHYIDouble0()) == -1 && getDaiFuAmount()
                                .compareTo(nHYIDouble0()) == -1)) {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount + 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 + 1));

                } else {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount - 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 - 1));

                }
            }
        }
        
        // check paycash, 支付代码00金额    支付代码00固定为现金
        if (shift.getPay00Amount() != null && z.getPay00Amount() != null
                && getChangeAmount() != null) {
            shift.setPay00Amount(shift.getPay00Amount().subtract(
                    getChangeAmount()));
            z.setPay00Amount(z.getPay00Amount().subtract(getChangeAmount()));
        }

        /*
         * End of data accumulation
         */
        if (!check) {
            if (step2 <= TRANSACTION_SAVE_SHIFT4) {
                shift.update(connection);
                this.step = TRANSACTION_SAVE_Z4;
                saveTo(TRANSACTION_SAVE_Z4);
            }
            if (step2 <= TRANSACTION_SAVE_Z4) {
                z.update(connection);
                this.step = TRANSACTION_SAVE_END;
                saveTo(TRANSACTION_SAVE_END);
            }
            // DepSales.updateAll(CreamToolkit.getInitialDate());
        }
        
        CreamToolkit.logMessage("calcHardTotals | end");
        
    }

//    private void rollBack(DbConnection connection) throws SQLException {
//        connection.rollback();
//        connection.close();
//        
//        // Bruce/2003-12-01
//        // 禁止操作
//        StateMachine.getInstance().setEventProcessEnabled(false);
//        try {
//            Thread.sleep(1000L * 60 * 60 * 24 * 10);
//        } catch (InterruptedException e) {
//        }
//
////      if (step <= TRANSACTION_SAVE_LINEITEM) {
////          int len = (int) getLineItems().length;
////          for (int i = 0; i < len; i++) {
////              LineItem li = (LineItem) lineItemArrayLast.get(i);
////              if (li.isCommit())
////                  li.deleteByPrimaryKey(connection);
////          }
////          Iterator iter = daiShouSalesCommitted.iterator();
////          while (iter.hasNext()) {
////              DaiShouSales2 dss = (DaiShouSales2) iter.next();
////              dss.deleteByPrimaryKey(connection);
////          }
////      }
////      if (isCommit() && step <= TRANSACTION_SAVE_TRAN)
////          deleteByPrimaryKey(connection);
////      CreamToolkit.writeTransactionLog("# Failed, rollback");
////
////      // Bruce/2003-12-01
////      // Still count up "NextTransactionNumber"
////      // 以免因为仍有残余交易记录在数据库会导致一直无法成功存储的问题
////      CreamPropertyUtil prop = CreamPropertyUtil.getInstance();
////      int nextTransactionNumber = getTransactionNumber().intValue() + 1;
////      String nextTranNoOrig = GetProperty.getNextTransactionNumber("1");
////      if (Integer.parseInt(nextTranNoOrig) < nextTransactionNumber) {
////          if (nextTransactionNumber > getMaxTranNumberInProperty())
////              nextTransactionNumber = 1;
////          prop.setProperty("NextTransactionNumber", ""
////                  + nextTransactionNumber);
////          prop.deposit(connection);
////      }
//    }

    /**
     * Store transaction data into tables(tranhead, trandetail).
     */
    synchronized public void store(DbConnection connection) throws SQLException {
        try {
            if (POSTerminalApplication.getInstance().getTrainingMode())
                return;

            // insert pending credit card detail data --
            CATAuthSalesState.getInstance().insertCreditCardInfo(connection);
            CATAuthSalesFailedState.getInstance().insertCreditCardInfo(connection);

            // Nitori着付二期: 結帳後，將著付商品去掉，支付增加一個負(正)項的“應收尾款”
            if ("W".equals(getState1()) || "X".equals(getState1()))
                removeDownPaymentItem();

            store(connection, TRANSACTION_SAVE_BEGIN);
        } catch (EntityNotFoundException e) {
            throw new SQLException(e.toString());
        }
    }

    /**
     * <pre>
     * step == 0 :初始化 
     * step == 1 :lineitem已保存 
     * step == 2 :transaction已保存 
     * step == 3 :dep已保存 
     * step == 4 :shift已保存 
     * step == 5 :z已保存(end) 
     * step == 6 : Store transaction data into tables(tranhead, trandetail).
     * </pre>
     */
    synchronized public void store(DbConnection connection, int step2) throws SQLException,
        EntityNotFoundException {

        Integer tranNumber = getTransactionNumber();
        // daiShouSalesCommitted = new ArrayList();
        int len = 0;
        switch (step2) {
        case TRANSACTION_SAVE_BEGIN: // 初始化
            this.step = TRANSACTION_SAVE_BEGIN;
            saveTo(TRANSACTION_SAVE_BEGIN);

            // Meyer/2003-02-26/
            // UpdateTaxMM only at version 1
            // if (mixAndMatchVersion == null
            // || mixAndMatchVersion.trim().equals("")
            // || mixAndMatchVersion.trim().xequals("1")) {
            // Add mix and match line item.
            if (this.getTotalMMAmount().compareTo(nHYIDouble0()) != 0
                    && !this.getDealType3().equals("3")) {
                Iterator mmKey = (mmMap.keySet()).iterator();
                String mmID = "";
                LineItem mmLineItem = null;
                while (mmKey.hasNext()) {
                    mmID = (String) mmKey.next();
                    mmLineItem = (LineItem) mmMap.get(mmID);
                    try {
                        mmLineItem.setTransactionNumber(tranNumber);
                        mmLineItem.setTerminalNumber(getTerminalNumber());
                        mmLineItem.setDetailCode("M");
                        mmLineItem.setDiscountType("M");
                        mmLineItem.setDiscountNumber(mmID);
                        mmLineItem.setRemoved(false);

                        mmLineItem.setUnitPrice(mmLineItem.getUnitPrice()
                                .negate());
                        // Meyer/2003-02-21/
                        mmLineItem.setOriginalPrice(mmLineItem.getUnitPrice()
                                .negate());
                        mmLineItem.setQuantity(mmLineItem.getQuantity());
                        mmLineItem.setAmount(mmLineItem.getAmount().negate());
                        addLineItem(mmLineItem, false);
                        // System.out.println("add mm lineitem");
                        this.getLineItems();
                    } catch (TooManyLineItemsException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                    }
                }
            }
            // }

            // 检查是否有奇怪的情况
            integrityCheck();

            // Calculate tax amounts and "after discount amount."
            setTaxAmount(nHYIDouble0());
            setTaxAmount0(nHYIDouble0());
            setTaxAmount1(nHYIDouble0());
            setTaxAmount2(nHYIDouble0());
            setTaxAmount3(nHYIDouble0());
            setTaxAmount4(nHYIDouble0());
            setTaxAmount5(nHYIDouble0());
            setTaxAmount6(nHYIDouble0());
            setTaxAmount7(nHYIDouble0());
            setTaxAmount8(nHYIDouble0());
            setTaxAmount9(nHYIDouble0());
            setTaxAmount10(nHYIDouble0());

            len = getLineItems().length;

            for (int i = 0; i < len; i++) {
                LineItem li = lineItems.get(i);
                POSTerminalApplication app = POSTerminalApplication.getInstance();
                if(!app.getSelfBuy().equals("S")){
                    li.setTransactionNumber(tranNumber);
                }

                if ((li.getDaishouNumber() == 0) &&
                    (li.getDetailCode().equals("S")
                        || li.getDetailCode().equals("R")
                        || (li.getDetailCode().equals("D")
                        && PARAM.isGrossTrandtlTaxAmt())
                        || li.getDetailCode().equals("V")
                        || li.getDetailCode().equals("E"))
                    ) {
                    if (!li.getDetailCode().equals("D")) {
                        if (li.getDiscountType() == null
                            || li.getDiscountType().equals("")) {

                            if (li.getAfterDiscountAmount() == null
                                || li.getUnitPrice().compareTo(
                                li.getAfterDiscountAmount()) == 0) {
                                li.setAfterDiscountAmount(li.getAmount());
                            }

                        }

                        // Meyer/2003-03-10/
                        if (mixAndMatchVersion == null
                            || mixAndMatchVersion.trim().equals("")
                            || mixAndMatchVersion.trim().equals("1")
                            || mixAndMatchVersion.trim().equals("3")) {
                            li.updateMM();
                        }
                    }

                    String typeId = li.getTaxType();
                    TaxType taxType = TaxType.queryByTaxID(typeId);
                    if (taxType != null) {
                        li.caculateAndSetTaxAmount();
                        // setTaxAmount to tranhead
                        HYIDouble amt = CreamToolkit.getAmountMethod(this, "getTaxAmount" + typeId);
                        if (amt != null) {
                            amt = amt.add(li.getTaxAmount());
                            CreamToolkit.setAmountMethod(this, "setTaxAmount" + typeId, amt);
                            setTaxAmount(getTaxAmount().add(li.getTaxAmount()));
                        }
                    }
                }
            }

            if (PARAM.isCalculateTaxAmtByUnitPrice()) {
                // do nothing
            } else {
                // 重算税额
                StateToolkit.recalcTaxAmount(this);
            }

            setSignOnNumber(PARAM.getShiftNumber());
//          setInvoiceNumber("0");
            setInOutStore("1");
            setDetailCount(getLineItems().length);
            setSystemDateTime(new java.util.Date());
            setCustomerCount(1);
            if (isPureDaiShou2())
                setCustomerCount(0);

            if (getDealType2().equals("4") || getDealType2().equals("8")
                    || getDealType2().equals("9") || getDealType2().equals("A")
                    || getDealType2().equals("B") || getDealType2().equals("C")
                    || getDealType2().equals("D") || getDealType2().equals("G")
                    || getDealType2().equals("H")
                    // || (this.getDaiShouAmount().compareTo(nHYIDouble0()) == 1
                    // || getDaiShouAmount2().compareTo(nHYIDouble0()) == 1
                    // || getDaiFuAmount().compareTo(nHYIDouble0()) == -1)
                    || getDaiFuAmount().compareTo(nHYIDouble0()) != 0) {
                setCustomerCount(0);
            }

            if (getDealType1().equals("0") && getDealType2().equals("0")
                    && (getDealType3().equals("1") // 作废
                            || getDealType3().equals("2") // 重印作废
                    || getDealType3().equals("3"))) { // 交易取消作废
                setCustomerCount(0 - getCustomerCount());
            } else if (getDealType1().equals("0") && getDealType2().equals("3") // 退货
                    && getDealType3().equals("4")) {
                setCustomerCount(0 - getCustomerCount());
            }

            calcInvoiceAmount();

            writeTransactionLog(connection);
            
        case TRANSACTION_SAVE_LINEITEM: // lineitem已保存 TRANSACTION_SAVE_LINEITEM
            this.step = TRANSACTION_SAVE_LINEITEM;
            saveTo(TRANSACTION_SAVE_LINEITEM);

            // Bruce/20030505/
            // 记住目前的配达编号
            if (getAnnotatedType() != null && getAnnotatedType().equals("P")
                    && !getDealType3().equals("4")) { // Bruce/20030509/
                // 存退货交易的时候不能去记配达单号，否则配达流水号会倒退而产生重复的情形
                PARAM.updatePeiDaNumber(getAnnotatedId());
            }

            for (int i =0; i < alipayList.size(); i++) {
                Alipay_detail li = alipayList.get(i);
                li.insert(connection);
                CreamToolkit.logMessage("Alipay_detail(" + tranNumber + ") trade_no="
                        + li.getTRADE_NO() + ", amt=" + li.getTOTAL_FEE() + " is stored.");
            }
            for (int i =0; i < weiXinList.size(); i++) {
                WeiXin_detail li = weiXinList.get(i);
                li.insert(connection);
                CreamToolkit.logMessage("WeiXin_detail(" + tranNumber + ") TRANSACTIONNUMBER="
                        + li.getTRANSACTIONNUMBER() + ", amt=" + li.getTOTAL_FEE() + " is stored.");
            }
            for (int i =0; i < unionpayList.size(); i++) {
                UnionPay_detail li = unionpayList.get(i);
                li.insert(connection);
                CreamToolkit.logMessage("UnionPay_detail(" + tranNumber + ") TRANSACTIONNUMBER="
                        + li.getTRANSACTIONNUMBER() + ", amt=" + li.getTOTAL_FEE() + " is stored.");
            }
            for (int i =0; i < selfbuyHeadList.size(); i++) {
                Selfbuy_head li = selfbuyHeadList.get(i);
                li.insert(connection);
                CreamToolkit.logMessage("Selfbuy_head(" + tranNumber + ") tmtranseq="
                        + li.getTmtranseq() + ", amt=" + li.getTotal() + " is stored.");
            }
            for (int i =0; i < ecDeliveryHeadList.size(); i++) {
                if (getDealType1().equals("*") || getDealType3().equals("3") || (getDealType2().equals("3") && getDealType3().equals("4"))) {//电商订单交易取消不处理

                } else {
                    EcDeliveryHead li = ecDeliveryHeadList.get(i);
                    li.setStatus("301");
                    li.insert(connection);
                    try {
                        Client.getInstance().processCommand("putEcDeliveryHead " +li.getOrderNumber()+","+li.getPosNo()+","+li.getTransactionNumber()+","+li.getPayId());
                    } catch (ClientCommandException e) {
                        e.printStackTrace();
                    }
                    CreamToolkit.logMessage("EcDeliveryHead(" + tranNumber + ") transactionNumber="
                            + li.getTransactionNumber() + ", amt=" + li.getAmount() + " is stored.");
                }
            }
            for (int i =0; i < selfbuyDetailList.size(); i++) {
                Selfbuy_detail li = selfbuyDetailList.get(i);
                li.insert(connection);
                CreamToolkit.logMessage("Selfbuy_detail(" + tranNumber + ") tmtranseq="
                        + li.getTmtranseq() + ", amt=" + li.getPrice() + " is stored.");
            }
            for (int i = 0; i < cmpayList.size(); i++) {
                Cmpay_detail li = cmpayList.get(i);
                li.insert(connection);
                CreamToolkit.logMessage("Cmpay_detail(" + tranNumber + ") orderId="
                        + li.getOrderId() + ", amt = " + li.getAmt() + " is stored.");
            }
            // Store transaction...
            len = getLineItems().length;
            for (int i = 0; i < len; i++) {
                LineItem li = (LineItem) lineItems.get(i);
                System.out.println("LineItem(" + tranNumber + ") seq="
                        + li.getLineItemSequence() + ", plu="
                        + li.getPluNumber() + ", qty=" + li.getQuantity()
                        + ", amt=" + li.getAmount() + " is stored.");
                li.insert(connection);
                CreamToolkit.logMessage("LineItem(" + tranNumber + ") seq="
                        + li.getLineItemSequence() + ", plu="
                        + li.getPluNumber() + ", qty=" + li.getQuantity()
                        + ", amt=" + li.getAmount() + " is stored.");
                    // 移到tran_Processing,确保daishousales2也能重算
//                  try {
//                      DaiShouSales2 dss = writeDaiShouSales2(li, false);
//                      if (dss != null) {
//                          daiShouSalesCommitted.add(dss);
//                      }
//                  } catch (SQLException e1) {
//                      rollBack();
//                      return;
//                  }
//              } else {
//                  POSTerminalApplication.getInstance().getWarningIndicator()
//                          .setMessage(
//                                  CreamToolkit.GetResource().getString(
//                                          "InsertFailed"));
//                  for (int j = 0; j < 10; j++)
//                      Toolkit.getDefaultToolkit().beep();
//                  CreamToolkit.logMessage("Err> LineItem(" + tranNumber
//                          + ") seq=" + li.getLineItemSequence() + ", plu="
//                          + li.getPluNumber() + ", qty=" + li.getQuantity()
//                          + ", qmt=" + li.getAmount() + " insert failed.");
//                  rollBack();
//                  return;
//              }
            }
        case TRANSACTION_SAVE_TRAN: // transaction已保存 TRANSACTION_SAVE_TRAN
            this.step = TRANSACTION_SAVE_TRAN;
            saveTo(TRANSACTION_SAVE_TRAN);
            insert(connection, false);
            if(DBToolkit.savedToDB) {
            // workcheck(Attendance)
                    String type = getDealType2();
                    if (type.equalsIgnoreCase("B") || type.equalsIgnoreCase("C")) {
                        try {
                            Attendance1 att = new Attendance1();
                            att.setZnumber(getZSequenceNumber());
                            att.setStoreID(getStoreNumber());
                            att.setDayDate(new java.util.Date());
                            att.setEmployeeID(getEmployeeNumber());

                            if (type.equals("B")) {
                                att.setBeginTime(getSystemDateTime());
                                att.setAttType("01");
                            }
                            else if (type.equals("C")) {
                                att.setDayDate(getDayDateForAtt());
                                att.setBeginTime(getBeginTimeForAtt());
                                att.setEndTime(getSystemDateTime());
                                att.setAttType("02");
                            }
                            att.setPosNumber(getTerminalNumber());
                            att.setCreateUserID(getCashierNumber());
                            att.setCreateDate(new java.util.Date());
//                            if (!att.exists(connection))
					             att.insert(connection);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
            }
            CreamToolkit.logMessage(toString() + " is stored.");

        case TRANSACTION_SAVE_TRANEND: // dep已保存
        case TRANSACTION_SAVE_Z:
        case TRANSACTION_SAVE_DEPSALES:
        case TRANSACTION_SAVE_DAISHOUSALES:
        case TRANSACTION_SAVE_DAISHOUSALES2:
        case TRANSACTION_SAVE_SHIFT2:
        case TRANSACTION_SAVE_Z2:
        case TRANSACTION_SAVE_SHIFT3:
        case TRANSACTION_SAVE_Z3:
        case TRANSACTION_SAVE_SHIFT4:
        case TRANSACTION_SAVE_Z4:
        case TRANSACTION_SAVE_END:
            // Update shift and z TRANSACTION_SAVE_TRANEND
            this.step = TRANSACTION_SAVE_TRANEND;
            saveTo(TRANSACTION_SAVE_TRANEND);
            CreamToolkit.logMessage("Updating Shift/Z(" + tranNumber + ")...");
            calcHardTotals(connection, step2);
            CreamToolkit.logMessage("Updating Shift/Z(" + tranNumber + ") OK!");
            CreamToolkit.writeTransactionLog("# OK");

            InlineCommandExecutor.getInstance().execute("putTransaction " + tranNumber);

            if (getDealType2().equals("9")) {
                //ShiftReport shift = ShiftReport.getCurrentShift(connection);
                // try {
                // Client.getInstance().processCommand(
                // "putShift " + shift.getZSequenceNumber() + " " +
                // shift.getSequenceNumber());
                // shift.setUploadState("1");
                // } catch (ClientCommandException e) {
                // e.printStackTrace(CreamToolkit.getLogger());
                // shift.setUploadState("2");
                // }
                // if (!POSTerminalApplication.ischeck) {
                // shift.update();
                // }
//                Cargo.getInstance().putWaitUploadJob(
//                        "putShift " + shift.getZSequenceNumber() + " "addadd
//                        + shift.getSequenceNumber());

                PARAM.updateShiftNumber(PARAM.getShiftNumber() + 1);
                PARAM.updateCashierNumber("");
                POSTerminalApplication.getInstance().setCurrentCashierNumber("");

            } else if (getDealType2().equals("D")) {

                int currentZ = ZReport.getCurrentZNumber(connection);
                PARAM.updateZNumber(currentZ + 1);
                PARAM.updateShiftNumber(0);

                // Upload daishousales 代售
                // boolean success = false;
                // Iterator itrDepSales =
                // DepSales.queryBySequenceNumber(this.getZSequenceNumber());

                // 如果要重算z，则putz放在重算后(ZState中)
                if (!PARAM.isRecalcAfterZ()) {
                    ZReport z = ZReport.getOrCreateCurrentZReport(connection);
                    System.out.println("Transaction: before get Cargo putZ z.getAccountingDate = " + z.getAccountingDate());
//                  Cargo.getInstance().putWaitUploadJob(
//                      "putZ " + z.getSequenceNumber());
                }
            }
            
            saveTo(TRANSACTION_SAVE_END);
            
            CreamToolkit.logMessage("TransactionEnd and store method end!");
            // 暂时不用该功能
            // if (!backUp())
            // CreamToolkit.getLogger().println("Error occured when back up
            // database");
        }
    }

    private void writeTransactionLog(DbConnection connection) {
        // Bruce/20030716/
        // Write transaction log
        // 这里记录了应该写进的tranhead, trandetail, daishousales2记录
        int len = (int) getLineItems().length;
        for (int i = 0; i < len; i++) {
            LineItem li = (LineItem) lineItems.get(i);
            CreamToolkit.writeTransactionLog(li.getInsertSqlStatement(true));
            try {
                writeDaiShouSales2(connection, li, true);
            } catch (SQLException e1) {
            }
        }
        CreamToolkit.writeTransactionLog(this.getInsertSqlStatement(true));
    }

    private boolean integrityCheck() {
        boolean ret = true;
        // Bruce/20031030/
        // 检查是否有奇怪的情况……
        int len = (int) getLineItems().length;
        LineItem lastLineItem = null;
        for (int i = 0; i < len; i++) {
            LineItem li = (LineItem) lineItems.get(i);

            // Bruce/20030719
            // Void line item integrity check
            // 因为在灿坤不知何因有时候更正的记录会被篡改
            if (li.getDetailCode().equals("E") && lastLineItem != null) {
                if (!li.getUnitPrice().equals(lastLineItem.getUnitPrice())
                        || !li.getQuantity().equals(
                                lastLineItem.getQuantity().negate())
                        || !li.getAmount().equals(
                                lastLineItem.getAmount().negate())) {
                    lastLineItem.setUnitPrice(li.getUnitPrice());
                    lastLineItem.setQuantity(li.getQuantity().negate());
                    lastLineItem.setAmount(li.getAmount().negate());
                    lastLineItem.setAfterDiscountAmount(li
                            .getAfterDiscountAmount().negate());
                    lastLineItem.setOriginalPrice(li.getOriginalPrice());
                    CreamToolkit.logMessage("LineItem Adjust> "
                            + lastLineItem.toString());
                    System.out.println("LineItem Adjust> "
                            + lastLineItem.toString());
                    ret = false;
                }
            }
            lastLineItem = li;
        }
        // Bruce/20030719
        // 因为在灿坤不知何因有时候PAYAMT1和CHANGEAMT会变成很大的值
        if (getPayAmount1() != null
                && (getPayAmount1().compareTo(new HYIDouble(999999999.99)) >= 0 || getPayAmount1()
                        .compareTo(new HYIDouble(-999999999.99)) <= 0)) {
            setPayAmount1(getNetSalesAmount());
            setChangeAmount(nHYIDouble0());
            CreamToolkit.logMessage("Transaction Adjust> " + this.toString());
            System.out.println("Transaction Adjust> " + this.toString());
            ret = false;
        }
        return ret;
    }

//  // 同步备份数据库
//  private boolean backUp() {
//      boolean windows = (System.getProperties().get("os.name").toString()
//              .indexOf("Windows") != -1);
//      if (windows)
//          return windows;
//
//      return Trigger.getInstance().backUp();
//
//  }

    private DaiShouSales2 writeDaiShouSales2(DbConnection connection, LineItem lineItem,
            boolean writeTranLog) throws SQLException {
        DaiShouSales2 dss = null;

        // ZhaoHong /2003-06-25 代收公共事业费
        if (lineItem.getDetailCode().equals("O") && !lineItem.getRemoved()) {
            dss = new DaiShouSales2();

            boolean isNormalTran = getVoidTransactionNumber() == null;
            boolean isReturnTran = "0".equals(getDealType1())
                    && "3".equals(getDealType2())
                    && "4".equals(getDealType3());

            boolean isReturnNew = "0".equals(getDealType1())
                    && "0".equals(getDealType2())
                    && "4".equals(getDealType3());

            // 重印、交易取消、前笔误打
            if (!isNormalTran && !isReturnTran) {

                int voidTranNo = getVoidTransactionNumber() / 100;
                int lineSeqNo = lineItem.getLineItemSequence();
                dss = DaiShouSales2.queryDaiShouSales(connection, voidTranNo, lineSeqNo);
                if (!writeTranLog)
                    dss.deleteByPrimaryKey(connection);
                else
                    CreamToolkit.writeTransactionLog(dss.getDeleteSqlStatement(true));

                return dss;
            }

            // 部分退货后产生的新的交易
            if (isReturnNew) {
                int lastTranNo = getTransactionNumber() - 1;
                String barcode = lineItem.getDaiShouBarcode();

                dss = DaiShouSales2.queryDaiShouSales(connection, lastTranNo, barcode);
                if (dss != null) {
                    if (!writeTranLog)
                        dss.deleteByPrimaryKey(connection);
                    else
                        CreamToolkit.writeTransactionLog(dss.getDeleteSqlStatement(true));
                }
                Transaction lastTran = Transaction.queryByTransactionNumber(connection, getTerminalNumber(),
                    lastTranNo);
                Transaction voidTran = Transaction.queryByTransactionNumber(connection, getTerminalNumber(),
                    lastTran.getVoidTransactionNumber() / 100); 

                if (!voidTran.getZSequenceNumber().equals(getZSequenceNumber()))
                    return dss;
            }

            if (isReturnTran && getVoidTransactionNumber() != null) {
                int voidTranNo = getVoidTransactionNumber() / 100;
                int lineSeqNo = lineItem.getLineItemSequence();
                DaiShouSales2 tmpD = DaiShouSales2.queryDaiShouSales(connection,
                        voidTranNo, lineSeqNo);
                if (tmpD != null && tmpD.getZSequenceNumber() == getZSequenceNumber()) {
                    if (!writeTranLog)
                        tmpD.deleteByPrimaryKey(connection);
                    else
                        CreamToolkit.writeTransactionLog(tmpD.getDeleteSqlStatement(true));
                }
            }

            if (dss == null)
                return null;
            
            dss.setStoreID(lineItem.getStoreID());
            // dss.setPosNumber(Integer.parseInt(getTerminalPhysicalNumber()));
            dss.setPosNumber(lineItem.getTerminalNumber());
            dss.setTransactionNumber(lineItem.getTransactionNumber());
            dss.setLineItemSeq(lineItem.getLineItemSequence());
            // dss.setID(li.getDiscountNumber());
            dss.setID(lineItem.getPluNumber());
            dss.setZSequenceNumber(getZSequenceNumber());
            dss.setAccountDate(new java.util.Date());
            dss.setPayTime(new java.util.Date());
            dss.setBarcode(lineItem.getDiscountNumber());
            dss.setPosAmount(lineItem.getAmount());
            dss.setScAmount(lineItem.getAmount());
            if (!writeTranLog) {
                dss.insert(connection);
                CreamToolkit.logMessage("DaiShouSales2 inserted ("
                        + lineItem.getTransactionNumber() + ") seq="
                        + lineItem.getLineItemSequence() + ", plu="
                        + lineItem.getPluNumber() + ", barcode="
                        + lineItem.getDaiShouBarcode() + ", amt="
                        + lineItem.getAmount() + " is stored.");
            } else {
                CreamToolkit.writeTransactionLog(dss.getInsertSqlStatement(true));
            }
        }
        return dss;
    }

    /**
     * Payment related properties. 2003-9-10
     * 去掉synchronized，因为在CreamPrinter_zh_CN.printPayment()里面会去call
     * 这个method，但是因为有可能另外一个thread正在synchronized Transaction.store()当中， 把current
     * transaction object锁住，这样必需等待store()结束打印才会继续，这样感觉速度很慢。
     */
    /* synchronized */public Iterator getPayments() {
        return paymentArray.iterator();
    }

    public int getPaymentCount() {
        return paymentArray == null ? 0 : paymentArray.size();
    }

    /**
     * if payment is null, then payment arraylist is clear if payment is not
     * exist in payment arraylist and payment arraylist size less than 4, then
     * payment arraylist add the payment, return 2 if payment is exist in
     * payment arraylist, then return 1 if payment is not exist in payment
     * arraylist and payment arraylist size more than 4, then return 0
     * 目前只支持一种信用卡支付 如果发现已有信用卡支付，则覆盖原来的，并return 3
     */
    synchronized public int addPayment(Payment payment) {
        if (payment == null) {
            paymentArray.clear();
            return 0;
        }
        boolean had = false;
        Payment creditCardPayment = Payment.queryCreditCardPayment();
        for (int i = 0; i < paymentArray.size(); i++) {
            Payment currPayment = paymentArray.get(i);
            if (currPayment.equals(creditCardPayment) && payment.equals(creditCardPayment)) {
                paymentArray.set(i, payment);
                return 3;
            }
            if (paymentArray.get(i).equals(payment)) {
                had = true;
                return 1;
            }
        }

        if (!had && paymentArray.size() < 4) {
            paymentArray.add(payment);
            return 2;
        } else {
            return 0;
        }
    }

    synchronized public void removePayment(Payment payment) {
        if (payment == null) {
            return ;
        }
        paymentArray.remove(payment);
        if (paymentArray.size() > 0)
            setLastestPayment((Payment) paymentArray.get(paymentArray.size() - 1));
        else
            lastestPayment = null;
    }

    // payment used at last in a transaction
    public void setLastestPayment(Payment lastestPayment) {
        this.lastestPayment = lastestPayment;
    }

    public Payment getLastestPayment() {
        return lastestPayment;
    }

    public HYIDouble getPayAmountByID(String id) {
        return getPayAmountByID(Integer.parseInt(id));
    }

    public HYIDouble getPayAmountByID(int id) {
        switch (id) {
        case 1:
            return getPayAmount1();
        case 2:
            return getPayAmount2();
        case 3:
            return getPayAmount3();
        case 4:
            return getPayAmount4();
        default:
            return null;
        }
    }

    public void setPayAmountByID(int id, HYIDouble amount) {
        switch (id) {
        case 1:
            setPayAmount1(amount);
            break;
        case 2:
            setPayAmount2(amount);
            break;
        case 3:
            setPayAmount3(amount);
            break;
        case 4:
            setPayAmount4(amount);
            break;
        }
    }

    public String getPayNumberByID(int id) {
        switch (id) {
        case 1:
            return getPayNumber1();
        case 2:
            return getPayNumber2();
        case 3:
            return getPayNumber3();
        case 4:
            return getPayNumber4();
        default:
            return "";
        }
    }

    public void setPayNumberByID(int id, String payId) {
        switch (id) {
        case 1:
            setPayNumber1(payId);
            break;
        case 2:
            setPayNumber2(payId);
            break;
        case 3:
            setPayNumber3(payId);
            break;
        case 4:
            setPayNumber4(payId);
            break;
        }
    }

    public String getPayNameByID(int id) {
        switch (id) {
        case 1:
            return getPayName1();
        case 2:
            return getPayName2();
        case 3:
            return getPayName3();
        case 4:
            return getPayName4();
        default:
            return "";
        }
    }

    public Payment getPaymentByID(int id) {
        switch (id) {
        case 1:
            return Payment.queryByPaymentID(getPayNumber1());
        case 2:
            return Payment.queryByPaymentID(getPayNumber2());
        case 3:
            return Payment.queryByPaymentID(getPayNumber3());
        case 4:
            return Payment.queryByPaymentID(getPayNumber4());
        default:
            return null;
        }
    }

    public void setPayAmountByID(String id, HYIDouble newValue) {
        int inner = Integer.parseInt(id);
        switch (inner) {
        case 1:
            setPayAmount1(newValue);
            break;
        case 2:
            setPayAmount2(newValue);
            break;
        case 3:
            setPayAmount3(newValue);
            break;
        case 4:
            setPayAmount4(newValue);
            break;
        }
    }

    /**
     * 按支付優先序返回一個TreeMap of payment and pay amount.
     */
    public Map<Payment, HYIDouble> getPaymentMap() {
        Map<Payment, HYIDouble> paymentMap = new TreeMap<Payment, HYIDouble>(new PaymentComparator());
        for (int idx = 1; idx <= 4; idx++) {
            Payment payment = getPaymentByID(idx);
            HYIDouble amount = getPayAmountByID(idx);
            if (payment != null && amount != null && !amount.equals(HYIDouble.zero()))
                paymentMap.put(payment, amount);
            else
                break;
        }
        return paymentMap;
    }

    public void addItemDiscount(HYIDouble itemDiscount) {
        this.itemDiscount = this.itemDiscount.addMe(itemDiscount);
    }

    public HYIDouble getItemDiscount() {
        return this.itemDiscount;
    }

    public void addTransactionListener(TransactionListener listener) {
        if (transactionListener == null)
            transactionListener = new ArrayList();
        transactionListener.add(listener);
    }

    public List getTransactionListener() {
        return transactionListener;
    }

    public void setTransactionListener(List transactionListener) {
        this.transactionListener = transactionListener;
    }

    /**
     * Fire event to every transactionListener.
     */
    public void fireEvent(TransactionEvent e) {
        if (transactionListener == null)
            return;

        Iterator iter = transactionListener.iterator();

        while (iter.hasNext()) {
            TransactionListener l = (TransactionListener) iter.next();
            l.transactionChanged(e);
        }
    }

    // Bruce/2002-02-01
    public void addLineItemSimpleVersion(LineItem lineItem) {
        lineItems.add(lineItem);
    }
    public void addAlipayList(Alipay_detail detail) {
        alipayList.add(detail);
    }
    public List<Alipay_detail> getAlipayList() {
        return alipayList;
    }
    public void addWeiXinList(WeiXin_detail detail) {
        weiXinList.add(detail);
    }
    public List<WeiXin_detail> getWeiXinList() {
        return weiXinList;
    }
    public void addUnionPayList(UnionPay_detail detail) {
        unionpayList.add(detail);
    }
    public List<UnionPay_detail> getUnionPayList() {
        return unionpayList;
    }
    public void addSelfbuyHeadList(Selfbuy_head detail) {
        selfbuyHeadList.add(detail);
    }
    public List<Selfbuy_head> getSelfbuyHeadList() {
        return selfbuyHeadList;
    }
    public void addEcDeliveryHeadList(EcDeliveryHead detail) {
        ecDeliveryHeadList.add(detail);
    }
    public List<EcDeliveryHead> getEcDeliveryHeadList() {
        return ecDeliveryHeadList;
    }
    public void addSelfbuyDetailList(Selfbuy_detail detail) {
        selfbuyDetailList.add(detail);
    }
    public List<Selfbuy_detail> getSelfbuyDetailList() {
        return selfbuyDetailList;
    }

    public void addCmPayList(Cmpay_detail detail) {
        cmpayList.add(detail);
    }

    public List<Cmpay_detail> getCmpayList() {
        return cmpayList;
    }

    public void addLineItem(LineItem lineItem) throws TooManyLineItemsException {
        addLineItem(lineItem, true);
    }
    
    public void addLineItem(LineItem lineItem, boolean isLimitMaxLineItems) throws TooManyLineItemsException {
        if (lineItem == null)
            return;

        if (isLimitMaxLineItems && getDisplayedLineItemsArray().size() >= maxLineItems) {
            if (!lineItem.getDetailCode().equals("E")
                && !lineItem.getDetailCode().equals("V")
                && !lineItem.getDetailCode().equals("M")
                && !lineItem.getDetailCode().equals("D")) {
                throw new TooManyLineItemsException();
            }
        }
        lineItems.add(lineItem);
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        if(!app.getSelfBuy().equals("S")){
            lineItem.setLineItemSequence(lineItems.size());
        }
        currentLineItem = lineItem;

        fireEvent(new TransactionEvent(this, TransactionEvent.ITEM_ADDED));

//        // currentLineItem is buffer lineItem
//        if (currentLineItem != null) {
//            lineItems.add(currentLineItem);
//        }
//        currentLineItem = lineItem;
//        // ZhaoHong 2003-07-21
//        if (currentLineItem != null) {
//            currentLineItem.setLineItemSequence(lineItems.size() + 1);
//            fireEvent(new TransactionEvent(this, TransactionEvent.ITEM_ADDED));
//        }
    }

    public void removeLineItem(LineItem lineItem) throws LineItemNotFoundException {
        if (lineItem == currentLineItem)
            currentLineItem = null;

        if (!lineItems.remove(lineItem))
            throw new LineItemNotFoundException();

        fireEvent(new TransactionEvent(this, TransactionEvent.ITEM_REMOVED));
    }

    public void removeSILineItem() {
        List<LineItem> toBeRemoved = new ArrayList<LineItem>();
        LineItem lastItem = null;
        for (LineItem item : lineItems) {
            if (item.getDetailCode().equals("D"))  {
                SI si = SI.queryBySIID(item.getPluNumber());
                if (!si.isAfterSI())
                    continue;
                toBeRemoved.add(item);
            } else {
                lastItem = item;
                if (item.getSiAmount() != null)
                    item.setOriginalAfterDiscountAmount(item.getAfterDiscountAmount());
            }
        }
        currentLineItem = lastItem;
        if (!toBeRemoved.isEmpty()) {
            lineItems.removeAll(toBeRemoved);
            fireEvent(new TransactionEvent(this, TransactionEvent.ITEM_REMOVED));
        }

//        if (!getLockEnable()) {
//            return;
//        }
//        // boolean hasSI = false;
//
//        // ZhaoHong 2003-07-01
//        List<LineItem> lineItems = (List<LineItem>)lineItems.clone();
//
//        // if (!getAppliedSIs().isEmpty())
//        //    hasSI = true;
//        getAppliedSIs().clear();
//        for (Iterator it = lineItems.iterator(); it.hasNext();) {
//            LineItem item = (LineItem) it.next();
//            if (item.getSiAmount() != null) {
//                item.setOriginalAfterDiscountAmount(item.getAfterDiscountAmount());
//            }
//        }
//
//        for (int i = lineItems.size() - 1; i >= 0; i--) {
//            lineItem = (LineItem) lineItems.get(i);
//            if (lineItem.getDetailCode().equals("D"))  {
//                SI si = SI.queryBySIID(lineItem.getPluNumber());
//                if (!si.isAfterSI())
//                    continue;
//                lineItemArrayLast.remove(i);
//                if (currentLineItem != null && currentLineItem.getDetailCode().equals("D")) {
//                    //currentLineItem = null;
//                    currentLineItem = (LineItem)lineItemArrayLast.get(i - 1);
//                }
//                if (lineItems.size() >= i - 1)
//                    lineItems.remove(i -1);
//                fireEvent(new TransactionEvent(this,
//                        TransactionEvent.ITEM_REMOVED));
//            }
//        }
        
        setSIPercentOffAmount0(nHYIDouble0());
        setSIPercentOffAmount1(nHYIDouble0());
        setSIPercentOffAmount2(nHYIDouble0());
        setSIPercentOffAmount3(nHYIDouble0());
        setSIPercentOffAmount4(nHYIDouble0());
        setSIPercentOffAmount5(nHYIDouble0());
        setSIPercentOffAmount6(nHYIDouble0());
        setSIPercentOffAmount7(nHYIDouble0());
        setSIPercentOffAmount8(nHYIDouble0());
        setSIPercentOffAmount9(nHYIDouble0());
        setSIPercentOffAmount10(nHYIDouble0());
        setSIPercentOffCount0(0);
        setSIPercentOffCount1(0);
        setSIPercentOffCount2(0);
        setSIPercentOffCount3(0);
        setSIPercentOffCount4(0);
        setSIPercentOffCount5(0);
        setSIPercentOffCount6(0);
        setSIPercentOffCount7(0);
        setSIPercentOffCount8(0);
        setSIPercentOffCount9(0);
        setSIPercentOffCount10(0);
        //updateSIAmt(new HYIDouble(0));
    }

    public void removeTmpLineItem(LineItem li) {
        if (li == currentLineItem)
            currentLineItem = null;

        lineItems.remove(li);
        //lineItemArrayLast.remove(li);
        fireEvent(new TransactionEvent(this, TransactionEvent.ITEM_REMOVED));
    }

    // public void removeLineItem(int sequenceNumber) throws
    // LineItemNotFoundException {
    // if (!lockEnable) {
    // return;
    // }
    // setLockEnable(false);
    // try {
    // lineItems.remove(sequenceNumber);
    // } catch (IndexOutOfBoundsException e) {
    // e.printStackTrace(CreamToolkit.getLogger());
    // CreamToolkit.logMessage(e.toString());
    // CreamToolkit.logMessage("Index out of bounds ");
    // CreamToolkit.logMessage("LineItem not found");
    // }
    //
    // //fireEvent(new TransactionEvent(this, TransactionEvent.ITEM_REMOVED,
    // sequenceNumber));
    // fireEvent(new TransactionEvent(this, TransactionEvent.ITEM_REMOVED));
    // }

    public void changeLineItem(int sequenceNumber, LineItem lineItem)
            throws LineItemNotFoundException {
        try {
            if (sequenceNumber == -1) {
                currentLineItem = lineItem;
            } else {
//              lineItemArrayLast.set(sequenceNumber, lineItem);
                lineItems.set(sequenceNumber, lineItem);
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            CreamToolkit.logMessage("Index out of bounds. LineItem not found");
            CreamToolkit.logMessage(lineItem.toString());
        }

        // fireEvent(new TransactionEvent(this, TransactionEvent.ITEM_CHANGED,
        // lineItem.getLineItemSequence()));
        fireEvent(new TransactionEvent(this, TransactionEvent.ITEM_CHANGED));
    }

    public void clearLineItem() {
/*
        Object[] objs = this.getLineItemsWithoutCurrentOne();
        for (int i = 0; i < objs.length; i++) {
            try {
                this.removeLineItem((LineItem) objs[i]);
            } catch (LineItemNotFoundException e) {
            }
        }
*/
        lineItems.clear();
        currentLineItem = null;

        fireEvent(new TransactionEvent(this, TransactionEvent.TRANS_RENEW));
        // CreamToolkit.logMessage("" + "clear lineitem");
    }

    public LineItem getLineItem(int no) {
        return (LineItem) getLineItems()[no - 1];
    }

//    public Transaction getCurrentTransaction() {
//        return currentTransaction;
//    }
    
    /**
     * currentLineItem is buffer lineItem
     * curTran.getCurrentLineItem() == null 目前作为交易是否结束的标志
     * @return
     */
    public LineItem getCurrentLineItem() {
        return currentLineItem;
    }

    // get lineItem's iterator in current transaction expect current lineItem
    public Iterator getLineItemsIterator() {
        return lineItems.iterator();
    }

//    public Iterator getLineItemsSimpleVersion() {
//        return lineItems.iterator();
//    }

    /**
     * Get line items (maybe without the current one).
     */
    public Object[] getLineItemsWithoutCurrentOne() {
        if (currentLineItem == null)
            return lineItems.toArray();
        else {
            List<LineItem> x = new ArrayList<LineItem>(lineItems);
            x.remove(currentLineItem);
            return x.toArray();
        }
    }

    public Object[] getNormalLineItemsArray() {
        Object[] pureArrayList = getLineItems();
        ArrayList purelineItemArrayList = new ArrayList();
        for (int i = 0; i < pureArrayList.length; i++) {
            LineItem lineItem = (LineItem) pureArrayList[i];
            if (lineItem.getDetailCode().equals("S")
                    || lineItem.getDetailCode().equals("R")) {
                if (!lineItem.getRemoved())
                    purelineItemArrayList.add(lineItem);
            }
        }
        return purelineItemArrayList.toArray();
    }

    /** 按銷售金額由小到大排序. */
    public Object[] getLineItemsSortedByAmount() {
        Object[] sortedItems = getNormalLineItemsArray();
        Arrays.sort(sortedItems, new Comparator() {
            public int compare(Object o1, Object o2) {
                LineItem item1 = (LineItem)o1;
                LineItem item2 = (LineItem)o2;
                return item1.getAmount().compareTo(item2.getAmount());
            }
        });
        return sortedItems;
    }

    public List<LineItem> lineItems() {
        return lineItems;
    }

    /*
     * Get line items including the current one.
     */
    public Object[] getLineItems() {
        return lineItems.toArray();
//        lineItemArrayLast = new ArrayList();
//        Iterator itr = lineItems.iterator();
//        while (itr.hasNext())
//            lineItemArrayLast.add(itr.next());
//        /*
//         * for (int i = 0; i < ; i++) { lineItemArrayLast.add(i,
//         * lineItems.get(i)); }
//         */
//
//        if (currentLineItem != null) {
//            lineItemArrayLast.add(currentLineItem);
//        }
//        return lineItemArrayLast.toArray();
    }

    public void addTaxMM(int count, HYIDouble amount) {
        mmCount = mmCount + count;
        mmAmount = mmAmount.addMe(amount);
        this.totalMMAmount = mmAmount;
        this.fireEvent(new TransactionEvent(this,
                TransactionEvent.TRANS_INFO_CHANGED));
    }

    public HYIDouble getTaxMMAmount() {
        return mmAmount.setScale(2, 4);
    }

    public void setTaxMMAmount(HYIDouble amount) {
        mmAmount = amount;
        this.totalMMAmount = mmAmount;
    }

    public int getTaxMMCount() {
        return mmCount;
    }

    public void setTaxMMCount(int count) {
        mmCount = count;
    }

    /*
     * public Hashtable getTaxMMAmountHashtable() { return taxMMAmount; } public
     * Hashtable getTaxMMCountHashtable() { return taxMMCount; }
     */

    public void updateTaxMM() {
        // CreamToolkit.logMessage("" + " *** updata mm ***");
        setMixAndMatchAmount0(nHYIDouble0().setScale(2, BigDecimal.ROUND_HALF_UP));
        setMixAndMatchAmount1(nHYIDouble0().setScale(2, BigDecimal.ROUND_HALF_UP));
        setMixAndMatchAmount2(nHYIDouble0().setScale(2, BigDecimal.ROUND_HALF_UP));
        setMixAndMatchAmount3(nHYIDouble0().setScale(2, BigDecimal.ROUND_HALF_UP));
        setMixAndMatchAmount4(nHYIDouble0().setScale(2, BigDecimal.ROUND_HALF_UP));
        setMixAndMatchAmount5(nHYIDouble0().setScale(2, BigDecimal.ROUND_HALF_UP));
        setMixAndMatchAmount6(nHYIDouble0().setScale(2, BigDecimal.ROUND_HALF_UP));
        setMixAndMatchAmount7(nHYIDouble0().setScale(2, BigDecimal.ROUND_HALF_UP));
        setMixAndMatchAmount8(nHYIDouble0().setScale(2, BigDecimal.ROUND_HALF_UP));
        setMixAndMatchAmount9(nHYIDouble0().setScale(2, BigDecimal.ROUND_HALF_UP));
        setMixAndMatchAmount10(nHYIDouble0().setScale(2, BigDecimal.ROUND_HALF_UP));

        // this.setMixAndMatchAmount0(getTaxMMAmount());
        this.setMixAndMatchCount0(getTaxMMCount());
        // CreamToolkit.logMessage("" + "***********************1" +
        // getTaxMMCount());

        Iterator iter = (taxMMAmount.keySet()).iterator();
        String key = "";
        // set to M&M amount
        String fieldName = "";
        HYIDouble fieldValue = null;
        while (iter.hasNext()) {
            key = (String) iter.next();
            fieldName = "MNMAMT" + key;
            fieldValue = (HYIDouble) taxMMAmount.get(key);
            setFieldValue(fieldName, fieldValue.setScale(2,
                    BigDecimal.ROUND_HALF_UP));
        }

        // set to M&M count
        /*
         * iter = (taxMMAmount.keySet()).iterator(); String fieldName2 = "";
         * Integer fieldValue2 = null; while (iter.hasNext()) { key =
         * (String)iter.next(); fieldName2 = "MNMCNT" + key; fieldValue2 =
         * (Integer)taxMMCount.get(key); setFieldValue(fieldName2, fieldValue2); }
         */
    }

    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_tranhead";
        else
            return "tranhead";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_tranhead";
        else
            return "tranhead";
    }

    /**
     * table's properties
     */

    // 表头
    // StoreNumber
    public String getStoreNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("storeID");
        else
            return (String) getFieldValue("STORENO");
    }

    public String getStoreName() {
        return Store.getStoreChineseName();
    }

    public void setStoreNumber(String storecode) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("storeID", storecode);
        else
            setFieldValue("STORENO", storecode);
    }

    // SystemDateTime
    public java.util.Date getSystemDateTime() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.util.Date) getFieldValue("systemDate");
        else
            return (java.util.Date) getFieldValue("SYSDATE");
    }

    public void setSystemDateTime(java.util.Date sysdate) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("systemDate", sysdate);
        else
            setFieldValue("SYSDATE", sysdate);
    }

    // TerminalNumber
    public Integer getTerminalNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("posNumber");
        else
            return (Integer) getFieldValue("POSNO");
    }

    public void setTerminalNumber(int tmcode) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("posNumber", new Integer(tmcode));
        else
            setFieldValue("POSNO", new Integer(tmcode));
    }

    // TransactionNumber
    /*
     * public Integer getTransactionNumber() { if
     * (hyi.cream.inline.Server.serverExist()) return
     * (Integer)getFieldValue("transactionNumber"); else { int n =
     * ((Integer)getFieldValue("TMTRANSEQ")).intValue(); if (n < 99999) { return
     * (Integer)getFieldValue("TMTRANSEQ"); } else { String s =
     * String.valueOf(n); return Integer.valueOf(s.substring(s.length() - 5)); } } }
     */

    public Integer getTransactionNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("transactionNumber");
        else
            return (Integer) getFieldValue("TMTRANSEQ");
    }

    /**
     * 打印出的交易序号，如果不需要经过处理，返回TransactionNumber
     * 
     * @return String
     */
    public String getPrintTranNumber() {
        try {
            if (PARAM.isConfuseTranNumber())
                return NumberConfuser.confuse(getTransactionNumber().toString());
            else
                return getTransactionNumber().toString();

        } catch (NullPointerException e) { // for NPE-safe
            return "";
        }
    }

    public void setTransactionNumber(int tmtranseq) {
        // CreamToolkit.logMessage("" + tmtranseq + "|||||||||||||||||||||");
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("transactionNumber", new Integer(tmtranseq));
        else
            setFieldValue("TMTRANSEQ", new Integer(tmtranseq));
    }

    public void setTransactionNumber(Integer tmtranseq) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("transactionNumber", tmtranseq);
        else
            setFieldValue("TMTRANSEQ", tmtranseq);
    }

    // SignOnNumber
    public Integer getSignOnNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("shiftCount");
        else
            return (Integer) getFieldValue("SIGNONID");
    }

    public void setSignOnNumber(int signonid) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("shiftCount", new Integer(signonid));
        else
            setFieldValue("SIGNONID", new Integer(signonid));
    }

    // ZSequenceNumber EODCNT INT UNSIGNED N Z帐序号
    /*
     * public Integer getZSequenceNumber() { return
     * (Integer)getFieldValue("EODCNT"); } public void setZSequenceNumber(int
     * signonid) { setFieldValue("EODCNT", new Integer(signonid)); }
     */

    // AccountingDate
    public java.util.Date getAccountingDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.util.Date) getFieldValue("accountDate");
        else
            return (java.util.Date) getFieldValue("ACCDATE");
    }

    public void setAccountingDate(java.util.Date accdate) {
        // CreamToolkit.logMessage("" + accdate);
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("accountDate", accdate);
        else
            setFieldValue("ACCDATE", accdate);
    }

    /**
     * TransactionType
     * 第一位 0 普通，1批发
     * 第二位 0 正常，1挂单
     * 默认=‘00’
     * 挂单=‘x1’
     * 批发=‘1x’
     */
    public String getTransactionType() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("transactionType");
        else
            return (String) getFieldValue("TRANTYPE");
    }

    public void setTransactionType(String trantype) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("transactionType", trantype);
        else
            setFieldValue("TRANTYPE", trantype);
    }
    
    
    /**
     * 0 普通，1批发
     */
    public String getTranType1() {
        if (getTransactionType() != null && getTransactionType().length() >= 1){
            return getTransactionType().substring(0,1);
        }
        return null;
    }
    
    public void setTranType1(String tranType1) {
        if (getTransactionType() != null && getTransactionType().length() >= 1){
            setTransactionType(tranType1 + getTransactionType().substring(1));
        } else {
            setTransactionType(tranType1 + "0");
        }
    }
    
    /**
     * 0 正常，1挂单
     */
    public String getTranType2() {
        if (getTransactionType() != null && getTransactionType().length() >= 1){
            return getTransactionType().substring(1);
        }
        return null;
    }
    
    public void setTranType2(String tranType2) {
        if (getTransactionType() != null && getTransactionType().length() >= 2){
            setTransactionType(getTransactionType().substring(0,1) + tranType2);
        } else {
            setTransactionType("0" + tranType2);
        }
    }
    
    /**
     * 是否為信用卡分期交易
     */
    public boolean isCreditCardInstallment() {
        return "I".equals(getTranType2());
    }

    /**
     * 是否為信用卡分期交易
     */
    public void setCreditCardInstallment(boolean installment) {
        System.out.println(">>>>>>> setCreditCardInstallment");
        setTranType2(installment ? "I" : "0");
    }

    public String getDisplayTransactionType() {
        // Display transaction type in Itemlist head
        String type = "";
        if (this != null) {
            type = getDealType2();
        }
        if (type == null || type.trim().length() == 0)
            type = "0";

        ResourceBundle res = CreamToolkit.GetResource();
        String displayName = res.getString("td" + type);
        return displayName;
    }

    //考勤系统日和上班考勤时间
    java.util.Date dayDateForAtt = null;
    java.util.Date BeginDateForAtt = null;
    public Date getDayDateForAtt() {
		return dayDateForAtt;
	}
	public void setDayDateForAtt(Date daydate) {
		dayDateForAtt = daydate;

	}
    public void setBeginTimeForAtt(Date begintime) {
		BeginDateForAtt = begintime;
	}
	public Date getBeginTimeForAtt() {
		return  BeginDateForAtt;
	}

    // DealType1
    public String getDealType1() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("dealType1");
        else
            return (String) getFieldValue("DEALTYPE1");
    }

    public void setDealType1(String dealtype1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("dealType1", dealtype1);
        else
            setFieldValue("DEALTYPE1", dealtype1);
    }

    /**
     * @return
     *  ...
     *  Q 团购结算
     *  P 开票单打印
     *  ...
     */
    public String getDealType2() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("dealType2");
        else
            return (String) getFieldValue("DEALTYPE2");
    }

    public void setDealType2(String dealtype2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("dealType2", dealtype2);
        else
            setFieldValue("DEALTYPE2", dealtype2);
        fireEvent(new TransactionEvent(this,
                TransactionEvent.TRANS_INFO_CHANGED));
    }

    // DealType3
    public String getDealType3() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("dealType3");
        else
            return (String) getFieldValue("DEALTYPE3");
    }

    public void setDealType3(String dealtype3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("dealType3", dealtype3);
        else
            setFieldValue("DEALTYPE3", dealtype3);
    }

    // VoidTransactionNumber VOIDSEQ INT UNSIGNED Y 被作废序号
    // allan 2007-03-12 trannumber+posno(2位)
    public Integer getVoidTransactionNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("voidTransactionNumber");
        else
            return (Integer) getFieldValue("VOIDSEQ");
    }

    public void setVoidTransactionNumber(Integer voidTNumber) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("voidTransactionNumber", voidTNumber);
        else
            setFieldValue("VOIDSEQ", voidTNumber);
    }

    // TerminalPhysicalNumber
    public String getTerminalPhysicalNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("machineNumber");
        else
            return (String) getFieldValue("TMCODEP");
    }

    public void setTerminalPhysicalNumber(String tmcodep) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("machineNumber", tmcodep);
        else
            setFieldValue("TMCODEP", tmcodep);
    }

    // CashierNumber
    public String getCashierNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("cashier");
        else
            return (String) getFieldValue("CASHIER");
    }

    public String getCashierName() {
        Cashier cas = Cashier.queryByCashierID(getCashierNumber());
        return (cas == null) ? "" : cas.getCashierName();
    }

    public void setCashierNumber(String cashier) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("cashier", cashier);
        else
            setFieldValue("CASHIER", cashier);
        // fire event for ScreenBanner display
        fireEvent(new TransactionEvent(this,
                TransactionEvent.TRANS_INFO_CHANGED));
    }

    // InvoiceID
    public String getInvoiceID() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("invoiceHead");
        else
            return (String) getFieldValue("INVNOHEAD");
    }

    public void setInvoiceID(String invnohead) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("invoiceHead", invnohead);
        else
            setFieldValue("INVNOHEAD", invnohead);
        fireEvent(new TransactionEvent(this,
                TransactionEvent.TRANS_INFO_CHANGED));
    }

    // InvoiceNumber
    public String getInvoiceNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("invoiceNumber");
        else
            return (String) getFieldValue("INVNO");
    }

    public void setInvoiceNumber(String invno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("invoiceNumber", invno);
        else {
            setFieldValue("INVNO", invno);
            fireEvent(new TransactionEvent(this, TransactionEvent.TRANS_INFO_CHANGED));
        }
    }

    // InvoiceCount
    public Integer getInvoiceCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("invoiceCount");
        else
            return (Integer) getFieldValue("INVCNT");
    }

    public void setInvoiceCount(int invcnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("invoiceCount", new Integer(invcnt));
        else {
            setFieldValue("INVCNT", new Integer(invcnt));
            fireEvent(new TransactionEvent(this, TransactionEvent.TRANS_INFO_CHANGED));
        }
        
    }

    // BuyerNumber
    public String getBuyerNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("idNumber");
        else
            return (String) getFieldValue("IDNO");
    }

    public void setBuyerNumber(String idno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("idNumber", idno);
        else
            setFieldValue("IDNO", idno);
        // fire event for ScreenBanner display
        fireEvent(new TransactionEvent(this,
                TransactionEvent.TRANS_INFO_CHANGED));
    }

    public ArrayList getDisplayedLineItemsArray() {
        Object[] pureArrayList = getLineItems();
        // CreamToolkit.logMessage("" + getLineItems().length + "!" );
        ArrayList purelineItemArrayList = new ArrayList();
        for (int i = 0; i < pureArrayList.length; i++) {
            LineItem lineItem = (LineItem) pureArrayList[i];
            LineItem lineItem2 = null;
            if (i + 1 < pureArrayList.length)
                lineItem2 = (LineItem) pureArrayList[i + 1];
            
            if (lineItem.getRemoved()){
                if (lineItem.getDetailCode().equals("E")
                        || lineItem.getDetailCode().equals("V")) {
                    continue;
                }
                if (lineItem2 != null
                        && lineItem2.getDetailCode().equals("E")) {
                    continue;
                }
            }
            // if (lineItem.getDetailCode().equals("D")) {
            // SI si = SI.queryBySIID(lineItem.getPluNumber());
            // if (si.isAfterSI())
            // continue;
            // }
            if (lineItem.getDetailCode().equals("M")
                    || lineItem.getDetailCode().equals("D")
                    || lineItem.getDetailCode().equals("A")) {
                continue;
            }
            purelineItemArrayList.add(lineItem);
            lineItem.setDisplayedLineItemSequence(purelineItemArrayList.size());
        }
        return purelineItemArrayList;
    }

    // DetailCount
    public Integer getDetailCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("detailCount");
        else
            return (Integer) getFieldValue("DETAILCNT");
    }

    public void setDetailCount(int detailcnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("detailCount", new Integer(detailcnt));
        else
            setFieldValue("DETAILCNT", new Integer(detailcnt));
    }

    // CustomerCount
    public Integer getCustomerCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("customerCount");
        else
            return (Integer) getFieldValue("CUSTCNT");
    }

    public void setCustomerCount(int custcnt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("customerCount", new Integer(custcnt));
        else
            setFieldValue("CUSTCNT", new Integer(custcnt));
    }

    // InOutStore
    public String getInOutStore() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("inOurID");
        else
            return (String) getFieldValue("INOUT");
    }

    public void setInOutStore(Object inout) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("inOurID", inout);
        else
            setFieldValue("INOUT", inout);
    }

    // CustomerAgeLevel
    public Integer getCustomerAgeLevel() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("customerCategory");
        else
            return (Integer) getFieldValue("CUSTID");
    }

    public void setCustomerAgeLevel(int custid) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("customerCategory", new Integer(custid));
        else
            setFieldValue("CUSTID", new Integer(custid));
    }

    public HYIDouble getPositiveSalesAmount() {
        HYIDouble discntAmt = nHYIDouble0();
        discntAmt = getItemDiscountTotal();
        // li.getAfterSIAmount()
        if (hyi.cream.inline.Server.serverExist())
            return ((HYIDouble) getFieldValue("grossSaleTotalAmount"))
                    .subtract(discntAmt);
        else
            return ((HYIDouble) getFieldValue("GROSALAMT")).subtract(discntAmt);
    }

    public HYIDouble getItemDiscountTotal() {
        /*
         * HYIDouble discntAmt = nHYIDouble0(); Iterator itr = getLineItemsIterator();
         * while (itr.hasNext()) { LineItem li = (LineItem)itr.next(); if
         * (li.getDetailCode().equals("T")) discntAmt =
         * discntAmt.add(li.getAmount()); }
         */
        return getItemDiscount().negate();
    }

    // Salesman
    public String getSalesman() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("saleMan");
        else
            return (String) getFieldValue("SALEMAN");
    }

    public void setSalesman(String salesman) {
        if (salesman != null && salesman.length() > 8)
            salesman = salesman.substring(0, 8); // make it safer for db

        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("saleMan", salesman);
        else
            setFieldValue("SALEMAN", salesman);
    }

    // 交易汇总
    public HYIDouble getGrossSalesAmount() {
        HYIDouble b;
        if (hyi.cream.inline.Server.serverExist())
            b = (HYIDouble) getFieldValue("grossSaleTotalAmount");
        else
            b = (HYIDouble) getFieldValue("GROSALAMT");
        if (b != null) {
            return b;
        } else {
            return nHYIDouble0();
        }
    }

    public void setGrossSalesAmount(HYIDouble grosalamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTotalAmount", grosalamt);
        else
            setFieldValue("GROSALAMT", grosalamt);
    }

    public HYIDouble getGrossSalesTax0Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("grossSaleTax0Amount");
        else
            return (HYIDouble) getFieldValue("GROSALTX0AMT");
    }

    public void setGrossSalesTax0Amount(HYIDouble grosaltx0amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax0Amount", grosaltx0amt);
        else
            setFieldValue("GROSALTX0AMT", grosaltx0amt);
    }

    public HYIDouble getGrossSalesTax1Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("grossSaleTax1Amount");
        else
            return (HYIDouble) getFieldValue("GROSALTX1AMT");
    }

    public void setGrossSalesTax1Amount(HYIDouble grosaltx1amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax1Amount", grosaltx1amt);
        else
            setFieldValue("GROSALTX1AMT", grosaltx1amt);
    }

    public HYIDouble getGrossSalesTax2Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("grossSaleTax2Amount");
        else
            return (HYIDouble) getFieldValue("GROSALTX2AMT");
    }

    public void setGrossSalesTax2Amount(HYIDouble grosaltx2amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax2Amount", grosaltx2amt);
        else
            setFieldValue("GROSALTX2AMT", grosaltx2amt);
    }

    public HYIDouble getGrossSalesTax3Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("grossSaleTax3Amount");
        else
            return (HYIDouble) getFieldValue("GROSALTX3AMT");
    }

    public void setGrossSalesTax3Amount(HYIDouble grosaltx3amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax3Amount", grosaltx3amt);
        else
            setFieldValue("GROSALTX3AMT", grosaltx3amt);
    }

    public HYIDouble getGrossSalesTax4Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("grossSaleTax4Amount");
        else
            return (HYIDouble) getFieldValue("GROSALTX4AMT");
    }

    public void setGrossSalesTax4Amount(HYIDouble grosaltx4amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax4Amount", grosaltx4amt);
        else
            setFieldValue("GROSALTX4AMT", grosaltx4amt);
    }
    
    public HYIDouble getGrossSalesTax5Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("grossSaleTax5Amount");
        else
            return (HYIDouble) getFieldValue("GROSALTX5AMT");
    }

    public void setGrossSalesTax5Amount(HYIDouble grosaltx5amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax5Amount", grosaltx5amt);
        else
            setFieldValue("GROSALTX5AMT", grosaltx5amt);
    }

    public HYIDouble getGrossSalesTax6Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("grossSaleTax6Amount");
        else
            return (HYIDouble) getFieldValue("GROSALTX6AMT");
    }

    public void setGrossSalesTax6Amount(HYIDouble grosaltx6amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax6Amount", grosaltx6amt);
        else
            setFieldValue("GROSALTX6AMT", grosaltx6amt);
    }

    public HYIDouble getGrossSalesTax7Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("grossSaleTax7Amount");
        else
            return (HYIDouble) getFieldValue("GROSALTX7AMT");
    }

    public void setGrossSalesTax7Amount(HYIDouble grosaltx7amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax7Amount", grosaltx7amt);
        else
            setFieldValue("GROSALTX7AMT", grosaltx7amt);
    }

    public HYIDouble getGrossSalesTax8Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("grossSaleTax8Amount");
        else
            return (HYIDouble) getFieldValue("GROSALTX8AMT");
    }

    public void setGrossSalesTax8Amount(HYIDouble grosaltx8amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax8Amount", grosaltx8amt);
        else
            setFieldValue("GROSALTX8AMT", grosaltx8amt);
    }

    public HYIDouble getGrossSalesTax9Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("grossSaleTax9Amount");
        else
            return (HYIDouble) getFieldValue("GROSALTX9AMT");
    }

    public void setGrossSalesTax9Amount(HYIDouble grosaltx9amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax9Amount", grosaltx9amt);
        else
            setFieldValue("GROSALTX9AMT", grosaltx9amt);
    }

    public HYIDouble getGrossSalesTax10Amount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("grossSaleTax10Amount");
        else
            return (HYIDouble) getFieldValue("GROSALTX10AMT");
    }

    public void setGrossSalesTax10Amount(HYIDouble grosaltx10amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("grossSaleTax10Amount", grosaltx10amt);
        else
            setFieldValue("GROSALTX10AMT", grosaltx10amt);
    }

    // tax0
    public HYIDouble getSIPercentPlusAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("siPlusTax0Amount");
        else
            return (HYIDouble) getFieldValue("SIPLUSAMT0");
    }

    public void setSIPercentPlusAmount0(HYIDouble siplusamt0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax0Amount", siplusamt0);
        else
            setFieldValue("SIPLUSAMT0", siplusamt0);
    }

    public Integer getSIPercentPlusCount0() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT0");
    }

    public void setSIPercentPlusCount0(int sipluscnt0) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPLUSCNT0", new Integer(sipluscnt0));
        else
            setFieldValue("SIPLUSCNT0", new Integer(sipluscnt0));
    }

    public HYIDouble getSIPercentOffAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountTax0Amount");
        else
            return (HYIDouble) getFieldValue("SIPAMT0");
    }

    public void setSIPercentOffAmount0(HYIDouble sipamt0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax0Amount", sipamt0);
        else
            setFieldValue("SIPAMT0", sipamt0);
    }

    public Integer getSIPercentOffCount0() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0; // (Integer)getFieldValue("SIPCNT0");
        else
            return (Integer) getFieldValue("SIPCNT0");
    }

    public void setSIPercentOffCount0(int sipcnt0) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPCNT0", new Integer(sipcnt0));
        else
            setFieldValue("SIPCNT0", new Integer(sipcnt0));
    }

    public HYIDouble getSIDiscountAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else
            return (HYIDouble) getFieldValue("SIMAMT0");
    }

    public void setSIDiscountAmount0(HYIDouble simamt0) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMAMT0", simamt0);
        else
            setFieldValue("SIMAMT0", simamt0);
    }

    public Integer getSIDiscountCount0() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT0");
    }

    public void setSIDiscountCount0(int simcnt0) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMCNT0", new Integer(simcnt0));
        else
            setFieldValue("SIMCNT0", new Integer(simcnt0));
    }

    public HYIDouble getMixAndMatchAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("mixAndMatchTax0Amount");
        else
            return (HYIDouble) getFieldValue("MNMAMT0");
    }

    public void setTaxMMAmount(Hashtable taxMMAmount) {
        this.taxMMAmount = taxMMAmount;
    }

//    public void setTaxMMCount(Hashtable taxMMCount) {
//        this.taxMMCount = taxMMCount;
//    }

    /*
     * public HYIDouble getTaxMMAmount() { Iterator iter =
     * (taxMMAmount.keySet()).iterator(); HYIDouble amount = nHYIDouble0();
     * while (iter.hasNext()) { String key = (String)iter.next(); HYIDouble
     * mmamount = (HYIDouble)taxMMAmount.get(key); amount =
     * amount.add(mmamount); } return amount; } public Hashtable
     * getTaxMMAmountHashtable() { return taxMMAmount; } public Hashtable
     * getTaxMMCountHashtable() { return taxMMCount; } public void updateTaxMM() {
     * Iterator iter = (taxMMAmount.keySet()).iterator(); String key = ""; //
     * set to M&M amount String fieldName = ""; HYIDouble fieldValue = null;
     * while (iter.hasNext()) { key = (String)iter.next(); fieldName = "MNMAMT" +
     * key; fieldValue = (HYIDouble)taxMMAmount.get(key);
     * setFieldValue(fieldName, fieldValue); } // set to M&M count iter =
     * (taxMMAmount.keySet()).iterator(); String fieldName2 = ""; Integer
     * fieldValue2 = null; while (iter.hasNext()) { key = (String)iter.next();
     * fieldName2 = "MNMCNT" + key; fieldValue2 = (Integer)taxMMCount.get(key);
     * setFieldValue(fieldName2, fieldValue2); } }
     */

    public void setMixAndMatchAmount0(HYIDouble mnmamt0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax0Amount", mnmamt0);
        else
            setFieldValue("MNMAMT0", mnmamt0);
    }

    public Integer getMixAndMatchCount0() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT0");
    }

    public void setMixAndMatchCount0(int mnmcnt0) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("MNMCNT0", new Integer(mnmcnt0));
        else
            setFieldValue("MNMCNT0", new Integer(mnmcnt0));
    }

    public HYIDouble getNotIncludedSales0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("notIncludedTax0Sale");
        else
            return (HYIDouble) getFieldValue("RCPGIFAMT0");
    }

    public void setNotIncludedSales0(HYIDouble rcpgifamt0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax0Sale", rcpgifamt0);
        else
            setFieldValue("RCPGIFAMT0", rcpgifamt0);
    }

    public HYIDouble getNetSalesAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTax0Amount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT0");
    }

    public void setNetSalesAmount0(HYIDouble netsalamt0) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax0Amount", netsalamt0);
        else
            setFieldValue("NETSALAMT0", netsalamt0);
    }

    // tax1
    public HYIDouble getSIPercentPlusAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("siPlusTax1Amount");
        else
            return (HYIDouble) getFieldValue("SIPLUSAMT1");
    }

    public void setSIPercentPlusAmount1(HYIDouble siplusamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax1Amount", siplusamt1);
        else
            setFieldValue("SIPLUSAMT1", siplusamt1);
    }

    public Integer getSIPercentPlusCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT1");
    }

    public void setSIPercentPlusCount1(int sipluscnt1) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPLUSCNT1", new Integer(sipluscnt1));
        else
            setFieldValue("SIPLUSCNT1", new Integer(sipluscnt1));
    }

    public HYIDouble getSIPercentOffAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountTax1Amount");
        else
            return (HYIDouble) getFieldValue("SIPAMT1");
    }

    public void setSIPercentOffAmount1(HYIDouble sipamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax1Amount", sipamt1);
        else
            setFieldValue("SIPAMT1", sipamt1);
    }

    public Integer getSIPercentOffCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT1");
    }

    public void setSIPercentOffCount1(int sipcnt1) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPCNT1", new Integer(sipcnt1));
        else
            setFieldValue("SIPCNT1", new Integer(sipcnt1));
    }

    public HYIDouble getSIDiscountAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else
            return (HYIDouble) getFieldValue("SIMAMT1");
    }

    public void setSIDiscountAmount1(HYIDouble simamt1) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMAMT1", simamt1);
        else
            setFieldValue("SIMAMT1", simamt1);
    }

    public Integer getSIDiscountCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT1");
    }

    public void setSIDiscountCount1(int simcnt1) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMCNT1", new Integer(simcnt1));
        else
            setFieldValue("SIMCNT1", new Integer(simcnt1));
    }

    public HYIDouble getMixAndMatchAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("mixAndMatchTax1Amount");
        else
            return (HYIDouble) getFieldValue("MNMAMT1");
    }

    public void setMixAndMatchAmount1(HYIDouble mnmamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax1Amount", mnmamt1);
        else
            setFieldValue("MNMAMT1", mnmamt1);
    }

    public Integer getMixAndMatchCount1() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT1");
    }

    public void setMixAndMatchCount1(int mnmcnt1) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("MNMCNT1", new Integer(mnmcnt1));
        else
            setFieldValue("MNMCNT1", new Integer(mnmcnt1));
    }

    public HYIDouble getNotIncludedSales1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("notIncludedTax1Sale");
        else
            return (HYIDouble) getFieldValue("RCPGIFAMT1");
    }

    public void setNotIncludedSales1(HYIDouble rcpgifamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax1Sale", rcpgifamt1);
        else
            setFieldValue("RCPGIFAMT1", rcpgifamt1);
    }

    public HYIDouble getNetSalesAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTax1Amount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT1");
    }

    public void setNetSalesAmount1(HYIDouble netsalamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax1Amount", netsalamt1);
        else
            setFieldValue("NETSALAMT1", netsalamt1);
    }

    // tax2
    public HYIDouble getSIPercentPlusAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("siPlusTax2Amount");
        else
            return (HYIDouble) getFieldValue("SIPLUSAMT2");
    }

    public void setSIPercentPlusAmount2(HYIDouble siplusamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax2Amount", siplusamt2);
        else
            setFieldValue("SIPLUSAMT2", siplusamt2);
    }

    public Integer getSIPercentPlusCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT2");
    }

    public void setSIPercentPlusCount2(int sipluscnt2) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPLUSCNT2", new Integer(sipluscnt2));
        else
            setFieldValue("SIPLUSCNT2", new Integer(sipluscnt2));
    }

    public HYIDouble getSIPercentOffAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountTax2Amount");
        else
            return (HYIDouble) getFieldValue("SIPAMT2");
    }

    public void setSIPercentOffAmount2(HYIDouble sipamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax2Amount", sipamt2);
        else
            setFieldValue("SIPAMT2", sipamt2);
    }

    public Integer getSIPercentOffCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT2");
    }

    public void setSIPercentOffCount2(int sipcnt2) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPCNT2", new Integer(sipcnt2));
        else
            setFieldValue("SIPCNT2", new Integer(sipcnt2));
    }

    public HYIDouble getSIDiscountAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else
            return (HYIDouble) getFieldValue("SIMAMT2");
    }

    public void setSIDiscountAmount2(HYIDouble simamt2) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMAMT2", simamt2);
        else
            setFieldValue("SIMAMT2", simamt2);
    }

    public Integer getSIDiscountCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT2");
    }

    public void setSIDiscountCount2(int simcnt2) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMCNT2", new Integer(simcnt2));
        else
            setFieldValue("SIMCNT2", new Integer(simcnt2));
    }

    public HYIDouble getMixAndMatchAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("mixAndMatchTax2Amount");
        else
            return (HYIDouble) getFieldValue("MNMAMT2");
    }

    public void setMixAndMatchAmount2(HYIDouble mnmamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax2Amount", mnmamt2);
        else
            setFieldValue("MNMAMT2", mnmamt2);
    }

    public Integer getMixAndMatchCount2() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT2");
    }

    public void setMixAndMatchCount2(int mnmcnt2) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("MNMCNT2", new Integer(mnmcnt2));
        else
            setFieldValue("MNMCNT2", new Integer(mnmcnt2));
    }

    public HYIDouble getNotIncludedSales2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("notIncludedTax2Sale");
        else
            return (HYIDouble) getFieldValue("RCPGIFAMT2");
    }

    public void setNotIncludedSales2(HYIDouble rcpgifamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax2Sale", rcpgifamt2);
        else
            setFieldValue("RCPGIFAMT2", rcpgifamt2);
    }

    public HYIDouble getNetSalesAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTax2Amount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT2");
    }

    public void setNetSalesAmount2(HYIDouble netsalamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax2Amount", netsalamt2);
        else
            setFieldValue("NETSALAMT2", netsalamt2);
    }

    // tax3
    public HYIDouble getSIPercentPlusAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("siPlusTax3Amount");
        else
            return (HYIDouble) getFieldValue("SIPLUSAMT3");
    }

    public void setSIPercentPlusAmount3(HYIDouble siplusamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax3Amount", siplusamt3);
        else
            setFieldValue("SIPLUSAMT3", siplusamt3);
    }

    public Integer getSIPercentPlusCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT3");
    }

    public void setSIPercentPlusCount3(int sipluscnt3) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPLUSCNT3", new Integer(sipluscnt3));
        else
            setFieldValue("SIPLUSCNT3", new Integer(sipluscnt3));
    }

    public HYIDouble getSIPercentOffAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountTax3Amount");
        else
            return (HYIDouble) getFieldValue("SIPAMT3");
    }

    public void setSIPercentOffAmount3(HYIDouble sipamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax3Amount", sipamt3);
        else
            setFieldValue("SIPAMT3", sipamt3);
    }

    public Integer getSIPercentOffCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT3");
    }

    public void setSIPercentOffCount3(int sipcnt3) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPCNT3", new Integer(sipcnt3));
        else
            setFieldValue("SIPCNT3", new Integer(sipcnt3));
    }

    public HYIDouble getSIDiscountAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else
            return (HYIDouble) getFieldValue("SIMAMT3");
    }

    public void setSIDiscountAmount3(HYIDouble simamt3) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMAMT3", simamt3);
        else
            setFieldValue("SIMAMT3", simamt3);
    }

    public Integer getSIDiscountCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT3");
    }

    public void setSIDiscountCount3(int simcnt3) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMCNT3", new Integer(simcnt3));
        else
            setFieldValue("SIMCNT3", new Integer(simcnt3));
    }

    public HYIDouble getMixAndMatchAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("mixAndMatchTax3Amount");
        else
            return (HYIDouble) getFieldValue("MNMAMT3");
    }

    public void setMixAndMatchAmount3(HYIDouble mnmamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax3Amount", mnmamt3);
        else
            setFieldValue("MNMAMT3", mnmamt3);
    }

    public Integer getMixAndMatchCount3() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT3");
    }

    public void setMixAndMatchCount3(int mnmcnt3) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("MNMCNT3", new Integer(mnmcnt3));
        else
            setFieldValue("MNMCNT3", new Integer(mnmcnt3));
    }

    public HYIDouble getNotIncludedSales3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("notIncludedTax3Sale");
        else
            return (HYIDouble) getFieldValue("RCPGIFAMT3");
    }

    public void setNotIncludedSales3(HYIDouble rcpgifamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax3Sale", rcpgifamt3);
        else
            setFieldValue("RCPGIFAMT3", rcpgifamt3);
    }

    public HYIDouble getNetSalesAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTax3Amount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT3");
    }

    public void setNetSalesAmount3(HYIDouble netsalamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax3Amount", netsalamt3);
        else
            setFieldValue("NETSALAMT3", netsalamt3);
    }

    // tax4
    public HYIDouble getSIPercentPlusAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("siPlusTax4Amount");
        else
            return (HYIDouble) getFieldValue("SIPLUSAMT4");
    }

    public void setSIPercentPlusAmount4(HYIDouble siplusamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax4Amount", siplusamt4);
        else
            setFieldValue("SIPLUSAMT4", siplusamt4);
    }

    public Integer getSIPercentPlusCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT4");
    }

    public void setSIPercentPlusCount4(int sipluscnt4) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPLUSCNT4", new Integer(sipluscnt4));
        else
            setFieldValue("SIPLUSCNT4", new Integer(sipluscnt4));
    }

    public HYIDouble getSIPercentOffAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountTax4Amount");
        else
            return (HYIDouble) getFieldValue("SIPAMT4");
    }

    public void setSIPercentOffAmount4(HYIDouble sipamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax4Amount", sipamt4);
        else
            setFieldValue("SIPAMT4", sipamt4);
    }

    public Integer getSIPercentOffCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT4");
    }

    public void setSIPercentOffCount4(int sipcnt4) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPCNT4", new Integer(sipcnt4));
        else
            setFieldValue("SIPCNT4", new Integer(sipcnt4));
    }

    public HYIDouble getSIDiscountAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else
            return (HYIDouble) getFieldValue("SIMAMT4");
    }

    public void setSIDiscountAmount4(HYIDouble simamt4) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMAMT4", simamt4);
        else
            setFieldValue("SIMAMT4", simamt4);
    }

    public Integer getSIDiscountCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT4");
    }

    public void setSIDiscountCount4(int simcnt4) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMCNT4", new Integer(simcnt4));
        else
            setFieldValue("SIMCNT4", new Integer(simcnt4));
    }

    public HYIDouble getMixAndMatchAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("mixAndMatchTax4Amount");
        else
            return (HYIDouble) getFieldValue("MNMAMT4");
    }

    public void setMixAndMatchAmount4(HYIDouble mnmamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax4Amount", mnmamt4);
        else
            setFieldValue("MNMAMT4", mnmamt4);
    }

    public Integer getMixAndMatchCount4() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT4");
    }

    public void setMixAndMatchCount4(int mnmcnt4) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("MNMCNT4", new Integer(mnmcnt4));
        else
            setFieldValue("MNMCNT4", new Integer(mnmcnt4));
    }

    public HYIDouble getNotIncludedSales4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("notIncludedTax4Sale");
        else
            return (HYIDouble) getFieldValue("RCPGIFAMT4");
    }

    public void setNotIncludedSales4(HYIDouble rcpgifamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax4Sale", rcpgifamt4);
        else
            setFieldValue("RCPGIFAMT4", rcpgifamt4);
    }

    public HYIDouble getNetSalesAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTax4Amount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT4");
    }

    public void setNetSalesAmount4(HYIDouble netsalamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax4Amount", netsalamt4);
        else
            setFieldValue("NETSALAMT4", netsalamt4);
    }
    // tax5
    public HYIDouble getSIPercentPlusAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("siPlusTax5Amount");
        else
            return (HYIDouble) getFieldValue("SIPLUSAMT5");
    }

    public void setSIPercentPlusAmount5(HYIDouble siplusamt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax5Amount", siplusamt5);
        else
            setFieldValue("SIPLUSAMT5", siplusamt5);
    }

    public Integer getSIPercentPlusCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT5");
    }

    public void setSIPercentPlusCount5(int sipluscnt5) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPLUSCNT5", new Integer(sipluscnt5));
        else
            setFieldValue("SIPLUSCNT5", new Integer(sipluscnt5));
    }

    public HYIDouble getSIPercentOffAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountTax5Amount");
        else
            return (HYIDouble) getFieldValue("SIPAMT5");
    }

    public void setSIPercentOffAmount5(HYIDouble sipamt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax5Amount", sipamt5);
        else
            setFieldValue("SIPAMT5", sipamt5);
    }

    public Integer getSIPercentOffCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT5");
    }

    public void setSIPercentOffCount5(int sipcnt5) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPCNT5", new Integer(sipcnt5));
        else
            setFieldValue("SIPCNT5", new Integer(sipcnt5));
    }

    public HYIDouble getSIDiscountAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else
            return (HYIDouble) getFieldValue("SIMAMT5");
    }

    public void setSIDiscountAmount5(HYIDouble simamt5) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMAMT5", simamt5);
        else
            setFieldValue("SIMAMT5", simamt5);
    }

    public Integer getSIDiscountCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT5");
    }

    public void setSIDiscountCount5(int simcnt5) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMCNT5", new Integer(simcnt5));
        else
            setFieldValue("SIMCNT5", new Integer(simcnt5));
    }

    public HYIDouble getMixAndMatchAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("mixAndMatchTax5Amount");
        else
            return (HYIDouble) getFieldValue("MNMAMT5");
    }

    public void setMixAndMatchAmount5(HYIDouble mnmamt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax5Amount", mnmamt5);
        else
            setFieldValue("MNMAMT5", mnmamt5);
    }

    public Integer getMixAndMatchCount5() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT5");
    }

    public void setMixAndMatchCount5(int mnmcnt5) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("MNMCNT5", new Integer(mnmcnt5));
        else
            setFieldValue("MNMCNT5", new Integer(mnmcnt5));
    }

    public HYIDouble getNotIncludedSales5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("notIncludedTax5Sale");
        else
            return (HYIDouble) getFieldValue("RCPGIFAMT5");
    }

    public void setNotIncludedSales5(HYIDouble rcpgifamt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax5Sale", rcpgifamt5);
        else
            setFieldValue("RCPGIFAMT5", rcpgifamt5);
    }

    public HYIDouble getNetSalesAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTax5Amount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT5");
    }

    public void setNetSalesAmount5(HYIDouble netsalamt5) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax5Amount", netsalamt5);
        else
            setFieldValue("NETSALAMT5", netsalamt5);
    }
    // tax6
    public HYIDouble getSIPercentPlusAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("siPlusTax6Amount");
        else
            return (HYIDouble) getFieldValue("SIPLUSAMT6");
    }

    public void setSIPercentPlusAmount6(HYIDouble siplusamt6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax6Amount", siplusamt6);
        else
            setFieldValue("SIPLUSAMT6", siplusamt6);
    }

    public Integer getSIPercentPlusCount6() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT6");
    }

    public void setSIPercentPlusCount6(int sipluscnt6) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPLUSCNT6", new Integer(sipluscnt6));
        else
            setFieldValue("SIPLUSCNT6", new Integer(sipluscnt6));
    }

    public HYIDouble getSIPercentOffAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountTax6Amount");
        else
            return (HYIDouble) getFieldValue("SIPAMT6");
    }

    public void setSIPercentOffAmount6(HYIDouble sipamt6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax6Amount", sipamt6);
        else
            setFieldValue("SIPAMT6", sipamt6);
    }

    public Integer getSIPercentOffCount6() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT6");
    }

    public void setSIPercentOffCount6(int sipcnt6) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPCNT6", new Integer(sipcnt6));
        else
            setFieldValue("SIPCNT6", new Integer(sipcnt6));
    }

    public HYIDouble getSIDiscountAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else
            return (HYIDouble) getFieldValue("SIMAMT6");
    }

    public void setSIDiscountAmount6(HYIDouble simamt6) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMAMT6", simamt6);
        else
            setFieldValue("SIMAMT6", simamt6);
    }

    public Integer getSIDiscountCount6() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT6");
    }

    public void setSIDiscountCount6(int simcnt6) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMCNT6", new Integer(simcnt6));
        else
            setFieldValue("SIMCNT6", new Integer(simcnt6));
    }

    public HYIDouble getMixAndMatchAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("mixAndMatchTax6Amount");
        else
            return (HYIDouble) getFieldValue("MNMAMT6");
    }

    public void setMixAndMatchAmount6(HYIDouble mnmamt6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax6Amount", mnmamt6);
        else
            setFieldValue("MNMAMT6", mnmamt6);
    }

    public Integer getMixAndMatchCount6() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT6");
    }

    public void setMixAndMatchCount6(int mnmcnt6) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("MNMCNT6", new Integer(mnmcnt6));
        else
            setFieldValue("MNMCNT6", new Integer(mnmcnt6));
    }

    public HYIDouble getNotIncludedSales6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("notIncludedTax6Sale");
        else
            return (HYIDouble) getFieldValue("RCPGIFAMT6");
    }

    public void setNotIncludedSales6(HYIDouble rcpgifamt6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax6Sale", rcpgifamt6);
        else
            setFieldValue("RCPGIFAMT6", rcpgifamt6);
    }

    public HYIDouble getNetSalesAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTax6Amount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT6");
    }

    public void setNetSalesAmount6(HYIDouble netsalamt6) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax6Amount", netsalamt6);
        else
            setFieldValue("NETSALAMT6", netsalamt6);
    }

    // tax7
    public HYIDouble getSIPercentPlusAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("siPlusTax7Amount");
        else
            return (HYIDouble) getFieldValue("SIPLUSAMT7");
    }

    public void setSIPercentPlusAmount7(HYIDouble siplusamt7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax7Amount", siplusamt7);
        else
            setFieldValue("SIPLUSAMT7", siplusamt7);
    }

    public Integer getSIPercentPlusCount7() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT7");
    }

    public void setSIPercentPlusCount7(int sipluscnt7) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPLUSCNT7", new Integer(sipluscnt7));
        else
            setFieldValue("SIPLUSCNT7", new Integer(sipluscnt7));
    }

    public HYIDouble getSIPercentOffAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountTax7Amount");
        else
            return (HYIDouble) getFieldValue("SIPAMT7");
    }

    public void setSIPercentOffAmount7(HYIDouble sipamt7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax7Amount", sipamt7);
        else
            setFieldValue("SIPAMT7", sipamt7);
    }

    public Integer getSIPercentOffCount7() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT7");
    }

    public void setSIPercentOffCount7(int sipcnt7) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPCNT7", new Integer(sipcnt7));
        else
            setFieldValue("SIPCNT7", new Integer(sipcnt7));
    }

    public HYIDouble getSIDiscountAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else
            return (HYIDouble) getFieldValue("SIMAMT7");
    }

    public void setSIDiscountAmount7(HYIDouble simamt7) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMAMT7", simamt7);
        else
            setFieldValue("SIMAMT7", simamt7);
    }

    public Integer getSIDiscountCount7() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT7");
    }

    public void setSIDiscountCount7(int simcnt7) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMCNT7", new Integer(simcnt7));
        else
            setFieldValue("SIMCNT7", new Integer(simcnt7));
    }

    public HYIDouble getMixAndMatchAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("mixAndMatchTax7Amount");
        else
            return (HYIDouble) getFieldValue("MNMAMT7");
    }

    public void setMixAndMatchAmount7(HYIDouble mnmamt7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax7Amount", mnmamt7);
        else
            setFieldValue("MNMAMT7", mnmamt7);
    }

    public Integer getMixAndMatchCount7() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT7");
    }

    public void setMixAndMatchCount7(int mnmcnt7) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("MNMCNT7", new Integer(mnmcnt7));
        else
            setFieldValue("MNMCNT7", new Integer(mnmcnt7));
    }

    public HYIDouble getNotIncludedSales7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("notIncludedTax7Sale");
        else
            return (HYIDouble) getFieldValue("RCPGIFAMT7");
    }

    public void setNotIncludedSales7(HYIDouble rcpgifamt7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax7Sale", rcpgifamt7);
        else
            setFieldValue("RCPGIFAMT7", rcpgifamt7);
    }

    public HYIDouble getNetSalesAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTax7Amount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT7");
    }

    public void setNetSalesAmount7(HYIDouble netsalamt7) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax7Amount", netsalamt7);
        else
            setFieldValue("NETSALAMT7", netsalamt7);
    }

    // tax8
    public HYIDouble getSIPercentPlusAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("siPlusTax8Amount");
        else
            return (HYIDouble) getFieldValue("SIPLUSAMT8");
    }

    public void setSIPercentPlusAmount8(HYIDouble siplusamt8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax8Amount", siplusamt8);
        else
            setFieldValue("SIPLUSAMT8", siplusamt8);
    }

    public Integer getSIPercentPlusCount8() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT8");
    }

    public void setSIPercentPlusCount8(int sipluscnt8) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPLUSCNT8", new Integer(sipluscnt8));
        else
            setFieldValue("SIPLUSCNT8", new Integer(sipluscnt8));
    }

    public HYIDouble getSIPercentOffAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountTax8Amount");
        else
            return (HYIDouble) getFieldValue("SIPAMT8");
    }

    public void setSIPercentOffAmount8(HYIDouble sipamt8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax8Amount", sipamt8);
        else
            setFieldValue("SIPAMT8", sipamt8);
    }

    public Integer getSIPercentOffCount8() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT8");
    }

    public void setSIPercentOffCount8(int sipcnt8) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPCNT8", new Integer(sipcnt8));
        else
            setFieldValue("SIPCNT8", new Integer(sipcnt8));
    }

    public HYIDouble getSIDiscountAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else
            return (HYIDouble) getFieldValue("SIMAMT8");
    }

    public void setSIDiscountAmount8(HYIDouble simamt8) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMAMT8", simamt8);
        else
            setFieldValue("SIMAMT8", simamt8);
    }

    public Integer getSIDiscountCount8() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT8");
    }

    public void setSIDiscountCount8(int simcnt8) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMCNT8", new Integer(simcnt8));
        else
            setFieldValue("SIMCNT8", new Integer(simcnt8));
    }

    public HYIDouble getMixAndMatchAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("mixAndMatchTax8Amount");
        else
            return (HYIDouble) getFieldValue("MNMAMT8");
    }

    public void setMixAndMatchAmount8(HYIDouble mnmamt8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax8Amount", mnmamt8);
        else
            setFieldValue("MNMAMT8", mnmamt8);
    }

    public Integer getMixAndMatchCount8() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT8");
    }

    public void setMixAndMatchCount8(int mnmcnt8) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("MNMCNT8", new Integer(mnmcnt8));
        else
            setFieldValue("MNMCNT8", new Integer(mnmcnt8));
    }

    public HYIDouble getNotIncludedSales8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("notIncludedTax8Sale");
        else
            return (HYIDouble) getFieldValue("RCPGIFAMT8");
    }

    public void setNotIncludedSales8(HYIDouble rcpgifamt8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax8Sale", rcpgifamt8);
        else
            setFieldValue("RCPGIFAMT8", rcpgifamt8);
    }

    public HYIDouble getNetSalesAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTax8Amount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT8");
    }

    public void setNetSalesAmount8(HYIDouble netsalamt8) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax8Amount", netsalamt8);
        else
            setFieldValue("NETSALAMT8", netsalamt8);
    }
    // tax9
    public HYIDouble getSIPercentPlusAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("siPlusTax9Amount");
        else
            return (HYIDouble) getFieldValue("SIPLUSAMT9");
    }

    public void setSIPercentPlusAmount9(HYIDouble siplusamt9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax9Amount", siplusamt9);
        else
            setFieldValue("SIPLUSAMT9", siplusamt9);
    }

    public Integer getSIPercentPlusCount9() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT9");
    }

    public void setSIPercentPlusCount9(int sipluscnt9) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPLUSCNT9", new Integer(sipluscnt9));
        else
            setFieldValue("SIPLUSCNT9", new Integer(sipluscnt9));
    }

    public HYIDouble getSIPercentOffAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountTax9Amount");
        else
            return (HYIDouble) getFieldValue("SIPAMT9");
    }

    public void setSIPercentOffAmount9(HYIDouble sipamt9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax9Amount", sipamt9);
        else
            setFieldValue("SIPAMT9", sipamt9);
    }

    public Integer getSIPercentOffCount9() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT9");
    }

    public void setSIPercentOffCount9(int sipcnt9) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPCNT9", new Integer(sipcnt9));
        else
            setFieldValue("SIPCNT9", new Integer(sipcnt9));
    }

    public HYIDouble getSIDiscountAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else
            return (HYIDouble) getFieldValue("SIMAMT9");
    }

    public void setSIDiscountAmount9(HYIDouble simamt9) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMAMT9", simamt9);
        else
            setFieldValue("SIMAMT9", simamt9);
    }

    public Integer getSIDiscountCount9() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT9");
    }

    public void setSIDiscountCount9(int simcnt9) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMCNT9", new Integer(simcnt9));
        else
            setFieldValue("SIMCNT9", new Integer(simcnt9));
    }

    public HYIDouble getMixAndMatchAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("mixAndMatchTax9Amount");
        else
            return (HYIDouble) getFieldValue("MNMAMT9");
    }

    public void setMixAndMatchAmount9(HYIDouble mnmamt9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax9Amount", mnmamt9);
        else
            setFieldValue("MNMAMT9", mnmamt9);
    }

    public Integer getMixAndMatchCount9() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT9");
    }

    public void setMixAndMatchCount9(int mnmcnt9) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("MNMCNT9", new Integer(mnmcnt9));
        else
            setFieldValue("MNMCNT9", new Integer(mnmcnt9));
    }

    public HYIDouble getNotIncludedSales9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("notIncludedTax9Sale");
        else
            return (HYIDouble) getFieldValue("RCPGIFAMT9");
    }

    public void setNotIncludedSales9(HYIDouble rcpgifamt9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax9Sale", rcpgifamt9);
        else
            setFieldValue("RCPGIFAMT9", rcpgifamt9);
    }

    public HYIDouble getNetSalesAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTax9Amount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT9");
    }

    public void setNetSalesAmount9(HYIDouble netsalamt9) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax9Amount", netsalamt9);
        else
            setFieldValue("NETSALAMT9", netsalamt9);
    }
    // tax10
    public HYIDouble getSIPercentPlusAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("siPlusTax10Amount");
        else
            return (HYIDouble) getFieldValue("SIPLUSAMT10");
    }

    public void setSIPercentPlusAmount10(HYIDouble siplusamt10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("siPlusTax10Amount", siplusamt10);
        else
            setFieldValue("SIPLUSAMT10", siplusamt10);
    }

    public Integer getSIPercentPlusCount10() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT10");
    }

    public void setSIPercentPlusCount10(int sipluscnt10) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPLUSCNT10", new Integer(sipluscnt10));
        else
            setFieldValue("SIPLUSCNT10", new Integer(sipluscnt10));
    }

    public HYIDouble getSIPercentOffAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("discountTax10Amount");
        else
            return (HYIDouble) getFieldValue("SIPAMT10");
    }

    public void setSIPercentOffAmount10(HYIDouble sipamt10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountTax10Amount", sipamt10);
        else
            setFieldValue("SIPAMT10", sipamt10);
    }

    public Integer getSIPercentOffCount10() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT10");
    }

    public void setSIPercentOffCount10(int sipcnt10) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIPCNT10", new Integer(sipcnt10));
        else
            setFieldValue("SIPCNT10", new Integer(sipcnt10));
    }

    public HYIDouble getSIDiscountAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else
            return (HYIDouble) getFieldValue("SIMAMT10");
    }

    public void setSIDiscountAmount10(HYIDouble simamt10) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMAMT10", simamt10);
        else
            setFieldValue("SIMAMT10", simamt10);
    }

    public Integer getSIDiscountCount10() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT10");
    }

    public void setSIDiscountCount10(int simcnt10) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SIMCNT10", new Integer(simcnt10));
        else
            setFieldValue("SIMCNT10", new Integer(simcnt10));
    }

    public HYIDouble getMixAndMatchAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("mixAndMatchTax10Amount");
        else
            return (HYIDouble) getFieldValue("MNMAMT10");
    }

    public void setMixAndMatchAmount10(HYIDouble mnmamt10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("mixAndMatchTax10Amount", mnmamt10);
        else
            setFieldValue("MNMAMT10", mnmamt10);
    }

    public Integer getMixAndMatchCount10() {
        if (hyi.cream.inline.Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT10");
    }

    public void setMixAndMatchCount10(int mnmcnt10) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("MNMCNT10", new Integer(mnmcnt10));
        else
            setFieldValue("MNMCNT10", new Integer(mnmcnt10));
    }

    public HYIDouble getNotIncludedSales10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("notIncludedTax10Sale");
        else
            return (HYIDouble) getFieldValue("RCPGIFAMT10");
    }

    public void setNotIncludedSales10(HYIDouble rcpgifamt10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("notIncludedTax10Sale", rcpgifamt10);
        else
            setFieldValue("RCPGIFAMT10", rcpgifamt10);
    }

    public HYIDouble getNetSalesAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTax10Amount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT10");
    }

    public void setNetSalesAmount10(HYIDouble netsalamt10) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTax10Amount", netsalamt10);
        else
            setFieldValue("NETSALAMT10", netsalamt10);
    }

    public HYIDouble getNetSalesAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("netSaleTotalAmount");
        else
            return (HYIDouble) getFieldValue("NETSALAMT");
    }

    public void setNetSalesAmount(HYIDouble netsalamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("netSaleTotalAmount", netsalamt);
        else
            setFieldValue("NETSALAMT", netsalamt);
    }

    public void setSelfbuyOrderid(Integer selfbuyOrderid) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("selfbuyOrderid", selfbuyOrderid);
        else
            setFieldValue("selfbuyOrderid", selfbuyOrderid);
    }

    public Integer getSelfbuyOrderid() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("selfbuyOrderid");
        else {
            return (Integer) getFieldValue("selfbuyOrderid");
        }
    }

    public void setSelfbuyOrderno(String selfbuyOrderno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("selfbuyOrderno", selfbuyOrderno);
        else
            setFieldValue("selfbuyOrderno", selfbuyOrderno);
    }

    public String getSelfbuyOrderno() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("selfbuyOrderno");
        else {
            return (String) getFieldValue("selfbuyOrderno");
        }
    }

    // TaxAmount 税别合计
    public HYIDouble getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(HYIDouble taxamt) {
        taxAmount = taxamt;
    }

    /**
     * 对该tran中各个计算字段置初始值（零），但折扣不动。
     */
    public void initForAccum() {
        setSalesAmount(nHYIDouble0());
        setInvoiceAmount(nHYIDouble0());
        setGrossSalesAmount(nHYIDouble0());
        setDaiFuAmount(nHYIDouble0());
        setDaiShouAmount(nHYIDouble0());
        setDaiShouAmount2(nHYIDouble0());
        setPreRcvAmount(nHYIDouble0());
        setUseRebateAmt(nHYIDouble0());

        setGrossSalesTax0Amount(nHYIDouble0());
        setGrossSalesTax1Amount(nHYIDouble0());
        setGrossSalesTax2Amount(nHYIDouble0());
        setGrossSalesTax3Amount(nHYIDouble0());
        setGrossSalesTax4Amount(nHYIDouble0());
        setGrossSalesTax5Amount(nHYIDouble0());
        setGrossSalesTax6Amount(nHYIDouble0());
        setGrossSalesTax7Amount(nHYIDouble0());
        setGrossSalesTax8Amount(nHYIDouble0());
        setGrossSalesTax9Amount(nHYIDouble0());
        setGrossSalesTax10Amount(nHYIDouble0());

        setNotIncludedSales0(nHYIDouble0());
        setNotIncludedSales1(nHYIDouble0());
        setNotIncludedSales2(nHYIDouble0());
        setNotIncludedSales3(nHYIDouble0());
        setNotIncludedSales4(nHYIDouble0());
        setNotIncludedSales5(nHYIDouble0());
        setNotIncludedSales6(nHYIDouble0());
        setNotIncludedSales7(nHYIDouble0());
        setNotIncludedSales8(nHYIDouble0());
        setNotIncludedSales9(nHYIDouble0());
        setNotIncludedSales10(nHYIDouble0());

        setNetSalesAmount(nHYIDouble0());
        setNetSalesAmount0(nHYIDouble0());
        setNetSalesAmount1(nHYIDouble0());
        setNetSalesAmount2(nHYIDouble0());
        setNetSalesAmount3(nHYIDouble0());
        setNetSalesAmount4(nHYIDouble0());
        setNetSalesAmount5(nHYIDouble0());
        setNetSalesAmount6(nHYIDouble0());
        setNetSalesAmount7(nHYIDouble0());
        setNetSalesAmount8(nHYIDouble0());
        setNetSalesAmount9(nHYIDouble0());
        setNetSalesAmount10(nHYIDouble0());

        setTaxAmount0(nHYIDouble0());
        setTaxAmount1(nHYIDouble0());
        setTaxAmount2(nHYIDouble0());
        setTaxAmount3(nHYIDouble0());
        setTaxAmount4(nHYIDouble0());
        setTaxAmount5(nHYIDouble0());
        setTaxAmount6(nHYIDouble0());
        setTaxAmount7(nHYIDouble0());
        setTaxAmount8(nHYIDouble0());
        setTaxAmount9(nHYIDouble0());
        setTaxAmount10(nHYIDouble0());
    }

    /*
     * 初始化信用卡信息
     */
    public void initCreditCardInfo() {
        setCreditCardExpireDate(null);
        setCreditCardType("");
        setCreditCardNumber("");
    }

    /*
     * 初始化支付卡信息
     */
    public void initPayCardInfo() {
        setPayCardNumber("");
    }
    
    /*
     * 把下面的内容从 initForAccum() 中剥离出来 如果是从DiscountState 进入到 SummaryState 就不执行下面的
     */
    public void initSIInfo() {
        setSIDiscountAmount0(nHYIDouble0());
        setSIDiscountAmount1(nHYIDouble0());
        setSIDiscountAmount2(nHYIDouble0());
        setSIDiscountAmount3(nHYIDouble0());
        setSIDiscountAmount4(nHYIDouble0());
        setSIDiscountAmount5(nHYIDouble0());
        setSIDiscountAmount6(nHYIDouble0());
        setSIDiscountAmount7(nHYIDouble0());
        setSIDiscountAmount8(nHYIDouble0());
        setSIDiscountAmount9(nHYIDouble0());
        setSIDiscountAmount10(nHYIDouble0());
        setSIDiscountCount0(0);
        setSIDiscountCount1(0);
        setSIDiscountCount2(0);
        setSIDiscountCount3(0);
        setSIDiscountCount4(0);
        setSIDiscountCount5(0);
        setSIDiscountCount6(0);
        setSIDiscountCount7(0);
        setSIDiscountCount8(0);
        setSIDiscountCount9(0);
        setSIDiscountCount10(0);

        setSIPercentPlusAmount0(nHYIDouble0());
        setSIPercentPlusAmount1(nHYIDouble0());
        setSIPercentPlusAmount2(nHYIDouble0());
        setSIPercentPlusAmount3(nHYIDouble0());
        setSIPercentPlusAmount4(nHYIDouble0());
        setSIPercentPlusAmount5(nHYIDouble0());
        setSIPercentPlusAmount6(nHYIDouble0());
        setSIPercentPlusAmount7(nHYIDouble0());
        setSIPercentPlusAmount8(nHYIDouble0());
        setSIPercentPlusAmount9(nHYIDouble0());
        setSIPercentPlusAmount10(nHYIDouble0());
        setSIPercentPlusCount0(0);
        setSIPercentPlusCount1(0);
        setSIPercentPlusCount2(0);
        setSIPercentPlusCount3(0);
        setSIPercentPlusCount4(0);
        setSIPercentPlusCount5(0);
        setSIPercentPlusCount6(0);
        setSIPercentPlusCount7(0);
        setSIPercentPlusCount8(0);
        setSIPercentPlusCount9(0);
        setSIPercentPlusCount10(0);

        setSIPercentOffAmount0(nHYIDouble0());
        setSIPercentOffAmount1(nHYIDouble0());
        setSIPercentOffAmount2(nHYIDouble0());
        setSIPercentOffAmount3(nHYIDouble0());
        setSIPercentOffAmount4(nHYIDouble0());
        setSIPercentOffAmount5(nHYIDouble0());
        setSIPercentOffAmount6(nHYIDouble0());
        setSIPercentOffAmount7(nHYIDouble0());
        setSIPercentOffAmount8(nHYIDouble0());
        setSIPercentOffAmount9(nHYIDouble0());
        setSIPercentOffAmount10(nHYIDouble0());
        setSIPercentOffCount0(0);
        setSIPercentOffCount1(0);
        setSIPercentOffCount2(0);
        setSIPercentOffCount3(0);
        setSIPercentOffCount4(0);
        setSIPercentOffCount5(0);
        setSIPercentOffCount6(0);
        setSIPercentOffCount7(0);
        setSIPercentOffCount8(0);
        setSIPercentOffCount9(0);
        setSIPercentOffCount10(0);
    }

    public void saveOriginalLineItemAfterDiscountAmount() {
        getLineItems();
        Iterator itr = getLineItemsIterator();
        if (itr == null)
            return;
        while (itr.hasNext()) {
            LineItem li = (LineItem) itr.next();
            if (li.getRemoved()) {
                continue;
            }
            if (li.getDetailCode().equals("S")) {
                li.setOriginalAfterDiscountAmount(li.getAfterDiscountAmount());
            }
        }
    }

    public void restoreOriginalLineItemAfterDiscountAmount() {
        getLineItems();
        Iterator itr = getLineItemsIterator();
        if (itr == null)
            return;
        while (itr.hasNext()) {
            LineItem li = (LineItem) itr.next();
            if (li.getRemoved())
                continue;
            if (li.getDetailCode().equals("S")
                    && li.getOriginalAfterDiscountAmount() != null)
                li.setAfterDiscountAmount(li.getOriginalAfterDiscountAmount());
        }
    }

    public void accumulate() {
        accumulate(true);
    }

    /**
     * 由明细LintItems重算交易trand的一些汇总项目:
     * - loop by lineItems to calculate total sales amt, count ...
     * - UpdateTaxMM ...
     * - updateSIAmt ...
     */
    public void accumulate(boolean recollectMMAmounts) {
        for (LineItem li : lineItems()) {
            // "S":销售
            // "R":退货
            // "D":SI折扣
            // "T":ItemDiscount折扣
            // "M":Mix & Match折扣
            // "B":退瓶
            // "I":代售
            // "O":代收公共事业费
            // "Q":代支
            // "V":指定更正
            // "E":立即更正
            // "N":PaidIn
            // "T":PaidOut

            //if (li.getRemoved()) {
            //    continue;
            //}

            char code = ' ';
            try {
                code = li.getDetailCode().charAt(0);
            } catch (Exception e) {
            }

            switch (code) {
            case 'I': // "I":代售
                getDaiShouAmount().addMe(li.getAmount());
                break;
            case 'J': // J":期刊预订
            case 'L': // L":期刊退刊
                getPreRcvAmount().addMe(li.getAmount());

                //Bruce/20121015/ 對於期刊商品，也需記帳
                accumGrossAmountByTaxType(li.getTaxType(), li);

                break;
            case 'O': // "O":代收公共事业费
                getDaiShouAmount2().addMe(li.getAmount());
                break;
            case 'Q': // "Q":代支
                getDaiFuAmount().addMe(li.getUnitPrice());
                break;
            case 'N': // "N":PaidIn
                getDaiShouAmount().addMe(li.getUnitPrice()); // 代售/PaidIn金额(当DEALTYPE2='4'时为PaidIn金额)
                break;
            case 'T': // "T":Paidout
                getDaiFuAmount().addMe(li.getUnitPrice());   // 代付/PaidOut金额(当DEALTYPE2='4'时为PaidOut金额)
                break;
            case 'K': // "K":期刊领刊
            case 'M': // "M":Mix & Match折扣
            case 'D': // "D":SI折扣
                break;

            default: // "S":销售, "R":退货, "B":退瓶, "V":指定更正, "E":立即更正, "H":借零, "G":投库
                //if (li.isVoidPeriodicalItem())
                //    //Bruce/20121015/ 對於期刊商品的更正，也需扣除對應的預售金額
                //    getPreRcvAmount().addMe(li.getAmount());
                //else

                accumGrossAmountByTaxType(li.getTaxType(), li);
            }
        }

        //Meyer/2003-02-26/ Modified UpdateTaxMM only when meeting version 1
        if (recollectMMAmounts &&
            (StringUtils.isEmpty(mixAndMatchVersion)
                || "1".equals(mixAndMatchVersion)
                || "3".equals(mixAndMatchVersion))) {
            this.updateTaxMM();
        }

        HYIDouble gross = HYIDouble.zero();
        gross = gross.addMe(getGrossSalesTax0Amount());
        gross = gross.addMe(getGrossSalesTax1Amount());
        gross = gross.addMe(getGrossSalesTax2Amount());
        gross = gross.addMe(getGrossSalesTax3Amount());
        gross = gross.addMe(getGrossSalesTax4Amount());
        gross = gross.addMe(getGrossSalesTax5Amount());
        gross = gross.addMe(getGrossSalesTax6Amount());
        gross = gross.addMe(getGrossSalesTax7Amount());
        gross = gross.addMe(getGrossSalesTax8Amount());
        gross = gross.addMe(getGrossSalesTax9Amount());
        gross = gross.addMe(getGrossSalesTax10Amount());
        setGrossSalesAmount(gross);

        if (getNetSalesAmount0() == null)
            setNetSalesAmount0(HYIDouble.zero().setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount1() == null)
            setNetSalesAmount1(HYIDouble.zero().setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount2() == null)
            setNetSalesAmount2(HYIDouble.zero().setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount3() == null)
            setNetSalesAmount3(HYIDouble.zero().setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount4() == null)
            setNetSalesAmount4(HYIDouble.zero().setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount5() == null)
            setNetSalesAmount5(HYIDouble.zero().setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount6() == null)
            setNetSalesAmount6(HYIDouble.zero().setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount7() == null)
            setNetSalesAmount7(HYIDouble.zero().setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount8() == null)
            setNetSalesAmount8(HYIDouble.zero().setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount9() == null)
            setNetSalesAmount9(HYIDouble.zero().setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount10() == null)
            setNetSalesAmount10(HYIDouble.zero().setScale(2, BigDecimal.ROUND_HALF_UP));

        setNetSalesAmount0(getGrossSalesTax0Amount()
            .subtract(getMixAndMatchAmount0())
            .subtractMe(getSIPercentOffAmount0())
            .subtractMe(getSIPercentPlusAmount0())
            .subtractMe(getSIDiscountAmount0())
            .addMe(getRoundDownAmount())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        setNetSalesAmount1(getGrossSalesTax1Amount()
            .subtract(getMixAndMatchAmount1())
            .subtractMe(getSIPercentOffAmount1())
            .subtractMe(getSIPercentPlusAmount1())
            .subtractMe(getSIDiscountAmount1())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        setNetSalesAmount2(getGrossSalesTax2Amount()
            .subtract(getMixAndMatchAmount2())
            .subtractMe(getSIPercentOffAmount2())
            .subtractMe(getSIPercentPlusAmount2())
            .subtractMe(getSIDiscountAmount2())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        setNetSalesAmount3(getGrossSalesTax3Amount()
            .subtract(getMixAndMatchAmount3())
            .subtractMe(getSIPercentOffAmount3())
            .subtractMe(getSIPercentPlusAmount3())
            .subtractMe(getSIDiscountAmount3())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        setNetSalesAmount4(getGrossSalesTax4Amount()
            .subtract(getMixAndMatchAmount4())
            .subtractMe(getSIPercentOffAmount4())
            .subtractMe(getSIPercentPlusAmount4())
            .subtractMe(getSIDiscountAmount4())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        setNetSalesAmount5(getGrossSalesTax5Amount()
            .subtract(getMixAndMatchAmount5())
            .subtractMe(getSIPercentOffAmount5())
            .subtractMe(getSIPercentPlusAmount5())
            .subtractMe(getSIDiscountAmount5())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        setNetSalesAmount6(getGrossSalesTax6Amount()
            .subtract(getMixAndMatchAmount6())
            .subtractMe(getSIPercentOffAmount6())
            .subtractMe(getSIPercentPlusAmount6())
            .subtractMe(getSIDiscountAmount6())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        setNetSalesAmount7(getGrossSalesTax7Amount()
            .subtract(getMixAndMatchAmount7())
            .subtractMe(getSIPercentOffAmount7())
            .subtractMe(getSIPercentPlusAmount7())
            .subtractMe(getSIDiscountAmount7())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        setNetSalesAmount8(getGrossSalesTax8Amount()
            .subtract(getMixAndMatchAmount8())
            .subtractMe(getSIPercentOffAmount8())
            .subtractMe(getSIPercentPlusAmount8())
            .subtractMe(getSIDiscountAmount8())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        setNetSalesAmount9(getGrossSalesTax9Amount()
            .subtract(getMixAndMatchAmount9())
            .subtractMe(getSIPercentOffAmount9())
            .subtractMe(getSIPercentPlusAmount9())
            .subtractMe(getSIDiscountAmount9())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        setNetSalesAmount10(getGrossSalesTax10Amount()
            .subtract(getMixAndMatchAmount10())
            .subtractMe(getSIPercentOffAmount10())
            .subtractMe(getSIPercentPlusAmount10())
            .subtractMe(getSIDiscountAmount10())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN));

        HYIDouble netsales = HYIDouble.zero();
        netsales = netsales.addMe(getNetSalesAmount0());
        netsales = netsales.addMe(getNetSalesAmount1());
        netsales = netsales.addMe(getNetSalesAmount2());
        netsales = netsales.addMe(getNetSalesAmount3());
        netsales = netsales.addMe(getNetSalesAmount4());
        netsales = netsales.addMe(getNetSalesAmount5());
        netsales = netsales.addMe(getNetSalesAmount6());
        netsales = netsales.addMe(getNetSalesAmount7());
        netsales = netsales.addMe(getNetSalesAmount8());
        netsales = netsales.addMe(getNetSalesAmount9());
        netsales = netsales.addMe(getNetSalesAmount10());
        setNetSalesAmount(netsales);
        setInvoiceAmount(netsales);

        // SalesAmount 小计金额 = 营业毛额+SI加成-SI折扣-M&M折扣+代售-代付+PainIn-PaidOut+代收
        setSalesAmount(netsales.add(getDaiFuAmount())
            .addMe(getDaiShouAmount())
            .addMe(getDaiShouAmount2()))
            //.addMe(getPreRcvAmount())) //Bruce/20121017 期刊改成记账
            ;
    }

//    public void updateSIAmt(HYIDouble daifuAmt) {
//        HYIDouble sales = nHYIDouble0();
//
////        /*
////         * Meyer / 2003-02-26 / Modified UpdateTaxMM only when meeting version 1
////         */
////        if (mixAndMatchVersion == null || mixAndMatchVersion.trim().equals("")
////                || mixAndMatchVersion.trim().equals("1")
////                || mixAndMatchVersion.trim().equals("3")) {
////
////            sales = getGrossSalesAmount().subtract(getTaxMMAmount());
////        } else if (mixAndMatchVersion.equals("2")) {
////            sales = getGrossSalesAmount().subtract(this.getMixAndMatchTotalAmount());
////        }
//
//        sales.addMe(getGrossSalesAmount());
//
//        sales.subtractMe(mixAndMatchVersion.equals("2") ?
//            this.getMixAndMatchTotalAmount() : getTaxMMAmount());
//
//        sales.subtractMe(this.getTotalDIYOverrideAmount());
//
//        Iterator itra = getAppliedSIs().values().iterator();
//        while (itra.hasNext()) {
//            sales.addMe((HYIDouble) itra.next());
//        }
//
//        // set daishouamount(paidin) and daifuamount(paidout) into salesamount
//        if (getDealType1().equals("0") && getDealType2().equals("0") && (getDealType3().equals("3")
//            || getDealType3().equals("2") || getDealType3().equals("1"))) {
//            setSalesAmount(sales.addMe(daifuAmt));
//            setInvoiceAmount(getSalesAmount());
//        } else if (getDaiFuAmount().compareTo(new HYIDouble(0)) == 1) {
//            setSalesAmount(sales.addMe(getDaiShouAmount()).addMe(getDaiShouAmount2()).addMe(getDaiFuAmount()));
//            setInvoiceAmount(getSalesAmount());
//        } else if ("Q".equals(getDealType2())){
//            setSalesAmount(getOriginEarnAmount().subtract(getPreRcvAmount()));
//        } else {
//            // setSalesAmount(
//            // sales.add(daifuAmt).add(getDaiShouAmount()).add(getDaiShouAmount2()).subtract(getDaiFuAmount()));
//            setSalesAmount(sales.addMe(daifuAmt).addMe(getDaiShouAmount()).addMe(
//                    getDaiShouAmount2()).addMe(getDaiFuAmount()).addMe(getPreRcvAmount()));
//            setInvoiceAmount(getSalesAmount());
//        }
//    }

    /** 將單品金額累加到分稅別的銷售毛額和淨額 */
    public void accumGrossAmountByTaxType(String taxType, LineItem lineItem) {
        if (taxType == null || taxType.trim().length() == 0)
            return;

        if ("M".equals(lineItem.getDetailCode()))
            return;

        HYIDouble amount = lineItem.getAmount();

        if ("0".equals(taxType)) {
            getGrossSalesTax0Amount().addMe(amount);
        } else if ("1".equals(taxType)) {
            getGrossSalesTax1Amount().addMe(amount);
        } else if ("2".equals(taxType)) {
            getGrossSalesTax2Amount().addMe(amount);
        } else if ("3".equals(taxType)) {
            getGrossSalesTax3Amount().addMe(amount);
        } else if ("4".equals(taxType)) {
            getGrossSalesTax4Amount().addMe(amount);
        } else if ("5".equals(taxType)) {
            getGrossSalesTax5Amount().addMe(amount);
        } else if ("6".equals(taxType)) {
            getGrossSalesTax6Amount().addMe(amount);
        } else if ("7".equals(taxType)) {
            getGrossSalesTax7Amount().addMe(amount);
        } else if ("8".equals(taxType)) {
            getGrossSalesTax8Amount().addMe(amount);
        } else if ("9".equals(taxType)) {
            getGrossSalesTax9Amount().addMe(amount);
        } else if ("10".equals(taxType)) {
            getGrossSalesTax10Amount().addMe(amount);
        }
    }

//    public HYIDouble getYingFuAmount() {
//        return new HYIDouble(0).addMe(getSalesAmount()).addMe(getDaiFuAmount())
//                .addMe(getDaiShouAmount());
//    }
//
    // salesAmount is equals the amount of all lineItem'amount
    public HYIDouble getSalesAmount() {
        /*
         * HYIDouble amount = nHYIDouble0(); for (int i = 0; i <
         * lineItems.size(); i++) { amount =
         * ((LineItem)lineItems.get(i)).getAmount().add(amount); } if
         * (currentLineItem != null) { amount =
         * currentLineItem.getAmount().add(amount); }
         */
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else {
            HYIDouble saleAmt = (HYIDouble) getFieldValue("SALEAMT");
            if (saleAmt == null)
                return nHYIDouble0();
            else
                return saleAmt;
        }
    }

    public void setSalesAmount(HYIDouble saleamt) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SALEAMT", saleamt);
        else
            setFieldValue("SALEAMT", saleamt);
    }
    
    public HYIDouble getInvoiceAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return nHYIDouble0();
        else {
            HYIDouble invamt = (HYIDouble) getFieldValue("INVAMT");
            if (invamt == null)
                return nHYIDouble0();
            else
                return invamt;
        }
    }

    public void setInvoiceAmount(HYIDouble invamt) {
        if (hyi.cream.inline.Server.serverExist())
            ;
        // setFieldValue("SALEAMT", saleamt);
        else
            setFieldValue("INVAMT", invamt);
    }



    // 代售 如电话卡等
    public HYIDouble getDaiShouAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("daiShouAmount");
        else
            return (HYIDouble) getFieldValue("DAISHOUAMT");
    }

    public void setDaiShouAmount(HYIDouble daishouamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouAmount", daishouamt);
        else
            setFieldValue("DAISHOUAMT", daishouamt);
    }

    // 代收公共事业费
    public HYIDouble getDaiShouAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("daiShouAmount2");
        else
            return (HYIDouble) getFieldValue("DAISHOUAMT2");
    }

    public void setDaiShouAmount2(HYIDouble daishouamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiShouAmount2", daishouamt);
        else
            setFieldValue("DAISHOUAMT2", daishouamt);
    }

    public HYIDouble getDaiFuAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("daiFuAmount");
        else
            return (HYIDouble) getFieldValue("DAIFUAMT");
    }

    public void setDaiFuAmount(HYIDouble daifuamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("daiFuAmount", daifuamt);
        else
            setFieldValue("DAIFUAMT", daifuamt);
    }

    // 支付明细

    public String getPayName1() {
        Payment payment = Payment.queryByPaymentID(getPayNumber1());
        if (payment != null)
            return payment.getPrintName();
        else
            return "";
    }

    public String getPayName2() {
        Payment payment = Payment.queryByPaymentID(getPayNumber2());
        if (payment != null)
            return payment.getPrintName();
        else
            return "";
    }

    public String getPayName3() {
        Payment payment = Payment.queryByPaymentID(getPayNumber3());
        if (payment != null)
            return payment.getPrintName();
        else
            return "";
    }

    public String getPayName4() {
        Payment payment = Payment.queryByPaymentID(getPayNumber4());
        if (payment != null)
            return payment.getPrintName();
        else
            return "";
    }

    public String getPayNumber1() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("payID1");
        else
            return (String) getFieldValue("PAYNO1");
    }

    public void setPayNumber1(String payno1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("payID1", payno1);
        else
            setFieldValue("PAYNO1", payno1);
    }

    public String getPayNumber2() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("payID2");
        else
            return (String) getFieldValue("PAYNO2");
    }

    public void setPayNumber2(String payno2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("payID2", payno2);
        else
            setFieldValue("PAYNO2", payno2);
    }

    public String getPayNumber3() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("payID3");
        else
            return (String) getFieldValue("PAYNO3");
    }

    public void setPayNumber3(String payno3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("payID3", payno3);
        else
            setFieldValue("PAYNO3", payno3);
    }

    public String getPayNumber4() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("payID4");
        else
            return (String) getFieldValue("PAYNO4");
    }

    public void setPayNumber4(String payno4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("payID4", payno4);
        else
            setFieldValue("PAYNO4", payno4);
    }

    /**
     * 取得支付金额合计。
     * 
     * @return 支付金额合计。
     */
    public HYIDouble getTotalPaymentAmount() {
        /*
        HYIDouble total = new HYIDouble(0.00);
        if (getPayAmount1() != null)
            total = total.addMe(getPayAmount1());
        if (getPayAmount2() != null)
            total = total.addMe(getPayAmount2());
        if (getPayAmount3() != null)
            total = total.addMe(getPayAmount3());
        if (getPayAmount4() != null)
            total = total.addMe(getPayAmount4());
        return total;
        */
        
        HYIDouble totalPayAmount = new HYIDouble(0.00);
        for (int i = 1; i <= 4; i++) {
            String payno = getPayNumberByID(i);
            if (StringUtils.isEmpty(payno))
                break;
            HYIDouble payAmount = getPayAmountByID(i);
            if (payAmount != null) {
                totalPayAmount.addMe(payAmount);
            }
        }
        return totalPayAmount;
    }

    /**
     * 取得赊帐支付金额合计
     * 
     * @return 赊帐支付金额合计
     */
    public HYIDouble getShePaymentAmount() {
        HYIDouble total = new HYIDouble("0.00");
        String shePayId = Payment.queryDownPaymentId();
        if (getPayAmount1() != null && getPayNumber4() != null
                && getPayNumber1().equals(shePayId))
            total = total.addMe(getPayAmount1());
        if (getPayAmount2() != null && getPayNumber2() != null
                && getPayNumber2().equals(shePayId))
            total = total.addMe(getPayAmount2());
        if (getPayAmount3() != null && getPayNumber3() != null
                && getPayNumber3().equals(shePayId))
            total = total.addMe(getPayAmount3());
        if (getPayAmount4() != null && getPayNumber4() != null
                && getPayNumber4().equals(shePayId))
            total = total.addMe(getPayAmount4());
        return total;
    }

    /**
     * 取得支付金额合计，但“赊帐”支付除外； 合计金额不包括已开发票
     *
     * @return 支付金额合计（除“赊帐”支付）。
     */
    public HYIDouble getActualReceivedPaymentAmount() {
        HYIDouble total = new HYIDouble(0.00);
        String shePayId = Payment.queryDownPaymentId();
        if (getPayAmount1() != null
                && getPayNumber1() != null
                && !getPayNumber1().equals(shePayId))
            // if (!(getPayNumber1().equals(rebateID) && excudeRebateInSum)
            if (Payment.queryByPaymentID(getPayNumber1()) == null 
                    || !Payment.queryByPaymentID(getPayNumber1()).isNonInvoice())
                total = total.addMe(getPayAmount1());
        if (getPayAmount2() != null
                && getPayNumber2() != null
                && !getPayNumber2().equals(shePayId))
            if (Payment.queryByPaymentID(getPayNumber2()) == null 
                    || !Payment.queryByPaymentID(getPayNumber2()).isNonInvoice())
                total = total.addMe(getPayAmount2());
        if (getPayAmount3() != null
                && getPayNumber3() != null
                && !getPayNumber3().equals(shePayId))
            if (Payment.queryByPaymentID(getPayNumber3()) == null 
                    || !Payment.queryByPaymentID(getPayNumber3()).isNonInvoice())
                total = total.addMe(getPayAmount3());

        if (getPayAmount4() != null
                && getPayNumber4() != null
                && !getPayNumber4().equals(shePayId))
            if (Payment.queryByPaymentID(getPayNumber4()) == null 
                    || !Payment.queryByPaymentID(getPayNumber4()).isNonInvoice())
                total = total.addMe(getPayAmount4());
        return total;
    }

    /**
     * 取得支付金额的说明文字。
     * 
     * @return 支付金额的说明文字。
     */
    public String getPaymentDescription() {
        StringBuffer desc = new StringBuffer();
        String payName;
        if (!StringUtils.isEmpty(payName = getPayName1())
                && !(this.getPayNumber1().equals(rebateID))) {
            desc.append(payName);
            desc.append(':');
            desc.append(getPayAmount1());
            desc.append("  ");
        }
        if (!StringUtils.isEmpty(payName = getPayName2())
                && !(getPayNumber2().equals(rebateID))) {
            desc.append(payName);
            desc.append(':');
            desc.append(getPayAmount2());
            desc.append("  ");
        }

        if (!StringUtils.isEmpty(payName = getPayName3())
                && !(getPayNumber3().equals(rebateID))) {
            desc.append(payName);
            desc.append(':');
            desc.append(getPayAmount3());
            desc.append("  ");
        }
        if (!StringUtils.isEmpty(payName = getPayName4())
                && !(getPayNumber4().equals(rebateID))) {
            desc.append(payName);
            desc.append(':');
            desc.append(getPayAmount4());
            desc.append("  ");
        }
        return desc.toString();
    }

    /**
     * 看是否要排除还圆金支付的部分
     * 
     * @return 发票显示合计金额
     */
    public HYIDouble getPrintNetSaleAmount() {
        HYIDouble netSaleAmt = getNetSalesAmount();
        /*
         * if (excudeRebateInSum) { HYIDouble useRebateAmt = getUseRebateAmt();
         * netSaleAmt = netSaleAmt.subtract(useRebateAmt); }
         */
        return netSaleAmt;
    }

    public HYIDouble getPayAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("payAmount1");
        else
            return (HYIDouble) getFieldValue("PAYAMT1");
    }

    public void setPayAmount1(HYIDouble payamt1) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("payAmount1", payamt1);
        else
            setFieldValue("PAYAMT1", payamt1);
    }

    public HYIDouble getPayAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("payAmount2");
        else
            return (HYIDouble) getFieldValue("PAYAMT2");
    }

    public void setPayAmount2(HYIDouble payamt2) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("payAmount2", payamt2);
        else
            setFieldValue("PAYAMT2", payamt2);
    }

    public HYIDouble getPayAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("payAmount3");
        else
            return (HYIDouble) getFieldValue("PAYAMT3");
    }

    public void setPayAmount3(HYIDouble payamt3) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("payAmount3", payamt3);
        else
            setFieldValue("PAYAMT3", payamt3);
    }

    public HYIDouble getPayAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("payAmount4");
        else
            return (HYIDouble) getFieldValue("PAYAMT4");
    }

    public void setPayAmount4(HYIDouble payamt4) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("payAmount4", payamt4);
        else
            setFieldValue("PAYAMT4", payamt4);
    }

    // payCash means: payNumber = 00
    public HYIDouble getPayCash() {
        String s = "";
        if (hyi.cream.inline.Server.serverExist()) {
            if (getPayNumber1() != null && getPayNumber1().equals("00")) {
                s = "payAmount1";
            } else if (getPayNumber2() != null && getPayNumber2().equals("00")) {
                s = "payAmount2";
            } else if (getPayNumber3() != null && getPayNumber3().equals("00")) {
                s = "payAmount3";
            } else if (getPayNumber4() != null && getPayNumber4().equals("00")) {
                s = "payAmount4";
            }
        } else {
            if (getPayNumber1() != null && getPayNumber1().equals("00")) {
                s = "PAYAMT1";
            } else if (getPayNumber2() != null && getPayNumber2().equals("00")) {
                s = "PAYAMT2";
            } else if (getPayNumber3() != null && getPayNumber3().equals("00")) {
                s = "PAYAMT3";
            } else if (getPayNumber4() != null && getPayNumber4().equals("00")) {
                s = "PAYAMT4";
            }
        }
        return (HYIDouble) getFieldValue(s);
    }

    public void setPayCash(HYIDouble payCash) {
        String s = "";
        if (hyi.cream.inline.Server.serverExist()) {
            if (getPayNumber1() != null && getPayNumber1().equals("00")) {
                s = "payAmount1";
            } else if (getPayNumber2() != null && getPayNumber2().equals("00")) {
                s = "payAmount2";
            } else if (getPayNumber3() != null && getPayNumber3().equals("00")) {
                s = "payAmount3";
            } else if (getPayNumber4() != null && getPayNumber4().equals("00")) {
                s = "payAmount4";
            }
        } else {
            if (getPayNumber1() != null && getPayNumber1().equals("00")) {
                s = "PAYAMT1";
            } else if (getPayNumber2() != null && getPayNumber2().equals("00")) {
                s = "PAYAMT2";
            } else if (getPayNumber3() != null && getPayNumber3().equals("00")) {
                s = "PAYAMT3";
            } else if (getPayNumber4() != null && getPayNumber4().equals("00")) {
                s = "PAYAMT4";
            }
        }
        setFieldValue(s, payCash);
    }

    public HYIDouble getTaxAmount0() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("tax0Amount");
        else
            return (HYIDouble) getFieldValue("TAXAMT0");
    }

    public void setTaxAmount0(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("tax0Amount", amt);
        else
            setFieldValue("TAXAMT0", amt);
    }

    public HYIDouble getTaxAmount1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("tax1Amount");
        else
            return (HYIDouble) getFieldValue("TAXAMT1");
    }

    public void setTaxAmount1(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("tax1Amount", amt);
        else
            setFieldValue("TAXAMT1", amt);
    }

    public HYIDouble getTaxAmount2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("tax2Amount");
        else
            return (HYIDouble) getFieldValue("TAXAMT2");
    }

    public void setTaxAmount2(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("tax2Amount", amt);
        else
            setFieldValue("TAXAMT2", amt);
    }

    public HYIDouble getTaxAmount3() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("tax3Amount");
        else
            return (HYIDouble) getFieldValue("TAXAMT3");
    }

    public void setTaxAmount3(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("tax3Amount", amt);
        else
            setFieldValue("TAXAMT3", amt);
    }

    public HYIDouble getTaxAmount4() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("tax4Amount");
        else
            return (HYIDouble) getFieldValue("TAXAMT4");
    }

    public void setTaxAmount4(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("tax4Amount", amt);
        else
            setFieldValue("TAXAMT4", amt);
    }
    public HYIDouble getTaxAmount5() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("tax5Amount");
        else
            return (HYIDouble) getFieldValue("TAXAMT5");
    }

    public void setTaxAmount5(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("tax5Amount", amt);
        else
            setFieldValue("TAXAMT5", amt);
    }
    public HYIDouble getTaxAmount6() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("tax6Amount");
        else
            return (HYIDouble) getFieldValue("TAXAMT6");
    }

    public void setTaxAmount6(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("tax6Amount", amt);
        else
            setFieldValue("TAXAMT6", amt);
    }
    public HYIDouble getTaxAmount7() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("tax7Amount");
        else
            return (HYIDouble) getFieldValue("TAXAMT7");
    }

    public void setTaxAmount7(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("tax7Amount", amt);
        else
            setFieldValue("TAXAMT7", amt);
    }
    public HYIDouble getTaxAmount8() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("tax8Amount");
        else
            return (HYIDouble) getFieldValue("TAXAMT8");
    }

    public void setTaxAmount8(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("tax8Amount", amt);
        else
            setFieldValue("TAXAMT8", amt);
    }
    public HYIDouble getTaxAmount9() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("tax9Amount");
        else
            return (HYIDouble) getFieldValue("TAXAMT9");
    }

    public void setTaxAmount9(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("tax9Amount", amt);
        else
            setFieldValue("TAXAMT9", amt);
    }
    public HYIDouble getTaxAmount10() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("tax10Amount");
        else
            return (HYIDouble) getFieldValue("TAXAMT10");
    }

    public void setTaxAmount10(HYIDouble amt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("tax10Amount", amt);
        else
            setFieldValue("TAXAMT10", amt);
    }

    public HYIDouble getChangeAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("changeAmount");
        else
            return (HYIDouble) getFieldValue("CHANGEAMT");
    }

    public void setChangeAmount(HYIDouble change) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("changeAmount", change);
        else
            setFieldValue("CHANGEAMT", change);
    }

    public HYIDouble getSpillAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("spillAmount");
        else
            return (HYIDouble) getFieldValue("OVERAMT");
    }

    public void setSpillAmount(HYIDouble spillAmount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("spillAmount", spillAmount);
        else
            setFieldValue("OVERAMT", spillAmount);
    }

    // 其他

    public String getCreditCardType() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("creditCardType");
        else
            return (String) getFieldValue("CRDTYPE");
    }

    public void setCreditCardType(String crdtype) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("creditCardType", crdtype);
        else
            setFieldValue("CRDTYPE", crdtype);
    }

    public String getCreditCardNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("creditCardNumber");
        else
            return (String) getFieldValue("CRDNO");
    }

    public void setCreditCardNumber(String crdno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("creditCardNumber", crdno);
        else
            setFieldValue("CRDNO", crdno);
    }

    public String getPayCardNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("payCardNumber");
        else
            return (String) getFieldValue("PAYCRDNO");
    }

    public void setPayCardNumber(String paycardno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("payCardNumber", paycardno);
        else
            setFieldValue("PAYCRDNO", paycardno);
    }
    
    public java.util.Date getCreditCardExpireDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.util.Date) getFieldValue("creditCardExpireDate");
        else
            return (java.util.Date) getFieldValue("CRDEND");
    }

    public void setCreditCardExpireDate(java.util.Date crdend) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("creditCardExpireDate", crdend);
        else
            setFieldValue("CRDEND", crdend);
    }

    public String getEmployeeNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("employeeNumber");
        else
            return (String) getFieldValue("EMPNO");
    }

    public void setEmployeeNumber(String empno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("employeeNumber", empno);
        else
            setFieldValue("EMPNO", empno);
    }

    public String getMemberIDShort() {
        String memberCardID = getMemberID();
        //String memberCardID = "2991123456781";
        int prefixLen = PARAM.getMemberCardIDPrefix().length();
        if (memberCardID == null){
            return null;
        } else if (memberCardID.length() <= prefixLen){
            return "";
        } else{
            // 最少的5位
            return memberCardID.substring(prefixLen, memberCardID.length() -1);
        }
    }
    
    public String getMemberID() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("memberID");
        else
            return (String) getFieldValue("MEMBERID");
    }

    public void setMemberID(String memberID) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("memberID", memberID);
        else
            setFieldValue("MEMBERID", memberID);
    }

    // 会员有效期
    public java.util.Date getMemberEndDate() {

        if (hyi.cream.inline.Server.serverExist())
            return (java.util.Date) getFieldValue("endDate");
        else
            return (java.util.Date) getFieldValue("ENDDATE");
    }

    public void setMemberEndDate(java.util.Date enddate) {

        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("endDate", enddate);
        else
            setFieldValue("ENDDATE", enddate);
    }

    /*
     * public HYIDouble getPayinAmount() { return
     * (HYIDouble)getFieldValue("RCVTMP"); } public void
     * setPayinAmount(HYIDouble rcvtmp) { setFieldValue("RCVTMP", rcvtmp); }
     */

    public HYIDouble getExchangeDifference() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("exchangeDifference");
        else
            return (HYIDouble) getFieldValue("CHANGAMT");
    }

    public void setExchangeDifference(HYIDouble changamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("exchangeDifference", changamt);
        else
            setFieldValue("CHANGAMT", changamt);
    }

    public String getAuthoringNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("authoringNumber");
        else
            return (String) getFieldValue("AUTHORNO");
    }

    public void setAuthoringNumber(String authorno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("authoringNumber", authorno);
        else
            setFieldValue("AUTHORNO", authorno);
    }

    public HYIDouble getRebateChangeAmount() {
        return (HYIDouble) getFieldValue("rebateChangeAmount");
    }

    public void setRebateChangeAmount(HYIDouble rebateChangeAmt) {
        setFieldValue("rebateChangeAmount", rebateChangeAmt.setScale(
                rebateAmountScale, BigDecimal.ROUND_HALF_UP));
    }

    public void setItemsRebateAmount(HYIDouble itemsRebateAmt) {

        itemRebateAmt = itemsRebateAmt.setScale(rebateAmountScale,
                BigDecimal.ROUND_HALF_UP);
        ;
    }

    public HYIDouble getItemsRebateAmount() {
        return itemRebateAmt;
    }

    public void setSpecialRebateAmount(HYIDouble splRebateAmt) {
        specialRebateAmt = splRebateAmt.setScale(rebateAmountScale,
                BigDecimal.ROUND_HALF_UP);
        ;
    }

    public HYIDouble getSpecialRebateAmount() {
        return specialRebateAmt;
    }

    // 新增的还原金 = 商品累计还圆金 + 额外的还原金
    public HYIDouble getRebateAmount() {
        return (HYIDouble) getFieldValue("rebateAmount");
    }

    public void setRebateAmount(HYIDouble rebateAmt) {
        setFieldValue("rebateAmount", rebateAmt.setScale(rebateAmountScale,
                BigDecimal.ROUND_HALF_UP));
    }

    // 本次合计还圆金（以后可能会包含还圆金找零）
    public HYIDouble getTotalRebateAmount() {
        return (HYIDouble) getFieldValue("totalRebateAmount");
    }

    public void setTotalRebateAmount(HYIDouble totalRebateAmt) {
        setFieldValue("totalRebateAmount", totalRebateAmt.setScale(
                rebateAmountScale, BigDecimal.ROUND_HALF_UP));
    }

    public void setBeforeTotalRebateAmt(HYIDouble amt) {
        beforeTotalRebate = amt.setScale(rebateAmountScale,
                BigDecimal.ROUND_HALF_UP);
    }

    public HYIDouble getBeforeTotalRebateAmt() {
        return beforeTotalRebate;
    }

    public void setUseRebateAmt(HYIDouble amt) {
        useRebateAmt = amt;
    }

    public HYIDouble getUseRebateAmt() {
        return useRebateAmt;
    }

    // 根据此判断是否要打印还元金积分信息
    public String getNeedPrintRebate() {
        HYIDouble big0 = new HYIDouble(0);
        if (getRebateAmount().compareTo(big0) != 0
                || getUseRebateAmt().compareTo(big0) != 0
                || getBeforeTotalRebateAmt().compareTo(big0) != 0)

            return "true";

        return null;
    }

    public boolean isPureDaiShou2() {
        // TODO 这样判断准确吗？
        boolean isPureDaiShou2 = true; // 是否是纯代收交易
        Iterator itr = getLineItemsIterator();
        LineItem li;
        while (itr.hasNext()) {
            li = (LineItem) itr.next();
            if (!li.getRemoved()
                    && (li.getDetailCode().equals("S") || li.getDetailCode()
                            .equals("R"))) {
                isPureDaiShou2 = false;
                break;
            }
        }
        return isPureDaiShou2;
    }
    
    public boolean isIncludedDaiShou2() {
        ArrayList displayedLineItems = getDisplayedLineItemsArray();
        if (displayedLineItems.size() == 0) {
            return false;
        } else {
            for (Object o : displayedLineItems){
                LineItem li = (LineItem)o;
                if (!li.getRemoved() && "O".equals(li.getDetailCode())){
                    return true;
                }
            }
            return false;
        }
    }
    
    public Map<String, HYIDouble> getAppliedMMs() {
        return appliedMMs;
    }

    public void clearAppliedMMs() {
        appliedMMs.clear();
    }

    public void addAppliedMM(String mmId, HYIDouble mmAmount) {
        if (mmAmount == null || mmId == null) {
            appliedMMs.clear();
            return;
        }
        appliedMMs.put(mmId, mmAmount);
    }

    /**
     * Reserve this for old receipt.conf.
     */
    public Map<SI, HYIDouble> getSIAmtList() {
        return appliedSIs;
    }

    public Map<SI, HYIDouble> getAppliedSIs() {
        return appliedSIs;
    }

    public void clearAppliedSIs() {
        appliedSIs.clear();
        lastSI = null;
    }

    public void addAppliedSI(SI si, HYIDouble siAmount) {
        if (siAmount == null || si == null)
            return;
        appliedSIs.put(si, siAmount);
        lastSI = si;
    }

    public SI getAppliedSIById(String siId) {
        for (SI si : appliedSIs.keySet()) {
            if (si.getSIID().equals(siId))
                return si;
        }
        return null;
    }

    public HYIDouble getSIDiscountAmountById(String siId) {
        for (SI si : appliedSIs.keySet()) {
            if (si.getSIID().equals(siId))
                return appliedSIs.get(si);
        }
        return null;
    }

    public SI getLastSI() {
        return lastSI;
    }

    // get 余额
    public HYIDouble getBalance(String paymentID) {
        HYIDouble balance = new HYIDouble(0);
        HYIDouble payAmount = new HYIDouble(0);
        // HYIDouble Amount = new HYIDouble(0);
        String payno = "";
        String payamt = "";
        for (int i = 1; i < 5; i++) {
            if (hyi.cream.inline.Server.serverExist()) {
                payno = "PayID" + i;
                payamt = "PayAmount" + i;
            } else {
                payno = "PAYNO" + i;
                payamt = "PAYAMT" + i;
            }
            if (getFieldValue(payno) == null) {
                break;
            }
            if (((String) getFieldValue(payno)).equals(paymentID)
                    && getFieldValue(payamt) != null) {
                payAmount = payAmount.addMe((HYIDouble) getFieldValue(payamt));
            }
        }
        balance = getSalesAmount().subtract(payAmount);
        return balance;
    }

    /** 未付金额 */
    public HYIDouble getBalance() {
        try {
            HYIDouble balance = nHYIDouble0();
            balance.addMe(getSalesAmount())
                .subtractMe(getTotalPaymentAmount())
                .addMe(getChangeAmount())
                .addMe(getSpillAmount());

            // Bruce/2003-12-17
            POSTerminalApplication app = POSTerminalApplication.getInstance();
            if ("X".equals(getState1())) // 如果是Nitori退着付殘金交易, 就讓銷售值為負的，不要動
                return balance;
            if (!app.getReturnItemState() && !getDealType2().equals("4") && balance.compareTo(nHYIDouble0()) < 0
                || app.getReturnItemState() && balance.compareTo(nHYIDouble0()) > 0) {
                balance = nHYIDouble0();
            }
            return balance;
        } catch (NullPointerException e) {
            return nHYIDouble0();
        }
    }

    /**
     * 用于赊账结算
     * @return
     */
    public HYIDouble getSheBalance() {
        HYIDouble balance = new HYIDouble("0");
        HYIDouble payAmount = new HYIDouble("0");
        String payno = "";
        String payamt = "";
        for (int i = 1; i < 5; i++) {
            if (hyi.cream.inline.Server.serverExist()) {
                payno = "PayID" + i;
                payamt = "PayAmount" + i;
            } else {
                payno = "PAYNO" + i;
                payamt = "PAYAMT" + i;
            }
            if (getFieldValue(payno) == null) {
                break;
            }
            if (getFieldValue(payamt) != null) {
                payAmount = payAmount.addMe((HYIDouble) getFieldValue(payamt));
            }
        }
        //balance = getPreRcvAmount().subtract(payAmount);
        balance = (getOriginEarnAmount() == null ? new HYIDouble(0)
                : getOriginEarnAmount()).subtract(payAmount);

        POSTerminalApplication app = POSTerminalApplication.getInstance();
        if (!app.getReturnItemState() && !getDealType2().equals("4") && balance.compareTo(nHYIDouble0()) < 0
                || app.getReturnItemState()
                && balance.compareTo(nHYIDouble0()) > 0) {
            balance = nHYIDouble0();
        }
        return balance;
    }

    public HYIDouble getItemsAmount() {
        Object objs[] = getLineItems();
        int l = objs.length;
        if (l == 0) {
            return nHYIDouble0();
        }
        LineItem item = null;
        String detailCode = "";
        HYIDouble amount = nHYIDouble0();
        for (int i = 0; i < l; i++) {
            item = (LineItem) objs[i];

            // check removed
            if (item.getRemoved()) {
                continue;
            }

            /*
             * check DetailCode: "S":销售 "D":SI折扣 "T":ItemDiscount折扣 "M":Mix &
             * Match折扣 "I":代收 "O":营业外商品 "V":指定更正 "E":立即更正
             */
            detailCode = item.getDetailCode();
            if (detailCode.equalsIgnoreCase("E")
                    || detailCode.equalsIgnoreCase("V")
                    || detailCode.equalsIgnoreCase("O")
                    || detailCode.equalsIgnoreCase("I")) {
                continue;
            }

            // accumulation item amount
            amount = amount.addMe(item.getAmount());
        }
        return amount;
    }

    /** ************************************************************************** */

    /**
     * 取得折扣的说明文字。
     * 
     * @return 各种折扣类型名称＋折扣金额
     */
    public String getSIDescription() {
        StringBuilder sb = new StringBuilder();
        for (SI si : getAppliedSIs().keySet()) {
            sb.append(si.getPrintName());
            sb.append(":");
            sb.append(getAppliedSIs().get(si).toString());
            sb.append("  ");
        }
        return sb.toString();
    }

    public HYIDouble getTotalSIAmount() {
        HYIDouble total = nHYIDouble0();
        for (HYIDouble siAmount : getAppliedSIs().values()) {
            total = total.addMe(siAmount);
        }
        return total;
    }

    // SIPercentPlusTotalAmount HYIDouble N 各种税别SI总加成金额
    public HYIDouble getSIPercentPlusTotalAmount() {
        HYIDouble total = nHYIDouble0();
        total = total.addMe(getSIPercentPlusAmount0());
        total = total.addMe(getSIPercentPlusAmount1());
        total = total.addMe(getSIPercentPlusAmount2());
        total = total.addMe(getSIPercentPlusAmount3());
        total = total.addMe(getSIPercentPlusAmount4());
        total = total.addMe(getSIPercentPlusAmount5());
        total = total.addMe(getSIPercentPlusAmount6());
        total = total.addMe(getSIPercentPlusAmount7());
        total = total.addMe(getSIPercentPlusAmount8());
        total = total.addMe(getSIPercentPlusAmount9());
        total = total.addMe(getSIPercentPlusAmount10());
        return total;
    }

    // SIPercentOffTotalAmount HYIDouble N 各种税别 SI总折扣金额
    public HYIDouble getSIPercentOffTotalAmount() {
        HYIDouble total = nHYIDouble0();
        total = total.addMe(getSIPercentOffAmount0());
        total = total.addMe(getSIPercentOffAmount1());
        total = total.addMe(getSIPercentOffAmount2());
        total = total.addMe(getSIPercentOffAmount3());
        total = total.addMe(getSIPercentOffAmount4());
        total = total.addMe(getSIPercentOffAmount5());
        total = total.addMe(getSIPercentOffAmount6());
        total = total.addMe(getSIPercentOffAmount7());
        total = total.addMe(getSIPercentOffAmount8());
        total = total.addMe(getSIPercentOffAmount9());
        total = total.addMe(getSIPercentOffAmount10());
        return total;
    }

    // SIDiscountTotalAmount HYIDouble N 各种税别总折让金额
    public HYIDouble getSIDiscountTotalAmount() {
        HYIDouble total = nHYIDouble0();
        total = total.addMe(getSIDiscountAmount0());
        total = total.addMe(getSIDiscountAmount1());
        total = total.addMe(getSIDiscountAmount2());
        total = total.addMe(getSIDiscountAmount3());
        total = total.addMe(getSIDiscountAmount4());
        total = total.addMe(getSIDiscountAmount5());
        total = total.addMe(getSIDiscountAmount6());
        total = total.addMe(getSIDiscountAmount7());
        total = total.addMe(getSIDiscountAmount8());
        total = total.addMe(getSIDiscountAmount9());
        total = total.addMe(getSIDiscountAmount10());
        return total;
    }

    // MixAndMatchTotalAmount HYIDouble N 各种税别 M&M折让金额
    public HYIDouble getMixAndMatchTotalAmount() {
        HYIDouble total = new HYIDouble(0);
        if (getMixAndMatchAmount0() != null) {
            total = total.addMe(getMixAndMatchAmount0());
        }
        if (getMixAndMatchAmount1() != null) {
            total = total.addMe(getMixAndMatchAmount1());
        }
        if (getMixAndMatchAmount2() != null) {
            total = total.addMe(getMixAndMatchAmount2());
        }
        if (getMixAndMatchAmount3() != null) {
            total = total.addMe(getMixAndMatchAmount3());
        }
        if (getMixAndMatchAmount4() != null) {
            total = total.addMe(getMixAndMatchAmount4());
        }
        if (getMixAndMatchAmount5() != null) {
            total = total.addMe(getMixAndMatchAmount5());
        }
        if (getMixAndMatchAmount6() != null) {
            total = total.addMe(getMixAndMatchAmount6());
        }
        if (getMixAndMatchAmount7() != null) {
            total = total.addMe(getMixAndMatchAmount7());
        }
        if (getMixAndMatchAmount8() != null) {
            total = total.addMe(getMixAndMatchAmount8());
        }
        if (getMixAndMatchAmount9() != null) {
            total = total.addMe(getMixAndMatchAmount9());
        }
        if (getMixAndMatchAmount10() != null) {
            total = total.addMe(getMixAndMatchAmount10());
        }
        return total;
    }

    // NotIncludedTotalSales HYIDouble N 各种税别已开发票支付金额
    public HYIDouble getNotIncludedTotalSales() {
        HYIDouble total = nHYIDouble0();
        total = total.addMe(getNotIncludedSales0());
        total = total.addMe(getNotIncludedSales1());
        total = total.addMe(getNotIncludedSales2());
        total = total.addMe(getNotIncludedSales3());
        total = total.addMe(getNotIncludedSales4());
        total = total.addMe(getNotIncludedSales5());
        total = total.addMe(getNotIncludedSales6());
        total = total.addMe(getNotIncludedSales7());
        total = total.addMe(getNotIncludedSales8());
        total = total.addMe(getNotIncludedSales9());
        total = total.addMe(getNotIncludedSales10());
        return total;
    }

    // NetSalesTotalAmount HYIDouble N 各种税别发票金额

    public HYIDouble getNetSalesTotalAmount() {
        HYIDouble total = nHYIDouble0();
        total = total.addMe(getNetSalesAmount0());
        total = total.addMe(getNetSalesAmount1());
        total = total.addMe(getNetSalesAmount2());
        total = total.addMe(getNetSalesAmount3());
        total = total.addMe(getNetSalesAmount4());
        total = total.addMe(getNetSalesAmount5());
        total = total.addMe(getNetSalesAmount6());
        total = total.addMe(getNetSalesAmount7());
        total = total.addMe(getNetSalesAmount8());
        total = total.addMe(getNetSalesAmount9());
        total = total.addMe(getNetSalesAmount10());
        return total;
    }

    public HYIDouble getPaidInAmount() {
        if (this.getDealType2().equals("4")) {
            return this.getDaiShouAmount();
        } else {
            return nHYIDouble0();
        }
    }

    public HYIDouble getPaidOutAmount() {
        if (this.getDealType2().equals("4")) {
            return this.getDaiFuAmount();
        } else {
            return nHYIDouble0();
        }
    }

    public void setTotalMMAmount(HYIDouble amount) {
        totalMMAmount = amount;
    }

    public HYIDouble getTotalMMAmount() {
        HYIDouble amount = null;
        /*
         * Meyer / 2003-02-26 / Modified
         */
        if (mixAndMatchVersion == null || mixAndMatchVersion.trim().equals("")
                || mixAndMatchVersion.trim().equals("1")
                || mixAndMatchVersion.trim().equals("3")) {

            // old version
            if (totalMMAmount == null) {
                amount = this.getTaxMMAmount();
            } else {
                return totalMMAmount;
            }

        } else if (mixAndMatchVersion.trim().equals("2")) {
            // version 2
            amount = this.getMixAndMatchTotalAmount();
            if (amount == null) {
                amount = this.getTaxMMAmount();
            }
        }
        return (HYIDouble) (amount.setScale(2, BigDecimal.ROUND_HALF_DOWN).clone());
    }

    public HYIDouble getDisplayTotalMMAmount() {
        HYIDouble amount = getTotalMMAmount();
        if (this.getTotalDIYOverrideAmount() != null)
            amount = amount.subtract(getTotalDIYOverrideAmount());
        return amount.setScale(2, BigDecimal.ROUND_HALF_DOWN).absMe();
    }

    /**
     * 将组合促销交易明细加入 mmMap(mmID-mmLineItem),
     * 由store()负责从mmMap中取出记录，加入到trans中，更新到数据库 每一个mmID对应一笔mmLineItem，
     */
    public void setMMDetail(String mmID, LineItem mmLineItem) {
        if (mmLineItem.getUnitPrice() != null
                && mmLineItem.getUnitPrice().compareTo(nHYIDouble0()) == 1) {
            mmMap.put(mmID, mmLineItem);
        }
    }

    /**
     * 清空组合促销交易明细Map - mmMap, 清空taxMMAmount
     */
    public void clearMMDetail() {
        mmMap.clear();
        taxMMAmount.clear();
    }

    /**
     * 将舍去的分放入roundDownItem 由store()负责把roundDownItem加入到trans中，更新到数据库
     */
    public void setRoundDownItem(LineItem roundDownItem) {
        this.roundDownItem = roundDownItem;
        try {
            this.addLineItem(roundDownItem, false);
        } catch (TooManyLineItemsException e) {
        }
    }

    public LineItem getRoundDownItem() {
        return roundDownItem;
    }

    /**
     * 清空组合促销交易明细Map - mmMap, 清空taxMMAmount
     */
    public void clearRoundDownDetail() {
        if (roundDownItem != null) {
            lineItems.remove(roundDownItem);
            if (roundDownItem == this.currentLineItem)
                currentLineItem = null;
            roundDownItem = null;
        }

        fireEvent(new TransactionEvent(this, TransactionEvent.ITEM_REMOVED));
    }

    private static String[][] posToScFieldNameArray;

    /**
     * Meyer/2003-02-20 return fieldName map of PosToSc as String[][]
     */
    public static String[][] getPosToScFieldNameArray() {
        if (posToScFieldNameArray == null) {
            posToScFieldNameArray = new String[][] {
                { "STORENO", "storeID" },
                { "SYSDATE", "systemDate" },
                { "POSNO", "posNumber" },
                { "TMTRANSEQ", "transactionNumber" },
                { "SIGNONID", "shiftCount" },
                { "EODCNT", "zSequenceNumber" },
                { "ACCDATE", "accountDate" },
                { "TRANTYPE", "transactionType" },
                { "DEALTYPE1", "dealType1" },
                { "DEALTYPE2", "dealType2" },
                { "DEALTYPE3", "dealType3" },
                { "VOIDSEQ", "voidTransactionNumber" },
                { "TMCODEP", "machineNumber" },
                { "CASHIER", "cashier" },
                { "INVNOHEAD", "invoiceHead" },
                { "INVNO", "invoiceNumber" },
                { "INVCNT", "invoiceCount" },
//              { "INVAMT", "invoiceAmount" },
                { "IDNO", "idNumber" },
                { "DETAILCNT", "detailCount" },
                { "CUSTCNT", "customerCount" },
                { "INOUT", "inOurID" },
                { "CUSTID", "customerCategory" },
                { "SALEMAN", "saleMan" },
                { "GROSALAMT", "grossSaleTotalAmount" },
                { "GROSALTX0AMT", "grossSaleTax0Amount" },
                { "GROSALTX1AMT", "grossSaleTax1Amount" },
                { "GROSALTX2AMT", "grossSaleTax2Amount" },
                { "GROSALTX3AMT", "grossSaleTax3Amount" },
                { "GROSALTX4AMT", "grossSaleTax4Amount" },
                { "GROSALTX5AMT", "grossSaleTax5Amount" },
                { "GROSALTX6AMT", "grossSaleTax6Amount" },
                { "GROSALTX7AMT", "grossSaleTax7Amount" },
                { "GROSALTX8AMT", "grossSaleTax8Amount" },
                { "GROSALTX9AMT", "grossSaleTax9Amount" },
                { "GROSALTX10AMT", "grossSaleTax10Amount" },
                { "SIPercentPlusTotalAmount", "siPlusTotalAmount" },
                { "SIPLUSAMT0", "siPlusTax0Amount" },
                { "SIPLUSAMT1", "siPlusTax1Amount" },
                { "SIPLUSAMT2", "siPlusTax2Amount" },
                { "SIPLUSAMT3", "siPlusTax3Amount" },
                { "SIPLUSAMT4", "siPlusTax4Amount" },
                { "SIPLUSAMT5", "siPlusTax5Amount" },
                { "SIPLUSAMT6", "siPlusTax6Amount" },
                { "SIPLUSAMT7", "siPlusTax7Amount" },
                { "SIPLUSAMT8", "siPlusTax8Amount" },
                { "SIPLUSAMT9", "siPlusTax9Amount" },
                { "SIPLUSAMT10", "siPlusTax10Amount" },
                { "SIPercentOffTotalAmount", "discountTotalAmount" },
                { "SIPAMT0", "discountTax0Amount" },
                { "SIPAMT1", "discountTax1Amount" },
                { "SIPAMT2", "discountTax2Amount" },
                { "SIPAMT3", "discountTax3Amount" },
                { "SIPAMT4", "discountTax4Amount" },
                { "SIPAMT5", "discountTax5Amount" },
                { "SIPAMT6", "discountTax6Amount" },
                { "SIPAMT7", "discountTax7Amount" },
                { "SIPAMT8", "discountTax8Amount" },
                { "SIPAMT9", "discountTax9Amount" },
                { "SIPAMT10", "discountTax10Amount" },
                { "MixAndMatchTotalAmount", "mixAndMatchTotalAmount" },
                { "MNMAMT0", "mixAndMatchTax0Amount" },
                { "MNMAMT1", "mixAndMatchTax1Amount" },
                { "MNMAMT2", "mixAndMatchTax2Amount" },
                { "MNMAMT3", "mixAndMatchTax3Amount" },
                { "MNMAMT4", "mixAndMatchTax4Amount" },
                { "MNMAMT5", "mixAndMatchTax5Amount" },
                { "MNMAMT6", "mixAndMatchTax6Amount" },
                { "MNMAMT7", "mixAndMatchTax7Amount" },
                { "MNMAMT8", "mixAndMatchTax8Amount" },
                { "MNMAMT9", "mixAndMatchTax9Amount" },
                { "MNMAMT10", "mixAndMatchTax10Amount" },
                { "NotIncludedTotalSales", "notIncludedTotalSale" },
                { "RCPGIFAMT0", "notIncludedTax0Sale" },
                { "RCPGIFAMT1", "notIncludedTax1Sale" },
                { "RCPGIFAMT2", "notIncludedTax2Sale" },
                { "RCPGIFAMT3", "notIncludedTax3Sale" },
                { "RCPGIFAMT4", "notIncludedTax4Sale" },
                { "RCPGIFAMT5", "notIncludedTax5Sale" },
                { "RCPGIFAMT6", "notIncludedTax6Sale" },
                { "RCPGIFAMT7", "notIncludedTax7Sale" },
                { "RCPGIFAMT8", "notIncludedTax8Sale" },
                { "RCPGIFAMT9", "notIncludedTax9Sale" },
                { "RCPGIFAMT10", "notIncludedTax10Sale" },
                { "NETSALAMT", "netSaleTotalAmount" },
                { "NETSALAMT0", "netSaleTax0Amount" },
                { "NETSALAMT1", "netSaleTax1Amount" },
                { "NETSALAMT2", "netSaleTax2Amount" },
                { "NETSALAMT3", "netSaleTax3Amount" },
                { "NETSALAMT4", "netSaleTax4Amount" },
                { "NETSALAMT5", "netSaleTax5Amount" },
                { "NETSALAMT6", "netSaleTax6Amount" },
                { "NETSALAMT7", "netSaleTax7Amount" },
                { "NETSALAMT8", "netSaleTax8Amount" },
                { "NETSALAMT9", "netSaleTax9Amount" },
                { "NETSALAMT10", "netSaleTax10Amount" },
                { "DAISHOUAMT", "daiShouAmount" },
                { "DAISHOUAMT2", "daiShouAmount2" },
                { "DAIFUAMT", "daiFuAmount" },
                { "rebateChangeAmount", "rebateChangeAmount" },
                { "rebateAmount", "rebateAmount" },
                { "totalRebateAmount", "totalRebateAmount" },
                { "PAYNO1", "payID1" }, { "PAYNO2", "payID2" },
                { "PAYNO3", "payID3" }, { "PAYNO4", "payID4" },
                { "PAYAMT1", "payAmount1" }, { "PAYAMT2", "payAmount2" },
                { "PAYAMT3", "payAmount3" },
                { "PAYAMT4", "payAmount4" },
                { "TAXAMT0", "tax0Amount" },
                { "TAXAMT1", "tax1Amount" },
                { "TAXAMT2", "tax2Amount" },
                { "TAXAMT3", "tax3Amount" },
                { "TAXAMT4", "tax4Amount" },
                { "TAXAMT5", "tax5Amount" },
                { "TAXAMT6", "tax6Amount" },
                { "TAXAMT7", "tax7Amount" },
                { "TAXAMT8", "tax8Amount" },
                { "TAXAMT9", "tax9Amount" },
                { "TAXAMT10", "tax10Amount" },
                { "CHANGEAMT", "changeAmount" },
                { "OVERAMT", "spillAmount" },
                { "CHANGAMT", "exchangeDifference" },
                { "CRDTYPE", "creditCardType" },
                { "CRDNO", "creditCardNumber" },
                { "CRDEND", "creditCardExpireDate" },
                { "EMPNO", "employeeNumber" }, { "MEMBERID", "memberID" },
                { "ENDDATE", "endDate" }, { "AUTHORNO", "authoringNumber" },
                // lxf /20030401 维修
                { "ANNOTATEDID", "annotatedID" },
                { "ANNOTATEDTYPE", "annotatedType" },
                { "PRERCVAMOUNT", "preRcvAmount" },
                { "state", "state" },
                { "weixin_amt", "weixin_amt" },
                { "selfbuyOrderid", "selfbuyOrderid" },
                { "selfbuyOrderno", "selfbuyOrderno" },

//                { "PAYCRDNO", "payCardNumber" },
            };
        }

        return posToScFieldNameArray;
    }

    public Object[] cloneForSC() {
        int i;
        String[][] fieldNameMap = getPosToScFieldNameArray();
        List dacReturned = new ArrayList();

        Transaction clonedTransaction = new Transaction();
        for (i = 0; i < fieldNameMap.length; i++) {
            Object value = this.getFieldValue(fieldNameMap[i][0]);
            if (value == null) {
                try {
                    value = getClass().getDeclaredMethod("get" + fieldNameMap[i][0]).invoke(this);
                } catch (Exception e) {
                    value = null;
                }
            }
            clonedTransaction.setFieldValue(fieldNameMap[i][1], value);
        }
        dacReturned.add(clonedTransaction);

        Object[] lineItems = getLineItems();
        if (lineItems != null) {
            for (i = 0; i < lineItems.length; i++) {
                LineItem lineItem = ((LineItem)lineItems[i]).cloneForSC();
                clonedTransaction.addLineItemSimpleVersion(lineItem);
                dacReturned.add(lineItem);
            }
        }
        List<Alipay_detail> alipayLists = getAlipayList();
        if (alipayLists != null && alipayLists.size() > 0) {
            for (Alipay_detail alipay_detail : alipayLists) {
                dacReturned.add(alipay_detail.cloneForSC());
            }
        }
        List<WeiXin_detail> weiXinLists = getWeiXinList();
        if (weiXinLists != null && weiXinLists.size() > 0) {
            for (WeiXin_detail weiXin_detail : weiXinLists) {
                dacReturned.add(weiXin_detail.cloneForSC());
            }
        }
        List<UnionPay_detail> uniontpayLists = getUnionPayList();
        if (uniontpayLists != null && uniontpayLists.size() > 0) {
            for (UnionPay_detail unionpay_detail : uniontpayLists) {
                dacReturned.add(unionpay_detail.cloneForSC());
            }
        }
        List<Selfbuy_head> selfbuyHeadLists = getSelfbuyHeadList();
        if (selfbuyHeadLists != null && selfbuyHeadLists.size() > 0) {
            for (Selfbuy_head selfbuy_head : selfbuyHeadLists) {
                dacReturned.add(selfbuy_head.cloneForSC());
            }
        }
        List<Selfbuy_detail> selfbuyDetailLists = getSelfbuyDetailList();
        if (selfbuyDetailLists != null && selfbuyDetailLists.size() > 0) {
            for (Selfbuy_detail selfbuy_detail : selfbuyDetailLists) {
                dacReturned.add(selfbuy_detail.cloneForSC());
            }
        }
       /* List<EcDeliveryHead> ecDeliveryHeadLists = getEcDeliveryHeadList();
        if (ecDeliveryHeadLists != null && ecDeliveryHeadLists.size() > 0) {
            for (EcDeliveryHead ecDeliveryHead : ecDeliveryHeadLists) {
                dacReturned.add(ecDeliveryHead.cloneForSC());
            }
        }*/
        List<Cmpay_detail> cMpay_details = getCmpayList();
        if (cMpay_details != null && cMpay_details.size() > 0) {
            for (Cmpay_detail cMpay_detail : cMpay_details) {
                dacReturned.add(cMpay_detail.cloneForSC());
            }
        }
        //Bruce/20080903/ Add associated carddetail
        if (getCardDetail() != null)
            dacReturned.add(getCardDetail());

        return dacReturned.toArray();
    }

    /**
     * 只针对 pos 端使用
     * 
     * @return 过期的交易序号
     */
    public static Iterator<String> getOutdateTranNumbers() {
        List<String> numbers = new ArrayList<String>();

        // 需要保留的天数
        DateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
        int tranReserved = PARAM.getTransactionReserved();
        long bound = new java.util.Date().getTime() - tranReserved * 1000L * 3600 * 24;
        String baseTime = yyyyMMdd.format(new java.sql.Date(bound));

        DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(
                "SELECT tmtranseq FROM tranhead WHERE accdate BETWEEN '1980-01-01' AND '"
                + baseTime + "'");
            while (resultSet.next()) {
                numbers.add(resultSet.getString("tmtranseq"));
            }

        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (resultSet != null)
                    resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            CreamToolkit.releaseConnection(connection);
        }
        return numbers.iterator();
    }

    /**
     * 删除过期交易
     */
    public static void deleteOutdatedData() {
        // 需要保留的天数
        int tranReserved = PARAM.getTransactionReserved();
        String today = CreamCache.getInstance().getDateFormate().format(
                new java.util.Date());
        long l = java.sql.Date.valueOf(today).getTime() - tranReserved * 1000L
                * 3600 * 24;
        String baseTime = CreamCache.getInstance().getDateTimeFormate().format(
                new java.sql.Date(l));

        DbConnection connection = null;
        Statement statement = null;
        // ResultSet resultSet = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM "
                    + getInsertUpdateTableNameStaticVersion()
                    + " WHERE "
                    + ((hyi.cream.inline.Server.serverExist()) ? "systemDate"
                            : "SYSDATE") + " < '" + baseTime + "'");
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Meyer/2003-01-22 计算组合促销总计金额
     */
    public HYIDouble getMMTotalAmt() {
        HYIDouble mmTotalAmt = new HYIDouble(0);

        LineItem lineItem; // 交易明细
        Iterator iter = lineItems.iterator();
        while (iter.hasNext()) {
            lineItem = (LineItem) iter.next();
            mmTotalAmt.addMe(lineItem.getAmount()).subtract(
                    lineItem.getAfterDiscountAmount());

            if (currentLineItem != null) {
                mmTotalAmt.addMe(currentLineItem.getAmount()).subtract(
                        currentLineItem.getAfterDiscountAmount());
            }
        }
        return mmTotalAmt;
    }

    public String getPeiDaNumberPrinted() {
        if (getAnnotatedType() != null && getAnnotatedType().equals("P"))
            return "PO: " + getAnnotatedId();
        else
            return "";
    }

    public String getWeiXiuNumber() {
        if (getAnnotatedType() != null && getAnnotatedType().equals("W"))
            return getAnnotatedId();
        else
            return null;
    }

    
    /**
     *      'P'; //普通配達
     *         'M'; //窗簾修改
     *         'O'; //窗簾訂做
     */
    public String getAnnotatedType() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("annotatedType");
        else
            return (String) getFieldValue("ANNOTATEDTYPE");
    }

    public void setAnnotatedType(String annotatedType) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("annotatedType", annotatedType);
        else
            setFieldValue("ANNOTATEDTYPE", annotatedType);
    }

    public boolean isPeiDa() {
        if (getAnnotatedType() == null || getAnnotatedType().trim().equals(""))
            return false;
        if (GetProperty.getAnnotatedTypeList().contains(getAnnotatedType()))
            return true;
        // if (getAnnotatedType().equals("H")
        // || getAnnotatedType().equals("P")
        // || getAnnotatedType().equals("M")
        // || getAnnotatedType().equals("O"))
        // return true;
        return false;
    }
    
    public String getAnnotatedId() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("annotatedId");
        else
            return (String) getFieldValue("ANNOTATEDID");
    }

    /**
     * 20030401 维修
     * 
     * @author ll
     */
    public void setAnnotatedId(String annotatedId) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("annotatedId", annotatedId);
        else
            setFieldValue("ANNOTATEDID", annotatedId);
        // fire event for ScreenBanner display
        if (getAnnotatedType() != null
                && getAnnotatedType().equals("P"))
            fireEvent(new TransactionEvent(this,
                    TransactionEvent.TRANS_INFO_CHANGED));
    }

    // public boolean update() {
    // //if (!hyi.cream.inline.Server.serverExist()) {
    // // CreamProperties prop = GetProperty;
    // // prop.setProperty("NextTransactionNumber", "" +
    // ++nextTransactionNumber);
    // // prop.deposit();
    // // nextTransactionNumber = -1; // let getNextTransactionNumber() reget
    // from table
    // //}
    // return super.update();
    // }

    public void insert(DbConnection connection, boolean needQueryAgain) throws SQLException {
        int nextTransactionNumber = this.getTransactionNumber() + 1;
        if (!hyi.cream.inline.Server.serverExist()) {
            CreamPropertyUtil prop = CreamPropertyUtil.getInstance();
            // nextTransactionNumber++;

            int nextTranNoOrig = PARAM.getNextTransactionNumber();
            if (nextTranNoOrig < nextTransactionNumber) {
                if (nextTransactionNumber > getMaxTranNumberInProperty())
                    nextTransactionNumber = 1;
                PARAM.updateNextTransactionNumber(nextTransactionNumber);
                //// nextTransactionNumber = -1; 
                //// let getNextTransactionNumber() re-get from table
            }
        }
        super.insert(connection, needQueryAgain);
    }

    
    public void update(DbConnection connection) throws SQLException, EntityNotFoundException {
        setAllFieldsDirty(true);
        super.update(connection);
    }

// public boolean isCommit() {
//      return commit;
//  }
//
//  public void setCommit(boolean commit) {
//      this.commit = commit;
//  }

    /**
     * 在交易保存完毕之前 需要等待
     * 
     * @return boolean
     */
    public boolean isStoreComplete() {
        return storeComplete;
    }

    public void setStoreComplete(boolean b) {
        this.storeComplete = b;
    }

    public void clearPaymentInfo() {
        setChangeAmount(new HYIDouble(0));
        setPayCash(new HYIDouble(0));
        setPayNumber1(null);
        setPayNumber2(null);
        setPayNumber3(null);
        setPayNumber4(null);
        setPayAmount1(null);
        setPayAmount2(null);
        setPayAmount3(null);
        setPayAmount4(null);
        addPayment(null);
        setSpillAmount(new HYIDouble(0));
    }

    public String getPrintRMBNetSaleAmount() {
        return getCapitalizationRMB(getPrintNetSaleAmount());
    }

    public String getCapitalizationRMB(HYIDouble amount) {
        String ret = "";
        try {
            HYIDouble val = (HYIDouble) amount.clone();
            val = val.multiply(new HYIDouble(100));
            val = val.setScale(0, BigDecimal.ROUND_HALF_UP);
            String str = String.valueOf(val.intValue());
            // boolean firstLetter = false;
            boolean isZero = true;
            boolean previousIsZero = false;
            // boolean zeroIsSkip = true;
            boolean isSectionStart = false;
            int len = str.length();
            // System.out.println("str : " + str + " | len : " + len);
            for (int i = 0; i < len; i++) {
                char ch = str.charAt(len - i - 1);
                // System.out.println("i : " + i + " | ch : " + ch + " | ret : "
                // + ret);
                switch (i) {
                case 0:
                case 1:
                case 2:
                case 6:
                    isSectionStart = true;
                    break;
                }

                previousIsZero = isZero;
                if (ch == '0')
                    isZero = true;
                else {
                    isZero = false; // 不是0
                    isSectionStart = false;
                }

                String num = "";
                switch (ch) {
                case '0':
                    if (isSectionStart)
                        ;
                    else if (isZero && previousIsZero)
                        ;
                    else
                        num = CreamToolkit.GetResource().getString("Zero");
                    break;
                case '1':
                    num = CreamToolkit.GetResource().getString("One");
                    break;
                case '2':
                    num = CreamToolkit.GetResource().getString("Two");
                    break;
                case '3':
                    num = CreamToolkit.GetResource().getString("Three");
                    break;
                case '4':
                    num = CreamToolkit.GetResource().getString("Four");
                    break;
                case '5':
                    num = CreamToolkit.GetResource().getString("Five");
                    break;
                case '6':
                    num = CreamToolkit.GetResource().getString("Six");
                    break;
                case '7':
                    num = CreamToolkit.GetResource().getString("Seven");
                    break;
                case '8':
                    num = CreamToolkit.GetResource().getString("Eight");
                    break;
                case '9':
                    num = CreamToolkit.GetResource().getString("Nine");
                    break;
                }

                switch (i) {
                case 2:
                    ret = num + CreamToolkit.GetResource().getString("Dollar")
                            + ret;
                    break;
                case 6:
                    ret = num
                            + CreamToolkit.GetResource().getString(
                                    "TenThousand") + ret;
                    break;
                }

                if (!isZero)
                    switch (i) {
                    case 0:
                        ret = num
                                + CreamToolkit.GetResource().getString("Cent")
                                + ret;
                        break;
                    case 1:
                        ret = num
                                + CreamToolkit.GetResource().getString(
                                        "TenCent") + ret;
                        break;
                    case 3:
                        ret = num + CreamToolkit.GetResource().getString("Ten")
                                + ret;
                        break;
                    case 4:
                        ret = num
                                + CreamToolkit.GetResource().getString(
                                        "Hundred") + ret;
                        break;
                    case 5:
                        ret = num
                                + CreamToolkit.GetResource().getString(
                                        "Thousand") + ret;
                        break;
                    case 7:
                        ret = num + CreamToolkit.GetResource().getString("Ten")
                                + ret;
                        break;
                    case 8:
                        ret = num
                                + CreamToolkit.GetResource().getString(
                                        "Hundred") + ret;
                        break;
                    case 9:
                        ret = num
                                + CreamToolkit.GetResource().getString(
                                        "Thousand") + ret;
                        break;
                    default:
                        break;
                    }
                else
                    ret = num + ret;
            }
            ret += CreamToolkit.GetResource().getString("Whole");
            System.out.println("getCapitalizationRMB | val : " + val
                    + " | ret : " + ret);
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return ret;
    }

    public void initPaymentInfoWithoutClearingDiscount() {
        setPayCash(new HYIDouble(0));
        setPayNumber1(null);
        setPayNumber2(null);
        setPayNumber3(null);
        setPayNumber4(null);
        setPayAmount1(null);
        setPayAmount2(null);
        setPayAmount3(null);
        setPayAmount4(null);
        addPayment(null);
        setChangeAmount(HYIDouble.zero());
        setSpillAmount(HYIDouble.zero());

        initCreditCardInfo();
        initPayCardInfo();
    }

    public void initPaymentInfo() {
        // if (sinkState instanceof IdleState) {
        // trans.setSalesAmount(new HYIDouble(0));
        setChangeAmount(new HYIDouble(0));
        setPayCash(new HYIDouble(0));
        setPayNumber1(null);
        setPayNumber2(null);
        setPayNumber3(null);
        setPayNumber4(null);
        setPayAmount1(null);
        setPayAmount2(null);
        setPayAmount3(null);
        setPayAmount4(null);
        addPayment(null);
        setSpillAmount(new HYIDouble(0));
        initCreditCardInfo();
        initPayCardInfo();
        initSIInfo();
        clearAppliedSIs();

        // trans.setRebateAmount(new HYIDouble(0));
        // trans.setTotalRebateAmount(new HYIDouble(0));
        // trans.setRebateChangeAmount(new HYIDouble(0));
        // Iterator iter = trans.getPayments();
        // int currentPayNumber = 0;
        // while (iter.hasNext()) {
        // currentPayNumber++;
        // Payment payment = (Payment)iter.next();
        // HYIDouble amount =
        // trans.getPayAmountByID(String.valueOf(currentPayNumber));
        // if(amount != null) {
        // if (amount.doubleValue() != 0)
        // trans.setPayAmountByID(String.valueOf(currentPayNumber), new
        // HYIDouble(0));
        // }
        // }
        // if (array != null) {
        // if (!array.isEmpty())
        // this.clearSI(trans, this.array);
        // }
        // trans.setSIDiscount(new HYIDouble(0));
        // if (this.discountState != null) {
        // array.clear();
        // discountState.init();
        // }

        Object[] lineItemArray = getLineItems();
        LineItem lineItem = null;
        for (int i = 0; i < lineItemArray.length; i++) {
            lineItem = (LineItem) lineItemArray[i];
            if (lineItem.getDetailCode().equals("S")) {
                lineItem.setAfterSIAmount(lineItem.getAmount());
                lineItem.setAfterDiscountAmount(lineItem.getAmount());
                // 將小計後折扣信息去掉(但如果是手工變價'O' or 促銷價信息'D'則不去掉)
                if (lineItem.getDiscountType() != null && lineItem.getDiscountType().equals("S")) {
                    lineItem.setDiscountType(null);
                }
                lineItem.setDiscountNumber(null);
            }
        }
        removeSILineItem();

        /*
         * for (int i=0; i <lineItems.length; i++) { lineItem =
         * (LineItem)lineItems[i]; if (lineItem.getDetailCode().equals("D"))
         * System.out.println(lineItem.getDescription() + "$@@@@@@@@"); }
         */

        // Meyer/2003-02-26/ Modified
        if (MIX_AND_MATCH_VERSION == null
                || MIX_AND_MATCH_VERSION.trim().equals("")
                || MIX_AND_MATCH_VERSION.trim().equals("1")) {
            setMixAndMatchAmount0(new HYIDouble(0));
            setMixAndMatchAmount1(new HYIDouble(0));
            setMixAndMatchAmount2(new HYIDouble(0));
            setMixAndMatchAmount3(new HYIDouble(0));
            setMixAndMatchAmount5(new HYIDouble(0));
            setMixAndMatchAmount6(new HYIDouble(0));
            setMixAndMatchAmount7(new HYIDouble(0));
            setMixAndMatchAmount8(new HYIDouble(0));
            setMixAndMatchAmount9(new HYIDouble(0));
            setMixAndMatchAmount10(new HYIDouble(0));
            // setSalesAmount(getSalesAmount().addMe(getTotalMMAmount()));
        } else if (MIX_AND_MATCH_VERSION.trim().equals("2")) {
            // trans.setSalesAmount(trans.getSalesAmount().subtract(trans.getMixAndMatchTotalAmount()));
        }
    }

    /**
     * for灿坤 如果明细中有单买商品并且没有发生组合促销,则不能结帐
     */
    public void setSalesCanNotEnd(boolean flag) {
        salesCanNotEnd = flag;
    }

    public boolean getSalesCanNotEnd() {
        return salesCanNotEnd;
    }

    public void saveTo(int step) {
    }

    public HYIDouble getYellowCardDiscountAmount() {
        HYIDouble amount = new HYIDouble(0);
        try {
            for (Iterator it = lineItems.iterator(); it.hasNext();) {
                LineItem item = (LineItem) it.next();
                if (item.getYellowAmount() != null)
                    amount = amount.addMe(item.getYellowAmount());
            }
        } catch (Exception e) {
            CreamToolkit
                    .logMessage("Exception : Trandtl not found YellowAmount!!!!!!!!!!");
        }
        return amount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }

    public boolean summarize() {
        Iterator itr = getLineItemsIterator();
        if (itr == null)
            return false;
        setSalesAmount(new HYIDouble(0));
        setInvoiceAmount(getSalesAmount());
        setDaiFuAmount(new HYIDouble(0));
        setDaiShouAmount(new HYIDouble(0));
        setDaiShouAmount2(new HYIDouble(0));
        setPreRcvAmount(new HYIDouble(0));

        //设置还元金
        //trans.setSpecialRebateAmount(new HYIDouble(0);
        setItemsRebateAmount(new HYIDouble(0));
        setRebateAmount(new HYIDouble(0));
        setTotalRebateAmount(new HYIDouble(0));

        while (itr.hasNext()) {
            LineItem li = (LineItem)itr.next();
            /*
                "S":销售
                "R":退货
                "D":SI折扣
                "M":Mix & Match折扣
                "B":退瓶
                "I":代售
                "O":代收公共事业费
                "Q":代付
                "V":指定更正
                "E":立即更正
                "N":PaidIn
                "T":PaidOut
                "J":期刊预订
            */
            if (li.getRemoved()) {
                continue;
            }
            if (li.getDetailCode().equals("S") || li.getDetailCode().equals("R")) {

                getSalesAmount().addMe(li.getAmount());
                setInvoiceAmount(getSalesAmount());

                //还元金
                getItemsRebateAmount().addMe(li.getAddRebateAmount());
                getRebateAmount().addMe(li.getAddRebateAmount());
                setTotalRebateAmount(getRebateAmount()); //暂时设置跟RebateAmount相同

            } else if (li.getDetailCode().equals("I") //代售
                || li.getDetailCode().equals("N")) {
                getDaiShouAmount().addMe(li.getAmount());

            } else if (li.getDetailCode().equals("O")) { // 代收
                getDaiShouAmount2().addMe(li.getAmount());

            } else if (li.getDetailCode().equals("Q")
                || li.getDetailCode().equals("T")) {
                getDaiFuAmount().addMe(li.getUnitPrice());

            } else if (li.getDetailCode().equals("D")) {
                String siId = li.getPluNumber();
                HYIDouble siAmount = li.getAmount();
                SI si = SI.queryBySIID(siId);
                if (si != null)
                    addAppliedSI(si, siAmount);

            } else if (li.getDetailCode().equals("J") //期刊
                    || li.getDetailCode().equals("L")) {
                getPreRcvAmount().addMe(li.getAmount());
            }
        }

        //System.out.println("totalRebateAmt=" + trans.getTotalRebateAmount());

        HYIDouble mmAmount = new HYIDouble(0);
        if (getMixAndMatchTotalAmount() != null)
           mmAmount = getMixAndMatchTotalAmount();
        if (mmAmount.doubleValue() == 0D)
           mmAmount = getTotalMMAmount();

        if (getDaiFuAmount().doubleValue() > 0D) {
            getSalesAmount()
                .addMe(getDaiFuAmount())
                .addMe(getDaiShouAmount())
                .addMe(getDaiShouAmount2());

        } else if (getDealType2().equals("Q")) {
            // 团购结算
            setSalesAmount(getPreRcvAmount());

        } else {
            getSalesAmount()
                .subtractMe(getDaiFuAmount())
                .addMe(getDaiShouAmount())
                .addMe(getDaiShouAmount2());
                //.addMe(getPreRcvAmount()); //Bruce/20121017 期刊改成记账
        }

        // 销售毛额 (不含组合促销折扣)
        //setGrossSalesAmount((HYIDouble)getSalesAmount().clone());
        setGrossSalesAmount(getSalesAmount().subtract(getDaiShouAmount())
            .subtract(getDaiShouAmount2()));

        // 销售净额
        getSalesAmount().subtractMe(mmAmount);
        getSalesAmount().addMe(getTotalSIAmount());

        // 发票金额
        setInvoiceAmount(getSalesAmount());
        return true;
    }

    static class FileSelector implements FilenameFilter {
        private String name = null;

        public FileSelector(String name) {
            this.name = name;
        }

        public boolean accept(File dir, String name) {
            if (name.trim().startsWith(this.name.trim()))
                return true;
            else if (name.trim().length() == 0)
                return true;
            else
                return false;
        }
    }

    public void setDIYOverrideAmount(String id, HYIDouble amount) {
        diyOverrideAmountMap.put(id, amount);
    }

    public HYIDouble getDIYOverrideAmount(String id) {
        return (HYIDouble) diyOverrideAmountMap.get(id);
    }

    public HYIDouble getTotalDIYOverrideAmount() {
        HYIDouble total = new HYIDouble(0);
        if (diyOverrideAmountMap.isEmpty())
            return total;
        for (Iterator it = diyOverrideAmountMap.keySet().iterator(); it
                .hasNext();) {
            total.addMe((HYIDouble) diyOverrideAmountMap.get(it.next()));
        }
        return total.setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }

    public HYIDouble getTotalDiscountAmount() {
        HYIDouble total = new HYIDouble(0);
        for (Iterator it = lineItems.iterator(); it.hasNext();) {
            LineItem item = (LineItem) it.next();
            if (item.getDiscountType() != null && item.getDiscountType().equals("D")) {
                total.addMe(item.getOriginalPrice().multiply(item.getQuantity())
                    .subtract(item.getAfterDiscountAmount()));
            }
        }
        return total.setScale(2, BigDecimal.ROUND_HALF_DOWN).negate();
    }

    public HYIDouble getTotalOriginalAmount() {
        HYIDouble total = new HYIDouble(0);
        for (Iterator it = lineItems.iterator(); it.hasNext();) {
            LineItem item = (LineItem) it.next();
            if (!item.getRemoved() && !item.getDetailCode().trim().equals("A")
                    && !item.getDetailCode().trim().equals("M")
                    && !item.getDetailCode().trim().equals("D")
                    && !item.getDetailCode().trim().equals("I")
                    && !item.getDetailCode().trim().equals("O")
                    ) {
                total = total.addMe(item.getOriginalPrice().multiply(item.getQuantity()));
            }
        }
        return total.setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }

    public HYIDouble getTotalMMAmount2() {
        HYIDouble amount = null;
        /*
         * Meyer / 2003-02-26 / Modified
         */
        if (mixAndMatchVersion == null || mixAndMatchVersion.trim().equals("")
                || mixAndMatchVersion.trim().equals("1")
                || mixAndMatchVersion.trim().equals("3")) {

            // old version
            if (totalMMAmount == null) {
                amount = this.getTaxMMAmount();
            } else {
                return totalMMAmount;
            }

        } else if (mixAndMatchVersion.trim().equals("2")) {
            // version 2
            amount = this.getMixAndMatchTotalAmount();
            if (amount == null) {
                amount = this.getTaxMMAmount();
            }
        }
        return ((HYIDouble) (amount.setScale(2, BigDecimal.ROUND_HALF_DOWN).clone())).add(getRoundDownAmount()).negate();
    }

    public HYIDouble getRoundDownAmount() {
        HYIDouble total = new HYIDouble(0);
        for (Iterator it = lineItems.iterator(); it.hasNext();) {
            LineItem item = (LineItem) it.next();
                if (item.getDiscountType() != null && item.getDiscountType().trim().equals("A"))
                    total = total.addMe(item.getOriginalPrice().multiply(item.getQuantity()));
        }
        return total.setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }
    
//     //预收 绝对值
//    public HYIDouble getPreRcvAmountAbs() {
//        if (getPreRcvAmount() == null)
//            return null;
//        else
//            return getPreRcvAmount().abs();
//    }
//    
//    public String getPreRcvAmountAbsWithTitle() {
//        String r = "";
//        if (getPreRcvAmount() == null)
//            r = "";
//        else if (getPreRcvAmount().compareTo(new HYIDouble(0)) <= 0)
//            r = CreamToolkit.GetResource().getString("SheMessage") + ": " +  getPreRcvAmountAbs();
//        else
//            r = CreamToolkit.GetResource().getString("PrePayMessage") + ": " +  getPreRcvAmountAbs();
//        return r;
//    }
//    
    // 预收
    public HYIDouble getPreRcvAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("preRcvAmount");
        else
            return (HYIDouble) getFieldValue("PRERCVAMOUNT");
    }

    public void setPreRcvAmount(HYIDouble preRcvAmount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("preRcvAmount", preRcvAmount);
        else
            setFieldValue("PRERCVAMOUNT", preRcvAmount);
    }
    

    /**
     * 会员原有赊欠预收款 绝对值
     * @return
     */
    public HYIDouble getOriginEarnAmountAbs() {
        if (getOriginEarnAmount() == null)
            return new HYIDouble(0);
        else
            return getOriginEarnAmount().abs();
    }
    
    /**
     * 会员原有赊欠预收款
     * @return
     */
    public HYIDouble getOriginEarnAmount() {
        return originEarnAmount;
    }

    public void setOriginEarnAmount(HYIDouble originEarnAmount) {
        Transaction.originEarnAmount = originEarnAmount;
    }
    
    /**
     * 记录各种状态
     * char1=   :交易类型 
     * char2="0":初始状态/"1":已转档
     * char3="0":初始状态
     * char4="0":初始状态
     * char5="0":初始状态
     * char6="0":初始状态
     * @return
     */
    protected String getState() {
        String state = (String) getFieldValue("state");
        return (state == null) ? "000000" : state;
    }

    protected void setState(String state) {
        setFieldValue("state", state);
    }

    public void setDefaultState() {
        setFieldValue("state", "000000");
    }
    
    /**
     * 交易种类用于期刊
     * @return
     * "0":一般交易 / "J":期刊预订 / "K":期刊领刊 / "L":期刊退刊
     * "T":团购 / "P":仅用于打印 / "Q":团购结算 / "D": 代配领取 / "B": 代配退回
     * "W":Nitori着付金交易 / "X":Nitori退着付殘金交易
     */
    public String getState1() {
        String state = getState();
        while (state.length() < 6)
            state = state + "0";
        return String.valueOf(state.charAt(0));
    }
    
    /**
     * 交易种类用于期刊
     * @param c
     * "0":一般交易/"J":期刊预订/"K":期刊领刊/"L":期刊退刊
     * "T":团购销售/"P":仅用于打印/"Q":团购结算
     * "W":Nitori着付金交易 / "X":Nitori退着付殘金交易
     */
    public void setState1(String c) {
        String state = getState();
        while (state.length() < 6)
            state = state + "0";
        setState(CreamToolkit.replace(state, c, 0));
    }
    
    /**
     * 交易种类用于期刊
     * @return
     * "0":初始状态/"1":期刊已转
     */
    public String getState2() {
        String state = getState();
        while (state.length() < 6)
            state = state + "0";
        return String.valueOf(state.charAt(1));
    }
    
    /**
     * 交易种类用于期刊
     * @param c
     * "0":初始状态/"1":期刊已转
     */
    public void setState2(String c) {
        String state = getState();
        while (state.length() < 6)
            state = state + "0";
        setState(CreamToolkit.replace(state, c, 1));
    }

    /**
     * 交易种类用于是否含赊账支付
     * @return
     * "0":初始状态/"1":正赊账/"2":负赊账/"3":忽略赊账
     */
    public String getState3() {
        String state = getState();
        while (state.length() < 6)
            state = state + "0";
        return String.valueOf(state.charAt(2));
    }
    
    /**
     * 交易种类用于是否含赊账支付
     * @param c
     * "0":初始状态/"1":正赊账/"2":负赊账/"3":忽略赊账
     */
    public void setState3(String c) {
        String state = getState();
        while (state.length() < 6)
            state = state + "0";
        setState(CreamToolkit.replace(state, c, 2));
    }

    public Map getDIYOverride() {
        return diyOverrideAmountMap;
    }

    /**
     * 當發票金額為零時，需要在發票上列印的文字。
     */
    public String getInvoiceIsZeroMsg() {
        return CreamToolkit.getString("ZeroInvoiceAmountPrintingMessage");
    }
    
    public boolean holdTransaction(DbConnection connection) throws InstantiationException {
        try {
            TransactionHold holdTran = new TransactionHold(this);
            holdTran.setTransactionNumber(TransactionHold.getNextHoldTransactionNumber(connection));
            if (holdTran.exists(connection))
                holdTran.update(connection);
            else
                holdTran.insert(connection);

            for (Iterator it = this.getLineItemsIterator(); it.hasNext();) {
                LineItemHold holdLine = new LineItemHold((LineItem) it.next());
                holdLine.setTransactionNumber(holdTran.getTransactionNumber());
                if (holdLine.exists(connection))
                    holdLine.update(connection);
                else
                    holdLine.insert(connection);
            }
//            setDealType1("*");
//            setDealType2("0");
//            setDealType3("5");
//            if (this.exists(connection))
//                this.update(connection);
//            else
//                this.insert(connection);
            return true;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return false;
        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e);
            return false;
        }
    }
    
    public Integer getDisplayedLineItemsSize() {
        int c = this.getDisplayedLineItemsArray().size();
        return new Integer(c);
    }
    
    public void clearDeliveryInfo() {
        setAnnotatedType("");
        setAnnotatedId("");
    }
    
    /**
     * 获得要打印明细列表
     * 用于台湾发票打印(一次性打印)
     * @return
     */
    public List getNeedPrintLineItems() {
        List needPrintLineItems = new ArrayList();
        Object[] items = getLineItems();
        for (int i = 0; i < items.length; i++) {
            LineItem item = (LineItem) items[i];
            if (item.getDetailCode() != null
                    && (item.getDetailCode().equalsIgnoreCase("s")
                            || item.getDetailCode()
                                    .equalsIgnoreCase("i")
                            || item.getDetailCode()
                                    .equalsIgnoreCase("o") || (getDealType2()
                            .equals("3") && item.getDetailCode()
                            .equalsIgnoreCase("r"))))
                needPrintLineItems.add(item);
        }
        return needPrintLineItems;
    }

    public static boolean existByVoidTranID(String voidTranID) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            StringBuffer selectStatement = new StringBuffer();

            selectStatement.append("SELECT TMTRANSEQ FROM tranhead")
                    .append(" WHERE VOIDSEQ = ").append(voidTranID);
            if (getValueOfStatement(connection, selectStatement.toString()) == null)
                return false;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return true;
    }
    
    public Integer getHoldTranNo() {
        return holdTranNo;
    }

    public void setHoldTranNo(Integer holdTranNo) {
        this.holdTranNo = holdTranNo;
    }

    private void udpateShiftZExData(DbConnection connection) throws SQLException {
        final HYIDouble zero = new HYIDouble(0);
        final HYIDouble plus = new HYIDouble(1);
        final HYIDouble minus = new HYIDouble(-1);
        HYIDouble sign;
        HYIDouble siSign;

        final String PAY = "PAY";
        final String RTN = "RTN";
        final String DOWN = "DOWN";
        String accountPrefix;
        String downPayPrefix; // 分期付款科目

        System.out.println("udpateShiftZExData(): dt1-dt2-dt3=" + getDealType1() + "-" + getDealType2()
            + "-" + getDealType3());
        
        if (this.getDealType1().equals("0") && this.getDealType2().equals("0") && this.getDealType3().equals("0")) {
            // 一般交易
            accountPrefix = PAY;
            sign = plus; // 增加正常支付次數
            siSign = plus;
        } else if (this.getDealType1().equals("0") && this.getDealType2().equals("3") && this.getDealType3().equals("4")) {
            // 全退
            accountPrefix = RTN;
            sign = plus; // 增加退貨次數
            siSign = minus;
        } else if (this.getDealType1().equals("0") && this.getDealType2().equals("0") && this.getDealType3().equals("4")) {
            // 部份退貨正項
            accountPrefix = RTN;
            sign = zero; // 不加減退貨次數
            siSign = plus;
        } else if (this.getDealType1().equals("*") && this.getDealType2().equals("0") && this.getDealType3().equals("0")) {
            // 被作廢
            accountPrefix = PAY;
            sign = plus; // 增加正常支付次數
            siSign = plus;
        } else if (this.getDealType1().equals("0") && this.getDealType2().equals("0") && this.getDealType3().equals("1")) {
            // 前筆誤打
            accountPrefix = PAY;
            sign = minus; // 減少正常支付次數
            siSign = minus;
        } else if (this.getDealType1().equals("0") && this.getDealType2().equals("0") && this.getDealType3().equals("3")) {
            // 交易取消
            accountPrefix = PAY;
            sign = minus; // 減少正常支付次數
            siSign = minus;
        } else if (this.getDealType1().equals("*") && this.getDealType2().equals("0") && this.getDealType3().equals("2")) {
            // 被作廢(重印)
            accountPrefix = PAY;
            sign = zero; // 不加減正常支付次數
            siSign = zero;
        } else if (this.getDealType1().equals("0") && this.getDealType2().equals("0") && this.getDealType3().equals("2")) {
            // 重印
            accountPrefix = PAY;
            sign = minus; // 減少正常支付次數
            siSign = minus;
        } else if (this.getDealType1().equals("0") && this.getDealType2().equals("Q") && this.getDealType3().equals("0")) {
            // 团购结算
            accountPrefix = PAY;
            sign = plus; // 增加正常支付次數
            siSign = plus;
        } else {
            // 其他不處理
            return;
        }
        downPayPrefix = DOWN + accountPrefix;

        // 重算支付金额，通过优先级把ChangeAmount和SpillAmount分摊到相应的支付中去
        List<Map<String, Object>> payList = new ArrayList<Map<String, Object>>();
        for (int i = 1; i < 5; i++ ) {
            String payID = (String) this.getFieldValue("PAYNO" + i);
            if (payID == null || payID.trim().equals(""))
                continue;
            Payment payment = Payment.queryByPaymentID(payID);
            if (payment != null) {
                Map<String, Object> pay = new HashMap<String, Object>();
                pay.put("PAYID", payID);
                pay.put("PAYAMT", this.getFieldValue("PAYAMT" + i));
                pay.put("PAYMENT", payment);
                payList.add(pay);
                // 设置赊账属性
                String shePayId = Payment.queryDownPaymentId();
                if (payID.equals(shePayId)) {
                    if (sign.compareTo(plus) == 0)
                        setState3("1");
                    else if (sign.compareTo(minus) == 0)
                        setState3("2");
                    else if (sign.compareTo(zero) == 0)
                        setState3("3");
                }
            }
        }
        Collections.sort(payList, new Comparator<Map<String, Object>>() {
            private PaymentComparator delegate = new PaymentComparator();
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                Payment payment1 = (Payment)o1.get("PAYMENT");
                Payment payment2 = (Payment)o2.get("PAYMENT");
                return delegate.compare(payment1, payment2);
            }
        });
        //for (Map  p : payList){
        //    Payment payment = (Payment)p.get("PAYMENT");
        //    System.out.println(payment.getSeqno() + "---" + payment.getPaymentID() + "---" + payment.getPrintName()) ;
        //}

        HYIDouble changeAmount = this.getChangeAmount();
        HYIDouble spillAmount = this.getSpillAmount();
        if (spillAmount != null && spillAmount.compareTo(new HYIDouble(0)) != 0 ||
            changeAmount != null && changeAmount.compareTo(new HYIDouble(0)) != 0) {
            for (Map<String, Object> map : payList) {
                Payment payment = (Payment) map.get("PAYMENT");
                if (spillAmount != null && spillAmount.compareTo(new HYIDouble(0)) != 0 &&
                        payment.isSpillable()) {// 该支付可分摊溢收
                    HYIDouble payamt = (HYIDouble) map.get("PAYAMT");
                    if (spillAmount.abs().compareTo(payamt.abs()) > 0) {
                        map.put("PAYAMT", new HYIDouble(0));
                        map.put("SPAYAMT", payamt);
                        spillAmount = spillAmount.subtract(payamt);
                    } else {
                        map.put("PAYAMT", payamt.subtract(spillAmount));
                        map.put("SPAYAMT", spillAmount);
                        spillAmount = new HYIDouble(0);
                    }
                }
                if (changeAmount != null && changeAmount.compareTo(new HYIDouble(0)) != 0 &&
                        payment.isChangeable() &&
                        payment.getPaymentID().equals(GetProperty.getChangeAmtPaymentID()))
                {
                    // 只有现金支付可分摊找零
                    HYIDouble payamt = (HYIDouble) map.get("PAYAMT");
                    if (changeAmount.abs().compareTo(payamt.abs()) > 0) {
                        map.put("PAYAMT", new HYIDouble(0));
                        map.put("CHGAMT", payamt);
                        changeAmount = changeAmount.subtract(payamt);
                    } else {
                        map.put("PAYAMT", payamt.subtract(changeAmount));
                        map.put("CHGAMT", changeAmount);
                        changeAmount = new HYIDouble(0);
                    }
                }
            }
        }

        // 只有现金支付可分摊找零
        if (changeAmount != null && changeAmount.compareTo(new HYIDouble(0)) != 0) {
            // 按照payList顺序对可找零支付（非现金支付）分摊找零金额
            HYIDouble otherChangeAmount = (HYIDouble)changeAmount.clone();
            for (Map<String, Object> map : payList) {
                Payment payment = (Payment) map.get("PAYMENT");
                if (payment.isChangeable() &&
                        !payment.getPaymentID().equals(GetProperty.getChangeAmtPaymentID()))
                {
                    HYIDouble payamt = (HYIDouble) map.get("PAYAMT");
                    if (otherChangeAmount.abs().compareTo(payamt.abs()) > 0) {
                        map.put("CHGAMT", payamt);
                        otherChangeAmount = otherChangeAmount.subtract(payamt);
                    } else {
                        map.put("CHGAMT", otherChangeAmount);
                        otherChangeAmount = new HYIDouble(0);
                    }
                }
            }

            Payment payment = Payment.queryByPaymentID(GetProperty.getChangeAmtPaymentID());
            if (payment != null) {
                Map<String, Object> pay = new HashMap<String, Object>();
                pay.put("PAYID", payment.getPaymentID());
                //pay.put("CHGAMT", changeAmount);// 把各种除了现金意外的支付的找零笼统记录到现金找零里面
                pay.put("PAYAMT", changeAmount.negate());
                pay.put("PAYMENT", payment);
                payList.add(pay);
            }
        }

        // 計算送貨傳票、退貨傳票、和一般客戶退貨次數和金額
        // - 0,0,0为正；
        // - 0，3，4为正，0，0，4为负 ==>两个相加可推出 实际退货的状况
        if (accountPrefix.equals(PAY) && isPeiDa()) { // 送貨傳票
            //updateEx(connection, sign, "DELIVERY_CNT", getNetSalesAmount(), "DELIVERY_AMT");
            updateEx(connection, sign, "DELIVERY_CNT", 
                    getNetSalesAmount0().add(getNetSalesAmount1()).add(getNetSalesAmount2()).add(
                    getNetSalesAmount3()).add(getNetSalesAmount4()), "DELIVERY_AMT");
        } else if (accountPrefix.equals(RTN) && isPeiDa()) { // 退貨傳票
            //updateEx(connection, sign, "RTN_DELIVERY_CNT", getNetSalesAmount(), "RTN_DELIVERY_AMT");
            updateEx(connection, sign, "RTN_DELIVERY_CNT", 
                    getNetSalesAmount0().add(getNetSalesAmount1()).add(getNetSalesAmount2()).add(
                    getNetSalesAmount3()).add(getNetSalesAmount4()), "RTN_DELIVERY_AMT");
        } else if (accountPrefix.equals(RTN) && !isPeiDa()) { //一般客戶退貨
            //updateEx(connection, sign, "RTN_NORMAL_CNT", getNetSalesAmount(), "RTN_NORMAL_AMT");
            updateEx(connection, sign, "RTN_NORMAL_CNT", 
                    getNetSalesAmount0().add(getNetSalesAmount1()).add(getNetSalesAmount2()).add(
                            getNetSalesAmount3()).add(getNetSalesAmount4()),"RTN_NORMAL_AMT");
        }

        // 計算各種支付金額次數、退貨金額次數、溢收金額 chgamt
        boolean containsDownPayment = isDownPaymentOrBackPaymentTransaction();
        for (Map<String, Object> pay : payList) {
            HYIDouble currentAmount = null;
            String payID = (String) pay.get("PAYID");
            Object payAmt = pay.get("PAYAMT");
            if (payAmt != null) {
                currentAmount = (HYIDouble) payAmt;
                if (payID != null && !payID.trim().equals("")) {
                    System.out.println("udpateShiftZExData(): " + accountPrefix + "CNT_" + payID+ "+=" + sign + ", " 
                        + accountPrefix + "AMT_" + payID + "+=" + currentAmount);
                    updateEx(connection, sign, accountPrefix + "CNT_" + payID, currentAmount, accountPrefix + "AMT_" + payID);
                    if (containsDownPayment) {
                        System.out.println("udpateShiftZExData(): " + downPayPrefix + "CNT_" + payID+ "+=" + sign + ", "
                            + downPayPrefix + "AMT_" + payID + "+=" + currentAmount);
                        updateEx(connection, sign, downPayPrefix + "CNT_" + payID, currentAmount, downPayPrefix + "AMT_" + payID);
                    }
                }
            }
            payAmt = pay.get("SPAYAMT"); //溢收
            if (payAmt != null) {
                currentAmount = (HYIDouble) payAmt;
                if (payID != null && !payID.trim().equals("")) {
                    //System.out.println("udpateShiftZExData(): OVERAMT_" + payID + "+=" + currentAmount);
                    //updateEx(connection, sign, null, currentAmount, "OVERAMT_" + payID);
                    System.out.println("udpateShiftZExData(): OVER_" + accountPrefix + "AMT_" + payID + "+=" + currentAmount);
                    updateEx(connection, sign, null, currentAmount, "OVER_" + accountPrefix + "AMT_" + payID);
                    if (containsDownPayment) {
                        System.out.println("udpateShiftZExData(): OVER_" + downPayPrefix + "AMT_" + payID + "+=" + currentAmount);
                        updateEx(connection, sign, null, currentAmount, "OVER_" + downPayPrefix + "AMT_" + payID);
                    }
                }
            }
            payAmt = pay.get("CHGAMT"); //chgamt
            if (payAmt != null) {
                currentAmount = (HYIDouble) payAmt;
                if (payID != null && !payID.trim().equals("")) {
                    System.out.println("udpateShiftZExData(): CHGAMT_" + payID + "+=" + currentAmount);
                    updateEx(connection, sign, null, currentAmount, "CHGAMT_" + payID);
                }
            }
        }


        TaxType discountTaxType = TaxType.queryByTaxID(PARAM.getDiscountTaxId());
        HYIDouble siAmtTotal = new HYIDouble(0);
        HashMap<String, HYIDouble> siAmt_hm = new HashMap<String, HYIDouble>();
        // 计算si折扣金额，次数
        for (int i = 0; i < this.getLineItems().length; i++) {
            LineItem li = (LineItem) getLineItems()[i];
            if (li.getDetailCode().equals("D")) {
                // si 折扣
                HYIDouble currentAmount = li.getAmount().negate();// 正常交易为负值，退货为正值
                String siId =  li.getPluNumber();
                updateEx(connection, siSign, "SICNT_" + siId, currentAmount, "SIAMT_" + siId);
                if (discountTaxType != null
                        && li.getTaxType() != null
                        && li.getTaxAmount() != null
                        && !"".equals(li.getTaxType().trim())){
                    // 直接累加, 因为有税率id(直接按照配置中设定的税率计算出了折扣税金）
                    updateEx(connection, siSign, null, li.getTaxAmount().negate(), "SITAX_" + siId);
                } else {
                    // 累计该笔交易的各种折扣金额，用于后面做对各个被折扣单品的税率做分摊计算
                    HYIDouble siAmt = siAmt_hm.get(siId);
                    siAmt_hm.put(siId, siAmt == null ? currentAmount : siAmt.addMe(currentAmount));
                    siAmtTotal.addMe(currentAmount);
                }
            }
        }

        if (discountTaxType == null
                && siAmtTotal.compareTo(new HYIDouble(0)) != 0){
            // 按销售单品累计si各种折扣对应的税率
            for (int i = 0; i < this.getLineItems().length; i++) {
                LineItem li = (LineItem) getLineItems()[i];
                if (li.getDetailCode().equals("D")) {
                    // si折扣 不处理
                } else {
                    // 普通商品
                    HYIDouble siTaxAmount = li.getSiTaxAmount();
                    TaxType taxType = TaxType.queryByTaxID(li.getTaxType());
                    String taxRound = taxType.getRound();
                    int taxDigit = taxType.getDecimalDigit().intValue();
                    int round = 0;
                    if (taxRound.equalsIgnoreCase("R")) {
                        round = BigDecimal.ROUND_HALF_UP;
                    } else if (taxRound.equalsIgnoreCase("C")) {
                        round = BigDecimal.ROUND_UP;
                    } else if (taxRound.equalsIgnoreCase("D")) {
                        round = BigDecimal.ROUND_DOWN;
                    }

                    HYIDouble currentAmount = new HYIDouble(0);
                    HYIDouble diff = new HYIDouble(0);
                    for (String siId : siAmt_hm.keySet()){
                        currentAmount = siTaxAmount.multiplyMe(siAmt_hm.get(siId))
                            .divideMe(siAmtTotal, taxDigit, round);
                        diff =  siTaxAmount.subtract(currentAmount);
                        updateEx(connection, siSign, null, currentAmount, "SITAX_" + siId);
                    }
                    // 把四舍五入的分摊到其中和si中
                    Iterator<String> it = siAmt_hm.keySet().iterator();
                    if (it.hasNext()){
                        String siId = it.next();
                        updateEx(connection, siSign, null, diff, "SITAX_" + siId);
                    }

                }
            }
        }

        // 按照金额排序后依次扣减
        Collections.sort(payList, new Comparator<Map>(){
            public int compare(Map o1, Map o2) {
                HYIDouble payAmt1 = (HYIDouble)((Map)o1).get("PAYAMT");
                HYIDouble payAmt2 = (HYIDouble)((Map)o2).get("PAYAMT");

                if (payAmt1 == null && payAmt2 == null){
                    return 0;
                } else if (payAmt1 == null){
                    return 1;
                } else if (payAmt2 == null){
                    return -1;
                }
                // 考虑退货为负数，这里用绝对值比较; 逆序
                return payAmt2.abs().compareTo(payAmt1.abs());
            }
        });
        for (Map  p : payList){
            Payment payment = (Payment)p.get("PAYMENT");
            System.out.println(payment.getSeqno() + "---" + payment.getPaymentID() + "---"+ payment.getPrintName() + "---" + ((HYIDouble)p.get("PAYAMT")).doubleValue()) ;
            // TODO 為何payment.getPrintName() 沒有值
        }


        // 针对购物袋：計算各種支付金額和次數 到购物袋专项中
        // BAGCNT_00 .. 01 BAGAMT_00 .. 01 .. 02 ..
        String typeStr = "BAG_" + accountPrefix;
        if (PARAM.isGatherShoppingBagInfo()){
            for (Map<String, Object> pay : payList) {
                HYIDouble currentAmount = null;
                String payID = (String) pay.get("PAYID");
                HYIDouble payAmt = (HYIDouble)pay.get("PAYAMT");
                if (payAmt != null && payID != null && !payID.trim().equals("")) {
                    if (shoppingBagTotalAmount.abs().compareTo(payAmt.abs()) > 0) {
                        currentAmount = payAmt;
                        System.out.println("udpateShiftZExData(): "
                                + typeStr + "CNT_" + payID + "+=" + sign + ", "
                                + typeStr + "AMT_" + payID + "+=" + currentAmount);
                        updateEx(connection, sign,
                                typeStr + "CNT_" + payID,
                                currentAmount,
                                typeStr + "AMT_" + payID);
                        shoppingBagTotalAmount.subtractMe(payAmt);
                    } else {
                        currentAmount = shoppingBagTotalAmount;
                        System.out.println("udpateShiftZExData(): "
                                + typeStr + "CNT_" + payID + "+=" + sign + ", "
                                + typeStr + "AMT_" + payID + "+=" + currentAmount);
                        updateEx(connection, sign,
                                typeStr + "CNT_" + payID,
                                currentAmount,
                                typeStr + "AMT_" + payID);
                        shoppingBagTotalAmount = new HYIDouble(0);
                        break;
                    }
                }
            }
        }
        
        // 针对配送费：計算各種支付金額和次數 到配送费专项中
        // SENDFEE_CNT_00 .. 01 SENDFEE_AMT_00 .. 01 .. 02 ..
        typeStr = "SENDFEE_" + accountPrefix;
        if (PARAM.isGatherSendFeeInfo()){
            for (Map<String, Object> pay : payList) {
                HYIDouble currentAmount = null;
                String payID = (String) pay.get("PAYID");
                HYIDouble payAmt = (HYIDouble)pay.get("PAYAMT");
                if (payAmt != null && payID != null && !payID.trim().equals("")) {
                    if (homeSendFeeTotalAmount.abs().compareTo(payAmt.abs()) > 0) {
                        currentAmount = payAmt;
                        System.out.println("udpateShiftZExData(): "
                                + typeStr + "CNT_" + payID + "+=" + sign + ", "
                                + typeStr + "AMT_" + payID + "+=" + currentAmount);
                        updateEx(connection, sign,
                                typeStr + "CNT_" + payID,
                                currentAmount,
                                typeStr + "AMT_" + payID);
                        homeSendFeeTotalAmount.subtractMe(payAmt);
                    } else {
                        currentAmount = homeSendFeeTotalAmount;
                        System.out.println("udpateShiftZExData(): "
                                + typeStr + "CNT_" + payID+ "+=" + sign + ", "
                                + typeStr + "AMT_" + payID + "+=" + currentAmount);
                        updateEx(connection, sign,
                                typeStr + "CNT_" + payID,
                                currentAmount,
                                typeStr + "AMT_" + payID);
                        homeSendFeeTotalAmount = new HYIDouble(0);
                        break;
                    }
                }
            }
        }

    }

    private void updateEx(DbConnection connection, HYIDouble cnt, String cntCode, HYIDouble amt, String amtCode) throws SQLException {
//        amt = cnt.multiply(amt);
        // for sEx
        ShiftEx sEx = new ShiftEx();
        sEx.setTerminalNumber(this.getTerminalNumber());
        sEx.setShiftSequenceNumber(this.getSignOnNumber());
        sEx.setZSequenceNumber(this.getZSequenceNumber());

        if (cntCode != null) { // && cnt.compareTo(new HYIDouble(0)) > 0) {
            sEx.setAccountCode(cntCode);
            sEx.setAmount(cnt);
            sEx.updateShiftEx(connection);
        }

        sEx.setAccountCode(amtCode);
        sEx.setAmount(amt);
        sEx.updateShiftEx(connection);

        // for zEx
        ZEx zEx = new ZEx();
        zEx.setTerminalNumber(this.getTerminalNumber());
        zEx.setZSequenceNumber(this.getZSequenceNumber());

        if (cntCode != null) { // && cnt.compareTo(new HYIDouble(0)) > 0) {
            zEx.setAccountCode(cntCode);
            zEx.setAmount(cnt);
            zEx.updateZEx(connection);
        }

        zEx.setAccountCode(amtCode);
        zEx.setAmount(amt);
        zEx.updateZEx(connection);
    }
    
    /**
     * @param selectedDaiShoudef
     * @param barcode
     */
    public void addDaiShouLineItem(DaiShouDef selectedDaiShoudef, String barcode, HYIDouble price) {
        if (selectedDaiShoudef == null)
            return;
        if (price == null){
            price = selectedDaiShoudef.getAmount(barcode);
        }
        HYIDouble qty = new HYIDouble(1);
        if (price.compareTo(nHYIDouble0()) < 0){
            price = price.negate();
            qty.negateMe();
        }
        LineItem lineItem = new LineItem();
        lineItem.setDescription(selectedDaiShoudef.getName());
        lineItem.setDescriptionAndSpecification(selectedDaiShoudef.getName());
        lineItem.setDetailCode("O"); //代收公共事业费
        //            lineItem.setPluNumber(
        //                selectedDaiShoudef.getItemNumber() != null && selectedDaiShoudef.getItemNumber().length() > 0
        //                    ? selectedDaiShoudef.getItemNumber()
        //                    : selectedDaiShoudef.getID());
        lineItem.setPluNumber(selectedDaiShoudef.getID());
        //            lineItem.setDiscountNumber(selectedDaiShoudef.getID());
        lineItem.setDiscountNumber(barcode);
        lineItem.setDaiShouID(selectedDaiShoudef.getID());

        //lineItem.setTaxType("0");
        lineItem.setRemoved(false);
        lineItem.setUnitPrice(price);
        lineItem.setOriginalPrice(price);
        lineItem.setQuantity(qty);
        lineItem.setAmount(price.multiply(lineItem.getQuantity()));
        lineItem.setAfterDiscountAmount(lineItem.getAmount());
        lineItem.setDaiShouBarcode(barcode);
        lineItem.setPrinted(false);

        lineItem.setTerminalNumber(this.getTerminalNumber());
        lineItem.setTransactionNumber(this.getTransactionNumber());
        try {
            this.addLineItem(lineItem);
            POSTerminalApplication.getInstance().setTransactionEnd(false);
        } catch (TooManyLineItemsException e) {
            CreamToolkit.logMessage(e);
            POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyLineItems"));
        }
    }

    /**
     * 計算發票金額。
     * 將發票金額扣除掉“已開發票”的净支付金額。
     */
    public void calcInvoiceAmount() {
        CreamToolkit.logMessage("INVAMT> Entering calcInvoiceAmount()...");
        calcNetPayment();

        setInvoiceAmount((HYIDouble)getNetSalesAmount().clone());
        CreamToolkit.logMessage("INVAMT> calcInvoiceAmount(): orig invamt=" + getInvoiceAmount());

        HYIDouble zero = HYIDouble.zero();
        if (getSpillAmount() == null)
            setSpillAmount(zero);
        HYIDouble overAmount = getSpillAmount();
        for (int i = 1; i < 5; i++) {
            Payment payment = getPaymentByID(i);
            if (payment == null)
                break;
            HYIDouble netAmount = netPayments.get(payment);
            CreamToolkit.logMessage("INVAMT> calcInvoiceAmount(): payment=" + payment.getPaymentID()
                    + ", " + payment.getScreenName() + ", " + payment.getPaymentType()
                    + ", netAmnt=" + netAmount);
            if (netAmount != null && !netAmount.equals(zero)) {
                if (payment.isNonInvoice()) {
                    getInvoiceAmount().subtractMe(netAmount);
                    CreamToolkit.logMessage("INVAMT> calcInvoiceAmount(): substract " + netAmount
                        + ", invamt=" + getInvoiceAmount());
                }
            }        }
        if (overAmount != null && !overAmount.equals(zero)) {
            getInvoiceAmount().addMe(overAmount);
            CreamToolkit.logMessage("INVAMT> calcInvoiceAmount(): overAmount " + overAmount
                    + ", invamt=" + getInvoiceAmount());
        }
    }

    /** 是否存在信用卡支付 */
    public Payment getCreditCardPayment() {
        for (int i = 1; i <= 4; i++) {
            Payment payment = getPaymentByID(i);
            if (payment.isCreditCard())
                return payment;
        }
        return null;
    }

    /** 是否存在信用卡支付 */
    public boolean existsCreditCardPayment() {
        for (int i = 1; i <= 4; i++) {
            Payment payment = getPaymentByID(i);
            if (payment == null)
                return false;
            else if (payment.isCreditCard())
                return true;
        }
        return false;
    }

    /** 是否存在連線的信用卡支付 */
    public boolean existsCATPayment() {
        //Bruce/20101121/ Don't connect to any CAT in training mode
        if (POSTerminalApplication.getInstance().getTrainingMode())
            return false;

        for (int i = 1; i <= 4; i++) {
            Payment payment = getPaymentByID(i);
            if (payment == null)
                return false;
            else if (payment.isCreditCardWithCATConnected())
                return true;
        }
        return false;
    }

    public HYIDouble getCreditCardAmount() {
        for (int i = 1; i <= 4; i++) {
            Payment p = getPaymentByID(i);
            if (p == null)
                break;
            if (p.isCreditCard())
                return getPayAmountByID(i);
        }
        return nHYIDouble0();
    }

    /**
     * 将信用卡支付金额减掉红利折抵金额。
     * 
     * @param bonusAmount 红利折抵金额（正常销售为负数）
     */
    public void creditCardSubstractBonusAmount(HYIDouble bonusAmount) {
        for (int i = 1; i <= 4; i++) {
            Payment p = getPaymentByID(i);
            if (p == null)
                break;
            if (p.isCreditCard()) {
                getPayAmountByID(i).addMe(bonusAmount);
                return;
            }
        }
    }

    /**
     * 將信用卡支付金額減掉紅利折抵金額，並生成紅利折抵支付。
     *
     * @param bonusAmount 紅利折抵金額（正常銷售為負數）
     */
    public void generateCreditCardBonusPayment(HYIDouble bonusAmount) {
        boolean okToSplit = false;
        Payment creditCardBonusPayment = Payment.queryCreditCardBonusPayment();
        for (int i = 1; i <= 4; i++) {
            Payment p = getPaymentByID(i);
            if (p == null && okToSplit) {
                setPayNumberByID(i, creditCardBonusPayment.getPaymentID());
                setPayAmountByID(i, bonusAmount.negate());
                fireEvent(new TransactionEvent(this, TransactionEvent.TRANS_INFO_CHANGED));
                break;
            }
            if (p.isCreditCard()) {
                getPayAmountByID(i).addMe(bonusAmount);
                okToSplit = true;
            }
        }
        //calcInvoiceAmount();
    }

    public Collection cloneForPOS() {
        int i;
        String[][] fieldNameMap = getPosToScFieldNameArray();
        List dacReturned = new ArrayList();

        Transaction clonedTransaction = new Transaction();
        for (i = 0; i < fieldNameMap.length; i++) {
            Object value = this.getFieldValue(fieldNameMap[i][1]);
            if (value == null) {
                try {
                    value = getClass().getDeclaredMethod("get" + fieldNameMap[i][1]).invoke(this);
                } catch (Exception e) {
                    value = null;
                }
            }
            clonedTransaction.setFieldValue(fieldNameMap[i][0], value);
        }
        dacReturned.add(clonedTransaction);

        Object[] lineItems = getLineItems();
        if (lineItems != null) {
            for (i = 0; i < lineItems.length; i++) {
                LineItem lineItem = ((LineItem)lineItems[i]).cloneForPOS();
                clonedTransaction.addLineItemSimpleVersion(lineItem);
                dacReturned.add(lineItem);
            }
        }

        List<Alipay_detail> alipayLists = getAlipayList();
        if (alipayLists != null && alipayLists.size() > 0) {
            for (Alipay_detail alipay_detail : alipayLists) {
                dacReturned.add(alipay_detail.cloneForSC());
            }
        }
        List<WeiXin_detail> weiXinLists = getWeiXinList();
        if (weiXinLists != null && weiXinLists.size() > 0) {
            for (WeiXin_detail weiXin_detail : weiXinLists) {
                dacReturned.add(weiXin_detail.cloneForSC());
            }
        }
        List<UnionPay_detail> unionpayLists = getUnionPayList();
        if (unionpayLists != null && unionpayLists.size() > 0) {
            for (UnionPay_detail unionpay_detail : unionpayLists) {
                dacReturned.add(unionpay_detail.cloneForSC());
            }
        }
        List<Selfbuy_head> selfbuyHeadLists = getSelfbuyHeadList();
        if (selfbuyHeadLists != null && selfbuyHeadLists.size() > 0) {
            for (Selfbuy_head selfbuy_head : selfbuyHeadLists) {
                dacReturned.add(selfbuy_head.cloneForSC());
            }
        }
        List<Selfbuy_detail> selfbuyDetailLists = getSelfbuyDetailList();
        if (selfbuyDetailLists != null && selfbuyDetailLists.size() > 0) {
            for (Selfbuy_detail selfbuy_detail : selfbuyDetailLists) {
                dacReturned.add(selfbuy_detail.cloneForSC());
            }
        }
        List<Cmpay_detail> cMpay_details = getCmpayList();
        if (cMpay_details != null && cMpay_details.size() > 0) {
            for (Cmpay_detail cMpay_detail : cMpay_details) {
                dacReturned.add(cMpay_detail.cloneForSC());
            }
        }
        //Bruce/20080903/ Add associated carddetail
        if (getCardDetail() != null) {
            clonedTransaction.setCardDetail(getCardDetail());
            dacReturned.add(getCardDetail());
        }

        return dacReturned;
    }

    synchronized public static Collection queryObject(String posNumber, String transactionNumber) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Transaction trans;
            try {
                trans = getSingleObject(connection, Transaction.class, Server.isAtServerSide() ?
                    "SELECT * FROM posul_tranhead WHERE posNumber=" + posNumber
                        + " AND transactionNumber=" + transactionNumber + " ORDER BY systemDate DESC LIMIT 1" :
                    "SELECT * FROM tranhead WHERE tmtranseq=" + transactionNumber
                );
            } catch (EntityNotFoundException e) {
                return null;
            }

            Iterator itr = null;
            try {
                itr = getMultipleObjects(connection, LineItem.class, Server.isAtServerSide() ?
                    "SELECT * FROM posul_trandtl WHERE posNumber=" + posNumber
                        + " AND transactionNumber=" + transactionNumber :
                    "SELECT * FROM trandetail WHERE TMTRANSEQ='" + transactionNumber + "'");
            } catch (EntityNotFoundException e) {
            }

            if (itr != null) {
                while (itr.hasNext()) {
                    LineItem lineItem = (LineItem) itr.next();
                    trans.addLineItemSimpleVersion(lineItem);
                }
            }

            Iterator it = null;
            try {
                it = getMultipleObjects(connection, Alipay_detail.class, Server.isAtServerSide() ?
                        "SELECT * FROM posul_alipay_detail WHERE POSNUMBER=" + posNumber
                                + " AND TRANSACTIONNUMBER=" + transactionNumber :
                        "SELECT * FROM alipay_detail WHERE TRANSACTIONNUMBER='" + transactionNumber + "'");
            } catch (EntityNotFoundException e) {
            }

            if (it != null) {
                while (it.hasNext()) {
                    Alipay_detail detail = (Alipay_detail) it.next();
                    trans.addAlipayList(detail);
                }
            }
            try {
                it = getMultipleObjects(connection, Cmpay_detail.class, Server.isAtServerSide() ?
                        "SELECT * FROM posul_cmpay_detail WHERE POSNUMBER=" + posNumber
                                + " AND TRANSACTIONNUMBER=" + transactionNumber :
                        "SELECT * FROM cmpay_detail WHERE TRANSACTIONNUMBER='" + transactionNumber + "'");
            } catch (EntityNotFoundException e) {
            }

            if (it != null) {
                while (it.hasNext()) {
                    Cmpay_detail detail = (Cmpay_detail) it.next();
                    trans.addCmPayList(detail);
                }
            }
            try {
                it = getMultipleObjects(connection, WeiXin_detail.class, Server.isAtServerSide() ?
                        "SELECT * FROM posul_weixin_detail WHERE POSNUMBER=" + posNumber
                                + " AND TRANSACTIONNUMBER=" + transactionNumber :
                        "SELECT * FROM weixin_detail WHERE TRANSACTIONNUMBER='" + transactionNumber + "'");
            } catch (EntityNotFoundException e) {
            }

            if (it != null) {
                while (it.hasNext()) {
                    WeiXin_detail detail = (WeiXin_detail) it.next();
                    trans.addWeiXinList(detail);
                }
            }
            try {
                it = getMultipleObjects(connection, UnionPay_detail.class, Server.isAtServerSide() ?
                        "SELECT * FROM posul_unionpay_detail WHERE POSNUMBER=" + posNumber
                                + " AND TRANSACTIONNUMBER=" + transactionNumber :
                        "SELECT * FROM unionpay_detail WHERE TRANSACTIONNUMBER='" + transactionNumber + "'");
            } catch (EntityNotFoundException e) {
            }

            if (it != null) {
                while (it.hasNext()) {
                    UnionPay_detail detail = (UnionPay_detail) it.next();
                    trans.addUnionPayList(detail);
                }
            }
            try {
                it = getMultipleObjects(connection, Selfbuy_head.class, Server.isAtServerSide() ?
                        "SELECT * FROM posul_selfbuy_head WHERE tmtranseq=" + transactionNumber :
                        "SELECT * FROM selfbuy_head WHERE tmtranseq='" + transactionNumber + "'");
            } catch (EntityNotFoundException e) {
            }

            if (it != null) {
                while (it.hasNext()) {
                    Selfbuy_head detail = (Selfbuy_head) it.next();
                    trans.addSelfbuyHeadList(detail);
                }
            }
            try {
                it = getMultipleObjects(connection, Selfbuy_detail.class, Server.isAtServerSide() ?
                        "SELECT * FROM posul_selfbuy_detail WHERE tmtranseq=" + transactionNumber :
                        "SELECT * FROM selfbuy_detail WHERE tmtranseq='" + transactionNumber + "'");
            } catch (EntityNotFoundException e) {
            }

            if (it != null) {
                while (it.hasNext()) {
                    Selfbuy_detail detail = (Selfbuy_detail) it.next();
                    trans.addSelfbuyDetailList(detail);
                }
            }
            try {
                it = getMultipleObjects(connection, EcDeliveryHead.class, Server.isAtServerSide() ?
                        "SELECT * FROM ec_delivery_head WHERE transactionNumber=" + transactionNumber :
                        "SELECT * FROM ec_delivery_head WHERE transactionNumber='" + transactionNumber + "'");
            } catch (EntityNotFoundException e) {
            }

            if (it != null) {
                while (it.hasNext()) {
                    EcDeliveryHead detail = (EcDeliveryHead) it.next();
                    trans.addEcDeliveryHeadList(detail);
                }
            }

            // Query associated CardDetail
            trans.setCardDetail(CardDetail.queryByTransactionNumber(connection,
                Integer.parseInt(posNumber), Integer.parseInt(transactionNumber)));

            return trans.cloneForPOS();
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
            return null;
        } finally {
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * 此交易是否存在信用卡紅利折扣.
     */
    public boolean hasCreditCardBonusDiscount() {
        if (PARAM.isCreditCardBonusRegardedAsPayment()) {
            for (int i = 1; i <= 4; i++) {
                Payment p = getPaymentByID(i);
                if (p != null && p.isCreditCardBonusPayment())
                    return true;
            }
            return false;

        } else {
            SI bonusSi = SI.queryBonusDiscount();
            if (bonusSi == null)
                return false;

            String bonusSiId = bonusSi.getSIID();
            for (LineItem lineItem : lineItems) {
                if ("D".equals(lineItem.getDetailCode()) && bonusSiId.equals(lineItem.getPluNumber()))
                    return true;
            }
            return false;
        }
    }

    /**
     * 信用卡紅利折扣.
     */
    public HYIDouble getCreditCardBonusDiscount() {
        if (PARAM.isCreditCardBonusRegardedAsPayment()) {
            for (int i = 1; i <= 4; i++) {
                Payment p = getPaymentByID(i);
                if (p != null && p.isCreditCardBonusPayment())
                    return getPayAmountByID(i);
            }
            return nHYIDouble0();

        } else {
            SI bonusSi = SI.queryBonusDiscount();
            if (bonusSi == null)
                return nHYIDouble0();

            String bonusSiId = bonusSi.getSIID();
            for (LineItem lineItem : lineItems) {
                if ("D".equals(lineItem.getDetailCode()) && bonusSiId.equals(lineItem.getPluNumber()))
                    return lineItem.getAmount();
            }
            return nHYIDouble0();
        }
    }

    /**
     * 他店退貨時，暫存他店店號.
     */
    public String getOtherStoreID() {
        return otherStoreID;
    }

    public void setOtherStoreID(String otherStoreID) {
        this.otherStoreID = otherStoreID;

        //Bruce/20090206/ 暫時將他店店號放在saleman字段
        setSalesman("S=" + otherStoreID);
    }

    /**
     * 是否是支付訂金或尾款的分期付款交易.
     */
    public boolean isDownPaymentOrBackPaymentTransaction() {
        String downPaymentItemNo = PARAM.getDownPaymentItemNo();
        for (LineItem lineItem : lineItems()) {
            if (downPaymentItemNo.equals(lineItem.getItemNumber()))
                return true;
        }
        return false;
    }

    /**
     * 將着付商品去掉，增加一個“應收尾款”支付，支付金額為(着付商品金額 * -1).
     */
    public void removeDownPaymentItem() {
        String downPaymentItemNo = PARAM.getDownPaymentItemNo();
        for (LineItem lineItem : lineItems()) {
            if (downPaymentItemNo.equals(lineItem.getItemNumber())) {
                lineItems().remove(lineItem);
                HYIDouble downPaymentAmount = lineItem.getAmount();
                String downPaymentId = Payment.queryDownPaymentId();
                for (int i = 1; i <= 4; i++) {
                    Payment p = getPaymentByID(i);
                    if (p == null) {
                        setPayNumberByID(i, downPaymentId);
                        setPayAmountByID(i, downPaymentAmount.negate());
                        initForAccum();
                        accumulate();
                        fireEvent(new TransactionEvent(this, TransactionEvent.TRANS_INFO_CHANGED));
                        return;
                    }
                }
            }
        }
    }

    /** 淨支付: 可找零的支付已扣減了找零金額。 */
    public Map<Payment, HYIDouble> getNetPayments() {
        return netPayments;
    }

    public void calcNetPayment() {
        // 計算"淨支付"：將可找零支付，減去找零金額. (溢收部份保留不動)
        int changeablePaymentIdx = -1;
        int changeablePaymentSeq = -1;
        for (int idx = 1; idx <= 4; idx++) {
            Payment payment = getPaymentByID(idx);
            if (payment == null)
                break;
            int paymentSeq = Integer.parseInt(payment.getSeqno());
            if (payment.isChangeable() && paymentSeq > changeablePaymentSeq) { // 找出優先序最大的可找零支付
                changeablePaymentIdx = idx;
                changeablePaymentSeq = paymentSeq;
            }
        }
        // 將可找零支付，減去找零金額
        HYIDouble changeAmount = getChangeAmount();
        for (int idx = 1; idx <= 4; idx++) {
            Payment payment = getPaymentByID(idx);
            HYIDouble payAmount = getPayAmountByID(idx);
            if (idx == changeablePaymentIdx && changeAmount != null &&
                changeAmount.compareTo(new HYIDouble(0)) > 0) {
                netPayments.put(payment, payAmount.subtract(changeAmount));
            } else if (payment != null && payAmount.compareTo(new HYIDouble(0)) > 0) {
                netPayments.put(payment, payAmount); 
            }
        }
    }

    /**
     * Check if the same DaiShou2Bill had been inputed
     *
     * @param daiShouBarcode
     * @return true if exist DaiShou2Bill
     *         true if SQL Exception
     *         false if count > 0
     */
    public static boolean isDaiShou2BillInputed(String daiShouBarcode) {
        // check if duplicated in current Transaction
        for (Object o : POSTerminalApplication.getInstance().getCurrentTransaction().getLineItems()) {
            LineItem li = (LineItem)o;
            if ("O".equals(li.getDetailCode()) && daiShouBarcode.equals(li.getDiscountNumber())) {
                return true;
            }
        }
        // check if duplicated in history transaction in db
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            String sql = "select count(h.tmtranseq)" +
                " from tranhead h, trandetail d" +
                " where h.dealType1='0' and h.dealType2='0' and (h.dealType3='0' or h.dealType3='4')" +
                " and d.tmcode=h.posno" +
                " and d.tmtranseq=h.tmtranseq" +
                " and d.codetx='O'" +
                " and discno='" + daiShouBarcode + "'";
            Object countObj = getValueOfStatement(connection, sql);
            if (countObj != null && CreamToolkit.retrieveIntValue(countObj) > 0) {
                return true;
            }
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return false;
    }

    public static boolean isExistInTranhead(String billNo, int billType) {
        // check if duplicated in history transaction in pos db
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            String additionCondition = "";
            // 5 团购
            // 6 开票
            // 7 代配领取 （开票，不收钱）
            // 8 代配退回 （要退钱)c
            if (billType == 5) {
                additionCondition = " and state like 'T%' ";
            } else if (billType == 6) {
                additionCondition = " and state like 'P%' and dealType2='P' ";
            } else if (billType == 7) {
                additionCondition = " and state like 'D%' and dealType2='P' ";
            } else if (billType == 8) {
                additionCondition = " and state like 'B%' and dealType2='P' ";
            }
            String sql = "select count(h.tmtranseq) from tranhead h" +
                " where annotatedid ='" + billNo + "' " + additionCondition;
            Object countObj = getValueOfStatement(connection, sql);
            if (countObj != null && CreamToolkit.retrieveIntValue(countObj) > 0) {
                return true;
            }
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return false;
    }
    public String getAlipayBuyerLogonId() {
        if (getAlipayList() != null && getAlipayList().size() > 0)
            return "支付宝消费账号:" + getAlipayList().get(0).getBUYER_LOGON_ID();
        return  "";
    }

    public void setPrintType(String printType) {
        this.printType = printType;
    }

    public String getPrintType() {
        return printType;
    }

    /**
     * 交易商品件数合计（教超打印用）
     */
    List<String> countDetailTypes = Arrays.asList(new String[] {"S", "R", "I", "O", "Q"});
    public int getDetailNumber() {
        HYIDouble number = HYIDouble.zero();
        if (lineItems != null) {
            for (LineItem lineItem : lineItems) {
                if (!lineItem.getRemoved() && countDetailTypes.contains(lineItem.getDetailCode().toUpperCase())) {
                    number.addMe(lineItem.getQuantity());
                }
            }
        }
        return number.intValue();
    }
}
