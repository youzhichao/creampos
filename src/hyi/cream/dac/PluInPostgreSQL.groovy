package hyi.cream.dac

import hyi.cream.exception.EntityNotFoundException
import hyi.cream.inline.Server
import hyi.cream.util.CreamToolkit
import hyi.cream.util.DbConnection
import hyi.cream.util.HYIDouble

import java.sql.SQLException
import java.sql.Time
import java.text.SimpleDateFormat

import static java.math.BigDecimal.ROUND_HALF_UP

/**
 * For creating PLU PostgreSQL dump file.
 *
 * @author Bruce You on 8/5/15.
 */
class PluInPostgreSQL extends PLU {

    static String ns(String s) {
        s ?: ''
    }

    static Integer ni(Integer s) {
        s ?: 0
    }

    static HYIDouble nn(HYIDouble s) {
        s ?: HYIDouble.zero()
    }

    static java.sql.Date nd(java.sql.Date d) {
        d ?: new java.sql.Date(0L)
    }

    static Time nt(Time d) {
        d ?: new Time(0L)
    }

    static String fd(java.sql.Date d) {
        new SimpleDateFormat('yyyy-MM-dd').format(d)
    }

    static String ft(Time d) {
        new SimpleDateFormat('HH:mm:ss').format(d)
    }

    /**
     * Create PostgreSQL dump file for PLU.
     *
     * @return A list with a dump File and its record count.
     */
    static Collection getAllObjectsForPOS() {
        File dumpFile = File.createTempFile("plu", ".sql")
        CreamToolkit.logMessage "Create plu dump file: ${dumpFile.absolutePath}"

        int count = 0
        dumpFile.withWriter('UTF-8') { writer ->
            writer << '''-- PostgreSQL database dump
SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;
SET search_path = public, pg_catalog;
BEGIN TRANSACTION;
DELETE FROM plu;
COPY plu (pluno, itemno, saletype, pluname, printname, catno, midcatno, microcatno, pluprice, plupmprice, plupmdates, plupmdatee, plupmtimes, plupmtimee, plumbprice, pluopprice1, pluopprice2, plusign1, plusign2, discbegdt, discenddt, plusign3, plutax, mamno, depid, shipno, smallunit, itemtype, magatype, magacount, custordtype, custordprice1, isord) FROM stdin;
'''
            DbConnection connection = null
            try {
                connection = CreamToolkit.getPooledConnection()
                Collection dacs = getMultipleObjects(connection, PLU.class,
                    'SELECT * FROM posdl_plu', getScToPosFieldNameMap())

                String kindType
                String signType
                String openPrice
                for (PLU plu in dacs) {
                    // 1. Determine values of PLUSIGN2, PLUSIGN3 from __KINDTYPE, __SIGNTYPE,
                    //    and __OPENPRICE, then add them into fieldMap.
                    kindType = (String)plu.getFieldValue("__KINDTYPE");
                    signType = (String)plu.getFieldValue("__SIGNTYPE");
                    openPrice = (String)plu.getFieldValue("__OPENPRICE");

                    //if (kindType != null && signType != null && openPrice != null) {
                    Integer[] attr = getPluSpecialValue(kindType, signType, openPrice)
                    plu.setFieldValue("PLUSIGN2", attr[0])
                    plu.setFieldValue("PLUSIGN3", attr[1])

                    // 2. Remove __KINDTYPE, __SIGNTYPE, and __OPENPRICE.
                    plu.fieldMap.remove("__KINDTYPE")
                    plu.fieldMap.remove("__SIGNTYPE")
                    plu.fieldMap.remove("__OPENPRICE")

                    Server.setFakeAtPosSide(true)
                    writer << "${plu.pluNumber.trim()}\t\
${plu.itemNumber.trim()}\t\
${ns(plu.saleType)}\t\
${ns(plu.screenName)}\t\
${ns(plu.printName)}\t\
${ns(plu.categoryNumber)}\t\
${ns(plu.midCategoryNumber)}\t\
${ns(plu.microCategoryNumber)}\t\
${nn(plu.unitPrice).setScale(2, ROUND_HALF_UP)}\t\
${nn(plu.specialPrice).setScale(2, ROUND_HALF_UP)}\t\
${fd(nd(plu.discountStartDate))}\t\
${fd(nd(plu.discountEndDate))}\t\
${ft(nt(plu.discountStartTime))}\t\
${ft(nt(plu.discountEndTime))}\t\
${nn(plu.memberPrice).setScale(2, ROUND_HALF_UP)}\t\
${nn(plu.discountPrice1).setScale(2, ROUND_HALF_UP)}\t\
${nn(plu.discountPrice2).setScale(2, ROUND_HALF_UP)}\t\
${ni(plu.getSIGroup())}\t\
${ni(plu.attribute1)}\t\
${fd(nd(plu.discountBeginDate))}\t\
${fd(nd(plu.discountEnd2))}\t\
${ni(plu.attribute2)}\t\
${ns(plu.taxType)}\t\
${ns(plu.mixAndMatchNumber)}\t\
${ns(plu.depID)}\t\
${ns(plu.shipNumber)}\t\
${ns(plu.smallUnit)}\t\
${ni(plu.itemtype)}\t\
${ni(plu.magatype)}\t\
${ni(plu.magacount)}\t\
${ni(plu.custordtype)}\t\
${nn(plu.custordprice1)}\t\
${ns(plu.isord)}\n"
                    Server.setFakeAtPosSide(false)
                    count++
                }

            } catch (EntityNotFoundException ignored) {
            } catch (SQLException e) {
                CreamToolkit.logMessage e
            } finally {
                CreamToolkit.releaseConnection(connection)
            }

            writer << '''\\.
COMMIT TRANSACTION;
'''
        }

        return [dumpFile, count]
    }
}
