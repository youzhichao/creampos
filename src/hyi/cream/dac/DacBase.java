package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.groovydac.Param;
import hyi.cream.inline.Server;
import hyi.cream.util.*;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Base class of DAC (Data Access Class).
 * 
 * <pre>
 * Meyer/1.5/2003-02-20 
 * Add method: getScToPosFieldNameMap(), getPosToScFieldNameArray()
 *  
 * Bruce/1.5/2002-03-10/ Add/Modify some methods for preparing to use in new inline: 
 * A public Map getFieldMap() 
 * A public static List getMultipleObjects(Class classObject, String selectStatement, Map fieldNameMap) 
 * M public boolean insert() 
 * A public boolean insert(boolean needQueryAgain) 
 * A public boolean deleteAll() 
 * A public boolean copyFile(String source, String destination) 
 * A public boolean backupAll() 
 * A public boolean restoreAll() 
 * A public boolean getUseClientSideTableName() 
 * A public void setUseClientSideTableName(boolean useCleintSideTableName) 
 * A public String getInsertSqlStatement(boolean useCleintSideTableName) 
 * M public boolean update(): use StringBuffer instead of String 
 * M public boolean insert(boolean needQueryAgain): use StringBuffer instead of String
 * </pre>
 * 
 * @author Dai, Bruce
 * @version 1.5
 */
public abstract class DacBase implements Serializable, Cloneable {

    transient public static final String VERSION = "1.5";
    transient protected boolean useCleintSideTableName;
    transient protected static final Integer integer0 = new Integer(0);
    transient protected static final SimpleDateFormat dfyyyyMMdd = CreamCache.getInstance().getDateFormate();
    transient protected static final SimpleDateFormat dfyyyyMMddHHmmss = CreamCache.getInstance().getDateTimeFormate();
    transient protected static final Param PARAM = Param.getInstance(); 

    protected UppercaseKeyMap fieldMap;
    private HashSet dirtyFields;
    private boolean allFieldsDirty;

    //transient static boolean useJVM;
    //private static boolean windows;
    //static {
    //    windows = (System.getProperties().get("os.name").toString().indexOf("Windows") != -1);
    //    //useJVM = GetProperty.getInlineClientUseJVM("yes").equalsIgnoreCase("yes");
    //}

    DacBase() {
        fieldMap = new UppercaseKeyMap();

        // a set always contains upper-case string
        dirtyFields = new HashSet() {
            private static final long serialVersionUID = 1L;

            public boolean add(Object value) {
                return super.add(((String)value).toUpperCase());
            }

            public boolean contains(Object value) {
                return super.contains(((String)value).toUpperCase());
            }
        };
    }

    public Map getFieldMap() {
        return fieldMap;
    }

    public UppercaseKeyMap getValues() {
        UppercaseKeyMap newMap = new UppercaseKeyMap();
        newMap.putAll(fieldMap);
        return newMap;
    }

    /**
     * 复制方法。
     */
    public Object clone() {
        try {
            DacBase dac = (DacBase)super.clone();
            Map fieldMap = dac.getFieldMap();
            dac.fieldMap = new UppercaseKeyMap();
            if (fieldMap == null)
                dac.fieldMap = null;
            else
                dac.fieldMap.putAll(fieldMap);
            return dac;
        } catch (CloneNotSupportedException e) {
            // Cannot happen
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Define primary key fields.
     */
    public abstract List getPrimaryKeyList();

//    /**
//     * Query a DAC object by the primary key, load into itself.
//     */
//    public void queryByPrimaryKey() throws EntityNotFoundException, SQLException {
//        List primaryKeyList = getPrimaryKeyList();
//        StringBuffer selectStatement = new StringBuffer();
//        selectStatement.append("SELECT * FROM ").append(getInsertUpdateTableName()).append(
//            " WHERE ");
//        for (int i = 0; i < primaryKeyList.size(); i++) {
//            String fieldname = (String)primaryKeyList.get(i);
//            if (i > 0) {
//                selectStatement.append(" AND ");
//            }
//            if (fieldMap.get(fieldname) instanceof java.util.Date) {
//                java.util.Date d = (java.util.Date)fieldMap.get(fieldname);
//                selectStatement.append(fieldname).append("='").append(
//                    CreamCache.getInstance().getDateTimeFormate().format(d)).append("'");
//            } else
//                selectStatement.append(fieldname).append("='").append(fieldMap.get(fieldname))
//                    .append("'");
//        }
//        getSingleObject(selectStatement.toString());
//    }

    /**
     * Reload myself from database.
     */
    public void load(DbConnection connection) throws EntityNotFoundException, SQLException {
        List primaryKeyList = getPrimaryKeyList();
        StringBuffer selectStatement = new StringBuffer();
        selectStatement.append("SELECT * FROM ").append(getInsertUpdateTableName()).append(
            " WHERE ");
        for (int i = 0; i < primaryKeyList.size(); i++) {
            String fieldname = (String)primaryKeyList.get(i);
            if (i > 0) {
                selectStatement.append(" AND ");
            }
            if (fieldMap.get(fieldname) instanceof java.util.Date) {
                java.util.Date d = (java.util.Date)fieldMap.get(fieldname);
                selectStatement.append(fieldname).append("='").append(
                    CreamCache.getInstance().getDateTimeFormate().format(d)).append("'");
            } else
                selectStatement.append(fieldname).append("='").append(fieldMap.get(fieldname))
                    .append("'");
        }
        getSingleObject(connection, selectStatement.toString());
    }

    /**
     * Test if ths DAC object has already existed in database.
     */
    public boolean exists(DbConnection connection) {
        List primaryKeyList = getPrimaryKeyList();
        StringBuffer selectStatement = new StringBuffer();
        for (int i = 0; i < primaryKeyList.size(); i++) {
            String fieldname = (String)primaryKeyList.get(i);
            // 只选择一个字段可使效率提高
            if (i == 0) {
                selectStatement.append("SELECT ").append(fieldname).append(" FROM ").append(
                    getInsertUpdateTableName()).append(" WHERE ");
            }
            if (i > 0)
                selectStatement.append(" AND ");
            if (fieldMap.get(fieldname) instanceof java.util.Date) {
                java.util.Date d = (java.util.Date)fieldMap.get(fieldname);
                selectStatement.append(fieldname).append("='").append(
                    CreamCache.getInstance().getDateTimeFormate().format(d)).append("'");
            } else
                selectStatement.append(fieldname).append("='").append(fieldMap.get(fieldname))
                    .append("'");
        }
        if (getValueOfStatement(connection, selectStatement.toString()) == null)
            return false;
        else
            return true;
    }

    // /**
    // * Query a DAC object by the primary key.
    // */
    // public boolean deleteByPrimaryKey() {
    // List primaryKeyList = getPrimaryKeyList();
    // StringBuffer deleteStatement = new StringBuffer();
    // deleteStatement.append("delete from ").append(
    // getInsertUpdateTableName()).append(" where ");
    // for (int i = 0; i < primaryKeyList.size(); i++) {
    // String fieldname = (String) primaryKeyList.get(i);
    // if (i > 0) {
    // deleteStatement.append(" and ");
    // }
    // if (fieldMap.get(fieldname) instanceof java.util.Date) {
    // java.util.Date d = (java.util.Date) fieldMap.get(fieldname);
    // deleteStatement.append(fieldname).append("='")
    // .append(
    // CreamCache.getInstance().getDateTimeFormate()
    // .format(d)).append("'");
    // } else
    // deleteStatement.append(fieldname).append("='").append(
    // fieldMap.get(fieldname)).append("'");
    // }
    // CreamToolkit.logMessage(deleteStatement.toString());
    //
    // return DBToolkit.executeUpdate(deleteStatement.toString());
    // }

    /**
     * Query a DAC object by the primary key.
     */
    public boolean deleteByPrimaryKey(DbConnection connection) throws SQLException {
        List primaryKeyList = getPrimaryKeyList();
        StringBuffer deleteStatement = new StringBuffer();
        deleteStatement.append("delete from ").append(getInsertUpdateTableName()).append(" where ");
        for (int i = 0; i < primaryKeyList.size(); i++) {
            String fieldname = (String)primaryKeyList.get(i);
            if (i > 0) {
                deleteStatement.append(" and ");
            }
            if (fieldMap.get(fieldname) instanceof java.util.Date) {
                java.util.Date d = (java.util.Date)fieldMap.get(fieldname);
                deleteStatement.append(fieldname).append("='").append(
                    CreamCache.getInstance().getDateTimeFormate().format(d)).append("'");
            } else
                deleteStatement.append(fieldname).append("='").append(fieldMap.get(fieldname))
                    .append("'");
        }
        String sql = deleteStatement.toString();
        if (DBToolkit.executeUpdate(connection, sql) == 1){
            CreamToolkit.logMessage(sql);
            return true;
        } else {
            return false;
        }
    }

    public static int fillResultMap(ResultSet resultSet, HashMap map) {
        return fillResultMap(resultSet, map, 0);
    }

    //ResultSet ==> Map
    public static int fillResultMap(ResultSet resultSet, HashMap map, int lowerUpperCase) {
        int count = -1;
        try {
            ResultSetMetaData metaData = resultSet.getMetaData();
            if (resultSet.next()) {
                int t = 0;
                count = metaData.getColumnCount();
                for (int i = 1; i <= count; i++) {
                    Object o = resultSet.getObject(i);
                    if (o != null) {
                        t = metaData.getColumnType(i);
                        if (t == Types.NUMERIC || t == Types.DECIMAL) {
                            o = new HYIDouble(((BigDecimal)o).doubleValue());
                        }
                    }
                    if (lowerUpperCase == 0) {
                        map.put(metaData.getColumnName(i), o);
                    } else if (lowerUpperCase == 1) {
                        map.put(metaData.getColumnName(i).toUpperCase(), o);
                    } else if (lowerUpperCase == -1) {
                        map.put(metaData.getColumnName(i).toLowerCase(), o);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
        }
        return count;
    }

    // Bruce/20070303/ Change return type from Map to Object to prevent problem of
    // lowercase/uppercase
    // field name among different databases.
    public static Object getValueOfStatement(DbConnection connection, String selectStatement) {
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            Object value = null;
            if (resultSet.next()) {
                value = resultSet.getObject(1);
                if (value != null) {
                    int type = metaData.getColumnType(1);
                    if (type == Types.NUMERIC || type == Types.DECIMAL)
                        value = new HYIDouble(((BigDecimal)value).doubleValue());

                    // Bruce/20051007> 解决使用新版本JDBC driver的问题
                    if (value instanceof Short)
                        value = new Integer(((Short)value).intValue());
                    else if (value instanceof Long)
                        value = new Integer(((Long)value).intValue());
                }
            }
            return value;
        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + selectStatement);
            CreamToolkit.logMessage(e);
            return null;
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }
        }
    }

//    public void getSingleObject(String selectStatement) throws EntityNotFoundException,
//        SQLException {
//
//        DbConnection connection = null;
//        Statement statement = null;
//        ResultSet resultSet = null;
//        try {
//            connection = CreamToolkit.getPooledConnection();
//            statement = connection.createStatement();
//            resultSet = statement.executeQuery(selectStatement);
//            ResultSetMetaData metaData = resultSet.getMetaData();
//            if (resultSet.next()) {
//                int t = 0;
//                for (int i = 1; i <= metaData.getColumnCount(); i++) {
//                    Object o = resultSet.getObject(i);
//                    if (o != null) {
//                        t = metaData.getColumnType(i);
//                        if (t == Types.NUMERIC || t == Types.DECIMAL) {
//                            o = new HYIDouble(((BigDecimal)o).doubleValue());
//                        }
//                    }
//                    this.fieldMap.put(metaData.getColumnName(i), o);
//                }
//            } else {
//                throw new EntityNotFoundException("Cannot find entity by: " + selectStatement);
//            }
//        } catch (SQLException e) {
//            CreamToolkit.logMessage("SQLException: " + selectStatement);
//            throw e;
//        } finally {
//            try {
//                if (resultSet != null)
//                    resultSet.close();
//                if (statement != null)
//                    statement.close();
//                if (connection != null)
//                    CreamToolkit.releaseConnection(connection);
//            } catch (SQLException e) {
//                e.printStackTrace(CreamToolkit.getLogger());
//            }
//        }
//    }

    public void getSingleObject(DbConnection connection, String selectStatement)
        throws EntityNotFoundException, SQLException {

        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            if (resultSet.next()) {
                int t = 0;
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object o = resultSet.getObject(i);
                    if (o != null) {
                        t = metaData.getColumnType(i);
                        if (t == Types.NUMERIC || t == Types.DECIMAL) {
                            o = new HYIDouble(((BigDecimal)o).doubleValue());
                        }
                    }
                    this.fieldMap.put(metaData.getColumnName(i), o);
                }
            } else {
                throw new EntityNotFoundException("Cannot find entity by: " + selectStatement);
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + selectStatement);
            throw e;
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        }
    }
    
    void setAllFieldsDirty(boolean b) {
        allFieldsDirty = b;
    }

    boolean isAllFieldsDirty() {
        return allFieldsDirty;
    }

    /**
     * Update the current DAC object into database(locate the record by the primary key).
     */
    public void update(DbConnection connection) throws SQLException, EntityNotFoundException {
        if (!isAllFieldsDirty() && dirtyFields.isEmpty())
            return;

        String tableName = getInsertUpdateTableName();
        Iterator iterator = ((Set)fieldMap.keySet()).iterator();
        // ResultSet resultSet = null;
        Object fieldValue = null;
        String fieldName = "";
        int updateCount = 0;

        StringBuffer nameParams = new StringBuffer(200);
        while (iterator.hasNext()) {
            fieldName = (String)iterator.next();

            // Meyer/2003-03-11/add field into update sql statement only when
            // the field existed
            if (isFieldExist(fieldName)) {

                if (!isAllFieldsDirty() // 如果call过makeAllFieldsDirty()，则全部进入update statement
                    && !dirtyFields.contains(fieldName))
                    continue;

                if (getFieldValue(fieldName) == null) {
                    fieldValue = new Object();
                } else {
                    fieldValue = getFieldValue(fieldName);
                    if (fieldValue instanceof java.util.Date) {
                        java.util.Date d = (java.util.Date)fieldValue;
                        nameParams.append(escapeFieldName(fieldName)).append("='").append(
                            dfyyyyMMddHHmmss.format(d).toString()).append("',");
                    } else if (fieldValue instanceof String) {
                        nameParams.append(escapeFieldName(fieldName)).append(" = '").append(
                            escapeSpecialCharacter((String)fieldValue)).append("',");
                    } else {
                        nameParams.append(escapeFieldName(fieldName)).append(" = ").append(fieldValue).append(",");
                    }
                }
            }
        }

        StringBuffer updateString = new StringBuffer(300);
        updateString.append("update ").append(tableName).append(" set ").append(
            nameParams.substring(0, nameParams.length() - 1)).append(" where ");
        List primaryKeyList = getPrimaryKeyList();
        for (int i = 0; i < primaryKeyList.size(); i++) {
            String fieldname = (String)primaryKeyList.get(i);
            if (i > 0) {
                updateString.append(" and ");
            }
            updateString.append(fieldname).append("='").append(fieldMap.get(fieldname)).append("'");
        }
//CreamToolkit.logMessage("udpate sql : " + updateString.toString());
        updateCount = DBToolkit.executeUpdate(connection, updateString.toString());
        dirtyFields.clear();
        setAllFieldsDirty(false);
        if (updateCount == 0)
            throw new EntityNotFoundException("Cannot find entity to update by: "
                + updateString.toString());
    }

    public static void executeQuery(DbConnection connection, String query) throws SQLException {
        DBToolkit.executeUpdate(connection, query);
    }

    private static String escapeSpecialCharacter(String fieldData) {
        int fromIndex = 0;
        while ((fromIndex = fieldData.indexOf("'", fromIndex)) != -1) {
            fieldData = fieldData.substring(0, fromIndex + 1) + "'"
                + fieldData.substring(fromIndex + 1, fieldData.length());
            fromIndex += 2;
        }

        fromIndex = 0;
        while ((fromIndex = fieldData.indexOf("\\", fromIndex)) != -1) {
            fieldData = fieldData.substring(0, fromIndex + 1) + "\\"
                + fieldData.substring(fromIndex + 1, fieldData.length());
            fromIndex += 2;
        }
        return fieldData;
    }

    /**
     * Constructs a DAC object from a row in ResultSet.
     */
    void constructFromResultSet(ResultSet resultSet) throws SQLException {
        ResultSetMetaData metaData = resultSet.getMetaData();
        int t = 0;
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            Object o = resultSet.getObject(i);
            if (o != null) {
                t = metaData.getColumnType(i);
                if (t == Types.NUMERIC || t == Types.DECIMAL) {
                    o = new HYIDouble(((BigDecimal)o).doubleValue());
                } else if (t == Types.CHAR) { // 因為PostgreSQL會在CHAR類型字段補滿空白
                    o = StringUtils.stripEnd((String)o, null);
                }
            }
            fieldMap.put(metaData.getColumnName(i), o);
        }
    }

    private void setBeanProperty(Map db2PropMap, String dbFieldName, Object fieldValue)
    {
        String beanPropName;
        if (db2PropMap == null)
            beanPropName = dbFieldName;
        else {
            beanPropName = (String)db2PropMap.get(dbFieldName);
            if (beanPropName == null)
                beanPropName = dbFieldName;
        }

        try {
            getClass().getDeclaredField(beanPropName).set(this, fieldValue);
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
            e.printStackTrace();
        }
    }

    /**
     * Subclass-provided database field name to Dac bean property name map.
     */
    protected Map getDatabaseFieldToBeanPropertyMap() {
        return null;
    }

    public void convertFieldMapToProperties() {
        Map fieldMap = getFieldMap();
        Map db2PropMap = getDatabaseFieldToBeanPropertyMap();
        Iterator fieldIter = fieldMap.keySet().iterator();
        while (fieldIter.hasNext()) {
            String dbFieldName = (String)fieldIter.next();
            Object fieldValue = fieldMap.get(dbFieldName);
            setBeanProperty(db2PropMap, dbFieldName, fieldValue);
        }

        // discard field map
        setFieldMap(null);
    }

    /**
     * Constructs a DAC object from a row in ResultSet, and directly set field value into
     * object properties instead of the fieldMap.
     */
    public void constructPojoFromResultSet(ResultSet resultSet) throws SQLException {
        ResultSetMetaData metaData = resultSet.getMetaData();
        int t = 0;
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            String dbFieldName = metaData.getColumnName(i).toUpperCase();
            Object fieldValue = resultSet.getObject(i);
            if (fieldValue != null) {
                t = metaData.getColumnType(i);
                if (t == Types.NUMERIC || t == Types.DECIMAL) {
                    fieldValue = new HYIDouble(((BigDecimal)fieldValue).doubleValue());
                } else if (t == Types.CHAR) { // 因為PostgreSQL會在CHAR類型字段補滿空白
                    fieldValue = StringUtils.stripEnd((String)fieldValue, null);
                }
            }
            setBeanProperty(getDatabaseFieldToBeanPropertyMap(), dbFieldName, fieldValue);
        }
        setFieldMap(null);
    }
    
    /**
     * Get field value object by field name.
     */
    public Object getFieldValue(String fieldName) {
        Object value;
        if (fieldMap == null) {
            try {
                value = getClass().getDeclaredField(fieldName).get(this);
            } catch (Exception e) {
                e.printStackTrace(CreamToolkit.getLogger());
                e.printStackTrace();
                return null;
            }
        } else {
            value = fieldMap.get(fieldName);
            if (value == null)
                value = fieldMap.get(fieldName);
        }

        // Bruce/20051006> 解决使用新版本JDBC driver的问题
        if (value instanceof Short)
            value = new Integer(((Short)value).intValue());
        else if (value instanceof Long)
            value = new Integer(((Long)value).intValue());

        return value;
    }

    /**
     * Get HYIDouble field value object by field name.
     */
    public HYIDouble getHYIDoubleFieldValue(String fieldName) {
        Object value = fieldMap.get(fieldName);
        HYIDouble result = null;
        if (value == null)
            value = fieldMap.get(fieldName/*.toLowerCase()*/);
        if (value != null) {
            result = new HYIDouble(((Double)value).doubleValue());
        }
        return result;
    }

    public void setFieldValue(String fieldName, Object fieldValue) {
        if (fieldName.equals(""))
            return;
        // <example for intercept the setter of given field: >
        // if ("SALEAMT".equals(fieldName)){
        // System.out.println("SALEAMT=" + fieldValue);
        // }
        fieldMap.put(fieldName, fieldValue);
        dirtyFields.add(fieldName);
    }

    /**
     * Query by a SELECT statement and construct a DAC object with type 'classObject'. classObject
     * must be derived from DacBase.
     */
    public static <T extends DacBase> T getSingleObject(DbConnection connection,
        Class<T> classObject, String selectStatement) throws SQLException, EntityNotFoundException {

        Statement statement = null;
        ResultSet resultSet = null;
        T dacObject;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            if (resultSet.next()) {
                dacObject = classObject.newInstance();
                dacObject.constructFromResultSet(resultSet);
                return dacObject;
            } else {
                throw new EntityNotFoundException("Cannot find entity by: " + selectStatement);
            }
        } catch (EntityNotFoundException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new EntityNotFoundException("Cannot find entity by: " + selectStatement);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }
        }
    }

    public static <T extends DacBase> T getSinglePojoObject(DbConnection connection,
        Class<T> classObject, String selectStatement) throws SQLException, EntityNotFoundException {
        
        Statement statement = null;
        ResultSet resultSet = null;
        T dacObject = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            if (resultSet.next()) {
                dacObject = classObject.newInstance();
                dacObject.constructPojoFromResultSet(resultSet);
                return dacObject;
            } else {
                throw new EntityNotFoundException("Cannot find entity by: " + selectStatement);
            }
        } catch (EntityNotFoundException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new EntityNotFoundException("Cannot find entity by: " + selectStatement);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }
        }
    }
    
    public static <T extends DacBase> List<T> getMultipleObjects2(DbConnection connection,
        Class<T> classObject, String selectStatement) throws EntityNotFoundException, SQLException {

        Statement statement = null;
        ResultSet resultSet = null;
        T dacObject = null;
        ArrayList<T> collection = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            while (resultSet.next()) {
                dacObject = classObject.newInstance();
                dacObject.constructFromResultSet(resultSet);
                if (collection == null)
                    collection = new ArrayList<T>();
                collection.add(dacObject);
            }
            if (collection == null)
                throw new EntityNotFoundException("Cannot find entity by: " + selectStatement);
            else
                return collection;

        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + selectStatement);
            throw e;

        } catch (InstantiationException e) {
            throw new EntityNotFoundException(e.toString());

        } catch (IllegalAccessException e) {
            throw new EntityNotFoundException(e.toString());

        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }
        }
    }

    public static <T extends DacBase> Iterator<T> getMultipleObjects(DbConnection connection,
        Class<T> classObject, String selectStatement) throws EntityNotFoundException, SQLException {
        List collection = getMultipleObjects2(connection, classObject, selectStatement);
        if (collection != null)
            return collection.iterator();
        else
            return null;
    }

    /**
     * Meyer/2003-02-12 接收一个Map，将其Key Object 转为小写的String后，连同原有Value建立新Map返回
     */
    public static Map toLowerCaseMap(Map origMap) {
        Map lowerCaseMap = new HashMap();
        Iterator iter = origMap.keySet().iterator();
        Object key;
        while (iter.hasNext()) {
            key = iter.next();
            lowerCaseMap.put(key.toString().toLowerCase(), origMap.get(key));
        }
        return lowerCaseMap;
    }

    /**
     * <pre>
     * Meyer/2003-02-12 修改origFieldNameMap为小写Key的Map, 并按小写Key获取Value 改正由于DAC子类中,
     * getAllObjectsForPOS()方法提供的fieldName与实际 数据库大小写不符导致的bug 
     * 
     * Meyer / 2003-02-10 Modified: when the fieldNameMap is null, return the collection for 
     * the same fieldNameMap as orignal table Get multiple DAC object with field name converted
     * </pre>
     */
    public static <T extends DacBase> List<T> getMultipleObjects(DbConnection connection,
        Class<T> classObject, String selectStatement, Map origFieldNameMap)
        throws EntityNotFoundException, SQLException {

        Statement statement = null;
        ResultSet resultSet = null;
        T dacObject = null;
        ArrayList<T> collection = new ArrayList();
        
        Map fieldNameMap = null;
        // Meyer / 2003-02-12
        if (origFieldNameMap != null)
            fieldNameMap = toLowerCaseMap(origFieldNameMap);


        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            while (resultSet.next()) {
                dacObject = classObject.newInstance();
                if (origFieldNameMap != null) {

                    // // Meyer / 2003-02-12
                    // Map fieldNameMap = toLowerCaseMap(origFieldNameMap);

                    ResultSetMetaData metaData = resultSet.getMetaData();
                    int t = 0;
                    for (int i = 1; i <= metaData.getColumnCount(); i++) {

                        // Meyer / 2003-02-12
                        String convertedFieldName = (String)fieldNameMap.get(metaData
                            .getColumnName(i).toLowerCase());

                        if (convertedFieldName != null) {
                            Object o = resultSet.getObject(i);
                            // Bruce> Fix a problem when field type is
                            // "nvarchar" in MSDE while using
                            // JDBC-ODBC connection.
                            if (o == null) {
                                try {
                                    o = resultSet.getString(i);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                t = metaData.getColumnType(i);
                                if (t == Types.NUMERIC || t == Types.DECIMAL) {
                                    o = new HYIDouble(((BigDecimal)o).doubleValue());
                                }
                            }
                            dacObject.setFieldValue(convertedFieldName, o);
                        }
                    }
                } else {
                    dacObject.constructFromResultSet(resultSet);
                }
                collection.add(dacObject);
            }
            if (collection.isEmpty())
                throw new EntityNotFoundException("Cannot find entity by: " + selectStatement);
            else
                return collection;

        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + selectStatement);
            throw e;

        } catch (InstantiationException e) {
            throw new EntityNotFoundException(e.toString());

        } catch (IllegalAccessException e) {
            throw new EntityNotFoundException(e.toString());

        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }
        }
    }

    public void clear() {
        fieldMap.clear();
    }

    public abstract String getInsertUpdateTableName();

    public boolean getUseClientSideTableName() {
        return useCleintSideTableName;
    }

    public void setUseClientSideTableName(boolean useCleintSideTableName) {
        this.useCleintSideTableName = useCleintSideTableName;
    }

    /**
     * Give a field list by table created sequence. Used by DacBase.getInsertSqlStatement().
     * 
     * @return Iterator represented a field list.
     */
    public Iterator getSequentialFieldList() {
        return null;
    }

    public String getDeleteSqlStatement(boolean useCleintSideTableName) {
        setUseClientSideTableName(true);
        String tableName = getInsertUpdateTableName();
        setUseClientSideTableName(false);

        List primaryKeyList = getPrimaryKeyList();
        StringBuffer deleteStatement = new StringBuffer();
        deleteStatement.append("delete from ").append(tableName).append(" where ");
        for (int i = 0; i < primaryKeyList.size(); i++) {
            String fieldname = (String)primaryKeyList.get(i);
            if (i > 0) {
                deleteStatement.append(" and ");
            }
            if (fieldMap.get(fieldname) instanceof java.util.Date) {
                java.util.Date d = (java.util.Date)fieldMap.get(fieldname);
                deleteStatement.append(fieldname).append("='").append(
                    CreamCache.getInstance().getDateTimeFormate().format(d)).append("'");
            } else
                deleteStatement.append(fieldname).append("='").append(fieldMap.get(fieldname))
                    .append("'");
        }
        return deleteStatement.toString();
    }

    public String escapeFieldName(String name)
    {
        if (CreamToolkit.isDatabaseMySQL() && name.equals("INOUT"))
            return "`" + name + "`";
        else
            return name;
    }

    public String getInsertSqlStatement(boolean useCleintSideTableName) {
        setUseClientSideTableName(true);
        String tableName = getInsertUpdateTableName();
        setUseClientSideTableName(false);

        Iterator iterator;
        iterator = getSequentialFieldList();
        boolean needFieldNames = false;
        // 2003-12-16 By ZhaoHong
        if (iterator == null) {
            iterator = (fieldMap.keySet()).iterator();
            // needFieldNames = true;
        }
        needFieldNames = true;

        Object fieldValue;
        String fieldName;

        StringBuffer nameParams = new StringBuffer(200);
        StringBuffer valueParams = new StringBuffer(300);

        // 只有在後端而且useCleintSideTableName=true（需要生成client端用的SQL語句）時，才不需檢查欄位是否存在
        boolean dontHaveToCheckFieldExistence = Server.isAtServerSide() && useCleintSideTableName;

        while (iterator.hasNext()) {
            fieldName = (String)iterator.next();

            if (!dontHaveToCheckFieldExistence && !isFieldExist(fieldName))
                continue;

            if (getFieldValue(fieldName) == null) {
                fieldValue = new Object();
                if (!needFieldNames) {
                    valueParams.append('\'');
                    valueParams.append('\'');
                    valueParams.append(',');
                }
            } else {
                fieldValue = getFieldValue(fieldName);
                if (fieldValue instanceof java.util.Date) {
                    java.util.Date d = (java.util.Date)fieldValue;
                    if (needFieldNames) {
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(',');
                    }
                    valueParams.append('\'');
                    valueParams.append(dfyyyyMMddHHmmss.format(d).toString());
                    valueParams.append('\'');
                    valueParams.append(',');
                } else if (fieldValue instanceof String) {
                    if (needFieldNames) {
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(',');
                    }
                    valueParams.append('\'');
                    valueParams.append(escapeSpecialCharacter((String)fieldValue));
                    valueParams.append('\'');
                    valueParams.append(',');
                } else {
                    if (needFieldNames) {
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(',');
                    }
                    valueParams.append('\'');
                    valueParams.append(fieldValue);
                    valueParams.append('\'');
                    valueParams.append(',');
                }
            }
        }

        if ((needFieldNames && nameParams.length() == 0) || valueParams.length() == 0) {
            return "";
        } else {
            if (needFieldNames)
                return "insert into " + tableName + "("
                    + nameParams.substring(0, nameParams.length() - 1) + ") values ("
                    + valueParams.substring(0, valueParams.length() - 1) + ")";
            else
                return "insert into " + tableName + " values ("
                    + valueParams.substring(0, valueParams.length() - 1) + ")";
        }
    }

    public String getInsertSQLStatement() {
        String tableName = getInsertUpdateTableName();
        Iterator iterator = (fieldMap.keySet()).iterator();
        Object fieldValue;
        String fieldName;
        StringBuffer nameParams = new StringBuffer(200);
        StringBuffer valueParams = new StringBuffer(300);

        while (iterator.hasNext()) {
            fieldName = (String)iterator.next();
            // Meyer/2003-03-11/add field into insert sql statement only when
            // the field existed
            if (isFieldExist(fieldName)) {
                if (getFieldValue(fieldName) == null) {
                    fieldValue = new Object();
                } else {
                    fieldValue = getFieldValue(fieldName);
                    if (fieldValue instanceof java.util.Date) {
                        java.util.Date d = (java.util.Date)fieldValue;
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(",");
                        valueParams.append("'");
                        valueParams.append(dfyyyyMMddHHmmss.format(d).toString());
                        valueParams.append("',");
                    } else if (fieldValue instanceof String) {
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(",");
                        valueParams.append("'");
                        valueParams.append(escapeSpecialCharacter((String)fieldValue));
                        valueParams.append("',");
                    } else {
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(",");
                        valueParams.append("'");
                        valueParams.append(fieldValue.toString());
                        valueParams.append("',");
                    }
                }
            }
        }
        return "insert into " + tableName + "(" + nameParams.substring(0, nameParams.length() - 1)
            + ") values (" + valueParams.substring(0, valueParams.length() - 1) + ")";
    }

    /**
     * INSERT within a database transaction.
     */
    public void insert(DbConnection connection, boolean needQueryAgain) throws SQLException {
        String insertString = getInsertSQLStatement();
        DBToolkit.execute(connection, insertString);
        if (needQueryAgain) {
            try {
                load(connection);
            } catch (EntityNotFoundException e) {
                // Almost impossible! I don't want to spread a rarely-happened exception,
                // so throw it as a SQLException instead.
                throw new SQLException(e.toString());
            }
        }
    }

    /**
     * INSERT within a database transaction.
     */
    public static int update(DbConnection connection, String updateString) throws SQLException {
        int s = DBToolkit.executeUpdate(connection, updateString);
        return s;
    }

    /**
     * INSERT within a database transaction, and SELECT it out again after INSERT.
     */
    public void insert(DbConnection connection) throws SQLException {
        insert(connection, true);
    }

    // public boolean insert() {
    // return insert(true);
    // }
    //
    // public boolean insert(boolean needQueryAgain) {
    // String insertString = getInsertSQLStatement();
    // boolean result = DBToolkit.execute(insertString);
    // if (result && needQueryAgain)
    // queryByPrimaryKey();
    // return result;
    // }

    Process c = null;

    int exitStatus = -10000;

//    /**
//     * Flush OS's buffer by "sync" command. Only on Linux.
//     */
//    protected void flushOSBuffer() {
//        try {
//            if (!windows && !hyi.cream.inline.Server.serverExist()
//                && GetProperty.getFlushOSBuffer("no").equalsIgnoreCase("yes")) {
//                Runnable check = new Runnable() {
//                    public void run() {
//                        for (int i = 0; i < 5; i++) {
//                            try {
//                                Thread.sleep(1000);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                                return;
//                            }
//                            // System.out.println(exitStatus + "|" + i);
//                            if (exitStatus == 0)
//                                return;
//                        }
//                        c.destroy();
//                    }
//                };
//                (new Thread(check)).start();
//                c = Runtime.getRuntime().exec("/bin/sync");
//                exitStatus = c.waitFor();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }

    // /**
    // * Delete all records in the table.
    // *
    // * @return false if failed, true otherwise.
    // */
    // public boolean deleteAll() {
    // return DBToolkit.execute("DELETE FROM " + getInsertUpdateTableName());
    // }

    /**
     * Delete all records in the table within a database transaction.
     */
    public void deleteAll(DbConnection connection) throws SQLException {
        DBToolkit.execute(connection, "DELETE FROM " + getInsertUpdateTableName());
    }

    public boolean copyFile(String source, String destination) {
        File src = new File(source);
        if (!src.exists())
            return false;
        File dest = new File(destination);
        try {
            java.io.BufferedInputStream in = new java.io.BufferedInputStream(
                new java.io.FileInputStream(src));
            java.io.BufferedOutputStream out = new java.io.BufferedOutputStream(
                new java.io.FileOutputStream(dest));
            int c;
            // System.out.println("Copy file " + source + " to " + destination);
            while ((c = in.read()) != -1) {
                out.write(c);
            }
            in.close();
            out.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

//    private class FileSelector implements FilenameFilter {
//        private File dir = null;
//
//        private String name = null;
//
//        public FileSelector(File dir, String name) {
//            this.dir = dir;
//            this.name = name;
//        }
//
//        public boolean accept(File dir, String name) {
//            if (name.trim().startsWith(this.name.trim()) && dir.equals(this.dir))
//                return true;
//            else
//                return false;
//
//        }
//    }

//    final int BACKUP = 0;
//
//    final int RESTORE = 1;

//    private boolean backupAndRestoreCream(int br) {
//        String dataDir = GetProperty.getMySQLDataDirectory("");
//        String backupDir = GetProperty.getMySQLBackupDirectory("");
//        if (dataDir.equals("") || backupDir.equals("")) {
//            CreamToolkit
//                .logMessage("You must setup MySQLDataDirectory and MySQLBackupDirectory in conf file.");
//
//            CreamPropertyUtil prop = CreamPropertyUtil.getInstance();
//            prop.setProperty("MySQLDataDirectory", "/var/lib/pgsql/data");
//            prop.setProperty("MySQLBackupDirectory", "/tmp");
//            prop.depositInIndependentTransaction();
//
//            dataDir = "/var/lib/pgsql/data";
//            backupDir = "/tmp";
//        }
//        if (!dataDir.endsWith(File.separator))
//            dataDir += File.separator;
//        String url = GetProperty.getDatabaseURL("");
//
//        int questionMarkPosition = url.indexOf('?');
//        int slashMarkPosition = url.lastIndexOf('/');
//        // dataDir += url.substring(url.lastIndexOf('/') + 1);
//        if (questionMarkPosition > slashMarkPosition) {
//            dataDir += url.substring(slashMarkPosition + 1, questionMarkPosition);
//        } else {
//            dataDir += url.substring(url.lastIndexOf('/') + 1);
//        }
//        // System.out.println("dataDir: " + dataDir);
//        if (!dataDir.endsWith(File.separator))
//            dataDir += File.separator;
//
//        if (!backupDir.endsWith(File.separator))
//            backupDir += File.separator;
//
//        File srcDir;
//        String destDir;
//        if (br == BACKUP) {
//            srcDir = new File(dataDir);
//            destDir = backupDir;
//        } else {
//            srcDir = new File(backupDir);
//            destDir = dataDir;
//        }
//        File[] all = srcDir.listFiles(new FileSelector(srcDir, this.getInsertUpdateTableName()
//            + "."));
//        String day = (new SimpleDateFormat("dd")).format(new Date());
//        for (int i = 0; i < all.length; i++) {
//            // File currentFile = all[i];
//            if (!copyFile(all[i].getAbsolutePath(), destDir + all[i].getName()))
//                return false;
//            if (br == BACKUP)
//                copyFile(all[i].getAbsolutePath(), destDir + all[i].getName() + day);
//        }
//        return true;
//    }

//    /**
//     * Backup table files. Copy database files from "MySQLDataDirectory" to "MySQLBackupDirectory".
//     * 
//     * @return true if sucessfully backup, false otherwise.
//     */
//    public boolean backupAll() {
//        return backupAndRestoreCream(BACKUP);
//    }
//
//    /**
//     * Restore table files. Copy database files from "MySQLBackupDirectory" to "MySQLDataDirectory".
//     * 
//     * @return true if sucessfully restored, false otherwise.
//     */
//    public boolean restoreAll() {
//        return backupAndRestoreCream(RESTORE);
//    }

    /**
     * Meyer / 2003-02-20
     */
    public static Map getScToPosFieldNameMap() {
        return new HashMap();
    }

    public static String[][] getPosToScFieldNameArray() {
        return new String[0][0];
    }

    /**
     * Meyer/2003-03-11/
     */
    protected Collection getExistedFieldList() {
        return null;
    }

    public boolean isFieldExist(String fieldName) {
        boolean fieldExist = true;
        Collection existedFieldList = getExistedFieldList();
        if (existedFieldList != null) {
            fieldExist = existedFieldList.contains(fieldName.toUpperCase());
            if (!fieldExist)
                fieldExist = existedFieldList.contains(fieldName.toLowerCase());
            if (!fieldExist)
                fieldExist = existedFieldList.contains(fieldName);
        }
        return fieldExist;
    }

    public static boolean isTableExisted(String tableName) {
        DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM " + tableName + " WHERE 1=2");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }
        }
        return (resultSet != null);
    }

    /**
     * Meyer/2003-02-20 增加getExistedFieldList 根据tableName得到fieldNameList
     */
    public static final Collection getExistedFieldList(String tableName) {
        Collection existedFieldList = new ArrayList();

        DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM " + tableName + " WHERE 1=2");
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                existedFieldList.add(metaData.getColumnName(i).toUpperCase());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return existedFieldList;
    }

    /**
     * Retrieve the record data by the format: "[class_name]\t[first_field]\t[second_field]\t...\n" +
     * "[class_name]\t[first_field]\t[second_field]\t...\n" + ...
     */
    public static String getTabSeperatedData(String className, String selectStatement) {
        DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        StringBuffer retBuffer = new StringBuffer();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            while (resultSet.next()) {
                retBuffer.append(className);
                retBuffer.append('\t');
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    retBuffer.append(resultSet.getString(i));
                    if (i != metaData.getColumnCount())
                        retBuffer.append('\t');
                }
                retBuffer.append('\n');
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return retBuffer.toString();
    }

    private static String toStringData(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ? "" : str);
    }

    private static HYIDouble toHYIDouble(String str) {
        try {
            if (str == null || str.length() == 0 || str.equalsIgnoreCase("null"))
                return new HYIDouble(0);
            else {
                // return new HYIDouble(str).setScale(2);
                return new HYIDouble(str);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return new HYIDouble(0);
        }
    }

    private static Integer toInteger(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ? integer0
            : new Integer(str));
    }

    private static Boolean toBoolean(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ? Boolean.FALSE
            : (str.equalsIgnoreCase("Y") || str.equals("true")) ? Boolean.TRUE : Boolean.FALSE);
    }

    private static java.sql.Date toDate(String str) {
        if (str == null || str.length() == 0 || str.equalsIgnoreCase("null"))
            return null;
        try {
            java.util.Date d = null;
            if (str.length() == 10) {
                d = dfyyyyMMdd.parse(str);
                // if (!useJVM)
                // d = new java.util.Date(d.getTime() + 16 * 60 * 60 * 1000);
            } else if (str.length() == 19) {
                d = dfyyyyMMddHHmmss.parse(str);
            }
            if (d == null)
                return null;
            else
                return new java.sql.Date(d.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static java.sql.Time toTime(String str) {
        if (str == null || str.length() == 0 || str.equalsIgnoreCase("null"))
            return null;
        java.sql.Time t = new java.sql.Time(0);
        if (str.lastIndexOf(":") != -1) {
            try {
                StringTokenizer st0 = new StringTokenizer(str, ":");
                String hour = st0.nextToken().trim();
                String minute = st0.nextToken().trim();
                String second = "0";
                if (hour.equals("24")) {
                    hour = "23";
                    minute = "59";
                }
                if (!st0.hasMoreTokens()) {
                    second = "0";
                } else {
                    second = st0.nextToken().trim();
                    if (second == null || second.length() == 0)
                        second = "0";
                }
                java.util.Date d = new java.util.Date(Integer.parseInt(hour) * 60 * 60 * 1000
                    + Integer.parseInt(minute) * 60 * 1000 + Integer.parseInt(second) * 1000);
                //if (!useJVM)
                //    d = new java.util.Date(d.getTime() + 16 * 60 * 60 * 1000);
                t = new java.sql.Time(d.getTime());
                return t;
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                CreamToolkit.logMessage("DacBase | " + e.toString());
            } catch (NumberFormatException e2) {
                e2.printStackTrace();
                CreamToolkit.logMessage("DacBase | " + e2.toString());
            }
            return t;

        } else {
            try {
                if (str.length() >= 4) {
                    String hour = str.substring(0, 2);
                    String minute = str.substring(2, 4);
                    String second = str.substring(4, 5);
                    if (hour.equals("24")) {
                        hour = "23";
                        minute = "59";
                    }
                    if (second == null || second.length() == 0)
                        second = "00";
                    java.util.Date d = new java.util.Date(Integer.parseInt(hour) * 60 * 60 * 1000
                        + Integer.parseInt(minute) * 60 * 1000 + Integer.parseInt(second) * 1000);
                    //if (!useJVM)
                    //    d = new java.util.Date(d.getTime() + 16 * 60 * 60 * 1000);
                    t = new java.sql.Time(d.getTime());
                    return t;
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            return t;
        }
    }

    private static String nextToken(StringTokenizer st) {
        String token = st.nextToken();
        if (token.equals("\t")) {
            return "";
        } else {
            try {
                st.nextToken(); // get the delimiter
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                // the last token don't have delimiter at end
            }
            return token;
        }
    }

    /**
     * Construct DacBase object from the record data by the format:
     * "[class_name]\t[first_field]\t[second_field]\t..."
     * 
     * @param data
     *            record data
     * @param fieldNames
     *            a two-dimensional array contains field names and field types.
     * @return DacBase object
     */
    public static DacBase constructFromTabSeperatedData(String data, Object[][] fieldNames) {
        String line = data;
        DacBase dacObject;
        StringTokenizer tokenizer = new StringTokenizer(line, "\t", true);

        // first token is class name
        if (!tokenizer.hasMoreTokens())
            return null;
        try {
            dacObject = (DacBase)Class.forName(tokenizer.nextToken()).newInstance();
            tokenizer.nextToken(); // skip the next delimiter
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Map fieldMap = dacObject.getFieldMap();
        fieldMap.clear();
        int i = 0;
        while (tokenizer.hasMoreTokens()) {
            String value = nextToken(tokenizer);
            Object valueObject = null;
            if (fieldNames[i][1].equals(String.class))
                valueObject = toStringData(value);
            else if (fieldNames[i][1].equals(Integer.class))
                valueObject = toInteger(value);
            else if (fieldNames[i][1].equals(HYIDouble.class))
                valueObject = toHYIDouble(value);
            else if (fieldNames[i][1].equals(java.sql.Date.class))
                valueObject = toDate(value);
            else if (fieldNames[i][1].equals(java.sql.Time.class))
                valueObject = toTime(value);
            else if (fieldNames[i][1].equals(Boolean.class))
                valueObject = toBoolean(value);

            fieldMap.put((String)fieldNames[i++][0], valueObject);
        }
        return dacObject;
    }

    public void clearFieldMap() {
        fieldMap = null;
    }

    public void setFieldMap(UppercaseKeyMap fieldMap)
    {
        this.fieldMap = fieldMap;
    }

    public static void reloadCache() {
        Cashier.createCache();
        Payment.createCache();
        CombPromotionHeader.createCache();
        CreditType.createCache();
        Dep.createCache();
        MixAndMatch.createCache();
        Payment.createCache();
        Reason.createCache();
        Store.createCache();
        TaxType.createCache();
    }
}
