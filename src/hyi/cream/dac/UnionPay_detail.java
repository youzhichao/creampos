package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * 银联交易明细unionpay_detail
 * @author Administrator
 *
 */
public class UnionPay_detail extends DacBase implements Serializable {

	static final String tableName = "unionpay_detail";
	private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("POSNUMBER");
		primaryKeys.add("STOREID");
        primaryKeys.add("TRANSACTIONNUMBER");
	}

	public UnionPay_detail() throws InstantiationException {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}


	public String getInsertUpdateTableName() {
		if (Server.serverExist()) {
                return "posul_unionpay_detail";
        }
		return tableName;
	}

	public static String getInsertUpdateTableNameStaic() {
        if (Server.serverExist()) {
            return "posul_unionpay_detail";
        }
        return "unionpay_detail";
    }

	public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
            {"STOREID", "STOREID"},
            {"POSNUMBER", "POSNUMBER"},
            {"TRANSACTIONNUMBER", "TRANSACTIONNUMBER"},
            {"ZSEQ","ZSEQ"},
            {"TOTAL_FEE", "TOTAL_FEE"},
            {"merId","merId"},
            {"SYSTEMDATE","SYSTEMDATE"},
            {"SEQ","SEQ"},
            {"orderId","orderId"},
            {"queryId","queryId"},
            {"txnTime","txnTime"},
		};
	}

	//STOREID,店号
	public String getSTORENUMBER() {
		return (String) getFieldValue("STOREID");
	}
	public void setSTORENUMBER(String storeNumber) {
		 setFieldValue("STOREID",storeNumber);
	}

	//POSNUMBER, 机号
	public Integer getPOSNUMBER() {
		return (Integer) getFieldValue("POSNUMBER");
	}
	public void setPOSNUMBER(Integer posNumber) {
		setFieldValue("POSNUMBER",posNumber);
	}

	//TRANSACTIONNUMBER, 交易序号
	public Integer getTRANSACTIONNUMBER() {
		return (Integer) getFieldValue("TRANSACTIONNUMBER");
	}
	public void setTRANSACTIONNUMBER(Integer employeeId) {
		setFieldValue("TRANSACTIONNUMBER",employeeId);
	}

    //ZSEQ,z账序号
    public Integer getZSEQ() {
        return (Integer)getFieldValue("ZSEQ");
    }

    public void setZSEQ(Integer s){
        setFieldValue("ZSEQ",s);
    }

	//Tatal_fee,交易金额
	public HYIDouble getTOTAL_FEE() {
		return (HYIDouble) getFieldValue("TOTAL_FEE");
	}
	public void setTOTAL_FEE(HYIDouble temperature) {
		setFieldValue("TOTAL_FEE",temperature);
	}

	//merId,商户代码
	public String getMerId() {
		return (String) getFieldValue("merId");
	}
	public void setMerId(String merId) {
		setFieldValue("merId",merId);
	}

    //SYSTEMDATE,时间
    public void setSYSTEMDATE(Date date) {
        setFieldValue("SYSTEMDATE",date);
    }
    public Date getSYSTEMDATE() {
        return (Date)getFieldValue("SYSTEMDATE");
    }

    //流水号
    public String getSEQ(){
        return (String)getFieldValue("SEQ");
    }
    public void setSEQ(String s){
        setFieldValue("SEQ",s);
    }

    //orderId,商户订单号
    public String getOrderId() {
        return (String)getFieldValue("orderId");
    }

    public void setOrderId(String s){
        setFieldValue("orderId",s);
    }

    //queryId,交易查询流水号
    public String getQueryId() {
        return (String)getFieldValue("queryId");
    }

    public void setQueryId(String s){
        setFieldValue("queryId",s);
    }

    //txnTime,交易时间
    public String getTxnTime() {
        return (String)getFieldValue("txnTime");
    }

    public void setTxnTime(String s){
        setFieldValue("txnTime",s);
    }

    public static Iterator queryByTranNo(DbConnection connection, int tranNo) {
        try {
            return getMultipleObjects(connection, UnionPay_detail.class,"select * from " + getInsertUpdateTableNameStaic() + " where  transactionNumber = " + tranNo);
        } catch (EntityNotFoundException e) {
//            CreamToolkit.logMessage(e);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        }
        return null;
    }

    public static UnionPay_detail queryByStoreIdTranNoPosNo(DbConnection connection, String storeId, String tranNo, String posId) {
        try {
            return getSingleObject(connection, UnionPay_detail.class,"select * from " + getInsertUpdateTableNameStaic()
                    + " where storeID = '" + storeId + "' and transactionNumber = " + tranNo + " and posNumber = " + posId);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } catch (EntityNotFoundException e) {
//            CreamToolkit.logMessage(e);
        }
        return null;
    }

    public UnionPay_detail cloneForSC() {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            UnionPay_detail clonedObj = new UnionPay_detail();
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = this.getFieldValue(fieldNameMap[i][0]);
                if (value == null) {
                    try {
                        value = this.getClass().getDeclaredMethod(
                                "get" + fieldNameMap[i][0], new Class[0]).invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedObj.setFieldValue(fieldNameMap[i][1], value);
            }
            return clonedObj;
        } catch (InstantiationException e) {
            return null;
        }
    }

    public static boolean deleteUnpinPayDetailByPrimaryKey(int posNumber,int transactionNumber) {
        DbConnection con  = null ;
        Statement stmt = null;
        boolean isOK = false;
        String sql = "delete from " + getInsertUpdateTableNameStaic() + " where POSNUMBER ="
                + posNumber + " and TRANSACTIONNUMBER =" + transactionNumber;
        try {
            con = CreamToolkit.getPooledConnection();
            stmt = con.createStatement();
            isOK = stmt.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }

            CreamToolkit.releaseConnection(con);
        }
        return isOK;
    }

    static public void deleteOutdatedData() {
        DbConnection connection = null;
        Statement statement = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            if (Server.serverExist()) {
                // 需要保留的天数
                int tranReserved = PARAM.getTransactionReserved();
                long l = new Date().getTime() - tranReserved * 1000L * 3600 * 24;
                String baseTime = CreamCache.getInstance().getDateTimeFormate().format(new java.sql.Date(l));
                statement.executeUpdate("DELETE FROM posul_unionpay_detail WHERE systemDate < '" + baseTime + "'");
            } else {
                Iterator<String> itr = Transaction.getOutdateTranNumbers();
                while (itr != null && itr.hasNext())
                    statement.executeUpdate("DELETE FROM unionpay_detail WHERE transactionNumber=" + itr.next());
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
    }
}
