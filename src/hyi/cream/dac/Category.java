package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Category definition class.
 *
 * @author Slackware, Bruce
 * @version 1.5
 */
public class Category extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public static Collection getAllObjectsForPOS()
     */
    transient public static final String VERSION = "1.5";

    static final String tableName = "cat";
    private static ArrayList primaryKeys = new ArrayList();

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    static {
        primaryKeys.add("CATNO");
        primaryKeys.add("MIDCATNO");
        primaryKeys.add("MICROCATNO");
    }

    public Category() {
    }

    public static Category queryByCategoryNumber(DbConnection connection, String catNo) {
        try {
            return getSingleObject(connection, Category.class,
                "SELECT * FROM " + tableName + " WHERE CATNO='" + catNo + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static Category queryByCategoryNumber(String catNo, String MidCategoryNumber) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return getSingleObject(connection, Category.class, "SELECT * FROM " + tableName
                + " WHERE CATNO='" + catNo + "'" + " and " + " MIDCATNO='" + MidCategoryNumber
                + "'" + " and " + " MICROCATNO='" + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static Category queryByCategoryNumber(String catNo, String MidCategoryNumber,
        String MicroCategoryNumber) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return (Category)getSingleObject(connection, Category.class, "SELECT * FROM "
                + tableName + " WHERE CATNO='" + catNo + "'" + " and " + " MIDCATNO='"
                + MidCategoryNumber + "'" + " and " + " MICROCATNO='" + MicroCategoryNumber + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    // Get properties value:
    public String getCategoryNumber() {
        return (String)getFieldValue("CATNO");
    }

    public String getMidCategoryNumber () {
        return (String)getFieldValue("MIDCATNO");//	VARCHAR(4)	N
    }

    public String getMicroCategoryNumber() {
        return (String)getFieldValue("MICROCATNO");// VARCHAR(4) N
    }

    public String getThinCategoryNumber() {
        return (String)getFieldValue("THINCATNO");// VARCHAR(4) N
    }

    public String getScreenName() {
        return (String)getFieldValue("CATNAME");
    }

    public String getPrintName() {
        return (String)getFieldValue("CATPNAME");//	VARCHAR(16)	N
    }

    public String getTaxType() {
        return (String)getFieldValue("DEPTAX");	//CHAR(1) N
    }

    public HYIDouble getOverrideAmountLimit() {
        return (HYIDouble)getFieldValue("OVERRIDEAMOUNTLIMIT");	//CHAR(1) N
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("categoryNumber", "CATNO");
        fieldNameMap.put("midCategoryNumber", "MIDCATNO");
        fieldNameMap.put("microCategoryNumber", "MICROCATNO");
        fieldNameMap.put("thinCategoryNumber", "THINCATNO");
        fieldNameMap.put("categoryName", "CATNAME");
        fieldNameMap.put("taxID", "DEPTAX");        
        fieldNameMap.put("overrideAmountLimit", "OVERRIDEAMOUNTLIMIT");        
        return fieldNameMap;
	}
    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.Category.class,
                "SELECT * FROM posdl_cat", getScToPosFieldNameMap());
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}
