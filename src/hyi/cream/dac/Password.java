package hyi.cream.dac;  

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Password extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private static ArrayList primaryKeys = new ArrayList();
	
	public List getPrimaryKeyList() {
		return  primaryKeys;
	}
	
	static final String tableName = "password";
	
	static {
		primaryKeys.add("LEVEL");
	}
		
	public static Password queryByLevel(DbConnection connection, int level) {
        try {
            return getSingleObject(connection, Password.class, "SELECT * FROM " + tableName
                + " WHERE LEVEL=" + level);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

	public String getInsertUpdateTableName() {
		return tableName;
	}

    /**
     * Constructor
     */
    public Password() {
    }

    /**
     * table's properties
     */
    public Integer getLevel() {
        return (Integer)getFieldValue("LEVEL");
    }

    public void setLevel(Integer level) {
        setFieldValue("LEVEL", level);
    }

    public String getPassword0() {
        return (String)getFieldValue("PASSWORD0");
    }

    public void setPassword0(String password0) {
        setFieldValue("PASSWORD0", password0);
    }

    public String getPassword1() {
        return (String)getFieldValue("PASSWORD1");
    }

    public void setPassword1(String password1) {
        setFieldValue("PASSWORD1", password1);
    }

    public String getPassword2() {
        return (String)getFieldValue("PASSWORD2");
    }

    public void setPassword2(String password2) {
        setFieldValue("PASSWORD2", password2);
    }

    public String getPassword3() {
        return (String)getFieldValue("PASSWORD3");
    }

    public void setPassword3(String password3) {
        setFieldValue("PASSWORD3", password3);
    }

    public String getPassword4() {
        return (String)getFieldValue("PASSWORD4");
    }

    public void setPassword4(String password4) {
        setFieldValue("PASSWORD4", password4);
    }

    public String getPassword5() {
        return (String)getFieldValue("PASSWORD5");
    }

    public void setPassword5(String password5) {
        setFieldValue("PASSWORD5", password5);
    }

    public String getPassword6() {
        return (String)getFieldValue("PASSWORD6");
    }

    public void setPassword6(String password6) {
        setFieldValue("PASSWORD6", password6);
    }

    public String getPassword7() {
        return (String)getFieldValue("PASSWORD7");
    }

    public void setPassword7(String password7) {
        setFieldValue("PASSWORD7", password7);
    }

    public String getPassword8() {
        return (String)getFieldValue("PASSWORD8");
    }

    public void setPassword8(String password8) {
        setFieldValue("PASSWORD8", password8);
    }

    public String getPassword9() {
        return (String)getFieldValue("PASSWORD9");
    }

    public void setPassword9(String password9) {
        setFieldValue("PASSWORD9", password9);
    }
}

 
