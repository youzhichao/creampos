package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.spos.BaseControl;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import hyi.cream.inline.Server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Payment type class. This is a read only cache object.
 * 
 * @author Slackware, Bruce
 * @version 1.5
 */
public class Payment extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * /Bruce/1.5/2002-03-10/ Add/Modify some methods for preparing to use in
     * new inline: Add: public static Collection getAllObjectsForPOS()
     */
    public transient static final String VERSION = "1.5";

    private static String tableName;
    transient static private Set<Payment> cache;

    private static ArrayList primaryKeys = new ArrayList();
    private static final Collection existedFieldList = getExistedFieldList(getInsertUpdateTableNameStaticVersion());
    public static boolean hasPaymentCategory = false;

    private transient BaseControl posDevice;

    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    static {
        primaryKeys.add("PAYID");
        createCache();
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Payment))
            return false;
        return getPaymentID().equals(((Payment) obj).getPaymentID());
    }

    public Payment() {
    }

    public static void createCache() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            tableName = Server.isAtServerSide() ? "posdl_payment" : "payment";
            Iterator<Payment> itr = getMultipleObjects(connection, Payment.class, "SELECT * FROM " + tableName);
            cache = new HashSet<Payment>();
            while (itr.hasNext()) {
                cache.add(itr.next());
            }
        } catch (EntityNotFoundException e) {
            cache = null;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            cache = null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    // query from cache
    public static Payment queryByPaymentID(String paymentID) {
        if (paymentID == null)
            return null;
        if (cache == null)
            return null;
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            Payment p = (Payment)itr.next();
            if (p.getPaymentID().equals(paymentID)) {
                return p;
            }
        }
        return null;
    }

    // query from cache
    public static Payment queryByPaymentName(String name) {
        if (name == null)
            return null;
        if (cache == null)
            return null;
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            Payment p = (Payment)itr.next();
            if (name.equals(p.getScreenName()) ||
                name.equals(p.getPrintName())) {
                return p;
            }
        }
        return null;
    }

    public static String queryPaymentIdByName(String name) {
        Payment payment = hyi.cream.dac.Payment.queryByPaymentName(name);
        return (payment != null) ? payment.getPaymentID() : null;
    }

    public static Iterator getAllPayment() {
        /*
         * if (cache == null) { return null; } return cache.iterator();
         */

        if (cache == null)
            return null;
        Iterator itr = cache.iterator();
        ArrayList al = new ArrayList();
        while (itr.hasNext()) {
            Payment r = (Payment) itr.next();
            if (al.size() == 0) {
                al.add(r);
            } else {
                boolean state = false;
                Payment temp = null;
                for (int i = 0; i < al.size(); i++) {
                    temp = (Payment) al.get(i);
                    if (Integer.parseInt(r.getPaymentID()) < Integer
                            .parseInt(temp.getPaymentID())) {
                        al.set(i, r);
                        Payment temp2 = null;
                        for (int j = i + 1; j < al.size(); j++) {
                            temp2 = (Payment) al.get(j);
                            al.set(j, temp);
                            temp = temp2;
                        }
                        al.add(temp);
                        state = true;
                        break;
                    }
                }
                if (!state) {
                    al.add(r);
                }
            }
        }
        if (al.size() == 0) {
            return null;
        } else {
            return al.iterator();
        }
    }

    // Get properties value:
    public String getPaymentID() {
        return (String) getFieldValue("PAYID");// PaymentID PAYID CHAR(2) N
    }

    public String getPaymentCategory() {
        return (String) getFieldValue("PAYCATEGORY");
    }

    public String getPrintName() {
        return (String) getFieldValue("PAYNAME");// PrintName PAYNAME VARCHAR(10) N
    }

    public String getScreenName() {
        return (String) getFieldValue("PAYCNAME");// ScreenName PAYCNAME VARCHAR(14) N
    }

    /**
     * CHAR[0]:外币
     * CHAR[1]:已开过发票（如提货卷）
     * CHAR[2]:有溢收是否可以结帐
     * CHAR[3]:认证
     * CHAR[4]:找零（不找零表示该支付可能产生溢收）
     * CHAR[5]:是否需要记录卡号
     * CHAR[6]:自动结清余额
     * CHAR[7]:刷卡
     */
    public String getPaymentType() {
        String p = (String) getFieldValue("PAYTYPE");
        p = p.trim();
        while (p.length() < 8) {
            p = "0" + p;
        }
        return p; // PaymentType PAYTYPE CHAR(8) N
    }

    public boolean isNonInvoice() {
        return getPaymentType().charAt(1) == '1';
    }

    public boolean isSpillable() {
        return getPaymentType().charAt(2) == '1';
    }

    public boolean isChangeable() {
        return getPaymentType().charAt(4) == '1';
    }

    public boolean isAutoTendering() {
        return getPaymentType().charAt(6) == '1';
    }

    public boolean isSlipping() {
        return getPaymentType().charAt(3) == '1'; 
    }

    public boolean isRecordingCardNumber() {
        return getPaymentType().charAt(5) == '1'; 
    }

//    /**
//     * 是否為信用卡支付.
//     */
//    public boolean isSwipingCard() {
//        return getPaymentType().charAt(7) == '1';
//    }

    /**
     * 是否為信用卡支付.
     */
    public boolean isCreditCard() {
        return getPaymentType().charAt(7) == '1';
    }

    /**
     * 是否為離線的信用卡支付.
     */
    public boolean isCreditCardWithoutCATConnected() {
        return getPaymentType().charAt(7) == '1' && getPosDevice() == null;
    }

    /**
     * 是否為連線的信用卡支付.
     */
    public boolean isCreditCardWithCATConnected() {
        return getPaymentType().charAt(7) == '1' && getPosDevice() != null;
    }

    public String getPaymentRate() {
        return (String) getFieldValue("PAYRATE");
    }

    public HYIDouble getDenomination() {
        return (HYIDouble) getFieldValue("DENOMINATION");
    }

    public String getSeqno() {
        return (String) getFieldValue("seqno");
    }

    public String getInsertUpdateTableName() {
        if (getUseClientSideTableName())
            return "payment";
        else if (hyi.cream.inline.Server.serverExist())
            return "posdl_payment";
        else
            return "payment";
    }

    // set properties value:
    public void setPaymentID(String aPaymentID) {
        setFieldValue("PAYID", aPaymentID);// PaymentID PAYID CHAR(2) N
    }

    public void setPaymentCategory(String aPaymentCategory) {
        setFieldValue("PAYCATEGORY", aPaymentCategory);
    }

    public void setPrintName(String aPrintName) {
        setFieldValue("PAYNAME", aPrintName);// PrintName PAYNAME VARCHAR(10) N
    }

    public void setScreenName(String aScreenName) {
        setFieldValue("PAYCNAME", aScreenName);// ScreenName PAYCNAME VARCHAR(14) N
    }

    public void setPaymentType(String aPayType) {
        setFieldValue("PAYTYPE", aPayType);// PaymentType PAYTYPE CHAR(8) N
    }

    public void setPaymentRate(String aPaymentRate) {
        setFieldValue("PAYRATE", aPaymentRate);// PaymentRate PAYRATE CHAR(2) Y
    }

    public void setDenomination(HYIDouble aDenomination) {
        setFieldValue("DENOMINATION", aDenomination);// DENOMINATION Decimal
    }

    public void setSeqno(String seqno) {
        setFieldValue("seqno", seqno);// priority Strng
    }


    /**
     * Meyer/2003-02-20 return fieldName map of PosToSc as Map
     */
    public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("payID", "PAYID");
        try {
            if ((new Payment()).isFieldExist("payCategory")) {
                fieldNameMap.put("payCategory", "PAYCATEGORY");
            }
        } catch (Exception e) {
        }
        fieldNameMap.put("payPrintName", "PAYNAME");
        fieldNameMap.put("payScreenName", "PAYCNAME");
        fieldNameMap.put("payType", "PAYTYPE");
        fieldNameMap.put("currencyID", "PAYRATE");
        fieldNameMap.put("denomination", "DENOMINATION");
        fieldNameMap.put("seqno", "seqno");

        return fieldNameMap;
    }

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.Payment.class,
                    "SELECT * FROM posdl_payment", getScToPosFieldNameMap());
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static void insertCashIfNotExist(DbConnection connection) throws SQLException {
        try {
            DacBase.getSingleObject(connection, hyi.cream.dac.Payment.class,
                "SELECT * FROM payment WHERE payID='00'");
        } catch (EntityNotFoundException e) {
            Payment payment = new Payment();
            payment.setPaymentID("00");
            payment.setPrintName(CreamToolkit.GetResource().getString("Cash"));
            payment.setScreenName(CreamToolkit.GetResource().getString("Cash"));
            payment.setPaymentType("00001010");
            payment.setPaymentRate("");
            payment.setDenomination(new HYIDouble(1));
            payment.setSeqno("0");
            payment.insert(connection);
        }
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posdl_payment";
        else
            return "payment";
    }

    public Collection getExistedFieldList() {
        return existedFieldList;
    }

    /**
     * 找“信用卡紅利抵用”支付。
     */
    public static Payment queryCreditCardBonusPayment() {
        for (Payment payment : cache)
            if (payment.isCreditCardBonusPayment())
                return payment;
        return null;
    }

    /**
     * 找“信用卡”支付。
     */
    public static Payment queryCreditCardPayment() {
        for (Payment payment : cache)
            if (payment.isCreditCard())
                return payment;
        return null;
    }

    /**
     * 是否為“信用卡紅利抵用”支付。
     */
    public boolean isCreditCardBonusPayment() {
        return "CB".equals(getPaymentCategory()); // CB means "Credit card Bonus"
    }

    /**
     * 找“應收尾款”、“應收帳”支付。
     */
    public static Payment queryDownPayment() {
        for (Payment payment : cache)
            if (payment.isDownPayment())
                return payment;
        return null;
    }

    /**
     * 找“應收尾款”、“應收帳”支付ID。
     */
    public static String queryDownPaymentId() {
        for (Payment payment : cache)
            if (payment.isDownPayment())
                return payment.getPaymentID();
        return "";
    }

    /**
     * 是否為“應收尾款”、“應收帳”支付。
     */
    public boolean isDownPayment() {
        return "DP".equals(getPaymentCategory()); // DP means "Down Payment"
    }

    /**
     * 關聯的POS device, 如CAT.
     */
    public BaseControl getPosDevice() {
        return posDevice;
    }

    public void setPosDevice(BaseControl posDevice) {
        this.posDevice = posDevice;
    }
}
