package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DBToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.groovydac.Param;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Dep definition class.
 *
 * @author Dai, Bruce
 * @version 1.5
 */
public class DepSales extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public static Object[] cloneForSC(Iterator iter)
     */
    transient public static final String VERSION = "1.5";

    static final String tableName = "depsales";
    private static ArrayList primaryKeys = new ArrayList();
    transient private static ArrayList depArray = new ArrayList();

    static {
        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("storeID");
            primaryKeys.add("accountDate");
            primaryKeys.add("posNumber");
            primaryKeys.add("zSequenceNumber");
            primaryKeys.add("depID");
        } else {
            primaryKeys.add("posNumber");
            primaryKeys.add("sequenceNumber");
            primaryKeys.add("depID");
        }
    }

    public DepSales() {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_depsales";
        else
            return "depsales";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_depsales";
        else
            return "depsales";
    }

//    public static DepSales queryByDepID(String s) {
//        if (depArray.size() == 0) {
////            SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
////            Iterator ite = getMultipleObjects(DepSales.class,
////                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
////                " WHERE accountDate='" +
////                df.format(CreamToolkit.getInitialDate()) + "'");
//            int curZNumber = ZReport.getCurrentZNumber();
//            Iterator ite = queryBySequenceNumber(new Integer(curZNumber));
//            if (ite != null) {
//                while (ite.hasNext()) {
//                    depArray.add((DepSales)ite.next());
//                }
//            } else {
//                createDepSales(curZNumber );
//            }
//        }
//        DepSales dps = null;
//        for (int i = 0; i < depArray.size(); i++) {
//            dps = (DepSales)depArray.get(i);
//            if (dps.getDepID().equalsIgnoreCase(s)) {
//                break;
//            }
//        }
//        return dps;
//    }

    public static DepSales queryByDepID(DbConnection connection, Integer zNumber, String depID)
        throws SQLException {
        DepSales depSales = null;
        if (depArray.size() == 0) {
            Iterator itr = queryBySequenceNumber(connection, zNumber);
            // DepSales 存在
            if (itr != null) {
                while (itr.hasNext()) {
                    DepSales dps = (DepSales)itr.next();
                    depArray.add(dps);
                }
                // 没有找到该depID 表明商品的DepID设置有错误
            } else {
                // DepSales 不存在 创建以后再取
                createDepSales(connection, zNumber.intValue());
            }
        }
        for (int i = 0; i < depArray.size(); i++) {
            DepSales dps = (DepSales)depArray.get(i);
            if (dps.getDepID().equalsIgnoreCase(depID)) {
                depSales = dps;
                break;
            }
        }
        return depSales;
    }

    public static Iterator createDepSales(DbConnection connection, int znumber) throws SQLException {
        depArray.clear();
        Iterator ite = Dep.getDepIDs();
        String depID = "";
        DepSales dps = null;
        if (ite != null) {
            while (ite.hasNext()) {
                depID = ((Dep)ite.next()).getDepID();

                dps = new DepSales();
                dps.setStoreNumber(Store.getStoreID());
                dps.setAccountDate(CreamToolkit.getInitialDate());
                dps.setDepID(depID);
                dps.setPOSNumber(PARAM.getTerminalNumber());
                dps.setSequenceNumber(znumber);
                dps.setUploadState("0");
                dps.setGrossSaleTotalAmount(new HYIDouble(0));
                dps.setDiscountTotalAmount(new HYIDouble(0));
                dps.setSiPlusTotalAmount(new HYIDouble(0));
                dps.setNotIncludedTotalSale(new HYIDouble(0));
                dps.setMixAndMatchTotalAmount(new HYIDouble(0));
                dps.setNetSaleTotalAmount(new HYIDouble(0));
                dps.setTaxAmount(new HYIDouble(0));
                dps.insert(connection);
                depArray.add(dps);
            }
        }
        return depArray.iterator();
    }

    public static void updateAll(DbConnection connection, Date d) throws SQLException {
        DepSales dps = null;
        for (int i = 0; i < depArray.size(); i++) {
            dps = (DepSales)depArray.get(i);
            dps.setAccountDate(d);
            try {
                dps.update(connection);
            } catch (EntityNotFoundException e) {
                throw new SQLException(e.toString());
            }
        }
    }

    public void update(DbConnection connection) throws SQLException, EntityNotFoundException {
        setAllFieldsDirty(true);
        super.update(connection);
    }

    public static Iterator queryBySequenceNumber(DbConnection connection, Integer number) {
        try {
            return getMultipleObjects(connection, DepSales.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
                " WHERE sequenceNumber = " + number + " ORDER BY depID");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    /**
     * Only used at the sc side, do nothing for pos side 
     */
    public static void deleteBySequenceNumber(DbConnection connection, int zNumber, int posNumber)
        throws SQLException {
        if (!hyi.cream.inline.Server.serverExist())
            return;
        String deleteSql = "DELETE FROM " + getInsertUpdateTableNameStaticVersion()
            + " WHERE zSequenceNumber =" + zNumber + " AND posNumber = " + posNumber;
        DBToolkit.executeUpdate(connection, deleteSql);
    }
    
    // properties

    // storeNumber,storeNumber,CHAR(6),N,店号
    public void setStoreNumber(String s) {
        this.setFieldValue("storeNumber", s);
    }

    public String getStoreNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)this.getFieldValue("storeID");
        else
            return (String)this.getFieldValue("storeNumber");
    }

    //accountDate,accountDate,DATE,N,日结系统日期
    public void setAccountDate(Date d) {
        this.setFieldValue("accountDate", d);
    }

    public Date getAccountDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (Date)this.getFieldValue("accountDate");
        else
            return (Date)this.getFieldValue("accountDate");
    }

    //sequenceNumber,sequenceNumber,INT UNSIGNED,N,Z帐序号
    public void setSequenceNumber(int i) {
        this.setFieldValue("sequenceNumber", new Integer(i));
    }

    public int getSequenceNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return ((Integer)this.getFieldValue("zSequenceNumber")).intValue();
        else
            return ((Integer)this.getFieldValue("sequenceNumber")).intValue();
    }

    //posNumber,posNumber,TINYINT UNSIGNED,N,收银机机号
    public void setPOSNumber(int i) {
        this.setFieldValue("posNumber", new Integer(i));
    }

    public int getPOSNumber() {
        return ((Integer)this.getFieldValue("posNumber")).intValue();
    }

    //uploadState,uploadState,ENUM("0","1"，"2"),N,上传FLG
    public void setUploadState(String s) {
        this.setFieldValue("uploadState", s);
    }

    public String getUploadState() {
        return (String)this.getFieldValue("uploadState");
    }

    //depID,depID,CHAR(2),N,帐务编号
    public void setDepID(String s) {
        this.setFieldValue("depID", s);
    }

    public String getDepID() {
        return (String)this.getFieldValue("depID");
    }

    //grossSaleTotalAmount,grossSaleTotalAmount,DECIMAL(12,2),营业毛额合计
    public void setGrossSaleTotalAmount(HYIDouble b) {
        this.setFieldValue("grossSaleTotalAmount", b);
    }

    public HYIDouble getGrossSaleTotalAmount() {
        return (HYIDouble)this.getFieldValue("grossSaleTotalAmount");
    }

    /**
     * 变更: 注意：siPlusTotalAmount 现在挪用为毛额的累计税金
     */
    //siPlusTotalAmount,siPlusTotalAmount,DECIMAL(12,2),SI总加成金额合计
    public void setSiPlusTotalAmount(HYIDouble b) {
        this.setFieldValue("siPlusTotalAmount", b);
    }

    /**
     * 变更: 注意：siPlusTotalAmount 现在挪用为毛额的累计税金
     */
    public HYIDouble getSiPlusTotalAmount() {
        return (HYIDouble)this.getFieldValue("siPlusTotalAmount");
    }

    //discountTotalAmount,discountTotalAmount,DECIMAL(12,2),SI总折扣金额合计
    public void setDiscountTotalAmount(HYIDouble b) {
        this.setFieldValue("discountTotalAmount", b);
    }

    public HYIDouble getDiscountTotalAmount() {
        return (HYIDouble)this.getFieldValue("discountTotalAmount");
    }

    //mixAndMatchTotalAmount,mixAndMatchTotalAmount,DECIMAL(12,2),总M&M折让金额合计
    public void setMixAndMatchTotalAmount(HYIDouble b) {
        this.setFieldValue("mixAndMatchTotalAmount", b);
    }

    public HYIDouble getMixAndMatchTotalAmount() {
        return (HYIDouble)this.getFieldValue("mixAndMatchTotalAmount");
    }

    //notIncludedTotalSale,notIncludedTotalSale,DECIMAL(12,2),已开发票支付金额合计
    public void setNotIncludedTotalSale(HYIDouble b) {
        this.setFieldValue("notIncludedTotalSale", b);
    }

    public HYIDouble getNotIncludedTotalSale() {
        return (HYIDouble)this.getFieldValue("notIncludedTotalSale");
    }

    //netSaleTotalAmount,netSaleTotalAmount,DECIMAL(12,2),发票金额合计
    public void setNetSaleTotalAmount(HYIDouble b) {
        this.setFieldValue("netSaleTotalAmount", b);
    }

    public HYIDouble getNetSaleTotalAmount() {
        return (HYIDouble)this.getFieldValue("netSaleTotalAmount");
    }

    // 税額
    public void setTaxAmount(HYIDouble b) {
        this.setFieldValue("taxAmount", b);
    }

    public HYIDouble getTaxAmount() {
        return (HYIDouble)this.getFieldValue("taxAmount");
    }
    
    public static Object[] getCurrentDepSales(DbConnection connection) throws SQLException {
        if (depArray.size() == 0) {
            int curZNumber = ZReport.getCurrentZNumber(connection);
            Iterator ite = queryBySequenceNumber(connection, new Integer(curZNumber));

            if (ite != null) {
                while (ite.hasNext()) {
                    depArray.add((DepSales)ite.next());
                }
            } else {
                createDepSales(connection, curZNumber);
            }
        }
        return depArray.toArray();
    }

    //Bruce/2002-1-28
    public static Iterator getUploadFailedList(DbConnection connection) {
        try {
            String failFlag = "2";
            return getMultipleObjects(connection, DepSales.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
                " WHERE uploadState='" + failFlag + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as String[][] 
	 */
	public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
            {"storeNumber", "storeID"},
            {"accountDate", "accountDate"},
            {"sequenceNumber", "zSequenceNumber"},
            {"posNumber", "posNumber"},
            {"uploadState", "uploadState"},
            {"depID", "depID"},
            {"grossSaleTotalAmount", "grossSaleTotalAmount"},
            {"siPlusTotalAmount", "siPlusTotalAmount"},
            {"discountTotalAmount", "discountTotalAmount"},
            {"mixAndMatchTotalAmount", "mixAndMatchTotalAmount"},
            {"notIncludedTotalSale", "notIncludedTotalSale"},
            {"netSaleTotalAmount", "netSaleTotalAmount"},
            {"taxAmount", "taxAmount"},
		};
	}
	
    /**
     * Clone DepSales objects for SC with converted field names.
     *
     * This method is only used at POS side.
     */
    public static Object[] cloneForSC(Iterator iter) {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        ArrayList objArray = new ArrayList();
        while (iter.hasNext()) {
            DepSales ds = (DepSales)iter.next();
            //System.out.println("depid=" + ds.getDepID());
            DepSales clonedDS = new DepSales();
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = ds.getFieldValue(fieldNameMap[i][0]);
                if (value == null) {
                    try {
                        value = ds.getClass().getDeclaredMethod(
                            "get" + fieldNameMap[i][0], new Class[0]).invoke(ds, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedDS.setFieldValue(fieldNameMap[i][1], value);
            }
            objArray.add(clonedDS);
        }
        return objArray.toArray();
    }
}
