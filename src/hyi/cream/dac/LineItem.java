package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.state.StateToolkit;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.awt.Color;
import java.io.Serializable;
import java.math.BigDecimal;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;

/**
 * Transaction detail DAC class.
 * 
 * @author Dai, Slackware, Bruce, Meyer
 * @version 1.6
 */
public class LineItem extends DacBase implements Serializable, Cloneable {
    private static final long serialVersionUID = -2159417719382752436L;

    /**
     * /Bruce/1.5/2002-03-10/ Add/Modify some methods for preparing to use in
     * new inline: Add: public LineItem cloneForSC()
     *
     * Meyer/1.6/2003-01-16/ Add some methods for calculating apportion and
     * present M&M
     *
     */
    transient public static final String VERSION = "1.6";

    private static final String OVERRIDE_AMOUNT_AUTH_CASHIER_NO_PREFIX = "auth:";
    private static final String OVERRIDE_AMOUNT_AUTH_CASHIER_NO_SUBFIX = ":";

    private static ArrayList primaryKeys = new ArrayList();

    private String description = "";

    transient private boolean printed = false;

    transient private HYIDouble afterSIAmount = null;

    transient private HYIDouble siAmount = null;

    private Hashtable SIAmtList = new Hashtable();

    transient private Color lineItemColor = new Color(255, 255, 255);

    private String siNo = "";

    private java.util.Date systemDate = CreamToolkit.getInitialDate();

    transient private HYIDouble tada = new HYIDouble(0);

    transient private String tdt = "";

    transient private String tdn = "";

    private int daishouNumber = 0;

    private int daishouDetailNumber = 0;

    private String daishouDetailName = "";

    private String daiShouBarcode = "";

    private String daiShouID = "";

    private String descriptionAndSpecification;

    private String specification;

    private String printName;

    // transient private boolean commit;

    private String season;

    private String style;

    private String color;

    private String size;

    private String itemBrand;

    private String sizeCategory;

    private String invCycleNo;

    private String smallUnit;

    private boolean isScaleItem;
    private boolean openPrice;
    private HYIDouble unitPrice2;
    private HYIDouble amount2;
    private String printSecondLine;

    // 灿坤 不用保存到数据库
    private boolean isNeedPhoneCardNumber = false; // 销售电话卡时，需要收银员输入卡号

    // Meyer/2003-02-10/ 在组合促销中使用
    private int noMatchedQty = Integer.MIN_VALUE; // 此明细中，未被组合促销分摊，赠送过的商品数量

    // private static final String daiShouVersion =
    // CreamProperties.getInstance().getProperty("DaiShouVersion") == null?
    // "" : CreamProperties.getInstance().getProperty("DaiShouVersion");

    // Meyer/2003-03-11/
    private static final Collection existedFieldList = getExistedFieldList(getInsertUpdateTableNameStaticVersion());

    private boolean onlyDiscount = false;

    private static int rebateAmountScale = 1;

    // 退货时用的变量，指向改变数量的原lineitem的lineItemSequence
    private int voidSequence = -1;

    //Bruce/20080606
    private String depID;

    private String shopShare;

    //Bruce/20121015
    transient private boolean voidPeriodicalItem;

    public String getShopShare() {
        return shopShare;
    }

    public void setShopShare(String shopShare) {
        this.shopShare = shopShare;
    }


    static {
        try {
            rebateAmountScale = PARAM.getRebateAmountScale();
        } catch (NumberFormatException e) {
        }
        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("storeID");
            primaryKeys.add("posNumber");
            primaryKeys.add("transactionNumber");
            primaryKeys.add("lineItemSequence");
            primaryKeys.add("systemDate");
        } else {
            primaryKeys.add("TMTRANSEQ");
            primaryKeys.add("TMCODE");
            primaryKeys.add("ITEMSEQ");
        }
    }

    public Collection getExistedFieldList() {
        return existedFieldList;
    }

    public LineItem() {
        this.setTaxAmount(new HYIDouble(0));
    }

    public LineItem(int i) throws InstantiationException {
        // constructor for doing nothing
    }

    public String toString() {
        return "LineItem (pos=" + this.getTerminalNumber() + ",no="
                + this.getTransactionNumber() + ",seq="
                + this.getLineItemSequence() + ",unitprice="
                + this.getUnitPrice() + ",qty=" + this.getQuantity() + ",amt="
                + this.getAmount() + ",aftamt=" + this.getAfterDiscountAmount()
                +",itemno=" + this.getItemNumber()
                + ")";
    }

    public Object clone() {
        return super.clone();
    }

    public java.util.List getPrimaryKeyList() {
        return primaryKeys;
    }

//    public static Iterator queryByTransactionNumber(DbConnection connection, String transactionNumber) {
//        try {
//            Iterator itr = getMultipleObjects(connection, LineItem.class, "SELECT * FROM "
//                + getInsertUpdateTableNameStaticVersion() + " WHERE TMTRANSEQ='"
//                + transactionNumber + "' ORDER BY ITEMSEQ");
//
//            ArrayList newArrayList = new ArrayList();
//            if (itr == null)
//                return newArrayList.iterator();
//            while (itr.hasNext()) {
//                LineItem lineItem = (LineItem) itr.next();
//                PLU plu = PLU.queryBarCode(lineItem.getPluNumber());
//                if (plu == null) {
//                    if (lineItem.getPluNumber() != null) {
//                        if (lineItem.getPluNumber().length() > 0) {
//                            // '08'：代售项目 '09'：代付项目'10'：PaidIn项目'11'：PaidOut项目
//                            String cat = "";
//                            // 代售
//                            if (lineItem.getDetailCode().equals("I"))
//                                cat = "08";
//                            else if (lineItem.getDetailCode().equals("O")) {
//                                // 公共事业费代收
//                                DaiShouSales2 dss = DaiShouSales2.queryDaiShouSales(connection,
//                                    lineItem.getTransactionNumber().intValue(),
//                                    lineItem.getLineItemSequence());
//                                if (dss != null) {
//                                    DaiShouDef dsd = DaiShouDef.queryByID(connection, dss.getID());
//                                    lineItem.setDescriptionAndSpecification(dsd.getName());
//                                    lineItem.setDescription(dsd.getName());
//                                    lineItem.setDaiShouBarcode(dss.getBarcode());
//                                    lineItem.setDaiShouID(dss.getID());
//                                }
//                            } else if (lineItem.getDetailCode().equals("Q"))
//                                cat = "09";
//                            else if (lineItem.getDetailCode().equals("N"))
//                                cat = "10";
//                            else if (lineItem.getDetailCode().equals("T"))
//                                cat = "11";
//                            Reason r = Reason.queryByreasonNumberAndreasonCategory(
//                                    lineItem.getPluNumber(), cat);
//                            if (r != null)
//                                lineItem.setDescription(r.getreasonName());
//                        } else {
//                            Category cat = Category.queryByCategoryNumber(lineItem.getCategoryNumber(),
//                                    lineItem.getMidCategoryNumber(), lineItem.getMicroCategoryNumber());
//                            if (cat != null) {
//                                lineItem.setDescription(cat.getScreenName());
//                            } else {
//                                TaxType t = TaxType.queryByTaxID(lineItem.getTaxType());
//                                if (t != null) {
//                                    lineItem.setDescription(t.getTaxName());
//                                }
//                            }
//                        }
//                    }
//                } else {
//                    String name = GetProperty.getPluPrintField("");
//                    if (name == "")
//                        name = "ScreenName";
//                    if (name.equals("ScreenName"))
//                        lineItem.setDescription(plu.getScreenName());
//                    else
//                        lineItem.setDescription(plu.getPrintName());
//                    lineItem.setIsScaleItem(plu.isWeightedPlu());
//                    lineItem.setOpenPrice(plu.isOpenPrice());
//                }
//                newArrayList.add(lineItem);
//            }
//            return newArrayList.iterator();
//
//        } catch (SQLException e) {
//            CreamToolkit.logMessage(e);
//            return null;
//        } catch (EntityNotFoundException e) {
//            return null;
//        }
//    }

    public static int getOrderIdAndOrderNo(DbConnection connection,int orderid,String orderno) {
        Object maxSeqNumber = getValueOfStatement(connection, "SELECT count(*) FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE selfbuyOrderid="
                + orderid + " and selfbuyOrderno='"+orderno+"'");

        if (maxSeqNumber == null)
            return -1;
        return CreamToolkit.retrieveIntValue(maxSeqNumber);
    }

    public static Iterator queryByTransactionNumber(DbConnection connection, int posNumber,
            int transactionNumber) {
        try {
            Iterator itr = null;
            if (hyi.cream.inline.Server.serverExist())
                itr = getMultipleObjects(connection, LineItem.class, "SELECT * FROM "
                        + getInsertUpdateTableNameStaticVersion()
                        + " WHERE PosNumber=" + posNumber
                        + " AND TransactionNumber=" + transactionNumber);
            else {
                itr = getMultipleObjects(connection, LineItem.class, "SELECT * FROM "
                        + getInsertUpdateTableNameStaticVersion()
                        + " WHERE TMTRANSEQ='" + transactionNumber + "'");
    
                ArrayList newArrayList = new ArrayList();
                if (itr == null)
                    return newArrayList.iterator();
                while (itr.hasNext()) {
                    LineItem lineItem = (LineItem) itr.next();
                    PLU plu = PLU.queryBarCode(lineItem.getPluNumber());
                    if (plu == null) {
                        if (lineItem.getPluNumber() != null) {
                            if (lineItem.getPluNumber().length() > 0) {
                                // '08'：代售项目'09'：代付项目'10'：PaidIn项目'11'：PaidOut项目
                                String cat = "";
                                // 代售
                                if (lineItem.getDetailCode().equals("I"))
                                    cat = "08";
                                else if (lineItem.getDetailCode().equals("O")) {
                                    // 公共事业费代收
                                    DaiShouSales2 dss = DaiShouSales2.queryDaiShouSales(connection,
                                        lineItem.getTransactionNumber().intValue(),
                                        lineItem.getLineItemSequence());
                                    if (dss != null) {
                                        DaiShouDef dsd = DaiShouDef.queryByID(connection, dss.getID());
                                        lineItem.setDescriptionAndSpecification(dsd.getName());
                                        lineItem.setDescription(dsd.getName());
                                        lineItem.setDaiShouBarcode(dss.getBarcode());
                                        lineItem.setDaiShouID(dss.getID());
                                    }
                                } else if (lineItem.getDetailCode().equals("Q"))
                                    cat = "09";
                                else if (lineItem.getDetailCode().equals("N"))
                                    cat = "10";
                                else if (lineItem.getDetailCode().equals("T"))
                                    cat = "11";
                                Reason r = Reason.queryByreasonNumberAndreasonCategory(
                                        lineItem.getPluNumber(), cat);
                                if (r != null)
                                    lineItem.setDescription(r.getreasonName());
                            } else {
                                //Bruce/20100824/ First check to see if it is a department/category item
                                String catNo = lineItem.getCategoryNumber();
                                String midCatNo = lineItem.getMidCategoryNumber();
                                String microCatNo = lineItem.getMicroCategoryNumber();
                                String itemNo = lineItem.getItemNumber();
                                if (catNo != null && midCatNo != null && microCatNo != null && itemNo != null &&
                                    (catNo + midCatNo + microCatNo).equals(itemNo)) {
                                    Category category = Category.queryByCategoryNumber(catNo, midCatNo, microCatNo);
                                    if (category != null)
                                        lineItem.setDescription(category.getScreenName());
                                } else {
                                    TaxType t = TaxType.queryByTaxID(lineItem.getTaxType());
                                    if (t != null) {
                                        lineItem.setDescription(t.getTaxName());
                                    }
                                }
                            }
                        }
                    } else {
                        String name = PARAM.getPluPrintField();
                        if (name.equals("pluname"))
                            lineItem.setDescription(plu.getScreenName());
                        else
                            lineItem.setDescription(plu.getPrintName());
                    }
                    newArrayList.add(lineItem);
                }
                return newArrayList.iterator();
            }
            return itr;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_trandtl";
        else
            return "trandetail";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_trandtl";
        else
            return "trandetail";
    }

    public boolean getPrinted() {
        return printed;
    }

    public void setPrinted(boolean printed) {
        this.printed = printed;
    }

    public java.util.Date getSystemDateTime() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.util.Date) getFieldValue("systemDate");
        else
            return systemDate;
    }

    public void setSystemDateTime(java.util.Date sd) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("systemDate", sd);
        systemDate = sd;
    }

    public String getStoreID() {
        return Store.getStoreID();
    }

    public HYIDouble getAfterSIAmount() {
        return afterSIAmount;
    }

    public void setAfterSIAmount(HYIDouble amt) {
        afterSIAmount = amt;
    }

    public Hashtable getSIAmtList() {
        return SIAmtList;
    }

    public void setSIAmtList(String siID, HYIDouble amt) {
        if (amt == null || siID == null) {
            SIAmtList.clear();
            return;
        }
        SIAmtList.put(siID, amt);
    }

    /** 是否這個line item有做過此SI折扣. */
    public boolean isDiscountedBySI(SI si) {
        boolean b = false;
        String discNo = this.getDiscountNumber();
        if (discNo != null) {
            StringTokenizer token = new StringTokenizer(discNo, ",");
            while (token.hasMoreTokens()) {
                String strTmp = (String) token.nextToken();
                if (strTmp.startsWith("S")) {
                    String siID = strTmp.substring(1);
                    if (siID.equals(si.getSIID())) {
                        b = true;
                        break;
                    }
                }
            } // end while
        }
        return b;
    }

    public Iterator getSIIterator(DbConnection connection) {
        try {
            if (this.getDetailCode().equals("D"))
                return null;
            ArrayList allSI = new ArrayList();
//            PLU plu = PLU.queryBarCode(this.getPluNumber());
            Iterator itr = getMultipleObjects(connection, SI.class, "SELECT * FROM "
                + SI.getInsertUpdateTableNameStaticVersion());
            while (itr.hasNext()) {
                SI si = (SI)itr.next();
                if (si.contains(this))
                    allSI.add(si);
            }
            return allSI.iterator();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public void makeNegativeValue() {
        Iterator itr = this.getValues().keySet().iterator();
        while (itr.hasNext()) {
            Object key = itr.next();
            Object value = this.getValues().get(key);
            if (value != null) {
                if (value instanceof HYIDouble) {
                    HYIDouble big = (HYIDouble) value;
                    String field = (String) key;
                    // if (getDetailCode().equals("I") //代售
                    // || getDetailCode().equals("O") //公共事业费代收
                    // || getDetailCode().equals("Q")
                    // || getDetailCode().equals("N")
                    // || getDetailCode().equals("T")) {
                    // if (field.compareToIgnoreCase("UNITPRICE") == 0
                    // || field.compareToIgnoreCase("WEIGHT") == 0)
                    // continue;
                    // } else {
                    // if (field.compareToIgnoreCase("UNITPRICE") == 0
                    // || field.compareToIgnoreCase("WEIGHT") == 0
                    // || field.compareToIgnoreCase("ORIGPRICE") == 0)
                    // //Bruce/20030426
                    // continue;
                    // }
                    if (field.compareToIgnoreCase("UNITPRICE") == 0
                            || field.compareToIgnoreCase("ORIGPRICE") == 0)
                        continue;

                    big = big.negate();
                    setFieldValue(field, big);
                }
            }
        }
    }

    /**
     * table's properties
     */

    public Integer getTransactionNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("transactionNumber");
        else
            return (Integer) getFieldValue("TMTRANSEQ");
    }

    public void setTransactionNumber(Integer tmtranseq) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("transactionNumber", tmtranseq);
        else
            setFieldValue("TMTRANSEQ", tmtranseq);
    }

    public Integer getTerminalNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("posNumber");
        else
            return (Integer) getFieldValue("TMCODE");
    }

    public void setTerminalNumber(Integer tmcode) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("posNumber", tmcode);
        else
            setFieldValue("TMCODE", tmcode);
    }

    /*
     * public int getLineItemSequence() { if (getFieldValue("ITEMSEQ") == null)
     * return -1; if (getFieldValue("ITEMSEQ").getClass() == Integer.class)
     * return ((Integer)getFieldValue("ITEMSEQ")).intValue(); else if
     * (getFieldValue("ITEMSEQ").getClass() == Short.class) return
     * ((Short)getFieldValue("ITEMSEQ")).intValue(); return -1; }
     */
    public int getLineItemSequence() {
        if (hyi.cream.inline.Server.serverExist()) {
            if (getFieldValue("lineItemSequence") == null)
                return -1;
            return ((Integer) getFieldValue("lineItemSequence")).intValue();
        } else {
            if (getFieldValue("ITEMSEQ") == null)
                return -1;
            return ((Integer) getFieldValue("ITEMSEQ")).intValue();
        }
    }

    int displayNumber = 0;

    public int getDisplayedLineItemSequence() {
        return displayNumber;
    }

    public void setDisplayedLineItemSequence(int no) {
        displayNumber = no;
    }

    public void setLineItemSequence(int itemseq) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("lineItemSequence", new Integer(itemseq));
        else
            setFieldValue("ITEMSEQ", new Integer(itemseq));
    }

    /**
        "S":销售
        "R":退货
        "D":SI折扣
        ---- 被取消  "T":ItemDiscount折扣
        "M":Mix & Match折扣
        "B":退瓶
        "I":(对)代售, (错)代收
        "O":代收公共事业费
        "Q":代支
        "V":指定更正
        "E":立即更正
        "H":借零
        "G":投库
        "N":PaidIn
        "T":PaidOut
        "A":RoundDown
        "J":期刊预订
        "K":期刊领刊
        "L":期刊退刊
     * @return
     */
    public String getDetailCode() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("detailCode");
        else
            return (String) getFieldValue("CODETX");
    }

    public void setDetailCode(String codetx) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("detailCode", codetx);
        else
            setFieldValue("CODETX", codetx);
    }

    public String getDetailCode1() {
        return (String) getFieldValue("CODER");
    }

    public void setDetailCode1(String coder) {
        setFieldValue("CODER", coder);
    }

    public String getCategoryNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("categoryNumber");
        else
            return (String) getFieldValue("CATNO");
    }

    public void setCategoryNumber(String catno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("categoryNumber", catno);
        else
            setFieldValue("CATNO", catno);
    }

    public String getMidCategoryNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("midCategoryNumber");
        else
            return (String) getFieldValue("MIDCATNO");
    }

    public void setMidCategoryNumber(String midcatno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("midCategoryNumber", midcatno);
        else
            setFieldValue("MIDCATNO", midcatno);
    }

    public String getMicroCategoryNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("microCategoryNumber");
        else
            return (String) getFieldValue("MICROCATNO");
    }

    public void setMicroCategoryNumber(String microcatno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("microCategoryNumber", microcatno);
        else
            setFieldValue("MICROCATNO", microcatno);
    }

    public String getThinCategoryNumber() {
        String value;
        if (hyi.cream.inline.Server.serverExist())
            value = (String) getFieldValue("thinCategoryNumber");
        else
            value = (String) getFieldValue("THINCATNO");
        return (value == null) ? "" : value;
    }

    public void setThinCategoryNumber(String thincatno) {
        String fieldName = (hyi.cream.inline.Server.serverExist()) ? "thinCategoryNumber"
                : "THINCATNO";
        if (!isFieldExist(fieldName))
            return;
        setFieldValue(fieldName, thincatno);
    }

    public String getPluNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("pluNumber");
        else
            return (String) getFieldValue("PLUNO");
    }

    public void setPluNumber(String pluno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pluNumber", pluno);
        else
            setFieldValue("PLUNO", pluno);
    }

    public HYIDouble getUnitPrice() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("unitPrice");
        else {
            return (HYIDouble) getFieldValue("UNITPRICE");
        }
    }

    public void setSelfbuyOrderid(Integer selfbuyOrderid) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("selfbuyOrderid", selfbuyOrderid);
        else
            setFieldValue("selfbuyOrderid", selfbuyOrderid);
    }

    public Integer getSelfbuyOrderid() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer) getFieldValue("selfbuyOrderid");
        else {
            return (Integer) getFieldValue("selfbuyOrderid");
        }
    }

    public void setSelfbuyOrderno(String selfbuyOrderno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("selfbuyOrderno", selfbuyOrderno);
        else
            setFieldValue("selfbuyOrderno", selfbuyOrderno);
    }

    public String getSelfbuyOrderno() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("selfbuyOrderno");
        else {
            return (String) getFieldValue("selfbuyOrderno");
        }
    }

    /**
     * for cankun
     *
     * @return
     */
    public HYIDouble getUnitPrice2() {
        if (getYellowAmount() != null
            && getYellowAmount().compareTo(new HYIDouble(0)) > 0) 
        {
            if (unitPrice2 == null){
                DbConnection connection = null;
                try {
                    connection = CreamToolkit.getPooledConnection();
                    PLU plu = PLU.queryByInStoreCode(getItemNumber());
                    if (plu.getMinPrice() == null
                        || plu.getMinPrice().compareTo(new HYIDouble(0)) == 0)
                        return this.getUnitPrice();
                    unitPrice2 = plu.getMinPrice();
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                    unitPrice2 = new HYIDouble(0);
                } finally {
                    CreamToolkit.releaseConnection(connection);
                }
            }
            return unitPrice2;
        } else
            return this.getUnitPrice();
    }

    public void setUnitPrice(HYIDouble unitprice) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("unitPrice", unitprice);
        else
            setFieldValue("UNITPRICE", unitprice);
    }

    public HYIDouble getQuantity() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("quantity");
        else
            return (HYIDouble) getFieldValue("QTY");
    }

    public void setQuantity(HYIDouble qty) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("quantity", qty);
        else
            setFieldValue("QTY", qty);

        // Meyer
        this.noMatchedQty = qty.intValue();
    }

    public HYIDouble getAfterRebateAmount() {
        HYIDouble bd = (HYIDouble) getFieldValue("afterRebateAmount");
        if (bd != null)
            return bd;
        else
            return new HYIDouble(0);
    }

    public void setAfterRebateAmount(HYIDouble afterRebateAmt) {
        setFieldValue("afterRebateAmount", afterRebateAmt);
    }

    public HYIDouble getRebateAmount() {
        HYIDouble bd = (HYIDouble) getFieldValue("rebateAmount");
        if (bd != null)
            return bd;
        else
            return new HYIDouble(0);
    }

    public void setRebateAmount(HYIDouble rebateAmt) {
        setFieldValue("rebateAmount", rebateAmt.setScale(rebateAmountScale,
                BigDecimal.ROUND_HALF_UP));
    }

    public HYIDouble getAddRebateAmount() {
        HYIDouble bd = (HYIDouble) getFieldValue("addRebateAmount");
        if (bd != null)
            return bd;
        else
            return new HYIDouble(0);
    }

    public void setAddRebateAmount(HYIDouble addRebateAmt) {
        setFieldValue("addRebateAmount", addRebateAmt.setScale(
                rebateAmountScale, BigDecimal.ROUND_HALF_UP));
    }

    private HYIDouble rebateRate = new HYIDouble(0.0);

    private HYIDouble originalAfterDiscountAmount;

    public void setRebateRate(HYIDouble rate) {
        rebateRate = rate;
    }

    public HYIDouble getRebateRate() {
        return rebateRate;
    }

    public HYIDouble getAmount() {
        if (getDetailCode() == null)
            return new HYIDouble(0);
        // 非普通商品，直接返回字段金额或零
        if (getDetailCode().equals("D")
                || getDetailCode().equals("M")
             // || ( getIsScaleItem()
                || (getWeight() != null &&
                getWeight().compareTo(new HYIDouble(0)) != 0
                // Bruce/20030422 fix a bug when
                // return the value of amount is negative
                )) {
            Object tmp = null;
            if (hyi.cream.inline.Server.serverExist())
                tmp = getFieldValue("amount");
            else
                tmp = getFieldValue("AMT");
            if (tmp == null)
                return new HYIDouble(0);
            else
                return (HYIDouble) tmp;
        }

        // 对普通商品，用单价乘以数量重设总金额
        HYIDouble amt = (getUnitPrice().multiply(getQuantity()))
            .setScale(2, BigDecimal.ROUND_HALF_UP);
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("amount", amt);
        else
            setFieldValue("AMT", amt);
        return amt;
    }

    public void setAmount(HYIDouble amt) {
        amt = amt.setScale(2, 4);
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("amount", amt);
        else
            setFieldValue("AMT", amt);
        if (getAfterSIAmount() == null) {
            afterSIAmount = amt;
        }
    }

    public HYIDouble getAmount2() {
        if (getYellowAmount() != null
            && getYellowAmount().compareTo(new HYIDouble(0)) > 0) {

            if (amount2 == null){
                PLU plu = PLU.queryByInStoreCode(getItemNumber());
                if (plu.getMinPrice() == null
                        || plu.getMinPrice().compareTo(new HYIDouble(0)) == 0)
                    amount2 = this.getAmount();
                amount2 = plu.getMinPrice().multiply(this.getQuantity());
            }
            return amount2;
        } else {
            return this.getAmount();
        }
    }

    public String getTaxType() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("taxID");
        else
            return (String) getFieldValue("TAXTYPE");
    }

    public void setTaxType(String taxtype) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxID", taxtype);
        else
            setFieldValue("TAXTYPE", taxtype);
    }

    public String getDescription() {
        if (StringUtils.isEmpty(description)) {

            // description retrieving --

            PLU plu = PLU.queryBarCode(getPluNumber());
            if (plu != null) {
                String name = PARAM.getPluPrintField();
                if (name.equals("pluname"))
                    description = plu.getScreenName();
                else
                    description = plu.getPrintName();

            } else {
                if (!StringUtils.isEmpty(getPluNumber())) {
                    // '08'：代售项目 '09'：代付项目 '10'：PaidIn项目 '11'：PaidOut项目
                    String cat = "";
                    // 代售
                    if (getDetailCode().equals("I")) { // 代售
                        cat = "08";
                    } else if (getDetailCode().equals("O")) { // 公共事业费代收
                        try {
                            DbConnection connection = CreamToolkit.getPooledConnection();
                            DaiShouSales2 dss = DaiShouSales2.queryDaiShouSales(connection,
                                getTransactionNumber().intValue(),
                                getLineItemSequence());
                            if (dss != null) {
                                DaiShouDef dsd = DaiShouDef.queryByID(connection, dss.getID());
                                setDescriptionAndSpecification(dsd.getName());
                                setDescription(dsd.getName());
                                setDaiShouBarcode(dss.getBarcode());
                                setDaiShouID(dss.getID());
                            } else {
                                setDescriptionAndSpecification("代收");
                                setDescription("代收");
                            }
                            CreamToolkit.releaseConnection(connection);
                        } catch (Exception e) {}

                    } else if (getDetailCode().equals("Q")) { // 代付
                        cat = "09";
                    } else if (getDetailCode().equals("N")) { // PaidIn
                        cat = "10";
                    } else if (getDetailCode().equals("T")) { // PaidOut
                        cat = "11";
                    }
                    Reason r = Reason.queryByreasonNumberAndreasonCategory(getPluNumber(), cat);
                    if (r != null)
                        description = r.getreasonName();

                } else {
                    Category category = Category.queryByCategoryNumber(getCategoryNumber(),
                        getMidCategoryNumber(), getMicroCategoryNumber());
                    if (category != null && category.getScreenName() != null && !category.getScreenName().trim().equals("")) {
                        setDescription(category.getScreenName());
                        setDescriptionAndSpecification(category.getScreenName());
                    } else {
                        TaxType t = TaxType.queryByTaxID(getTaxType());
                        if (t != null) {
                            description = t.getTaxName();
                        }
                    }
                }
            }
        }

        String str = description;
        if (PARAM.isPrintLineItemWithDtlCode1())
            str += getDtlcode1() == null ? "" : getDtlcode1();
        return str;
    }

    public String getPrintName() {
        if (printName != null)
            return printName; // get cached one

        if (getPluNumber() == null)
            return "";

        PLU plu = PLU.queryBarCode(getPluNumber());
        if (plu == null) {
            if (getDescription() != null) {
                printName = getDescription();
                return printName;
            } else
                return "";
        } else {
            printName = plu.getPrintName();
            return printName;
        }

    }

    public void setDescription(String description) {
        this.description = description;
    }

    // Add in second iteration
    // TaxAmount 税额
    public HYIDouble getTaxAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("taxAmount");
        else
            return (HYIDouble) getFieldValue("TAXAMT");
    }

    /**
     * 计算该行交易的折扣的TaxAmount = 毛额的税 - 净额的税
     */
    public HYIDouble getSiTaxAmount() {
        HYIDouble amt = StateToolkit.calculateTaxAmt(TaxType.queryByTaxID(this
                .getTaxType()), this.getAmount(), this
                .getQuantity());
        HYIDouble amt1 = StateToolkit.calculateTaxAmt(TaxType.queryByTaxID(this
                .getTaxType()), this.getAfterDiscountAmount(), this
                .getQuantity());
        return amt.subtractMe(amt1);
    }

    /**
     * 计算该行交易的 毛额TaxAmount
     */
    public HYIDouble getGrossSaleTaxAmount() {
        HYIDouble amt = StateToolkit.calculateTaxAmt(TaxType.queryByTaxID(this
                .getTaxType()), this.getAmount(), this
                .getQuantity());
        return amt;
    }

    /**
     * 计算该行交易的TaxAmount 并设置入TaxAmount
     */
    public HYIDouble caculateAndSetTaxAmount() {
        HYIDouble amt = StateToolkit.calculateTaxAmt(
                TaxType.queryByTaxID(this.getTaxType()),
                PARAM.isGrossTrandtlTaxAmt() ? this.getAmount() : this.getAfterDiscountAmount(),
                this.getQuantity());
        this.setTaxAmount(amt);
        //System.out.println("caculateAndSetTaxAmount> getIsGrossTrandtlTaxAmt=" + GetProperty.getIsGrossTrandtlTaxAmt()
        //    + ", amt=" + getAmount() + ", aftdscamt=" + getAfterDiscountAmount() + ", tax=" + amt);
        return amt;
    }

    public void setTaxAmount(HYIDouble TaxAmount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount", TaxAmount);
        else
            setFieldValue("TAXAMT", TaxAmount);
    }

    public String getTaxPrintedLabel() {
        TaxType taxType = TaxType.queryByTaxID(getTaxType());
        if (taxType == null)
            return " ";
        return (taxType.getPercent() != null && taxType.getPercent().compareTo(new HYIDouble(0)) != 0) ? "T" : " ";
//
//        return getTaxAmount().doubleValue() == 0.0D ? " " : "T";
    }

    public HYIDouble getAfterDiscountAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("afterDiscountAmount");
        else
            return (HYIDouble) getFieldValue("AFTDSCAMT");
    }

    public void setAfterDiscountAmount(HYIDouble discamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("afterDiscountAmount", discamt);
        else
            setFieldValue("AFTDSCAMT", discamt);
    }

    public String getDiscountType() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("discountType");
        else
            return (String) getFieldValue("DISCTYPE");
    }

    public void setDiscountType(String disctype) {
        // TODO 各种值的含义要整理
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountType", disctype);
        else
            setFieldValue("DISCTYPE", disctype);
    }

    public String getDiscountNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("discountID");
        else
            return (String) getFieldValue("DISCNO");
    }

    public void setDiscountNumber(String discountNumber) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountID", discountNumber);
        else
            setFieldValue("DISCNO", discountNumber);
    }

    /**
     * 根据DiscountNumber 得到所有折扣类型 其中DiscountNumber的形式为 Mxxx,Mxxx,Sxx,Sxx,Sxx
     *
     * @since 2003-07-15
     * @return ArrayList
     */
    public ArrayList getDiscTypeObjects() {
        ArrayList dos = new ArrayList();
        String discountNumber = getDiscountNumber();
        String id = "";
        StringTokenizer stn = new StringTokenizer(discountNumber, ",");
        while (stn.hasMoreTokens()) {
            id = stn.nextToken();
            if (id.startsWith("M")) {
                // 暂时不处理组合促销
            } else if (id.startsWith("S")) { // SI 折扣
                SI si = SI.queryBySIID(id.substring(1));
                if (si != null)
                    dos.add(si);
            }
        }
        return dos;
    }

    public String getSpecification() {
        if (specification == null){
            if (getPluNumber() == null)
                specification = "";
            PLU plu = PLU.queryBarCode(getPluNumber());
            if (plu == null)
                specification = "";
            else
                specification = plu.getSpecification();
        }
        return specification;

    }

    public void setDescriptionAndSpecification(String str) {
        descriptionAndSpecification = str;
    }

    public String getDescriptionAndSpecification() {
        if (descriptionAndSpecification == null) {
            // get cached one
            if (getPluNumber() == null)
                descriptionAndSpecification = "";
            PLU plu = PLU.queryBarCode(getPluNumber());
            if (plu == null) {
                if (getDescription() != null) {
                    descriptionAndSpecification = getDescription();
                } else
                    descriptionAndSpecification = "";
            } else {
                descriptionAndSpecification = plu.getNameAndSpecification();
            }
        }
        return descriptionAndSpecification;
    }

    private void getAdditionalInfo() {
        if (getItemNumber() == null)
            return;

        PLU plu = PLU.queryByItemNumber(getItemNumber());
        if (plu == null) {
            smallUnit = "";
            invCycleNo = "";
            sizeCategory = "";
            itemBrand = "";
            size = "";
            color = "";
            style = "";
            season = "";
        } else {
            smallUnit = plu.getSmallUnit();
            invCycleNo = plu.getInvCycleNo();
            sizeCategory = plu.getSizeCategory();
            itemBrand = plu.getItemBrand();
            size = plu.getSize();
            color = plu.getColor();
            style = plu.getStyle();
            season = plu.getSeason();
        }
    }

    public String getSmallUnit() {
        if (smallUnit != null)
            return smallUnit;
        getAdditionalInfo();
        return smallUnit;
    }

    public String getInvCycleNo() {
        if (invCycleNo != null)
            return invCycleNo;
        getAdditionalInfo();
        return invCycleNo;
    }

    public String getSizeCategory() {
        if (sizeCategory != null)
            return sizeCategory;
        getAdditionalInfo();
        return sizeCategory;
    }

    public String getItemBrand() {
        if (itemBrand != null)
            return itemBrand;
        getAdditionalInfo();
        return itemBrand;
    }

    public String getSize() {
        if (size != null)
            return size;
        getAdditionalInfo();
        return size;
    }

    public String getColor() {
        if (color != null)
            return color;
        getAdditionalInfo();
        return color;
    }

    public String getStyle() {
        if (style != null)
            return style;
        getAdditionalInfo();
        return style;
    }

    public String getSeason() {
        if (season != null)
            return season;
        getAdditionalInfo();
        return season;
    }

    public void setSmallUnit(String value) {
        smallUnit = value;
    }

    public void setInvCycleNo(String value) {
        invCycleNo = value;
    }

    public void setSizeCategory(String value) {
        sizeCategory = value;
    }

    public void setItemBrand(String value) {
        itemBrand = value;
    }

    public void setSize(String value) {
        size = value;
    }

    public void setColor(String value) {
        color = value;
    }

    public void setStyle(String value) {
        style = value;
    }

    public void setSeason(String value) {
        season = value;
    }

    // Removed ITEMVOID ENUM("0","1") N 更正flag
    // ITEMVOID is "1" if this line item has been removed（被更正）.
    // Removed property is true if this line item has been removed.
    public boolean getRemoved() {
        String r = null;
        if (hyi.cream.inline.Server.serverExist())
            r = (String) getFieldValue("itemVoid");
        else
            r = (String) getFieldValue("ITEMVOID");
        if (r == null) {
            return false;
        }
        if (r.length() == 0) {
            return false;
        }
        if (r.equals("1"))
            return true;
        else
            return false;
    }

    public void setRemoved(boolean removed) {
        /*
         * if (removed) setFieldValue("ITEMVOID", "1"); else
         * setFieldValue("ITEMVOID", "0");
         */
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("itemVoid", ((removed) ? "1" : "0"));
        else
            setFieldValue("ITEMVOID", ((removed) ? "1" : "0"));
    }

    public Color getLineItemColor() {
        return lineItemColor;
    }

    public void setLineItemColor(Color c) {
        lineItemColor = c;
    }

    public String getSINo() {
        return siNo;
    }

    public void setSINo(String sino) {
        this.siNo = sino;
    }

    public HYIDouble getTempAfterDiscountAmount() {
        return tada;
    }

    public void setTempAfterDiscountAmount(HYIDouble discamt) {
        tada = discamt;
    }

    public String getTempDiscountType() {
        return tdt;
    }

    public void setTempDiscountType(String disctype) {
        tdt = disctype;
    }

    public String getTempDiscountNumber() {
        return tdn;
    }

    public void setTempDiscountNumber(String discountNumber) {
        tdn = discountNumber;
    }

    public void setOpenPrice(boolean isOpenPrice) {
        this.openPrice = isOpenPrice;
    }

    public boolean isOpenPrice() {
        return openPrice;
        // if (this.getPluNumber() == null)
        // return false;
        //
        // PLU plu = PLU.queryBarCode(getPluNumber());
        // if (plu == null)
        // return false;
        // return plu.isOpenPrice();
    }

    public void updateMM() {
        if (tada == null)
            tada = new HYIDouble(0);

        if (getAfterDiscountAmount() == null) {
            setAfterDiscountAmount(this.getAmount().add(tada));
        } else {
            setAfterDiscountAmount(this.getAfterDiscountAmount().add(tada));
        }
        String t = tdt;
        String n = tdn;
        if (t != null && n.length() > 0)
            setDiscountType(t);

        if (n != null && n.length() > 0) {
            if (getDiscountNumber() == null)
                setDiscountNumber("M" + n);
            else {
                String disCno = this.getDiscountNumber();
                int index = disCno.indexOf("O");
                if (index >= 0) {
                    String left = disCno.substring(0, index);
                    String right = disCno.substring(index, disCno.length());
                    if (right.indexOf(",") > 0) {
                        right = right.substring(right.indexOf(",") + 1, right
                                .length());
                    } else {
                        right = "";
                        if (left.length() > 0)
                            left = left.substring(0, left.length() - 1);
                    }
                    disCno = left + right;
                }
                if (!disCno.equals(""))
                    disCno += ",";
                this.setDiscountNumber(disCno + "M" + n);
            }
        }

        tada = new HYIDouble(0);
        tdt = "";
        tdn = "";
        // System.out.println("type1 = " + getDiscountType());
    }

    public void setDaishouNumber(int daishouNumber) {
        this.daishouNumber = daishouNumber;
    }

    public int getDaishouNumber() {
        return daishouNumber;
    }

    public LineItem cloneForSC() {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            LineItem clonedLineItem = new LineItem(0);
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = this.getFieldValue(fieldNameMap[i][0]);
                if (value == null) {
                    try {
                        value = getClass().getDeclaredMethod("get" + fieldNameMap[i][0]).invoke(this);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedLineItem.setFieldValue(fieldNameMap[i][1], value);
            }
            return clonedLineItem;
        } catch (InstantiationException e) {
            return null;
        }
    }

    public LineItem cloneForPOS() {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            LineItem clonedLineItem = new LineItem(0);
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = this.getFieldValue(fieldNameMap[i][1]);
                if (value == null) {
                    try {
                        value = getClass().getDeclaredMethod("get" + fieldNameMap[i][1]).invoke(this);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedLineItem.setFieldValue(fieldNameMap[i][0], value);
            }
            return clonedLineItem;
        } catch (InstantiationException e) {
            return null;
        }
    }

    static public void deleteOutdatedData() {
        DbConnection connection = null;
        Statement statement = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            if (hyi.cream.inline.Server.serverExist()) {
                // 需要保留的天数
                int tranReserved = PARAM.getTransactionReserved();
                long l = new java.util.Date().getTime() - tranReserved * 1000L * 3600 * 24;
                String baseTime = CreamCache.getInstance().getDateTimeFormate().format(new java.sql.Date(l));
                statement.executeUpdate("DELETE FROM posul_trandtl WHERE systemDate < '" + baseTime + "'");
            } else {
                Iterator<String> itr = Transaction.getOutdateTranNumbers();
                while (itr != null && itr.hasNext())
                    statement.executeUpdate("DELETE FROM trandetail WHERE tmtranseq=" + itr.next());
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
    }

    public void setDaishouDetailNumber(int n1) {
        daishouDetailNumber = n1;
    }

    public int getDaishouDetailNumber() {
        return daishouDetailNumber;
    }

    public void setDaishouDetailName(String s) {
        daishouDetailName = s;
    }

    public String getDaishouDetailName() {
        return daishouDetailName;
    }

    /**
     * Meyer / 2003-02-19 / Add field : itemNumber, originalPrice
     *
     */
    public String getItemNumber() {
        // String
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("itemNumber");
        else
            return (String) getFieldValue("ITEMNO");
    }

    public void setItemNumber(String anItemNumber) {
        String fieldName = "";
        if (hyi.cream.inline.Server.serverExist()) {

            fieldName = "itemNumber";
        } else {
            fieldName = "ITEMNO";
        }
        if (isFieldExist(fieldName)) {
            setFieldValue(fieldName, anItemNumber);
        }
    }

    public HYIDouble getOriginalPrice() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("originalPrice");
        else
            return (HYIDouble) getFieldValue("ORIGPRICE");
    }

    public void setOriginalPrice(HYIDouble anOriginalPrice) {
        String fieldName = "";
        if (hyi.cream.inline.Server.serverExist()) {
            fieldName = "originalPrice";
        } else {
            fieldName = "ORIGPRICE";
        }

        if (isFieldExist(fieldName)) {
            setFieldValue(fieldName, anOriginalPrice);
        }
    }

    /**
     * Set 折扣率代号。
     *
     * @param id
     *            折扣率代号
     */
    public void setDiscountRateID(String id) {
        setFieldValue("discountRateID", id);
    }

    /**
     * Get 折扣率代号。
     *
     * @return 折扣率代号
     */
    public String getDiscountRateID() {
        return (String) getFieldValue("discountRateID");
    }

    /**
     * Set 折扣率。
     *
     * @param rate
     *            折扣率
     */
    public void setDiscountRate(HYIDouble rate) {
        setFieldValue("discountRate", rate);
    }

    /**
     * Get 折扣率。
     *
     * @return 折扣率
     */
    public HYIDouble getDiscountRate() {
        return (HYIDouble) getFieldValue("discountRate");
    }

    /**
     * 按金额权重分摊折扣金额，更新交易明细的 "aftDscAmt" - 折让后金额
     *
     * @param isLastApportionPlu -
     *            boolean, 是否是本次促销分摊中的最后一个，是则摊得全部剩余折扣金额
     * @param leaveApportionQty -
     *            int，可供分摊的商品数量
     * @param totalAmt -
     *            BigDecimal，分摊涉及的总金额
     * @param totalDiscAmt -
     *            BigDecimal，要分摊的总折扣金额
     * @param leaveDiscAmt -
     *            BigDecimal，可供分摊的剩余折扣金额
     *
     */
    public int apportion(boolean isLastApportionPlu, int leaveApportionQty,
            HYIDouble totalAmt, HYIDouble totalDiscAmt, HYIDouble leaveDiscAmt) {
        // 初始化 noMatchedQty
        initNoMatchedQty();
        int matchedQty = (noMatchedQty > leaveApportionQty) ? leaveApportionQty
                : noMatchedQty;

        System.out.println("分摊折让: pluCode=" + this.getItemNumber());
        if (isLastApportionPlu && leaveApportionQty == matchedQty) {
            System.out.println("    The Last Apportion Plu");
        }
        System.out.println("    Before: totalQty=" + this.getQuantity()
                + "/leaveQty=" + leaveApportionQty + "/totalDiscAmt="
                + totalDiscAmt + "/leaveDiscAmt=" + leaveDiscAmt);

        noMatchedQty -= matchedQty;
        leaveApportionQty = leaveApportionQty - matchedQty;

        /*
         * 分摊: matchedDiscAmt = ((matchedQty * unitPrice) / totalAmt) *
         * totalDiscAmt
         *
         * 当isLastApportionPlu 并且 noMatchedQty = 0 时，matchedDiscAmt =
         * leaveDiscAmt, 将剩下的折让全部赋给最后一笔交易明细
         */
        System.out.println("    ------ : isLastApportionPlu=" + isLastApportionPlu
                + "/leaveApportionQty=" + leaveApportionQty);
        HYIDouble matchedDiscAmt = new HYIDouble(0);
        if (isLastApportionPlu && leaveApportionQty == 0) {
            matchedDiscAmt = leaveDiscAmt;
        } else {
            matchedDiscAmt = (this.getUnitPrice().multiply(
                    new HYIDouble(matchedQty)).divide(totalAmt, 4,
                    BigDecimal.ROUND_HALF_UP).multiply(totalDiscAmt));
        }
        System.out.println("    ------ : matchedQty=" + matchedQty
                + "/leaveQty=" + leaveApportionQty
                + "/matchedDiscAmt=" + matchedDiscAmt
                + "/leaveDiscAmt=" + leaveDiscAmt);

        //        MATCHEDDISCAMT = MATCHEDDISCAMT.SETSCALE(
//                2, BIGDECIMAL.ROUND_HALF_UP);
        leaveDiscAmt = (leaveDiscAmt.subtract(matchedDiscAmt));
//                .setScale(2, BigDecimal.ROUND_HALF_UP);

        System.out.println("    After: matchedQty=" + matchedQty
                + "/leaveQty=" + leaveApportionQty
                + "/matchedDiscAmt=" + matchedDiscAmt
                + "/leaveDiscAmt=" + leaveDiscAmt);

        // update trandetail: TAXAMT, AFTDSCAMT, DISCTYPE
        HYIDouble aftDscAmt = this.getAfterDiscountAmount().subtract(
                matchedDiscAmt);
        this.setAfterDiscountAmount(aftDscAmt.setScale(
                2, BigDecimal.ROUND_HALF_UP));
        this.caculateAndSetTaxAmount();
        this.setDiscountType("M");

        return matchedQty;
    }

    /**
     * 赠送：
     *
     * @param leaveGiftQty -
     *            int, 可供赠送的商品数量
     *
     */
    public int present(int leaveGiftQty) {
        System.out.println("赠送：pluCode=" + this.getItemNumber());
        System.out.println("    Before: totalQty=" + this.getQuantity()
                + "/leaveGiftQty=" + leaveGiftQty);
        // 初始化 noMatchedQty
        initNoMatchedQty();
        int matchedQty = (noMatchedQty > leaveGiftQty) ? leaveGiftQty
                : noMatchedQty;
        HYIDouble matchedGiftAmt = new HYIDouble(0);
        matchedGiftAmt = this.getOriginalPrice()//.getUnitPrice()
                .multiply(new HYIDouble(matchedQty));
        noMatchedQty -= matchedQty;
        leaveGiftQty = leaveGiftQty - matchedQty;

        System.out.println("    After: matchedQty=" + matchedQty
                + "/leaveGiftQty=" + leaveGiftQty + "/matchedGiftAmt="
                + matchedGiftAmt);

        // update trandetail: TAXAMT, AFTDSCAMT, DISCTYPE
        HYIDouble aftDscAmt = this.getAfterDiscountAmount().subtract(
                matchedGiftAmt);
        this.setAfterDiscountAmount(aftDscAmt
                .setScale(2, BigDecimal.ROUND_HALF_UP));
        this.caculateAndSetTaxAmount();
        this.setDiscountType("M");

        return matchedQty;
    }

    /**
     * Init the noMatchedQty.
     */
    public void initNoMatchedQty() {
        if (noMatchedQty == Integer.MIN_VALUE) {
            this.noMatchedQty = this.getQuantity().intValue();
        }
    }

    public void resetNoMatchedQty() {
        this.noMatchedQty = this.getQuantity().intValue();
    }

    public int getNoMatchedQty() {
        return noMatchedQty;
    }

    private static String[][] posToScFieldNameArray;

    /**
     * Meyer/2003-02-20 return fieldName map of PosToSc as String[][]
     */
    public static String[][] getPosToScFieldNameArray() {
        if (posToScFieldNameArray == null) {
            posToScFieldNameArray = new String[][] {
                { "StoreID", "storeID" },
                { "SystemDateTime", "systemDate" },
                { "TMCODE", "posNumber" },
                { "TMTRANSEQ", "transactionNumber" },
                { "ITEMSEQ", "lineItemSequence" },
                { "CODETX", "detailCode" },
                { "ITEMVOID", "itemVoid" },
                { "CATNO", "categoryNumber" },
                { "MIDCATNO", "midCategoryNumber" },
                { "MICROCATNO", "microCategoryNumber" },
                { "THINCATNO", "thinCategoryNumber" },
                { "PLUNO", "pluNumber" },
                { "UNITPRICE", "unitPrice" },
                { "QTY", "quantity" },
                { "WEIGHT", "weight" },
                { "DISCNO", "discountID" },
                { "AMT", "amount" },
                { "TAXTYPE", "taxID" },
                { "TAXAMT", "taxAmount" },
                // Meyer / 2003-02-19 / Add 2 Field
                { "ITEMNO", "itemNumber" },
                { "ORIGPRICE", "originalPrice" },
                // Meyer/2003-03-10/add 1 field
                { "DISCTYPE", "discountType" },
                // Meyer/2003-03-11/Add 1 field
                { "AFTDSCAMT", "afterDiscountAmount" },
                { "discountRateID", "discountRateID" },
                { "discountRate", "discountRate" },
                { "rebateAmount", "rebateAmount" },
                { "addRebateAmount", "addRebateAmount" },
                { "afterRebateAmount", "afterRebateAmount" },
                { "content", "content" },
                { "yellowAmount", "yellowAmount" },
                { "diyIndex", "diyIndex" },
                { "dtlcode1", "dtlcode1" },
                { "selfbuyOrderid", "selfbuyOrderid" },
                { "selfbuyOrderno", "selfbuyOrderno" },
            };
        }
        return posToScFieldNameArray;
    }

    public HYIDouble getDiscountAmount() {
        return !getIsScaleItem() ?
            getUnitPrice().subtract(getOriginalPrice()).multiply(getQuantity()) :
            HYIDouble.zero();
    }

    public HYIDouble getWeight() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("weight");
        else
            return (HYIDouble) getFieldValue("WEIGHT");
    }

    public void setWeight(HYIDouble weight) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("weight", weight);
        else
            setFieldValue("WEIGHT", weight);
    }

    public void setDaiShouBarcode(String barcode) {
        this.daiShouBarcode = barcode;
    }

    public String getDaiShouBarcode() {
        return daiShouBarcode;
    }

    public void setDaiShouID(String id) {
        this.daiShouID = id;
    }

    public String getDaiShouID() {
        return daiShouID;
    }

//    public boolean isCommit() {
//        return commit;
//    }
//
//    public void setCommit(boolean commit) {
//        this.commit = commit;
//    }

    public void setOriginalAfterDiscountAmount(HYIDouble value) {
        originalAfterDiscountAmount = value;
    }

    public HYIDouble getOriginalAfterDiscountAmount() {
        return originalAfterDiscountAmount;
    }

    public void setNeedPhoneCardNumber(boolean need) {
        isNeedPhoneCardNumber = need;
    }

    public boolean getNeedPhoneCardNumber() {
        return isNeedPhoneCardNumber;
    }


    public void setAuthorNoContent(String authorNo) {
        if (authorNo == null){
            authorNo = "";
        }
        // TODO 暂时将授权变价收银员代号放在trandetail.content内, 用auth:012345678:括起来标识, 以后要放到
        authorNo = OVERRIDE_AMOUNT_AUTH_CASHIER_NO_PREFIX
            + authorNo
            + OVERRIDE_AMOUNT_AUTH_CASHIER_NO_SUBFIX;
        setFieldValue("content", authorNo);
    }

    public void setContent(String content) {
        setFieldValue("content", content);
    }

    public String getContent() {
        return (String) getFieldValue("content");
    }

    /**
     * 是否是磅秤商品?
     */
    public boolean getIsScaleItem() {
        return isScaleItem;
    }

    public void setIsScaleItem(boolean isScaleItem) {
        this.isScaleItem = isScaleItem;
    }

    /**
     * 13码，18码磅秤商品不能用数量键
     */
    boolean quantityEnabled = true;

    private String dtlcode1desc;

    public boolean getQuantityEnabled() {
        return quantityEnabled;
    }

    public void setQuantityEnabled(boolean flag) {
        quantityEnabled = flag;
    }

    public HYIDouble getYellowAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble) getFieldValue("yellowAmount");
        else
            return (HYIDouble) getFieldValue("yellowAmount");
    }

    public void setYellowAmount(HYIDouble amount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("yellowAmount", amount);
        else
            setFieldValue("yellowAmount", amount);
    }

    public boolean isOnlyDiscount() {
        return onlyDiscount;
    }

    public void setOnlyDiscount(boolean flag) {
        this.onlyDiscount = flag;
    }

    public String getDIYIndex() {
        if (hyi.cream.inline.Server.serverExist())
            return (String) getFieldValue("diyIndex");
        else
            return (String) getFieldValue("diyIndex");
    }

    public void setDIYIndex(String index) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("diyIndex", index);
        else
            setFieldValue("diyIndex", index);
    }

    public HYIDouble getTotalOriginalAmount() {
        return getOriginalPrice().multiply(getQuantity()).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public HYIDouble getSiAmount() {
        return siAmount;
    }

    public void setSiAmount(HYIDouble siAmount) {
        this.siAmount = siAmount;
    }

    public int getVoidSequence() {
        return voidSequence;
    }

    public void setVoidSequence(int voidSequence) {
        this.voidSequence = voidSequence;
    }

    public String getDtlcode1() {
        return (String) getFieldValue("dtlcode1");
    }

    public void setDtlcode1(String dtlcode1) {
        setFieldValue("dtlcode1", dtlcode1);
    }

    public String getPrintSecondLine() {
        if (printSecondLine == null) {
            if (!"J".equalsIgnoreCase(getDetailCode())) {
                printSecondLine = "";
            } else if (getContent() == null || getContent().trim().length() == 0){
                printSecondLine = "";
            } else {
                PLU plu = PLU.queryBarCode(getPluNumber());
                if (plu.getItemtype() == null || plu.getItemtype() != 10) {
                    printSecondLine = "";
                } else {
                    String str = getContent().trim();
                    printSecondLine = "";
                    try {
                        printSecondLine = str.substring(0, 4) + "--"
                        + str.substring(5, 8) +
                        MessageFormat.format(CreamToolkit.GetResource().getString(
                            "OrderPerioicalMessage1"), str.substring(10));
                    } catch (Exception e) {
                    }
                }
            }
        }

        return printSecondLine;
    }

    public String getDepID() {
        if (depID == null) {
            PLU plu = PLU.queryBarCode(this.getPluNumber());
            if (plu != null)
                depID = plu.getDepID();
        }        
        return depID;
    }

    public void setDepID(String depID) {
        this.depID = depID;
    }

    public void update(DbConnection connection) throws SQLException, EntityNotFoundException {
        setAllFieldsDirty(true);
        super.update(connection);
    }

    /** 是否为更正一个期刊商品。 */
    public boolean isVoidPeriodicalItem() {
        return voidPeriodicalItem;
    }

    public void setVoidPeriodicalItem(boolean voidPeriodicalItem) {
        this.voidPeriodicalItem = voidPeriodicalItem;
    }
}
