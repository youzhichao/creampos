package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ExchangeRate extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    //RATEID
	//static method and initializer
	private static ArrayList primaryKeys = new ArrayList();
	
	public List getPrimaryKeyList() {
		return  primaryKeys;
	}
	
	static final String tableName = "exchangerate";
	
	static {
		primaryKeys.add("RATEID");
	}
		
	public static ExchangeRate queryByCurrencyID(DbConnection connection, String ID) {
		try {
            return getSingleObject(connection, ExchangeRate.class,
            	"SELECT * FROM " + tableName + " WHERE RATEID='" + ID + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}
	
	public ExchangeRate() {
	}

    // Get properties value:
	public String getCurrencyID() {
		return (String)getFieldValue("RATEID");//CurrencyID	RATEID	CHAR(2)
	}

	public String getCurrencyName() {
		return (String)getFieldValue("NAME");//CurrencyName	NAME VARCHAR(12)
	}

	public HYIDouble getExchangeRate() {
		return (HYIDouble)getFieldValue("RATE");//ExchangeRate	RATE DECIMAL(7,3)
	}

    /*
    Class Property Name	Field name	Type
    CurrencyID RATEID CHAR(2)
    CurrencyName NAME VARCHAR(12)
    ExchangeRate RATE DECIMAL(7,3)
    */

	public String getInsertUpdateTableName() {
		return tableName;
	}
}
