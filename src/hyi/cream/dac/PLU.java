package hyi.cream.dac;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetMemberNumberState;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.HYIDouble;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.DateFormat;
import java.util.*;

/**
 * Item PLU(Price LookUp) DAC class.
 *
 * @author Dai, Slackware, Bruce
 * @version 1.5
 */
public class PLU extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *       M  public String getInsertUpdateTableName()
     *       A  public static String getInsertUpdateTableNameStaticVersion()
     *       M  public static PLU queryByPluNumber(String pluNumber)
     *       M  public static PLU queryBarCode(String barCode)
     *       A  public static Collection getAllObjectsForPOS()
     *       A  private static Integer[] getPluSpecialValue(String k, String s, String o)
     *
     *    /Meyer/2003-01-10
     *         增加方法 getSalingPrice()，取得当前的销售单价
     */
    public transient static final String VERSION = "1.5";

    static final String tableName = "plu";
    static final int MAX_CACHE_SIZE = 10;
    private static ArrayList primaryKeys = new ArrayList();
//    private static HashMap cache = new HashMap();
    private int uid;
    private List sequentialFieldList;

    // Table fields ---

    public String PLUNO;
    public String ITEMNO;
    public String PLUNAME;
    public String PRINTNAME;
    public String SHIPNO;
    public String DEPID;
    public String CATNO;
    public String MIDCATNO;
    public String MICROCATNO;
    public String THINCATNO;
    public HYIDouble PLUPRICE;
    public HYIDouble PLUPMPRICE;
    public HYIDouble PLUMBPRICE;
    public HYIDouble PLUOPPRICE1;
    public HYIDouble PLUOPPRICE2;
    public java.sql.Date PLUPMDATES;
    public java.sql.Date PLUPMDATEE;
    public Time PLUPMTIMES;
    public Time PLUPMTIMEE;
    public Integer PLUSIGN1;
    public java.sql.Date DISCBEGDT;
    public java.sql.Date DISCENDDT;
    public Integer PLUSIGN2;
    public Integer PLUSIGN3;
    public String PLUTAX;
    public String SMALLUNIT;
    public String INVCYCLENO;
    public String MAMNO;
    public String SALETYPE;
    public Integer ITEMTYPE;
    public Integer MAGATYPE;
    public Integer MAGACOUNT;
    public Integer CUSTORDTYPE;
    public HYIDouble CUSTORDPRICE1;
    public String ISORD;
    public String SPECIFICATION;
    public String SIZECATEGORY;
    public String ITEMBRAND;
    public String SIZE;
    public String COLOR;
    public String STYLE;
    public String SEASON;
    public String PLUPRICECHANGEID;
    public HYIDouble MINPRICE;
    public HYIDouble YELLOWLIMIT;


    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    static {
        primaryKeys.add("PLUNO");
    }

    public PLU() {

    }

    public String getInsertUpdateTableName() {
        if (getUseClientSideTableName())
            return "plu";
        else if (hyi.cream.inline.Server.serverExist())
            return "posdl_plu";
        else
            return "plu";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posdl_plu";
        else
            return "plu";
    }

    private static final Collection existedFieldList = getExistedFieldList(getInsertUpdateTableNameStaticVersion());    
    public Collection getExistedFieldList() {
        return existedFieldList;        
    }

    // Property getters
    public String getPluNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("pluNumber");
        else
            return (String)getFieldValue("PLUNO");
    }

    public String getItemNumber() {
        return getInStoreCode();
    }

    public String getInStoreCode() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("itemNumber");
        else
            return (String)getFieldValue("ITEMNO");
    }

    public String getScreenName() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("pluName");
        else
            return (String)getFieldValue("PLUNAME");
    }

    public String getPrintName() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("pluEnglishName");
        else
            return (String)getFieldValue("PRINTNAME");
    }

    public String getShipNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("shipNumber");
        else
            return (String)getFieldValue("SHIPNO");
    }

    public String getCategoryNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("categoryNumber");
        else
            return (String)getFieldValue("CATNO");
    }

    public String getMidCategoryNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("midCategoryNumber");
        else
            return (String)getFieldValue("MIDCATNO");
    }

    public String getMicroCategoryNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("microCategoryNumber");
        else
            return (String)getFieldValue("MICROCATNO");
    }

    public String getThinCategoryNumber() {
        String value;
        if (hyi.cream.inline.Server.serverExist())
            value = (String)getFieldValue("thinCategoryNumber");
        else
            value = (String)getFieldValue("THINCATNO");
        return (value == null) ? "" : value;
    }

    public HYIDouble getUnitPrice() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("storeUnitPrice");
        else
            return (HYIDouble)getFieldValue("PLUPRICE");
    }

    public HYIDouble getSpecialPrice() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("specialPrice");
        else
            return (HYIDouble)getFieldValue("PLUPMPRICE");
    }

    public HYIDouble getMemberPrice() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("memberPrice");
        else
            return (HYIDouble)getFieldValue("PLUMBPRICE");
    }
    
    /**
     * 根据member.favorable 返回价格
     * @return 享受优惠方式: 1-会员价 2-会员折扣 3-售价(可能只积分不折价)
     */
    public HYIDouble getMemberPrice(Member m) {
        if (PARAM.getMemberUseRegularPrice())
            return getUnitPrice();

        HYIDouble r =  getMemberPrice(); //预设会员价
        if (m != null){
            String f = m.getFavourable();
            if ("1".equals(f)){
                //r = getMemberPrice(); //预设会员价，r不变
            } else if ("2".equals(f) && m.getDiscount() != null){
                r = getUnitPrice().multiply(m.getDiscount())
                        .divide(new HYIDouble(100), BigDecimal.ROUND_HALF_UP);
                System.out.println(new Date() + " member discount="
                        + m.getDiscount() + " price=" + r);
            } else if ("3".equals(f)){
                r = getUnitPrice();
            }
        }
        //System.out.println(new Date() + "getMemberPrice(member)=" + r);
        return r;
    }

    public HYIDouble getDiscountPrice1() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("couponPrice1");
        else
            return (HYIDouble)getFieldValue("PLUOPPRICE1");
    }

    public HYIDouble getDiscountPrice2() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("couponPrice2");
        else
            return (HYIDouble)getFieldValue("PLUOPPRICE2");
    }

    public java.sql.Date getDiscountStartDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.sql.Date)getFieldValue("specialBeginDate");
        else
            return (java.sql.Date)getFieldValue("PLUPMDATES");
    }

    public java.sql.Date getDiscountEndDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.sql.Date)getFieldValue("specialEndDate");
        else
            return (java.sql.Date)getFieldValue("PLUPMDATEE");
    }

    public java.sql.Time getDiscountStartTime() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.sql.Time)getFieldValue("specialBeginTime");
        else
            return (java.sql.Time)getFieldValue("PLUPMTIMES");
    }

    public java.sql.Time getDiscountEndTime() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.sql.Time)getFieldValue("specialEndTime");
        else
            return (java.sql.Time)getFieldValue("PLUPMTIMEE");
    }

    //public Integer getSI

    public Integer getSIGroup() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("siGroup");
        else {
            final Object plusign1 = getFieldValue("PLUSIGN1");
            if (plusign1 instanceof String)
                return Integer.parseInt((String) plusign1);
            else
                return (Integer)plusign1;
        }
    }

    public java.sql.Date getDiscountBeginDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.sql.Date)getFieldValue("discountBeginDate");
        else
            return (java.sql.Date)getFieldValue("DISCBEGDT");
    }

    public java.sql.Date getDiscountEnd2() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.sql.Date)getFieldValue("discountEndDate");
        else
            return (java.sql.Date)getFieldValue("DISCENDDT");
    }

    public Integer getAttribute1() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("PLUSIGN2");
        else
            return (Integer)getFieldValue("PLUSIGN2");
    }

    public Integer getAttribute2() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("PLUSIGN3");
        else
            return (Integer)getFieldValue("PLUSIGN3");
    }

    public String getTaxType() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("taxID");
        else
            return (String)getFieldValue("PLUTAX");
    }

    /*public String getPriceLimit() {
        return (String)getFieldValue("PLULIMAMT");
    }*/

    public String getDepID() {
        if (hyi.cream.inline.Server.serverExist())
            return ((String)getFieldValue("depID")).trim();
        else
            return ((String)getFieldValue("DEPID")).trim();
    }

    /**
     * 取得商品规格。
     * 
     * @return 商品规格。
     * @since 2003-04-13 for 灿坤
     */
    public String getSpecification() {
        String spec = (String)getFieldValue("SPECIFICATION");
        return (spec == null) ? "" : spec;
    }

    /**
     * 取得商品名加上规格文字。
     * 
     * @return 商品名和规格。
     * @since 2003-04-13 for 灿坤
     */
    public String getNameAndSpecification() {
        return getScreenName() + getSpecification();
    }

    public String getSmallUnit() {
        String value = (String)getFieldValue("SMALLUNIT");
        return (value == null) ? "" : value;
    }

    public String getInvCycleNo() {
        String value = (String)getFieldValue("INVCYCLENO");
        return (value == null) ? "" : value;
    }

    public String getSizeCategory() {
        String value = (String)getFieldValue("SIZECATEGORY");
        return (value == null) ? "" : value;
    }

    public String getItemBrand() {
        String value = (String)getFieldValue("ITEMBRAND");
        return (value == null) ? "" : value;
    }

//    對pos幾的plu添加服飾相關字段, 用于在POS錄入商品畫面顯示
//    ==========================================================================
//    alter table plu add column sizeCategory varchar(20);
//
//    alter table plu add column itemBrand    varchar(50);
//    alter table plu add column size         varchar(20);
//    alter table plu add column color        varchar(20);
//
//    alter table plu add column style        varchar(20);
//    alter table plu add column season       varchar(20);
//    ==========================================================================
    public String getSize() {
        String value = (String)getFieldValue("SIZE");
        return (value == null) ? "" : value;
    }

    public String getColor() {
        String value = (String)getFieldValue("COLOR");
        return (value == null) ? "" : value;
    }

    public String getStyle() {
        String value = (String)getFieldValue("STYLE");
        return (value == null) ? "" : value;
    }

    public String getSeason() {
        String value = (String)getFieldValue("SEASON");
        return (value == null) ? "" : value;
    }

    public String getMixAndMatchNumber() {
        String m = "";
        if (hyi.cream.inline.Server.serverExist())
            m = (String)getFieldValue("mixAndMatchNumber");
        else
            m = (String)getFieldValue("MAMNO");
        if (m == null)
            m = "0000";
        else
            m = m.trim();
        while (m.length() < 4) {
            m = m + "0";
        }
        return m;
    }

    public String getMixAndMatchID() {
        if (getMixAndMatchNumber() == null || getMixAndMatchNumber().equals("")) {
            return "";
        } else {
            return getMixAndMatchNumber().substring(0, 3);
        }
    }

    public String getMixAndMatchGroupID() {
        if (getMixAndMatchNumber() == null || getMixAndMatchNumber().equals("")) {
            return "";
        } else {
            return getMixAndMatchNumber().charAt(3) + "";
        }
    }

//    /**
//     * Query PLU by a PLU number.
//     */
//    public static PLU queryByPluNumber(String pluNumber) {
//        /*
//         * Meyer/2003-02-28/去点PLU的CACHE
//         */
//        // if (cache.containsKey(pluNumber))
//        //   return (PLU)cache.get(pluNumber);
//
//        try {
//            PLU plu;
//            if (hyi.cream.inline.Server.serverExist()) {
//                String query = "SELECT storeid FROM store";
//                Object storeID = getValueOfStatement(query);
//                String sel = "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
//                    " WHERE " +
//                    ((storeID == null) ? "" : " storeID='" + storeID.toString() + "' AND ") +
//                    " pluNumber='" + pluNumber + "'";
//                plu = (PLU)getSingleObject(PLU.class, sel);
//            } else {
//                plu = (PLU)getSingleObject(PLU.class,
//                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
//                    " WHERE PLUNO='" + pluNumber + "'");
//            }
//    
//            /*
//            if (cache.size() < MAX_CACHE_SIZE)
//               cache.put(pluNumber, plu);
//            else {
//               cache.clear();
//               cache.put(pluNumber, plu);
//            }
//    
//            */
//            return plu;
//
//        } catch (SQLException e) {
//            CreamToolkit.logMessage(e);
//            return null;
//        } catch (EntityNotFoundException e) {
//            return null;
//        }
//    }

    public boolean equals(Object obj) {
        if (!(obj instanceof PLU))
            return false;
        PLU objCp = (PLU)obj;
        return this.getPluNumber().equals(objCp.getPluNumber());
    }

    public static PLU queryByItemNumber(String itemNumber) {
        return queryByInStoreCode(itemNumber);
    }

    /**
     * 2005-09-13 新增plu cache
     * @param inStoreCode
     * @return
     */
    public static PLU queryByInStoreCode(String inStoreCode) {
        //long time = System.currentTimeMillis();
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            int prefixLength = PARAM.getPadItemNumber();
            int len = inStoreCode.length();
            for (int i = 0; i < prefixLength - len; i++) {
                inStoreCode = "0" + inStoreCode;
            }
            PLU plu = null;
            if (CreamCache.getInstance().isUsePLUCache()) {
                plu = CreamCache.getInstance().getPLUByItemNumber(inStoreCode);
                if (plu != null)
                    return plu;
            }
            if (hyi.cream.inline.Server.serverExist())
                plu = getSingleObject(connection, PLU.class,
                    "select * from " + tableName + " where itemNumber='" + inStoreCode + "'");
            else
                plu = getSinglePojoObject(connection, PLU.class,
                    "select * from " + tableName + " where ITEMNO='" + inStoreCode + "'");
            if (CreamCache.getInstance().isUsePLUCache())
                CreamCache.getInstance().addPLU(CreamCache.BY_ITEMNUMBER, inStoreCode, plu);
            return plu;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
            //System.out.println("PLU.queryByInStoreCode(): "
            //    + (System.currentTimeMillis() - time) + "ms");
        }
    }

    /**
     * 2005-09-13 新增plu cache
     */
    public static PLU queryBarCode(String barCode) {
        if (StringUtils.isEmpty(barCode))
            return null;
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            PLU plu = null;
            if (CreamCache.getInstance().isUsePLUCache()) {
                plu = CreamCache.getInstance().getPLUByBarCode(barCode);
                if (plu != null)
                    return plu;
            }
            if (hyi.cream.inline.Server.serverExist())
                plu = getSingleObject(connection, PLU.class,
                    "select * from " + tableName + " where pluNumber='" + barCode + "'");
            else
                plu = getSingleObject(connection, PLU.class,
                    "select * from " + tableName + " where PLUNO='" + barCode + "'");
            if (CreamCache.getInstance().isUsePLUCache())
                CreamCache.getInstance().addPLU(CreamCache.BY_BARCODE, barCode, plu);
            return plu;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static String[] querySuggestItem(String codePrefix) {
        DbConnection connection = null;
        try {
            List<String> candidates = new ArrayList<String>();
            connection = CreamToolkit.getPooledConnection();

            Statement stat = connection.createStatement();
            ResultSet result = stat
                .executeQuery("SELECT itemno, pluname FROM plu WHERE itemno LIKE '"
                    + codePrefix + "%' LIMIT 10");
            while (result.next()) {
                candidates.add(result.getString(1) + " " + result.getString(2));
            }

//          result = stat
//              .executeQuery("SELECT pluno, pluname FROM plu WHERE pluno LIKE '"
//                  + codePrefix + "%' LIMIT 5");
//          while (result.next()) {
//              candidates.add(result.getString(1) + " " + result.getString(2));
//          }
            result.close();
            stat.close();

            String[] items = new String[candidates.size()];
            return candidates.toArray(items);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new String[0];
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public void setUID(int uid) {
        this.uid = uid;
    }

    public int getUID() {
        return uid;
    }

    /**
     * Get all data for downloading to POS. This method is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Collection dacs = DacBase.getMultipleObjects(connection, hyi.cream.dac.PLU.class,
                "SELECT * FROM posdl_plu", getScToPosFieldNameMap());

            String kindType;
            String signType;
            String openPrice;
            Iterator iter = dacs.iterator();
            while (iter.hasNext()) {
                PLU plu = (PLU)iter.next();
    
                // 1. Determine values of PLUSIGN2, PLUSIGN3 from __KINDTYPE, __SIGNTYPE,
                //    and __OPENPRICE, then add them into fieldMap.
                kindType = (String)plu.getFieldValue("__KINDTYPE");
                signType = (String)plu.getFieldValue("__SIGNTYPE");
                openPrice = (String)plu.getFieldValue("__OPENPRICE");
    
                //if (kindType != null && signType != null && openPrice != null) {
                Integer[] attr = getPluSpecialValue(kindType, signType, openPrice);
                plu.setFieldValue("PLUSIGN2", attr[0]);
                plu.setFieldValue("PLUSIGN3", attr[1]);
    
                // 2. Remove __KINDTYPE, __SIGNTYPE, and __OPENPRICE.
                plu.fieldMap.remove("__KINDTYPE");
                plu.fieldMap.remove("__SIGNTYPE");
                plu.fieldMap.remove("__OPENPRICE");
            }
            return dacs;

        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    static Integer[] getPluSpecialValue(String kindType, String signType, String openPrice) {
        BigInteger attribute1 = new BigInteger("00");

        if (kindType == null && openPrice == null) {

        } else if (openPrice == null) {
            if (kindType.equals("3"))
                attribute1 = attribute1.setBit(0);   // (SC)���用 -> (POS)营业外商品
        } else if (kindType == null){
            if (openPrice.equals("3"))
                attribute1 =  attribute1.setBit(6);  // (SC)磅秤,�格自磅秤�入 -> (POS)磅秤
        } else {
            //cream.plu.attribute1
            //   bit0: set to 1 if cake.plu.kindType = '3'
            //   bit3: set to 1 if cake.plu.openprice = '3'
            if (kindType.equals("3"))
                attribute1 =  attribute1.setBit(0);  // (SC)���用 -> (POS)营业外商品
            if (openPrice.equals("3"))
                attribute1 =  attribute1.setBit(6);  // (SC)磅秤,�格自磅秤�入 -> (POS)磅秤
        }

        //cream.plu.attribute2
        //   bit0: set to 1 if cake.plu.kindType = '1'     特���
        //   bit2: set to 1 if cake.plu.kindType = '2'     �售�用
        //   bit3: set to 0
        //   bit4: set to 1 if cake.plu.signType[0] = 'Y'  认证
        //   bit6: set to 1 if cake.plu.signType[3] = 'Y'  //Bruce/20030324 收银机变价 override amount
        //   bit7: set to 1 if cake.plu.openprice = '2'    零秤,手��格

        BigInteger attribute2 = new BigInteger("0");

        if (kindType == null && signType == null && openPrice == null) {
            ;
        } else {
            if (kindType == null)
                kindType = "";
            if (signType == null)
                signType = "";
            if (openPrice == null)
                openPrice = "";

            if (kindType.equals("1"))
                attribute2 =  attribute2.setBit(0);     // (SC)特��� -> (POS)代收  ??
            else if (kindType.equals("2"))
                attribute2 =  attribute2.setBit(2);     // (SC)�售�用 -> (POS)代付  ??

            if (signType.length() > 0) {
                if (signType.charAt(0) == 'Y' || signType.charAt(0) == 'y' || signType.charAt(0) == '1')
                    attribute2 = attribute2.setBit(4);  // (SC)���性 -> (POS)认证
            }
            //Bruce/2003-06-09
            if (signType.length() >= 10) {
                if (signType.charAt(9) == 'Y' || signType.charAt(9) == 'y' || signType.charAt(9) == '1')
                    attribute2 = attribute2.setBit(5);  // (SC)限量促销商品 -> (POS)限量促销  
            }
            //Bruce/2003-06-09
            if (signType.length() >= 9) {
                if (signType.charAt(8) == 'Y' || signType.charAt(8) == 'y' || signType.charAt(8) == '1')
                    attribute2 = attribute2.setBit(6);  // (SC)可手动变价Override Amount -> (POS)可手动变价
            }

            if (openPrice.equals("2"))
                attribute2 =  attribute2.setBit(7);     // (SC)零秤,手��格 -> (POS)开放价格

            //Allan/2004-11-05
            if (signType.length() >= 14) {
                if (signType.charAt(13) == 'Y' || signType.charAt(13) == 'y' || signType.charAt(13) == '1')
                    attribute2 = attribute2.setBit(1);  // (SC)可否打折 discount -> (POS)可手动打折
            }

        }
        Integer [] attr = new Integer[2];
        attr[0] = new Integer(attribute1.intValue());
        attr[1] = new Integer(attribute2.intValue());
        return attr;
    }

    public static void importSomePLUs(DbConnection connection, Object[] plus) throws SQLException {
        if (!hyi.cream.inline.Server.serverExist()) {
            for (int i = 0; i < plus.length; i++) {
                PLU plu = (PLU)plus[i];

                //Meyer/2003-02-19
                plu.fieldMap.remove("PLUPRICECHANGEID");

                //Bruce/2005-10-14 否则下面的update不work
                plu.setAllFieldsDirty(true);

                if (plu.exists(connection))
                    try {
                        plu.update(connection);
                    } catch (EntityNotFoundException e) {
                        throw new SQLException(e.toString());
                    }
                else
                    plu.insert(connection);
            }
        }
    }

    /**
     * 如果在特卖期间，则返回特卖价，否则返回null。
     * 
     * @return 特卖价 or null.
     */
    public HYIDouble getSpecialPriceIfWithinTimeLimit() {
        HYIDouble specialPrice = this.getSpecialPrice();

        if (specialPrice != null && specialPrice.compareTo(new HYIDouble(0)) > 0) {

            //Bruce/20080625/ Rewrite the logic for special price retrieval

            DateFormat df = CreamCache.getInstance().getDateFormate();
            DateFormat tf = CreamCache.getInstance().getTimeFormate();

            Date now = new Date();
            String dateNow = df.format(now);
            String timeNow = tf.format(now);

            String discStartDate = df.format(getDiscountStartDate());
            String discEndDate = df.format(getDiscountEndDate());

            String discStartTime = tf.format(getDiscountStartTime());
            String discEndTime = tf.format(getDiscountEndTime());
            
            if (discStartDate.compareTo(dateNow) <= 0 && dateNow.compareTo(discEndDate) <= 0 &&
                discStartTime.compareTo(timeNow) <= 0 && timeNow.compareTo(discEndTime) <= 0)
                return specialPrice;
        }
        return null;
            
//            java.sql.Date curDate = java.sql.Date.valueOf(df.format(now));
//            java.sql.Time curTime = java.sql.Time.valueOf(tf.format(now));
//
//            java.sql.Date discStartDate = this.getDiscountStartDate();
//            java.sql.Date discEndDate = this.getDiscountEndDate();
//
//            
//            if (discStartDate != null && discEndDate != null
//                && curDate.compareTo(discStartDate) >= 0 && curDate.compareTo(discEndDate) <= 0) {
//                
//                java.sql.Time discStartTime = this.getDiscountStartTime();
//                java.sql.Time discEndTime = this.getDiscountEndTime();
//                if (discStartTime == null ||  
//                    discEndTime == null ||
//                    (curTime.compareTo(discStartTime) >= 0 && curTime.compareTo(discEndTime) <= 0)
//                    ) {
//                    return specialPrice;
//                }
//            }
//        }
//        return null;   
    }

    /**
     * 取得单品售价。
     * 
     * <P><PRE>Decision table for determining saling price:
     *
     *    会员否   OnlyMemberCanUseSpecialPrice 特卖期间   使用售价
     *    -------  ---------------------------- -------   -------
     *      no              no                  no        unitPrice
     *      no              yes                 no        unitPrice
     *      yes             no                  no        memberPrice
     *      yes             yes                 no        memberPrice
     *      no              yes                 yes       unitPrice
     *      no              no                  yes       specialPrice
     *      yes             no                  yes       specialPrice
     *      yes             yes                 yes       specialPrice
     * </PRE>
     * 
     * @return Return a Object array with two elements, first is the saling price by a HYIDouble
     * object, the second is a String to specify what price it is used: "UNIT_PRICE": 正常单价，
     * "MEMBER_PRICE": 会员价，"SPECIAL_PRICE": 特卖价，"ORDER_PRICE": 预订价.
     */
    public Object[] getSalingPriceDetail() {
        Object[] salingPriceDetail = new Object[2];

        // 期刊预订时只能取预订价,如果预订价=null,则走正常流程
        CreamSession session = CreamSession.getInstance();
        if (session.getWorkingState() == WorkingStateEnum.PERIODICAL_ORDER_STATE
                || session.getWorkingState() == WorkingStateEnum.PERIODICAL_DRAW_STATE
                || session.getWorkingState() == WorkingStateEnum.PERIODICAL_RETURN_STATE)
        {
            salingPriceDetail[1] = "ORDER_PRICE";
            salingPriceDetail[0] = this.getCustordprice1();
            if (salingPriceDetail[0] != null)
                return salingPriceDetail;
        }

        boolean onlyMemberCanUseSpecialPrice = PARAM.getOnlyMemberCanUseSpecialPrice();
        String memberID = POSTerminalApplication.getInstance().getCurrentTransaction().getMemberID();
        boolean isMember = (memberID != null && memberID.length() > 0);

        HYIDouble salingPrice;
        HYIDouble unitPrice = this.getUnitPrice();
        salingPrice = unitPrice;
        salingPriceDetail[1] = "UNIT_PRICE";

        if (!PARAM.getMemberUseRegularPrice()) {
            HYIDouble memberPrice = this.getMemberPrice(GetMemberNumberState.getMember());
            if (isMember && memberPrice != null
                    && (unitPrice == null || memberPrice.compareTo(unitPrice) < 0)) {
                // 取比较低的价格
                salingPrice = memberPrice;
                salingPriceDetail[1] = "MEMBER_PRICE";
            }
        }

        if (!onlyMemberCanUseSpecialPrice ||
            (onlyMemberCanUseSpecialPrice && isMember)) 
        {
            HYIDouble sp = getSpecialPriceIfWithinTimeLimit();
            if (sp != null && salingPrice != null
                    && (salingPrice == null || sp.compareTo(salingPrice) < 0)) {
                // 取比较低的价格
                salingPrice = sp;
                salingPriceDetail[1] = "SPECIAL_PRICE";
            }
        }
        
        salingPriceDetail[0] = (salingPrice == null) ? new HYIDouble(0.00) : salingPrice;
        return salingPriceDetail;
    }
    
    /**
     * get pluPriceChangeID
     */
    public String getPluPriceChangeID() {
        return (String)getFieldValue("PLUPRICECHANGEID");
    }
    
    /**
     * 获得最低价[for 灿坤]
     */
    public HYIDouble getMinPrice() {
        return (HYIDouble)getFieldValue("MINPRICE");
    }
    
    /**
     * Meyer/2003-02-20
     * return fieldName map of PosToSc as Map
     */
    public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("pluNumber", "PLUNO");
        fieldNameMap.put("pluName", "PLUNAME");
        fieldNameMap.put("pluEnglishName", "PRINTNAME");
        fieldNameMap.put("itemNumber", "ITEMNO");
        fieldNameMap.put("shipNumber", "SHIPNO");
        fieldNameMap.put("taxID", "PLUTAX");
        fieldNameMap.put("storeUnitPrice", "PLUPRICE");
        fieldNameMap.put("openPrice", "__OPENPRICE");
        fieldNameMap.put("specialPrice", "PLUPMPRICE");
        fieldNameMap.put("specialBeginDate", "PLUPMDATES");
        fieldNameMap.put("specialEndDate", "PLUPMDATEE");
        fieldNameMap.put("specialBeginTime", "PLUPMTIMES");
        fieldNameMap.put("specialEndTime", "PLUPMTIMEE");
        fieldNameMap.put("memberPrice", "PLUMBPRICE");
        fieldNameMap.put("couponPrice1", "PLUOPPRICE1");
        fieldNameMap.put("couponPrice2", "PLUOPPRICE2");
        fieldNameMap.put("discountBeginDate", "DISCBEGDT");
        fieldNameMap.put("discountEndDate", "DISCENDDT");
        fieldNameMap.put("siGroup", "PLUSIGN1");
        fieldNameMap.put("categoryNumber", "CATNO");
        fieldNameMap.put("midCategoryNumber", "MIDCATNO");
        fieldNameMap.put("microCategoryNumber", "MICROCATNO");
        fieldNameMap.put("thinCategoryNumber", "THINCATNO");
        fieldNameMap.put("kindType", "__KINDTYPE");
        fieldNameMap.put("signType", "__SIGNTYPE");
        fieldNameMap.put("mixAndMatchNumber", "MAMNO");
        fieldNameMap.put("depID", "depID");
        fieldNameMap.put("specification", "SPECIFICATION");
        fieldNameMap.put("smallUnit", "SMALLUNIT");
        fieldNameMap.put("invCycleNo", "INVCYCLENO");
        fieldNameMap.put("sizeCategory", "SIZECATEGORY");
        fieldNameMap.put("itemBrand", "ITEMBRAND");
        fieldNameMap.put("size", "SIZE");
        fieldNameMap.put("color", "COLOR");
        fieldNameMap.put("style", "STYLE");
        fieldNameMap.put("season", "SEASON");
        if (GetProperty.getDownloadMinPrice("no").equalsIgnoreCase("yes"))     //OverrideUsePlu"))
            fieldNameMap.put("minPrice", "MINPRICE");
        if (GetProperty.getOverrideVersion("1").equals("2"))
            fieldNameMap.put("yellowLimit", "YELLOWLIMIT");
        fieldNameMap.put("saleType", "SALETYPE");
        
        fieldNameMap.put("itemtype", "ITEMTYPE");
        fieldNameMap.put("magaType", "MAGATYPE");
        fieldNameMap.put("magaCount", "MAGACOUNT");
        fieldNameMap.put("custOrdType", "CUSTORDTYPE");
        fieldNameMap.put("custOrdPrice1", "CUSTORDPRICE1");
        fieldNameMap.put("isOrd", "ISORD");
        return fieldNameMap;
    }

    /**
     * Give a field list by table created sequence. Used by DacBase.getInsertSqlStatement().
     * 
     * @return Iterator represented a field list.
     */
    public Iterator getSequentialFieldList() {

        if (sequentialFieldList == null) {
            String[] f = {
                "PLUNO",
                "ITEMNO",
                "SHIPNO",
                "PLUNAME",
                "PRINTNAME",
                "CATNO",
                "MIDCATNO",
                "MICROCATNO",
                "PLUPRICE",
                "PLUPMPRICE",
                "PLUPMDATES",
                "PLUPMDATEE",
                "PLUPMTIMES",
                "PLUPMTIMEE",
                "PLUMBPRICE",
                "PLUOPPRICE1",
                "PLUOPPRICE2",
                "PLUSIGN1",
                "PLUSIGN2",
                "DISCBEGDT",
                "DISCENDDT",
                "PLUSIGN3",
                "PLUTAX",
                "MAMNO",
                "DEPID",
            };
            ArrayList list2 = new ArrayList(Arrays.asList(f));

            if (isFieldExist("thinCategoryNumber")) {
                list2.add(list2.indexOf("MICROCATNO") + 1, "THINCATNO");
            }
            //Bruce/20030413
            // Add "specification" for 灿坤
            if (isFieldExist("SPECIFICATION"))      // server-side field name
                list2.add("SPECIFICATION");         // client-side field name

            if (isFieldExist("SMALLUNIT"))
                list2.add("SMALLUNIT");
            if (isFieldExist("INVCYCLENO"))
                list2.add("INVCYCLENO");
            if (isFieldExist("SIZECATEGORY"))
                list2.add("SIZECATEGORY");
            if (isFieldExist("ITEMBRAND"))
                list2.add("ITEMBRAND");
            if (isFieldExist("SIZE"))
                list2.add("SIZE");
            if (isFieldExist("COLOR"))
                list2.add("COLOR");
            if (isFieldExist("STYLE"))
                list2.add("STYLE");
            if (isFieldExist("SEASON"))
                list2.add("SEASON");
            if (isFieldExist("MINPRICE"))
                list2.add("MINPRICE");
            if (isFieldExist("SALETYPE"))
                list2.add("SALETYPE");
            if (isFieldExist("YELLOWLIMIT"))
                list2.add("YELLOWLIMIT");

            // for 期刊
            if (isFieldExist("ITEMTYPE")) //商品类型
                list2.add("ITEMTYPE");
            if (isFieldExist("MAGATYPE")) //期刊类型
                list2.add("MAGATYPE");
            if (isFieldExist("MAGACOUNT")) //一年期数
                list2.add("MAGACOUNT");
            if (isFieldExist("CUSTORDTYPE")) //期刊预订方式
                list2.add("CUSTORDTYPE");
            if (isFieldExist("CUSTORDPRICE1")) //预订单价
                list2.add("CUSTORDPRICE1");
            if (isFieldExist("ISORD")) //是否可预定
                list2.add("ISORD");

            sequentialFieldList = list2;
        }
        return sequentialFieldList.iterator();
    }


    /**
     * 是否为open price?
     */
    public boolean isOpenPrice() {
        BigInteger attrib = new BigInteger (String.valueOf(getAttribute2()));
        return attrib.testBit(7);
    }

    /**
     * 是否为限量促销商品？
     */
    public boolean isQuantityLimited() {
        BigInteger attrib = new BigInteger (String.valueOf(getAttribute2()));
        return attrib.testBit(5);
    }
    
    /**
     * 是否可以override amount?
     */
    public boolean canOverrideAmount() {
        if (hyi.cream.inline.Server.serverExist()) {
            return false;
        } else {
            BigInteger attrib = new BigInteger(String.valueOf(getAttribute2()));
            return attrib.testBit(6);
        }
    }
    
    /**
     * 是否可以discount amount/rate? 永亿
     */
    public boolean canDiscount() {
        if (hyi.cream.inline.Server.serverExist()) {
            return false;
        } else {
            BigInteger attrib = new BigInteger(String.valueOf(getAttribute2()));
            return attrib.testBit(1);
        }
    }
    
    /**
     * 是否为秤重商品？
     */
    public boolean isWeightedPlu() {
        if (!hyi.cream.inline.Server.serverExist()) {
            BigInteger attrib = new BigInteger(String.valueOf(getAttribute1()));
            return attrib.testBit(6);
        } else
            return false;
    }

    /**
     * 是否为资金商品？
     */
    public boolean isCashFlowPlu() {
        if (!hyi.cream.inline.Server.serverExist()) {
            BigInteger attrib = new BigInteger(String.valueOf(getAttribute1()));
            return attrib.testBit(5);
        } else
            return false;
    }
    
    /**
        saleType char[1]
        '0' or Null :可组买也可单买 [default]
        '1' :只可单买
        '2' :只可组买
     * @return
     */
    public String getSaleType() {
        return (String)getFieldValue("SALETYPE");
    }
    
    public void setSaleType(String saleType) {
        setFieldValue("SALETYPE", saleType);
    }

    public HYIDouble getYellowLimit() {
        if (hyi.cream.inline.Server.serverExist())
            return (HYIDouble)getFieldValue("YELLOWLIMIT");
        else
            return (HYIDouble)getFieldValue("YELLOWLIMIT");
    }

    /**
     * 商品类型    1普通商品、10期刊
     * @return
     */
    public Integer getItemtype() {
        return (Integer)getFieldValue("ITEMTYPE");
    }
    
    public void setItemtype(Integer itemtype) {
        setFieldValue("ITEMTYPE", itemtype);
    }

    /**
     * 期刊类型    参照字典2119
     * @return
     */
    public Integer getMagatype() {
        return (Integer)getFieldValue("MAGATYPE");
    }
    
    public void setMagatype(Integer magatype) {
        setFieldValue("MAGATYPE", magatype);
    }

    /**
     * 一年期数    用户输入
     * @return
     */
    public Integer getMagacount() {
        return (Integer)getFieldValue("MAGACOUNT");
    }
    
    public void setMagacount(Integer magacount) {
        setFieldValue("MAGACOUNT", magacount);
    }

    /**
     * 期刊预订方式    参照字典2120
     * @return
     */
    public Integer getCustordtype() {
        return (Integer)getFieldValue("CUSTORDTYPE");
    }
    
    public void setCustordtype(Integer custordtype) {
        setFieldValue("CUSTORDTYPE", custordtype);
    }

    /**
     * 预订单价    一本期刊只对应一个预订价，总部下发
     * @return
     */
    public HYIDouble getCustordprice1() {
        return (HYIDouble)getFieldValue("CUSTORDPRICE1");
    }
    
    public void setCustordprice1(HYIDouble custordprice1) {
        setFieldValue("CUSTORDPRICE1", custordprice1);
    }

    /**
     * 是否可预定    N：不可 Y：可以
     * @return
     */
    public String getIsord() {
        return (String)getFieldValue("ISORD");
    }
    
    public void setIsord(String isord) {
        setFieldValue("ISORD", isord);
    }

    @Override
    protected Map getDatabaseFieldToBeanPropertyMap()
    {
        return null;
    }

//    public static void main(String[] args) {
//        Date curDate = new Date();
//        java.sql.Time curTime = java.sql.Time.valueOf(
//            new java.text.SimpleDateFormat("HH:mm:ss").format(curDate));
//
//        java.sql.Date date1 = java.sql.Date.valueOf(args[0]);
//        java.sql.Time time1 = java.sql.Time.valueOf(args[1]);
//
//        System.out.println(curDate);
//        System.out.println(curDate.after(date1));
//        System.out.println(date1);
//
//        System.out.println(curTime);
//        System.out.println(curTime.after(time1));
//        System.out.println(time1);
//    }
}


