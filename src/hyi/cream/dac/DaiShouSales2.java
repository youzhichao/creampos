package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author Administrator
 */
public class DaiShouSales2 extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("StoreID");
        primaryKeys.add("AccountDate");
		primaryKeys.add("PosNumber");
		primaryKeys.add("TransactionNumber");
        primaryKeys.add("LineItemSeq");
		primaryKeys.add("PayTime");
	}

	public DaiShouSales2() {

	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (hyi.cream.inline.Server.serverExist())
			return "posul_daishousales2";
		else
			return "daishousales2";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (hyi.cream.inline.Server.serverExist())
			return "posul_daishousales2";
		else
			return "daishousales2";
	}

	public static Iterator getCurrentDaishouSales(DbConnection connection, int znumber) {
		try {
            return getMultipleObjects(connection, DaiShouSales2.class,
            	"SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
            	" WHERE ZSequenceNumber = " + znumber);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}

    /**
     * only used at the sc side, do nothing for pos side 
     */
    public static void deleteBySequenceNumber(DbConnection connection, int zNumber, int posNumber) throws SQLException {
        // if (!hyi.cream.inline.Server.serverExist())
        // return;
        String deleteSql = "DELETE FROM " + getInsertUpdateTableNameStaticVersion() 
                          + " WHERE zSequenceNumber =" + zNumber 
                          + " AND posNumber = " + posNumber;    
        System.out.println("Delete daishousales2 sql : " + deleteSql);
        executeQuery(connection, deleteSql);
    }

	public static Iterator getUploadFailedList(DbConnection connection) {
		try {
		    String failFlag = "2";
            return getMultipleObjects(connection, DaiShouSales2.class,
            	"SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
            	" WHERE uploadState='" + failFlag + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}

    public static DaiShouSales2 queryDaiShouSales(DbConnection connection, int tranNo, int seqNo) {
        try {
            return getSingleObject(connection, DaiShouSales2.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
                " WHERE TransactionNumber = " + tranNo + " AND LineItemSeq = " + seqNo);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static DaiShouSales2 queryDaiShouSales(DbConnection connection, int tranNo, String barcode) {
        try {
            return getSingleObject(connection, DaiShouSales2.class, "SELECT * FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE TransactionNumber = " + tranNo
                + " AND Barcode = " + barcode);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    /**
     * Clone DaishouSales2 objects for SC
     * 
     * This method is only used at POS side.
     */
	public static Object[] cloneForSC(Iterator iter) {
		String[][] fieldNameMap = getPosToScFieldNameArray();

		ArrayList objArray = new ArrayList();
		while (iter.hasNext()) {
			DaiShouSales2 ds = (DaiShouSales2)iter.next();
			//	System.out.println("daishouid1=" + ds.getFirstNumber() + ", daishouid2=" + ds.getSecondNumber());
			DaiShouSales2 clonedDS = new DaiShouSales2();
			for (int i = 0; i < fieldNameMap.length; i++) {
				Object value = ds.getFieldValue(fieldNameMap[i][0]);
				if (value == null) {
					try {
						value = ds.getClass().getDeclaredMethod(
							"get" + fieldNameMap[i][0], new Class[0]).invoke(ds, new Object[0]);
					} catch (Exception e) {
						value = null;
					}
				}
				clonedDS.setFieldValue(fieldNameMap[i][1], value);
			}
			objArray.add(clonedDS);
		}
		return objArray.toArray();
	}

	/**
	 * return fieldName map of PosToSc as String[][] 
	 */
	public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
			{"StoreID", "StoreID"},
			{"PosNumber", "PosNumber"},
            {"TransactionNumber","TransactionNumber"},
            {"LineItemSeq","LineItemSeq"},
			{"ZSequenceNumber", "ZSequenceNumber"},
			{"AccountDate", "AccountDate"},
			{"PayTime", "PayTime"},
			{"ID", "ID"},
			{"Barcode", "Barcode"},
			{"PosAmount", "PosAmount"},
			{"ScAmount", "ScAmount"}
		};
	}
	
	
	/***********  getters and setters  *****************/
	public void setStoreID(String storeID) {
		setFieldValue("StoreID", storeID);
	}

	public String getStoreID() {
		return ((String) getFieldValue("StoreID"));
	}

	public void setPosNumber(int posnumber) {
		setFieldValue("PosNumber", new Integer(posnumber));
	}

	public int getPosNumber() {
		return ((Integer) getFieldValue("PosNumber")).intValue();
	}

    public void setTransactionNumber(int tranNo) {
        setFieldValue("TransactionNumber", new Integer(tranNo));

    }
    
    public int getTransactionNumber() {
        return ((Integer) getFieldValue("TransactionNumber")).intValue();
    }
    
    public void setLineItemSeq(int lineSeq) {
        setFieldValue("LineItemSeq", new Integer(lineSeq));

    }

    public int getLineItemSeq() {
        return ((Integer) getFieldValue("LineItemSeq")).intValue();
    }
    
	public void setZSequenceNumber(int znumber) {
		setFieldValue("ZSequenceNumber", new Integer(znumber));
	}

	public int getZSequenceNumber() {
		return ((Integer) getFieldValue("ZSequenceNumber")).intValue();
	}

	public String getID() {
		return (String) getFieldValue("ID");
	}

	public void setID(String id) {
		setFieldValue("ID", id);
	}

	public Date getAccountDate() {
		return (Date) getFieldValue("AccountDate");
	}

	public void setAccountDate(Date accountDate) {
		setFieldValue("AccountDate", accountDate);
	}

	public Date getPayTime() {
		return (Date) getFieldValue("PayTime");
	}

	public void setPayTime(Date payTime) {
		setFieldValue("PayTime", payTime);
	}

	public String getBarcode() {
		return (String) getFieldValue("Barcode");
	}

	public void setBarcode(String code) {
		setFieldValue("Barcode", code);
	}

	public HYIDouble getPosAmount() {
		return (HYIDouble) getFieldValue("PosAmount");
	}

	public void setPosAmount(HYIDouble posAmount) {
		setFieldValue("PosAmount", posAmount);
	}

	public HYIDouble getScAmount() {
		return (HYIDouble) getFieldValue("ScAmount");
	}

	public void setScAmount(HYIDouble scAmount) {
		setFieldValue("ScAmount", scAmount);
	}

	public void setUploadState(String uploadState) {
		setFieldValue("uploadState", uploadState);
	}

	public String getUploadState() {
		return (String)getFieldValue("uploadState");
	}

	public static void main(String[] args) {
	}
}
