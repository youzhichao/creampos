package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Transaction detail hold DAC class.
 * 
 * @version 1.0
 */
public class LineItemHold extends LineItem {
    private static final long serialVersionUID = 1L;

	public LineItemHold() {
		super();
	}

	public LineItemHold(LineItem li) throws InstantiationException {
		this();
		this.fieldMap.putAll(li.getFieldMap());
	}

	public String toString() {
		return "LineItemHold (pos=" + this.getTerminalNumber() + ",no="
				+ this.getTransactionNumber() + ",seq="
				+ this.getLineItemSequence() + ",unitprice="
				+ this.getUnitPrice() + ",qty=" + this.getQuantity() + ",amt="
				+ this.getAmount() + ",aftamt=" + this.getAfterDiscountAmount()
				+ ")";
	}

	public static void deleteByPosNO(DbConnection connection, int posNO) throws SQLException {
		executeQuery(connection, "DELETE FROM " + getInsertUpdateTableNameStaticVersion());
	}

	public static void deleteByTranNumber(DbConnection connection, int tranNumber) throws SQLException {
		executeQuery(connection, "DELETE FROM " + getInsertUpdateTableNameStaticVersion()
				+ " WHERE TMTRANSEQ = " + tranNumber);
	}
	
	public static Iterator queryByTransactionNumber(DbConnection connection, String transactionNumber) {
        try {
    		Iterator itr = getMultipleObjects(connection, LineItem.class, "SELECT * FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE TMTRANSEQ='"
                + transactionNumber + "'");
    
    		ArrayList newArrayList = new ArrayList();
    		if (itr == null)
    			return newArrayList.iterator();
    		while (itr.hasNext()) {
    			LineItem lineItem = (LineItem) itr.next();
    			PLU plu = PLU.queryBarCode(lineItem.getPluNumber());
    			if (plu == null) {
    				if (lineItem.getPluNumber() != null) {
    					if (lineItem.getPluNumber().length() > 0) {
    						// '08'：代售项目 '09'：代付项目'10'：PaidIn项目'11'：PaidOut项目
    						String cat = "";
    						// 代售
    						if (lineItem.getDetailCode().equals("I"))
    							cat = "08";
    						else if (lineItem.getDetailCode().equals("O")) {
    							// 公共事业费代收
    							DaiShouSales2 dss = DaiShouSales2.queryDaiShouSales(connection,
                                    lineItem.getTransactionNumber().intValue(),
    								lineItem.getLineItemSequence());
    							if (dss != null) {
    								DaiShouDef dsd = DaiShouDef.queryByID(connection, dss.getID());
    								lineItem.setDescriptionAndSpecification(dsd.getName());
    								lineItem.setDescription(dsd.getName());
    								lineItem.setDaiShouBarcode(dss.getBarcode());
    								lineItem.setDaiShouID(dss.getID());
    							}
    						} else if (lineItem.getDetailCode().equals("Q"))
    							cat = "09";
    						else if (lineItem.getDetailCode().equals("N"))
    							cat = "10";
    						else if (lineItem.getDetailCode().equals("T"))
    							cat = "11";
    						Reason r = Reason.queryByreasonNumberAndreasonCategory(
    								lineItem.getPluNumber(), cat);
    						if (r != null)
    							lineItem.setDescription(r.getreasonName());
    					} else {
    						/*
    						 * TaxType t =
    						 * TaxType.queryByTaxID(lineItem.getTaxType()); if (t !=
    						 * null) { lineItem.setDescription(t.getTaxName()); }
    						 */
    						Dep dep = Dep.queryByDepID(lineItem
    								.getMicroCategoryNumber());
    						if (dep != null) {
    							lineItem.setDescription(dep.getDepName());
    						}
    					}
    				}
    			} else {
    				String name = PARAM.getPluPrintField();
    				if (name.equals("pluname"))
    					lineItem.setDescription(plu.getScreenName());
    				else
    					lineItem.setDescription(plu.getPrintName());
    			}//
    			newArrayList.add(lineItem);
    		}
    		return newArrayList.iterator();

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}

	public static Iterator queryByTransactionNumber(DbConnection connection, int posNumber,
			String transactionNumber) {
	    try {
    		Iterator itr = null;
    		if (hyi.cream.inline.Server.serverExist())
    			itr = getMultipleObjects(connection, LineItem.class, "SELECT * FROM "
    					+ getInsertUpdateTableNameStaticVersion()
    					+ " WHERE PosNumber=" + posNumber
    					+ " AND TransactionNumber=" + transactionNumber);
    		else
    			itr = getMultipleObjects(connection, LineItem.class, "SELECT * FROM "
    					+ getInsertUpdateTableNameStaticVersion()
    					+ " WHERE TMTRANSEQ='" + transactionNumber + "'");
    
    		ArrayList newArrayList = new ArrayList();
    		if (itr == null)
    			return newArrayList.iterator();
    		while (itr.hasNext()) {
    			LineItem lineItem = (LineItem) itr.next();
    			PLU plu = PLU.queryBarCode(lineItem.getPluNumber());
    			if (plu == null) {
    				if (lineItem.getPluNumber() != null) {
    					if (lineItem.getPluNumber().length() > 0) {
    						// '08'：代售项目'09'：代付项目'10'：PaidIn项目'11'：PaidOut项目
    						String cat = "";
    						// 代售
    						if (lineItem.getDetailCode().equals("I"))
    							cat = "08";
    						else if (lineItem.getDetailCode().equals("O")) {
    							// 公共事业费代收
    							DaiShouSales2 dss = DaiShouSales2.queryDaiShouSales(
                                    connection, lineItem.getTransactionNumber().intValue(),
    								lineItem.getLineItemSequence());
    							if (dss != null) {
    								DaiShouDef dsd = DaiShouDef.queryByID(connection, dss.getID());
    								lineItem.setDescriptionAndSpecification(dsd.getName());
    								lineItem.setDescription(dsd.getName());
    								lineItem.setDaiShouBarcode(dss.getBarcode());
    								lineItem.setDaiShouID(dss.getID());
    							}
    						} else if (lineItem.getDetailCode().equals("Q"))
    							cat = "09";
    						else if (lineItem.getDetailCode().equals("N"))
    							cat = "10";
    						else if (lineItem.getDetailCode().equals("T"))
    							cat = "11";
    						Reason r = Reason.queryByreasonNumberAndreasonCategory(
    								lineItem.getPluNumber(), cat);
    						if (r != null)
    							lineItem.setDescription(r.getreasonName());
    					} else {
    						Category cat = Category.queryByCategoryNumber(lineItem.getCategoryNumber(), 
    								lineItem.getMidCategoryNumber(), lineItem.getMicroCategoryNumber());
    						if (cat != null) {
    							lineItem.setDescription(cat.getScreenName());
    						} else {
	    						TaxType t = TaxType.queryByTaxID(lineItem.getTaxType());
	    						if (t != null) {
	    							lineItem.setDescription(t.getTaxName());
	    						}
    						}
    					}
    				}
    			} else {
    				String name = PARAM.getPluPrintField();
    				if (name.equals("pluname"))
    					lineItem.setDescription(plu.getScreenName());
    				else
    					lineItem.setDescription(plu.getPrintName());
    			}
    			newArrayList.add(lineItem);
    		}
    		return newArrayList.iterator();

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}

	public LineItemHold(int i) throws InstantiationException {
		// constructor for doing nothing
	}

	public String getInsertUpdateTableName() {
		return "trandetailhold";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		return "trandetailhold";
	}
}
