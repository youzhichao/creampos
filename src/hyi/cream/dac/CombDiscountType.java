package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * CombDiscountType definition class.
 *
 * @author Meyer 
 * @version 1.0
 */
public class CombDiscountType extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**      
     * 更新记录
     *
     * Meyer / 1.0 / 2003-01-02
     * 		1. Create the class file
     * 
     */
    transient public static final String VERSION = "1.0";

    static final String tableName = "combination_discount_type";
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("CDT_ID");
    }

    /**
     * Constructor
     */
    public CombDiscountType() {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static CombDiscountType queryByID(DbConnection connection, String cdtID) {
        try {
            return getSingleObject(connection, CombDiscountType.class,
                " SELECT * FROM " + tableName + 
                " WHERE CDT_ID='" + cdtID + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public String getCdtID() {
        return (String)getFieldValue("CDT_ID");
    }

    public String getCdtName() {
        return (String)getFieldValue("CDT_NAME");
    }   

	/**
	 * Meyer/2003-02-21/Modified
	 * 优先级别值为字符整数,数值越小优先级别越高
	 * 		1>2>3>4>5>6>7...
	 * 
	 */
    public String getPriority() {
        return (String)getFieldValue("CDT_PRIORITY");
    }

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
	        Map fieldNameMap = null;			      
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.CombDiscountType.class,
            	"SELECT * FROM posdl_combination_discount_type", fieldNameMap);
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}