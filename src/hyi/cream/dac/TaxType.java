package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * TaxType definition class.
 *
 * @author Dai, Bruce
 * @version 1.6
 */
public class TaxType extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * /Bruce/1.6/2002-05-08/
     *    修改后台小数位数字段名为 "numberCount". 解决POS端小数位数字段值不正确的问题.
     *
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public static Collection getAllObjectsForPOS()
     */
    transient public static final String VERSION = "1.6";

    static final String tableName = "taxtype";
    private static ArrayList primaryKeys = new ArrayList();
    transient private static Set cache;

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    static {
        primaryKeys.add("TAXID");
        if (!hyi.cream.inline.Server.serverExist())
            createCache();
    }

    public static void createCache() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Iterator itr = getMultipleObjects(connection, TaxType.class, "SELECT * FROM " + tableName);
            cache = new HashSet();
            while (itr.hasNext()) {
                cache.add(itr.next());
            }
        } catch (EntityNotFoundException e) {
            cache = null;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            cache = null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public boolean equals(Object obj) {
        if ( !(obj instanceof TaxType))
            return false;
        return getTaxID().equals(((TaxType)obj).getTaxID());
    }

    public static TaxType queryByTaxID (String taxID) {
        //System.out.println("query tax id = " + taxID);
        if (cache == null) {
            //System.out.println("cache = null");
            return null;
        }
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            TaxType t = (TaxType)itr.next();
            //System.out.println("tax id = " + t.getTaxID());
            if (t.getTaxID().equals(taxID)){
                //System.out.println("return " + t);
                return t;
            }
        }
        //System.out.println("return null");
        return null;
        /*return (TaxType)getSingleObject(TaxType.class,
            "SELECT * FROM " + tableName + " WHERE TAXID='" + taxID + "'");//*/
    }

    public static boolean getTaxedType(String id) {
        TaxType t = TaxType.queryByTaxID(id);
        if (t != null) {
           if (!t.getType().equals("0"))
              return true;
        }
        return false;
    }

    public TaxType() {
    }

    //Get properties values:
    //TaxID	TAXID	CHAR(1)	N
    public String getTaxID() {
        return (String)getFieldValue("TAXID");
    }
    //Type	TYPE	CHAR(1)	N
    public String getType() {
        return (String)getFieldValue("TYPE");
    }

    //TAXNM
    public String getTaxName() {
         return (String)getFieldValue("TAXNM");
    }

    public void setTaxName(String name) {
         setFieldValue("TAXNM", name);
    }


    //Round	ROUND	CHAR(1)	N
    public String getRound() {
        return (String)getFieldValue("ROUND");
    }
    //Percent	PERCENT	DECIMAL(5,2)	N
    public HYIDouble getPercent() {
        return (HYIDouble)getFieldValue("PERCENT");
    }

    public Integer getDecimalDigit() {
        return (Integer)getFieldValue("DECIMALCNT");
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static Iterator queryByTaxType(DbConnection connection, String taxType) {
        try {
            return getMultipleObjects(connection, TaxType.class,
                "SELECT * FROM TAXTYPE WHERE TYPE='" + taxType + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Map fieldNameMap = new HashMap();
            fieldNameMap.put("taxID", "TAXID");
            fieldNameMap.put("taxType", "TYPE");
            fieldNameMap.put("taxName", "TAXNM");
            fieldNameMap.put("roundType", "ROUND");
            fieldNameMap.put("numberCount", "DECIMALCNT");
            fieldNameMap.put("taxRate", "PERCENT");
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.TaxType.class,
                "SELECT * FROM posdl_taxtype", fieldNameMap);
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}
