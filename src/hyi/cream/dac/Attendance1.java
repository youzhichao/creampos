/*
 * Created on 2005-3-15
 * for quanjia WorkCheck(考勤系统)
 * 
 */
package hyi.cream.dac;

import hyi.cream.POSTerminalApplication;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.InOutNumberState;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Administrator
 */
public class Attendance1 extends DacBase implements Serializable {

    private static final long serialVersionUID = 1L;

	private static ArrayList primaryKeys = new ArrayList();

	static {
        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("ID");
            primaryKeys.add("storeID");
            primaryKeys.add("posNumber");
            primaryKeys.add("busiDate");
            primaryKeys.add("dayDate");
            primaryKeys.add("employeeID");
            primaryKeys.add("attType");
//            primaryKeys.add("createDate");
        } else {
//            primaryKeys.add("ID");
            primaryKeys.add("storeID");
            primaryKeys.add("dayDate");
            primaryKeys.add("employeeID");
            primaryKeys.add("attType");
            primaryKeys.add("createDate");
        }
	}

	public Attendance1() {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (hyi.cream.inline.Server.serverExist())
			return "posul_attendance";
		else
			return "attendance";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (hyi.cream.inline.Server.serverExist())
			return "posul_attendance";
		else
			return "attendance";
	}

    //检查是否已经上班考勤 或者下班考勤了.
    public static boolean existInorOut(String empoyeeId, String attType) {
        boolean isInOut = false;
        Transaction trans = POSTerminalApplication.getInstance().getCurrentTransaction();
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            //员工考勤上班时 ,检查数据库是否已经考勤过上班.
            Date currentDateTime = new Date();
            if ("01".equals(attType)) {
                Attendance1 att = getSingleObject(connection, Attendance1.class, "SELECT * FROM "
                        + getInsertUpdateTableNameStaticVersion() +
                        " where employeeID = '" + empoyeeId + "' and attType = '01' order by id desc limit 1");
                if (att != null) {
                    Date delay20hourDateTime = new Date((att.getBeginTime()).getTime() + (20 * 60 * 60 * 1000L));
                    if (currentDateTime.getTime() <= delay20hourDateTime.getTime()){
                        isInOut = true;// 20小时内 ,只允许一次上班考勤 和一次下班考勤
                        try {
                            Attendance1 att11 = getSingleObject(connection, Attendance1.class, "SELECT * FROM "
                                    + getInsertUpdateTableNameStaticVersion() +
                                    " where employeeID = '" + empoyeeId + "' order by id desc limit 1");
                            if(att11!=null && "02".equals(att11.getAttType())){
                                    InOutNumberState.str = "本次上下班都已完成, 不能继续考勤.";
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }
            //员工考勤下班时 ,检查数据库是否已经考勤过下班
            else if ("02".equals(attType)) {
                Attendance1 att = null;
                try {
                    att = getSingleObject(connection, Attendance1.class, "SELECT * FROM "
                            + getInsertUpdateTableNameStaticVersion() +
                            " where employeeID = '" + empoyeeId + "' order by id desc limit 1");
                    if (att != null && "02".equals(att.getAttType())) { // 已经是下班状态 ,20小时内不能再上下班
                        isInOut = true;
                        Attendance1 att1 = getSingleObject(connection, Attendance1.class, "SELECT * FROM "
                        + getInsertUpdateTableNameStaticVersion() +
                        " where employeeID = '" + empoyeeId + "' and attType = '01' order by id desc limit 1");
                        if(att1 != null){
                            Date delay20hourDateTime1 = new Date((att1.getBeginTime()).getTime() + (20 * 60 * 60 * 1000L));
                            if (currentDateTime.getTime() <= delay20hourDateTime1.getTime())   //已经上下班了 ,就不能再上下班了
                            {
                                InOutNumberState.str = "本次上下班都已完成, 不能继续考勤.";
                            }
                        }
                    } else if (att != null && "01".equals(att.getAttType())) {
                        Date delay20hourDateTime = new Date((att.getBeginTime()).getTime() + (20 * 60 * 60 * 1000L));
                        if (currentDateTime.getTime() > delay20hourDateTime.getTime())   //超过20小时 ,不能下班 ,只能上班
                            isInOut = true;
                        if(!isInOut) {
                            trans.setDayDateForAtt(att.getDayDate());
                            trans.setBeginTimeForAtt(att.getBeginTime());
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (EntityNotFoundException e) {
                    isInOut = true;
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return isInOut;
    }

    //select * from work_check WHERE ID =( SELECT MAX(ID) from acctendance);  最后一个考勤的
	public static Iterator getCurrentOneOfAttendance(DbConnection connection)
        throws EntityNotFoundException {
        try {
            return getMultipleObjects(connection, Attendance1.class, "SELECT * FROM "
                + getInsertUpdateTableNameStaticVersion() +
                    " WHERE ID = (SELECT MAX(ID) from " + getInsertUpdateTableNameStaticVersion() + ")" );
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            throw new EntityNotFoundException(e.toString());
        }
    }

    public static Iterator getCurrentAttendance(DbConnection connection, int zNumber)
        throws EntityNotFoundException {
        try {
            return getMultipleObjects(connection, Attendance.class, "SELECT * FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE zNumber = " + zNumber);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            throw new EntityNotFoundException(e.toString());
        }
    }

	/**
	 * only used at the sc side, do nothing for pos side
	 *
	 */
	public static void deleteBySequenceNumber(DbConnection connection, int zNumber) throws SQLException {
		if (!hyi.cream.inline.Server.serverExist())
			return;
		String deleteSql = "DELETE FROM "
				+ getInsertUpdateTableNameStaticVersion()
				+ " WHERE zNumber = " + zNumber;
		// System.out.println(deleteSql);
		executeQuery(connection, deleteSql);
	}


	/**
	 * Clone Attendance objects for SC
	 *
	 * This method is only used at POS side.
	 */
	public static Object[] cloneForSC(Iterator iter) {
		String[][] fieldNameMap = getPosToScFieldNameArray();

		try {
			ArrayList objArray = new ArrayList();
			while (iter.hasNext()) {
				Attendance1 ad = (Attendance1) iter.next();
//				System.out.println("daishouid1=" + ds.getFirstNumber() + ",
//				daishouid2=" + ds.getSecondNumber());
				Attendance1 clonedAD = new Attendance1();
				for (int i = 0; i < fieldNameMap.length; i++) {
					Object value = ad.getFieldValue(fieldNameMap[i][0]);
					if (value == null) {
						try {
							value = ad.getClass().getDeclaredMethod(
									"get" + fieldNameMap[i][0], new Class[0])
									.invoke(ad, new Object[0]);
						} catch (Exception e) {
							value = null;
						}
					}
					clonedAD.setFieldValue(fieldNameMap[i][1], value);
				}
				objArray.add(clonedAD);
			}
			return objArray.toArray();
		} catch (Exception e) {
			return null;
		}
	}

    public Attendance1 cloneForSC() {
        return (Attendance1)this;
    }

	/**
	 * return fieldName map of PosToSc as String[][]
	 */
	public static String[][] getPosToScFieldNameArray() {
		return new String[][] {
                { "ID", "ID" },
				{ "storeID", "storeID" },
                { "posNumber", "posNumber" },
				{ "busiDate", "busiDate" },
				{ "dayDate", "dayDate" },
				{ "employeeID", "employeeID" },
				{ "beginTime", "beginTime" },
				{ "endTime", "endTime" },
                { "createUserID", "createUserID" },
                { "createDate", "createDate" },
                { "attType", "attType" },
		};
	}

	/** ********* getters and setters **************** */
	public void setId(Integer id) { //id是自增变量
		setFieldValue("ID", id);
	}

    public Integer getId() {
	    return ((Integer) getFieldValue("ID"));
	}

    public void setZnumber(Integer znumber) {
		setFieldValue("zNumber", znumber);
	}

    public Integer getZnumber() {
	    return ((Integer) getFieldValue("zNumber"));
	}

    public void setPosNumber(Integer posNumber) {
		setFieldValue("posNumber", posNumber);
	}

    public Integer getPosNumber() {
	    return ((Integer) getFieldValue("posNumber"));
	}

	public void setStoreID(String storeid) {
		setFieldValue("storeID", storeid);
	}

	public String getStoreID() {
		return ((String) getFieldValue("storeID"));
	}

	public void setBusiDate(Date busidate) {
		setFieldValue("busiDate", busidate);
	}

	public Date getBusiDate() {
		return (Date) getFieldValue("busiDate");
	}

	public void setDayDate(Date daydate) {
		setFieldValue("dayDate", daydate);

	}

	public Date getDayDate() {
		return (Date)getFieldValue("dayDate");
	}

	public void setEmployeeID(String employeeid) {
		setFieldValue("employeeID", employeeid);

	}

	public String getEmployeeID() {
		return (String) getFieldValue("employeeID");
	}

    public void setAttType(String attType) {
		setFieldValue("attType", attType);

	}

	public String getAttType() {
		return (String) getFieldValue("attType");
	}

	public void setBeginTime(Date begintime) {
		setFieldValue("beginTime", begintime);
	}

	public Date getBeginTime() {
		return  (Date)getFieldValue("beginTime");
	}

	public void setEndTime(Date endtime) {
		setFieldValue("endTime", endtime);
	}

	public Date getEndTime() {
		return (Date) getFieldValue("endTime");
	}

    public void setCreateUserID(String createuserid) {
		setFieldValue("createUserID", createuserid);

	}

	public String getCreateUserID() {
		return (String) getFieldValue("createUserID");
	}

    public void setCreateDate(Date createdate) {
		setFieldValue("createDate", createdate);
	}

	public Date getCreateDate() {
		return (Date) getFieldValue("createDate");
	}

    public void setTcpflag(int tmtranseq) {
        setFieldValue("TCPFLAG", new Integer(tmtranseq));
    }

	public static void main(String[] args) {
	}

	public static Iterator getAttendance(String zNumber) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            if (zNumber == null) {
                if (isZReportEnd()) //日结后的考勤记录在下一张Z账表里
                    zNumber = String.valueOf(ZReport.getCurrentZNumber(connection) + 1);
                else
                    zNumber = String.valueOf(ZReport.getCurrentZNumber(connection));
            }
            List list = null;
            if (!"Z".equals(zNumber))
                list = DacBase.getMultipleObjects(connection, Attendance1.class,
                        "select * from attendance where zNumber = " + zNumber + " and TCPFLAG <> 1", null);
            else list = DacBase.getMultipleObjects(connection, Attendance1.class,
                    "select * from attendance where TCPFLAG <> 1", null);
            System.out.println("genAttendance legth : " + list.size());
            return list.iterator();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static Iterator getUploadFailedList(DbConnection connection) {
        try {
//            String failFlag = "2";
            return getMultipleObjects(connection, Attendance1.class,
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
                            " WHERE TCPFLAG <> 1");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static java.util.Date getDefaultEndAccDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(1971, 0, 1, 0, 0, 0); // = 1971-01-01 00:00:00
        return cal.getTime();
    }

    public static boolean isZReportEnd() {
        DbConnection connection = null;
        boolean isZreportEnd = false;
        try {
            connection = CreamToolkit.getTransactionalConnection();
            Date initDate = CreamToolkit.getInitialDate();
            ZReport z = ZReport.getOrCreateCurrentZReport(connection);
            if (CreamToolkit.compareDate(z.getAccountingDate(), initDate) != 0) {
                isZreportEnd = true;
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
        return isZreportEnd;
    }

}
