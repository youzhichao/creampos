package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * CombGiftDetail definition class.
 *
 * @author Meyer 
 * @version 1.0
 */
public class CombGiftDetail extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 更新记录
     * 
     * Meyer / 1.0 / 2003-01-02
     * 		1. Create the class file
     * 
     */
    transient public static final String VERSION = "1.0";

    static final String tableName = "combination_gift_detail";
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("CGD_CPH_ID");
        primaryKeys.add("CGD_GIFT_ID");
    }

    /**
     * Constructor
     */
    public CombGiftDetail() {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static Iterator queryByCphID(String aCphID) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return CombGiftDetail.getMultipleObjects(connection,
            	CombGiftDetail.class,
                "SELECT * FROM combination_gift_detail" + 
                " WHERE CGD_CPH_ID='" + aCphID + "'"
            );
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public String getCphID() {
        return (String)getFieldValue("CGD_CPH_ID");
    }

    public String getPluCode() {
        return (String)getFieldValue("CGD_GIFT_ID");
    }

	/**
	 * Meyer/2003-02-21/Modified because of LogicChina's requirement 
	 * 		datatype of CGD_QUANTITY in db cahnge to decimal(10,2)
	 */
    public Integer getQuantity() {
        Object value = getFieldValue("CGD_QUANTITY");
        if (value instanceof HYIDouble)
            return new Integer(
            	((HYIDouble)getFieldValue("CGD_QUANTITY")).intValue());
        else
            return (Integer)value;
    }

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
	        Map fieldNameMap = null;			      
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.CombGiftDetail.class,
            	"SELECT * FROM posdl_combination_gift_detail", fieldNameMap);
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}