/*
 * Created on 2003-6-10
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.dac;

import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import java.math.BigDecimal;
import hyi.cream.util.DbConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Administrator
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class LimitQtySold extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    public transient static final String VERSION = "1.5";
    static final String tableName = "pricechghead";

    /* (non-Javadoc)
     * @see hyi.cream.dac.DacBase#getPrimaryKeyList()
     */

    public LimitQtySold() {
    }

    public List getPrimaryKeyList() {
        return null;
    }

    /* (non-Javadoc)
     * @see hyi.cream.dac.DacBase#getInsertUpdateTableName()
     */
    public String getInsertUpdateTableName() {
        return "pricechghead";

    }

    public static String getInsertUpdateTableNameStaticVersion() {
        return "pricechghead";
    }

    synchronized public static Collection queryObject(String strItemNo, String strAddNum) {
        return queryObject(strItemNo, strAddNum, null);
    }

    synchronized public static Collection queryObject(String strItemNo, String strAddNum, String strMemberID) {

        int intLimitQty = 0, intQtySold = 0, intMemberLimitQty = 0, intMemberQtySold = 0;
        String strPriceChgID, strBeginDate, strEndDate, strBeginTime, strEndTime;
        HYIDouble limitPrice = new HYIDouble(0.00);       //限量价格
        HYIDouble limitRebateRate = new HYIDouble(0.00);  //限量还圆金比率
        boolean blnSaleable = false;

        DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        LimitQtySold lqs;
        lqs = new LimitQtySold();

        try {
            Integer.parseInt(strAddNum);
        } catch (NumberFormatException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return null;
        }
        List list = new ArrayList();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            String strSql =
                "SELECT h.priceChgID, h.beginDate, h.endDate, h.beginTime, h.endTime, "
                    + "d.itemID, d.limitQty, d.memberLimitQty, d.actualQty, d.limitPrice, d.limitRebateRate "
                    + "FROM pricechghead h LEFT JOIN pricechgdtl d "
                    + "ON h.priceChgID = d.priceChgID "
                    + "WHERE SYSDATE() BETWEEN h.beginDate AND h.endDate AND "
                    + "CURTIME() BETWEEN h.beginTime AND h.endTime "
                    + "AND d.itemID = '"
                    + strItemNo
                    + "' "
                    + "ORDER BY h.priceChgID DESC";

            resultSet = statement.executeQuery(strSql);
            Object o = null;
            if (resultSet.next()) {
                strPriceChgID = resultSet.getString("priceChgID");
                intLimitQty = resultSet.getInt("limitQty");
                intQtySold = resultSet.getInt("actualQty");
                intMemberLimitQty = resultSet.getInt("memberLimitQty");
//                limitPrice  = resultSet.getBigDecimal("limitPrice");
//                limitRebateRate = resultSet.getBigDecimal("limitRebateRate");
				o = resultSet.getBigDecimal("limitPrice");
				if (o != null) {
					limitPrice.reset(((BigDecimal)o).doubleValue());
				}
				o = resultSet.getBigDecimal("limitRebateRate");
				if (o != null) {
					limitRebateRate.reset(((BigDecimal)o).doubleValue());
				}
                strBeginDate = resultSet.getDate("beginDate").toString();
                strEndDate = resultSet.getDate("endDate").toString();
                strBeginTime = resultSet.getTime("beginTime").toString();
                strEndTime = resultSet.getTime("endTime").toString();
                resultSet.close();

                if (intLimitQty > 0 && (intLimitQty <= intQtySold)) {
                    blnSaleable = false;
                } else if (intMemberLimitQty > 0 && strMemberID != null) {
                	blnSaleable = true;
                    strSql =
                        "select sum(d.quantity) as Total from posul_tranhead h, "
                            + "posul_trandtl d, itemplu i "
                            + "where h.posNumber = d.posNumber and "
                            + "h.transactionNumber = d.transactionNumber and "
                            + "h.systemDate = d.systemDate and "
                            + "d.pluNumber = i.pluNumber and "
                            + "h.systemDate between '"
                            + strBeginDate
                            + " "
                            + strBeginTime
                            + "' "
                            + "and '"
                            + strEndDate
                            + " "
                            + strEndTime
                            + "' "
                            + "and h.memberID = '"
                            + strMemberID
                            + "' "
                            + "and i.itemNumber = '"
                            + strItemNo
                            + "'";

                    try {
                        resultSet = statement.executeQuery(strSql);
                        if (resultSet.next()) {
                            if (resultSet.getBigDecimal("Total") != null)
                                intMemberQtySold = resultSet.getBigDecimal("Total").intValue();
                        }

                        if (intMemberLimitQty <= intMemberQtySold)
                            blnSaleable = false;
                        else
                        	blnSaleable = true;
                    } catch (SQLException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                    }
                }
                
                if (Integer.parseInt(strAddNum) < 0 || lqs.isSaleable()) {
                    strSql =
                        "UPDATE pricechgdtl SET actualQty = actualQty + ("
                            + strAddNum
                            + ") "
                            + "WHERE priceChgID = '"
                            + strPriceChgID
                            + "' "
                            + "AND itemID = '"
                            + strItemNo
                            + "'";
                    try {
                        statement.executeUpdate(strSql);
                    } catch (SQLException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);

            lqs.setSaleable(blnSaleable);
            lqs.setMemberQtySold(intMemberQtySold);
            lqs.setSaleable(blnSaleable);
            lqs.setQtySold(intQtySold);
            lqs.setLimitPrice(limitPrice);
            lqs.setLimitRebateRate(limitRebateRate);

            list.add(lqs);
        }
        return list;
    }


    /**
     * 取得限量促销价格
     * @return the price of limit quantity sale.
     */
    public HYIDouble getLimitPrice() {
        Object d = getFieldValue("limitPrice");
        return d != null ? (HYIDouble)d:new HYIDouble(0.00);
    }
    
    public void setLimitPrice(HYIDouble price) {
        this.setFieldValue("limitPrice", price);
    }
    /**
     * 返回限量促销还圆金比率
     * @return the rebate rate of limit quantity sale.
     */
    public HYIDouble getLimitRebateRate() {
        Object d = getFieldValue("limitRebateRate");
        return d != null ? (HYIDouble)d:new HYIDouble(0.00);
    }
    
    public void setLimitRebateRate(HYIDouble rate) {
        this.setFieldValue("limitRebateRate", rate);
    }
    
    /**
     * @return 如果该商品为总数量的限量促销，则返回该商品在限量促销期间的总销售量. 
     * 否则返回0
     */
    public int getQtySold() {
        return ((Integer)getFieldValue("actualQty")).intValue();
    }

    public void setQtySold(int qtySold) {
        this.setFieldValue("actualQty", new Integer(qtySold));
    }


    /**
     * @return 如果该商品为限制会员的限量促销，则返回该商品在限量促销期间销售给该
     * 会员的销售量. 否则返回0
     */
    public int getMemberQtySold() {
        return ((Integer)getFieldValue("memberQtySold")).intValue();
    }

    public void setMemberQtySold(int memberQtySold) {
        this.setFieldValue("memberQtySold", new Integer(memberQtySold));
    }

    /**
     * @return 是否可以限量促销
     */
    public boolean isSaleable() {
        return ((Boolean)getFieldValue("isSaleable")).booleanValue();
    }

    public void setSaleable(boolean salable) {
        this.setFieldValue("isSaleable", new Boolean(salable));
    }


}
