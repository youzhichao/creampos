/**
 * DAC class
 * @since 2000
 * @author slackware
 */

package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.groovydac.Param;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class CategorySales extends DacBase implements Serializable {//R&W
    private static final long serialVersionUID = 1L;

	static final String tableName = "catsales";
	static CategorySales staticCS  = null;
	static Iterator currentCategorySales = null;
	boolean addCustom = false;
    HYIDouble mmAmount = new HYIDouble(0);

	private static ArrayList primaryKeys = new ArrayList();

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	static {
		primaryKeys.add("CATNO");
		primaryKeys.add("MIDCATNO");
		primaryKeys.add("MICROCATNO");
		primaryKeys.add("ZCNT");
	}                           

	public static CategorySales queryByDateTime(DbConnection connection, java.util.Date date) {
		SimpleDateFormat df = CreamCache.getInstance().getDateTimeFormate();
		//System.out.println("SELECT * FROM " + tableName + " WHERE SYSDATETIME='" + df.format(date).toString() + "'");
		try {
            return (CategorySales)getSingleObject(connection, CategorySales.class,
            	"SELECT * FROM " + tableName + " WHERE SYSDATETIME='" + df.format(date).toString() + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}        

    public static Iterator queryByCatID(DbConnection connection, String catID, Date date) {
        Integer zcnt;
        ArrayList catArray = new ArrayList();
        if (date != null) {
            Iterator zIter = ZReport.queryByDateTime(connection, date);
            while (zIter.hasNext()) {
                zcnt = ((ZReport)zIter.next()).getSequenceNumber();
                try {
                    catArray.add(getSingleObject(connection, CategorySales.class, "select * from "
                        +  tableName + " where CATNO='" + catID + "' AND MIDCATNO='' AND ZCNT=" + zcnt));
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                } catch (EntityNotFoundException e) {
                    CreamToolkit.logMessage(e);
                }
            }
        } else {
            try {
                zcnt = ZReport.getOrCreateCurrentZReport(connection).getSequenceNumber();
                connection.commit(); // because may create a new Z
                catArray.add(getSingleObject(connection, CategorySales.class, "select * from "
                    +  tableName + " where CATNO='" + catID + "' AND MIDCATNO='' AND ZCNT=" + zcnt));
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } catch (EntityNotFoundException e) {
                CreamToolkit.logMessage(e);
            }
        }
        return catArray.iterator();
    }           

    public static Iterator queryByCatIDAndMidCatID(DbConnection connection, String catID,
            String midcatID, Date date) {
        Integer zcnt;
        ArrayList catArray = new ArrayList();
        if (date != null) {
            Iterator zIter = ZReport.queryByDateTime(connection, date);
            while (zIter.hasNext()) {
                zcnt = ((ZReport)zIter.next()).getSequenceNumber();
                try {
                    catArray.add(getSingleObject(connection, CategorySales.class, "select * from "
                        +  tableName + " where CATNO='" + catID + "' AND MIDCATNO='"
                        + midcatID + "' AND MICROCATNO='' AND ZCNT=" + zcnt));
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                } catch (EntityNotFoundException e) {
                    CreamToolkit.logMessage(e);
                }
            }
        } else {
            try {
                zcnt = ZReport.getOrCreateCurrentZReport(connection).getSequenceNumber();
                connection.commit(); // because may create a new Z
                catArray.add(getSingleObject(connection, CategorySales.class, "select * from "
                    +  tableName + " where CATNO='" + catID + "' AND MIDCATNO='"
                    + midcatID + "' AND MICROCATNO='' AND ZCNT=" + zcnt));
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } catch (EntityNotFoundException e) {
                CreamToolkit.logMessage(e);
            }
        }
        return catArray.iterator();
    }

    public static Iterator queryByCatIDAndMidCatIDAndMicroCatID(DbConnection connection, String catID,
            String midcatID, String microcatID, Date date) {
        Integer zcnt;
        ArrayList catArray = new ArrayList();
        if (date != null) {
            Iterator zIter = ZReport.queryByDateTime(connection, date);
            //Iterator catIter = null;
            //CategorySales cat = null;
            while (zIter.hasNext()) {
                zcnt = ((ZReport)zIter.next()).getSequenceNumber();
                try {
                    catArray.add(getSingleObject(connection, CategorySales.class,
                        "select * from " +  tableName + " where CATNO='" + catID
                        + "' AND MIDCATNO='" + midcatID + "' AND MICROCATNO='"
                        + microcatID + "' AND ZCNT=" + zcnt));
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                } catch (EntityNotFoundException e) {
                    CreamToolkit.logMessage(e);
                }
            }
        } else {
            try {
                zcnt = ZReport.getOrCreateCurrentZReport(connection).getSequenceNumber();
                connection.commit(); // because may create a new Z
                catArray.add(getSingleObject(connection, CategorySales.class,
                    "select * from " +  tableName + " where CATNO='" + catID
                    + "' AND MIDCATNO='" + midcatID + "' AND MICROCATNO='"
                    + microcatID + "' AND ZCNT=" + zcnt));
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } catch (EntityNotFoundException e) {
                CreamToolkit.logMessage(e);
            }
        }
        return catArray.iterator();
    }

	public static Iterator getCurrentCategorySales(DbConnection connection) {
		if (currentCategorySales == null) {
            try {
                currentCategorySales = getMultipleObjects(connection, CategorySales.class,
                    "SELECT * FROM " +  tableName + " WHERE zcnt=" + PARAM.getZNumber());
            } catch (EntityNotFoundException e) {
                currentCategorySales = constructFromCategory(connection);
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                // keep currentCategorySales null to dispose problem
            }
		}
		return currentCategorySales;
	}

	public static void updateAll(DbConnection connection) throws SQLException {
        try {
            Iterator itr = getCurrentCategorySales(connection);
            while (itr.hasNext()) {
                CategorySales catSale = (CategorySales)itr.next();
                catSale.update(connection);
            }
        } catch (EntityNotFoundException e) {
            throw new SQLException(e.toString());
        }
    }

	public static void updateAll_Z(DbConnection connection) throws SQLException {
        try {
    		Iterator itr =  getCurrentCategorySales(connection);
    		while (itr.hasNext()) {
    			CategorySales catSale = (CategorySales)itr.next();
    			catSale.setSystemDateTime(new Date());
    			catSale.update(connection);
    		}
        } catch (EntityNotFoundException e) {
            throw new SQLException(e.toString());
        }
	}

	public CategorySales() {
		setSystemDateTime(CreamToolkit.getInitialDate());
		setStoreNumber(Store.getStoreID());
		setTerminalNumber(PARAM.getTerminalNumber());
		setSequenceNumber(PARAM.getZNumber());
	}

	public static Iterator constructFromCategory(DbConnection connection) {
	    ArrayList it = new ArrayList();
        try {
            Iterator cats = getMultipleObjects(connection, Category.class, "SELECT * FROM " + Category.tableName);
            while (cats.hasNext()) {
                Category cat = (Category)cats.next();
                staticCS = new CategorySales();
                staticCS.setCategoryNumber(cat.getCategoryNumber());
                staticCS.setMidCategoryNumber(cat.getMidCategoryNumber());
                staticCS.setMicroCategoryNumber(cat.getMicroCategoryNumber());
                it.add(staticCS);
            }
            return it.iterator();

        } catch (EntityNotFoundException e) {
            return it.iterator();

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return it.iterator();
        }
    }
	
	public static ArrayList getAvailableCategory(DbConnection connection, LineItem li) {
		Iterator itr =  getCurrentCategorySales(connection);
		ArrayList array = new ArrayList();
		while (itr.hasNext()) {
			CategorySales catSale = (CategorySales)itr.next();
			if (catSale.belongTo(li))
				array.add(li);
		}
		return array;
	}

	public static ArrayList getAvailableCategory(DbConnection connection, PLU li) {
		Iterator itr =  getCurrentCategorySales(connection);
		ArrayList array = new ArrayList();
		while (itr.hasNext()) {
			CategorySales catSale = (CategorySales)itr.next();
			if (catSale.belongTo(li))
				array.add(li);
		}
		return array;
	}


	public static Iterator getAllCategorySales(DbConnection connection) {
        try {
            return getMultipleObjects(connection, CategorySales.class, "SELECT * FROM " +  tableName 
                + " WHERE ZCNT=" + PARAM.getZNumber());
        } catch (EntityNotFoundException e) {
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        }

		Iterator itr = CategorySales.constructFromCategory(connection);
        try {
            while (itr.hasNext()) {
    		   CategorySales catSale = (CategorySales)itr.next();
               if (catSale.exists(connection))
                   catSale.load(connection);
               else
                   catSale.insert(connection);
    		}
            return itr;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null; // serious problem
        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e);
            return null; // serious problem
        }
	}

	public void setCustomAdd(boolean add){
		addCustom = add;
	}

	public boolean getCustomAdd() {
        return addCustom;
	}

	public boolean belongTo (LineItem lineItem) {
		String c = lineItem.getCategoryNumber();
		String m1 = lineItem.getMidCategoryNumber();
		String m2 = lineItem.getMicroCategoryNumber();
		if (this.getCategoryNumber().equals(c)
			&& this.getMidCategoryNumber().equals(m1)
			&& this.getMicroCategoryNumber().equals(m2))
			return true;
		return false;
	}

	public boolean belongTo (PLU lineItem) {
		String c = lineItem.getCategoryNumber();
		String m1 = lineItem.getMidCategoryNumber();
		String m2 = lineItem.getMicroCategoryNumber();
		if (this.getCategoryNumber().equals(c)
			&& this.getMidCategoryNumber().equals(m1)
			&& this.getMicroCategoryNumber().equals(m2))
			return true;
		return false;
	}

    public void setMMTempAmount(HYIDouble mmAmount) {
        this.mmAmount = this.mmAmount.addMe(mmAmount);
    }

    public void updateMM() {
        setMMAmount(getMMAmount().addMe(mmAmount));
    }



	//Get properties value	
//SystemDateTime	SYSDATETIME	DATETIME
	public Date getSystemDataTime() {
		return (Date)getFieldValue("SYSDATETIME");
	}
	public void setSystemDateTime(Date SystemDateTime){
		setFieldValue("SYSDATETIME", SystemDateTime);
	}
	
//SequenceNumber	SHIFTCNT	TINYINT UNSIGNED	N
	public Integer getSequenceNumber() {
		return (Integer)getFieldValue("ZCNT");
	}

	public void setSequenceNumber(Integer SQNumber) {
		setFieldValue("ZCNT", SQNumber);
	}
//StoreNumber	STORECODE	CHAR(6)	N
	public String getStoreNumber() {
		return (String)getFieldValue("STORECODE");
	}

	public void setStoreNumber(String storeNumber) {
		setFieldValue("STORECODE", storeNumber);
	}

//TerminalNumber	TMCODE	TINYINT UNSIGNED	N
	public Integer getTerminalNumber() {
	    return (Integer)getFieldValue("TMCODE");
	}
	public void setTerminalNumber(Integer TMCODE) {
        setFieldValue("TMCODE", TMCODE);
	}

//UploadState
    public String getUploadState() {
        return (String)getFieldValue("TCPFLG");
    }

    public void setUploadState(String tcpflg) {
        setFieldValue("TCPFLG", tcpflg);
    }

//TransactionCount
    public Integer getTransactionCount() {
        return (Integer)getFieldValue("TRANCNT");
    }

    public void setTransactionCount(Integer trancnt) {
        setFieldValue("TRANCNT", trancnt);
    }

//GrossSalesTotalAmount
    public HYIDouble getGrossSalesTotalAmount() {
        return (HYIDouble)getFieldValue("GROSSAMT");
    }

    public void setGrossSalesTotalAmount(HYIDouble grossamt) {
        setFieldValue("GROSSAMT", grossamt);
    }

//PlusCount
    public Integer getPlusCount() {
        return (Integer)getFieldValue("PLUSCOUNT");
    }

    public void setPlusCount(Integer pluscount) {
        setFieldValue("PLUSCOUNT", pluscount);
    }

//PlusAmount
    public HYIDouble getPlusAmount() {
        return (HYIDouble)getFieldValue("PLUSAMOUNT");
    }

    public void setPlusAmount(HYIDouble plusamount) {
        setFieldValue("PLUSAMOUNT", plusamount);
    }

	//--------------------------------------------------------------------------
	//CategoryNumber	CATNO CHAR(4)	N
	public String getCategoryNumber() {
		return (String)getFieldValue("CATNO");//CategoryNumber	CATNO CHAR(4)	N
	}

	public void setCategoryNumber(String CategoryNumber) {
		setFieldValue("CATNO", CategoryNumber);//CategoryNumber	CATNO CHAR(4)	N
	}

	//MidCategoryNumber MIDCATNO	VARCHAR(4)	N
	public String getMidCategoryNumber() {
		return (String)getFieldValue("MIDCATNO");//MidCategoryNumber MIDCATNO	VARCHAR(4)	N
	}

	public void setMidCategoryNumber(String MidCategoryNumber) {
	    setFieldValue("MIDCATNO", MidCategoryNumber);//MidCategoryNumber MIDCATNO	VARCHAR(4)	N
	}

    //MicroCategoryNumber MICROCATNO VARCHAR(4)	N
	public String getMicroCategoryNumber() {
		return (String)getFieldValue("MICROCATNO");//MicroCategoryNumber MICROCATNO VARCHAR(4)	N
	}

	public void setMicroCategoryNumber(String MicroCategoryNumber) {
		setFieldValue("MICROCATNO", MicroCategoryNumber);//MicroCategoryNumber MICROCATNO VARCHAR(4)	N
	}
	//*************************************************************************

	

	public HYIDouble getAmount() {
		return (HYIDouble)getFieldValue("AMOUNT");
	}

	public void setAmount(HYIDouble Amount) {
		setFieldValue("AMOUNT", Amount);
	}
	//

	public Integer getDiscountCount() {
		return (Integer)getFieldValue("DCOUNT");
	}

	public void setDiscountCount(Integer DiscountCount) {
		setFieldValue("DCOUNT", DiscountCount);
	}
	//

	public HYIDouble getDiscountAmount() {
		return (HYIDouble)getFieldValue("DAMOUNT");//DiscountAmount	DAMOUNT	DECIMAL(10,2)	N
	}
	public void setDiscountAmount(HYIDouble DiscountAmount) {
		 setFieldValue("DAMOUNT", DiscountAmount);//DiscountAmount	DAMOUNT	DECIMAL(10,2)	N
	}
	//

	public Integer getMMCount() {
		return (Integer)getFieldValue("MMCOUNT");//MMCount MMCOUNT INT	N
	}
	public void setMMCount(Integer MMount) {
	    setFieldValue("MMOUNT", MMount);
	}
	//

	public HYIDouble getMMAmount() {
		return (HYIDouble)getFieldValue("MMAmount"); //MMAmount MMAMOUNT DECIMAL(10,2)	N
	}
	public void setMMAmount(HYIDouble MMAmount) {
		setFieldValue("MMAmount", MMAmount); //MMAmount MMAMOUNT DECIMAL(10,2)	N
	}
	//

	public Integer getCustomCount() {
		return (Integer)getFieldValue("CUSTCOUNT");
	}//CustomerCount CUSTCOUNT	INT	N

	public void setCustomCount(Integer CustomCount) {
		setFieldValue("CUSTCOUNT", CustomCount);
	}//CustomerCount CUSTCOUNT	INT	N

	public String getInsertUpdateTableName() {
		return tableName;
	}

}
