package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class HourlySales extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

	private static ArrayList primaryKeys = new ArrayList();
	
	public List getPrimaryKeyList() {
		return  primaryKeys;
	}

	private static HourlySales currentHourlySales = null;
	
	static final String tableName = "hourlysales";
	
	static {
		primaryKeys.add("TMCODE");
		primaryKeys.add("ZCNT");
	}
		
	public static HourlySales queryBySequenceNumber(DbConnection connection, String ID) {
		try {
            return getSingleObject(connection, HourlySales.class,
            	"SELECT * FROM " + tableName + " WHERE ZCNT='" + ID + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}                                                      
		
	public static HourlySales queryByTerminalNumber(DbConnection connection, String ID) {
		try {
            return getSingleObject(connection, HourlySales.class,
            	"SELECT * FROM " + tableName + " WHERE TMCODE='" + ID + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}                   

	public String getInsertUpdateTableName() {
		return tableName;
	}

    /**
     * Constructor
     */
	public HourlySales() {
		setAccountingDate(CreamToolkit.getInitialDate());
		setSystemDateTime(new Date());
		setSequenceNumber(PARAM.getShiftNumber());
		setStoreNumber(Store.getStoreID());
		setTerminalNumber(PARAM.getTerminalNumber());
		setUploadState("0");
	}

	public static HourlySales queryByDateTime(DbConnection connection, java.util.Date date) {
		try {
		    SimpleDateFormat df = CreamCache.getInstance().getDateFormate();
		    //System.out.println("SELECT * FROM " + tableName + " WHERE SYSDATETIME='" + df.format(date).toString() + "'");
            return getSingleObject(connection, HourlySales .class,
            	"SELECT * FROM " + tableName + " WHERE ACCDATE='" + df.format(date).toString() + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}

	public static HourlySales getCurrentHourlySales(DbConnection connection) throws SQLException {
		if (currentHourlySales == null) {
			HourlySales hourly = HourlySales.queryByDateTime(connection, CreamToolkit.getInitialDate());
			if (hourly != null)
				currentHourlySales = hourly;
			else {
				currentHourlySales = createHourlySales();
				currentHourlySales.insert(connection);
			}
		}
		return currentHourlySales;
	}

	public static HourlySales createHourlySales() {
		currentHourlySales = new HourlySales();
		return currentHourlySales;
	}

    /**
     * table's properties
     */

//表头

//AccountingDate
    public Date getAccountingDate() {
        return (Date)getFieldValue("ACCDATE");
    }

    public void setAccountingDate(Date accdate) {
        setFieldValue("ACCDATE", accdate);
    }

//SystemDateTime
    public Date getSystemDateTime() {
        return (Date)getFieldValue("SYSDATETIME");
    }

    public void setSystemDateTime(Date sysdatetime) {
        setFieldValue("SYSDATETIME", sysdatetime);
    }

//SequenceNumber
    public Integer getSequenceNumber() {
        return (Integer)getFieldValue("ZCNT");
    }

    public void setSequenceNumber(Integer zcnt) {
        setFieldValue("ZCNT", zcnt);
    }

//StoreNumber
    public String getStoreNumber() {
        return (String)getFieldValue("STORECODE");
    }

    public void setStoreNumber(String storecode) {
        setFieldValue("STORECODE", storecode);
    }

//TerminalNumber
    public Integer getTerminalNumber() {
        return (Integer)getFieldValue("TMCODE");
    }

    public void setTerminalNumber(Integer tmcode) {
        setFieldValue("TMCODE", tmcode);
    }

//UploadState	TCPFLG	ENUM("0","1","2")	N
	public String getUploadState() {
		return (String)getFieldValue("TCPFLG");
	}

	public void setUploadState(String TcpFlag) {
		 setFieldValue("TCPFLG", TcpFlag);
	}

//时段销售

//Amount00
    public HYIDouble getAmount00() {
        return (HYIDouble)getFieldValue("AMT00");
    }

    public void setAmount00(HYIDouble amt00) {
        setFieldValue("AMT00", amt00);
    }

//Quantity00
    public Integer getQuantity00() {
        return (Integer)getFieldValue("QTY00");
    }

    public void setQuantity00(Integer qty00) {
        setFieldValue("QTY00", qty00);
    }

//Amount01
    public HYIDouble getAmount01() {
        return (HYIDouble)getFieldValue("AMT01");
    }

    public void setAmount01(HYIDouble amt01) {
        setFieldValue("AMT01", amt01);
    }

//Quantity01
    public Integer getQuantity01() {
        return (Integer)getFieldValue("QTY01");
    }

    public void setQuantity01(Integer qty01) {
        setFieldValue("QTY01", qty01);
    }     

//Amount02
    public HYIDouble getAmount02() {
        return (HYIDouble)getFieldValue("AMT02");
    }

    public void setAmount02(HYIDouble amt02) {
        setFieldValue("AMT02", amt02);
    }

//Quantity02
    public Integer getQuantity02() {
        return (Integer)getFieldValue("QTY02");
    }

    public void setQuantity02(Integer qty02) {
        setFieldValue("QTY02", qty02);
    }

//Amount03
    public HYIDouble getAmount03() {
        return (HYIDouble)getFieldValue("AMT03");
    }

    public void setAmount03(HYIDouble amt03) {
        setFieldValue("AMT03", amt03);
    }

//Quantity03
    public Integer getQuantity03() {
        return (Integer)getFieldValue("QTY03");
    }

    public void setQuantity03(Integer qty03) {
        setFieldValue("QTY03", qty03);
    }

//Amount04
    public HYIDouble getAmount04() {
        return (HYIDouble)getFieldValue("AMT04");
    }

    public void setAmount04(HYIDouble amt04) {
        setFieldValue("AMT04", amt04);
    }

//Quantity04
    public Integer getQuantity04() {
        return (Integer)getFieldValue("QTY04");
    }

    public void setQuantity04(Integer qty04) {
        setFieldValue("QTY04", qty04);
    }

//Amount05
    public HYIDouble getAmount05() {
        return (HYIDouble)getFieldValue("AMT05");
    }

    public void setAmount05(HYIDouble amt05) {
        setFieldValue("AMT05", amt05);
    }

//Quantity05
    public Integer getQuantity05() {
        return (Integer)getFieldValue("QTY05");
    }

    public void setQuantity05(Integer qty05) {
        setFieldValue("QTY05", qty05);
    }

//Amount06
    public HYIDouble getAmount06() {
        return (HYIDouble)getFieldValue("AMT06");
    }

    public void setAmount06(HYIDouble amt06) {
        setFieldValue("AMT06", amt06);
    }

//Quantity06
    public Integer getQuantity06() {
        return (Integer)getFieldValue("QTY06");
    }

    public void setQuantity06(Integer qty06) {
        setFieldValue("QTY06", qty06);
    }

//Amount07
    public HYIDouble getAmount07() {
        return (HYIDouble)getFieldValue("AMT07");
    }

    public void setAmount07(HYIDouble amt07) {
        setFieldValue("AMT07", amt07);
    }

//Quantity07
    public Integer getQuantity07() {
        return (Integer)getFieldValue("QTY07");
    }

    public void setQuantity07(Integer qty07) {
        setFieldValue("QTY07", qty07);
    }

//Amount08
    public HYIDouble getAmount08() {
        return (HYIDouble)getFieldValue("AMT08");
    }

    public void setAmount08(HYIDouble amt08) {
        setFieldValue("AMT08", amt08);
    }

//Quantity08
    public Integer getQuantity08() {
        return (Integer)getFieldValue("QTY08");
    }

    public void setQuantity08(Integer qty08) {
        setFieldValue("QTY08", qty08);
    }

//Amount09
    public HYIDouble getAmount09() {
        return (HYIDouble)getFieldValue("AMT09");
    }

    public void setAmount09(HYIDouble amt09) {
        setFieldValue("AMT09", amt09);
    }

//Quantity09
    public Integer getQuantity09() {
        return (Integer)getFieldValue("QTY09");
    }

    public void setQuantity09(Integer qty09) {
        setFieldValue("QTY09", qty09);
    }

//Amount10
    public HYIDouble getAmount10() {
        return (HYIDouble)getFieldValue("AMT10");
    }

    public void setAmount10(HYIDouble amt10) {
        setFieldValue("AMT10", amt10);
    }

//Quantity10
    public Integer getQuantity10() {
        return (Integer)getFieldValue("QTY10");
    }

    public void setQuantity10(Integer qty10) {
        setFieldValue("QTY10", qty10);
    }

//Amount11
    public HYIDouble getAmount11() {
        return (HYIDouble)getFieldValue("AMT11");
    }

    public void setAmount11(HYIDouble amt11) {
        setFieldValue("AMT11", amt11);
    }

//Quantity11
    public Integer getQuantity11() {
        return (Integer)getFieldValue("QTY11");
    }

    public void setQuantity11(Integer qty11) {
        setFieldValue("QTY11", qty11);
    }

//Amount12
    public HYIDouble getAmount12() {
        return (HYIDouble)getFieldValue("AMT12");
    }

    public void setAmount12(HYIDouble amt12) {
        setFieldValue("AMT12", amt12);
    }

//Quantity12
    public Integer getQuantity12() {
        return (Integer)getFieldValue("QTY12");
    }

    public void setQuantity12(Integer qty12) {
        setFieldValue("QTY12", qty12);
    }

//Amount13
    public HYIDouble getAmount13() {
        return (HYIDouble)getFieldValue("AMT13");
    }

    public void setAmount13(HYIDouble amt13) {
        setFieldValue("AMT13", amt13);
    }

//Quantity13
    public Integer getQuantity13() {
        return (Integer)getFieldValue("QTY13");
    }

    public void setQuantity13(Integer qty13) {
        setFieldValue("QTY13", qty13);
    }

//Amount14
    public HYIDouble getAmount14() {
        return (HYIDouble)getFieldValue("AMT14");
    }

    public void setAmount14(HYIDouble amt14) {
        setFieldValue("AMT14", amt14);
    }

//Quantity14
    public Integer getQuantity14() {
        return (Integer)getFieldValue("QTY14");
    }

    public void setQuantity14(Integer qty14) {
        setFieldValue("QTY14", qty14);
    }

//Amount15
    public HYIDouble getAmount15() {
        return (HYIDouble)getFieldValue("AMT15");
    }

    public void setAmount15(HYIDouble amt15) {
        setFieldValue("AMT15", amt15);
    }

//Quantity15
    public Integer getQuantity15() {
        return (Integer)getFieldValue("QTY15");
    }

    public void setQuantity15(Integer qty15) {
        setFieldValue("QTY15", qty15);
    }

//Amount16
    public HYIDouble getAmount16() {
        return (HYIDouble)getFieldValue("AMT16");
    }

    public void setAmount16(HYIDouble amt16) {
        setFieldValue("AMT16", amt16);
    }

//Quantity16
    public Integer getQuantity16() {
        return (Integer)getFieldValue("QTY16");
    }

    public void setQuantity16(Integer qty16) {
        setFieldValue("QTY16", qty16);
    }

//Amount17
    public HYIDouble getAmount17() {
        return (HYIDouble)getFieldValue("AMT17");
    }

    public void setAmount17(HYIDouble amt17) {
        setFieldValue("AMT17", amt17);
    }

//Quantity17
    public Integer getQuantity17() {
        return (Integer)getFieldValue("QTY17");
    }

    public void setQuantity17(Integer qty17) {
        setFieldValue("QTY17", qty17);
    }

//Amount18
    public HYIDouble getAmount18() {
        return (HYIDouble)getFieldValue("AMT18");
    }

    public void setAmount18(HYIDouble amt18) {
        setFieldValue("AMT18", amt18);
    }

//Quantity18
    public Integer getQuantity18() {
        return (Integer)getFieldValue("QTY18");
    }

    public void setQuantity18(Integer qty18) {
        setFieldValue("QTY18", qty18);
    }

//Amount19
    public HYIDouble getAmount19() {
        return (HYIDouble)getFieldValue("AMT19");
    }

    public void setAmount19(HYIDouble amt19) {
        setFieldValue("AMT19", amt19);
    }

//Quantity19
    public Integer getQuantity19() {
        return (Integer)getFieldValue("QTY19");
    }

    public void setQuantity19(Integer qty19) {
        setFieldValue("QTY19", qty19);
    }

//Amount20
    public HYIDouble getAmount20() {
        return (HYIDouble)getFieldValue("AMT20");
    }

    public void setAmount20(HYIDouble amt20) {
        setFieldValue("AMT20", amt20);
    }

//Quantity20
    public Integer getQuantity20() {
        return (Integer)getFieldValue("QTY20");
    }

    public void setQuantity20(Integer qty20) {
        setFieldValue("QTY20", qty20);
    }

//Amount21
    public HYIDouble getAmount21() {
        return (HYIDouble)getFieldValue("AMT21");
    }

    public void setAmount21(HYIDouble amt21) {
        setFieldValue("AMT21", amt21);
    }

//Quantity21
    public Integer getQuantity21() {
        return (Integer)getFieldValue("QTY21");
    }

    public void setQuantity21(Integer qty21) {
        setFieldValue("QTY21", qty21);
    }

//Amount22
    public HYIDouble getAmount22() {
        return (HYIDouble)getFieldValue("AMT22");
    }

    public void setAmount22(HYIDouble amt22) {
        setFieldValue("AMT22", amt22);
    }

//Quantity22
    public Integer getQuantity22() {
        return (Integer)getFieldValue("QTY22");
    }

    public void setQuantity22(Integer qty22) {
        setFieldValue("QTY22", qty22);
    }

//Amount23
    public HYIDouble getAmount23() {
        return (HYIDouble)getFieldValue("AMT23");
    }

    public void setAmount23(HYIDouble amt23) {
        setFieldValue("AMT23", amt23);
    }

//Quantity23
    public Integer getQuantity23() {
        return (Integer)getFieldValue("QTY23");
    }

    public void setQuantity23(Integer qty23) {
        setFieldValue("QTY23", qty23);
	}

	public void setAmtByTime(HYIDouble salesAmt, int qty) {
	    Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat ("HH");
		String nowHour = df.format(date);
		HYIDouble amt = (HYIDouble)getFieldValue("AMT" + nowHour);
		//System.out.println("AMT" + nowHour);
		Integer quantity = (Integer)getFieldValue("QTY" + nowHour);
		/*System.out.println(this.getValues());
		System.out.println(salesAmt);
		System.out.println(amt);*/
		setFieldValue("AMT" + nowHour, salesAmt.add(amt));
		qty = qty + quantity.intValue();
		setFieldValue("QTY" + nowHour, new Integer(qty));
	}

	/*
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();Calendar call = Calendar.getInstance();
		System.out.println(cal.get(cal.SECOND));
		try {
		    Thread.currentThread().sleep(2000);
		} catch (Exception e) {}
		System.out.println(cal.get(cal.SECOND));
		try {
			Thread.currentThread().sleep(2000);
		} catch (Exception e) {}
		
		System.out.println(call.get(cal.SECOND));
	}*/
}

 
