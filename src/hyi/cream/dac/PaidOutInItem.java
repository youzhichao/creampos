package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PaidOutInItem extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

	static final String tableName = "paidoutinitem";
	private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("ID");
	}

    /**
     * Constructor
     */
    public PaidOutInItem() {
    }

	public List getPrimaryKeyList() {
		return  primaryKeys;
	}

	public static PaidOutInItem queryByID(DbConnection connection, String ID) {
		try {
            return getSingleObject(connection, PaidOutInItem.class,
            	"SELECT * FROM " + tableName + " WHERE ID='" + ID + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}

	public static ArrayList queryPaidType(DbConnection connection, int paidType) {
        ArrayList paidItem = new ArrayList();
        if (paidType == 0) {
            for (int i = 0; i < 5; i++) {
                paidItem.add(queryByID(connection, String.valueOf(i)));
            }
        } else {
            paidItem.add(queryByID(connection, "A")); 
            paidItem.add(queryByID(connection, "B"));
            paidItem.add(queryByID(connection, "C"));
            paidItem.add(queryByID(connection, "D"));
            paidItem.add(queryByID(connection, "E"));
        }               
        return paidItem;
	}

    public static Iterator queryAll(DbConnection connection) {
        try {
            return getMultipleObjects(connection, PaidOutInItem.class,
                "SELECT * FROM " + tableName);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

	public String getID() {
		return (String)getFieldValue("ID");
	}

	public String getPluNumber() {
		return (String)getFieldValue("PLUNO");
	}

	public String getInsertUpdateTableName() {
		return tableName;
	}
}