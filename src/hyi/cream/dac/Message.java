package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Message class.
 *
 * @author Bruce
 * @version 1.5
 * 2004.03.18
 * 全家功能：如果出现新的总部发布的通报,
 *           由于要知道来源，因此需要type字段
 */
public class Message extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    transient public static final String VERSION = "1.5";

    static final String tableName = "commdl_message";
    private static ArrayList primaryKeys = new ArrayList();
    private static boolean enableMessageDownload;
    private static List emptyList = new ArrayList();
    private static String dbType = "PostgreSQL"; //GetProperty.getDBType("MySql");
    
    static {
        primaryKeys.add("id");
        primaryKeys.add("source");
        
        String s = GetProperty.getEnableMessageDownload("");
        if (s.equalsIgnoreCase("yes"))
            enableMessageDownload = true;
    }

    /**
     * Constructor
     */
    public Message() {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    // properties

    public int getId() {
        return ((Integer)this.getFieldValue("id")).intValue();
    }

    public String getSource() {
        return (String)this.getFieldValue("source");
    }

    public String getMessage() {
        if (dbType.equalsIgnoreCase("Sybase"))
        	return (String)this.getFieldValue("attachment");
        else 
        	return (String)this.getFieldValue("message");
    }

    // for Serializable
    public String getInsertUpdateTableName() {
        return tableName;
    }
    
	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("id", "id");
        fieldNameMap.put("source", "source");
        if (dbType.equalsIgnoreCase("Sybase"))
        	fieldNameMap.put("attachment", "attachment");
        else
        	fieldNameMap.put("message", "message");
        return fieldNameMap;	
	}

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();

            if (!enableMessageDownload)
                return emptyList;         
            String sql = "";
            if (dbType.equalsIgnoreCase("Sybase"))
            	sql = "SELECT id, source, attachment FROM commdl_message"
                   	+ " WHERE downloadtopos = '1' AND status = '1'"
    				+ " AND"
    				+ " (NOW() BETWEEN displayBeginDate AND displayEndDate) ORDER BY id DESC";
            else
            	sql = 
    	            "SELECT id, source, message FROM commdl_message"
                    + " WHERE downloadtopos = '1' AND status = '1'"
                    + " AND"
                    + " (NOW() BETWEEN displayBeginDate AND displayEndDate"
                    + "  OR (DATE_FORMAT(displayBeginDate, '%Y-%m-%d') = '1970-01-01'"
                    + "     AND (TIME_FORMAT(NOW(), '%H:%i:%s') BETWEEN"
                    + "          TIME_FORMAT(displayBeginDate, '%H:%i:%s')"
                    + "          AND TIME_FORMAT(displayEndDate, '%H:%i:%s')"
                    + "         )"
                    + "     )"
                    + " )";
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.Message.class, sql,
                getScToPosFieldNameMap());

        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}
