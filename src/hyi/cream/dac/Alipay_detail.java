package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * 支付宝交易明细alipay_detail
 * @author Administrator
 *
 */
public class Alipay_detail extends DacBase implements Serializable {

	static final String tableName = "alipay_detail";
	private static ArrayList primaryKeys = new ArrayList();

	static {
        primaryKeys.add("storeID");
        primaryKeys.add("posNumber");
        primaryKeys.add("transactionNumber");
	}

	public Alipay_detail() throws InstantiationException {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	
	public String getInsertUpdateTableName() {
		if (Server.serverExist()) {
            return "posul_alipay_detail";
        }
		return tableName;
	}
	
	public static String getInsertUpdateTableNameStaic() {
        if (Server.serverExist()) {
            return "posul_alipay_detail";
        }
        return "alipay_detail";
    }
	
	public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
            {"storeID", "storeID"},
            {"posNumber", "posNumber"},
            {"transactionNumber", "transactionNumber"},
            {"zseq", "zseq"},
            {"TOTAL_FEE", "TOTAL_FEE"},
            {"TRADE_NO","TRADE_NO"},
            {"systemDate","systemDate"},
            {"SEQ","SEQ"},
            {"BUYER_LOGON_ID","BUYER_LOGON_ID"},
		};
	}
	
	//STORENUMBER,店号
	public void setSTORENUMBER(String storeNumber) {
		 setFieldValue("storeID",storeNumber);
	}
    public String getSTORENUMBER() {
        return (String)getFieldValue("storeID");
    }
	//POSNUMBER, 机号
	public void setPOSNUMBER(Integer posNumber) {
		setFieldValue("posNumber",posNumber);
	}
    public Integer getPOSNUMBER() {
        return (Integer)getFieldValue("posNumber");
    }
	//TRANSACTIONNUMBER, 交易序号
	public Integer getTRANSACTIONNUMBER() {
		return (Integer) getFieldValue("transactionNumber");
	}
	public void setTRANSACTIONNUMBER(Integer employeeId) {
		setFieldValue("transactionNumber",employeeId);
	}
	//zseq, z帐序号
	public Integer getZSeq() {
		return (Integer) getFieldValue("zseq");
	}
	public void setZSeq(Integer employeeId) {
		setFieldValue("zseq",employeeId);
	}

	//Tatal_fee,交易金额
	public HYIDouble getTOTAL_FEE() {
		return (HYIDouble) getFieldValue("TOTAL_FEE");
	}
	public void setTOTAL_FEE(HYIDouble temperature) {
		setFieldValue("TOTAL_FEE",temperature);
	}
	
	//TRADE_NO，支付宝订单号
	public String getTRADE_NO() {
		return (String) getFieldValue("TRADE_NO");
	}
	public void setTRADE_NO(String updateDateTime) {
		setFieldValue("TRADE_NO",updateDateTime);
	}

    //SYSTEMDATE,时间
    public void setSYSTEMDATE(Date date) {
        setFieldValue("systemDate",date);
    }
    public Date getSYSTEMDATE() {
        return (Date)getFieldValue("systemDate");
    }

    //流水号

    public void setSEQ(String s){
        setFieldValue("SEQ",s);
    }
    public String getSEQ() {
        return (String)getFieldValue("SEQ");
    }
    //BUYER_LONGON_ID支付宝帐号
    public String getBUYER_LOGON_ID() {
        return (String)getFieldValue("BUYER_LOGON_ID");
    }

    public void setBUYER_LOGON_ID(String s){
        setFieldValue("BUYER_LOGON_ID",s);
    }

    public static Iterator queryByTranNo(DbConnection connection, int tranNo) {
        try {
            return getMultipleObjects(connection, Alipay_detail.class,"select * from " + getInsertUpdateTableNameStaic() + " where  transactionNumber = " + tranNo);
        } catch (EntityNotFoundException e) {
//            CreamToolkit.logMessage(e);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        }
        return null;
    }

    public static Alipay_detail queryByStoreIdTranNoPosNo(DbConnection connection,String storeId,String tranNo,String posId) {
        try {
            return getSingleObject(connection, Alipay_detail.class,"select * from " + getInsertUpdateTableNameStaic()
                    + " where storeID = '" + storeId + "' and transactionNumber = " + tranNo + " and posNumber = " + posId);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } catch (EntityNotFoundException e) {
//            CreamToolkit.logMessage(e);
        }
        return null;
    }

    public Alipay_detail cloneForSC() {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            Alipay_detail clonedObj = new Alipay_detail();
            for (String[] aFieldNameMap : fieldNameMap) {
                Object value = this.getFieldValue(aFieldNameMap[0]);
                if (value == null) {
                    try {
                        value = this.getClass().getDeclaredMethod(
                                "get" + aFieldNameMap[0], new Class[0]).invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedObj.setFieldValue(aFieldNameMap[1], value);
            }
            return clonedObj;
        } catch (InstantiationException e) {
            return null;
        }
    }

    static public void deleteOutdatedData() {
        DbConnection connection = null;
        Statement statement = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            if (hyi.cream.inline.Server.serverExist()) {
                // 需要保留的天数
                int tranReserved = PARAM.getTransactionReserved();
                long l = new java.util.Date().getTime() - tranReserved * 1000L * 3600 * 24;
                String baseTime = CreamCache.getInstance().getDateTimeFormate().format(new java.sql.Date(l));
                statement.executeUpdate("DELETE FROM posul_alipay_detail WHERE systemDate < '" + baseTime + "'");
            } else {
                Iterator<String> itr = Transaction.getOutdateTranNumbers();
                while (itr != null && itr.hasNext())
                    statement.executeUpdate("DELETE FROM alipay_detail WHERE transactionNumber=" + itr.next());
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
    }
}
