package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DBToolkit;
import hyi.cream.util.HYIDouble;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.math.BigDecimal;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Inventory class.
 *
 * @author Bruce
 * @version 1.0
 */
public class Inventory extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    transient public static final String VERSION = "1.0";

    private static ArrayList primaryKeys = new ArrayList();
    private String description;

    //private String storeID;
    //private Date busiDate;
    private Integer itemSequence;
    //private String stockNumber;
    private String itemNumber;
    private HYIDouble actStockQty;
    private HYIDouble storeUnitPrice;
    private HYIDouble totalPriceAmt;
    private boolean remove;

    public boolean isRemove() {
        return remove;
    }

    public void setRemove(boolean remove) {
        this.remove = remove;
    }

    private static Object[][] inventoryFields = {
        {"storeID", String.class},
        {"busiDate", java.sql.Date.class},
        {"itemSequence", Integer.class},
        {"inventoryNumber", String.class},
        {"posNumber", Integer.class},
        {"stockNumber", String.class},
        {"itemNumber", String.class},
        {"actStockQty", HYIDouble.class},
        {"actGiftQTY", HYIDouble.class},
        {"storeUnitPrice", HYIDouble.class},
        {"cost", HYIDouble.class},
        {"totalCostAmt", HYIDouble.class},
        {"totalPriceAmt", HYIDouble.class},
        {"uploadState", String.class},
        {"updateUserID", String.class},
        {"updateDateTime", java.sql.Date.class},
        {"dtlcode1", String.class},
    };

    static {
        primaryKeys.add("itemSequence");
    }

    public Inventory() {
    }

    private HYIDouble trimTrailingZero(HYIDouble x) {
        String xv = x.toString();
        if (xv.lastIndexOf(".") != -1) {
            int i = xv.length() - 1;
            while (xv.charAt(i) == '0') {
                xv = xv.substring(0, i);
                i--;
            }
            return new HYIDouble(xv);
        } else {
            return x;
        }
    }
    
    public Object clone() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            //Bruce/2003-12-12
            Inventory inv =  queryByItemSequence(connection, "" + getItemSequence());
            PLU plu = PLU.queryByInStoreCode("" + inv.getItemNumber());
            if (plu != null)
                inv.setDescription(plu.getScreenName());
            inv.setUnitPrice(inv.getUnitPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
            inv.setActStockQty(trimTrailingZero(inv.getActStockQty()));
            //inv.setAmount(inv.getAmount().setScale(2, HYIDouble.ROUND_HALF_UP));
            inv.setUpdateDateTime(new Date());
            inv.setRemove(isRemove());
            return inv;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public String toString() {
        return "Inventory(POS=" + this.getPOSNumber() + ", Seq=" 
            + this.getItemSequence().toString() + ", ItemNo=" 
            + this.getItemNumber() + ", Qty="
            + this.getActStockQty() + ")";
    }
    
    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_inventory";
        else
            return "inventory";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_inventory";
        else
            return "inventory";
    }

    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public static Inventory queryByItemSequence(DbConnection connection, String itemNo) {
        try {
            return getSingleObject(connection, Inventory.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE itemSequence=" + itemNo);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static int queryMaxItemSequence() throws SQLException {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Object maxItemSeq = getValueOfStatement(connection, "SELECT MAX(itemSequence) AS ID FROM inventory");
            return (maxItemSeq == null) ? 0 : CreamToolkit.retrieveIntValue(maxItemSeq); 
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            throw e;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static int queryNextItemSequence() throws SQLException {
        return queryMaxItemSequence() + 1;
    }

//    public static int queryRecordCount() {
//        Object count = getValueOfStatement("SELECT COUNT(*) AS RECCNT FROM inventory");
//        return (count == null) ? 0 : CreamToolkit.retrieveIntValue(count); 
//    }
//
//    public static int queryRecordCountNotUploaded() {
//        Object count = getValueOfStatement("SELECT COUNT(*) AS RECCNT FROM inventory WHERE uploadState != '1'");
//        return (count == null) ? 0 : CreamToolkit.retrieveIntValue(count); 
//    }

    public static Iterator queryAll(DbConnection connection) {
        try {
            return getMultipleObjects(connection, Inventory.class, "SELECT * FROM inventory");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static Iterator queryUnloadRecords(DbConnection connection) {
        try {
            return getMultipleObjects(connection, Inventory.class,
                "SELECT * FROM inventory WHERE uploadState != '1'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    /**
     * 将显示会用到的DAC fields写到class fields，并将原DAC中的field map丢弃，
     * 以节约memory overhead。
     */
    public void discardFieldMap() {
        //storeID = (String)getFieldValue("storeID");
        //busiDate = (Date)getFieldValue("busiDate");
        itemSequence = (Integer)getFieldValue("itemSequence");
        //posNumber = Integer.parseInt(getFieldValue("posNumber").toString());
        //stockNumber = (String)getFieldValue("stockNumber");
        itemNumber = (String)getFieldValue("itemNumber");
        actStockQty = (HYIDouble)getFieldValue("actStockQty");
        storeUnitPrice = (HYIDouble)getFieldValue("storeUnitPrice");
        totalPriceAmt = (HYIDouble)getFieldValue("totalPriceAmt");
        //uploadState = (String)getFieldValue("uploadState");
        
        clearFieldMap();
    }

    // properties

    public String getStoreID() {
        return (String)getFieldValue("storeID");
    }

    public void setStoreID(String storeID) {
        setFieldValue("storeID", storeID);
    }

    public Date getBusiDate() {
        return (Date)getFieldValue("busiDate");
    }

    public void setBusiDate(java.util.Date busiDate) {
        setFieldValue("busiDate", busiDate);
    }

    public Integer getItemSequence() {
        return (itemSequence != null) ? itemSequence : (Integer)getFieldValue("itemSequence");
    }

    public void setItemSequence(int seq) {
        setFieldValue("itemSequence", new Integer(seq));
    }

    public int getPOSNumber() {
        return Integer.parseInt(getFieldValue("posNumber").toString());
    }

    public void setPOSNumber(int posNumber) {
        setFieldValue("posNumber", new Integer(posNumber));
    }

    public String getStockNumber() {
        return (String)getFieldValue("stockNumber");
    }

    public void setStockNumber(String stockNumber) {
        setFieldValue("stockNumber", stockNumber);
    }

    public String getItemNumber() {
        return (itemNumber != null) ? itemNumber : (String)getFieldValue("itemNumber");
    }

    public void setItemNumber(String itemNumber) {
        setFieldValue("itemNumber", itemNumber);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HYIDouble getActStockQty() {
        return (actStockQty != null) ? actStockQty : (HYIDouble)getFieldValue("actStockQty");
    }

    public void setActStockQty(HYIDouble actStockQty) {
        setFieldValue("actStockQty", actStockQty);
    }

    //public HYIDouble getActGiftQTY() {
    //    return (HYIDouble)getFieldValue("actGiftQty");
    //}
    //
    //public void setActGiftQTY(HYIDouble actGiftQty) {
    //    setFieldValue("actGiftQty", actGiftQty);
    //}

    public HYIDouble getUnitPrice() {
        return (storeUnitPrice != null) ? storeUnitPrice : (HYIDouble)getFieldValue("storeUnitPrice");
    }

    public void setUnitPrice(HYIDouble unitprice) {
        setFieldValue("storeUnitPrice", unitprice);
    }

    //public HYIDouble getCost() {
    //    return (HYIDouble)getFieldValue("cost");
    //}
    //
    //public void setCost(HYIDouble cost) {
    //    setFieldValue("cost", cost);
    //}

    public HYIDouble getAmount() {
        return (totalPriceAmt != null) ? totalPriceAmt : (HYIDouble)getFieldValue("totalPriceAmt");
    }

    public void setAmount(HYIDouble amount) {
        setFieldValue("totalPriceAmt", amount);
    }

    public String getUploadState() {
        return (String)getFieldValue("uploadState");
    }

    public void setUploadState(String updateState) {
        setFieldValue("uploadState", updateState);
    }

    public String getUpdateUserID() {
        return (String)getFieldValue("updateUserID");
    }

    public void setUpdateUserID(String updateUserId) {
        setFieldValue("updateUserID", updateUserId);
    }

    public java.util.Date getUpdateDateTime() {
        return (java.util.Date)getFieldValue("updateDateTime");
    }

    public void setUpdateDateTime(java.util.Date updateDateTime) {
        setFieldValue("updateDateTime", updateDateTime);
    }

    public static boolean updateUploadState(String status) {
    	return DBToolkit.execute("UPDATE inventory SET uploadState='" + status + "' WHERE uploadState<>'1'");
    }

	public String getDtlcode1() {
		return (String) getFieldValue("dtlcode1");
	}

	public void setDtlcode1(String dtlcode1) {
		setFieldValue("dtlcode1", dtlcode1.trim());
	}

    /**
     * Clone Inventory objects for SC with converted field names.
     *
     * This method is only used at POS side.
     */
    public static Object[] cloneForSC(Iterator iter) {
        ArrayList objArray = new ArrayList();
        while (iter.hasNext())
            objArray.add(iter.next());

        return objArray.toArray();
    }

    /**
     * Get inventory data by the following format:
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  ...
     */
    public static String getTabSeperatedData() {
        return getTabSeperatedData("hyi.cream.dac.Inventory", 
            "SELECT storeID, busiDate, itemSequence, inventoryNumber, posNumber, stockNumber, itemNumber, actStockQty," +
            "actGiftQTY, storeUnitPrice, cost, totalCostAmt, totalPriceAmt, uploadState, updateUserID, updateDateTime, dtlcode1 FROM inventory");
    }

    /**
     * Get the unsent inventory data by the following format:
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  ...
     */
    public static String getUnsentTabSeperatedData() {
        return getTabSeperatedData("hyi.cream.dac.Inventory", 
            "SELECT storeID, busiDate, itemSequence, inventoryNumber, posNumber, stockNumber, itemNumber, actStockQty," +
            "actGiftQTY, storeUnitPrice, cost, totalCostAmt, totalPriceAmt, uploadState, updateUserID, updateDateTime, dtlcode1 FROM inventory" +
            " WHERE uploadState != '1'");
    }

    /**
     * Construct DacBase objects from the record data by the format:
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  ...
     * 
     * @param sThread ServerThread used to call its processObjectGot() after constructing
     *                 an Inventory object.
     * @param data A String with Inventory data in above format.
     */
    public static void constructFromTabSeperatedData(
        hyi.cream.inline.ServerThread sThread, String data) {

        String line;
        BufferedReader reader = new BufferedReader(new StringReader(data));

        try {
            Object[] inventoryDac = new Object[1];
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("hyi.cream.dac.Inventory"))
                    inventoryDac[0] = constructFromTabSeperatedData(line, inventoryFields);
                if (inventoryDac[0] != null)
                    sThread.processObjectGot(inventoryDac); // this will insert into database
            }
        } catch (IOException e) {
            CreamToolkit.logMessage(e);
        }
    }
}
