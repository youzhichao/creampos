package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Cashier definition class.
 * 
 * @author Slackware, Bruce
 * @version 1.5
 */
public class Cashier extends DacBase implements Serializable {// Read only
    private static final long serialVersionUID = 1L;

	/**
	 * /Bruce/1.5/2002-03-10/ Add/Modify some methods for preparing to use in n
	 * ew inline: Add: public static Collection getAllObjectsForPOS()
	 */
	transient public static final String VERSION = "1.5";

	static final String tableName = "cashier";

	transient static private Set cache;

	private static ArrayList primaryKeys = new ArrayList();

	static {
		if (!hyi.cream.inline.Server.serverExist()) {
			primaryKeys.add("CASNO");
			createCache();
		}
	}

	// constructor
	public Cashier() {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public static void createCache() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Iterator itr = getMultipleObjects(connection, Cashier.class, "SELECT * FROM " + tableName);
            cache = new HashSet();
            while (itr.hasNext())
                cache.add(itr.next());
        } catch (EntityNotFoundException e) {
            cache = null;
        } catch (SQLException e) {
            cache = null;
            CreamToolkit.logMessage(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

	public static Cashier queryByCashierID(String cashierID) {	
		/*
		 * return (Cashier)getSingleObject(Cashier.class, "SELECT * FROM " +
		 * tableName + " WHERE CASNO='" + cashierID + "'");//
		 */
		if (cache == null)
			return null;
		Iterator itr = cache.iterator();
		while (itr.hasNext()) {
			Cashier p = (Cashier) itr.next();
			if (p.getCashierNumber().equals(cashierID)) {
				return p;
			}
		}
		return null;
	}

	public static boolean checkDigitCashierID(String cashierID) {
		String chechStr;
		int beginIndex, sum = 0;
		boolean isValid = false;

		beginIndex = cashierID.charAt(0) - 48;
		if (beginIndex < 0)
			return false;

		try {

			/* 校验位前的奇数位数字×3 ＋ 偶数位数字×1 ＋ 校验位之和的个位数为0 */
			chechStr = String.valueOf(cashierID.charAt(cashierID.length()-1));
			for (int i = 0; i < cashierID.length()-1; i++) {
				if ((i + 1) % 2 == 0)
					sum += cashierID.charAt(i) - 48; // '0'的ASCII码为48
				else
					sum += 3 * (cashierID.charAt(i) - 48);
			}
			sum += Integer.parseInt(chechStr);
			if (sum % 10 == 0)
				isValid = true;
		} catch (StringIndexOutOfBoundsException e) {
			e.printStackTrace(hyi.cream.util.CreamToolkit.getLogger());
		} catch (NumberFormatException e) {
			e.printStackTrace(hyi.cream.util.CreamToolkit.getLogger());
			
		}
		return isValid;
	}

	public static Cashier CheckValidCashier(String cashierID, String password,
			String level) {
		if (cache == null)
			return null;
		Iterator itr = cache.iterator();
		while (itr.hasNext()) {
			Cashier p = (Cashier) itr.next();
			if (p.getCashierNumber().equals(cashierID)) {
				if (p.getPassword().equals(password))
					if (Integer.parseInt(p.getCashierLevel()) <= Integer
							.parseInt(level))
						return p;
			}
		}
		return null;
	}

	public static Cashier CheckValidCashier(String cashierID, String password) {
		/*
		 * return (Cashier)getSingleObject(Cashier.class, "SELECT * FROM " +
		 * tableName + " WHERE CASNO='" + cashierID + "'");//
		 */
		if (cache == null)
			return null;
		Iterator itr = cache.iterator();
		while (itr.hasNext()) {
			Cashier p = (Cashier) itr.next();
			if (p.getCashierNumber().equals(cashierID)) {
				if (p.getPassword().equals(password))
					return p;
			}
		}
		return null;
	}

	// end of static block

	// Override equals() of Object;
	public boolean equals(Object obj) {
		if (!(obj instanceof Cashier))
			return false;
		return getCashierNumber().equals(((Cashier) obj).getCashierNumber());
	}

    public static void updateCashierRightsInCache(String cashierID, String rights) {
        System.out.println("Cashier.updateCashierRightsInCache(): prepare to set cashier "
            + cashierID + "'s rights to " + rights);
        if (cache == null) {
            System.out.println("Cashier.updateCashierRightsInCache(): but cache is empty.");
            return;
        }
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            Cashier p = (Cashier) itr.next();
            if (p.getCashierNumber().equals(cashierID)) {
                System.out.println("Cashier.updateCashierRightsInCache(): set cashier "
                    + cashierID + "'s rights to " + rights);
                p.setCashierRights(rights);
                break;
            }
        }
    }

	// Properties
	public String getCashierNumber() {
		return (String) getFieldValue("CASNO");
	}

	public String getCashierName() {
		return (String) getFieldValue("CASNAME");
	}

	public String getPassword() {
		return (String) getFieldValue("CASPASS");
	}

	public String getCashierLevel() {
		return (String) getFieldValue("CASLEVEL");
	}

	public String getInsertUpdateTableName() {
		return tableName;
	}

    public String getCashierRights() {
        return (String)getFieldValue("CASRIGHTS");
    }

    public void setCashierRights(String rights) {
        setFieldValue("CASRIGHTS", rights);
    }

	/**
	 * Meyer/2003-02-20 return fieldName map of PosToSc as Map
	 */
	public static Map getScToPosFieldNameMap() {
		Map fieldNameMap = new HashMap();
		fieldNameMap.put("cashierID", "CASNO");
		fieldNameMap.put("cashierName", "CASNAME");
		fieldNameMap.put("cashierPassword", "CASPASS");
		fieldNameMap.put("cashierLevel", "CASLEVEL");
		// 2003/02/09, Added by Meyer
		fieldNameMap.put("cashierRights", "CASRIGHTS");

		return fieldNameMap;
	}

	/**
	 * Get all data for downloading to POS. This methid is used by inline
	 * server.
	 */
	public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.Cashier.class,
            		"SELECT * FROM posdl_cashier", getScToPosFieldNameMap());
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
	}
}
