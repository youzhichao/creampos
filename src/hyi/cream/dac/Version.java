package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Version extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("posVersion");

	}

	public Version() {

	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (hyi.cream.inline.Server.serverExist())
			return "posul_posversion";
		else
			return "posversion";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (hyi.cream.inline.Server.serverExist())
			return "posul_posversion";
		else
			return "posversion";
	}

	public static Iterator getAllVersion() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return getMultipleObjects(connection, Version.class, "SELECT * FROM "
            		+ getInsertUpdateTableNameStaticVersion()
            		);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
	}

	/**
	 * Clone Version objects for SC
	 * 
	 * This method is only used at POS side.
	 */
	public static Object[] cloneForSC(Iterator iter) {
		String[][] fieldNameMap = getPosToScFieldNameArray();

		ArrayList objArray = new ArrayList();
		while (iter.hasNext()) {
			Version vs = (Version) iter.next();
			Version clonedVS = new Version();
			for (int i = 0; i < fieldNameMap.length; i++) {
				Object value = vs.getFieldValue(fieldNameMap[i][0]);
				if (value == null) {
					try {
						value = vs.getClass().getDeclaredMethod(
								"get" + fieldNameMap[i][0], new Class[0])
								.invoke(vs, new Object[0]);
					} catch (Exception e) {
						value = null;
					}
				}
				clonedVS.setFieldValue(fieldNameMap[i][1], value);
			}
			objArray.add(clonedVS);
		}
		return objArray.toArray();
	}

	/**
	 * return fieldName map of PosToSc as String[][]
	 */
	public static String[][] getPosToScFieldNameArray() {
		return new String[][] { { "posNumber", "posnumber" },
				{ "posVersion", "posversion" }, { "storeID", "storeid" },

		};
	}

	public void setVersion(String posversion) {
		setFieldValue("posVersion", posversion);
	}

	public String getVersion() {
		return ((String) getFieldValue("posVersion"));
	}

	public void setStoreID(String storeid) {
		setFieldValue("storeID", storeid);

	}

	public String getStoreID() {
		return (String) getFieldValue("storeID");
	}

	public void setPosnumber(String posnumber) {
		setFieldValue("posNumber", posnumber);
	}

	public String getPosnumber() {
		return  (String)getFieldValue("posNumber");
		//return ((Integer) getFieldValue("PosNumber")).intValue();
	}

	public static void main(String[] args) {
	}

	// putAttendance(null);
	// putAttendance(121);
	/**
	 * 从tranhead-->attendance return iterator
	 */
	public static void genVersion() {
        DbConnection connection = null;
		try {
            connection = CreamToolkit.getTransactionalConnection();
			Version version = new Version();
			version.setPosnumber(PARAM.getTerminalNumber() + "");
			version.setVersion(hyi.cream.Version.getVersion());
			version.setStoreID(Store.getStoreID());
			if (!version.exists(connection))
				version.insert(connection);
            else
                version.update(connection);
            connection.commit();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
	}

	public static Iterator getCurrentVersion() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            List list = DacBase.getMultipleObjects(connection, Version.class, "SELECT * FROM "
    			+ getInsertUpdateTableNameStaticVersion(), null);
            return list.iterator();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }

//		List versions = new ArrayList();
//		for (Iterator it = list.iterator(); it.hasNext();) {
//			System.out.println("genposversion legth : " + list.size());
//			try {
//				Version version = new Version();
//				version = (Version) it.next();
//				version.getVersion();
//				version.getStoreID();
//				version.getPosnumber();
//				versions.add(version);
//			} catch (InstantiationException e) {
//
//			}
//
//		}
//		return versions.iterator();
	}
}
