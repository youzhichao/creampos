package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * CombPromotionHeader definition class.
 *
 * @author Meyer 
 * @version 1.0
 */
public class CombPromotionHeader extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 更新记录
     * 
     * Meyer / 1.0 / 2003-01-02
     *         1. Create the class file
     * 
     */
    transient public static final String VERSION = "1.0";

    static final String tableName = "combination_promotion_header";
    private static ArrayList primaryKeys = new ArrayList();
    transient static private Map cache;

    static {
        primaryKeys.add("CPH_ID");
        createCache();
    }

    public static void createCache() {
        if (!GetProperty.getCombPromotionHeaderUserCache().equalsIgnoreCase("true") 
                || hyi.cream.inline.Server.serverExist())
            return;

        cache = new HashMap();
        Calendar now = Calendar.getInstance();
        Calendar bef = Calendar.getInstance();
        bef.set(Calendar.DAY_OF_YEAR, now.get(Calendar.DAY_OF_YEAR) - 1);
//        Calendar aft = Calendar.getInstance();
//        aft.set(Calendar.DAY_OF_YEAR, now.get(Calendar.DAY_OF_YEAR) + 1);
        String sql = "SELECT * FROM "
            + tableName + " WHERE " +
//            "CPH_SALES_SDATE <= '"
//            + CreamCache.getInstance().getDateFormate2().format(bef.getTime())
//            + "' AND" +
            " CPH_SALES_EDATE > '"
            + CreamCache.getInstance().getDateFormate2().format(bef.getTime())
            + "'";
        CreamToolkit.logMessage("CombPromotionHeader Cache Sql : " + sql);

        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Iterator itr = getMultipleObjects(connection, CombPromotionHeader.class, sql);
            int count = 0;
            while (itr.hasNext()) {
                CombPromotionHeader cph = (CombPromotionHeader) itr.next();
                cache.put(cph.getCphID(), cph);
                count++;
            }
            CreamToolkit.logMessage("CombPromotionHeader Cache Count : " + count);

        } catch (EntityNotFoundException e) {
            cache = null;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            cache = null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Constructor
     */
    public CombPromotionHeader() {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static CombPromotionHeader queryByID(String cphID) {
        if (GetProperty.getCombPromotionHeaderUserCache().equalsIgnoreCase("true"))
            return (CombPromotionHeader)cache.get(cphID);
        else {
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getPooledConnection();
                return getSingleObject(connection, CombPromotionHeader.class,
                    " SELECT * FROM " + tableName + 
                    " WHERE CPH_ID='" + cphID + "'");
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                return null;
            } catch (EntityNotFoundException e) {
                return null;
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
        }
    }

    public String getCphID() {
        return (String)getFieldValue("CPH_ID");
    }

    public Date getBeginDate() {
        return (Date)getFieldValue("CPH_SALES_SDATE");
    }

    public Date getEndDate() {
        return (Date)getFieldValue("CPH_SALES_EDATE");
    }

    public Date getBeginTime() {
        return (Date)getFieldValue("CPH_DISCOUNT_SDATE");
    }

    public Date getEndTime() {
        return (Date)getFieldValue("CPH_DISCOUNT_EDATE");
    }

    public Date getBeginTime2() {
        return (Date)getFieldValue("CPH_DISCOUNT_SDATE2");
    }
    
    public Date getEndTime2() {
        return (Date)getFieldValue("CPH_DISCOUNT_EDATE2");
    }

    public boolean isMondayMatch() {
        return "1".equals((String)getFieldValue("CPH_MONDAY"));
    }

    public boolean isTuesdayMatch() {
        return "1".equals((String)getFieldValue("CPH_TUESDAY"));
    }

    public boolean isWednesdayMatch() {
        return "1".equals((String)getFieldValue("CPH_WEDNESDAY"));
    }
    
    public boolean isThursdayMatch() {
        return "1".equals((String)getFieldValue("CPH_THURSDAY"));
    }
       
    public boolean isFridayMatch() {
        return "1".equals((String)getFieldValue("CPH_FRIDAY"));
    }
       
    public boolean isSaturdayMatch() {
        return "1".equals((String)getFieldValue("CPH_SATURDAY"));
    }
       
    public boolean isSundayMatch() {
        return "1".equals((String)getFieldValue("CPH_SUNDAY"));
    }

    /**
     * 折让方式（对应COMBINATION_DISCOUNT_TYPE档)
     */
    public String getDiscountType() {
        return (String)getFieldValue("CPH_DISCOUNT_TYPE");
    }

    /**
     * 打折最小销售金额
     */
    public HYIDouble getMinSalesAmount() {
        return (HYIDouble)getFieldValue("CPH_SALES_AMOUNT");
    }
    
    /**
     * 折让金额
     */
    public HYIDouble getDiscountAmount() {
        return (HYIDouble)getFieldValue("CPH_DISCOUNT_AMOUNT");
    }

    /**
     * 促销优惠折扣比例为一整数,表示折扣的百分比数
     *         如 10 表示 10%
     *
     */
    public Integer getDiscountPercentage() {
        return (Integer)getFieldValue("CPH_DISCOUNT_PERCENTAGE");
    }

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Map fieldNameMap = null;
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.CombPromotionHeader.class,
                "SELECT * FROM posdl_combination_promotion_header", fieldNameMap);
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}

