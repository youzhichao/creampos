package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * CombSaleDetail definition class.
 *
 * @author Meyer 
 * @version 1.0
 */
public class CombSaleDetail extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Meyer / 1.0 / 2003-01-02
     * 		1. Create the class file
     * 
     * 
     */
    
    transient public static final String VERSION = "1.0";

    static final String tableName = "combination_sale_detail";
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("CSD_CPH_ID");
        primaryKeys.add("CSD_ARTICLE_ID");
    }

    /**
     * Constructor
     */
    public CombSaleDetail() {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static CombSaleDetail queryByID(DbConnection connection, String cphID, String articleID) {
        try {
            return getSingleObject(connection, CombSaleDetail.class,
                " SELECT * FROM " + tableName + 
                " WHERE CSD_CPH_ID='" + cphID + "'" + 
                " AND CSD_ARTICLE_ID='" + articleID + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static List queryCphIDByArticleID(String anArticleID) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            String sql = CreamToolkit.isDatabaseMySQL() ?
                " SELECT DISTINCT a.CSD_CPH_ID" + 
                " FROM combination_sale_detail a,combination_promotion_header b" + 
                " WHERE a.CSD_CPH_ID=b.CPH_ID" + 
                " AND a.CSD_ARTICLE_ID='" + anArticleID + "'" + 
                " AND CURRENT_DATE() BETWEEN b.CPH_SALES_SDATE AND b.CPH_SALES_EDATE" + 
                " AND(CURRENT_TIME() BETWEEN b.CPH_DISCOUNT_SDATE AND b.CPH_DISCOUNT_EDATE" + 
                " 	OR CURRENT_TIME() BETWEEN b.CPH_DISCOUNT_SDATE2 AND b.CPH_DISCOUNT_EDATE2" + 
                " )" + 
                " AND LEFT(SUBSTRING(CONCAT(b.CPH_MONDAY,b.CPH_TUESDAY,b.CPH_WEDNESDAY," + 
                " b.CPH_THURSDAY,b.CPH_FRIDAY,b.CPH_SATURDAY,b.CPH_SUNDAY),WEEKDAY(NOW())+1),1)=1" :
            CreamToolkit.isDatabasePostgreSQL() ?
                "SELECT DISTINCT a.CSD_CPH_ID FROM combination_sale_detail a,combination_promotion_header b " +
                " WHERE a.CSD_CPH_ID=b.CPH_ID AND a.CSD_ARTICLE_ID='" + anArticleID + "' AND " +
                " current_date BETWEEN b.CPH_SALES_SDATE AND b.CPH_SALES_EDATE AND" +
                " (current_time BETWEEN b.CPH_DISCOUNT_SDATE AND b.CPH_DISCOUNT_EDATE OR current_time BETWEEN b.CPH_DISCOUNT_SDATE2 AND b.CPH_DISCOUNT_EDATE2) AND " +
                " substring(b.CPH_SUNDAY||b.CPH_MONDAY||b.CPH_TUESDAY||b.CPH_WEDNESDAY||b.CPH_THURSDAY||b.CPH_FRIDAY||b.CPH_SATURDAY||b.CPH_SUNDAY," +
                " extract(dow from now()::timestamp)::int + 1,1)='1'" :
                "";
            return CombSaleDetail.getMultipleObjects2(connection, CombSaleDetail.class,sql);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
    
    public static Iterator queryByCphID(String aCphID) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return CombSaleDetail.getMultipleObjects(connection,
            	CombSaleDetail.class,
                " SELECT * FROM combination_sale_detail WHERE CSD_CPH_ID='" + aCphID + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static boolean isGroupPromotion(String aCphID) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            CombSaleDetail csDetail =
            	CombSaleDetail.getSingleObject(connection, CombSaleDetail.class,
                	" SELECT * FROM combination_sale_detail WHERE CSD_CPH_ID='" + aCphID + "'");
    		String groupID = csDetail.getGroupID();
    		int groupQty = csDetail.getGroupQty().intValue();
    		return (!groupID.equals("") && groupQty > 0);         
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return false;
        } catch (EntityNotFoundException e) {
            return false;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public String getCphID() {
        return (String)getFieldValue("CSD_CPH_ID");
    }

    public String getPluCode() {
        return (String)getFieldValue("CSD_ARTICLE_ID");
    }

	/**
	 * Meyer/2003-02-21/Modified because of LogicChina's requirement 
	 * 		datatype of CSD_QUANTITY in db cahnge to decimal(10,2)
	 */
    public Integer getQuantity() {
        return new Integer(
        	((HYIDouble)getFieldValue("CSD_QUANTITY")).intValue()
        );
    }

    public String getGroupID() {
    	Object obj = getFieldValue("CSD_GROUP_ID");
    	String groupID = "";    	
    	if (obj != null) {
    		groupID = ((Integer)obj).toString();
    	}
        return groupID;
    }
    
	/**
	 * 
	 * Meyer/2003-02-28/Modified because of LogicChina's requirement 
	 * 		datatype of CSD_GROUP_QTY in db change back to integer
	 * 
	 * 
	 * Meyer/2003-02-21/Modified because of LogicChina's requirement 
	 * 		datatype of CSD_GROUP_QTY in db change to decimal(10,2)
	 */
    public Integer getGroupQty() {
        return (Integer)getFieldValue("CSD_GROUP_QTY");
    }


    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
	        Map fieldNameMap = null;			      
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.CombSaleDetail.class,
            	"SELECT * FROM posdl_combination_sale_detail", fieldNameMap);
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
    
//    /*
//     * 测试方法
//     */
//    public static void main(String [] args) {
//    	CombSaleDetail csd = null;
//    	Iterator csdIter = null;
//
//		System.out.println("*** queryByID: " + args[0] + "/" + args[1]);
//		csd = CombSaleDetail.queryByID(args[0], args[1]);
//    	System.out.println("getCphID " + csd.getCphID());
//    	System.out.println("getPluCode " + csd.getPluCode());
//    	System.out.println("getQuantity " + csd.getQuantity());
//    	System.out.println("getGroupID " + csd.getGroupID());
//    	System.out.println("getGroupQty " + csd.getGroupQty());		
//    	
//    	    	
//		System.out.println("*** queryByCphID: " + args[0]);
//    	csdIter = CombSaleDetail.queryByCphID(args[0]);
//		while (csdIter.hasNext()) {
//	    	csd = (CombSaleDetail)csdIter.next();
//	    	System.out.println("getCphID " + csd.getCphID());
//	    	System.out.println("getPluCode " + csd.getPluCode());
//	    	System.out.println("getQuantity " + csd.getQuantity());
//	    	System.out.println("getGroupID " + csd.getGroupID());
//	    	System.out.println("getGroupQty " + csd.getGroupQty());
//		}
//		
//		
//		System.out.println("*** queryCphIDByArticleID: " + args[1]);
//    	csdIter = CombSaleDetail.queryCphIDByArticleID(args[1]).iterator();
//		while (csdIter.hasNext()) {
//	    	csd = (CombSaleDetail)csdIter.next();
//	    	System.out.println("getCphID " + csd.getCphID());
//		}
//		
//		System.out.println("*** isGroupPromotion: " + args[0]);
//		System.out.println("isGroupPromotion " + CombSaleDetail.isGroupPromotion(args[0]));
//    }
}