package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Discount type (单品折扣率定义档) class.
 *
 * @author Bruce
 */
public class DiscountType extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    static final String tableName = "distype";
//    transient static private Set cache;
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("disNo");
    }

    public DiscountType() {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        if (getUseClientSideTableName())
            return "distype";
        else if (hyi.cream.inline.Server.serverExist())
            return "posdl_distype";
        else
            return "distype";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posdl_distype";
        else
            return "distype";
    }

    public static DiscountType queryByID(String id) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return DacBase.getSingleObject(connection, DiscountType.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() 
                + " WHERE disNo='" + id + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static DiscountType queryByDiscountRate(HYIDouble rate) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Iterator iter = DacBase.getMultipleObjects(connection, DiscountType.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion());
            while (iter != null && iter.hasNext()) {
                DiscountType dis = (DiscountType)iter.next();
                if (rate.compareTo(dis.getUpLimit()) <= 0 
                    && rate.compareTo(dis.getLowLimit()) >= 0) {
                    return dis;
                }
            }
            return null;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    // properties

    public String getID() {
        return (String)this.getFieldValue("disNo");
    }

    public HYIDouble getUpLimit() {
        return (HYIDouble)this.getFieldValue("disUpLimit");
    }

    public HYIDouble getLowLimit() {
        return (HYIDouble)this.getFieldValue("disLowLimit");
    }


//    /**
//     * Return fieldName map from SC to POS. 
//     */
//    public static Map getScToPosFieldNameMap() {
//        Collection dfs = DacBase.getExistedFieldList(getInsertUpdateTableNameStaticVersion());
//        Map fieldNameMap = new HashMap();
//        Iterator iter = dfs.iterator();
//        while (iter.hasNext()) {
//            String fieldName = (String)iter.next();
//            fieldNameMap.put(fieldName, fieldName);
//        }
//        return fieldNameMap;
//    }

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.DiscountType.class,
                "SELECT * FROM posdl_distype", null);
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}
