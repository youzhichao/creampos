package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class Member extends DacBase implements Serializable {//Read only
    private static final long serialVersionUID = 1L;

	static final String tableName = "member";
	private static ArrayList primaryKeys = new ArrayList();
    private boolean needConfirm; 
	
	public List getPrimaryKeyList() {
		return  primaryKeys;
	}

	static {
		primaryKeys.add("idCardNumber");
	}
	
	//constructor
	public Member() {
	}

	public String getInsertUpdateTableName() {
		return tableName;
	}
	
	
	/**
	 * 从SC 中取出数据为下载到POS系统做准备
	 * 
	 * @return Collection
	 */
	public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            String sql = "SELECT * FROM " + tableName;
            return DacBase.getMultipleObjects(connection, Member.class, sql, null);
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
	}
	

	//Get properties value:
	public String getID() {
		return (String)getFieldValue("memberID");
	}

    public String getMemberCardID() {
        return (String)getFieldValue("idCardNumber");
    }

    public String getIdentityID() {
        return (String)getFieldValue("memberemcid");
    }

    public String getTelephoneNumber() {
        return (String)getFieldValue("telNumber");
    }

	public String getName() {
		return (String)getFieldValue("memberName");
	}

    public HYIDouble getMemberTotalBonus() {
        return (HYIDouble)getFieldValue("membertotalbonus");
    }
    
    public HYIDouble getMemberActionBonus() {
        return (HYIDouble)getFieldValue("memberactionbonus");
    }
    
    public Date getMemberEndDate() {
        return (Date)getFieldValue("endDate");
    }
    
    /**
     * 会员卡类型：1 学生 2 教师 3 批发商
     */
    public String getMemberType() {
    	return (String)getFieldValue("memberType");
    }
    public String getMPNumber() {
    	return (String)getFieldValue("MPNumber");
    }

    /**
     * 享受优惠方式: 1-会员价 2-会员折扣 3-售价(可能只积分不折价)
     */
    public String getFavourable() {
    	return (String)getFieldValue("favourable");
    }
    /**
     * 折扣值
     */
    public HYIDouble getDiscount() {
    	return (HYIDouble)getFieldValue("discount");
    }
    public String getSex() {
    	return (String)getFieldValue("sex");
    }
    public String getAddress() {
    	return (String)getFieldValue("address");
    }
    public String getCustMemo() {
    	return (String)getFieldValue("custMemo");
    }
    public String getMemberabnorm_cd() {
    	return (String)getFieldValue("memberabnorm_cd");
    }
    public String getUpdateUserID() {
    	return (String)getFieldValue("updateUserID");
    }
    public Date getUpdateDate() {
    	return (Date)getFieldValue("updateDate");
    }
    public HYIDouble getLastYearRebateAmount() {
    	return (HYIDouble)getFieldValue("lastYearRebateAmount");
    }
    public HYIDouble getLastThisYearRebateAmount() {
    	return (HYIDouble)getFieldValue("thisYearRebateAmount");
    }
    
    /**
     * Query object method. This method will be invoked by ServerThread when
     * receiving "getObject hyi.cream.dac.Member [memberID]" command.
     * 
     * @param data Can be member ID, member card ID, telephone number, or identity ID.
     * @return Collection Return a Collection contains a Member DAC object.
     */
    public static Collection queryObject(String data) {
        List list = new ArrayList();
        Member member;

        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();

            if (data.length() == 12) { // maybe it is a first 12 digits of idCardNumber
                // Try treat it as a idCardNumber...
                try {
                    member = DacBase.getSingleObject(connection, Member.class,
                        "SELECT * FROM commdl_member WHERE updateBeginDate = '1970-01-01' AND "
                            + "sequenceNumber = 1 AND " + "idCardNumber = '" + data
                            + CreamToolkit.getEAN13CheckDigit(data) + "'");
                    list.add(member);
                    return list;
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                } catch (EntityNotFoundException e) {
                }

                try {
                    member = (Member)DacBase.getSingleObject(connection, Member.class,
                        "SELECT * FROM member WHERE idCardNumber='" + data
                            + CreamToolkit.getEAN13CheckDigit(data) + "'");
                    list.add(member);
                    return list;
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                } catch (EntityNotFoundException e) {
                }
            }

            // check as member ID first
            try {
                member = (Member)DacBase.getSingleObject(connection, Member.class,
                    "SELECT * FROM commdl_member WHERE updateBeginDate = '1970-01-01' AND "
                        + "sequenceNumber = 1 AND memberId = '" + data + "'");
                list.add(member);
                return list;
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } catch (EntityNotFoundException e) {
            }

            // then check it as either id card number, member ID, or tel number
            try {
                member = (Member)DacBase.getSingleObject(connection, Member.class,
                    "SELECT * FROM commdl_member WHERE updateBeginDate = '1970-01-01' AND "
                        + "sequenceNumber = 1 AND (" + "idCardNumber = '" + data + "' OR "
                        + "memberId = '" + data + "' OR " + "telNumber = '" + data + "')");
                list.add(member);
                return list;
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } catch (EntityNotFoundException e) {
            }

            // then check it by ignoring updateBeginDate and sequenceNumber
            try {
                member = (Member)DacBase.getSingleObject(connection, Member.class,
                    "SELECT * FROM member WHERE idCardNumber='" + data + "' OR " + "memberId = '"
                        + data + "' OR " + "telNumber = '" + data + "' OR " + "memberemcid='"
                        + data + "'");
                list.add(member);
                return list;
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } catch (EntityNotFoundException e) {
            }

            // Try treat it as a memberID...
            // member = (Member)DacBase.getSingleObject(Member.class,
            // "SELECT * FROM member WHERE memberID='" + data + "'");
            // if (member != null) {
            // list.add(member);
            // return list;
            // }
            //
            //
            // // Try treat it as telNumber...
            // member = (Member)DacBase.getSingleObject(Member.class,
            // "SELECT * FROM member WHERE telNumber='" + data + "'");
            // if (member != null) {
            // list.add(member);
            // return list;
            // }
            //
            // // Try treat it as memberemcid()...
            //        member = (Member)DacBase.getSingleObject(Member.class,
            //            "SELECT * FROM member WHERE memberemcid='" + data + "'");
            //        if (member != null) {
            //            list.add(member);
            //            return list;
            //        }
            return list;

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Query member data by a given ID.
     */
    public static Member queryByMemberID(String id) {
    	if (id == null) {
    		return null;
    	}
    	Member member = null;
    	if (PARAM.isQueryMemberFromPos()) {
            DbConnection connection = null;
			try {
				connection = CreamToolkit.getPooledConnection();
				String idCardNumber = id;
				if (id.length() == (12 - PARAM.getMemberCardIDPrefix().length())) {
					idCardNumber = PARAM.getMemberCardIDPrefix() + id; 
				}
				member = DacBase.getSingleObject(connection, Member.class,
						"SELECT * FROM member WHERE idCardNumber='" + idCardNumber
								+ CreamToolkit.getEAN13CheckDigit(idCardNumber) + "'");
				
				if (PARAM.isQueryMemberBlur()) {
					if (member == null) {
						member = DacBase.getSingleObject(connection,
								Member.class,
								"SELECT * FROM member WHERE idCardNumber='" + id
										+ "' OR " + "memberId = '" + id + "' OR "
										+ "telNumber = '" + id + "' OR "
										+ "memberemcid='" + id + "'");
					}
					if (member == null) {
						member = DacBase.getSingleObject(connection,
								Member.class,
								"SELECT * FROM member WHERE " +
										" idCardNumber LIKE '%" + id + "%' " +
										"OR memberId like '%" + id + "%' " +
										"OR telNumber like '%" + id + "%' " +
										"OR  memberemcid like '%" + id + "%' ");
					}
				}
			} catch (SQLException e) {
				CreamToolkit.logMessage(e);
			} catch (EntityNotFoundException e) {
				CreamToolkit.logMessage(e);
			}
    	} else if (!Client.getInstance().isConnected()) {
            ; // do nothing
        } else {
            // Send command "queryMember" to inline server.
            try {
                Client client = Client.getInstance();
                client.processCommand("queryMember " + id);
                member = (Member)client.getReturnObject();
                if (member != null) {
                    member.setNeedConfirm(false);  // No need user confirmation
                    return member;
                } else {                     
                    return tryCreateAnUnkownMember(id);
                }
            } catch (ClientCommandException e) {
            	CreamToolkit.logMessage(e);
            }
        }
    	
    	if (member != null) {
    		member.setNeedConfirm(false);
    	} else {
    		member = tryCreateAnUnkownMember(id);
    	}
    	return member;
    }

    /**
     * @param id
     * @return create a new member, needConfirm is set true
     */
    public static Member tryCreateAnUnkownMember(String id) {
        // 如果非会员打头编号，拒绝生成Member object
    	//if (!id.startsWith("2991") && !id.startsWith("2990"))
        if (!id.startsWith(PARAM.getMemberCardIDPrefix()))
            return null;

        // 如果没有输入校验码，则先帮它补上
        if (id.length() == 12)
            id += CreamToolkit.getEAN13CheckDigit(id);
        else if (!CreamToolkit.checkEAN13(id))
            return null;   // 如果足13码，但是检验码不对，也拒绝生成Member object

        Member member;
        try {
            member = new Member();
            member.setFieldValue("memberID", id.substring(4, 12));
            member.setFieldValue("memberName", CreamToolkit.GetResource().getString("Unknown"));
            member.setFieldValue("idCardNumber", id);
            member.setFieldValue("telNumber", CreamToolkit.GetResource().getString("Unknown"));
            member.setFieldValue("memberemcid", CreamToolkit.GetResource().getString("Unknown"));
            member.setFieldValue("membertotalbonus", new HYIDouble(0.00));
            member.setFieldValue("memberactionbonus", new HYIDouble(0.00));
            member.setNeedConfirm(true);    // need user confirmation
            return member;
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * 在离线状态下，若前台只输入前12码会员卡号，则必需由用户确认是否接受此会员卡号，
     * 在这种情况下，needConfirm会被设置为true。
     * 
     * @return boolean 是否需要让用户确认接受此会员卡号
     */
    public boolean isNeedConfirm() {
        return needConfirm;
    }

    /**
     * 在离线状态下，若前台只输入前12码会员卡号，则必需由用户确认是否接受此会员卡号，
     * 在这种情况下，needConfirm会被设置为true。
     * 
     * @param needConfirm 是否需要让用户确认接受此会员卡号
     */
    public void setNeedConfirm(boolean needConfirm) {
        this.needConfirm = needConfirm;
    }
}




