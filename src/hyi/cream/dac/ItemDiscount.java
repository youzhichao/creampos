
package hyi.cream.dac;  

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ItemDiscount extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private static ArrayList primaryKeys = new ArrayList();
	
	public List getPrimaryKeyList() {
		return  primaryKeys;
	}
	
	static final String tableName = "ITEMDISCOUNT";
	
	static {
		primaryKeys.add("ID");
	}
		
	public static ItemDiscount queryByID(DbConnection connection, String ID) {
		try {
            return getSingleObject(connection, ItemDiscount.class,
            	"SELECT * FROM " + tableName + " WHERE ID='" + ID + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}

	public String getInsertUpdateTableName() {
		return tableName;
	}

    /**
     * Constructor
     */
    public ItemDiscount() {
    }

    /**
     * table's properties
     */ 
	public String getID() {
		return (String)getFieldValue("ID");
	}

	public String getScreenName() {
		return (String)getFieldValue("SNAME");
	}

	public String getPrintName() {
		return (String)getFieldValue("PNAME");
	}

	public String getType() {
		return (String)getFieldValue("TYPE");
	}

	public HYIDouble getValue() {
		return (HYIDouble)getFieldValue("DVALUE");
	}

	public String getBase() {
		return (String)getFieldValue("DBASE");
	}

	public String getAttribute() {
	    return (String)getFieldValue("ATTR");
	}
}

 