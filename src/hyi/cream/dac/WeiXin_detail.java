package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * 微信交易明细weixin_detail
 * @author Administrator
 *
 */
public class WeiXin_detail extends DacBase implements Serializable {

	static final String tableName = "weixin_detail";
	private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("POSNUMBER");
		primaryKeys.add("STOREID");
        primaryKeys.add("TRANSACTIONNUMBER");
	}

	public WeiXin_detail() throws InstantiationException {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	
	public String getInsertUpdateTableName() {
		if (Server.serverExist()) {
                return "posul_weixin_detail";
        }
		return tableName;
	}
	
	public static String getInsertUpdateTableNameStaic() {
        if (Server.serverExist()) {
            return "posul_weixin_detail";
        }
        return "weixin_detail";
    }
	
	public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
            {"STOREID", "STOREID"},
            {"POSNUMBER", "POSNUMBER"},
            {"TRANSACTIONNUMBER", "TRANSACTIONNUMBER"},
            {"ZSEQ","ZSEQ"},
            {"TOTAL_FEE", "TOTAL_FEE"},
            {"TRANSACTION_ID","TRANSACTION_ID"},
            {"SYSTEMDATE","SYSTEMDATE"},
            {"SEQ","SEQ"},
            {"REFUND_ID","REFUND_ID"},
            {"OPENID","OPENID"},
            {"COUPON_FEE","COUPON_FEE"},
            {"NON_COUPOND_FEE","NON_COUPOND_FEE"},
            {"promotion_id","promotion_id"},
		};
	}
	
	//STOREID,店号
	public String getSTORENUMBER() {
		return (String) getFieldValue("STOREID");
	}
	public void setSTORENUMBER(String storeNumber) {
		 setFieldValue("STOREID",storeNumber);
	}

	//POSNUMBER, 机号
	public Integer getPOSNUMBER() {
		return (Integer) getFieldValue("POSNUMBER");
	}
	public void setPOSNUMBER(Integer posNumber) {
		setFieldValue("POSNUMBER",posNumber);
	}
	
	//TRANSACTIONNUMBER, 交易序号
	public Integer getTRANSACTIONNUMBER() {
		return (Integer) getFieldValue("TRANSACTIONNUMBER");
	}
	public void setTRANSACTIONNUMBER(Integer employeeId) {
		setFieldValue("TRANSACTIONNUMBER",employeeId);
	}

	//Tatal_fee,交易金额
	public HYIDouble getTOTAL_FEE() {
		return (HYIDouble) getFieldValue("TOTAL_FEE");
	}
	public void setTOTAL_FEE(HYIDouble temperature) {
		setFieldValue("TOTAL_FEE",temperature);
	}
	
	//TRANSACTION_ID,微信交易订单号
	public String getTRANSACTION_ID() {
		return (String) getFieldValue("TRANSACTION_ID");
	}
	public void setTRANSACTION_ID(String updateDateTime) {
		setFieldValue("TRANSACTION_ID",updateDateTime);
	}

    //SYSTEMDATE,时间
    public void setSYSTEMDATE(Date date) {
        setFieldValue("SYSTEMDATE",date);
    }
    public Date getSYSTEMDATE() {
        return (Date)getFieldValue("SYSTEMDATE");
    }

    //流水号
    public String getSEQ(){
        return (String)getFieldValue("SEQ");
    }
    public void setSEQ(String s){
        setFieldValue("SEQ",s);
    }

    //REFUND_ID,微信退款交易订单号
    public String getREFUND_ID() {
        return (String)getFieldValue("REFUND_ID");
    }

    public void setREFUND_ID(String s){
        setFieldValue("REFUND_ID",s);
    }

 //OPENID,用户标识
    public String getOPENID() {
        return (String)getFieldValue("OPENID");
    }

    public void setOPENID(String s){
        setFieldValue("OPENID",s);
    }

    //COUPON_FEE,现金券金额
    public HYIDouble getCOUPON_FEE() {
        return (HYIDouble)getFieldValue("COUPON_FEE");
    }

    public void setCOUPON_FEE(HYIDouble s){
        setFieldValue("COUPON_FEE",s);
    }

    //NON_COUPOND_FEE, non_coupond_fee  = total_fee - coupond_fee
    public HYIDouble getNON_COUPOND_FEE() {
        return (HYIDouble)getFieldValue("NON_COUPOND_FEE");
    }

    public void setNON_COUPOND_FEE(HYIDouble s){
        setFieldValue("NON_COUPOND_FEE",s);
    }

    //promotion_id
    public String getPromotion_id() {
        return (String)getFieldValue("promotion_id");
    }

    public void setPromotion_id(String s){
        setFieldValue("promotion_id",s);
    }

    //ZSEQ,z账序号
    public Integer getZSEQ() {
        return (Integer)getFieldValue("ZSEQ");
    }

    public void setZSEQ(Integer s){
        setFieldValue("ZSEQ",s);
    }

    public static Iterator queryByTranNo(DbConnection connection, int tranNo) {
        try {
            return getMultipleObjects(connection, WeiXin_detail.class,"select * from " + getInsertUpdateTableNameStaic() + " where  transactionNumber = " + tranNo);
        } catch (EntityNotFoundException e) {
//            CreamToolkit.logMessage(e);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        }
        return null;
    }

    public static WeiXin_detail queryByStoreIdTranNoPosNo(DbConnection connection,String storeId,String tranNo,String posId) {
        try {
            return getSingleObject(connection, WeiXin_detail.class,"select * from " + getInsertUpdateTableNameStaic()
                    + " where storeID = '" + storeId + "' and transactionNumber = " + tranNo + " and posNumber = " + posId);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } catch (EntityNotFoundException e) {
//            CreamToolkit.logMessage(e);
        }
        return null;
    }

    public WeiXin_detail cloneForSC() {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            WeiXin_detail clonedObj = new WeiXin_detail();
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = this.getFieldValue(fieldNameMap[i][0]);
                if (value == null) {
                    try {
                        value = this.getClass().getDeclaredMethod(
                                "get" + fieldNameMap[i][0], new Class[0]).invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedObj.setFieldValue(fieldNameMap[i][1], value);
            }
            return clonedObj;
        } catch (InstantiationException e) {
            return null;
        }
    }

    public static boolean deleteWeiXinDetailByPrimaryKey(int posNumber,int transactionNumber) {
        DbConnection con  = null ;
        Statement stmt = null;
        boolean isOK = false;
        String sql = "delete from " + getInsertUpdateTableNameStaic() + " where POSNUMBER ="
                + posNumber + " and TRANSACTIONNUMBER =" + transactionNumber;
        try {
            con = CreamToolkit.getPooledConnection();
            stmt = con.createStatement();
            isOK = stmt.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }

            CreamToolkit.releaseConnection(con);
        }
        return isOK;
    }

    static public void deleteOutdatedData() {
        DbConnection connection = null;
        Statement statement = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            if (hyi.cream.inline.Server.serverExist()) {
                // 需要保留的天数
                int tranReserved = PARAM.getTransactionReserved();
                long l = new java.util.Date().getTime() - tranReserved * 1000L * 3600 * 24;
                String baseTime = CreamCache.getInstance().getDateTimeFormate().format(new java.sql.Date(l));
                statement.executeUpdate("DELETE FROM posul_weixin_detail WHERE systemDate < '" + baseTime + "'");
            } else {
                Iterator<String> itr = Transaction.getOutdateTranNumbers();
                while (itr != null && itr.hasNext())
                    statement.executeUpdate("DELETE FROM weixin_detail WHERE transactionNumber=" + itr.next());
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
    }
}
