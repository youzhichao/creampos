/*
 * Created on 2005-3-15
 * for quanjia WorkCheck(考勤系统)
 * 
 */
package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Administrator
 */
public class EcDeliveryDetail extends DacBase implements Serializable {

    private static final long serialVersionUID = 1L;

	private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("storeID");
		primaryKeys.add("orderNumber");
		primaryKeys.add("seq");
	}

	private boolean needConfirm;

	public EcDeliveryDetail() {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (Server.serverExist())
			return "ec_delivery_detail";
		else
			return "ec_delivery_detail";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (Server.serverExist())
			return "ec_delivery_detail";
		else
			return "ec_delivery_detail";
	}

	public void setStoreID(String storeID) {
		setFieldValue("storeID", storeID);
	}

	public String getStoreID() {
		return ((String) getFieldValue("storeID"));
	}

	public void setOrderNumber(String fcardnumber) {
		setFieldValue("orderNumber", fcardnumber);
	}

	//订单号
	public String getOrderNumber() {
		return ((String) getFieldValue("orderNumber"));
	}

	//序号
	public int getSeq() {
		return ((Integer) getFieldValue("seq"));
	}

	public void setSeq(int seq) {
		setFieldValue("seq", seq);
	}

	public void setItemNumber(String itemNumber) {
		setFieldValue("itemNumber", itemNumber);
	}

	//品号
	public String getItemNumber() {
		return ((String) getFieldValue("itemNumber"));
	}

	public void setPluName(String pluName) {
		setFieldValue("pluName", pluName);
	}

	//品名
	public String getPluName() {
		return ((String) getFieldValue("pluName"));
	}

	public void setUnitPrice(HYIDouble unitPrice) {
		setFieldValue("unitPrice", unitPrice);
	}

	//原价
	public HYIDouble getUnitPrice() {
		return ((HYIDouble) getFieldValue("unitPrice"));
	}

	public void setPrice(HYIDouble price) {
		setFieldValue("price", price);
	}

	//实际售价
	public HYIDouble getPrice() {
		return ((HYIDouble) getFieldValue("price"));
	}

	//数量
	public int getQuantity() {
		return ((Integer) getFieldValue("quantity"));
	}

	public void setQuantity(int quantity) {
		setFieldValue("quantity", quantity);
	}

	public void setAmount(HYIDouble amount) {
		setFieldValue("amount", amount);
	}

	//金额
	public HYIDouble getAmount() {
		return ((HYIDouble) getFieldValue("amount"));
	}

	//单位
	public String getUnit() {
		return ((String) getFieldValue("unit"));
	}

	public void setUnit(String unit) {
		setFieldValue("unit", unit);
	}

	//规格
	public String getSpecification() {
		return (String) getFieldValue("specification");
	}

	public void setSpecification(String specification) {
		setFieldValue("specification", specification);
	}


	/**
     *
     */
    public static Collection queryObject(String data) {
    	Server.log("ec_delivery_detail queryObject");
        List list = new ArrayList();
		List<EcDeliveryDetail> member;

        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();


            // check as member ID first
            try {
				Map fieldNameMap = null;
				Server.log("ec_delivery_detail | sql : " + "SELECT * FROM ec_delivery_detail WHERE orderNumber = '" + data + "'");
				return DacBase.getMultipleObjects(connection, EcDeliveryDetail.class,
						"SELECT * FROM ec_delivery_detail WHERE orderNumber = '" + data + "'", fieldNameMap);
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } catch (EntityNotFoundException e) {
            }


            return list;

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }


}
