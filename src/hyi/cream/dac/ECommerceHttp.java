package hyi.cream.dac;


import hyi.cream.groovydac.Param;
import org.mortbay.util.ajax.JSON;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;



public class ECommerceHttp {


    public static String ecommerceUrl = Param.getInstance().getEcommerceUrl();

    public static void main(String[] args) {
        System.out.println(System.currentTimeMillis() / 1000);
        String requestParameters = "{\n" +
                "  \"orderId\": 53,\n" +
                "  \"payAmount\": 5,\n" +
                "  \"payId\": \"16\",\n" +
                "  \"payInfo\": {\n" +
                "    \"cardNo\": \"卡号、券号等\",\n" +
                "    \"userId\": \"第三方支付账号\",\n" +
                "    \"transactionId\": \"第三方支付流水号\"\n" +
                "  },\n" +
                "  \"payTime\": \"2019-06-28 11:22:03\",\n" +
                "  \"storeId\": \"100062\",\n" +
                "  \"tranNo\": \"123456\"\n" +
                "}";
        getResult("api/store/order/update/100062/3205/52/1/20190628141610",requestParameters,"GET");
    }


    //会员认证
    public static HashMap getResult(String urls, String requestParameters, String method) {
        System.out.println(requestParameters);

        String ss = ecommerceUrl+urls;//url+"?token="+zt.gettoken();
        System.out.println("url: "+ss);

        Object rstObject = null;

        try {
            // 4.执行postMethod,调用http接口
            URL url = new URL(ss);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            int timeout = 60;
            timeout = timeout * 1000;
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setConnectTimeout(timeout);
//            conn.setReadTimeout(10000)
            conn.setReadTimeout(timeout);
            conn.setRequestMethod(method);
            conn.setRequestProperty("content-type","application/json");

            conn.setDoOutput(true);
            if (method.equals("POST")) {
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(requestParameters);
                wr.flush();

                // Get the response
                BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line;
                String result = "";
                while ((line = rd.readLine()) != null) {
                    result += line;
                }
                System.out.println("result:" + result);
                //result=null ,!=null?
                if (result == null || "".equals(result)) {
                    return null;
                } else {
                    rstObject = JSON.parse(result);   //!=null
                }
                wr.close();
                rd.close();
            } else {
                // Get the response
                BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line;
                String result = "";
                while ((line = rd.readLine()) != null) {
                    result += line;
                }
                System.out.println("result:" + result);
                //result=null ,!=null?
                if (result == null || "".equals(result)) {
                    return null;
                } else {
                    rstObject = JSON.parse(result);   //!=null
                }
                rd.close();
            }

            return (HashMap) rstObject;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    public static String getMD5Str(String str) {
        MessageDigest messageDigest = null;
        try {
            System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())
                    + " --------------------------- process MemberHttp ---------------------------");

            System.out.println("MD5加密数据：" + str);
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(str.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException caught!");
            System.exit(-1);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        byte[] byteArray = messageDigest.digest();
        StringBuffer md5StrBuff = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
                md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
            else
                md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
        }
        String res = md5StrBuff.toString();
        System.out.println("sign = "+res);
        return res;
    }


}
