/*
 * Created on 2005-3-15
 * for quanjia WorkCheck(考勤系统)
 * 
 */
package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Administrator
 */
public class EcDeliveryHead extends DacBase implements Serializable {

    private static final long serialVersionUID = 1L;

	private static ArrayList primaryKeys = new ArrayList();
	private List<EcDeliveryDetail> ecDeliveryDetails = new ArrayList<EcDeliveryDetail>();

	static {
		primaryKeys.add("storeID");
		primaryKeys.add("orderNumber");
	}

	private boolean needConfirm;

	public EcDeliveryHead() {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (Server.serverExist())
			return "ec_delivery_head";
		else
			return "ec_delivery_head";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (Server.serverExist())
			return "ec_delivery_head";
		else
			return "ec_delivery_head";
	}

	public void setStoreID(String storeID) {
		setFieldValue("storeID", storeID);
	}

	public String getStoreID() {
		return ((String) getFieldValue("storeID"));
	}

	public void setOrderNumber(String fcardnumber) {
		setFieldValue("orderNumber", fcardnumber);
	}

	//订单号
	public String getOrderNumber() {
		return ((String) getFieldValue("orderNumber"));
	}

	public void setAmount(HYIDouble amount) {
		setFieldValue("amount", amount);
	}

	//总金额
	public HYIDouble getAmount() {
		return ((HYIDouble) getFieldValue("amount"));
	}

	public void setServiceType(String serviceType) {
		setFieldValue("serviceType", serviceType);
	}

	//服务类型(1.到店取货 2.到店付款取货)
	public String getServiceType() {
		return ((String) getFieldValue("serviceType"));
	}
	
	public void setOrderTime(Date orderTime) {
		setFieldValue("orderTime", orderTime);
	}

	//下单时间
	public Date getOrderTime() {
		return ((Date) getFieldValue("orderTime"));
	}
	
	public void setStatus(String status) {
		setFieldValue("status", status);
	}

	//订单状态
	public String getStatus() {
		return ((String) getFieldValue("status"));
	}
		
	public void setPosNo(int posNo) {
		setFieldValue("posNo", posNo);
	}

	public int getPosNo() {
		return ((Integer) getFieldValue("posNo"));
	}
	
	public void setTransactionNumber(int transactionNumber) {
		setFieldValue("transactionNumber", transactionNumber);
	}

	//交易号
	public int getTransactionNumber() {
		return ((Integer) getFieldValue("transactionNumber"));
	}
	
	public void setMemberId(String memberId) {
		setFieldValue("memberId", memberId);
	}

	//会员号
	public String getMemberId() {
		return ((String) getFieldValue("memberId"));
	}

	public void setMemberName(String memberName) {
		setFieldValue("memberName", memberName);
	}

	//会员名称
	public String getMemberName() {
		return (String) getFieldValue("memberName");
	}

	public String getPhone() {
		return (String) getFieldValue("phone");
	}

	public void setPhone(String phone) {
		setFieldValue("phone", phone);
	}

	//送货地址
	public String getAddress() {
		return (String) getFieldValue("address");
	}

	public void setAddress(String address) {
		setFieldValue("address", address);
	}

	//送货备注
	public String getNote() {
		return (String) getFieldValue("note");
	}

	public void setNote(String note) {
		setFieldValue("note", note);
	}

	//送货时间
	public Date getDeliveryTime() {
		return (Date) getFieldValue("deliveryTime");
	}

	public void setDeliveryTime(Date deliveryTime) {
		setFieldValue("deliveryTime", deliveryTime);
	}

	//送货类型(1.到店自取 2.送货上门)
	public String getDeliveryType() {
		return (String) getFieldValue("deliveryType");
	}

	public void setDeliveryType(String deliveryType) {
		setFieldValue("deliveryType", deliveryType);
	}

	//支付方式
	public String getPayId() {
		return (String) getFieldValue("payId");
	}

	public void setPayId(String payId) {
		setFieldValue("payId", payId);
	}

	//订单编号
	public int getOrderId() {
		return (Integer) getFieldValue("orderId");
	}

	public void setOrderId(int orderId) {
		setFieldValue("orderId", orderId);
	}

	public List<EcDeliveryDetail> getEcDeliveryDetails() {
		return this.ecDeliveryDetails;
	}

	public void setEcDeliveryDetails(List<EcDeliveryDetail> ecDeliveryDetails) {
		this.ecDeliveryDetails = ecDeliveryDetails;
	}

	public static Iterator queryByTranNo(DbConnection connection, int tranNo) {
		try {
			return getMultipleObjects(connection, EcDeliveryHead.class,"select * from " + getInsertUpdateTableNameStaticVersion() + " where transactionNumber = " + tranNo);
		} catch (EntityNotFoundException e) {
//            CreamToolkit.logMessage(e);
		} catch (SQLException e) {
			CreamToolkit.logMessage(e);
		}
		return null;
	}

	/**
     *
     */
    public static Collection queryObject(String data) {
    	Server.log("ec_delivery_head queryObject");
        List list = new ArrayList();
        EcDeliveryHead member;

        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();


            // check as member ID first
            try {
                member = (EcDeliveryHead)DacBase.getSingleObject(connection, EcDeliveryHead.class,
                    "SELECT * FROM ec_delivery_head WHERE orderNumber = '" + data + "'");
            	Server.log("ec_delivery_head | sql : " + "SELECT * FROM ec_delivery_head WHERE orderNumber = '" + data + "'");
                list.add(member);
            	Server.log("ec_delivery_head | orderNumber : " + member.getOrderNumber());
                return list;
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } catch (EntityNotFoundException e) {
            }


            return list;

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

	/**
     *
     */
    public static int updateObject(String data) {
    	Server.log("ec_delivery_head updateObject");
        int result = 0;
        EcDeliveryHead member;

        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();


            // check as member ID first
            try {
				String[] str = data.split(",");
				result = DacBase.update(connection,"update ec_delivery_head set status = '301',posNo= '"+str[1]+"',transactionNumber = '"+str[2]+"'," +
						"payId = '"+str[3]+"' where orderNumber = '" + str[0] + "'");
            	Server.log("ec_delivery_head | sql : " + "update ec_delivery_head set status = '301',posNo= '"+str[1]+"',transactionNumber = '"+str[2]+"'," +
						"payId = '"+str[3]+"' WHERE orderNumber = '" + str[0] + "'");
                return result;
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }


            return result;

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return 0;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    
   /**
     * 在离线状态下，若前台只输入前12码会员卡号，则必需由用户确认是否接受此会员卡号，
     * 在这种情况下，needConfirm会被设置为true。
     * 
     * @return boolean 是否需要让用户确认接受此会员卡号
     */
    public boolean isNeedConfirm() {
        return needConfirm;
    }

    /**
     * 在离线状态下，若前台只输入前12码会员卡号，则必需由用户确认是否接受此会员卡号，
     * 在这种情况下，needConfirm会被设置为true。
     * 
     * @param needConfirm 是否需要让用户确认接受此会员卡号
     */
    public void setNeedConfirm(boolean needConfirm) {
        this.needConfirm = needConfirm;
    }

}
