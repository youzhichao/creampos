package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * 自助点单单头selfbuy_head
 * @author Administrator
 *
 */
public class Selfbuy_head extends DacBase implements Serializable {

	static final String tableName = "selfbuy_head";
	private static ArrayList primaryKeys = new ArrayList();
	private static ArrayList goods = new ArrayList();

	static {
		primaryKeys.add("storeid");
		primaryKeys.add("orderid");
		primaryKeys.add("orderno");
	}

	public Selfbuy_head() throws InstantiationException {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

    public String getInsertUpdateTableName() {
        if (Server.serverExist()) {
            return "posul_selfbuy_head";
        }
        return tableName;
    }

    public static String getInsertUpdateTableNameStaic() {
        if (Server.serverExist()) {
            return "posul_selfbuy_head";
        }
        return "selfbuy_head";
    }

    public static  String[][] getPosToScFieldNameArray() {
        return new String[][]{
                {"storeid", "storeid"},
                {"orderid", "orderid"},
                {"orderno", "orderno"},
                {"tradetype","tradetype"},
                {"shopid", "shopid"},
                {"orderedat","orderedat"},
                {"total","total"},
                {"discount","discount"},
                {"payment","payment"},
                {"paid","paid"},
                {"paidat","paidat"},
                {"tmtranseq","tmtranseq"},
                {"systemdate","systemdate"},
                {"trade_no","trade_no"},
                {"buyer_login_id","buyer_login_id"},
        };
    }

	//STOREID,店号
	public String getStoreId() {
		return (String) getFieldValue("storeid");
	}
	public void setStoreId(String storeNumber) {
		 setFieldValue("storeid",storeNumber);
	}

	//orderid, 订单id
	public Integer getOrderId() {
		return (Integer) getFieldValue("orderid");
	}
	public void setOrderId(Integer posNumber) {
		setFieldValue("orderid",posNumber);
	}

	//orderno, 订单号
	public String getOrderNo() {
		return (String) getFieldValue("orderno");
	}
	public void setOrderNo(String employeeId) {
		setFieldValue("orderno",employeeId);
	}

    //tradetype,订单类型
    public Integer getTradetype() {
        return (Integer)getFieldValue("tradetype");
    }

    public void setTradetype(Integer s){
        setFieldValue("tradetype",s);
    }

	//shopid,门店号
	public String getShopid() {
		return (String) getFieldValue("shopid");
	}
	public void setShopid(String temperature) {
		setFieldValue("shopid",temperature);
	}

	//orderedat,下单时间
	public Date getOrderedat() {
		return (Date) getFieldValue("orderedat");
	}
	public void setOrderedat(Date merId) {
		setFieldValue("orderedat",merId);
	}

    //total,小计金额
    public void setTotal(HYIDouble date) {
        setFieldValue("total",date);
    }
    public HYIDouble getTotal() {
        return (HYIDouble)getFieldValue("total");
    }

    //discount,优惠金额
    public HYIDouble getDiscount(){
        return (HYIDouble)getFieldValue("discount");
    }
    public void setDiscount(HYIDouble s){
        setFieldValue("discount",s);
    }

    //payment,付款方式
    public String getPayment() {
        return (String)getFieldValue("payment");
    }

    public void setPayment(String s){
        setFieldValue("payment",s);
    }

    //paid,付款金额
    public HYIDouble getpaid() {
        return (HYIDouble)getFieldValue("paid");
    }

    public void setPaid(HYIDouble s){
        setFieldValue("paid",s);
    }

    //paidat,付款时间
    public Date getPaidat() {
        return (Date)getFieldValue("paidat");
    }

    public void setPaidat(Date s){
        setFieldValue("paidat",s);
    }

    //tmtranseq,交易号
    public Integer getTmtranseq() {
        return (Integer)getFieldValue("tmtranseq");
    }

    public void setTmtranseq(Integer s){
        setFieldValue("tmtranseq",s);
    }

    //SYSTEMDATE,时间
    public void setSYSTEMDATE(Date date) {
        setFieldValue("systemDate",date);
    }
    public Date getSYSTEMDATE() {
        return (Date)getFieldValue("systemDate");
    }

    //trade_no, 支付宝交易id
    public String getTradeNo() {
        return (String) getFieldValue("trade_no");
    }
    public void setTradeNo(String employeeId) {
        setFieldValue("trade_no",employeeId);
    }

    //buyer_logon_id, 支付宝登录id
    public String getBuyerLoginId() {
        return (String) getFieldValue("buyer_login_id");
    }
    public void setBuyerLoginId(String employeeId) {
        setFieldValue("buyer_login_id",employeeId);
    }

    //订单明细
    public ArrayList<Selfbuy_detail> getGoods() {
        return goods;
    }

    public void setGoodst(ArrayList<Selfbuy_detail> s){
        goods = s;
    }

    public static Iterator queryByTranNo(DbConnection connection, int tranNo) {
        try {
            return getMultipleObjects(connection, Selfbuy_head.class,"select * from " + getInsertUpdateTableNameStaic() + " where  tmtranseq = " + tranNo);
        } catch (EntityNotFoundException e) {
//            CreamToolkit.logMessage(e);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        }
        return null;
    }

    public Selfbuy_head cloneForSC() {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            Selfbuy_head clonedObj = new Selfbuy_head();
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = this.getFieldValue(fieldNameMap[i][0]);
                if (value == null) {
                    try {
                        value = this.getClass().getDeclaredMethod(
                                "get" + fieldNameMap[i][0], new Class[0]).invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedObj.setFieldValue(fieldNameMap[i][1], value);
            }
            return clonedObj;
        } catch (InstantiationException e) {
            return null;
        }
    }

    static public void deleteOutdatedData() {
        DbConnection connection = null;
        Statement statement = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            if (Server.serverExist()) {
                // 需要保留的天数
                int tranReserved = PARAM.getTransactionReserved();
                long l = new Date().getTime() - tranReserved * 1000L * 3600 * 24;
                String baseTime = CreamCache.getInstance().getDateTimeFormate().format(new java.sql.Date(l));
                statement.executeUpdate("DELETE FROM posul_selfbuy_head WHERE systemDate < '" + baseTime + "'");
            } else {
                Iterator<String> itr = Transaction.getOutdateTranNumbers();
                while (itr != null && itr.hasNext())
                    statement.executeUpdate("DELETE FROM selfbuy_head WHERE tmtranseq=" + itr.next());
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
    }

}
