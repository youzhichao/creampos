package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Dep definition class.
 *
 * @author Dai, Bruce
 * @version 1.5
 */
public class Dep extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public static Collection getAllObjectsForPOS()
     */
    transient public static final String VERSION = "1.5";

    static final String tableName = "dep";
//    transient static private Set cache;
    transient static private List cache;
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("depID");
        if (!hyi.cream.inline.Server.serverExist())
             createCache();
    }

    /**
     * Constructor
     */
    public Dep() {
    }

    public static void createCache() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Iterator itr = getMultipleObjects(connection, Dep.class, "SELECT * FROM " + tableName
                + " order by " + primaryKeys.get(0));
            cache = new ArrayList();
            while (itr.hasNext()) {
                cache.add(itr.next());
            }
        } catch (EntityNotFoundException e) {
            cache = null;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            cache = null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        if (getUseClientSideTableName())
            return "dep";
        else if (hyi.cream.inline.Server.serverExist())
            return "posdl_dep";
        else
            return "dep";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posdl_dep";
        else
            return "dep";
    }

    public static Dep queryByDepID(String s) {
        if (cache == null)
            return null;
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            Dep p = (Dep)itr.next();
            if (p.getDepID().equals(s)){
                return p;
            }
        }
        return null;
    }

    public static Iterator getDepIDs() {
        if (cache != null) {
            return cache.iterator();
        } else {
            return null;
        }
    }

    // properties

    //depID,depID,CHAR(2),N,帐务编号
    public String getDepID() {
        return (String)this.getFieldValue("depID");
    }

    //depName,depName,VARCHAR(20),N,帐务名称
    public String getDepName() {
        return (String)this.getFieldValue("depName");
    }

    /**
     * 帐务类别税率。
     * 
     * @return 税别代号
     */
    public String getDepTax() {
        return (String)this.getFieldValue("depTax");
    }

	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Collection dfs = DacBase.getExistedFieldList(getInsertUpdateTableNameStaticVersion());

        Map fieldNameMap = new HashMap();
        fieldNameMap.put("depID", "depID");
        fieldNameMap.put("depName", "depName");
        if (dfs.contains("depTax"))
            fieldNameMap.put("depTax", "depTax");
        return fieldNameMap;
	}
	
    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            return DacBase.getMultipleObjects(connection, hyi.cream.dac.Dep.class,
                "SELECT * FROM posdl_dep order by " + primaryKeys.get(0), getScToPosFieldNameMap());
        } catch (EntityNotFoundException e) {
            return new ArrayList();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return new ArrayList();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}
