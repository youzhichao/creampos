package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import java.math.BigInteger;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * CashForm class.
 *
 * @author Dai, Bruce
 * @version 1.5
 */
public class CashForm extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public static Object[] cloneForSC(Iterator iter)
     */
    transient public static final String VERSION = "1.5";

    private static ArrayList primaryKeys = new ArrayList();
//    transient private static ArrayList depArray = new ArrayList();

    static {
        primaryKeys.add("storeID");
        primaryKeys.add("posNumber");
        primaryKeys.add("zSequenceNumber");
        primaryKeys.add("shiftSequenceNumber");
    }

    /**
     * Constructor
     */
    public CashForm() {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_cashform";
        else
            return "cashform";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_cashform";
        else
            return "cashform";
    }

    public static Iterator queryUnsentCashForm(DbConnection connection) {
        try {
            return getMultipleObjects(connection, CashForm.class, "SELECT * FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE uploadState<>'1'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static CashForm queryByZNumberAndShiftNumber(DbConnection connection, int zNumber,
        int shiftNumber) {
        try {
            return getSingleObject(connection, CashForm.class, "SELECT * FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE zSequenceNumber='" + zNumber
                + "' AND shiftSequenceNumber='" + shiftNumber + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static HYIDouble queryAmountByDatePosNumberAndPaymentID(DbConnection connection,
        Date date, int posNumber, String payID) {

        HYIDouble total = new HYIDouble("0.00");
        if (hyi.cream.inline.Server.serverExist()) {
            Iterator iter;
            try {
                iter = getMultipleObjects(connection, CashForm.class,
                    "SELECT * FROM view_CashForm" +
                    " WHERE accountDate='" + date.toString() + "' AND posNumber=" +
                    posNumber);
                DecimalFormat df = new DecimalFormat("00");
                int payIDNum=0;
                payIDNum = Integer.parseInt(payID);
                if (iter != null && iter.hasNext()) {
                    while (iter.hasNext()) {
                        CashForm jdk = (CashForm)iter.next();
                        if (payID.equals("0") || payID.equals("00")) {  // cash
                            total = total.addMe(jdk.getTotalCash());
                        } else {
                            HYIDouble amt = (HYIDouble)CashForm.class.getDeclaredMethod(
                                "getPay" + df.format(payIDNum) + "Amount",
                                new Class[0]).invoke(jdk, new Object[0]);
                            total = total.addMe(amt);
                        }
                    }
                }
                return total;
            } catch (EntityNotFoundException e) {
                return total;
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                return total;
            } catch (Exception e) {
                CreamToolkit.logMessage(e);
                return total;
            }
        } else {
            return null;
        }
    }

    //  properties
    // storeID : STORENO
    public void setStoreID(String s) {
        setFieldValue("storeID", s);
    }

    public String getStoreID() {
        return (String)getFieldValue("storeID");
    }
    public void setStoreNumber(String s) {
        setFieldValue("storeID", s);
    }

    public String getStoreNumber() {
        return (String)getFieldValue("storeID");
    }

    public String getStoreName() {
        return Store.getStoreChineseName();
    }

    //Bruce/20021008/
    public void setAccountingDate(Date d) {
         setFieldValue("accountDate", d);
    }

    //Bruce/20021008/
    public Date getAccountingDate() {
        return (Date)getFieldValue("accountDate");
    }

    // posNumber : POSNO
    public void setPOSNumber(Integer i) {
        setFieldValue("posNumber", i);
    }

    public Integer getPOSNumber() {
        Object o = getFieldValue("posNumber");
        if (o instanceof Byte)
            return new Integer(((Byte)o).intValue());
        else
            return (Integer)o;
    }

    public void setTerminalNumber(Integer i) {
        setFieldValue("posNumber", i);
    }

    public Integer getTerminalNumber() {
        Object o = getFieldValue("posNumber");
        if (o instanceof Byte)
            return new Integer(((Byte)o).intValue());
        else
            return (Integer)o;
    }

    // sequenceNumber : SHIFTCNT
    public void setSequenceNumber(Integer i) {
        setFieldValue("shiftSequenceNumber", i);
    }

    public Integer getSequenceNumber() {
        return (Integer)getFieldValue("shiftSequenceNumber");
    }

    // zSequenceNumber : EODCNT
    public void setZSequenceNumber(Integer i) {
        setFieldValue("zSequenceNumber", i);
    }

    public Integer getZSequenceNumber() {
        return (Integer) getFieldValue("zSequenceNumber");
    }

    // uploadState : TCPFLG
    public void setUploadState(String s) {
        setFieldValue("uploadState", s);
    }

    public String getUploadState() {
        return (String) getFieldValue("uploadState");
    }
    // cashierID : CASHIERNO
    public void setCashierID(String s) {
        setFieldValue("cashierID", s);
    }

    public String getCashierID() {
        return (String) getFieldValue("cashierID");
    }
    public void setCashierNumber(String s) {
        setFieldValue("cashierID", s);
    }

    public String getCashierNumber() {
        return (String) getFieldValue("cashierID");
    }

    public String getCashierName() {
        Cashier cas = Cashier.queryByCashierID(getCashierNumber());
        if (cas == null)
            return getCashierNumber();
        else
            return cas.getCashierName();
    }

    // hundred : HUNDRED
    public void setHundred(Integer i) {
            setFieldValue("hundred", i);
    }

    public Integer getHundred() {
            return (Integer)getFieldValue("hundred");
    }

    // fifty : FIFTY
    public void setFifty(Integer i) {
            setFieldValue("fifty", i);
    }

    public Integer getFifty() {
            return (Integer)getFieldValue("fifty");
    }

    // twenty : TWENTY
    public void setTwenty(Integer i) {
            setFieldValue("twenty", i);
    }

    public Integer getTwenty() {
            return (Integer)getFieldValue("twenty");
    }

    // ten : TEN
    public void setTen(Integer i) {
            setFieldValue("ten", i);
    }

    public Integer getTen() {
            return (Integer)getFieldValue("ten");
    }

    // five : FIVE
    public void setFive(Integer i) {
            setFieldValue("five", i);
    }

    public Integer getFive() {
            return (Integer)getFieldValue("five");
    }

    // two : TWO
    public void setTwo(Integer i) {
            setFieldValue("two", i);
    }

    public Integer getTwo() {
            return (Integer)getFieldValue("two");
    }

    // one : ONE
    public void setOne(Integer i) {
            setFieldValue("one", i);
    }

    public Integer getOne() {
            return (Integer)getFieldValue("one");
    }

    // fiveJiao : FIVEJIAO
    public void setFiveJiao(Integer i) {
            setFieldValue("fiveJiao", i);
    }

    public Integer getFiveJiao() {
            return (Integer)getFieldValue("fiveJiao");
    }

    // twoJiao : TWOJIAO
    public void setTwoJiao(Integer i) {
            setFieldValue("twoJiao", i);
    }

    public Integer getTwoJiao() {
            return (Integer)getFieldValue("twoJiao");
    }

    // oneJiao : ONEJIAO
    public void setOneJiao(Integer i) {
            setFieldValue("oneJiao", i);
    }

    public Integer getOneJiao() {
            return (Integer)getFieldValue("oneJiao");
    }

    // fiveCents : FIVECENTS
    public void setFiveCents(Integer i) {
            setFieldValue("fiveCents", i);
    }

    public Integer getFiveCents() {
            return (Integer)getFieldValue("fiveCents");
    }

    // twoCents : TWOCENTS
    public void setTwoCents(Integer i) {
            setFieldValue("twoCents", i);
    }

    public Integer getTwoCents() {
            return (Integer)getFieldValue("twoCents");
    }

    // oneCents : ONECENTS
    public void setOneCents(Integer i) {
            setFieldValue("oneCents", i);
    }

    public Integer getOneCents() {
            return (Integer)getFieldValue("oneCents");
    }

    // pay00Amount : PAY00AMT
    public void setPay00Amount(HYIDouble b) {
            setFieldValue("pay00Amount", b);
    }

    public HYIDouble getPay00Amount() {
            return (HYIDouble)getFieldValue("pay00Amount");
    }

    // pay01Amount : PAY01AMT
    public void setPay01Amount(HYIDouble b) {
            setFieldValue("pay01Amount", b);
    }

    public HYIDouble getPay01Amount() {
            return (HYIDouble)getFieldValue("pay01Amount");
    }

    // pay02Amount : PAY02AMT
    public void setPay02Amount(HYIDouble b) {
            setFieldValue("pay02Amount", b);
    }

    public HYIDouble getPay02Amount() {
            return (HYIDouble)getFieldValue("pay02Amount");
    }

    // pay03Amount : PAY03AMT
    public void setPay03Amount(HYIDouble b) {
            setFieldValue("pay03Amount", b);
    }

    public HYIDouble getPay03Amount() {
            return (HYIDouble)getFieldValue("pay03Amount");
    }

    // pay04Amount : PAY04AMT
    public void setPay04Amount(HYIDouble b) {
            setFieldValue("pay04Amount", b);
    }

    public HYIDouble getPay04Amount() {
            return (HYIDouble)getFieldValue("pay04Amount");
    }

    // pay05Amount : PAY05AMT
    public void setPay05Amount(HYIDouble b) {
            setFieldValue("pay05Amount", b);
    }

    public HYIDouble getPay05Amount() {
            return (HYIDouble)getFieldValue("pay05Amount");
    }

    // pay06Amount : PAY06AMT
    public void setPay06Amount(HYIDouble b) {
            setFieldValue("pay06Amount", b);
    }

    public HYIDouble getPay06Amount() {
            return (HYIDouble)getFieldValue("pay06Amount");
    }

    // pay07Amount : PAY07AMT
    public void setPay07Amount(HYIDouble b) {
            setFieldValue("pay07Amount", b);
    }

    public HYIDouble getPay07Amount() {
            return (HYIDouble)getFieldValue("pay07Amount");
    }

    // pay08Amount : PAY08AMT
    public void setPay08Amount(HYIDouble b) {
            setFieldValue("pay08Amount", b);
    }

    public HYIDouble getPay08Amount() {
            return (HYIDouble)getFieldValue("pay08Amount");
    }

    // pay09Amount : PAY09AMT
    public void setPay09Amount(HYIDouble b) {
            setFieldValue("pay09Amount", b);
    }

    public HYIDouble getPay09Amount() {
            return (HYIDouble)getFieldValue("pay09Amount");
    }

    // pay10Amount : PAY10AMT
    public void setPay10Amount(HYIDouble b) {
            setFieldValue("pay10Amount", b);
    }

    public HYIDouble getPay10Amount() {
            return (HYIDouble)getFieldValue("pay10Amount");
    }

    // pay11Amount : PAY11AMT
    public void setPay11Amount(HYIDouble b) {
            setFieldValue("pay11Amount", b);
    }

    public HYIDouble getPay11Amount() {
            return (HYIDouble)getFieldValue("pay11Amount");
    }

    // pay12Amount : PAY12AMT
    public void setPay12Amount(HYIDouble b) {
            setFieldValue("pay12Amount", b);
    }

    public HYIDouble getPay12Amount() {
            return (HYIDouble)getFieldValue("pay12Amount");
    }

    // pay13Amount : PAY13AMT
    public void setPay13Amount(HYIDouble b) {
            setFieldValue("pay13Amount", b);
    }

    public HYIDouble getPay13Amount() {
            return (HYIDouble)getFieldValue("pay13Amount");
    }

    // pay14Amount : PAY14AMT
    public void setPay14Amount(HYIDouble b) {
            setFieldValue("pay14Amount", b);
    }

    public HYIDouble getPay14Amount() {
            return (HYIDouble)getFieldValue("pay14Amount");
    }

    // pay15Amount : PAY15AMT
    public void setPay15Amount(HYIDouble b) {
            setFieldValue("pay15Amount", b);
    }

    public HYIDouble getPay15Amount() {
            return (HYIDouble)getFieldValue("pay15Amount");
    }

    // pay16Amount : PAY16AMT
    public void setPay16Amount(HYIDouble b) {
            setFieldValue("pay16Amount", b);
    }

    public HYIDouble getPay16Amount() {
            return (HYIDouble)getFieldValue("pay16Amount");
    }

    // pay17Amount : PAY17AMT
    public void setPay17Amount(HYIDouble b) {
            setFieldValue("pay17Amount", b);
    }

    public HYIDouble getPay17Amount() {
            return (HYIDouble)getFieldValue("pay17Amount");
    }

    // pay18Amount : PAY18AMT
    public void setPay18Amount(HYIDouble b) {
            setFieldValue("pay18Amount", b);
    }

    public HYIDouble getPay18Amount() {
            return (HYIDouble)getFieldValue("pay18Amount");
    }

    // pay19Amount : PAY19AMT
    public void setPay19Amount(HYIDouble b) {
            setFieldValue("pay19Amount", b);
    }

    public HYIDouble getPay19Amount() {
            return (HYIDouble)getFieldValue("pay19Amount");
    }

    // pay20Amount : PAY20AMT
    public void setPay20Amount(HYIDouble b) {
            setFieldValue("pay20Amount", b);
    }

    public HYIDouble getPay20Amount() {
            return (HYIDouble)getFieldValue("pay20Amount");
    }

    // pay21Amount : PAY21AMT
    public void setPay21Amount(HYIDouble b) {
            setFieldValue("pay21Amount", b);
    }

    public HYIDouble getPay21Amount() {
            return (HYIDouble)getFieldValue("pay21Amount");
    }

    // pay22Amount : PAY22AMT
    public void setPay22Amount(HYIDouble b) {
            setFieldValue("pay22Amount", b);
    }

    public HYIDouble getPay22Amount() {
            return (HYIDouble)getFieldValue("pay22Amount");
    }

    // pay23Amount : PAY23AMT
    public void setPay23Amount(HYIDouble b) {
            setFieldValue("pay23Amount", b);
    }

    public HYIDouble getPay23Amount() {
            return (HYIDouble)getFieldValue("pay23Amount");
    }

    // pay24Amount : PAY24AMT
    public void setPay24Amount(HYIDouble b) {
            setFieldValue("pay24Amount", b);
    }

    public HYIDouble getPay24Amount() {
            return (HYIDouble)getFieldValue("pay24Amount");
    }

    // pay25Amount : PAY25AMT
    public void setPay25Amount(HYIDouble b) {
            setFieldValue("pay25Amount", b);
    }

    public HYIDouble getPay25Amount() {
            return (HYIDouble)getFieldValue("pay25Amount");
    }

    // pay26Amount : PAY26AMT
    public void setPay26Amount(HYIDouble b) {
            setFieldValue("pay26Amount", b);
    }

    public HYIDouble getPay26Amount() {
            return (HYIDouble)getFieldValue("pay26Amount");
    }

    // pay27Amount : PAY27AMT
    public void setPay27Amount(HYIDouble b) {
            setFieldValue("pay27Amount", b);
    }

    public HYIDouble getPay27Amount() {
            return (HYIDouble)getFieldValue("pay27Amount");
    }

    // pay28Amount : PAY28AMT
    public void setPay28Amount(HYIDouble b) {
            setFieldValue("pay28Amount", b);
    }

    public HYIDouble getPay28Amount() {
            return (HYIDouble)getFieldValue("pay28Amount");
    }

    // pay29Amount : PAY29AMT
    public void setPay29Amount(HYIDouble b) {
            setFieldValue("pay29Amount", b);
    }

    public HYIDouble getPay29Amount() {
            return (HYIDouble)getFieldValue("pay29Amount");
    }

    // pay01Count : PAY01CNT
    public void setPay01Count(Integer b) {
            setFieldValue("pay01Count", b);
    }

    public Integer getPay01Count() {
            return (Integer)getFieldValue("pay01Count");
    }

    // pay02Count : PAY02CNT
    public void setPay02Count(Integer b) {
            setFieldValue("pay02Count", b);
    }

    public Integer getPay02Count() {
            return (Integer)getFieldValue("pay02Count");
    }

    // pay03Count : PAY03CNT
    public void setPay03Count(Integer b) {
            setFieldValue("pay03Count", b);
    }

    public Integer getPay03Count() {
            return (Integer)getFieldValue("pay03Count");
    }

    // pay04Count : PAY04CNT
    public void setPay04Count(Integer b) {
            setFieldValue("pay04Count", b);
    }

    public Integer getPay04Count() {
            return (Integer)getFieldValue("pay04Count");
    }

    // pay05Count : PAY05CNT
    public void setPay05Count(Integer b) {
            setFieldValue("pay05Count", b);
    }

    public Integer getPay05Count() {
            return (Integer)getFieldValue("pay05Count");
    }

    // pay06Count : PAY06CNT
    public void setPay06Count(Integer b) {
            setFieldValue("pay06Count", b);
    }

    public Integer getPay06Count() {
            return (Integer)getFieldValue("pay06Count");
    }

    // pay07Count : PAY07CNT
    public void setPay07Count(Integer b) {
            setFieldValue("pay07Count", b);
    }

    public Integer getPay07Count() {
            return (Integer)getFieldValue("pay07Count");
    }

    // pay08Count : PAY08CNT
    public void setPay08Count(Integer b) {
            setFieldValue("pay08Count", b);
    }

    public Integer getPay08Count() {
            return (Integer)getFieldValue("pay08Count");
    }

    // pay09Count : PAY09CNT
    public void setPay09Count(Integer b) {
            setFieldValue("pay09Count", b);
    }

    public Integer getPay09Count() {
            return (Integer)getFieldValue("pay09Count");
    }

    // pay10Count : PAY10CNT
    public void setPay10Count(Integer b) {
            setFieldValue("pay10Count", b);
    }

    public Integer getPay10Count() {
            return (Integer)getFieldValue("pay10Count");
    }

    // pay11Count : PAY11CNT
    public void setPay11Count(Integer b) {
            setFieldValue("pay11Count", b);
    }

    public Integer getPay11Count() {
            return (Integer)getFieldValue("pay11Count");
    }

    // pay12Count : PAY12CNT
    public void setPay12Count(Integer b) {
            setFieldValue("pay12Count", b);
    }

    public Integer getPay12Count() {
            return (Integer)getFieldValue("pay12Count");
    }

    // pay13Count : PAY13CNT
    public void setPay13Count(Integer b) {
            setFieldValue("pay13Count", b);
    }

    public Integer getPay13Count() {
            return (Integer)getFieldValue("pay13Count");
    }

    // pay14Count : PAY14CNT
    public void setPay14Count(Integer b) {
            setFieldValue("pay14Count", b);
    }

    public Integer getPay14Count() {
            return (Integer)getFieldValue("pay14Count");
    }

    // pay15Count : PAY15CNT
    public void setPay15Count(Integer b) {
            setFieldValue("pay15Count", b);
    }

    public Integer getPay15Count() {
            return (Integer)getFieldValue("pay15Count");
    }

    // pay16Count : PAY16CNT
    public void setPay16Count(Integer b) {
            setFieldValue("pay16Count", b);
    }

    public Integer getPay16Count() {
            return (Integer)getFieldValue("pay16Count");
    }

    // pay17Count : PAY17CNT
    public void setPay17Count(Integer b) {
            setFieldValue("pay17Count", b);
    }

    public Integer getPay17Count() {
            return (Integer)getFieldValue("pay17Count");
    }

    // pay18Count : PAY18CNT
    public void setPay18Count(Integer b) {
            setFieldValue("pay18Count", b);
    }

    public Integer getPay18Count() {
            return (Integer)getFieldValue("pay18Count");
    }

    // pay19Count : PAY19CNT
    public void setPay19Count(Integer b) {
            setFieldValue("pay19Count", b);
    }

    public Integer getPay19Count() {
            return (Integer)getFieldValue("pay19Count");
    }

    // pay20Count : PAY20CNT
    public void setPay20Count(Integer b) {
            setFieldValue("pay20Count", b);
    }

    public Integer getPay20Count() {
            return (Integer)getFieldValue("pay20Count");
    }

    // pay21Count : PAY21CNT
    public void setPay21Count(Integer b) {
            setFieldValue("pay21Count", b);
    }

    public Integer getPay21Count() {
            return (Integer)getFieldValue("pay21Count");
    }

    // pay22Count : PAY22CNT
    public void setPay22Count(Integer b) {
            setFieldValue("pay22Count", b);
    }

    public Integer getPay22Count() {
            return (Integer)getFieldValue("pay22Count");
    }

    // pay23Count : PAY23CNT
    public void setPay23Count(Integer b) {
            setFieldValue("pay23Count", b);
    }

    public Integer getPay23Count() {
            return (Integer)getFieldValue("pay23Count");
    }

    // pay24Count : PAY24CNT
    public void setPay24Count(Integer b) {
            setFieldValue("pay24Count", b);
    }

    public Integer getPay24Count() {
            return (Integer)getFieldValue("pay24Count");
    }

    // pay25Count : PAY25CNT
    public void setPay25Count(Integer b) {
            setFieldValue("pay25Count", b);
    }

    public Integer getPay25Count() {
            return (Integer)getFieldValue("pay25Count");
    }

    // pay26Count : PAY26CNT
    public void setPay26Count(Integer b) {
            setFieldValue("pay26Count", b);
    }

    public Integer getPay26Count() {
            return (Integer)getFieldValue("pay26Count");
    }

    // pay27Count : PAY27CNT
    public void setPay27Count(Integer b) {
            setFieldValue("pay27Count", b);
    }

    public Integer getPay27Count() {
            return (Integer)getFieldValue("pay27Count");
    }

    // pay28Count : PAY28CNT
    public void setPay28Count(Integer b) {
            setFieldValue("pay28Count", b);
    }

    public Integer getPay28Count() {
            return (Integer)getFieldValue("pay28Count");
    }

    // pay29Count : PAY29CNT
    public void setPay29Count(Integer b) {
            setFieldValue("pay29Count", b);
    }

    public Integer getPay29Count() {
            return (Integer)getFieldValue("pay29Count");
    }

    public static Iterator getUploadFailedList(DbConnection connection) throws EntityNotFoundException {
        String failFlag = "2";
        try {
            return getMultipleObjects(connection, CashForm.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
                " WHERE uploadState='" + failFlag + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            throw new EntityNotFoundException(e.toString());
        }
    }

    public HYIDouble getTotalCash() {
        int total =
            getHundred().intValue() * 10000  + getFifty().intValue() *    5000 + getTwenty().intValue() * 2000 +
            getTen().intValue() *      1000  + getFive().intValue() *      500 + getTwo().intValue() *     200 +
            getOne().intValue() *       100  + getFiveJiao().intValue() *   50 + getTwoJiao().intValue() *  20 +
            getOneJiao().intValue() *    10  + getFiveCents().intValue() *   5 + getTwoCents().intValue() *  2 +
            getOneCents().intValue() *    1;
        return new HYIDouble(new BigInteger("" + total), 2);
        /*
        return new HYIDouble(getHundred().intValue() * 100
               + getFifty().intValue() * 50
               + getTwenty().intValue() * 20
               + getTen().intValue() * 10
               + getFive().intValue() * 5
               + getTwo().intValue() * 2
               + getOne().intValue()
               + getFiveJiao().intValue() * 0.5
               + getTwoJiao().intValue() * 0.2
               + getOneJiao().intValue() * 0.1
               + getFiveCents().intValue() * 0.05
               + getTwoCents().intValue() * 0.02
               + getOneCents().intValue() * 0.01);
        */
    }

    public HYIDouble getOtherPayAmount() {
        Iterator itr = Payment.getAllPayment();
        if (itr == null) {
            return new HYIDouble(0);
        }
        HYIDouble totalPayAmount = new HYIDouble(0);
        while (itr.hasNext()) {
            Payment p = (Payment)itr.next();
            String payID = p.getPaymentID();
            if (payID.equals("00")) // skip cash
                continue;
            String key1 = "pay" + payID + "Amount";
            //String key2 = "pay" + payID + "Count";
            Object o = getFieldValue(key1);
            if (o == null || !(o instanceof HYIDouble)) {
                continue;
            }
            HYIDouble value1 = (HYIDouble)o;
            totalPayAmount = totalPayAmount.addMe(value1);
        }
        return totalPayAmount;
    }

    public HYIDouble getTotalPayAmount() {
        return getOtherPayAmount().add(getTotalCash());
    }

	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as String[][] 
	 */
	public static  String[][] getPosToScFieldNameArray() {
		return new String[][] {
            {"storeID", "storeID"},
            {"accountDate", "accountDate"},     //Bruce/20021008
            {"posNumber", "posNumber"},
            {"shiftSequenceNumber", "shiftSequenceNumber"},
            {"zSequenceNumber", "zSequenceNumber"},
            {"uploadState", "uploadState"},
            {"hundred", "hundred"},
            {"fifty", "fifty"},
            {"twenty", "twenty"},
            {"ten", "ten"},
            {"five", "five"},
            {"two", "two"},
            {"one", "one"},
            {"fiveJiao", "fiveJiao"},
            {"fiveJiao", "twoJiao"},
            {"oneJiao", "oneJiao"},
            {"fiveCents", "fiveCents"},
            {"twoCents", "twoCents"},
            {"oneCents", "oneCents"},
            {"pay01Amount", "pay01Amount"},
            {"pay02Amount", "pay02Amount"},
            {"pay03Amount", "pay03Amount"},
            {"pay04Amount", "pay04Amount"},
            {"pay05Amount", "pay05Amount"},
            {"pay06Amount", "pay06Amount"},
            {"pay07Amount", "pay07Amount"},
            {"pay08Amount", "pay08Amount"},
            {"pay09Amount", "pay09Amount"},
            {"pay10Amount", "pay10Amount"},
            {"pay01Count", "pay01Count"},
            {"pay02Count", "pay02Count"},
            {"pay03Count", "pay03Count"},
            {"pay04Count", "pay04Count"},
            {"pay05Count", "pay05Count"},
            {"pay06Count", "pay06Count"},
            {"pay07Count", "pay07Count"},
            {"pay08Count", "pay08Count"},
            {"pay09Count", "pay09Count"},
            {"pay10Count", "pay10Count"},
            {"pay11Amount", "pay11Amount"},
            {"pay12Amount", "pay12Amount"},
            {"pay13Amount", "pay13Amount"},
            {"pay14Amount", "pay14Amount"},
            {"pay15Amount", "pay15Amount"},
            {"pay16Amount", "pay16Amount"},
            {"pay17Amount", "pay17Amount"},
            {"pay18Amount", "pay18Amount"},
            {"pay19Amount", "pay19Amount"},
            {"pay20Amount", "pay20Amount"},
            {"pay21Amount", "pay21Amount"},
            {"pay22Amount", "pay22Amount"},
            {"pay23Amount", "pay23Amount"},
            {"pay24Amount", "pay24Amount"},
            {"pay25Amount", "pay25Amount"},
            {"pay26Amount", "pay26Amount"},
            {"pay27Amount", "pay27Amount"},
            {"pay28Amount", "pay28Amount"},
            {"pay29Amount", "pay29Amount"},
            {"pay11Count", "pay11Count"},
            {"pay12Count", "pay12Count"},
            {"pay13Count", "pay13Count"},
            {"pay14Count", "pay14Count"},
            {"pay15Count", "pay15Count"},
            {"pay16Count", "pay16Count"},
            {"pay17Count", "pay17Count"},
            {"pay18Count", "pay18Count"},
            {"pay19Count", "pay19Count"},
            {"pay20Count", "pay20Count"},
            {"pay21Count", "pay21Count"},
            {"pay22Count", "pay22Count"},
            {"pay23Count", "pay23Count"},
            {"pay24Count", "pay24Count"},
            {"pay25Count", "pay25Count"},
            {"pay26Count", "pay26Count"},
            {"pay27Count", "pay27Count"},
            {"pay28Count", "pay28Count"},
            {"pay29Count", "pay29Count"}
        };

	}
	
    /**
     * Clone CashForm objects for SC with converted field names.
     *
     * This method is only used at POS side.
     */
    public CashForm cloneForSC() {
        return this;
        /*
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            CashForm clonedShift = new CashForm();
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = this.getFieldValue(fieldNameMap[i][0]);
                if (value == null) {
                    try {
                        value = this.getClass().getDeclaredMethod(
                            "get" + fieldNameMap[i][0], new Class[0]).invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedShift.setFieldValue(fieldNameMap[i][1], value);
            }
            return clonedShift;
        } catch (InstantiationException e) {
            return null;
        }
        */
    }
    

}
