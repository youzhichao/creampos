package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DBToolkit;

import hyi.cream.util.DbConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;

/**
 * Transaction DAC class.
 * 
 */
public class TransactionHold extends Transaction {
    private static final long serialVersionUID = 1L;

    public TransactionHold() {
        super();
    }

    public TransactionHold(Transaction tran) {
        this();
        fieldMap.putAll(tran.getFieldMap());
    }

    public String getInsertUpdateTableName() {
        return "tranheadhold";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        return "tranheadhold";
    }

    /**
     * 取得下个交易序号。
     * 
     * @return Next hold transaction number.
     */
    public static int getNextHoldTransactionNumber(DbConnection connection) {
        int nextTransactionNumber;
        int maxSeqInTranhead = 0;

        Statement statement = null;
        ResultSet rst = null;
        try {
            String sql = "SELECT tmtranseq FROM tranheadhold order by tmtranseq desc LIMIT 1 ";
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            rst = statement.executeQuery(sql);
            if (rst.next())
                maxSeqInTranhead = rst.getInt("tmtranseq");

            rst.close();
            rst = null;
            statement.close();
            statement = null;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (rst != null) {
                    rst.close();
                    rst = null;
                }
                if (statement != null) {
                    statement.close();
                    statement = null;
                }
            } catch (SQLException e) {
            }
        }

        nextTransactionNumber = maxSeqInTranhead + 1;
        return nextTransactionNumber;
    }

    public static int holdTranCount(DbConnection connection, int posNumber) {
        return (Integer) getValueOfStatement(connection, "SELECT COUNT(*) AS count FROM "
                + getInsertUpdateTableNameStaticVersion()
                + " WHERE POSNO=" + posNumber);
    }

    public static Iterator queryByPosNumber(DbConnection connection, int posNumber) {
        try {
            return getMultipleObjects(connection, Transaction.class, 
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                    + " WHERE POSNO=" + posNumber + " ORDER BY TMTRANSEQ");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    public static Transaction queryByTransactionNumber(DbConnection connection, int posNumber,
            String number) {
        Transaction tran = null;
        try {
            if (hyi.cream.inline.Server.serverExist()) {
                tran = getSingleObject(connection, Transaction.class,
                        "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                                + " WHERE PosNumber=" + posNumber
                                + " AND TransactionNumber=" + number);
            } else {
                tran = getSingleObject(connection, Transaction.class,
                        "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                                + " WHERE TMTRANSEQ='" + number + "'"
                                +" AND POSNO=" + posNumber);
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
        Iterator itr = LineItemHold.queryByTransactionNumber(connection, posNumber, number);
        if (itr != null) {
            while (itr.hasNext()) {
                tran.addLineItemSimpleVersion((LineItem) itr.next());
            }
        }
        tran.setHoldTranNo(Integer.parseInt(number));
        return tran;
    }

    public static void deleteByPosNO(DbConnection connection, int posNO) throws SQLException {
        executeQuery(connection, "DELETE FROM " + getInsertUpdateTableNameStaticVersion()
                + " WHERE POSNO = " + posNO);
        LineItemHold.deleteByPosNO(connection, posNO);
    }

    public static void deleteByTranNumber(DbConnection connection, int tranNumber) throws SQLException {
        executeQuery(connection, "DELETE FROM " + getInsertUpdateTableNameStaticVersion()
                + " WHERE TMTRANSEQ = " + tranNumber);
        LineItemHold.deleteByTranNumber(connection, tranNumber);
    }

    public Transaction toTransaction() throws InstantiationException {
        Transaction tran = new Transaction();
        tran.init();
        tran.getFieldMap().putAll(getFieldMap());
        return tran;
    }

    public void insert(DbConnection connection, boolean needQueryAgain) throws SQLException {
        String insertString = getInsertSQLStatement();
        DBToolkit.execute(connection, insertString);
        if (needQueryAgain) {
            try {
                load(connection);
            } catch (EntityNotFoundException e) {
                throw new SQLException(e.toString());
            }
        }
    }
    
    public static void makeCancel(DbConnection connection) throws SQLException {
        int posNumber = PARAM.getTerminalNumber();
        Iterator iter = TransactionHold.queryByPosNumber(connection, posNumber);
        if (iter == null)
            return;
        while (iter.hasNext()) {
            Transaction holdTran = (Transaction) iter.next();
            holdTran = TransactionHold.queryByTransactionNumber(connection, holdTran.getTerminalNumber(), holdTran.getTransactionNumber().toString());
            Object[] list = holdTran.getLineItems();
//            trans = Transaction.createCurrentTransaction();
//            int nextTranNumber = Transaction.getNextTransactionNumber();
            Transaction trans = new Transaction();
            trans.init();

            for (int i = 0; i < list.length; i++) {
                LineItem lineItem = (LineItem) list[i];
                try {
                    LineItem tmp = (LineItem) lineItem.clone();
                    tmp.setTransactionNumber(trans.getTransactionNumber());
                    trans.addLineItem(tmp, false);
                } catch (TooManyLineItemsException e) {
                }
            }
            trans.setHoldTranNo(holdTran.getTransactionNumber());
            //trans.setTransactionType("01");
            trans.setTranType1("1");

            trans.setDealType1("*");
            trans.setDealType2("0");
            trans.setDealType3("0");
            trans.initForAccum();
            trans.initSIInfo();
            trans.accumulate();
            trans.store(connection);

            TransactionHold.deleteByTranNumber(connection, trans.getHoldTranNo().intValue());

            int nextTranNumber = Transaction.getNextTransactionNumber();
            Transaction newTran = Transaction.queryByTransactionNumber(connection, posNumber, trans.getTransactionNumber());
            newTran.setTransactionNumber(nextTranNumber - 1);
            newTran.setDealType1("0");
            newTran.setDealType2("0");
            newTran.setDealType3("3");
            newTran.setStoreNumber(Store.getStoreID());
            newTran.setTerminalNumber(PARAM.getTerminalNumber());
            newTran.setVoidTransactionNumber(new Integer((nextTranNumber - 1)
                    * 100 + newTran.getTerminalNumber().intValue()));
            newTran.setTransactionNumber(Transaction.getNextTransactionNumber());
            // Integer.parseInt(GetProperty.getProperty("NextTransactionSequenceNumber")));
            newTran.setSignOnNumber(PARAM.getShiftNumber());
            newTran.setTransactionType(trans.getTransactionType());
            newTran.setTerminalPhysicalNumber(PARAM.getTerminalPhysicalNumber());
            newTran.setCashierNumber(PARAM.getCashierNumber());
            newTran.setInvoiceCount(trans.getInvoiceCount());
            newTran.setInvoiceID(trans.getInvoiceID());
            newTran.setInvoiceNumber(trans.getInvoiceNumber());
            // 统一编号与原有交易相同，所以这里不要重置
            // newTran.setBuyerNumber("");

            newTran.makeNegativeValue();
            // for daishou
            Iterator ite = trans.getLineItemsIterator();
            Iterator ite2 = newTran.getLineItemsIterator();
            while (ite.hasNext()) {
                LineItem li = (LineItem) ite.next();
                LineItem li2 = (LineItem) ite2.next();
                li2.setDaishouNumber(li.getDaishouNumber());
            }

            newTran.store(connection);
        }
    }
}
