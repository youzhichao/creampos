package hyi.cream.dac;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CreditType extends DacBase implements Serializable {
    static final long serialVersionUID = 1L;
    static final String tableName = "credittype";
    private static ArrayList primaryKeys = new ArrayList();
      //static private Set cache;
    private static ArrayList cache;
    
    static {
        primaryKeys.add("nostart");
        primaryKeys.add("noend");
        if (!hyi.cream.inline.Server.serverExist())
            createCache();
    }
    
    public static Iterator queryAll(DbConnection connection) {
        try {
            return DacBase.getMultipleObjects(connection, CreditType.class, "SELECT * FROM " + tableName);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;

        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            return null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public static void reset() {
        cache = new ArrayList();
        createCache();
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public CreditType() throws InstantiationException {

    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public boolean equals(Object obj) {
        if ( !(obj instanceof CreditType))
            return false;
        return (getNoStart().equals(((CreditType)obj).getNoStart()) &&  getNoStart().equals(((CreditType)obj).getNoStart()));
    }

    public String getNoStart() {
        return (String)getFieldValue("nostart");
    }

    public void setNoStart(String value) {
        setFieldValue("nostart", value);
    }

    public String getNoEnd() {
        return (String)getFieldValue("noend");
    }

    public void setNoEnd(String value) {
        setFieldValue("noend", value);
    }

    public String getNoName() {
        return (String)getFieldValue("noname");
    }

    public void setNoName(String value) {
        setFieldValue("noname", value);
    }

    public static String getNoScreen(String noType) {
        String desc = "";
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            CreditType p = (CreditType)itr.next();
            if (p.getNoType().equals(noType)) {
                desc = p.getNoScreen();
                break;
            }
        }
        return desc;
    }

    public String getNoScreen() {
        return (String)getFieldValue("noscreen");
    }

    public void setNoScreen(String value) {
        setFieldValue("noscreen", value);
    }

    public String getCDKey() {
        return (String)getFieldValue("cdkey");
    }

    public void setCDKey(String value) {
        setFieldValue("cdkey", value);
    }

    public String getChkLen() {
        return (String)getFieldValue("chklen");
    }

    public void setChkLen(String value) {
        setFieldValue("chklen", value);
    }
    
    public String getNoType() {
        return (String)getFieldValue("notype");
    }

    public void setNoType(String value) {
        setFieldValue("notype", value);
    }

    public String getDeviceNo() {
        return (String)getFieldValue("deviceno");
    }

    public void setDeviceNo(String value) {
        setFieldValue("deviceno", value);
    }

    public static void createCache() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            cache = new ArrayList();
            Iterator itr = getMultipleObjects(connection, CreditType.class, "SELECT * FROM " + tableName);
            if (itr != null) {
                while (itr.hasNext()) {
                    cache.add(itr.next());
                }
            }
        } catch (EntityNotFoundException e) {
            cache = null;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            cache = null;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
    
    public static CreditType checkCreditNo(String creditNo) {
        if (creditNo == null)
            return null;
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            CreditType p = (CreditType)itr.next();
            //System.out.println(p.getNoType() + "  " + p.getNoName() + "  " + p.getNoScreen() + "  " + p.getNoStart() + "  " + p.getNoEnd());
            //是否要檢查長度.
            int len = creditNo.length();
            int chkLen = Integer.parseInt(p.getChkLen());
            //check credit no length.
            if (chkLen != 0 && len != chkLen) {
                continue;
            }

            //creditNo是否大於等於起始號碼.
            len = p.getNoStart().length();
            if (creditNo.length() < len) {
                continue;
            }
            if (creditNo.substring(0, len).compareTo(p.getNoStart()) < 0) {
                continue;
            }
            
            //creditNo是否小於等於結束號碼.
            len = p.getNoEnd().length();
            if (creditNo.length() < len) {
                continue;
            }
            
            if (creditNo.substring(0, len).compareTo(p.getNoEnd()) >     0) {
                continue;
            }

            return p;
        }
        return null;
    }
/*
    public static void main(String[] args) {
        CreditType type = CreditType.checkCreditNo("1040800092622308");
        if (type != null) {
            System.out.println("card type:" + type.getNoName());
            System.out.println("id:" + type.getNoType());
        }
    }
*/
}
