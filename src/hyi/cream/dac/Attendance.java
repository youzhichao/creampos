/*
 * Created on 2005-3-15
 * for quanjia WorkCheck(考勤系统)
 * 
 */
package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author Administrator
 */
public class Attendance extends DacBase implements Serializable {

    private static final long serialVersionUID = 1L;

	private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("storeID");
		primaryKeys.add("employee");
		primaryKeys.add("shiftType");
		primaryKeys.add("signOnDateTime");

	}

	public Attendance() {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (hyi.cream.inline.Server.serverExist())
			return "posul_work_check";
		else
			return "work_check";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (hyi.cream.inline.Server.serverExist())
			return "posul_work_check";
		else
			return "work_check";
	}

	public static Iterator getCurrentAttendance(DbConnection connection, int zNumber)
        throws EntityNotFoundException {
        try {
            return getMultipleObjects(connection, Attendance.class, "SELECT * FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE zNumber = " + zNumber);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            throw new EntityNotFoundException(e.toString());
        }
    }

	/**
	 * only used at the sc side, do nothing for pos side
	 * 
	 */
	public static void deleteBySequenceNumber(DbConnection connection, int zNumber) throws SQLException {
		if (!hyi.cream.inline.Server.serverExist())
			return;
		String deleteSql = "DELETE FROM "
				+ getInsertUpdateTableNameStaticVersion()
				+ " WHERE zNumber = " + zNumber;
		// System.out.println(deleteSql);
		executeQuery(connection, deleteSql);
	}


	/**
	 * Clone Attendance objects for SC
	 * 
	 * This method is only used at POS side.
	 */
	public static Object[] cloneForSC(Iterator iter) {
		String[][] fieldNameMap = getPosToScFieldNameArray();

		try {
			ArrayList objArray = new ArrayList();
			while (iter.hasNext()) {
				Attendance ad = (Attendance) iter.next();
				// System.out.println("daishouid1=" + ds.getFirstNumber() + ",
				// daishouid2=" + ds.getSecondNumber());
				Attendance clonedAD = new Attendance();
				for (int i = 0; i < fieldNameMap.length; i++) {
					Object value = ad.getFieldValue(fieldNameMap[i][0]);
					if (value == null) {
						try {
							value = ad.getClass().getDeclaredMethod(
									"get" + fieldNameMap[i][0], new Class[0])
									.invoke(ad, new Object[0]);
						} catch (Exception e) {
							value = null;
						}
					}
					clonedAD.setFieldValue(fieldNameMap[i][1], value);
				}
				objArray.add(clonedAD);
			}
			return objArray.toArray();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * return fieldName map of PosToSc as String[][]
	 */
	public static String[][] getPosToScFieldNameArray() {
		return new String[][] { 
				{ "storeID", "storeid" },
				{ "employee", "employee" }, 
				{ "shiftType", "shifttype" },
				{ "signOnDateTime", "signondatetime" }, 
				{ "status", "status" },
				{ "rsvDateTime", "rsvdatetime" },

		};
	}

	/** ********* getters and setters **************** */
	public void setZnumber(Integer znumber) {
		setFieldValue("zNumber", znumber);
	}

	public void setStoreID(String storeID) {
		setFieldValue("storeID", storeID);
	}

	public String getStoreID() {
		return ((String) getFieldValue("storeID"));
	}

	public void setEmployee(String employee) {
		setFieldValue("employee", employee);
	}

	public String getEmployee() {
		return (String) getFieldValue("employee");
	}

	public void setShiftType(String shiftTp) {
		setFieldValue("shiftType", shiftTp);

	}

	public String getShiftType() {
		return (String)getFieldValue("shiftType");
	}

	public void setSignOnDateTime(Date sgnondttm) {
		setFieldValue("signOnDateTime", sgnondttm);

	}

	public Date getSignOnDateTime() {
		return (Date) getFieldValue("signOnDateTime");
	}

	public void setStatus(String status) {
		setFieldValue("status", status);
	}

	public String getStatus() {
		return  (String)getFieldValue("status");
	}

	public void setRsvDateTime(Date rsvdttm) {
		setFieldValue("rsvDateTime", rsvdttm);
	}

	public Date getRsvDateTime() {
		return (Date) getFieldValue("rsvDateTime");
	}



	public static void main(String[] args) {
	}

	/**
	 * 从tranhead-->attendance
	 * return iterator
	 */
	public static Iterator genAttendance(String zNumber) {
        DbConnection connection = null;
        List attendances = new ArrayList();
        try {
            connection = CreamToolkit.getPooledConnection();

            if (zNumber == null)
    			zNumber = String.valueOf(ZReport.getCurrentZNumber(connection));
    
    		// tranhead --> attendance (znumber = zNumber)
    		List list = DacBase.getMultipleObjects(connection, hyi.cream.dac.Transaction.class,
                    "SELECT * FROM tranhead where (dealtype2='B' or dealtype2='C' or dealtype2='J' or dealtype2='K') and eodcnt="
                        + zNumber, null);
            System.out.println("genAttendance legth : " + list.size());		
    		for (Iterator it = list.iterator(); it.hasNext();) {
    
				Transaction trans = (Transaction) it.next();

				Attendance attendance = new Attendance();
				attendance.setZnumber(trans.getZSequenceNumber());
				attendance.setStoreID(trans.getStoreNumber());
				attendance.setEmployee(trans.getEmployeeNumber());
				String type = trans.getDealType2();
				if ("B".equals(type)) {
					attendance.setShiftType("01");
				}
				if ("C".equals(type)) {
					attendance.setShiftType("02");
				}
				if ("J".equals(type)) {
					attendance.setShiftType("03");
				}
				if ("K".equals(type)) {
					attendance.setShiftType("04");
				}
				attendance.setSignOnDateTime(trans.getSystemDateTime());
				if (!attendance.exists(connection))
					attendance.insert(connection);
				attendances.add(attendance);
    		}
    		return attendances.iterator();

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return attendances.iterator();

        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            return attendances.iterator();

        } finally {
            CreamToolkit.releaseConnection(connection);
        }
	}
}
