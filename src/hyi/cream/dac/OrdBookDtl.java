/*
 * Created on 2005-3-15
 * for quanjia WorkCheck(考勤系统)
 * 
 */
package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author Administrator
 */
public class OrdBookDtl extends DacBase implements Serializable {

    private static final long serialVersionUID = 1L;

	private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("storeID");
		primaryKeys.add("billNo");
		primaryKeys.add("itemNo");
		primaryKeys.add("itemSeq");
		primaryKeys.add("dtlseq");
	}

	public OrdBookDtl() {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (hyi.cream.inline.Server.serverExist())
			return "ordbookdtl";
		return "";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (hyi.cream.inline.Server.serverExist())
			return "ordbookdtl";
		return "";
	}

	public void setStoreID(String storeID) {
		setFieldValue("storeID", storeID);
	}

	public String getStoreID() {
		return ((String) getFieldValue("storeID"));
	}

	public void setBillNo(String billNo) {
		setFieldValue("billNo", billNo);
	}

	public String getBillNo() {
		return ((String) getFieldValue("billNo"));
	}

	public void setItemNo(String itemNo) {
		setFieldValue("itemNo", itemNo);
	}

	public String getItemNo() {
		return ((String) getFieldValue("itemNo"));
	}
	
	public void setDtlcode1(String dtlcode1) {
		setFieldValue("dtlcode1", dtlcode1);
	}

	public String getDtlcode1() {
		return ((String) getFieldValue("dtlcode1"));
	}
	
	public void setUpdateUserId(String updateUserId) {
		setFieldValue("updateUserId", updateUserId);
	}

	public String getUpdateUserId() {
		return ((String) getFieldValue("updateUserId"));
	}
	
	public void setFfu(String ffu) {
		setFieldValue("ffu", ffu);
	}

	public String getFfu() {
		return ((String) getFieldValue("ffu"));
	}
	
	public void setItemSeq(Integer itemSeq) {
		setFieldValue("itemSeq", itemSeq);
	}

	public Integer getItemSeq() {
		return (Integer) getFieldValue("itemSeq");
	}

	public void setDtlseq(Integer dtlseq) {
		setFieldValue("dtlseq", dtlseq);
	}

	public Integer getDtlseq() {
		return (Integer) getFieldValue("dtlseq");
	}

	public void setPlanQty(Integer planQty) {
		setFieldValue("planQty", planQty);
	}

	public Integer getPlanQty() {
		return (Integer) getFieldValue("planQty");
	}
	
	public void setQtyBase(Integer qtyBase) {
		setFieldValue("qtyBase", qtyBase);
	}

	public Integer getQtyBase() {
		return (Integer) getFieldValue("qtyBase");
	}
	
	public void setPosNo(Integer posNo) {
		setFieldValue("posNo", posNo);
	}

	public Integer getPosNo() {
		return (Integer) getFieldValue("posNo");
	}
	
	public void setTranNo(Integer tranNo) {
		setFieldValue("tranNo", tranNo);
	}

	public Integer getTranNo() {
		return (Integer) getFieldValue("tranNo");
	}
	
	public void setState(Integer state) {
		setFieldValue("state", state);
	}

	public Integer getState() {
		return (Integer) getFieldValue("state");
	}
	
	public void setIsorder(Integer isorder) {
		setFieldValue("isorder", isorder);
	}

	public Integer getIsorder() {
		return (Integer) getFieldValue("isorder");
	}
	
	public Date getUpdateDate() {
		return (Date) getFieldValue("updateDate");
	}

	public void setUpdateDate(Date updateDate) {
		setFieldValue("updateDate", updateDate);
	}

	public static Iterator getCurrentAttendance(DbConnection connection,
			int zNumber) throws EntityNotFoundException {
		try {
			return getMultipleObjects(connection, OrdBookDtl.class,
					"SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
							+ " WHERE zNumber = " + zNumber);
		} catch (SQLException e) {
			CreamToolkit.logMessage(e);
			throw new EntityNotFoundException(e.toString());
		}
	}

	/**
	 * only used at the sc side, do nothing for pos side
	 * 
	public static void deleteBySequenceNumber(DbConnection connection, int zNumber)
			throws SQLException {
		if (!hyi.cream.inline.Server.serverExist())
			return;
		String deleteSql = "DELETE FROM "
				+ getInsertUpdateTableNameStaticVersion() + " WHERE zNumber = "
				+ zNumber;
		// System.out.println(deleteSql);
		executeQuery(connection, deleteSql);
	}
	 */
}
