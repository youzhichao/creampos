package hyi.cream.dac;

import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DBToolkit;
import hyi.cream.util.HYIDouble;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * 代收明细 class.
 * <P>
 * 
 * @author dai
 */
public class DaishouSales extends DacBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
	 * Constructor
	 */
	transient public static final String VERSION = "1.5";

	static final String tableName = "daishousales";

	private static ArrayList primaryKeys = new ArrayList();

	static {
		if (hyi.cream.inline.Server.serverExist()) {
			primaryKeys.add("storeID");
			primaryKeys.add("accountDate");
			primaryKeys.add("posNumber");
			primaryKeys.add("zSequenceNumber");
			primaryKeys.add("firstNumber");
			primaryKeys.add("secondNumber");
		} else {
			primaryKeys.add("posNumber");
			primaryKeys.add("zSequenceNumber");
			primaryKeys.add("firstNumber");
			primaryKeys.add("secondNumber");
		}
	}

	/**
	 * Constructor
	 */
	public DaishouSales() {
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (hyi.cream.inline.Server.serverExist())
			return "posul_daishousales";
		else
			return "daishousales";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (hyi.cream.inline.Server.serverExist())
			return "posul_daishousales";
		else
			return "daishousales";
	}

	public static DaishouSales queryByFirstNumberAndSecondNumber(DbConnection connection,
			String znumber, String s1, String s2) {
		DaishouSales sqldaishousales = null;
		try {
			sqldaishousales = getSingleObject(connection,
					DaishouSales.class, "SELECT * FROM "
							+ getInsertUpdateTableNameStaticVersion()
							+ " WHERE zSequenceNumber=" + znumber
							+ " AND firstNumber=" + s1
							+ " AND secondNumber=" + s2);
		} catch (SQLException e) {
            CreamToolkit.logMessage(e);
		} catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e);
        }
		return sqldaishousales;
	}

	public static void createDaishouSales(DbConnection connection, int znumber) throws SQLException {

		// get firstnumber and firstname from table: reason
		Iterator ite1 = Reason.queryByreasonCategory("08");
		if (ite1 == null) {
			return;
		}
		Map daishou1Map = new HashMap();
		Reason r = null;
		while (ite1.hasNext()) {
			r = (Reason) ite1.next();
			daishou1Map.put(r.getreasonNumber(), r.getreasonName());
		}
		Iterator ite2 = daishou1Map.keySet().iterator();
		if (ite2 == null) {
			return;
		}
		String key = "";
		String value = "";
		String fileName = "";
		while (ite2.hasNext()) {
			key = (String) ite2.next();
			value = (String) daishou1Map.get(key);
			
			// get secondnumber and secondname from file: daishou[x].conf
			fileName = "daishou" + Integer.valueOf(key).intValue() + ".conf";// wipe of 0 prefix
			fileName = CreamToolkit.getConfigDir() + fileName;

			File propFile = new File(fileName);
			ArrayList menuArrayList = new ArrayList();
			try {
				FileInputStream filein = new FileInputStream(propFile);
				InputStreamReader inst = null;
				inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
				BufferedReader in = new BufferedReader(inst);
				String line;
				char ch = ' ';
				int i;

				do {
					do {
						line = in.readLine();
						if (line == null) {
							break;
						}
						while (line.equals("")) {
							line = in.readLine();
						}
						i = 0;
						do {
							ch = line.charAt(i);
							i++;
						} while ((ch == ' ' || ch == '\t') && i < line.length());
					} while (ch == '#' || ch == ' ' || ch == '\t');

					if (line == null) {
						break;
					}
					menuArrayList.add(line);
				} while (true);
			} catch (FileNotFoundException e) {
				CreamToolkit.logMessage("File is not found: " + propFile
						+ ", at daishousales");
				continue;
			} catch (IOException e) {
				CreamToolkit.logMessage("IO exception at daishousales");
				continue;
			}

			for (int j = 0; j < menuArrayList.size(); j++) {
				String line = (String) menuArrayList.get(j);
				StringTokenizer t = new StringTokenizer(line, ",.");
				String key2 = t.nextToken();
				// String pluN = t.nextToken();
				String value2 = t.nextToken();

				DaishouSales d = new DaishouSales();
				d.setPosNumber(PARAM.getTerminalNumber()); // Bruce
				d.setAccountDate(new Date()); // Bruce
				d.setZSequenceNumber(znumber);
				d.setFirstNumber("" + Integer.valueOf(key).intValue()); // james/20080616/wipe of 0 prefix
				d.setFirstName(value);
				d.setSecondNumber("" + Integer.valueOf(key2).intValue());// james/20080616/wipe of 0 prefix
				d.setSecondName(value2.trim());
				d.setPosAmount(new HYIDouble(0));
				d.setPosCount(0);
				d.insert(connection);
			}
		}
	}

	public static Iterator getCurrentDaishouSales(DbConnection connection, int znumber) {
		try {
            return getMultipleObjects(connection, DaishouSales.class, "SELECT * FROM "
            		+ getInsertUpdateTableNameStaticVersion()
            		+ " WHERE zSequenceNumber = " + znumber);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}

	/**
	 * Only used at the sc side, do nothing for pos side
	 */
	public static void deleteBySequenceNumber(DbConnection connection, int zNumber, int posNumber)
        throws SQLException {
        if (!hyi.cream.inline.Server.serverExist())
            return;
        String deleteSql = "DELETE FROM " + getInsertUpdateTableNameStaticVersion()
            + " WHERE zSequenceNumber =" + zNumber + " AND posNumber = " + posNumber;
        DBToolkit.executeUpdate(connection, deleteSql);
    }

	// properties
	public void setPosNumber(Integer posnumber) {
		setFieldValue("posNumber", posnumber);
	}

	public int getPosNumber() {
		return ((Integer) getFieldValue("posNumber")).intValue();
	}

	public void setZSequenceNumber(int znumber) {
		setFieldValue("zSequenceNumber", new Integer(znumber));
	}

	public int getZSequenceNumber() {
		return ((Integer) getFieldValue("zSequenceNumber")).intValue();
	}

	public void setAccountDate(Date accdate) {
		setFieldValue("accountDate", accdate);
	}

	public Date getAccountDate() {
		return (Date) getFieldValue("accountDate");
	}

	public void setFirstNumber(String fnumber) {
		setFieldValue("firstNumber", fnumber);
	}

	public String getFirstNumber() {
		return (String) getFieldValue("firstNumber");
	}

	public void setFirstName(String fname) {
		setFieldValue("firstName", fname);
	}

	public String getFirstName() {
		return (String) getFieldValue("firstName");
	}

	public void setSecondNumber(String snumber) {
		setFieldValue("secondNumber", snumber);
	}

	public String getSecondNumber() {
		return (String) getFieldValue("secondNumber");
	}

	public void setSecondName(String sname) {
		setFieldValue("secondName", sname);
	}

	public String getSecondName() {
		return (String) getFieldValue("secondName");
	}

	public void setPosCount(int count) {
		setFieldValue("posCount", new Integer(count));
	}

	public int getPosCount() {
		return ((Integer) getFieldValue("posCount")).intValue();
	}

	public void setPosAmount(HYIDouble amount) {
		setFieldValue("posAmount", amount);
	}

	public HYIDouble getPosAmount() {
		return (HYIDouble) getFieldValue("posAmount");
	}

	public void setTcpflg(String tcpflg) {
		setFieldValue("tcpflg", tcpflg);
	}

	public String getTcpflg() {
		return (String) getFieldValue("tcpflg");
	}

	// Bruce/2002-4-27

	public static Iterator getUploadFailedList(DbConnection connection) {
		try {
		    String failFlag = "2";
            return getMultipleObjects(connection, DaishouSales.class, "SELECT * FROM "
            		+ getInsertUpdateTableNameStaticVersion() + " WHERE tcpflg='"
            		+ failFlag + "'");
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            return null;
        } catch (EntityNotFoundException e) {
            return null;
        }
	}

	public String getStoreID() {
		return Store.getStoreID();
	}

	public HYIDouble getSCCount() {
		if (hyi.cream.inline.Server.serverExist())
			return (HYIDouble) getFieldValue("scCount");
		else
			return new HYIDouble(0.0);
	}

	public HYIDouble getSCAmount() {
		if (hyi.cream.inline.Server.serverExist())
			return (HYIDouble) getFieldValue("scAmount");
		else
			return new HYIDouble(0.0);
	}

	/**
	 * Meyer/2003-02-20 return fieldName map of PosToSc as String[][]
	 */
	public static String[][] getPosToScFieldNameArray() {
		return new String[][] { { "StoreID", "storeID" },
				{ "posNumber", "posNumber" },
				{ "zSequenceNumber", "zSequenceNumber" },
				{ "accountDate", "accountDate" },
				{ "firstNumber", "firstNumber" }, { "firstName", "firstName" },
				{ "secondNumber", "secondNumber" },
				{ "secondName", "secondName" }, { "posCount", "posCount" },
				{ "posAmount", "posAmount" }, { "scCount", "scCount" },
				{ "scAmount", "scAmount" } };
	}

	/**
	 * Clone DaishouSales objects for SC with converted field names.
	 * 
	 * This method is only used at POS side.
	 */
	public static Object[] cloneForSC(Iterator iter) {
		String[][] fieldNameMap = getPosToScFieldNameArray();

		ArrayList objArray = new ArrayList();
		while (iter.hasNext()) {
			DaishouSales ds = (DaishouSales) iter.next();
			// System.out.println("daishouid1=" + ds.getFirstNumber()
			// 		+ ", daishouid2=" + ds.getSecondNumber());
			DaishouSales clonedDS = new DaishouSales();
			for (int i = 0; i < fieldNameMap.length; i++) {
				Object value = ds.getFieldValue(fieldNameMap[i][0]);
				if (value == null) {
					try {
						value = ds.getClass().getDeclaredMethod(
								"get" + fieldNameMap[i][0], new Class[0])
								.invoke(ds, new Object[0]);
					} catch (Exception e) {
						value = null;
					}
				}
				clonedDS.setFieldValue(fieldNameMap[i][1], value);
			}
			objArray.add(clonedDS);
		}
		return objArray.toArray();
	}
}
