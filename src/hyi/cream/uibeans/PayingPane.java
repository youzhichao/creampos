package hyi.cream.uibeans;

import hyi.cream.POSTerminalApplication;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.uibeans.customer.CustPayingPaneBanner;
import hyi.cream.util.CreamToolkit;

public class PayingPane extends PayingPaneBanner 
{
	public PayingPane() 
		throws ConfigurationNotFoundException
	{ 
		super(CreamToolkit.getConfigurationFile(PayingPane.class));                                    
	}

	public void setVisible(boolean b) {
        super.setVisible(b);
		POSTerminalApplication.getInstance().setPayingPaneVisible(b);
	}

	public void repaint() {
		super.repaint(); //0, 0, 0, getWidth(), getHeight());
        PayingPaneBanner p1 = POSTerminalApplication.getInstance().getPayingPane1();
        if (p1 != null)
            p1.repaint();
        PayingPaneBanner p2 = POSTerminalApplication.getInstance().getPayingPane2();
        if (p2 != null) {
            p2.repaint();
        }
        DynaPayingPaneBanner p4 = POSTerminalApplication.getInstance().getDyanPayingPane();
        if (p4 != null)
            p4.repaint();
	}

    public void forceRepaint() {
        forcePaint();
        PayingPaneBanner p1 = POSTerminalApplication.getInstance().getPayingPane1();
        if (p1 != null)
            p1.forcePaint();
        PayingPaneBanner p2 = POSTerminalApplication.getInstance().getPayingPane2();
        if (p2 != null) {
            p2.forcePaint();
        }
        CustPayingPaneBanner p3 = POSTerminalApplication.getInstance().getCustPayingPane();
        if (p3 != null) {
        	p3.forcePaint();
        }
        DynaPayingPaneBanner p4 = POSTerminalApplication.getInstance().getDyanPayingPane();
        if (p4 != null)
            p4.forcePaint();
    }
}
