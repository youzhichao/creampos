package hyi.cream.uibeans;

/**
 * Button for translating between traditional and simplified Chinese on-the-fly.
 *
 * @since 2009/5/6 下午 06:01:35
 * @author Bruce You
 */
public class StraitButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param label clear label on button.
     */
    public StraitButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    public StraitButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}
