package hyi.cream.uibeans.duo2;

import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.event.TransactionEvent;
import hyi.cream.event.TransactionListener;
import hyi.cream.util.CreamFormatter;
import hyi.cream.util.HYIDouble;

import java.awt.*;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import org.apache.log4j.Logger;

public class CustDispItemList extends Canvas implements TransactionListener
{
    private static final long serialVersionUID = 1L;

//    static Logger logger = Logger.getLogger(CustDispItemList.class);
    public static final int TITLE = 0;
    public static final int RECORD = 1;
    private int[] rowHeight;
    private int rowCount;
    private Image backgroundImage;
    private int lineItemCount;

    static public class ColumnDefine
    {
        public static final String LEFT = "l";
        public static final String RIGHT = "r";
        public static final String CENTER = "c";
        public String label;
        public int defWidth;
        public int width;
        public String alignment;
        public String property;
        public String pattern;
    }

    private ColumnDefine[] colDef = new ColumnDefine[0];

    class RowData
    {
        String name;
        BigDecimal qty;
        BigDecimal salesAmt;
        BigDecimal unitPrice;


        public String getName()
        {
            return name;
        }
        public void setName(String name)
        {
            this.name = name;
        }
        public BigDecimal getQty()
        {
            return qty;
        }
        public void setQty(BigDecimal qty)
        {
            this.qty = qty;
        }
        public BigDecimal getSalesAmt()
        {
            return salesAmt;
        }
        public void setSalesAmt(BigDecimal salesAmt)
        {
            this.salesAmt = salesAmt;
        }

        public BigDecimal getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }
    }

    private RowData[] rowData = new RowData[0];

    public CustDispItemList()
    {
        //setDoubleBuffered(true);
    }
    
    public void paint(Graphics g)
    {
        System.out.println("CustDispItemList paint..");
        if (backgroundImage == null)
        {
            g.setColor(Color.white);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
        else
        {
            g.drawImage(backgroundImage, 0, 0, this);
        }
        
        drawRecords(g);
    }

    private void drawRecords(Graphics g)
    {
        try
        {
            Rectangle bound = new Rectangle(1, rowHeight[TITLE] + 1, 0, rowHeight[RECORD]);
            String value;

            System.out.println("CustDisp.drawRecords> rowData.length=" + rowData.length);

            if (rowData.length == 0)
                return;

            //int firstRec = 0;
            rowCount = rowData.length;
            //if (rowData.length > rowCount)
            //    firstRec = rowData.length - rowCount;

            // logger.debug("第一筆資料的index:" + firstRec);
            for (int row = 0; row < rowCount; row++)
            {
                bound.x = 1;
                System.out.println("CustDisp.drawRecords> draw row " + row);
                for (int col = 0; col < colDef.length; col++)
                {
                    bound.width = colDef[col].width;
                    value = getFieldValue(rowData[row], colDef[col].property, colDef[col].pattern);
                    if (col == 1) {
                        value = value + "x";
                    }
                    drawField(g, bound, colDef[col].alignment, value, RECORD);
                    bound.x = bound.x + colDef[col].width; // + 1;
                }
                bound.y = bound.y + rowHeight[RECORD]; // + 1;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private String getFieldValue(RowData rowData, String property, String pattern)
    {
        Object ret = null;
        try
        {
            CreamFormatter formatter = CreamFormatter.getInstance();
            Method method = rowData.getClass().getDeclaredMethod("get" + property, new Class[0]);
            ret = method.invoke(rowData, new Object[0]);
            if (ret == null)
            {
                return "";
            }
            String value;
            if (ret instanceof String)
            {
                value = ret.toString();
            }
            else if (ret instanceof BigDecimal)
            {
                value = formatter.format(((BigDecimal)ret).doubleValue(), pattern);
            }
            else if (ret instanceof Integer || ret instanceof Long || ret instanceof Short)
            {
                value = formatter.format(Long.parseLong(ret.toString()), pattern);
            }
            else if (ret instanceof HYIDouble)
            {
                value = formatter.format(((HYIDouble)ret).doubleValue(), pattern);
            }
            else if (ret instanceof Double || ret instanceof Float)
            {
                value = formatter.format(new Double(ret.toString()).doubleValue(), pattern);
            }
            else if (ret instanceof Date)
            {
                value = formatter.format((Date)ret, pattern);
            }
            else
            {
                throw new Exception("尚未支援的資料型態");
            }
            return value;
        }
        catch (Exception e)
        {
            //logger.error("get value fail", e);
            return "Sample";
        }
    }

    private void drawField(Graphics g, Rectangle bound, /*Color fg, Color bg,*/ String alignment,
        String text, int type)
    {
        //System.out.println("CustDisp.drawField> text=" + text + ", bound=" + bound);
        ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

//        text = CacaoResourceHome.retouchString(text);
//        Color c = g.getColor();
//        if (bg != null)
//        {
//            // 與底色不同才畫.
//            if (!bg.equals(bgColor[type]))
//            {
//                g.setColor(bg);
//                g.fillRect(bound.x, bound.y, bound.width, bound.height);
//            }
//        }

        Insets border = new Insets(0, 0, 0, 0);
        g.setClip(bound.x + border.left, bound.y + border.top, bound.width - border.left
            - border.right, bound.height - border.top - border.bottom);

//        if (fg != null)
//        {
//            g.setColor(fg);
//        }
//        else
//        {
//            g.setColor(c);
//        }
//        g.setFont(font[type]);
        //antiAliasingText((Graphics2D)g);
        FontMetrics fm = g.getFontMetrics();
        int w = bound.width - border.left - border.right;
        int x;
        int y = bound.y + bound.height - border.bottom - fm.getDescent();
        if (alignment.equals(ColumnDefine.LEFT))
        {
            x = bound.x + border.left;
        }
        else if (alignment.equals(ColumnDefine.CENTER))
        {
            if (fm.stringWidth(text) > w)
            {
                x = bound.x + border.left;
            }
            else
            {
                x = bound.x + border.left + (w - fm.stringWidth(text)) / 2;
            }
        }
        else
        {
            x = bound.x + border.left + (w - fm.stringWidth(text));
        }
        // System.out.println("draw string at:" + x + "," + y + " [" + text + "]
        // color:" + g.getColor());
        g.drawString(text, x, y);
        //g.setClip(0, 0, (int) currentSize.getWidth(), (int) currentSize.getHeight());
        //g.setColor(c);
    }

//    private void drawBorder(Graphics g)
//    {
//        int y = 0;
//        g.setColor(Color.BLACK);
//        g.drawLine(0, y, (int) currentSize.getWidth(), y);
//        y = rowHeight[TITLE];
//        //g.setColor(Color.LIGHT_GRAY);
//        g.setColor(Color.WHITE);
//        g.drawLine(1, rowHeight[TITLE], (int) currentSize.getWidth(), rowHeight[TITLE]);
//        for (int i = 1; i <= rowCount; i++)
//        {
//            if (i == rowCount)
//                y = (int) currentSize.getHeight() - 1;
//            else
//                y = y + (rowHeight[RECORD]); // + 1);
//            g.drawLine(1, y, (int) currentSize.getWidth(), y);
//        }
//
//        int x = 0;
//        for (int i = 0; i < colDef.length; i++)
//        {
//            if (i == 0)
//            {
//                g.setColor(Color.BLACK);
//            }
//            else
//            {
//                //g.setColor(Color.LIGHT_GRAY);
//                g.setColor(Color.WHITE);
//            }
//            g.drawLine(x, 1, x, (int) currentSize.getHeight() - 1);
//            x = x + colDef[i].width; // + 1;
//        }
//        g.drawLine((int) currentSize.getWidth() - 1, 1, (int) currentSize.getWidth() - 1, (int) currentSize.getHeight() - 1);
//    }

//    private Point getLocation(int x, int y)
//    {
//        Rectangle bound = new Rectangle(1, rowHeight[TITLE] + 1, 0, rowHeight[RECORD]);
//        for (int row = 0; row < rowCount; row++)
//        {
//            bound.x = 1;
//            for (int col = 0; col < colDef.length; col++)
//            {
//                bound.width = colDef[col].width;
//
//                if (bound.contains(x, y))
//                {
//                    return new Point(col, row);
//                }
//                bound.x = bound.x + colDef[col].width + 1;
//            }
//            bound.y = bound.y + rowHeight[RECORD]; // + 1;
//        }
//        return null;
//    }

    private Transaction currentTransaction;

    public void transactionChanged(TransactionEvent e)
    {
        System.out.println("CustDispItemList.transactionChanged().");
        //if (e.getStateChanged() == TransactionEvent.TRANS_INFO_CHANGED)
        //{
        //    return;
        //}
        List rowDataList = new ArrayList();

        //取出transaction中的資料放到RowData.
        Object source = e.getSource();
        Transaction tx;
        if (source instanceof Transaction)
        {
            tx = (Transaction) e.getSource();
            currentTransaction = tx;
        }
        else
            tx = currentTransaction;
        
        if (tx == null)
            return;

        final Object[] lineItems = tx.getLineItems();
        for (int i = 0; i < lineItems.length; i++) {
            LineItem lineItem = (LineItem) lineItems[i];
            if (!isDisplayItem(lineItem) /*|| lineItem.getDisItem().endsWith("2")*/) {
                continue;
            }
            RowData rowData = new RowData();
//            if (lineItem.getECOrderNo() != null && !"".equals(lineItem.getECOrderNo())) {
//                rowData.name = lineItem.getECStoreName() + lineItem.getECOrderNo();
//            } else {
                rowData.name = getItemName(lineItem);
//            }
            rowData.qty = getQty(lineItem);
            rowData.salesAmt = getSalesAmt(lineItem);
            rowData.unitPrice = getUnitPrice(lineItem);
            rowDataList.add(rowData);
        }
        int x = rowDataList.size() - lineItemCount;
        rowDataList = rowDataList.subList(x < 0 ? 0 : x, rowDataList.size());
        rowData = (RowData[])rowDataList.toArray(new RowData[rowDataList.size()]);
        repaint();
    }

//    @Override
//    public void setTransaction(Transaction transaction) {
//    }

    private boolean isDisplayItem(LineItem lineItem)
    {
//        String exclusiveId = CreamProperties.getInstance().getProperty("ItemListExcludeCodeId");
//        // logger.debug("不顯示的LineItem CodeId:" + exclusiveId);
//        if (exclusiveId != null && exclusiveId.indexOf(lineItem.getCodeId()) >= 0)
//        {
//            logger.debug("codeId:" + lineItem.getCodeId() + " don't display");
//            return false;
//        }

        return true;
    }

    public String getItemName(LineItem lineItem)
    {
        return lineItem.getDescription();
    }


    public BigDecimal getQty(LineItem lineItem)
    {
        try {
            return new BigDecimal(lineItem.getQuantity().toString());
        } catch (Exception e) {
            return new BigDecimal("0");
        }
    }

    public BigDecimal getSalesAmt(LineItem lineItem)
    {
        try {
            return new BigDecimal(lineItem.getAfterDiscountAmount().toString());
        } catch (Exception e) {
            return new BigDecimal("0");
        }
    }

    public BigDecimal getUnitPrice(LineItem lineItem) {
         try {
            return new BigDecimal(lineItem.getUnitPrice().toString());
        } catch (Exception e) {
            return new BigDecimal("0");
        }
    }

    public void setColDef(ColumnDefine[] colDef)
    {
        this.colDef = colDef;

        rowCount = lineItemCount;

        int w = getBounds().width;
        int h = getBounds().height;
        //System.out.println("CustDisp> w=" + w + ", h=" + h);

        rowHeight = new int[2];
        //font = new Font[2];
        rowHeight[TITLE] = 0;
        rowHeight[RECORD] = h / lineItemCount;
        //font[TITLE] = (Font)res.getObject(id + "_TitleFont");
        //font[RECORD] = (Font)res.getObject(id + "_RecordFont");
        //bgColor = new Color[2];
        //bgColor[TITLE] = (Color)res.getObject(id + "_TitleBGColor");
        //bgColor[RECORD] = (Color)res.getObject(id + "_RecordBGColor");
        //fontColor = new Color[2];
        //fontColor[TITLE] = (Color)res.getObject(id + "_TileFontColor");
        //fontColor[RECORD] = (Color)res.getObject(id + "_RecordFontColor");
        //selectedBGColor = (Color)res.getObject(id + "_SelectedBGColor");
        //specialPriceItemFontColor = (Color)res.getObject(id + "_SpecialPriceItemFontColor");
        //mmItemFontColor = (Color)res.getObject(id + "_MMItemFontColor");
        //mmTitleItemFontColor = (Color)res.getObject(id + "_MMTitleItemFontColor");
        //fpItemFontColor = (Color)res.getObject(id + "_FPItemFontColor");
        //fpTitleItemFontColor = (Color)res.getObject(id + "_FPTitleItemFontColor");

        //columnCount = colDef.length;
        //int h = rowHeight[TITLE] + rowCount * rowHeight[RECORD] + (rowCount + 1);
        //int w = columnCount + 1;
        //for (int i = 0; i < columnCount; i++)
        //{
        //    w += colDef[i].width;
        //}
    }

    public void setBackgroundImage(Image backgroundImage)
    {
        this.backgroundImage = backgroundImage;
    }

    public void setLineItemCount(int lineItemCount)
    {
        this.lineItemCount = lineItemCount;
    }

    // private TexturePaint getTextTexture() {
    // Font f = font[RECORD].deriveFont(20f);
    // TextLayout tl = new TextLayout("教育訓練", f, new FontRenderContext(null,
    // false, false));
    // int sw = (int) tl.getBounds().getWidth();
    // int sh = (int) (tl.getAscent()+tl.getDescent());
    // BufferedImage bi = new BufferedImage(sw, sh, BufferedImage.TYPE_INT_RGB);
    // Graphics2D tG2 = bi.createGraphics();
    // tG2.setBackground(WHITE);
    // tG2.clearRect(0,0,sw,sh);
    // tG2.setColor(Color.GRAY);
    // tl.draw(tG2, 0, (float) tl.getAscent());
    // Rectangle r = new Rectangle(0,0,sw,sh);
    // return new TexturePaint(bi,r);
    // }

}

