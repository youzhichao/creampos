package hyi.cream.uibeans.duo2;

import hyi.cream.event.TransactionEvent;
import hyi.cream.event.TransactionListener;
import hyi.cream.groovydac.Param;
import hyi.cream.uibeans.duo2.CustDispItemList.ColumnDefine;
import hyi.cream.uibeans.duo2.Marquee;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * Customer dispaly theme: File name: ./ads/theme_[theme name].conf
 * <p/>
 * <pre>
 * ScreenSize=x, h
 * BackgroundImage=[background picture file name]
 * MarqueeArea=x, y, w, h
 * MarqueeFontAndSize=[font name],[size]
 * MarqueeTextColor=r, g, b
 * LineItemArea=x, y, w, h
 * LineItemCount=c
 * LineItemFontAndSize=[font name],[size]
 * LineItemTextColor=r, g, b
 * LineItemContent=品名,220,l,Name,;數量,45,r,Qty,##0;金額,92,r,SalesAmt,#####0
 * TenderArea=x, y, w, h
 * TenderFontAndSize=[font name],[size]
 * TenderTextColor=r, g, b
 * TenderContent=合計金額,170,r,SaleAmt,#####0;收　　您,170,r,PayTotal,#####0;餘　　額,170,r,Balance,#####0;找　　零,170,r,ChangeAmt,#####0
 * VideoArea=x, y, w, h
 * SmallAdsArea1=x, y, w, h
 * SmallAdsArea2=x, y, w, h
 * </pre>
 * <p/>
 * Small Ads. area: File name: ./ads/small_ads_area1.conf, ./ads/small_ads_area2.conf
 * <p/>
 * <pre>
 * picture1.png, 10
 * picture2.jpg, 70
 * </pre>
 *
 * @author Bruce You
 */
public class SecondScreen extends JFrame implements TransactionListener {
    private static final long serialVersionUID = 1L;

    public static final int NORMAL_PRIORITY_NICE = 0;

    private int playerNiceValue = NORMAL_PRIORITY_NICE;

    private static SecondScreen instance;

    private SimpleDateFormat dateFormatter;
    private PrintWriter logger;
    private List themeInfos = new ArrayList();
    private int currentThemeIndex = -1;
    private JPanel backgroundPane;

    // private JFlashPlayer flashPlayer;

    private List adsPictureInfos1 = new ArrayList();
    private List adsPictureInfos2 = new ArrayList();
    private int currentAdsPictureIndex1 = -1;
    private int currentAdsPictureIndex2 = -1;

    private Marquee marquee;
    private String marqueeMessages;
    private Panel videoPanel;
    private Writer videoCommandWriter;
    private CustDispItemList itemList;
    private CustDispTenderList tenderList;
    private PicturePanel smallAdsArea1;
    private PicturePanel smallAdsArea2;

    private class ThemeInfo {
        String themeName;

        // Screen size
        Dimension screenSize;

        Point position;

        // Background image
        File background;
        BufferedImage backgroundImage;

        // Marquee
        Rectangle marqueeArea;
        Font marqueeFont;
        Color marqueeTextColor;

        // Item list
        Rectangle lineItemArea;
        int lineItemCount;
        Font lineItemFont;
        Color lineItemTextColor;
        ColumnDefine[] lineItemColumns;

        // Tender
        Rectangle tenderArea;
        Font tenderFont;
        Color tenderTextColor;
        ColumnDefine[] tenderColumns;

        // Video
        Rectangle videoArea;

        // Picture ads
        Rectangle smallAdsArea1;
        Rectangle smallAdsArea2;
    }

    private class AdsPictureInfo {
        String fileName;
        int displayTime; // in seconds
    }

    private class PicturePanel extends Canvas {
        private static final long serialVersionUID = 1L;

        private Image currentImage;

        public void paint(Graphics g) {
            g.drawImage(currentImage, 0, 0, this);
        }

        public Image getCurrentImage() {
            return currentImage;
        }

        public void setCurrentImage(BufferedImage currentImage) {
            int w = getBounds().width;
            int h = getBounds().height;
            this.currentImage = currentImage.getScaledInstance(w, h, Image.SCALE_SMOOTH);
            repaint();
        }
    }

    public SecondScreen(String title, GraphicsConfiguration gc) {
        super(title, gc);

        // stop any suspended MPlayer
        try {
            Runtime.getRuntime().exec("/usr/bin/killall -9 mplayer").waitFor();
        } catch (Exception e) {
        }

        openLog();

        // load theme configuration
        loadConfiguration();
        if (themeInfos.isEmpty())
            return;

        // get current theme
        currentThemeIndex = 0;

        // set frame size
        setSize(((ThemeInfo) themeInfos.get(currentThemeIndex)).screenSize);
        setLocation(((ThemeInfo) themeInfos.get(currentThemeIndex)).position);

        // create content pane for drawing background picture, and set null layout
        backgroundPane = new JPanel() {
            public void paintComponent(Graphics g) {
                if (g.getClipBounds().equals(marquee.getBounds()))
                    return;

                ThemeInfo themeInfo = getCurrentTheme();
                if (themeInfo != null && themeInfo.backgroundImage != null) {
                    g.drawImage(themeInfo.backgroundImage, 0, 0, this);
                }
            }
        };
        setLayout(new BorderLayout());
        getContentPane().add(backgroundPane, BorderLayout.CENTER);
        backgroundPane.setLayout(null);

        createMarqueeArea();
        createVideoArea();
        createLineItemArea();
        createTenderArea();
        //createSmallAdsArea();

        //setCursor();
        instance = this;
    }

    private void openLog() {
        try {
            dateFormatter = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");
            logger = new PrintWriter(new OutputStreamWriter(
                    new FileOutputStream("log/"+ File.separator + "player.log", true), "GBK"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void log(String msg) {
        String dateString = dateFormatter.format(new Date());
        logger.print("[" + dateString + "] ");
        logger.println(msg);
        logger.flush();
    }

    public JPanel getBackgroundPane() {
        return backgroundPane;
    }

    public static SecondScreen getInstance() {
        return instance;
    }

    private void sleepInSecond(int second) {
        try {
            Thread.sleep(second * 1000L);
        } catch (InterruptedException e) {
        }
    }

    private void createSmallAdsArea() {
        ThemeInfo themeInfo = getCurrentTheme();
        if (themeInfo == null)
            return;

        if (!adsPictureInfos1.isEmpty()) {
            if (smallAdsArea1 != null) {
                smallAdsArea1.setBounds(themeInfo.smallAdsArea1);
            } else {
                smallAdsArea1 = new PicturePanel();
                smallAdsArea1.setBounds(themeInfo.smallAdsArea1);
                getBackgroundPane().add(smallAdsArea1);
                new Thread() {
                    public void run() {
                        for (; ; ) {
                            AdsPictureInfo adsPictureInfo = (AdsPictureInfo) adsPictureInfos1.get(currentAdsPictureIndex1);
                            smallAdsArea1.setCurrentImage(
                                    getBufferedImageFromFile(new File(adsPictureInfo.fileName)));
                            sleepInSecond(adsPictureInfo.displayTime);
                            if (++currentAdsPictureIndex1 >= adsPictureInfos1.size())
                                currentAdsPictureIndex1 = 0;
                        }
                    }
                }.start();
            }
        }
        if (!adsPictureInfos2.isEmpty()) {
            if (smallAdsArea2 != null) {
                smallAdsArea2.setBounds(themeInfo.smallAdsArea2);
            } else {
                smallAdsArea2 = new PicturePanel();
                smallAdsArea2.setBounds(themeInfo.smallAdsArea2);
                getBackgroundPane().add(smallAdsArea2);
                new Thread() {
                    public void run() {
                        for (; ; ) {
                            AdsPictureInfo adsPictureInfo = (AdsPictureInfo) adsPictureInfos2.get(currentAdsPictureIndex2);
                            smallAdsArea2.setCurrentImage(
                                    getBufferedImageFromFile(new File(adsPictureInfo.fileName)));
                            sleepInSecond(adsPictureInfo.displayTime);
                            if (++currentAdsPictureIndex2 >= adsPictureInfos2.size())
                                currentAdsPictureIndex2 = 0;
                        }
                    }
                }.start();
            }
        }
    }

    private ThemeInfo getCurrentTheme() {
        return (ThemeInfo) themeInfos.get(currentThemeIndex);
    }

    public void changeTheme(boolean forward) {
        if (themeInfos.size() > 1) {
            if (forward) {
                if (++currentThemeIndex >= themeInfos.size())
                    currentThemeIndex = 0;
            } else {
                if (--currentThemeIndex < 0)
                    currentThemeIndex = themeInfos.size() - 1;
            }

            createMarqueeArea();
            createVideoArea();
            createLineItemArea();
            createTenderArea();
            //createSmallAdsArea();
            repaint();
        }
    }

    private void createMarqueeArea() {
        ThemeInfo themeInfo = getCurrentTheme();
        if (themeInfo != null) {
            if (marquee == null) {
                marquee = new Marquee();
                getBackgroundPane().add(marquee);
            }

            marquee.setBounds(themeInfo.marqueeArea);
            marquee.setFont(themeInfo.marqueeFont);
            marquee.setForeground(themeInfo.marqueeTextColor);
            int x = themeInfo.marqueeArea.x;
            int y = themeInfo.marqueeArea.y;
            int w = themeInfo.marqueeArea.width;
            int h = themeInfo.marqueeArea.height;
            marquee.setBackgroundImage(themeInfo.backgroundImage.getSubimage(x, y, w, h));
            setMarqueeMessages(null);
            System.out.println("createMarqueeArea()");
        }
    }

    public void setMarqueeMessages(List<String> messages) {
//        if (marquee != null)
//            return;
//
        if (messages == null || messages.isEmpty()) {
            marquee.setBroadcastMessage(Param.getInstance().getSecondScreenMarquee());
            return;
        }
        StringBuilder buf = new StringBuilder();
        for (String message : messages) {
            buf.append(message);
            buf.append('\t');
        }
        String msg = buf.toString();
        if (!msg.equals(marqueeMessages)) {
            marquee.setBroadcastMessage(msg);
            marqueeMessages = msg;
        }
    }

    private void createVideoArea() {
        ThemeInfo themeInfo = getCurrentTheme();
        if (themeInfo != null) {
            if (videoPanel == null) {
                videoPanel = new Panel();
                videoPanel.setName("VideoPanel");
                videoPanel.setBackground(Color.black);
                videoPanel.setBounds(themeInfo.videoArea);
                getBackgroundPane().add(videoPanel);
            } else {
                videoPanel.setBounds(themeInfo.videoArea);
                videoPanel.setVisible(true);
                videoPanel.repaint();
                //sendVideoCommand("pause");
                sendVideoCommand("pt_step +1");
                sendVideoCommand("pt_step -1");
            }
            System.out.println("createVideoArea()");
        }
    }

    private void createLineItemArea() {
        ThemeInfo themeInfo = getCurrentTheme();
        if (themeInfo != null) {
            if (itemList == null) {
                itemList = new CustDispItemList();
                getBackgroundPane().add(itemList);
            }

            itemList.setFont(themeInfo.lineItemFont);
            itemList.setForeground(themeInfo.lineItemTextColor);
            itemList.setBounds(themeInfo.lineItemArea);
            itemList.setVisible(true);
            itemList.setLineItemCount(themeInfo.lineItemCount);
            int x = themeInfo.lineItemArea.x;
            int y = themeInfo.lineItemArea.y;
            int w = themeInfo.lineItemArea.width;
            int h = themeInfo.lineItemArea.height;
            itemList.setBackgroundImage(themeInfo.backgroundImage.getSubimage(x, y, w, h));
            itemList.setColDef(themeInfo.lineItemColumns);
            itemList.transactionChanged(new TransactionEvent(this, TransactionEvent.ITEM_CHANGED));
            System.out.println("createLineItemArea()");
        }
    }

    private void createTenderArea() {
        ThemeInfo themeInfo = getCurrentTheme();
        if (themeInfo != null) {
            if (tenderList == null) {
                tenderList = new CustDispTenderList();
                getBackgroundPane().add(tenderList);
            }

            tenderList.setFont(themeInfo.tenderFont);
            tenderList.setForeground(themeInfo.tenderTextColor);
            tenderList.setBounds(themeInfo.tenderArea);
            int x = themeInfo.tenderArea.x;
            int y = themeInfo.tenderArea.y;
            int w = themeInfo.tenderArea.width;
            int h = themeInfo.tenderArea.height;
            tenderList.setBackgroundImage(themeInfo.backgroundImage.getSubimage(x, y, w, h));

            /*System.out.println("lg.1:" + themeInfo.tenderColumns.length);
            System.out.println("lg.2:" + themeInfo.tenderColumns[0].label);
            System.out.println("lg.3:" + themeInfo.tenderColumns[0].width);
            System.out.println("lg.4:" + themeInfo.tenderColumns[1].label);
            System.out.println("lg.5:" + themeInfo.tenderColumns[1].width);*/

            tenderList.setColDef(themeInfo.tenderColumns);
            tenderList.repaint(); System.out.println("createTenderArea()");
        }
    }

    private void loadConfiguration() {
        File dir = new File("./ads");
        if (!dir.exists() || !dir.isDirectory())
            return;

        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith("theme_") && name.endsWith(".conf");
            }
        });
        for (int i = 0; i < files.length; i++) {
            try {
                File themeConfFile = files[i];
                ThemeInfo themeInfo = new ThemeInfo();
                String s = themeConfFile.getName().substring(6);
                themeInfo.themeName = s.substring(0, s.length() - 5);

                CreamToolkit.logMessage("Theme name:" + themeInfo.themeName);

                Properties prop = new Properties();
                prop.load(new FileInputStream(themeConfFile));
                prop.load(new FileInputStream(themeConfFile));
                prop.load(new InputStreamReader(new FileInputStream(themeConfFile),
                    CreamToolkit.getEncoding()));

                /*---------------------------------------------------------------------------
                Map prop = new HashMap();
                try {
                    FileInputStream filein = new FileInputStream(themeConfFile);
                    InputStreamReader inst = null;
                    inst = new InputStreamReader(filein, GetProperty.getConfFileLocale("GBK"));
                    BufferedReader in = new BufferedReader(inst);
                    String line;
                    char ch = ' ';
                    int x;

                    do {
                        do {
                            line = in.readLine();
                            if (line == null) {
                                break;
                            }
                            while (line != null && line.equals("")) {
                                line = in.readLine();
                            }
                            if (line == null)
                                break;
                            x = 0;
                            do {
                                ch = line.charAt(x);
                                x++;
                            } while ((ch == ' ' || ch == '\t') && x < line.length());
                        } while (ch == '#' || ch == ' ' || ch == '\t');

                        if (line == null) {
                            break;
                        }
                        StringTokenizer st = new StringTokenizer(line, "=");
                        if (st.countTokens() == 2) {
                            prop.put(st.nextToken(), st.nextToken());
                        }
                    } while (true);

                } catch (Exception e) {
                    CreamToolkit.logMessage("File is not found: " + themeConfFile);
                    continue;
                }
                //---------------------------------------------------------------------------
                */

                themeInfo.screenSize = parseDimensionInfo((String) prop.get("ScreenSize"));
                themeInfo.position = parsePointInfo((String) prop.get("Position"));
                themeInfo.background = new File((String) prop.get("BackgroundImage"));
                themeInfo.backgroundImage = getBufferedImageFromFile(themeInfo.background);
                themeInfo.marqueeArea = parseRectangleInfo((String) prop.get("MarqueeArea"));
                themeInfo.marqueeTextColor = parseColorInfo((String) prop.get("MarqueeTextColor"));
                themeInfo.marqueeFont = parseFontInfo((String) prop.get("MarqueeFontAndSize"));
                themeInfo.lineItemArea = parseRectangleInfo((String) prop.get("LineItemArea"));
                themeInfo.lineItemCount = Integer.parseInt((String) prop.get("LineItemCount"));
                themeInfo.lineItemTextColor = parseColorInfo((String) prop.get("LineItemTextColor"));
                themeInfo.lineItemFont = parseFontInfo((String) prop.get("LineItemFontAndSize"));
                themeInfo.lineItemColumns = parseColumnInfo((String) prop.get("LineItemContent"));
                themeInfo.tenderArea = parseRectangleInfo((String) prop.get("TenderArea"));
                themeInfo.tenderFont = parseFontInfo((String) prop.get("TenderFontAndSize"));
                themeInfo.tenderTextColor = parseColorInfo((String) prop.get("TenderTextColor"));
                themeInfo.tenderColumns = parseColumnInfo((String) prop.get("TenderContent"));
                themeInfo.videoArea = parseRectangleInfo((String) prop.get("VideoArea"));
                themeInfo.smallAdsArea1 = parseRectangleInfo((String) prop.get("SmallAdsArea1"));
                themeInfo.smallAdsArea2 = parseRectangleInfo((String) prop.get("SmallAdsArea2"));
                themeInfos.add(themeInfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        loadAdsPictureInfo("./ads/small_ads_area1.conf", adsPictureInfos1);
        if (!adsPictureInfos1.isEmpty())
            currentAdsPictureIndex1 = 0;
        loadAdsPictureInfo("./ads/small_ads_area2.conf", adsPictureInfos2);
        if (!adsPictureInfos2.isEmpty())
            currentAdsPictureIndex2 = 0;
    }

    public static String[] split(String str, String delim) {
        List ts = new ArrayList();
        StringTokenizer t = new StringTokenizer(str, delim);
        while (t.hasMoreTokens()) {
            ts.add(t.nextToken().trim());
        }
        String[] ta = new String[ts.size()];
        for (int i = 0; i < ts.size(); i++)
            ta[i] = (String) ts.get(i);
        return ta;
    }

    private void loadAdsPictureInfo(String confFilename, List adsPictureInfos) {
        try {
            List lines = FileUtils.readLines(new File(confFilename), CreamToolkit.getEncoding());
            Iterator iter = lines.iterator();
            while (iter.hasNext()) {
                String ln = (String) iter.next();
                String line = (String) ln;
                String[] v = split(line, ",");
                File file = new File(v[0]);
                if (file.exists()) {
                    try {
                        AdsPictureInfo adsPictureInfo = new AdsPictureInfo();
                        adsPictureInfo.fileName = file.getAbsolutePath();
                        adsPictureInfo.displayTime = Integer.parseInt(v[1]);
                        adsPictureInfos.add(adsPictureInfo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ColumnDefine[] parseColumnInfo(String value) {
        //System.out.println("lg:x" + value);
        String[] v = split(value, ";");
        ColumnDefine[] columnDefines = new ColumnDefine[v.length];
        //System.out.printf("parseColumnInfo> columnDefines count=%d\n", v.length);
        for (int i = 0; i < v.length; i++) {
            String u = v[i];
            //System.out.printf("parseColumnInfo> columnDefines[%d]=%s\n", i, u);
            try {
                // u like: 數量,45,r,Qty,##0
                String[] w = split(u, ",");
                columnDefines[i] = new ColumnDefine();
                if (w[0].equals("Pay"))
                    columnDefines[i].label = "";
                else
                    columnDefines[i].label = w[0];
                //System.out.println("lg:parseColumnInfo> columnDefines[%d].label="+ ((ColumnDefine)(columnDefines[i])).label);
                columnDefines[i].width = Integer.parseInt(w[1]);
                //System.out.println("lg:parseColumnInfo> columnDefines[%d].width=" + ((ColumnDefine)(columnDefines[i])).width);
                columnDefines[i].alignment = w[2];
                //System.out.printf("parseColumnInfo> columnDefines[%d].alignment=%s\n", i, columnDefines[i].alignment);
                columnDefines[i].property = w[3];
                //System.out.printf("parseColumnInfo> columnDefines[%d].property=%s\n", i, columnDefines[i].property);
                columnDefines[i].pattern = w.length >= 5 ? w[4] : "";
                //System.out.printf("parseColumnInfo> columnDefines[%d].pattern=%s\n", i, columnDefines[i].pattern);
                //System.out.println("lg:i:"+ i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //System.out.println("lg:parse over");
        return columnDefines;
    }

    private Color parseColorInfo(String value) {
        try {
            String[] v = split(value, ",");
            return new Color(Integer.parseInt(v[0]), Integer.parseInt(v[1]), Integer.parseInt(v[2]));
        } catch (Exception e) {
            return Color.black;
        }
    }

    private Dimension parseDimensionInfo(String value) {
        String[] v = split(value, ",");
        return new Dimension(Integer.parseInt(v[0]), Integer.parseInt(v[1]));
    }

    private Point parsePointInfo(String value) {
        String[] v = split(value, ",");
        return new Point(Integer.parseInt(v[0]), Integer.parseInt(v[1]));
    }

    private Rectangle parseRectangleInfo(String value) {
        String[] v = split(value, ",");
        return new Rectangle(Integer.parseInt(v[0]), Integer.parseInt(v[1]),
                Integer.parseInt(v[2]), Integer.parseInt(v[3]));
    }

    private Font parseFontInfo(String value) {
        String[] v = split(value, ",");
        return new Font(v[0], Font.PLAIN, Integer.parseInt(v[1]));
    }

    /**
     * Form a BufferedImage from a image file.
     *
     * @param
     */
    public BufferedImage getBufferedImageFromFile(File iconFile) {
        Image image = Toolkit.getDefaultToolkit().getImage(iconFile.toString());
        MediaTracker tracker = new MediaTracker(this);
        tracker.addImage(image, 0);
        try {
            tracker.waitForID(0);
        } catch (InterruptedException e) {
        }
        return toBufferedImage(image);

        /*
        try
        {
            FileInputStream in = new FileInputStream(iconFile);
            ImageInputStream iin = ImageIO.createImageInputStream(in);
            ImageReader reader = ImageIO.getImageReaders(iin).next();
            reader.setInput(iin, true, true);
            BufferedImage image = reader.read(0);
            iin.close();
            return image;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
        */
    }

    private BufferedImage toBufferedImage(Image image) {
        if (image instanceof BufferedImage) {
            return (BufferedImage) image;
        }
        // This code ensures that all the pixels in the image are loaded
        image = new ImageIcon(image).getImage();
        // Determine if the image has transparent pixels; for this method's
        // implementation, see Determining If an Image Has Transparent Pixels
        boolean hasAlpha = false; //hasAlpha(image);
        // Create a buffered image with a format that's compatible with the screen
        BufferedImage bimage = null;
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        try {
            // Determine the type of transparency of the new buffered image
            int transparency = Transparency.OPAQUE;
            if (hasAlpha) {
                transparency = Transparency.BITMASK;
            }
            // Create the buffered image
            GraphicsDevice gs = ge.getDefaultScreenDevice();
            GraphicsConfiguration gc = gs.getDefaultConfiguration();
            bimage = gc.createCompatibleImage(
                    image.getWidth(null), image.getHeight(null), transparency);
        } catch (Exception e) {
            // The system does not have a screen
        }
        if (bimage == null) {
            // Create a buffered image using the default color model
            int type = BufferedImage.TYPE_INT_RGB;
            if (hasAlpha) {
                type = BufferedImage.TYPE_INT_ARGB;
            }
            bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);
        }
        // Copy image to buffered image
        Graphics g = bimage.createGraphics();
        // Paint the image onto the buffered image
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return bimage;
    }

    /**
     * 查mplayer slave mode command說明：
     * <pre>
     * gzip -d -c /usr/share/doc/mplayer-doc/tech/slave.txt.gz|less
     * </pre>
     */
    public void playVideo() {
        try {
            System.out.println("playVideo()");
            while (!isShowing())
                Thread.sleep(2000);

            // ONLY WORK ON LINUX
            Process p = Runtime.getRuntime().exec("xwininfo -all -name HYI_CUST_FRAME");
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            String videoPanelLine = "";
            while ((line = br.readLine()) != null)
                if (line.indexOf("PanelPeer") != -1)
                    videoPanelLine = line; // get the last line
            br.close();
            final String videoPanelWid = split(videoPanelLine.trim(), " ")[0];
            log("VideoPanelWid: " + videoPanelWid);

            new Thread() {
                public void run() {
                    for (; ; ) {
                        try {
                            final String mplayerCommand = //"nice -n " + getPlayerNiceValue()
                                    " /usr/local/bin/mplayer -quiet -ontop -loop 5 -wid "
                                            + videoPanelWid
                                            + " -vo x11 -af volume=10.1 -framedrop"
                                            + " -slave " // -aspect 4:3 -noconsolecontrols
                                            + " -ao oss "
                                            + " -zoom " // + " -y " + videoPanel.getHeight()
                                            + " -playlist ./videos/playlist.txt";
                            log(mplayerCommand);

                            Process playerProcess = Runtime.getRuntime().exec(mplayerCommand);
                            final InputStream stdout = playerProcess.getInputStream();
                            final BufferedReader stderr = new BufferedReader(new InputStreamReader(playerProcess.getErrorStream()));
                            videoCommandWriter = new OutputStreamWriter(playerProcess
                                    .getOutputStream(), "latin1");

                            byte[] bytes = new byte[512];
                            for (; ; ) {
                                if (stdout.read(bytes) == -1)
                                    break;

                                String line;
                                if ((line = stderr.readLine()) != null)
                                    log("stderr> " + line);
                            }
                            log("Exit code=" + playerProcess.waitFor());
                            sleep(1500);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Send video command to mplayer.
     */
    public void sendVideoCommand(String command) {
        try {
            videoCommandWriter.write(command);
            videoCommandWriter.write('\n');
            videoCommandWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void transactionChanged(TransactionEvent e) {
        //System.out.println("lg:y" + ((Transaction) e.getSource()).getChangeAmount());
        if (itemList != null)
            itemList.transactionChanged(e);
        if (tenderList != null)
            tenderList.transactionChanged(e);
    }

//    @Override
//    public void setTransaction(Transaction transaction) {
//    }

    public int getPlayerNiceValue() {
        return playerNiceValue;
    }

    public void setPlayerNiceValue(int playerNiceValue) {
        this.playerNiceValue = playerNiceValue;
    }

/*
    public static void main(String[] args) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        GraphicsDevice gd = gs[0];
        SecondScreen custDispFrame = new SecondScreen("HYI_CUST_FRAME", gd.getDefaultConfiguration());
        custDispFrame.setSize(800, 600);
        custDispFrame.setVisible(true);
    }
*/
}
