package hyi.cream.uibeans.duo2;

import groovy.lang.Closure;
import hyi.cream.util.CreamToolkit;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.TimingTarget;
import org.jdesktop.animation.timing.interpolation.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * Marquee.
 * - 可以显示三种信息，priority: warningMessage > informationMessage > broadcastMessage
 *
 * @author Bruce's photo:
 *
 *   $$Z$$$$$$$$$$7$77777NNN8N8ZOOO7?D8$7$O$7I7Z88III888888OOMO$8M:~~~===++++++++++++
 *   $$$$$$$$$$$$$7$7777NND7$8$OOOOO8OOO7IO$7ZOOO8IIDO777$Z887NN$O8:~~~===++++++?++++
 *   $$$$$$$$$$$$$$$777ZNDZ7IIOOOOOOZZI8N77OOOOO887$NNNNNNN8O7$NOZOO~~~====++++++++?+
 *   $$$$$$$$$$$$$$$$$7D8$OO7?IOOOO$7ZN$7OOOOOOOZIINNNNNNNNNNMMND78OO~~~====+++++++??
 *   $$Z$$$$$$$$$$$$7$78IOOO$$????ZNN8+ZOOOOOOZ??ZNNNNNNNNNNNNNNNI88ID~~~==++++++?+++
 *   $$Z$$$$$$$$$$$$$7Z7?OOOOO?7MNNI$OOOZ?+?DNNNNNNNNNNNNNNNNNNNNI88INO~~===+++++++++
 *   $$$Z$$$$$$$$$$$$Z$7D$OOOO$7NN?7II8NNNNNNNNNNNNNNNNMNNNNNNNNN7OOO8MZ~=++++++?++++
 *   $$$Z$$$$$$$$$$$$ZZ8OZOOOO?MNMNNMNNNNND$????+++++IZDNNNNNNNNNM7O8$NN~==+++++?++++
 *   $$$Z$$$$$$$$$$$ZZZNO$OOOINNNNNNMMZ?+++++++===++++????ZNNNNNNNM7OOIO7~==+++++++++
 *   $$$$$$$$$$$$$$$O$NDM?Z?ZNNNNNN7?+++++=~~~==~==+??7$$$$Z$8NNNNNNIOOOO=+=+++++++++
 *   Z$$Z$$$$$$$$$$$OOZI7$ZNNNNNN$7$$7?+?====~~==+??I$OZZZOOOZD8NNNNNIOOO~===++++++++
 *   $$$$$$$$$$$$$$$ZOOOINNNNNNZOO8OOOOZ7I??=~~=+?????+++???IIZODONNNN$OO?====+++++++
 *   $$ZZ$$$$$$$$$$$OOOO$NNNMD8$7I?++?+++???+===+???+?$IDD88888DNMDOMNN$OZ===++++++++
 *   $$ZZ$$$$$$$$$$$OOOONNN8O7ODNNNNNNZ.OZ???=~=+$DD7..$8O8$$$7ZO$8M7NNDZZ====++?++++
 *   $ZZ$Z$$$$$$$$$$IOOZNMND8$ZZI7$OZDOZ$I:I$NDZM7?II$$?8$OIZO$ZZ8IN7MNN8Z===++++++++
 *   $$$$Z$$$$$$$$$$8$ODNNMD$ZZ$Z8I8DO+Z$??+DO==+OII+?I7I+?I7$$ZZZ8M$MNND8?==++?++++=
 *   8DNDO$$$Z$$$$$$NZZNMMN$$$ZO$I+++I77I??=M+~=+7DII+????II777II?DM$ZNNDN7=+++++++++
 *   NNNDDDNDZ$$Z$$$NNZNMMNMZ7Z7?IIII????++7O+==+IOOI????????I??7NDZIZNNNNI==++++++++
 *   DNNNNDDNNNZ$$$$NNNN8Z$MN$7I+++++++?$DO7+?++?I77I77I$$7II+=++??II7DNNN===++++++++
 *   Z8DNNDDDDDDZ$$$NNNM8Z777III7$$$7I+?II?+??+??I7II??????++=++???IIIDMMN=++++++++++
 *   ZZO8O88DNND8$$$ZNNM8ZI??I?++++++++++?I?I7+??I777$I++=?+++++???IIIOMMD=++++++++++
 *   ZOO7ZONNNNNOZZ$$NNMDOII?++++++++==+I?+???+++?III?II?++===+++?I?IIZNMD=++++++++++
 *   NNO$77OMMNNZ$Z$$DNMNZIII?+++====+??7?+++++==+?????7???=++++??IIII$NM8+++++++++++
 *   MMMMDD8DMI~~=ZONNNMMOIIII++====+???I77$Z$II?I$7777I?+???+++?IIIII7NMO+++++++++++
 *   NMMMMD8M+~=~===NOMMM87III?++=+???????II7II??II7777II???+????II?I77NMI+++++++++++
 *   MMNNNMNM====++??OMMM8$IIII??????????I?????????IIII77II??+??II??I77NM=+++++++++++
 *   MNNNNMMDN??I??++MMMMN$II?II???????I??++??+++=?IIIIIII???+??I???I7$M8++++++?+++++
 *   MMMMMMMMMNII?+IMMNMMMO7I??I?????IIII77$777I7$7$$ZZZ$87?????I???IIDM++++??+++++++
 *   MMMMMMMMNDMMNOMOOZNMNZ$7????????I787?I=I:+~===++$$7Z$I??+??I??I77N++++++++++++++
 *   MMMMMMMMMMMMMNMM$$$MMMO$7??I??????II$ZZOIII+I+I7$$$$??????II??I$??+++?+++++++???
 *   D8MMMMMNMMMMMMMM$$$7NMMZ$I??I??+??+?I7II???IIIII7$7I?????II?I7$7+?+?+???+???++++
 *   NNNDMMMMMNMMMMMMMMNZ$INMO$7III?I????IIIIII???IIIIIII???IIIII7$77?++?+++++++++?++
 *   MMMNNNNNDDNMMMMMMMDDO8DNNDZ7IIII??????I7I77I7IIIIIIIIII?III7$$+Z++++++++++++++++
 *   MMMND8NNDZDMMMMMMM8ZZDMND88Z$77I?II????????????IIIIIII?I777$$:I$+++++++?++??+?++
 *   MMNNNMMN8ZZMMMMMMMMMNNNDODN8ZZ$$7IIII?????++????IIIIII7$$$77OM$7+++?+++??++??+++
 *   MMMMNDD8ZOO8NMMMMMMN$OO88NNDZZZ$Z$7II????++++++????II7$$$777MMN$?++++?++???????+
 *   MMMMMMM8ZZZZ8DM8NMMMNODMDD8NMO$$$$Z$7III?????????II7$$$777$7MMMN8+??????+?????+?
 *   MMMMNMMDZDDZO8MZNMMMOO$88NMNNDZ$$$$$$ZZ$777$$$77$ZZ$777777$$MMMMM8?+??+?????????
 *   MMMNMN8NMDOZDDN8OMMNDNDNNMMNNN$Z$$$77$$$$$$$$$$$$777777777$ZMMMNNNN????????+????
 *   DMNMNNMNDDMNN888O8NMO8NNNMNNNM$$$7777777777777777I77777777$DMMMMNND$+???????????
 *   88M8MMDMM8ODMNO888DNND8DNNNNMM$$$$777IIIIIIIIIIIIIII7777$7ZMMMMMNNN8$7I?????????
 *   MO8NNDNNDNM88DDNN888ODDDNNNDMMZ77$$77IIIIII?I?III777I7777ZMMMMMMMMND$$7Z7?????I?
 *   DDOODN8NNO8NNO8NNND8D8DDN8OOM8Z777777IIIIIIIIIIIIIII7II78MMMMMMMMMNZ7777777I????
 *   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 *
 */
public class Marquee extends JComponent implements MouseListener, MouseWheelListener {

    private static final float ANIMATION_DELAY = 4500f;

    /** Non-linear accelaration: ease in/ease out with start/end delay. */
    private class SplineInterpolatorWithDelay implements Interpolator {
        float delayStart;
        float delayEnd;
        float splineWidth;
        Interpolator interpolator;

        private SplineInterpolator delegate = new SplineInterpolator(1f, 0f, 0f, 1f);
        private SplineInterpolator delegate2 = new SplineInterpolator(.3f, .6f, .6f, .3f);
        private SplineInterpolator delegate3 = new SplineInterpolator(0f, 0.6f, 0f, 1f);

        void useEaseInEaseOutInterpolator() { interpolator = delegate; }
        void useFastInFastOutInterpolator() { interpolator = delegate2; }
        void useFastInSlowOutInterpolator() { interpolator = delegate3; }

        public float interpolate(float lengthFraction) {
            return (lengthFraction < delayStart) ? 0f :
                   (lengthFraction > delayEnd) ? 1f :
                   interpolator.interpolate((lengthFraction - delayStart ) / splineWidth);
        }
    };
    private SplineInterpolatorWithDelay interpolator = new SplineInterpolatorWithDelay();

    private Color flashingBackgroundStartColr = new Color(0.8f, 0.0f, 0.0f, 0.6f);
    private Color flashingBackgroundEndColr = new Color(0.8f, 0.0f, 0.0f, 0.0f);

    private String broadcastMessage;
    private String[] broadcastMessages;
    private int currentBroadcastMessageIdx;
    private String origBroadcastMessage;
    private String warningMessage;
    private String origWarningMessage;
    private String informationMessage;
    private String stillMessage;
    private String origInformationMessage;

    private BufferedImage backgroundImage;
    private String backgroundImageFile;
    private int backgroundImageW;
    private int backgroundImageH;

    private Color origBackground;

    /** Fill background by a GradientPaint. */
    private GradientPaint gradientPaint;

    private int origW, origH;
    private int warningMessageX;
    private int broadcastMessageX;
    private int informationMessageX;
    private int messageY;

    private Animator backgroundAnimator;
    private Animator wiggleAnimator;
    private Animator marqueeAnimator;
    private boolean wiggleAnimatorIsPaused;
    private boolean marqueeAnimatorIsPaused;
    private FontMetrics fm;
    private String textAlign = "C";

    private Closure actionPerformed;

    public Marquee() {
        addMouseListener(this);
        addMouseWheelListener(this);
    }

    public Closure getActionPerformed() {
        return actionPerformed;
    }

    public void setActionPerformed(Closure actionPerformed) {
        this.actionPerformed = actionPerformed;
    }

    public String getBroadcastMessage() {
        return broadcastMessage;
    }

    private String getCurrentBroadcastMessage() {
        if (broadcastMessages == null)
            return "";
        else {
            try {
                return broadcastMessages[currentBroadcastMessageIdx];
            } catch (Exception e) {
                return "";
            }
        }
    }

    public void setBroadcastMessage(String broadcastMessage) {
        CreamToolkit.logMessage("setBroadcastMessage : " + broadcastMessage);
        if (isMarqueeAnimationRunning())
            stopMarqueeAnimator();

        this.origBroadcastMessage = broadcastMessage;
        this.broadcastMessage = broadcastMessage;

        if (broadcastMessage == null)
            broadcastMessages = null;
        else
            broadcastMessages = broadcastMessage.split("\t");

        currentBroadcastMessageIdx = 0;
        CreamToolkit.logMessage("repaint " + broadcastMessages);
        repaint();
    }

    /** 不做animation的、静止的message string. */
    public String getStillMessage() {
        return stillMessage;
    }

    /** 不做animation的、静止的message string. */
    public void setStillMessage(String stillMessage) {
        this.stillMessage = stillMessage;

        if (isMarqueeAnimationRunning())
                stopMarqueeAnimator();

        if (isWiggleAnimationRunning())
            stopWiggleAnimator();

        repaint();
    }

    public String getInformationMessage() {
        return informationMessage;
    }

    public void setInformationMessage(String informationMessage) {
        this.origInformationMessage = informationMessage;
        this.informationMessage = informationMessage;

        if (isMarqueeAnimationRunning())
            stopMarqueeAnimator();

        if (isWiggleAnimationRunning())
            stopWiggleAnimator();

        repaint();
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.origWarningMessage = warningMessage;
        this.warningMessage = warningMessage; //chineseConverter.convert(warningMessage);

        // Restarting wiggle animation if it's running
        if (isWiggleAnimationRunning())
            stopWiggleAnimator();

        if (StringUtils.isEmpty(warningMessage)) {
            // Stop background animation if setting warningMessage to empty string
            if (backgroundAnimator != null && backgroundAnimator.isRunning())
                backgroundAnimator.stop();
            backgroundAnimator = wiggleAnimator = null;
            if (origBackground != null)
                setBackground(origBackground);
        }
        repaint();
    }

    public void refresh() {
        if (!StringUtils.isEmpty(origBroadcastMessage))
            this.broadcastMessage = origBroadcastMessage;
            //this.broadcastMessage = chineseConverter.convert(origBroadcastMessage);
        if (!StringUtils.isEmpty(origInformationMessage))
            this.informationMessage = origInformationMessage;
            //this.informationMessage = chineseConverter.convert(origInformationMessage);
        if (!StringUtils.isEmpty(origWarningMessage))
            this.warningMessage = origWarningMessage;
            //this.warningMessage = chineseConverter.convert(origWarningMessage);
    }

    private void setupBackgroundAndWiggleAnimation(Graphics g) {
        if (backgroundAnimator == null || !backgroundAnimator.isRunning()) {
            origBackground = getBackground();
            KeyFrames keyFrames = new KeyFrames(
                KeyValues.create(
                    flashingBackgroundStartColr,
                    flashingBackgroundEndColr
                ));
            PropertySetter setter = new PropertySetter(this, "background", keyFrames);
            backgroundAnimator = new Animator(500, Animator.INFINITE, null, setter);
            backgroundAnimator.start();
        }

        //Font font = getFont();
        //FontMetrics fm = g.getFontMetrics(font);
        Rectangle2D bounds = fm.getStringBounds(warningMessage, g);

        // don't animate if contains a linefeed character
        if (warningMessage.contains("^") || getWidth() > bounds.getWidth()) {
            // No need to animate if message width is shorter than displaying window
            if (isWiggleAnimationRunning())
                stopWiggleAnimator();
            if ("C".equals(textAlign))
                warningMessageX = (int)(getWidth() - bounds.getWidth()) / 2; // center-aligned
            else
                warningMessageX = 2; // left-aligned

        } else {
            if (!isSizeChanged(g) && isWiggleAnimationRunning())
                return;

            if (isWiggleAnimationRunning())
                stopWiggleAnimator();

            // Create a wiggle animator
            int dist = (int)bounds.getWidth() - getWidth();
            int duration = Math.max(dist * 6, 500); // 根據滾動的距離決定動畫時間區間
            duration += (int)ANIMATION_DELAY * 2;   // 加上頭尾暫停時間
            interpolator.delayStart = ANIMATION_DELAY / duration; // 計算delay時間佔比
            interpolator.delayEnd = 1f - interpolator.delayStart;
            interpolator.splineWidth = 1f - interpolator.delayStart * 2f;
            interpolator.useEaseInEaseOutInterpolator();
            wiggleAnimator = PropertySetter.createAnimator(duration, this, "warningMessageX",
                0, -dist);
            wiggleAnimator.setRepeatCount(Animator.INFINITE);
            //wiggleAnimator.setStartDelay((int)ANIMATION_DELAY);
            wiggleAnimator.setInterpolator(interpolator);
            wiggleAnimator.start();
        }
    }

    private void setupInformationWiggleAnimation(Graphics g) {
        //Font font = getFont();
        //FontMetrics fm = g.getFontMetrics(font);
        Rectangle2D bounds = fm.getStringBounds(informationMessage, g);

        if (getWidth() > bounds.getWidth()) {
            // No need to animate if message width is shorter than displaying window
            if (isWiggleAnimationRunning())
                stopWiggleAnimator();
            if ("C".equals(textAlign))
                informationMessageX = (int)(getWidth() - bounds.getWidth()) / 2;  // center-aligned
            else
                informationMessageX = 2; // left-aligned

        } else {
            if (!isSizeChanged(g) && isWiggleAnimationRunning())
                return;

            if (isWiggleAnimationRunning())
                stopWiggleAnimator();

            // Create a wiggle animator
            int dist = (int)bounds.getWidth() - getWidth();
            int duration = Math.max(dist * 6, 500); // 根據滾動的距離決定動畫時間區間
            duration += (int)ANIMATION_DELAY * 2;   // 加上頭尾暫停時間
            interpolator.delayStart = ANIMATION_DELAY / duration; // 計算delay時間佔比
            interpolator.delayEnd = 1f - interpolator.delayStart;
            interpolator.splineWidth = 1f - interpolator.delayStart * 2f;
            interpolator.useEaseInEaseOutInterpolator();
            wiggleAnimator = PropertySetter.createAnimator(duration, this, "informationMessageX",
                0, -dist);
            wiggleAnimator.setRepeatCount(Animator.INFINITE);
            //wiggleAnimator.setStartDelay((int)ANIMATION_DELAY);
            wiggleAnimator.setInterpolator(interpolator);
            wiggleAnimator.start();
        }
    }

    private void setupMarqueeAnimation(Graphics g) {

        if (!isSizeChanged(g) && isMarqueeAnimationRunning())
            return;

        if (isMarqueeAnimationRunning())
            stopMarqueeAnimator();

        Rectangle2D bounds = fm.getStringBounds(broadcastMessage, g);

        // Create a marquee animator
        int dist = (int)bounds.getWidth() + getWidth();
        int duration = Math.max(dist * 10, 500);  // 根據滾動的距離決定動畫時間區間
        duration += (int)ANIMATION_DELAY * 2;    // 加上頭尾暫停時間
        interpolator.delayStart = ANIMATION_DELAY / duration; // 計算delay時間佔比
        interpolator.delayEnd = 1f - interpolator.delayStart;
        interpolator.splineWidth = 1f - interpolator.delayStart * 2f;
        interpolator.useFastInFastOutInterpolator();
        marqueeAnimator = PropertySetter.createAnimator(duration, this, "broadcastMessageX",
            getWidth(), (int)-bounds.getWidth());
        marqueeAnimator.setRepeatCount(Animator.INFINITE);
        marqueeAnimator.setRepeatBehavior(Animator.RepeatBehavior.LOOP);
        //marqueeAnimator.setStartDelay((int)ANIMATION_DELAY);
        marqueeAnimator.setInterpolator(interpolator);
        marqueeAnimator.start();
    }

    private void setupMarqueeAnimation2(Graphics g) {
        if (!isSizeChanged(g) && isMarqueeAnimationRunning())
            return;

        if (isMarqueeAnimationRunning())
            stopMarqueeAnimator();

        if (broadcastMessages == null)
            return;

        // Create a marquee animator
        int dist = getWidth();
        int duration = Math.max(dist * 2, 500); // 根據滾動的距離決定動畫時間區間
        duration += (int)ANIMATION_DELAY;       // 加上頭尾暫停時間
        interpolator.delayStart = 0;
        interpolator.delayEnd = ANIMATION_DELAY / duration; // 計算delay時間佔比
        interpolator.splineWidth = 1f - interpolator.delayEnd;
        interpolator.useFastInSlowOutInterpolator();
        marqueeAnimator = PropertySetter.createAnimator(duration, this, "broadcastMessageX",
            getWidth(), 3);
        marqueeAnimator.setRepeatCount(Animator.INFINITE);
        marqueeAnimator.setRepeatBehavior(Animator.RepeatBehavior.LOOP);
        //marqueeAnimator.setStartDelay((int)ANIMATION_DELAY);
        marqueeAnimator.setInterpolator(interpolator);
        marqueeAnimator.addTarget(new TimingTarget() {
            @Override public void timingEvent(float v) {}
            @Override public void begin() {}
            @Override public void end() {}
            @Override public void repeat() {
                // shift to next message when repeat back
                currentBroadcastMessageIdx = (currentBroadcastMessageIdx + 1) % broadcastMessages.length;
            }
        });
        marqueeAnimator.start();
    }

    public int getWarningMessageX() {
        return warningMessageX;
    }

    public void setWarningMessageX(int warningMessageX) {
        this.warningMessageX = warningMessageX;
        repaint();
    }

    public int getInformationMessageX() {
        return informationMessageX;
    }

    public void setInformationMessageX(int informationMessageX) {
        this.informationMessageX = informationMessageX;
        repaint();
    }

    public int getBroadcastMessageX() {
        return broadcastMessageX;
    }

    public String getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(String textAlign) {
        this.textAlign = textAlign;
    }

    public void setBroadcastMessageX(int broadcastMessageX) {
        this.broadcastMessageX = broadcastMessageX;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (fm == null) {
            fm = getGraphics().getFontMetrics(getFont());
            messageY = (getHeight() + fm.getHeight()) / 2 - fm.getDescent();
        }

        ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        // Priority: warningMessage > informationMessage > broadcastMessage
        if (!StringUtils.isEmpty(stillMessage)) {
            showStillMessage(g);
        } else if (!StringUtils.isEmpty(warningMessage)) {
            setupBackgroundAndWiggleAnimation(g);
            showWarningMessage(g);
        } else if (!StringUtils.isEmpty(informationMessage)) {
            setupInformationWiggleAnimation(g);
            showInformationMessage(g);
        } else if (!StringUtils.isEmpty(broadcastMessage)) {
            setupMarqueeAnimation2(g);
            showBroadcastMessage(g);
        } else {
            paintBackground(g);
        }
    }

    public BufferedImage getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(BufferedImage backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getBackgroundImageFile() {
        return backgroundImageFile;
    }

    public void setBackgroundImageFile(String backgroundImageFile) {
        this.backgroundImageFile = backgroundImageFile;
    }

    private void loadImage(int panelW, int panelH) {
        try {
            backgroundImageW = panelW;
            backgroundImageH = panelH;
            backgroundImage = Util.getBufferedImageFromFile(backgroundImageFile.toString(), panelW, panelH, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void paintBackground(Graphics g) {
        int panelW = getWidth();
        int panelH = getHeight();
        if (backgroundImageFile != null &&
            (panelW != backgroundImageW || panelH != backgroundImageH))
            loadImage(panelW, panelH);

        if (backgroundImage != null && StringUtils.isEmpty(warningMessage)) {
            g.drawImage(backgroundImage, 0, 0, this);
        } else {
            if (gradientPaint != null && StringUtils.isEmpty(warningMessage))
                ((Graphics2D)g).setPaint(gradientPaint);
            else
                g.setColor(this.getBackground());

            g.fillRect(0, 0, panelW, panelH);
        }
    }

    private boolean isSizeChanged(Graphics g) {
        if (getWidth() != origW || getHeight() != origH) {
            origW = getWidth();
            origH = getHeight();
            Font font = getFont();
            fm = g.getFontMetrics(font);
            messageY = (getHeight() + fm.getHeight()) / 2 - fm.getDescent();
            return true;
        } else
            return false;
    }

    private void showStillMessage(Graphics g) {
        paintBackground(g);
        Util.drawCenteringString((Graphics2D) g, getForeground(), getFont(), stillMessage,
                0, 0, getWidth(), getHeight(), 0, 3);
    }

    private void showWarningMessage(Graphics g) {
        paintBackground(g);
        if (warningMessage.contains("^")) {
            Util.drawCenteringString((Graphics2D) g, getForeground(), getFont(), warningMessage,
                    0, 0, getWidth(), getHeight(), 0, 3);
        } else {
            g.setColor(this.getForeground());
            g.drawString(warningMessage, warningMessageX, messageY);
        }
    }

    private void showInformationMessage(Graphics g) {
        paintBackground(g);
        g.setColor(this.getForeground());
        g.drawString(informationMessage, informationMessageX, messageY);
    }

    private void showBroadcastMessage(Graphics g) {
        paintBackground(g);
        g.setColor(getForeground());
        g.drawString(getCurrentBroadcastMessage(), broadcastMessageX, messageY);
    }

    private boolean isWiggleAnimationRunning() {
        return wiggleAnimator != null && (wiggleAnimatorIsPaused || wiggleAnimator.isRunning());
    }

    private boolean isMarqueeAnimationRunning() {
        return marqueeAnimator != null && (marqueeAnimator.isRunning() || marqueeAnimatorIsPaused);
    }

    private void stopWiggleAnimator() {
        if (wiggleAnimator != null)
            wiggleAnimator.stop();
        wiggleAnimatorIsPaused = false;
    }

    private void stopMarqueeAnimator() {
        if (marqueeAnimator != null)
            marqueeAnimator.stop();
        marqueeAnimatorIsPaused = false;
    }

    public GradientPaint getGradientPaint() {
        return gradientPaint;
    }

    public void setGradientPaint(GradientPaint gradientPaint) {
        this.gradientPaint = gradientPaint;
    }

    public void mousePressed(MouseEvent e) {
        if (isWiggleAnimationRunning()) {
            wiggleAnimator.pause();
            wiggleAnimatorIsPaused = true;
        }
        if (isMarqueeAnimationRunning()) {
            marqueeAnimator.pause();
            marqueeAnimatorIsPaused = true;
        }
        if (actionPerformed != null)
            actionPerformed.call();
    }

    public void mouseReleased(MouseEvent e) {
        if (isWiggleAnimationRunning()) {
            wiggleAnimator.resume();
            wiggleAnimatorIsPaused = false;
        }
        if (isMarqueeAnimationRunning()) {
            marqueeAnimator.resume();
            marqueeAnimatorIsPaused = false;
        }
    }

    public void mouseExited(MouseEvent e) {
        if (isWiggleAnimationRunning()) {
            wiggleAnimator.resume();
            wiggleAnimatorIsPaused = false;
        }
        if (isMarqueeAnimationRunning()) {
            marqueeAnimator.resume();
            marqueeAnimatorIsPaused = false;
        }
    }

    public void mouseClicked(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        int delta = (e.getWheelRotation() > 0) ? 20 : -20;
        Color background = getBackground();
        int newAlpha = Math.max(Math.min(background.getAlpha() + delta, 255), 0);
        setBackground(new Color(background.getRed(), background.getGreen(), background.getBlue(), newAlpha));
        repaint();
    }
}