package hyi.cream.uibeans.duo2;

import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.event.TransactionEvent;
import hyi.cream.event.TransactionListener;
import hyi.cream.uibeans.duo2.CustDispItemList.ColumnDefine;
import hyi.cream.util.CreamFormatter;
import hyi.cream.util.HYIDouble;

import java.awt.*;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Date;

//import org.apache.log4j.Logger;

public class CustDispTenderList extends Canvas implements TransactionListener
{
    private static final long serialVersionUID = 1L;

//    static Logger logger = Logger.getLogger(CustDispTenderList.class);

    public static final int TITLE = 0;
    public static final int RECORD = 1;

    private Transaction currentTransaction;
    private int[] rowHeight;
    //private Font[] font;
    //private Color[] bgColor;
    //private Color[] fontColor;
    //private int rowCount;
    //private int columnCount;
    private Image backgroundImage;
    //private Point pressedField;
    private int lineItemCount = 2; // two lines

//    private Dimension currentSize = new Dimension(0, 0);
    
    //private Color selectedBGColor;

    // private int eventMask = 0;
    //private String id = "";
    //private Color specialPriceItemFontColor = Color.RED;
    //private Color mmItemFontColor = Color.GRAY;
    //private Color mmTitleItemFontColor = Color.BLUE;
    //private Color fpItemFontColor = Color.GRAY;
    //private Color fpTitleItemFontColor = Color.GREEN;
    //private Color daiShouLinkItemFontColor = new Color(196,47,47);
    //private boolean selectabled = false;
    //private boolean removabled = true;
    //private boolean scrollabled = true;
    //private Dimension currentSize;

//    static public class ColumnDefine
//    {
//        public static final String LEFT = "l";
//        public static final String RIGHT = "r";
//        public static final String CENTER = "c";
//        public String label;
//        public int defWidth;
//        public int width;
//        public String alignment;
//        public String property;
//        public String pattern;
//    }
    private ColumnDefine[] colDef = new ColumnDefine[0];

//    class RowData
//    {
//        String name;
//        BigDecimal qty;
//        BigDecimal salesAmt;
//        public String getName()
//        {
//            return name;
//        }
//        public void setName(String name)
//        {
//            this.name = name;
//        }
//        public BigDecimal getQty()
//        {
//            return qty;
//        }
//        public void setQty(BigDecimal qty)
//        {
//            this.qty = qty;
//        }
//        public BigDecimal getSalesAmt()
//        {
//            return salesAmt;
//        }
//        public void setSalesAmt(BigDecimal salesAmt)
//        {
//            this.salesAmt = salesAmt;
//        }
//    }
//
//    private RowData[] rowData = new RowData[0];

    public CustDispTenderList()
    {
        //setDoubleBuffered(true);
    }
    
//    public CustDispItemList2(String id)
//    {
//        setId(id);
//    }

//    public void setId(String id)
//    {
//        this.id = id;
//        if (!isDesignTime())
//        {
//            init();
//        }
//    }

//    private void init()
//    {
//        configuration();
//        /*
//         * for (int i = 0; i < colDef.length; i++) { System.out.println(colDef[i].label + "," +
//         * colDef[i].stringWidth + "," + colDef[i].alignment + "," + colDef[i].property); }
//         */
//        loadProperties();
//        columnCount = colDef.length;
//        int h = rowHeight[TITLE] + rowCount * rowHeight[RECORD] + (rowCount + 1);
//        int w = columnCount + 1;
//        for (int i = 0; i < columnCount; i++)
//        {
//            w += colDef[i].width;
//        }
//        setSize(new Dimension(w, h));
//
//        // setSize(currentSize);
//        // setBackground(Color.WHITE);
//    }

//    private void configuration()
//    {
//        ArrayList<String> strings = new ArrayList<String>();
//        int lineCnt = 0;
//        // File propFile = CreamToolkit.getConfigurationFile(getClass(), id);
//        // FileInputStream filein = null;
//        InputStream is = getClass().getResourceAsStream("/" + id.toLowerCase() + ".conf");
//        InputStreamReader inst = null;
//        BufferedReader in = null;
//        try
//        {
//            // filein = new FileInputStream(propFile);
//            // if (CreamToolkit.locale.equals(Locale.TRADITIONAL_CHINESE))
//            // inst = new InputStreamReader(is, "BIG5");
//            // else if (CreamToolkit.locale.equals(Locale.SIMPLIFIED_CHINESE))
//            // inst = new InputStreamReader(is, "GBK");
//            // else
//            inst = new InputStreamReader(is, "utf-8");
//            in = new BufferedReader(inst);
//            String line;
//            String prev;
//            String next;
//            ColumnDefine def;
//            char ch = ' ';
//            int m;
//            do
//            {
//                do
//                {
//                    line = in.readLine();
//                    lineCnt++;
//                    // 讀完了.
//                    if (line == null)
//                    {
//                        return;
//                    }
//
//                    if (line.equals(""))
//                        continue;
//                    m = 0;
//                    do
//                    {
//                        ch = line.charAt(m);
//                        m++;
//                    }
//                    while ((ch == ' ' || ch == '\t') && m < line.length());
//                }
//                while (line.equals("") || ch == '#' || ch == ' ' || ch == '\t');
//
//                StringTokenizer t = new StringTokenizer(line, ",", true);
//
//                strings.clear();
//                prev = ",";
//                while (t.hasMoreElements())
//                {
//                    next = t.nextToken().trim();
//                    if (!next.equals(","))
//                    {
//                        strings.add(next);
//                    }
//                    else
//                    {
//                        if (prev.equals(","))
//                        {
//                            strings.add("");
//                        }
//                    }
//                    prev = next;
//                }
//                if (prev.equals(","))
//                {
//                    strings.add("");
//                }
//                if (strings.size() < 5)
//                {
//                    CreamToolkit.logMessage("處理第" + lineCnt + "行資料錯誤");
//                    continue;
//                }
//                def = new ColumnDefine();
//                def.label = $(strings.get(0));
//                def.defWidth = def.width = CreamToolkit.zoomingPixelValue(Integer.parseInt((String)strings.get(1)));
//                def.alignment = strings.get(2);
//                def.property = strings.get(3);
//                def.pattern = strings.get(4);
//                // create new array.
//                ColumnDefine[] newAry = new ColumnDefine[colDef.length + 1];
//                if (colDef.length > 0)
//                {
//                    System.arraycopy(colDef, 0, newAry, 0, colDef.length);
//                }
//                newAry[colDef.length] = def;
//                colDef = newAry;
//            }
//            while (true);
//        }
//        catch (IOException e)
//        {
//            CreamToolkit.logMessage(e.toString());
//            CreamToolkit.logMessage("IO Error at " + this);
//        }
//        finally
//        {
//            try
//            {
//                if (in != null)
//                    in.close();
//            }
//            catch (IOException ie)
//            {
//                CreamToolkit.logMessage("Error", ie);
//            }
//        }
//    }

//    private void loadProperties()
//    {
//        ResourceBundle res = CacaoResourceHome.getBundle(CacaoResourceHome.SYSTEM_SETTING);
//
//        currentSize = (Dimension) res.getObject(id + "_PanelSize");
//        rowCount = ((Integer)res.getObject(id + "_RowCount")).intValue();
//
//        rowHeight = new int[2];
//        font = new Font[2];
//        rowHeight[TITLE] = ((Integer)res.getObject(id + "_TitleHeight")).intValue();
//        rowHeight[RECORD] = ((Integer)res.getObject(id + "_RecordHeight")).intValue();
//        font[TITLE] = (Font)res.getObject(id + "_TitleFont");
//        font[RECORD] = (Font)res.getObject(id + "_RecordFont");
//        bgColor = new Color[2];
//        bgColor[TITLE] = (Color)res.getObject(id + "_TitleBGColor");
//        bgColor[RECORD] = (Color)res.getObject(id + "_RecordBGColor");
//        fontColor = new Color[2];
//        fontColor[TITLE] = (Color)res.getObject(id + "_TileFontColor");
//        fontColor[RECORD] = (Color)res.getObject(id + "_RecordFontColor");
//        selectedBGColor = (Color)res.getObject(id + "_SelectedBGColor");
//        specialPriceItemFontColor = (Color)res.getObject(id + "_SpecialPriceItemFontColor");
//        mmItemFontColor = (Color)res.getObject(id + "_MMItemFontColor");
//        mmTitleItemFontColor = (Color)res.getObject(id + "_MMTitleItemFontColor");
//        fpItemFontColor = (Color)res.getObject(id + "_FPItemFontColor");
//        fpTitleItemFontColor = (Color)res.getObject(id + "_FPTitleItemFontColor");
//
//    }
    
    
//    public Dimension getCurrentSize()
//    {
//        return currentSize;
//    }
//
//    public void setCurrentSize(Dimension currentSize)
//    {
//        this.currentSize = currentSize;
//    }
//    
//    @Override
//    public Dimension getPreferredSize()
//    {
//        return currentSize;
//    }

    public void paint(Graphics g)
    {
        // logger.debug(id + " paint...");
        // logger.debug("print itemList(selectable:" + selectabled + ")");

        //adjustFieldWidth();
        // g.setClip(0, 0, currentSize.width, currentSize.height);
        
        if (backgroundImage == null)
        {
            g.setColor(Color.white);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
        else
        {
            g.drawImage(backgroundImage, 0, 0, this);
        }
        
        //drawTitle(g);
        drawRecords(g);
        //drawBorder(g);
    }

//    /**
//     * Adjust column width and row height
//     */
//    private void adjustFieldWidth()
//    {
//        int tw = 0;
//        for (ColumnDefine cd : colDef)
//        {
//            tw += cd.defWidth;
//        }
//        for (ColumnDefine cd : colDef)
//        {
//            cd.width = cd.defWidth * (int) currentSize.getWidth() / tw;
//        }
//
//        rowHeight[RECORD] = ((int) currentSize.getHeight() - rowHeight[TITLE]) / rowCount;
//    }

//    private void drawTitle(Graphics g)
//    {
//        //g.setColor(bgColor[TITLE]);
//        //g.fillRect(1, 1, (int) currentSize.getWidth() - 1, rowHeight[TITLE]);
//        Rectangle bound = new Rectangle(1, 1, 0, rowHeight[TITLE]);
//        for (int i = 0; i < colDef.length; i++)
//        {
//            bound.width = colDef[i].width;
//            // System.out.println("bound:" + bound);
//            drawField(g, bound, "c", colDef[i].label, TITLE);
//            bound.x += colDef[i].width; // + 1;
//        }
//    }

    private void drawRecords(Graphics g)
    {
        Rectangle bound = new Rectangle(1, rowHeight[TITLE] + 1, 0, rowHeight[RECORD]);
        String value;

        //System.out.println("CustDispTenderList.drawRecords> entering");

        if (currentTransaction == null)
            return;

        //int firstRec = 0;
        //rowCount = rowData.length;
        //if (rowData.length > rowCount)
        //    firstRec = rowData.length - rowCount;

        int i = 0;
        bound.x = 1;
        for (int col = 0; col < colDef.length; col++)
        {
            ColumnDefine field = (ColumnDefine)colDef[col];

            System.out.println("lg:colDef.length:" + colDef.length);
            System.out.println("lg:label:" + field.label);
            System.out.println("lg:width:" + field.width);

            bound.width = field.width;
            value = getFieldValue(field.property, field.pattern);
            drawField(g, bound, field.alignment, field.label, value, RECORD);
//            bound.x = bound.x + field.width;
            bound.x = bound.x + field.width + 15;
            i++;
            
            if (i % 2 == 0)
            {
                bound.x = 1;
                bound.y = bound.y + rowHeight[RECORD];
            }
        }

//        // logger.debug("第一筆資料的index:" + firstRec);
//        for (int row = 0; row < rowCount; row++)
//        {
//            bound.x = 1;
//            for (int col = 0; col < colDef.length; col++)
//            {
//                bound.width = colDef[col].width;
//                value = getFieldValue(colDef[col].property, colDef[col].pattern);
//                drawField(g, bound, colDef[col].alignment, value, RECORD);
//                bound.x = bound.x + colDef[col].width; // + 1;
//            }
//            bound.y = bound.y + rowHeight[RECORD]; // + 1;
//        }
    }

    private String getFieldValue(String property, String pattern)
    {
        Object ret = null;
        try
        {
            CreamFormatter formatter = CreamFormatter.getInstance();
            Method method = currentTransaction.getClass().getDeclaredMethod("get" + property, new Class[0]);
            ret = method.invoke(currentTransaction, new Object[0]);
//            CreamToolkit.logMessage("背屏显示参数：showTenderINfoInSecondScreen = " + currentTransaction.isShowTenderInfoInSecondScreen());
//            if (!currentTransaction.isShowTenderInfoInSecondScreen())
//                return "";
            if (ret == null)
            {
                return "";
            }
            String value;
            if (ret instanceof String)
            {
                value = ret.toString();
            }
            else if (ret instanceof BigDecimal)
            {
                value = formatter.format(((BigDecimal)ret).doubleValue(), pattern);
            }
            else if (ret instanceof Integer || ret instanceof Long || ret instanceof Short)
            {
                value = formatter.format(Long.parseLong(ret.toString()), pattern);
            }
            else if (ret instanceof HYIDouble)
            {
                value = formatter.format(((HYIDouble)ret).doubleValue(), pattern);
            }
            else if (ret instanceof Double || ret instanceof Float)
            {
                value = formatter.format(new Double(ret.toString()).doubleValue(), pattern);
            }
            else if (ret instanceof Date)
            {
                value = formatter.format((Date)ret, pattern);
            }
            else
            {
                throw new Exception("尚未支援的資料型態");
            }
            return value;
        }
        catch (Exception e)
        {
            //logger.error("get value fail", e);
            return "Sample";
        }
    }

    private void drawField(Graphics g, Rectangle bound, /*Color fg, Color bg,*/ String alignment,
        String label, String text, int type)
    {
        System.out.println("CustDispTenderList.drawField> text=" + text + ", bound=" + bound);
        ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

//        label = CacaoResourceHome.retouchString(label);
//        text = CacaoResourceHome.retouchString(text);
//        Color c = g.getColor();
//        if (bg != null)
//        {
//            // 與底色不同才畫.
//            if (!bg.equals(bgColor[type]))
//            {
//                g.setColor(bg);
//                g.fillRect(bound.x, bound.y, bound.width, bound.height);
//            }
//        }

        Insets border = new Insets(0, 0, 0, 8);
        g.setClip(bound.x + border.left, bound.y + border.top, bound.width - border.left
            - border.right, bound.height - border.top - border.bottom);

//        if (fg != null)
//        {
//            g.setColor(fg);
//        }
//        else
//        {
//            g.setColor(c);
//        }
//        g.setFont(font[type]);
        //antiAliasingText((Graphics2D)g);
        FontMetrics fm = g.getFontMetrics();
        int w = bound.width - border.left - border.right;
        int x;
        int y = bound.y + bound.height - border.bottom - fm.getDescent();
        if (alignment.equals(ColumnDefine.LEFT))
        {
            x = bound.x + border.left;
        }
        else if (alignment.equals(ColumnDefine.CENTER))
        {
            if (fm.stringWidth(text) > w)
            {
                x = bound.x + border.left;
            }
            else
            {
                x = bound.x + border.left + (w - fm.stringWidth(text)) / 2;
            }
        }
        else
        {
            x = bound.x + border.left + (w - fm.stringWidth(text));
        }
        System.out.println("CustDispTenderList.drawField> draw string at:" + x + "," + y + " [" + text + "]");
        // color:" + g.getColor());
        g.drawString(label, bound.x + border.left, y);
        g.drawString(text, x, y);
        //g.setClip(0, 0, (int) currentSize.getWidth(), (int) currentSize.getHeight());
        //g.setColor(c);
    }

//    private void drawBorder(Graphics g)
//    {
//        int y = 0;
//        g.setColor(Color.BLACK);
//        g.drawLine(0, y, (int) currentSize.getWidth(), y);
//        y = rowHeight[TITLE];
//        //g.setColor(Color.LIGHT_GRAY);
//        g.setColor(Color.WHITE);
//        g.drawLine(1, rowHeight[TITLE], (int) currentSize.getWidth(), rowHeight[TITLE]);
//        for (int i = 1; i <= rowCount; i++)
//        {
//            if (i == rowCount)
//                y = (int) currentSize.getHeight() - 1;
//            else
//                y = y + (rowHeight[RECORD]); // + 1);
//            g.drawLine(1, y, (int) currentSize.getWidth(), y);
//        }
//
//        int x = 0;
//        for (int i = 0; i < colDef.length; i++)
//        {
//            if (i == 0)
//            {
//                g.setColor(Color.BLACK);
//            }
//            else
//            {
//                //g.setColor(Color.LIGHT_GRAY);
//                g.setColor(Color.WHITE);
//            }
//            g.drawLine(x, 1, x, (int) currentSize.getHeight() - 1);
//            x = x + colDef[i].width; // + 1;
//        }
//        g.drawLine((int) currentSize.getWidth() - 1, 1, (int) currentSize.getWidth() - 1, (int) currentSize.getHeight() - 1);
//    }

//    private Point getLocation(int x, int y)
//    {
//        Rectangle bound = new Rectangle(1, rowHeight[TITLE] + 1, 0, rowHeight[RECORD]);
//        for (int row = 0; row < rowCount; row++)
//        {
//            bound.x = 1;
//            for (int col = 0; col < colDef.length; col++)
//            {
//                bound.width = colDef[col].width;
//
//                if (bound.contains(x, y))
//                {
//                    return new Point(col, row);
//                }
//                bound.x = bound.x + colDef[col].width + 1;
//            }
//            bound.y = bound.y + rowHeight[RECORD]; // + 1;
//        }
//        return null;
//    }

    public void transactionChanged(TransactionEvent e)
    {
//        if (e.getStateChanged() == TransactionEvent.TRANS_INFO_CHANGED)
//        {
//            return;
//        }
//        List<RowData> rowDataList = new ArrayList<RowData>(); 
//        取出transaction中的資料放到RowData.

        currentTransaction = (Transaction) e.getSource();
//        for (LineItem lineItem : tx.getLineItemByPrintType())
//        {
//            if (!isDisplayItem(lineItem) || lineItem.getDisItem().endsWith("2"))
//            {
//                continue;
//            }
//            RowData rowData = new RowData();
//            rowData.name = getItemName(lineItem);
//            rowData.qty = getQty(lineItem);
//            rowData.salesAmt = getSalesAmt(lineItem);
//            rowDataList.add(rowData);
//        }
//        int x = rowDataList.size() - lineItemCount;
//        rowDataList = rowDataList.subList(x < 0 ? 0 : x, rowDataList.size());
//        rowData = rowDataList.toArray(new RowData[rowDataList.size()]);
        repaint();
    }

//    @Override
//    public void setTransaction(Transaction transaction) {
//    }

    private boolean isDisplayItem(LineItem lineItem)
    {
        return true;
    }

    public String getItemName(LineItem lineItem)
    {
        return lineItem.getDescription();
    }

    public BigDecimal getQty(LineItem lineItem)
    {
        try {
            return new BigDecimal(lineItem.getQuantity().toString());
        } catch (Exception e) {
            return new BigDecimal("0");
        }
    }

    public BigDecimal getSalesAmt(LineItem lineItem)
    {
        try {
            return new BigDecimal(lineItem.getAfterDiscountAmount().toString());
        } catch (Exception e) {
            return new BigDecimal("0");
        }
    }

    public void setColDef(ColumnDefine[] colDef)
    {
        this.colDef = colDef;

        //rowCount = lineItemCount;

        //int w = getBounds().width;
        int h = getBounds().height;
        //System.out.println("CustDisp> w=" + w + ", h=" + h);

        rowHeight = new int[2];
        //font = new Font[2];
        rowHeight[TITLE] = 0;
        rowHeight[RECORD] = h / lineItemCount;
        //font[TITLE] = (Font)res.getObject(id + "_TitleFont");
        //font[RECORD] = (Font)res.getObject(id + "_RecordFont");
        //bgColor = new Color[2];
        //bgColor[TITLE] = (Color)res.getObject(id + "_TitleBGColor");
        //bgColor[RECORD] = (Color)res.getObject(id + "_RecordBGColor");
        //fontColor = new Color[2];
        //fontColor[TITLE] = (Color)res.getObject(id + "_TileFontColor");
        //fontColor[RECORD] = (Color)res.getObject(id + "_RecordFontColor");
        //selectedBGColor = (Color)res.getObject(id + "_SelectedBGColor");
        //specialPriceItemFontColor = (Color)res.getObject(id + "_SpecialPriceItemFontColor");
        //mmItemFontColor = (Color)res.getObject(id + "_MMItemFontColor");
        //mmTitleItemFontColor = (Color)res.getObject(id + "_MMTitleItemFontColor");
        //fpItemFontColor = (Color)res.getObject(id + "_FPItemFontColor");
        //fpTitleItemFontColor = (Color)res.getObject(id + "_FPTitleItemFontColor");

        //columnCount = colDef.length;
        //int h = rowHeight[TITLE] + rowCount * rowHeight[RECORD] + (rowCount + 1);
        //int w = columnCount + 1;
        //for (int i = 0; i < columnCount; i++)
        //{
        //    w += colDef[i].width;
        //}
    }

    public void setBackgroundImage(Image backgroundImage)
    {
        this.backgroundImage = backgroundImage;
    }

    public void setLineItemCount(int lineItemCount)
    {
        this.lineItemCount = lineItemCount;
    }

    // private TexturePaint getTextTexture() {
    // Font f = font[RECORD].deriveFont(20f);
    // TextLayout tl = new TextLayout("教育訓練", f, new FontRenderContext(null,
    // false, false));
    // int sw = (int) tl.getBounds().getWidth();
    // int sh = (int) (tl.getAscent()+tl.getDescent());
    // BufferedImage bi = new BufferedImage(sw, sh, BufferedImage.TYPE_INT_RGB);
    // Graphics2D tG2 = bi.createGraphics();
    // tG2.setBackground(WHITE);
    // tG2.clearRect(0,0,sw,sh);
    // tG2.setColor(Color.GRAY);
    // tl.draw(tG2, 0, (float) tl.getAscent());
    // Rectangle r = new Rectangle(0,0,sw,sh);
    // return new TexturePaint(bi,r);
    // }

}

