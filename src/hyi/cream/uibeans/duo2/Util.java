package hyi.cream.uibeans.duo2;

import hyi.cream.util.FastByteArrayOutputStream;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.io.*;
import java.net.URL;
import java.security.MessageDigest;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.*;

/**
 * Utilities.
 *
 * @author Bruce You
 */
public class Util {
    //private static ChineseConverter chineseConverter = ChineseConverter.getInstance();

    private static GraphicsConfiguration gc;
    private static RescaleOp brighterRescaleOp;
    private static LookupOp brighterLookupOp;

    private static Map<String, Thread> imageGetterThreads = Collections.synchronizedMap(new HashMap<String, Thread>());

    static {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        gc = gd.getDefaultConfiguration();

        brighterRescaleOp = new RescaleOp(1.2f, 0.0f, null);

        byte[] brighterOpTable = new byte[256];
        for (int i = 0; i < 256; i++)
            brighterOpTable[i] = (byte) (Math.sqrt((float) i / 255.0) * 255);
        ByteLookupTable brighterLookupTable = new ByteLookupTable(0, brighterOpTable);
        brighterLookupOp = new LookupOp(brighterLookupTable, null);
    }

    /*public static void main(String[] args)
    {
        System.out.println(getMoreSaturatedRandomColor());
    }*/

    public static Color getLightnessColor(Color color) {
        float[] nsb = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
        nsb[2] += 0.25;
        if (nsb[2] > 1) {
            nsb[2] = 1;
        } else if (nsb[2] < 0) {
            nsb[2] = 0;
        }
        Color newColor = Color.getHSBColor(nsb[0], nsb[1], nsb[2]);
        return new Color(newColor.getRed(), newColor.getGreen(), newColor.getBlue(), color.getAlpha());
    }

    public static Color getDarknessColor(Color color) {
        float[] nsb = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
        nsb[2] -= 0.15;
        if (nsb[2] > 1) {
            nsb[2] = 1;
        } else if (nsb[2] < 0) {
            nsb[2] = 0;
        }
        Color newColor = Color.getHSBColor(nsb[0], nsb[1], nsb[2]);
        return new Color(newColor.getRed(), newColor.getGreen(), newColor.getBlue(), color.getAlpha());
    }

    /**
     * Convert "1b0a0b0c" to byte array: [0x1b, 0x0a, 0x0b, 0x0c].
     */
    public static byte[] convertHexValuesToByteArray(String string) {
        byte[] bytes = new byte[string.length() / 2];
        int pos = 0;
        for (int i = 0; i < string.length(); i += 2) {
            int v = Integer.parseInt(string.substring(i, i + 2), 16);
            bytes[pos] = (byte)(v & 0xff);
            pos++;
        }
        return bytes;
    }
    
    public static Color convertColorFromString(String colorString) {
        try {
            int r = Integer.parseInt(colorString.substring(1, 3), 16);
            int g = Integer.parseInt(colorString.substring(3, 5), 16);
            int b = Integer.parseInt(colorString.substring(5, 7), 16);
            int a = 255;
            if (colorString.length() >= 9)
                a = Integer.parseInt(colorString.substring(7, 9), 16);

            return new Color(r, g, b, a);
        } catch (Exception e) {
            return Color.LIGHT_GRAY;
        }
    }

    public static String convertColorToString(Color color) {
        StringWriter sw = new StringWriter();
        new PrintWriter(sw).printf("#%02X%02X%02X%02X", color.getRed(), color.getGreen(), color
            .getBlue(), color.getAlpha());
        return sw.toString();
    }

    public static Color getRandomColor() {
        return new Color(new Random().nextInt(256 - 130) + 130,
            new Random().nextInt(256 - 130) + 130, new Random().nextInt(256 - 130) + 130);
    }

    public static Color getMoreSaturatedRandomColor() {
        java.util.List<Integer> rgbs = new ArrayList<Integer>();
        rgbs.add(new Random().nextInt(256 - 30) + 30);
        rgbs.add(new Random().nextInt(256 - 30) + 30);
        rgbs.add(new Random().nextInt(10));
        Collections.shuffle(rgbs);
        return new Color(rgbs.get(0), rgbs.get(1), rgbs.get(2));
    }

    public static String getRandomColorString() {
        return convertColorToString(getRandomColor());
    }

    public static String getMoreSaturatedRandomColorString() {
        return convertColorToString(getMoreSaturatedRandomColor());
    }

    public static Color getMoreSaturatedRandomTransparentColor(int alpha) {
        java.util.List<Integer> rgbs = new ArrayList<Integer>();
        rgbs.add(new Random().nextInt(256 - 30) + 30);
        rgbs.add(new Random().nextInt(256 - 30) + 30);
        rgbs.add(new Random().nextInt(10));
        Collections.shuffle(rgbs);
        return new Color(rgbs.get(0), rgbs.get(1), rgbs.get(2), alpha);
    }

    public static String getMoreSaturatedRandomTransparentColorString(int alpha) {
        return convertColorToString(getMoreSaturatedRandomTransparentColor(alpha));
    }

    /**
     * Returns a copy of the object, or null if the object cannot be serialized.
     */
    public static Object deepClone(Object orig) {
        Object obj = null;
        try {
            // Write the object out to a byte array
            FastByteArrayOutputStream fbos = new FastByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(fbos);
            out.writeObject(orig);
            out.flush();
            out.close();

            // Retrieve an input stream from the byte array and read
            // a copy of the object back in.
            ObjectInputStream in = new ObjectInputStream(fbos.getInputStream());
            obj = in.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
        return obj;
    }

    public static void waitCursor(Window window) {
        window.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }

    public static void defaultCursor(Window window) {
        window.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    public static void drawString(Graphics2D g, Color color, Font font, String text,
                                  int x, int y, int width, int height, int inset, int lineSpacing,
                                  char lineDelimiter, int textAglinment) {
        if (text == null || text.trim().length() == 0)
            return;

        //text = chineseConverter.convert(text);

        g.setColor(color);
        //如果有lineDelimiter就依照使用者設定換行.
        java.util.List<TextLayout> lines = new ArrayList<TextLayout>();
        int wrappingWidth = width - inset * 2;
        int wrappingHeight = height - inset * 2;
        int h = 0;
        if (lineDelimiter != '\0' && text.indexOf(lineDelimiter) >= 0) {

            StringTokenizer tokenizer = new StringTokenizer(text, lineDelimiter + "");
            while (tokenizer.hasMoreTokens()) {
                String str = tokenizer.nextToken();
                AttributedString as = new AttributedString(str);
                as.addAttribute(TextAttribute.FONT, font);
                AttributedCharacterIterator aci = as.getIterator();

                FontRenderContext frc = g.getFontRenderContext();
                LineBreakMeasurer lbm = new LineBreakMeasurer(aci, frc);
                TextLayout line = lbm.nextLayout(wrappingWidth);
                int lh = (int) line.getBounds().getHeight() + (h == 0 ? 0 : lineSpacing);
                lines.add(line);
                h += lh;
            }
        } else {
            AttributedString as = new AttributedString(text);
            as.addAttribute(TextAttribute.FONT, font);
            AttributedCharacterIterator aci = as.getIterator();

            FontRenderContext frc = g.getFontRenderContext();
            LineBreakMeasurer lbm = new LineBreakMeasurer(aci, frc);

            while (lbm.getPosition() < aci.getEndIndex()) {
                TextLayout line = lbm.nextLayout(wrappingWidth);
                int lh = (int) line.getBounds().getHeight() + (h == 0 ? 0 : lineSpacing);
                if (h + lh > wrappingHeight)
                    break;

                lines.add(line);
                h += lh;
            }
        }
        int y2 = y + (height - h) / 2;
        h = 0;
        for (TextLayout line : lines) {
            h += line.getBounds().getHeight();
            if (h > wrappingHeight)
                break;
            if (textAglinment == -1) {
                //靠左
                line.draw(g, x + inset, y2 + h);
            } else if (textAglinment == 1) {
                //靠右
                line.draw(g, x + width - (int) line.getBounds().getWidth(), y2 + h);
            } else {
                //置中
                line.draw(g, x + (width - (int) line.getBounds().getWidth() - inset) / 2, y2 + h);
            }
            h += lineSpacing;
        }
    }

    public static void drawCenteringString(Graphics2D g, Color color, Font font, String text,
                                           int x, int y, int width, int height, int inset, int lineSpacing) {
        drawAlignedString(g, color, font, text, x, y, width, height, inset, lineSpacing, 'C', false);
    }

    public static void drawCenteringString(Graphics2D g, Color color, Font font, String text,
                                           int x, int y, int width, int height, int inset, int lineSpacing, boolean underlined) {
        drawAlignedString(g, color, font, text, x, y, width, height, inset, lineSpacing, 'C', underlined);
    }

    public static void drawLeftAlignedString(Graphics2D g, Color color, Font font, String text,
                                             int x, int y, int width, int height, int inset, int lineSpacing) {
        drawAlignedString(g, color, font, text, x, y, width, height, inset, lineSpacing, 'L', false);
    }

    public static void drawRightAlignedString(Graphics2D g, Color color, Font font, String text,
                                              int x, int y, int width, int height, int inset, int lineSpacing) {
        drawAlignedString(g, color, font, text, x, y, width, height, inset, lineSpacing, 'R', false);
    }

    private static void drawAlignedString(Graphics2D g, Color color, Font font, String text,
                                          int x, int y, int width, int height, int inset, int lineSpacing, char aligned,
                                          boolean underlined) {
        if (text == null || text.trim().length() == 0)
            return;

        //text = chineseConverter.convert(text);
        //gllg set particular color to some fonts
        if (text.startsWith("找零：")) {
            color = new Color(255, 0, 0);
        }

        if (text.startsWith("请提醒顾客注册集享卡")) {
            color = new Color(255, 0, 0);
        }

        g.setColor(color);
        //如果有'^'就依照使用者設定換行.
        java.util.List<TextLayout> lines = new ArrayList<TextLayout>();
        int wrappingWidth = width - inset * 2;
        int wrappingHeight = height - inset * 2;
        int h = 0;
        if (text.indexOf("^") >= 0) {
            StringTokenizer tokenizer = new StringTokenizer(text, "^");
            while (tokenizer.hasMoreTokens()) {
                String str = tokenizer.nextToken();
                AttributedString as = new AttributedString(str);
                as.addAttribute(TextAttribute.FONT, font);
                AttributedCharacterIterator aci = as.getIterator();

                FontRenderContext frc = g.getFontRenderContext();
                LineBreakMeasurer lbm = new LineBreakMeasurer(aci, frc);
                TextLayout line = lbm.nextLayout(wrappingWidth);
                int lh = (int) line.getBounds().getHeight() + (h == 0 ? 0 : lineSpacing);
                lines.add(line);
                h += lh;
            }
        } else {
            AttributedString as = new AttributedString(text);
            as.addAttribute(TextAttribute.FONT, font);
            AttributedCharacterIterator aci = as.getIterator();

            FontRenderContext frc = g.getFontRenderContext();
            LineBreakMeasurer lbm = new LineBreakMeasurer(aci, frc);

            while (lbm.getPosition() < aci.getEndIndex()) {
                TextLayout line = lbm.nextLayout(wrappingWidth);
                int lh = (int) line.getAscent() + (h == 0 ? 0 : lineSpacing);
                //int lh = (int)line.getBounds().getHeight() + (h == 0 ? 0 : lineSpacing);
                if (h + lh > wrappingHeight)
                    break;

                lines.add(line);
                h += lh;
            }
        }
        int y2 = y + (height - h) / 2;
        h = 0;
        for (TextLayout line : lines) {
            h += line.getAscent();
            //h += line.getBounds().getHeight();
            if (h > wrappingHeight)
                break;
            int gap = 0;
            switch (aligned) {
            case 'L':
                gap = 0;
                break;
            case 'R':
                gap = (width - inset * 2) - (int)line.getBounds().getWidth() - 1;
                break;
            case 'C':
                gap = ((width - inset * 2) - (int)line.getBounds().getWidth() - 1) / 2;
            }
            line.draw(g, x + gap, y2 + h - 1);
            if (underlined) {
                g.setColor(color.darker());
                g.drawLine(x + gap, y2 + h + 2, x + gap + (int) line.getBounds().getWidth(), y2 + h + 2);
            }
            h += lineSpacing;
        }
    }

    static public void setupLookAndFeel() {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
        }
    }

    static public void centeringFrame(Window window) {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        window.setLocation(dim.width / 2 - window.getWidth() / 2, dim.height / 2
            - window.getHeight() / 2);
    }

    static public void centeringDialog(Container parent, JDialog dialog) {
        dialog.setLocation((parent.getWidth() - dialog.getWidth()) / 2 + parent.getX(), (parent
            .getHeight() - dialog.getHeight())
            / 2 + parent.getY());
    }

    static public void adjustJTableColumnWidths(JTable table) {
        // strategy - get max width for cells in column and
        // make that the preferred width
        TableColumnModel columnModel = table.getColumnModel();
        for (int col = 0; col < table.getColumnCount(); col++) {
            int maxwidth = 0;
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer rend = table.getCellRenderer(row, col);
                Object value = table.getValueAt(row, col);
                Component comp = rend.getTableCellRendererComponent(table, value, false, false,
                    row, col);
                maxwidth = Math.max(comp.getPreferredSize().width, maxwidth);
            } // for row

            TableColumn column = columnModel.getColumn(col);
            TableCellRenderer headerRenderer = column.getHeaderRenderer();
            if (headerRenderer == null)
                headerRenderer = table.getTableHeader().getDefaultRenderer();
            Object headerValue = column.getHeaderValue();
            Component headerComp = headerRenderer.getTableCellRendererComponent(table, headerValue,
                false, false, 0, col);
            maxwidth = Math.max(maxwidth, headerComp.getPreferredSize().width);
            column.setPreferredWidth(maxwidth);
        } // for col
    }

    /**
     * Form a BufferedImage from a image file.
     */
    public static BufferedImage getBufferedImageFromFile(File imageFile) {
        try {
            /*FileInputStream fin = new FileInputStream(iconFile);
            ImageInputStream iin = ImageIO.createImageInputStream(fin);
            ImageReader reader = ImageIO.getImageReaders(iin).next();
            reader.setInput(iin, true, true);
            BufferedImage image = reader.read(0);
            iin.close();*/

            BufferedImage image = ImageIO.read(imageFile);
            return image;
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public static BufferedImage getBufferedImageFromFile(final String imageFile, final int width, final int height,
                                                         final Component repaintComponent) throws Exception {
        try {
            makeCacheDirectory();

            File cacheFile = getCachedImageFile(imageFile, width, height);
            if (cacheFile != null) {
                System.out.println("Get image of " + imageFile + " of size " + width + "x" + height +
                        " from cache file: " + cacheFile);
                return toCompatibleImage(ImageIO.read(cacheFile));
            }

            if (imageFile.startsWith("http")) {
                if (imageGetterThreads.get(imageFile) == null) {
                    // Create a thread for downloading image
                    Thread imageGetterThread = new Thread() {
                        public void run() {
                            try {
                                BufferedImage img;
                                File cacheFileWithOriginalSize = getCachedImageFile(imageFile);
                                if (cacheFileWithOriginalSize == null) {
                                    System.out.println("Get image from URL: " + imageFile);
                                    img = toCompatibleImage(ImageIO.read(new URL(imageFile)));
                                    writeCachedImageFile(img, imageFile); // cache original file
                                } else {
                                    System.out.println("Get image of " + imageFile +
                                            " of original size from cache file: " + cacheFileWithOriginalSize);
                                    img = toCompatibleImage(ImageIO.read(cacheFileWithOriginalSize));
                                }
                                img = getScaledInstance(img, width, height);
                                writeCachedImageFile(img, imageFile, width, height); // cache scaled image file

//                                if (repaintComponent instanceof PosButtonWindow)
//                                    ((PosButtonWindow)repaintComponent).clearCachedImages();
                                repaintComponent.repaint();

                                imageGetterThreads.remove(imageFile);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    imageGetterThreads.put(imageFile, imageGetterThread);
                    imageGetterThread.start();
                }
                throw new Exception();

            } else {
                System.out.println("Get image from local file: " + imageFile);
                BufferedImage img = toCompatibleImage(ImageIO.read(new File(imageFile)));
                img = getScaledInstance(img, width, height);
                writeCachedImageFile(img, imageFile, width, height); // cache scaled image file
                return img;
            }
        } catch (Exception e) {
            if (e instanceof Exception)
                throw (Exception)e;
            else
                return null;
        }
    }


    /**
     * Form a BufferedImage from a image file.
     */
    public static BufferedImage getBufferedImageFromFile(String imageFile) {
        try {
            if (imageFile.startsWith("http")) {
                System.out.println("Get image from URL: " + imageFile);
                return ImageIO.read(new URL(imageFile));
            } else
                return ImageIO.read(new File(imageFile));
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public static String getMD5Hash(String value) {
        try {
            final StringBuilder sbMd5Hash = new StringBuilder(32);
            final MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(value.getBytes("UTF-8"));
            final byte data[] = m.digest();
            for (byte element : data) {
                sbMd5Hash.append(Character.forDigit((element >> 4) & 0xf, 16));
                sbMd5Hash.append(Character.forDigit(element & 0xf, 16));
            }
            return sbMd5Hash.toString();
        } catch (Exception e) {
            return null;
        }
    }

    private static BufferedImage toCompatibleImage(BufferedImage image) {
        int w = image.getWidth();
        int h = image.getHeight();
        int transparency = image.getColorModel().getTransparency();
        BufferedImage result = gc.createCompatibleImage(w, h, transparency);
        Graphics2D g2 = result.createGraphics();
        g2.drawRenderedImage(image, null);
        g2.dispose();
        return result;
    }

    private static BufferedImage copy(BufferedImage source, BufferedImage target) {
        Graphics2D g2 = target.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        double scalex = (double) target.getWidth() / source.getWidth();
        double scaley = (double) target.getHeight() / source.getHeight();
        AffineTransform xform = AffineTransform.getScaleInstance(scalex, scaley);
        g2.drawRenderedImage(source, xform);
        g2.dispose();
        return target;
    }

    private static BufferedImage getScaledInstance(BufferedImage image, int width, int height) {
        int transparency = image.getColorModel().getTransparency();
        return copy(image, gc.createCompatibleImage(width, height, transparency));
    }

    private static File getCachedImageFile(String imageFile, int width, int height) {
        String cacheFilename = "cache" + File.separator + getMD5Hash(imageFile + width + "x" + height) + ".png";
        final File cacheFile = new File(cacheFilename);
        return cacheFile.exists() ? cacheFile : null;
    }

    private static File getCachedImageFile(String imageFile) {
        String cacheFilename = "cache" + File.separator + getMD5Hash(imageFile) + ".png";
        final File cacheFile = new File(cacheFilename);
        return cacheFile.exists() ? cacheFile : null;
    }

    private static void writeCachedImageFile(BufferedImage image, String imageFile, int width, int height) {
        try {
            String cacheFilename = "cache" + File.separator + getMD5Hash(imageFile + width + "x" + height) + ".png";
            ImageIO.write(image, "png", new File(cacheFilename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeCachedImageFile(BufferedImage image, String imageFile) {
        try {
            String cacheFilename = "cache" + File.separator + getMD5Hash(imageFile) + ".png";
            ImageIO.write(image, "png", new File(cacheFilename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static File getCachedReflectiveImageFile(String imageFile, int width, int height) {
        String cacheFilename = "cache" + File.separator + getMD5Hash("Rfl_" + imageFile + width + "x" + height) + ".png";
        final File cacheFile = new File(cacheFilename);
        return cacheFile.exists() ? cacheFile : null;
    }

    private static void makeCacheDirectory() {
        File cacheDir = new File("cache");
        if (!cacheDir.exists())
            cacheDir.mkdir();
    }



    public static BufferedImage blur(BufferedImage image) {
        float data[] = { 0.0625f, 0.125f, 0.0625f, 0.125f, 0.25f, 0.125f,
            0.0625f, 0.125f, 0.0625f };
        Kernel kernel = new Kernel(3, 3, data);
        ConvolveOp convolve = new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP, null);
        BufferedImage destImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        convolve.filter(image, destImage);
        return destImage;
    }

    public static BufferedImage increaseImageBrightness(BufferedImage image) {
        try {
            return brighterRescaleOp.filter(image, null);

        } catch (Exception e) {
            // some image with transparent color will fail by above method
            return brighterLookupOp.filter(image, null);
        }
    }

    /**
     * This method returns a buffered image with the contents of an image.
     */
    public static BufferedImage convertImageToBufferedImage(Image image) {
        if (image instanceof BufferedImage)
            return (BufferedImage) image;

        // This code ensures that all the pixels in the image are loaded
        image = new ImageIcon(image).getImage();
        // Determine if the image has transparent pixels; for this method's
        // implementation, see e661 Determining If an Image Has Transparent Pixels
        boolean hasAlpha = false; //hasAlpha(image);
        // Create a buffered image with a format that's compatible with the screen
        BufferedImage bimage = null;
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        try {
            // Determine the type of transparency of the new buffered image
            int transparency = Transparency.OPAQUE;
            if (hasAlpha)
                transparency = Transparency.BITMASK;

            // Create the buffered image
            GraphicsDevice gs = ge.getDefaultScreenDevice();
            GraphicsConfiguration gc = gs.getDefaultConfiguration();
            bimage = gc.createCompatibleImage(
                image.getWidth(null), image.getHeight(null), transparency);
        } catch (HeadlessException e) {
            // The system does not have a screen
        }
        if (bimage == null) {
            // Create a buffered image using the default color model
            int type = BufferedImage.TYPE_INT_RGB;
            if (hasAlpha)
                type = BufferedImage.TYPE_INT_ARGB;

            bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);
        }
        // Copy image to buffered image
        Graphics g = bimage.createGraphics();
        // Paint the image onto the buffered image
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return bimage;
    }
}