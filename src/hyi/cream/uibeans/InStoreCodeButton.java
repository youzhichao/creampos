
// Copyright (c) 2000 HYI
package hyi.cream.uibeans;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class InStoreCodeButton extends POSButton {

    /**
     * Constructor
     */
	public InStoreCodeButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

	public InStoreCodeButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}


