package hyi.cream.uibeans;


/**
 * 客层键.
 */
public class AgeLevelButton extends POSButton {
    private String ageID;

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param ageLevelLabel age level label on button.
     * @param ageID age level ID.
     */
	public AgeLevelButton(int row, int column, int level, String ageLevelLabel, String ageID) {
		super(row, column, level, ageLevelLabel);
        this.ageID = ageID;
	}

	public AgeLevelButton(int row, int column, int level, String ageLevelLabel, int keyCode, String ageID) {
		super(row, column, level, ageLevelLabel, keyCode);
		this.ageID = ageID;
	}

    @Override
    protected String paramString() {
        return ageID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AgeLevelButton that = (AgeLevelButton)o;
        if (ageID != null ? !ageID.equals(that.ageID) : that.ageID != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (ageID != null ? ageID.hashCode() : 0);
        return result;
    }

    public int getAgeID() {
        return Integer.parseInt(ageID);
	}
	
	/*
	public int getKeyCode() {
		return keyCode;
	}*/
}

