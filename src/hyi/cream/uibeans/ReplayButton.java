package hyi.cream.uibeans;

public class ReplayButton extends POSButton {

	/**
	 * Constructor.
	 * 
	 * @param row
	 *            row position.
	 * @param column
	 *            column position.
	 * @param ageLevelLabel
	 *            age level label on button.
	 * @param ageID
	 *            age level ID.
	 */
	public ReplayButton(int row, int column, int level, String diyLabel) {
		super(row, column, level, diyLabel);
	}

	public ReplayButton(int row, int column, int level, String diyLabel,
			int keyCode) {
		super(row, column, level, diyLabel, keyCode);
	}
}
