package hyi.cream.uibeans;

/**
 * 单品折扣率键.
 */
public class DiscountRateButton extends POSButton {

    /**
     * Constructor.
     * 
     * @param row row position.
     * @param column column position.
     * @param clearLabel clear label on button.
     */
    public DiscountRateButton(int row, int column, int level, String clearLabel) {
        super(row, column, level, clearLabel);
    }

    public DiscountRateButton(int row, int column, int level, String clearLabel, int keyCode) {
        super(row, column, level, clearLabel, keyCode);
    }
}