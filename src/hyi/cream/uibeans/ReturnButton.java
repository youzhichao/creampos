package hyi.cream.uibeans;

public class ReturnButton extends POSButton {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param label taxed amount label on button.
     */
	public ReturnButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }
 
    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param label taxed amount label on button.
     * @param keyCode key code
     */
	public ReturnButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}

