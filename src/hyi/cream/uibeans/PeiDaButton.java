package hyi.cream.uibeans;

/**
 * @author ll
 */
public class PeiDaButton extends POSButton {
	private String pluCode     = "";
	//private String keyCode;

	/**
	 * Constructor.
	 *
	 * @param row row position
	 * @param column column position
	 * @param label a string represents the button label
	*/
	public PeiDaButton(int row, int column, int level, String label) {
		super(row, column, level, label);
	}

	/**
	 * Constructor.
	 *
	 * @param row row position
	 * @param column column position
	 * @param numberLabel a string represents the button label
	 * @param keyCode key code
	*/
	public PeiDaButton(int row, int column, int level, String label, int keyCode) {
		super(row, column, level, label, keyCode);
	}

	/**
	 * Constructor.
	 *
	 * @param row row position
	 * @param column column position
	 * @param numberLabel a string represents the button label
	 * @param keyCode key code
	 * @param pluCode PLU number
	*/
	public PeiDaButton(int row, int column, int level, String label, int keyCode, String pluCode) {
		super(row, column, level, label, keyCode);
		this.pluCode = pluCode;
	}
	
	/**
	 * Get default PLU code.
	 *
	 * @return PLU code.
	 */
	public String getPluCode() {
		return pluCode;
	}
}
