package hyi.cream.uibeans;

/**
 * 电商订单键.
 */
public class ECommerceButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
	public ECommerceButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param numberLabel a string represents the button label
     * @param keyCode key code
    */
	public ECommerceButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}

