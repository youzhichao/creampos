package hyi.cream.uibeans;

import hyi.cream.POSTerminalApplication;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.groovydac.Param;
import hyi.cream.state.StateMachine;
import hyi.cream.util.ChineseConverter;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.Keylock;
import hyi.spos.events.StatusUpdateEvent;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class PopupMenuPane extends Window {
    private static final long serialVersionUID = 1L;

    private static POSTerminalApplication app = POSTerminalApplication.getInstance();
    private static ArrayList selectMenu = new ArrayList();
    static PopupMenuPane popupMenuPane = null;

    public static int SINGLE_SELECT = 0;
    public static int MULTIPLE_SELECT = 1;

    private int width;
    private int height;
    private List menuArrayList = new ArrayList();
	private int selectMode;
    private boolean selectState = false;
    private boolean menuPageDown = false;
    private boolean menuPageUp = false;  
    private boolean state = false;
	private int menuIndex = 0;
    private Image offscreen;
	private Graphics og;
    private ActionListener actionListener;
	private int index = 0;
	private	 List<PopupMenuListener> listeners = new ArrayList<PopupMenuListener>();
    private boolean inputEnabled = true;
    private String message = "";
    private String itemNo = "";


    public static PopupMenuPane getInstance() {
        try {
            if (popupMenuPane == null) {
                popupMenuPane = new PopupMenuPane(app.getMainFrame());
            }
        } catch (InstantiationException e) {
        }
        return popupMenuPane;
    }

    public static void close() {
        popupMenuPane.dispose();
        popupMenuPane = null;
    }

    private class FakeKeylock extends Keylock {
        int keyPos;
        FakeKeylock(int keyPos) {
            this.keyPos = keyPos;
        }

        public int getKeyPosition() {
            return keyPos;
        }

        @Override
        public void claim(int timeout) throws JposException {}

        @Override
        public void close() throws JposException {}

        @Override
        public void open(String logicalName) throws JposException {}

        @Override
        public void release() throws JposException {}
    }

    public PopupMenuPane(Frame owner) throws InstantiationException {
        super(owner);
        //!!!!!Bruce
        //this.setTitle("ePOS Menu");
        this.menuArrayList = null;
        
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                // 模拟按了[清除]再按[Keylock 2]
                StateMachine.getInstance().processEvent(new POSButtonEvent(
                    new ClearButton(0, 0, 0, "Clear")));
                StateMachine.getInstance().processEvent(new StatusUpdateEvent(
                    new FakeKeylock(2), 0));
            }
        });
    }

    public void setInputEnabled(boolean inputEnabled) {
        this.inputEnabled = inputEnabled;
    }

    public void clear() {
        itemNo = "";
    }

	public boolean keyDataListener(int prompt) {
        //System.out.println("------------- PopupMenuPane | keyDataListener : " + prompt);
        int pageUpCode = app.getPOSButtonHome().getPageUpCode();
        int pageDownCode = app.getPOSButtonHome().getPageDownCode();
        int clearCode = app.getPOSButtonHome().getClearCode();
        int enterCode = app.getPOSButtonHome().getEnterCode();
        List<String> numberCodeArray = app.getPOSButtonHome().getNumberCode();
        List<POSButton> numberButtonArray = app.getPOSButtonHome().getNumberButton();
        //System.out.println("PopupM>> isSuspended=" + StateMachine.getInstance().isSuspended());
        if (!inputEnabled && prompt != clearCode) {
            return true;
        }

        if (StateMachine.getInstance().getKeyWarning() || StateMachine.getInstance().isSuspended()) {
            return false;
        }

        // check scanner cashier ID card
        if (!app.getChecked() && app.getScanCashierNumber()) {
            return false;
        }
                                                            
        if (message.equals("")) {
            message = POSTerminalApplication.getInstance().getMessageIndicator().getMessage();
        }

        if (prompt == pageUpCode) {
		    menuPageUp = true;
		    repaint();
            return true;
        } else if (prompt == pageDownCode) {
		    menuPageDown = true;
		    repaint();
            return true;
        } else if (numberCodeArray.contains(String.valueOf(prompt))) {
			itemNo += String.valueOf(((NumberButton)numberButtonArray.get(numberCodeArray.indexOf(String.valueOf(prompt)))).getNumberLabel());
            state = false;
            POSTerminalApplication.getInstance().getMessageIndicator().setMessage(itemNo);
		} else if (prompt == clearCode) {
            if (itemNo.equals("")) {
                this.setVisible(false);  
                message = "";
                setIndex(-1);
                for (PopupMenuListener listener : listeners)
                    listener.menuItemSelected();
                listeners.clear();
                return true;
            } else {
			    itemNo = "";
                POSTerminalApplication.getInstance().getMessageIndicator().setMessage(message);
                return true;
            }
		} else if (prompt == enterCode) {
            if (!state) {
                if (itemNo.equals("")) {
                    POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
					return true;
                }
                if (!CreamToolkit.checkInput(itemNo, 0)) {
                    itemNo = "";
                    POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
					return true;
				}
                int menuIndex = Integer.parseInt(itemNo);
                itemNo = "";
                if (menuIndex > menuArrayList.size()) {
                    app.getMessageIndicator().setMessage(message);
                    itemNo = "";
                    POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
					return true;
                }
                setIndex(menuIndex - 1);
                if (selectMode == SINGLE_SELECT) {
                    setVisible(false);
                    for (PopupMenuListener listener : listeners)
                        listener.menuItemSelected();
                    listeners.clear();
                } else if (selectMode == MULTIPLE_SELECT) {
                    if (selectMenu.contains(itemNo)) {
                        selectMenu.remove(menuIndex - 1);
                    } else {
                        selectMenu.add(String.valueOf(menuIndex - 1));
                    }
                    state = true;
                    repaint();
                }
            } else {
                state = false;
                setVisible(false);
                for (PopupMenuListener listener : listeners)
                    listener.menuItemSelected();
                listeners.clear();
                return true;
            }
		}
        //POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
        return true;
	}

	public void centerPopupMenu() {
//		boolean two = GetProperty.getTwoScreen("no").equalsIgnoreCase("yes");
		int high;
		if (Param.getInstance().getMenuHigh() == 13) {
			 high = 400;
		} else {
			 high = 350;
		}
//		if (!two) {
//			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//			width = 350;
//			height = high + getInsets().top + getInsets().bottom;
//			setBounds((screenSize.width - width) / 2, (screenSize.height - height) / 2 + 50, width, height);
//		} else {

        width = 350;
        height = high + getInsets().top + getInsets().bottom;
        String posX = "0"; // GetProperty.getposX("0");
        String posY = "0"; //GetProperty.getposY("0");
        int x = 0, y = 0;
        try {
            x = Integer.parseInt(posX);
            y = Integer.parseInt(posY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Dimension appSize = POSTerminalApplication.getInstance().getMainFrame().getSize();
        //int mainWidth = Integer.parseInt(GetProperty.getCustomerScreenX("800"));
        int mainWidth = (int)appSize.getWidth();
        setBounds((mainWidth - width) / 2 + x, (appSize.height - height) / 2 + 50 + y, width, height);
//		}
	}

	public void setPopupMenuListener(PopupMenuListener listener) {
		listeners.clear();
		listeners.add(listener);
	}

    public void setSelectMode(int selectMode) {
        this.selectMode = selectMode;
    }

    public void setMenu(List menu) {
        menuArrayList = menu;
        finished();
        repaint();
    }

    public void addMenu(Object menu) {
        this.menuArrayList.add(menu);
    }

    public boolean removeMenu(int ind, Object menu) {
        if (ind > menuArrayList.size() - 1) {
            return false;
        }
        menuArrayList.set(ind, menu);
        return true;
    }

    public List getMenu() {
        return menuArrayList;
    }

    public void finished() {
        menuIndex = 0;
        menuPageDown = false;
        menuPageUp = false;
    }

    public Dimension getPreferredSize() {
		return new Dimension(500, 400);
    }

    public Dimension getMinimumSize() {
		return new Dimension(500, 400);
    }

//    public void invalidate() {
//        super.invalidate();
//        offscreen = null;
//    }

    public void update(Graphics g) {
        if (isShowing()) {
            paint(g);
        }
    }

	String fontName = Param.getInstance().getPopupMenuPaneFont();
	//Image image = CreamToolkit.getImage("popupmenu.jpg");
	Polygon nextPageHint = null;
	Polygon previousPageHint = null;
	
	public void paint(Graphics g) {
        if (offscreen == null) {
            offscreen = createImage(width, height);
        }

        og = offscreen.getGraphics();
        
        ((Graphics2D)og).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        //og.setClip(0, 0, getPreferredSize().width, getPreferredSize().height);

	    /*MediaTracker tracker = new MediaTracker(this);
        tracker.addImage(image, 0);
        try {
            tracker.waitForID(0);
        } catch(InterruptedException e) {
			CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Interrupted exception at " + this);
		}*/

        int t = getInsets().top;
        int b = getInsets().bottom;


		//og.drawImage(image, 0, 0, this);
		og.setColor(EPOSBackground.getInstance().getColor4());
		og.fill3DRect(0, 0 + t, getSize().width, getSize().height - t - b, true);
		og.setColor(new Color(255, 255, 200));	//!!!!!Bruce
		og.fillRect(15, 15 + t, getSize().width - 33 , getSize().height - 45 - t - b);
		og.setColor(EPOSBackground.getInstance().getColor4());
		og.draw3DRect(15, 15 + t, getSize().width - 33 , getSize().height - 45 - t - b, true);

        if (menuArrayList == null || menuArrayList.size() == 0) {
            //Bruce/20030318
            g.drawImage(offscreen, 0, 0, this);
            og.dispose();
			return;
		}
		if (previousPageHint == null) {
			int mx = 225;
			int my = getHeight() - (t + b) - 25; //340 - 40;
			int [] xx = {mx - 8, mx, mx + 8};
			int [] yy = {my + 18 + t, my + t, my + 18 + t};
			previousPageHint = new Polygon(xx, yy, 3);
		}
		if (nextPageHint == null) {
			int mx = 255;
			int my = getHeight() - (t + b) - 25 + 18; //358 - 40;
			int [] xx = {mx - 8, mx, mx + 8};
			int [] yy = {my - 18 + t, my + t, my - 18 + t};
		    nextPageHint = new Polygon(xx, yy, 3);
		}

        Font f = new Font(fontName, Font.PLAIN, 20);
        FontMetrics fm = og.getFontMetrics(f);
        og.setFont(f);
		og.setColor(Color.black);
		int length;
		if (Param.getInstance().getMenuHigh() == 13) {
			length = 13;
		} else {
			length = 10;
		}
        if (menuPageDown == true && (menuIndex + length) < menuArrayList.size()) {
            menuIndex = menuIndex + length;
            menuPageDown = false;
        } else {
            menuPageDown = false;
		}

		if (menuPageUp == true && (menuIndex - length) < menuArrayList.size() &&
			(menuIndex - length) >= 0) {
            menuIndex = menuIndex - length;
            menuPageUp = false;
        } else {
            menuPageUp = false;
		}
		
		if (menuArrayList.size() > length) {
			og.setColor(new Color(0, 0, 128));
			int currentPage = 1;
			if (menuIndex > 0) {
				currentPage = (menuIndex + length) / length;
			}
			int allPage = 1;
			if (menuArrayList.size() % length != 0)
			   allPage = menuArrayList.size() / length + 1;
			else {
			   allPage = menuArrayList.size() / length;
			}
			if (currentPage != 1) {
            	og.fillPolygon(previousPageHint);
			}
			if (currentPage != allPage) {
                og.fillPolygon(nextPageHint);
			}
			//og.fillPolygon(nextPage);
			
		}

        int menusum = 0;
        String str;
        String printstr;
        int strlength;
		int stri;
        ChineseConverter chineseConverter = ChineseConverter.getInstance();

		while (menusum < length && menuIndex >= 0 && menuIndex < menuArrayList.size()) {
			boolean m = false;
			if (menuArrayList.get(menuIndex) == null)
				str = "";
			else 
				str = chineseConverter.convert(menuArrayList.get(menuIndex).toString());
			if (selectMenu.contains(str))
			   m = true;
			StringTokenizer tk = new StringTokenizer(str, ",");
			if (tk.countTokens() != 0) {
				str = tk.nextToken();
			}
			strlength = fm.stringWidth(str);
			stri = str.length();
			printstr = str;
			while (strlength > width - 50) {
				printstr = printstr.substring(0, stri);
				strlength = fm.stringWidth(printstr);
				stri--;
			}

			//if (!printstr.equals(""))
			//	drawIt(og, 30, 26 + 32 * menusum - (28 - fm.getAscent()) / 2, m);

			if (m)
				og.setColor(Color.orange);
			else
				og.setColor(Color.black);

			og.drawString(printstr, 32,
						  40 + 30 * menusum - (31 - fm.getAscent()) / 2 + t);

			menusum++;
            menuIndex++;
		}

		menuIndex = menuIndex - menusum;
		g.drawImage(offscreen, 0, 0, this);
        og.dispose();
	}

//!!!!!Bruce
//	private void drawIt(Graphics g, int x, int y, boolean m) {
//		/*Graphics2D g2 = (Graphics2D)g;
//		double w = 190, h = 20, arcw = 4, arch = 4;
//		//RoundRectangle2D.Double(double x, double y, double w, double h, double arcw, double arch)
//		RoundRectangle2D r2D = new RoundRectangle2D.Double(x, y, w, h, arcw, arch);
//		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//		GradientPaint gp = new GradientPaint(new Point2D.Double(x, y - 30), Color.white, new Point2D.Double(x + 150, y + 50), new Color(0, 0, 200), true);
//		if (m)
//			g2.setPaint(Color.orange);
//		else
//		    g2.setPaint(Color.cyan);
//		BasicStroke sk = new BasicStroke(5,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND);
//		g2.setStroke(sk);
//		r2D.setFrame(x, y, w, h);
//		g2.draw(r2D);
//		g2.setPaint(new Color(2, 2, 2));
//		r2D.setFrame(x + 1 , y , w + 1, h + 2);
//		g2.fill(r2D);
//		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);*/
//	}

	public void addActionListener(ActionListener listener) {
		actionListener = AWTEventMulticaster.add(actionListener,listener);
        enableEvents(AWTEvent.MOUSE_EVENT_MASK);
    }

    public void removeActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.remove(actionListener,listener);
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getPluNumber() {
//        StringTokenizer t = new StringTokenizer(menuArrayList.get(index).toString(), ",.");
//        t.nextToken();
//        t.nextToken();
//        return t.nextToken();
      //最后一个逗号后面的部分为pluNumber  2003-08-21 ZhaoH
      //避免品名中含有'.'时取pluNumber出错。  
      String tmp = menuArrayList.get(index).toString();
      return tmp.substring(tmp.lastIndexOf(',') + 1);

	}                    

    public String getPluNumber(int index) {
//        StringTokenizer t = new StringTokenizer(menuArrayList.get(index).toString(), ",.");
//        t.nextToken();
//        t.nextToken();
//        return t.nextToken();
        //最后一个逗号后面的部分为pluNumber  2003-08-21 ZhaoH
        //避免品名中含有'.'时取pluNumber出错。
        String tmp = menuArrayList.get(index).toString();
        return tmp.substring(tmp.lastIndexOf(',') + 1);
	}

	public String getAllDescription() {
		return menuArrayList.get(index).toString();
	}
	
	public ArrayList getSelectMenu() {
        return selectMenu;
	}

	public boolean getSelectedState() {
        return selectState;
    }

    public String getSelectedDescription() {
//        StringTokenizer t = new StringTokenizer(menuArrayList.get(index).toString(), ",.");
//        t.nextToken();
//        return t.nextToken();
        //第1个'.'号和最后一个逗号之间的部分为pluDescription  2003-08-21 ZhaoH
        //避免品名中含有'.'时取pluDescription出错。
        String tmp = menuArrayList.get(index).toString();
        return tmp.substring(tmp.indexOf('.') + 1, tmp.lastIndexOf(','));
    }

    /**
     * 获得第index段文字，以,分段
     * @param index
     * @return
     */
    public String getSelectedPartText(int part) {
    	
        String tmp = menuArrayList.get(index).toString();
        int count = 1;
        int point = 0;
        String ret = null;
        while ((point = tmp.indexOf(",")) >= 0) {
        	tmp = tmp.substring(point + 1, tmp.length());
        	count++;
        	if (count == part)
        		break;
        }
        
        if (count == part || count + 1 == part) {
			if (tmp.indexOf(",") >= 0)
				ret = tmp.substring(0, tmp.indexOf(','));
			else
				ret = tmp;
        }
        return ret;
    }
    
    public int getSelectedNumber() {
        return index;
    }

    public boolean getSelectedMode() {
        if (index == -1) {
            return false;
        } else {
            return true;
        }
    }

    /*public void fireEvent(ActionEvent e) {
        Iterator iter = pl.iterator();
        while (iter.hasNext()) {
            POSButtonListener l = (POSButtonListener)iter.next();
            l.buttonPressed(e);
        }
    }*/

    public void processMouseEvent (MouseEvent e) {
        switch (e.getID()) {
			case MouseEvent.MOUSE_PRESSED:
				repaint();
                break;
            case MouseEvent.MOUSE_RELEASED: 
                if (e.getX() > 235 && e.getX() < 275 &&
						   e.getY() > 10 && e.getY() < 70) {
                    menuPageUp = true;
                    repaint();
                } else if (e.getX() > 230 && e.getX() < 285 &&
						   e.getY() > 90 && e.getY() < 140) {
                    menuPageDown = true;
                    repaint();
				} else if (e.getX() > 235 && e.getX() < 275 &&
						   e.getY() > 290 && e.getY() < 330) {
                    setVisible(false);
                    setIndex(-1);
                    actionListener.actionPerformed(new ActionEvent(this, 0, "close"));
                } else if (e.getX() > 23 && e.getX() < 220) {
                    int i = 1;
                    while (i <= menuArrayList.size() - menuIndex) {
						if (e.getY() > (22 + 32 * (i - 1)) && e.getY() < 38 + 32 * (i - 1)) {
                            setIndex(menuIndex + i - 1);
							if (selectMode == SINGLE_SELECT) {
								setVisible(false);
								break;
							} else if (selectMode == MULTIPLE_SELECT) {
								Object item = menuArrayList.get(menuIndex + i - 1);
								if (selectMenu.contains(item)) {
									selectMenu.remove(item);
								} else {
									selectMenu.add(item);
								}
								repaint();
								break;
							}
						}
                        i++;
                    }
					actionListener.actionPerformed(new ActionEvent(this, 0, "select"));
                }
                break;
            case MouseEvent.MOUSE_ENTERED:
                break;
            case MouseEvent.MOUSE_EXITED:
                break;
        }
        super.processMouseEvent(e);
    }
    
    /**
     * 由于pop window弹出是会出现焦点丢失问题
     * 所以采用鼠标自动点击使当前窗口获得焦点
     */
    public void setVisible(boolean b) {
    	if (b && Param.getInstance().isNeedMouseRobot()) {
			 try {
				 Robot rb = new Robot();
				 //rb.mouseMove(0, 0);
				 rb.mouseMove(EPOSBackground.getInstance().itemListX - 10,
				 	EPOSBackground.getInstance().itemListY - 10);
				 rb.mousePress(InputEvent.BUTTON1_MASK);
			     rb.mouseRelease(InputEvent.BUTTON1_MASK);
			 } catch(AWTException e){
			 }
    	}
        super.setVisible(b);
        if (b)
            repaint();
    }
}


