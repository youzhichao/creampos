package hyi.cream.uibeans;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.awt.*;
import java.text.*;
import hyi.cream.event.*;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.inline.*;

/*
 *  修改说明  /lxf 20030508
 * 	    screenbanner.conf 有多行，第一有效行是定义font的name & size
 * 		之后每3行为一个显示行 
 */	
public class ScreenBanner extends Banner {
	public ScreenBanner() throws ConfigurationNotFoundException {
		super(CreamToolkit.getConfigurationFile(ScreenBanner.class));
	}
	
	public ScreenBanner(Color bgColor, Color valueColor, Color headerColor) 
		throws ConfigurationNotFoundException 
	{
		super(CreamToolkit.getConfigurationFile(ScreenBanner.class)
			, bgColor, valueColor, headerColor);
	}
}
