package hyi.cream.uibeans.touch;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Category;
import hyi.cream.dac.DacBase;
import hyi.cream.dac.PLU;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.groovydac.Param;
import hyi.cream.state.GetProperty;
import hyi.cream.state.StateMachine;
import hyi.cream.state.WarningState;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


//import jpos.Keylock;

public class TouchPane extends Window {
	private static final long serialVersionUID = 3257853177364624946L;

	private static TouchPane panel;

	private Panel categoryPane;

	private Panel detailPane;

	private static POSTerminalApplication app = POSTerminalApplication
			.getInstance();

	private String message = "";

	private ArrayList bufferedObjects = new ArrayList();
	private TouchButtonCard categoryCard;
	private TouchButtonCard detailCard;
	private Panel touchCard;
	private HashMap touchCardCacheMap = new HashMap();
	
	Color BUTTON_BG_COLOR = new Color(107, 133, 254);
	private int level;
	private String type;
	
	public static TouchPane getInstance() {
		if (panel == null)
			panel = new TouchPane(app.getMainFrame());
		return panel;
	}

	private TouchPane(Frame owner) {
		super(owner);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
                // 模拟按了[清除] 
				StateMachine.getInstance().processEvent(
						new POSButtonEvent(new ClearButton(0, 0, 0, "Clear")));
				// 再按[Keylock 2]
//				StateMachine.getInstance().processEvent(
//						new StatusUpdateEvent(new FakeKeylock(2), 0));
			}
		});
		init();
	}

	/**
	 * 
	 */
	private void init() {
		this.setLayout(null);

		java.util.List list = new ArrayList();
		
		if (GetProperty.getTouchMenuVersion().equals("1")) {
			// from cat && plu
			Iterator it = null ;//Category.queryByCategoryTouch();
			Category cg = null;
			int id = 1;
			while (it != null && it.hasNext()) {
				cg = (Category) it.next();
				ScreenButtonProperty sbp = new ScreenButtonProperty();
				sbp.setId(id);
				id++;
				sbp.setCatNo(cg.getCategoryNumber());
				if (cg.getMidCategoryNumber() == null || 
						cg.getMidCategoryNumber().trim().equals(""))
					sbp.setMidCatNo(null);
				else
					sbp.setMidCatNo(cg.getMidCategoryNumber());
				if (cg.getMicroCategoryNumber() == null ||
						cg.getMicroCategoryNumber().trim().equals(""))
					sbp.setMicroCatNo(null);
				else
					sbp.setMicroCatNo(cg.getMicroCategoryNumber());
				sbp.setBgColor(BUTTON_BG_COLOR);
				sbp.setScreenName(cg.getScreenName());
				list.add(sbp);
			}
			categoryCard = new TouchButtonCard(1, list);
			this.add(categoryCard);
			
			detailCard = new TouchButtonCard(new Color(12, 24, 84));
			if (list.size() > 0) {
				ScreenButtonProperty sbp = (ScreenButtonProperty) list.get(0);
				initDetailPane(sbp.getCatNo(), 
						sbp.getMidCatNo(), 
						sbp.getMicroCatNo());
			}
			this.add(detailCard);
		} else if (GetProperty.getTouchMenuVersion().equals("2")) {
			// from touchbutton.conf
			touchCard = new Panel(new CardLayout()); 
			Panel touchA = new TouchButtonCard2();
			Panel touchB = new AlphabetPanel();
			touchCard.add("A", touchA);
			touchCard.add("B", touchB);
			this.add(touchCard);
		}
	}

	public void show(String type) {
        ((CardLayout) (touchCard.getLayout())).show(touchCard, type);
	}
	
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		if (categoryCard != null)
			categoryCard.setBounds(6, 30, 70, this.getHeight() - 30);
		if (touchCard != null)
			touchCard.setBounds(4, 4,  this.getWidth() - 8, this.getHeight() - 8);
		if (detailCard != null)
			detailCard.setBounds(76, 30, this.getWidth() - 76, this.getHeight() - 30);
 	}
	
	String catNo;
	String midCatNo;
	String microCatNo;
	boolean newDetail;
	public void setPara(String catNo, String midCatNo, String microCatNo) {
		newDetail = true;
		this.catNo = catNo;
		this.midCatNo = midCatNo;
		this.microCatNo = microCatNo;
	}
	
	public void initDetailPane(String catNo, String midCatNo, String microCatNo) {
		String key = catNo + midCatNo + microCatNo;
		java.util.List list = new ArrayList();
		if (!touchCardCacheMap.containsKey(key)) {
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getPooledConnection();
    			String sql = "SELECT * FROM "
    				+ PLU.getInsertUpdateTableNameStaticVersion()
    				+ " WHERE CATNO = '" + catNo;
    			if (midCatNo != null)
    				sql += "' AND MIDCATNO = '" + midCatNo;
    			if (microCatNo != null)
    				sql += "' AND MICROCATNO = '" + microCatNo;
    			sql += "'";
                Iterator it = DacBase.getMultipleObjects(connection, PLU.class, sql);
    			PLU plu = null;
    			if (it == null)
    				return;
    			int id = 1;
    			while (it.hasNext()) {
    				plu = (PLU) it.next();
    				ScreenButtonProperty sbp = new ScreenButtonProperty();
    				sbp.setId(id);
    				id++;
    				if (catNo == null || catNo.trim().equals(""))
    					sbp.setMidCatNo(null);
    				else
    					sbp.setMidCatNo(catNo);
    				if (midCatNo == null || midCatNo.trim().equals(""))
    					sbp.setMidCatNo(null);
    				else
    					sbp.setMidCatNo(midCatNo);
    				if (microCatNo == null || microCatNo.trim().equals(""))
    					sbp.setMicroCatNo(null);
    				else
    					sbp.setMicroCatNo(microCatNo);
    				sbp.setBgColor(BUTTON_BG_COLOR);
    				sbp.setScreenName(plu.getScreenName());
    				sbp.setPluNo(plu.getPluNumber());
    				list.add(sbp);
    			}
    			touchCardCacheMap.put(key, list);

            } catch (EntityNotFoundException e) {
                CreamToolkit.logMessage(e);
                return;
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                return;
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
		} else {
			list = (java.util.List) touchCardCacheMap.get(key); 
		}
		detailCard.setButtonList(list);
		detailCard.repaint();
	}

//	private class FakeKeylock extends Keylock {
//		int keyPos;
//
//		FakeKeylock(int keyPos) {
//			this.keyPos = keyPos;
//		}
//
//		public int getKeyPosition() {
//			return keyPos;
//		}
//	}

	
	public boolean keyDataListener(int prompt) {
		int clearCode = app.getPOSButtonHome().getClearCode();
//		ArrayList numberCodeArray = app.getPOSButtonHome().getNumberCode();
//		ArrayList numberButtonArray = app.getPOSButtonHome().getNumberButton();

		// System.out.println("PopupM>> isSuspended=" +
		// StateMachine.getInstance().isSuspended());
		if (prompt != clearCode && POSButtonHome2.getInstance().getTouchPaneAllowButton(prompt) == null) {
			return true;
		}
		if (StateMachine.getInstance().getKeyWarning()
				|| StateMachine.getInstance().isSuspended()) {
			return false;
		}

		// check scanner cashier ID card
		if (!app.getChecked() && app.getScanCashierNumber()) {
			return false;
		}

		if (message.equals("")) {
			message = POSTerminalApplication.getInstance()
					.getMessageIndicator().getMessage();
		}
		if (prompt == clearCode) {
			if (StateMachine.getInstance().getCurrentState().equals(WarningState.class)
					|| type.equalsIgnoreCase("invoice"))
				StateMachine.getInstance().processEvent(
						new POSButtonEvent(new ClearButton(0, 0, 0, "Clear")));
			else
				this.setVisible(false);
			return true;
		} else if (POSButtonHome2.getInstance().getTouchPaneAllowButton(prompt) != null) {
			POSButton pb = POSButtonHome2.getInstance().getTouchPaneAllowButton(prompt);
			if (pb instanceof TouchMenuButton) {
				this.setLevel(((TouchMenuButton) pb).getLevel());
				this.setType2(((TouchMenuButton) pb).getType());
				this.setBackground(((TouchMenuButton) pb).getBackground());
				this.repaint();
				touchCard.repaint();
				this.setVisible(true);
			} else {
				if (!type.equalsIgnoreCase("invoice"))
					this.setVisible(false);
				StateMachine.getInstance().processEvent(
						new POSButtonEvent(pb));
			}
			return true;
		} else {
			// state = false;
			setVisible(false);
			Iterator iter = ((ArrayList) bufferedObjects.clone()).iterator();
			bufferedObjects.clear();
			// Object obj = null;
			while (iter.hasNext()) {
				((PopupMenuListener) iter.next()).menuItemSelected();
			}
			return true;
		}
	}

	public void addToList(Object p) {
		bufferedObjects.clear();
		bufferedObjects.add(p);
	}

	public Panel getCategoryPane() {
		return categoryPane;
	}

	public Panel getDetailPane() {
		return detailPane;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setType2(String type) {
		this.type = type;
	}

	/**
	 * jdk1.8下不能使用getType
	 * @return
	 */
	public String getType2() {
		return type;
	}

    /**
     * 由于pop window弹出是会出现焦点丢失问题
     * 所以采用鼠标自动点击使当前窗口获得焦点
     */
    public void setVisible(boolean b) {
    	if (b && Param.getInstance().isNeedMouseRobot()) {
			 try {
				 Robot rb = new Robot();
				 //rb.mouseMove(0, 0);
				 rb.mouseMove(EPOSBackground.getInstance().itemListX - 10,
				 	EPOSBackground.getInstance().itemListY - 10);
				 rb.mousePress(InputEvent.BUTTON1_MASK);
			     rb.mouseRelease(InputEvent.BUTTON1_MASK);
			 } catch(AWTException e){
			 }
    	}
        super.setVisible(b);
    }

//	public static void main(String[] args) {
//		JFrame frame = new JFrame();
//		TouchPane panel = new TouchPane(frame);
//		frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
//		frame.getContentPane().add(panel);
//		frame.setSize(300, 400);
//		Dimension paneSize = frame.getSize();
//		Dimension screenSize = frame.getToolkit().getScreenSize();
//		frame.setLocation((screenSize.width - paneSize.width) / 2,
//				(screenSize.height - paneSize.height) / 2);
//		frame.setVisible(true);
//		frame.show();
//	}
}