/*
 Starbucks Touch POS Project - CACAO
 Programed by Yuan-Wen Zheng ( Andy )
 Last modified: Dec 10, 2001
 */

package hyi.cream.uibeans.touch;

import hyi.cream.event.POSButtonEvent;
import hyi.cream.state.StateMachine;
import hyi.cream.uibeans.POSButton;
import hyi.cream.uibeans.PluButton;
import hyi.cream.util.CreamProperties;
import hyi.cream.groovydac.Param;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TouchButtonCard extends Panel implements MouseListener {
	HashMap thisButtonMap;

	POSButton pressedButton = null;

	boolean keyPress = false;

	// boolean paintAll = true;
	// Hashtable tab;
	// Point centerIndex;
	FontMetrics fm;

	Image offscreen;

	Image subscreen;

	// String font = CreamProperties.getInstance().getProperty("Standard_Font");

	int BUTTON_HEIGHT = 68;

	int BUTTON_WIDTH = 68;

	int GAP = 4;

	int pressPositionX = 0;

	int pressPositionY = 0;

	int releasePositionX = 0;

	int releasePositionY = 0;

	int level;

	ArrayList pl = new ArrayList();

	Color bgColor = new Color(255, 255, 192);
	
	public TouchButtonCard() {
		this.thisButtonMap = new HashMap();
		this.level = 0;
		addMouseListener(this);
	}

	public TouchButtonCard(Color bgColor) {
		this();
		this.bgColor = bgColor;
	}

	private java.util.List buttonList = new ArrayList(0);

	public TouchButtonCard(int level, java.util.List buttonList) {
		this.thisButtonMap = new HashMap();
		setButtonList(buttonList);
		this.level = level;
		addMouseListener(this);
	}

	public int getLevel() {
		return level;
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void paint(Graphics g) {
		Image buttonImage = null;
		thisButtonMap.clear();
		if (subscreen == null)
			subscreen = createImage(getSize().width, getSize().height);
		// else
		// g.drawImage(subscreen, 0, 0, this);

		Graphics sc = subscreen.getGraphics();
		sc.setColor(bgColor);
		sc.setClip(0, 0, getWidth(), getHeight());
		sc.fillRect(0, 10, getWidth(), getHeight());

		int hCount = this.getHeight() / BUTTON_HEIGHT;
		int wCount = this.getWidth() / BUTTON_WIDTH;
		int count = 0;
		for (int i = 0; i < hCount; i++) {
			for (int j = 0; j < wCount; j++) {
				Point position = new Point(j, i);
				if (count < buttonList.size()) {
					ScreenButtonProperty sbp = (ScreenButtonProperty) buttonList.get(count);
					count++;
					buttonImage = createImage(BUTTON_WIDTH, BUTTON_HEIGHT);
					if (keyPress && pressPositionX == j && pressPositionY == i) {
						sc.drawImage(drawPressedButton(buttonImage, sbp.getBgColor(), 
								sbp.getScreenName()), j* BUTTON_WIDTH, 
								i * BUTTON_HEIGHT, this);
					} else {
						sc.drawImage(drawRaisedButton(buttonImage, sbp.getBgColor(),
								sbp.getScreenName()), j* BUTTON_WIDTH, 
								i * BUTTON_HEIGHT, this);
					}
					thisButtonMap.put(position, sbp);
				}
			}
		}
		g.drawImage(subscreen, 0, 0, this);
		sc.dispose();
	}

	public void mousePressed(MouseEvent e) {
		pressPositionX = (int) (e.getX() / BUTTON_WIDTH);
		pressPositionY = (int) (e.getY() / BUTTON_HEIGHT);
		Point position = new Point(pressPositionX, pressPositionY);
		if (thisButtonMap.containsKey(position)) {
			keyPress = true;
			repaint(BUTTON_WIDTH * pressPositionX, BUTTON_HEIGHT
					* pressPositionY, BUTTON_WIDTH, BUTTON_HEIGHT);
		}
	}

	public void mouseReleased(MouseEvent e) {
		releasePositionX = (int) (e.getX() / BUTTON_WIDTH);
		releasePositionY = (int) (e.getY() / BUTTON_HEIGHT);
		keyPress = false;
		Point position = new Point(pressPositionX, pressPositionY);
		if (thisButtonMap.containsKey(position)) {
			repaint(BUTTON_WIDTH * pressPositionX, BUTTON_HEIGHT
					* pressPositionY, BUTTON_WIDTH, BUTTON_HEIGHT);
			if (releasePositionX == pressPositionX
					&& releasePositionY == pressPositionY) {
				actionPerformed(position);
			}
		}
	}

	ScreenButtonProperty sbp1;
	public void actionPerformed(Point position) {
		getToolkit().getDefaultToolkit().beep();
		sbp1 = (ScreenButtonProperty) thisButtonMap.get(position);
		if (level == 1 && thisButtonMap.containsKey(position)) {
			TouchPane.getInstance().initDetailPane(
					sbp1.getCatNo(), sbp1.getMidCatNo(), sbp1.getMicroCatNo());
		} else if (level == 0) {
			TouchPane.getInstance().setVisible(false);
			StateMachine.getInstance().processEvent(
					new POSButtonEvent(new PluButton(0, 0, 0, "plu", sbp1
							.getPluNo())));
		}
	}
	
	// abstract public void paint(Graphics g);

	private boolean reload;

	public java.util.List getButtonList() {
		return buttonList;
	}

	public void setButtonList(List buttonList) {
		this.buttonList = buttonList;
		reload = true;

	}

	public Image drawRaisedButton(Image offscreen, Color buttonColor,
			String buttonLabel) {
		Graphics g = offscreen.getGraphics();
		g.setClip(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		// g.clearRect(0, 0, BUTTON2_WIDTH, BUTTON2_HEIGHT);

		String fontName = Param.getInstance().getScreenButtonFont();
		Font f = new Font(fontName, Font.BOLD, 14);
		g.setFont(f);
		FontMetrics fm = g.getFontMetrics();
		int centerX = 0;
		int centerY = 0;
		int centerX2 = 0;
		int centerY2 = 0;
		int idx;
		int shadow = 2;
		int border = 1;
		int x = 2;
		int y = 2;
		int width = BUTTON_WIDTH - 4;
		int height = BUTTON_HEIGHT - 4;

		// g.setColor(new Color(255,255,192));
		// g.fillRect(0, 0, BUTTON3_WIDTH, BUTTON3_HEIGHT);
		Color SHADOW_COLOR = Color.gray;
		g.setColor(Color.gray);
		g.fillRect(x + shadow, y + shadow, width, height);

		g.setColor(Color.white);
		g.fillRect(x, y, width, height);

		g.setColor(buttonColor);
		g.fillRect(x + border, y + border, width - 2 * border, height - 2
				* border);
		Color FONT_COLOR = new Color(22, 38, 134);
		g.setColor(FONT_COLOR);

		if (!buttonLabel.equals("")) {
			boolean isNumber = true;
			if (buttonLabel.length() > 2)
				isNumber = false;
			else {
				for (int i = 0; i < buttonLabel.length(); i++) {
					if (buttonLabel.charAt(i) > 256) {
						isNumber = false;
						break;
					}
				}
			}
			if (isNumber) {
				g.setFont(f);
				fm = g.getFontMetrics();
			}

			buttonLabel = processButtonLabel(8, buttonLabel);

			if ((idx = buttonLabel.indexOf('^')) == -1) {
				centerX = (BUTTON_WIDTH - 10 - fm.stringWidth(buttonLabel)) / 2;
				centerY = isNumber ? BUTTON_HEIGHT / 2 + 7 : BUTTON_HEIGHT / 2;
				g.drawString(buttonLabel, centerX, centerY);
			} else {
				centerX = (BUTTON_WIDTH - 10 - fm.stringWidth(buttonLabel
						.substring(0, idx))) / 2 + 4;
				centerY = BUTTON_HEIGHT / 2 - 12;
				g.drawString(buttonLabel.substring(0, idx), centerX, centerY);
				centerX2 = (BUTTON_WIDTH - 10 - fm.stringWidth(buttonLabel
						.substring(idx + 1))) / 2 + 4;
				centerY2 = BUTTON_HEIGHT / 2 + 12;
				g.drawString(buttonLabel.substring(idx + 1), centerX2,
						centerY2);
			}
		}
		return offscreen;
	}

	public Image drawPressedButton(Image offscreen, Color buttonColor,
			String buttonLabel) {
		Graphics g = offscreen.getGraphics();
		g.setClip(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		// g.clearRect(0, 0, BUTTON2_WIDTH, BUTTON2_HEIGHT);

		String fontName = Param.getInstance().getScreenButtonFont();
		Font f = new Font(fontName, Font.BOLD, 14);
		g.setFont(f);
		FontMetrics fm = g.getFontMetrics();
		int centerX = 0;
		int centerY = 0;
		int centerX2 = 0;
		int centerY2 = 0;
		int idx;
		int shadow = 2;
		int border = 1;
		int x = 2;
		int y = 2;
		int width = BUTTON_WIDTH - 4;
		int height = BUTTON_HEIGHT - 4;
		// g.setColor(new Color(255,255,192));
		// g.fillRect(0, 0, BUTTON3_WIDTH, BUTTON3_HEIGHT);
		x += shadow;
		y += shadow;

		g.setColor(Color.white);
		g.fillRect(x, y, width, height);

		g.setColor(buttonColor);
		g.fillRect(x + border, y + border, width - 2 * border, height - 2
				* border);
		
//		g.fillRoundRect(shadow, shadow, BUTTON_WIDTH - 2 * shadow, BUTTON_HEIGHT - 2 * shadow, 4,
//				4);
		Color FONT_COLOR = new Color(22, 38, 134);
		g.setColor(FONT_COLOR);

		boolean isNumber = true;
		if (buttonLabel.length() > 2)
			isNumber = false;
		else {
			for (int i = 0; i < buttonLabel.length(); i++) {
				if (buttonLabel.charAt(i) > 256) {
					isNumber = false;
					break;
				}
			}
		}
		if (isNumber) {
			g.setFont(f);
			fm = g.getFontMetrics();
		}

		buttonLabel = processButtonLabel(8, buttonLabel);
		if ((idx = buttonLabel.indexOf('^')) == -1) {
			centerX = shadow
					+ (BUTTON_WIDTH - 6 - fm.stringWidth(buttonLabel)) / 2;
			centerY = isNumber ? shadow + BUTTON_HEIGHT / 2 + 7 : shadow
					+ BUTTON_HEIGHT / 2;
			g.drawString(buttonLabel, centerX, centerY);
		} else {
			centerX = shadow
					+ (BUTTON_WIDTH - 6 - fm.stringWidth(buttonLabel
							.substring(0, idx))) / 2;
			centerY = shadow + BUTTON_HEIGHT / 2 - 12;
			g.drawString(buttonLabel.substring(0, idx), centerX, centerY);
			centerX2 = shadow
					+ (BUTTON_WIDTH - 6 - fm.stringWidth(buttonLabel
							.substring(idx + 1))) / 2;
			centerY2 = shadow + BUTTON_HEIGHT / 2 + 12;
			g.drawString(buttonLabel.substring(idx + 1), centerX2, centerY2);
		}
		return offscreen;
	}

	private String processButtonLabel(int limit, String label) {
		if (label.indexOf('^') == -1) {
			int len = label.length();
			int cnt = 0;
			for (int i = 0; i < len; i++)
				cnt = ((int) (label.charAt(i)) < 256) ? cnt + 1 : cnt + 2;

			if (cnt > limit) {
				cnt = (int) cnt / 2;
				for (len = 0; cnt > 0; len++) {
					if ((int) (label.charAt(len)) < 256)
						cnt--;
					else
						cnt -= 2;
				}
				label = label.substring(0, len) + '^' + label.substring(len);
			}
		}
		return label;
	}
}
