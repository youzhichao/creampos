package hyi.cream.uibeans.touch;

import java.awt.Color;

public class ScreenButtonProperty {
	int id;
	String catNo;
	String midCatNo;
	String microCatNo;
	String pluNo;
	Color bgColor;
	String screenName;
	public Color getBgColor() {
		return bgColor;
	}
	public void setBgColor(Color bgColor) {
		this.bgColor = bgColor;
	}
	public String getCatNo() {
		return catNo;
	}
	public void setCatNo(String catNo) {
		this.catNo = catNo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMicroCatNo() {
		return microCatNo;
	}
	public void setMicroCatNo(String microCatNo) {
		this.microCatNo = microCatNo;
	}
	public String getMidCatNo() {
		return midCatNo;
	}
	public void setMidCatNo(String midCatNo) {
		this.midCatNo = midCatNo;
	}
	public String getPluNo() {
		return pluNo;
	}
	public void setPluNo(String pluNo) {
		this.pluNo = pluNo;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
}
