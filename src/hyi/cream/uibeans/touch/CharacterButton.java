package hyi.cream.uibeans.touch;

import hyi.cream.uibeans.POSButton;

public class CharacterButton extends POSButton {

    private String numberLabel;

    /**
     * Constructor.
     * @param row row position
     * @param column column position
     * @param numberLabel a string represents the number, such as, "a", "b",
     *          "f", "z", etc.
     */
	public CharacterButton(int row, int column, int level, String numberLabel) {
		super(row, column, level, numberLabel);
        this.numberLabel = numberLabel;
	}

    public CharacterButton(int row, int column, int level, String numberLabel, int keyCode) {
		super(row, column, level, numberLabel, keyCode);
		this.numberLabel = numberLabel;
	}

	public String getNumberLabel() {
		return numberLabel;
	}
}
