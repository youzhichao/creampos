package hyi.cream.uibeans.touch;
import hyi.cream.uibeans.EPOSBackground;
import hyi.cream.uibeans.POSButton;
import hyi.cream.util.CreamProperties;
import hyi.cream.groovydac.Param;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class TouchButton extends Canvas implements MouseListener {
	POSButton pressedButton = null;
	boolean keyPress = false;
	FontMetrics fm;
	Image offscreen;
	Image subscreen;
	// w.h.68 font.14 limit.8
	// w.h.60 font.13 limit.7
	// w.h.50 font.14 limit.6
	int BUTTON_HEIGHT = 60;
	int BUTTON_WIDTH = 60;
	int GAP = 4;
	int level;
//	ArrayList pl = new ArrayList();
	// Color bgColor = new Color(255, 255, 192);
	Color BUTTON_BG_COLOR = new Color(12, 24, 84);

	public TouchButton() {
		this(0);
	}

	public TouchButton(POSButton pb) {
		this();
		this.pb = pb;
	}

	public TouchButton(int level) {
		this.level = level;
		addMouseListener(this);
	}

	public int getLevel() {
		return level;
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void paint(Graphics g) {
		Image buttonImage = null;
		if (subscreen == null)
			subscreen = createImage(getSize().width, getSize().height);
		// else
		// g.drawImage(subscreen, 0, 0, this);

		Graphics sc = subscreen.getGraphics();
		
		sc.setColor(EPOSBackground.getInstance().getColor4());
		BUTTON_WIDTH = getWidth();
		BUTTON_HEIGHT = getHeight();
		sc.setClip(0, 0, getWidth(), getHeight());
//		sc.fillRect(0, 2, getWidth(), getHeight());

		if (keyPress) {
			sc.drawImage(drawPressedButton(subscreen, Color.blue, pb.getLabel()), BUTTON_WIDTH, BUTTON_HEIGHT, this);
		} else {
			sc.drawImage(drawRaisedButton(subscreen, Color.blue, pb.getLabel()), BUTTON_WIDTH, BUTTON_HEIGHT, this);
		}
		g.drawImage(subscreen, 0, 0, this);
		sc.dispose();
	}

	public void mousePressed(MouseEvent e) {
		keyPress = true;
		repaint();
	}

	public void mouseReleased(MouseEvent e) {
		keyPress = false;
		repaint();
		actionPerformed();
	}

	POSButton pb;

	public void actionPerformed() {
		getToolkit().getDefaultToolkit().beep();
//		if (level == 1) {
			// TouchPane.getInstance().initDetailPane(
			// sbp1.getCatNo(), sbp1.getMidCatNo(), sbp1.getMicroCatNo());
//		} else if (level == 0) {
			TouchPane.getInstance().setLevel(pb.getLevel());
			TouchPane.getInstance().setVisible(true);
//			StateMachine.getInstance().processEvent(new POSButtonEvent(pb));
//		}
	}

	// abstract public void paint(Graphics g);

	private boolean reload;

	Font f = new Font(Param.getInstance().getScreenButtonFont(), Font.BOLD, 14);

	public Image drawRaisedButton(Image offscreen, Color buttonColor,
			String buttonLabel) {
		Graphics g = offscreen.getGraphics();
		g.setClip(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
//		 g.clearRect(0, 0, BUTTON2_WIDTH, BUTTON2_HEIGHT);

		g.setFont(f);
		FontMetrics fm = g.getFontMetrics();
		int shadow = 2;
		int border = 1;
		int x = 0;
		int y = 0;
		int width = BUTTON_WIDTH;
		int height = BUTTON_HEIGHT;

		g.setColor(Color.gray);
		g.fillRect(x + shadow, y + shadow, width - shadow, height - shadow);

		g.setColor(Color.white);
		g.fillRect(x, y, width - shadow, height - shadow);
		g.setColor(buttonColor);
//		g.setColor(Color.red);
		g.fillRect(x + border, y + border, width - shadow - 2 * border, height
				- shadow - 2 * border);
		Color FONT_COLOR = new Color(22, 38, 134);
//		g.setColor(FONT_COLOR);
		g.setColor(Color.red);
		drawButtonLabel(g, f, fm, buttonLabel, 0);
		return offscreen;
	}

	public Image drawPressedButton(Image offscreen, Color buttonColor,
			String buttonLabel) {
		Graphics g = offscreen.getGraphics();
		g.setClip(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		// g.clearRect(0, 0, BUTTON2_WIDTH, BUTTON2_HEIGHT);

		g.setFont(f);
		FontMetrics fm = g.getFontMetrics();
		int shadow = 2;
		int border = 1;
		int x = 0;
		int y = 0;
		int width = BUTTON_WIDTH;
		int height = BUTTON_HEIGHT;	
		// g.setColor(new Color(255,255,192));
		// g.fillRect(0, 0, BUTTON3_WIDTH, BUTTON3_HEIGHT);
		x += shadow;
		y += shadow;

		g.setColor(Color.white);
		g.fillRect(x, y, width - shadow, height - shadow);

		g.setColor(buttonColor);
		g.fillRect(x + border, y + border, width - shadow - 2 * border, height
				- shadow - 2 * border);

		// g.fillRoundRect(shadow, shadow, BUTTON_WIDTH - 2 * shadow,
		// BUTTON_HEIGHT - 2 * shadow, 4,
		// 4);
		Color FONT_COLOR = new Color(22, 38, 134);
		g.setColor(FONT_COLOR);
		g.setColor(Color.black);

		drawButtonLabel(g, f, fm, buttonLabel, shadow);
		return offscreen;
	}

	private void drawButtonLabel(Graphics g, Font f, FontMetrics fm,
			String buttonLabel, int shadow) {
		int centerX = shadow;
		int centerY = shadow;
		if (!buttonLabel.equals("")) {
			boolean isNumber = true;
			if (buttonLabel.length() > 2)
				isNumber = false;
			else {
				for (int i = 0; i < buttonLabel.length(); i++) {
					if (buttonLabel.charAt(i) > 256) {
						isNumber = false;
						break;
					}
				}
			}
			if (isNumber) {
				g.setFont(f);
				fm = g.getFontMetrics();
			}
			List labels = processButtonLabel2(6, buttonLabel);
			if (labels.size() == 1) {
				buttonLabel = (String) labels.get(0);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY = isNumber ? BUTTON_HEIGHT / 2 + 7 : BUTTON_HEIGHT / 2;
				g.drawString(buttonLabel, centerX, centerY);
			} else if (labels.size() == 2) {
				int magicH = (BUTTON_HEIGHT - 2 * fm.getHeight()) / 4;
				int subH = BUTTON_HEIGHT - 2 * fm.getHeight() - 4 * magicH;
				buttonLabel = (String) labels.get(0);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY = subH + 2 * magicH + fm.getHeight() / 2;
				g.drawString(buttonLabel, centerX, centerY);
				buttonLabel = (String) labels.get(1);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY += magicH + fm.getHeight();
				g.drawString(buttonLabel, centerX, centerY);
			} else {
				int magicH = (BUTTON_HEIGHT - 3 * fm.getHeight()) / 5;
				int subH = BUTTON_HEIGHT - 3 * fm.getHeight() - 5 * magicH;
				buttonLabel = (String) labels.get(0);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY = subH + 2 * magicH + fm.getHeight() / 2;
				g.drawString(buttonLabel, centerX, centerY);
				buttonLabel = (String) labels.get(1);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY += magicH + fm.getHeight();
				g.drawString(buttonLabel, centerX, centerY);
				buttonLabel = (String) labels.get(2);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY += magicH + fm.getHeight();
				g.drawString(buttonLabel, centerX, centerY);
			}
		}
	}

	private String processButtonLabel(int limit, String label) {
		if (label.indexOf('^') == -1) {
			int len = label.length();
			int cnt = 0;
			for (int i = 0; i < len; i++)
				cnt = ((int) (label.charAt(i)) < 256) ? cnt + 1 : cnt + 2;
			if (cnt > limit) {
				cnt = (int) cnt / 2;
				for (len = 0; cnt > 0; len++) {
					if ((int) (label.charAt(len)) < 256)
						cnt--;
					else
						cnt -= 2;
				}
				label = label.substring(0, len) + '^' + label.substring(len);
			}
		}
		return label;
	}

	private List processButtonLabel2(int limit, String label) {
		List retList = new ArrayList(0);
		StringTokenizer t = new StringTokenizer(label, "^", true);
		int startIndex = 0;
		if (label.indexOf('^') == -1) {
			int len = label.length();
			int curLimit = limit;
			int i = 0;
			for (i = 0; i < len; i++) {
				curLimit = ((int) (label.charAt(i)) < 256) ? curLimit - 1
						: curLimit - 2;
				if (curLimit < 2 || i == len - 1) {
					retList.add(label.substring(startIndex, i + 1));
					startIndex = i + 1;
					curLimit = limit;
					;
				}
			}
		} else
			while (t.hasMoreTokens()) {

			}
		return retList;
	}

	public static void main(String[] args) {
		/*
		TouchButtonCard2 card2 = new TouchButtonCard2();
		System.out.println(card2.processButtonLabel2(8, "1一士大夫地方就可就可拮抗剂回家"));
		System.out.println(card2.processButtonLabel2(8, "1一士"));
		System.out.println(card2.processButtonLabel2(8, "智力魔方"));
		System.out.println(card2.processButtonLabel2(8, "清除"));
		System.out.println(card2.processButtonLabel2(8, "一"));
		System.out.println(card2.processButtonLabel2(8, "一二"));
		System.out.println(card2.processButtonLabel2(8, "一二三"));
		System.out.println(card2.processButtonLabel2(8, "一二三四"));
		System.out.println(card2.processButtonLabel2(8, "一二三四五"));
		System.out.println(card2.processButtonLabel2(8, "一二三四五六"));
		System.out.println(card2.processButtonLabel2(8, "一二三四五六七"));
		System.out.println(card2.processButtonLabel2(8, "一二三四五六七八"));
		System.out.println(card2.processButtonLabel2(8, "一二三四五六七八九"));
		System.out.println(card2.processButtonLabel2(8, "一二三四五六七八九十"));
		*/
	}
}
