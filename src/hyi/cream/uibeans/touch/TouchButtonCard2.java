/*
 Starbucks Touch POS Project - CACAO
 Programed by Yuan-Wen Zheng ( Andy )
 Last modified: Dec 10, 2001
 */
package hyi.cream.uibeans.touch;

import hyi.cream.POSTerminalApplication;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.state.GetProperty;
import hyi.cream.state.StateMachine;
import hyi.cream.uibeans.EmptyButton;
import hyi.cream.uibeans.POSButton;
import hyi.cream.uibeans.PluMenuButton;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;


public class TouchButtonCard2 extends Panel implements MouseListener {

    private static final long serialVersionUID = 1L;

    private static POSTerminalApplication app = POSTerminalApplication
	.getInstance();
	HashMap thisButtonMap;
	POSButton pressedButton = null;
	boolean keyPress = false;
	FontMetrics fm;
	Image offscreen;
	Image subscreen;
	// w.h.68 font.14 limit.8
	// w.h.60 font.13 limit.7
	// w.h.50 font.14 limit.6
	int BUTTON_HEIGHT = 60;
	int BUTTON_WIDTH = 60;
	int GAP = 4;
	int pressPositionX = 0;
	int pressPositionY = 0;
	int releasePositionX = 0;
	int releasePositionY = 0;
	int xCount = 12;
	int yCount = 5;		
	int level;
	ArrayList pl = new ArrayList();
	// Color bgColor = new Color(255, 255, 192);
	Color PANE_BG_COLOR = new Color(225,225,128);

	private java.util.List buttonList = new ArrayList(0);

	Font touchButtonFont;
	
	public TouchButtonCard2() {
		this(0);
	}

	public TouchButtonCard2(int level) {
		this.thisButtonMap = new HashMap();
		this.level = level;
		addMouseListener(this);
        xCount = 12;
        yCount = 5;
        int fontSize = 13;
        touchButtonFont = new Font("Dialog", Font.BOLD, fontSize);
//		try {
//			xCount = Integer.parseInt(GetProperty.getTouchButtonXCount("12"));
//			yCount = Integer.parseInt(GetProperty.getTouchButtonYCount("5"));
//		} catch (Exception e) {}
//		try {
//			int fontSize = Integer.parseInt(GetProperty.getTouchButtonFontSize("13"));
//			touchButtonFont = new Font(GetProperty.getTouchButtonFont("STZhongSong"), Font.BOLD, fontSize);
//		} catch (Exception e) {}
	}

	public int getLevel() {
		return level;
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void paint(Graphics g) {
		PANE_BG_COLOR = TouchPane.getInstance().getBackground();

		setButtonList(app.getPOSButtonHome().getTouchButton(TouchPane.getInstance().getType2()));

		Image buttonImage = null;
		thisButtonMap.clear();	
		if (subscreen == null)
			subscreen = createImage(getSize().width, getSize().height);
		// else
		// g.drawImage(subscreen, 0, 0, this);

		Graphics sc = subscreen.getGraphics();
		sc.setColor(PANE_BG_COLOR);
		sc.setClip(0, 0, getWidth(), getHeight());
		sc.fillRect(0, 10, getWidth(), getHeight());

		BUTTON_HEIGHT = this.getHeight() / yCount;
		BUTTON_WIDTH = this.getWidth() / xCount;
		int count = 0;
		for (int i = 0; i < yCount; i++) {
			for (int j = 0; j < xCount; j++) {
				Point position = new Point(j, i);
				buttonImage = createImage(BUTTON_WIDTH, BUTTON_HEIGHT);
				if (count < buttonList.size()) {
					POSButton pb = (POSButton) buttonList.get(count);
					count++;
					if (pb instanceof EmptyButton) {
						sc.drawImage(drawEmptyButton(buttonImage, pb.getBackground(), ""), j
								* BUTTON_WIDTH, i * BUTTON_HEIGHT, this);
					} else if (keyPress && pressPositionX == j && pressPositionY == i) {
						sc.drawImage(drawPressedButton(buttonImage, pb
								.getBackground(), pb.getLabel()), j
								* BUTTON_WIDTH, i * BUTTON_HEIGHT, this);
						thisButtonMap.put(position, pb);
					} else {
						sc.drawImage(drawRaisedButton(buttonImage, pb
								.getBackground(), pb.getLabel()), j
								* BUTTON_WIDTH, i * BUTTON_HEIGHT, this);
						thisButtonMap.put(position, pb);
					}
				} else {
					// new Color(255,255,128)
					sc.drawImage(drawEmptyButton(buttonImage, Color.white, ""), j
							* BUTTON_WIDTH, i * BUTTON_HEIGHT, this);
				}
			}
		}
		g.drawImage(subscreen, 0, 0, this);
		sc.dispose();
	}

	public void mousePressed(MouseEvent e) {
		pressPositionX = (int) (e.getX() / BUTTON_WIDTH);
		pressPositionY = (int) (e.getY() / BUTTON_HEIGHT);
		Point position = new Point(pressPositionX, pressPositionY);
		if (thisButtonMap.containsKey(position)) {
			keyPress = true;
			repaint(BUTTON_WIDTH * pressPositionX, BUTTON_HEIGHT
					* pressPositionY, BUTTON_WIDTH, BUTTON_HEIGHT);
		}
	}

	public void mouseReleased(MouseEvent e) {
		releasePositionX = (int) (e.getX() / BUTTON_WIDTH);
		releasePositionY = (int) (e.getY() / BUTTON_HEIGHT);
		keyPress = false;
		Point position = new Point(pressPositionX, pressPositionY);
		if (thisButtonMap.containsKey(position)) {
			repaint(BUTTON_WIDTH * pressPositionX, BUTTON_HEIGHT
					* pressPositionY, BUTTON_WIDTH, BUTTON_HEIGHT);
			if (releasePositionX == pressPositionX
					&& releasePositionY == pressPositionY) {
				actionPerformed(position);
			}
		}
	}

	POSButton pb1;
	public void actionPerformed(Point position) {
		Toolkit.getDefaultToolkit().beep();
		pb1 = (POSButton) thisButtonMap.get(position);
		if (level == 1 && thisButtonMap.containsKey(position)) {
			// TouchPane.getInstance().initDetailPane(
			// sbp1.getCatNo(), sbp1.getMidCatNo(), sbp1.getMicroCatNo());
		} else if (level == 0) {
//			TouchPane.getInstance().setVisible(false);
			if (pb1 instanceof PluMenuButton)
				((PluMenuButton) pb1).showMenuList();
			else
				StateMachine.getInstance().processEvent(new POSButtonEvent(pb1));
		}
	}

	// abstract public void paint(Graphics g);

	// private boolean reload;

	public java.util.List getButtonList() {
		return buttonList;
	}

	public void setButtonList(List buttonList) {
		if (buttonList == null)
			buttonList = new ArrayList();
		this.buttonList = buttonList;
		// reload = true;
	}

	public Image drawRaisedButton(Image offscreen, Color buttonColor,
			String buttonLabel) {
		Graphics g = offscreen.getGraphics();
		g.setClip(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		// g.clearRect(0, 0, BUTTON2_WIDTH, BUTTON2_HEIGHT);

		g.setFont(touchButtonFont);
		FontMetrics fm = g.getFontMetrics();
		int shadow = 2;
		int border = 1;
		int x = 3;
		int y = 3;
		int width = BUTTON_WIDTH - 3;
		int height = BUTTON_HEIGHT - 3;

		// g.setColor(new Color(255,255,192));
		// g.fillRect(0, 0, BUTTON3_WIDTH, BUTTON3_HEIGHT);
		g.setColor(Color.gray);
		g.fillRect(x + shadow, y + shadow, width - shadow, height - shadow);

		g.setColor(Color.white);
		g.fillRect(x, y, width - shadow, height - shadow);

		g.setColor(buttonColor);
		g.fillRect(x + border, y + border, width - shadow - 2 * border, height
				- shadow - 2 * border);
		Color FONT_COLOR = new Color(22, 38, 134);
		g.setColor(FONT_COLOR);
		drawButtonLabel(g, touchButtonFont, fm, buttonLabel, 0);
		return offscreen;
	}

	public Image drawEmptyButton(Image offscreen, Color buttonColor,
			String buttonLabel) {
		Graphics g = offscreen.getGraphics();
		g.setClip(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);

		g.setFont(touchButtonFont);
		// FontMetrics fm = g.getFontMetrics();
		int shadow = 2;
		// int border = 1;
		int x = 3;
		int y = 3;
		int width = BUTTON_WIDTH - 3;
		int height = BUTTON_HEIGHT - 3;

		g.setColor(buttonColor);
		g.fillRect(x, y, width - shadow, height - shadow);
		return offscreen;
	}

	public Image drawPressedButton(Image offscreen, Color buttonColor,
			String buttonLabel) {
		Graphics g = offscreen.getGraphics();
		g.setClip(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		// g.clearRect(0, 0, BUTTON2_WIDTH, BUTTON2_HEIGHT);

		g.setFont(touchButtonFont);
		FontMetrics fm = g.getFontMetrics();
		int shadow = 2;
		int border = 1;
		int x = 3;
		int y = 3;
		int width = BUTTON_WIDTH - 3;
		int height = BUTTON_HEIGHT - 3;
		// g.setColor(new Color(255,255,192));
		// g.fillRect(0, 0, BUTTON3_WIDTH, BUTTON3_HEIGHT);
		x += shadow;
		y += shadow;

		g.setColor(Color.white);
		g.fillRect(x, y, width - shadow, height - shadow);

		g.setColor(buttonColor);
		g.fillRect(x + border, y + border, width - shadow - 2 * border, height
				- shadow - 2 * border);

		// g.fillRoundRect(shadow, shadow, BUTTON_WIDTH - 2 * shadow,
		// BUTTON_HEIGHT - 2 * shadow, 4,
		// 4);
		Color FONT_COLOR = new Color(22, 38, 134);
		g.setColor(FONT_COLOR);

		drawButtonLabel(g, touchButtonFont, fm, buttonLabel, shadow);
		return offscreen;
	}

	private void drawButtonLabel(Graphics g, Font f, FontMetrics fm,
			String buttonLabel, int shadow) {
		int centerX = shadow;
		int centerY = shadow;
		if (!buttonLabel.equals("")) {
			boolean isNumber = true;
			if (buttonLabel.length() > 2)
				isNumber = false;
			else {
				for (int i = 0; i < buttonLabel.length(); i++) {
					if (buttonLabel.charAt(i) > 256) {
						isNumber = false;
						break;
					}
				}
			}
			if (isNumber) {
				g.setFont(f);
				fm = g.getFontMetrics();
			}
			List labels = processButtonLabel2(6, buttonLabel);
			if (labels.size() == 1) {
				buttonLabel = (String) labels.get(0);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY = isNumber ? BUTTON_HEIGHT / 2 + 7 : BUTTON_HEIGHT / 2;
				g.drawString(buttonLabel, centerX, centerY);
			} else if (labels.size() == 2) {
				int magicH = (BUTTON_HEIGHT - 2 * fm.getHeight()) / 4;
				int subH = BUTTON_HEIGHT - 2 * fm.getHeight() - 4 * magicH;
				buttonLabel = (String) labels.get(0);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY = subH + 2 * magicH + fm.getHeight() / 2;
				g.drawString(buttonLabel, centerX, centerY);
				buttonLabel = (String) labels.get(1);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY += magicH + fm.getHeight();
				g.drawString(buttonLabel, centerX, centerY);
			} else {
				int magicH = (BUTTON_HEIGHT - 3 * fm.getHeight()) / 5;
				int subH = BUTTON_HEIGHT - 3 * fm.getHeight() - 5 * magicH;
				buttonLabel = (String) labels.get(0);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY = subH + 2 * magicH + fm.getHeight() / 2;
				g.drawString(buttonLabel, centerX, centerY);
				buttonLabel = (String) labels.get(1);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY += magicH + fm.getHeight();
				g.drawString(buttonLabel, centerX, centerY);
				buttonLabel = (String) labels.get(2);
				centerX = (BUTTON_WIDTH - fm.stringWidth(buttonLabel)) / 2;
				centerY += magicH + fm.getHeight();
				g.drawString(buttonLabel, centerX, centerY);
			}
		}
	}

//	private String processButtonLabel(int limit, String label) {
//		if (label.indexOf('^') == -1) {
//			int len = label.length();
//			int cnt = 0;
//			for (int i = 0; i < len; i++)
//				cnt = ((int) (label.charAt(i)) < 256) ? cnt + 1 : cnt + 2;
//			if (cnt > limit) {
//				cnt = (int) cnt / 2;
//				for (len = 0; cnt > 0; len++) {
//					if ((int) (label.charAt(len)) < 256)
//						cnt--;
//					else
//						cnt -= 2;
//				}
//				label = label.substring(0, len) + '^' + label.substring(len);
//			}
//		}
//		return label;
//	}

	private List processButtonLabel2(int limit, String label) {
		List retList = new ArrayList(0);
		StringTokenizer t = new StringTokenizer(label, "^", true);
		int startIndex = 0;
		if (label.indexOf('^') == -1) {
			int len = label.length();
			// int cnt = 0;
			int curLimit = limit;
			int i = 0;
			for (i = 0; i < len; i++) {
				curLimit = ((int) (label.charAt(i)) < 256) ? curLimit - 1
						: curLimit - 2;
				if (curLimit < 2 || i == len - 1) {
					retList.add(label.substring(startIndex, i + 1));
					startIndex = i + 1;
					curLimit = limit;
					;
				}
			}
		} else
			while (t.hasMoreTokens()) {

			}
		return retList;
	}

//	public static void main(String[] args) {
//		TouchButtonCard2 card2 = new TouchButtonCard2();
//		System.out.println(card2.processButtonLabel2(8, "1一士大夫地方就可就可拮抗剂回家"));
//		System.out.println(card2.processButtonLabel2(8, "1一士"));
//		System.out.println(card2.processButtonLabel2(8, "智力魔方"));
//		System.out.println(card2.processButtonLabel2(8, "清除"));
//		System.out.println(card2.processButtonLabel2(8, "一"));
//		System.out.println(card2.processButtonLabel2(8, "一二"));
//		System.out.println(card2.processButtonLabel2(8, "一二三"));
//		System.out.println(card2.processButtonLabel2(8, "一二三四"));
//		System.out.println(card2.processButtonLabel2(8, "一二三四五"));
//		System.out.println(card2.processButtonLabel2(8, "一二三四五六"));
//		System.out.println(card2.processButtonLabel2(8, "一二三四五六七"));
//		System.out.println(card2.processButtonLabel2(8, "一二三四五六七八"));
//		System.out.println(card2.processButtonLabel2(8, "一二三四五六七八九"));
//		System.out.println(card2.processButtonLabel2(8, "一二三四五六七八九十"));
//
//	}
}
