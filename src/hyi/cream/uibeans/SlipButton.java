
package hyi.cream.uibeans;

/**
 * 认证键.
 */
public class SlipButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param slipLabel slip label on button.
     */
	public SlipButton(int row, int column, int level, String slipLabel) {
		super(row, column, level, slipLabel);
	}

	public SlipButton(int row, int column, int level, String slipLabel, int keyCode) {
		super(row, column, level, slipLabel, keyCode);
	}
}

