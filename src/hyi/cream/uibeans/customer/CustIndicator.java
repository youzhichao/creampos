package hyi.cream.uibeans.customer;

import java.awt.*;
import java.awt.event.*;

import hyi.cream.state.GetProperty;
import hyi.cream.util.*;
import hyi.cream.*;
import hyi.cream.groovydac.Param;

public class CustIndicator extends Canvas {

	private int paintedStarty;
	private int paintedStartx;
	private String paintedMessage = "~+~%";
	private int offScreenH;
	private int offScreenW;
	private int fontHeight;
	public static final int SCROLL_MARQUEE = 0;
	public static final int SCROLL_WIGGLE = 1;
	public static final int SCROLL_MARQUEE2 = 2;

	private String message = "";
	private int printwidths = 0;
	private int mode = 0;
	private Image offscreen = null;
	private Graphics og = null;
	private int startx = 0;
	private int starty = 0;
	private int fontAscent = 0;
	private String fontName = Param.getInstance().getIndicatorFont();
	private int fontSize = Param.getInstance().getIndicatorFontSize();
	private Font font;
	private Color msgColor = Color.black;

	public CustIndicator() {
		mode = SCROLL_WIGGLE;
	}

	public CustIndicator(int mode) {
		this.mode = mode;
	}

	public CustIndicator(Color msgColor) {
		this();
		this.msgColor = msgColor;
	}

	public CustIndicator(int mode, Color msgColor) {
		this.mode = mode;
		this.msgColor = msgColor;
	}

	private Color getMsgColor() {
		return msgColor;
	}

	private Color getBackColor() {
		return Color.white;
	}

	private Font getMsgFont() {
		return new Font(fontName, Font.PLAIN, fontSize);
	}

	public void update(Graphics g) {
		if (isShowing()) {
			paint(g);
		}
	}

	public synchronized void setMessage(String message) {
		this.message = message;
		if (mode == SCROLL_MARQUEE || mode == SCROLL_MARQUEE2) {
			startx = printwidths;
		} else {
			startx = 0;
		}
		notifyAll(); // notify content change
	}

	public synchronized String getMessage() {
		return message;
	}

	public void setprintwidths(int printwidths) {
		this.printwidths = printwidths;
		startx = 0;
	}

	private int printheights;

	public void setprintheights(int printheights) {
		this.printheights = printheights;
	}

	public int getprintwidths() {
		return printwidths;
	}

	public Dimension getPreferredSize() {
		// return new Dimension(printwidths, 20);
		return new Dimension(printwidths, printheights);
	}

	private boolean display = true;

	synchronized public boolean getDisplay() {
		if (startx == this.printwidths)
			display = false;
		else
			display = true;
		return display;
	}

	synchronized public void setDisplay(boolean display) {
		this.display = display;
	}

	public void paint(Graphics g) {
		if (getWidth() <= 0 || getHeight() <= 0)
			return;

		if (offscreen == null) {
			offscreen = createImage(getWidth(), getHeight());
		}

		// og = offscreen.getGraphics();
			font = getMsgFont();
			FontMetrics fm = g.getFontMetrics(font);
			starty = (getHeight() - fm.getAscent()) / 2 + fm.getAscent() - 2;

			og = offscreen.getGraphics();
			og.setFont(font);
			((Graphics2D) og).setRenderingHint(
					RenderingHints.KEY_TEXT_ANTIALIASING,
					RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			og.setColor(getBackColor());
			og.fillRoundRect(0, 0, getWidth(), getHeight(), 13, 13);
			og.fillRect(0, getHeight() - 10, getWidth(), 10);
			og.setColor(getMsgColor());
			og.drawString(message, startx, starty);
			paintedMessage = message;
			paintedStartx = startx;
			paintedStarty = starty;
			g.drawImage(offscreen, 0, 0, this);
			og.dispose();
	}
}
