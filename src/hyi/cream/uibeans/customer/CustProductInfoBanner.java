package hyi.cream.uibeans.customer;

import hyi.cream.POSTerminalApplication;
import hyi.cream.SystemInfo;
import hyi.cream.event.SystemInfoEvent;
import hyi.cream.event.SystemInfoListener;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.uibeans.EPOSBackground;
import hyi.cream.uibeans.ScreenBanner;
import hyi.cream.util.CreamToolkit;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;

public class CustProductInfoBanner extends Canvas implements SystemInfoListener {

	private String amountStringPainted = "";
	private String printStringPainted = "";
	private String qtyStringPainted = "";
	private int offScreenH;
	private int offScreenW;
	private ArrayList lineTitles = new ArrayList();
	private ArrayList lineFields = new ArrayList();
	private ArrayList lineWidths = new ArrayList();
	private SystemInfo systemInfo;
	private Image offscreen;
	private Dimension size = null;
	private Color valueColor = Color.black;
	private Color headerColor = Color.black;
	private String fontName;
	private int fontSize;
	private Image connectedImage;
	private Image disconnectedImage;
	private Color backgroundColor = Color.black;
	private Font fv;
	private Font fh;
	private boolean antiAlias = true;
	private java.util.ResourceBundle res = CreamToolkit.GetResource();
	private int arcWidth = 26;

	public CustProductInfoBanner(File propFile, Color bgColor,
			Color valueColor, Color headerColor)
			throws ConfigurationNotFoundException {
		this(propFile);
		this.backgroundColor = bgColor;
		this.headerColor = headerColor;
		this.valueColor = valueColor;
	}

	public CustProductInfoBanner(File propFile)
			throws ConfigurationNotFoundException {
		connectedImage = Toolkit.getDefaultToolkit().getImage(
				ScreenBanner.class.getResource("connected.gif"));
		disconnectedImage = Toolkit.getDefaultToolkit().getImage(
				ScreenBanner.class.getResource("disconnected.gif"));

		char ch = '#';
		try {
			FileInputStream filein = new FileInputStream(propFile);
			InputStreamReader inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
			BufferedReader in = new BufferedReader(inst);
			int i = 0;
			String line = "";
			boolean getLine = false;
			while ((line = in.readLine()) != null) {
				getLine = true;
				do {
					if (!getLine)
						line = in.readLine();
					getLine = false;
					while (line.equals("")) {
						line = in.readLine();
					}
					i = 0;
					do {
						ch = line.charAt(i);
						i++;
					} while ((ch == ' ' || ch == '\t') && i < line.length());
				} while (ch == '#' || ch == ' ' || ch == '\t');

				String s = "";
				if (line.startsWith("font")) {
					StringTokenizer t0 = new StringTokenizer(line, ",", true);
					while (t0.hasMoreTokens()) {
						s = t0.nextToken();
						if (s.startsWith("fontName")) {
							fontName = s.substring("fontName".length() + 1, s
									.length());
						} else if (s.startsWith("fontSize")) {
							s = s
									.substring("fontSize".length() + 1, s
											.length());
							try {
								fontSize = Integer.parseInt(s);
							} catch (Exception e) {
								fontSize = 16;
							}
						} else if (s.startsWith("antiAlias")) {
							antiAlias = s.substring("antiAlias".length() + 1,
									s.length()).equalsIgnoreCase("yes");
						} else if (s.startsWith("arcWidth")) {
							s = s
									.substring("arcWidth".length() + 1, s
											.length());
							try {
								arcWidth = Integer.parseInt(s);
							} catch (Exception e) {
								arcWidth = 26;
							}
						} else if (s.startsWith("antiAlias")) {
						}
					}
					fh = new Font(fontName, Font.PLAIN, fontSize);
					fv = new Font(fontName, Font.BOLD, fontSize + 1);
				}
			}
		} catch (FileNotFoundException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("File not found: " + propFile + ", at "
					+ this);
		} catch (IOException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("IO exception at " + this);
		}
	}

	/**
	 * Sets the associated transaction object. It'll also register itself as the
	 * system information listener of the system information object. *
	 * 
	 * @param systemInfo
	 *            the sytem information object
	 */
	public void setSystemInfo(SystemInfo systemInfo) {
		this.systemInfo = systemInfo;
	}

	/**
	 * Returns the associated transaction object.
	 */
	public SystemInfo getSystemInfo() {
		return systemInfo;
	}

	/**
	 * Invoked when transaction has been changed.
	 * 
	 * @param e
	 *            an event object represents the changes.
	 */
	private boolean isChanged = false;

	public void systemInfoChanged(SystemInfoEvent e) {
		isChanged = true;
		repaint();
	}

	public void update(Graphics g) {
		paint(g);
	}

	public void paint(Graphics g) {
		int startx = 5;
		int starty = 0;
		int width = getWidth() - 10;
		String itemNameString;
		String amountString;
		String qtyString;

		if (offscreen == null || offScreenW != getWidth()
				|| offScreenH != getHeight()) {
			offscreen = createImage(getWidth(), getHeight());
			offScreenW = getWidth();
			offScreenH = getHeight();
			amountStringPainted = "~"; // make it repaint, don't use buffered
		}

		Graphics og = offscreen.getGraphics();
		try {
			java.util.List printList = new Vector();
			FontMetrics fmh = og.getFontMetrics(fh);
			FontMetrics fmv = og.getFontMetrics(fv);
			itemNameString = systemInfo.getCurrentLineItemName();
			amountString = systemInfo.getCurrentLineItemAmount();
			qtyString = systemInfo.getCurrentLineItemQty();

			printStringPainted = itemNameString;
			amountStringPainted = amountString;
			qtyStringPainted = qtyString;

			Graphics2D g2 = (Graphics2D) og;
			if (antiAlias)
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);
			og.setColor(backgroundColor);
			CreamToolkit.drawInfoFrame(og, 0, 0, getWidth(), getHeight(),
					arcWidth);

			// 超长处理
			String subPrintString = itemNameString;
			while (subPrintString.length() > 0) {
				int stringLength = fmh.stringWidth(subPrintString);
				int stri = subPrintString.length();
				if (stringLength <= width && !subPrintString.equals("")) {
					printList.add(subPrintString);
					subPrintString = "";
				}
				String str = "";
				while (stringLength > width) {
					str = subPrintString.substring(0, stri);
					stringLength = fmh.stringWidth(str);
					stri--;
				}
				if (!str.equals(""))
					printList.add(str);
				if (stri != subPrintString.length()
						&& !subPrintString.equals(""))
					subPrintString = subPrintString.substring(stri + 1,
							subPrintString.length());
			}
			if (printList.isEmpty())
				printList.add("");
			// 高的处理
			java.util.List marginList = new Vector();
			int marginLine = (printList.size() + 3) * 2;
			int itemNameHeight = fmv.getHeight();
			int amountStringHeight = fmv.getHeight();
			int margin = 0;
			int marginH = 0;
			int marginW = 10;
			int curHeight = 0;
			int subHeight = getHeight() - itemNameHeight * printList.size() - 3
					* amountStringHeight;
			if (marginLine > 0) {
				while (subHeight < 0) {
					printList.remove(printList.size() - 1);
					subHeight = getHeight() - itemNameHeight * printList.size()
							- 3 * amountStringHeight;
				}
				if (subHeight > marginLine) {
					margin = Math.round(subHeight / marginLine - 0.5f);
					subHeight = subHeight - marginLine * margin;
				}
				for (int i = 0; i < marginLine; i++) {
					int margin0 = margin;
					if (subHeight > 0) {
						margin0 = margin;
						subHeight--;
					}
					marginList.add(new Integer(margin0));
				}
//				og.setColor(EPOSBackground.getInstance().getColor6());
				int marginIndex = 0;
				og.setColor(headerColor);
				og.setFont(fh);
				startx += marginW;
				marginH = ((Integer) marginList.get(marginIndex)).intValue();
				starty += marginH;
				og.drawString(res.getString("PluName2"), startx, starty
						+ fmh.getAscent());
				og.setFont(fv);
				for (int i = 0; i < printList.size(); i++) {
					marginH = ((Integer) marginList.get(++marginIndex))
							.intValue()
							+ ((Integer) marginList.get(++marginIndex))
									.intValue();
					starty += fmh.getHeight() + marginH;
					og.drawString("  " + (String) printList.get(i), startx,
							starty + fmh.getAscent());
				}

				starty += fmh.getHeight()
						+ ((Integer) marginList.get(++marginIndex)).intValue();
				marginH = ((Integer) marginList.get(marginIndex + 1))
						.intValue()
						+ ((Integer) marginList.get(marginIndex + 2))
								.intValue()
						+ ((Integer) marginList.get(marginIndex + 3))
								.intValue()
						+ ((Integer) marginList.get(marginIndex + 4))
								.intValue();
				og.setColor(EPOSBackground.getInstance().getColor6());
				og.fillRect(0, starty, getWidth() / 3, 2 * fmv.getHeight()
						+ marginH - arcWidth / 2);
				og.fillRoundRect(0, starty + 2 * fmv.getHeight() + marginH
						- arcWidth, getWidth() / 6, arcWidth + 4, arcWidth,
						arcWidth);
				og.fillRect(getWidth() / 9, starty + 2 * fmv.getHeight()
						+ marginH - arcWidth, (2 * getWidth()) / 9,
						arcWidth + 4);
				og.setColor(headerColor);

				og.setFont(fv);
				startx = marginW;
				marginH = ((Integer) marginList.get(++marginIndex)).intValue();
				starty += marginH;
				og.drawString(res.getString("PricingPluPrice2"), startx, starty
						+ fmv.getAscent());
				int valuex = getWidth() - fmv.stringWidth(amountString) - 5;
				og.drawString(amountString, valuex, starty + fmv.getAscent());

				marginH = ((Integer) marginList.get(++marginIndex)).intValue()
						+ ((Integer) marginList.get(++marginIndex)).intValue();
				starty += fmh.getHeight() + marginH;
				startx = marginW;
				og.setFont(fh);
				og.drawString(res.getString("Quantity2"), startx, starty
						+ fmv.getAscent());
				og.setFont(fv);
				valuex = getWidth() - fmv.stringWidth(qtyStringPainted) - 5;
				og.drawString(qtyStringPainted, valuex, starty
						+ fmv.getAscent());
			}

			g.drawImage(offscreen, 0, 0, this);
			og.dispose();
		} catch (Exception e) {
			e.printStackTrace(CreamToolkit.getLogger());
		}

//		// for transaction event forward
//		if (isChanged) {
//			if (POSTerminalApplication.getInstance().getCurrentTransaction()
//					.decreaseLockCount() == 0) {
//				POSTerminalApplication.getInstance().getCurrentTransaction()
//						.setLockEnable(true);
//			}
//			isChanged = false;
//		}
	}
}
