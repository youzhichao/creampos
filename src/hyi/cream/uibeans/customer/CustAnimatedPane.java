package hyi.cream.uibeans.customer;

import hyi.cream.game.DigPrize;

import java.awt.Graphics;
import java.awt.Panel;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class CustAnimatedPane extends Panel {
	private CustIndicator informationIndicator;
	private Properties prop;
	private DigPrize dp;

	public CustAnimatedPane() {
		init();
	}
	
	private void getConfigFileData(){
		String root = "conf"; 
		try {
			String conf = "conf" + File.separator + "digprize.conf";
			prop = new Properties();
			prop.load(new FileInputStream(conf));
			System.out.println(prop);
		} catch (Exception e) { }

		/*
		String sRow = prop.getProperty("row");
		String sColumn = prop.getProperty("column");
		String sMaxClickTimes = prop.getProperty("maxClickTimes");
		String sButtBeginImage = prop.getProperty("beginImage");
		String sFirstImage = prop.getProperty("firstImage");
		String sSecondImage = prop.getProperty("secondImage");
		String sThirdImage = prop.getProperty("thirdImage");
		String sFirst = prop.getProperty("first");
		String sSecond = prop.getProperty("second");
		String sThird = prop.getProperty("third");
		String sMaxFirst = prop.getProperty("maxfirst");
		String sMaxSecond = prop.getProperty("maxsecond");
		String sMaxThird = prop.getProperty("maxthird");

		try {
			dp.setRow(Integer.parseInt(sRow));
			dp.setColumn(Integer.parseInt(sColumn));
			dp.setMaxClickTimes(Integer.parseInt(sMaxClickTimes));
			dp.setBeginImage(root + File.separator + sButtBeginImage);
			dp.setFirstImage(root + File.separator + sFirstImage);
			dp.setSecondImage(root + File.separator + sSecondImage);
			dp.setThirdImage(root + File.separator + sThirdImage);
			dp.setFirstPrize(Integer.parseInt(sFirst));
			dp.setSecondPrize(Integer.parseInt(sSecond));
			dp.setThirdPrize(Integer.parseInt(sThird));
			dp.setMaxFirstPrize(Integer.parseInt(sMaxFirst));
			dp.setMaxSecondPrize(Integer.parseInt(sMaxSecond));
			dp.setMaxThirdPrize(Integer.parseInt(sMaxThird));
			
		} catch (Exception e) {	
			e.printStackTrace();
		}
		*/
	}

	private void init() {
		this.setLayout(null);
		informationIndicator = new CustIndicator();
		informationIndicator.setMessage("This is test......................");
//		this.add(informationIndicator);
		if (informationIndicator != null) {
			informationIndicator.setBounds(0, 10,
					this.getWidth(), 50);
//					EPOSBackground.getInstance().informationIndicatorH);
			informationIndicator.repaint();
//			new Thread(informationIndicator).start();
		}
		getConfigFileData();
		dp = new DigPrize();
//		dp.init();

//		this.add(dp);
//		dp.repaint();
//		dp.setBounds(0, 60, this.getWidth(), this.getHeight() - 60);
	}
	
	public void again() {
		dp.setBounds(0, 60, this.getWidth(), this.getHeight() - 60);
	}

	/*
	public void paint(Graphics g) {
		super.paint(g);
		if (informationIndicator != null) {
			informationIndicator.setBounds(0, 10,
					this.getWidth(), 50);
//					EPOSBackground.getInstance().informationIndicatorH);
			informationIndicator.repaint();
		}
		dp.setBounds(0, 60, this.getWidth(), this.getHeight() - 60);
		dp.setVisible(true);
		dp.repaint();
	}
	*/
}
