package hyi.cream.uibeans.customer;

import hyi.cream.SystemInfo;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.uibeans.EPOSBackground;
import hyi.cream.util.CreamToolkit;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Panel;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class CustomerPanel /*extends Panel*/ {
/*
	private CustomerBanner timeBanner;
	private CustPayingPaneBanner custPayingPane;
	private CustProductInfoBanner custProductInfo;
	private CustomerBanner custAdvertWord;
	private CustomerTextBanner custWelcome;
	private int marginH = 10;
	private int marginW = 10;
	private CustAnimatedPane  animatedPane;
//	private Button  animatedPane;
	
	private void init() {
		this.setBackground(EPOSBackground.getInstance().getColor1());
		
	}
	
	public CustomerPanel(Cursor c, SystemInfo systemInfo, Transaction currentTransaction) throws ConfigurationNotFoundException {
		init();
		createComponents(c, systemInfo, currentTransaction);
//		setComponetsBounds();
	}
	
	public void setComponetsBounds() {
		if (GetProperty.getCreateAdPanel("no").equalsIgnoreCase("yes")) {
			// animatedPane
			int width = this.getWidth() / 2;
			int startx = marginW;
			int starty = marginH;
			int compW = width - 2 * marginW;
			int compH = this.getHeight() - 3 * marginH;
			CustIndicator informationIndicator;

			if (animatedPane != null) {

//				animatedPane.setBackground(Color.black);
				animatedPane.setBounds(startx, starty, compW, compH);
				animatedPane.repaint();
//				animatedPane.again();
				System.out.println("------------- animatedPane repaint.........");
			}
			
			// timebanne
			startx = width + marginW;
			starty = marginH;
			compW = width - 2 * marginW;
			compH = 30;
			if (timeBanner != null) {
				timeBanner.setBounds(startx, starty, compW, compH);
				timeBanner.repaint();
			}
			
			starty += compH + marginH;
			compH = 150;
			if (custPayingPane != null) {
				custPayingPane.setBounds(startx, starty, compW, compH);
				custPayingPane.repaint();
			}
			
			starty += compH + marginH;
			if (custProductInfo != null) {
				custProductInfo.setBounds(startx, starty, compW, compH);
				custProductInfo.repaint();
			}
			
			starty += compH + marginH;
			compH = 50;
			if (custAdvertWord != null) {
				custAdvertWord.setBounds(startx, starty, compW, compH);
				custAdvertWord.repaint();
			}

			starty += compH + marginH;
			compH = 80;
			if (custWelcome != null) {
				custWelcome.setBounds(startx, starty, compW, compH);
				custWelcome.repaint();
			}
		}
	}
	
	public void createComponents(Cursor c, SystemInfo systemInfo, Transaction currentTransaction) throws ConfigurationNotFoundException {
		if (GetProperty.getCreateAdPanel("no").equalsIgnoreCase("yes")) {
			Properties prop = null;
			try {
				String conf = "conf" + File.separator + "digprize.conf";
				prop = new Properties();
				prop.load(new FileInputStream(conf));
				System.out.println(prop);
			} catch (Exception e) { }
			animatedPane = new CustAnimatedPane();
			animatedPane.setCursor(c);
			this.add(animatedPane);
			
			// timebanner
			timeBanner = new CustomerBanner(CreamToolkit
					.getConfigurationFile("timeBanner"), 
//					EPOSBackground.getInstance().getColor8(),
					Color.black,
					Color.white, Color.white);
			timeBanner.setSystemInfo(systemInfo);
			systemInfo.addSystemInfoListener(timeBanner);
			timeBanner.setCursor(c);
			this.add(timeBanner);

			custPayingPane = new CustPayingPaneBanner(CreamToolkit
					.getConfigurationFile("custPayingPane"));
			custPayingPane.setCursor(c);
			custPayingPane.setTransaction(currentTransaction);
			systemInfo.addSystemInfoListener(custPayingPane);
			this.add(custPayingPane);

			custProductInfo = new CustProductInfoBanner(CreamToolkit
					.getConfigurationFile("custProductInfo"),
					EPOSBackground.getInstance().getTextBackgroundColor(),
					Color.black, Color.black);
			custProductInfo.setCursor(c);
			custProductInfo.setSystemInfo(systemInfo);
			systemInfo.addSystemInfoListener(custProductInfo);
			this.add(custProductInfo);

			custAdvertWord = new CustomerBanner(CreamToolkit
					.getConfigurationFile("custAdvertWord"),
					Color.black,
					Color.white, Color.white);
			custAdvertWord.setCursor(c);
			custAdvertWord.setSystemInfo(systemInfo);
			systemInfo.addSystemInfoListener(custAdvertWord);
			this.add(custAdvertWord);

			custWelcome = new CustomerTextBanner(CreamToolkit
					.getConfigurationFile("custWelcome"), 
					Color.white, Color.black, Color.black);
			custWelcome.setCursor(c);
			custWelcome.setSystemInfo(systemInfo);
			systemInfo.addSystemInfoListener(custWelcome);
			this.add(custWelcome);
		}
	}
*/
}
