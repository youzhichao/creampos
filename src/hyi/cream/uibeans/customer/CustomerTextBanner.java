package hyi.cream.uibeans.customer;

import hyi.cream.POSTerminalApplication;
import hyi.cream.SystemInfo;
import hyi.cream.groovydac.Param;
import hyi.cream.event.SystemInfoEvent;
import hyi.cream.event.SystemInfoListener;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.uibeans.EPOSBackground;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

public class CustomerTextBanner extends Canvas implements SystemInfoListener {

	private SystemInfo systemInfo;
	// private String backImage = "screenbanner.jpg";
	// Image ig = CreamToolkit.getImage(backImage);
	private Image offscreen;
	private Dimension size = null;
	private Color valueColor = Color.white;
	private Color headerColor = Color.white;
	private String fontName = "SimHei"; // Param.getInstance().getScreenBannerFont();
	private int fontSizeL = 20;
	private int fontSizeS = 16;
	private Color backgroundColor = Color.black;
	private Font fv;
	private String align;
	private String field;
	private boolean antiAlias = true;
	private int arcWidth = 26;

	public CustomerTextBanner(File propFile, Color bgColor, Color valueColor,
			Color headerColor) throws ConfigurationNotFoundException {
		this(propFile);
		this.backgroundColor = bgColor;
		this.headerColor = headerColor;
		this.valueColor = valueColor;
	}

	/**
	 * When constructing an ScreenBanner object, it'll first read the
	 * configuration file "screenbanner.conf," which defines all the properties
	 * of ScreenBanner, including fields and widths of the two displaying lines.
	 * The fields are the properties of the associated SystemInfo objects.
	 */
	public CustomerTextBanner(File propFile)
			throws ConfigurationNotFoundException {
		char ch = '#';
		try {
			FileInputStream filein = new FileInputStream(propFile);
			InputStreamReader inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
			BufferedReader in = new BufferedReader(inst);
			int i = 0;
			String line = "";
			boolean getLine = false;
			out: while ((line = in.readLine()) != null) {
				getLine = true;
				do {
					if (!getLine) {
						line = in.readLine();
						if (line == null)
							break out;
					}
					getLine = false;
					while (line.equals("")) {
						line = in.readLine();
						if (line == null)
							break out;
					}
					i = 0;
					do {
						ch = line.charAt(i);
						i++;
					} while ((ch == ' ' || ch == '\t') && i < line.length());
				} while (ch == '#' || ch == ' ' || ch == '\t');

				String s = "";
				if (line.startsWith("font")) {
					StringTokenizer t0 = new StringTokenizer(line, ",", true);
					while (t0.hasMoreTokens()) {
						s = t0.nextToken();
						if (s.startsWith("fontName")) {
							fontName = s.substring("fontName".length() + 1, s
									.length());
						} else if (s.startsWith("fontSizeL")) {
							try {
								s = s.substring("fontSizeL".length() + 1, s
										.length());
								fontSizeL = Integer.parseInt(s);
							} catch (Exception e) {
								fontSizeL = 20;
							}
						} else if (s.startsWith("align")) {
							try {
								s = s.substring("align".length() + 1, s
										.length());
								align = s;
							} catch (Exception e) {
								align = "R";
							}
						} else if (s.startsWith("antiAlias")) {
							antiAlias = s.substring("antiAlias".length() + 1,
									s.length()).equalsIgnoreCase("yes");
						} else if (s.startsWith("field")) {
							field = s.substring("field".length() + 1, s
									.length());
						} else if (s.startsWith("fontSizeS")) {
							try {
								s = s.substring("fontSizeS".length() + 1, s
										.length());
								fontSizeS = Integer.parseInt(s);
							} catch (Exception e) {
								fontSizeS = 16;
							}
						} else if (s.startsWith("arcWidth")) {
							s = s
									.substring("arcWidth".length() + 1, s
											.length());
							try {
								arcWidth  = Integer.parseInt(s);
							} catch (Exception e) {
								arcWidth = 26;
							}
						}
					}
				}

			}
		} catch (FileNotFoundException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("File not found: " + propFile + ", at "
					+ this);
		} catch (IOException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("IO exception at " + this);
		}
	}

	/**
	 * Sets the associated transaction object. It'll also register itself as the
	 * system information listener of the system information object. *
	 * 
	 * @param systemInfo
	 *            the sytem information object
	 */
	public void setSystemInfo(SystemInfo systemInfo) {
		this.systemInfo = systemInfo;
	}

	/**
	 * Returns the associated transaction object.
	 */
	public SystemInfo getSystemInfo() {
		return systemInfo;
	}

	/**
	 * Invoked when transaction has been changed.
	 * 
	 * @param e
	 *            an event object represents the changes.
	 */
	private boolean isChanged = false;

	public void systemInfoChanged(SystemInfoEvent e) {
		isChanged = true;
		repaint();
		/*
		 * if
		 * (POSTerminalApplication.getInstance().getCurrentTransaction().decreaseLockCount() ==
		 * 0) {
		 * POSTerminalApplication.getInstance().getCurrentTransaction().setLockEnable(true); }
		 */
	}

	public Dimension getPreferredSize() {
		return size;
	}

	public Dimension getMinimumSize() {
		return size;
	}

	public void invalidate() {
		super.invalidate();
		offscreen = null;
	}

	public void update(Graphics g) {
		if (isShowing()) {
			paint(g);
		}
	}

	private java.util.List processHeight(int fontHeight, int lines) {
		java.util.List list = new Vector();
		try {
			int height = getHeight();
			int sub = height - lines * fontHeight;
			int sHeight = 0;
			if (sub > 0) {
				sHeight = Math.round(sub / (lines + 1));
				sub = sub - (lines + 1) * sHeight;
				while (sub > 0) {
					sub = Math.round(sub / (lines + 1) - 0.5f);
				}
				sHeight += sub;
			}
			list.add(new Integer(sHeight));
			int lastHeight = sHeight;
			for (int i = 0; i < lines - 1; i++) {
				list.add(new Integer(sHeight + lastHeight + fontHeight));
				lastHeight += sHeight + fontHeight;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public void paint(Graphics g) {
		if (getWidth() <= 0 || getHeight() <= 0) {
			EPOSBackground.getInstance().repaint();
			return;
		}

		int width = getWidth();
		int startx = 0;
		String printString;
		if (offscreen == null) {
			offscreen = createImage(getWidth(), getHeight());
		}
		Graphics og = offscreen.getGraphics();
		if (antiAlias)
			((Graphics2D) og).setRenderingHint(
					RenderingHints.KEY_TEXT_ANTIALIASING,
					RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		og.setColor(backgroundColor);
		CreamToolkit.drawInfoFrame(og, 0, 0, getWidth(), getHeight(), arcWidth);

		//
		String[] printStrs = { "" };
		try {
			Method method = SystemInfo.class.getDeclaredMethod("get" + field,
					new Class[0]);
			Object retObject = method.invoke(systemInfo, new Object[0]);
			printStrs = (String[]) retObject;
		} catch (Exception e) {
			e.printStackTrace(CreamToolkit.getLogger());
		}

		if (printStrs.length == 1) {
			fv = new Font(fontName, Font.PLAIN, fontSizeL);
			align = "C";
		} else {
			fv = new Font(fontName, Font.PLAIN, fontSizeS);
		}

		FontMetrics fm = og.getFontMetrics(fv);

		java.util.List hList = processHeight(fm.getHeight(), printStrs.length);

		try {
			int line = 0;
			while (line < printStrs.length) {

				if (printStrs[line] == null)
					printString = "";
				else
					printString = printStrs[line].toString();

				int stri = printString.length();
				// int in =
				// Integer.parseInt((String)FirstLineWidths.get(i))
				// * 7;
				int stringlength = fm.stringWidth(printString);

				// 超长处理
				if (align == null || !align.equalsIgnoreCase("skip"))
					while (stringlength > width) {
						printString = printString.substring(0, stri);
						stringlength = fm.stringWidth(printString);
						stri--;
					}

				// 绘制Value
				og.setColor(valueColor);
				og.setFont(fv);

				if (align != null && align.equalsIgnoreCase("R"))
					og.drawString(printString, width - stringlength, fm
							.getAscent()
							+ ((Integer) hList.get(line)).intValue());
				else if (align != null && align.equalsIgnoreCase("C"))
					og.drawString(printString, (width - stringlength) / 2, fm
							.getAscent()
							+ ((Integer) hList.get(line)).intValue());
				else
					og.drawString(printString, startx, fm.getAscent()
							+ ((Integer) hList.get(line)).intValue());
				line++;
			}
			g.drawImage(offscreen, 0, 0, this);
			og.dispose();
		} catch (Exception e) {
			e.printStackTrace(CreamToolkit.getLogger());
		}

//		// for transaction event forward
//		if (isChanged) {
//			if (POSTerminalApplication.getInstance().getCurrentTransaction()
//					.decreaseLockCount() == 0) {
//				POSTerminalApplication.getInstance().getCurrentTransaction()
//						.setLockEnable(true);
//			}
//			isChanged = false;
//		}
	}

}
