package hyi.cream.uibeans;

public class DaiShouPluButton extends POSButton {
	private String catNO;
	private String detailNO;
	private String pluNO;
	
	public DaiShouPluButton(int row, int column, int level, String label,
			int keyCode, String pluNO, String catNO, String detailNO) {
		super(row, column, level, label, keyCode);
		this.pluNO = pluNO;
		this.catNO = catNO;
		this.detailNO = detailNO;
	}

	/**
	 * @return the catNO
	 */
	public String getCatNO() {
		return catNO;
	}

	/**
	 * @return the detailNO
	 */
	public String getDetailNO() {
		return detailNO;
	}

	/**
	 * @return the pluNO
	 */
	public String getPluNO() {
		return pluNO;
	}


}
