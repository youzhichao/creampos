package hyi.cream.uibeans;

/**
 * 訂金尾款退回/着付殘金退回 按鍵.
 */
public class BackPaymentRefundButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param label clear label on button.
     */
    public BackPaymentRefundButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    public BackPaymentRefundButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}