package hyi.cream.uibeans;

public class DIYButton extends POSButton {

	/**
	 * Constructor.
	 * 
	 * @param row
	 *            row position.
	 * @param column
	 *            column position.
	 * @param ageLevelLabel
	 *            age level label on button.
	 * @param ageID
	 *            age level ID.
	 */
	public DIYButton(int row, int column, int level, String diyLabel) {
		super(row, column, level, diyLabel);
	}

	public DIYButton(int row, int column, int level, String diyLabel,
			int keyCode) {
		super(row, column, level, diyLabel, keyCode);
	}
}
