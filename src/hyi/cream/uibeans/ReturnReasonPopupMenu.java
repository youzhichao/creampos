/*
 * Created on 2004-8-13
 *
 */
package hyi.cream.uibeans;

import java.util.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.state.*;
import hyi.cream.util.*;

/**
 * @author pyliu
 *
 */
public class ReturnReasonPopupMenu implements PopupMenuListener {

	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private PopupMenuPane popupMenu = null;
	private ArrayList menuItems = new ArrayList();
	private int returnType;
	private String lineItemNo;
	public static final int RETURN_ALL = 0;
	public static final int RETURN_ITEM = 1;
	private ResourceBundle res = CreamToolkit.GetResource();

	public ReturnReasonPopupMenu(int returnType, String lineItemNo) {
		this.returnType = returnType;
		this.lineItemNo = lineItemNo;
		popupMenu = PopupMenuPane.getInstance();
	}

	public void popupMenu() {
		if (popupMenu.isVisible()) {
			return;
		}
		menuItems = getMenuItems();
		if (menuItems == null || menuItems.isEmpty())
			return;
		popupMenu.setPopupMenuListener(this);
		popupMenu.setMenu(menuItems);
		popupMenu.centerPopupMenu();
		popupMenu.setSelectMode(0);
		//!!!!!Bruce
		//popupMenu.setModal(true);
		popupMenu.setVisible(true);
	}
	
	protected ArrayList getMenuItems() {
		ArrayList list = new ArrayList();
		// 19 : 销退原因
		int i = 1;
		Iterator it = Reason.queryByreasonCategory("19");
		if (it != null) {
			while (it.hasNext()) {
				Reason r = (Reason) it.next();
				String name = r.getreasonName();
				if (name != null && !name.trim().equals("")) {
					list.add(i + ". " + name + "/" + r.getreasonNumber());
					i = i + 1;
				}
			}
		}
		return list;
	}
	
	public void menuItemSelected() {
		if (popupMenu.getSelectedMode()) {
			int selectIndex = popupMenu.getSelectedNumber();
			String reasonID = (String)menuItems.get(selectIndex);
			System.out.println("reasonID: " + reasonID);

			StringTokenizer tk = new StringTokenizer(reasonID, " /");
			try {
				tk.nextToken();
				tk.nextToken();
				reasonID = tk.nextToken();
				ReturnSummaryState rss = (ReturnSummaryState)StateMachine.getInstance().getStateInstance("hyi.cream.state.ReturnSummaryState");
				if (returnType == RETURN_ALL) {
					rss.setReturnReasonID(reasonID);
				} else if (returnType == RETURN_ITEM) {
					HashMap returnReasonIDHm = rss.getReturnReasonIDHm();
					if (returnReasonIDHm == null) {
						returnReasonIDHm = new HashMap();
						rss.setReturnReasonIDHm(returnReasonIDHm);
					}
					returnReasonIDHm.put(lineItemNo, reasonID);
				} else {
					//
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
			}
			menuItems.clear();
			app.getMessageIndicator().setMessage(res.getString("ReturnAll"));
		} else {
			System.out.println("select nothing ...");
			popupMenu();
		}
		//!!!!!Bruce
		//popupMenu.setModal(false);
		popupMenu.setVisible(false);
	}
}
