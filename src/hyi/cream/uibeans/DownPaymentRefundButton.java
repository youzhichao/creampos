package hyi.cream.uibeans;

/**
 * 訂金退回/着付手付金退回 按鍵.
 */
public class DownPaymentRefundButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param label clear label on button.
     */
    public DownPaymentRefundButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    public DownPaymentRefundButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}