package hyi.cream.uibeans;

import java.util.*;
import java.io.*;
import java.awt.event.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.state.GetProperty;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.spos.JposException;
import hyi.spos.POSKeyboard;
import hyi.spos.events.DataEvent;

//import jpos.*;
//import jpos.events.*;

/**
 * 代收键. ③
 */
public class DaiShouButton extends POSButton implements ActionListener,
		PopupMenuListener {
	private ResourceBundle res = CreamToolkit.GetResource();

	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private ArrayList menu = new ArrayList();

	private ArrayList submenu = new ArrayList();

	private static ArrayList reasonArray = new ArrayList();

	private static int selectItem = 0;

	private int level = 0;

	private int daishouNumber = 0;

	private String pluNumber = "";

	private int daishouDetailNumber = 0;

	private String daishouDetailName = "";

	private String canPrint = "y";

	private String needPhoneCardNumber = "n";

	/**
	 * Constructor.
	 * 
	 * @param row
	 *            row position
	 * @param column
	 *            column position
	 * @param label
	 *            a string represents the button label
	 */
	public DaiShouButton(int row, int column, int level, String label) {
		super(row, column, level, label);
	}

	/**
	 * Constructor.
	 * 
	 * @param row
	 *            row position
	 * @param column
	 *            column position
	 * @param label
	 *            a string represents the button label
	 * @param keyCode
	 *            key code
	 */
	public DaiShouButton(int row, int column, int level, String label,
			int keyCode) {
		super(row, column, level, label, keyCode);
	}

	/**
	 * When user presses a keyboard button, it comes a JavaPOS's data event. If
	 * it is matched with the key code, then it fires POSButtonEvent to
	 * POSButtonListener.
	 */
	public void dataOccurred(DataEvent e) {

		if (!POSTerminalApplication.getInstance().getChecked()
				&& POSTerminalApplication.getInstance().getScanCashierNumber()) {
			return;
		}

		if (!POSTerminalApplication.getInstance().getEnabledPopupMenu()
				|| !(POSTerminalApplication.getInstance().getKeyPosition() == 2 || POSTerminalApplication
						.getInstance().getKeyPosition() == 3)) {
			return;
		}
		try {
			POSKeyboard p = (POSKeyboard) e.getSource();
			if (getKeyCode() == p.getPOSKeyData()) {
				Iterator daiShouItem = Reason.queryByreasonCategory("08");
				Reason r = null;
				reasonArray.clear();
				int i = 1;
				menu.clear();
				while (daiShouItem.hasNext()) {
					r = (Reason) daiShouItem.next();
					reasonArray.add(r);
					menu.add(i + "." + r.getreasonName());
					i++;
				}

				PopupMenuPane pop = app.getPopupMenuPane();
				pop.setMenu(menu);
				pop.setVisible(true);
				level = 1;
				pop.setPopupMenuListener(this);
				app.getMessageIndicator().setMessage(
						CreamToolkit.GetResource().getString("InputSelect"));
			}
		} catch (JposException ex) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("Jpos exception at " + this);
		}
	}

	public void menuItemSelected() {
		if (level == 0) {
			return;
		} else if (level == 1) {
			if (app.getPopupMenuPane().getSelectedMode()) {
				selectItem = app.getPopupMenuPane().getSelectedNumber() + 1;
				daishouNumber = selectItem;
				String filename = "daishou" + selectItem + ".conf";
				submenu = readFromFile(filename);

				PopupMenuPane pop = app.getPopupMenuPane();
				pop.setMenu(submenu);
				pop.setVisible(true);
				level = 2;
				pop.setPopupMenuListener(this);
				// menu.clear();
				app.getMessageIndicator().setMessage(
						CreamToolkit.GetResource().getString("InputSelect"));
			} else {
				menu.clear();
				app.getPopupMenuPane().finished();
			}
		} else if (level == 2) {
			if (app.getPopupMenuPane().getSelectedMode()) {
				selectItem = app.getPopupMenuPane().getSelectedNumber();
				daishouDetailNumber = selectItem + 1;

				/*
				 * daishouDetailName =
				 * app.getPopupMenuPane().getSelectedDescription(); pluNumber =
				 * app.getPopupMenuPane().getPluNumber(selectItem);
				 * 
				 * //01.A4,6920687730202,np,w
				 */
				// 2004.10.21
				String str = app.getPopupMenuPane().getAllDescription();
				StringTokenizer tk = new StringTokenizer(str, ",");
				int count = 0;
				canPrint = "y";
				needPhoneCardNumber = "n";
				String subStr = "";
				while (tk.countTokens() != 0) {
					subStr = tk.nextToken();
					if (count == 0)
						daishouDetailName = subStr.substring(subStr
								.indexOf('.') + 1, subStr.length());
					else if (count == 1)
						pluNumber = subStr;
					else if (count == 2)
						canPrint = subStr;
					else if (count == 3)
						needPhoneCardNumber = subStr;
					count++;
				}
				firePOSButtonEvent(new POSButtonEvent(this));
				submenu.clear();
			} else {
				submenu.clear();
				PopupMenuPane pop = app.getPopupMenuPane();
				pop.setMenu(menu);
				pop.setVisible(true);
				pop.repaint();
				level = 1;
				pop.setPopupMenuListener(this);
				app.getMessageIndicator().setMessage(
						CreamToolkit.GetResource().getString("InputSelect"));
			}
		}
	}

	public ArrayList readFromFile(String filename) {
		if (filename.indexOf(File.separator) == -1)
			filename = CreamToolkit.getConfigDir() + filename;
		File propFile = new File(filename);
		ArrayList menuArrayList = new ArrayList();
		try {
			FileInputStream filein = new FileInputStream(propFile);
			InputStreamReader inst = null;
			inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
			BufferedReader in = new BufferedReader(inst);
			String line;
			char ch = ' ';
			int i;

			do {
				do {
					line = in.readLine();
					if (line == null) {
						break;
					}
					while (line.equals("")) {
						line = in.readLine();
					}
					i = 0;
					do {
						ch = line.charAt(i);
						i++;
					} while ((ch == ' ' || ch == '\t') && i < line.length());
				} while (ch == '#' || ch == ' ' || ch == '\t');

				if (line == null) {
					break;
				}
				menuArrayList.add(line);
			} while (true);
		} catch (FileNotFoundException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("File is not found: " + propFile + ", at "
					+ this);
			return null;
		} catch (IOException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("IO exception at " + this);
			return null;
		}
		return menuArrayList;
	}

	public int getDaiShouNumber() {
		return daishouNumber;
	}

	public String getPluNumber() {
		return pluNumber;
	}

	public int getDaishouDetailNumber() {
		return daishouDetailNumber;
	}

	public String getDaishouDetailName() {
		return daishouDetailName;
	}

	/*
	 * 此代售可否打印
	 */
	public boolean isCanPrint() {
		if (canPrint.equalsIgnoreCase("y"))
			return true;
		return false;
	}

	/*
	 * 此代售是否需要卡号
	 */
	public boolean isNeedPhoneCardNumber() {
		if (needPhoneCardNumber.equalsIgnoreCase("y"))
			return true;
		return false;
	}

	public Reason getReason() {
		return (Reason) reasonArray.get(selectItem);
	}
}
