package hyi.cream.uibeans;

import hyi.cream.dac.*;
import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.spos.JposException;
import hyi.spos.POSKeyboard;
import hyi.spos.events.DataEvent;

import java.util.*;
//import jpos.*;
//import jpos.events.*;

/**
 * 金额键.
 */
public class TaxedAmountButton extends POSButton implements PopupMenuListener {

    private boolean showAllDeps;
    private String taxID = "";
    private String depID = "";
    private PopupMenuPane popupMenu = null;
    private ArrayList menu = new ArrayList();
    private String depType;

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param label taxed amount label on button.
     * @param taxID tax ID
     */
	public TaxedAmountButton(int row, int column, int level, String label, String taxID) {
        super(row, column, level, label);
        this.taxID = taxID;
    }
 
    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param label taxed amount label on button.
     * @param keyCode key code
     * @param taxID tax ID
     */
	public TaxedAmountButton(int row, int column, int level, String label, int keyCode,
        String taxID) {
        super(row, column, level, label, keyCode);
        this.taxID = taxID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TaxedAmountButton that = (TaxedAmountButton)o;
        if (taxID != null ? !taxID.equals(that.taxID) : that.taxID != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (taxID != null ? taxID.hashCode() : 0);
        return result;
    }

    @Override
    protected String paramString() {
        return taxID;
    }

    public String getTaxID() {
        return taxID;
    }

    public String getDepID() {
        return depID;
    }

    public ArrayList getMenu(String taxID) {
        //Bruce/20030317/
        // For Familymart: 如果TaxedAmountButton设置的taxID等于"0"时，就显示所有的部门。

        ArrayList menu = new ArrayList();

        if (showAllDeps || taxID.equals("0")) {
            showAllDeps = true;
            depType = null;
        } else {
            TaxType tax = TaxType.queryByTaxID(taxID);
            if (tax != null) {
                int taxPercent = tax.getPercent().multiply(new HYIDouble(100)).intValue();
        
                switch (taxPercent) {
                case 0:
                    depType = "0";
                    break;
                case 5:
                    depType = "5"; 
                    break;
                case 13:
                    depType = "D";  
                    break;
                case 17:
                    depType = "H";  
                    break;
                }
            }
        }

        Iterator ite = Dep.getDepIDs();
        int i = 1;
        if (ite != null) {
            while (ite.hasNext()) {
                Dep dep = (Dep)ite.next();
                String depID = dep.getDepID();
                if (depType == null || depID.endsWith(depType)) {
                    menu.add(i + ". " + dep.getDepName() + "/" + depID);
                    i = i + 1;
                }
            }
            return menu;
        } else {
            return null;
        }
    }

    /**
     * Override POSButton.dataOccurred().
     */
	public void dataOccurred(DataEvent e) {

        if (!POSTerminalApplication.getInstance().getChecked()
            && POSTerminalApplication.getInstance().getScanCashierNumber()) {
            return;
        }

        if (!POSTerminalApplication.getInstance().getEnabledPopupMenu()
            || !(POSTerminalApplication.getInstance().getKeyPosition() == 2
                || POSTerminalApplication.getInstance().getKeyPosition() == 3)) {
            return;
        }
		try {
			POSKeyboard p = (POSKeyboard)e.getSource();
			if (getKeyCode() == p.getPOSKeyData()) {
				popupMenu = PopupMenuPane.getInstance();
				if (popupMenu.isVisible()) {
					return;
				}
                menu = getMenu(getTaxID());
                if (menu == null) {
                    return;
                }
				popupMenu.setMenu(menu);
				popupMenu.centerPopupMenu();
				popupMenu.setSelectMode(0);
				popupMenu.setVisible(true);
				if (!popupMenu.isVisible()) {
                    menu.clear();
					firePOSButtonEvent(new POSButtonEvent(this));
				} else {
					popupMenu.setPopupMenuListener(this);
				}
            }
        } catch (JposException ex) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Jpos exception at " + this);
        }
    }

    public void menuItemSelected() {
        if (popupMenu.getSelectedMode()) {
            int selectIndex = popupMenu.getSelectedNumber();
            depID = (String)menu.get(selectIndex);
            //depID = depID.substring(depID.length() - 2);

            StringTokenizer tk = new StringTokenizer(depID, " /");
            try {
                tk.nextToken();
                tk.nextToken();
                depID = tk.nextToken();     // third token is dep id
            } catch (NoSuchElementException e) {
            }
            
            //Bruce/20030401
            //Get taxID. 如果TaxedAmountButton设置的taxID等于"0"时，就试图取得该dep的depTax,
            //如果拿不到就固定设成"1"
            if (depType == null) {
                Dep dep = Dep.queryByDepID(depID);
                if (dep != null)
                    taxID = dep.getDepTax();
                if (taxID == null)
                    taxID = "1";
            }

            menu.clear();
            firePOSButtonEvent(new POSButtonEvent(this));
		}
    }
}

