package hyi.cream.uibeans;

/**
 * 单品折扣率代号键.
 */
public class DiscountRateIDButton extends POSButton {

    /**
     * Constructor.
     * 
     * @param row row position.
     * @param column column position.
     * @param clearLabel clear label on button.
     */
    public DiscountRateIDButton(int row, int column, int level, String clearLabel) {
        super(row, column, level, clearLabel);
    }

    public DiscountRateIDButton(int row, int column, int level, String clearLabel, int keyCode) {
        super(row, column, level, clearLabel, keyCode);
    }
}