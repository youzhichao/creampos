package hyi.cream.uibeans;

import hyi.cream.POSTerminalApplication;
import hyi.cream.groovydac.Param;
import hyi.cream.dac.CreditType;
import hyi.cream.dac.Payment;
import hyi.cream.dac.SI;
import hyi.cream.dac.Transaction;
import hyi.cream.event.SystemInfoEvent;
import hyi.cream.event.SystemInfoListener;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.util.ChineseConverter;
import hyi.cream.util.ConfReader;
import org.apache.commons.lang.StringUtils;

import java.awt.*;
import java.awt.event.ActionListener;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.List;

public class PayingPaneBanner extends Canvas implements SystemInfoListener {

    private static final long serialVersionUID = 1L;
    private int offScreenH;
    private int offScreenW;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ArrayList headers = new ArrayList();
    private List<String> fields = new ArrayList();
    private ArrayList types = new ArrayList();
    private ActionListener actionListener = null;
    private Transaction trans = null;
    private Image offscreen = null;
    private Graphics og = null;
    private int paperNo = 0;
    static int paperMax = 0;
    static private Properties ageLevel = null;
    private Font fh;
    private Font fv;
    private boolean antiAlias = true;

    /**
     * When constructing an PayingPane object, it'll first read the
     * configuration file "PayingPane.conf," which defines all the properties of
     * PayingPane, including headers and fields. The fields are the properties
     * of the associated PayingPane objects.
     */
    public PayingPaneBanner(File propFile) throws ConfigurationNotFoundException {
        char ch = ' ';
        String line = "";
        try {
            //FileInputStream filein = new FileInputStream(propFile);
            //InputStreamReader inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
            //BufferedReader in = new BufferedReader(inst);
            ConfReader in = new ConfReader(propFile);

            line = in.readLine();
            while (line != null) {
                if (line != "") {
                    boolean available = false;
                    for (int i = 0; i < line.length(); i++) {
                        ch = line.charAt(i);
                        if (ch != ' ') {
                            available = true;
                            break;
                        }
                    }
                    if (available && line.startsWith("font")) {
                        int fontSizeH = 16;
                        String fontNameH = "STZhongSong";
                        int fontSizeV = 16;
                        String fontNameV = "STZhongSong";
                        StringTokenizer t0 = new StringTokenizer(line, ",",
                                true);
                        while (t0.hasMoreTokens()) {
                            String s = t0.nextToken();
                            if (s.startsWith("fontNameH")) {
                                fontNameH = s.substring("fontNameH".length() + 1, s.length());

                                // Fix font for simplified Chinese
                                if (!Param.getInstance().tranditionalChinese() && fontNameH.equals("cwTeXHeiBold"))
                                    fontNameH = "SimHei";

                            } else if (s.startsWith("fontSizeH")) {
                                try {
                                    s = s.substring("fontSizeH".length() + 1, s.length());
                                    fontSizeH = Integer.parseInt(s);
                                } catch (Exception e) {
                                    fontSizeH = 16;
                                }
                            } else if (s.startsWith("fontNameV")) {
                                fontNameV = s.substring("fontNameV".length() + 1, s.length());

                                // Fix font for simplified Chinese
                                if (!Param.getInstance().tranditionalChinese() && fontNameV.equals("cwTeXHeiBold"))
                                    fontNameV = "SimHei";

                            } else if (s.startsWith("fontSizeV")) {
                                try {
                                    s = s.substring("fontSizeV".length() + 1, s.length());
                                    fontSizeV = Integer.parseInt(s);
                                } catch (Exception e) {
                                    fontSizeV = 16;
                                }
                            } else if (s.startsWith("antiAlias")) {
                                antiAlias = s.substring(
                                        "antiAlias".length() + 1, s.length())
                                        .equalsIgnoreCase("yes");
                            }
                        }
                        fv = new Font(fontNameV, Font.PLAIN, fontSizeV);
                        fh = new Font(fontNameH, Font.PLAIN, fontSizeH);

                    } else if (available && ch != '#') {
                        StringTokenizer t1 = new StringTokenizer(line, ",");
                        headers.add(t1.nextToken());
                        fields.add(t1.nextToken());
                        types.add(t1.nextToken());
                    }
                }
                line = in.readLine();
            }
            initAgeLevel();

        } catch (NoSuchElementException e) {
            CreamToolkit.logMessage("Format error: " + propFile + " at line : " + line);
        }
    }

    public void initAgeLevel() {
        ageLevel = new Properties();
        try {
            //FileInputStream age = new FileInputStream(CreamToolkit.getConfigurationFile("agelevel"));
            //InputStreamReader inst = new InputStreamReader(age, GetProperty.getConfFileLocale());
            //BufferedReader in = new BufferedReader(inst);
            ConfReader reader = new ConfReader(CreamToolkit.getConfigurationFile("agelevel"));

            while (true) {
                String next;
                next = reader.readLine();
                if (next == null)
                    break;
                StringTokenizer token = new StringTokenizer(next, "=");
                while (token.hasMoreTokens()) {
                    String key = token.nextToken();
                    String value = "";
                    if (token.hasMoreTokens())
                        value = token.nextToken();
                    ageLevel.put(key, value);
                }
            }
            reader.close();
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        }
    }

    public boolean keyDataListener(int prompt) {
        int pageUpCode = app.getPOSButtonHome().getPageUpCode();
        int pageDownCode = app.getPOSButtonHome().getPageDownCode();
        if (prompt == pageUpCode) {
            if (paperNo <= 0) {
                return true;
            }
            paperNo--;
            repaint();
            return true;
        } else if (prompt == pageDownCode) {
            if (paperNo >= paperMax) {
                return true;
            }
            paperNo++;
            repaint();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the associated transaction object. It'll also register itself as the
     * transaction listener of the transaction object.
     *
     * @param trans
     *            the transaction object
     */
    public void setTransaction(Transaction trans) {
        this.trans = trans;
    }

    /**
     * Returns the associated transaction object.
     */
    public Transaction getTransaction() {
        return trans;
    }

    // 0 means normal
    // 1 means cash in and cash out
    // 2 means returnNumber
    private int mode = 0;

    /**
     * @param mode
     *      0 means normal
     *      1 means cash in and cash out
     *      2 means returnNumber
     */
    public void setMode(int mode) {
        this.mode = mode;
    }

    /**
     *      0 means normal
     *      1 means cash in and cash out
     *      2 means returnNumber
     * @return
     */
    public int getMode() {
        return mode;
    }

    // public void invalidate() {
    // super.invalidate();
    // offscreen = null;
    // }

    public void update(Graphics g) {
        paint(g);
    }

    /**
     * Overrides the paint methods of Components.
     */
    Image ig = CreamToolkit.getImage("payingpane.jpg");
    String fontName = Param.getInstance().getPayingPaneFont();
    Font f = new Font(fontName, Font.BOLD, 18);

    public void forcePaint() {
        paint(getGraphics());
    }

    public void paint(Graphics g) {

        if (offscreen == null || offScreenW != getWidth()
                || offScreenH != getHeight()) {
            offscreen = createImage(getWidth(), getHeight());
            offScreenW = getWidth();
            offScreenH = getHeight();
        }

        og = offscreen.getGraphics();
        if (antiAlias)
            ((Graphics2D) og).setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
        og.setColor(EPOSBackground.getInstance().getTextBackgroundColor());
        og.fillRect(0, 0, getSize().width, getSize().height);
        og.setFont(f);

        if (trans == null)
            return;

        ChineseConverter chineseConverter = ChineseConverter.getInstance();
        FontMetrics fm;
        String strObj;
        List titleList = new ArrayList();
        List valueList = new ArrayList();

        // check show mode
        if (getMode() == 1) {
            Iterator payments = Payment.getAllPayment();
            Payment payment = null;
            String paymentName = "";
            String paymentID = "";
            HYIDouble amount = null;
            String amt = "";
            // 显示支付方式
            while (payments.hasNext()) {
                payment = (Payment) payments.next();
                paymentName = payment.getPrintName();
                paymentID = payment.getPaymentID();
                System.out.println("PPP" + paymentID);

                // 显示支付名
                titleList.add(chineseConverter.convert(paymentName));
                for (int i = 1; i <= 4; i++) {
                    String fieldName = "PAYNO" + i;
                    if (paymentID.equals(trans.getFieldValue(fieldName))) {
                        amount = (HYIDouble) trans.getFieldValue("PAYAMT" + i);
                        System.out.println("AMT:" + amount);
                        break;
                    }
                }

                if (amount != null && amount.compareTo(new HYIDouble(0)) == 1) {
                    amt = amount.toString();
                    System.out.println("DISPLAY:" + amt);
                    // 显示支付金额
                } else {
                    amt = "0.00";
                    // 显示支付金额
                }
                valueList.add(amt);
                amount = null;
            }

        } else if (getMode() == 2) {
            // 退货不显示

        } else {
            // fixed show item defined in configration file,
            // dynamic show item: salesAmount, taxAmount, payment, spillAmount, si,
            // if it exist
            try {
                for (int m = 0; m < fields.size(); m++) {
                    String field = fields.get(m);
                    if (field.equals("SI")) {
                        for (SI si : trans.getAppliedSIs().keySet()) {
                            HYIDouble siAmount = trans.getAppliedSIs().get(si);
                            titleList.add(chineseConverter.convert(si.getScreenName()));
                            valueList.add(siAmount.toString());
                        }

                    // check if "payment"
                    } else if (field.equals("payment")) {
                        for (int i = 1; i <= 4 /*trans.getPaymentCount()*/; i++) {
                            Payment payment = trans.getPaymentByID(i);
                            if (payment != null) {
                                String payName = payment.getScreenName();
                                HYIDouble payAmount = trans.getPayAmountByID(i).setScale(2, 4);
                                valueList.add(payAmount.toString());
                                titleList.add(payName);

                                String cardNo = null;
                                String cardName = null;
                                if (payment.isRecordingCardNumber()) {
                                    cardNo = trans.getPayCardNumber();
                                    cardName = CreamToolkit.getString("CardNumber");
                                } else if (payment.isCreditCard()) {
                                    cardNo = trans.getCreditCardNumber();
                                    if (!StringUtils.isEmpty(cardNo)) {
                                        CreditType ct = CreditType.checkCreditNo(cardNo);
                                        if (ct == null)
                                            cardName = CreamToolkit.getString("CardNumber");
                                        else
                                            cardName  = ct.getNoName();
                                    }
                                }
                                if (!StringUtils.isEmpty(cardNo)) {
                                    valueList.add(cardNo);
                                    titleList.add(chineseConverter.convert(cardName));
                                }
                            }
                        }

                    } else {
                        // check daishou and daifu amount
                        if (field.equals("DaiShouAmount") || field.equals("DaiFuAmount")) {
                            if (trans.getDealType2() != null
                                    && (trans.getDealType2().equals("4"))) {
                                continue;
                            }
                        }

                        // check cashin, cashout and return transaction's
                        // agelevel
                        if (field.equals("CustomerAgeLevel")) {
                            if (trans.getDealType2() != null
                                && (trans.getDealType2().equals("1")
                                    || trans.getDealType2().equals("2")
                                    || trans.getDealType2().equals("3")
                                    || trans.getDealType2().equals("4")
                                    || trans.getDealType2().equals("G")
                                    || trans.getDealType2().equals("H")
                                    || trans.getDealType2().equals("100"))) {
                                continue;
                            }
                        }
                        Method method = Transaction.class.getDeclaredMethod(
                                "get" + field, new Class[0]);
                        // Font shouldPay = null;
                        // int unitWidth1 = 0;
                        Object result = method.invoke(trans, new Object[0]);
                        if (result instanceof HYIDouble) {
                            HYIDouble bd = ((HYIDouble) result).setScale(2);
                            if (bd == null
                                    || bd.compareTo(new HYIDouble(0)) == 0) {
                                if (((String) types.get(m)).equals("y")) {
                                    strObj = "0.00";
                                    valueList.add(strObj);
                                    strObj = chineseConverter.convert(headers.get(m).toString());
                                    titleList.add(strObj);
                                }
                            } else {
                                if (field.equals("TotalMMAmount")) {
                                    bd = bd.negate();
                                }
                                strObj = bd.toString();
                                valueList.add(strObj);
                                strObj = chineseConverter.convert(headers.get(m).toString());
                                titleList.add(strObj);
                            }
                        } else if (result instanceof Integer) {
                            Integer bd = (Integer) result;//
                            if (bd == null
                                    || bd.compareTo(new Integer(-1)) == 0) {
                                if (((String) types.get(m)).equals("y")) {
                                    strObj = "0";
                                    valueList.add(strObj);
                                    strObj = chineseConverter.convert(headers.get(m).toString());
                                    titleList.add(strObj);
                                }
                            } else {
                                // CustomerAgeLevel
                                if (field.equals("CustomerAgeLevel")) {
                                    if (ageLevel.containsKey(bd.toString()))
                                        strObj = ageLevel.getProperty(bd
                                                .toString());
                                    else
                                        strObj = "unknown";
                                } else
                                    strObj = bd.toString();
                                valueList.add(strObj);
                                strObj = chineseConverter.convert(headers.get(m).toString());
                                titleList.add(strObj);
                            }
                        }
                    }
                }

                og.setColor(Color.black);
                java.util.List list = processBounds(titleList, valueList, og);
                for (int i = 0; i < titleList.size(); i++) {
                    java.util.List list1 = (java.util.List) list.get(i);
                    int startx1 = ((Integer) list1.get(0)).intValue();
                    int starty1 = ((Integer) list1.get(1)).intValue();
                    int startx2 = ((Integer) list1.get(2)).intValue();
                    int starty2 = ((Integer) list1.get(3)).intValue();
                    og.setFont(fh);
                    fm = og.getFontMetrics(fh);
                    og.drawString(titleList.get(i).toString() + ":", startx1,
                            starty1 + fm.getAscent());
                    og.setFont(fv);
                    fm = og.getFontMetrics(fv);
                    og.drawString(valueList.get(i).toString(), startx2, starty2
                            + fm.getAscent());
//                    if (!cardNoList.isEmpty() && cardNoList.get(i) != null)
//                        og.drawString(cardNoList.get(i).toString(), startx2, starty2
//                                + 100);
                }

            } catch (Exception e) {
                CreamToolkit.logMessage(e);
            }
        }
        g.drawImage(offscreen, 0, 0, null);
        og.dispose();

        // // When payingpane2 finished its paint, notify EPOSBackground to
        // paint.
        // // Ref. POSTerminalApplication.setPayingPaneVisible()
        // if (propFile.toString().indexOf("payingpane2") != -1) {
        // synchronized (EPOSBackground.getInstance().waitForDrawing) {
        // EPOSBackground.getInstance().waitForDrawing.notifyAll();
        // }
        // }
    }

    // initialize paper number
    public void clear() {
        paperNo = 0;
        paperMax = 0;
    }

    public void addActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.add(actionListener, listener);
        enableEvents(AWTEvent.MOUSE_EVENT_MASK);
    }

    public void removeActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.remove(actionListener, listener);
    }

    /*
     * public void processMouseEvent (MouseEvent e) { switch (e.getID()) { case
     * MouseEvent.MOUSE_PRESSED:
     *  // previous paper button if (e.getX() > 123 && e.getX() < 203 &&
     * e.getY() > 390 && e.getY() < 410) { if (paperNo <= 0) { return; }
     * paperNo--; repaint();
     *  // next paper button } else if (e.getX() > 214 && e.getX() < 284 &&
     * e.getY() > 390 && e.getY() < 410) { if (paperNo >= paperMax) { return; }
     * paperNo++; repaint(); } break;
     *
     * case MouseEvent.MOUSE_RELEASED: break;
     *
     * case MouseEvent.MOUSE_ENTERED: break;
     *
     * case MouseEvent.MOUSE_EXITED: break; } super.processMouseEvent(e); }
     */

    private java.util.List processBounds(java.util.List titleList,
            java.util.List valueList, Graphics og) {
        int marginW = 10;
        int marginH = 0;
        int leftMargin = 0;
        java.util.List list = new Vector();
        if (titleList.size() != valueList.size())
            return list;
        int maxHead = 0;
        int maxValue = 0;
        FontMetrics fm = null;
        fm = og.getFontMetrics(fh);
        int fontHeightH = fm.getHeight();
        fm = og.getFontMetrics(fv);
        int fontHeightV = fm.getHeight();
        if (fontHeightH != fontHeightV)
            marginH = 5;
        for (int i = 0; i < titleList.size(); i++) {
            fm = og.getFontMetrics(fh);
            int length = fm.stringWidth(titleList.get(i).toString() + ":");
            if (length > maxHead)
                maxHead = length;
            fm = og.getFontMetrics(fv);
            length = fm.stringWidth(valueList.get(i).toString());
            if (length > maxValue)
                maxValue = length;
        }
        if (getWidth() > maxHead + maxValue + marginW) {
            leftMargin = Math
                    .round((getWidth() - maxHead - maxValue - marginW) / 2 - 0.5f);
            marginW = getWidth() - maxHead - maxValue - 2 * leftMargin;
        } else if (getWidth() > maxHead + maxValue) {
            leftMargin = Math
                    .round((getWidth() - maxHead - maxValue - marginW) / 2 - 0.5f);
            marginW = getWidth() - maxHead - maxValue - 2 * leftMargin;
        }
        // int rightHead = leftMargin + maxHead;
        // int rightValue = getWidth() - leftMargin;

        int h = marginH;
        for (int i = 0; i < titleList.size(); i++) {
            java.util.List list1 = new Vector();
            if (i > 0)
                h += fontHeightV;
            fm = og.getFontMetrics(fh);
            int w = maxHead + leftMargin
                    - fm.stringWidth(titleList.get(i).toString() + ":");
            list1.add(new Integer(w));
            list1.add(new Integer(h + (fontHeightV - fontHeightH) / 2));
            fm = og.getFontMetrics(fv);
            w = getWidth() - leftMargin
                    - fm.stringWidth(valueList.get(i).toString());
            list1.add(new Integer(w));
            list1.add(new Integer(h));
            list.add(list1);
        }
        return list;
    }

    public void systemInfoChanged(SystemInfoEvent e) {
        repaint();
    }
}
