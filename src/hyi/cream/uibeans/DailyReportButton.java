package hyi.cream.uibeans;

/**
 * 营业日报键.
 */
public class DailyReportButton extends POSButton {

    /**
     * Constructor.
     * 
     * @param row row position.
     * @param column column position.
     * @param clearLabel clear label on button.
     */
    public DailyReportButton(int row, int column, int level, String clearLabel) {
        super(row, column, level, clearLabel);
    }

    public DailyReportButton(int row, int column, int level, String clearLabel, int keyCode) {
        super(row, column, level, clearLabel, keyCode);
    }
}