
// Copyright (c) 2000 hyi
package hyi.cream.uibeans;

import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PageDownButton extends POSButton {   

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
    public PageDownButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
     * @param keyCode key code
    */
    public PageDownButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}

 
