package hyi.cream.uibeans;

import hyi.cream.POSButtonHome2;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.event.POSButtonListener;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;

public class POSButton extends Component implements ActionListener/*, DataListener*/ {
    private static final long serialVersionUID = 1L;

    protected int row;
    protected int column;
    protected int level;
    protected String label;
    private ArrayList pl = new ArrayList();
//    private ArrayList kl = new ArrayList();
    private ScreenButton sb;
    protected int keyCode;

    //static POSKeyboard posKeyboard           = null;

    /**
     * Default constructor.
     */
    public POSButton() {
    }
    
    public POSButton(int row, int column, int level, String label, int keyCode) {
        this.row = row;
		this.column = column;
		this.level = level;
        this.label = label;
        this.keyCode = keyCode;
    }

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param label button label.
     */
	/*public POSButton(int row, int column, String label) {
        this.row = row;
		this.column = column;
		this.level = level;
        this.label = label;
	}*/

	public POSButton(int row, int column, int level, String label) {
        this.row = row;
		this.column = column;
		this.level = level;
		this.label = label;
	}

    /**
     * 默認相同的類型的button就視為equals.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        String name = getClass().getSimpleName();
        if (name.endsWith("Button"))
            name = name.substring(0, name.length() - 6);
        return name + "," + paramString();
    }

    @Override
    protected String paramString() {
        return "";
    }

    /**
     * Let it be a screen button.
     * <P>It'll first create a ScreenButton, and register "this" as an action
     * listener of the ScreenButton.
     * @param imageFile image filename for the screen button.
     */
	public void becomeScreenButton(String imageFile, String layer, String r, String g, String b)
		throws FileNotFoundException {
		Color c = null;
		if (imageFile != null) {
			if (imageFile.equals("")) {
				c = new Color (Integer.parseInt(r), Integer.parseInt(g), Integer.parseInt(b));
				sb = new ScreenButton(row, column, label, c, Integer.parseInt(layer));
			} else {
				sb = new ScreenButton(row, column, label, imageFile, Integer.parseInt(layer));
			}
		}
	    sb.addActionListener(this);
	}
    
    /**
     * Let it be a screen button.
     * <P>It'll first create a ScreenButton, and register "this" as an action
     * listener of the ScreenButton.
     * @param imageFile image filename for the screen button.
     */
	public void becomeTouchButton(String imageFile, String layer, String r, String g, String b)
		throws FileNotFoundException {
		Color c = null;
		if (imageFile != null) {
			if (imageFile.equals("")) {
				c = new Color (Integer.parseInt(r), Integer.parseInt(g), Integer.parseInt(b));
				sb = new ScreenButton(row, column, label, c, Integer.parseInt(layer));
			} else {
				sb = new ScreenButton(row, column, label, imageFile, Integer.parseInt(layer));
			}
		}
	    sb.addActionListener(this);
	}
    
//    /**
//     * Let it be a keyboard button.
//     * <P>It'll first check to see if there exists a POSKeyboard. if not,
//     * create one, and then register "this" as an data listener of the
//     * POSKeyboard.
//     * @param keyCode the key code.
//     */
//    public void becomeKeyboardButton() {
//        if (posKeyboard == null) {
//            try {
//                posKeyboard = POSPeripheralHome.getInstance().getPOSKeyboard();
//                posKeyboard.claim(0);
//                posKeyboard.setDeviceEnabled(true);
//                posKeyboard.setDataEventEnabled(true);
//            } catch (Exception e) {
//                CreamToolkit.logMessage(e.getMessage());
//            }
//        }
//        posKeyboard.addDataListener(this);
//    }

    /**
     * When user presses a screen button, it comes a action event. Then it
     * fires POSButtonEvent to POSButtonListener.
     */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof ScreenButton) {
			ScreenButton sb =  (ScreenButton)e.getSource();
			if (this instanceof PluButton) {
				PluButton p = (PluButton)this;
				p.setPluColor(sb.getColor());
			}
		}
		firePOSButtonEvent(new POSButtonEvent(this));
    }

//    /**
//     * When user presses a keyboard button, it comes a JavaPOS's data event.
//     * If it is matched with the key code, then it fires POSButtonEvent to
//     * POSButtonListener.
//     */
//	public void dataOccurred(DataEvent e) {
//        try {
//            POSKeyboard p = (POSKeyboard)e.getSource();
//			if (getKeyCode() == p.getPOSKeyData()) {
//			    firePOSButtonEvent(new POSButtonEvent(this));
//            }
//        } catch (JposException ex) {
//            CreamToolkit.logMessage(e.toString());
//            CreamToolkit.logMessage("Jpos exception at " + this);
//        }
//    }

//    /**
//     * Add a POSButtonListener.
//     */
//	public void addPOSButtonListener(POSButtonListener l) {
//	     pl.add(l);
//	}

    //Object mutex = new Object();
    public void firePOSButtonEvent(POSButtonEvent e) {
        POSButtonHome2.getInstance().buttonPressed(e);
        // Iterator iter = pl.iterator();
        // while (iter.hasNext()) {
        // POSButtonListener l = (POSButtonListener)iter.next();
        // l.buttonPressed(e);
        //        }
    }

    public void setKeyCode(int keyCode) {
        this.keyCode = keyCode;
    }

    public int getKeyCode() {
        return keyCode;
    }

    /**
     * Setter of property "Label."
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Getter of property "Label."
     */
    public String getLabel() {
        return label;
    }

    /**
     * Setter of property "RowPosition."
     */
    public void setRowPosition(int row) {
        this.row = row;
    }

    /**
     * Getter of property "RowPosition."
     */
    public int getRowPosition() {
        return row;
    }

    /**
     * Setter of property "ColumnPosition."
     */
    public void setColumnPosition(int col) {
        column = col;
    }

    /**
     * Getter of property "ColumnPosition."
     */
    public int getColumnPosition() {
        return column;
    }
    
    public Dimension getPreferredSize() {
        return new Dimension(56, 43);
    }

    public Dimension getMinimumSize() {
        return new Dimension(56, 43);
    }
    
    public int getLevel() {
    	return level;
    }
}

