package hyi.cream.uibeans;

import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.*;

import hyi.cream.state.GetProperty;
import hyi.cream.util.*;
import hyi.cream.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * 支付Menu键
 */
public class PaymentMenuButton extends POSButton implements ActionListener {
    private String defFilename;
    private PopupMenuPane popupMenu;
    private ArrayList menuArrayList;

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param payMenuLabel pay menu label on button.
     * @param defFilename definition file of the pay menu.
     */
	public PaymentMenuButton(int row, int column, int level, String paymentMenuLabel, String defFilename) {
        super(row, column, level, paymentMenuLabel);
        this.defFilename = defFilename;
        setDefFile(defFilename);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PaymentMenuButton that = (PaymentMenuButton)o;
        if (defFilename != null ? !defFilename.equals(that.defFilename) : that.defFilename != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (defFilename != null ? defFilename.hashCode() : 0);
        result = 31 * result + (popupMenu != null ? popupMenu.hashCode() : 0);
        result = 31 * result + (menuArrayList != null ? menuArrayList.hashCode() : 0);
        return result;
    }

    @Override
    protected String paramString() {
        return defFilename;
    }

    //  read message defined in configration file
    public void setDefFile(String defFilename) {
        this.defFilename = defFilename;
        File propFile = new File(defFilename);
        try {
            FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = new InputStreamReader(filein,  GetProperty.getConfFileLocale());
            BufferedReader in = new BufferedReader(inst);
            String line;
            char ch = ' ';
            int i;

            do {
                do {
                    line = in.readLine();
                    if (line == null) {
                        break;
                    }
                    while (line.equals("")) {
                        line = in.readLine();
                    }
                    i = 0;
                    do {
                        ch = line.charAt(i);
                        i++;
                    } while ((ch == ' ' || ch == '\t')&& i < line.length());
                } while (ch == '#' || ch == ' ' || ch == '\t');

                if (line == null) {
                    break;
                }
                menuArrayList.add(line);
            } while (true);
        } catch (FileNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("File not found: " + propFile + ", at " + this);
        } catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("IO exception at " + this);
        }
    }

    public void actionPerformed(ActionEvent e) {
        popupMenu = PopupMenuPane.getInstance();
        popupMenu.setMenu(menuArrayList);
        popupMenu.setBounds(215, 240, 293, 332);
        //!!!!!Bruce
        //popupMenu.setModal(true);
        popupMenu.setVisible(true);
    }
}

