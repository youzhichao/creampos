package hyi.cream.uibeans;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.awt.*;
import java.text.*;
import hyi.cream.event.*;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.*;
import hyi.cream.groovydac.Param;
import hyi.cream.state.GetProperty;
import hyi.cream.util.*;
import hyi.cream.inline.*;

public class TotalAmountBanner extends Canvas implements SystemInfoListener {

    private String amountPainted = "";
    private String mmAmountPainted = "";
    private String salesAmountPainted = "";
    private int offScreenH;
    private int offScreenW;
    private SystemInfo systemInfo;
    //private String backImage           = "screenbanner.jpg";
    //Image ig = CreamToolkit.getImage(backImage);
    private Image offscreen;
    private Color headerColor = Color.white;
    private String fontName;
    private int fontSize = 16;
    private Color backgroundColor = Color.black;
    private Font f1;
    private Font f2;
    private boolean invalid = true;

    public TotalAmountBanner(File propFile, Color bgColor, Color valueColor, Color headerColor)
        throws ConfigurationNotFoundException {
        this(propFile);
        this.backgroundColor = bgColor;
        setBackground(backgroundColor);
        this.headerColor = headerColor;
    }

    public TotalAmountBanner(File propFile) throws ConfigurationNotFoundException {
        char ch = '#';
        try {
            //FileInputStream filein = new FileInputStream(propFile);
            //InputStreamReader inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
            //BufferedReader in = new BufferedReader(inst);

            ConfReader in = new ConfReader(propFile);
            int i = 0;
            String line = "";
            boolean getLine = false;
            while ((line = in.readLine()) != null) {
                getLine = true;
                do {
                    if (!getLine)
                        line = in.readLine();
                    getLine = false;
                    while (line.equals("")) {
                        line = in.readLine();
                    }
                    i = 0;
                    do {
                        ch = line.charAt(i);
                        i++;
                    } while ((ch == ' ' || ch == '\t') && i < line.length());
                }
                while (ch == '#' || ch == ' ' || ch == '\t');

                String s = "";
                if (line.startsWith("font")) {
                    StringTokenizer t0 = new StringTokenizer(line, ",", true);
                    while (t0.hasMoreTokens()) {
                        s = t0.nextToken();
                        if (s.startsWith("fontName")) {
                            fontName = s.substring("fontName".length() + 1, s.length());

                            // Fix font for simplified Chinese
                            if (!Param.getInstance().tranditionalChinese() && fontName.equals("cwTeXHeiBold"))
                                fontName = "SimHei";

                        } else if (s.startsWith("fontSize")) {
                            s = s.substring("fontSize".length() + 1, s.length());
                            try {
                                fontSize = Integer.parseInt(s);
                            } catch (Exception e) {
                                fontSize = 16;
                            }
                        }
                    }
                    f1 = new Font(fontName, Font.PLAIN, fontSize);
                    f2 = new Font(fontName, Font.BOLD, fontSize);
                }
            }
        } catch (NoSuchElementException e) {
            CreamToolkit.logMessage("Format error: " + propFile);
        }
    }

    /**
     * Sets the associated transaction object. It'll also register itself
     * as the system information listener of the system information object.
     * 
     ** @param systemInfo the sytem information object
     */
    public void setSystemInfo(SystemInfo systemInfo) {
        this.systemInfo = systemInfo;
    }

    /**
     * Returns the associated transaction object.
     */
    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    /**
     * Invoked when transaction has been changed.
     * 
     * @param e an event object represents the changes.
     */
    public void systemInfoChanged(SystemInfoEvent e) {
        repaint();
    }

    public void update(Graphics g) {
        paint(g);
    }

    public void paint(Graphics g) {
        if (getWidth() <= 0 || getHeight() <= 0) {
            EPOSBackground.getInstance().repaint();
            return;
        }

        ChineseConverter chineseConverter = ChineseConverter.getInstance();
        int startx = 5;
        int starty = 0;
        //int width = getWidth() - 10;
        int marginy = 3;

        if (offscreen == null || offScreenW != getWidth() || offScreenH != getHeight()) {
            offscreen = createImage(getWidth(), getHeight());
            offScreenW = getWidth();
            offScreenH = getHeight();
            salesAmountPainted = "~";      // make it repaint, don't use buffered
        }

        Graphics og = offscreen.getGraphics();

        try {
            String salesAmount = systemInfo.getSalesAmount();
            String mmAmount = systemInfo.getTotoalMMAmount();
            String amount = systemInfo.getSubtotoalAmount();
            if (!invalid && salesAmount.equals(salesAmountPainted)
                && mmAmount.equals(mmAmountPainted)
                && amount.equals(amountPainted)) {
                g.drawImage(offscreen, 0, 0, this);
                og.dispose();
                return;
            } else {
                invalid = false;
                salesAmountPainted = salesAmount;
                mmAmountPainted = mmAmount;
                amountPainted = amount;
            }

            //((Graphics2D)og).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            ((Graphics2D)og).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            og.setColor(backgroundColor);
            og.fillRect(0, 0, getSize().width - 2, getSize().height - 2);

            //java.util.List printList = new Vector();
            FontMetrics fm1 = og.getFontMetrics(f1);
            FontMetrics fm2 = og.getFontMetrics(f2);

            og.setColor(headerColor);
            String salesAmountStr = CreamToolkit.GetResource().getString("AccountReceivable");

            og.setFont(f2);
            starty = getHeight() - marginy - fm2.getHeight();
            if (salesAmount.trim().equals("") || salesAmount.trim().equals("0"))
                salesAmount = "0.00";
            og.drawString(salesAmount,
            //startx + fm1.stringWidth(salesAmountStr) + 5,
            getWidth() - (startx + fm2.stringWidth(salesAmount) + 5), starty + fm2.getAscent() + 5);

            og.setFont(f1);
            starty = getHeight() - marginy - fm1.getHeight();
            og.drawString(salesAmountStr, startx, starty + fm2.getAscent() + 5);

            starty -= marginy;
            og.drawLine(0, starty, getWidth(), starty);

            String mmAmountStr = CreamToolkit.getString("Discount");
            starty -= marginy + fm1.getHeight();
            og.drawString(mmAmountStr, startx, starty + fm1.getAscent());
            if (mmAmount.trim().equals("") || mmAmount.trim().equals("0"))
                mmAmount = "0.00";
            og.drawString(mmAmount,
            //startx + fm1.stringWidth(mmAmountStr) + 5,
            getWidth() - (startx + fm1.stringWidth(mmAmount) + 5), starty + fm1.getAscent());

            String amountStr = CreamToolkit.getString("Subtotal");
            starty -= marginy + fm1.getHeight();
            og.drawString(amountStr, startx, starty + fm1.getAscent());
            if (amount.trim().equals("") || amount.trim().equals("0"))
                amount = "0.00";
            og.drawString(amount,
            //startx + fm1.stringWidth(amountStr) + 5,
            getWidth() - (startx + fm1.stringWidth(amount) + 5), starty + fm1.getAscent());

            g.drawImage(offscreen, 0, 0, this);
            og.dispose();
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }

//        //  for transaction event forward
//        if (isChanged) {
//            if (POSTerminalApplication.getInstance().getCurrentTransaction().decreaseLockCount() == 0) {
//                POSTerminalApplication.getInstance().getCurrentTransaction().setLockEnable(true);
//            }
//            isChanged = false;
//        }

        //        // When finishing its paint, notify EPOSBackground to paint.
        //        // Ref. POSTerminalApplication.setPayingPaneVisible()
        //        synchronized (EPOSBackground.getInstance().waitForDrawing) {
        //            EPOSBackground.getInstance().waitForDrawing.notifyAll();
        //            System.out.println("notifying..."+ this.toString());
        //        }

    }

    //    public void setVisible(boolean b) {
    //        super.setVisible(b);
    //        if (!b) {
    //            // When finishing its paint, notify EPOSBackground to paint.
    //            // Ref. POSTerminalApplication.setPayingPaneVisible()
    //            synchronized (EPOSBackground.getInstance().waitForDrawing) {
    //                EPOSBackground.getInstance().waitForDrawing.notifyAll();
    //                System.out.println("\nnotifying..." + this.toString());
    //            }
    //        }
    //    }

    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }
}
