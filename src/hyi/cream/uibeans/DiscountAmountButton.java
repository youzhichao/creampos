package hyi.cream.uibeans;

/**
 * 单品折扣金额键.
 */
public class DiscountAmountButton extends POSButton {

    /**
     * Constructor.
     * 
     * @param row row position.
     * @param column column position.
     * @param clearLabel clear label on button.
     */
    public DiscountAmountButton(int row, int column, int level, String clearLabel) {
        super(row, column, level, clearLabel);
    }

    public DiscountAmountButton(int row, int column, int level, String clearLabel, int keyCode) {
        super(row, column, level, clearLabel, keyCode);
    }
}