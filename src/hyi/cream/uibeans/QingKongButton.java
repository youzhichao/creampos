
package hyi.cream.uibeans;

/**
 * 清空键.
 */
public class QingKongButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param clearLabel clear label on button.
     */
	public QingKongButton(int row, int column, int level, String clearLabel) {
        super(row, column, level, clearLabel);
	}

	public QingKongButton(int row, int column, int level, String clearLabel, int keyCode) {
        super(row, column, level, clearLabel, keyCode);
	}

}

 