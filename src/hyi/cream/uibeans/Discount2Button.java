package hyi.cream.uibeans;

/**
 * 印花价二键.   ②
 */
public class Discount2Button extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
	public Discount2Button(int row, int column, int level,  String label) {
        super(row, column, level, label);
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param numberLabel a string represents the button label
     * @param keyCode key code
    */
	public Discount2Button(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}

