package hyi.cream.uibeans;

import java.util.*;
import java.awt.*;
import java.awt.event.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;     

//import jpos.*;
//import jpos.events.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class TransactionVoidButton extends POSButton /*implements ActionListener,
            KeyListener*/ {
    //private TextField tf = null;
    //private Dialog d = null;
    //private int pwdLength = 0;
    //private String password = "";
    //private int level;
    //private boolean already = false;
    //private ResourceBundle res = CreamToolkit.GetResource();

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
    public TransactionVoidButton(int row, int column, int level, String label) {
        super(row, column, level, label);
        //this.level = level;
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param numberLabel a string represents the button label
     * @param keyCode key code
    */
    public TransactionVoidButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode); 
        //this.level = level;
    }           

    /**
     * When user presses a keyboard button, it comes a JavaPOS's data event.
     * If it is matched with the key code, then it fires POSButtonEvent to
     * POSButtonListener.
     */
	/*public void dataOccurred(DataEvent e) {
        try {
            PINPad p = (PINPad)e.getSource();
			if (getKeyCode() == p.getPrompt()) {
                
                if (getAlready()) {
                    firePOSButtonEvent(new POSButtonEvent(this));
                    return;
                }

                POSTerminalApplication app = POSTerminalApplication.getInstance();
                d = new Dialog(new Frame(), true);
                tf = new TextField(res.getString("Password"));
                tf.addKeyListener(this);

                //center display
                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                int x = (screenSize.width / 2) - (200 / 2);
                int y = (screenSize.height / 2) - (50 / 2);
                d.setBounds(x, y, 200, 50);

                d.add(tf);
                d.setVisible(true);

                //check password
                Password pwd = Password.queryByLevel(level);
                String passwordKey = "PASSWORD";
                String passwordValue = "";
                boolean passwordRight = false;
                for (int i = 0; i < 10; i++) {
                    passwordKey = passwordKey + i;
                    passwordValue = (String)pwd.getFieldValue(passwordKey);
                    if (passwordValue != null &&
                        passwordValue.equalsIgnoreCase(password)) {
                        passwordRight = true;
                        break;
                    }
                }
                if (passwordRight) {
                    firePOSButtonEvent(new POSButtonEvent(this));  
                } else {
                    app.getMessageIndicator().setMessage("PASSWORD IS WRONG");
                }
            }
        } catch (JposException ex) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Jpos exception at " + this);
        }
    }
    
    public void actionPerformed(ActionEvent e) {
        if (getAlready()) {    
            firePOSButtonEvent(new POSButtonEvent(this));
            return;
        }

        POSTerminalApplication app = POSTerminalApplication.getInstance();
        d = new Dialog(new Frame(), true);
        tf = new TextField(res.getString("Password"));
        tf.addKeyListener(this);

        //center display
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screenSize.width / 2) - (200 / 2);
        int y = (screenSize.height / 2) - (50 / 2);
        d.setBounds(x, y, 200, 50);
        
        d.add(tf);
        d.setVisible(true);

        //check password
        Password pwd = Password.queryByLevel(level);
        String passwordKey = "PASSWORD";
        String passwordValue = "";
        boolean passwordRight = false;
        for (int i = 0; i < 10; i++) {
            passwordKey = passwordKey + i;
            passwordValue = (String)pwd.getFieldValue(passwordKey);
            if (passwordValue != null &&
                passwordValue.equalsIgnoreCase(password)) {
                passwordRight = true;
                break;
            }
        }
        if (passwordRight) {
            firePOSButtonEvent(new POSButtonEvent(this));  
        } else {
            app.getMessageIndicator().setMessage("PASSWORD IS WRONG");
        }
    }

    public boolean getAlready() {
        return already;
    }

    public void setAlready(boolean already) {
        this.already = already;
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
        if (pwdLength == 0) {
            password = e.getKeyChar() + "";
            tf.setText("");           
            tf.setEchoChar('*');
        } else {
            password = password + e.getKeyChar();
        }
        pwdLength++;
        if (pwdLength == 6) {
            pwdLength = 0;
            d.setVisible(false);
        }
    }*/
}

