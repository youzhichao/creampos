package hyi.cream.uibeans;

/**
 * Button for screen capture.
 */
public class ScreenCaptureButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param label clear label on button.
     */
    public ScreenCaptureButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    public ScreenCaptureButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}