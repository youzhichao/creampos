package hyi.cream.uibeans;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.util.*;

import hyi.cream.state.GetProperty;
import hyi.cream.util.*;
import hyi.cream.*;
import hyi.cream.groovydac.Param;
import hyi.cream.uibeans.*;

public class ScreenButton extends Canvas {
	private int row;
	private int column;
	private int layer;
	private String label;
	private String imageFile;
	private String defaultImage = "screenbutton0.jpg";
	private Color buttonColor;
	private Color primitiveColor;
	private String tempImageFile;
	private String selectedImageFile;
    private ActionListener actionListener;
	private int x = 0;
	private int mouseMode = 1;
	private int displayMode = 0;
	private Color stringColor = new Color(255, 255, 10);
	private Color inverseStringColor = new Color(200, 200, 255);
	private Graphics graphic = null;

	public Color getColor() {
		if (primitiveColor != null)
			return primitiveColor;
		else
			return stringColor;
	}
	
	/*static private ArrayList tabButtons = new ArrayList();
	static {
		tabButtons.add("Tab1");
		tabButtons.add("Tab2");
		tabButtons.add("Tab3");
		tabButtons.add("Tab4");
		tabButtons.add("Tab5");
	}*/

    public ScreenButton() {
		imageFile = defaultImage;
        label = "";
	}

	public ScreenButton(String imageFile) {
		this.imageFile = imageFile;
		label = "";
	}


    // add to button panel when construct
    public ScreenButton(int row, int column, String label, String imageFile) {
        this.imageFile = imageFile;
        tempImageFile = imageFile;
        this.label = label;
        this.row = row;
		this.column = column;
		ButtonPanel buttonPanel = ButtonPanel.getInstance();
        buttonPanel.add(row, column, this);
	}

	public ScreenButton(int row, int column, String label, String imageFile, int layer) {
		this.imageFile = imageFile;
		tempImageFile = imageFile;
		this.label = label;
		this.row = row;
		this.column = column;
		this.layer = layer;
        ButtonPanel buttonPanel = ButtonPanel.getInstance();
        buttonPanel.add(row, column, layer, this);
	}

	public ScreenButton(int row, int column, String label, Color buttonColor, int layer) {
		this.imageFile = "";//imageFile;
		tempImageFile = ""; //imageFile;
		this.label = label;
		this.buttonColor = buttonColor;
		primitiveColor = buttonColor;
		this.row = row;
		this.column = column;
		this.layer = layer;
		displayMode = 1;
        ButtonPanel buttonPanel = ButtonPanel.getInstance();
        buttonPanel.add(row, column, layer, this);
	}

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
    public String getImageFile() {
    	return imageFile;
    }

    public void setImageFile(String imageFile) {
    	this.imageFile = imageFile;
    }

    public void addActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.add(actionListener,listener);
        enableEvents(AWTEvent.MOUSE_EVENT_MASK);
    }

    public void removeActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.remove(actionListener,listener);
    }

    public Dimension getPreferredSize() {
		return new Dimension(51, 52);
	}

	public Dimension getMinimumSize() {
		return new Dimension(51, 52);
	}

	private void drawImage(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		double x = 2, y = 2, w = 46, h = 46;
		Ellipse2D e = new Ellipse2D.Double(x, y, w, h);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//GradientPaint gp = new GradientPaint(50, 50, Color.white, 95, 95, buttonColor, true);
		g2.setPaint(buttonColor);
		BasicStroke sk = new BasicStroke(4,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND);
		g2.setStroke(sk);
				
		if (mouseMode == 0) {
			e.setFrame(x, y + 1, w, h);
		} else
            e.setFrame(x, y, w, h);
		g2.draw(e);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}

	private void drawHead(Graphics g){
	    Graphics2D g2 = (Graphics2D)g;
		double x = 1, y = 1, w = 39, h = 39;
		Ellipse2D e = new Ellipse2D.Double(x, y, w, h);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//GradientPaint gp = new GradientPaint(50, 50, Color.white, 95, 95, buttonColor, true);
		g2.setPaint(buttonColor);
		BasicStroke sk = new BasicStroke(4,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND);
		g2.setStroke(sk);
				
		if (mouseMode == 0) {
			e.setFrame(x, y + 1, w, h);
		} else
            e.setFrame(x, y, w, h);
		//g2.draw(e);
		g2.fill(e);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}

	
	public void paint(Graphics g) {
		this.graphic = g;
		if (displayMode == 0) {
			Image image = CreamToolkit.getImage(imageFile);
			MediaTracker tracker = new MediaTracker(this);
			tracker.addImage(image, 1);
			try {
				tracker.waitForID(1);
			} catch(InterruptedException e) {
				CreamToolkit.logMessage(e.toString());
				CreamToolkit.logMessage("Interrupted exception at " + this);
			}
			g.drawImage(image, 0, 0, this);	
			//  draw label showed on button image
			//  if tempImageFile is not equal "screenbutton0.jpg",
			//  means this image file is user-defined,
			//  then return
			if (tempImageFile != defaultImage) {
				return;
			}
		} else {
			if (this.layer == -1)
			    drawHead(g);
			else
				drawImage(g);
		}

        //Draw String 		
        String fontName = Param.getInstance().getItemListFont(); //.getTouchButtonFont("");
        Font f = new Font(fontName, Font.BOLD, 14);
        FontMetrics fm = g.getFontMetrics(f);
        g.setFont(f);

        int w = getPreferredSize().width;
        int h = getPreferredSize().height;
        int length = label.length();

        while (fm.stringWidth(label.substring(0, length)) > (w - 12)) {
            length--;
		}
		if (displayMode == 1) {
			/*if (mouseMode == 0) {
				int b = 255 - stringColor.getBlue();
				int r = 255 - stringColor.getRed();
				int green = 255 - stringColor.getGreen();
				Color c = new Color(r, green ,b);
				g.setColor(c);
			} else
				g.setColor(stringColor);*/
			if (this.layer == -1) {
				int b = 255 - buttonColor.getBlue();
				int r = 255 - buttonColor.getRed();
				int green = 255 - buttonColor.getGreen();
				Color c = new Color(r, green ,b);
				g.setColor(c);
				//g.setColor(Color.cyan);
			} else
				//drawImage(g);
				g.setColor(buttonColor);
		} else {
			if (mouseMode == 0) {
				g.setColor(inverseStringColor);
			} else
				g.setColor(stringColor);
		}
		//  x: consistency with image and label between pressed and released
		if (length == (label.length())) {
			if (this.layer == -1)
				g.drawString(label, (w - 8 - fm.stringWidth(label)) / 2 + x/2,
								(h - 8 + fm.getAscent()) / 2 - 2 + x);
			else
				g.drawString(label, (w - fm.stringWidth(label)) / 2 + x/2,
								(h + fm.getAscent()) / 2 - 2 + x);

        } else {
            g.drawString(label.substring(0, length),
				(w - fm.stringWidth(label.substring(0, length))) / 2 - 2 + x/2,
                h / 2 - 2 + x);
            g.drawString(label.substring(length,label.length()),
                (w - fm.stringWidth(label.substring(length, label.length()))) / 2 - 1 + x/2,
                h / 2 + fm.getAscent() - 2 + x);
        }
    }

	public void processMouseEvent (MouseEvent e) {
        switch (e.getID()) {
			case MouseEvent.MOUSE_PRESSED:
				mouseMode = 0;
				if (displayMode == 0) {
					Thread t = new Thread() {
						public void run() {
							//  pressed image file name = "2" + released image ifle name
							if (!ScreenButton.this.imageFile.startsWith("2")
								&& !ScreenButton.this.imageFile.startsWith("3") ) {
								ScreenButton.this.tempImageFile = ScreenButton.this.imageFile;
								ScreenButton.this.imageFile = "2" + ScreenButton.this.imageFile;
							}
							if (ScreenButton.this.imageFile.startsWith("3")) {
								ScreenButton.this.tempImageFile = ScreenButton.this.imageFile;
								ScreenButton.this.imageFile = "2" + ScreenButton.this.imageFile.substring(1, ScreenButton.this.imageFile.length());

							}
							ScreenButton.this.x = 2;
							ScreenButton.this.repaint();
						}
					};
					t.start();					
				} else {
					Thread t = new Thread() {
						public void run() {
							//make inverse color
							int r = 255 - buttonColor.getRed();
							int b = 255 - buttonColor.getBlue();							
							int g = 255 - buttonColor.getGreen();
							buttonColor = new Color(r, g, b);
							ScreenButton.this.x = 2;
							ScreenButton.this.repaint();
						}
					};
					t.start();
				}
				break;
			case MouseEvent.MOUSE_RELEASED:
				if (mouseMode == 1) {
					return;
				} else {
					mouseMode = 1;
				}
				if (displayMode == 0) {
					Thread th = new Thread() {
						public void run() {
							ScreenButton.this.imageFile = ScreenButton.this.tempImageFile;
							ScreenButton.this.x = 0;
							ScreenButton.this.repaint();
							if (ScreenButton.this.actionListener != null) {
								 ScreenButton.this.actionListener.actionPerformed(new ActionEvent(
								 ScreenButton.this, ActionEvent.ACTION_PERFORMED, ScreenButton.this.label));
				            }
						}
					};
					th.start();				
				} else {
					Thread t = new Thread() {
						public void run() {
						    int b = 255 - buttonColor.getBlue();
							int r = 255 - buttonColor.getRed();
							int g = 255 - buttonColor.getGreen();
							buttonColor = new Color (r, g, b);
							ScreenButton.this.x = 0;
							ScreenButton.this.repaint();
							if (ScreenButton.this.actionListener != null) {
								 ScreenButton.this.actionListener.actionPerformed(new ActionEvent(
								 ScreenButton.this, ActionEvent.ACTION_PERFORMED, ScreenButton.this.label));
							}
						}
					};
					t.start();
				}
				break;
			case MouseEvent.MOUSE_ENTERED:
				break;
			case MouseEvent.MOUSE_EXITED:
				// if exit after pressed, then equals release 
				if (mouseMode == 1) {
					return;
				} else {
					mouseMode = 1;
				}
				if (displayMode == 0) {
					Thread thr = new Thread() {
						public void run() {
							ScreenButton.this.imageFile = ScreenButton.this.tempImageFile;
							ScreenButton.this.x = 0;
							ScreenButton.this.repaint();
						}
					};
					thr.start();
				} else {
	                Thread t = new Thread() {
						public void run() {
							int b = 255 - buttonColor.getBlue();
							int r = 255 - buttonColor.getRed();
							int g = 255 - buttonColor.getGreen();
							buttonColor = new Color (r, g, b);
							ScreenButton.this.repaint();
							ScreenButton.this.repaint();
						}
					};
					t.start();
				}
				break;
        }
        super.processMouseEvent(e);
    }

}



