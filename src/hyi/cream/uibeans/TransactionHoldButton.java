package hyi.cream.uibeans;

public class TransactionHoldButton extends POSButton {

	/**
	 * Constructor.
	 * 
	 * @param row
	 *            row position.
	 * @param column
	 *            column position.
	 * @param label
	 *            label on button.
	 */
	public TransactionHoldButton(int row, int column, int level, String label) {
		super(row, column, level, label);
	}

	public TransactionHoldButton(int row, int column, int level, String label,
			int keyCode) {
		super(row, column, level, label, keyCode);
	}
}
