package hyi.cream.uibeans;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.awt.*;
import java.text.*;

import hyi.cream.event.*;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.*;
import hyi.cream.groovydac.Param;
import hyi.cream.state.GetProperty;
import hyi.cream.util.*;
import hyi.cream.inline.*;

public class ProductInfoBanner extends Canvas implements SystemInfoListener {

    private String amountStringPainted = "";
    private String printStringPainted = "";
    private int offScreenH;
    private int offScreenW;
    private SystemInfo systemInfo;
    private Image offscreen;
    private Color headerColor = Color.white;
    private String fontName;
    private int fontSize;
    private Color backgroundColor = Color.black;
    private Font fv;
    private Font fh;
    private boolean antiAlias = true;
    private boolean invalid = true;

    public ProductInfoBanner(File propFile, Color bgColor, Color valueColor, Color headerColor)
        throws ConfigurationNotFoundException {
        this(propFile);
        this.backgroundColor = bgColor;
        setBackground(backgroundColor);
        this.headerColor = headerColor;
    }

    public ProductInfoBanner(File propFile) throws ConfigurationNotFoundException {
        try {
            char ch = '#';
    
            //FileInputStream filein = new FileInputStream(propFile);
            //InputStreamReader inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
            //BufferedReader in = new BufferedReader(inst);
            ConfReader in = new ConfReader(propFile);

            int i = 0;
            String line = "";
            boolean getLine = false;
            while ((line = in.readLine()) != null) {
                getLine = true;
                do {
                    if (!getLine)
                        line = in.readLine();
                    getLine = false;
                    while (line.equals("")) {
                        line = in.readLine();
                    }
                    i = 0;
                    do {
                        ch = line.charAt(i);
                        i++;
                    } while ((ch == ' ' || ch == '\t') && i < line.length());
                }
                while (ch == '#' || ch == ' ' || ch == '\t');

                String s = "";
                if (line.startsWith("font")) {
                    StringTokenizer t0 = new StringTokenizer(line, ",", true);
                    while (t0.hasMoreTokens()) {
                        s = t0.nextToken();
                        if (s.startsWith("fontName")) {
                            fontName = s.substring("fontName".length() + 1, s.length());

                            // Fix font for simplified Chinese
                            if (!Param.getInstance().tranditionalChinese() && fontName.equals("cwTeXHeiBold"))
                                fontName = "SimHei";

                        } else if (s.startsWith("fontSize")) {
                            s = s.substring("fontSize".length() + 1, s.length());
                            try {
                                fontSize = Integer.parseInt(s);
                            } catch (Exception e) {
                                fontSize = 16;
                            }
                        } else if (s.startsWith("antiAlias")) {
                            antiAlias = s.substring("antiAlias".length() + 1, s.length()).equalsIgnoreCase("yes");
                        }
                    }
                    fh = new Font(fontName, Font.PLAIN, fontSize);
                    fv = new Font(fontName, Font.BOLD, fontSize);
                }
            }
        } catch (NoSuchElementException e) {
            CreamToolkit.logMessage("Format error: " + propFile);
        }
    }

    /**
     * Sets the associated transaction object. It'll also register itself
     * as the system information listener of the system information object.
     * 
     ** @param systemInfo the sytem information object
     */
    public void setSystemInfo(SystemInfo systemInfo) {
        this.systemInfo = systemInfo;
    }

    /**
     * Returns the associated transaction object.
     */
    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    /**
     * Invoked when transaction has been changed.
     * 
     * @param e an event object represents the changes.
     */
    private boolean isChanged = false;
    public void systemInfoChanged(SystemInfoEvent e) {
        isChanged = true;
        repaint();
        //if (POSTerminalApplication.getInstance().getCurrentTransaction().decreaseLockCount() == 0) {
        //	POSTerminalApplication.getInstance().getCurrentTransaction().setLockEnable(true);
        //
    }

    public void update(Graphics g) {
        paint(g);
    }

    public void paint(Graphics g) {
        int startx = 5;
        int starty = 0;
        int width = getWidth() - 10;
        String printString;
        String amountString;
        ChineseConverter chineseConverter = ChineseConverter.getInstance();

        if (offscreen == null || offScreenW != getWidth() || offScreenH != getHeight()) {
            offscreen = createImage(getWidth(), getHeight());
            offScreenW = getWidth();
            offScreenH = getHeight();
            amountStringPainted = "~";      // make it repaint, don't use buffered
        }

        Graphics og = offscreen.getGraphics();

        try {
            java.util.List printList = new Vector();
            FontMetrics fmh = og.getFontMetrics(fh);
            FontMetrics fmv = og.getFontMetrics(fv);
            printString = chineseConverter.convert(systemInfo.getCurrentLineItemName());
            amountString = systemInfo.getCurrentLineItemAmount();

            if (!invalid && printString.equals(printStringPainted) && amountString.equals(amountStringPainted)) {
                g.drawImage(offscreen, 0, 0, this);
                og.dispose();
                //System.out.println("PAINT> paint buffered product info");
                return;
            } else {
                printStringPainted = printString;
                amountStringPainted = amountString;
                invalid = false;
            }

            Graphics2D g2 = (Graphics2D)og;
            if (antiAlias)
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            og.setColor(backgroundColor);
            og.fillRect(0, 0, getSize().width, getSize().height);

            // 超长处理
            String subPrintString = printString;
            while (subPrintString.length() > 0) {
                int stringLength = fmh.stringWidth(subPrintString);
                int stri = subPrintString.length();
                if (stringLength <= width && !subPrintString.equals("")) {
                    printList.add(subPrintString);
                    subPrintString = "";
                }
                String str = "";
                while (stringLength > width) {
                    str = subPrintString.substring(0, stri);
                    stringLength = fmh.stringWidth(str);
                    stri--;
                }
                if (!str.equals(""))
                    printList.add(str);
                if (stri != subPrintString.length() && !subPrintString.equals(""))
                    subPrintString = subPrintString.substring(stri + 1, subPrintString.length());
            }

            // 高的处理
            java.util.List marginList = new Vector();
            int marginLine = 2 * (printList.size() + 1) - 1;
            int line = printList.size() + 1;
            int itemNameHeight = fmh.getAscent();
            int amountStringHeight = fmv.getAscent();
            int margin = 0;
            int subHeight = getHeight() - itemNameHeight * printList.size() - amountStringHeight;
            if (line > 1) {
                while (subHeight < 0) {
                    printList.remove(printList.size() - 1);
                    subHeight = getHeight() - itemNameHeight * printList.size() - amountStringHeight;
                }
                if (subHeight > marginLine) {
                    margin = Math.round(subHeight / marginLine - 0.5f);
                    subHeight = subHeight - line * margin;
                }
                for (int i = 0; i < line; i++) {
                    int margin0 = margin;
                    if (subHeight > 0) {
                        margin0 = margin;
                        subHeight--;
                    }
                    marginList.add(new Integer(margin0));
                }
                //System.out.println("marginList : " + marginList);
                og.setColor(headerColor);
                og.setFont(fh);
                starty = ((Integer)marginList.get(0)).intValue();
                for (int i = 0; i < printList.size(); i++) {
                    og.drawString((String)printList.get(i), startx, starty + fmh.getAscent());
                    starty += ((Integer)marginList.get(i + 1)).intValue() + fmh.getAscent();
                }

                og.setFont(fv);
                startx = getWidth() - fmv.stringWidth(amountString) - 5;
                og.drawString(amountString, startx, starty + fmv.getAscent());
            }

            g.drawImage(offscreen, 0, 0, this);
            og.dispose();
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }

//        // for transaction event forward
//        if (isChanged) {
//            if (POSTerminalApplication.getInstance().getCurrentTransaction().decreaseLockCount() == 0) {
//                POSTerminalApplication.getInstance().getCurrentTransaction().setLockEnable(true);
//            }
//            isChanged = false;
//        }
    }

    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }
}
