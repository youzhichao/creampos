
package hyi.cream.uibeans;


/**
 * 单品折扣键(Item disount).
 */
public class ItemDiscountButton extends POSButton {
    private String ID = "";

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param siLabel SI label on button.
     * @param siID SI ID.
     */
	public ItemDiscountButton(int row, int column, int level, String siLabel, String ID) {
		super(row, column, level, siLabel);
        this.ID = ID;
    }
    
	public ItemDiscountButton(int row, int column, int level, String siLabel, int keyCode, String ID) {
		super(row, column, level, siLabel, keyCode);      
        this.ID = ID;
	}

    public String getID() {
        return ID;
    }

    @Override
    protected String paramString() {
        return ID;
    }
}


 
