
package hyi.cream.uibeans;

/**
 * 单品规格键.
 */
public class PluAttrButton extends POSButton {

    private String pluAttr   = "";

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param pluAttrLabel PLU attribute label on button.
     * @param pluAttr PLU attribute ID.
     */
    public PluAttrButton(int row, int column, int level, String pluAttrLabel, String pluAttr) {
        super(row, column, level, pluAttrLabel);
        this.pluAttr = pluAttr;
    }

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param pluAttrLabel PLU attribute label on button.
     * @param keyCode key code
     * @param pluAttr PLU attribute ID.
     */
    public PluAttrButton(int row, int column, int level, String pluAttrLabel,
                         int keyCode, String pluAttr) {
        super(row, column, level, pluAttrLabel, keyCode);
        this.pluAttr = pluAttr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PluAttrButton that = (PluAttrButton)o;
        if (pluAttr != null ? !pluAttr.equals(that.pluAttr) : that.pluAttr != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (pluAttr != null ? pluAttr.hashCode() : 0);
        return result;
    }

    @Override
    protected String paramString() {
        return pluAttr;
    }

    public String getPluAttr() {
        return pluAttr;
    }
}

 