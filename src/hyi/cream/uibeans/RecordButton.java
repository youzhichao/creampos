package hyi.cream.uibeans;

public class RecordButton extends POSButton {

	/**
	 * Constructor.
	 * 
	 * @param row
	 *            row position.
	 * @param column
	 *            column position.
	 * @param ageLevelLabel
	 *            age level label on button.
	 * @param ageID
	 *            age level ID.
	 */
	public RecordButton(int row, int column, int level, String diyLabel) {
		super(row, column, level, diyLabel);
	}

	public RecordButton(int row, int column, int level, String diyLabel,
			int keyCode) {
		super(row, column, level, diyLabel, keyCode);
	}
}
