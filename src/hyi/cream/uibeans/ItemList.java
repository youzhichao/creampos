package hyi.cream.uibeans;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.math.*;
import java.util.*;
import java.lang.reflect.*;
import hyi.cream.event.*;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.*;
import hyi.cream.groovydac.Param;
import hyi.cream.state.GetProperty;
import hyi.cream.util.*;
import hyi.cream.dac.*;
import org.apache.commons.lang.StringUtils;


public class ItemList extends Canvas implements TransactionListener {

    private int offScreenH;
    private int offScreenW;
    private POSTerminalApplication app     = POSTerminalApplication.getInstance();
    private ArrayList colWidths            = new ArrayList();
    private ArrayList columnHeaders        = new ArrayList();
    private ArrayList columnAlignments     = new ArrayList();
    private ArrayList itemHeaders          = new ArrayList();
    private int itemNo                     = 0;
    private ArrayList selectStatu    = new ArrayList();
    private boolean pressed1               = false;
    private boolean pressed2               = false;
    private ActionListener actionListener  = null;
    private Image offscreen                = null;
    private Graphics og                    = null;
    //private Graphics2D g2                = null;
    int maxLineInPage                      = 10;
    private String fontName                = Param.getInstance().getItemListFont();

    private Color gridColor                = Color.black;
    private Color defaultColor             = Color.black; //new Color(0, 0, 0);
    private Color removedColor             = new Color(155, 200, 255);
    private Dimension size                 = null;
    public Transaction trans        = null;
    private int itemIndex            = 0;
    private int itemAccount          = 0;
    private int selectionMode              = 0;
    private boolean selected               = false;
    private Object[] lineItemsArray        = null;

    static final public int SINGLE_SELECTION = 0;
    static final public int MULTIPLE_SELECTION = 1;

    //private int selectedItem               = 0;
    private Color backgroundColor = new Color(165, 178, 255);

    private HYIDouble subTotal            = new HYIDouble(0);
    private int fontSize;
    private Font headerFont;
    private Font font;
    private boolean antiAlias = true;
    private boolean showAmountWithScale2;
    private String backLabel; // 背景大标题字符

    /**
     * When constructing an ItemList object, it'll first read the configuration
     * file "itemlist.conf," which defines all the properties of ItemList,
     * including headers, column widths, colum alignments, and fields.
     * The fields are the properties of the associated LineItem objects.
     */
    public ItemList() throws ConfigurationNotFoundException {
        char ch = ' ';
        File propFile = CreamToolkit.getConfigurationFile(ItemList.class);
        try {
            //FileInputStream filein = new FileInputStream(propFile);
            //InputStreamReader inst = null;
            //inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
            //BufferedReader in = new BufferedReader(inst);
            ConfReader in = new ConfReader(propFile);

            String line = "";
            int i;
            do {
                line = in.readLine();
                while (line.equals("")) {
                    line = in.readLine();
                }
                i = 0;
                do {
                    ch = line.charAt(i);
                    i++;
                } while ((ch == ' ' || ch == '\t')&& i < line.length());
            } while (ch == '#' || ch == ' ' || ch == '\t');

            String s = "";
            if (line.startsWith("font")) {
                StringTokenizer t0 = new StringTokenizer(line, ",", true);
                while (t0.hasMoreTokens()) {
                    s = t0.nextToken();
                    if (s.startsWith("fontName")) {
                        fontName = s.substring("fontName".length() + 1, s.length());

                        // Fix font for simplified Chinese
                        if (!Param.getInstance().tranditionalChinese() && fontName.equals("cwTeXHeiBold"))
                            fontName = "SimHei";

                    } else if (s.startsWith("fontSize")) {
                        try {
                            s = s.substring("fontSize".length() + 1, s.length());
                            fontSize = Integer.parseInt(s);
                        } catch (Exception e) {
                            fontSize = 18;
                        }
                    } else if (s.startsWith("antiAlias")) {
                        antiAlias = s.substring("antiAlias".length() + 1, s.length()).equalsIgnoreCase("yes");
                    }
                }
                font = new Font(fontName, Font.PLAIN, fontSize);
                headerFont = new Font(fontName, Font.PLAIN, fontSize + 1);
                do {
                    line = in.readLine();
                    while (line.equals("")) {
                        line = in.readLine();
                    }
                    i = 0;
                    do {
                        ch = line.charAt(i);
                        i++;
                    } while ((ch == ' ' || ch == '\t')&& i < line.length());
                } while (ch == '#' || ch == ' ' || ch == '\t');
            }

            StringTokenizer t1 = new StringTokenizer(line, ",");
            while (t1.hasMoreTokens()) {
                columnHeaders.add(t1.nextToken());
            }

            do {
                line = in.readLine();
                while (line.equals("")) {
                    line = in.readLine();
                }
                i = 0;
                do {
                    ch = line.charAt(i);
                    i++;
                } while ((ch == ' ' || ch == '\t')&& i < line.length());
            } while (ch == '#' || ch == ' ' || ch == '\t');

            StringTokenizer t2 = new StringTokenizer(line, ",");
            while (t2.hasMoreTokens()) {
                colWidths.add(t2.nextToken());
            }
            do {
                line = in.readLine();
                while (line.equals("")) {
                    line = in.readLine();
                }
                i = 0;
                do {
                    ch = line.charAt(i);
                    i++;
                } while ((ch == ' ' || ch == '\t')&& i < line.length());
            } while (ch == '#' || ch == ' ' || ch == '\t');

            StringTokenizer t3 = new StringTokenizer(line, ",");
            while (t3.hasMoreTokens()) {
                columnAlignments.add(t3.nextToken());
            }
            do {
                line = in.readLine();
                while (line.equals("")) {
                    line = in.readLine();
                }
                i = 0;
                do {
                    ch = line.charAt(i);
                    i++;
                } while ((ch == ' ' || ch == '\t')&& i < line.length());
            } while (ch == '#' || ch == ' ' || ch == '\t');

            StringTokenizer t4 = new StringTokenizer(line, ",");
            while (t4.hasMoreTokens()) {
                itemHeaders.add(t4.nextToken());
            }

            in.close();
        } catch (NoSuchElementException e) {
            CreamToolkit.logMessage("Format error: " + propFile);
        }

        //Bruce/2003-12-01
        //增加一个property "ShowAmountWithScale2" (default is "no"), 表示销售画面
        //是否金额要显示两位小数。
        showAmountWithScale2 = Param.getInstance().isShowAmountWithScale2();
    }

    public void setSelectedItem(int itemNo) {
        int sq = itemNo;
        Integer sel = new Integer(sq - 1);
        if (selectStatu.contains(sel)) {
            selectStatu.remove(sel);
            if (selectionMode == SINGLE_SELECTION) {
                selected = false;
            }
        } else {
            if (selectionMode == SINGLE_SELECTION && selected) {
                selectStatu.clear();
                selectStatu.add(sel);
                //selectedItem = sq - 1;
                selected = true;
            } else {
                selectStatu.add(sel);
                //if (selectionMode == SINGLE_SELECTION) {
                    //selectedItem = sq - 1;
                //}
                selected = true;
            }
        }
        repaint();
    }

    public boolean keyDataListener(int prompt) {
        int pageUpCode = app.getPOSButtonHome().getPageUpCode();
        int pageDownCode = app.getPOSButtonHome().getPageDownCode();
        if (prompt == pageUpCode) {
            pressed2 = true;
            repaint();
            return true;
        } else if (prompt == pageDownCode) {
            pressed1 = true;
            repaint();
            return true;
        } else {
            return false;
        }
    }

    public int getDisplayedItemNo() {
        return itemNo;
    }

    /**
     * Sets the associated transaction object. It'll also register itself
     * as the transaction listener of the transaction object.
     * @param trans the transaction object
     */
    public void setTransaction(Transaction trans) {
        this.trans = trans;
        //trans.addTransactionListener(this);
    }

    /**
     * Returns the associated transaction object.
     */
    public Transaction getTransaction() {
        return trans;
    }

    /**
     * Returns the enumerator from that we can obtain the assocaited
     *        LineItem objects.
     */
    public Iterator getLineItems() {
        if (getTransaction() != null) {
             return trans.getLineItemsIterator();
        } else {
             return null;
        }
    }

    public Dimension getPreferredSize() {
        return size;
    }

    public Dimension getMinimumSize() {
        return size;

    }

    public void update(Graphics g) {
        paint(g);
    }

    /**
     * Overrides the paint methods of Components.
     */
    public void paint(Graphics g) {
        int itemHeadersAmount;
        int startx = 13;
        int prex = 5;
        int prey = 4;
        int itemsum = 0;
        int marginstartx = 0;
        int MARGIN = 5;
        String stringObject;
        ChineseConverter chineseConverter = ChineseConverter.getInstance();

        size = new Dimension(getWidth(), getHeight());

        if (offscreen == null || offScreenW != getWidth() || offScreenH != getHeight()) {
            if (offscreen != null) {
                offscreen = null;
                System.gc();
            }
            offscreen = createImage(getWidth(), getHeight());
            offScreenW = getWidth();
            offScreenH = getHeight();
        }

        og = offscreen.getGraphics();
        java.util.List hList = processHeight(prey, og.getFontMetrics(headerFont).getHeight()
            , og.getFontMetrics(font).getHeight());

        maxLineInPage = hList.size() - 1;

        if (antiAlias)
            ((Graphics2D)og).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // paint back label
        if (app.getItemList().isAntiAlias())
            ((Graphics2D)og).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        og.setColor(Color.white);
        og.setClip(0, 0, getSize().width, getSize().height);

        og.setColor(backgroundColor);
        og.fill3DRect(0, 0, getSize().width, getSize().height, true);

        FontMetrics fm = og.getFontMetrics(headerFont);
        startx = fm.getHeight();

        int rectStartY = prey;
        int rectHeight = getSize().height - 2 * prey;
        int rectStartX = prex;
        int rectWidth = getSize().width - 2 * prex;
        og.setColor(Color.white);
        og.fillRect(rectStartX, rectStartY, rectWidth, rectHeight);
        //g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        if (colWidths.isEmpty())
            return;

        //  显示背景大标题字符
        if (!StringUtils.isEmpty(backLabel)) 
                //&& CreamProperties.getInstance().getProperty("ItemListNeedBackLabel", "yes").equalsIgnoreCase("yes"))
        {
            Font bigFont = new Font(fontName, Font.BOLD, 128);
            FontMetrics fm2 = og.getFontMetrics(bigFont);
            int backLabelW = fm2.stringWidth(backLabel);
            int backLabelH = fm2.getAscent();
            //og.setColor(new Color(230, 235, 255));
            og.setColor(new Color(221, 227, 227));
            og.setFont(bigFont);
            og.drawString(chineseConverter.convert(backLabel),
                rectStartX + (rectWidth - backLabelW) / 2,
                rectStartY + (rectHeight - backLabelH) / 2 + backLabelH);
        }

        java.util.List wList = processWidth(rectStartX, rectWidth, colWidths);

        // draw gridding
        og.setFont(headerFont);
        itemHeadersAmount = colWidths.size();
        og.setColor(gridColor);
        int liney = ((Integer) ((HashMap) hList.get(0)).values().toArray()[0]).intValue()
            + ((Integer) ((HashMap) hList.get(0)).keySet().toArray()[0]).intValue();
        og.drawLine(prex, liney, getSize().width - prex, liney);
        startx = 0;
        for (int n = 1; n < itemHeadersAmount; n++) {
            startx = ((Integer) ((HashMap) wList.get(n)).keySet().toArray()[0]).intValue();
            og.drawLine(startx, rectStartY, startx, rectStartY + rectHeight);
        }

        if (columnHeaders == null) {
            return;
        }

        //  draw header
        for (int i = 0; i < columnHeaders.size(); i++) {
            String header = columnHeaders.get(i).toString().trim();
            header = chineseConverter.convert(header);
//            marginstartx = (Integer.parseInt((String)colWidths.get(i)) * (size.width - 30) / 100 -
            startx = ((Integer) ((HashMap) wList.get(i)).keySet().toArray()[0]).intValue()
                + (((Integer) ((HashMap) wList.get(i)).values().toArray()[0]).intValue()
                - fm.stringWidth(header)) / 2;
            og.setColor(defaultColor);
            og.drawString(header, startx,
//                         25 - ((16 - fm.getHeight()) / 2));
                fm.getAscent()
                + ((Integer) ((HashMap) hList.get(0)).keySet().toArray()[0]).intValue());
        }

        if (columnAlignments.isEmpty()) {
            return;
        }

        //  pressed1 = true , means pressed nextPage button
        //  param 'itemIndex' means first lineItem sequence of current display page
        //  if lineItem arrayList has next page then show next page

        if (lineItemsArray != null) {
            if (pressed1 == true && (itemIndex + maxLineInPage) < lineItemsArray.length) {
                this.setItemIndex(itemIndex + maxLineInPage);
                pressed1 = false;
            } else {
                pressed1 = false;
            }
        //  pressed2 = true , means pressed previousPage button
        //  param 'itemIndex' means first lineItem sequence of current display page
        //  if lineItem arrayList has previous page then show previous page
            if (pressed2 == true && (itemIndex - maxLineInPage) < lineItemsArray.length &&
                    (itemIndex - maxLineInPage) >= 0) {
                this.setItemIndex(itemIndex - maxLineInPage);
                pressed2 = false;
            } else {
                pressed2 = false;
            }
        }

        if (itemIndex < 0)
            itemIndex = 0;

        FontMetrics fml = og.getFontMetrics(font);
        subTotal            = new HYIDouble(0);
        int startY = 28;
        try {
            if (lineItemsArray != null) {
                //  param 'itemsum' means sequence of current lineItem of current page
                while (itemsum < maxLineInPage && itemIndex < lineItemsArray.length && itemIndex >= 0) {//lineItemsArray.length
                    LineItem li = (LineItem)lineItemsArray[itemIndex];
                    //  param 'selectStatu' means selected state of lineItem
                    //  0 means no selected, 1 means selected
                    //  param 'itemAccount' means amount of all lineItem showed
                    /*
                    if (selectStatu.size() <= itemIndex) {
                        selectStatu.add(itemAccount, "0");
                    }*/
                    //  lineItem selected display with different font and color
                    startx = 13;
                    boolean selected = false;
                    if (selectStatu.contains(new Integer(itemIndex))) {
                        /*
                        if (!li.getRemoved()) {
                            og.setColor(Color.white);
                            og.fillRect(startx + 2, startY - fml.getHeight() + 9 - ((28 - fml.getHeight()) / 2) + (itemsum + 1) * 26, 585, fml.getHeight() - 4);
                        }
                        */
                        selected = true;
                    }
                    for (int m = 0; m < itemHeadersAmount; m++) {
                        methodName = "get" + itemHeaders.get(m).toString();
                        Method method = LineItem.class.getDeclaredMethod(methodName,
                                 new Class[0]);
                        Object retObject = method.invoke(li, new Object[0]);
                        if (retObject == null) {
                            stringObject = "";
                        } else if (retObject instanceof HYIDouble) {
                            // check HYIDouble Object digits equals 00
                            // if true then rounding digits
                            HYIDouble bigDecimal = (HYIDouble)retObject;

                            //Bruce/2003-12-01
                            if (showAmountWithScale2) {
                                if ("getWeight".equals(methodName))
                                    bigDecimal = bigDecimal.setScale(Param.getInstance().getWeightDigit(),
                                            BigDecimal.ROUND_HALF_UP);
                                else if ("getQuantity".equals(methodName)) {
                                    bigDecimal = bigDecimal.setScale(Param.getInstance().getQuantityDigit(),
                                            BigDecimal.ROUND_HALF_UP);
                                }
                                else
                                    bigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
                            } else {
                                String str = bigDecimal.toString();
                                int len = bigDecimal.scale();
                                String zeros = "";
                                //for (int i = 0; i < len; i++)  -> inefficient!
                                //    zeros = zeros + "0";
                                switch (len) {
                                    case 1:
                                        zeros = "0";
                                        break;
                                    case 2:
                                        zeros = "00";
                                        break;
                                    case 3:
                                        zeros = "000";
                                        break;
                                    case 4:
                                        zeros = "0000";
                                        break;
                                    case 5:
                                        zeros = "00000";
                                        break;
                                    case 6:
                                        zeros = "00000";
                                        break;
                                }
                                if (str.endsWith(zeros)) {
                                    bigDecimal = bigDecimal.setScale(0, BigDecimal.ROUND_HALF_UP);
                                } else {
                                    bigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
                                }
                            }
                            stringObject = bigDecimal.toString();
                        } else {
                            stringObject = chineseConverter.convert(retObject.toString());
                        }
                        int stringlength = fml.stringWidth(stringObject);
                        String st = columnAlignments.get(m).toString();
//                        int in = Integer.parseInt((String)colWidths.get(m)) * (size.width - 30) / 100;
                        int in = ((Integer) ((HashMap) wList.get(m)).values().toArray()[0]).intValue();
                        int stri = stringObject.length();
                        while (stringlength > in) {
                            stringObject = stringObject.substring(0, stri);
                            stringlength = fml.stringWidth(stringObject);
                            stri--;
                        }
                        if ((stringlength + 2 * MARGIN) > in)
                            MARGIN = 0;
                        else
                            MARGIN = 5;
                        //  check align mode
                        //  "L" means left, "R" means right, "C" means center
                        if (st.equalsIgnoreCase("L") || stringlength > in) {
                            marginstartx = MARGIN;
                        } else if (st.equalsIgnoreCase("R") && stringlength < in) {
                            marginstartx = in - stringlength - MARGIN;
                        } else if (st.equalsIgnoreCase("C") && stringlength < in) {
                            marginstartx = (in - stringlength) / 2;
                        }
                        if (selected) {
                            if (!li.getRemoved()) {
                                og.setColor(defaultColor);
                                og.setFont(font);
                            } else {
                                og.setColor(removedColor);
                                int starty = startY - fml.getHeight() / 3;
                                og.setFont(font);
                                og.drawLine(20,  starty - ((22 - fml.getHeight()) / 2) + (itemsum + 1) * 22,
                                    750, starty - ((22 - fml.getHeight()) / 2) + (itemsum + 1) * 22);
                            }
                            og.drawString(stringObject, ((Integer) ((HashMap) wList.get(m)).keySet().toArray()[0]).intValue() + marginstartx
                                , ((Integer) ((HashMap) hList.get(itemsum + 1)).keySet().toArray()[0]).intValue() + fml.getAscent());
                        } else {
                            og.setColor(this.defaultColor);//li.getLineItemColor());//new Color(80, 236, 236)
                            og.setFont(font);
                            fml = og.getFontMetrics(font);
                            if (li.getRemoved()) {// && !li.getDetailCode().equalsIgnoreCase("V") && !li.getDetailCode().equalsIgnoreCase("E"))
                                og.setColor(removedColor);
                                og.drawLine(
                                    ((Integer) ((HashMap) wList.get(m)).keySet().toArray()[0]).intValue() + marginstartx
                                    , ((Integer) ((HashMap) hList.get(itemsum + 1)).keySet().toArray()[0]).intValue() + fml.getAscent() / 2
                                    , rectWidth
//                                    ,((Integer) ((HashMap) wList.get(wList.size() - 1)).keySet().toArray()[0]).intValue() + marginstartx
                                    , ((Integer) ((HashMap) hList.get(itemsum + 1)).keySet().toArray()[0]).intValue() + fml.getAscent() / 2);
                            }
                            og.drawString(stringObject, ((Integer) ((HashMap) wList.get(m)).keySet().toArray()[0]).intValue() + marginstartx
                                , ((Integer) ((HashMap) hList.get(itemsum + 1)).keySet().toArray()[0]).intValue() + fml.getAscent());
                        }
                        startx = startx + in;
                    }
                    itemsum++;
                    itemAccount = itemIndex;
                    itemIndex++;

                    if (!li.getRemoved())
                        subTotal = subTotal.addMe(li.getAmount());
                }
                this.setItemIndex(itemIndex - itemsum);
            }
            int page = itemIndex / Math.max(maxLineInPage, 1) + 1;
            app.getSystemInfo().setPageNumber(String.valueOf(page));

            // for total amounts
            HYIDouble b2 = new HYIDouble(0);//组合促销
            if (trans.getMixAndMatchTotalAmount() != null)
               b2 = trans.getMixAndMatchTotalAmount().negate();
            if (b2.doubleValue() == 0)
               b2 = trans.getTotalMMAmount().negate();

            app.getSystemInfo().setSubtotalAmount(trans.getGrossSalesAmount().toString());  //小计
            app.getSystemInfo().setTotalMMAmount(b2.addMe(trans.getTotalSIAmount())  //组合促销＋折扣
                .setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            app.getSystemInfo().setSalesAmount(trans.getSalesAmount()
                .setScale(2, BigDecimal.ROUND_HALF_UP).toString());   //合计

        } catch (ConcurrentModificationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            repaint();
            return;
        } catch (NoSuchMethodException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InvocationTargetException e) {
            CreamToolkit.logMessage("Method Name=" + methodName);
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        g.drawImage(offscreen, 0, 0, null);
        og.dispose();

//        //  for transaction forward event
//        if (trans.decreaseLockCount() == 0) {
//            trans.setLockEnable(true);
//        }
//        CreamToolkit.showText(trans, 2);
    }

    public void setItemIndex(int itemIndex) {
        if (itemIndex < 0)  {
            // CreamToolkit.logMessage("Error ItemList paint | itemIndex < 0 : " + itemIndex);
            itemIndex = 0;
        }
        this.itemIndex = itemIndex;
    }

    private String methodName;
    public void transactionChanged(TransactionEvent e) {
        if (trans == null)
            trans = app.getCurrentTransaction();

        //  if it hide then it will not repaint, so check it if showing
        lineItemsArray = trans.getDisplayedLineItemsArray().toArray();
        if (!this.isShowing()) {
//            if (trans.decreaseLockCount() == 0) {
//                trans.setLockEnable(true);
//            }
            return;
        }

        if (!trans.summarize())
            return;

        itemNo = lineItemsArray.length;
        if (trans.getCurrentLineItem() != null) {
            if ((trans.getDealType1() == null || trans.getDealType2() == null || trans.getDealType3() == null)
                || (
                    !(trans.getDealType1().equals("0") && trans.getDealType2().equals("3") && trans.getDealType3().equals("4"))
                    && !(trans.getDealType1().equals("0") && trans.getDealType2().equals("0") && trans.getDealType3().equals("1"))
                    && !(trans.getDealType1().equals("0") && trans.getDealType2().equals("0") && trans.getDealType3().equals("2"))
                    && (!trans.getCurrentLineItem().getDetailCode().equals("V") && !trans.getCurrentLineItem().getDetailCode().equals("E"))
                   )
               )
            {
                int length = lineItemsArray.length;
                if (length > maxLineInPage) {
                    this.setItemIndex(((length - 1) / maxLineInPage) * maxLineInPage);
                }
            }
        }
        repaint();
    }

    public void addActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.add(actionListener,listener);
        enableEvents(AWTEvent.MOUSE_EVENT_MASK);

    }//*/

    /*public void addKeyListener(KeyListener k) {
         //System.out.println(kListener+"|1");
         super.addKeyListener(k);
         //kListener = AWTEventMulticaster.add(kListener,k);
         //System.out.println(kListener+"_");
         //System.out.println(kl + "|");
         enableEvents(AWTEvent.KEY_EVENT_MASK);
    }*/


    public void removeActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.remove(actionListener,listener);
    }

    //  clear selected state
    public void selectFinished() {
        selectStatu.clear();
        selectionMode = 0;
        repaint();
    }

    /**
     * Set selection mode.
     *
     * @param selectionMode SINGLE_SELECTION or MULTIPLE_SELECTION.
     */
    public void setSelectionMode(int selectionMode) {
        this.selectionMode = selectionMode;
    }

    /**
     * Get the selected line items.
     *
     * @return return the selected line item's index by an array. Index is
     *           0-based. If nothing selected, then return a null.
     */
    public int[] getSelectedItems() {
        ArrayList selectedItemsArray = new ArrayList();
        if (selectStatu.size() == 0) {
            return null;
        }
        for (int i = 0; i < selectStatu.size(); i++) {
            selectedItemsArray.add(selectStatu.get(i));
        }
        int[] selectedItems = new int[selectedItemsArray.size()];
        for (int m = 0; m < selectedItemsArray.size(); m++) {
            selectedItems[m] = ((Integer)selectedItemsArray.get(m)).intValue();
        }
        selectStatu.clear();
        //repaint();
        return selectedItems;
    }
    
    /*
     * return List : 共有n + 2行
     *             list.get(0) : 标题行
     *             list.get(1...list.getSize() - 2):商品行
     *             list.get(list.getSize() -1):总计行
     *             (HashMap) list.get(i);
     *                 hashmap | key 表字开始写的位置；value表行高
     * 
     * 修正:20030509 
     *     去掉总计行
     * return List : 共有n + 1行
     *             list.get(0) : 标题行
     *             list.get(1...list.getSize() - 1):商品行
     *             (HashMap) list.get(i);
     *                 hashmap | key 表字开始写的位置；value表行高
     */
    private java.util.List processHeight(int prey, int fHeight, int flHeight) {
        java.util.List list = new Vector();
        try {
            int height = new Double(getSize().getHeight()).intValue() - 2 * prey;
            int sHeight = 2;   // 行间隔
            int n = Math.round((height - fHeight - 4 * sHeight)
                / (flHeight + sHeight));
            int sub = height - n * flHeight - fHeight - (n + 2 + 2) * sHeight;
            while (sub > n + 4) {
                sub -= n + 4;
                sHeight++;
            }
            int height1 = prey;
            int height2 = 0;

            HashMap hm = new HashMap();
            height1 += sHeight;
            height2 = fHeight + sHeight;
            hm.put(new Integer(height1), new Integer(height2));
            list.add(hm);

            height1 += fHeight + 2 * sHeight;
            for (int i = 1; i <= n; i++) {
                height2 = sHeight;
                height2 += flHeight;
                hm = new HashMap();
                hm.put(new Integer(height1), new Integer(height2));
                list.add(hm);
                height1 += sHeight + flHeight;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    private java.util.List processWidth(int startx, int width, java.util.List colWidths) {
        java.util.List list = new Vector();
        try {
            java.util.List rateList = new Vector();
            int base = 0;
            for (int i = 0; i < colWidths.size(); i++) {
                int rate = Integer.parseInt((String) colWidths.get(i));
                rateList.add(new Integer(rate));
                base += rate;
            }

            int totalSubWidth = width;
            for (int i = 0; i < rateList.size(); i++) {
                int width1 = Math.round(width * ((Integer) rateList.get(i)).intValue()
                    / base - 0.5f);
                totalSubWidth -= width1;
                list.add(new Integer(width1));
            }

            int subWidth = 0;
            if (totalSubWidth > rateList.size()) {
                subWidth = Math.round(totalSubWidth / rateList.size() - 0.5f);
                totalSubWidth -= subWidth * rateList.size();
            }

            int width2 = startx;
            for (int i = 0; i < list.size(); i++) {
                int width1 = ((Integer) list.get(i)).intValue();
                width1 += subWidth;
                if (totalSubWidth > 0) {
                    width1++;
                    totalSubWidth--;
                }
                HashMap hm = new HashMap();
                hm.put(new Integer(width2), new Integer(width1));
                list.set(i, hm);
                width2 += width1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Font getFont() {
        return font;
    }

    public Font getHeaderFont() {
        return headerFont;
    }

    public boolean isAntiAlias() {
        return antiAlias;
    }

    public void setAntiAlias(boolean antiAlias) {
        this.antiAlias = antiAlias;
    }

    /**
     * 背景大标题字符
     */
    public String getBackLabel() {
        return backLabel;
    }

    /**
     * 设置背景大标题字符
     */
    public void setBackLabel(String backLabel) {
        this.backLabel = backLabel;
    }


    public boolean isDIY() {
        return false;
//        return isDIY;
    }

    public String getCurrentIndex() {
        return "0";
//        return this.diyIndex;
    }

    public void setDIY(boolean enabled, String index) {
//        this.isDIY = enabled;
//        if (enabled) {
//            this.diyIndex = index;
//        }
    }

}