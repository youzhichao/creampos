package hyi.cream.uibeans;

public class DIYOverrideButton extends POSButton {
	public DIYOverrideButton(int row, int column, int level,
			String overrideamountLabel) {
		super(row, column, level, overrideamountLabel);
	}

	public DIYOverrideButton(int row, int column, int level,
			String overrideamountLabel, int keyCode) {
		super(row, column, level, overrideamountLabel, keyCode);
	}
}
