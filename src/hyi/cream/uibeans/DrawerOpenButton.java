
package hyi.cream.uibeans;

/**
 * 开抽屉键.
 */
public class DrawerOpenButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param drawerOpenLabel inquiry label on button.
     */
	public DrawerOpenButton(int row, int column, int level, String drawerOpenLabel) {
        super(row, column, level, drawerOpenLabel);
    }

    public DrawerOpenButton(int row, int column, int level, String numberLabel, int keyCode) {
        super(row, column, level, numberLabel, keyCode);
    }
}

 