package hyi.cream.uibeans;

public class PeriodicalButton extends POSButton {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param label periodical label on button.
     */
	public PeriodicalButton(int row, int column, int level, String label) {
        super(row, column, level, label);
	}

	public PeriodicalButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
	}
}
