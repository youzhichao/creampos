/*
 * Created on 2003-4-1
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.uibeans;

/**
 * @author ll
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WeiXiuButton extends POSButton {
	private String pluCode     = "";
	private static WeiXiuButton weiXiuButton = null;
	public static WeiXiuButton getInstance() {
		return weiXiuButton;				
	}
	/**
	 * Constructor.
	 *
	 * @param row row position
	 * @param column column position
	 * @param label a string represents the button label
	*/
	public WeiXiuButton(int row, int column, int level, String label) {
		super(row, column, level, label);
		weiXiuButton = this;
	}

	/**
	 * Constructor.
	 *
	 * @param row row position
	 * @param column column position
	 * @param numberLabel a string represents the button label
	 * @param keyCode key code
	*/
	public WeiXiuButton(int row, int column, int level, String label, int keyCode) {
		super(row, column, level, label, keyCode);
		weiXiuButton = this;
	}

	/**
	 * Constructor.
	 *
	 * @param row row position
	 * @param column column position
	 * @param numberLabel a string represents the button label
	 * @param keyCode key code
	 * @param pluCode PLU number
	*/
	public WeiXiuButton(int row, int column, int level, String label, int keyCode, String pluCode) {
		super(row, column, level, label, keyCode);
		this.pluCode = pluCode;
		weiXiuButton = this;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        WeiXiuButton that = (WeiXiuButton)o;
        if (pluCode != null ? !pluCode.equals(that.pluCode) : that.pluCode != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (pluCode != null ? pluCode.hashCode() : 0);
        return result;
    }

    @Override
    protected String paramString() {
        return pluCode;
    }

	/**
	 * Get default PLU code.
	 *
	 * @return PLU code.
	 */
	public String getPluCode() {
		return pluCode;
	}
}
