package hyi.cream.uibeans;

import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;  

//import jpos.*;
//import jpos.events.*;

/**
 * 代收键.    ③
 */
public class DaiShouButton3 extends POSButton {
    private ResourceBundle res = CreamToolkit.GetResource();
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    
    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
    public DaiShouButton3(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
     * @param keyCode key code
    */
    public DaiShouButton3(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }

}

