
package hyi.cream.uibeans;

/**
 * 发票号码键.
 */
public class InvoiceNoButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param invoiceNoLabel invoice number label on button.
     */
    public InvoiceNoButton(int row, int column, int level, String invoiceNoLabel,  int keyCode) {
        super(row, column, level, invoiceNoLabel, keyCode);
    }
}

 