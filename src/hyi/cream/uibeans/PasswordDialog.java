package hyi.cream.uibeans;

import java.awt.*;
import java.util.*;

import hyi.cream.state.GetProperty;
import hyi.cream.util.*;
import hyi.cream.groovydac.Param;

/**
 * A Class class.
 * <P>
 * @author
 */
public class PasswordDialog extends Dialog { 
    private Image offscreen                       = null;
	private Graphics og                           = null;
    private	ArrayList bufferedObjects = new ArrayList();
    private String password = "";
    private int length = 0;
    private boolean modal = false; 
    private ResourceBundle res = CreamToolkit.GetResource(); 

    static PasswordDialog passwordDialog = null;

    public static PasswordDialog getInstance() {
        try {
            if (passwordDialog == null) {
                passwordDialog = new PasswordDialog();
            }
        } catch (InstantiationException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Instantiation exception at " + PasswordDialog.class);
        }
        return passwordDialog;
    }

    public PasswordDialog() throws InstantiationException {
        super(new Frame(), false);
    }

    public void setLength(int length) {
        this.length = length;
    }         

    public void invalidate() {
        super.invalidate();
        offscreen = null;
    }

    public void update(Graphics g) {
        paint(g);
    }

    public void paint(Graphics g) {

        if (offscreen == null) {
            offscreen = createImage(getSize().width, getSize().height);
        } else {
            g.drawImage(offscreen, 0, 0, this);
		}
        og = offscreen.getGraphics();
		og.setClip(0, 0, getSize().width, getSize().height);

        //  show password
        og.setFont(new Font(Param.getInstance().getIndicatorFont(), Font.BOLD, 14));
        og.setColor(new Color(255, 255, 255)); 
        og.fillRoundRect(10, 5, 180, 40, 5, 5);
        og.setColor(new Color(0, 0, 0));
        if (password.equals("")) {
            og.drawString(res.getString("Password"), 20, 40);
        } else {
            String strObj = "";
            while (strObj.length() < password.length()) {
                strObj = strObj + "*";
            }
            og.drawString(strObj, 20, 40);
        }

        g.drawImage(offscreen, 0, 0, this);
        og.dispose();
    }

    public void addToList(Object obj) {
        bufferedObjects.add(obj);
    }

    private void finished() {
        setVisible(false);
        Iterator iter = ((ArrayList)bufferedObjects.clone()).iterator();
        bufferedObjects.clear();
        Object obj = null;
        while (iter.hasNext()) {
            ((PasswordDialogListener)iter.next()).checking();
        }
        password = "";
    }

    public boolean keyListener(int prompt, char keyChar) {
        if (prompt == 45) {
            password = "";
            repaint();
        } else if (prompt == 111) {
            password = "";
            finished();
        } else if (prompt == 10) {
            if (length == 0) {
                finished();
            }
        } else if (prompt >= '0' && prompt <= '9') {
            password = password + keyChar;
            repaint();
            if (length != 0 && length == password.length()) {
                finished();
            }
        } else {
            return false;
        }
        return true;
	}

    public String getPassword() {
        return password;
    }
}
 
