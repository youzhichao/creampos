package hyi.cream.uibeans;

/**
 * 锁定键
 */

public class LockButton extends POSButton {
   public LockButton(int row, int column, int level, String inquiryLabel) {
      super(row, column, level, inquiryLabel);
   }

  public LockButton(int row, int column, int level, String inquiryLabel, int keyCode) {
        super(row, column, level, inquiryLabel,keyCode);
   }

}