package hyi.cream.uibeans

import hyi.cream.POSButtonHome2
import hyi.cream.POSTerminalApplication
import hyi.cream.groovydac.CardDetail
import hyi.cream.state.GetProperty
import hyi.cream.util.CreamToolkit
import hyi.cream.util.HYIDouble
import java.awt.*
import java.awt.geom.Rectangle2D
import hyi.cream.groovydac.Param
import hyi.cream.util.ChineseConverter

/**
 * 信用卡明細輸入畫面。
 *
 * <p>Copyright: Copyright (c) 2008 HYI Ltd., Co.</p>
 *
 * @author Bruce You
 * @since 2008-09-05
 * @version 1.0
 */
class CreditCardForm extends Window /*Dialog*/ {

    //     總交易金額: [XXXXXXXXXX.XX]
    // 0   卡號: [XXXXXXXXXXXXXXXX]
    // 1   有效期: [XXXX]
    // 2   端末機編號: [XXXXXXXX]
    // 3   授權碼: [XXXXXXXXX]
    // 4   調閱編號: [XXXXXX]
    // 5   序號: [XXXXXXXXXXXX]
    // 6   紅利抵用: [XXXXXXXXXX.XX]
    // 7   紅利後實付金額: [XXXXXXXXXX.XX]
    // 8   分期付款交易輸入'1': [X]
    //   G0

    private static final int G0 = 12

    private static final Color gridBgColor = new Color(127, 127, 255)

    private static final int EDIT_FORM_STATE = 0
    private static final int YESNO_FORM_STATE = 1

    static CreditCardForm creditCardForm

    private ResourceBundle res = CreamToolkit.GetResource()
    private Font font
    private Font numberFont

    private NumberLabel[] numberLabels = new NumberLabel[9]
    CardDetail cardDetail
    private int currentFieldIdx = 0
    private int bottomY
    private int state
    private String answer

    private int pageUpCode
    private int pageDownCode
    private int clearCode
    private int enterCode
    private ArrayList numberCodeArray
    private ArrayList numberButtonArray
    Rectangle titleRect = new Rectangle(0, 0, 10, 10)

    private CreditCardForm(Frame owner) {
        //super(owner, true) // modal dialog
        super(owner)

        String fontName = Param.instance.payingPaneFont
        font = new Font(fontName, Font.PLAIN, 20)
        setFont(font)
        setBackground(Color.black)

        for (int i = 0; i < numberLabels.length; i++)
            numberLabels[i] = new NumberLabel()
        numberLabels[0].selected = true
    }

    public static CreditCardForm getInstance() {
        if (creditCardForm == null) {
            creditCardForm = new CreditCardForm(POSTerminalApplication.instance.mainFrame)
        }
        return creditCardForm
    }

    public static void close() {
        if (creditCardForm)
            creditCardForm.dispose()
        creditCardForm = null
    }

    public boolean keyDataListener(int keyCode) {

        if (state == EDIT_FORM_STATE) {
            if (keyCode == clearCode) {
                //state = YESNO_FORM_STATE
                //answer = ""
                //numberLabels[currentFieldIdx].setSelected(false)
                numberLabels[currentFieldIdx].backspace()
            } else if (numberCodeArray.contains(String.valueOf(keyCode))) {
                def numberLabel = numberButtonArray[numberCodeArray.indexOf(String.valueOf(keyCode))].numberLabel
                numberLabels[currentFieldIdx].appendDigits(numberLabel)
            } else if (keyCode == pageDownCode || keyCode == enterCode) {
                if (currentFieldIdx < numberLabels.length - 1) {
                    numberLabels[currentFieldIdx].applyChange()
                    numberLabels[currentFieldIdx].setSelected(false)
                    numberLabels[++currentFieldIdx].setSelected(true)
                } else {
                    numberLabels[currentFieldIdx].applyChange()
                    numberLabels[currentFieldIdx].setSelected(false)
                    state = YESNO_FORM_STATE
                    answer = ""
                }
            } else if (keyCode == pageUpCode) {
                if (currentFieldIdx > 0) {
                    numberLabels[currentFieldIdx].applyChange()
                    numberLabels[currentFieldIdx].setSelected(false)
                    numberLabels[--currentFieldIdx].setSelected(true)
                }
            } else {
                Toolkit.getDefaultToolkit().beep()
            }
        } else {
            if (numberCodeArray.contains(String.valueOf(keyCode))) {
                String num = ((NumberButton)numberButtonArray.get(numberCodeArray.indexOf(String.valueOf(keyCode)))).getNumberLabel()
                if (num == '1') {          // OK
                    answer = '1'
                } else if (num == '2') {   // continue modifying
                    answer = '2'
                } else if (num == '3') {   // cancel
                    answer = '3'
                }
            } else if (keyCode == clearCode) {  // continue modifying
                numberLabels[currentFieldIdx].setSelected(true)
                answer = ''
                state = EDIT_FORM_STATE
            } else if (keyCode == enterCode) {
                if (answer == '2') {
                    numberLabels[currentFieldIdx].setSelected(true)
                    state = EDIT_FORM_STATE
                } else if (answer == '1') {   // OK
                    updateCardDetail()
                    setVisible(false)
                    notifyWaiter()
                } else if (answer == '3') {   // cancel
                    setVisible(false)
                    notifyWaiter()
                } else {
                    Toolkit.getDefaultToolkit().beep()
                }
            } else {
                Toolkit.getDefaultToolkit().beep()
            }
        }
        repaint()
        return true
    }

    synchronized void waitForAnswer() {
        wait();
    }

    synchronized void notifyWaiter() {
        notifyAll();
    }

    public void setBounds() {
        final int width = 620
        final int height = 362
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize()
        setBounds((int)((screenSize.width - width) / 2), (int)((screenSize.height - height) / 2),
            width, height)
    }

    def initCreditCardData() {
        //     總交易金額: [XXXXXXXXXX.XX]
        // 0   卡號: [XXXXXXXXXXXXXXXX]
        // 1   有效期: [XXXX]
        // 2   端末機編號: [XXXXXXXX]
        // 3   授權碼: [XXXXXXXXX]
        // 4   調閱編號: [XXXXXX]
        // 5   序號: [XXXXXXXXXXXX]
        // 6   紅利抵用: [XXXXXXXXXX.XX]
        // 7   紅利後實付金額: [XXXXXXXXXX.XX]
        // 8   分期付款交易輸入'1': [X]
        numberLabels[0].stringValue = '' // cardDetail.cardNo
        numberLabels[1].stringValue = '' // cardDetail.cardExpDate
        numberLabels[2].stringValue = '' // cardDetail.terminalId
        numberLabels[3].stringValue = '' // cardDetail.approvalCode
        numberLabels[4].stringValue = '' // cardDetail.addendum.invoiceNo
        numberLabels[5].stringValue = '' // cardDetail.addendum.refNo
        numberLabels[6].stringValue = ''
        numberLabels[6].bigDecimalValue = new HYIDouble(0.00) // cardDetail.addendum.bonusDiscount
        numberLabels[7].stringValue = ''
        numberLabels[7].bigDecimalValue = new HYIDouble(0.00) // cardDetail.addendum.bonusPaid
        numberLabels[8].stringValue = ''
    }

    public void update(Graphics g) {
        paint(g)
    }

    private String c(String string) {
        ChineseConverter chineseConverter = ChineseConverter.getInstance();
        return chineseConverter.convert(string);
    }

    public void paint(Graphics g) {
        try {
            ChineseConverter chineseConverter = ChineseConverter.getInstance();
            ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

            paintFrameBorderAndLabels(g)

            // draw numberLabels
            for (numberLabel in numberLabels) {
                if (numberLabel.invalid) {
                    numberLabel.paint(g)
                    numberLabel.invalid = false
                }
            }
            g.setFont font
            if (state == EDIT_FORM_STATE) {
                g.clearRect(140, bottomY + 24, 400, 32)
                g.drawString(c('使用[上頁],[下頁]或[確認]在欄位間移動 '), 140, bottomY + 24 * 2)
                g.drawString("  ", 140 + 14 * 24, bottomY + 30)
            } else {
                FontMetrics fm = g.getFontMetrics()
                def text = c('[1] 確認儲存  [2/清除] 繼續修改  [3] 放棄返回 ->')
                g.clearRect(140, bottomY + 24, 400, 32)
                g.drawString(text, 140, bottomY + 24 * 2)
                int sw = fm.getStringBounds(text, g).width + 12
                g.drawString("           ", 140 + sw, bottomY + 30)
                g.drawString(answer, 140 + sw, bottomY + 24 * 2)
            }
        } catch (Exception e) {
            CreamToolkit.logMessage(e)
        }
    }

    def paintFrameBorderAndLabels(Graphics g) {
        g.setFont(font)
        FontMetrics fm1 = g.getFontMetrics()
        g.setFont(NumberLabel.FONT)
        FontMetrics fm2 = g.getFontMetrics()

        // draw border
        g.clearRect(0, 0, getWidth(), getHeight())
        Insets insets = this.getInsets()
        def ix = insets.left
        def iy = insets.top
        def iw = getWidth() - ix - insets.right
        def ih = getHeight() - iy - insets.bottom

        g.setColor(Color.white)
        g.draw3DRect(ix, iy, iw, ih, true)
        g.draw3DRect(ix + 1, iy + 1, iw - 2, ih - 2, true)

        // draw title
        g.setColor(gridBgColor)
        g.fill3DRect(ix + 2, iy + 2, iw - 4, fm1.getHeight(), true)
        titleRect = new Rectangle(ix + 2, iy + 2, iw - 4, fm1.getHeight())
        g.setFont(font)
        g.setColor(Color.white)
        g.drawString(c('請填信用卡明細資料'), (int)((getWidth() - 24 * 7) / 2), (int)(fm1.getHeight() + iy - 2))

        //     總交易金額: [XXXXXXXXXX.XX]
        // 0   卡號: [XXXXXXXXXXXXXXXX]
        // 1   有效期: [XXXX]
        // 2   端末機編號: [XXXXXXXX]
        // 3   授權碼: [XXXXXXXXX]
        // 4   調閱編號: [XXXXXX]
        // 5   序號: [XXXXXXXXXXXX]
        // 6   紅利抵用: [XXXXXXXXXX.XX]
        // 7   紅利後實付金額: [XXXXXXXXXX.XX]
        //   G0

        Rectangle2D rect = fm1.getStringBounds("X", g)
        int yb = (int)rect.height - 4
        int hb = (int)rect.height

        //     總交易金額: [XXXXXXXXXX.XX]
        int fh = fm1.getHeight() + 6
        int y = fm1.getHeight() * 5 / 2 + iy
        g.drawString(c("總交易金額: ${cardDetail.transAmt}"), G0, y)

        // 0   卡號: [9999999999999999]
        def text = c('卡號: ')
        y += fh
        g.drawString(text, G0, y)
        numberLabels[0].setBounds((int)(G0 + fm1.getStringBounds(text, g).width), y - yb,
            (int)(fm2.getStringBounds('9999999999999999', g).width) + 8, hb)
        numberLabels[0].len = 16
        numberLabels[0].rightAlign = false

        // 1   有效期: [9999]
        text = c('有效期: ')
        y += fh
        g.drawString(text, G0, y)
        numberLabels[1].setBounds((int)(G0 + fm1.getStringBounds(text, g).width), y - yb,
            (int)(fm2.getStringBounds('9999', g).width + 8), hb)
        numberLabels[1].len = 4
        numberLabels[1].rightAlign = false

        // 2   端末機編號: [99999999]
        text = c('端末機編號: ')
        y += fh
        g.drawString(text, G0, y)
        numberLabels[2].setBounds((int)(G0 + fm1.getStringBounds(text, g).width), y - yb,
            (int)(fm2.getStringBounds('99999999', g).width + 8), hb)
        numberLabels[2].len = 8
        numberLabels[2].rightAlign = false

        // 3   授權碼: [999999999]
        text = c('授權碼: ')
        y += fh
        g.drawString(text, G0, y)
        numberLabels[3].setBounds((int)(G0 + fm1.getStringBounds(text, g).width), y - yb,
            (int)(fm2.getStringBounds('999999999', g).width + 8), hb)
        numberLabels[3].len = 9
        numberLabels[3].rightAlign = false

        // 4   調閱編號: [999999]
        text = c('調閱編號: ')
        y += fh
        g.drawString(text, G0, y)
        numberLabels[4].setBounds((int)(G0 + fm1.getStringBounds(text, g).width), y - yb,
            (int)(fm2.getStringBounds('999999', g).width + 8), hb)
        numberLabels[4].len = 6
        numberLabels[4].rightAlign = false

        // 5   序號: [999999999999]
        text = c('序號: ')
        y += fh
        g.drawString(text, G0, y)
        numberLabels[5].setBounds((int)(G0 + fm1.getStringBounds(text, g).width), y - yb,
            (int)(fm2.getStringBounds('999999999999', g).width + 8), hb)
        numberLabels[5].len = 12
        numberLabels[5].rightAlign = false

        // 6   紅利抵用: [9999999999.99]
        text = c('紅利抵用: ')
        y += fh
        g.drawString(text, G0, y)
        numberLabels[6].setBounds((int)(G0 + fm1.getStringBounds(text, g).width), y - yb,
            (int)(fm2.getStringBounds('9999999999.99', g).width + 8), hb)
        numberLabels[6].len = 13
        numberLabels[6].rightAlign = true

        // 7   紅利後實付金額: [9999999999.99]
        text = c('紅利後實付金額: ')
        y += fh
        g.drawString(text, G0, y)
        numberLabels[7].setBounds((int)(G0 + fm1.getStringBounds(text, g).width), y - yb,
            (int)(fm2.getStringBounds('9999999999.99', g).width + 8), hb)
        numberLabels[7].len = 13
        numberLabels[7].rightAlign = true
        bottomY = y

        // 8   分期付款交易輸入'1': [9]
        text = c("分期付款交易輸入'1': ")
        y += fh
        g.drawString(text, G0, y)
        numberLabels[8].setBounds((int)(G0 + fm1.getStringBounds(text, g).width), y - yb,
            (int)(fm2.getStringBounds('9', g).width + 8), hb)
        numberLabels[8].len = 1
        numberLabels[8].rightAlign = false
        bottomY = y

        for (numberLabel in numberLabels)
            numberLabel.paint(g)
    }

    public void edit() {
        // 为了防止在输入期间，用户会连续按多次回车键导致系统重起，
        // 所以将POSButtonHome中的enterCode暂时改掉，好让例如Tec6400KB146中的
        // POSButtonHome.getInstance().getEnterCode()拿到的变成是ClearButton
        // 的key code。也就是在现金清点单输入期间，必须改成按多次清除键才能让系统
        // 重起。
        try {
            int origEnterCode = enterCode
            POSButtonHome2 posButtonHome = POSButtonHome2.getInstance()
            posButtonHome.setEnterCode(posButtonHome.getClearCode())

            state = EDIT_FORM_STATE
            numberLabels[0].selected = true
            for (int i = 1; i < numberLabels.length; i++)
                numberLabels[i].selected = false
            setVisible(true)
            waitForAnswer()

            // 把enterCode改回来
            posButtonHome.setEnterCode(origEnterCode)
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger())
        }
    }

    private void updateCardDetail() {
        cardDetail.cardNo = numberLabels[0].stringValue
        cardDetail.cardExpDate = numberLabels[1].stringValue
        cardDetail.terminalId = numberLabels[2].stringValue
        cardDetail.approvalCode = numberLabels[3].stringValue
        cardDetail.addendum.invoiceNo = numberLabels[4].stringValue
        cardDetail.addendum.refNo = numberLabels[5].stringValue
        cardDetail.addendum.bonusDiscount = numberLabels[6].bigDecimalValue
        cardDetail.addendum.bonusPaid = numberLabels[7].bigDecimalValue
        cardDetail.addendum.installmentType = numberLabels[8].stringValue
        // 分期付款欄位設成'1'的時候，表示是信用卡分期付款交易
        if ('1' != cardDetail.addendum.installmentType)
            cardDetail.addendum.installmentType = null
    }

    /**
     * Get card number and exipration date by swiping credit card at POS's MSR. 
     */
    public void sendMSRData(String accountNumber, String expirationDate) {
        numberLabels[0].stringValue = accountNumber
        numberLabels[1].stringValue = expirationDate
        numberLabels[0].invalid = numberLabels[1].invalid = true
        repaint()
    }

    /**
     * @return true: modified and saved, false: want to cancel and back
     */
    public boolean modify(CardDetail cardDetail) {
        // Get key code definitions
        POSButtonHome2 posButtonHome = POSButtonHome2.getInstance()
        pageUpCode = posButtonHome.getPageUpCode()
        pageDownCode = posButtonHome.getPageDownCode()
        clearCode = posButtonHome.getClearCode()
        enterCode = posButtonHome.getEnterCode()
        numberCodeArray = posButtonHome.getNumberCode()
        numberButtonArray = posButtonHome.getNumberButton()

        this.cardDetail = cardDetail
        initCreditCardData()

        currentFieldIdx = 0
        edit()

        return answer.equals("1")
    }
}

/*private*/ class NumberLabel {
    static final Font FONT = new Font('Courier', Font.PLAIN, 20)

    int y
    int x
    int width
    int height
    int len                             // chars count
    boolean selected
    boolean rightAlign = true
    Integer intValue = null
    HYIDouble bigDecimalValue = null    // contains either bigDecimalValue or intValue
    String stringValue                  // this is for editting buffer
    boolean invalid                     // for painting
    boolean rewriteMode

    void setSelected(boolean selected) {
        this.selected = selected
        invalid = true
        rewriteMode = true
    }

    void applyChange() {
        if (stringValue == null || stringValue.length() == 0)
            return
        try {
            if (bigDecimalValue != null)    // 如果bigDecimalValue有值，说明原本为BigDecimal型
                bigDecimalValue = new HYIDouble(stringValue)
            else if (intValue != null)      // the same
                intValue = new Integer(stringValue)
        } catch (NumberFormatException e) {
            e.printStackTrace()
        }
    }

    void setBounds(int x, int y, int width, int height) {
        this.x = x
        this.y = y
        this.width = width
        this.height = height
    }

    static String padBlank(Integer value, boolean rightAlign, int digits) {
        padBlank(value.toString(), rightAlign, digits)
    }

    static String padBlank(HYIDouble value, boolean rightAlign, int digits) {
        padBlank(value.toString(), rightAlign, digits)
    }

    static String padBlank(String value, boolean rightAlign, int digits) {
        StringBuffer fld = new StringBuffer(value)
        while (fld.length() < digits) {    // pad space
            if (rightAlign)
                fld.insert(0, ' ')
            else
                fld.append(' ')
        }
        return fld.toString()
    }

    def appendDigits(String digit) {
        if (stringValue == null) {
            stringValue = ''
            invalid = true
        }
        if (rewriteMode) {
            stringValue = digit
            invalid = true
            rewriteMode = false   // switch to append mode
        } else if (stringValue.length() < len) {
            stringValue += digit
            invalid = true
        }
    }

    def backspace() {
        if (stringValue == null) {
            stringValue = ''
            invalid = true
        }
        if (rewriteMode) {
            stringValue = ''
            invalid = true
            rewriteMode = false   // switch to append mode
        } else if (stringValue.length() > 0) {
            if (stringValue.length() > 1)
                stringValue = stringValue[0..-2]
            else
                stringValue = ''
            invalid = true
        }
    }

    void paint(Graphics g) {
        g.setFont FONT
        if (!selected) {
            g.setColor(Color.white)
            g.clearRect(x, y, width, height)
            g.draw3DRect(x, y, width, height, false)
            if (stringValue != null)
                g.drawString(padBlank(stringValue, rightAlign, len), x + 4, y + height - 4)
            else if (bigDecimalValue != null)
                g.drawString(padBlank(bigDecimalValue, rightAlign, len), x + 4, y + height - 4)
            else
                g.drawString(padBlank(intValue, rightAlign, len), x + 4, y + height - 4)
        } else {
            g.setColor(Color.white)
            g.fill3DRect(x, y, width, height, false)
            g.setColor(Color.blue)
            if (stringValue != null)
                g.drawString(padBlank(stringValue, rightAlign, len), x + 4, y + height - 4)
            else if (bigDecimalValue != null)
                g.drawString(padBlank(bigDecimalValue, rightAlign, len), x + 4, y + height - 4)
            else
                g.drawString(padBlank(intValue, rightAlign, len), x + 4, y + height - 4)
            g.setColor(Color.white)
        }
    }
}
