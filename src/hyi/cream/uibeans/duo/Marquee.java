package hyi.cream.uibeans.duo;

import org.apache.commons.lang.StringUtils;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.interpolation.*;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;

/**
 * Marquee.
 * - 可以显示三种信息，priority: warningMessage > informationMessage > broadcastMessage
 *
 * @author Bruce's photo:
 *
 *   $$Z$$$$$$$$$$7$77777NNN8N8ZOOO7?D8$7$O$7I7Z88III888888OOMO$8M:~~~===++++++++++++
 *   $$$$$$$$$$$$$7$7777NND7$8$OOOOO8OOO7IO$7ZOOO8IIDO777$Z887NN$O8:~~~===++++++?++++
 *   $$$$$$$$$$$$$$$777ZNDZ7IIOOOOOOZZI8N77OOOOO887$NNNNNNN8O7$NOZOO~~~====++++++++?+
 *   $$$$$$$$$$$$$$$$$7D8$OO7?IOOOO$7ZN$7OOOOOOOZIINNNNNNNNNNMMND78OO~~~====+++++++??
 *   $$Z$$$$$$$$$$$$7$78IOOO$$????ZNN8+ZOOOOOOZ??ZNNNNNNNNNNNNNNNI88ID~~~==++++++?+++
 *   $$Z$$$$$$$$$$$$$7Z7?OOOOO?7MNNI$OOOZ?+?DNNNNNNNNNNNNNNNNNNNNI88INO~~===+++++++++
 *   $$$Z$$$$$$$$$$$$Z$7D$OOOO$7NN?7II8NNNNNNNNNNNNNNNNMNNNNNNNNN7OOO8MZ~=++++++?++++
 *   $$$Z$$$$$$$$$$$$ZZ8OZOOOO?MNMNNMNNNNND$????+++++IZDNNNNNNNNNM7O8$NN~==+++++?++++
 *   $$$Z$$$$$$$$$$$ZZZNO$OOOINNNNNNMMZ?+++++++===++++????ZNNNNNNNM7OOIO7~==+++++++++
 *   $$$$$$$$$$$$$$$O$NDM?Z?ZNNNNNN7?+++++=~~~==~==+??7$$$$Z$8NNNNNNIOOOO=+=+++++++++
 *   Z$$Z$$$$$$$$$$$OOZI7$ZNNNNNN$7$$7?+?====~~==+??I$OZZZOOOZD8NNNNNIOOO~===++++++++
 *   $$$$$$$$$$$$$$$ZOOOINNNNNNZOO8OOOOZ7I??=~~=+?????+++???IIZODONNNN$OO?====+++++++
 *   $$ZZ$$$$$$$$$$$OOOO$NNNMD8$7I?++?+++???+===+???+?$IDD88888DNMDOMNN$OZ===++++++++
 *   $$ZZ$$$$$$$$$$$OOOONNN8O7ODNNNNNNZ.OZ???=~=+$DD7..$8O8$$$7ZO$8M7NNDZZ====++?++++
 *   $ZZ$Z$$$$$$$$$$IOOZNMND8$ZZI7$OZDOZ$I:I$NDZM7?II$$?8$OIZO$ZZ8IN7MNN8Z===++++++++
 *   $$$$Z$$$$$$$$$$8$ODNNMD$ZZ$Z8I8DO+Z$??+DO==+OII+?I7I+?I7$$ZZZ8M$MNND8?==++?++++=
 *   8DNDO$$$Z$$$$$$NZZNMMN$$$ZO$I+++I77I??=M+~=+7DII+????II777II?DM$ZNNDN7=+++++++++
 *   NNNDDDNDZ$$Z$$$NNZNMMNMZ7Z7?IIII????++7O+==+IOOI????????I??7NDZIZNNNNI==++++++++
 *   DNNNNDDNNNZ$$$$NNNN8Z$MN$7I+++++++?$DO7+?++?I77I77I$$7II+=++??II7DNNN===++++++++
 *   Z8DNNDDDDDDZ$$$NNNM8Z777III7$$$7I+?II?+??+??I7II??????++=++???IIIDMMN=++++++++++
 *   ZZO8O88DNND8$$$ZNNM8ZI??I?++++++++++?I?I7+??I777$I++=?+++++???IIIOMMD=++++++++++
 *   ZOO7ZONNNNNOZZ$$NNMDOII?++++++++==+I?+???+++?III?II?++===+++?I?IIZNMD=++++++++++
 *   NNO$77OMMNNZ$Z$$DNMNZIII?+++====+??7?+++++==+?????7???=++++??IIII$NM8+++++++++++
 *   MMMMDD8DMI~~=ZONNNMMOIIII++====+???I77$Z$II?I$7777I?+???+++?IIIII7NMO+++++++++++
 *   NMMMMD8M+~=~===NOMMM87III?++=+???????II7II??II7777II???+????II?I77NMI+++++++++++
 *   MMNNNMNM====++??OMMM8$IIII??????????I?????????IIII77II??+??II??I77NM=+++++++++++
 *   MNNNNMMDN??I??++MMMMN$II?II???????I??++??+++=?IIIIIII???+??I???I7$M8++++++?+++++
 *   MMMMMMMMMNII?+IMMNMMMO7I??I?????IIII77$777I7$7$$ZZZ$87?????I???IIDM++++??+++++++
 *   MMMMMMMMNDMMNOMOOZNMNZ$7????????I787?I=I:+~===++$$7Z$I??+??I??I77N++++++++++++++
 *   MMMMMMMMMMMMMNMM$$$MMMO$7??I??????II$ZZOIII+I+I7$$$$??????II??I$??+++?+++++++???
 *   D8MMMMMNMMMMMMMM$$$7NMMZ$I??I??+??+?I7II???IIIII7$7I?????II?I7$7+?+?+???+???++++
 *   NNNDMMMMMNMMMMMMMMNZ$INMO$7III?I????IIIIII???IIIIIII???IIIII7$77?++?+++++++++?++
 *   MMMNNNNNDDNMMMMMMMDDO8DNNDZ7IIII??????I7I77I7IIIIIIIIII?III7$$+Z++++++++++++++++
 *   MMMND8NNDZDMMMMMMM8ZZDMND88Z$77I?II????????????IIIIIII?I777$$:I$+++++++?++??+?++
 *   MMNNNMMN8ZZMMMMMMMMMNNNDODN8ZZ$$7IIII?????++????IIIIII7$$$77OM$7+++?+++??++??+++
 *   MMMMNDD8ZOO8NMMMMMMN$OO88NNDZZZ$Z$7II????++++++????II7$$$777MMN$?++++?++???????+
 *   MMMMMMM8ZZZZ8DM8NMMMNODMDD8NMO$$$$Z$7III?????????II7$$$777$7MMMN8+??????+?????+?
 *   MMMMNMMDZDDZO8MZNMMMOO$88NMNNDZ$$$$$$ZZ$777$$$77$ZZ$777777$$MMMMM8?+??+?????????
 *   MMMNMN8NMDOZDDN8OMMNDNDNNMMNNN$Z$$$77$$$$$$$$$$$$777777777$ZMMMNNNN????????+????
 *   DMNMNNMNDDMNN888O8NMO8NNNMNNNM$$$7777777777777777I77777777$DMMMMNND$+???????????
 *   88M8MMDMM8ODMNO888DNND8DNNNNMM$$$$777IIIIIIIIIIIIIII7777$7ZMMMMMNNN8$7I?????????
 *   MO8NNDNNDNM88DDNN888ODDDNNNDMMZ77$$77IIIIII?I?III777I7777ZMMMMMMMMND$$7Z7?????I?
 *   DDOODN8NNO8NNO8NNND8D8DDN8OOM8Z777777IIIIIIIIIIIIIII7II78MMMMMMMMMNZ7777777I????
 *   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 *
 */
public class Marquee extends Canvas implements MouseListener {

    private static final float ANIMATION_DELAY = 480f;

    /** Non-linear accelaration: ease in/ease out with start/end delay. */
    private class SplineInterpolatorWithDelay implements Interpolator {
        float delayStart;
        float delayEnd;
        float splineWidth;
        Interpolator interpolator;

        private SplineInterpolator delegate = new SplineInterpolator(1f, 0f, 0f, 1f);
        private SplineInterpolator delegate2 = new SplineInterpolator(.3f, .6f, .6f, .3f);

        void useEaseInEaseOutInterpolator() { interpolator = delegate; }
        void useFastInFastOutInterpolator() { interpolator = delegate2; }

        public float interpolate(float lengthFraction) {
            return (lengthFraction < delayStart) ? 0f :
                   (lengthFraction > delayEnd) ? 1f :
                   interpolator.interpolate((lengthFraction - delayStart) / splineWidth);
        }
    };

    private int offScreenH;
    private int offScreenW;
    private Image offscreen;
    private int numOfLines = 1;
    private int rowHeight;

    private SplineInterpolatorWithDelay interpolator = new SplineInterpolatorWithDelay();

    private Color flashingBackgroundStartColr = new Color(0.8f, 0.0f, 0.0f, 0.6f);
    private Color flashingBackgroundEndColr = new Color(0.8f, 0.0f, 0.0f, 0.0f);

    private String broadcastMessage;
    private String origBroadcastMessage;
    private String warningMessage;
    private String origWarningMessage;
    private String informationMessage;
    private String origInformationMessage;

    private Image backgroundImage;
    private Color origBackground;
    private int origW, origH;
    private int warningMessageX;
    private int broadcastMessageX;
    private int informationMessageX;
    private int messageY;

    private Animator backgroundAnimator;
    private Animator wiggleAnimator;
    private Animator marqueeAnimator;
    private boolean wiggleAnimatorIsPaused;
    private boolean marqueeAnimatorIsPaused;
    private FontMetrics fm;
    //private ChineseConverter chineseConverter = ChineseConverter.getInstance();

    public Marquee() {
        addMouseListener(this);
    }

    public String getBroadcastMessage() {
        return broadcastMessage;
    }

    public void setBroadcastMessage(String broadcastMessage) {
        this.origBroadcastMessage = broadcastMessage;
        this.broadcastMessage = broadcastMessage; //chineseConverter.convert(broadcastMessage);

        if (isMarqueeAnimationRunning())
            stopMarqueeAnimator();

        repaint();
    }

    public String getInformationMessage() {
        return informationMessage;
    }

    public void setInformationMessage(String informationMessage) {
        this.origInformationMessage = informationMessage;
        this.informationMessage = informationMessage; //chineseConverter.convert(informationMessage);

        if (isMarqueeAnimationRunning())
            stopMarqueeAnimator();

        if (isWiggleAnimationRunning())
            stopWiggleAnimator();
        
        repaint();
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.origWarningMessage = warningMessage;
        this.warningMessage = warningMessage; //chineseConverter.convert(warningMessage);

        // Restarting wiggle animation if it's running
        if (isWiggleAnimationRunning())
            stopWiggleAnimator();

        if (StringUtils.isEmpty(warningMessage)) {
            // Stop background animation if setting warningMessage to empty string
            if (backgroundAnimator != null && backgroundAnimator.isRunning())
                backgroundAnimator.stop();
            backgroundAnimator = wiggleAnimator = null;
            if (origBackground != null)
                setBackground(origBackground);
        }
        repaint();
    }

    public void refresh() {
        if (!StringUtils.isEmpty(origBroadcastMessage))
            this.broadcastMessage = origBroadcastMessage; //chineseConverter.convert(origBroadcastMessage);
        if (!StringUtils.isEmpty(origInformationMessage))
            this.informationMessage = origInformationMessage; //chineseConverter.convert(origInformationMessage);
        if (!StringUtils.isEmpty(origWarningMessage))
            this.warningMessage = origWarningMessage; //chineseConverter.convert(origWarningMessage);
    }

    private Rectangle2D getStringBounds(Graphics g, FontMetrics fm, String message) {
        if (numOfLines > 1)
            return fm.getStringBounds(message.split("\\n")[0], g);
        else
            return fm.getStringBounds(message, g);
    }

    private void setupBackgroundAndWiggleAnimation(Graphics g) {
        if (backgroundAnimator == null || !backgroundAnimator.isRunning()) {
            origBackground = getBackground();
            KeyFrames keyFrames = new KeyFrames(
                KeyValues.create(
                    flashingBackgroundStartColr,
                    flashingBackgroundEndColr
                ));
            PropertySetter setter = new PropertySetter(this, "background", keyFrames);
            backgroundAnimator = new Animator(500, Animator.INFINITE, null, setter);
            backgroundAnimator.start();
        }

        //Font font = getFont();
        //FontMetrics fm = g.getFontMetrics(font);
        Rectangle2D bounds = getStringBounds(g, fm, warningMessage);

        if (getWidth() > bounds.getWidth()) {
            // No need to animate if message width is shorter than displaying window
            if (isWiggleAnimationRunning())
                stopWiggleAnimator();
            warningMessageX = (int)(getWidth() - bounds.getWidth()) / 2;

        } else {
            if (!isSizeChanged(g) && isWiggleAnimationRunning())
                return;

            if (isWiggleAnimationRunning())
                stopWiggleAnimator();

            // Create a wiggle animator
            int dist = (int)bounds.getWidth() - getWidth();
            int duration = Math.max(dist * 6, 500); // 根據滾動的距離決定動畫時間區間
            duration += (int)ANIMATION_DELAY * 2;   // 加上頭尾暫停時間
            interpolator.delayStart = ANIMATION_DELAY / duration; // 計算delay時間佔比
            interpolator.delayEnd = 1f - interpolator.delayStart;
            interpolator.splineWidth = 1f - interpolator.delayStart * 2f;
            interpolator.useEaseInEaseOutInterpolator();
            wiggleAnimator = PropertySetter.createAnimator(duration, this, "warningMessageX",
                0, -dist);
            wiggleAnimator.setRepeatCount(Animator.INFINITE);
            //wiggleAnimator.setStartDelay((int)ANIMATION_DELAY);
            wiggleAnimator.setInterpolator(interpolator);
            wiggleAnimator.start();
        }
    }

    private void setupInformationWiggleAnimation(Graphics g) {
        Rectangle2D bounds = getStringBounds(g, fm, informationMessage);

        if (getWidth() > bounds.getWidth()) {
            // No need to animate if message width is shorter than displaying window
            if (isWiggleAnimationRunning())
                stopWiggleAnimator();
            informationMessageX = (int)(getWidth() - bounds.getWidth()) / 2;

        } else {
            if (!isSizeChanged(g) && isWiggleAnimationRunning())
                return;

            if (isWiggleAnimationRunning())
                stopWiggleAnimator();

            // Create a wiggle animator
            int dist = (int)bounds.getWidth() - getWidth();
            int duration = Math.max(dist * 6, 500); // 根據滾動的距離決定動畫時間區間
            duration += (int)ANIMATION_DELAY * 2;   // 加上頭尾暫停時間
            interpolator.delayStart = ANIMATION_DELAY / duration; // 計算delay時間佔比
            interpolator.delayEnd = 1f - interpolator.delayStart;
            interpolator.splineWidth = 1f - interpolator.delayStart * 2f;
            interpolator.useEaseInEaseOutInterpolator();
            wiggleAnimator = PropertySetter.createAnimator(duration, this, "informationMessageX",
                0, -dist);
            wiggleAnimator.setRepeatCount(Animator.INFINITE);
            //wiggleAnimator.setStartDelay((int)ANIMATION_DELAY);
            wiggleAnimator.setInterpolator(interpolator);
            wiggleAnimator.start();
        }
    }

    private void setupMarqueeAnimation(Graphics g) {

        if (!isSizeChanged(g) && isMarqueeAnimationRunning())
            return;

        if (isMarqueeAnimationRunning())
            stopMarqueeAnimator();

        Rectangle2D bounds = getStringBounds(g, fm, broadcastMessage);

        // Create a marquee animator
        int dist = (int)bounds.getWidth() + getWidth();
        int duration = Math.max(dist * 11 / 2, 500);  // 根據滾動的距離決定動畫時間區間
        duration += (int)ANIMATION_DELAY * 2;    // 加上頭尾暫停時間
        interpolator.delayStart = ANIMATION_DELAY / duration; // 計算delay時間佔比
        interpolator.delayEnd = 1f - interpolator.delayStart;
        interpolator.splineWidth = 1f - interpolator.delayStart * 2f;
        interpolator.useFastInFastOutInterpolator();
        marqueeAnimator = PropertySetter.createAnimator(duration, this, "broadcastMessageX",
            getWidth(), (int)-bounds.getWidth());
        marqueeAnimator.setRepeatCount(Animator.INFINITE);
        marqueeAnimator.setRepeatBehavior(Animator.RepeatBehavior.LOOP);
        //marqueeAnimator.setStartDelay((int)ANIMATION_DELAY);
        marqueeAnimator.setInterpolator(interpolator);
        marqueeAnimator.start();
    }

    public int getNumOfLines() {
        return numOfLines;
    }

    public void setNumOfLines(int numOfLines) {
        this.numOfLines = numOfLines;
    }

    public Image getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(Image backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public int getWarningMessageX() {
        return warningMessageX;
    }

    public void setWarningMessageX(int warningMessageX) {
        this.warningMessageX = warningMessageX;
        repaint();
    }

    public int getInformationMessageX() {
        return informationMessageX;
    }

    public void setInformationMessageX(int informationMessageX) {
        this.informationMessageX = informationMessageX;
        repaint();
    }

    public int getBroadcastMessageX() {
        return broadcastMessageX;
    }

    public void setBroadcastMessageX(int broadcastMessageX) {
        this.broadcastMessageX = broadcastMessageX;
        repaint();
    }

    public void update(Graphics g) {
        if (isShowing())
            paint(g);
    }

    public void paint(Graphics rg) {
        if (rg == null || getWidth() == 0 || getHeight() == 0)
            return;

        if (fm == null) {
            fm = getGraphics().getFontMetrics(getFont());
            if (numOfLines > 1) {
                rowHeight = getHeight() / numOfLines;
                messageY = rowHeight - fm.getDescent();
            } else
                messageY = (getHeight() + fm.getHeight()) / 2 - fm.getDescent();
        }

        if (offscreen == null || offScreenW != getWidth() || offScreenH != getHeight()) {
            offscreen = createImage(getWidth(), getHeight());
            offScreenW = getWidth();
            offScreenH = getHeight();
        }

        Graphics2D g = (Graphics2D)offscreen.getGraphics();

        if (backgroundImage == null) {
            g.setColor(this.getBackground());
            g.fillRect(0, 0, getWidth(), getHeight());
        } else {
            g.drawImage(backgroundImage, 0, 0, this);
        }

        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        // Priority: warningMessage > informationMessage > broadcastMessage
        if (!StringUtils.isEmpty(warningMessage)) {
            setupBackgroundAndWiggleAnimation(g);
            showWarningMessage(g);
        } else if (!StringUtils.isEmpty(informationMessage)) {
            setupInformationWiggleAnimation(g);
            showInformationMessage(g);
        } else if (!StringUtils.isEmpty(broadcastMessage)) {
            setupMarqueeAnimation(g);
            showBroadcastMessage(g);
        } else {
            g.setColor(this.getBackground());
            g.fillRect(0, 0, getWidth(), getHeight());
        }

        rg.drawImage(offscreen, 0, 0, this);
        g.dispose();
    }

    private boolean isSizeChanged(Graphics g) {
        if (getWidth() != origW || getHeight() != origH) {
            origW = getWidth();
            origH = getHeight();
            Font font = getFont();
            fm = g.getFontMetrics(font);
            messageY = (getHeight() + fm.getHeight()) / 2 - fm.getDescent(); 
            return true;
        } else
            return false;
    }

    private void showMessasge(Graphics g, String message, int messageX, int messageY) {
        g.setColor(getForeground());
        if (numOfLines > 1) {
            int y = messageY;
            String[] text = message.split("\\n");
            for (int i = 0; i < numOfLines; i++) {
                g.drawString(text[i], messageX, y);
                y += rowHeight;
            }
        } else {
            g.drawString(message, messageX, messageY);
        }
    }

    private void showWarningMessage(Graphics g) {
        showMessasge(g, warningMessage, warningMessageX, messageY);
    }

    private void showInformationMessage(Graphics g) {
        showMessasge(g, informationMessage, informationMessageX, messageY);
    }

    private void showBroadcastMessage(Graphics g) {
        showMessasge(g, broadcastMessage, broadcastMessageX, messageY);
    }

    private boolean isWiggleAnimationRunning() {
        return wiggleAnimator != null && (wiggleAnimatorIsPaused || wiggleAnimator.isRunning());
    }

    private boolean isMarqueeAnimationRunning() {
        return marqueeAnimator != null && (marqueeAnimatorIsPaused || marqueeAnimator.isRunning());
    }

    private void stopWiggleAnimator() {
        if (wiggleAnimator != null)
            wiggleAnimator.stop();
        wiggleAnimatorIsPaused = false;
    }

    private void stopMarqueeAnimator() {
        if (marqueeAnimator != null)
            marqueeAnimator.stop();
        marqueeAnimatorIsPaused = false;
    }

    public void mousePressed(MouseEvent e) {
        if (isWiggleAnimationRunning()) {
            wiggleAnimator.pause();
            wiggleAnimatorIsPaused = true;
        }
        if (isMarqueeAnimationRunning()) {
            marqueeAnimator.pause();
            marqueeAnimatorIsPaused = true;
        }
    }

    public void mouseReleased(MouseEvent e) {
        if (isWiggleAnimationRunning()) {
            wiggleAnimator.resume();
            wiggleAnimatorIsPaused = false;
        }
        if (isMarqueeAnimationRunning()) {
            marqueeAnimator.resume();
            marqueeAnimatorIsPaused = false;
        }
    }

    public void mouseExited(MouseEvent e) {
        if (isWiggleAnimationRunning()) {
            wiggleAnimator.resume();
            wiggleAnimatorIsPaused = false;
        }
        if (isMarqueeAnimationRunning()) {
            marqueeAnimator.resume();
            marqueeAnimatorIsPaused = false;
        }
    }

    public void mouseClicked(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
}