package hyi.cream.uibeans.duo;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.security.MessageDigest;
import java.util.*;
import java.util.List;

/**
 * SecondScreen for 广州7-11. Bruce You @ 2010-12-08
 * 
 * Customer dispaly theme: File name: ./ads/theme_[theme name].conf
 * <p/>
 * <pre>
 * ScreenSize=x, h
 * BackgroundImage=[background picture file name]
 * MarqueeArea=x, y, w, h
 * MarqueeFontAndSize=[font name],[size]
 * MarqueeTextColor=r, g, b
 * LineItemArea=x, y, w, h
 * LineItemNumOfLines=c
 * LineItemFontAndSize=[font name],[size]
 * LineItemTextColor=r, g, b
 * LineItemContent=品名,220,l,Name,;數量,45,r,Qty,##0;金額,92,r,SalesAmt,#####0
 * TenderArea=x, y, w, h
 * TenderFontAndSize=[font name],[size]
 * TenderTextColor=r, g, b
 * TenderNumOfLines=c
 * TenderContent=合計金額,170,r,SaleAmt,#####0;收　　您,170,r,PayTotal,#####0;餘　　額,170,r,Balance,#####0;找　　零,170,r,ChangeAmt,#####0
 * VideoArea=x, y, w, h
 * SmallAdsArea1=x, y, w, h
 * SmallAdsArea2=x, y, w, h
 * </pre>
 * <p/>
 * Small Ads. area: File name: ./ads/small_ads_area1.conf, ./ads/small_ads_area2.conf
 * <p/>
 * <pre>
 * picture1.png, 10
 * picture2.jpg, 70
 * </pre>
 *
 * @author Bruce You
 */
public class SecondScreen extends Frame //implements TransactionListener
{
    private static final long serialVersionUID = 1L;

    public static final int HIGH_PRIORITY_NICE = -8;
    public static final int NORMAL_PRIORITY_NICE = 0;
    public static final int LOW_PRIORITY_NICE = 10;

    private int playerNiceValue = NORMAL_PRIORITY_NICE;

    private GraphicsConfiguration gc;
    private static SecondScreen instance;
    private List themeInfos = new ArrayList();
    private int currentThemeIndex = -1;
    private Panel contentPane;

    // private JFlashPlayer flashPlayer;

    //private List adsPictureInfos1 = new ArrayList();
    //private List adsPictureInfos2 = new ArrayList();
    //private int currentAdsPictureIndex1 = -1;
    //private int currentAdsPictureIndex2 = -1;

    private Marquee marquee;
    private Marquee itemList2;
    private Marquee tenderList2;
    private Panel videoPanel;
    private Writer videoCommandWriter;
    //private CustDispItemList itemList;
    //private CustDispTenderList tenderList;
    //private PicturePanel smallAdsArea1;
    //private PicturePanel smallAdsArea2;

    private class ThemeInfo {
        String themeName;

        // Screen size
        Dimension screenSize;

        Point position;

        // Background image
        String background;
        BufferedImage backgroundImage;

        // Marquee
        Rectangle marqueeArea;
        Font marqueeFont;
        Color marqueeTextColor;

        // Item list
        Rectangle lineItemArea;
        //int lineItemCount;
        int lineItemNumOfLines;
        Font lineItemFont;
        Color lineItemTextColor;
        //ColumnDefine[] lineItemColumns;

        // Tender
        Rectangle tenderArea;
        Font tenderFont;
        int tenderNumOfLines;
        Color tenderTextColor;
        //ColumnDefine[] tenderColumns;

        // Video
        Rectangle videoArea;

        // Picture ads
        //Rectangle smallAdsArea1;
        //Rectangle smallAdsArea2;
    }

    private class AdsPictureInfo {
        String fileName;
        int displayTime; // in seconds
    }

//    private class PicturePanel extends Canvas
//    {
//        private static final long serialVersionUID = 1L;
//
//        private Image currentImage;
//
//        public void paint(Graphics g)
//        {
//            g.drawImage(currentImage, 0, 0, this);
//        }
//
//        public Image getCurrentImage()
//        {
//            return currentImage;
//        }
//
//        public void setCurrentImage(BufferedImage currentImage)
//        {
//            int w = getBounds().width;
//            int h = getBounds().height;
//            this.currentImage = currentImage.getScaledInstance(w, h, Image.SCALE_SMOOTH);
//            repaint();
//        }
//    }

    public SecondScreen(String title, GraphicsConfiguration gc) {
        super(title, gc);

        this.gc = gc;

        // stop any suspended MPlayer
        try {
            Runtime.getRuntime().exec("/usr/bin/killall mplayer").waitFor();
        } catch (Exception e) {
        }

        // load theme configuration
        loadConfiguration();
        if (themeInfos.isEmpty())
            return;

        // get current theme
        currentThemeIndex = 0;

        // set frame size
        setSize(((ThemeInfo) themeInfos.get(currentThemeIndex)).screenSize);
        this.setLocation(((ThemeInfo) themeInfos.get(currentThemeIndex)).position);

        // create content pane for drawing background picture, and set null layout
        contentPane = new Panel() {
            private static final long serialVersionUID = 1L;

            public void paint(Graphics g) {
                ThemeInfo themeInfo = getCurrentTheme();
                if (themeInfo != null && themeInfo.backgroundImage != null) {
                    g.drawImage(themeInfo.backgroundImage, 0, 0, this);
                }
            }
        };
        setLayout(new BorderLayout());
        add(contentPane, BorderLayout.CENTER);
        contentPane.setLayout(null);

        contentPane.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    VideoControlDialog d = new VideoControlDialog(SecondScreen.this, true);
                    d.setLocation(60, 60);
                    d.setVisible(true);
                }
            }
        });

        createMarqueeArea();
        createLineItemArea2();
        createTenderArea2();
        createVideoArea();
        //createSmallAdsArea();

        setCursor();
        instance = this;
    }

    public Panel getContentPane() {
        return contentPane;
    }

    public static SecondScreen getInstance() {
        return instance;
    }

//    private void sleepInSecond(int second)
//    {
//        try
//        {
//            Thread.sleep(second * 1000L);
//        }
//        catch (InterruptedException e)
//        {
//        }
//    }

//    private void createSmallAdsArea()
//    {
//        ThemeInfo themeInfo = getCurrentTheme();
//        if (themeInfo == null)
//            return;
//
//        if (!adsPictureInfos1.isEmpty())
//        {
//            if (smallAdsArea1 != null)
//            {
//                smallAdsArea1.setBounds(themeInfo.smallAdsArea1);
//            }
//            else
//            {
//                smallAdsArea1 = new PicturePanel();
//                smallAdsArea1.setBounds(themeInfo.smallAdsArea1);
//                getContentPane().add(smallAdsArea1);
//                new Thread() { public void run()
//                {
//                    for (;;)
//                    {
//                        AdsPictureInfo adsPictureInfo = (AdsPictureInfo)adsPictureInfos1.get(currentAdsPictureIndex1);
//                        smallAdsArea1.setCurrentImage(
//                            getBufferedImageFromFile(new File(adsPictureInfo.fileName)));
//                        sleepInSecond(adsPictureInfo.displayTime);
//                        if (++currentAdsPictureIndex1 >= adsPictureInfos1.size())
//                            currentAdsPictureIndex1 = 0;
//                    }
//                }}.start();
//            }
//        }
//        if (!adsPictureInfos2.isEmpty())
//        {
//            if (smallAdsArea2 != null)
//            {
//                smallAdsArea2.setBounds(themeInfo.smallAdsArea2);
//            }
//            else
//            {
//                smallAdsArea2 = new PicturePanel();
//                smallAdsArea2.setBounds(themeInfo.smallAdsArea2);
//                getContentPane().add(smallAdsArea2);
//                new Thread() { public void run()
//                {
//                    for (;;)
//                    {
//                        AdsPictureInfo adsPictureInfo = (AdsPictureInfo)adsPictureInfos2.get(currentAdsPictureIndex2);
//                        smallAdsArea2.setCurrentImage(
//                            getBufferedImageFromFile(new File(adsPictureInfo.fileName)));
//                        sleepInSecond(adsPictureInfo.displayTime);
//                        if (++currentAdsPictureIndex2 >= adsPictureInfos2.size())
//                            currentAdsPictureIndex2 = 0;
//                    }
//                }}.start();
//            }
//        }
//    }

    private void setCursor() {
        Cursor c = Toolkit.getDefaultToolkit().createCustomCursor(
            Toolkit.getDefaultToolkit().createImage(new byte[0]), new Point(), "invisible");
        setCursor(c);
        Component[] components = getComponents();
        for (int i = 0; i < components.length; i++) {
            Component comp = components[i];
            comp.setCursor(null);
        }
    }

    private ThemeInfo getCurrentTheme() {
        return (ThemeInfo) themeInfos.get(currentThemeIndex);
    }

    public void changeTheme(boolean forward) {
        if (themeInfos.size() > 1) {
            if (forward) {
                if (++currentThemeIndex >= themeInfos.size())
                    currentThemeIndex = 0;
            } else {
                if (--currentThemeIndex < 0)
                    currentThemeIndex = themeInfos.size() - 1;
            }

            createMarqueeArea();
            createLineItemArea2();
            createTenderArea2();
            createVideoArea();
            //createSmallAdsArea();
            repaint();
        }
    }

    private void createMarqueeArea() {
        ThemeInfo themeInfo = getCurrentTheme();
        if (themeInfo != null) {
            if (marquee == null) {
                marquee = new Marquee();
                getContentPane().add(marquee);
                //new Thread(marquee).start();
            }

            marquee.setBounds(themeInfo.marqueeArea);
            marquee.setFont(themeInfo.marqueeFont);
            marquee.setForeground(themeInfo.marqueeTextColor);
            //marquee.setBackground(new Color(0x33, 0x66, 0, 0));
            int x = themeInfo.marqueeArea.x;
            int y = themeInfo.marqueeArea.y;
            int w = themeInfo.marqueeArea.width;
            int h = themeInfo.marqueeArea.height;
            marquee.setBackgroundImage(themeInfo.backgroundImage.getSubimage(x, y, w, h));
            marquee.setBroadcastMessage("活动期间，凡全店消费满10元（不含代收、代售、整套YOKA公仔预购），即可获得世足加油点标一点。");
        }
    }

    private void createVideoArea() {
        ThemeInfo themeInfo = getCurrentTheme();
        if (themeInfo != null) {
            if (videoPanel == null) {
                videoPanel = new Panel();
                videoPanel.setName("VideoPanel");
                videoPanel.setBackground(Color.black);
                videoPanel.setBounds(themeInfo.videoArea);
                getContentPane().add(videoPanel);
            } else {
                videoPanel.setBounds(themeInfo.videoArea);
                videoPanel.setVisible(true);
                videoPanel.repaint();
                //sendVideoCommand("pause");
                sendVideoCommand("pt_step +1");
                sendVideoCommand("pt_step -1");
            }
        }
    }

    private void createLineItemArea2() {
        ThemeInfo themeInfo = getCurrentTheme();
        if (themeInfo != null) {
            if (itemList2 == null) {
                itemList2 = new Marquee();
                getContentPane().add(itemList2);
            }

            itemList2.setFont(themeInfo.lineItemFont);
            itemList2.setForeground(themeInfo.lineItemTextColor);
            itemList2.setBounds(themeInfo.lineItemArea);
            itemList2.setNumOfLines(themeInfo.lineItemNumOfLines);
            int x = themeInfo.lineItemArea.x;
            int y = themeInfo.lineItemArea.y;
            int w = themeInfo.lineItemArea.width;
            int h = themeInfo.lineItemArea.height;
            itemList2.setBackgroundImage(themeInfo.backgroundImage.getSubimage(x, y, w, h));
            itemList2.setInformationMessage("YOKA公仔       X 2 = 1,234.00\n程序员杂志     X 1 =   456.00");
        }
    }

//    private void createLineItemArea()
//    {
//        ThemeInfo themeInfo = getCurrentTheme();
//        if (themeInfo != null)
//        {
//            if (itemList == null)
//            {
//                itemList = new CustDispItemList();
//                getContentPane().add(itemList);
//            }
//
//            itemList.setFont(themeInfo.lineItemFont);
//            itemList.setForeground(themeInfo.lineItemTextColor);
//            itemList.setBounds(themeInfo.lineItemArea);
//            itemList.setVisible(true);
//            itemList.setLineItemCount(themeInfo.lineItemCount);
//            int x = themeInfo.lineItemArea.x;
//            int y = themeInfo.lineItemArea.y;
//            int w = themeInfo.lineItemArea.width;
//            int h = themeInfo.lineItemArea.height;
//            itemList.setBackgroundImage(themeInfo.backgroundImage.getSubimage(x, y, w, h));
//            itemList.setColDef(themeInfo.lineItemColumns);
//            itemList.transactionChanged(new TransactionEvent(this, TransactionEvent.ITEM_CHANGED));
//        }
//    }

    private void createTenderArea2() {
        ThemeInfo themeInfo = getCurrentTheme();
        if (themeInfo != null) {
            if (tenderList2 == null) {
                tenderList2 = new Marquee();
                getContentPane().add(tenderList2);
            }

            tenderList2.setFont(themeInfo.tenderFont);
            tenderList2.setForeground(themeInfo.tenderTextColor);
            tenderList2.setBounds(themeInfo.tenderArea);
            tenderList2.setNumOfLines(themeInfo.tenderNumOfLines);
            int x = themeInfo.tenderArea.x;
            int y = themeInfo.tenderArea.y;
            int w = themeInfo.tenderArea.width;
            int h = themeInfo.tenderArea.height;
            tenderList2.setBackgroundImage(themeInfo.backgroundImage.getSubimage(x, y, w, h));
            tenderList2.setInformationMessage(
                "合计:   1,690.00             \n收您:   1,700.00  找零: 10.00");
        }
    }

//    private void createTenderArea()
//    {
//        ThemeInfo themeInfo = getCurrentTheme();
//        if (themeInfo != null)
//        {
//            if (tenderList == null)
//            {
//                tenderList = new CustDispTenderList();
//                getContentPane().add(tenderList);
//            }
//
//            tenderList.setFont(themeInfo.tenderFont);
//            tenderList.setForeground(themeInfo.tenderTextColor);
//            tenderList.setBounds(themeInfo.tenderArea);
//            int x = themeInfo.tenderArea.x;
//            int y = themeInfo.tenderArea.y;
//            int w = themeInfo.tenderArea.width;
//            int h = themeInfo.tenderArea.height;
//            tenderList.setBackgroundImage(themeInfo.backgroundImage.getSubimage(x, y, w, h));
//
//            System.out.println("lg.1:" + themeInfo.tenderColumns.length);
//            System.out.println("lg.2:" + themeInfo.tenderColumns[0].label);
//            System.out.println("lg.3:" + themeInfo.tenderColumns[0].width);
//            System.out.println("lg.4:" + themeInfo.tenderColumns[1].label);
//            System.out.println("lg.5:" + themeInfo.tenderColumns[1].width);
//
//            tenderList.setColDef(themeInfo.tenderColumns);
//            tenderList.repaint();
//        }
//    }

    private void loadConfiguration() {
        File dir = new File("./ads");
        if (!dir.exists() || !dir.isDirectory())
            return;

        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith("theme_") && name.endsWith(".conf");
            }
        });
        for (int i = 0; i < files.length; i++) {
            try {
                File themeConfFile = files[i];
                ThemeInfo themeInfo = new ThemeInfo();
                String s = themeConfFile.getName().substring(6);
                themeInfo.themeName = s.substring(0, s.length() - 5);

                System.out.println("Theme name:" + themeInfo.themeName);

                Properties prop = new Properties();
                prop.load(new FileInputStream(themeConfFile));
                prop.load(new FileInputStream(themeConfFile));
                prop.load(new InputStreamReader(new FileInputStream(themeConfFile), "UTF-8"));

                themeInfo.screenSize = parseDimensionInfo((String) prop.get("ScreenSize"));
                themeInfo.position = parsePointInfo((String) prop.get("Position"));
                themeInfo.background = (String) prop.get("BackgroundImage");
                themeInfo.backgroundImage = getBufferedImageFromFile(themeInfo.background,
                    themeInfo.screenSize.width, themeInfo.screenSize.height);
                themeInfo.marqueeArea = parseRectangleInfo((String) prop.get("MarqueeArea"));
                themeInfo.marqueeTextColor = parseColorInfo((String) prop.get("MarqueeTextColor"));
                themeInfo.marqueeFont = parseFontInfo((String) prop.get("MarqueeFontAndSize"));
                themeInfo.lineItemArea = parseRectangleInfo((String) prop.get("LineItemArea"));
                //themeInfo.lineItemCount = Integer.parseInt((String)prop.get("LineItemCount"));
                themeInfo.lineItemNumOfLines = Integer.parseInt((String) prop.get("LineItemNumOfLines"));
                themeInfo.lineItemTextColor = parseColorInfo((String) prop.get("LineItemTextColor"));
                themeInfo.lineItemFont = parseFontInfo((String) prop.get("LineItemFontAndSize"));
                //themeInfo.lineItemColumns = parseColumnInfo((String)prop.get("LineItemContent"));
                themeInfo.tenderArea = parseRectangleInfo((String) prop.get("TenderArea"));
                themeInfo.tenderFont = parseFontInfo((String) prop.get("TenderFontAndSize"));
                themeInfo.tenderTextColor = parseColorInfo((String) prop.get("TenderTextColor"));
                //themeInfo.tenderColumns = parseColumnInfo((String)prop.get("TenderContent"));
                themeInfo.tenderNumOfLines = Integer.parseInt((String) prop.get("TenderNumOfLines"));
                themeInfo.videoArea = parseRectangleInfo((String) prop.get("VideoArea"));
                //themeInfo.smallAdsArea1 = parseRectangleInfo((String)prop.get("SmallAdsArea1"));
                //themeInfo.smallAdsArea2 = parseRectangleInfo((String)prop.get("SmallAdsArea2"));
                themeInfos.add(themeInfo);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*loadAdsPictureInfo("./ads/small_ads_area1.conf", adsPictureInfos1);
        if (!adsPictureInfos1.isEmpty())
            currentAdsPictureIndex1 = 0;
        loadAdsPictureInfo("./ads/small_ads_area2.conf", adsPictureInfos2);
        if (!adsPictureInfos2.isEmpty())
            currentAdsPictureIndex2 = 0;*/
    }

//    public static String[] split(String str, String delim) {
//        List ts = new ArrayList();
//        StringTokenizer t = new StringTokenizer(str, delim);
//        while (t.hasMoreTokens()) {
//            ts.add(t.nextToken().trim());
//        }
//        String[] ta = new String[ts.size()];
//        for (int i = 0; i < ts.size(); i++)
//            ta[i] = (String) ts.get(i);
//        return ta;
//    }
//
//    private void loadAdsPictureInfo(String confFilename, List adsPictureInfos) {
//        try {
//            List lines = FileUtils.readLines(new File(confFilename), "UTF-8");
//            Iterator iter = lines.iterator();
//            while (iter.hasNext()) {
//                String ln = (String) iter.next();
//                String line = (String) ln;
//                String[] v = line.split("\\s*,\\s*");
//                File file = new File(v[0]);
//                if (file.exists()) {
//                    try {
//                        AdsPictureInfo adsPictureInfo = new AdsPictureInfo();
//                        adsPictureInfo.fileName = file.getAbsolutePath();
//                        adsPictureInfo.displayTime = Integer.parseInt(v[1]);
//                        adsPictureInfos.add(adsPictureInfo);
//                    }
//                    catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    private ColumnDefine[] parseColumnInfo(String value)
//    {
//        String[] v = value.split("\\s*;\\s*");
//        ColumnDefine[] columnDefines = new ColumnDefine[v.length];
//        //System.out.printf("parseColumnInfo> columnDefines count=%d\n", v.length);
//        for (int i = 0; i < v.length; i++)
//        {
//            String u = v[i];
//            //System.out.printf("parseColumnInfo> columnDefines[%d]=%s\n", i, u);
//            try {
//                // u like: 數量,45,r,Qty,##0
//                String[] w = u.split("\\s*,\\s*");
//                columnDefines[i] = new ColumnDefine();
//                columnDefines[i].label = w[0];
//                System.out.println("lg:parseColumnInfo> columnDefines[%d].label="+ ((ColumnDefine)(columnDefines[i])).label);
//                columnDefines[i].width = Integer.parseInt(w[1]);
//                System.out.println("lg:parseColumnInfo> columnDefines[%d].width=" + ((ColumnDefine)(columnDefines[i])).width);
//                columnDefines[i].alignment = w[2];
//                //System.out.printf("parseColumnInfo> columnDefines[%d].alignment=%s\n", i, columnDefines[i].alignment);
//                columnDefines[i].property = w[3];
//                //System.out.printf("parseColumnInfo> columnDefines[%d].property=%s\n", i, columnDefines[i].property);
//                columnDefines[i].pattern = w.length >= 5 ? w[4] : "";
//                //System.out.printf("parseColumnInfo> columnDefines[%d].pattern=%s\n", i, columnDefines[i].pattern);
//                System.out.println("lg:i:"+ i);
//            }
//            catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        System.out.println("lg:parse over");
//        return columnDefines;
//    }

    private Color parseColorInfo(String value) {
        try {
            String[] v = value.split("\\s*,\\s*");
            return new Color(Integer.parseInt(v[0].trim()), Integer.parseInt(v[1].trim()), Integer.parseInt(v[2].trim()));
        }
        catch (Exception e) {
            return Color.black;
        }
    }

    private Dimension parseDimensionInfo(String value) {
        String[] v = value.split("\\s*,\\s*");
        return new Dimension(Integer.parseInt(v[0]), Integer.parseInt(v[1].trim()));
    }

    private Point parsePointInfo(String value) {
        String[] v = value.split("\\s*,\\s*");
        return new Point(Integer.parseInt(v[0]), Integer.parseInt(v[1].trim()));
    }

    private Rectangle parseRectangleInfo(String value) {
        String[] v = value.split("\\s*,\\s*");
        return new Rectangle(Integer.parseInt(v[0]), Integer.parseInt(v[1]),
            Integer.parseInt(v[2]), Integer.parseInt(v[3].trim()));
    }

    private Font parseFontInfo(String value) {
        String[] v = value.split("\\s*,\\s*");
        return new Font(v[0], Font.PLAIN, Integer.parseInt(v[1].trim()));
    }

    /**
     * Form a BufferedImage from a image file.
     *
     * @param dimension
     */
    public BufferedImage getBufferedImageFromFile(File iconFile) {
        /*
        Image image = Toolkit.getDefaultToolkit().getImage(iconFile.toString());
        MediaTracker tracker = new MediaTracker(this);
        tracker.addImage(image, 0);
        try {
            tracker.waitForID(0);
        } catch (InterruptedException e) {
        }
        return toBufferedImage(image);
        */

        try {
            FileInputStream in = new FileInputStream(iconFile);
            ImageInputStream iin = ImageIO.createImageInputStream(in);
            ImageReader reader = ImageIO.getImageReaders(iin).next();
            reader.setInput(iin, true, true);
            BufferedImage image = reader.read(0);
            iin.close();
            return image;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getMD5Hash(String value)
    {
        try {
            final StringBuilder sbMd5Hash = new StringBuilder();
            final MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(value.getBytes("UTF-8"));
            final byte data[] = m.digest();
            for (byte element : data) {
                sbMd5Hash.append(Character.forDigit((element >> 4) & 0xf, 16));
                sbMd5Hash.append(Character.forDigit(element & 0xf, 16));
            }
            return sbMd5Hash.toString();
        } catch (Exception e) {
            return null;
        }
    }

    private BufferedImage toCompatibleImage(BufferedImage image)
    {
        int w = image.getWidth();
        int h = image.getHeight();
        int transparency = image.getColorModel().getTransparency();
        BufferedImage result = gc.createCompatibleImage(w, h, transparency);
        Graphics2D g2 = result.createGraphics();
        g2.drawRenderedImage(image, null);
        g2.dispose();
        return result;
    }

    private BufferedImage copy(BufferedImage source, BufferedImage target)
    {
        Graphics2D g2 = target.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        double scalex = (double) target.getWidth()/ source.getWidth();
        double scaley = (double) target.getHeight()/ source.getHeight();
        AffineTransform xform = AffineTransform.getScaleInstance(scalex, scaley);
        g2.drawRenderedImage(source, xform);
        g2.dispose();
        return target;
    }

    private BufferedImage getScaledInstance(BufferedImage image, int width, int height)
    {
        int transparency = image.getColorModel().getTransparency();
        return copy(image, gc.createCompatibleImage(width, height, transparency));
    }

    /**
     * Form a BufferedImage from a image file with the given scaled size. It'll first try to
     * find the scaled image from cache directory.
     */
    private BufferedImage getBufferedImageFromFile(String imageFile, int width, int height) {
        try {
            String cacheFilename = "cache" + File.separator + getMD5Hash(imageFile + width + "x" + height)
                + ".png";
            File cacheFile = new File(cacheFilename);
            if (cacheFile.exists()) {
                System.out.println("Get image of " + imageFile + "from cache file: " + cacheFilename);
                return toCompatibleImage(ImageIO.read(cacheFile));
            }

            BufferedImage img;
            if (imageFile.startsWith("http")) {
                System.out.println("Get image from URL: " + imageFile);
                img = toCompatibleImage(ImageIO.read(new URL(imageFile)));
            } else {
                System.out.println("Get image from file: " + imageFile);
                img = toCompatibleImage(ImageIO.read(new File(imageFile)));
            }
            img = getScaledInstance(img, width, height);
            ImageIO.write(img, "png", cacheFile);
            return img;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    /**
     * 查mplayer slave mode command說明：
     * <pre>
     * gzip -d -c /usr/share/doc/mplayer-doc/tech/slave.txt.gz|less
     * </pre>
     */
    public void playVedio() {
        try {
            while (!isShowing())
                Thread.sleep(2000);

            // ONLY WORK ON LINUX
            Process p = Runtime.getRuntime().exec("xwininfo -all -name HYI_CUST_FRAME");
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            String videoPanelLine = "";
            while ((line = br.readLine()) != null)
                if (line.contains("PanelPeer"))
                    videoPanelLine = line; // get the last line
            br.close();
            final String videoPanelWid = videoPanelLine.trim().split("\\s")[0];
            System.out.println("VideoPanelWid: " + videoPanelWid);

            new Thread() {
                public void run() {
                    try {
                        for (; ;) {
                            final String mplayerCommand = "nice -n " + getPlayerNiceValue()
                                + " /usr/bin/mplayer -ontop -loop 2 -wid "
                                + videoPanelWid
                                + " -vo xv -af volume=10.1 -framedrop"
                                + " -slave -aspect 4:3" // -aspect 4:3 -noconsolecontrols
                                + " -ao oss"
                                + " -zoom" // + " -y " + videoPanel.getHeight()
                                + " -playlist ./videos/playlist.txt";
                            System.out.println(mplayerCommand);

                            Process playerProcess = Runtime.getRuntime().exec(mplayerCommand);
                            final InputStream stdout = playerProcess.getInputStream();
                            videoCommandWriter = new OutputStreamWriter(playerProcess
                                .getOutputStream(), "latin1");
                            try {
                                byte[] bytes = new byte[512];
                                for (; ;)
                                    if (stdout.read(bytes) == -1)
                                        break;
                            }
                            catch (IOException e) {
                                e.printStackTrace();
                            }
                            playerProcess.waitFor();
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Send video command to mplayer.
     */
    public void sendVideoCommand(String command) {
        try {
            videoCommandWriter.write(command);
            videoCommandWriter.write('\n');
            videoCommandWriter.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void volumeUp() {
        sendVideoCommand("volume +1");
    }

    public void volumeDown() {
        sendVideoCommand("volume -1");
    }

    public void brightnessUp() {
        sendVideoCommand("brightness +7");
    }

    public void brightnessDown() {
        sendVideoCommand("brightness -7");
    }

    public void contrastUp() {
        sendVideoCommand("contrast +7");
    }

    public void contrastDown() {
        sendVideoCommand("contrast -7");
    }

    public void playPreviousVideo() {
        sendVideoCommand("pt_step -1");
    }

    public void playNextVideo() {
        sendVideoCommand("pt_step +1");
    }

    /*public void transactionChanged(TransactionEvent e)
    {
        if (itemList != null)
            itemList.transactionChanged(e);
        if (tenderList != null)
            tenderList.transactionChanged(e);
    }*/

    public int getPlayerNiceValue() {
        return playerNiceValue;
    }

    public void setPlayerNiceValue(int playerNiceValue) {
        this.playerNiceValue = playerNiceValue;
    }

    public static void main(String[] args) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        System.out.println("Device count:" + gs.length);
        for (int j = 0; j < gs.length; j++) {
            GraphicsDevice gd = gs[j];
            System.out.println("Device id:" + gd.getIDstring());
            if (gd.getIDstring().equals(":0.0")) {
                SecondScreen secondScreen = new SecondScreen("HYI_CUST_FRAME", gd.getDefaultConfiguration());
                secondScreen.setVisible(true);
                secondScreen.playVedio();
                //custDispFrame.playFlash();
            }
        }
    }
}
