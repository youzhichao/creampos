
package hyi.cream.uibeans;


/**
 * 折扣键(Select Item).
 */
public class SIButton extends POSButton {
    private String siID;

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param siLabel SI label on button.
     * @param siID SI ID.
     */
	public SIButton(int row, int column, int level, String siLabel, String siID) {
		super(row, column, level, siLabel);
        this.siID = siID;
    }
    
    public SIButton(int row, int column, int level, String siLabel, int keyCode, String siID) {
		super(row, column, level, siLabel, keyCode);
        this.siID = siID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SIButton siButton = (SIButton)o;
        if (siID != null ? !siID.equals(siButton.siID) : siButton.siID != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (siID != null ? siID.hashCode() : 0);
        return result;
    }

    @Override
    protected String paramString() {
        return siID;
    }

    public String getSiID() {
        return new String(siID);
    }
}


 