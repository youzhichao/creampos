package hyi.cream.uibeans;

public class WholesaleClearingButton extends POSButton {

	private static final long serialVersionUID = 1L;

	/**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param label wholesaleclearing label on button.
     */
	public WholesaleClearingButton(int row, int column, int level, String label) {
        super(row, column, level, label);
	}

	public WholesaleClearingButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
	}
}
