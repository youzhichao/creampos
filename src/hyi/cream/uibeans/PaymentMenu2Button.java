package hyi.cream.uibeans;

import java.util.*;
import java.awt.event.*;
import hyi.cream.util.*;
import hyi.cream.*;
import hyi.cream.event.*;
//import jpos.events.*;
//import jpos.*;
import hyi.spos.JposException;
import hyi.spos.POSKeyboard;
import hyi.spos.events.DataEvent;

/**
 * 支付Menu键
 */
public class PaymentMenu2Button extends POSButton implements ActionListener, PopupMenuListener {
    private String payID;
    private ArrayList menuArrayList = new ArrayList();
    private PopupMenuPane popupMenu = PopupMenuPane.getInstance();
    private ArrayList memberArrayList = new ArrayList();
    private ArrayList allArrayList = new ArrayList();

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param payMenuLabel pay menu label on button.
     * @param defFilename definition file of the pay menu.
	public PaymentMenu2Button(int row, int column, int level, String paymentMenuLabel, String payID) {
        super(row, column, level, paymentMenuLabel, payID);
        this.payID = payID;
    }
     */
    public PaymentMenu2Button(int row, int column, int level, String paymentMenu2Label) {
        super(row, column, level, paymentMenu2Label);
    }


    public PaymentMenu2Button(int row, int column, int level, String paymentMenu2Label, String payID) {
        super(row, column, level, paymentMenu2Label);
        this.payID = payID;
        loadPayment();
    }

	public PaymentMenu2Button(int row, int column, int level, String payLabel, int keyCode, String payID) {
		super(row, column, level, payLabel, keyCode);
        this.payID = payID;
        loadPayment();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PaymentButton that = (PaymentButton)o;
        if (payID != null ? !payID.equals(that.payID) : that.payID != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (payID != null ? payID.hashCode() : 0);
        return result;
    }

    @Override
    protected String paramString() {
        return payID;
    }

    public String getPaymentID() {
        return popupMenu.getSelectedPartText(2);
    }

    private void loadPayment() {
    	try {
	    	String sql = "SELECT payid, paycname, paytype FROM payment WHERE paycategory = '" + 
					payID + "' ORDER BY payid";
	    	java.util.List list = CreamToolkit.getResultData2(sql);
	    	int count = 0;
	    	for (Iterator it = list.iterator(); it.hasNext();) {
	    		count++;
	    		Map record = (Map) it.next();
	    		String paytype = (String) record.get("paytype");
	    		while (paytype.length() < 8)
	    			paytype += "0" + paytype;
	    		if (paytype.charAt(2) == '1')
	    			continue;
	    		if (record.get("payid").toString().equals(payID)) 
		    		allArrayList.add(String.valueOf(count) +  "." + 
		    				(String) record.get("paycname") + "," +
							(String) record.get("payid"));
	    		else
		    		memberArrayList.add(String.valueOf(count) +  "." + 
		    				(String) record.get("paycname") + "," +
							(String) record.get("payid"));
	    	}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    public void clear() {
		popupMenu.setIndex(-1);
	}

    public ArrayList getMenu() {
		return menuArrayList;
	}

	public void setPopMenu(PopupMenuPane p) {
    	popupMenu = p;
	}

	public void actionPerformed(ActionEvent e) {
        if (!POSTerminalApplication.getInstance().getCrdPaymentMenuVisible()
            || POSTerminalApplication.getInstance().getKeyPosition() != 2) {
            return;
        }
        popupMenu.setMenu(menuArrayList);
		//popupMenu.setBounds(215, 240, 291, 350);
        //!!!!!Bruce
		//popupMenu.setModal(true);
		popupMenu.setSelectMode(0);
        popupMenu.setVisible(true);
        if (!popupMenu.getSelectedMode()) {
			return;
		}
		super.actionPerformed(e);
		//firePOSButtonEvent(new POSButtonEvent(this));
	}

	public void dataOccurred(DataEvent e) {

        if (!POSTerminalApplication.getInstance().getChecked()
            && POSTerminalApplication.getInstance().getScanCashierNumber()) {
            return;
        }

        if (!POSTerminalApplication.getInstance().getCrdPaymentMenuVisible()
        		|| !(POSTerminalApplication.getInstance().getKeyPosition() == 2
                || POSTerminalApplication.getInstance().getKeyPosition() == 3)) {
            return;
        }
        
		try {
			POSKeyboard p = (POSKeyboard)e.getSource();
			if (getKeyCode() == p.getPOSKeyData()) {
				popupMenu = PopupMenuPane.getInstance();
				if (popupMenu.isVisible()) {// || !popupMenu.isEnabled() ) {
					return;//
				}
				if (!POSTerminalApplication.getInstance().getCrdPaymentMenuVisible())
					return;
				menuArrayList.clear();
		        if (POSTerminalApplication.getInstance().getCurrentTransaction().getMemberID() == null
		        		|| POSTerminalApplication.getInstance().getCurrentTransaction().getMemberID().trim().equals(""))
		        {
//		        	POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
//		        			CreamToolkit.GetResource().getString("OnlyMemberUseCrd"));
		        	menuArrayList.addAll(allArrayList);
		        } else {
		        	menuArrayList.addAll(allArrayList);
		        	menuArrayList.addAll(memberArrayList);
		        }
				popupMenu.setMenu(menuArrayList);
				popupMenu.centerPopupMenu();
				//popupMenu.setModal(true);
				popupMenu.setSelectMode(0);
				//StateMachine.getInstance().setEventProcessEnabled(false);
				//popupMenu.setEventEnable(false);
				popupMenu.setVisible(true);
				if (!popupMenu.isVisible()) {
					//System.out.println(">>>>>>>$$$$$>>>>>>>>");
					firePOSButtonEvent(new POSButtonEvent(this));
				} else {
					popupMenu.setPopupMenuListener(this);
				}
            }
        } catch (JposException ex) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Jpos exception at " + this);
        }
    }

    public void menuItemSelected() {
        if (popupMenu.getSelectedMode()) {
            firePOSButtonEvent(new POSButtonEvent(this));
		}
    }

}

