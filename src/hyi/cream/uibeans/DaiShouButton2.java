/*
 * Created on 2003-6-17
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.uibeans;

import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;  

//import jpos.*;
//import jpos.events.*;

/**
 * @author Administrator
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DaiShouButton2 extends POSButton {
	private ResourceBundle res = CreamToolkit.GetResource();
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ArrayList menu = new ArrayList();                                 
	private static int selectItem = 0;
	private int level = 0;
	private int daishouNumber = 0;

	/**
	 * 
	 */
	public DaiShouButton2() {
		super();
	}

	/**
	 * @param row
	 * @param column
	 * @param level
	 * @param label
	 * @param keyCode
	 */
	public DaiShouButton2(int row, int column, int level, String label, int keyCode) {
		super(row, column, level, label, keyCode);
	}

	/**
	 * @param row
	 * @param column
	 * @param level
	 * @param label
	 */
	public DaiShouButton2(int row, int column, int level, String label) {
		super(row, column, level, label);
	}


}
