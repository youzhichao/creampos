package hyi.cream.uibeans;

import java.util.*;
import hyi.cream.*;
import hyi.cream.dac.*;
/**
 * @author Administrator
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class OverridePopupMenu implements PopupMenuListener {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private LineItem lineItem;
    private PopupMenuPane popupMenu = null;
    private ArrayList menu = new ArrayList();
    
    public OverridePopupMenu(LineItem _lineItem) {
		this.lineItem = _lineItem;
    }
    
	public void popupMenu() {
		popupMenu = PopupMenuPane.getInstance();
		
		if (popupMenu.isVisible()) {
			return;
		}
	    menu = getMenu();
        if (menu == null || menu.isEmpty())
            return;
		popupMenu.setMenu(menu);
		popupMenu.centerPopupMenu();
		popupMenu.setSelectMode(0);
		popupMenu.setVisible(true);
		if (!popupMenu.isVisible()) {
            menu.clear();
		} else {
			popupMenu.setPopupMenuListener(this);
		}
	}
	
	protected ArrayList getMenu() {
		ArrayList list = new ArrayList();
		// 18 : 变价原因
		int i = 1;
		Iterator it = Reason.queryByreasonCategory("18");
		if (it != null) {
			while (it.hasNext()) {
				Reason r = (Reason) it.next();
				String name = r.getreasonName();
                if (name != null && !name.trim().equals("")) {
                    list.add(i + ". " + name + "/" + r.getreasonNumber());
                    i = i + 1;
                }
			}
		}
		return list;
	}
	

    public void menuItemSelected() {
        if (popupMenu.getSelectedMode()) {
            int selectIndex = popupMenu.getSelectedNumber();
            String reasonID = (String)menu.get(selectIndex);
            System.out.println("reasonID : " + reasonID);

            StringTokenizer tk = new StringTokenizer(reasonID, " /");
            try {
                tk.nextToken();
                tk.nextToken();
                reasonID = tk.nextToken();

                String disCno = lineItem.getDiscountNumber();
                if (disCno != null && !disCno.trim().equals("")) { 
                    int index = disCno.indexOf("O");
                    if (index >= 0) {
                    	String left = disCno.substring(0, index);
                    	String right = disCno.substring(index, disCno.length());
                    	if (right.indexOf(",") > 0) {
                    		right = right.substring(right.indexOf(",") + 1
                    				, right.length());
                    	} else {
                    		right = "";
                    		if (left.length() > 0)
                    			left = left.substring(0, left.length() - 1);
                    	}
                    	disCno = left + right;
                    }
					if (!disCno.equals(""))
                    	disCno += ",";

                    index = disCno.indexOf("M");
                    if (index >= 0) {
                    	String left = disCno.substring(0, index);
                    	String right = disCno.substring(index, disCno.length());
                    	if (right.indexOf(",") > 0) {
                    		right = right.substring(right.indexOf(",") + 1
                    				, right.length());
                    	} else {
                    		right = "";
                    		if (left.length() > 0)
                    			left = left.substring(0, left.length() - 1);
                    	}
                    	disCno = left + right;
                    }
					if (!disCno.equals(""))
                    	disCno += ",";
				} else {
                	disCno = "";
                }
            	disCno += "O" + reasonID;
            	lineItem.setDiscountNumber(disCno);

            } catch (NoSuchElementException e) {}
            menu.clear();
		} else {
			System.out.println("select nothing ...");
			popupMenu();
		}
    }
}
