
package hyi.cream.uibeans;
//
//import jpos.*;
//import jpos.events.*;
import java.lang.*;
import hyi.cream.event.*;

public class NumberButton extends POSButton {

    private String numberLabel;

    /**
     * Constructor.
     * @param row row position
     * @param column column position
     * @param numberLabel a string represents the number, such as, "0", "1",
     *          ".", "00", etc.
     */
	public NumberButton(int row, int column, int level, String numberLabel) {
		super(row, column, level, numberLabel);
        this.numberLabel = numberLabel;
	}

    public NumberButton(int row, int column, int level, String numberLabel, int keyCode) {
		super(row, column, level, numberLabel, keyCode);
		this.numberLabel = numberLabel;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        NumberButton that = (NumberButton)o;
        if (numberLabel != null ? !numberLabel.equals(that.numberLabel) : that.numberLabel != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (numberLabel != null ? numberLabel.hashCode() : 0);
        return result;
    }

    @Override
    protected String paramString() {
        return numberLabel;
    }

    public String getNumberLabel() {
		return numberLabel;
	}
}
 