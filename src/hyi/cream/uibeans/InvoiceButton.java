package hyi.cream.uibeans;


public class InvoiceButton extends POSButton {
//	implements ActionListener,PopupMenuListener {
	
	private static final long serialVersionUID = 3256440296215556400L;

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
	public InvoiceButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param numberLabel a string represents the button label
     * @param keyCode key code
    */
	public InvoiceButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}
