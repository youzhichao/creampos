package hyi.cream.uibeans;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.touch.TouchPane;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.POSKeyboard;
import hyi.spos.events.DataEvent;

import java.awt.event.ActionListener;

//import jpos.JposException;
//import jpos.POSKeyboard;
//import jpos.events.DataEvent;

public class TouchMenuButton extends POSButton implements ActionListener,
		PopupMenuListener {
	
	private static final long serialVersionUID = 3256440296215556400L;

	private TouchPane touchPanel;
	private String type;
    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
	public TouchMenuButton(int row, int column, int level, String label, String type) {
        super(row, column, level, label);
        this.type = type;
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param numberLabel a string represents the button label
     * @param keyCode key code
    */
	public TouchMenuButton(int row, int column, int level, String label, int keyCode, String type) {
        super(row, column, level, label, keyCode);
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TouchMenuButton that = (TouchMenuButton)o;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    protected String paramString() {
        return type;
    }

	public void dataOccurred(DataEvent e) {
		if (!POSTerminalApplication.getInstance().getChecked()
				&& POSTerminalApplication.getInstance().getScanCashierNumber())
			return;

		if (!POSTerminalApplication.getInstance().getEnabledPopupMenu()
				|| !(POSTerminalApplication.getInstance().getKeyPosition() == 2 || POSTerminalApplication
						.getInstance().getKeyPosition() == 3))
			return;

		try {
			POSKeyboard p = (POSKeyboard) e.getSource();
			if (getKeyCode() == p.getPOSKeyData()) {
				touchPanel = TouchPane.getInstance();
				touchPanel.show("A");
				touchPanel.setVisible(true);
				touchPanel.setLevel(level);
				touchPanel.setType2(type);
				touchPanel.setBackground(this.getBackground());
			}
		} catch (JposException ex) {
			CreamToolkit.logMessage(ex.toString());
			CreamToolkit.logMessage("Jpos exception at " + this);
		} catch (Exception e1) {
			CreamToolkit.logMessage("Error : " + e1.toString());
		}
	}

	public String getType() {
		return type;
	}
	
	public void menuItemSelected() {
		System.out.println("---- menuItemSelected....");
	}
}
