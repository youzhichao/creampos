package hyi.cream.uibeans;

import hyi.cream.SystemInfo;
import hyi.cream.event.SystemInfoEvent;
import hyi.cream.event.SystemInfoListener;
import hyi.cream.exception.ConfigurationNotFoundException;
import hyi.cream.groovydac.Param;
import hyi.cream.inline.Client;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.ChineseConverter;
import hyi.cream.util.ConfReader;

import java.awt.*;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 *  修改说明  /lxf 20030508
 *         screenbanner.conf 有多行，第一有效行是定义font的name & size
 *         之后每3行为一个显示行 
 */
public class Banner extends Canvas implements SystemInfoListener {

    private static final long serialVersionUID = -4547129945450054270L;
    private ArrayList lineTitles = new ArrayList();
    private ArrayList lineFields = new ArrayList();
    private ArrayList lineWidths = new ArrayList();

    private SystemInfo systemInfo;
    // private String backImage = "screenbanner.jpg";
    // Image ig = CreamToolkit.getImage(backImage);
    private Image offscreen;
    private Dimension size = null;
    private Color valueColor = Color.blue.darker().darker();
    private Color headerColor = Color.white;
    private String fontName = "SimHei"; //Param.getInstance().getScreenBannerFont();
    private int fontSize = 16;
    private Image connectedImage;
    private Image disconnectedImage;
    private Color backgroundColor = Color.black;
    private Font fv;
    private Font fh;
    private String align;
    private boolean antiAlias = true;

    public Banner(File propFile, Color bgColor, Color valueColor,
            Color headerColor) throws ConfigurationNotFoundException {
        this(propFile);
        this.backgroundColor = bgColor;
        setBackground(backgroundColor);
        this.headerColor = headerColor;
        this.valueColor = valueColor;
    }

    /**
     * When constructing an ScreenBanner object, it'll first read the
     * configuration file "screenbanner.conf," which defines all the properties
     * of ScreenBanner, including fields and widths of the two displaying lines.
     * The fields are the properties of the associated SystemInfo objects.
     */
    public Banner(File propFile) throws ConfigurationNotFoundException {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        connectedImage = toolkit.getImage(ScreenBanner.class.getResource("connected.gif"));
        disconnectedImage = toolkit.getImage(ScreenBanner.class.getResource("disconnected.gif"));

        MediaTracker tracker = new MediaTracker(this);
        tracker.addImage(connectedImage, 0);
        tracker.addImage(disconnectedImage, 1);
        try {
            tracker.waitForID(0);
            tracker.waitForID(1);
        } catch (InterruptedException e) {
            CreamToolkit.logMessage("Cannot load connected.gif and disconnected.gif");
        }

        try {
            char ch = '#';

            //FileInputStream filein = new FileInputStream(propFile);
            //InputStreamReader inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
            //BufferedReader in = new BufferedReader(inst);

            ConfReader in = new ConfReader(propFile);
            int i = 0;
            String line = "";
            boolean getLine;
            out: while ((line = in.readLine()) != null) {
                getLine = true;
                do {
                    if (!getLine) {
                        line = in.readLine();
                        if (line == null)
                            break out;
                    }
                    getLine = false;
                    while ("".equals(line)) {
                        line = in.readLine();
                        if (line == null)
                            break out;
                    }
                    i = 0;
                    do {
                        ch = line.charAt(i);
                        i++;
                    } while ((ch == ' ' || ch == '\t') && i < line.length());
                } while (ch == '#' || ch == ' ' || ch == '\t');

                String token = "";
                if (line.startsWith("font")) {
                    StringTokenizer t0 = new StringTokenizer(line, ",", true);
                    while (t0.hasMoreTokens()) {
                        token = t0.nextToken();
                        if (token.startsWith("fontName")) {
                            fontName = token.substring("fontName".length() + 1, token.length());

                            // Fix font for simplified Chinese
                            if (!Param.getInstance().tranditionalChinese() && fontName.equals("cwTeXHeiBold"))
                                fontName = "SimHei";

                        } else if (token.startsWith("fontSize")) {
                            try {
                                token = token.substring("fontSize".length() + 1, token.length());
                                fontSize = Integer.parseInt(token);
                            } catch (Exception e) {
                                fontSize = 16;
                            }
                        } else if (token.startsWith("align")) {
                            try {
                                token = token.substring("align".length() + 1, token.length());
                                align = token;
                            } catch (Exception e) {
                                align = "R";
                            }
                        } else if (token.startsWith("antiAlias")) {
                            antiAlias = token.substring("antiAlias".length() + 1,
                                    token.length()).equalsIgnoreCase("yes");
                        }
                    }
                    fv = new Font(fontName, Font.PLAIN, fontSize - 1);
                    fh = new Font(fontName, Font.PLAIN, fontSize); // !!!!!Bruce
                    do {
                        line = in.readLine();
                        while ("".equals(line)) {
                            line = in.readLine();
                        }
                        i = 0;
                        do {
                            ch = line.charAt(i);
                            i++;
                        } while ((ch == ' ' || ch == '\t') && i < line.length());
                    } while (ch == '#' || ch == ' ' || ch == '\t');
                }

                StringTokenizer t1 = new StringTokenizer(line, ",", true);
                ArrayList titleList = new ArrayList();
                while (t1.hasMoreTokens()) {
                    token = t1.nextToken();
                    if (",".equals(token)) {
                        titleList.add("");
                    } else {
                        titleList.add(token);
                        if (t1.hasMoreTokens()) {
                            token = t1.nextToken();
                        }
                    }
                }
                do {
                    line = in.readLine();
                    while ("".equals(line)) {
                        line = in.readLine();
                    }
                    i = 0;
                    do {
                        ch = line.charAt(i);
                        i++;
                    } while ((ch == ' ' || ch == '\t') && i < line.length());
                } while (ch == '#' || ch == ' ' || ch == '\t');

                StringTokenizer t2 = new StringTokenizer(line, ",", true);
                ArrayList fieldList = new ArrayList();
                while (t2.hasMoreTokens()) {
                    token = t2.nextToken();
                    if (",".equals(token)) {
                        fieldList.add("");
                    } else {
                        fieldList.add(token);
                        if (t2.hasMoreTokens()) {
                            token = t2.nextToken();
                        }
                    }
                }

                do {
                    line = in.readLine();
                    while ("".equals(line)) {
                        line = in.readLine();
                    }
                    i = 0;
                    do {
                        ch = line.charAt(i);
                        i++;
                    } while ((ch == ' ' || ch == '\t') && i < line.length());
                } while (ch == '#' || ch == ' ' || ch == '\t');

                StringTokenizer t3 = new StringTokenizer(line, ",", true);
                ArrayList widthList = new ArrayList();
                while (t3.hasMoreTokens()) {
                    token = t3.nextToken();
                    if (",".equals(token)) {
                        widthList.add("");
                    } else {
                        widthList.add(token);
                        if (t3.hasMoreTokens()) {
                            token = t3.nextToken();
                        }
                    }
                }
                lineTitles.add(titleList);
                lineFields.add(fieldList);
                lineWidths.add(widthList);
            }
            // System.out.println("titles : " + lineTitles + " | fields : " +
            // lineFields
            // + " | widths : " + lineWidths);
    
            in.close();
        } catch (NoSuchElementException e) {
            CreamToolkit.logMessage("Format error: " + propFile);
        }
    }

    /**
     * Sets the associated transaction object. It'll also register itself as the
     * system information listener of the system information object. *
     *
     * @param systemInfo
     *            the sytem information object
     */
    public void setSystemInfo(SystemInfo systemInfo) {
        this.systemInfo = systemInfo;
    }

    /**
     * Returns the associated transaction object.
     */
    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    /**
     * Invoked when transaction has been changed.
     *
     * @param e
     *            an event object represents the changes.
     */
    private boolean isChanged = false;

    public void systemInfoChanged(SystemInfoEvent e) {
        isChanged = true;
        repaint();
        /*
         * if
         * (POSTerminalApplication.getInstance().getCurrentTransaction().decreaseLockCount() ==
         * 0) {
         * POSTerminalApplication.getInstance().getCurrentTransaction().setLockEnable(true); }
         */
    }

    public Dimension getPreferredSize() {
        return size;
    }

    public Dimension getMinimumSize() {
        return size;
    }

    public void invalidate() {
        super.invalidate();
        offscreen = null;
    }

    public void update(Graphics g) {
        if (isShowing()) {
            paint(g);
        }
    }

    private java.util.List processHeight(int fontHeight, int lines) {
        java.util.List list = new Vector();
        try {
            int height = getHeight();
            int sub = height - lines * fontHeight;
            int sHeight = 0;
            if (sub > 0) {
                sHeight = Math.round(sub / (lines + 1));
                sub = sub - (lines + 1) * sHeight;
                while (sub > 0) {
                    sub = Math.round(sub / (lines + 1) - 0.5f);
                }
                sHeight += sub;
            }
            list.add(new Integer(sHeight));
            int lastHeight = sHeight;
            for (int i = 0; i < lines - 1; i++) {
                list.add(new Integer(sHeight + lastHeight + fontHeight));
                lastHeight += sHeight + fontHeight;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private java.util.List processWidth(java.util.List wList) {
        java.util.List list = new Vector();
        int totalRate = 100;
        int totalWidth = getWidth();
        int totalSubWidth = totalWidth;
        try {
            for (int i = 0; i < wList.size(); i++) {
                int width = Math.round((new Integer((String) wList.get(i)))
                        .intValue()
                        * totalWidth / totalRate);
                list.add(new Integer(width));
                totalSubWidth -= width;
            }
            int subWidth = 0;
            if (totalSubWidth > 0) {
                subWidth = Math
                        .round(totalSubWidth / (wList.size() + 1) - 0.5f);
            }
            int width = 0;
            for (int i = 0; i < list.size(); i++) {
                width += subWidth;
                int width1 = ((Integer) list.get(i)).intValue();
                HashMap hm = new HashMap();
                hm.put(new Integer(width), new Integer(width1));
                list.set(i, hm);
                width += width1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    // Font f = new Font(fontName, Font.PLAIN, 32);
    // Font f = new Font(fontName, Font.PLAIN, 24);
    public void paint(Graphics g) {
        if (getWidth() <= 0 || getHeight() <= 0) {
            EPOSBackground.getInstance().repaint();
            return;
        }

        ChineseConverter chineseConverter = ChineseConverter.getInstance();
        int startx = 0;
        String printString;
        SimpleDateFormat formatter = CreamCache.getInstance()
                .getDateTimeFormate2();

        if (offscreen == null) {
            offscreen = createImage(getWidth(), getHeight());
        }
        Graphics og = offscreen.getGraphics();
        if (antiAlias)
            ((Graphics2D) og).setRenderingHint(
                    RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        og.setColor(backgroundColor);
        og.setClip(0, 0, getWidth(), getHeight());

        og.fillRect(0, 0, getWidth(), getHeight());

        FontMetrics fm = og.getFontMetrics(fh);

        java.util.List hList = processHeight(fm.getHeight(), lineTitles.size());

        try {
            int line = 0;
            while (line < lineTitles.size()) {
                List titleList = (List) lineTitles.get(line);
                List fieldList = (List) lineFields.get(line);
                List widthList = (List) lineWidths.get(line);
                List wList = processWidth(widthList);

                for (int i = 0; i < fieldList.size(); i++) {
                    // 取值
                    String propertyName = fieldList.get(i).toString();
                    if ("Line".equals(propertyName)) {
                        og.drawLine(0, ((Integer) hList.get(line)).intValue(),
                                getWidth(), ((Integer) hList.get(line))
                                        .intValue());
                        continue;
                    }
                    Method method = SystemInfo.class.getDeclaredMethod("get"
                            + propertyName, new Class[0]);
                    Object retObject = method.invoke(systemInfo, new Object[0]);
                    // System.out.println("method = " + FirstLineFields.get(i));
                    HashMap hm = (HashMap) wList.get(i);
                    Iterator it = hm.keySet().iterator();
                    int width = 0;
                    if (it.hasNext()) {
                        startx = ((Integer) it.next()).intValue();
                        width = ((Integer) hm.get(new Integer(startx)))
                                .intValue();
                    }

                    // 绘制Header
                    printString = titleList.get(i).toString();
                    if (printString.equals("null"))
                        printString = "";
                    og.setColor(headerColor);
                    og.setFont(fh);
                    og.drawString(chineseConverter.convert(printString), startx, fm.getAscent()
                            + ((Integer) hList.get(line)).intValue());

                    startx = startx + fm.stringWidth(printString);

                    // 显示处理
                    if (retObject instanceof Date) {
                        printString = formatter.format(retObject);
                    } else {
                        // System.out.println(i);
                        if (retObject == null)
                            printString = "";
                        else
                            printString = retObject.toString();
                    }

                    int stri = printString.length();
                    // int in = Integer.parseInt((String)FirstLineWidths.get(i))
                    // * 7;
                    int stringlength = fm.stringWidth(printString)
                            + fm.stringWidth(titleList.get(i).toString());

                    // 超长处理
                    if (align == null || !align.equalsIgnoreCase("skip"))
                        while (stringlength > width) {
                            printString = printString.substring(0, stri);
                            stringlength = fm.stringWidth(printString);
                            stri--;
                        }

                    // 绘制Value
                    // System.out.println("head get font : " + og.getFont());
                    og.setColor(valueColor);
                    og.setFont(fv);

                    if (align != null && align.equalsIgnoreCase("R"))
                        og.drawString(printString, width - stringlength, fm
                                .getAscent()
                                + ((Integer) hList.get(line)).intValue());
                    else
                        og.drawString(printString, startx, fm.getAscent()
                                + ((Integer) hList.get(line)).intValue());

                    if ("Connected".equals(propertyName)) {
                        if (Client.getInstance().isConnected()) {
                            og.drawImage(connectedImage, startx
                                    + fm.stringWidth(printString),
                                    ((Integer) hList.get(line)).intValue() - 3,
                                    this);
                        } else {
                            og.drawImage(disconnectedImage, startx
                                    + fm.stringWidth(printString),
                                    ((Integer) hList.get(line)).intValue() - 3,
                                    this);
                        }
                        if (!SystemInfo.isWanConnected()) {
                            String wanString = SystemInfo.getWanConnected();
                            og.drawString(wanString,
                                    startx + disconnectedImage.getWidth(null) + fm.stringWidth(printString),
                                    fm.getAscent() + ((Integer) hList.get(line)).intValue());
                            og.drawImage(disconnectedImage, startx + disconnectedImage.getWidth(null)
                                            + fm.stringWidth(printString) + fm.stringWidth(wanString)
                                    , ((Integer) hList.get(line)).intValue() - 3, this);

                        } else {
                            String wanString = SystemInfo.getWanConnected();
                            og.drawString(wanString,
                                    startx + connectedImage.getWidth(null) + fm.stringWidth(printString),
                                    fm.getAscent() + ((Integer) hList.get(line)).intValue());
                            og.drawImage(connectedImage, startx + connectedImage.getWidth(null)
                                            + fm.stringWidth(printString) + fm.stringWidth(wanString)
                                    , ((Integer) hList.get(line)).intValue() - 3, this);

                        }
                    }
                }
                line++;
            }
            try {
                g.drawImage(offscreen, 0, 0, this);
                og.dispose();
            } catch (Exception e) {}
        } catch (NoSuchMethodException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InvocationTargetException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }

//        // for transaction event forward
//        if (isChanged) {
//            if (POSTerminalApplication.getInstance().getCurrentTransaction()
//                    .decreaseLockCount() == 0) {
//                POSTerminalApplication.getInstance().getCurrentTransaction()
//                        .setLockEnable(true);
//            }
//            isChanged = false;
//        }
    }
}
