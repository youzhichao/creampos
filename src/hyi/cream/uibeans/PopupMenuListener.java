package hyi.cream.uibeans;

/**
 * Popup menu listener.
 */
public interface PopupMenuListener {
    public void menuItemSelected();
}
 
