
package hyi.cream.uibeans;

/**
 * 交易取消键.
 */
public class CancelButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param cancelLabel cancel label on button.
     */
    public CancelButton(int row, int column, int level, String cancelLabel) {
		super(row, column, level, cancelLabel);
	}

	public CancelButton(int row, int column, int level, String cancelLabel, int keyCode) {
		super(row, column, level, cancelLabel, keyCode);
	}
}

 