
package hyi.cream.uibeans;

/**
 * 来客数键.
 */
public class CustomNumberButton extends POSButton {

	/**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
	 * @param label label on button.
     */
	public CustomNumberButton(int row, int column, int level, String label) {
		super(row, column, level, label);
    }
}

 