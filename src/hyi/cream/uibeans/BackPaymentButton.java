package hyi.cream.uibeans;

/**
 * 訂金尾款/着付殘金 按鍵.
 */
public class BackPaymentButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param label clear label on button.
     */
    public BackPaymentButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    public BackPaymentButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}