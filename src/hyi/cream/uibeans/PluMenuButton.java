package hyi.cream.uibeans;

import hyi.cream.POSTerminalApplication;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.POSKeyboard;
import hyi.spos.events.DataEvent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

//import jpos.JposException;
//import jpos.POSKeyboard;
//import jpos.events.DataEvent;

/**
 * PLU Menu键
 */
public class PluMenuButton extends POSButton implements ActionListener,
		PopupMenuListener {
	private String defFilename;

	private PopupMenuPane popupMenu = PopupMenuPane.getInstance();

	private ArrayList menuArrayList = new ArrayList();

	/**
	 * Constructor.
	 * 
	 * @param row
	 *            row position.
	 * @param column
	 *            column position.
	 * @param pluMenuLabel
	 *            PLU menu label on button.
	 * @param defFilename
	 *            definition file of the PLU menu.
	 */
	public PluMenuButton(int row, int column, int level, String pluMenuLabel,
			String defFilename) {
		super(row, column, level, pluMenuLabel);
		this.defFilename = defFilename;
		setDefFile(defFilename);
	}

	public PluMenuButton(int row, int column, int level, String pluMenuLabel,
			int keyCode, String defFileName) {
		super(row, column, level, pluMenuLabel, keyCode);
		this.defFilename = defFileName;
		setDefFile(defFileName);
	}

	public PluMenuButton(int row, int column, int level, String pluMenuLabel,
			ArrayList menuArrayList) {
		super(row, column, level, pluMenuLabel);
		this.menuArrayList = menuArrayList;
	}

	public PluMenuButton(int row, int column, int level, String pluMenuLabel) {
		super(row, column, level, pluMenuLabel);
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PluMenuButton that = (PluMenuButton)o;
        if (defFilename != null ? !defFilename.equals(that.defFilename) : that.defFilename != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (defFilename != null ? defFilename.hashCode() : 0);
        return result;
    }

    @Override
    protected String paramString() {
        return defFilename;
    }

	public String getPluNo() {
		return popupMenu.getPluNumber();
	}

	public void clear() {
		popupMenu.setIndex(-1);
	}

	public ArrayList getMenu() {
		return menuArrayList;
	}

	public void setPopMenu(PopupMenuPane p) {
		popupMenu = p;
	}

	public void setDefFile(String defFilename) {
		this.defFilename = defFilename;
		if (this.defFilename.indexOf(File.separator) == -1)
			this.defFilename = CreamToolkit.getConfigDir() + this.defFilename;
		File propFile = new File(this.defFilename);
		try {
			FileInputStream filein = new FileInputStream(propFile);
			InputStreamReader inst = null;
			inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
			BufferedReader in = new BufferedReader(inst);
			String line;
			char ch = ' ';
			int i;

			do {
				do {
					line = in.readLine();
					if (line == null) {
						break;
					}
					while (line.equals("")) {
						line = in.readLine();
					}
					i = 0;
					do {
						ch = line.charAt(i);
						i++;
					} while ((ch == ' ' || ch == '\t') && i < line.length());
				} while (ch == '#' || ch == ' ' || ch == '\t');

				if (line == null) {
					break;
				}
				menuArrayList.add(line);
			} while (true);
		} catch (FileNotFoundException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("File is not found: " + propFile + ", at "
					+ this);
		} catch (IOException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("IO exception at " + this);
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (!POSTerminalApplication.getInstance().getEnabledPopupMenu()
				|| POSTerminalApplication.getInstance().getKeyPosition() != 2) {
			return;
		}
		popupMenu.setMenu(menuArrayList);
		// popupMenu.setBounds(215, 240, 291, 350);
		//!!!!!Bruce
		//popupMenu.setModal(true);
		popupMenu.setSelectMode(0);
		popupMenu.setVisible(true);
		if (!popupMenu.getSelectedMode()) {
			return;
		}
		super.actionPerformed(e);
		// firePOSButtonEvent(new POSButtonEvent(this));
	}

	public void dataOccurred(DataEvent e) {
//		System.out.println("---- PluMenuButton | dataOccurred....");
		if (!POSTerminalApplication.getInstance().getChecked()
				&& POSTerminalApplication.getInstance().getScanCashierNumber()) {
			return;
		}

		if (!POSTerminalApplication.getInstance().getEnabledPopupMenu()
				|| !(POSTerminalApplication.getInstance().getKeyPosition() == 2 || POSTerminalApplication
						.getInstance().getKeyPosition() == 3)) {
			return;
		}

		try {
			POSKeyboard p = (POSKeyboard) e.getSource();
			if (getKeyCode() == p.getPOSKeyData()) {
				showMenuList();
			}
		} catch (JposException ex) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("Jpos exception at " + this);
		}
	}

	public void showMenuList() {
		popupMenu = PopupMenuPane.getInstance();
		if (popupMenu.isVisible()) {// || !popupMenu.isEnabled() ) {
			return;//
		}
		popupMenu.setMenu(menuArrayList);
		popupMenu.centerPopupMenu();
		// popupMenu.setModal(true);
		popupMenu.setSelectMode(0);
		// StateMachine.getInstance().setEventProcessEnabled(false);
		// popupMenu.setEventEnable(false);
		popupMenu.setVisible(true);
		if (!popupMenu.isVisible()) {
			// System.out.println(">>>>>>>$$$$$>>>>>>>>");
			firePOSButtonEvent(new POSButtonEvent(this));
		} else {
			popupMenu.setPopupMenuListener(this);
		}
	}
	
	public void menuItemSelected() {
		if (popupMenu.getSelectedMode()) {
			firePOSButtonEvent(new POSButtonEvent(this));
		}
	}

	/*
	 * public void keyReleased(KeyEvent e) { }
	 * 
	 * public void keyPressed(KeyEvent e) { char keyChar = e.getKeyChar(); int
	 * prompt = (int)keyChar; System.out.println(e.getSource() + "++++"); }
	 * 
	 * public void keyTyped(KeyEvent e) { char keyChar = e.getKeyChar(); int
	 * prompt = (int)keyChar; /*if (keyChar == ';' || keyChar == '%') { msr =
	 * true; return; } System.out.println(prompt +"|" + this); }
	 */
}
