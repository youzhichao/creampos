package hyi.cream.uibeans;

public class WholesaleButton extends POSButton {

	private static final long serialVersionUID = 1L;

	/**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param label wholesale label on button.
     */
	public WholesaleButton(int row, int column, int level, String label) {
        super(row, column, level, label);
	}

	public WholesaleButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
	}
}
