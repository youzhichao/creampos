
package hyi.cream.uibeans;

/**
 *  银联键.
 */
public class UnionPayButton extends POSButton {
    String payID;

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param payLabel pay label on button.
     * @param payID pay ID.
     */
	public UnionPayButton(int row, int column, int level, String payLabel, String payID) {
        super(row, column, level, payLabel);
        this.payID = payID;
	}

	public UnionPayButton(int row, int column, int level, String payLabel, int keyCode, String payID) {
		super(row, column, level, payLabel, keyCode);
        this.payID = payID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UnionPayButton that = (UnionPayButton)o;
        if (payID != null ? !payID.equals(that.payID) : that.payID != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (payID != null ? payID.hashCode() : 0);
        return result;
    }

    @Override
    protected String paramString() {
        return payID;
    }

    public String getPaymentID() {
        return payID;
    }
}

 