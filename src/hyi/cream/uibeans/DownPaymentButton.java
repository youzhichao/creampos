package hyi.cream.uibeans;

/**
 * 訂金/着付手付金 按鍵.
 */
public class DownPaymentButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param label clear label on button.
     */
    public DownPaymentButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    public DownPaymentButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}