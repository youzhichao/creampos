package hyi.cream.alipay;

import hyi.cream.dac.Store;
import hyi.cream.util.CreamToolkit;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author : liupingping
 *
 * 支付宝接口
 */
public class AlipayHttpPostRequest {

    public static  String sign_type = "MD5"; // 签名方式
    public static String format_type = "xml"; //请求格式
    public static String _input_charset = "GBK";//参数编码字符集
    public static String key = Store.getAlipay_security_code();//商户密钥
    public static String partner = Store.getAlipay_partner_id();
    public static String dynamic_id_type = "barcode";  // soundwave：声波 qrcode：二维码 barcode：条码

    //SOUNDWAVE_PAY_OFFLINE：声波支付  FINGERPRINT_FAST_PAY：指纹支付 BARCODE_PAY_OFFLINE：条码支付
    public static String product_code = "BARCODE_PAY_OFFLINE";
    public static String notify_url = "";
    public static String it_b_pay = "5m";

    public static String  getPropery() {

        if (key == null || key.equals(""))
            return null;
        CreamToolkit.logMessage("Alipay_security_code：" + key);
        if (partner == null || partner.equals(""))
            return null;
        CreamToolkit.logMessage("Alipay_partner_id：" + partner);
            return "";
    }

    //下单和并支付
    public static String  getCreateAndPay(String out_trade_no,String total_fee,String dynamic_id,String body,String bak,
                                          String goods_detail, String extend_params) {

        CreamToolkit.logMessage("调用" + bak + "支付接口----------------");
        String  service = "alipay.acquire.createandpay";//接口名称
        String subject = "教育超市 "+ Store.getStoreChineseName() + " 消费";
        //把请求参数打包成数组
        Map sParaTemp = new HashMap();
        sParaTemp.put("service", service);
        sParaTemp.put("partner", partner);
        sParaTemp.put("_input_charset", _input_charset);
        //sParaTemp.put("seller_email", seller_email);
        sParaTemp.put("out_trade_no", out_trade_no);
        sParaTemp.put("subject", subject);
        sParaTemp.put("total_fee", total_fee);
        sParaTemp.put("product_code", product_code);
        sParaTemp.put("dynamic_id_type", dynamic_id_type);
        sParaTemp.put("dynamic_id", dynamic_id);
        sParaTemp.put("body",body);
        sParaTemp.put("notify_url",notify_url);
        sParaTemp.put("it_b_pay",it_b_pay);
        sParaTemp.put("format_type",format_type);
        sParaTemp.put("goods_detail",goods_detail);
        sParaTemp.put("extend_params",extend_params);
        String sHtmlText = "";
        try {
            //建立请求
            sHtmlText = AlipaySubmit.buildRequest("", "", sParaTemp);
        } catch (Exception e)  {
            e.printStackTrace();
        }
        CreamToolkit.logMessage(sHtmlText);
        return sHtmlText;
    }

    //退款
    public static String  getRefund(String out_trade_no,String refund_amount,String bak) {

        CreamToolkit.logMessage("调用" + bak + "退款接口----------------");
        String  service = "alipay.acquire.refund";//接口名称
        //把请求参数打包成数组
        Map sParaTemp = new HashMap();
        sParaTemp.put("service", service);
        sParaTemp.put("partner", partner);
        sParaTemp.put("_input_charset", _input_charset);
        sParaTemp.put("out_trade_no", out_trade_no);
        sParaTemp.put("refund_amount",refund_amount);
        String sHtmlText = "";
        try {
            //建立请求
            sHtmlText = AlipaySubmit.buildRequest("", "", sParaTemp);
        } catch (Exception e)  {
            e.printStackTrace();
        }
        CreamToolkit.logMessage(sHtmlText);
        return sHtmlText;
    }

    //撤销
    public static String  getCancel(String out_trade_no,String bak) {

        CreamToolkit.logMessage("调用" + bak + "撤销接口----------------");
        String  service = "alipay.acquire.cancel";//接口名称
        //把请求参数打包成数组
        Map sParaTemp = new HashMap();
        sParaTemp.put("service", service);
        sParaTemp.put("partner", partner);
        sParaTemp.put("_input_charset", _input_charset);
        sParaTemp.put("out_trade_no", out_trade_no);
        String sHtmlText = "";
        try {
            //建立请求
            sHtmlText = AlipaySubmit.buildRequest("", "", sParaTemp);
        } catch (Exception e)  {
            e.printStackTrace();
        }
        CreamToolkit.logMessage(sHtmlText);
        return sHtmlText;
    }

    //查询
    public static String  getQuery(String out_trade_no,String bak) {

        CreamToolkit.logMessage("调用" + bak + "查询接口----------------");
        String  service = "alipay.acquire.query";//接口名称
        //把请求参数打包成数组
        Map sParaTemp = new HashMap();
        sParaTemp.put("service", service);
        sParaTemp.put("partner", partner);
        sParaTemp.put("_input_charset", _input_charset);
        sParaTemp.put("out_trade_no", out_trade_no);
        String sHtmlText = "";
        try {
            //建立请求
            sHtmlText = AlipaySubmit.buildRequest("", "", sParaTemp);
        } catch (Exception e)  {
            e.printStackTrace();
        }
        CreamToolkit.logMessage(sHtmlText);
        return sHtmlText;
    }

    /**
     *
     * @param partner 合作者身份ID
     * @param product_code 密钥
     * @param url 支付宝URL
     * @param page_no 查询页号
     * @param gmt_start_time 账务查询开始时间
     * @param gmt_end_time 账务查询结束时间
     * @param iw_account_log_id 账务流水号
     * @param trade_no 支付宝交易号
     * @param merchant_out_order_no 商户订单号
     * @param deposit_bank_no 充值网银流水号
     * @param page_size 分页大小
     * @param trans_code 交易类型代码
     * @return
     */
    public static String  getPageQuery(String partner,String product_code, String url, String page_no,
            String gmt_start_time, String gmt_end_time, String iw_account_log_id, String trade_no,
            String merchant_out_order_no, String deposit_bank_no, String page_size, String trans_code) {

        CreamToolkit.logMessage("调用账务明细分页查询接口----------------");
        String  service = "account.page.query";//接口名称
        key = product_code;
        //把请求参数打包成数组
        Map sParaTemp = new HashMap();
        AlipaySubmit.ALIPAY_GATEWAY_NEW = url;
        sParaTemp.put("service", service);
        sParaTemp.put("partner", partner);
        sParaTemp.put("_input_charset", _input_charset);
        sParaTemp.put("page_no", page_no);
        if (!gmt_start_time.equals(""))
            sParaTemp.put("gmt_start_time", gmt_start_time);
        if (!gmt_end_time.equals(""))
            sParaTemp.put("gmt_end_time", gmt_end_time);
        if (!iw_account_log_id.equals(""))
            sParaTemp.put("iw_account_log_id", iw_account_log_id);
        if (!trade_no.equals(""))
            sParaTemp.put("trade_no", trade_no);
        if (!merchant_out_order_no.equals(""))
            sParaTemp.put("merchant_out_order_no", merchant_out_order_no);
        if (!deposit_bank_no.equals(""))
            sParaTemp.put("deposit_bank_no", deposit_bank_no);
        if (!page_size.equals(""))
            sParaTemp.put("page_size", page_size);
        if (!trans_code.equals(""))
            sParaTemp.put("trans_code", trans_code);
        String sHtmlText = "";
        try {
            //建立请求
            sHtmlText = AlipaySubmit.buildRequest("", "", sParaTemp);
        } catch (Exception e)  {
            e.printStackTrace();
        }
        CreamToolkit.logMessage(sHtmlText);
        CreamToolkit.logMessage(sHtmlText);
        return sHtmlText;
    }

    public static String getErrorMessage(String errorCode) {
        errorCode = errorCode.toUpperCase();
        if (errorCode.equals("ILLEGAL_SIGN"))
            return "签名不正确";
        if (errorCode.equals("ILLEGAL_DYN_MD5_KEY"))
            return "动态密钥信息错误";
        if (errorCode.equals("ILLEGAL_ENCRYPT"))
            return "加密不正确";
        if (errorCode.equals("ILLEGAL_ARGUMENT"))
            return "参数不正确";
        if (errorCode.equals("ILLEGAL_SERVICE"))
            return "Service参数不正确";
        if (errorCode.equals("ILLEGAL_USER"))
            return "用户ID不正确";
        if (errorCode.equals("ILLEGAL_PARTNER"))
            return "合作伙伴ID不正确";
        if (errorCode.equals("ILLEGAL_EXTERFACE"))
            return "接口配置不正确";
        if (errorCode.equals("ILLEGAL_PARTNER_EXTERFACE"))
            return "合作伙伴接口信息不正确";
        if (errorCode.equals("ILLEGAL_SECURITY_PROFILE"))
            return "未找到匹配的密钥配置";
        if (errorCode.equals("ILLEGAL_AGENT"))
            return "代理ID不正确";
        if (errorCode.equals("ILLEGAL_SIGN_TYPE"))
            return "签名类型不正确";
        if (errorCode.equals("ILLEGAL_CHARSET"))
            return "字符集不合法";
        if (errorCode.equals("HAS_NO_PRIVILEGE"))
            return "无权访问";
        if (errorCode.equals("INVALID_CHARACTER_SET"))
            return "字符集无效";
        if (errorCode.equals("TRADE_BUYER_NOT_MATCH"))
            return "交易买家不匹配";
        if (errorCode.equals("CONTEXT_INCONSISTENT"))
            return "交易信息被篡改";
        if (errorCode.equals("TRADE_HAS_SUCCESS"))
            return "交易已经支付";
        if (errorCode.equals("TRADE_NOT_EXIST"))
            return "交易不存在";
        if (errorCode.equals("TRADE_HAS_CLOSE"))
            return "交易已经关闭";
        if (errorCode.equals("REASON_ILLEGAL_STATUS"))
            return "交易的状态不合法";
        if (errorCode.equals("EXIST_FORBIDDEN_WORD"))
            return "订单信息中包含违禁词";
        if (errorCode.equals("PARTNER_ERROR"))
            return "合作伙伴信息不正确";
        if (errorCode.equals("ACCESS_FORBIDDEN"))
            return "没有权限使用该产品";
        if (errorCode.equals("SELLER_NOT_EXIST"))
            return "卖家不存在";
        if (errorCode.equals("BUYER_NOT_EXIST"))
            return "买家不存在";
        if (errorCode.equals("BUYER_ENABLE_STATUS_FORBID"))
            return "买家状态非法，无法继续交易";
        if (errorCode.equals("BUYER_SELLER_EQUAL"))
            return "卖家买家账号相同，不能进行交易";
        if (errorCode.equals("INVALID_PARAMETER"))
            return "参数无效";
        if (errorCode.equals("UN_SUPPORT_BIZ_TYPE"))
            return "不支持的业务类型";
        if (errorCode.equals("INVALID_RECEIVE_ACCOUNT"))
            return "卖家不在设置的收款账户列表之中";
        if (errorCode.equals("ROYALTY_INFO_VALIDATE_FAIL"))
            return "分账信息校验失败";
        if (errorCode.equals("SYSTEM_ERROR"))
            return "支付宝系统错误";
        if (errorCode.equals("SESSION_TIMEOUT"))
            return "session超时";
        if (errorCode.equals("ILLEGAL_TARGET_SERVICE"))
            return "错误的target_service";
        if (errorCode.equals("ILLEGAL_ACCESS_SWITCH_SYSTEM"))
            return "partner不允许访问该类型的系统";
        if (errorCode.equals("EXTERFACE_IS_CLOSED"))
            return "接口已关闭";
        if (errorCode.equals("TRADE_SETTLE_ERROR"))
            return "分账信息校验失败";
        if (errorCode.equals("SOUNDWAVE_PARSER_FAIL"))
            return "动态ID解析失败";
        if (errorCode.equals("REFUND_AMT_NOT_EQUAL_TOTAL"))
            return "撤销或退款金额与订单金额不一致";
        if (errorCode.equals("DISCORDANT_REPEAT_REQUEST"))
            return "同一笔退款或撤销单号金额不一致";
        if (errorCode.equals("TRADE_ROLE_ERROR"))
            return "没有该笔交易的退款或撤销权限";
        if (errorCode.equals("REASON_TRADE_BEEN_FREEZEN"))
            return "交易已经被冻结";
        if (errorCode.equals("TRADE_FINISHED"))
            return "交易成功且结束，即不可再做任何操作";
        if (errorCode.equals("TRADE_PENDING"))
            return "等待卖家收款（买家付款后，如果卖家账号被冻结）";
        if (errorCode.equals("TRADE_STATUS_ERROR"))
            return "交易状态不合法";
        if (errorCode.equals("TRADE_HAS_FINISHED"))
            return "交易已结束";
        return "未知错误,"+errorCode +  "," + CreamToolkit.GetResource().getString("AlipayFail");
    }

    public static String getMD5Str(String str) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(str.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            CreamToolkit.logMessage("NoSuchAlgorithmException caught!");
            System.exit(-1);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        byte[] byteArray = messageDigest.digest();
        StringBuffer md5StrBuff = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
                md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
            else
                md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
        }
        return md5StrBuff.toString();
    }

    public static void main(String [] args) {

        //out_trade_no = storeid+posNumber+transactionnumber
        //getCreateAndPay("1111131587509","0.10","11111111111","iphone手机");
        getCancel(args[0],"");
        //getQuery("HZ0120131127001");
//        getRefund("1001031135891702","8.00","");
//        getPageQuery("2088801054284657","","","1","2015-03-02 23:00:00","2015-03-03 00:00:00","","","","","","");
    }
}