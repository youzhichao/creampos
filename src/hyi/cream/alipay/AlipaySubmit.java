package hyi.cream.alipay;

import hyi.cream.groovydac.Param;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import org.apache.commons.httpclient.NameValuePair;

import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
/* *
 *类名：AlipaySubmit
 *功能：支付宝各接口请求提交类
 *详细：构造支付宝各接口表单HTML文本，获取远程HTTP数据
 *版本：3.3
 *日期：2012-08-13
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipaySubmit {
    
    /**
     * 支付宝提供给商户的服务接入网关URL(新)
     */
    public static  String ALIPAY_GATEWAY_NEW = GetProperty.getAlipayURL();

    /**
     * 生成签名结果
     * @param sPara 要签名的数组
     * @return 签名结果字符串
     */
	public static String buildRequestMysign(Map sPara) {
    	String prestr = AlipayCore.createLinkString(sPara); //把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
        String mysign = "";
        if(AlipayHttpPostRequest.sign_type.equals("MD5") ) {
            System.out.println("MD5私钥：" + AlipayHttpPostRequest.key);
        	mysign = MD5.sign(prestr, AlipayHttpPostRequest.key, AlipayHttpPostRequest._input_charset);
        }
        return mysign;
    }
	
    /**
     * 生成要请求给支付宝的参数数组
     * @param sParaTemp 请求前的参数数组
     * @return 要请求的参数数组
     */
    private static Map buildRequestPara(Map sParaTemp) {
        //除去数组中的空值和签名参数
        Map sPara = AlipayCore.paraFilter(sParaTemp);
        //生成签名结果
        String mysign = buildRequestMysign(sPara);
        System.out.println("sign_type：" + AlipayHttpPostRequest.sign_type);
        System.out.println("sign：" + mysign);
        //签名结果与签名方式加入请求提交参数组中
        sPara.put("sign", mysign);
        sPara.put("sign_type", AlipayHttpPostRequest.sign_type);

        return sPara;
    }

    /**
     * 建立请求，以模拟远程HTTP的POST请求方式构造并获取支付宝的处理结果
     * 如果接口中没有上传文件参数，那么strParaFileName与strFilePath设置为空值
     * 如：buildRequest("", "",sParaTemp)
     * @param strParaFileName 文件类型的参数名
     * @param strFilePath 文件路径
     * @param sParaTemp 请求参数数组
     * @return 支付宝处理结果
     * @throws Exception
     */
    public static String buildRequest(String strParaFileName, String strFilePath,Map sParaTemp) throws Exception {
        //待请求参数数组
        Map sPara = buildRequestPara(sParaTemp);

        HttpProtocolHandler httpProtocolHandler = HttpProtocolHandler.getInstance();

        HttpRequest request = new HttpRequest();
        //设置编码集
        request.setCharset(AlipayHttpPostRequest._input_charset);
        request.setParameters(generatNameValuePair(sPara));
        request.setUrl(ALIPAY_GATEWAY_NEW/*+"_input_charset="+AlipayHttpPostRequest._input_charset*/);
        System.out.println("Url：" + request.getUrl());
        HttpResponse response = httpProtocolHandler.execute(request,strParaFileName,strFilePath);
        if (response == null) {
            return null;
        }

        String strResult = response.getStringResult();

        return strResult;
    }

    /**
     * MAP类型数组转换成NameValuePair类型
     * @param properties  MAP类型数组
     * @return NameValuePair类型数组
     */
    private static NameValuePair[] generatNameValuePair(Map properties) {
        NameValuePair[] nameValuePair = new NameValuePair[properties.size()];
        int i = 0;
        Iterator it = properties.entrySet().iterator();
        for (;it.hasNext();) {
            Map.Entry entry =  (Map.Entry)it.next();
            String key = entry.getKey().toString();
            String value = entry.getValue().toString();
            if (!ALIPAY_GATEWAY_NEW.equals("https://mapi.alipay.com/gateway.do?"))
                if (key.equals("body") || key.equals("subject") || key.equals("goods_detail")) {
                    try {
                        value = URLEncoder.encode(value, AlipayHttpPostRequest._input_charset);
                    }catch (Exception e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                    }
                }
            //parm += "&"+key+"="+value;
            nameValuePair[i++] = new NameValuePair(key, value);

        }
        //parm = parm.substring(1);
        //System.out.println(parm);
        return nameValuePair;
    }
}
