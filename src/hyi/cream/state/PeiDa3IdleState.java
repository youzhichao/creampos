package hyi.cream.state;

import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Reason;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.Scanner;
import hyi.spos.ToneIndicator;
import hyi.spos.events.DataEvent;

import java.util.EventObject;
import java.util.MissingResourceException;

/**
 * ANNOTATEDTYPE="P" 表示该交易为传票
 * ANNOTATEDID 记录传票号码+打印版本
 *  
 * POS传票流程:
 *      1)POS扫描传票单号
 *      2)到SC取该传票的head&dtl
 *      3)dtl完全从SC转档过来
 *      4)head通过计算获得
 *      5)结帐
 *      6)第2步POS SC offline时
 *      7)询问是否强制结帐
 *      8)是:pos扫描传票金额,生成一笔特殊dtl 做4)以下步骤
 *      9)否:返回销售画面
 * 
 * @author ll
 */
public class PeiDa3IdleState extends State {

    private static PeiDa3IdleState peiDa3IdleState;

    private boolean error;
    private String billNoInput;
    private String billNo;
    private static String billNoPrefix;
    private boolean downPayment; // 訂金/着付手付金 
    private boolean backPayment; // 訂金尾款/着付殘金
    private boolean backRefundPaymet; // 退訂金尾款/退着付殘金

    private ToneIndicator tone = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    public static PeiDa3IdleState getInstance() {
        try {
            if (peiDa3IdleState == null) {
                peiDa3IdleState = new PeiDa3IdleState();
            }
        } catch (InstantiationException ex) {
        }
        return peiDa3IdleState;
    }

    /**
     * Constructor
     */
    public PeiDa3IdleState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        Object eventSource = event.getSource();
        if (sourceState instanceof IdleState) {
            billNoInput = "";
            showMessage("PeiDa3Message");
            downPayment = eventSource instanceof DownPaymentButton;
            backPayment = eventSource instanceof BackPaymentButton;
            backRefundPaymet = eventSource instanceof BackPaymentRefundButton;
        } else if ((sourceState instanceof PeiDa3OnlineState
                || sourceState instanceof PeiDa3OfflineState
                || sourceState instanceof PeiDa3AmountState)
                && eventSource instanceof ClearButton) {
            billNoInput = "";
            resetTranInfo();
            showMessage("PeiDa3Message");
        } else if (sourceState instanceof PeiDa3IdleState) {
            if (eventSource instanceof ClearButton) {
                if (!billNoInput.equals("")) {
                    billNoInput = "";
                    showMessage("PeiDa3Message");
                    showWarningMessage("");
                }
            } else if (eventSource instanceof NumberButton) {
                try {
                    if (tone != null) {
                        tone.clearOutput();
                        tone.release();
                    }
                } catch (JposException je) {
                }
                NumberButton pb = (NumberButton) eventSource;
                billNoInput = billNoInput + pb.getNumberLabel();
                showMessage(billNoInput);
                showWarningMessage("");
            }
        }
    }

    private void resetTranInfo() {
        app.getCurrentTransaction().setAnnotatedType("");
        app.getCurrentTransaction().setAnnotatedId("");
        app.getCurrentTransaction().setBuyerNumber("");
        app.getCurrentTransaction().setMemberID("");
    }

    public Class exit(EventObject event, State sinkState) {
        if (error) {
            error = false;
            app.getWarningIndicator().setMessage("");
            resetTranInfo();
            return IdleState.class;
        }
        Object eventSource = event.getSource();
        if (eventSource instanceof ClearButton) {
            if (billNoInput.equals("")) {
                resetTranInfo();
                return IdleState.class;
            }

            if (tone != null) {
                try {
                    tone.clearOutput();
                } catch (JposException je) {
                    je.printStackTrace();
                    je.printStackTrace(CreamToolkit.getLogger());
                }
            }
            app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("PeiDa3Message"));
            return PeiDa3IdleState.class;
        } if (eventSource instanceof NumberButton) {
            ;
        } else {
            if (eventSource instanceof Scanner) {
                try {
                    /* Bruce/2008-10-07/ Don't check the barcode type.
                    if (((Scanner) eventSource).getScanDataType() == ScannerConst.SCAN_SDT_UNKNOWN
                            || ((Scanner) eventSource).getScanDataType() == ScannerConst.SCAN_SDT_Code39) {
                        setWarningMessage(CreamToolkit.GetResource().getString(
                                "InputWrong"));
                        return PeiDa3IdleState.class;
                    }
                    */
                    billNoInput = new String(((Scanner)eventSource).getScanData(((DataEvent)event).seq));
                } catch (MissingResourceException e1) {
                    e1.printStackTrace();
                    e1.printStackTrace(CreamToolkit.getLogger());
                } catch (JposException e2) {
                    e2.printStackTrace();
                    e2.printStackTrace(CreamToolkit.getLogger());
                }
            }
            // if (eventSource instanceof EnterButton) {
            // return PeiDa3State.class;
            if (billNoInput.equals("") || billNoInput.length() != 13 || !billNoInput.startsWith("28")) {
                POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
                try {
                    tone = posHome.getToneIndicator();
                } catch (Exception ne) {
                    CreamToolkit.logMessage(ne);
                }
                try {
                    if (!tone.getDeviceEnabled())
                        tone.setDeviceEnabled(true);
                    if (!tone.getClaimed()) {
                        tone.claim(0);
                        // tone.claim(0);
                        tone.setAsyncMode(true);
                        tone.sound(99999, 500);
                    }
                } catch (JposException je) {
                    je.printStackTrace();
                    je.printStackTrace(CreamToolkit.getLogger());
                } catch (Exception e) {
                    e.printStackTrace();
                    e.printStackTrace(CreamToolkit.getLogger());
                }
                app.getWarningIndicator().setMessage(
                        CreamToolkit.GetResource().getString(
                                "PeiDaLengthWarning"));
                return PeiDa3IdleState.class;
            } else {
                billNo = billNoInput.substring(2, 12);
                //int statusInt = -1;
                String statusStr = "";
                try {
                    /*
                     *  -1=无法连接sc
                     *  可結帳
                     *      'H'; //批發配達
                     *      'P'; //普通配達
                     *      'M'; //窗簾修改
                     *      'O'; //窗簾訂做
                     *  1=不存在
                     *  2=已作廢
                     *  3=售後處理
                     *  4=結清,pos不可結
                     *  5=被退過貨的
                     *  6=單據版本不符合
                     *  7=退货机可退貨,pos不可结帐
                     *  8=异常
                     */
                    String statusAndSrcBillNo = StateToolkit.getPeiDaStatus(getBillNoPrefix() + billNo);
                    // status = deliveryhead.結帳狀態 + "," + deliveryhead.srcBillNo(原始單號)
                    String[] x = statusAndSrcBillNo.split(",");
                    statusStr = x[0];
                } catch (java.net.SocketException e) {
                    CreamToolkit.logMessage(e);
                    return PeiDa3OfflineState.class;
                } catch (Exception e) {
                    CreamToolkit.logMessage(e);
                }
                System.out.println("--------- peida status: " + statusStr);

                if (backPayment && "4".equals(statusStr) ||         // 如果是訂金尾款，而且POS已結清
                    backRefundPaymet /*&& "4".equals(statusStr)*/ ||    // 如果是退訂金尾款 /*，而且POS已結清*/
                    GetProperty.getAnnotatedTypeList().contains(statusStr)) { // 或者為可結帳狀態
                    //if (status == 'H'){
                    if (GetProperty.getAnnotatedTypeWholesale().equals(statusStr)){
                        app.getCurrentTransaction().setTranType1("1");  // 批發交易類型
                        //app.getCurrentTransaction().setAnnotatedType("P"); // H 固定 設置為P
                    } else {
                        app.getCurrentTransaction().setAnnotatedType(statusStr);
                    }
                    app.getCurrentTransaction().setAnnotatedId(getBillNoPrefix() + billNo);
                    showWarningMessage("");
                    showMessage("PeiDaConfirm");

                    if (isDownPayment())
                        getCurrentTransaction().setState1("W"); // 記錄此交易為Nitori着付金交易，此交易將不印發票

                    return (backPayment || backRefundPaymet) ? InputDownPaymentState.class // (DownPayment: PD2ID)
                        : PeiDa3OnlineState.class;      // (DownPayment: PD2PD)
                }
                error = true;
                if ("-1".equals(statusStr) || "8".equals(statusStr))  // 8=异常
                    showMessage("PeiDaError", billNo);
                else if ("1".equals(statusStr))  //1=不存在
                    showMessage("PeiDaError1", billNo);
                else if ("2".equals(statusStr))  //2=已作廢
                    showMessage("PeiDaError2", billNo);
                else if ("3".equals(statusStr))  //3=售後處理
                    showMessage("PeiDaError3", billNo);
                else if ("4".equals(statusStr))  //4=結清,pos不可結
                    showMessage("PeiDaError4", billNo);
                else if ("5".equals(statusStr))  //5=被退過貨的
                    showMessage("PeiDaError5", billNo);
                else if ("6".equals(statusStr))  //6=單據版本不符合
                    showMessage("PeiDaError6", billNo);
                else if ("7".equals(statusStr))  //7=退货机可退貨,pos不可结帐
                    showMessage("PeiDaError7", billNo);
                else
                    showMessage("此傳票尚未結帳！");
            }
        }
        return PeiDa3IdleState.class;
    }

    public String getBillNo() {
        return billNo;
    }

    public static String getBillNoPrefix() {
        if (billNoPrefix == null){
            Reason r = Reason.queryByreasonNumberAndreasonCategory("01", "34");
            // 配达单前缀（字母）,不包含在条码中
            if (r != null && r.getreasonName() != null)
                billNoPrefix = r.getreasonName().trim();// 注意：trim比较安全，但是意味着不能用空格做前缀
            else
                billNoPrefix = "";
    }
        return billNoPrefix;
    }

    /**
     * 訂金尾款/着付殘金
     */
    public boolean isBackPayment() {
        return backPayment;
    }

    /**
     * 訂金/着付手付金
     */
    public boolean isDownPayment() {
        return downPayment;
    }

    /**
     * 退訂金尾款/退着付殘金
     */
    public boolean isBackRefundPaymet() {
        return backRefundPaymet;
    }
}
