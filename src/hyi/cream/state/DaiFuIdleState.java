// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Reason;
import hyi.cream.dac.Transaction;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.uibeans.AgeLevelButton;
import hyi.cream.uibeans.CancelButton;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.DaiFuButton;
import hyi.cream.uibeans.PopupMenuListener;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.Keylock;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;

/**
 * @author dai
 */
public class DaiFuIdleState extends State implements PopupMenuListener {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	// private ResourceBundle res = CreamToolkit.GetResource();
	private PopupMenuPane p = app.getPopupMenuPane();
	private ArrayList reasonArray = new ArrayList();
	private String pluNo = "";
	//private DaiFuButton daiFuButton = null;

	static DaiFuIdleState daiFuIdleState = null;

	public static DaiFuIdleState getInstance() {
		try {
			if (daiFuIdleState == null) {
				daiFuIdleState = new DaiFuIdleState();
			}
		} catch (InstantiationException ex) {
		}
		return daiFuIdleState;
	}

	/**
	 * Constructor
	 */
	public DaiFuIdleState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
		// System.out.println("DaiFuIdleState entry");

		if (event != null && event.getSource() instanceof DaiFuButton) {
			app.getSystemInfo().setIsDaiFu(true);
			//daiFuButton = ((DaiFuButton) event.getSource());
			ArrayList menu = new ArrayList();
			Iterator daiFuItem = Reason.queryByreasonCategory("09");
			Reason r = null;
			//String menuString = "";
			//PLU plu = null;
			int i = 1;
			while (daiFuItem != null && daiFuItem.hasNext()) {
				r = (Reason) daiFuItem.next();
				reasonArray.add(r);
				menu.add(i + "." + r.getreasonName());
				i++;
			}

			p.setMenu(menu);
			p.setVisible(true);
			p.setPopupMenuListener(this);
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString("InputSelect"));
		} else {
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString("DaifuMessage"));
		}
	}

	public Class exit(EventObject event, State sinkState) {
		// System.out.println("DaiFuIdleState exit");

		app.getMessageIndicator().setMessage("");
		app.getWarningIndicator().setMessage("");

		// check enter button
		if (event.getSource() instanceof AgeLevelButton) {
			int agelevel = ((AgeLevelButton) event.getSource()).getAgeID();
			Transaction trans = app.getCurrentTransaction();
			trans.setCustomerAgeLevel(agelevel);

			if (!app.getTrainingMode()
					&& PARAM.getTranPrintType().equalsIgnoreCase("step")) {
				
                DbConnection connection = null;
                try {
                    connection = CreamToolkit.getTransactionalConnection();
					if (!CreamPrinter.getInstance().getHeaderPrinted()) {
						CreamPrinter.getInstance().printHeader(connection);
						CreamPrinter.getInstance().setHeaderPrinted(true);
					}
	
					Object[] lineItemArray = trans.getLineItems();
					LineItem curLineItem = (LineItem) lineItemArray[lineItemArray.length - 1];
                    CreamPrinter.getInstance().printLineItem(connection, curLineItem);
                    connection.commit();
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                } finally {
                    CreamToolkit.releaseConnection(connection);
                }
			}
			return SummaryState.class;
		}

		// check cancel button
		if (event.getSource() instanceof CancelButton) {
			return CancelState.class;
		}

		// check select button
		if (event.getSource() instanceof SelectButton) {
			return DaiFuReadyState.class;
		}

		// check clear button
		if (event.getSource() instanceof ClearButton) {
		    Transaction trans = app.getCurrentTransaction();
			if (trans.getCurrentLineItem() == null) {
				trans.setDealType2("0");
				return IdleState.class;
			} else {
				return DaiFuIdleState.class;
			}
		}

		// check keylock
		if (event.getSource() instanceof Keylock) {
			Keylock k = (Keylock) event.getSource();
			try {
				int kp = k.getKeyPosition();
				app.setKeyPosition(kp);
				if (CreamToolkit.getSinkStateFromKeyLockCode(kp) != null) {
					return CreamToolkit.getSinkStateFromKeyLockCode(kp);
				} else {
					return DaiFuIdleState.class;
				}
			} catch (JposException e) {
				System.out.println(e);
			}
			/*
			 * Keylock k = (Keylock)event.getSource(); try { int kp =
			 * k.getKeyPosition(); int ckp = app.getKeyPosition(); if (kp == 3) {
			 * app.setKeyPosition(kp); return KeyLock2State.class; } else if (kp ==
			 * 6) { app.setKeyPosition(kp); return ConfigState.class; } else if
			 * (kp == 2) { app.setKeyPosition(kp); return DaiFuIdleState.class; }
			 * else if (kp == 4) { app.setKeyPosition(kp); return
			 * KeyLock1State.class; } } catch (JposException e) { }
			 */
		}

		return sinkState.getClass();
	}

	public String getPluNo() {
		return pluNo;
	}

	public void menuItemSelected() {
		if (p.getSelectedMode()) {
			int index = p.getSelectedNumber();
			pluNo = ((Reason) reasonArray.get(index)).getreasonNumber();
			POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
			POSButtonHome2.getInstance().buttonPressed(e);
		} else {
			POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
			POSButtonHome2.getInstance().buttonPressed(e);
		}
	}
}
