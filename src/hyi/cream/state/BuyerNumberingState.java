
// Copyright (c) 2000 HYI

package hyi.cream.state;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class BuyerNumberingState extends SomeAGNumberingState {
    static BuyerNumberingState buyerNumberingState = null;

    public static BuyerNumberingState getInstance() {
        try {
            if (buyerNumberingState == null) {
                buyerNumberingState = new BuyerNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return buyerNumberingState;
    }

    /**
     * Constructor
     */
    public BuyerNumberingState() throws InstantiationException {
    }
}

  
