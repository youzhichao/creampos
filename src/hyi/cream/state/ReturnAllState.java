package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.event.TransactionEvent;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.ReturnReasonPopupMenu;

import java.util.EventObject;

public class ReturnAllState extends State {
    static ReturnAllState returnAllState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    public static ReturnAllState getInstance() {
        try {
            if (returnAllState == null) {
                returnAllState = new ReturnAllState();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return returnAllState;
    }

    public ReturnAllState() throws InstantiationException {
    }

    @Override
    public void entry(EventObject event, State sourceState) {
        Transaction trans = app.getCurrentTransaction();
        if (//event.getSource() instanceof EnterButton && 
             //sourceState instanceof MixAndMatchState) {
             sourceState instanceof ReturnSelect1State) { 

            showMessage("ReturnAll"); // "确认退货"

            //maybe popup return reason pane
            if (PARAM.isReturnReasonPopup()) {
                showMessage("ReturnReason");
                new ReturnReasonPopupMenu(ReturnReasonPopupMenu.RETURN_ALL, "0").popupMenu();
                showMessage("ReturnAll");
            }
            //trans.makeNegativeValue();
            trans.fireEvent(new TransactionEvent(trans, TransactionEvent.TRANS_INFO_CHANGED));                

        } else if (event.getSource() instanceof ClearButton
            && sourceState instanceof ReturnSummaryState) {
            showMessage("ReturnAll");
            trans.makeNegativeValue();
            trans.fireEvent(new TransactionEvent(trans, TransactionEvent.TRANS_INFO_CHANGED));                
        }
    }

    @Override
    public Class exit(EventObject event, State sinkState) {
        if (sinkState.getClass().equals(ReturnSummaryState.class)) {
            // 将所有lineItems设置为removed
            for (LineItem lineItem : app.getCurrentTransaction().lineItems())
                lineItem.setRemoved(true);
        }
        return sinkState.getClass();
    }
}
