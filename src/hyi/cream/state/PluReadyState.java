package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.*;
import hyi.cream.groovydac.SsmpLog;
import hyi.cream.exception.CreateLineItemException;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.inline.InlineCommandExecutor;
import hyi.cream.inline.MasterDownloadThread;
import hyi.cream.uibeans.BarCodeButton;
import hyi.cream.uibeans.DaiShouButton;
import hyi.cream.uibeans.DaiShouPluButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.InStoreCodeButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.uibeans.PluButton;
import hyi.cream.uibeans.PluMenuButton;
import hyi.cream.uibeans.TaxedAmountButton;
import hyi.cream.uibeans.WeiXiuButton;
import hyi.cream.uibeans.touch.TouchPane;
import hyi.cream.util.*;
import hyi.spos.JposException;
import hyi.spos.Scanner;
import hyi.spos.ScannerConst;
import hyi.spos.events.DataEvent;

import java.awt.Color;
import java.awt.Toolkit;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.EventObject;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.List;

import org.apache.commons.lang.StringUtils;

//import jpos.JposException;

/**
 * PluReadyState class.
 * 
 * @author dai, bruce, zhao
 */
public class PluReadyState extends State {

    private static PluReadyState instance = new PluReadyState();

    private String number = "";

    private String numberString = "";

    private PLU plu = null;

    // private PLU itemno = null;

    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    private LineItem lineItem = null;

    private Class exitState = null;

    // private HYIDouble quan = new HYIDouble(1.00);

    private static final HYIDouble zero = new HYIDouble(0);

    private ResourceBundle res = CreamToolkit.GetResource();

    private Color lineItemColor = new Color(70, 206, 255);

    private Color defaultColor = new Color(70, 206, 255);

    private String pluSign3 = "";

    private int daishouNumber = 0;

    private boolean isDaishouPlu = false;

    private int daishouDetailNumber = 0;

    private String daishouDetailName = "";

    private String memberCardIDPrefix = PARAM.getMemberCardIDPrefix();

    // private boolean needCalculateRebate = false;

    long start = 0;

    long end = 0;

    boolean needPreventScanner = false;

    boolean isPeriodical = false;

    String dtlcode1 = "";

    /**
     * Constructor
     */
    private PluReadyState() {
    }

    public static PluReadyState getInstance() {
        return instance;
    }

    public void entry(EventObject event, State sourceState) {
        isPeriodical = false;
        dtlcode1 = "";
        // super.entry(event, sourceState);
        Object eventSource = event.getSource();
        app.getWarningIndicator().setMessage("");
        if (eventSource instanceof PluButton) {
            lineItemColor = ((PluButton)eventSource).getPluColor();
        } else {
            lineItemColor = defaultColor;
        }
        try {
            if (app.getCurrentTransaction().getEcDeliveryHeadList().size() > 0) {
                setWarningMessage(res.getString("ECommerceOrdersAreNotAllowedToEnterGoods"));
                exitState = WarningState.class;
                return;
            }

            if (!PARAM.isDaiShou2MixSales()
                    && app.getCurrentTransaction().isIncludedDaiShou2()){
                setWarningMessage(res.getString("DaiShou2MixSalesNotAllow"));
                exitState = WarningState.class;
                return;
            }

            if (sourceState instanceof IdleState) {
                if (eventSource instanceof PluButton) {
                    number = ((PluButton) eventSource).getPluCode();
                    
                    if (GetProperty.isEnableTouchPanel()) {
                        // Allan/20060706 客户希望 待售商品不能通过扫描 销售，只能通过代售键,和触屏方式
                        if (TouchPane.getInstance().isVisible())
                            needPreventScanner = false;
                    }

                } else if (eventSource instanceof PluMenuButton) {
                    number = ((PluMenuButton) eventSource).getPluNo();
                    ((PluMenuButton) eventSource).clear();

                } else if (eventSource instanceof Scanner) {
                    if (GetProperty.isEnableTouchPanel()) {
                        if (TouchPane.getInstance() != null) {
                            TouchPane.getInstance().setVisible(false);
                        }
                    }
                    number = new String(((Scanner) eventSource).getScanData(((DataEvent)event).seq));
                    CreamToolkit.logMessage("app.getCurrentTransaction().getCurrentLineItem() == null"+(app.getCurrentTransaction().getCurrentLineItem() == null) +"number="+number);
                    if (app.getCurrentTransaction().getCurrentLineItem() == null){
                        if (number.equals("88000028")) {
                            exitState = Shutdown2State.class;
                            SelfbuyHttpPostRequest.selfbuyShift2();
                            DbConnection connection = null;
                            ZReport z = new ZReport();
                            try {
                                connection = CreamToolkit.getTransactionalConnection();
                                z = ZReport.getOrCreateCurrentZReport(connection);
                                connection.commit();
                            } catch (SQLException e) {
                                app.setEnableKeylock(true);
                                CreamToolkit.logMessage(e);
                                CreamToolkit.haltSystemOnDatabaseFatalError(e);
                            } finally {
                                CreamToolkit.releaseConnection(connection);
                            }

                            if (PARAM.isZPrint())
                                CreamPrinter.getInstance().printZReport(z, this);
//                            app.setShift2(false);
                            //日结后立即上传
                            if (Client.getInstance().isConnected()) {
                                InlineCommandExecutor.getInstance().execute("putAttendance " + z.getSequenceNumber());
                                InlineCommandExecutor.getInstance().execute("putZ " + z.getSequenceNumber());
                            }
                            return;
                        } else if (number.equals("88000038")) {
                            CreamToolkit.halt();
                            exitState = InitialState.class;
                            return;
                        } else if (number.equals("88000018")) {
                            PluUpdateState.downloadMaster();
                            exitState = InitialState.class;
                            return;
                        }
                    }

                    //// Bruce/20030401
                    //// 灿坤specific: 若扫描进来的是以2991打头，则检查是否为会员编号
                    ////if (number.startsWith("2991") || number.startsWith("2990")) {
                    if (!StringUtils.isEmpty(memberCardIDPrefix) && number.startsWith(memberCardIDPrefix)) {
                        exitState = dealWithMember();
                        return;
                    }

                    // Allan/20030417 电子秤
                    if (!StringUtils.isEmpty(PARAM.getScaleBarCodeFormat())
                            && processScaleBarCode(number))
                        return;

                    // Allan 20060601 客户希望 待售商品不能通过扫描 销售，只能通过代售键
                    if (!StringUtils.isEmpty(PARAM.getPreventScannerCatNo()))
                        needPreventScanner = true;

                    exitState = IdleState.class;

                } else if (eventSource instanceof DaiShouButton) {
                    isDaishouPlu = true;
                    daishouNumber = ((DaiShouButton) eventSource).getDaiShouNumber();
                    daishouDetailNumber = ((DaiShouButton) eventSource).getDaishouDetailNumber();
                    daishouDetailName = ((DaiShouButton) eventSource).getDaishouDetailName();
                    number = ((DaiShouButton) eventSource).getPluNumber();

                    // Allan/20060601 客户希望 待售商品不能通过扫描 销售，只能通过代售键
                    needPreventScanner = false;

                } else if (eventSource instanceof DaiShouPluButton) {
                    isDaishouPlu = true;
                    daishouNumber = Integer.parseInt(((DaiShouPluButton) eventSource).getCatNO());
                    daishouDetailNumber = Integer.parseInt(((DaiShouPluButton) eventSource).getDetailNO());
                    daishouDetailName = ((DaiShouPluButton) eventSource).getLabel();
                    number = ((DaiShouPluButton) eventSource).getPluNO();

                    // Allan/20060601 客户希望 待售商品不能通过扫描 销售，只能通过代售键
                    needPreventScanner = false;

                } else if (eventSource instanceof WeiXiuButton) {
                    String type = app.getCurrentTransaction().getAnnotatedType();
                    if (type != null && !type.equals("")) {
                        setExitState(IdleState.class);
                        return;
                    }

                    number = ((WeiXiuButton) eventSource).getPluCode();
                    plu = PLU.queryBarCode(number);
                    sub(plu);
                    if (exitState.getName().equalsIgnoreCase("hyi.cream.state.OpenPriceState")
                        || exitState.getName().equalsIgnoreCase("hyi.cream.state.MixAndMatchState"))
                        setExitState(WeiXiuState.class);
                    return;
                }

                plu = PLU.queryBarCode(number);
                if (plu == null)
                    plu = PLU.queryByInStoreCode(number);
                if (plu == null && number.length() == 15) {
                    //System.out.println("---------- plureadystate maybe periodical : " + number);
                    // 期刊
                    isPeriodical = true;
                    dtlcode1 = number.substring(13, 15);
                    number = number.substring(0, 13);
                    plu = PLU.queryBarCode(number);
                    if (plu == null)
                        plu = PLU.queryByInStoreCode(number);
                    if (plu != null && (plu.getItemtype() == null || plu.getItemtype() != 10)) {
                        plu = null;
                        number += dtlcode1;
                    }
                }

                // Allan/20060601 客户希望 待售商品不能通过扫描 销售
                if (needPreventScanner
                    && plu != null
                    && plu.getCategoryNumber() != null
                    && plu.getCategoryNumber().trim().equals(PARAM.getPreventScannerCatNo())) {
                    setWarningMessage(res.getString("ScannerPrevented"));
                    exitState = WarningState.class;
                    return ;
                }
//                if (eventSource instanceof DaiShouPluButton) {
//                    daishouDetailName = ((DaiShouButton) eventSource)
//                        .getDaishouDetailName();
//                }
                sub(plu);

            } else if (sourceState instanceof PluSpecState) {
                String pluAttr = ((PluSpecState) sourceState).getPluAttr();
                if (eventSource instanceof PluButton) {
                    number = ((PluButton) eventSource).getPluCode(pluAttr);
                }
                plu = PLU.queryBarCode(number);
                // quan = new HYIDouble(((PluSpecState) sourceState).getNumberString());
                sub(plu);
                ((PluSpecState) sourceState).setNumberString("1");

            } else if (sourceState instanceof NumberingState) {
                numberString = ((NumberingState) sourceState).getNumberString();
                if (eventSource instanceof PluButton) {
                    // quan = new HYIDouble(numberString);
                    number = ((PluButton) eventSource).getPluCode();
                    plu = PLU.queryBarCode(number);
                    if (plu == null)
                        plu = PLU.queryByInStoreCode(number);
                    sub(plu);
                } else if (eventSource instanceof PluMenuButton) {
                    // quan = new HYIDouble(numberString);
                    number = ((PluMenuButton) eventSource).getPluNo();
                    plu = PLU.queryBarCode(number);
                    sub(plu);
                } else if (eventSource instanceof Scanner) {
                    DataEvent dataEvent = (DataEvent)event;
                    int scanDataType = ((Scanner) eventSource).getScanDataType(dataEvent.seq);
                    if (scanDataType == ScannerConst.SCAN_SDT_UNKNOWN
                            || scanDataType == ScannerConst.SCAN_SDT_Code39) {
                        setWarningMessage(res.getString("InputWrong"));
                        exitState = WarningState.class;
                        return;
                    }
                    // quan = new HYIDouble(numberString);
                    number = new String(((Scanner) eventSource).getScanData(((DataEvent)event).seq));

                    plu = PLU.queryBarCode(number);
                    if (plu == null)
                        plu = PLU.queryByInStoreCode(number);
                    if (plu == null && number.length() == 15) {
                        System.out.println("---------- plureadystate maybe periodical : " + number);
                        // 期刊
                        isPeriodical = true;
                        dtlcode1 = number.substring(13, 15);
                        number = number.substring(0, 13);
                        plu = PLU.queryBarCode(number);
                        if (plu == null)
                            plu = PLU.queryByInStoreCode(number);
                        if (plu != null && (plu.getItemtype() == null || plu.getItemtype() != 10)) {
                            plu = null;
                            number += dtlcode1;
                        }
                    }
                    sub(plu);
                } else if (eventSource instanceof BarCodeButton) {
                    // lxf 20030417 电子秤
                    // System.out.println("2");
                    if (!processScaleBarCode(numberString)) {
                        plu = PLU.queryBarCode(numberString);
                        if (plu == null)
                            plu = PLU.queryByInStoreCode(numberString);
                        number = numberString;
                        sub(plu);
                    }
                } else if (eventSource instanceof InStoreCodeButton
                        || eventSource instanceof EnterButton) {

                    CreamToolkit.logMessage("app.getCurrentTransaction().getCurrentLineItem() == null"+(app.getCurrentTransaction().getCurrentLineItem() == null) +"number="+numberString);
                    if (app.getCurrentTransaction().getCurrentLineItem() == null){
                        if (numberString.equals("88000028")) {
                            SelfbuyHttpPostRequest.selfbuyShift2();
                            DbConnection connection = null;
                            ZReport z = new ZReport();
                            try {
                                connection = CreamToolkit.getTransactionalConnection();
                                z = ZReport.getOrCreateCurrentZReport(connection);
                                connection.commit();
                            } catch (SQLException e) {
                                app.setEnableKeylock(true);
                                CreamToolkit.logMessage(e);
                                CreamToolkit.haltSystemOnDatabaseFatalError(e);
                            } finally {
                                CreamToolkit.releaseConnection(connection);
                            }

                            if (PARAM.isZPrint())
                                CreamPrinter.getInstance().printZReport(z, this);
                            //日结后立即上传
                            if (Client.getInstance().isConnected()) {
                                InlineCommandExecutor.getInstance().execute("putAttendance " + z.getSequenceNumber());
                                InlineCommandExecutor.getInstance().execute("putZ " + z.getSequenceNumber());
                            }
//                            app.setShift2(false);
                            exitState = Shutdown2State.class;
                            return;
                        } else if (numberString.equals("88000038")) {
                            CreamToolkit.halt();
                            exitState = InitialState.class;
                            return;
                        } else if (numberString.equals("88000018")) {
                            PluUpdateState.downloadMaster();
                            exitState = InitialState.class;
                            return;
                        }
                    }
                    // allan 20060601 客户希望 待售商品不能通过扫描 销售，只能通过代售键
                    if (!StringUtils.isEmpty(PARAM.getPreventScannerCatNo()))
                        needPreventScanner = true;
                    // lxf 20030417 电子秤
                    // System.out.println("3");
                    if (!processScaleBarCode(numberString)) {
                        plu = PLU.queryByInStoreCode(numberString);
                        if (plu == null)
                            plu = PLU.queryBarCode(numberString);
                        if (plu == null && numberString.length() == 15) {
                            // 期刊
                            isPeriodical = true;
                            dtlcode1 = numberString.substring(13, 15);
                            number = numberString.substring(0, 13);
                            plu = PLU.queryBarCode(number);
                            if (plu == null)
                                plu = PLU.queryByInStoreCode(number);
                            if (plu != null && (plu.getItemtype() == null  || plu.getItemtype() != 10)) {
                                plu = null;
                                number += dtlcode1;
                            }
                            numberString = number;
                        }

                        if (needPreventScanner
                            && plu != null
                            && plu.getCategoryNumber() != null
                            && plu.getCategoryNumber().trim().equals(PARAM.getPreventScannerCatNo())) {
                            setWarningMessage(res.getString("ScannerPrevented"));
                            exitState = WarningState.class;
                            return ;
                        }
                        number = numberString;
                        if (plu != null)
                            SsmpLog.report10006(numberString);
                        sub(plu);
                    }
                } else if (eventSource instanceof TaxedAmountButton) {
                    lineItem = new LineItem();
                    String taxID = ((TaxedAmountButton) eventSource).getTaxID();
                    String depID = ((TaxedAmountButton) eventSource).getDepID();
                    setExitState(MixAndMatchState.class);
                    lineItem.setTerminalNumber(app.getCurrentTransaction().getTerminalNumber());
                    if (app.getReturnItemState()) {
                        lineItem.setDetailCode("R");
                        lineItem.setQuantity(new HYIDouble(-1));
                    } else {
                        lineItem.setQuantity(new HYIDouble(1));
                        lineItem.setDetailCode("S");
                    }

                    lineItem.setPluNumber("");
                    HYIDouble amount = new HYIDouble(numberString);
                    lineItem.setUnitPrice(amount);
                    // Meyer/2003-02-21/
                    lineItem.setOriginalPrice(amount);
                    lineItem.setItemNumber("");

                    lineItem.setCategoryNumber("98");
                    lineItem.setMicroCategoryNumber(depID);
                    lineItem.setTransactionNumber(Transaction.getNextTransactionNumber());
                    lineItem.setTaxType(taxID);

                    TaxType taxType = TaxType.queryByTaxID(taxID);

                    if (taxType == null) {
                        setWarningMessage(res.getString("WrongTaxPlu"));
                        setExitState(WarningState.class);
                        WarningState.setExitState(IdleState.class);
                        return;
                    }

                    // lineItem.setDescription(taxType.getTaxName());
                    Dep dep = Dep.queryByDepID(depID);
                    if (dep == null) {
                        setWarningMessage(res.getString("WrongTaxPlu"));
                        setExitState(WarningState.class);
                        WarningState.setExitState(IdleState.class);
                        return;
                    }
                    lineItem.setDescription(dep.getDepID() + ":" + dep.getDepName());
                    lineItem.setAfterDiscountAmount(lineItem.getAmount());
                    lineItem.setAfterSIAmount(lineItem.getAmount());
                    lineItem.caculateAndSetTaxAmount();

                    if (this.lineItemColor != null)
                        lineItem.setLineItemColor(lineItemColor);

                    app.getCurrentTransaction().addLineItem(lineItem);
                    SsmpLog.report10004(taxID, depID, amount);

                    // Bruce/20021227/
                    POSTerminalApplication.getInstance().setTransactionEnd(false);
                }
//            } else if (sourceState instanceof SelectGiftState) {
//                if (eventSource instanceof NumberButton) {
//                    SelectGiftState sgs = (SelectGiftState) sourceState;
//                    sgs.setGiftIndex(sgs.getCurrentTrigger(), app.getCurrentTransaction()
//                            .getLineItemsWithoutCurrentOne().length + 1);
//                    sgs.setTriggerUsed(sgs.getCurrentTrigger(), true);
//                    number = sgs.getRetGift().getItemNO();
//                    plu = PLU.queryByInStoreCode(number);
//                    if (plu == null)
//                        plu = PLU.queryByInStoreCode(number);
//
//                    sub2(plu, new HYIDouble(1), sgs.getRetGift().getPrice());
//                    setExitState(MixAndMatchState.class);
//                }
            } else if (sourceState instanceof CategoryInputState) {
                List<String> deps = PARAM.getExclusiveZhunguiDepsList();
                if (deps != null && deps.size() > 0) {
                    boolean isMix = false;
                    // new item is dep input
                    // new item is not zhuangui item, check if lineItems contains zhuanguai item
                    for (LineItem item : getLineItems()) {
                        PLU lp = PLU.queryBarCode(item.getPluNumber());
                        if (lp == null || lp.getDepID() == null) {
                            // 部门销售键已经被拦住了
                            // lp is special item. OK
                            // isMix = false;
                        } else if (deps.contains(lp.getDepID())) {
                            // lp is a zhuangui item. NG
                            isMix = true;
                            break;
                        } else {
                            // lp is not a zhuangui item OK
                            //isMix = false;
                        }
                    }
                    if (isMix) {
                        setWarningMessage(res.getString("ZhuanGuiCanNotMixSalesWithGeneralPLU") + " : " + number);
                        WarningState.setExitState(IdleState.class);
                        number = "";
                        exitState = WarningState.class;
                        return;
                    }
                }
                lineItem = new LineItem();
                String categoryId = ((CategoryInputState) sourceState).getNumber();
                numberString = ((CategoryInputState) sourceState).getAmount();
                String catno = String.valueOf(categoryId.charAt(0));
                String midcatno = String.valueOf(categoryId.charAt(1));
                String microcatno = String.valueOf(categoryId.charAt(2));
                setExitState(MixAndMatchState.class);
                Category category = Category.queryByCategoryNumber(catno, midcatno, microcatno);
                lineItem.setTerminalNumber(app.getCurrentTransaction().getTerminalNumber());
                if (app.getReturnItemState()) {
                    lineItem.setDetailCode("R");
                    lineItem.setQuantity(new HYIDouble(-1));
                } else {
                    lineItem.setQuantity(new HYIDouble(1));
                    lineItem.setDetailCode("S");
                }

                lineItem.setPluNumber("");
                HYIDouble amount = new HYIDouble(numberString);
                lineItem.setUnitPrice(amount);
                // Meyer/2003-02-21/
                lineItem.setOriginalPrice(amount);
                lineItem.setItemNumber(categoryId);
                lineItem.setCategoryNumber(catno);
                lineItem.setMidCategoryNumber(midcatno);
                lineItem.setMicroCategoryNumber(microcatno);
                lineItem.setTransactionNumber(Transaction.getNextTransactionNumber());
                lineItem.setTaxType(category.getTaxType());

                TaxType taxType = TaxType.queryByTaxID(category.getTaxType());
                if (taxType == null) {
                    setWarningMessage(res.getString("WrongTaxPlu"));
                    setExitState(WarningState.class);
                    WarningState.setExitState(IdleState.class);
                    return;
                }
                lineItem.setDescription(category.getScreenName());
                lineItem.setAfterDiscountAmount(lineItem.getAmount());
                lineItem.setAfterSIAmount(lineItem.getAmount());
                lineItem.caculateAndSetTaxAmount();

                if (this.lineItemColor != null)
                    lineItem.setLineItemColor(lineItemColor);

                app.getCurrentTransaction().addLineItem(lineItem);
                SsmpLog.report10005(categoryId, amount);

                // Bruce/20021227/
                POSTerminalApplication.getInstance().setTransactionEnd(false);
            } else if (sourceState instanceof ShopShareSelectState) {
                numberString = ((ShopShareSelectState) sourceState).getShopBarcode();
                if (eventSource instanceof EnterButton) {
                    plu = PLU.queryBarCode(numberString);
                    String shopShare = numberString.substring(10, 12);
                    if (plu == null)
                        plu = PLU.queryByInStoreCode(numberString);
                    sub(plu, shopShare);
                }
            }
        } catch (TooManyLineItemsException e3) {
            isDaishouPlu = false;
            CreamToolkit.logMessage(e3);
            app.getWarningIndicator().setMessage(res.getString("TooManyLineItems"));
            exitState = IdleState.class;
        } catch (JposException e) {
            isDaishouPlu = false;
            CreamToolkit.logMessage(e);
            CreamToolkit.logMessage("Jpos exception at " + this);
        }
    }

    private Class dealWithMember() {
        // 若已经在交易输入过程中，不允许输入会员编号，发出警讯
        // if (!app.getTransactionEnd()) {
        // Bruce/2003-06-23 若画面上看不到任何商品（例如可能被更正掉），仍允许输入会员编号。
        if (app.getCurrentTransaction().getDisplayedLineItemsArray().size() != 0
            && !PARAM.isInputMemberCardIDInTheMiddleOfTransaction()) {
            setWarningMessage(res.getString("CannotInputMemberID"));
            WarningState.setExitState(IdleState.class);
            return WarningState.class;
        }

        if (PARAM.isQueryMemberInfo()) {
            Member member = Member.queryByMemberID(number);
            if (member == null) {
                setWarningMessage(res.getString("WrongMemberID"));
                WarningState.setExitState(IdleState.class);
                GetMemberNumberState.setMember(null);
                return WarningState.class;
            }

            // "只有类型为“批发商”的会员才能使用该销售类型"
            if ("1".equals(app.getCurrentTransaction().getTranType1())
                    && !"3".equals(member.getMemberType())) {
                setWarningMessage(res.getString("WholesaleTranTypeOnlyForWholesaleMember"));
                GetMemberNumberState.setMember(null);
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            } else {
                app.getCurrentTransaction().setMemberID(member.getMemberCardID());
                app.getCurrentTransaction().setMemberEndDate(member.getMemberEndDate());
                app.getCurrentTransaction().setBeforeTotalRebateAmt(member.getMemberActionBonus());
                // will be used by ShowMemberInfoState
                GetMemberNumberState.setMember(member);
                return ShowMemberInfoState.class;
            }
        } else {
            Member member = Member.tryCreateAnUnkownMember(number);
            app.getCurrentTransaction().setMemberID(member.getMemberCardID());
            // will be used by ShowMemberInfoState
            GetMemberNumberState.setMember(member);
            return IdleState.class;
        }

    }

    public Class exit(EventObject event, State sinkState) {
        // super.exit(event, sinkState);
        Toolkit.getDefaultToolkit().beep();
        return exitState;
    }

    public Class getExitState() {
        return exitState;
    }

    public void setExitState(Class exitState) {
        this.exitState = exitState;
    }

    private void sub(PLU plu) {
        sub(plu, null);
    }

    private void sub(PLU plu, String shopShare) {
        if (plu != null) {
            List<String> deps = PARAM.getExclusiveZhunguiDepsList();
            if (deps != null && deps.size() > 0) {
                boolean isMix = false;
                if (plu.getDepID() == null) {
                    // new item is special item, do not check if mix
                } else if (deps.contains(plu.getDepID())) {
                    // new item is zhuangui item, check if all lineItems is zhuangui items
                    for (LineItem item : getLineItems()) {
                        PLU lp = PLU.queryBarCode(item.getPluNumber());
                        if (lp == null || lp.getDepID() == null) {
                            if ("S".equals(item.getDetailCode())) {
                                //目前只有部门销售, 应拦住[专柜]不可以与[部门销售]并存
                                isMix = true;
                                break;
                            }
                            // lp is special item. OK
                            // isMix = false;
                        } else if (deps.contains(lp.getDepID())) {
                            // lp is a zhuangui item. OK
                            // isMix = false;
                        } else {
                            // lp is not a zhuangui item NG
                            isMix = true;
                            break;
                        }
                    }
                } else {
                    // new item is not zhuangui item, check if lineItems contains zhuanguai item
                    for (LineItem item : getLineItems()) {
                        PLU lp = PLU.queryBarCode(item.getPluNumber());
                        if (lp == null || lp.getDepID() == null) {
                            // 部门销售键已经被拦住了
                            // lp is special item. OK
                            // isMix = false;
                        } else if (deps.contains(lp.getDepID())) {
                            // lp is a zhuangui item. NG
                            isMix = true;
                            break;
                        } else {
                            // lp is not a zhuangui item OK
                            //isMix = false;
                        }
                    }
                }
                if (isMix) {
                    setWarningMessage(res.getString("ZhuanGuiCanNotMixSalesWithGeneralPLU") + " : " + number);
                    WarningState.setExitState(IdleState.class);
                    number = "";
                    exitState = WarningState.class;
                    return;
                }
            }

            if (plu.getPluNumber().equals(GetProperty.getWholesaleSpecialPluno())) {
                setWarningMessage(res.getString("PluWaring2") + " : " + number);
                number = "";
                setExitState(WarningState.class);
                WarningState.setExitState(IdleState.class);
                return ;
            }
            if (plu.getItemtype() != null && plu.getItemtype() == 10 && dtlcode1.trim().length() == 0) {
                isDaishouPlu = false;
                setWarningMessage(res.getString("PerioicalWaring3"));
                number = "";
                setExitState(WarningState.class);
                WarningState.setExitState(IdleState.class);
                return ;
            }

            if (plu.getAttribute2() != null)
                pluSign3 = Integer.toBinaryString(plu.getAttribute2()
                        .intValue());
            else
                pluSign3 = "";
            while (pluSign3.length() < 8) {
                pluSign3 = "0" + pluSign3;
            }
            // //CreamToolkit.logMessage("PluReadyState sub : plusign3 = " +
            // pluSign3);
            int len = pluSign3.length();
            if (len != 0) {
                if (pluSign3.startsWith("1")) {
                    setExitState(OpenPriceState.class);
                } else {
                    setExitState(MixAndMatchState.class);
                }
            } else {
                setExitState(MixAndMatchState.class);
            }
            try {
                lineItem = createLineItem(app.getCurrentTransaction(), plu, new HYIDouble(1));
                if (shopShare != null) {
                    lineItem.setShopShare(shopShare);
                }
            } catch (CreateLineItemException e) {
                setWarningMessage(e.getMessage());
                setExitState(WarningState.class);
                WarningState.setExitState(IdleState.class);
                return;
            }
            // check daishou and daifu
            if (isDaishouPlu) {
                lineItem.setDetailCode("I");
                lineItem.setDaishouNumber(daishouNumber);
                if (StringUtils.isEmpty(PARAM.getPreventScannerCatNo())) {
                    lineItem.setDiscountNumber(String.valueOf(daishouNumber));
                } else
                    lineItem.setDiscountNumber(String.valueOf(daishouNumber)
                            + "-" + String.valueOf(daishouDetailNumber));
                lineItem.setDaishouDetailName(daishouDetailName);
                lineItem.setDaishouDetailNumber(daishouDetailNumber);
                lineItem.setDescription(daishouDetailName);
                lineItem.setDescriptionAndSpecification(daishouDetailName);
                lineItem.setTaxAmount(new HYIDouble(0));
                isDaishouPlu = false;
            }

            if (isPeriodical)
                lineItem.setDtlcode1(dtlcode1);

            try {
                if (this.lineItemColor != null) {
                    lineItem.setLineItemColor(lineItemColor);
                }
                app.getCurrentTransaction().addLineItem(lineItem);

                // Bruce/20021227/
                POSTerminalApplication.getInstance().setTransactionEnd(false);
                // showText(lineItem);
            } catch (TooManyLineItemsException e) {
                isDaishouPlu = false;
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Too many LineItem exception at "
                        + this);
                app.getWarningIndicator().setMessage(
                        res.getString("TooManyLineItems"));
                exitState = IdleState.class;
            }
        } else if (plu == null) {
            plu = PLU.queryByInStoreCode(number);
            if (plu == null) {
                SsmpLog.report10001(number);
                isDaishouPlu = false;
                setWarningMessage(res.getString("PluWaring") + " : " + number);
                number = "";
                setExitState(WarningState.class);
                WarningState.setExitState(IdleState.class);
            }
        }
    }

    public static LineItem createLineItem(Transaction transaction, PLU plu,
        HYIDouble quantity) throws CreateLineItemException {
        return createLineItem(transaction, plu, quantity, null);
    }

    public static LineItem createLineItem(Transaction transaction, PLU plu,
        HYIDouble quantity, HYIDouble sellPrice) throws CreateLineItemException {

        POSTerminalApplication app = POSTerminalApplication.getInstance();
        ResourceBundle res = CreamToolkit.GetResource();

        HYIDouble taxAmount = new HYIDouble("0.00");

        LineItem lineItem = new LineItem();

        // check return item
        if (app.getReturnItemState()) {
            quantity = quantity.negate();
            lineItem.setDetailCode("R");
        } else {
            lineItem.setDetailCode("S");
        }

        // set lineItem properties

        //Bruce/20080606 Don't have to get transaction number from database here
        //lineItem.setTransactionNumber(new Integer(Transaction.getNextTransactionNumber()));
        lineItem.setTransactionNumber(transaction.getTransactionNumber());

        lineItem.setTerminalNumber(app.getCurrentTransaction().getTerminalNumber());
        lineItem
                .setLineItemSequence(app.getCurrentTransaction().getLineItems().length + 1);
        lineItem.setRemoved(false);
        lineItem.setCategoryNumber(plu.getCategoryNumber());
        lineItem.setMicroCategoryNumber(plu.getMicroCategoryNumber());
        lineItem.setMidCategoryNumber(plu.getMidCategoryNumber());
        lineItem.setThinCategoryNumber(plu.getThinCategoryNumber());
        lineItem.setPluNumber(plu.getPluNumber());
        lineItem.setDescription(plu.getScreenName());
        lineItem.setDescriptionAndSpecification(plu
                .getNameAndSpecification());
        lineItem.setQuantity(quantity.setScale(2, BigDecimal.ROUND_HALF_UP));
        lineItem.setTaxType(plu.getTaxType());
        lineItem.setIsScaleItem(plu.isWeightedPlu());
        lineItem.setOpenPrice(plu.isOpenPrice());

        //Bruce/2003-08-08
        lineItem.setSmallUnit(plu.getSmallUnit());
        lineItem.setInvCycleNo(plu.getInvCycleNo());
        lineItem.setSizeCategory(plu.getSizeCategory());
        lineItem.setItemBrand(plu.getItemBrand());
        lineItem.setSize(plu.getSize());
        lineItem.setColor(plu.getColor());
        lineItem.setStyle(plu.getStyle());
        lineItem.setSeason(plu.getSeason());

        //Bruce/20080606/ Cache this for speedup.
        lineItem.setIsScaleItem(plu.isWeightedPlu());
        lineItem.setDepID(plu.getDepID());

        if (app.getItemList().isDIY())
            lineItem.setDIYIndex(app.getItemList().getCurrentIndex());

        // Meyer/2003-02-19/ Add 2 Field: itemNumber, originalPrice
        lineItem.setItemNumber(plu.getInStoreCode());
        lineItem.setOriginalPrice(plu.getUnitPrice());

        TaxType taxType = TaxType.queryByTaxID(plu.getTaxType());
        if (taxType == null) {
//            setWarningMessage(res.getString("WrongTaxPlu"));
//            setExitState(WarningState.class);
//            WarningState.setExitState(IdleState.class);
            throw new CreateLineItemException(res.getString("WrongTaxPlu"));
        }

//        int round = 0;
//        String taxRound = taxType.getRound();
//        if (taxRound.equalsIgnoreCase("R")) {
//            round = BigDecimal.ROUND_HALF_UP;
//        } else if (taxRound.equalsIgnoreCase("C")) {
//            round = BigDecimal.ROUND_UP;
//        } else if (taxRound.equalsIgnoreCase("D")) {
//            round = BigDecimal.ROUND_DOWN;
//        }
//        String taxTp = taxType.getType();

        //
        // Meyer/2003-02-19/ Modifyed, get the price for saling ,maybe
        // special price
        // set discountType = 'D' if meeting special price
        Object[] salingPriceDetail = plu.getSalingPriceDetail();
        HYIDouble pluPrice = (HYIDouble) salingPriceDetail[0];

        String memberID = POSTerminalApplication.getInstance()
                .getCurrentTransaction().getMemberID();
        if (memberID == null)
            memberID = "";
        boolean isMemberPriceExist = false;
        if (memberID.length() > 0 && plu.getMemberPrice() != null
                && !plu.getMemberPrice().equals(zero))
            isMemberPriceExist = true;
        if ("SPECIAL_PRICE".equalsIgnoreCase((String) salingPriceDetail[1])) {
            // Bruce/20030519
            // 如果使用的是特卖价，而且是会员，且会员价非为空的话，则OriginalPrice填会员价
            lineItem.setDiscountType("D");
            if (isMemberPriceExist && PARAM.getOriginalPriceVersion().equals("1")){
                //lineItem.setOriginalPrice(plu.getMemberPrice());
                HYIDouble memberPrice = plu.getMemberPrice(GetMemberNumberState.getMember());
                if (memberPrice != null){
                    lineItem.setOriginalPrice(memberPrice);
                }
            }
        } else if ("ORDER_PRICE".equalsIgnoreCase((String) salingPriceDetail[1])) {
            lineItem.setOriginalPrice(pluPrice);
        }

        // 限量促销
        LimitQtySold lqs = null;
        // System.out.println("is Limit Quantity Sale [" +
        // plu.isQuantityLimited() + "]");
        if (plu.isQuantityLimited()) {
            try {
                // 查询限价销售信息, 需要连线
                Client client = Client.getInstance();
                client.processCommand("queryLimitQtySold "
                        + lineItem.getItemNumber() + " 1 " + memberID);
                lqs = (LimitQtySold) client.getReturnObject();
                if (lqs != null && lqs.isSaleable()) {
                    // System.out.println("Limit Quantity Sold:\n rebate
                    // rate[" + lqs.getLimitRebateRate() + "] + price [" +
                    // lqs.getLimitPrice() + "]");
                    lineItem.setDiscountType("L");
                    pluPrice = lqs.getLimitPrice();
                    if (isMemberPriceExist && PARAM.getOriginalPriceVersion().equals("1")){
                        //lineItem.setOriginalPrice(plu.getMemberPrice());
                        HYIDouble memberPrice = plu.getMemberPrice(GetMemberNumberState.getMember());
                        if (memberPrice != null){
                            lineItem.setOriginalPrice(memberPrice);
                        }
                    }
                }
            } catch (ClientCommandException e) {
                CreamToolkit.logMessage(e);
            }
        }

        // 限量销售商品每次只能录入一单品
        if (lineItem.getDiscountType() != null
                && lineItem.getDiscountType().equals("L")
                && quantity.intValue() > 1) {
//            setWarningMessage(res.getString("LimitQtyNumber"));
//            setExitState(WarningState.class);
//            WarningState.setExitState(IdleState.class);
//            return false;
            throw new CreateLineItemException(res.getString("LimitQtyNumber"));

        }
        lineItem.setUnitPrice(pluPrice); // 通过取价格逻辑，填写单价

//        TaxType taxType = TaxType.queryByTaxID(plu.getTaxType());
//        if (taxType == null) {
//            setWarningMessage(res.getString("WrongTaxPlu"));
//            exitState = WarningState.class;
//            WarningState.setExitState(IdleState.class);
//            return false;
//        }
        // 售价并非从pos.plu中获得
        if (sellPrice != null)
            pluPrice = sellPrice;
        lineItem.setUnitPrice(pluPrice);

        lineItem.setAfterDiscountAmount(lineItem.getAmount());
        lineItem.setAfterSIAmount(lineItem.getAmount());

//        int round = 0;
//        String taxRound = taxType.getRound();
//        if (taxRound.equalsIgnoreCase("R")) {
//            round = BigDecimal.ROUND_HALF_UP;
//        } else if (taxRound.equalsIgnoreCase("C")) {
//            round = BigDecimal.ROUND_UP;
//        } else if (taxRound.equalsIgnoreCase("D")) {
//            round = BigDecimal.ROUND_DOWN;
//        }
//        String taxTp = taxType.getType();
//
//        HYIDouble taxPercent = taxType.getPercent();
//        int taxDigit = taxType.getDecimalDigit().intValue();
//        if (taxTp.equalsIgnoreCase("1")) {
//            lineItem.setUnitPrice(pluPrice);
//            HYIDouble unit = new HYIDouble(1);
//            taxAmount = ((lineItem.getAmount().multiply(taxPercent))
//                    .divide(unit.addMe(taxPercent), 2)).setScale(taxDigit,
//                    round);
//        } else if (taxTp.equalsIgnoreCase("2")) {
//            HYIDouble unit = new HYIDouble(1);
//            //重设单价
//            lineItem
//                    .setUnitPrice(pluPrice.multiply(unit.addMe(taxPercent)));
//            taxAmount = (lineItem.getAmount().multiply(taxPercent))
//                    .setScale(taxDigit, round);
//        } else {
//             //重设单价
//            lineItem.setUnitPrice(pluPrice);
//            HYIDouble tax = new HYIDouble(0.00);
//            taxAmount = tax.setScale(taxDigit, round);
//        }
//        lineItem.setTaxAmount(taxAmount);
        lineItem.caculateAndSetTaxAmount(); //注意: 不再重设单价

        /*
         * if (pluSign3.charAt(1) == '1') { lineItem.setDetailCode("I"); }
         * else if (pluSign3.charAt(3) == '1') {
         * lineItem.setDetailCode("Q"); } else if (pluSign3.charAt(4) ==
         * '1') { lineItem.setDetailCode("B"); } else {
         * lineItem.setDetailCode("S"); }
         */

        // ZhaoH 计算还元金
        if (StateToolkit.needCheckRebate(app.getCurrentTransaction())) {
            HYIDouble rate;
            // 限量促销从LimitQtySold 取还圆金比率
            if ("L".equalsIgnoreCase(lineItem.getDiscountType()))
                rate = lqs.getLimitRebateRate();
            else
                rate = Rebate.getRebateRate(lineItem.getItemNumber());

            lineItem.setRebateRate(rate);
            // HYIDouble itemRebate =
            // lineItem.getAfterDiscountAmount().multiply(rate).setScale(1,
            // BigDecimal.ROUND_HALF_UP);
            // modify 2004-03-29
            int scale = 1;
            try {
                scale = PARAM.getRebateAmountScale();
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            HYIDouble itemRebate = lineItem.getAfterDiscountAmount()
                    .multiply(rate).setScale(scale,
                    BigDecimal.ROUND_HALF_UP);
            lineItem.setAddRebateAmount(itemRebate); // 本单品增加还元金
        } else {
            lineItem.setAddRebateAmount(new HYIDouble(0.0));
        }

        // diy模式下，无组合促销功能
        if (!app.getItemList().isDIY())
            // 显示该商品是组合促销商品
            if (PARAM.isMixAndMatchPrompt()) {
                Iterator it = MMGroup.queryByITEMNO(plu.getInStoreCode());
                if (it != null) {
                    while (it.hasNext()) {
                        if (MixAndMatchState.validMM(((MMGroup) it.next())
                                .getID()) != null) {
                            app.getWarningIndicator().setMessage(
                                    res.getString("MixAndMatchPrompt1"));
                            break;
                        }
                    }
                }

            }
        return lineItem;
    }

    /**
     * @author ll 2003.04.17 分析电子称PLU
     * @param number
     */
    public boolean processScaleBarCode(String number) {
        boolean ret = false;
        int type = 0;
        java.util.Map formatMap = app.getScaleBarCodeFormat();

        if (formatMap == null)
            return ret;
        for (Iterator it = formatMap.keySet().iterator(); it.hasNext();) {
            try {
                int totalLength = Integer.parseInt((String) it.next());
                java.util.List formatList = (java.util.List) formatMap
                        .get(String.valueOf(totalLength));
                if (formatList == null || formatList.isEmpty())
                    continue;
                if ((number.length()) != totalLength)
                    continue;
                String pref = (String) formatList.get(0);
                if (!number.startsWith(pref)) {
                    continue;
                }

                int iStart = ((Integer) formatList.get(1)).intValue();
                int iEnd = ((Integer) formatList.get(2)).intValue();
                int wStart = -1;
                int wEnd = -1;
                int wComma = -1;
                int pStart = -1;
                int pEnd = -1;
                int pComma = -1;
                String itemNo = number.substring(iStart, iEnd);
                HYIDouble wCount = null;
                HYIDouble pCount = null;

                if (totalLength == 8) {
                    // X-plu Q-重量 B-金额
                    // 8码: 2XXXXXXC 自编码 价格从plu取
//                } else if (totalLength == 13) {

                } else if (totalLength == 13) {
                    if (((Integer) formatList.get(6)).intValue() == 0) {
                        wStart = ((Integer) formatList.get(3)).intValue();
                        wEnd = ((Integer) formatList.get(4)).intValue();
                        wComma = ((Integer) formatList.get(5)).intValue();
                        String weight = number.substring(wStart, wEnd);
                        weight = weight.substring(0, wComma) + "."
                                + weight.substring(wComma, weight.length());
                        wCount = new HYIDouble(weight);
                        wCount = wCount.setScale(3, BigDecimal.ROUND_HALF_UP);
                        type = 1;
                    } else {
                        // 13码: 2XXXXXXBBBBBC 价格从条码中取
                        pStart = ((Integer) formatList.get(6)).intValue();
                        pEnd = ((Integer) formatList.get(7)).intValue();
                        pComma = ((Integer) formatList.get(8)).intValue();
                        String price = number.substring(pStart, pEnd);
                        price = price.substring(0, pComma) + "."
                                + price.substring(pComma, price.length());
                        pCount = new HYIDouble(price);
                        pCount = pCount.setScale(2, BigDecimal.ROUND_HALF_UP);
                        wCount = new HYIDouble(1);
                        type = 0;
                    }

                } else if (totalLength == 18) {
                    // 18码: 2XXXXXXQQQQQBBBBBC 重量,价格从条码中取
                    wStart = ((Integer) formatList.get(3)).intValue();
                    wEnd = ((Integer) formatList.get(4)).intValue();
                    wComma = ((Integer) formatList.get(5)).intValue();
                    pStart = ((Integer) formatList.get(6)).intValue();
                    pEnd = ((Integer) formatList.get(7)).intValue();
                    pComma = ((Integer) formatList.get(8)).intValue();
                    String weight = number.substring(wStart, wEnd);
                    weight = weight.substring(0, wComma) + "."
                            + weight.substring(wComma, weight.length());
                    String price = number.substring(pStart, pEnd);
                    price = price.substring(0, pComma) + "."
                            + price.substring(pComma, price.length());
                    wCount = new HYIDouble(weight);
                    wCount = wCount.setScale(2, BigDecimal.ROUND_HALF_UP);
                    pCount = new HYIDouble(price);
                    pCount = pCount.setScale(2, BigDecimal.ROUND_HALF_UP);
                }
                plu = PLU.queryByInStoreCode(itemNo);

                if (type == 1)
                    pCount = CreamToolkit.getPluPrice(app.getCurrentTransaction(),plu);

                // if (plu != null)
                // System.out.println("plu is not null");
                sub2(plu, wCount, pCount, type);
                ret = true;
                break;
                /*
                 * sub(plu); LineItem lineItem =
                 * curTransaction.getCurrentLineItem();
                 * lineItem.setWeight(wCount); lineItem.setAmount(pCount);
                 */
            } catch (Exception e) {
                e.printStackTrace();
                // e.printStackTrace(CreamToolkit.getLogger());
            }
        }
        return ret;
    }

    private void sub2(PLU plu, HYIDouble weight, HYIDouble price, int type) {
        if (plu != null) {
            if (plu.getAttribute2() != null)
                pluSign3 = Integer.toBinaryString(plu.getAttribute2()
                        .intValue());
            else
                pluSign3 = "";
            while (pluSign3.length() < 8) {
                pluSign3 = "0" + pluSign3;
            }
            // //CreamToolkit.logMessage("PluReadyState sub : plusign3 = " +
            // pluSign3);
            int len = pluSign3.length();
            if (len != 0) {
                if (pluSign3.startsWith("1")) {
                    setExitState(OpenPriceState.class);
                } else {
                    setExitState(MixAndMatchState.class);
                }
            } else {
                setExitState(MixAndMatchState.class);
            }
            HYIDouble quan = null;
            if (weight == null) {
                quan = new HYIDouble(1);
            } else
                quan = weight;
            try {
                lineItem = createLineItem(app.getCurrentTransaction(), plu, new HYIDouble(1));
            } catch (CreateLineItemException e) {
                setWarningMessage(e.getMessage());
                setExitState(WarningState.class);
                WarningState.setExitState(IdleState.class);
                return;
            }
            // check daishou and daifu
            if (isDaishouPlu) {
                lineItem.setDetailCode("I");
                lineItem.setDaishouNumber(daishouNumber);
                if (StringUtils.isEmpty(PARAM.getPreventScannerCatNo())) {
                    lineItem.setDiscountNumber(String.valueOf(daishouNumber));
                } else
                    lineItem.setDiscountNumber(String.valueOf(daishouNumber)
                            + "-" + String.valueOf(daishouDetailNumber));
                lineItem.setDaishouDetailName(daishouDetailName);
                lineItem.setDaishouDetailNumber(daishouDetailNumber);
                lineItem.setDescription(daishouDetailName);
                lineItem.setDescriptionAndSpecification(daishouDetailName);
                lineItem.setTaxAmount(new HYIDouble(0));
                isDaishouPlu = false;
            }

            try {
                if (this.lineItemColor != null) {
                    lineItem.setLineItemColor(lineItemColor);
                }
                if (weight != null) {
                    lineItem.setWeight(weight);
                    lineItem.setQuantity(weight);
                    lineItem.setQuantityEnabled(false);
                }
                if (price != null) {
                    if (type == 1) {
                        HYIDouble amt = price.multiply(weight);
                        lineItem.setAmount(amt);
                        lineItem.setAfterDiscountAmount(amt);
                    } else {
                        lineItem.setAmount(price);
                        lineItem.setAfterDiscountAmount(price);
                    }


                    lineItem.setQuantityEnabled(false);
                } else {
                    HYIDouble pluAmt = CreamToolkit.getPluPrice(app.getCurrentTransaction(),
                            plu);
                    lineItem.setAmount(pluAmt);
                    lineItem.setAfterDiscountAmount(pluAmt);
                }
                app.getCurrentTransaction().addLineItem(lineItem);

                // Bruce/20021227/
                POSTerminalApplication.getInstance().setTransactionEnd(false);
                // showText(lineItem);
            } catch (TooManyLineItemsException e) {
                isDaishouPlu = false;
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Too many LineItem exception at "
                        + this);
                app.getWarningIndicator().setMessage(
                        res.getString("TooManyLineItems"));
                exitState = IdleState.class;
            }
        } else if (plu == null) {
            isDaishouPlu = false;
            setWarningMessage(res.getString("PluWaring") + " : " + number);
            number = "";
            setExitState(WarningState.class);
            WarningState.setExitState(IdleState.class);
        }
    }

}
