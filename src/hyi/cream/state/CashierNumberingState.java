/**
 * State class
 * @since 2000
 * @author slackware
 */

package hyi.cream.state;

public class CashierNumberingState extends SomeAGNumberingState {
    static CashierNumberingState cashierNumberingState = null;

    public static CashierNumberingState getInstance() {
        try {
            if (cashierNumberingState == null) {
                cashierNumberingState = new CashierNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return cashierNumberingState;
    }

    /**
     * Constructor
     */
    public CashierNumberingState() throws InstantiationException {
    }
}



