package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.*;
import hyi.cream.groovydac.CardDetail;
import hyi.cream.groovydac.CardDetailChinaTrust;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;

import java.awt.*;
import java.io.IOException;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;

import org.apache.commons.lang.StringUtils;

public class SetPropertyState extends State {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private String numberString = "";
    private String property = "";
    private String text = "";

    static SetPropertyState setPropertyState = null;

    public static SetPropertyState getInstance() {
        try {
            if (setPropertyState == null) {
                setPropertyState = new SetPropertyState();
            }
        } catch (InstantiationException ex) {
        }
        return setPropertyState;
    }

    /**
     * Constructor
     */
    public SetPropertyState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof SelectButton
            && sourceState instanceof ConfigState) {
            property = ((ConfigState)sourceState).getProperty();
            CreamToolkit.logMessage("SetPropertyState | " + property);
            if (property.equalsIgnoreCase("ReceiptPrint")) {
                text = m("SetProperty1");
            } else if (property.equalsIgnoreCase("CheckDrawerClose")) {
                text = m("SetProperty1");
            } else if (property.equalsIgnoreCase("DrawerCloseTime")) {
                text = m("SetProperty2");
            } else if (property.equalsIgnoreCase("ZPrint")) {
                text = m("SetProperty1");
            } else if (property.equalsIgnoreCase("ShiftPrint")) {
                text = m("SetProperty1");
            } else if (property.equalsIgnoreCase("SetPOSNumber")) {
                text = m("InputPOSNumber");
            } else if (property.equalsIgnoreCase("SetBeginningTransactionNumber")) {
                text = m("InputBeginningTransactionNumber");
            } else if (property.equalsIgnoreCase("ClearAllTransaction")) {
                text = m("ClearAllTransactionConfirm");
            } else if (property.equalsIgnoreCase("SetRemoteBackupIP")) {
                text = m("SetRemoteMachineIP");
            }
            numberString = "";
            app.getMessageIndicator().setMessage(text);
        }

        if (event.getSource() instanceof NumberButton
            && sourceState instanceof SetPropertyState) {
            numberString = numberString + ((NumberButton)event.getSource()).getNumberLabel();
            app.getMessageIndicator().setMessage(numberString);
        }

        if (event.getSource() instanceof ClearButton
            && sourceState instanceof SetPropertyState) {
            numberString = "";
            app.getMessageIndicator().setMessage(text);
        }

        if (event.getSource() instanceof EnterButton
            && sourceState instanceof SetPropertyState) {
            numberString = "";
            app.getMessageIndicator().setMessage(text);
        }
    }

    public Class exit(EventObject event, State sinkState) {

        if (event.getSource() instanceof EnterButton) {
            if (numberString.equals("")) {
                return SetPropertyState.class;
            }

            if (!property.equalsIgnoreCase("SetRemoteBackupIP") && !CreamToolkit.checkInput(numberString, 0)) {
                numberString = "";
                return SetPropertyState.class;
            }

            //  set DrawerCloseTime property
            if (property.equalsIgnoreCase("DrawerCloseTime")) {
                PARAM.updateDrawerCloseTime(Integer.parseInt(numberString));
                return ConfigState.class;

            //Bruce/20030318
            } else if (property.equalsIgnoreCase("SetPOSNumber")) {
                int posno;
                try {
                    posno = Integer.parseInt(numberString);
                    if (posno <= 0 || posno > 30)
                        return SetPropertyState.class;
                } catch (NumberFormatException e2) {
                    return SetPropertyState.class;
                }

                try {
                    Process p = Runtime.getRuntime().exec("sudo /bin/bash /home/hyi/cream/setposno.sh " + posno);
                    POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                        CreamToolkit.GetResource().getString("ReadyToRestart"));
                    p.waitFor();
                    CreamToolkit.stopPos(1);
                } catch (IOException e) {
                    CreamToolkit.logMessage(e);
                } catch (InterruptedException e) {
                    CreamToolkit.logMessage(e);
                }
                return ConfigState.class;

            //Bruce/20030402
            } else if (property.equalsIgnoreCase("SetBeginningTransactionNumber")) {
                DbConnection connection = null;
                try {
                    int tranNo = Integer.parseInt(numberString);
                    if (tranNo > 0) {
                        PARAM.updateNextTransactionNumber(tranNo);

                        POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                            CreamToolkit.GetResource().getString("ReadyToRestart"));
                        CreamToolkit.stopPos(1);
                        return ConfigState.class;
                    } else {
                        return SetPropertyState.class;
                    }
                } catch (NumberFormatException e2) {
                    Toolkit.getDefaultToolkit().beep();
                    return SetPropertyState.class;
                } finally {
                    CreamToolkit.releaseConnection(connection);
                }

            //Bruce/20030402
            } else if (property.equalsIgnoreCase("ClearAllTransaction")) {
                if (numberString.equals("1")) {
                    DbConnection connection = null;
                    try {
                        connection = CreamToolkit.getTransactionalConnection();
                        new Transaction().deleteAll(connection);
                        new LineItem().deleteAll(connection);
                        new Alipay_detail().deleteAll(connection);
                        new WeiXin_detail().deleteAll(connection);
                        new UnionPay_detail().deleteAll(connection);
                        new ZReport().deleteAll(connection);
                        new ShiftReport().deleteAll(connection);
                        new CashForm().deleteAll(connection);
                        new DepSales().deleteAll(connection);
                        new DaishouSales().deleteAll(connection);
                        new DaiShouSales2().deleteAll(connection);
                        new CategorySales().deleteAll(connection);
                        new TransactionHold().deleteAll(connection);
                        new LineItemHold().deleteAll(connection);
                        new ZEx().deleteAll(connection);
                        new ShiftEx().deleteAll(connection);
                        new CardDetailChinaTrust().deleteAll(connection);
                        new CardDetail().deleteAll(connection);
                        PARAM.updateNextTransactionNumber(1);
                        PARAM.updateInvoiceNumber("");
                        PARAM.updateInvoiceID("");

                        POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                            CreamToolkit.GetResource().getString("ReadyToRestart"));

                        try {
                            new Selfbuy_head().deleteAll(connection);
                            new Selfbuy_detail().deleteAll(connection);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        connection.commit();
                        CreamToolkit.stopPos(1);

                    } catch (Exception e) {
                        CreamToolkit.logMessage(e);
                    } finally {
                        CreamToolkit.releaseConnection(connection);
                    }
                }
                return ConfigState.class;

            } else if (property.equalsIgnoreCase("SetRemoteBackupIP")) {
                //CreamPropertyUtil prop = CreamPropertyUtil.getInstance();
                //prop.setProperty("MySQLRemoteBackupDirectory", numberString + "::backup_path/");
                //prop.depositInIndependentTransaction(new String[] {"MySQLRemoteBackupDirectory"});
                return ConfigState.class;

            //  set about 'yes' or 'no' property
            } else {
                boolean returnConfig = false;
                String propertyNameInParam = StringUtils.uncapitalize(property);
                if (numberString.equals("1")) {
                    PARAM.setProperty(propertyNameInParam, "yes");
                    returnConfig = true;
                } else if (numberString.equals("2")) {
                    PARAM.setProperty(propertyNameInParam, "no");
                    returnConfig = true;
                } else {
                    app.getWarningIndicator().setMessage(m("InputAgain"));
                }
                //System.out.println(property + "|" + prop.getProperty(property));
                if (property.compareTo("ReceiptPrint") == 0) {
                    //boolean print = true;
                    //String value = GetProperty.getproperty("");
                    //if (value.compareTo("yes") == 0)
                    //    print = true;
                    //else
                    //    print = false;
                    CreamPrinter.getInstance().setPrintEnabled(numberString.equals("1"));
                }
                if (returnConfig)
                    return ConfigState.class;
                else
                    return SetPropertyState.class;
            }
        }

        if (event.getSource() instanceof ClearButton) {
            if (numberString.equals("")) {
                return ConfigState.class;
            } else {
                return SetPropertyState.class;
            }
        }
        return sinkState.getClass();
    }
}