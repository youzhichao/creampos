package hyi.cream.state;

import hyi.cream.inline.InlineCommandExecutor;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;
//import jpos.*;
import hyi.spos.JposException;
import hyi.spos.events.DataEvent;

public class InOutNumberState extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private ResourceBundle res = CreamToolkit.GetResource();

	private String number = "";

    private String scanID = "";

	private String dealType2 = "";

	static InOutNumberState inOutNumberState = null;
	
	private Cashier cashier;

    private String passwordNumber = ""; //考勤要验证密码;

    private String passwordNumber1 = "";//这个变量用来存放考勤时pos机上输入密码显示****用.

    private boolean isCheckCashierPsswd = false; //用来判断是否为验证密码的状态

	public static InOutNumberState getInstance() {
		try {
			if (inOutNumberState == null) {
				inOutNumberState = new InOutNumberState();
			}
		} catch (InstantiationException ex) {
		}
		return inOutNumberState;
	}

	/**
	 * Constructor
	 */
	public InOutNumberState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
		// System.out.println("This is InOutNumberState's Entry!");
		if (sourceState instanceof InOutSelectState
		/* && event.getSource() instanceof NumberButton */) {
		    Transaction trans = app.getCurrentTransaction();
			dealType2 = trans.getDealType2(); // "B" for logon, "C" for logout
			number = "";
            passwordNumber = "";  scanID =""; passwordNumber1 = "";
			app.getMessageIndicator().setMessage(res.getString("ENumber"));
		} else if (sourceState instanceof InOutNumberState
				&& event.getSource() instanceof ClearButton) {
            if (!isCheckCashierPsswd){
                number = "";
                scanID = "";
                app.getMessageIndicator().setMessage(res.getString("ENumber"));
            } else
                app.getMessageIndicator().setMessage(res.getString("Password"));
		}
	}

	public Class exit(EventObject event, State sinkState) {
		// System.out.println("This is InOutNumberState's Exit!");
		if (event.getSource() instanceof NumberButton) {
            if(!isCheckCashierPsswd){
                number = number + ((NumberButton) event.getSource()).getNumberLabel();
                app.getMessageIndicator().setMessage(number);
            } else {
                passwordNumber = passwordNumber + ((NumberButton) event.getSource()).getNumberLabel();
                passwordNumber1 += "*";
                app.getMessageIndicator().setMessage(passwordNumber1);
            }
			return InOutNumberState.class;
		}

		if (((event.getClass().getName().equals("jpos.events.DataEvent") || event.getClass().getName().equals("hyi.spos.events.DataEvent"))
				&& event.getSource().getClass().getName().endsWith("Scanner"))
                || (!"".equals(scanID) && isCheckCashierPsswd && event.getSource() instanceof EnterButton)) {
			try {
                if (!isCheckCashierPsswd) {
                    scanID = "";number = ""; //扫描工号时清空一下.
                    hyi.spos.Scanner scanner = (hyi.spos.Scanner) event.getSource();
                    scanner.setDataEventEnabled(true);
                    scanID = new String(((hyi.spos.Scanner) event.getSource()).getScanData(((DataEvent) event).seq));
                    if (scanID.equals("")) {
                        return InOutNumberState.class;
                    } else {
                        if (!checkValidityOfCardID(scanID)) {
//                            scanID = "";
                            return InOutNumberState.class;
                        } else {
                            app .getWarningIndicator().setMessage("");
                            app.getMessageIndicator().setMessage(res.getString("Password"));
                            isCheckCashierPsswd = true;
                            return InOutNumberState.class;
                        }
                    }
                } else {
                    if(!checkCashierIDandPsswd(scanID, passwordNumber))
                        return InOutNumberState.class;
                    if(checkInOutOfCardID(scanID , dealType2)){ //已考勤过
                        scanID = "";
                        passwordNumber = "";
                        passwordNumber1 = "";
                        isCheckCashierPsswd = false ;
                        return InOutNumberState.class;
                    }
//					boolean ifCheckIDLenthCheckInout = GetProperty
//							.getIfCheckIDLenthCheckInout("no")
//							.equalsIgnoreCase("yes");
//
//					// modify 2005-03-22 全家希望任何员工码都接受
//
//					boolean onlyCheckIDLenOnCheckInOut = GetProperty
//							.getOnlyCheckIDLenOnCheckInOut("no")
//							.equalsIgnoreCase("yes");
//
//					// modify 2004-03-29 灿坤希望只要是7位的员工码就接受
//
//					boolean checkCashierDigit = GetProperty
//							.getCheckCashierDigit("no")
//							.equalsIgnoreCase("yes");
//					// modify 2005-04-06 检查最后一位校验码
//
//					if (ifCheckIDLenthCheckInout) { // 不检查长度与位数
//						if (scanID.length() == 0) {
//							app.getWarningIndicator().setMessage(
//									res.getString("CashierNumberWrong"));
//							scanID = "";
//							return InOutNumberState.class;
//						}
//
//					} else if (onlyCheckIDLenOnCheckInOut) { // 检查是否为7位
//						int idLen = 7;
//						try {
//							idLen = Integer.parseInt(GetProperty
//									.getCheckInOutIDLens("7"));
//						} catch (NumberFormatException e) {
//						}
//						if (scanID.length() != idLen) {
//							app.getWarningIndicator().setMessage(
//									res.getString("CashierNumberWrong"));
//							scanID = "";
//							return InOutNumberState.class;
//						} else {
//							if (checkCashierDigit) { // 检查最后一位校验码
//								boolean checkDigit = Cashier
//										.checkDigitCashierID(scanID);
//								if (!checkDigit) {
//									app.getWarningIndicator().setMessage(
//											res.getString("DigitNumberError"));
//									scanID = "";
//									return InOutNumberState.class;
//								}
//							}
//						}
//					} else if (checkCashierDigit) { // 检查最后一位校验码
//						boolean checkDigit = Cashier
//								.checkDigitCashierID(scanID);
//						if (!checkDigit) {
//							app.getWarningIndicator().setMessage(
//									res.getString("DigitNumberError"));
//							scanID = "";
//							return InOutNumberState.class;
//						}
//					} else {
//						Cashier c = Cashier.queryByCashierID(scanID);
//						if (c == null) {
//							app.getWarningIndicator().setMessage(
//									res.getString("CashierNumberWrong"));
//							scanID = "";
//							return InOutNumberState.class;
//						}
//					}

					app.getWarningIndicator().setMessage("");

					Transaction trans = app.getCurrentTransaction();
                    DbConnection connection = null;
                    try {
                        connection = CreamToolkit.getTransactionalConnection();
                        trans.setEmployeeNumber(scanID);
                        if (Attendance1.isZReportEnd()) //日结后做考勤时 ,头档Z账序号字段记为下一Z账序号.
                            trans.setZSequenceNumber(new Integer(trans.getZSequenceNumber().intValue() + 1));
    					trans.store(connection);
    					trans.clear();
                        connection.commit();
                    } catch (SQLException e) {
                        CreamToolkit.logMessage(e);
                        CreamToolkit.haltSystemOnDatabaseFatalError(e);
                    } finally {
                        CreamToolkit.releaseConnection(connection);
                        CreamToolkit.logMessage("start to putAttendance ....");
                        InlineCommandExecutor.getInstance().execute("putAttendance");
                    }

                    app.setChecked(false);
					// app.setSalemanChecked(false);
					if (!PARAM.isContinuousLogon()) {
						trans.setDealType2("0");
                        cashier = null;
						return KeyLock1State.class;
					} else {
						trans.setDealType2(dealType2);
						app.getMessageIndicator().setMessage(
								res.getString("CashierNo") + scanID
                                        + getCashierName()
										+ res.getString("AttendanceMessage")
										+ " "
										+ res.getString("PleaseEnterClear"));
						scanID = "";
                        passwordNumber = "";
                        passwordNumber1 = "";
                        isCheckCashierPsswd = false;
						cashier = null;
						return InOutNumberState.class;
					}
				}
			} catch (JposException je) {
			}
		}

		if (event.getSource() instanceof ClearButton) {
            if(!passwordNumber.equals("")){
                passwordNumber = "";
                passwordNumber1 = "";
                app.getMessageIndicator().setMessage("");
				app.getWarningIndicator().setMessage("");
				return InOutNumberState.class;
            } else isCheckCashierPsswd = false ;

            if(!scanID.equals("") && passwordNumber.equals("")){
                scanID = "";
                isCheckCashierPsswd = false ;
				app.getMessageIndicator().setMessage("");
				app.getWarningIndicator().setMessage("");
				return InOutNumberState.class;
            }

			if (number.equals("")) {
				app.getWarningIndicator().setMessage("");
				return InOutSelectState.class;
			} else {
				number = "";
                isCheckCashierPsswd = false;
				app.getMessageIndicator().setMessage("");
				app.getWarningIndicator().setMessage("");
				return InOutNumberState.class;
			}
		}

		if (event.getSource() instanceof EnterButton) {
            if (!isCheckCashierPsswd) {
                if (number.equals("")) {
                    return InOutNumberState.class;
                } else {
//				if (!checkValidityOfCardID(number))
//					return InOutNumberState.class;
                    scanID = "";
                    if (!checkValidityOfCardID(number)) {
                        return InOutNumberState.class;
                    } else if (checkValidityOfCardID(number)) {
                        app.getWarningIndicator().setMessage("");
                        app.getMessageIndicator().setMessage(res.getString("Password"));
                        isCheckCashierPsswd = true;
                        return InOutNumberState.class;
                    }
                }
            } else {
                if(!checkCashierIDandPsswd(number, passwordNumber))
                        return InOutNumberState.class;
                if (checkInOutOfCardID(number, dealType2)) {
                    number = "";
                    passwordNumber = "";
                    passwordNumber1 = "";
                    isCheckCashierPsswd = false;
                    return InOutNumberState.class;
                }

                app.getWarningIndicator().setMessage("");

				Transaction trans = app.getCurrentTransaction();
                DbConnection connection = null;
                try {
                    connection = CreamToolkit.getTransactionalConnection();
                    trans.setEmployeeNumber(number);
                    if (Attendance1.isZReportEnd())
                        trans.setZSequenceNumber(new Integer(trans.getZSequenceNumber().intValue()+1));
                    trans.store(connection);
                    trans.clear();
                    connection.commit();
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                    CreamToolkit.haltSystemOnDatabaseFatalError(e);
                } finally {
                    CreamToolkit.releaseConnection(connection);
                    CreamToolkit.logMessage("start to putAttendance1 ....");
                    InlineCommandExecutor.getInstance().execute("putAttendance");
                }

				app.setChecked(false);
				// app.setSalemanChecked(false);

				if (!PARAM.isContinuousLogon()) {
					trans.setDealType2("0");
					cashier = null;
					return KeyLock1State.class;
				} else {
					trans.setDealType2(dealType2);
					app.getMessageIndicator().setMessage(
							res.getString("CashierNo") + number 
							        + getCashierName()
									+ res.getString("AttendanceMessage") + " "
									+ res.getString("PleaseEnterClear"));
					number = "";
                    passwordNumber = "";
                    passwordNumber1 = "";
                    isCheckCashierPsswd = false;
					cashier = null;
					return InOutNumberState.class;
				}
			}
		}
		return sinkState.getClass();
	}

	private String getCashierName() {
	    return cashier != null ? "(" + cashier.getCashierName() + ")" : "";
	}

	private boolean checkValidityOfCardID(String cardID) {

		boolean isAllowAll = !PARAM.isCheckIDLengthOnCheckInOut();
		// modify 2005-03-22 全家希望任何员工码都接受

		boolean onlyCheckIDLenOnCheckInOut = PARAM.isOnlyCheckIDLenOnCheckInOut();

		// modify 2004-03-29 灿坤希望只要是7位的员工码就接受

		boolean checkCashierDigit = PARAM.isCheckCashierDigit();
		// modify 2005-04-06 检查最后一位校验码

		if (isAllowAll) { // 不检查长度与位数
			if (cardID.length() == 0) {
				app.getWarningIndicator().setMessage(
						res.getString("CashierNumberWrong"));
				cardID = "";
				return false;
			}
		} else if (onlyCheckIDLenOnCheckInOut) { // 检查是否为7位
			int idLen = PARAM.getCheckInOutIDLens();
			if (cardID.length() != idLen) {
				app.getWarningIndicator().setMessage(
						res.getString("CashierNumberWrong"));
				cardID = "";
				return false;
			} else {
				if (checkCashierDigit) { // 检查最后一位校验码
					boolean checkDigit = Cashier.checkDigitCashierID(cardID);
					if (!checkDigit) {
						app.getWarningIndicator().setMessage(
								res.getString("DigitNumberError"));
						cardID = "";
						return false;
					}
				}
			}
		} else if (checkCashierDigit) { // 检查最后一位校验码
			boolean checkDigit = Cashier.checkDigitCashierID(cardID);
			if (!checkDigit) {
				app.getWarningIndicator().setMessage(
						res.getString("DigitNumberError"));
				cardID = "";
				return false;
			}
		} else {
			cashier = Cashier.queryByCashierID(cardID);
			if (cashier == null) {
				app.getWarningIndicator().setMessage(
						res.getString("CashierNumberWrong"));
				cardID = "";
				return false;
			}
		}
		return true;                              
	}

    private boolean checkCashierIDandPsswd(String cashierID, String passWord) {
        if(Cashier.CheckValidCashier(cashierID, passWord)==null){
            app.getWarningIndicator().setMessage(
						res.getString("WrongPassword1"));
            passWord = "";
            return false;
        }
        return true ;
    }

    //检测考勤人员是否已经上班或下班考勤过.
    public static String str = "";
    private boolean checkInOutOfCardID(String cardID, String dealType) {
        String attType ="";
        str = "";
        if("B".equals(dealType)){
            attType = "01";
            str = res.getString("notOut");
        }else if("C".equals(dealType)){
            attType = "02";
            str = res.getString("notIn");
        }else {
        }
        if(Attendance1.existInorOut(cardID,attType)){
            app.getWarningIndicator().setMessage(str);
            return true;
        }
        return false;
    }

}
