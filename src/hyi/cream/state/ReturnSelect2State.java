package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.SI;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.uibeans.ReturnReasonPopupMenu;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * 部份退貨輸入退貨項次和數量的state.
 */
public class ReturnSelect2State extends State {
    static ReturnSelect2State returnSelect2State = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private String numberString = "";
    private ArrayList selectedIndexes = new ArrayList();
    private ResourceBundle res = CreamToolkit.GetResource();
    private boolean isQuantity = false;
    private String selectID = "";
    private String quantity = "";

    // 第n个line item -> 因部份数量删除所生成的相应正项line item
    Map<Integer, LineItem> newlyAddedItems = new HashMap<Integer, LineItem>();

    public static ReturnSelect2State getInstance() {
        try {
            if (returnSelect2State == null) {
                returnSelect2State = new ReturnSelect2State();
            }
        } catch (InstantiationException ex) {
        }
        return returnSelect2State;
    }

    public ReturnSelect2State() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof NumberButton
            && sourceState instanceof ReturnOneState) {

            isQuantity = false;
            numberString = ((NumberButton)event.getSource()).getNumberLabel();
            selectID = "";
            quantity = "";

            app.getWarningIndicator().setMessage("");
            app.getItemList().setSelectionMode(1);
        } else if (event.getSource() instanceof NumberButton
            && sourceState instanceof ReturnSelect2State) {
            numberString = numberString + ((NumberButton)event.getSource()).getNumberLabel();
        }
        if (isQuantity)
            app.getMessageIndicator().setMessage(res.getString("InputReturnQty") + numberString);
        else
            app.getMessageIndicator().setMessage(/*res.getString("InputReturnSequence") +*/  numberString);
    }

    public Class exit(EventObject event, State sinkState) {
        Transaction currentTrans = app.getCurrentTransaction();
        app.getWarningIndicator().setMessage("");
        if (event.getSource() instanceof EnterButton) {
            if (CreamToolkit.checkInput(numberString, 0)) {
                if (!isQuantity) {
                    selectID = numberString;
                    numberString = "";
                    isQuantity = true;
                    return ReturnSelect2State.class;
                } else {
                    quantity = numberString;
                    numberString = "";
                    isQuantity = false;
                }
            } else if (!isQuantity) {
                numberString = "";
                app.getWarningIndicator().setMessage(res.getString("InputWrong"));
                return ReturnOneState.class;
            } else {
                isQuantity = false;
            }

            app.getMessageIndicator().setMessage("");
            int i = 0;
            for (LineItem li : currentTrans.lineItems()) {
                if (Integer.parseInt(selectID) == (i + 1)) {
                    if (!PARAM.isCanDaiShou2Return() && li.getDetailCode().equalsIgnoreCase("O")) {
                        //代收交易不允许退
                        selectID = "";
                        showWarningMessage("DontAllowDaiShou2Return");
                        return ReturnOneState.class;
                    } else if (li.getVoidSequence() >= 0) { // 如果這行是因部份數量退而產生的臨時item
                        selectID = "";
                        showWarningMessage("ReturnWarning5");
                        return ReturnOneState.class;
                    }
                    int qty = 0;
                    if (StringUtils.isEmpty(quantity))
                        qty = li.getQuantity().intValue();
                    else
                        qty = Integer.parseInt(quantity);
                    if (qty > li.getQuantity().intValue()) {
                        quantity = "";
                        isQuantity = true;
                        showWarningMessage("InputQuantityIsTooLarge");
                        return ReturnSelect2State.class;
                    }
                    if (selectedIndexes.contains(new Integer(i + 1))) { // 如果之前退過，不讓重覆退
                        selectID = "";
                        showWarningMessage("ReturnWarning5");
                        return ReturnOneState.class;
                    } else if (qty != 0) {
                        if (PARAM.getReturnReasonPopup()) {
                            app.getMessageIndicator().setMessage(res.getString("ReturnReason"));
                            new ReturnReasonPopupMenu(ReturnReasonPopupMenu.RETURN_ITEM, numberString).popupMenu();
                        }
                        try {
                            selectedIndexes.add(i + 1);
                            app.getItemList().setSelectedItem(i + 1);
                            li.setRemoved(true);
                            currentTrans.changeLineItem(li.getLineItemSequence() - 1, li);
                        } catch (LineItemNotFoundException e) {
                            e.printStackTrace(); // impossible
                        }
                        if (qty != 0) {
                            this.createLineItemLeft(currentTrans, li, qty);
                        }
                        break;
                    }
                }
                i++;
            }
            selectID = "";
            return ReturnOneState.class;
        }

        if (event.getSource() instanceof ClearButton) {
            if (numberString.equals("")) {
                return ReturnOneState.class;
            } else {
                numberString = "";
                selectID = "";
                quantity = "";
                app.getMessageIndicator().setMessage("");
                return ReturnOneState.class;
            }
        }

        return sinkState.getClass();
    }

    /**
     * 計算退款金額。
     *
     * 例如：退一個B,
     *   A x 1 = 25
     *   B x 2 = 50 Removed
     *   C x 3 = 60
     *   B x 1 = 25 Newly-Added
     * 則退款金額為 -50 + 25 = -25
     */
    public HYIDouble calcRefundAmount() {
        HYIDouble refundAmount = new HYIDouble(0);
        for (LineItem lineItem : app.getCurrentTransaction().lineItems()) {
            if (newlyAddedItems.containsKey(lineItem.getLineItemSequence()))
                refundAmount.addMe(lineItem.getAfterDiscountAmount());
            else if (lineItem.getRemoved())
                refundAmount.subtractMe(lineItem.getAfterDiscountAmount());
        }
        return refundAmount;
    }

    public ArrayList getSelectedIndexes() {
        return selectedIndexes;
    }

    private void createLineItemLeft(Transaction currentTrans, LineItem refundItem, int refundQty) {
        boolean isNew;
        LineItem newItem;

        int remainQty = refundItem.getQuantity().intValue() - refundQty;
        if (remainQty == 0)
            return;
        if (newlyAddedItems.containsKey(refundItem.getLineItemSequence())) {
            isNew = false;
            newItem = newlyAddedItems.get(refundItem.getLineItemSequence());
        } else {
            isNew = true;
            newItem = (LineItem)refundItem.clone();
            newItem.setLineItemSequence(currentTrans.getLineItems().length + 1);
            newItem.setVoidSequence(refundItem.getLineItemSequence());
        }
        newItem.setDetailCode("S");
        newItem.setRemoved(false);
        newItem.setQuantity(new HYIDouble(remainQty));
        newItem.setAmount(newItem.getUnitPrice().multiply(newItem.getQuantity()));

        int scale = 2;
        if (!StringUtils.isEmpty(refundItem.getDiscountNumber())) {
            String[] discNos = refundItem.getDiscountNumber().split(",");
            // 取第一個applied SI的計算小數位數做為計算分攤afterDiscountAmount的小數位數
            if (!StringUtils.isEmpty(discNos[0]) && discNos[0].startsWith("S")) {
                SI si = currentTrans.getAppliedSIById(discNos[0].substring(1));
                scale = si.getScale();
            }
        }

        newItem.setAfterDiscountAmount(refundItem.getAfterDiscountAmount().multiply(
            newItem.getQuantity()).divide(refundItem.getQuantity(), scale, BigDecimal.ROUND_DOWN)); // ROUND_DOWN對客戶有利
        if (refundItem.getAfterSIAmount() != null)
            newItem.setAfterSIAmount(refundItem.getAfterSIAmount().multiply(
                newItem.getQuantity()).divide(refundItem.getQuantity(), scale, BigDecimal.ROUND_DOWN));
        newItem.caculateAndSetTaxAmount();

        newlyAddedItems.put(newItem.getLineItemSequence(), newItem);
        try {
            if (isNew)
                currentTrans.addLineItem(newItem, false);
            else
                currentTrans.changeLineItem(newItem.getLineItemSequence() - 1, newItem);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTmpLineItemMap() {
        this.newlyAddedItems.clear();
    }
}