package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.ZReport;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;

import java.sql.SQLException;
import java.util.EventObject;
import java.util.ResourceBundle;

public class CancelZState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
	private static CancelZState zState = null;
	private ResourceBundle res = CreamToolkit.GetResource();

	public static CancelZState getInstance() {
        try {
            if (zState == null) {
				zState = new CancelZState();
            }
        } catch (InstantiationException ex) {
        }
        return zState;
    }

    /**
     * Constructor
     */
	public CancelZState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
		app.getMessageIndicator().setMessage(res.getString("ConfirmZCancel"));
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof EnterButton) {
            DbConnection connection = null;
            try {
    			app.getWarningIndicator().setMessage(res.getString("zEnd") + ", " + res.getString("NoDataWriteToDisc"));

                connection = CreamToolkit.getTransactionalConnection();
    			ZReport z = ZReport.getOrCreateCurrentZReport(connection);
    			if (PARAM.isZPrint())
					CreamPrinter.getInstance().printZReport(z, this);
                
                connection.commit();
                return ConfirmZState.getSourceState();

            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                CreamToolkit.haltSystemOnDatabaseFatalError(e);
                return ConfirmZState.getSourceState();
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
       } else 
		  return sinkState.getClass();		
	}
}