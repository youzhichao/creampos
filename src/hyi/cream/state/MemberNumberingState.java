
// Copyright (c) 2000 HYI
package hyi.cream.state;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class MemberNumberingState extends SomeAGNumberingState {
    static MemberNumberingState memberNumberingState = null;

    public static MemberNumberingState getInstance() {
        try {
            if (memberNumberingState == null) {
                memberNumberingState = new MemberNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return memberNumberingState;
    }

    /**
     * Constructor
     */
    public MemberNumberingState() throws InstantiationException {
    }
}

 