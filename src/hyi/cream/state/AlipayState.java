package hyi.cream.state;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.alipay.AlipayHttpPostRequest;
import hyi.cream.alipay.AlipaySubmit;
import hyi.cream.dac.Alipay_detail;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.spos.JposException;
import hyi.spos.Scanner;
import hyi.spos.events.DataEvent;

import java.math.BigDecimal;
import java.util.Date;
import java.util.EventObject;
import java.util.Iterator;
import java.util.ResourceBundle;

//支付宝扫描条码
public class AlipayState extends State {
	private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private ResourceBundle res            = CreamToolkit.GetResource();

    private String numberStr = "";
    private String payID = "";
    private Indicator warningIndicator = app.getWarningIndicator();
    private Indicator messgeIndicator = app.getMessageIndicator();
    static AlipayState alipayState = null;
    public static int seq = 0;
    public static boolean isClearButton = false;
    private String payAmount = "";
    public static AlipayState getInstance() {
        try {
            if (alipayState == null) {
                alipayState = new AlipayState();
            }
        } catch (InstantiationException ex) {
        }
        return alipayState;
    }

    public AlipayState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        Object eventSource = event.getSource();
        if (sourceState instanceof Numbering2State) {
            isClearButton = false;
            app.setAlipay(false);
        	numberStr = "";
            payID = ((AlipayButton)eventSource).getPaymentID();
        	warningIndicator.setMessage("");
            messgeIndicator.setMessage(res.getString("PleaseInputAlipayNumber"));
        }
        if (sourceState instanceof SummaryState) {
            isClearButton = false;
            app.setAlipay(false);
        	numberStr = "";
            payID = ((AlipayButton)eventSource).getPaymentID();
        	warningIndicator.setMessage("");
            messgeIndicator.setMessage(res.getString("PleaseInputAlipayNumber"));
        } else if (sourceState instanceof AlipayState) {
            if (eventSource instanceof NumberButton) {
                numberStr = numberStr + ((NumberButton) eventSource).getNumberLabel();
                warningIndicator.setMessage("");
                messgeIndicator.setMessage(numberStr);
            } else if (eventSource instanceof ClearButton) {
                numberStr = "";
                warningIndicator.setMessage("");
                messgeIndicator.setMessage(res.getString("PleaseInputAlipayNumber"));
            } else if (eventSource instanceof EnterButton || eventSource instanceof Scanner) {
                numberStr = "";
                messgeIndicator.setMessage(res.getString("PleaseInputAlipayNumber"));
            } 
        }
	}

	public Class exit(EventObject event, State sinkState) {
		Object eventSource = event.getSource();
        if (eventSource instanceof EnterButton || eventSource instanceof Scanner) {
        	if (eventSource instanceof EnterButton) {
        		if (numberStr.equals("")) {
        			warningIndicator.setMessage("");
        			return this.getClass();
        		}
        	}
        	if (eventSource instanceof Scanner) {
        		try {
                    DataEvent dataEvent = (DataEvent)event;
                    numberStr = new String(((Scanner)eventSource).getScanData(dataEvent.seq));
                } catch (JposException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("Jpos exception at " + this);
                }
        	}
            Transaction curTransaction =  app.getCurrentTransaction();
            if (AlipayHttpPostRequest.getPropery() == null) {
                app.getWarningIndicator().setMessage(res.getString("AlipayProperyFail"));
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome2.getInstance().buttonPressed(e);
                return this.getClass();
            }
            if (app.getTrainingMode()) {
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome2.getInstance().buttonPressed(e);
                return Paying1State.class;
            }
            payAmount = curTransaction.getBalance().toString();
            Alipay_detail detail = processAlipay(curTransaction,payAmount) ;
            if (detail == null) {
                CreamToolkit.logMessage("AlipayProcessState clearButton SummaryState");
                return this.getClass();
            }
            return Paying1State.class;

        } else if (eventSource instanceof ClearButton) {
            if (numberStr.equals("")) {
                isClearButton = false;
            	warningIndicator.setMessage("");
                return SummaryState.class;
            } else {
                return this.getClass();
            }
            
        } else if (eventSource instanceof NumberButton) {
            return this.getClass();
        }
		return null;
	}

    public Alipay_detail processAlipay(Transaction trans, String inputPayingAmount) {
        CreamToolkit.logMessage("check transaction for alipay ...");

        Object[] lineItemArray =  trans.getLineItems();
        String  body = "";
        String goods_detail = "";
        int qty = 0;
        HYIDouble amt = new HYIDouble(0);
        int j = 0;
        boolean ok = false;
        for (Object aLineItemArray : lineItemArray) {
            LineItem l = (LineItem)aLineItemArray;
            if (!l.getRemoved()) {
                if (body.length() > 130) {
                    if (!ok) {
                        body = body.substring(1) + "等等...";
                        ok = true;
                    }
                } else {
                    body += "," + l.getDescription();
                }

                String detail = ",{\"goodsId\":\"" + l.getPluNumber() + "\",\"goodsName\":\"" + l.getDescription() +
                        "\",\"price\":\"" + l.getUnitPrice().setScale(2, 4) +
                        "\",\"quantity\":\"" + l.getQuantity().intValue() + "\"}";
                if (j < 5)
                    goods_detail += detail;
                else {
                    qty += l.getQuantity().intValue();
                    amt = amt.add(l.getUnitPrice());
                }
                j++;
            }
        }
        if (qty > 0) {
            goods_detail += ",{\"goodsId\":\"others\",\"goodsName\":\"其它商品\",\"price\":\""+amt+"\",\"quantity\":\""+qty+"\"}";
        }
        goods_detail = "["+goods_detail.substring(1)+"]";
        if (body.length() <= 130)
            body = body.substring(1);

        Iterator it =  trans.getPayments();
        int count = 0;
        while(it.hasNext()) {
            it.next();
            count++;
        }
        if (count > 0)
            body = "部分金额支付，商品："+body;
        else
            body = "全额支付，商品：" + body;
        app.getMessageIndicator().setMessage(res.getString("AlipayGetCreateAndPay"));
        seq++;
        String seqStr = "";
        if (seq < 10)
            seqStr = "0"+seq;
        String out_trade_no = trans.getStoreNumber()+trans.getTerminalNumber()+trans.getTransactionNumber()+seqStr;
        String extend_params = "{\"STORE_ID\":\""+trans.getStoreNumber()+"\",\"TERMINAL_ID\":\""+trans.getTerminalNumber()+"\"}";
        String dynamic_id = getNumberStr();
        payID = getPayID();
//        if (!SystemInfo.isWanConnected()) {
//            CreamToolkit.logMessage("WanOfflineCanNotTrade... ");
//            app.getWarningIndicator().setMessage(res.getString("WanOfflineCanNotTrade"));
//            return null;
//        }
        AlipaySubmit.ALIPAY_GATEWAY_NEW = GetProperty.getAlipayURL();
        String xml = AlipayHttpPostRequest.getCreateAndPay(out_trade_no, inputPayingAmount, dynamic_id, body, "", goods_detail, extend_params); //支付接口
//         String xml = "<?xml version=\"1.0\" encoding=\"GBK\"?>\n" +
//                "<alipay><is_success>T</is_success><request><param name=\"body\">全额支付，商品：翻盖中华</param><param name=\"dynamic_id_type\">barcode</param><param name=\"subject\">C-Store 测试店 消费</param><param name=\"sign_type\">MD5</param><param name=\"out_trade_no\">111114118601</param><param name=\"sign\">cbabcff7ca8aef95cd59fc23d815891d</param><param name=\"_input_charset\">GBK</param><param name=\"it_b_pay\">5m</param><param name=\"dynamic_id\">283738132191375149</param><param name=\"product_code\">BARCODE_PAY_OFFLINE</param><param name=\"total_fee\">2250.00</param><param name=\"service\">alipay.acquire.createandpay</param><param name=\"format_type\">xml</param><param name=\"partner\">2088211336339115</param></request><response><alipay><buyer_logon_id>blu***@hotmail.com</buyer_logon_id><buyer_user_id>2088302231723674</buyer_user_id><out_trade_no>111114118601</out_trade_no><result_code>ORDER_SUCCESS_PAY_INPROCESS</result_code><trade_no>2014011411001004670015087595</trade_no></alipay></response><sign>aa671f0a8d7dfc0eac5402fef4002085</sign><sign_type>MD5</sign_type></alipay>";
        if (xml == null || xml.equals("")) {
            return query(trans, inputPayingAmount, seqStr, out_trade_no);
        }
        String isSuccess  = getValueFromXml(xml,"<is_success>","</is_success>");
        if (isSuccess.equals("T")) {
            String resultCode = getValueFromXml(xml,"<result_code>","</result_code>");
            if (resultCode.equals("ORDER_FAIL")) {
                String error = getValueFromXml(xml,"<detail_error_code>","</detail_error_code>");
                if (error.equals("TRADE_HAS_SUCCESS")) {//交易已经支付
                    return query(trans, inputPayingAmount, seqStr, out_trade_no);
                } else {
                    String errorMessage = getValueFromXml(xml, "<detail_error_des>", "</detail_error_des>");
                    if (errorMessage.equals("")) {
                        app.getWarningIndicator().setMessage(AlipayHttpPostRequest.getErrorMessage(error));
                    } else {
                        app.getWarningIndicator().setMessage(errorMessage);
                    }
                    return null;
                }
            } else if (resultCode.equals("ORDER_SUCCESS_PAY_SUCCESS")) {
                String tradeNo = getValueFromXml(xml, "<trade_no>", "</trade_no>");
                String buyerLogonId = getValueFromXml(xml,"<buyer_logon_id>","</buyer_logon_id>");
                return addAlipayDetail(inputPayingAmount,tradeNo,trans,seqStr, buyerLogonId);
            } else if (resultCode.equals("ORDER_SUCCESS_PAY_INPROCESS")) { //交易创建，等待买付款
                String tradeNo = getValueFromXml(xml, "<trade_no>", "</trade_no>");
                //异步请求
                if (processNotiryRequert(out_trade_no)) {
                    app.getWarningIndicator().setMessage(res.getString("AlipayGetCreateAndPayFail"));
                    return null;
                } else {
                    String buyerLogonId = getValueFromXml(xml,"<buyer_logon_id>","</buyer_logon_id>");
                    return addAlipayDetail(inputPayingAmount,tradeNo,trans,seqStr, buyerLogonId);
                }

            } else if (resultCode.equals("ORDER_SUCCESS_PAY_FAIL")) {
                processCancel(out_trade_no);
                String errorMessage  = getValueFromXml(xml,"<detail_error_des>","</detail_error_des>");
                if (errorMessage.equals("")) {
                    String error = getValueFromXml(xml,"<detail_error_code>","</detail_error_code>");
                    app.getWarningIndicator().setMessage(AlipayHttpPostRequest.getErrorMessage(error));
                } else {
                    app.getWarningIndicator().setMessage(errorMessage);
                }
                return null;
            } else {
                String errorMessage  = getValueFromXml(xml,"<detail_error_des>","</detail_error_des>");
                if (errorMessage.equals("")) {
                    String error = getValueFromXml(xml,"<detail_error_code>","</detail_error_code>");
                    app.getWarningIndicator().setMessage(AlipayHttpPostRequest.getErrorMessage(error));
                } else {
                    app.getWarningIndicator().setMessage(errorMessage);
                }
                return null;
            }
        } else {
            String error = getValueFromXml(xml,"<error>","</error>");
            if (error.toUpperCase().equals("SYSTEM_ERROR"))  {
                String xmls;
                app.getMessageIndicator().setMessage(res.getString("AlipayGetQuery"));
                AlipaySubmit.ALIPAY_GATEWAY_NEW = GetProperty.getAlipayURL();
                xmls = AlipayHttpPostRequest.getQuery(out_trade_no,"");//查询
                if (xmls == null || xmls.equals("")) {

                    app.getWarningIndicator().setMessage(AlipayHttpPostRequest.getErrorMessage(error));
                    processCancel(out_trade_no);
                    return null;
                }
                String isSuccess2  = getValueFromXml(xml,"<is_success>","</is_success>");
                if (isSuccess2.equals("T")) {
                    String resultCode = getValueFromXml(xml,"<result_code>","</result_code>");
                    if (resultCode.equals("SUCCESS")) {
                        String tradeStatus = getValueFromXml(xml,"<trade_status>","</trade_status>");
                        if (tradeStatus.equals("TRADE_SUCCESS"))  {
                            String tradeNo = getValueFromXml(xml, "<trade_no>", "</trade_no>");
                            String buyerLogonId = getValueFromXml(xml,"<buyer_logon_id>","</buyer_logon_id>");
                            return addAlipayDetail(inputPayingAmount,tradeNo,trans,seqStr, buyerLogonId);
                        } else if (tradeStatus.equals("WAIT_BUYER_PAY")) {
                            String tradeNo = getValueFromXml(xml, "<trade_no>", "</trade_no>");
                            //异步请求
                            if (processNotiryRequert(out_trade_no)) {
                                app.getWarningIndicator().setMessage(res.getString("AlipayGetCreateAndPayFail"));
                                return null;
                            } else {
                                String buyerLogonId = getValueFromXml(xml,"<buyer_logon_id>","</buyer_logon_id>");
                                return addAlipayDetail(inputPayingAmount,tradeNo,trans,seqStr, buyerLogonId);
                            }
                        }
                    }
                }
            }
            app.getWarningIndicator().setMessage(AlipayHttpPostRequest.getErrorMessage(error));
            processCancel(out_trade_no);
        }
        return null;
    }

    private Alipay_detail query(Transaction trans, String inputPayingAmount, String seqStr, String out_trade_no) {
        String xml;
        xml = processQuery(out_trade_no);//查询是否成功
        if (xml == null) {
            //如果为空，刚先撤消再支付
            processCancel(out_trade_no);
            app.getWarningIndicator().setMessage(res.getString("AlipayGetCreateAndPayFail"));
            return null;
        } else {
            //查询成功，如支付成功则保存数据，交易结束
            //如果失败，则先撤消再支付
            String isSuccess2  = getValueFromXml(xml,"<is_success>","</is_success>");
            if (isSuccess2.equals("T")) {
                String resultCode = getValueFromXml(xml,"<result_code>","</result_code>");
                if (resultCode.equals("SUCCESS")) {
                    String tradeStatus = getValueFromXml(xml,"<trade_status>","</trade_status>");
                    if (tradeStatus.equals("TRADE_SUCCESS"))  {
                        String tradeNo = getValueFromXml(xml, "<trade_no>", "</trade_no>");
                        String buyerLogonId = getValueFromXml(xml,"<buyer_logon_id>","</buyer_logon_id>");
                        return addAlipayDetail(inputPayingAmount,tradeNo,trans,seqStr, buyerLogonId);
                    } else if (tradeStatus.equals("WAIT_BUYER_PAY")) {
                        String tradeNo = getValueFromXml(xml, "<trade_no>", "</trade_no>");
                        //异步请求
                        if (processNotiryRequert(out_trade_no)) {
                            app.getWarningIndicator().setMessage(res.getString("AlipayGetCreateAndPayFail"));
                            return null;
                        } else {
                            String buyerLogonId = getValueFromXml(xml,"<buyer_logon_id>","</buyer_logon_id>");
                            return addAlipayDetail(inputPayingAmount,tradeNo,trans,seqStr, buyerLogonId);
                        }
                    }
                }
            }
            processCancel(out_trade_no);

            app.getWarningIndicator().setMessage(res.getString("AlipayGetCreateAndPayFail"));
            return null;
        }
    }

    public boolean processNotiryRequert(String out_trade_no) {
        String xml;
        String resultCode;
        app.setAlipay(true);
        app.getWarningIndicator().setMessage(res.getString("AlipayWaitBuyerPay"));
        for (int i = 0 ; i < 60*5; i++) {//请求五分钟
            try {
                Thread.sleep(1000);
                if (isClearButton) {//如果按了清除键，才停止请求
                    processCancel(out_trade_no);
                    return true;
                }
                app.getMessageIndicator().setMessage(res.getString("RunTime") + (300-i));
                if ((i+1)%5==0) {//5秒请求一次
                    CreamToolkit.logMessage("isClearButton = " + isClearButton);

                    AlipaySubmit.ALIPAY_GATEWAY_NEW = GetProperty.getAlipayURL();
                    xml = AlipayHttpPostRequest.getQuery(out_trade_no,"");//查询
//                    xml = "<is_success>T</is_success><result_code>SUCCESS</result_code>";
                    if (xml == null || xml.equals("")) {
                        CreamToolkit.logMessage("WanOfflineCanNotTrade... ");
                        app.getWarningIndicator().setMessage(res.getString("WanOfflineCanNotTrade"));
                        continue;
                    }
                    String isSuccess  = getValueFromXml(xml,"<is_success>","</is_success>");
                    if (isSuccess.equals("T")) {
                        resultCode = getValueFromXml(xml,"<result_code>","</result_code>");
                        if (resultCode.equals("SUCCESS")) {
                            String tradeStatus = getValueFromXml(xml,"<trade_status>","</trade_status>");
                            if (tradeStatus.equals("TRADE_SUCCESS"))  {
                                return false;
                            }
                        } else {
                            String error = getValueFromXml(xml,"<detail_error_code>","</detail_error_code>");
                            app.getWarningIndicator().setMessage(AlipayHttpPostRequest.getErrorMessage(error) );
                        }
                    } else {
                        String error = getValueFromXml(xml,"<error>","</error>");
                        app.getWarningIndicator().setMessage(AlipayHttpPostRequest.getErrorMessage(error));
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
        processCancel(out_trade_no);
        return true;
    }

    //支付宝查询操作
    public String  processQuery(String out_trade_no) {
        String xml;
        app.getMessageIndicator().setMessage(res.getString("AlipayGetQuery"));
        AlipaySubmit.ALIPAY_GATEWAY_NEW = GetProperty.getAlipayURL();
        xml = AlipayHttpPostRequest.getQuery(out_trade_no,"");//查询
        if (xml == null || xml.equals("")) {
            return null;
        }
        String isSuccess  = getValueFromXml(xml,"<is_success>","</is_success>");
        if (isSuccess.equals("T")) {
            String resultCode = getValueFromXml(xml,"<result_code>","</result_code>");
            if (resultCode.equals("SUCCESS")) {
                String tradeStatus = getValueFromXml(xml,"<trade_status>","</trade_status>");
                if (tradeStatus.equals("TRADE_SUCCESS"))  {
                    return xml;
                }
            }
        }
        return xml;
    }

    //支付宝撤销操作
    public void processCancel(String out_trade_no) {
        String xml;
        String isSuccess;
        String resultCode;
        AlipaySubmit.ALIPAY_GATEWAY_NEW = GetProperty.getAlipayURL();
        xml = AlipayHttpPostRequest.getCancel(out_trade_no,"");//撤销
        if (xml == null || xml.equals("")) {
            CreamToolkit.logMessage("WanOfflineCanNotTrade... ");
            app.getWarningIndicator().setMessage(res.getString("WanOfflineCanNotTrade"));
            return ;
        }
        isSuccess  = getValueFromXml(xml,"<is_success>","</is_success>");
        if (isSuccess.equals("T")) {
            resultCode = getValueFromXml(xml,"<result_code>","</result_code>");
            if (resultCode.equals("SUCCESS")) {
                String trade_no = getValueFromXml(xml,"<trade_no>","</trade_no>");
                if (trade_no.equals(""))
                    CreamToolkit.logMessage(res.getString("AlipayCancelFail"));
                else
                    CreamToolkit.logMessage(
                            res.getString("AlipayCancelSuccess"));
            } else {
                String error = getValueFromXml(xml,"<detail_error_code>","</detail_error_code>");
                CreamToolkit.logMessage(AlipayHttpPostRequest.getErrorMessage(error));
            }
        } else {
            String error = getValueFromXml(xml,"<detail_error_code>","</detail_error_code>");
            CreamToolkit.logMessage(AlipayHttpPostRequest.getErrorMessage(error));
        }
    }

    public Alipay_detail addAlipayDetail(String inputPayingAmount,String tradeNo,Transaction trans,String seq, String buyerLogonId) {
        Alipay_detail alipay_detail;
        try{
            alipay_detail = new Alipay_detail();
            alipay_detail.setSTORENUMBER(trans.getStoreNumber());
            alipay_detail.setPOSNUMBER(trans.getTerminalNumber());
            alipay_detail.setTRANSACTIONNUMBER(trans.getTransactionNumber());
            alipay_detail.setZSeq(trans.getZSequenceNumber());
            alipay_detail.setTOTAL_FEE(new HYIDouble(inputPayingAmount));
            alipay_detail.setTRADE_NO(tradeNo);
            alipay_detail.setSEQ(seq);
            alipay_detail.setBUYER_LOGON_ID(buyerLogonId);
            alipay_detail.setSYSTEMDATE(new Date());
            trans.addAlipayList(alipay_detail);
            CreamToolkit.logMessage("transactionNumber：" + alipay_detail.getTRANSACTIONNUMBER() + "，tradeNo："
                    + alipay_detail.getTRADE_NO() + "，logon_id："+alipay_detail.getBUYER_LOGON_ID()+"，systemDate："
                    + alipay_detail.getSYSTEMDATE().toString()+ " is add trans.addAlipayDetailList");
            AlipayState.seq = 0;
            return alipay_detail;
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
            CreamToolkit.logMessage("Alipay process Fail");
        }
        return null;
    }

    public String getValueFromXml(String xml, String token1, String token2) {
        String result = "";
        int a = xml.indexOf(token1);
        int b = xml.indexOf(token2);

        if (a >= 0 && b >= 0) {
            result = xml.substring(a+token1.length(), b);
        }
        return result;
    }

    public String getNumberStr() {
        return numberStr;
    }
    public String getPayID() {
        return payID;
    }
    public String getPayAmount() {
        return payAmount;
    }
}
