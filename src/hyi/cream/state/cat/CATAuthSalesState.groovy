package hyi.cream.state.cat

import hyi.cream.POSPeripheralHome3
import hyi.cream.dac.SI
import hyi.cream.dac.Store
import hyi.cream.dac.Transaction
import hyi.cream.dac.ZReport
import hyi.cream.groovydac.CardDetail
import hyi.cream.groovydac.CardDetailChinaTrust
import hyi.cream.state.*
import hyi.cream.util.HYIDouble
import hyi.spos.CAT
import hyi.spos.CATConst
import hyi.spos.services.ChinaTrustCAT
import hyi.cream.util.DbConnection;
import java.text.SimpleDateFormat
import hyi.cream.dac.CreditType
import hyi.cream.CreamSession

/**
 * CAT authorization type.
 */
enum CATAuthType { SALES, REFUND, PARTIAL_REFUND, VOID };

/**
 * CATAuthSalesState by Groovy.
 *
 * @author Bruce You
 * @since 2008/8/30 下午 11:13:14
 */
class CATAuthSalesState extends State {

    /** Singleton instance. */
    static CATAuthSalesState instance = new CATAuthSalesState()

    /** Auth type. */
    CATAuthType authType

    private exitState

    /** Pending, ready for inserting record. */
    private CardDetail pendingCardDetail
    private CardDetailChinaTrust pendingCardDetailChinaTrust

    public void entry(EventObject event, State sourceState) {
        showMessage('PleaseSwipeCardAtExternalCAT')

        CAT cat = POSPeripheralHome3.getInstance().getCAT();
        assert cat != null
        cat.clearOutput()

        def creditCardAmount
        Transaction trans = getCurrentTransaction() // 如果顯示的是退貨交易，在支付的部份已經準備好退的信用卡金額

        switch (authType) {
        case CATAuthType.SALES:     // (CAT:PY2CA)
            creditCardAmount = trans.getCreditCardAmount()
            if (creditCardAmount.compareTo(new HYIDouble(0)) >= 0)
                cat.authorizeSales(0, creditCardAmount, 0L, -1)
            else
                cat.authorizeRefund(0, creditCardAmount, 0L, -1)
            break;

        case CATAuthType.VOID:      // (VoidLast:VR2CA)
            creditCardAmount = trans.getCreditCardAmount().negate()
            if (cat instanceof ChinaTrustCAT) {
                // 中國信託CAT的取消交易要給原交易的host ID (和invoice No)
                cat.cardCompanyID = trans.cardDetail.addendum.hostId
            }
            cat.authorizeVoid(trans.cardDetail.addendum.invoiceNo.toInteger(), creditCardAmount, 0L, -1)
            break;

        case CATAuthType.REFUND:    // (Refund1:RE2CA)
            creditCardAmount = trans.getCreditCardAmount()
            if (cat instanceof ChinaTrustCAT &&
                (trans.hasCreditCardBonusDiscount() || trans.isCreditCardInstallment())) {

                // 中國信託CAT的紅利抵用退貨或分期付款退貨時，要給原交易的Terminal ID和Ref No
                def origTrans = CreamSession.instance.getTransactionToBeRefunded();
                cat.refNo = origTrans.cardDetail.addendum.refNo
                cat.terminalId = origTrans.cardDetail.terminalId

                // 送給CAT機的退貨金額，必須是原交易總額，也就是實付信用卡金額加上紅利金額
                creditCardAmount.subtractMe(trans.getCreditCardBonusDiscount().abs())
            }
            cat.authorizeRefund(0, creditCardAmount, 0L, -1)
            break;

        case CATAuthType.PARTIAL_REFUND:    // (Refund2:RE2CA)
            creditCardAmount = trans.getCreditCardAmount()
            cat.authorizeRefund(0, creditCardAmount, 0L, -1)
            break;
        }

        if (cat instanceof ChinaTrustCAT) {
            if (cat.centerResultCode == '0000') { // success
                createCreditCardInfo(cat, trans, creditCardAmount)

                exitState =
                    (authType == CATAuthType.REFUND || authType == CATAuthType.PARTIAL_REFUND) ?
                        ReturnSummaryState.instance.saveRefundData() : // (Refund1:CA2DO)
                    (authType == CATAuthType.VOID) ?
                        TransactionVoidState.instance.voidLastTransaction() : // (VoidLast:CA2IN)
                    DrawerOpenState.class // (CAT:CA2DO)

            } else // failed
                exitState = CATAuthSalesFailedState.class // (CAT:CA2CA),(Refund1:CA2CA),(VoidLast:CA2CA)

        } else {
            // other brand of CAT will be processed here...
            exitState = CATAuthSalesFailedState.class
        }
    }

    def isNormalSales() {
        return authType == CATAuthType.SALES;
    }

    def createCreditCardInfo(CAT cat, Transaction trans, HYIDouble creditCardAmount) {

        if (cat instanceof ChinaTrustCAT
            && cat.paymentCondition == CATConst.CAT_PAYMENT_BONUS_COMBINATION_1
            && isNormalSales()) {

            // cat.bonusDiscount 为红利折抵金额（正常销售为负数）

            if (PARAM.isCreditCardBonusRegardedAsPayment()) {
                trans.generateCreditCardBonusPayment(cat.bonusDiscount)
            } else {
                SI bonusSi = SI.queryBonusDiscount()
                SIDiscountState.instance.processDiscount(bonusSi, cat.bonusDiscount, trans, false, false)
                trans.creditCardSubstractBonusAmount(cat.bonusDiscount)
                trans.initForAccum()
                trans.accumulate()
            }
        }

        println(">>>>>>> CAT.hostid=${cat.hostId}")

        if (cat instanceof ChinaTrustCAT
            && cat.hostId == "03" // 信用卡分期付款
            && isNormalSales()) {
            trans.setCreditCardInstallment true
        }
        
        // tranhead --

        if (authType == CATAuthType.SALES || authType == CATAuthType.REFUND || authType == CATAuthType.PARTIAL_REFUND) {
            trans.setCreditCardNumber cat.accountNumber

            // e.g., cat.expireDate = '0509'
            // then set expire date to 2009-05-31
            if (cat.expireDate?.length() >= 4) {
                try {
                    def expireDate = [
                        cat.expireDate[2..3].toInteger() + 100, // year + 1900
                        cat.expireDate[0..1].toInteger(),       // 0-based month
                        1] as Date                              // 1-based day of month
                    trans.setCreditCardExpireDate expireDate - 1
                } catch (Exception e) {
                    // ignore
                }
            }

            CreditType creditCardType = CreditType.checkCreditNo(cat.accountNumber)
            if (creditCardType != null)
                trans.setCreditCardType(creditCardType.getNoType())

            // 如果是信用卡分期付款交易，使用記錄交易類型
            trans.setState2("I")
        }

        // carddetail --

        String expirationDate = '0000'
        try {
            expirationDate = (authType == CATAuthType.VOID) ?
                new SimpleDateFormat("MMyy").format(trans.getCreditCardExpireDate()) :
                cat.expireDate
        } catch (Exception e) {
            if (authType == CATAuthType.VOID)
                println 'Cannot get expiration date on void: trans.getCreditCardExpireDate()=' + trans.getCreditCardExpireDate();
            else
                println 'Cannot get expiration date: cat.expireDate=' + cat.expireDate 
        }

        def transAmt = creditCardAmount
        if (authType == CATAuthType.REFUND &&
            (trans.hasCreditCardBonusDiscount() || trans.isCreditCardInstallment()))
            transAmt = CreamSession.instance.transactionToBeRefunded.cardDetail.transAmt.negate()

        pendingCardDetail = new CardDetail(
            accountDate: [1970 - 1900, 0, 1] as Date,   // 營業日期
            posNumber: getPOSNumber(),                  // POS機號
            storeId: Store.getStoreID(),                // 門市代號

            // 交易序號: 退貨/取消 -> 即將要生成的退貨/取消交易序號, 正常 -> 本筆交易序號
            transactionNumber: isNormalSales() ? trans.getTransactionNumber() : Transaction.getNextTransactionNumber(),

            zSequenceNumber: ZReport.getCurrentZNumber(),   // Z帳序號
            isVoid: '0',                                    // 是否被作廢（取消/退貨/調整）（'0':沒有被作廢、'1':被作廢）

            // 若此筆為被作廢交易，記錄作廢此筆交易的相應取消/退貨/調整的交易序號；
            // 若此筆為作廢交易（取消/退貨/調整），則記錄被作廢的交易序號
            voidTransactionNumber: isNormalSales() ? 0 :
                authType == CATAuthType.VOID ? trans.transactionNumber :
                CreamSession.instance.getTransactionToBeRefunded().transactionNumber,

            transDateTime: cat.transactionDateTime,     // 交易日期與時間

            // 卡號
            cardNo: (authType == CATAuthType.VOID) ? trans.getCreditCardNumber() : cat.accountNumber,

            // 有效期
            cardExpDate: expirationDate,                

            approvalCode: cat.approvalCode,             // 授權碼
            terminalId: cat.terminalId,                 // 端末機編號
            transAmt: transAmt,                         // 總交易金額, 退貨/取消是負數
            source: 'C',                                // 數據來源 ('C': CAT數據、'P': 收銀機輸入數據、'L': 缺乏明細數據、'S': SC補登數據)
            updateDate: cat.transactionDateTime)        // 最後修改日期
            //updateUserId: GetProperty.getCashierNumber(""))

        // carddetail_chinatrust --

        if (cat instanceof ChinaTrustCAT) {
            pendingCardDetailChinaTrust = new CardDetailChinaTrust(
                accountDate: [1970 - 1900, 0, 1] as Date, // 營業日期
                posNumber: getPOSNumber(),                // POS機號
                storeId: Store.getStoreID(),              // 門市代號
                zSequenceNumber: ZReport.getCurrentZNumber(),   // Z帳序號

                // 交易類別（'01':SALE銷售、'02':REFUND退貨、'03':OFFLINE離線、'04':PRE_AUTH預取授權、'30':VOID取消、
                // '40':TIP小費、'41':ADJUST調整、'50':SETTLEMENT調整、'98':ECHO測試）
                transType: cat.transactionTypeChars,

                respCode: cat.respCode,                   // 回覆代碼
                hostId: cat.hostId,                       // 授權銀行編碼
                invoiceNo: cat.transactionNumber,         // 調閱編號
                refNo: cat.refNo,                         // 序號
                bonusPaid: cat.bonusPaid,                 // 紅利抵用的實付金額
                bonusDiscount: cat.bonusDiscount,         // 紅利抵用的折抵金額
                installmentPeriod: cat.installmentPeriod, // 期數
                installmentFee: cat.installmentFee,       // 手續費
                installmentType: cat.installmentType,     // 收費方式（'1':一般分期、'2':收費分期）
                installmentPutOffType: cat.installmentPutOffType, // 延後付款方式（'1':無延後付款、'2':延後付款、'3':彈性付款）
                installmentAmount1: cat.installmentAmount1, // 首期金額（若為彈性付款，此欄位為後期的首期金額）
                installmentAmount2: cat.installmentAmount2, // 每期金額（若為彈性付款，此欄位為後期的每期金額）
                installmentAmount3: cat.installmentAmount3, // 彈性付款前期的每期金額
                rawData: cat.rawData)                     // CAT機返回的原始數據（目前中國信託的應該是144 bytes）

            pendingCardDetail.addendum = pendingCardDetailChinaTrust
        }
    }

    /**
     * Save pending card detail data. This will be called from Transaction.store().
     */
    def insertCreditCardInfo(DbConnection connection) {
        // insert carddetail
        def cardDetailId = pendingCardDetail?.insert(connection, true)

        // insert carddetail_chinatrust
        if (pendingCardDetailChinaTrust != null) {
            pendingCardDetailChinaTrust.carddetailId = cardDetailId
            pendingCardDetailChinaTrust.insert(connection)
        }

        // discard pending records
        discardPendingData()
    }

    def discardPendingData() {
        pendingCardDetail = pendingCardDetailChinaTrust = null
    }

    public Class exit(EventObject event, State sinkState) {
        return exitState;
    }
}