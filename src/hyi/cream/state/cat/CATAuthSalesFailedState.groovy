package hyi.cream.state.cat

import hyi.cream.POSButtonHome2
import hyi.cream.POSPeripheralHome3
import hyi.cream.dac.SI
import hyi.cream.dac.Store
import hyi.cream.dac.Transaction
import hyi.cream.dac.ZReport
import hyi.cream.event.POSButtonEvent
import hyi.cream.groovydac.CardDetail
import hyi.cream.groovydac.CardDetailChinaTrust
import hyi.cream.state.*
import hyi.cream.uibeans.*
import hyi.cream.util.HYIDouble
import hyi.spos.CAT
import hyi.cream.util.DbConnection;
import org.apache.commons.lang.StringUtils
import hyi.cream.dac.CreditType
import hyi.cream.CreamSession

/**
 * CATAuthSalesFailedState by Groovy.
 *
 * @author Bruce You
 * @since 2008/9/2 下午 12:23:36
 */
class CATAuthSalesFailedState extends State implements PopupMenuListener {

    /** Singleton instance. */
    static CATAuthSalesFailedState instance = new CATAuthSalesFailedState()

    private PopupMenuPane menu = getApp().getPopupMenuPane();
    private int selectItem
    //private refund

    /** Pending, ready for inserting record. */
    private CardDetail pendingCardDetail
    private CardDetailChinaTrust pendingCardDetailChinaTrust

    public void entry(EventObject event, State sourceState) {
        showMessage 'CATAuthSalesFailed'

        CAT cat = POSPeripheralHome3.getInstance().getCAT();
        assert cat != null

        // Get error message and show in popupmenu

        def normalSales = CATAuthSalesState.instance.authType == CATAuthType.SALES
        menu.setMenu([
            "回覆代碼: ${cat.getCenterResultCode()}",
            "授權結果: ${cat.getCenterResultCodeDescription1()}",
            "主機回應: ${cat.getCenterResultCodeDescription2()}",
            " ",
            '[1] 重新嘗試授權',
            '[2] 人工輸入授權資料',
            normalSales ? '[清除] 重新輸入支付方式' : '[清除] 返回',
            ' ',
            '請選擇[1]、[2]、或[清除]'])
        menu.setPopupMenuListener this
        menu.setVisible true
    }

    public Class exit(EventObject event, State sinkState)
    {
        def source = event.getSource()
        def authType = CATAuthSalesState.instance.authType

        Transaction trans = getCurrentTransaction()
            //(authType == CATAuthType.REFUND) ? ReturnSummaryState.instance.getOldTran() : // 被退貨交易
            //(authType == CATAuthType.PARTIAL_REFUND) ? ReturnSummaryState.instance.getShowTran() : // 被退貨交易
            //getCurrentTransaction()     // 銷售：當前交易，取消：被取消的交易

        if (source instanceof SelectButton) {
            if (selectItem == 0) {          // 重新嘗試授權
                menu.setVisible false
                return CATAuthSalesState.class

            } else if (selectItem == 1) {   // 人工輸入授權資料
                menu.setVisible false
                def creditCardAmount = trans.getCreditCardAmount()
                createCreditCardInfo(trans, creditCardAmount, authType)
                def creditCardForm = CreditCardForm.instance
                creditCardForm.setBounds();

                // open editting form
                def okOrAbort = creditCardForm.modify(pendingCardDetail)
                if (okOrAbort) {            // 人工輸入信用卡明細資料完成
                    fillManualCreditCardInfoIntoTransaction(trans, authType == CATAuthType.SALES)

                    return (authType == CATAuthType.REFUND || authType == CATAuthType.PARTIAL_REFUND) ?
                            ReturnSummaryState.instance.saveRefundData() :        // (Refund1:CF2DO), (Refund2:CF2DO)
                        (authType == CATAuthType.VOID) ?
                            TransactionVoidState.instance.voidLastTransaction() : // (VoidLast:CF2IN)
                        DrawerOpenState.class // (CAT:CF2DO)

                } else {                    // 放棄返回
                    discardPendingData()
                    switch (authType) {
                    case CATAuthType.SALES:
                        trans.initPaymentInfoWithoutClearingDiscount();
                        return SummaryState.class  // (CAT:CF2SM)

                    case CATAuthType.REFUND:
                    case CATAuthType.PARTIAL_REFUND:
                        return ReturnSummaryState.instance.backToRefund()  // (Refund1:CA2RT),(Refund2:CA2RT)

                    case CATAuthType.VOID:
                        return VoidReadyState.class; // (VoidLast:CA2VR)
                    }
                }
            } else
                return this.class
        }
        // else is pressed [Clear] button
        menu.setVisible false

        // clear payment info
        if (authType == CATAuthType.SALES)
            trans.initPaymentInfoWithoutClearingDiscount();

        return (authType == CATAuthType.SALES) ? SummaryState.class :
               (authType == CATAuthType.VOID) ? VoidReadyState.class :
               ReturnSummaryState.instance.backToRefund()
    }

    public void menuItemSelected() {
        if (menu.getSelectedMode()) {
            selectItem = menu.getSelectedNumber();
            POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
            POSButtonHome2.getInstance().buttonPressed(e);
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome2.getInstance().buttonPressed(e);
        }
    }

    def createCreditCardInfo(Transaction trans, HYIDouble creditCardAmount, CATAuthType authType) {
        boolean normalSales = authType == CATAuthType.SALES

        // carddetail --

        pendingCardDetail = new CardDetail(
            accountDate: [1970 - 1900, 0, 1] as Date,   // 營業日期
            storeId: Store.getStoreID(),                // 門市代號
            posNumber: getPOSNumber(),                  // POS機號

            // 交易序號: 退貨/取消 -> 即將要生成的 退貨/取消 交易序號, 正常 -> 本筆交易序號
            transactionNumber: normalSales ? trans.getTransactionNumber() : Transaction.getNextTransactionNumber(),

            zSequenceNumber: ZReport.getCurrentZNumber(),   // Z帳序號
            isVoid: '0',                                    // 是否被作廢（取消/退貨/調整）（'0':沒有被作廢、'1':被作廢）

            // 若此筆為被作廢交易，記錄作廢此筆交易的相應取消/退貨/調整的交易序號；
            // 若此筆為作廢交易（取消/退貨/調整），則記錄被作廢的交易序號
            voidTransactionNumber: normalSales ? 0 : CreamSession.instance.getTransactionToBeRefunded().transactionNumber,

            // 總交易金額
            transAmt: creditCardAmount,     // normalSales? creditCardAmount : creditCardAmount.negate(),

            // 數據來源 ('C': CAT數據、'P': 收銀機輸入數據、'L': 缺乏明細數據、'S': SC補登數據)
            source: 'P')

        // carddetail_chinatrust --

        if (POSPeripheralHome3.instance.existsChinaTrustCAT()) {
            pendingCardDetailChinaTrust = new CardDetailChinaTrust(
                posNumber: getPOSNumber(),                // POS機號
                accountDate: [1970 - 1900, 0, 1] as Date, // 營業日期
                storeId: Store.getStoreID(),              // 門市代號
                zSequenceNumber: ZReport.getCurrentZNumber(),   // Z帳序號

                // 交易類別（'01':SALE銷售、'02':REFUND退貨、'03':OFFLINE離線、'04':PRE_AUTH預取授權、'30':VOID取消、
                // '40':TIP小費、'41':ADJUST調整、'50':SETTLEMENT調整、'98':ECHO測試）
                transType: (authType == CATAuthType.SALES) ? '01' :
                           (authType == CATAuthType.REFUND || authType == CATAuthType.PARTIAL_REFUND) ? '02' : '30'
            )

            pendingCardDetail.addendum = pendingCardDetailChinaTrust
        }

        // 退貨時，幫他把信用卡號填好
        if (!normalSales) {
            pendingCardDetail.cardNo = pendingCardDetail.cardExpDate = ''
            pendingCardDetail.cardNo = trans.cardDetail?.cardNo
            pendingCardDetail.cardExpDate = trans.cardDetail?.cardExpDate
            // 退貨而且原交易有紅利折抵時，幫他把紅利折抵金額填好
            if (trans.hasCreditCardBonusDiscount()) {
                def bonusAmount = trans.getCreditCardBonusDiscount()
                pendingCardDetail.transAmt = creditCardAmount.subtract(bonusAmount)  // 交易金額顯示負數, e.g., -100 - (-10) = -90
                pendingCardDetailChinaTrust.bonusDiscount = bonusAmount.negate()     // 紅利折抵金額, e.g., 10
                pendingCardDetailChinaTrust.bonusPaid = pendingCardDetail.transAmt   // 紅利折抵後實付金額, e.g., -90
            }
        }
    }

    private void fillManualCreditCardInfoIntoTransaction(Transaction trans, boolean normalSales) {
        if (pendingCardDetail == null)
            return

        trans.setCreditCardNumber pendingCardDetail.cardNo

        // e.g., cat.expireDate = '0509'
        // then set expire date to 2009-05-31
        if (pendingCardDetail.cardExpDate?.length() >= 4) {
            try {
                def expireDate = [
                    pendingCardDetail.cardExpDate[2..3].toInteger() + 100, // year + 1900
                    pendingCardDetail.cardExpDate[0..1].toInteger(),       // 0-based month
                    1] as Date                              // 1-based day of month
                trans.setCreditCardExpireDate expireDate - 1
            } catch (Exception e) {
                // ignore
            }
        }

        CreditType creditCardType = CreditType.checkCreditNo(pendingCardDetail.cardNo)
        if (creditCardType != null)
            trans.setCreditCardType(creditCardType.getNoType())

        def now = new Date()
        pendingCardDetail.transDateTime = now   // 交易日期與時間
        pendingCardDetail.updateDate = now      // 最後修改日期

        if (StringUtils.isEmpty(pendingCardDetail.cardNo) ||
            StringUtils.isEmpty(pendingCardDetail.cardExpDate))
            pendingCardDetail.source = 'L'  // Lack credit card info

        if (POSPeripheralHome3.instance.existsChinaTrustCAT() && normalSales &&
            pendingCardDetail.addendum.bonusDiscount != null &&
            pendingCardDetail.addendum.bonusDiscount.compareTo(new HYIDouble(0)) > 0) {

            // bonusDiscount 为红利折抵金额（正常销售为负数）
            def bonusDiscount = pendingCardDetail.addendum.bonusDiscount.negate(); // CreditCardForm返回的红利折抵金额为正数

            if (PARAM.isCreditCardBonusRegardedAsPayment()) {
                trans.generateCreditCardBonusPayment(bonusDiscount)
            } else {
                // 計算信用卡紅利折扣
                SI bonusSi = SI.queryBonusDiscount()
                SIDiscountState.instance.processDiscount(bonusSi, bonusDiscount, trans, false, false);
                trans.creditCardSubstractBonusAmount(bonusDiscount);
                trans.initForAccum();
                trans.accumulate();
            }
        }

        if (POSPeripheralHome3.instance.existsChinaTrustCAT() && normalSales &&
            pendingCardDetail.addendum.installmentType == '1') { // 信用卡分期付款
            trans.setCreditCardInstallment true
        }

    }

    def insertCreditCardInfo(DbConnection connection) {
        if (pendingCardDetail == null)
            return

        // insert carddetail
        def cardDetailId = pendingCardDetail.insert(connection, true)

        // insert carddetail_chinatrust
        if (pendingCardDetailChinaTrust != null) {
            pendingCardDetailChinaTrust.carddetailId = cardDetailId
            pendingCardDetailChinaTrust.insert(connection)
        }

        // discard pending records
        discardPendingData()
    }

    def discardPendingData() {
        pendingCardDetail = pendingCardDetailChinaTrust = null
    }
}