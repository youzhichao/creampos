// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.Payment;
import hyi.cream.dac.Transaction;
import hyi.cream.state.wholesale.WholesaleClearingState;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;


/**
 * A Class class.
 * <P>
 * @author dai
 */
public class Numbering2State extends State {

    private String printString          = "";
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static Numbering2State numbering2State = null;
	private String payMenuId = "";
	
    public static Numbering2State getInstance() {
        try {
            if (numbering2State == null) {
                numbering2State = new Numbering2State();
            }
        } catch (InstantiationException ex) {
        }
        return numbering2State;
    }

    /**
     * Constructor
     */
    public Numbering2State() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
    	app.getPopupMenuPane().setEnabled(true);
    	app.getPopupMenuPane().clear();
//        app.setEnabledPopupMenu(true);
    	app.setCrdPaymentMenuVisible(true);
        if (sourceState.getClass() == SummaryState.class
        		|| sourceState.getClass() == WholesaleClearingState.class
        ) {
            printString = "";
        }
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            printString = printString + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(printString);
            app.getWarningIndicator().setMessage("");
        }
        app.getWarningIndicator().setMessage("");
    }

    public Class exit(EventObject event, State sinkState) {
//        app.setEnabledPopupMenu(false);
    	app.setCrdPaymentMenuVisible(false);
        if (event.getSource() instanceof ClearButton) {
            printString = "";
            app.getMessageIndicator().setMessage(printString);
//            return SummaryState.class;
        }
        
        if (sinkState == null){
            Transaction tran = app.getCurrentTransaction();
            //HYIDouble totalAmount = tran.getSalesAmount();
            if (CreamToolkit.checkInput(printString, new HYIDouble(0))) {
                /*HYIDouble payCash = new HYIDouble(printString);
                payCash = payCash.setScale(2);
                if (payCash.compareTo(totalAmount) == -1) {
                    app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("CashNotEnough"));
                    return SummaryState.class;
                } else {
                    tran.setPayCash(payCash);
                    tran.setChangeAmount(payCash.subtract(totalAmount));
                    app.getPayingPane().repaint();*/
            	if (event.getSource() instanceof PaymentButton) {
            		payMenuId = ((PaymentButton) event.getSource()).getPaymentID();
            		System.out.println("----------------- paymentButton " + payMenuId);
            		Payment payment = Payment.queryByPaymentID(payMenuId);
            		if (payment != null &&  payment.isRecordingCardNumber())
            			return ReadCrdNoState.class; // 读卡号
            		else
            			return Paying1State.class;
            	} else if (event.getSource() instanceof PaymentMenu2Button) {
            		payMenuId = ((PaymentMenu2Button) event.getSource()).getPaymentID();
            		return ReadCrdNoState.class;
            	} else if (event.getSource() instanceof AlipayButton) {
                    if (app.getReturnItemState() || app.getIsAllReturn()) {
                        setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseAlipay"));
                        return Warning2State.class;
                    }
                    if (CreamSession.getInstance().getWorkingState().equals(WorkingStateEnum.PERIODICAL_DRAW_STATE)) {
                        setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseAlipay2"));
                        return Warning2State.class;
                    }
                    payMenuId = ((AlipayButton)event.getSource()).getPaymentID();
                    if (tran.getAlipayList().size() > 0) {
                        setWarningMessage(CreamToolkit.GetResource().getString("AlipayOnlyUse"));
                        return Warning2State.class;
                    }
                    //2015-05-14 @pingping 修改payment表没有payID=16的支付时,支付宝支付成功，程序却不让过的问题。
                    Payment payment = Payment.queryByPaymentID(payMenuId);
                    if (payment == null) {
                        return Warning2State.class;
                    }
                    return AlipayState.class;
                } else if (event.getSource() instanceof WeiXinButton) {
                    if (app.getReturnItemState() || app.getIsAllReturn()) {
                        setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseWeiXin"));
                        return Warning2State.class;
                    }
                    if (CreamSession.getInstance().getWorkingState().equals(WorkingStateEnum.PERIODICAL_DRAW_STATE)) {
                        setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseWeiXin2"));
                        return Warning2State.class;
                    }
                    payMenuId = ((WeiXinButton)event.getSource()).getPaymentID();
                    if (tran.getWeiXinList().size() > 0) {
                        setWarningMessage(CreamToolkit.GetResource().getString("WeiXinOnlyUse"));
                        return Warning2State.class;
                    }
                    //2015-05-14 @pingping 修改payment表没有payID=20的支付时,微信支付成功，程序却不让过的问题。
                    Payment payment = Payment.queryByPaymentID(payMenuId);
                    if (payment == null) {
                        return Warning2State.class;
                    }
                    return WeiXinState.class;
                } else if (event.getSource() instanceof UnionPayButton) {
                    if (app.getReturnItemState() || app.getIsAllReturn()) {
                        setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseUnionPay"));
                        return Warning2State.class;
                    }
                    if (CreamSession.getInstance().getWorkingState().equals(WorkingStateEnum.PERIODICAL_DRAW_STATE)) {
                        setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseUnionPay2"));
                        return Warning2State.class;
                    }
                    payMenuId = ((UnionPayButton)event.getSource()).getPaymentID();
                    if (tran.getUnionPayList().size() > 0) {
                        setWarningMessage(CreamToolkit.GetResource().getString("UnionPayOnlyUse"));
                        return Warning2State.class;
                    }
                    //2015-05-14 @pingping 修改payment表没有payID=20的支付时,微信支付成功，程序却不让过的问题。
                    Payment payment = Payment.queryByPaymentID(payMenuId);
                    if (payment == null) {
                        return Warning2State.class;
                    }
                    return UnionPayState.class;
                } else if (event.getSource() instanceof CmpayButton) {
                    if (app.getReturnItemState() || app.getIsAllReturn()) {
                        setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseCmpay"));
                        return Warning2State.class;
                    }
                    if (CreamSession.getInstance().getWorkingState().equals(WorkingStateEnum.PERIODICAL_DRAW_STATE)) {
                        setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseCmpay2"));
                        return Warning2State.class;
                    }
                    payMenuId = ((CmpayButton)event.getSource()).getPaymentID();
                    if (tran.getCmpayList().size() > 0) {
                        setWarningMessage(CreamToolkit.GetResource().getString("CmpayOnlyUse"));
                        return Warning2State.class;
                    }
                    Payment payment = Payment.queryByPaymentID(payMenuId);
                    if (payment == null) {
                        return Warning2State.class;
                    }
                    return CmpayState.class;
                } else
                    return Paying1State.class;
                //}
            } else {
                printString = "";
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.WHOLESALECLEARING_STATE)
                	return WholesaleClearingState.class;
                return SummaryState.class;
            }
        }
        return sinkState.getClass();
    }

    public String getNumberString() {
        return printString;
    }
    
    public String getPayMenuId() {
    	return payMenuId;
    }
}

 
