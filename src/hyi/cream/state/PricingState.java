
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;
import hyi.spos.JposException;
import hyi.spos.events.DataEvent;

import java.util.*;
//import jpos.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PricingState extends State implements PopupMenuListener {

    private String pluNumber            = "";
    private String number               = "";
    private int numberType              = 0;
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    private PopupMenuPane p             = app.getPopupMenuPane();
    private ResourceBundle res          = CreamToolkit.GetResource();

    static PricingState pricingState    = null;

    public static PricingState getInstance() {
        try {
            if (pricingState == null) {
                pricingState = new PricingState();
            }
        } catch (InstantiationException ex) {
        }
        return pricingState;
    }

    /**
     * Constructor
     */
    public PricingState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("PricingState entry");
        Object eventSource = event.getSource();
        if (sourceState instanceof Numbering4State) {
            Numbering4State n = (Numbering4State)sourceState;
            number = n.getNumberString();
            numberType = n.getNumberType();
        } else if (sourceState instanceof PriceLookupState
                   && eventSource instanceof hyi.spos.Scanner) {
            try {
                //((hyi.spos.Scanner)eventSource).setDecodeData(true);
                number = new String(((hyi.spos.Scanner)eventSource).getScanData(((DataEvent)event).seq));
                String tmp = processScaleBarCode(number);
                if (tmp != null)
                	number = tmp;
                numberType = Numbering4State.PLUNO;
            } catch (JposException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Jpos exception at " + this);
            }
        } else if (sourceState instanceof PricingState
                   && eventSource instanceof hyi.spos.Scanner) {
            try {
                //((hyi.spos.Scanner)eventSource).setDecodeData(true);
                pluNumber = new String(((hyi.spos.Scanner)eventSource).getScanData(((DataEvent)event).seq));
                numberType = Numbering4State.PLUNO;
            } catch (JposException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Jpos exception at " + this);
            }
        }
        PLU plu = null;

        //Bruce/20030505
        // 不再分别是输入条码还是货号
        //if (numberType == Numbering4State.PLUNO) {
        //    plu = PLU.queryByPluNumber(number);
        //} else if (numberType == Numbering4State.BARCODE) {
        //    plu = PLU.queryBarCode(number);
        //} else if (numberType == Numbering4State.INSTORECODE) {
        //    plu = PLU.queryByInStoreCode(number);
        //}
        plu = PLU.queryBarCode(number);
        if (plu == null)
            plu = PLU.queryByInStoreCode(number);

        if (plu != null) {            
            ArrayList menu = new ArrayList();
            menu.add(res.getString("PricingPluNumber") + plu.getPluNumber());
            menu.add(res.getString("PricingItemNumber") + plu.getInStoreCode());
            menu.add(res.getString("PricingPluName") + plu.getScreenName());
            if (plu.isFieldExist("specification")) {
                String spec = plu.getSpecification();
                menu.add(res.getString("Specification") + (spec == null ? "" : spec));
            }
            menu.add(res.getString("PricingPluPrice") + plu.getUnitPrice());
            
            if (PARAM.isInquiryPirceShowAllSpecialPriceInfo()){
            	menu.add(res.getString("PricingPluSpecialPriceStartDate") + CreamCache.getInstance().getDateFormate().format(plu.getDiscountStartDate()));
            	menu.add(res.getString("PricingPluSpecialPriceEndDate") + CreamCache.getInstance().getDateFormate().format(plu.getDiscountEndDate()));
            	menu.add(res.getString("PricingPluSpecialPriceStartTime") + CreamCache.getInstance().getTimeFormate().format(plu.getDiscountStartTime()));
            	menu.add(res.getString("PricingPluSpecialPriceEndTime") + CreamCache.getInstance().getTimeFormate().format(plu.getDiscountEndTime()));
            	menu.add(res.getString("PricingPluSpecialPrice") + plu.getSpecialPrice());
            } else {
            	HYIDouble specialPrice = plu.getSpecialPriceIfWithinTimeLimit();
            	if (specialPrice != null)
            		menu.add(res.getString("PricingPluSpecialPrice") + specialPrice);
            }
            //menu.add(res.getString("PricingPluDiscountPrice1") + plu.getDiscountPrice1());
            //menu.add(res.getString("PricingPluDiscountPrice2") + plu.getDiscountPrice2());
            
            // 无会员价，就显示售价
            if (PARAM.isInquiryPriceNeedMemberInfo()) {
	            HYIDouble amt = plu.getMemberPrice();
	            menu.add(res.getString("PricingPluMemberPrice") + 
	            		(amt == null ? plu.getUnitPrice() : amt).toString());
	            TaxType taxType = TaxType.queryByTaxID(plu.getTaxType());
	            if (taxType != null)
	                menu.add(res.getString("PricingPluTaxType") + taxType.getTaxName());
	            else
	                menu.add(res.getString("PricingPluTaxType") + plu.getTaxType());
	            if ("06".equalsIgnoreCase(PARAM.getRebatePaymentID()))
	            {
		            HYIDouble rebateRate = Rebate.getRebateRate(plu.getItemNumber());
		            if (rebateRate != null)
		                menu.add(res.getString("ActualRebate") + rebateRate);
	            }
            }
            p.setMenu(menu);
            p.setVisible(true);
            p.setPopupMenuListener(this);
            p.setInputEnabled(false);
            app.getMessageIndicator().setMessage(res.getString("Warning"));
        } else {
            app.getWarningIndicator().setMessage(res.getString("PricingWarning"));
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PriceLookMessage"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("PricingState exit");
        app.getMessageIndicator().setMessage("");
        if (event.getSource() instanceof ClearButton) {
            p.setVisible(false);
            p.setInputEnabled(true);
            app.getWarningIndicator().setMessage("");
        }
        return sinkState.getClass();
    }

    public void menuItemSelected() {
        if (!p.getSelectedMode()) {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome2.getInstance().buttonPressed(e);
        }
    }

    public String processScaleBarCode(String number) {
    	String no = null;
	        //System.out.println("number : " + number);
		java.util.Map formatMap = app.getScaleBarCodeFormat();
		if (formatMap == null)
			return no;
		for (Iterator it = formatMap.keySet().iterator(); it.hasNext();) {
	        try {
	            int totalLength = Integer.parseInt((String) it.next());
	            java.util.List formatList =  
	            		(java.util.List) formatMap.get(String.valueOf(totalLength));
	            if (formatList == null || formatList.isEmpty())
	                continue;
	            if (number.length() != totalLength)
	                continue;
	            String pref = (String) formatList.get(0);
	            if (!number.startsWith(pref)) {
	                continue;
	            }
	
	            int iStart = ((Integer) formatList.get(1)).intValue();
	            int iEnd   = ((Integer) formatList.get(2)).intValue();
	            String itemNo = number.substring(iStart, iEnd);
	            no = itemNo;
	            break;
	            /*
	            sub(plu);
	            LineItem lineItem = curTransaction.getCurrentLineItem();
	            lineItem.setWeight(wCount);
	            lineItem.setAmount(pCount);
	            */
	        } catch (Exception e) {
	            //e.printStackTrace(CreamToolkit.getLogger());
	        }
		} 
        return no;
    }

}

