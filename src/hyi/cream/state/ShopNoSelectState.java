package hyi.cream.state;

import java.util.*;

import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

/**
 * Created by IntelliJ IDEA.
 * User: liugang
 * Date: Aug 12, 2009
 * Time: 4:42:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShopNoSelectState extends State {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static ShopNoSelectState shopNoSelectState = null;
    public String shopNoString = "";

    public static ShopNoSelectState getInstance() {
        try {
            if (shopNoSelectState == null) {
                shopNoSelectState = new ShopNoSelectState();
            }
        } catch (InstantiationException ex) {
        }
        return shopNoSelectState;
    }

    /**
     * Constructor
     */
    public ShopNoSelectState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        Object eventSource = event.getSource();
		if ((sourceState instanceof IdleState && eventSource instanceof ShopButton)
            || (sourceState instanceof ShopNoSelectState && eventSource instanceof ClearButton)) {
            shopNoString = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputShopNo"));
        }

        if (sourceState instanceof ShopCategorySelectState && eventSource instanceof ClearButton) {
            shopNoString = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputShopNo"));
        }

        if (eventSource instanceof NumberButton) {
			String s = ((NumberButton)eventSource).getNumberLabel();
            if (sourceState instanceof ShopNoSelectState) {
                shopNoString = shopNoString + s;
                app.getMessageIndicator().setMessage(shopNoString);
            } 
        }

	}

	public Class exit(EventObject event, State sinkState) {
        Object eventSource = event.getSource();
		if (sinkState instanceof IdleState && eventSource instanceof ClearButton) {
		    app.getWarningIndicator().setMessage("");
            shopNoString = "";
            return IdleState.class;
        }
        if (eventSource instanceof NumberButton) {
            return ShopNoSelectState.class;
        } else if (eventSource instanceof EnterButton) {
            if ("".equals(shopNoString))
                return ShopNoSelectState.class;
            else
                return ShopCategorySelectState.class;

        } else {
            return WarningState.class;
        }

	}
}


