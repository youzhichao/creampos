package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Transaction;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.util.NumberConfuser;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * A Class class.
 * <P>
 * 
 * @author
 */
public class ReturnNumberState extends State {
    static ReturnNumberState returnNumberState = null;

    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    //private Transaction toVoidTran = null;

    private static String numberString = "";

    private String transactionID = "";

    private ResourceBundle res = CreamToolkit.GetResource();

    public static ReturnNumberState getInstance() {
        try {
            if (returnNumberState == null) {
                returnNumberState = new ReturnNumberState();
            }
        } catch (InstantiationException ex) {
        }
        return returnNumberState;
    }

    /**
     * Constructor
     */
    public ReturnNumberState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        app.getItemList().setBackLabel(res.getString("BigReturnNumberLabel"));
        if (event.getSource() instanceof NumberButton
                && sourceState instanceof ReturnNumberState) {
            NumberButton pb = (NumberButton) event.getSource();
            numberString = numberString + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(numberString);
        } else {
            transactionID = "";
            numberString = "";
            Transaction trans = app.getCurrentTransaction();
            trans.setDealType1("0");
            trans.setDealType2("3");
            trans.setDealType3("4");
            app.getMessageIndicator().setMessage(res.getString("ReturnReady2"));
            // if (sourceState instanceof CashierRightsCheckState
            // || (event.getSource() instanceof SelectButton && sourceState
            // instanceof KeyLock1State)) {
            app.getItemList().setItemIndex(0);
            // }
        }
    }

//    private Date nDaysAgo(int n) {
//        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, -n);
//        c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c
//                .get(Calendar.DAY_OF_MONTH), 0, 0, 0);
//        return c.getTime();
//    }

    public Class exit(EventObject event, State sinkState) {
        DbConnection connection = null;
        Transaction toVoidTran = null;
        try {
            if (event.getSource() instanceof EnterButton) {
                transactionID = numberString;

                // 如果交易序号是经过打乱的，把它还原
                if (PARAM.isConfuseTranNumber())
                    transactionID = NumberConfuser.defuse(transactionID);

                // check toVoidTran
                connection = CreamToolkit.getPooledConnection();
                toVoidTran = Transaction.queryByTransactionNumber(connection, PARAM.getTerminalNumber(),
                        Integer.parseInt(transactionID));
                CreamSession.getInstance().setTransactionToBeRefunded(toVoidTran);
                if (toVoidTran == null
                        //
                        // dealtype1
                        // '0':一般交易
                        // '*':被作废交易
                        //
                        // dealtype2
                        // '0':销售交易
                        // '1':退现金交易
                        // '2':退换货交易
                        // '3':退货交易
                        // '4':PaidIn/PaidOut交易
                        // '5':
                        // '6':
                        // '7':
                        // '8':SIGN-ON
                        // '9':SIGN-OFF交班
                        // 'A':SOD
                        // 'B':上班作业
                        // 'C':下班作业
                        // 'D':EOD
                        // 'E':事后登录
                        // 'F':礼卷销售
                        // 'G':投库
                        // 'H':借零
                        // 'I':
                        //
                        // dealtype3
                        // '0':正常
                        // '1':作废，表示本笔为作废交易
                        // '2':重印作废，表示本笔为重印作废交易
                        // '3':交易取消作废
                        // '4':退货交易

                        // 2003-07-18 ZhaoHong
                        // 一般交易 && 销售交易 && 正常或退货交易
                        || !toVoidTran.getDealType1().equals("0")
                        || !toVoidTran.getDealType2().equals("0")
                        || !(toVoidTran.getDealType3().equals("0") || toVoidTran
                                .getDealType3().equals("4")) // check daishou
                                                                // and daifu
                        || (toVoidTran.getDaiFuAmount().compareTo(
                                new HYIDouble(0)) == -1) // Bruce/20030519/
//                        // 30天前的交易不允许退货
//                        || toVoidTran.getSystemDateTime().before(nDaysAgo(30)) // Bruce/20030519/
//                        // For灿坤：隔日的配达交易不允许做退货
//                        || (toVoidTran.getAnnotatedType() != null
//                                && toVoidTran.getAnnotatedType().equals("P") && toVoidTran
//                                .getSystemDateTime().before(nDaysAgo(0)))
                        /*
                         * ||
                         * (oldTran.getDaiShouAmount().compareTo(new
                         * HYIDouble(0)) ==
                         * 1)
                         */

                ) {
                    app.getWarningIndicator().setMessage(
                            res.getString("ReturnWarning"));
                    return ReturnNumberState.class;
                } else if (toVoidTran.getLineItems().length == 0) {
                    // 明细资料丢失
                    app.getWarningIndicator().setMessage(
                            res.getString("ReturnDetailLostWarning"));
                    return ReturnNumberState.class;
                } else {
                    if (toVoidTran.getState1().equals("J")) {
                        try {
                            Client c = Client.getInstance();
                            c.processCommand("queryBookReturn " + toVoidTran.getTransactionNumber());
                            String m = (String) c.getReturnObject2();
                            if (m == null) {
                                app.getWarningIndicator().setMessage(
                                        res.getString("NotReturn2"));
                                return ReturnNumberState.class;
                            }
                            if (m != null && m.equals("false")) {
                                app.getWarningIndicator().setMessage(
                                        res.getString("NotReturn"));
                                return ReturnNumberState.class;
                            }
                        } catch (ClientCommandException e) {
                            e.printStackTrace();
                        }
                    }
                    app.getWarningIndicator().setMessage("");
                    app.setNewCashierID("");
                    app.setReturnFromSC(false);
                    return ReturnShowState.class;
                }
            }

            if (event.getSource() instanceof ClearButton) {
                if (numberString.equals("")) {
                    app.getMessageIndicator().setMessage("");
                    app.getWarningIndicator().setMessage("");
                    app.getItemList().setBackLabel(null);
                    return CashierRightsCheckState.getSourceState();
                    // return KeyLock1State.class;
                } else {
                    numberString = "";
                    if (toVoidTran != null) {
                        toVoidTran = null;
                    }
                    app.getMessageIndicator().setMessage(
                            res.getString("ReturnReady2"));
                    return ReturnNumberState.class;
                }
            }

            if (event.getSource() instanceof NumberButton) {
                app.getWarningIndicator().setMessage("");
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }

        return sinkState.getClass();
    }

    public String getTransactionID() {
        return transactionID;
    }
}
