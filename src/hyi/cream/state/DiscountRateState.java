package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;
import java.math.*;

public class DiscountRateState extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private static DiscountRateState discountRateState = null;
	private String tempNumber = "";
	private LineItem curLineItem = null;
	private boolean useDiscountRate;
	private HYIDouble rate = null;

    public static DiscountRateState getInstance() {
		try {
			if (discountRateState == null) {
				discountRateState = new DiscountRateState();
			}
		} catch (InstantiationException ex) {
		}
		return discountRateState;
	}

	public DiscountRateState() throws InstantiationException {
		useDiscountRate = PARAM.isUseDiscountRate();
	}

	public void entry(EventObject event, State sourceState) {
	    Transaction trans = app.getCurrentTransaction();
		curLineItem = trans.getCurrentLineItem();
		if (sourceState instanceof CashierRightsCheckState)
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString(
							"PleaseInputDiscountRate"));
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof NumberButton) {
			NumberButton pb = (NumberButton) event.getSource();
			// System.out.println(" OverrideAmountState exit number" +
			// pb.getNumberLabel());
			if (pb.getNumberLabel().equals("-")
					|| (pb.getNumberLabel().equals(".") && tempNumber.length() == 0)) {
				// System.out.println(" OverrideAmountState exit number 1 "
				// +tempPrice);
			} else {
				tempNumber = tempNumber + pb.getNumberLabel();
				// System.out.println(" OverrideAmountState exit number 2"
				// +tempPrice);
			}
			app.getMessageIndicator().setMessage(tempNumber);
			return DiscountRateState.class;
		}

		if (event.getSource() instanceof ClearButton) {
			if (tempNumber.length() == 0)
				return IdleState.class;
			tempNumber = "";
			app.getMessageIndicator().setMessage(tempNumber);
			return DiscountRateState.class;
		}

		if (event.getSource() instanceof EnterButton) {

			if (tempNumber.trim().length() <= 0)
				tempNumber = "0";

			// Check number format
			try {
				HYIDouble d = new HYIDouble(tempNumber);
				// Bruce/20030826/
				// 检查是否为一个很大或很小的数
				if (d.compareTo(new HYIDouble(99.99)) > 0
						|| d.compareTo(new HYIDouble(0)) < 0)
					throw new NumberFormatException("");
			} catch (NumberFormatException e) {
				app.getMessageIndicator().setMessage(
						CreamToolkit.GetResource().getString(
								"PleaseInputDiscountRateAgain"));
				tempNumber = "";
				return DiscountRateState.class;
			}

			rate = new HYIDouble(tempNumber);
			boolean isWeight = false;
			if (curLineItem.getWeight() != null
					&& curLineItem.getWeight().compareTo(new HYIDouble(0)) != 0)
				isWeight = true;

			if (tempNumber.trim().length() > 0 && !isWeight) {
				curLineItem.setUnitPrice(curLineItem.getOriginalPrice()
						.subtract(
								curLineItem.getOriginalPrice().multiply(
										rate.divide(new HYIDouble(100.0), 2,
												BigDecimal.ROUND_HALF_UP))));
			} else if (tempNumber.trim().length() > 0 && isWeight) {
				curLineItem.setUnitPrice(curLineItem.getAmount().subtract(
						curLineItem.getAmount().multiply(
								rate.divide(new HYIDouble(100.0), 2,
										BigDecimal.ROUND_HALF_UP))));
			}
			// curLineItem.setUnitPrice(new HYIDouble(tempNumber));
			else
				curLineItem.setUnitPrice(new HYIDouble(0.00));

			// curLineItem.setQuantity(new HYIDouble(1.00));
			
			HYIDouble tmepDiscountAmount=curLineItem.getAmount().multiply(
					rate.divide(new HYIDouble(100.0), 2,
							BigDecimal.ROUND_HALF_UP)).setScale(1,BigDecimal.ROUND_HALF_DOWN);
			
			System.out.print("tmepDiscountAmount----------" +tmepDiscountAmount
					+ " ---- " + curLineItem.getAmount() +rate);
			
			//curLineItem.setDiscountAmount(tmepDiscountAmount);
			if (tempNumber.trim().length() > 0 && isWeight) {
				curLineItem.setAmount(curLineItem.getAmount().subtract(tmepDiscountAmount));
			} else {
				curLineItem.setAmount(curLineItem.getUnitPrice().multiply(
						curLineItem.getQuantity()));
			}
			
			curLineItem.setAfterDiscountAmount(curLineItem.getAmount());
			// curLineItem.setAddRebateAmount(curLineItem.getAfterDiscountAmount().multiply(curLineItem.getRebateRate()));
			curLineItem.setAfterSIAmount(curLineItem.getAmount());
			curLineItem.caculateAndSetTaxAmount();
			curLineItem.setDiscountType("D");

			// Bruce/2003-08-08
			if (useDiscountRate) {
				try {
					HYIDouble rate = curLineItem.getUnitPrice().divide(
							curLineItem.getAmount(), 2,
							BigDecimal.ROUND_HALF_UP);
					curLineItem.setDiscountRate(rate);
					DiscountType dis = DiscountType.queryByDiscountRate(rate);
					if (dis != null) {
						curLineItem.setDiscountRateID(dis.getID());
					}
				} catch (ArithmeticException e) {
					// 初始价格为0，不设置折扣信息 zhaohong 2003-10-30
				}
			}

			try {
				app.getCurrentTransaction().changeLineItem(-1, curLineItem);
			} catch (LineItemNotFoundException e) {
				CreamToolkit.logMessage(e.toString());
				CreamToolkit.logMessage("LineItem not found at " + this);
			}
			CreamToolkit.showText(app.getCurrentTransaction(), 0);
			tempNumber = "";
			System.out.println("DiscountAmountState | exit IdleState");
			return IdleState.class;
		}

		return DiscountRateState.class;
	}

}