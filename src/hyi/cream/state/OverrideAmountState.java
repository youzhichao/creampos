package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;
import java.math.*;

public class OverrideAmountState extends State {
	static final String LOWER_WHOLESALE_PRICE = "LOWER_WHOLESALE_PRICE";
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	public static String tempPrice = "";
	static OverrideAmountState overrideAmountState = null;
    private LineItem curLineItem = null;
//    private boolean computeDiscountRate;
	private boolean yellowState;
    
    public static OverrideAmountState getInstance() {
        try {
            if (overrideAmountState == null) {
                overrideAmountState = new OverrideAmountState();
            }
        } catch (InstantiationException ex) {
        }
        return overrideAmountState;
    }
	
	public OverrideAmountState() throws InstantiationException {
//        computeDiscountRate = GetProperty.getComputeDiscountRate("no")
//            .equalsIgnoreCase("yes");
    }
	
	public void entry(EventObject event, State sourceState) {
	    Transaction trans = app.getCurrentTransaction();
		curLineItem = trans.getCurrentLineItem();
        if (sourceState instanceof CashierRightsCheckState) {
        	tempPrice = "";
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PleaseInputPriceAndEnterOver"));
        }
		if (sourceState instanceof YellowAmountState) {
			tempPrice = "";
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PleaseInputOverridePriceAndEnterOver"));
			yellowState = true;
		}
        //if (sourceState instanceof Validate)
        //    app.getMessageIndicator().setMessage("请输入价格，按[回车]键结束");
	}
	
	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof NumberButton) {
			NumberButton pb = (NumberButton)event.getSource();
                //System.out.println(" OverrideAmountState exit number" + pb.getNumberLabel());
			if (pb.getNumberLabel().equals("-") || (pb.getNumberLabel().equals(".") && tempPrice.length() == 0)) {
                //System.out.println(" OverrideAmountState exit number 1 " +tempPrice);
			} else { 
				tempPrice = tempPrice + pb.getNumberLabel();
                //System.out.println(" OverrideAmountState exit number 2" +tempPrice);
			}
			app.getMessageIndicator().setMessage(tempPrice);
			return OverrideAmountState.class;
		}

        if (event.getSource() instanceof ClearButton) {
            if (tempPrice.length() == 0) {
				if (yellowState)
					return YellowAmountState.class;
                return IdleState.class;
            }
            tempPrice = "";
            app.getMessageIndicator().setMessage(tempPrice);
            return OverrideAmountState.class;
        }

		if (event.getSource() instanceof EnterButton) {
            //System.out.println(" OverrideAmountState exit enterbutton" +tempPrice);
            
//            if (app.getReturnSaleMode()) {
//                curLineItem.setUnitPrice(new HYIDouble(tempPrice));
//                curLineItem.setAmount(curLineItem.getUnitPrice().multiply(curLineItem.getQuantity()));
//                curLineItem.setAfterDiscountAmount(curLineItem.getAmount());
//                TaxType taxType = TaxType.queryByTaxID(curLineItem.getTaxType());
//                // acculate tax
//                int round = 0;
//                String taxRound = taxType.getRound();
//                if (taxRound.equalsIgnoreCase("R")) {
//                    round = HYIDouble.ROUND_HALF_UP;
//                } else if (taxRound.equalsIgnoreCase("C")) {
//                    round = HYIDouble.ROUND_UP;
//                } else if (taxRound.equalsIgnoreCase("D")) {
//                    round = HYIDouble.ROUND_DOWN;
//                }
//                String taxTp = taxType.getType();
//                HYIDouble taxPercent = taxType.getPercent();
//                HYIDouble taxAmount = new HYIDouble(0);
//                int taxDigit = taxType.getDecimalDigit().intValue();
//                if (taxTp.equalsIgnoreCase("1")) {
//                    HYIDouble unit = new HYIDouble(1);
//                    taxAmount = ((curLineItem.getAmount().multiply(taxPercent)).divide(
//                            unit.add(taxPercent),2,round));
//                } else if (taxTp.equalsIgnoreCase("2")) {
//                    HYIDouble unit = new HYIDouble(1);
//                    taxAmount = (curLineItem.getAmount().multiply(taxPercent)).setScale(taxDigit, round);
//                } else {
//                    HYIDouble tax = new HYIDouble(0.00);
//                    taxAmount = tax.setScale(taxDigit, round);
//                }
//                //- acculate tax
//                curLineItem.setAfterSIAmount(curLineItem.getAmount());
//                curLineItem.setTaxAmount(taxAmount);
//    
//                curLineItem.setDiscountType("O");
//                try {
//                    app.getCurrentTransaction().changeLineItem(-1, curLineItem);
//                } catch (LineItemNotFoundException e) {
//                    CreamToolkit.logMessage(e.toString());
//                    CreamToolkit.logMessage("LineItem not found at " + this);
//                }
//                CreamToolkit.showText(app.getCurrentTransaction(), 0);
//                tempPrice = "";
//                return IdleState.class;
//            }

            if (tempPrice.trim().length() <= 0)
                tempPrice = "0";

            // Check number format
            try {
                HYIDouble d = new HYIDouble(tempPrice);
                //Bruce/20030826/
                //检查是否为一个很大或很小的数
                if (d.compareTo(new HYIDouble(999999.99)) > 0
                    || d.compareTo(new HYIDouble(-999999.99)) < 0)
                    throw new NumberFormatException("");
            } catch (NumberFormatException e) {
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PleaseInputPriceAgain"));
                tempPrice = "";
                return OverrideAmountState.class;
            }

            /*
            //Bruce/20030324
            String overrideAmountRangeLimit =
                GetProperty.getProperty("OverrideAmountRangeLimit");
                */
            // 20040526 变价不能低于 plu.minPrice [for cankun]
	        if (GetProperty.getDownloadMinPrice("no").equalsIgnoreCase("yes")) {
				HYIDouble minPrice = PLU.queryByItemNumber(curLineItem.getItemNumber()).getMinPrice();
				HYIDouble specialPrice = PLU.queryByItemNumber(curLineItem.getItemNumber()).getSpecialPriceIfWithinTimeLimit();
	        	
	        	if (minPrice != null && specialPrice != null) {
					if (specialPrice.compareTo(minPrice) < 0) {
						minPrice = specialPrice;
					}
	        	} else if (minPrice == null) {
					minPrice = specialPrice;
	        	} else {
	        		//
	        	}
	        	
	        	if (minPrice != null && (new HYIDouble(tempPrice)).compareTo(minPrice) < 0) {
	        		//输入价格不能低于 xxx 请重新输入价格 
                    app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputPriceCannotLower")
                    		+ "[" + minPrice.setScale(2)  + "]" + 
                    		CreamToolkit.GetResource().getString("PleaseInputPriceAgain"));
                    					
                    tempPrice = "";
                    return OverrideAmountState.class;
	        	}
	        }
	        
	        	    
            
            // 20040412 override amount use cat.OverrideAmountLimit [for cankun]
            String overrideAmountRangeLimit = getOverrideAmountRangeLimit(curLineItem);
            if (!overrideAmountRangeLimit.trim().equals("")
            		&& !overrideAmountRangeLimit.trim().equals("1")) {
                HYIDouble pluRangeValue = curLineItem.getOriginalPrice().multiply(
                    new HYIDouble(overrideAmountRangeLimit));
                HYIDouble plusRangePrice =
                    curLineItem.getOriginalPrice().add(pluRangeValue);
                HYIDouble minusRangePrice =
                    curLineItem.getOriginalPrice().subtract(pluRangeValue);
                //System.out.println(
                //    "OverrideAmountState plusRangePrice = "
                //        + plusRangePrice
                //        + " minusRangePrice = "
                //        + minusRangePrice);
                if ((new HYIDouble(tempPrice)).compareTo(plusRangePrice) > 0
                    || (new HYIDouble(tempPrice)).compareTo(minusRangePrice) < 0) {
                    app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PleaseInputPrice")
                    		+ "[" + minusRangePrice.setScale(2)  + "--" 
							+ plusRangePrice.setScale(2) + "]" + "，"+ CreamToolkit.GetResource().getString("PressEnterOver"));
                    tempPrice = "";
                    return OverrideAmountState.class;
                }
            }
            
            //may be popup overrideamout reason pane
//            if (GetProperty.getOverrideAmountReasonPopup("no").equalsIgnoreCase("yes")) {
//            	(new OverridePopupMenu(curLineItem)).popupMenu();
//            }
            
	        // james/20071125 
	        if (PARAM.isControlPriceChangeLowest()){
				HYIDouble memberMinPrice = PLU.queryByItemNumber(curLineItem.getItemNumber()).getDiscountPrice1();
				HYIDouble wholeSaleMinPrice = PLU.queryByItemNumber(curLineItem.getItemNumber()).getDiscountPrice2();
				//TODO ? 负金额商品--> 用abs比较
				
//				if (wholeSaleMinPrice != null && (new HYIDouble(tempPrice).abs()).compareTo(wholeSaleMinPrice.abs()) < 0) {
//					//提示：输入价格不能低于 xxx 请重新输入价格 
//					app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputPriceCannotLower")
//							+ "[" + wholeSaleMinPrice.setScale(2)  + "]" + 
//							CreamToolkit.GetResource().getString("PleaseInputPriceAgain"));
//					tempPrice = "";
//					return OverrideAmountState.class;
//				}
            
				// 如果低于批发最低价格，提示重新输入价格
				if (wholeSaleMinPrice != null && (new HYIDouble(tempPrice).abs()).compareTo(wholeSaleMinPrice.abs()) < 0) {
	        		System.out.println("this.getClass().getSimpleName()>" + this.getClass().getSimpleName());
					// CashierRightsCheckState.setSourceState("IdleState");
					CashierRightsCheckState.setSourceState(OverrideAmountState.class.getSimpleName());
					//CashierRightsCheckState.setTargetState("OverrideAmountState");
					//CashierRightsCheckState.setTargetState(this.getClass().getSimpleName());
					CashierRightsCheckState.setTargetState(OverrideAmountState.LOWER_WHOLESALE_PRICE);
					return CashierRightsCheckState.class;
                }
            
	        	// 如果低于会员最低价，而且当前变价用户权限不足，就提示输入新的足够权限的帐号，并存储有权限的人的资料 
	        	if (memberMinPrice != null && (new HYIDouble(tempPrice).abs()).compareTo(memberMinPrice.abs()) < 0) {
	        		System.out.println("this.getClass().getSimpleName()>" + this.getClass().getSimpleName());
					// CashierRightsCheckState.setSourceState("IdleState");
					CashierRightsCheckState.setSourceState(OverrideAmountState.class.getSimpleName());
					//CashierRightsCheckState.setTargetState("OverrideAmountState");
					//CashierRightsCheckState.setTargetState(this.getClass().getSimpleName());
					CashierRightsCheckState.setTargetState(OverrideAmount2State.class.getSimpleName());
					return CashierRightsCheckState.class;
                }
            }
                
            return OverrideAmount2State.class;
		}
		
		return OverrideAmountState.class;
	}
	
	protected String getOverrideAmountRangeLimit(LineItem curLineItem) {
		String limit = null;
		try {
	        if (PARAM.isOverrideUseCat()) {
	        	String catNo = curLineItem.getCategoryNumber();
	        	String midCatNo = curLineItem.getMidCategoryNumber();
	        	String microCatNo = curLineItem.getMicroCategoryNumber();
	        	limit = getOverrideAmountRangeLimit(catNo, midCatNo, microCatNo);
	        }
		} catch (Exception e) {
			e.printStackTrace(CreamToolkit.getLogger());
		}
		if (limit == null || limit.trim().equals("") || limit.trim().equals("1"))
        	limit = PARAM.getOverrideAmountRangeLimit().toString();
		return limit;
	}

	protected String getOverrideAmountRangeLimit(String catNo, String midCatNo, String microCatNo) {
		String limit = null;
    	Category cat = Category.queryByCategoryNumber(catNo, midCatNo, microCatNo);
    	if (cat != null) {
    		limit = cat.getOverrideAmountLimit().toString();
    		//System.out.println("limit : " + limit);
    		if (limit != null && !limit.trim().equals("") && !limit.trim().equals("1")) {
    			return limit;
    		}
    	}
		if (!microCatNo.equals(""))
			microCatNo = "";
		else if (!midCatNo.equals(""))
			midCatNo = "";
		else if (midCatNo.equals(""))
			return null;
		limit = getOverrideAmountRangeLimit(catNo, midCatNo, microCatNo);
		return limit;
	}	
	
	public LineItem getCurLineItem() {
		return curLineItem;
	}
}