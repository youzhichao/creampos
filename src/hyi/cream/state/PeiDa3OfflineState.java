package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.CreamToolkit;
import hyi.spos.ToneIndicator;

import java.util.EventObject;

//import jpos.ToneIndicator;

public class PeiDa3OfflineState extends State {
	private static PeiDa3OfflineState peiDa3OfflineState;

	private String billNo;

	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private ToneIndicator tone = null;

	public static PeiDa3OfflineState getInstance() {
		try {
			if (peiDa3OfflineState == null) {
				peiDa3OfflineState = new PeiDa3OfflineState();
			}
		} catch (InstantiationException ex) {
		}
		return peiDa3OfflineState;
	}

	/**
	 * Constructor
	 */
	public PeiDa3OfflineState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
		if (sourceState instanceof PeiDa3IdleState)
			billNo = ((PeiDa3IdleState) sourceState).getBillNo();
		app.getMessageIndicator().setMessage(
				CreamToolkit.GetResource().getString("PeiDaOfflineConfirm"));
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof EnterButton) {
			app.getCurrentTransaction().setAnnotatedType("P");
			app.getCurrentTransaction().setAnnotatedId(PeiDa3IdleState.getBillNoPrefix() + billNo);
			return PeiDa3AmountState.class;
		} else if (event.getSource() instanceof ClearButton) {
			billNo = "";
			app.getCurrentTransaction().clearLineItem();
			app.getCurrentTransaction().clearDeliveryInfo();
		}
		return PeiDa3IdleState.class;
	}

}
