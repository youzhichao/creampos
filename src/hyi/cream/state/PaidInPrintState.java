package hyi.cream.state;

// for event processing

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;

import java.sql.SQLException;
import java.util.EventObject;

public class PaidInPrintState extends State {
	static PaidInPrintState paidInPrintState = null;

	public static PaidInPrintState getInstance() {
		try {
			if (paidInPrintState == null) {
				paidInPrintState = new PaidInPrintState();
			}
		} catch (InstantiationException ex) {
		}
		return paidInPrintState;
	}

	/**
	 * Constructor
	 */
	public PaidInPrintState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
		// System.out.println("PaidInPrintState entry!");

		POSTerminalApplication posTerminal = POSTerminalApplication
				.getInstance();
		if (!PARAM.isPrintPaidInOut() && posTerminal.getCurrentTransaction().getDealType2().equals("4")) {
			CreamToolkit.showText(posTerminal.getCurrentTransaction(), 0);
			return;
		}

		if (sourceState instanceof PaidInOpenPriceState) {
			if (!posTerminal.getTrainingMode()
					&& PARAM.getTranPrintType().equalsIgnoreCase("step")) {
				Transaction cTransaction = posTerminal.getCurrentTransaction();
				Object[] lineItemArrayLast = cTransaction
						.getLineItems();
				if (lineItemArrayLast.length > 1) {
                    DbConnection connection = null;
                    try {
                        connection = CreamToolkit.getTransactionalConnection();
						if (!CreamPrinter.getInstance().getHeaderPrinted()) {
							CreamPrinter.getInstance().printHeader(connection);
							CreamPrinter.getInstance().setHeaderPrinted(true);
						}
    					LineItem lineItem = (LineItem) lineItemArrayLast[lineItemArrayLast.length - 1];
    					if (lineItem.getDetailCode().equals("V"))
    						CreamPrinter.getInstance().printLineItem(connection, lineItem);
    					lineItem = (LineItem) lineItemArrayLast[lineItemArrayLast.length - 2];
    					CreamPrinter.getInstance().printLineItem(connection, lineItem);
                        connection.commit();
                    } catch (SQLException e) {
                        CreamToolkit.logMessage(e);
                        CreamToolkit.haltSystemOnDatabaseFatalError(e);
                    } finally {
                        CreamToolkit.releaseConnection(connection);
                    }
				}
				if (!cTransaction.getBuyerNumber().equals("")
						&& !posTerminal.getBuyerNumberPrinted()) {
					posTerminal.setBuyerNumberPrinted(true);
				}
			}
		}
	}

	public Class exit(EventObject event, State sinkState) {
		// System.out.println("PaidInPrintState exit!");

		if (sinkState != null)
			return sinkState.getClass();
		else
			return null;
	}
}
