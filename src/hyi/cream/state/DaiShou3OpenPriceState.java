package hyi.cream.state;

import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.spos.JposException;
import hyi.spos.ToneIndicator;

import java.math.BigDecimal;
import java.util.EventObject;

/**
 * @author ll 2004-11-8
 */
public class DaiShou3OpenPriceState extends State {

    private String openPrice            = "";
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
//	private Thread warningThread        = null;
	private ToneIndicator tone          = null;
    
    static DaiShou3OpenPriceState daiShou3OpenPriceState = null;

    public static DaiShou3OpenPriceState getInstance() {
        try {
            if (daiShou3OpenPriceState == null) {
                daiShou3OpenPriceState = new DaiShou3OpenPriceState();
            }
        } catch (InstantiationException ex) {
        }
        return daiShou3OpenPriceState;
    }

    /**
     * Constructor
     */
    public DaiShou3OpenPriceState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
		//System.out.println("DaiFuOpenPriceState entry!");

        Object eventSource = null;
        if (event != null) {
            eventSource = event.getSource();
        }

		if (sourceState instanceof DaiShou3IdleState) {
            openPrice = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
        }

        if (sourceState instanceof DaiShou3OpenPriceState
        		&& eventSource instanceof ClearButton) {
            try {
                if (tone != null) {
                    tone.clearOutput();
                    tone.release();
                }
            } catch (JposException je) { }
            openPrice = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
            app.getWarningIndicator().setMessage("");
        }

        if (sourceState instanceof DaiShou3OpenPriceState
            && eventSource instanceof NumberButton) {
            try {
                if (tone != null) {
                    tone.clearOutput();
                    tone.release();
                }
            } catch (JposException je) { }
            NumberButton pb = (NumberButton)event.getSource();
            openPrice = openPrice + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(openPrice);
            app.getWarningIndicator().setMessage("");
        }
    }


    public Class exit(EventObject event, State sinkState) {
        //System.out.println("DaiFuOpenPriceState exit");

        HYIDouble price = null;
        //app.getMessageIndicator().setMessage("");
        if (event.getSource() instanceof EnterButton) { 

            if (openPrice.equals("")) {
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                return DaiShou3OpenPriceState.class;
            }

            if (!CreamToolkit.checkInput(openPrice, new HYIDouble(0))) {
                openPrice = "";
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                return DaiShou3OpenPriceState.class;
            }

            price = new HYIDouble(openPrice);
            if (price.compareTo(PARAM.getMaxPrice()) == 1) {
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyPrice"));
                openPrice = "";
                return DaiShou3OpenPriceState.class;
            }

            Transaction curTran = app.getCurrentTransaction();
            LineItem lineItem = curTran.getCurrentLineItem();
            lineItem.setUnitPrice(price.setScale(2, BigDecimal.ROUND_HALF_UP));

			/*
			 * Meyer/2003-02-21/
			 */
			lineItem.setOriginalPrice(price.setScale(2, BigDecimal.ROUND_HALF_UP));
            lineItem.setAmount(price.setScale(2, BigDecimal.ROUND_HALF_UP));
            try {
                curTran.changeLineItem(1, lineItem);
            } catch (LineItemNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("LineItem not found at " + this);
			}
			try {
				if (tone != null) {
					tone.clearOutput();
					tone.release();
				}
			} catch (JposException je) { }
            app.getWarningIndicator().setMessage("");
            return DaiShou3IdleState.class;
//			return DaiFuPrintState.class;
        }

        if (!(event.getSource() instanceof NumberButton)
            && !(event.getSource() instanceof ClearButton)) {
            POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
            try {
                tone = posHome.getToneIndicator();
                if (!tone.getDeviceEnabled())
                    tone.setDeviceEnabled(true);
                if (!tone.getClaimed()) {
                     tone.claim(0);
                     tone.setAsyncMode(true);
                     tone.sound(99999, 500);
                }                    
            } catch (Exception ne) {
                CreamToolkit.logMessage(ne);
            }
            app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
			return DaiShou3OpenPriceState.class;
        }

        return sinkState.getClass();
	}
}
