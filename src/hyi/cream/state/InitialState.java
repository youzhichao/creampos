package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.ShiftReport;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.*;
import hyi.spos.services.GPrinterCashDrawer;
import org.apache.commons.lang.StringUtils;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.EventObject;
import java.util.ResourceBundle;

public class InitialState extends State {
    POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
    static InitialState initialState = null;
    private ResourceBundle res = CreamToolkit.GetResource();
    private boolean changeInvoice;

    public static InitialState getInstance() {
        try {
            if (initialState == null) {
                initialState = new InitialState();
            }
        } catch (InstantiationException ex) {
        }
        return initialState;
    }

    /**
     * Constructor
     */
    public InitialState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        System.gc();

        checkIfInvoicePrintingUnfinished();
        
        // 在每笔交易开始前初始化数据
        changeInvoice = false;
        posTerminal.setBuyerNumberPrinted(false);
        posTerminal.setTransactionEnd(true);
        CreamPrinter.getInstance().setHeaderPrinted(false);
        ReceiptGenerator receiptGenerator = ReceiptGenerator.getInstance();
        receiptGenerator.setLineItemsPrintedInThisPage(0);
        receiptGenerator.setPageNumber(0); // 發票列印張數

        if (event == null && sourceState == null) {
            // PayingPane payingPane = posTerminal.getPayingPane();
            // payingPane.setBounds(0, 0, 620, 342);
            // posTerminal.getButtonPanel().showLayer(2);
            return;
        }
        if (sourceState == null || sourceState instanceof DrawerOpenState) {
            posTerminal.setChecked(false);
            posTerminal.setSalemanChecked(false);
            CreamSession.getInstance().removeAll();
            CreamToolkit.logMessage("InitialState | entry | sourceState : " + sourceState);
        }

        if (PARAM.isCountInvoiceEnable()) {
            int invNo = -1; //查看StateToolkit.genNextInvoiceNumber()文档
            try {
                invNo = Integer.parseInt(PARAM.getInvoiceNumber());
                // invNo--;
            } catch (Exception e) {
            }
            if (invNo == -1) {
	            changeInvoice = true;
	            posTerminal.getWarningIndicator().setMessage(res.getString("LastInvoice"));
            } else {
	            // 只需判断最后3位，台湾发票号是0--249 每打250张
	            int countDown = 250 - (invNo % 250); // 还剩几张
	            System.out.println("--------- InitialState | pages : " + (countDown ) + " | "
	                + invNo);
	            if (countDown <= 5) {
	                Object[] args = { "" + (countDown) };
	                posTerminal.getWarningIndicator().setMessage(
	                    MessageFormat.format(res.getString("InvoiceNumberCountDown"), args));
	            }
            }
        }

        /*
         * if (sourceState instanceof DrawerOpenState || sourceState instanceof ShiftState ||
         * sourceState instanceof ZState || eventSource instanceof EnterButton) {
         * //posTerminal.getMessageIndicator().setMessage("");
         * //posTerminal.getWarningIndicator().setMessage("");
         * //posTerminal.showIndicator("message"); //posTerminal.showIndicator("warning");
         * //posTerminal.getButtonPanel().showLayer(2); }
         */
        if ((sourceState instanceof CancelState || sourceState instanceof TransactionHoldState)
        		&& event.getSource() instanceof EnterButton || sourceState instanceof IdleState) {
            // Clear currentTransaction.
            posTerminal.getCurrentTransaction().clear();
            // posTerminal.getCurrentTransaction().setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
            // posTerminal.getCurrentTransaction().setTransactionNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber")));
            posTerminal.getPayingPane().setVisible(false);
            posTerminal.getItemList().setItemIndex(0);
            posTerminal.getItemList().repaint();
            posTerminal.getItemList().setVisible(true);
        }

        if (sourceState instanceof KeyLock2State) {
            posTerminal.getItemList().setVisible(true);
            posTerminal.getPayingPane().setVisible(false);
            posTerminal.getItemList().setItemIndex(0);
            posTerminal.getItemList().repaint();
            posTerminal.getCurrentTransaction().clear();
            // posTerminal.getCurrentTransaction().setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
            // posTerminal.getCurrentTransaction().setTransactionNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber")));
        }

        if(event != null){
            if (event.getSource() instanceof GPrinterCashDrawer && sourceState instanceof DrawerOpenState) {
                posTerminal.getItemList().setVisible(true);
                posTerminal.getPayingPane().setVisible(false);
                posTerminal.getItemList().setItemIndex(0);
                posTerminal.getCurrentTransaction().clear();
                posTerminal.getItemList().repaint();
            }
        }
    }

    private void checkIfInvoicePrintingUnfinished() {
        if (PARAM.isInvoicePrintingUnfinished()) {
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getTransactionalConnection();
                CreamPrinter printer = CreamPrinter.getInstance();
                printer.printCancel(connection, "CANCELED!");
                connection.commit();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
        }
    }

    public Class exit(EventObject event, State sinkState) {
        DbConnection connection = null;
        try {
        	CreamSession.getInstance().setWorkingState(WorkingStateEnum.IDLE_STATE);
            connection = CreamToolkit.getPooledConnection();

            System.out.println("State< <- hyi.cream.state.InitialState exit...");
            // super.exit(event, sinkState);
            // If the CashierNumber in CreamProperties is an empty string,
            // return CashierNumberingState.class, otherwise return IdleState.class.
            Class exitState = null;

            //Bruce/20091230/ Make IdleState initialize the current Transaction, even it is
            // interrupted by the input of invoice number
            IdleState.getInstance().setStart(true);
            
            if (changeInvoice) {
            	posTerminal.getCurrentTransaction().clear();
                // posTerminal.getCurrentTransaction().setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
                // posTerminal.getCurrentTransaction().setTransactionNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber")));
                posTerminal.getPayingPane().setVisible(false);
                posTerminal.getItemList().setItemIndex(0);
                posTerminal.getItemList().repaint();
                posTerminal.getItemList().setVisible(true);
                exitState = InvoiceNumberReadingState.class;
            }
            // else if (posTerminal.getBeginState())
            // exitState = InitialState.class;
            else {
            	
            	ShiftReport shift = ShiftReport.getCurrentShift(connection);
            	if (shift != null) {
//            		CreamToolkit.logMessage("shift.getPay00Amount()  =" + shift.getPay00Amount());
//            		CreamToolkit.logMessage("shift.getPaidInAmount() =" + shift.getPaidInAmount());
//            		CreamToolkit.logMessage("shift.getPaidOutAmount()=" + shift.getPaidOutAmount());
//            		CreamToolkit.logMessage("shift.getCashInAmount() =" + shift.getCashInAmount());
//            		CreamToolkit.logMessage("shift.getCashOutAmount()=" + shift.getCashOutAmount());
            		CreamToolkit.logMessage("InitialState | shift.getTotalCashAmount()=" + shift.getTotalCashAmount());
            		POSTerminalApplication.getInstance().getSystemInfo().setShiftTotalCashAmount(shift.getTotalCashAmount());
            	}
                exitState = IdleState.class;
            }

            if (posTerminal.getScanSalemanNumber()) {
                if (posTerminal.getSalemanChecked()) {
                    posTerminal.setSalemanChecked(false);
                }
                if (!posTerminal.getBeginState()) {
                    exitState = SalemanState.class;
                }
            }

            if (event == null && sinkState == null) {
                if (StringUtils.isEmpty(PARAM.getCashierNumber())
                    || ShiftReport.getCurrentShift(connection) == null) {
                    if (!posTerminal.getTrainingMode()) {
                        posTerminal.getCurrentTransaction().setDealType2("8");
                        return CashierState.class;
                    }
                } else
                    return exitState;
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }

        if (sinkState != null)
            return sinkState.getClass();
        else
            return null;
    }
}