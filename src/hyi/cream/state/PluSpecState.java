
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.uibeans.*;

import java.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PluSpecState extends State {

    private String pluAttr              = "";
    private String numberString         = "1";
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static PluSpecState pluSpecState = null;

    public static PluSpecState getInstance() {
        try {
            if (pluSpecState == null) {
                pluSpecState = new PluSpecState();
            }
        } catch (InstantiationException ex) {
        }
        return pluSpecState;
    }

    /**
     * Constructor
     */
    public PluSpecState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        Object eventSource = event.getSource();
        pluAttr = ((PluAttrButton)eventSource).getPluAttr();
        if (sourceState instanceof NumberingState) {
            numberString = ((NumberingState)sourceState).getNumberString();
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (sinkState instanceof IdleState) {
            pluAttr = "";
        }
        return sinkState.getClass();
    }

    public String getPluAttr() {
        return pluAttr;
    }

    public String getNumberString() {
        return numberString;
    }

    public void setNumberString(String numberString) {
        this.numberString = numberString;
    }
}

  
