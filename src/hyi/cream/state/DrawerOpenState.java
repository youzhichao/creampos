package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.DaiShouDef;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.state.cat.CATAuthSalesState;
import hyi.cream.state.cat.CATAuthType;
import hyi.cream.state.wholesale.WholesaleEndState;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.util.ReceiptGenerator;
import hyi.spos.CashDrawer;
import hyi.spos.JposException;
import hyi.spos.events.StatusUpdateEvent;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.EventObject;
import java.util.ResourceBundle;

public class DrawerOpenState extends State {
    static DrawerOpenState instance = null;
    private boolean isReturnTran = false;
    private boolean isCashTran = false;
    private boolean isShowWarning = true;
    private Class exitState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();

    // Meyer/2003-02-26
    // Get MixAndMatchVersion
    private static final String MIX_AND_MATCH_VERSION = PARAM.getMixAndMatchVersion();

    public static DrawerOpenState getInstance() {
        try {
            if (instance == null) {
                instance = new DrawerOpenState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    /**
     * Constructor
     */
    public DrawerOpenState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        final POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
        isShowWarning = false;
        isReturnTran = false;
        isCashTran = false;
        Transaction tran = app.getCurrentTransaction();
        tran.calcInvoiceAmount();

        if (sourceState.getClass().getSimpleName().startsWith("CAT")) {
            CATAuthType authType = CATAuthSalesState.getInstance().getAuthType();
            isReturnTran = authType == CATAuthType.REFUND || authType == CATAuthType.PARTIAL_REFUND;
            // isReturnTran: false=(CAT:CA2DO), true=(Refund1:CA2DO)
            exitState = InitialState.class;

        } else if (sourceState instanceof Paying3State
            || sourceState instanceof DrawerOpenConfirmState
            || sourceState instanceof WholesaleEndState) {
            if (app.getReturnItemState() && CreamSession.getInstance().getWorkingState() !=
                WorkingStateEnum.WHOLESALE_STATE)
                exitState = CashierRightsCheckState.getSourceState();
            else
                exitState = InitialState.class;

        } else {
            exitState = KeyLock1State.class;
            if (sourceState instanceof ReturnSummaryState) {
                isReturnTran = true;
                exitState = CashierRightsCheckState.getSourceState();
            } else if (sourceState instanceof MixAndMatchState) {
                isReturnTran = true;
            } else if (sourceState instanceof CashInIdleState
                || sourceState instanceof CashOutIdleState) {
                isCashTran = true;
                exitState = CashierRightsCheckState.getSourceState();
            }
        }
        if (!isCashTran) {
            try {
                CreamToolkit.showText(tran, 1);
            } catch (Exception e) {
                CreamToolkit.showText(tran, 1);
            }
        }

        // Process training mode, open drawer
        if (app.getTrainingMode()) {
            openDrawerForTrainingMode(posHome);
            return;
        }

        // 最后一次性打印
        String state1 = tran.getState1();  
        boolean isPrintTranOnce =  ("P".equals(state1) || "D".equals(state1) 
                || "B".equals(state1) || "T".equals(state1) || "J".equals(state1)
                || "K".equals(state1) || "Q".equals(state1) || "L".equals(state1));
        // 除了P:打印开票单，都要开抽屉
        boolean isNeedOpenDrawer = !("P".equals(state1) || "D".equals(state1));
        boolean isCheckDrawerClose = PARAM.isCheckDrawerClose();

        // Open drawer
        if (isNeedOpenDrawer) {
            try {
                CashDrawer drawer = posHome.getCashDrawer();
                if (!posHome.getEventForwardEnabled())
                    posHome.setEventForwardEnabled(true);
                if (!drawer.getDeviceEnabled())
                    drawer.setDeviceEnabled(true);
                if (isCheckDrawerClose)
                    drawer.addStatusUpdateListener(posHome);
                drawer.openDrawer();
            } catch (Exception e) {
                CreamToolkit.logMessage(e);
            }
        }
        
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();

            // TODO comment what is doing now in the following 3 lines?
            ReturnSelect2State rs2s = StateMachine.getInstance().getStateInstance(ReturnSelect2State.class);
            if (rs2s != null)
                rs2s.clearTmpLineItemMap();
            
            // Print payment and update invoiceCount for this tran
            boolean downPaymentTrans = "W".equals(tran.getState1()) || "X".equals(tran.getState1());
            if (!isReturnTran && !downPaymentTrans) // Nitori着付金交易不印發票
                printRemainder(connection, tran, isPrintTranOnce);

            /*if (!isReturnTran && downPaymentTrans      // 付訂金、付尾款
                || tran.getDealType2().equals("G")     // 投庫
                || tran.getDealType2().equals("H")     // 借零
                || tran.getDealType2().equals("4")) {  // PaidIn/PaidOut
                refreshInvoiceCount(tran); // in order to make invoice number empty
                tran.update(connection);
            }*/                

            // 保存交易
            app.getMessageIndicator().setMessage(res.getString("TransactionIsStoring"));
            if (!app.getTrainingMode()) {
                refreshInvoiceCount(tran);
                storeTransaction(connection, tran);
                CreamToolkit.logMessage("DrawerOpenState | storeTransaction | success ...");
            }
            app.getMessageIndicator().setMessage("");

            Transaction newTran = null;
            if ("P".equals(tran.getDealType2()) && "B".equals(tran.getState1())) {
                // 对代配退货，要帮它添加一笔交易，以代收的方式记录，一起保存
                newTran = genDaishouTranForDaiPeiReturn(connection, tran);
                storeTransaction(connection, newTran);
            }
            
            // close drawer
            waitForCloseDrawer(posHome, (isNeedOpenDrawer && isCheckDrawerClose));

            connection.commit();
            // 保存交易完成
            tran.setStoreComplete(true);
            CreamToolkit.logMessage("DrawerOpenState | transaction committed");
            if (newTran != null)
                newTran.setStoreComplete(true);
            //Cargo.getInstance().arouseWaitUploadJob();
            //CreamToolkit.logMessage("DrawerOpenState | putNeedCardJob");

            // Prepare next new transaction beforehand
            app.createClonedBlankTransaction();

        } catch (EntityNotFoundException e) {
            dealWithSaveDataException(e);
        } catch (SQLException e) {
            dealWithSaveDataException(e);
        } catch (Exception e) {
            dealWithSaveDataException(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    private Transaction genDaishouTranForDaiPeiReturn(DbConnection connection, Transaction oldTran) {
        Transaction t = (Transaction)oldTran.deepClone();
        // - clear
        // - copy misc from oldTran
        // - add lineItem
        // - recalculate
        // - copy payment from oldTran
        t.init(connection);
        // 设为退货交易
        t.setDealType1("0");
        t.setDealType1("3");
        t.setDealType1("4");
        t.clearAppliedSIs();
        t.removeSILineItem();

        t.setAccountingDate(oldTran.getAccountingDate());
        t.setSystemDateTime(oldTran.getSystemDateTime());
        t.setBuyerNumber(oldTran.getBuyerNumber());
        t.setAnnotatedType(oldTran.getAnnotatedType());
        t.setAnnotatedId(oldTran.getAnnotatedId());
        t.setCustomerAgeLevel(oldTran.getCustomerAgeLevel());
        DaiShouDef appendDaiShouDef = (DaiShouDef)CreamSession.getInstance().
            getAttribute(WorkingStateEnum.WHOLESALE_STATE,"appendDaiShouDef");
         // 数量和金额是负数
        t.addDaiShouLineItem(appendDaiShouDef, oldTran.getAnnotatedId(), (HYIDouble)oldTran.getSalesAmount().clone());
        t.initForAccum();
        t.accumulate();
        for (int i = 1; i <= 4; i++) {
            String payamt = "PAYAMT" + i;
            String payid = "PAYNO" + i;
            t.setFieldValue(payamt, ((HYIDouble) oldTran.getFieldValue(payamt)));
            t.setFieldValue(payid, ((String) oldTran.getFieldValue(payid)));
        }
        t.setChangeAmount(oldTran.getChangeAmount());
        t.setSpillAmount(oldTran.getSpillAmount());
        //t.setInvoiceCount(1); //TODO ?
        //t.calcInvoiceAmount();
        // TODO 该交易是否能打印?
        return t;
    }

    /**
     * @param e
     */
    private void dealWithSaveDataException(Exception e) {
        CreamToolkit.logMessage(e);
        // 儲存交易失敗後，若發票已經開始打印，要在發票上印CANCEL字樣
        if (PARAM.isInvoicePrintingUnfinished()) {
            CreamToolkit.logMessage("DrawerOpenState | print CANCELED!");
            CreamPrinter printer = CreamPrinter.getInstance();
            try {
                printer.printCancel(null, "CANCELED!");
            } catch (SQLException e1) { // never happened
                CreamToolkit.logMessage(e);
            }
        }
        CreamToolkit.haltSystemOnDatabaseFatalError(e);
    }

    /**
     * 1.Generate a wait-for-drawer-closed thread
     * 2.checkdrawerclose
     * @param posHome
     * @param isWaitForCloseDrawer
     */
    private void waitForCloseDrawer(final POSPeripheralHome3 posHome, boolean isWaitForCloseDrawer) {
        CreamToolkit.logMessage("DrawerOpenState | entry | start checkdrawerclose");
        if (isWaitForCloseDrawer) {
            CreamToolkit.logMessage("DrawerOpenState | entry | waiting drawer close begin...");
            // Generate a wait-for-drawer-closed thread
            new Thread(new Runnable() {
                public void run() {
                    try {
                        // Start a timer.
                        int wait = PARAM.getDrawerCloseTime();
                        isShowWarning = true;
                        startWarning(wait);
                        CashDrawer drawer = posHome.getCashDrawer();
                        if (drawer != null) {
                            CreamToolkit.logMessage("DrawerOpenState | entry | waitForDrawerClose");
                            drawer.waitForDrawerClose(wait * 1000, 0, 0, 300);
                            CreamToolkit
                                .logMessage("DrawerOpenState | entry | waitForDrawerClose return");
                            if (showWarningHint != null)
                                showWarningHint.interrupt();
                            drawer.removeStatusUpdateListener(posHome);
                            CreamToolkit
                                .logMessage("DrawerOpenState | entry | removeStatusUpdateListener");
                        }
                        isShowWarning = false;
                    } catch (Exception e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                        return;
                    }
                }
            }).start();

        } else {
            // fire fake signal
            new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                    try {
                        CashDrawer drawer = posHome.getCashDrawer();
                        posHome.statusUpdateOccurred(new StatusUpdateEvent((drawer != null)
                            ? drawer : new CashDrawer() {
                                @Override
                                public void claim(int timeout) throws JposException {}

                                @Override
                                public void close() throws JposException {}

                                @Override
                                public void open(String logicalName) throws JposException {}

                                @Override
                                public void release() throws JposException {}

                                @Override
                                public boolean getDrawerOpened() throws JposException { return false; }

                                @Override
                                public void openDrawer() throws JposException {}

                                @Override
                                public void waitForDrawerClose(int beepTimeout, int beepFrequency, int beepDuration, int beepDelay) throws JposException {}

                            }, 0));
                        CreamToolkit.logMessage("DrawerOpenState | entry | checkdrawerclose end");
                    } catch (Exception e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                        return;
                    }
                }
            }).start();

        }
    }

    /**
     * Print payment and update invoiceCount for this tran
     * @param connection
     * @param tran
     * @param printTranOnce
     * @throws SQLException
     * @throws EntityNotFoundException
     */
    private void printRemainder(DbConnection connection, Transaction tran, boolean printTranOnce) throws SQLException, EntityNotFoundException {
        // Print payment
        if (/* !tran.getDealType3().equals("4") &&
             */!tran.getDealType2().equals("G")         // 投庫
                && !tran.getDealType2().equals("H")     // 借零
                && !tran.getDealType2().equals("4")     // PaidIn/PaidOut
                // Bruce/2003-12-24/
                // 部分退货交易不要在这印payment，因为已经在ReturnSummaryState印过了
                && !tran.getDealType3().equals("4")
//                  && !app.getReturnItemState()) 
                && !app.getIsAllReturn()) {

            if (tran.isPeiDa() || printTranOnce) {
                try {
                    CreamPrinter printer = CreamPrinter.getInstance();
                    printer.setPrintEnabled(true);

                    // 打印头信息
                    printer.printHeader(connection, tran);
                    printer.setHeaderPrinted(true);
                    Object[] lis = tran.getLineItems();
                    for (int i = 0; i < lis.length; i++) {
                        LineItem item = (LineItem) lis[i];
                        if (item.getDetailCode() != null
                                && (item.getDetailCode().equals("S")
                                    || item.getDetailCode().equals("I")
                                    || item.getDetailCode().equals("O")
                                    || (item.getDetailCode().equals("R") && tran.getDealType2().equals("3"))
                                    )) {
                            item.setPrinted(false);
                        }
                        printer.printLineItem(connection, tran, item);
                    }
                    //CreamPrinter.getInstance().printPayment(connection, tran);
                } catch (Exception e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }
            } else if (PARAM.getTranPrintType().equalsIgnoreCase("step")) {
                //CreamToolkit.logMessage("DrawerOpenState | print payment ...");
                //CreamPrinter.getInstance().printPayment(connection, tran);
            } else if (PARAM.getTranPrintType().equalsIgnoreCase("once")) {
                CreamPrinter.getInstance().printHeader(connection);
                Object[] items = tran.getLineItems();
                for (int i = 0; i < items.length; i++) {
                    LineItem item = (LineItem)items[i];
                    if (item.getDetailCode() != null
                            && (item.getDetailCode().equals("S")
                                    || item.getDetailCode().equals("I")
                                    || item.getDetailCode().equals("O")
                                    || (item.getDetailCode().equals("R") && tran.getDealType2().equals("3"))
                                    )) {
                        CreamPrinter.getInstance().printLineItem(connection, item);
                    }
                }
                //CreamPrinter.getInstance().printPayment(connection, tran);
            }
            CreamToolkit.logMessage("DrawerOpenState | print payment ...");
            CreamPrinter.getInstance().printPayment(connection, tran);
            CreamToolkit.logMessage("DrawerOpenState | print end ...");
        }
    }

    private void refreshInvoiceCount(Transaction tran) {
        int receiptPageCount = ReceiptGenerator.getInstance().getPageNumber();
        tran.setInvoiceCount(receiptPageCount);
        if (receiptPageCount == 0) {
            tran.setInvoiceID("");
            tran.setInvoiceNumber("");
        }
    }

    private void openDrawerForTrainingMode(final POSPeripheralHome3 posHome) {
        Runnable training = new Runnable() {
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                try {
                    CashDrawer drawer = posHome.getCashDrawer();
                    if (!posHome.getEventForwardEnabled())
                        posHome.setEventForwardEnabled(true);
                    CreamToolkit.sleepInSecond(1);
                    posHome.statusUpdateOccurred(new StatusUpdateEvent(drawer, 0));
                } catch (Exception ne) {
                    CreamToolkit.logMessage(ne.toString());
                    return;
                }
            }
        };
        new Thread(training).start();
        return;
    }

    static Thread showWarningHint = null;

    private void startWarning(int w) {
        final int wait = w;
        Runnable startWarning = new Runnable() {
            public void run() {
                try {
                    Thread.sleep(wait * 1000 + 1000);
                    if (isShowWarning)
                        app.getWarningIndicator().setMessage(res.getString("PleaseCloseDrawer"));
                } catch (Exception e) {
                    return;
                }
            }
        };
        showWarningHint = new Thread(startWarning);
        showWarningHint.start();
    }

    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");

        if (app.getTrainingMode()) {
            return exitState;
        }
        if (app.getBuyerNumberPrinted())
            app.setBuyerNumberPrinted(false);

        app.setIsAllReturn(false);
        if (app.getReturnItemState()) {
            app.setReturnItemState(false);
            app.getItemList().setBackLabel(null);
        }

        Transaction tran = app.getCurrentTransaction(); 
        if (tran.isStoreComplete()) {
            if (updateSCPeiDaStatus(tran) != 0) {
                CreamToolkit.logMessage("updateSCPeiDaStatus fail!");
//                app.getWarningIndicator().setMessage(res.getString("PeiDaError8"));
            }
        }
        
        if (PARAM.isCountInvoiceEnable()) {
            int invNo = 0;
            try {
                invNo = Integer.parseInt(PARAM.getInvoiceNumber());
                // invNo--;
            } catch (Exception e) {
            }
            // 只需判断最后3位，台湾发票号是0--249 每打250张
            int countDown = 250 - (invNo % 250); // 还剩几张
            System.out.println("--------- InitialState | pages : " + (countDown ) + " | "
                + invNo);
            if (countDown <= 5) {
                Object[] args = { "" + (countDown) };
                app.getWarningIndicator().setMessage(
                    MessageFormat.format(res.getString("InvoiceNumberCountDown"), args));
            }
        }

        return exitState;
    }

    private void storeTransaction(DbConnection connection, Transaction tran) throws SQLException {
        CreamToolkit.logMessage("DrawerOpenState | storeTransaction");

        synchronized (POSTerminalApplication.waitForTransactionStored) { // -> see CheckCashierPasswordState

            // Meyer/2003-02-26/
            // UpdateTaxMM only when meeting version 1
            if (MIX_AND_MATCH_VERSION == null || MIX_AND_MATCH_VERSION.trim().equals("")
                || MIX_AND_MATCH_VERSION.trim().equals("1")
                || MIX_AND_MATCH_VERSION.trim().equals("3")) {
                tran.updateTaxMM();
            }

            // Object lineItems[] = cTransac.getLineItems();
            // HYIDouble amount = new HYIDouble(0);
            // HYIDouble lineAmount = new HYIDouble(0);
            if (!isReturnTran) { // 退貨交易已經在ReturnSummaryState中保存過了，這裡不需要再store()一次
                CreamToolkit.logMessage("DrawerOpenState | storeTransaction | start ...");
                tran.store(connection);
                // connection.commit();
            }

        }
    }

    private int updateSCPeiDaStatus(Transaction tran) {
        if (tran.isPeiDa()) {
            String storeID = tran.getStoreNumber();
            String deliveryID = tran.getAnnotatedId();
            String posNo = tran.getTerminalNumber().toString();
            String tranNo = tran.getTransactionNumber().toString();
            String empId = tran.getCashierNumber();

//            String[] params = { "storeID", storeID, "deliveryID", deliveryID.substring(0, 8),
//                "posNo", posNo, "tranNo", tranNo, "empId", empId };
//            Object o = null;
//            try {
//                o = InlineClient.getDataFromHttp("updateDeliveryStatus", params);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            try {
                Client inlineClient = Client.getInstance();
                // updateDeliveryStatus {storeID} {deliveryNo} {posNumber} {transactionNumber} {updateUserId}
                String command = "updateDeliveryStatus ";
                command += storeID;
                command += " " + deliveryID;
                command += " " + posNo;
                command += " " + tranNo;
                command += " " + empId;
                inlineClient.processCommand(command);
                Object o = inlineClient.getReturnObject2();
                if (o == null || o.toString().equals("null"))
                    return 1;
                return (Integer)o;
            } catch (ClientCommandException e) {
                CreamToolkit.logMessage(e);
                return 1;
            }
        }
        return 0;
    }
}
