package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * ReturnSelect1State: 選擇全退、部份退。
 */
public class ReturnSelect1State extends State {
    static ReturnSelect1State returnSelect1State = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private String returnType;
    private ResourceBundle res = CreamToolkit.GetResource();

    public static ReturnSelect1State getInstance() {
        try {
            if (returnSelect1State == null) {
                returnSelect1State = new ReturnSelect1State();
            }
        } catch (InstantiationException ex) {
        }
        return returnSelect1State;
    }

    public ReturnSelect1State() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        app.getWarningIndicator().setMessage("");

        if (event.getSource() instanceof NumberButton) {
            String numberLabel = ((NumberButton)event.getSource()).getNumberLabel();
            if (numberLabel.equals("1") || numberLabel.equals("2")) {
                // "选择：（1）全退，（2）部分退" 后进入到此
                returnType = ((NumberButton)event.getSource()).getNumberLabel();
                app.getMessageIndicator().setMessage(returnType);
            }

        } else if (event.getSource() instanceof ClearButton
            && sourceState instanceof ReturnAllState) {
            app.getMessageIndicator().setMessage(res.getString("ReturnSelect1"));

        } else if (event.getSource() instanceof EnterButton
            && sourceState instanceof ReturnShowState) {
            if (CreamSession.getInstance().getTransactionToBeRefunded().isPeiDa()) {
                returnType = "1"; //配达固定为returnType="1"
            } else 
                returnType = ((NumberButton)event.getSource()).getNumberLabel();
            app.getMessageIndicator().setMessage(returnType);
        }                 
    }

    public Class exit(EventObject event, State sinkState) {
        CreamToolkit.logMessage("ReturnSelect1State | returnType : " + returnType);
        if (event.getSource() instanceof EnterButton) {
            // 这里下面主要是检查是否可以全退或这部分退货
            clearMessage();
            if (returnType.equals("1")) {
                if (canReturnAll()) {
                    //return MixAndMatchState.class;
                    return ReturnAllState.class;
                } else {
                    showWarningMessage("DontAllowDaiShou2Return");
                    return ReturnShowState.class;
                }

            } else if (returnType.equals("2")) {
                //Bruce/20030423/
                // Check property "DontAllowPartialReturn" for 灿坤
                if (!PARAM.isAllowPartialReturn()) {
                    showWarningMessage("DontAllowPartialReturn");
                    return ReturnShowState.class;
                } else if (CreamSession.getInstance().getTransactionToBeRefunded()
                        .hasCreditCardBonusDiscount()) { // 有信用卡紅利折抵交易無法進行部分退貨
                    showWarningMessage("DontAllowPartialReturnWithBonusDiscount");
                    return ReturnShowState.class;
                } else {
                    clearWarningMessage();
                    return ReturnOneState.class;
                }
            } else {
                clearWarningMessage();
                return ReturnShowState.class;
            }
        }

        if (event.getSource() instanceof ClearButton) {
            clearWarningMessage();
            return ReturnShowState.class; 
        }
        return sinkState.getClass();
    }

    private boolean canReturnAll() {
        boolean b = true;
        if (!PARAM.isCanDaiShou2Return()) {
            //看交易中是否有代收
            if (CreamSession.getInstance().getTransactionToBeRefunded().getDaiShouAmount2()
                .compareTo(new HYIDouble(0)) == 1) 
                b = false;
        }
        return b;
    }
    
    /**
     * 1 == 全退
     * 2 == 部分退
     * @return
     */
    public String getReturnType() {
        return returnType;
    }
}
