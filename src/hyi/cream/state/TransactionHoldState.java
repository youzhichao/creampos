package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.ShiftReport;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.ZReport;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.ReceiptGenerator;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;
import java.util.ResourceBundle;

public class TransactionHoldState extends State {

    private static TransactionHoldState instance = new TransactionHoldState();
    private ResourceBundle res = CreamToolkit.GetResource();
    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    private TransactionHoldState() {}

    public static TransactionHoldState getInstance() {
        return instance;
    }

    public void entry(EventObject event, State sourceSate) {
        app.getMessageIndicator().setMessage(res.getString("HoldTransaction"));
    }

    public Class exit(EventObject event, State sinkSate) {
        if (event.getSource() instanceof ClearButton) {
            app.getMessageIndicator().setMessage("");
            return IdleState.class;
        } else if (event.getSource() instanceof EnterButton) {
            app.getMessageIndicator().setMessage("");
            Transaction trn = POSTerminalApplication.getInstance()
                .getCurrentTransaction();
            if (!trn.getDealType2().equals("0"))
                return IdleState.class;
            // 挂单交易存入tranhead_hold, trandetail_hold.
            boolean succ = false;
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getTransactionalConnection();
                trn.setInvoiceCount(ReceiptGenerator.getInstance().getPageNumber());
                //trn.setTransactionType("01");
                trn.setTranType1("1");
                succ = trn.holdTransaction(connection);

                try {
                    CreamPrinter printer = CreamPrinter.getInstance();
                    if (!printer.getHeaderPrinted()) {
                        if (!printer.isPrinterAtRightStart()) { // 檢查印表機是否已定位
                            setWarningMessage(CreamToolkit.GetResource().getString("PrinterIsNotAtRightPosition"));
                            PrintPluWarningState.setExitState(IdleState.class);
                            return PrintPluWarningState.class;
                        }
                        printer.printHeader(connection, trn);
                        printer.setHeaderPrinted(true);
                    }

                    if (!printer.isPrinterHealthy()) { // 檢查印表機是否ready
                        setWarningMessage(CreamToolkit.GetResource().getString("PrinterIsNotReady"));
                        PrintPluWarningState.setExitState(IdleState.class);
                        return PrintPluWarningState.class;
                    }

                    for (Object lineItemObj : trn.getLineItems()) {
                        LineItem lineItem = (LineItem)lineItemObj;
                        if (!lineItem.getPrinted())
                            printer.printLineItem(connection, lineItem);
                    }
                    // 统计挂单次数
                    ZReport curZ = ZReport.getCurrentZReport(connection);
                    if (curZ.getHoldtrancount() != null) {
                        curZ.setHoldtrancount(curZ.getHoldtrancount().intValue() + 1);
                    } else {
                        curZ.setHoldtrancount(1);
                    }
                    ShiftReport curShift = ShiftReport.getCurrentShift(connection);
                    if (curShift.getHoldtrancount() != null) {
                        curShift.setHoldtrancount(curShift.getHoldtrancount().intValue() + 1);
                    } else {
                        curShift.setHoldtrancount(1);
                    }
//        			GetProperty.setInvoicePrintingUnfinished(true);
//                    CreamPropertyUtil.getInstance().direct(new String[] {"InvoicePrintingUnfinished"});95

                } catch (Exception e) {
                    e.printStackTrace();
                }
                CreamToolkit.logMessage("TransactionHoldState | print HELD!");
                CreamPrinter printer = CreamPrinter.getInstance();
                printer.printCancel(connection, "HELD!!");
                connection.commit();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (SQLException e1) {
                // never happened
                CreamToolkit.logMessage(e1);
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
            if (!succ) {
                String err = res.getString("HoldTransactionFail");
                CreamToolkit.logMessage("Error>> " + err);
                app.getMessageIndicator().setMessage(err);
                return WarningState.class;
            }
            return InitialState.class;
        }
        app.getMessageIndicator().setMessage("error button");
        return WarningState.class;
    }
}
