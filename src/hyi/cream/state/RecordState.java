package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.util.HYIEventObject;
import hyi.spos.events.DataEvent;
import hyi.spos.events.StatusUpdateEvent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.Map;

//import jpos.events.DataEvent;
//import jpos.events.StatusUpdateEvent;

public class RecordState extends State {
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static RecordState replayState    = null;

    public static RecordState getInstance() {
        try {
            if (replayState == null) {
            	replayState = new RecordState();
            }
        } catch (InstantiationException ex) {
        }
        return replayState;
    }

    /**
     * Constructor
     */
    public RecordState() throws InstantiationException {
    }


	@Override
	public void entry(EventObject event, State sourceState) {
		app.getMessageIndicator().setMessage("Enter...");
	}

	@Override
	public Class exit(EventObject event, State sinkState) {
		StateMachine stateMachine = StateMachine.getInstance();
		if (stateMachine.getRecorded() == 0) {
			stateMachine.getRecordList().clear();
			stateMachine.setRecorded(1);
		} else if (stateMachine.getRecorded() == 1) {
			stateMachine.setRecorded(0);
			File file = new File("record");
			file.mkdir();
			file = new File(
//					"record" + File.separator  
//					+ CreamCache.getInstance().getDateTimeFormate4().format(new Date()) 
//					+ 
					"record.dac");
			try {
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream((file)));
				
				oos.writeObject(convert(stateMachine.getRecordList()));
				
				oos.flush();
				oos.close();
				System.out.println("save to -- " + stateMachine.getRecordList());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 
        return IdleState.class;
	}

	private List<HYIEventObject> convert(List<Map<EventObject, Long>> events) {
		List<HYIEventObject> es = new ArrayList<HYIEventObject>();
		for (Map<EventObject, Long> map : events) {
			EventObject event = map.keySet().iterator().next();
			HYIEventObject e = new HYIEventObject();
			e.setEventClass(event.getClass());
			e.setSource(event.getSource());
			e.setSleepTimes(map.get(event));
			if (event instanceof StatusUpdateEvent) {
				continue;
//				if (((StatusUpdateEvent) event).getSource() instanceof Keylock) {
//					continue;
//				}
			} else if (event instanceof DataEvent) {
				continue;
//				e.setStatus(((DataEvent) event).getStatus());
			}
			es.add(e);
		}
		return es;
	}
}
