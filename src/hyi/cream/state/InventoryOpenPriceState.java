package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Inventory;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.Indicator;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * 盘点开放金额输入State.
 */
public class InventoryOpenPriceState extends State {
    private static InventoryOpenPriceState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Indicator messageIndicator = app.getMessageIndicator();
    private Indicator warningIndicator = app.getWarningIndicator();
    private ResourceBundle res = CreamToolkit.GetResource();
    private static StringBuffer price = new StringBuffer();

    public static InventoryOpenPriceState getInstance() {
        try {
            if (instance == null) {
                instance = new InventoryOpenPriceState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public InventoryOpenPriceState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (price.length() == 0)
            messageIndicator.setMessage(res.getString("PleaseInputInventoryItemPrice"));
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            price.append(pb.getNumberLabel());
            messageIndicator.setMessage(price.toString());
            warningIndicator.setMessage("");
            return sinkState.getClass();
        }
        if (event.getSource() instanceof ClearButton) {
            price.setLength(0);
            warningIndicator.setMessage("");
            return sinkState.getClass();
        }
        if (event.getSource() instanceof EnterButton) {
            if (price.length() != 0) {
                try {
                    Inventory inv = InventoryIdleState.getCurrentInventory();
                    HYIDouble amt = new HYIDouble(price.toString());
                    if (amt.scale() > 4)
                        amt = amt.setScale(4, BigDecimal.ROUND_HALF_UP);
                    inv.setAmount(amt);
                    inv.setActStockQty(amt.divide(inv.getUnitPrice(), 2, BigDecimal.ROUND_HALF_UP));
                    app.getDacViewer().repaint();
                    warningIndicator.setMessage("");
                    return InventoryStoreState.class;
                } catch (NumberFormatException e) {
                    price.setLength(0);
                    warningIndicator.setMessage(res.getString("InputWrong"));
                }
            }
            return InventoryQuantityState.class;
        }
        return sinkState.getClass();
    }

    public static void clearPrice() {
        price.setLength(0);
    }
}
