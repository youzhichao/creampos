/**
 * State class
 * @since 2000
 * @author slackware
 */

package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.*;
import hyi.cream.state.wholesale.WholesaleClearingState;
import hyi.cream.state.cat.CATAuthSalesState;
import hyi.cream.state.cat.CATAuthType;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.awt.Toolkit;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.lang.StringUtils;

public class Paying3State extends State {
    // private ShiftReport shift = null;
    // private ZReport z = null;
    //private static int transactionNumber = -1;

    // private HYIDouble [] payments = {new HYIDouble(0), new HYIDouble(0), new
    // HYIDouble(0), new HYIDouble(0),};
    static Paying3State paying3State = null;

    private static Class exitState = null;

    private HYIDouble cashAmt = new HYIDouble(0.00);

    private HYIDouble changeAmountUpperLimit;
    private HYIDouble spillAmountUpperLimit;

    public static Paying3State getInstance() {
        try {
            if (paying3State == null) {
                paying3State = new Paying3State();
            }
        } catch (InstantiationException ex) {
        }
        return paying3State;
    }

    /**
     * Constructor
     */
    public Paying3State() throws InstantiationException {
        // Bruce/2003-12-01
        // 增加property "ChangeAmountUpperLimit"(default is 100)，如果找零金额大于等于此值，则报警
        changeAmountUpperLimit = PARAM.getChangeAmountUpperlimit();
        spillAmountUpperLimit = PARAM.getSpillAmountUpperlimit(); 

        if (paying3State != null) {
            throw new InstantiationException(this.getClass().getName());
        } else {
            // shift = ShiftReport.getCurrentShift();
            // z = ZReport.getCurrentZReport();
            paying3State = this;
        }
    }

    public void entry(EventObject event, State sourceState) {
        // Bruce/2003-12-01
        // System.out.println("This is Paying3State's Entry!");
        // POSTerminalApplication posTerminal =
        // POSTerminalApplication.getInstance();
        // posTerminal.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("DataTransfers"));
        // KeylockWarningState Keylock
    }

    // Calculate the total支付金额, if支付金额 < 应付金额,
    // return SummaryState.class
    // Otherwise print payment information on receipt, cut paper,
    // store currentTransaction object
    // If the current ShfitReport's SystemDateTime is not '1900/01/01
    // 00:00:00'(表示已经做过交班),
    // then count up ShiftNumber in CreamProperties,
    // and create/insert a new one. Do the same thing on ZReport.
    // Accumulate data into ShiftReport, ZReport, and CategorySales.
    // (Ref. Data Accumulation Flow).

    public Class exit(EventObject event, State sinkState) {
        if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.WHOLESALECLEARING_STATE)
            exitState = WholesaleClearingState.class;
        else
            exitState = SummaryState.class;

        cashAmt = new HYIDouble(0.00);

        POSTerminalApplication app = POSTerminalApplication.getInstance();
        Transaction trans = app.getCurrentTransaction();

        if (sinkState != null || event != null)
            return null;

        // Calculate the total payment amount
        HYIDouble subTotalAmt; //  记录应收金额
        if (CreamSession.getInstance().getWorkingState() ==
            WorkingStateEnum.WHOLESALECLEARING_STATE) {
            subTotalAmt = (HYIDouble)trans.getOriginEarnAmount().clone(); // 赊账金额作为 应收金额
        } else {
            subTotalAmt = trans.getSalesAmount();
            // .add(cTransac.getDaiFuAmount().add(cTransac.getDaiShouAmount()))
            // ;
        }
        HYIDouble totalPayAmt = new HYIDouble(0); // 各种支付方式的付款金额的总和

        // 由于新增支付规则，某些支付溢收不允许结帐。系统应该先处理此种支付，再处理其它
        // 排序支付
        List payNormalList = new ArrayList(4); // 普通支付
        List payOverYList = new ArrayList(4); // 有溢收可以结帐支付
        List payOverNList = new ArrayList(4); // 有溢收就不可以结帐支付
        splitPayList(trans, payNormalList, payOverYList, payOverNList);
        List payList = new ArrayList();
        payList.addAll(payOverNList);
        payList.addAll(payOverYList);
        payList.addAll(payNormalList);
        // end of 支付排序

        boolean nextPayment = true;
        Iterator it = payList.iterator();
        for (; nextPayment && it.hasNext();) {
            String id = it.next().toString();
            String payID = (String) trans.getFieldValue("PAYNO" + id);
            Payment payment = Payment.queryByPaymentID(payID);
            HYIDouble amt = (HYIDouble) trans.getFieldValue("PAYAMT" + id);
            if (amt != null) {
                totalPayAmt = totalPayAmt.addMe(amt);
                boolean isRebate = payment.getPaymentID().equals(PARAM.getRebatePaymentID());

                if ("4".equals(trans.getDealType2())) {
                    // paidin/out 要特殊处理, 不找零，不溢收
                    trans.setChangeAmount(new HYIDouble(0));
                    trans.setSpillAmount(new HYIDouble(0));
                    nextPayment = false;
                } else if ("Q".equals(trans.getDealType2()) && trans.getOriginEarnAmount().compareTo(new HYIDouble(0)) <= 0) {
                    // 团购预收 不找零，不溢收
                    trans.setChangeAmount(new HYIDouble(0));
                    trans.setSpillAmount(new HYIDouble(0));
                    nextPayment = true;
                } else if ((
                            !(app.getReturnItemState() || "X".equals(app.getCurrentTransaction().getState1())/*Nitori退着付殘金交易*/)
                                && totalPayAmt.doubleValue() > subTotalAmt.doubleValue())
                        || (
                            (app.getReturnItemState() || "X".equals(app.getCurrentTransaction().getState1())/*Nitori退着付殘金交易*/)
                                && totalPayAmt.doubleValue() < subTotalAmt.doubleValue())) {
                    // 负项销售状态时，判断金额大小要完全相反
                    // -- 考虑 找零和或溢收:
                    // 1. 非负向销售：总支付 > 应收金额
                    // 2. 是负向销售：总支付 < 应收金额
                    if (!payment.isChangeable()) {// 不可找零
                        HYIDouble spillAmount = totalPayAmt.subtract(subTotalAmt);

                        if (payOverNList.size() > 0 && spillAmount.compareTo(new HYIDouble(0)) > 0) { // 有溢收就不可以结帐
                            showWarningMessage("DontAllowGenerateSpillAmount");
                            //"不允许有溢收,按清除后重新结账。"
                            return exitState;
                        }

                        if (spillAmount.compareTo(spillAmountUpperLimit) > 0) {
                            showWarningMessage("InputAmountIsTooLarge");
                            trans.clearPaymentInfo();
                            return exitState;
                        }

                        trans.setSpillAmount(spillAmount);
                        trans.setChangeAmount(new HYIDouble(0));

                        // 总支付已经大于 应收了，目前还在处理 不可找零 的支付，
                        // 后面如果还有其他支付，有多余的付款金额
                        if (payNormalList.size() > 0) {
                            app.getWarningIndicator().setMessage(
                                    CreamToolkit.GetResource().getString(
                                    "CashWarning2"));//"有多余的付款金额"
                            return exitState;
                        }
                    } else {// 可找零
                        trans.setSpillAmount(new HYIDouble(0));
                        if (isRebate) {
                            trans.setRebateChangeAmount(totalPayAmt
                                    .subtract(subTotalAmt)); // 还元金结余
                            trans.setTotalRebateAmount(
                                    trans.getRebateChangeAmount()
                                    .add(trans.getRebateAmount())); // 还元金累计=还元金结余+本次新增还元金
                        } else {
                            // Bruce/2003-12-01
                            // 增加property "ChangeAmountUpperLimit"(default is 100)，如果找零金额大于等于此值，则报警
                            HYIDouble chgAmount = totalPayAmt.subtract(subTotalAmt);
                            if ((!app.getReturnItemState() || "X".equals(app.getCurrentTransaction().getState1())/*Nitori退着付殘金交易*/)
                                    //&& CreamSession.getInstance().getWorkingState() != WorkingStateEnum.WHOLESALECLEARING_STATE
                                    && chgAmount.compareTo(changeAmountUpperLimit) >= 0) {
                                showWarningMessage("InputAmountIsTooLarge");
                                trans.clearPaymentInfo();
                                return exitState;
                            }
                            trans.setChangeAmount(chgAmount);
                        }
                    }
                    nextPayment = false;
                    // nextPayment = it.hasNext();
                } else if (totalPayAmt.doubleValue() == subTotalAmt.doubleValue()) {
                    trans.setChangeAmount(new HYIDouble(0));
                    trans.setSpillAmount(new HYIDouble(0));
                    nextPayment = false;
                    // nextPayment = it.hasNext();
                } else
                    nextPayment = true;

                if (payment.getPaymentID().equals("00")) // 现金
                    cashAmt = amt;

                if (isRebate)
                    trans.setUseRebateAmt(amt); // 本次使用还元金

                if (payment.isNonInvoice()) {// 已开过发票
                    if (trans.getBalance().compareTo(new HYIDouble(0)) == 0) {
                        // currTran.setNetSalesAmount(currTran
                        // .getNetSalesAmount().subtract(amt)
                        // .addMe(currTran.getSpillAmount()));
                        trans.setInvoiceAmount(trans.getInvoiceAmount()
                            .subtract(amt)
                            .addMe(trans.getSpillAmount())
                            .addMe(trans.getChangeAmount()));
                    }
                }
            }

        } // end for

        // Bruce/20030417 (Modified for 灿坤)
        // 若property "DontAllowGenerateSpillAmount" 为 yes，表示不允许生成溢收
//                if (GetProperty.getProperty(
//                        "DontAllowGenerateSpillAmount", "no").equalsIgnoreCase(
//                        "yes")
//                        && currTran.getSpillAmount()
//                                .compareTo(new HYIDouble(0)) != 0) {
//                    app.getWarningIndicator().setMessage(
//                            CreamToolkit.GetResource().getString(
//                                    "DontAllowGenerateSpillAmount"));
//                    exitState = SummaryState.class;
//                    return exitState;
//                }

        if (!nextPayment && it.hasNext()) {
            app.getWarningIndicator().setMessage(
                    CreamToolkit.GetResource().getString(
                    "CashWarning2")); //"有多余的付款金额"
            return exitState;
        }

        // if (!app.getReturnItemState() && nextPayment) {

        // 负项销售可用多种支付
        if (nextPayment && !(
            "Q".equals(trans.getDealType2()) && trans.getOriginEarnAmount().compareTo(new HYIDouble(0)) <= 0)) {
            app.getWarningIndicator().setMessage(CreamToolkit.getString("CashWarning")); //付款金额不足
            Toolkit.getDefaultToolkit().beep();
            app.getPayingPane().forceRepaint();

        } else {
            if ("Q".equals(trans.getDealType2())) {
                // 团购还款 或 团购预收
                HYIDouble clearingAmt = new HYIDouble(0);
                if (trans.getPayAmount1() != null)
                    clearingAmt.addMe(trans.getPayAmount1());
                if (trans.getPayAmount2() != null)
                    clearingAmt.addMe(trans.getPayAmount2());
                if (trans.getPayAmount3() != null)
                    clearingAmt.addMe(trans.getPayAmount3());
                if (trans.getPayAmount4() != null)
                    clearingAmt.addMe(trans.getPayAmount4());
                if (trans.getChangeAmount() != null)
                    clearingAmt.subtractMe(trans.getChangeAmount());
                if (trans.getSpillAmount() == null)
                    clearingAmt.subtractMe(trans.getSpillAmount());
                trans.setPreRcvAmount(clearingAmt.subtract(trans.getOriginEarnAmount()));
            }

            if (PARAM.isAllowSpecialRebate())
                countSpecialRebate(trans);

            // 若為傳票交易，再度檢查傳票版本，若版本不符，顯示警告訊息，不能讓user結帳
            if ("P".equals(trans.getAnnotatedType()) && !StringUtils.isEmpty(trans.getAnnotatedId())) {
                boolean versionCorrect = true;
                try {
                    /*
                     *  -1=无法连接sc
                     *  可結帳
                     *      'H'; //批發配達
                     *      'P'; //普通配達
                     *      'M'; //窗簾修改
                     *      'O'; //窗簾訂做
                     *  1=不存在
                     *  2=已作廢
                     *  3=售後處理
                     *  4=結清,pos不可結
                     *  5=被退過貨的
                     *  6=單據版本不符合
                     *  7=退货机可退貨,pos不可结帐
                     *  8=异常
                     */
                    String statusAndSrcBillNo = StateToolkit.getPeiDaStatus(trans.getAnnotatedId());
                    // status = deliveryhead.結帳狀態 + "," + deliveryhead.srcBillNo(原始單號)
                    String[] x = statusAndSrcBillNo.split(",");
                    String statusStr = x[0];
                    if ("6".equals(statusStr))  //6=單據版本不符合，不讓結帳
                        versionCorrect = false;
                } catch (java.net.SocketException e) {
                    CreamToolkit.logMessage(e);
                    //versionCorrect = false; 無法連線還是讓他結帳好了
                } catch (Exception e) {
                    CreamToolkit.logMessage(e);
                    versionCorrect = false;
                }
                if (!versionCorrect) {
                    showWarningMessage("PeiDaError6_2");
                    return SummaryState.class;
                }

            }
            if (trans.existsCATPayment()) {
                CATAuthSalesState.getInstance().setAuthType(CATAuthType.SALES);
                exitState = CATAuthSalesState.class;

            } else if (!PARAM.isNeedCheckOutConfirm() || trans.getAlipayList().size() > 0 || trans.getCmpayList().size() > 0 || trans.getWeiXinList().size() > 0 || trans.getUnionPayList().size() > 0
                    || trans.getEcDeliveryHeadList().size() > 0) {
                final Transaction tranh = app.getCurrentTransaction();
                new Thread() {
                    public void run() {
                        if (tranh.getEcDeliveryHeadList().size() > 0) {
                            tranh.getEcDeliveryHeadList().get(0).setPayId(tranh.getPayNumber1());
                            if (tranh.getEcDeliveryHeadList().get(0).getServiceType().equals("1") || tranh.getEcDeliveryHeadList().get(0).getServiceType().equals("3")) {
                                ECommerceHttp echttp = new ECommerceHttp();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                                echttp.getResult("api/store/order/update/" + tranh.getEcDeliveryHeadList().get(0).getStoreID() + "/" + tranh.getEcDeliveryHeadList().get(0).getTransactionNumber() + "/" +
                                        "" + tranh.getEcDeliveryHeadList().get(0).getOrderId() + "/1/" + sdf.format(new Date()) + "", "", "GET");
                            } else if (tranh.getEcDeliveryHeadList().get(0).getServiceType().equals("2")) {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String requestParameters = "{\n" +
                                        "  \"orderId\": " + tranh.getEcDeliveryHeadList().get(0).getOrderId() + ",\n" +
                                        "  \"payAmount\": " + tranh.getEcDeliveryHeadList().get(0).getAmount() + ",\n" +
                                        "  \"payId\": \"" + tranh.getEcDeliveryHeadList().get(0).getPayId() + "\",\n" +
                                        "  \"payInfo\": {\n" +
                                        "    \"cardNo\": \"\",\n" +
                                        "    \"userId\": \"\",\n" +
                                        "    \"transactionId\": \"\"\n" +
                                        "  },\n" +
                                        "  \"payTime\": \"" + sdf.format(new Date()) + "\",\n" +
                                        "  \"storeId\": \"" + tranh.getEcDeliveryHeadList().get(0).getStoreID() + "\",\n" +
                                        "  \"tranNo\": \"" + tranh.getEcDeliveryHeadList().get(0).getTransactionNumber() + "\"\n" +
                                        "}";
                                ECommerceHttp echttp = new ECommerceHttp();
                                echttp.getResult("api/store/order/pay", requestParameters, "POST");
                            }
                        }
                    }
                }.start();
                exitState = hyi.cream.state.DrawerOpenState.class;

            } else {
                exitState = hyi.cream.state.DrawerOpenConfirmState.class;
            }

            // System.out.println("cTransac.getPayAmount2()=" +
            // cTransac.getPayAmount2());

            // Bruce/2003-12-02
            // 如果用repaint()的话，很奇怪，不会马上画出来。所以只好搞一个forceRepaint()。
            app.getPayingPane().forceRepaint();
            // app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("DataTransfers"));
        }
        return exitState;

        //} finally {
            // System.out.println("transactionNumber=" + transactionNumber);
            // System.out.println("cTransac.getTransactionNumber().intValue=" +
            // cTransac.getTransactionNumber().intValue());
            // if (exitState != SummaryState.class)
            //transactionNumber = trans.getTransactionNumber().intValue();
        //}
    } // KeylockWarningState Keylock

    /**
     * @param currTran
     * @param payNormalList
     * @param payOverYList
     * @param payOverNList
     */
    private void splitPayList(final Transaction currTran, List payNormalList, List payOverYList, List payOverNList) {
        for (int i = 1; i <= 4; i++) {
            String payID = (String) currTran.getFieldValue("PAYNO" + i);
            if (payID == null || payID.trim().equals("")) {
                break; // PAYNOi 里面没有写 payID ，就不用去看payment的金额了
            }
            Payment payment = Payment.queryByPaymentID(payID);
            if (payment == null) {
                break;
            }

            if (!payment.isChangeable()) { // 不找零
                if (payment.isSpillable()) {
                    payOverYList.add(new Integer(i));
                } else {
                    payOverNList.add(new Integer(i));
                }
            } else {
                payNormalList.add(new Integer(i));// 可找零
            }
        }
    }

    /*
     * 如果现金支付的金额大于产生还元金的明细金额总和，额外再增加还元金
     */
    private void countSpecialRebate(Transaction tran) {
        if (tran.getMemberID() == null || tran.getMemberID().trim().equals(""))
            return;

        Iterator itr = tran.getLineItemsIterator();
        HYIDouble sum = new HYIDouble(0.0); // 还元金的明细金额总和
        // cashAmt 现金支付的金额
        String specialRebateRate = PARAM.getSpecialRebateRate();

        while (itr.hasNext()) {
            LineItem li = (LineItem) itr.next();
            System.out.println("lineitem : " + li.getLineItemSequence()
                    + " | RebateRate : " + li.getRebateRate());

            if (li.getAddRebateAmount().abs().compareTo(new HYIDouble(0.00)) != 0
                    && li.getAddRebateAmount().abs().compareTo(
                            new HYIDouble(-0.00)) != 0) {
                System.out.println("getAddRebateAmount : "
                        + li.getAddRebateAmount());
                sum = sum.addMe(li.getAfterDiscountAmount());
            }
        }

        // 如果现金支付的金额，不小于还元金商品销售总额，累计还元金增加特别的金额
        // 特别还元金<=有还元金商品销售总额*specialRebate
        HYIDouble specialRebate = new HYIDouble(0);
        if (sum.compareTo(cashAmt) <= 0)
            specialRebate = sum.multiply(new HYIDouble(specialRebateRate))
                    .setScale(1, BigDecimal.ROUND_HALF_UP);
        else
            specialRebate = cashAmt.multiply(new HYIDouble(specialRebateRate))
                    .setScale(1, BigDecimal.ROUND_HALF_UP);
        System.out.println("sum : " + sum + " | cashAmt : " + cashAmt
                + " | specialRebate : " + specialRebate);
        tran.setSpecialRebateAmount(specialRebate);
        tran.setRebateAmount(tran.getRebateAmount().addMe(specialRebate)
                .setScale(1, BigDecimal.ROUND_HALF_UP));

        tran.setTotalRebateAmount(tran.getRebateAmount());
    }

}