/**
 * State class
 * @since 2000
 * @author slackware
 */
 
package hyi.cream.state;

//for event processing
import java.util.*;

import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

public class ReturnSlippingState extends State {
	private boolean validated = false;
    static ReturnSlippingState slipping2State = null;

    public static ReturnSlippingState getInstance() {
        try {
            if (slipping2State == null) {
                slipping2State = new ReturnSlippingState();
            }
        } catch (InstantiationException ex) {
        }
        return slipping2State;
    }

    /**
     * Constructor
     */
    public ReturnSlippingState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
		//System.out.println("This is ReturnSlippingState's Entry!");
		//POSPeripheralHome posHome = POSPeripheralHome.getInstance();
		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
		//DiscountState	null
		if (sourceState instanceof ReturnSummaryState
            || sourceState instanceof MixAndMatchState) {
//Show message "请按[认证]键打印, [确认]键继续" on messageIndicator.
			posTerminal.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("ReturnSlipping"));
			//posTerminal.showIndicator("message");
		} else if (sourceState.getClass().getName().endsWith("ReturnSlippingState")
			&& event.getSource() instanceof SlipButton) {//ReturnSlippingState	SlipButton
//Print a string on slip.
			CreamPrinter.getInstance().printSlip(this);
		  	validated = true;
		}//KeylockWarningState	Keylock
    }
//exit()
//Sink State	Event Source	Action	Collaborated Things
	public Class exit(EventObject event, State sinkState) {
		//System.out.println("This is ReturnSlippingState's Exit!");
		//ReturnSlippingState	SlipButton	Do nothing.
		//Paying2State	EnterButton	Clear messageIndicator.
		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
		if (event.getSource().getClass().getName().endsWith("EnterButton")) {
			if (validated) {
				posTerminal.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("ReturnEndMessage"));
				validated = false;
				return DrawerOpenState.class;
			}
			return ReturnSlippingState.class;
		}
		if (sinkState != null) {
			return sinkState.getClass();
		} else
            return null;
	}	//KeylockWarningState Keylock

}


