package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.EventObject;
import java.util.ResourceBundle;

public class TransactionVoidState extends State {

    // Singleton --
    private static TransactionVoidState instance = new TransactionVoidState();
    private TransactionVoidState() {}
    public static TransactionVoidState getInstance() { return instance; }

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();

    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof VoidReadyState && event.getSource() instanceof EnterButton) {
            // (VoidLast:VR2TV)
            voidLastTransaction();
        }
    }

    public Class<? extends State> voidLastTransaction() {
        //Transaction curTran = app.getCurrentTransaction();
        if (app.getTrainingMode()) {
            getCurrentTransaction().clear();
            return InitialState.class;
        }
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();
/*
            int transactionID = Transaction.getNextTransactionNumber() - 1;
            int posNumber = GetProperty.getTerminalNumber();
*/
            Transaction trans = getCurrentTransaction(); //Transaction.queryByTransactionNumber(connection, posNumber, transactionID);
            if (trans == null // 在 VoidReadyState已經檢查過了，這裡其實應該不需要檢查了，just for safe
                || (!trans.getDealType1().equals("0") || !trans.getDealType2().equals("0") ||
                    (!trans.getDealType3().equals("0") && !trans.getDealType3().equals("4")))) {
                app.getMessageIndicator().setMessage(res.getString("ReturnWarning"));

            } else {
                boolean isReturn = ReturnSummaryState.alipayReturn(connection,trans,app.getCurrentTransaction(),app,res);
                if (!isReturn) {
                    return InitialState.class;
                } else {
                    if (trans.getAlipayList().size() > 0) {
                        app.getWarningIndicator().setMessage(res.getString("AlipayRefundSuccess"));
                        try{
                            Thread.sleep(1000);
                        } catch (Exception e) {
                            e.printStackTrace(CreamToolkit.getLogger());
                        }
                    }
                }
                if (trans.getCmpayList().size() > 0 ) {
                    isReturn = ReturnSummaryState.cmpayReturn(connection, (Transaction)trans.deepClone(), app.getCurrentTransaction(), app, res);
                    if (!isReturn) {
                        return InitialState.class;
                    } else {
                        if (trans.getCmpayList().size() > 0) {
                            app.getWarningIndicator().setMessage(res.getString("CmpayRefundSuccess"));
                            try{
                                Thread.sleep(1000);
                            } catch (Exception e) {
                                e.printStackTrace(CreamToolkit.getLogger());
                            }
                        }
                    }
                }
                if (trans.getWeiXinList().size() > 0 ) {
                    isReturn = ReturnSummaryState.weiXinReturn(connection, (Transaction)trans.deepClone(), app.getCurrentTransaction(), app, res);
                    if (!isReturn) {
                        return InitialState.class;
                    } else {
                        if (trans.getWeiXinList().size() > 0) {
                            app.getWarningIndicator().setMessage(res.getString("WeiXinRefundSuccess"));
                            try{
                                Thread.sleep(1000);
                            } catch (Exception e) {
                                e.printStackTrace(CreamToolkit.getLogger());
                            }
                        }
                    }
                }
                if (trans.getUnionPayList().size() > 0 ) {
                    isReturn = ReturnSummaryState.unionPayReturn(connection, (Transaction)trans.deepClone(), app.getCurrentTransaction(), app, res);
                    if (!isReturn) {
                        return InitialState.class;
                    } else {
                        if (trans.getUnionPayList().size() > 0) {
                            app.getWarningIndicator().setMessage(res.getString("UnionPayRefundSuccess"));
                            try{
                                Thread.sleep(1000);
                            } catch (Exception e) {
                                e.printStackTrace(CreamToolkit.getLogger());
                            }
                        }
                    }
                }
                // cancel this transaction
                trans.setDealType1("*");
                trans.setDealType2("0");
                trans.setDealType3("0");
                trans.update(connection);

/* Old code --
                trans.clearLineItem();
                // set to transaction
                trans.clear();
                trans.setDealType2("0");
                trans.setInvoiceNumber(GetProperty.getInvoiceNumber("00000000"));
                trans.setInvoiceID(GetProperty.getInvoiceID("??"));
                trans.setSignOnNumber(Integer.parseInt(GetProperty.getShiftNumber("")));
                trans.setCashierNumber(GetProperty.getCashierNumber(""));
                //curTran = Transaction.queryByTransactionNumber(connection, posNumber, transactionID);
                trans.setStoreNumber(GetProperty.getStoreNumber(""));
                trans.setTerminalNumber(Integer.parseInt(GetProperty.getTerminalNumber("")));
                curTran.setTransactionType(oldTran.getTransactionType());
                curTran.setTerminalPhysicalNumber(GetProperty.getTerminalPhysicalNumber(""));
                curTran.setInvoiceID(oldTran.getInvoiceID());
                curTran.setInvoiceNumber(oldTran.getInvoiceNumber());
                curTran.setInvoiceCount(oldTran.getInvoiceCount());
                // 统一编号与原有交易相同，所以这里不要重置
                //t.setBuyerNumber("");
                // set to LineItem
                Iterator iter = LineItem.queryByTransactionNumber(connection, posNumber, transactionID);
                // int count = 0;
                if (iter != null) {
                    LineItem oldLineItem;
                    LineItem newLineItem;
                    while (iter.hasNext()) {
                        oldLineItem = (LineItem)iter.next();
                        newLineItem = oldLineItem;

                        // 2003-06-13 Updated by zhaohong 如果是限价销售,实际销售数量减 1
                        if (oldLineItem.getDiscountType() != null
                            && oldLineItem.getDiscountType().equals("L")) {
                            try {
                                Client.getInstance()
                                    .processCommand(
                                        "queryLimitQtySold " + oldLineItem.getItemNumber()
                                            + " -1 ");
                            } catch (ClientCommandException e) {
                            }
                        }

                        newLineItem.setTerminalNumber(posNumber);
                        newLineItem.setTransactionNumber(Transaction.getNextTransactionNumber());
                        try {
                            if (newLineItem.getDetailCode() != null
                                && newLineItem.getDetailCode().equals("D")) {
                                SI siDac = SI.queryBySIID(newLineItem.getPluNumber());
                                curTran.addAppliedSI(siDac.getSIID(), newLineItem.getAmount());
                            }
                            curTran.addLineItem(newLineItem, false);
                            // count++;
                        } catch (TooManyLineItemsException e) {
                        }
                    }
                }
*/
                trans.setDealType1("0");
                trans.setDealType2("0");
                trans.setDealType3("1");
                trans.setVoidTransactionNumber(trans.getTransactionNumber() * 100 + trans.getTerminalNumber());
                trans.setSystemDateTime(new Date());
                trans.setTransactionNumber(Transaction.getNextTransactionNumber());

                for (LineItem item : trans.lineItems()) {
                    item.setTransactionNumber(trans.getTransactionNumber());

                    // 2003-06-13 Updated by zhaohong 如果是限价销售,实际销售数量减 1
                    if (item.getDiscountType() != null
                        && item.getDiscountType().equals("L")) {
                        try {
                            Client.getInstance().processCommand("queryLimitQtySold " + item.getItemNumber() + " -1");
                        } catch (ClientCommandException e) {
                            CreamToolkit.logMessage(e);
                        }
                    }
                }

                trans.makeNegativeValue();
                trans.store(connection);

                showMessage("TransactionEnd");
                showWarningMessage("ReturnEndMessage");
                connection.commit();
            }
            Transaction.generateABrandNewTransaction();
            return InitialState.class;

        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
            return InitialState.class;
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
            return InitialState.class;
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (sinkState instanceof InitialState) {
            app.setChecked(false);
            // app.setSalemanChecked(false);
        }
        return sinkState.getClass();
    }
}
