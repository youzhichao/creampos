package hyi.cream.state;

import java.util.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;
import hyi.cream.inline.*;

/**
 * 输入盘点数据进入点。此State负责搜集盘点人员编号.
 */
public class InventoryState extends State {
    private static InventoryState inventoryState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private static StringBuffer cashierNumber = new StringBuffer();
    private static Class originalState = null;
	private boolean canNotDo = false;     
    
    public static InventoryState getInstance() {
        try {
            if (inventoryState == null) {
                inventoryState = new InventoryState();
            }
        } catch (InstantiationException e) {
        }
        return inventoryState;
    }

    public InventoryState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //Bruce/2003-12-01
        //修改因为没有归零，导致第二次盘点时数据无法显示
        app.getDacViewer().setCurrentIdx(0);

        if (event.getSource() instanceof NumberButton) {
			if (canNotDo) {
				return;
			}
            NumberButton pb = (NumberButton)event.getSource();
            cashierNumber.append(pb.getNumberLabel());
            app.getMessageIndicator().setMessage(cashierNumber.toString());
            return;
        } else if (sourceState.getClass().getName().indexOf("KeyLock") != -1) {
            setOriginalState(sourceState.getClass());
            
			if (!ifSingleDoingInventory()) {
				canNotDo = true;
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace(CreamToolkit.getLogger());
				}
			    return;
			} else { 
				canNotDo = false;
			}
			clearCashierNumber();
        }
    
        if (cashierNumber.length() == 0) {
			if (canNotDo) {
				return;
			}
            app.getMessageIndicator().setMessage(res.getString("InputInventoryPerson"));
        } else {
			if (canNotDo) {
				return;
			}
            app.getWarningIndicator().setMessage("");
        }
        CreamCache.getInstance().clearCache();
        System.gc();
        CreamCache.getInstance().setUsePLUCache(false);
    }

    public Class exit(EventObject event, State sinkState) {
    	if (canNotDo) {
    		return getOriginalState();
    	}
        if (event.getSource() instanceof EnterButton) {
            Cashier cashier = Cashier.queryByCashierID(cashierNumber.toString());
            if (cashier == null) {
                cashierNumber.setLength(0);
                app.getWarningIndicator().setMessage(res.getString("InventoryPersonNotFound"));
                return InventoryState.class;
            }
            return InventoryStockState.class;

        } else if (event.getSource() instanceof ClearButton) {
            if (cashierNumber.length() == 0) {
                return getOriginalState();
            } else {
                cashierNumber.setLength(0);
                return InventoryState.class;
            }
        }

        if (cashierNumber.length() == 0) {
            app.getWarningIndicator().setMessage("");
        }
        return sinkState.getClass();
    }

    public static String getCashierNumber() {
        return cashierNumber.toString();
    }

    public static void clearCashierNumber() {
        InventoryState.cashierNumber.setLength(0);
    }

    public static Class getOriginalState() {
        return originalState;
    }

    public static void setOriginalState(Class originalState) {
        InventoryState.originalState = originalState;
    }
    
	private boolean ifSingleDoingInventory() {
		boolean result = false;
//		try {
			int count = -1;
			count = Client.getInstance().countDoingInventory();
			if (count == 1 ) {	          // Server have single pandin Inventory
				app.getWarningIndicator().setMessage("");					
				result = true;
			} else if (count == 0 ) {	  // Server have not init pandin Inventory yet
				app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("NoInitializeInventory"));
				app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PleaseEnterClear"));
			} else {                     // Something wrong on tables inventoryhead 
				app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("CanNotInventory"));
				//连接错误 或 后台数据库inventoryhead表异常。
				app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PleaseEnterClear"));				
			}
//		} catch (ClientCommandException e) {
//		} finally {
			return result;    	
//		}
	}   
    
}
