package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.groovydac.Param;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.state.periodical.PeriodicalNoReadyState;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;

import org.apache.commons.lang.StringUtils;

public class PrintPluState extends State {
    private static PrintPluState printPluState = null;
    private Class<? extends State> sinkStateClass = IdleState.class;

    public static PrintPluState getInstance() {
        try {
            if (printPluState == null) {
                printPluState = new PrintPluState();
            }
        } catch (InstantiationException ex) {
        }
        return printPluState;
    }

    /**
     * Constructor
     */
    public PrintPluState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceSate) {
    	sinkStateClass = IdleState.class;
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        if ((PARAM.isPrintPaidInOut() && app.getCurrentTransaction().getDealType2().equals("4"))
        		|| app.getReturnItemState()) {
            //Bruce/20100401 雖然目前做PaidIn/PaidOut並不會走到PrintPluState。
            return;
        }
        if (!app.getTrainingMode() && PARAM.getTranPrintType().equalsIgnoreCase("step")) {
            Transaction cTransaction = app.getCurrentTransaction();
            Object[] lineItemArrayLast = cTransaction.getLineItems();

            // If lastLineItem of currentTransaction is printed, do nothing and return.
            // If lastLineItem of currentTransaction is 指定更正，then print all line items out.

            if (lineItemArrayLast.length > 1) {

                //Bruce/20100105/
                // Fix bug: 如果在最後一張發票快速的輸入超過六個商品，可能會導致發票多做了切紙和印小計的動作。
                if (PARAM.isCountInvoiceEnable() &&
                    (StringUtils.isEmpty(PARAM.getInvoiceID()) ||
                     StringUtils.isEmpty(PARAM.getInvoiceNumber()))) {
                    //Bruce/20100401 如果在正常情況下發票字軌或發票號碼不見，提醒收銀員重新輸入發票資訊
                    showWarningMessage("PleaseResetInvoiceNumber");
                    return;
                }

                CreamPrinter printer = CreamPrinter.getInstance();
                DbConnection connection = null;
                try {
                    connection = CreamToolkit.getTransactionalConnection();
	                if (!printer.getHeaderPrinted()) {
	                    if (!printer.isPrinterAtRightStart()) { // 檢查印表機是否已定位
	                        sinkStateClass = PrintPluWarningState.class;
	                        setWarningMessage(m("PrinterIsNotAtRightPosition"));
	                        return;
	                    }
	                    printer.printHeader(connection);
	                    printer.setHeaderPrinted(true);
	                }
	
	                if (!printer.isPrinterHealthy()) { // 檢查印表機是否ready
	                    sinkStateClass = PrintPluWarningState.class;
	                    setWarningMessage(m("PrinterIsNotReady"));
	                    return;
	                }

	                if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_ORDER_STATE
	                		|| CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_DRAW_STATE 
	                		|| CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_RETURN_STATE) 
	                	sinkStateClass = PeriodicalNoReadyState.class;
	                
                    for (int i = 0; i < lineItemArrayLast.length - 1; i++) { // buffer one line
                        LineItem lineItem = (LineItem)lineItemArrayLast[i];
                        if (!lineItem.getPrinted()) {
                            System.out.printf("print line item : %d", i);
                            printer.printLineItem(connection, lineItem);
                        }
                    }
                    connection.commit();
                    
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                    CreamToolkit.haltSystemOnDatabaseFatalError(e);
                } finally {
                    CreamToolkit.releaseConnection(connection);
                }

                // LineItem lineItem = (LineItem)lineItemArrayLast[lineItemArrayLast.length - 2];
                // printer.printLineItem(lineItem);
                /*
                 * (if (lineItem.getDetailCode().equals("V")) {
                 * CreamPrinter.getInstance().printLineItem(lineItem); }
                 */

                // Otherwise, print the second last LineItem of
                // currentTransaction to printer
                // if it has not been printed yet. (Ref. Receipt Layout)
                // Print小计and cut/feed paper if needed.
            } else {
                if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_ORDER_STATE
                		|| CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_DRAW_STATE 
                		|| CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_RETURN_STATE) 
                	sinkStateClass = PeriodicalNoReadyState.class;
            }
            if (!cTransaction.getBuyerNumber().equals("") && !app.getBuyerNumberPrinted()) {
                // CreamPrinter.getInstance().printBuyerNumber(cTransaction.getBuyerNumber());
                app.setBuyerNumberPrinted(true);
            }
        }
        CreamToolkit.showText(app.getCurrentTransaction(), 0);
    }

    public Class exit(EventObject event, State sinkState) {
        return sinkStateClass;
    }
}
