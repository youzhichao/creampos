package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.util.HYIEventObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

//import jpos.events.DataEvent;
//import jpos.events.StatusUpdateEvent;

public class ReplayState extends State {
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static ReplayState replayState    = null;

    public static ReplayState getInstance() {
        try {
            if (replayState == null) {
            	replayState = new ReplayState();
            }
        } catch (InstantiationException ex) {
        }
        return replayState;
    }

    /**
     * Constructor
     */
    public ReplayState() throws InstantiationException {
    }


	@Override
	public void entry(EventObject event, State sourceState) {
		app.getMessageIndicator().setMessage("Enter...");
	}

	@Override
	public Class exit(EventObject event, State sinkState) {
		StateMachine.getInstance().setRecorded(2);
		new Thread() {
			public void run() {
				try {
					Thread.currentThread().sleep(1000);
				} catch (InterruptedException e2) {
					e2.printStackTrace();
				}
				List<HYIEventObject> backups = new ArrayList<HYIEventObject>();
				
				File file = new File("record.dac");
				try {
					ObjectInputStream ois = new ObjectInputStream(new FileInputStream((file)));
					backups = (List<HYIEventObject>) ois.readObject();
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
System.out.println("--------- read : " + backups);
//				for (EventObject e : StateMachine.getInstance().getRecordList()) {
//					backups.add(e);
//				}
	 		List<? extends EventObject> events = convert(backups);
			for (EventObject e : events) {
				try {
System.out.println("------------ ReplayState : " + e);				
					StateMachine.getInstance().processEvent(e);
					Thread.currentThread().sleep(100);
				} catch (Exception e1) {}
			}
			StateMachine.getInstance().setRecorded(0);
//			backups.clear();
			}
			
		}.start();

        return sinkState.getClass();
	}

	private List<? extends EventObject> convert(List<HYIEventObject> events) {
		List es = new ArrayList();
		for (HYIEventObject event : events) {
			try {
				long sleepTimes = event.getSleepTimes();
				Thread.currentThread().sleep(sleepTimes);
				Class eventClass = event.getEventClass();
				Object source = event.getSource();
				int status = event.getStatus();
				EventObject e = null;
				if (eventClass.getName().indexOf("StatusUpdateEvent") >= 0) {
					e = (EventObject) eventClass.getConstructor(Object.class, Integer.class)
							.newInstance(source, status);
					
				} else if (eventClass.getName().indexOf("DataEvent") >= 0) {
					e = (EventObject) eventClass.getConstructor(Object.class, Integer.class)
							.newInstance(source, status);
				} else {
					e = (EventObject) eventClass.getConstructor(Object.class)
							.newInstance(source);
				}
//				es.add(e);
				System.out.println("------------ ReplayState : " + e);				
				StateMachine.getInstance().processEvent(e);
			} catch (Exception e) {
			}
		}
		return es;
	}

}
