package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.Cashier;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.Keylock;

import java.util.EventObject;

public class CashierRightsCheckState extends State {
    private String cashierName = "";
    private String oldCashierName = "";
    private String cashierPassword = "";
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private String display = "";
    private boolean flag = false;
    private boolean srcLock = false;
    static CashierRightsCheckState cashierRightsCheckState;
    private static String targetState = "";
    private static String sourceState = "";
    private String exitState = "";
    private boolean checked = false;

    // 0. 初始状态  1. 需要转到目标位置　2. 已经到达过目标位置 3. 已经从目标位置返回
    private int turnKeyState = 0;

    private boolean needTurnKeyAtOverrideAmount = false;
    private boolean onlyMemberUseOverrideAmount = false;
    private boolean needTurnKey = false;
    private int beginKeyPosition = -1;     //随便赋一个值，等需要用到的地方再赋值
    private int objectiveKeyPosition = -1; //随便赋一个值，等需要用到的地方再赋值

    public static CashierRightsCheckState getInstance() {
        try {
            if (cashierRightsCheckState == null) {
                cashierRightsCheckState = new CashierRightsCheckState();
            }
        } catch (InstantiationException ex) {
        }
        return cashierRightsCheckState;
    }

    public CashierRightsCheckState() throws InstantiationException {
        cashierName = "";
        needTurnKeyAtOverrideAmount = PARAM.isTurnKeyAtOverrideAmount();
    }

    private void initstate() {
        this.initstate1();
        turnKeyState = 0;
    }

    private void initstate1() {
        cashierName = "";
        cashierPassword = "";
        display = "";
        flag = false;
    }

    boolean canUseOverrideAmount;

    public void entry(EventObject event, State sourceState) {
        exitState = "";
//      TODO OverrideAmountState 应该用Class.getName获得和判断匹配性
        needTurnKey = needTurnKeyAtOverrideAmount && (targetState.equals("OverrideAmountState")
            || targetState.equals("YellowAmountState"));

        if (targetState.equals("OverrideAmountState")
            || targetState.equals("YellowAmountState")) {
            String memberID = POSTerminalApplication.getInstance().getCurrentTransaction().getMemberID();
            if (PARAM.isOnlyMemberUseOverrideAmount()
                && memberID != null && memberID.trim().length() > 0) {
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("IsOnlyMemberCanUseOverride"));
                return;
            }
        }

        // begin of deal with key lock
        if (needTurnKey) {
            if (turnKeyState == 0) {
                turnKeyState = 1;
                beginKeyPosition = app.getKeyPosition();
                objectiveKeyPosition = beginKeyPosition + 1; //初始位置右边一位

                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("TurnKeyTo"));
                return;
            }
        }

        if (event.getSource() instanceof Keylock) {
            Keylock k = (Keylock)event.getSource();
            try {
                int kp = k.getKeyPosition();
                if (kp == objectiveKeyPosition && turnKeyState == 1) {
                    app.setKeyPosition(kp);
                    app.getMessageIndicator().setMessage(
                        CreamToolkit.GetResource().getString("TurnKeyBack"));
                    turnKeyState = 2;
                    return;
                } else if (kp == beginKeyPosition && turnKeyState == 2) {
                    app.setKeyPosition(kp);
                    turnKeyState = 3;
                    //还原
                    beginKeyPosition = -1;
                    objectiveKeyPosition = -1;
                } else {
                    return;
                }
            } catch (JposException e) {
                e.printStackTrace();
            }
        } else {
            if (needTurnKey && turnKeyState != 0 && turnKeyState != 3)
                return;
        }

        if (sourceState instanceof LockingState)
            srcLock = true;
        // end of deal with key lock

        // begin to check cashier right
        oldCashierName = PARAM.getCashierNumber();
        if (!checked) {
            checked = true;
            if (checkRight(cashierName, oldCashierName, targetState)) {
                exitState = targetState;
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("Enter"));
                return;
            } else {
                initstate1();
            }
        }
        if (!"".equals(display.trim()))
            return;
        if (flag) {
            app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("Password"));
            app.getMessageIndicator().setMessage("");
        } else {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputCashierNumberAgain"));
            app.getWarningIndicator().setMessage("");
        }
    }

    public Class exit(EventObject event, State sinkState) {

        if (event.getSource() instanceof Keylock
            || (needTurnKey && turnKeyState != 0 && turnKeyState != 3)) { //等待转Key
            return CashierRightsCheckState.class;

        } else if (event.getSource() instanceof ClearButton) {
            if (srcLock) {
                app.getMessageIndicator().setMessage("");
                app.getWarningIndicator().setMessage("");
                checked = false;
                initstate();
                return LockingState.class;
            } else {
                app.getMessageIndicator().setMessage("");
                app.getWarningIndicator().setMessage("");
                checked = false;
                initstate();
                return returnState(sourceState);
            }

        } else if (exitState != null && !"".equals(exitState.trim())) {
            checked = false;
            initstate();
            return returnState(targetState);

        } else if (event.getSource() instanceof NumberButton) {
            app.getWarningIndicator().setMessage("");
            if (flag) {
                NumberButton nb = (NumberButton)event.getSource();
                cashierPassword += nb.getNumberLabel().trim();
                display = "";
                for (int i = 0; i < cashierPassword.length(); i++)
                    display += '*';
                app.getMessageIndicator().setMessage(display);
            } else {
                NumberButton nb = (NumberButton)event.getSource();
                cashierName += nb.getNumberLabel().trim();
                display = cashierName;
                app.getMessageIndicator().setMessage(cashierName);
            }
            return CashierRightsCheckState.class;

        } else if (event.getSource() instanceof EnterButton) {
            // 账号输入完成，开始检查
            display = "";
            if (cashierName != null && cashierName.trim() != "") {
                cashierName = cashierName.trim();
                flag = true;
                // 提示输入密码
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("Password"));
                app.getMessageIndicator().setMessage("");

                if (cashierPassword != "") {
                    app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("inquire")); // 查询中...
                    Cashier cashierDAC = null;
                    if (srcLock) {
                        cashierDAC = Cashier.CheckValidCashier(cashierName, cashierPassword);
                    } else {
                        //String returnableLevel = GetProperty.getReturnableLevel("");
                        //if ("".equals(returnableLevel.trim()))
                        //    returnableLevel = "3";
                        cashierDAC = Cashier.CheckValidCashier(cashierName, cashierPassword/*, returnableLevel*/);
                    }
                    return checkAndReturn(cashierDAC);
                }
            }
        }
        return CashierRightsCheckState.class;
    }

    /**
     * @param cashierDAC
     * @return 如果验证通过则返回目标类，否则返回自身类
     */
    private Class checkAndReturn(Cashier cashierDAC) {
        if (cashierDAC == null) {
            // 账号密码无效
            app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WrongPassORNoLevel"));
            initstate();
            return CashierRightsCheckState.class;
        } else {
            // 账号密码有效，检查权限
            checked = true;
            if (!checkRight(cashierName, oldCashierName, targetState)) {
                initstate();
                return CashierRightsCheckState.class;
            }
            checked = false;
            app.setNewCashierID(cashierName);
            initstate();
            app.getWarningIndicator().setMessage("");
            app.getMessageIndicator().setMessage("");
            flag = true;
            return returnState(targetState);
        }
    }

    public static void setSourceState(String state) {
        sourceState = state;
    }

    public static Class getSourceState() {
        try {
            return Class.forName("hyi.cream.state." + sourceState);
        } catch (Exception e) {
        }
        return null;
    }

    public static void setTargetState(String state) {
        targetState = state;
    }

    private boolean checkRight(String cashierName, String oldCashierName, String targetState) {
        boolean ret = false;
        try {
            //CreamToolkit.logMessage("CashierRightsCheckState | checkRight | targetState : " + targetState);
            String name = cashierName;
            if (cashierName == null || "".equals(cashierName))
                name = oldCashierName;
            Cashier cashier = Cashier.queryByCashierID(name);
            String cashierRights = "";
            if (cashier != null) {
                if (cashier.getFieldValue("CASRIGHTS") == null)
                    return true;
                else
                    cashierRights = (String)cashier.getFieldValue("CASRIGHTS");
            } else
                return false;

            cashierRights = cashierRights + "00000000000000000000"; //右边补20位0，防止后面出现IndexOutOfBoundException

            //CreamToolkit.logMessage("CashierRightsCheckState | checkRight | cashierRights : " + cashierRights);
            if (targetState == null || "".equals(targetState.trim()))
                ret = true;
            else if (targetState.equalsIgnoreCase("VoidReadyState")) { //前笔误打
                if (cashierRights.charAt(0) == '1')
                    ret = true;
            } else if (targetState.equalsIgnoreCase("CancelState")) { //交易取消
                if (cashierRights.charAt(1) == '1')
                    ret = true;
            } else if (targetState.equalsIgnoreCase("ReturnNumberState")
                || targetState.equalsIgnoreCase("ReturnNumber2State")) { //退货
                if (cashierRights.charAt(2) == '1')
                    ret = true;
            } else if (targetState.equalsIgnoreCase("ReturnSaleState")) { //负项销售
                if (cashierRights.charAt(2) == '1')
                    ret = true;
            } else if (targetState.equalsIgnoreCase("DrawerOpenState2")) {//开屉
                if (cashierRights.charAt(3) == '1')
                    ret = true;
            } else if (targetState.equalsIgnoreCase("CashInIdleState")) {//借零
                if (cashierRights.charAt(4) == '1')
                    ret = true;
            } else if (targetState.equalsIgnoreCase("CashOutIdleState")) {//投库
                if (cashierRights.charAt(5) == '1')
                    ret = true;
            } else if (targetState.equalsIgnoreCase("OverrideAmountState")
                || targetState.equals("YellowAmountState")) { //变价
                if (cashierRights.charAt(6) == '1')
                    ret = true;
            } else if (targetState.equalsIgnoreCase("DiscountRateState")) { //打折
                if (cashierRights.charAt(7) == '1')
                    ret = true;
            } else if (targetState.equalsIgnoreCase("DiscountAmountState")) { //打折
                if (cashierRights.charAt(7) == '1')
                    ret = true;
            } else if (targetState.equalsIgnoreCase("OverrideAmount2State")) { //低于会员最低价
                if (cashierRights.charAt(8) == '1')
                    ret = true;
            } else if (OverrideAmountState.LOWER_WHOLESALE_PRICE.equals(targetState)) {
                //变价 低于批发价格
                if (cashierRights.charAt(9) == '1')
                    ret = true;
            }
            // 期刊退货也需要权限控制,同退货
            if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_RETURN_STATE
                && cashierRights.charAt(2) == '1')
                ret = true;

        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return ret;
    }

    private Class returnState(String stateName) {
        try {
            if (OverrideAmountState.LOWER_WHOLESALE_PRICE.equals(stateName)) {
                return OverrideAmount2State.class;
            }
            return Class.forName("hyi.cream.state." + stateName);
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return null;
    }
}