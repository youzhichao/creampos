package hyi.cream.state;
import hyi.cream.POSTerminalApplication;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;

public class LockingState extends State {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static LockingState lockingState = null;
    public static LockingState getInstance() {
        try {
             if (lockingState == null) {
                lockingState = new LockingState();
             }
         } catch (InstantiationException ex) {
         }
         return lockingState;
     }

    public LockingState() throws InstantiationException {

    }

    public void entry (EventObject event, State sourceState) {
        app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("Locking"));
    }

    public Class exit(EventObject event, State sinkState) {
       System.out.println("exit Locking State");
       return null;
    }

}