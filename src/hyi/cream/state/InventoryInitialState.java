package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Inventory;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * 盘点初始化State.
 * 
 * @author Bruce
 */
public class InventoryInitialState extends State {
    private static InventoryInitialState inventoryInitiState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private String inputNumber = "";
    private Class originalState = null;
   
    public static InventoryInitialState getInstance() {
        try {
            if (inventoryInitiState == null) {
                inventoryInitiState = new InventoryInitialState();
            }
        } catch (InstantiationException e) {
        }
        return inventoryInitiState;
    }

    public InventoryInitialState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            inputNumber = inputNumber + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(inputNumber);
            return;
        } else if (sourceState.getClass().getName().indexOf("KeyLock") != -1) {
            originalState = sourceState.getClass();
        }
        
        if (inputNumber.length() == 0)
            app.getMessageIndicator().setMessage(res.getString("InventoryInitConfirm"));
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            if (inputNumber.equals("1")) { 
                DbConnection connection = null;
                try {
                    // delete history data first
                    connection = CreamToolkit.getTransactionalConnection();
                    new Inventory().deleteAll(connection);

                    // remember the last initializing date
                    SimpleDateFormat df = CreamCache.getInstance().getDateFormate();
                    String today = df.format(new java.util.Date());
                    PARAM.updateInventoryInitDate(today);
                    CreamToolkit.logMessage("Inventory data has been initialized.");
                    app.getMessageIndicator().setMessage(res.getString("InventoryInitSuccess"));

                    connection.commit();
                    CreamToolkit.sleepInSecond(2);

                } catch (SQLException e) {
                    app.getMessageIndicator().setMessage(res.getString("InventoryInitFailed"));
                    CreamToolkit.logMessage(e);
                } finally {
                    CreamToolkit.releaseConnection(connection);
                }
                inputNumber = "";
                return originalState;
            } else {
                inputNumber = "";
                return InventoryInitialState.class;
            }
        } else if (event.getSource() instanceof ClearButton) {
            inputNumber = "";
            return originalState;
        }
        
        return sinkState.getClass();
    }
}
