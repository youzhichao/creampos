package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSButtonHome2;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.dac.Cashier;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Member;
import hyi.cream.dac.PLU;
import hyi.cream.dac.Reason;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.TransactionHold;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.state.periodical.PeriodicalIdleState;
import hyi.cream.state.wholesale.WholesaleClearingState;
import hyi.cream.state.wholesale.WholesaleIdleState;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.util.ReceiptGenerator;
import hyi.spos.JposException;
import hyi.spos.Keylock;
import hyi.spos.LineDisplay;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;

public class IdleState extends State {
    private boolean indicatorCleared = false;
    private ResourceBundle res = CreamToolkit.GetResource();
    static IdleState idleState = null;
    private POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
    private boolean start = false;
    private boolean changeInvoice;
    private boolean mustCheckOut;
    private final HYIDouble HYIDOUBLE_ZERO = new HYIDouble(0);
    private HYIDouble cashdrawerAmountMaximum = PARAM.getCashdrawerAmountMaximum();

    public void setStart(boolean s) {
        start = s;
    }

    public static IdleState getInstance() {
        try {
            if (idleState == null) {
                idleState = new IdleState();
            }
        } catch (InstantiationException ex) {
        }
        return idleState;
    }

    /**
     * Constructor
     */
    public IdleState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {

        /*if (posTerminal.getTransactionEnd() &&
            CreamSession.getInstance().isSomeMastersNeedToDownload()) {
            final PopupMenuPane menu = getApp().getPopupMenuPane();
            menu.setMenu(Arrays.asList(new String[] {
                "有新的主檔需要下傳，請問您要：",
                "",
                "[1] 現在立即下傳",
                "[清除] 稍後再下傳",
                "",
                "請選擇[1]或[清除]"
            }));
            menu.setPopupMenuListener(new PopupMenuListener() {
                @Override
                public void menuItemSelected() {
                    if (0 == menu.getSelectedNumber()) {
                        try {
                            Client.getInstance().downloadMasters();
                            CreamToolkit.stopPos(1);
                        } catch (ClientCommandException e) {
                            CreamToolkit.logMessage(e);
                        }
                    }
                }
            });
            menu.setVisible(true);
            return;
        }*/

        if (PARAM.getCreateButtonPanel()) {
            posTerminal.getButtonCardLayout().show(posTerminal.getButtonPanel(),"BUTTON_PANEL1");
        }
        Transaction curTran = posTerminal.getCurrentTransaction();

        changeInvoice = false;
        mustCheckOut = false;

        if (start && posTerminal.getItemList() != null) {
            posTerminal.getItemList().setDIY(false, null);
            if (posTerminal.getTrainingMode())
                posTerminal.getItemList().setBackLabel(res.getString("TrainingMode"));
            else
                posTerminal.getItemList().setBackLabel("");
        }

        /*if (sourceState instanceof InvoiceNumberReadingState
            && ReceiptGenerator.getInstance().getPageNumber() > 1
            && posTerminal.getCurrentTransaction().getLineItemsWithoutCurrentOne().length > 1
            && StateToolkit.isInvoiceChanged()) {
            CreamPrinter.getInstance().setHeaderPrinted(false);
        }*/

        if (PARAM.isCountInvoiceEnable()) {
            int invNo = -1;
            try {
                invNo = Integer.parseInt(PARAM.getInvoiceNumber());
            } catch (Exception e) {
            }
            if (invNo == -1) {
                changeInvoice = true;
                this.showWarningMessage("LastInvoice");

                // 模拟user按InvoiceButton. 在State里面必需在开一个thread来做。
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                        }
                        POSButtonHome2.getInstance().buttonPressed(
                            new POSButtonEvent(new InvoiceButton(0, 0, 0, "")));
                    }
                }).start();

            } else {
                // 只需判断最后3位，台湾发票号是0--249 每打250张
                int countDown = 250 - (invNo % 250); // 还剩几张
                System.out.println("--------- IdleState | pages : " + (countDown) + " | " + invNo);
                if (countDown <= 5) {
                    Object[] args = { "" + (countDown) };
                    posTerminal.getWarningIndicator().setMessage(MessageFormat
                            .format(res.getString("InvoiceNumberCountDown"), args));
                }
                /*
                // 最后一张发票
                // 由于发票号必须连续，所以最后一张一定要结帐
                // 以防万一，小于最大打印单品数量
                if (countDown == 1 && ReceiptGenerator.getInstance().getLineItemsPrintedInThisPage() >= ReceiptGenerator.getInstance().getLineItemPerPage() - 1) {
                    mustCheckOut = true;
                    // 模拟user按AgeLevelButton. 在State里面必需在开一个thread来做。
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                            }
                            POSButtonHome.getInstance().buttonPressed(
                                new POSButtonEvent(new AgeLevelButton(0, 0, 0, "", "1")));
                        }
                    }).start();
                }
                */
            }
        }

        // super.entry(event, sourceState);
        if (posTerminal.getMessageIndicator() != null)
            showMessage("SelectPlu");
        if (posTerminal.getItemList() != null && posTerminal.getItemList().isDIY())
            showMessage("SelectDIYPlu");
        posTerminal.getPopupMenuPane().setEnabled(true);
        posTerminal.getPopupMenuPane().clear();
        posTerminal.setEnabledPopupMenu(true);
        posTerminal.getItemList().repaint();
        boolean checkWarning = false;
        if (posTerminal.getScanCashierNumber() && !posTerminal.getChecked()) {
            checkWarning = true;
            showMessage("CheckWarning");
            posTerminal.getSystemInfo().setSalesManNumber("");
        }

        if (posTerminal.getScanSalemanNumber()
                && !posTerminal.getSalemanChecked()) {
            checkWarning = true;
            showMessage("CheckSalemanWarning");
            // posTerminal.getSystemInfo().setSalesManNumber("");
        }

        if (!checkWarning) {
            if (posTerminal.getItemList() != null && posTerminal.getItemList().isDIY())
                showMessage("SelectDIYPlu");
            else
                showMessage("SelectPlu");
        }

        if (getCurrentTransaction().getWeiXinList().size() > 0) {
            String poid = getCurrentTransaction().getWeiXinList().get(0).getPromotion_id();
            if (poid != null && !poid.equals("")) {
                showWarningMessage(res.getString("UsedCoupons"));
            } else {
                showWarningMessage(res.getString("UnusedCoupons"));
            }
        }

        // Bruce/2003-12-24
        // 解决MixAndMatchVersion=2时，若从结账画面返回至销售画面时，会因调用Transaciton.
        // initPaymentInfo()导致LineItem的AfterDiscountAmount的值变成与Amount相同
        if (sourceState instanceof SummaryState)
            curTran.restoreOriginalLineItemAfterDiscountAmount();

        // if (sourceState instanceof GetSalemanNumberState) {
        // start = true;
        // }

        if (sourceState instanceof CheckCashierPasswordState) {
            start = true;
            posTerminal.getCurrentTransaction().setDealType2("0");
        }
        if (start) {
            ((MixAndMatchState) StateMachine.getInstance().getStateInstance(
                    "hyi.cream.state.MixAndMatchState")).clearCache();
//            ((SelectGiftState) StateMachine.getInstance().getStateInstance(
//                    "hyi.cream.state.SelectGiftState")).initSelectGiftState();
            CreamCache.getInstance().clearCache();
//            if (GetProperty.getCreateAdPanel("no").equalsIgnoreCase("yes"))
//                posTerminal.setAnimatedVisible(false, null);
        }

        if (posTerminal.getBeginState())
            CreamToolkit.showTurnKeyToOffPositionMessage();

//        if (sourceState instanceof TransactionHoldShowState) {
//            if (event.getSource() instanceof EnterButton)
//                curTran = ((TransactionHoldShowState) sourceState)
//                        .getHoldTran();
//        } else if (sourceState instanceof TransactionUnHoldState)
//            curTran = posTerminal.getCurrentTransaction();

        if (cashdrawerAmountMaximum.compareTo(HYIDOUBLE_ZERO) > 0) {
            // > 0, 表示需要检查
            //String msg = " "; // 给""会有问题, 所以给空格" "
            String msg = ""; // 现在给""也没有问题了
            System.out.println("posTerminal.getSystemInfo().getShiftTotalCashAmount() = " + posTerminal.getSystemInfo().getShiftTotalCashAmount());
            System.out.println("IdleState.cashdrawerAmountMaximum = " + cashdrawerAmountMaximum);
            if (posTerminal.getSystemInfo().getShiftTotalCashAmount().compareTo(
                    cashdrawerAmountMaximum) > 0) {
                msg = res.getString("CashdrawerAmountIsFull");
            }
            // TODO ? 是否会覆盖掉别的Information, 或者被其他Information覆盖
            posTerminal.getInformationIndicator().setMessage(msg);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        // super.exit(event, sinkState);
        // String str = String.valueOf(Transaction.getNextTransactionNumber());
        // POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        // check restart transaction
        posTerminal.setEnabledPopupMenu(false);
        posTerminal.getWarningIndicator().setMessage("");

        if (start) {
            Transaction.generateABrandNewTransaction();
            start = false;
            try {
                LineDisplay lineDisplay = POSPeripheralHome3.getInstance().getLineDisplay();
                lineDisplay.clearText();
                String str = PARAM.getWelcomeMessage();
                if (!str.trim().equals(""))
                    lineDisplay.displayText(str, 1);
            } catch (Exception e) {
                CreamToolkit.logMessage(e);
            }

            if (event.getSource() instanceof Keylock) {
                if (posTerminal.getScanSalemanNumber()
                        && !posTerminal.getSalemanChecked()) {
                    return SalemanState.class;
                }
            }
        }

        Transaction curTran = posTerminal.getCurrentTransaction();

        if (changeInvoice) {
            // posTerminal.getCurrentTransaction().Clear();
            // posTerminal.getCurrentTransaction().setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
            // posTerminal.getCurrentTransaction().setTransactionNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber")));
//            posTerminal.getPayingPane().setVisible(false);
//            posTerminal.getItemList().setItemIndex(0);
//            posTerminal.getItemList().repaint();
//            posTerminal.getItemList().setVisible(true);
//            return InvoiceNumberReadingState.class;
//            return WarningState.class;
            changeInvoice = false;
        }

        if (mustCheckOut) {
            posTerminal.getWarningIndicator().setMessage(res.getString("LastInvoiceMustCheckOut"));
            mustCheckOut = false;
        }

//        if (posTerminal.isChangeInvoiceMode()) {
//            posTerminal.getCurrentTransaction().Clear();
//            // posTerminal.getCurrentTransaction().setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
//            // posTerminal.getCurrentTransaction().setTransactionNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber")));
//            posTerminal.getPayingPane().setVisible(false);
//            posTerminal.getItemList().itemIndex = 0;
//            posTerminal.getItemList().repaint();
//            posTerminal.getItemList().setVisible(true);
//            return InvoiceNumberReadingState.class;
//        }

        if (event.getSource() instanceof DailyReportButton) {
            if (!posTerminal.getTransactionEnd())
                return IdleState.class;
            else
                return DailyReportDateState.class;
        }

        if (event.getSource() instanceof MemberIDButton) {
            // 若已经在交易输入过程中，不允许输入会员编号，发出警讯
            // if (!posTerminal.getTransactionEnd()) {
            // Bruce/2003-06-23 若画面上看不到任何商品（例如可能被更正掉），仍允许输入会员编号。
            if (curTran.getDisplayedLineItemsArray().size() != 0
                && !PARAM.isInputMemberCardIDInTheMiddleOfTransaction()) {
                setWarningMessage(res.getString("CannotInputMemberID"));
                return WarningState.class;
            } else {
                return MemberState.class;
            }
        }

        if (event.getSource() instanceof ECommerceButton) {
            // 若已经在交易输入过程中，不允许输入电商订单，发出警讯
            if (curTran.getDisplayedLineItemsArray().size() != 0) {
                setWarningMessage(res.getString("NoTradingAllowedECommerceOrder"));
                return WarningState.class;
            } else {
                return ECommerceOrdersState.class;
            }
        }

        if (event.getSource() instanceof ShopButton) {
            if (curTran.getDisplayedLineItemsArray().size() != 0) {
                setWarningMessage(res.getString("CannotUseShopButton"));
                return WarningState.class;
            } else {
                return ShopCategorySelectState.class;
            }
        }

        if (event.getSource() instanceof TransactionTypeButton) {
            if (!posTerminal.getTransactionEnd()) {
                // 若已经在交易输入过程中，不允许切換交易类型
                // // Bruce/2003-06-23 若画面上看不到任何商品（例如可能被更正掉），仍允许输入切換交易类型
                // if (curTran.getDisplayedLineItemsArray().size() != 0) {
                setWarningMessage(res.getString("MidwayCannotChangeTranType"));
                return WarningState.class;
            } else {
                if ("0".equals(curTran.getTranType1())) {
                    Member m = GetMemberNumberState.getMember();
                    if (m != null && "3".equals(m.getMemberType())){
                        // 只有“批发”会员才能用“批发”交易类型
                        curTran.setTranType1("1");
                    } else {
                        setWarningMessage(res
                                .getString("WholesaleTranTypeOnlyForWholesaleMember"));
                        return WarningState.class;
                    }
                } else {
                    //} else if ("1".equals(curTran.getTranType1())) {
                    curTran.setTranType1("0");
                }
                sinkState = IdleState.this;
                return IdleState.class;
            }
        }


        if (event.getSource() instanceof QingKongButton) {
            posTerminal.getCurrentTransaction().clearLineItem();
            return InitialState.class;
        }

        if (event.getSource() instanceof ReprintButton) {
            if (posTerminal.getCurrentTransaction().getTransactionNumber()
                    .intValue() != Transaction.getNextTransactionNumber())
                return ReprintState.class;
            else if (posTerminal.getCurrentTransaction()
                    .getLineItems().length > 0) {
                this.setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            } else
                return ReprintState.class;
        }

        if (!indicatorCleared) {
            if (!posTerminal.getMessageIndicator().getMessage().equals("")) {
                posTerminal.getMessageIndicator().setMessage("");
            }
            indicatorCleared = true;
        }

        // check BuyerNoButton
        if (event.getSource() instanceof BuyerNoButton) {
            return BuyerState.class;
//            if (posTerminal.getBuyerNumberPrinted() == false) {
//                return BuyerState.class;
//            } else {
//                setWarningMessage(res.getString("ErrorOfBuyerNo1"));
//                return WarningState.class;
//            }
        }

        // check InvoiceNoButton
        if (event.getSource() instanceof InvoiceNoButton) {
            if (curTran.getCurrentLineItem() == null) {
                return InvoiceState.class;
            } else {
                return WarningState.class;
            }
        }

        // check AgeLevelButton
        if (event.getSource() instanceof AgeLevelButton) {
            if (curTran == null)
                return IdleState.class;
            else if (curTran.getLineItems().length == 0) {
                return IdleState.class;
            } else {
                Object[] li = curTran.getLineItems();
                LineItem line = null;
                boolean available = true;
                for (int i = 0; i < li.length; i++) {
                    line = (LineItem) li[i];
                    if (!line.getRemoved()) {
                        available = true;
                        break;
                    } else {
                        available = false;
                    }
                }
                if (!available) {
                    posTerminal.getWarningIndicator().setMessage(
                            res.getString("NoAvailablePlu"));
                    return IdleState.class;
                }
            }

//            SelectGiftState selectGiftState = ((SelectGiftState)StateMachine
//                .getInstance().getStateInstance("hyi.cream.state.SelectGiftState"));
//            if (selectGiftState.isPermit())
//                return SelectGiftState.class;

            if (!posTerminal.getTrainingMode()
                    && !(!PARAM.isPrintPaidInOut() && posTerminal.getCurrentTransaction().getDealType2().equals("4"))
                    && !posTerminal.getReturnItemState()
                    && PARAM.getTranPrintType().equalsIgnoreCase("step")) {

                CreamPrinter printer = CreamPrinter.getInstance();
                DbConnection connection = null;
                try {
//                    connection = CreamToolkit.getTransactionalConnection();
                    connection = CreamToolkit.getPooledConnection();
                    if (!printer.getHeaderPrinted()) {
                        if (!printer.isPrinterAtRightStart()) { // 檢查印表機是否已定位
                            setWarningMessage(CreamToolkit.GetResource().getString("PrinterIsNotAtRightPosition"));
                            PrintPluWarningState.setExitState(IdleState.class);
                            return PrintPluWarningState.class;
                        }
                        printer.printHeader(connection);
                        printer.setHeaderPrinted(true);
                    }

                    if (!printer.isPrinterHealthy()) { // 檢查印表機是否ready
                        setWarningMessage(CreamToolkit.GetResource().getString("PrinterIsNotReady"));
                        PrintPluWarningState.setExitState(IdleState.class);
                        return PrintPluWarningState.class;
                    }

                    for (Object lineItemObj : curTran.getLineItems()) {
                        LineItem lineItem = (LineItem)lineItemObj;
                        if (!lineItem.getPrinted())
                            printer.printLineItem(connection, lineItem);
                    }
//                    connection.commit();
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                    CreamToolkit.haltSystemOnDatabaseFatalError(e);
                    return IdleState.class;
                } finally {
                    CreamToolkit.releaseConnection(connection);
                }
            }
            return SummaryState.class;
        }

        // check DaiFuButton
        if (event.getSource() instanceof DaiFuButton) {
            if (curTran.getCurrentLineItem() == null) {
                return DaiFuIdleState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        // check DaiShouButton2
        if (event.getSource() instanceof DaiShouButton2) {
            if (PARAM.isDaiShou2MixSales()){
                return DaiShou2State.class;
            } else if (curTran.isPureDaiShou2()){
                return DaiShou2State.class;
            } else {
                setWarningMessage(res.getString("DaiShou2MixSalesNotAllow"));
                return WarningState.class;
////            } else {
//                ArrayList displayedLineItems = curTran.getDisplayedLineItemsArray();
//                if (displayedLineItems.size() == 0) {
//                    return DaiShou2State.class;
//                } else {
//                    for (Object o : displayedLineItems){
//                        LineItem li = (LineItem)o;
//                        if (!"O".equals(li.getDetailCode())){
//                            setWarningMessage(res.getString("NotAllowDaiShou2MixSales"));
//                            return WarningState.class;
//                        }
//                    }
//                    return DaiShou2State.class;
//                }
            }
        }

        // check PaidOutButton
        if (event.getSource() instanceof PaidOutButton) {
            Iterator paidOutItem = Reason.queryByreasonCategory("11");
            if (paidOutItem == null)
                return IdleState.class;

            if (curTran.getCurrentLineItem() == null) {
                return PaidOutIdleState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        // check PaidInButton
        if (event.getSource() instanceof PaidInButton) {
            Iterator paidInItem = Reason.queryByreasonCategory("10");
            if (paidInItem == null)
                return IdleState.class;
            if (curTran.getCurrentLineItem() == null) {
                return PaidInIdleState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        // check TransactionVoidButton 前笔误打
        if (event.getSource() instanceof TransactionVoidButton) {
            CreamToolkit.logMessage("IdleState | you enter TransactionVoidButton");
            if (curTran.getCurrentLineItem() == null) {

                DbConnection connection = null;
                try {
                    connection = CreamToolkit.getPooledConnection();
                    Transaction oldTran = Transaction.queryByTransactionNumber(connection,
                        PARAM.getTerminalNumber(), Transaction.getNextTransactionNumber() - 1);
                    
                    if (oldTran.isPeiDa()) {
                        setWarningMessage(res.getString("PeiDaError10"));
                        return WarningState.class;
                    }
                    
                    // Bruce/20030324
                    Collection fds = Cashier.getExistedFieldList("cashier");
    
//                    if (!oldTran.getDealType3().equals("1")
//                            && !oldTran.getDealType3().equals("2")
//                            && !oldTran.getDealType3().equals("3")
//                            && !oldTran.getDealType2().equals("3")
//                            && !oldTran.getDealType1().equals("*")) {
                    if (oldTran == null
                            || (!oldTran.getDealType1().equals("0")
                                    || !oldTran.getDealType2().equals("0")
                                    || (!oldTran.getDealType3().equals("0")
                                            && !oldTran.getDealType3().equals("4")))
                            || ("J".equals(oldTran.getState1())  //期刊
                            || "K".equals(oldTran.getState1())//期刊
                            || "L".equals(oldTran.getState1())//期刊
                            || "T".equals(oldTran.getState1())//团购销售
                            || "Q".equals(oldTran.getState1())//团购结算
                            || "D".equals(oldTran.getState1())//代配领取
                            || "B".equals(oldTran.getState1())//代配退回
                            || "P".equals(oldTran.getState1()))//打印
                    )
                    {

                        setWarningMessage(res.getString("VoidWarning"));
                        return WarningState.class;
                    } else {
                        curTran.setDealType3("1");

                        if (!fds.contains("CASRIGHTS"))
                            return VoidReadyState.class;
    
                        // modify by lxf 2003.02.13
                        CreamToolkit.logMessage("IdleState | VoidReadyState | start ...");
                        CashierRightsCheckState.setSourceState("InitialState");
                        CashierRightsCheckState.setTargetState("VoidReadyState");
    
                        return CashierRightsCheckState.class;
                        // return VoidReadyState.class;
                    }
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                    CreamToolkit.haltSystemOnDatabaseFatalError(e);
                    return WarningState.class;
                } finally {
                    CreamToolkit.releaseConnection(connection);
                }

            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        // check CancelButton 交易取消
        if (event.getSource() instanceof CancelButton) {
            CreamToolkit.logMessage("IdleState | you enter CancelButton");

            // Bruce/20030324
            Collection fds = Cashier.getExistedFieldList("cashier");

            if (curTran.getCurrentLineItem() == null) {
                setWarningMessage(res.getString("InputWrong"));
                return WarningState.class;
            } else if (!fds.contains("CASRIGHTS")) { // 不存在CASRIGHTS就直接让他做
                return CancelState.class;
            } else {
                // modify by lxf 2003.02.13
                CreamToolkit.logMessage("IdleState | CancelState | start ...");
                CashierRightsCheckState.setSourceState("IdleState");
                CashierRightsCheckState.setTargetState("CancelState");
                return CashierRightsCheckState.class;
                // return CancelState.class;
            }
        }

        // check CheckInOutButton
        if (event.getSource() instanceof CheckInOutButton) {
            if (curTran.getCurrentLineItem() == null
                    && posTerminal.getTransactionEnd()) {
                // return InOutSelectState.class;
                return KeyLock1State.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        // check RemoveButton
        if (event.getSource() instanceof RemoveButton) {
            if (curTran.getCurrentLineItem() == null
                    || curTran.getCurrentLineItem().getPrinted()
                    || curTran.getCurrentLineItem().getRemoved()) {
                this.setWarningMessage(res.getString("NoVoidPlu"));
                return WarningState.class;
            } else {

                // Bruce/20030428
                // 在配达交易中不允许做更正（因为灿坤的总部配达系统无法处理trandetail中有负项的东东）
                String type = posTerminal.getCurrentTransaction()
                        .getAnnotatedType();
                if (type != null && type.equals("P")) {
                    setWarningMessage(res.getString("CannotRemovePluInPeiDa"));
                    return WarningState.class;
                }
                if (curTran.getEcDeliveryHeadList().size() > 0) {
                    setWarningMessage(res.getString("ECommerceOrdersAreNotAllowedToUpdateGoods"));
                    return WarningState.class;
                }
                if (!PARAM.getImmediateRemove()) {
                    setWarningMessage(res.getString("NotImmediateRemove"));
                    return WarningState.class;
                }
                return VoidState.class;
            }
        }

        if (event.getSource() instanceof ClearButton) {
            if (posTerminal.getReturnItemState()
                    && posTerminal.getTransactionEnd()) {
                posTerminal.setBuyerNumberPrinted(false);
                CreamPrinter.getInstance().setHeaderPrinted(false);
                posTerminal.getCurrentTransaction().clearLineItem();
                posTerminal.getCurrentTransaction().clear();
                posTerminal.getPayingPane().setVisible(false);
                posTerminal.getItemList().setItemIndex(0);
                posTerminal.getItemList().repaint();
                posTerminal.getItemList().setVisible(true);
                posTerminal.setTransactionEnd(true);
                posTerminal.setReturnItemState(false);
                posTerminal.getItemList().setBackLabel(null);
                return CashierRightsCheckState.getSourceState();
            } else if (posTerminal.getTransactionEnd() && curTran.getMemberID() != null) {
                // clear all info about member
                curTran.setMemberID(null);
                curTran.setMemberEndDate(null);
                curTran.setBeforeTotalRebateAmt(new HYIDouble(0));
                // will be used by ShowMemberInfoState
                GetMemberNumberState.setMember(null);
            } else if (posTerminal.getItemList().isDIY()) {
                posTerminal.getItemList().setDIY(false, "0");
                posTerminal.getItemList().setBackLabel("");
            }
            return IdleState.class;
        }

        // Bruce/20030324
        // check OverrideAmountButton
        if (event.getSource() instanceof OverrideAmountButton) {
            LineItem lineItem = curTran.getCurrentLineItem();
            if (lineItem != null && !lineItem.getPrinted()
                    && !lineItem.getRemoved() && !lineItem.getPrinted()) {
                PLU plu = PLU.queryBarCode(lineItem.getPluNumber());

                if ((plu != null && plu.canOverrideAmount())
                        || PARAM.getCanOverrideAmount()
                        || (PARAM.isOnlyMemberCanUseOverrideAmount()
                                && (curTran.getMemberID() != null))) {

                    // modify by lxf 2003.04.09
                    CashierRightsCheckState.setSourceState("IdleState");
                    if (GetProperty.getOverrideVersion("1").equals("2"))
                        CashierRightsCheckState
                                .setTargetState("YellowAmountState");
                    else
                        CashierRightsCheckState
                                .setTargetState("OverrideAmountState");

                    return CashierRightsCheckState.class;
                } else {
                    posTerminal.getMessageIndicator().setMessage(res.getString("CannotOverrideAmount"));
                    return WarningState.class;
                }
            } else {
                posTerminal.getMessageIndicator().setMessage(res.getString("CannotOverrideAmount"));
                return WarningState.class;
            }
        }

        // hyi.cream.uibeans.WeiXiuButton
        if (event.getSource() instanceof WeiXiuButton) {
            // Bruce/20030428
            // 按过配达键后，就不允许再按维修键
            String type = posTerminal.getCurrentTransaction()
                    .getAnnotatedType();
            if (type != null && type.equals("P")) {
                setWarningMessage(res.getString("CannotUseWeiXiuInPeiDa"));
                return WarningState.class;
            }

            return WeiXiuState.class;
        }

        // 訂金/着付手付金, 訂金尾款/着付殘金 
        if (event.getSource() instanceof DownPaymentButton
            || event.getSource() instanceof BackPaymentRefundButton
            || event.getSource() instanceof BackPaymentButton) { // (DownPayment: ID2PD)
            if (curTran.getDisplayedLineItemsArray().size() != 0) {
                setWarningMessage(res.getString("PeiDaMessage"));
                return WarningState.class;
            }
            return PeiDa3IdleState.class;
        }

        // hyi.cream.uibeans.PeiDaButton
        if (event.getSource() instanceof PeiDaButton) {
            // Bruce/20030428
            // 按过维修键后，就不允许再按配达键
            String type = posTerminal.getCurrentTransaction()
                    .getAnnotatedType();
            if (type != null && type.equalsIgnoreCase("W")) {
                setWarningMessage(res.getString("PeiDaMessage"));
                return WarningState.class;
            }
            String peiDaVersion = PARAM.getPeiDaVersion();
            if (peiDaVersion.equals("1")) // 配达单号自动生成
                return PeiDaState.class;
            else if (peiDaVersion.equals("2")) // 配达单号手动录入
                return PeiDa2State.class;
            else if (peiDaVersion.equals("3")) // 配达明细从sc获得,配达单号扫描获得
            {
                if (curTran.getDisplayedLineItemsArray().size() != 0) {
                    setWarningMessage(res.getString("PeiDaMessage"));
                    return WarningState.class;
                }
                return PeiDa3IdleState.class;
            }
        }

        // 按下折扣率
        if (event.getSource() instanceof DiscountRate2Button) {
            LineItem lineItem = curTran.getCurrentLineItem();
            if (lineItem != null && !lineItem.getPrinted()
                    && !lineItem.getRemoved()
                    && lineItem.getDiscountType() != ("D")) {
                PLU plu = PLU.queryBarCode(lineItem.getPluNumber());
                if (plu != null && plu.canDiscount()) {

                    CreamToolkit
                            .logMessage("IdleState | DiscountRateState | start ...");
                    CashierRightsCheckState.setSourceState("IdleState");
                    CashierRightsCheckState.setTargetState("DiscountRateState");

                    return CashierRightsCheckState.class;
                } else {
                    return WarningState.class;
                }
            } else {
                return WarningState.class;
            }
        }

        // 按下折扣金额键
        if (event.getSource() instanceof DiscountAmountButton) {
            LineItem lineItem = curTran.getCurrentLineItem();
            if (lineItem != null && !lineItem.getPrinted()
                    && !lineItem.getRemoved()
                    && lineItem.getDiscountType() != ("D")
            ) {
                PLU plu = PLU.queryBarCode(lineItem.getPluNumber());
                if (plu != null && plu.canDiscount()) {

                    CreamToolkit
                            .logMessage("IdleState | DiscountAmountState | start ...");
                    CashierRightsCheckState.setSourceState("IdleState");
                    CashierRightsCheckState
                            .setTargetState("DiscountAmountState");

                    return CashierRightsCheckState.class;
                } else {
                    return WarningState.class;
                }
            } else {
                return WarningState.class;
            }
        }

        // check DaiShouButton3
        if (event.getSource() instanceof DaiShouButton3) {
            if (curTran.getCurrentLineItem() == null) {
                return DaiShou3IdleState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        if (event.getSource() instanceof DownPaymentRefundButton || event.getSource() instanceof BackPaymentRefundButton) {
            CreamToolkit.logMessage("IdleState | you enter DownPaymentRefundButton or BackPaymentRefundButton");
            if (curTran.getCurrentLineItem() == null) {
                CashierRightsCheckState.setSourceState("InitialState");
                CashierRightsCheckState.setTargetState("ReturnNumber2State");

                ReturnNumber2State.getInstance().setRefundType(
                    //event.getSource() instanceof DownPaymentRefundButton ?
                        ReturnNumber2State.RefundType.DownPayment);
                    //: ReturnNumber2State.RefundType.BackPayment

                return CashierRightsCheckState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        if (event.getSource() instanceof ReturnButton) {
            CreamToolkit.logMessage("IdleState | you enter ReturnButton");
            if (curTran.getCurrentLineItem() == null) {
                Collection fds = Cashier.getExistedFieldList("cashier");
                //if (!fds.contains("CASRIGHTS"))
                //    return VoidReadyState.class;

                // modify by lxf 2003.02.13
                CreamToolkit.logMessage("IdleState | ReturnState | start ...");
                CashierRightsCheckState.setSourceState("InitialState");
                if (PARAM.getReturnVersion().equals("1"))
                    CashierRightsCheckState.setTargetState("ReturnNumberState");
                else {
                    CashierRightsCheckState.setTargetState("ReturnNumber2State");
                    ReturnNumber2State.getInstance().setRefundType(ReturnNumber2State.RefundType.Normal);
                }
                return CashierRightsCheckState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        if (event.getSource() instanceof ReturnSaleButton) {
            CreamToolkit.logMessage("IdleState | you enter ReturnSaleButton");
            if (curTran.getCurrentLineItem() == null) {
                Collection fds = Cashier.getExistedFieldList("cashier");
                if (!fds.contains("CASRIGHTS"))
                    return ReturnSaleState.class;

                // modify by lxf 2003.02.13
                CreamToolkit.logMessage("IdleState | ReturnSaleState | start ...");
                CashierRightsCheckState.setSourceState("InitialState");
                CashierRightsCheckState.setTargetState("ReturnSaleState");
                return CashierRightsCheckState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        if (event.getSource() instanceof CashInButton) {
            CreamToolkit.logMessage("IdleState | you enter CashInButton");
            if (curTran.getCurrentLineItem() == null) {
                Collection fds = Cashier.getExistedFieldList("cashier");
                if (!fds.contains("CASRIGHTS"))
                    return VoidReadyState.class;

                // modify by lxf 2003.02.13
                CreamToolkit.logMessage("IdleState | CashInIdleState | start ...");
                CashierRightsCheckState.setSourceState("InitialState");
                CashierRightsCheckState.setTargetState("CashInIdleState");
                return CashierRightsCheckState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        if (event.getSource() instanceof CashOutButton) {
            CreamToolkit.logMessage("IdleState | you enter CashOutButton");
            if (curTran.getCurrentLineItem() == null) {
                Collection fds = Cashier.getExistedFieldList("cashier");
                if (!fds.contains("CASRIGHTS"))
                    return VoidReadyState.class;

                // modify by lxf 2003.02.13
                CreamToolkit.logMessage("IdleState | CashOutIdleState | start ...");
                CashierRightsCheckState.setSourceState("InitialState");
                CashierRightsCheckState.setTargetState("CashOutIdleState");
                return CashierRightsCheckState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        if (event.getSource() instanceof DrawerOpenButton) {
            CreamToolkit.logMessage("IdleState | you enter DrawerOpenButton");
            if (curTran.getCurrentLineItem() == null) {
                Collection fds = Cashier.getExistedFieldList("cashier");
                if (!fds.contains("CASRIGHTS"))
                    return VoidReadyState.class;

                // modify by lxf 2003.02.13
                CreamToolkit.logMessage("IdleState | DrawerOpenState2 | start ...");
                CashierRightsCheckState.setSourceState("InitialState");
                CashierRightsCheckState.setTargetState("DrawerOpenState2");
                return CashierRightsCheckState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }


        if (event.getSource() instanceof TrainingButton) {
            CreamToolkit.logMessage("IdleState | you enter TrainingButton");
            if (curTran.getCurrentLineItem() == null) {
                posTerminal.setTrainingMode(!posTerminal.getTrainingMode());
                return IdleState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return WarningState.class;
            }
        }

        if (event.getSource() instanceof DIYOverrideButton) {
            if (posTerminal.getItemList().isDIY())
                return DIYOverrideAmountState.class;
            else
                return IdleState.class;
        }

        if (event.getSource() instanceof InvoiceButton) {
            return InvoiceNumberReadingState.class;
        }

        if (event.getSource() instanceof TransactionHoldButton) {
            if (posTerminal.getTrainingMode()) {
                posTerminal.getMessageIndicator().setMessage(
                        res.getString("HoldTransactionsWarning"));
                return WarningState.class;
            }
            if (!curTran.getDealType2().equals("0")) {
                posTerminal.getMessageIndicator().setMessage(
                        res.getString("HoldTransactionsWarning2"));
                return WarningState.class;
            }
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getPooledConnection();

                // 如果没有输入任何单品应该是不用挂单.
                boolean canHold = false;
                for (int i = 0; i < curTran.getLineItems().length; i++) {
                    LineItem li = (LineItem) curTran.getLineItems()[i];
                    if (!li.getRemoved()) {
                        canHold = true;
                        break;
                    }
                }
                if (!canHold) {
                    Iterator iter = TransactionHold.queryByPosNumber(connection, PARAM.getTerminalNumber());
                    if (iter != null && iter.hasNext())
                        return TransactionUnHoldState.class;
    
                    posTerminal.getMessageIndicator().setMessage(
                        res.getString("CanNotHoldTransaction"));
                    return WarningState.class;
                }
                return TransactionHoldState.class;
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                CreamToolkit.haltSystemOnDatabaseFatalError(e);
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
        }

        if (event.getSource() instanceof PeriodicalButton) {
            // 若已经在交易输入过程中，不允许使用期刊键，发出警讯
            // if (!posTerminal.getTransactionEnd()) {
            // Bruce/2003-06-23 若画面上看不到任何商品（例如可能被更正掉），仍允许使用
            if (curTran.getDisplayedLineItemsArray().size() != 0) {
                setWarningMessage(res.getString("CannotUsePerioicalButton"));
                return WarningState.class;
            } else {
                return PeriodicalIdleState.class;
            }
        }

        if (event.getSource() instanceof WholesaleButton) {
            // 若已经在交易输入过程中，不允许使用团购键，发出警讯
            // if (!posTerminal.getTransactionEnd()) {
            // Bruce/2003-06-23 若画面上看不到任何商品（例如可能被更正掉），仍允许使用
            if (curTran.getDisplayedLineItemsArray().size() != 0) {
                setWarningMessage(res.getString("CannotUseWholesaleButton"));
                return WarningState.class;
            } else {
                return WholesaleIdleState.class;
            }
        }

        if (event.getSource() instanceof WholesaleClearingButton) {
            // 若已经在交易输入过程中，不允许使用团购结算键，发出警讯
            // if (!posTerminal.getTransactionEnd()) {
            // Bruce/2003-06-23 若画面上看不到任何商品（例如可能被更正掉），仍允许使用
            if (curTran.getDisplayedLineItemsArray().size() != 0) {
                setWarningMessage(res.getString("CannotUseWholesaleClearingButton"));
                return WarningState.class;
            } else {
                CreamSession.getInstance().setWorkingState(WorkingStateEnum.WHOLESALECLEARING_STATE);
                return WholesaleClearingState.class;
            }
        }

        // check keylock
        if (event.getSource() instanceof Keylock) {
            Keylock k = (Keylock) event.getSource();
            try {
                int kp = k.getKeyPosition();
                posTerminal.setKeyPosition(kp);
                if (CreamToolkit.getSinkStateFromKeyLockCode(kp) != null) {
                    if (CreamToolkit.getSinkStateFromKeyLockCode(kp)
                            .equals(InitialState.class)) {
                        return IdleState.class;
                    } else {
                        return CreamToolkit.getSinkStateFromKeyLockCode(kp);
                    }
                } else {
                    return IdleState.class;
                }
            } catch (JposException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            /*
             * Keylock k = (Keylock)event.getSource(); try { int kp =
             * k.getKeyPosition(); int ckp = posTerminal.getKeyPosition(); if
             * (kp == 3) { posTerminal.setKeyPosition(kp); return
             * KeyLock2State.class; } else if (kp == 6) {
             * posTerminal.setKeyPosition(kp); return ConfigState.class; } else
             * if (kp == 2) { posTerminal.setKeyPosition(kp); return
             * IdleState.class; } else if (kp == 4) {
             * posTerminal.setKeyPosition(kp); return KeyLock1State.class; } }
             * catch (JposException e) { System.out.println(e); }
             */
        }

        /*
         * if (event.getSource() instanceof PageUpButton || event.getSource()
         * instanceof PageDownButton) { return IdleState.class; }
         */

        // null MSR
//        if (sinkState == null && event.getSource() instanceof MSR) {
//            // Bruce/20030324
//            // Because no use now.
//            // Object eventSource = event.getSource();
//            // String cardNumber = null;
//            // String creditCardType = null;
//            // try {
//            // switch (((MSR)eventSource).getTracksToRead()) {
//            // case 1:
//            // cardNumber = new String(((MSR)eventSource).getTrack1Data());
//            // break;
//            // case 2:
//            // cardNumber = new String(((MSR)eventSource).getTrack2Data());
//            // break;
//            // case 3:
//            // cardNumber = new String(((MSR)eventSource).getTrack3Data());
//            // break;
//            // }
//            // } catch (JposException e) {
//            // e.printStackTrace(CreamToolkit.getLogger());
//            // }
//            // int cardNo = Integer.parseInt(cardNumber.substring(0, 4));
//            // if (cardNo >= 0100 && cardNo <= 3199) {
//            // creditCardType = "01";
//            // } else if (cardNo == 4563 || cardNo == 4579) {
//            // creditCardType = "02";
//            // } else if (cardNo >= 4000 && cardNo <= 4999) {
//            // creditCardType = "03";
//            // } else if (cardNo >= 5000 && cardNo <= 5999) {
//            // creditCardType = "05";
//            // } else if ((cardNo >= 3400 && cardNo <= 3499)
//            // || cardNo >= 3700 && cardNo <= 3799) {
//            // creditCardType = "06";
//            // } else if (cardNo == 1800 || cardNo == 2131
//            // || (cardNo >= 3528 && cardNo <= 3589)) {
//            // creditCardType = "08";
//            // } else if (cardNo == 1040 || cardNo == 2030
//            // || (cardNo >= 3000 && cardNo <= 3099)
//            // || (cardNo >= 3600 && cardNo <= 3699)
//            // || (cardNo >= 3800 && cardNo <= 3899)) {
//            // creditCardType = "07";
//            // } else if (cardNo >= 0 || cardNo <= 9) {
//            // creditCardType = "99";
//            // }
//        }

        // KeylockWarningState Keylock
        if (sinkState != null) {
            return sinkState.getClass();
        } else {
            return null;
        }
    }

//    public void setCurrentTran(Transaction tran) {
//        this.curTran = tran;
//    }
}
