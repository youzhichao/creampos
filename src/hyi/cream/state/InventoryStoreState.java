package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Inventory;
import hyi.cream.uibeans.Indicator;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * 盘点记录储存和打印State.
 */
public class InventoryStoreState extends State {
    private static InventoryStoreState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Indicator warningIndicator = app.getWarningIndicator();
    private ResourceBundle res = CreamToolkit.GetResource();

    public static InventoryStoreState getInstance() {
        try {
            if (instance == null) {
                instance = new InventoryStoreState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public InventoryStoreState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        DbConnection connection = null;
        Inventory inv = InventoryIdleState.getCurrentInventory();
        try {
            connection = CreamToolkit.getTransactionalConnection();
            inv.insert(connection, false);
    
            // 打印盘点记录
            CreamPrinter.getInstance().printInventoryItem(inv);
    
            //Bruce/2003-12-02
            //将显示会用到的DAC fields写到class fields，并将原DAC中的field map丢弃，
            //以节约memory overhead
            inv.discardFieldMap();
            
            connection.commit();
        } catch (SQLException e) {
            warningIndicator.setMessage(res.getString("InventoryDataStoreFailed"));
            CreamToolkit.logMessage("Err> Store inventory record failed! " + inv.toString());
            CreamToolkit.logMessage(e);
            // CreamToolkit.haltSystemOnDatabaseFatalError();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        InventoryIdleState.clearItemNumber();
        InventoryQuantityState.clearQuantity();
        InventoryOpenPriceState.clearPrice();
        return sinkState.getClass();
    }
}
