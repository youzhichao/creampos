package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.Store;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;

/**
 * PeiDaState class.
 */
public class PeiDaState extends State {

	private String annotatedid          = "";
	private POSTerminalApplication app  = POSTerminalApplication.getInstance();
//	private ResourceBundle res = CreamToolkit.GetResource();
	static  PeiDaState PeiDaState = null;
//	private ToneIndicator tone          = null;
//	private String pluCode = "";
//	private PLU plu                       = null;
//	private LineItem lineItem             = null;
//	private Transaction curTransaction    = app.getCurrentTransaction();
	

	public static PeiDaState getInstance() {
		try {
			if (PeiDaState == null) {
				PeiDaState = new PeiDaState();
			}
		} catch (InstantiationException ex) {
		}
		return PeiDaState;
	}

	/**
	 * Constructor
	 */
	public PeiDaState() throws InstantiationException {
	}

	/* (non-Javadoc)
	 * @see hyi.cream.state.State#entry(java.util.EventObject, hyi.cream.state.State)
	 */
	public void entry(EventObject event, State sourceState) {
		//System.out.println("This is PeiDaState Entry!");
		if (sourceState instanceof IdleState) {         
			annotatedid = "";
//			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("WeiXiuMessage"));
		}
		app.getWarningIndicator().setMessage("");
	}

	/* (non-Javadoc)
	 * @see hyi.cream.state.State#exit(java.util.EventObject, hyi.cream.state.State)
	 */
	public Class exit(EventObject event, State sinkState) {
		//System.out.println("PeiDaState exit");
		if (!app.getTransactionEnd()) {
			app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("PeiDaMessage"));
			return IdleState.class;
		}

		Transaction trans = app.getCurrentTransaction();
		String type = trans.getAnnotatedType();
		if (type != null && type.equals("P")) {
            //Bruce/20030505
            //再次按配达键的时候，取消原配达编号
            trans.setAnnotatedType("");
            trans.setAnnotatedId(annotatedid);  
			return IdleState.class;
		}
		trans.setAnnotatedType("P");
		annotatedid = genPeiDaNumber();
		trans.setAnnotatedId(annotatedid);
//		System.out.println("annotatedid : " + trans.getAnnotatedId());
		app.getWarningIndicator().setMessage("");
        
        //Bruce/20030505
        //在交易一开始的时候，不要设置transactionEnd=false，因为这样会导致无法输入
        //会员卡号
        //app.setTransactionEnd(false);

		return IdleState.class;
	}

    /**
     * 取得配达编号。PeiDa number format: "SSSSSPPYMMDDLLL"。
     * 
     * SSSSS: Store number
     * PP: POS number
     * YMMDD: date
     * LLL: sequential number
     * 
     * @return A new PeiDa number.
     */
	private String genPeiDaNumber() {
		String number = "";
		String storeNumber = Store.getStoreID();
		if (storeNumber.length() > 5) {
			storeNumber = storeNumber.substring(storeNumber.length() - 5
				, storeNumber.length());
		} else {
			while (storeNumber.length() < 5)
				storeNumber = "0" + storeNumber;
		}
		String terminalNumber = PARAM.getTerminalNumber() + "";
		if (terminalNumber.length() > 2) {
			terminalNumber = terminalNumber.substring(terminalNumber.length() - 2
				, terminalNumber.length());
		} else {
			while (terminalNumber.length() < 2)
			terminalNumber = "0" + terminalNumber;
		}
		String date = (CreamCache.getInstance().getDateFormate2()).format(new java.util.Date());
		date = date.substring(1, date.length());
		String peiDaNumber = PARAM.getPeiDaNumber();
        String dateInPeiDa = "";
        try {
            dateInPeiDa = peiDaNumber.substring(7, 12);
        } catch (StringIndexOutOfBoundsException e) {
        }
		if (!peiDaNumber.equals("") && dateInPeiDa.equals(date)) {
			int count = 0;
			try {
                number = peiDaNumber.substring(12, peiDaNumber.length());
				count = Integer.parseInt(number);
				count++;
			} catch (Exception e) {
				count = 1;
			}
			number = String.valueOf(count);
			while (number.length() < 3)
				number = "0" + number;
			number = date + number;
		} else 
			number = date + "001";

        //Bruce/20030505
        //暂时不存配达编号，等交易储存的时候在记
		//GetProperty.setProperty("PeiDaNumber", number);
        
		number = storeNumber + terminalNumber + number;
		return number;
	}
}
