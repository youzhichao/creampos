/**
 * State class
 * @since 2000
 * @author slackware
 */

package hyi.cream.state;

// for event processing

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;

import java.sql.SQLException;
import java.util.EventObject;

public class DaiFuPrintState extends State {
	static DaiFuPrintState daiFuPrintState = null;

	public static DaiFuPrintState getInstance() {
		try {
			if (daiFuPrintState == null) {
				daiFuPrintState = new DaiFuPrintState();
			}
		} catch (InstantiationException ex) {
		}
		return daiFuPrintState;
	}

	/**
	 * Constructor
	 */
	public DaiFuPrintState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
		// System.out.println("DaiFuPrintState entry!");

		POSTerminalApplication posTerminal = POSTerminalApplication
				.getInstance();
		if (!PARAM.isPrintPaidInOut() && posTerminal.getCurrentTransaction().getDealType2().equals("4")) {
			return;
		}

		if (sourceState instanceof DaiFuOpenPriceState) {
			Transaction cTransaction = posTerminal.getCurrentTransaction();
			Object[] lineItemArrayLast = cTransaction.getLineItems();
			if (!posTerminal.getTrainingMode()
					&& PARAM.getTranPrintType().equalsIgnoreCase("step")) {
				if (lineItemArrayLast.length > 1) {
                    DbConnection connection = null;
                    try {
                        connection = CreamToolkit.getTransactionalConnection();
    					if (!CreamPrinter.getInstance().getHeaderPrinted()) {
    						CreamPrinter.getInstance().printHeader(connection);
    						CreamPrinter.getInstance().setHeaderPrinted(true);
    					}
    					LineItem lineItem = (LineItem) lineItemArrayLast[lineItemArrayLast.length - 1];
    					if (lineItem.getDetailCode().equals("V"))
    						CreamPrinter.getInstance().printLineItem(connection, lineItem);
    					lineItem = (LineItem) lineItemArrayLast[lineItemArrayLast.length - 2];
    					CreamPrinter.getInstance().printLineItem(connection, lineItem);
                        connection.commit();
                    } catch (SQLException e) {
                        CreamToolkit.logMessage(e);
                    } finally {
                        CreamToolkit.releaseConnection(connection);
                    }
				}
				if (!cTransaction.getBuyerNumber().equals("")
						&& !posTerminal.getBuyerNumberPrinted()) {
					posTerminal.setBuyerNumberPrinted(true);
				}
			}
		}
		CreamToolkit.showText(posTerminal.getCurrentTransaction(), 0);
	}

	public Class exit(EventObject event, State sinkState) {
		// System.out.println("DaiFuPrintState exit!");

		return (sinkState != null) ? sinkState.getClass() : null;
//		if (sinkState != null)
//			return sinkState.getClass();
//		else
//			return null;
	}
}
