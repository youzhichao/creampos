package hyi.cream.state;

// for event processing
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;

public class PaidOutPrintState extends State {
	static PaidOutPrintState paidOutPrintState = null;

	public static PaidOutPrintState getInstance() {
		try {
			if (paidOutPrintState == null) {
				paidOutPrintState = new PaidOutPrintState();
			}
		} catch (InstantiationException ex) {
		}
		return paidOutPrintState;
	}

	/**
	 * Constructor
	 */
	public PaidOutPrintState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
		// System.out.println("PaidOutPrintState entry!");

		POSTerminalApplication posTerminal = POSTerminalApplication
				.getInstance();
		if (!PARAM.isPrintPaidInOut() && posTerminal.getCurrentTransaction().getDealType2().equals("4")) {
			CreamToolkit.showText(posTerminal.getCurrentTransaction(), 0);
			return;
		}

		if (sourceState instanceof PaidOutOpenPriceState) {
			if (!posTerminal.getTrainingMode()
					&& PARAM.getTranPrintType().equalsIgnoreCase("step")) {
				Transaction cTransaction = posTerminal.getCurrentTransaction();
				Object[] lineItemArrayLast = cTransaction
						.getLineItems();
				if (lineItemArrayLast.length > 1) {
                    DbConnection connection = null;
                    try {
                        connection = CreamToolkit.getTransactionalConnection();
						if (!CreamPrinter.getInstance().getHeaderPrinted()) {
							CreamPrinter.getInstance().printHeader(connection);
							CreamPrinter.getInstance().setHeaderPrinted(true);
						}
    					LineItem lineItem = (LineItem) lineItemArrayLast[lineItemArrayLast.length - 1];
    					if (lineItem.getDetailCode().equals("V"))
    						CreamPrinter.getInstance().printLineItem(connection, lineItem);
    					lineItem = (LineItem) lineItemArrayLast[lineItemArrayLast.length - 2];
    					CreamPrinter.getInstance().printLineItem(connection, lineItem);
                        connection.commit();
                    } catch (SQLException e) {
                        CreamToolkit.logMessage(e);
                        CreamToolkit.haltSystemOnDatabaseFatalError(e);
                    } finally {
                        CreamToolkit.releaseConnection(connection);
                    }
				}
				if (!cTransaction.getBuyerNumber().equals("")
						&& !posTerminal.getBuyerNumberPrinted()) {
					posTerminal.setBuyerNumberPrinted(true);
				}
			}
		}
	}

	public Class exit(EventObject event, State sinkState) {
		// System.out.println("PaidOutPrintState exit!");

		if (sinkState != null)
			return sinkState.getClass();
		else
			return null;
	}
}
