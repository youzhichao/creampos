
/**
 * State class
 * @since 2000
 * @author slackware
 */
 
package hyi.cream.state;

//for event processing
import hyi.cream.POSTerminalApplication;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;

public class SlippingState extends State {
    private boolean validated = false;
    static SlippingState slippingState = null;

    public static SlippingState getInstance() {
        try {
            if (slippingState == null) {
                slippingState = new SlippingState();
            }
        } catch (InstantiationException ex) {
        }
        return slippingState;
    }

    /**
     * Constructor
     */
    public SlippingState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        //System.out.println("This is SlippingState's Entry!");
        //POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        //Paying1State    null
        if (sourceState.getClass().getName().endsWith("Paying1State")
            && event == null) {
//Show message "请按[认证]键打印, [确认]键继续" on messageIndicator.
               posTerminal.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("SlippingHint"));
            //posTerminal.showIndicator("message");
        } else if (sourceState.getClass().getName().endsWith("SlippingState")
            && event.getSource().getClass().getName().endsWith("SlipButton")) {//SlippingState    SlipButton
//Print a string on slip.
            CreamPrinter.getInstance().printSlip(this);
            validated = true;
        }//KeylockWarningState    Keylock
    }
//exit()
//Sink State    Event Source    Action    Collaborated Things
    public Class exit(EventObject event, State sinkState) {
        //System.out.println("This is SlippingState's Exit!");
        //SlippingState    SlipButton    Do nothing.
        //Paying2State    EnterButton    Clear messageIndicator.
        if (event.getSource().getClass().getName().endsWith("EnterButton")) {
            if (validated) {
                POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
                posTerminal.getMessageIndicator().setMessage("");
                validated = false;
                return Paying1State.class;
            }
            return SlippingState.class;
        }
        if (sinkState != null) {
            return sinkState.getClass();
        } else
            return null;
    }    //KeylockWarningState Keylock

}


