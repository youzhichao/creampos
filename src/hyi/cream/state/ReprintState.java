package hyi.cream.state;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;
import java.util.Iterator;
import java.util.ResourceBundle;

public class ReprintState extends State {
    private static ReprintState instance = new ReprintState();
    POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Transaction previousTran = null;
    private String number = "";
    private int previousNumber = 0;
    private ResourceBundle res = CreamToolkit.GetResource();
    private boolean changeInvoice;
    private boolean cancelIt;
    private boolean reprintGenerateVoidTransaction;

    private ReprintState() {}

    public static ReprintState getInstance() {
        return instance;
    }

    public void entry(EventObject event, State sourceState) {
        DbConnection connection = null;
        try {
            reprintGenerateVoidTransaction = PARAM.getReprintGenerateVoidTransaction();
            int posNumber = PARAM.getTerminalNumber();
            connection = CreamToolkit.getPooledConnection();
            changeInvoice = false;
            number = String.valueOf(Transaction.getNextTransactionNumber());
            previousNumber = Integer.parseInt(number) - 1;
            previousTran = Transaction.queryByTransactionNumber(connection, posNumber, previousNumber, true);
            if (previousTran == null 
                || (!previousTran.getDealType1().equals("0")
                || !previousTran.getDealType2().equals("0")
                || (!previousTran.getDealType3().equals("0") && !previousTran.getDealType3().equals("4")))
                ) {
                app.getWarningIndicator().setMessage(res.getString("ReprintWarning"));
    
                //模拟user按ClearButton. 在State里面必需在开一个thread来做。
                cancelIt = true;
                new Thread(new Runnable() {
                    public void run() {
                        POSButtonHome2.getInstance().buttonPressed(new POSButtonEvent(new ClearButton(0,0,0,"")));
                    }
                }).start();
    
                return;
            }
            if (!previousTran.getDealType1().equals("*")) {
    
            } else {
                app.getWarningIndicator().setMessage(res.getString("ReturnWarning"));
                POSButtonHome2.getInstance().buttonPressed(new POSButtonEvent(new ClearButton(0, 0, 0, "")));
                return;
            }

            if (!StateToolkit.invoicePageEnough(previousTran)) {
                changeInvoice = true;
                return ;
            }
    
            ResourceBundle res = CreamToolkit.GetResource();
            app.getMessageIndicator().setMessage(res.getString("ReprintConfirm"));
            if (reprintGenerateVoidTransaction) {
                previousTran.setDealType1("0");
                previousTran.setDealType2("0");
                previousTran.setDealType3("2");
            }
            Iterator iter = LineItem.queryByTransactionNumber(connection, posNumber, previousNumber);
            while (iter.hasNext()) {
                LineItem li = (LineItem)iter.next();
                try {
                    app.getCurrentTransaction().addLineItem(li, false);
                } catch (TooManyLineItemsException e) {
                    System.out.println(e);
                }
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        if (event.getSource() instanceof ClearButton) {
            posTerminal.getCurrentTransaction().clearLineItem();
            posTerminal.getCurrentTransaction().clear();
            posTerminal.getCurrentTransaction().setDealType1("0");
            posTerminal.getCurrentTransaction().setDealType1("0");
            posTerminal.getCurrentTransaction().setDealType1("0");
            posTerminal.getItemList().setItemIndex(0);
            cancelIt = false;
        } else if (!cancelIt && event.getSource() instanceof EnterButton) {
            if (changeInvoice) {
                InvoiceNumberReadingState state = InvoiceNumberReadingState.getInstance();
                state.setExitState(ReprintState.class);
                app.getWarningIndicator().setMessage(res.getString("LastInvoice"));
                return InvoiceNumberReadingState.class;
            }

            Integer oldInvoiceCount = previousTran.getInvoiceCount();
            String oldInvoiceID = previousTran.getInvoiceID();
            String oldInvoiceNumber = previousTran.getInvoiceNumber();
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getTransactionalConnection();

                if (!reprintGenerateVoidTransaction) {
                    posTerminal.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("Reprinting"));
                    previousTran.setPrintType(CreamToolkit.GetResource().getString("Reprint"));
                    CreamPrinter.getInstance().reprint(connection, previousTran);
                    posTerminal.getItemList().setTransaction(posTerminal.getCurrentTransaction());
                    posTerminal.getCurrentTransaction().clear();
                    posTerminal.getItemList().repaint();
                    previousTran = null;
                    posTerminal.getMessageIndicator().setMessage(res.getString("ReprintEnd"));
                    posTerminal.setChecked(false);
                    return InitialState.class;
                }
                if (!previousTran.getDealType1().equals("*")) {
                    previousTran.setDealType1("*");
                    previousTran.setDealType3("0"); // because set to 2 at entry()
                    if (posTerminal.getTrainingMode())
                        previousTran.clear();
                    else
                        previousTran.update(connection);
                }
                posTerminal.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("Reprinting"));
                previousTran.setTransactionNumber(previousTran.getTransactionNumber().intValue() + 2);

                Transaction abolishTran = previousTran;
                abolishTran.setDealType1("0");
                abolishTran.setDealType3("2");
                abolishTran.setState2("0"); //恢复未转档初始状态
                abolishTran.setTransactionNumber(Integer.parseInt(number));
                abolishTran.setVoidTransactionNumber(new Integer(previousNumber * 100
                    + previousTran.getTerminalNumber().intValue()));
                abolishTran.setCustomerCount(0 - abolishTran.getCustomerCount().intValue());
                abolishTran.setInvoiceCount(oldInvoiceCount);
                abolishTran.setInvoiceID(oldInvoiceID);
                abolishTran.setInvoiceNumber(oldInvoiceNumber);
                abolishTran.makeNegativeValue();
                if (posTerminal.getTrainingMode())
                    abolishTran.clear();
                else {
                    abolishTran.getAlipayList().clear();
                    abolishTran.getCmpayList().clear();
                    abolishTran.getWeiXinList().clear();
                    abolishTran.getUnionPayList().clear();
                    abolishTran.store(connection);
                }

                Transaction newTran = abolishTran;
                newTran.makeNegativeValue();
                newTran.setDealType1("0");
                newTran.setTransactionNumber(Integer.parseInt(number) + 1);
                newTran.setVoidTransactionNumber(null);
                newTran.setDealType3("0");
                if (posTerminal.getTrainingMode()) {
                    newTran.clear();
                } else {
                    newTran.getAlipayList().clear();
                    newTran.getCmpayList().clear();
                    newTran.getWeiXinList().clear();
                    newTran.getUnionPayList().clear();
                    newTran.setInvoiceCount(oldInvoiceCount);
                    newTran.setInvoiceID(PARAM.getInvoiceID());
                    newTran.setInvoiceNumber(PARAM.getInvoiceNumber());
                    newTran.store(connection);
                    CreamPrinter.getInstance().reprint(connection, newTran);
                }
    
                posTerminal.getMessageIndicator().setMessage(res.getString("ReprintEnd"));
                posTerminal.setChecked(false);

                connection.commit();

            } catch (EntityNotFoundException e) {
                CreamToolkit.logMessage(e);
                CreamToolkit.haltSystemOnDatabaseFatalError(e);
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                CreamToolkit.haltSystemOnDatabaseFatalError(e);
            } finally {
                if(reprintGenerateVoidTransaction) {
                    previousTran.setInvoiceID(oldInvoiceID);
                    previousTran.setInvoiceNumber(oldInvoiceID);
                }
                CreamToolkit.releaseConnection(connection);
            }
        }
        posTerminal.getItemList().setTransaction(posTerminal.getCurrentTransaction());
        posTerminal.getCurrentTransaction().clear();
        posTerminal.getItemList().repaint();
        previousTran = null; //Bruce/20080414/ Don't keep any Transaction reference
        return InitialState.class;
    }
}
