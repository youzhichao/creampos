package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.EcDeliveryHead;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Payment;
import hyi.cream.dac.Transaction;
import hyi.cream.state.periodical.PeriodicalNoReadyState;
import hyi.cream.state.periodical.PeriodicalPartReturnState;
import hyi.cream.state.periodical.PeriodicalReturnEndState;
import hyi.cream.state.wholesale.WholesaleEndState;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.util.EventObject;
import java.util.ResourceBundle;

public class SummaryState extends State {

    POSTerminalApplication app = POSTerminalApplication.getInstance();

    private Class exitState = null;
    private Class sourceStateClazz = null;

    // DiscountState discountState = null;
    // ArrayList array = null;
    static SummaryState summaryState = null;

    private static ResourceBundle res = CreamToolkit.GetResource();
    private static boolean changeInvoice = false; // 发票纸是否需要更换

    /**
     * Meyer/2003-02-26 Get MixAndMatchVersion
     */
    public static SummaryState getInstance() {
        try {
            if (summaryState == null) {
                summaryState = new SummaryState();
            }
        } catch (InstantiationException ex) {
        }
        return summaryState;
    }

    /**
     * Constructor
     */
    public SummaryState() throws InstantiationException {
    }

    public void RoundDownItemProportion() {
        Transaction curTran = app.getCurrentTransaction();
        curTran.clearRoundDownDetail();
        Object[] lineItems = curTran.getLineItems();
        HYIDouble tmpAmount = new HYIDouble(0);
        for (int i = 0; i < lineItems.length; i++) {

            LineItem lineItem = (LineItem) lineItems[i];
            if (!lineItem.getDetailCode().equalsIgnoreCase("O")
                && !lineItem.getRemoved()
                && lineItem.getAfterDiscountAmount() != null)
            {
                tmpAmount = tmpAmount.addMe(((LineItem) lineItems[i])
                        .getAfterDiscountAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
            }
        }

        BigDecimal amount1 = new BigDecimal(tmpAmount.toString());
        BigDecimal rdAmount = amount1.subtract(amount1.setScale(1,
                BigDecimal.ROUND_HALF_UP));
        HYIDouble roundDownAmount = (new HYIDouble(rdAmount.doubleValue()))
                .negate();
        System.out.println("=============roundDownAmount" + roundDownAmount);
        //
        if (roundDownAmount.compareTo(new HYIDouble(0)) != 0
                && roundDownAmount.compareTo(new HYIDouble(-0)) != 0)
        {
            LineItem roundDownItem = new LineItem();
            roundDownItem.setTerminalNumber(curTran.getTerminalNumber());
            roundDownItem.setPluNumber(PARAM.getRoundItemNo());

            roundDownItem.setQuantity(new HYIDouble(1));
            roundDownItem.setUnitPrice(roundDownAmount);

            /*
             * jacky/2005-01-20/
             */
            roundDownItem.setItemNumber(PARAM.getRoundItemNo());

            roundDownItem.setOriginalPrice(roundDownAmount);

            roundDownItem.setAmount(roundDownAmount);
            roundDownItem.setAfterDiscountAmount(roundDownAmount);
            roundDownItem.setDetailCode("A");
            roundDownItem.setDiscountType("A");
            roundDownItem.setCategoryNumber(PARAM.getRoundItemCategory());
            curTran.setRoundDownItem(roundDownItem);
        }
    }

    public void entry(EventObject event, State sourceState) {

        //Bruce/20080414
        if (PARAM.isJustCalcMMAtSummaryState()) {
            StateMachine sm = StateMachine.getInstance();
            MixAndMatchState mms = sm.getStateInstance(MixAndMatchState.class);
            mms.calcMM(false);
            mms.postCalcMM();
        }

        sourceStateClazz = sourceState.getClass();
        changeInvoice = false;
        Transaction curTran = app.getCurrentTransaction();
        if (curTran.getSalesCanNotEnd()) {
            exitState = IdleState.class;
            app.getWarningIndicator().setMessage(res.getString("SalesCanNotEnd"));
            return;
        }

        if (curTran.getEcDeliveryHeadList().size() > 0) {
            if (curTran.getEcDeliveryHeadList().get(0).getServiceType().equals("1") || curTran.getEcDeliveryHeadList().get(0).getServiceType().equals("3")) {
                curTran.setPayNumber1(curTran.getEcDeliveryHeadList().get(0).getPayId());
                curTran.setPayAmount1(curTran.getEcDeliveryHeadList().get(0).getAmount());
                app.getWarningIndicator().setMessage(res.getString("ECommerceOrderPressOKToSaveDirectly"));
            }
        }

//      if (!StateToolkit.invoicePageEnough(curTran)) {
//          changeInvoice = true;
//          exitState = InvoiceNumberReadingState.class;
//          return;
//      }

        if (PARAM.isRoundDown()) {
            this.RoundDownItemProportion();
        }

        app.getPayingPane1().setMode(0);
        app.getPayingPane2().setMode(0);

        if (sourceState != null
            && !(sourceState instanceof DrawerOpenConfirmState)
            && !(sourceState instanceof SummaryState)
            && !(sourceState instanceof Warning2State)
            && !(sourceState instanceof Paying3State)) {

            if (sourceState instanceof Numbering2State
                || sourceState instanceof PluReadyState
                || sourceState instanceof SIDiscountState
                || sourceState.getClass().getSimpleName().startsWith("CAT")
                || sourceState instanceof AlipayProcessState || sourceState instanceof AlipayState
                || sourceState instanceof UnionPayProcessState || sourceState instanceof UnionPayState
                || sourceState instanceof CmpayState || sourceState instanceof WeiXinProcessState || sourceState instanceof WeiXinState) {
                if (curTran.getAlipayList().size() > 0 || curTran.getCmpayList().size() > 0 || curTran.getWeiXinList().size() > 0 || curTran.getUnionPayList().size() > 0) {
                    exitState = this.getClass();
                }
                else
                    exitState = IdleState.class;
            } else {
                if (PARAM.getCreateButtonPanel()) {
                    if (sourceState instanceof IdleState) {
                        app.getButtonCardLayout().show(app.getButtonPanel(), "BUTTON_PANEL2");
                    }
                }
                exitState = sourceState.getClass();
            }
        }
        if (sourceState instanceof DrawerOpenConfirmState) {
            if (curTran.getAlipayList().size() > 0 || curTran.getCmpayList().size() > 0 || curTran.getWeiXinList().size() > 0 || curTran.getUnionPayList().size() > 0) {
                exitState = this.getClass();
            }
        }

        app.getMessageIndicator().setMessage("");
        if (sourceState instanceof PeiDa3OnlineState || app.getCurrentTransaction().isPeiDa())
            exitState = PeiDa3OnlineState.class;

        if (sourceState instanceof IdleState
                || sourceState instanceof PeriodicalNoReadyState
                || sourceState instanceof PeriodicalReturnEndState
                || sourceState instanceof WholesaleEndState
                || sourceState instanceof PluReadyState
                || sourceState instanceof PeiDa3OnlineState
                || sourceState instanceof PeiDa3AmountState
                || sourceState instanceof SIDiscountState
                || sourceState instanceof PeriodicalPartReturnState) {
            // posTerminal.getPopupMenuPane().setVisible(false);
            if (!(sourceState instanceof SIDiscountState)) {
                curTran.initSIInfo();
                int custID = ((AgeLevelButton) event.getSource()).getAgeID();
                curTran.setCustomerAgeLevel(custID);
            } else {
                // 如果剛輸入了小計後折扣，則清除所有支付信息，讓收銀員重新輸入
                curTran.clearRoundDownDetail();
                curTran.initPaymentInfoWithoutClearingDiscount();
                showMessage("PleaseInputPayment");
            }

            curTran.initForAccum();
            curTran.accumulate();
            if (sourceState instanceof PeriodicalPartReturnState) {
                curTran.setGrossSalesAmount(new HYIDouble(0));
                
            }
            //WenHuan/2008-11-05/ 解决 amount 与afteramount 不一致。
            // 引起的原因：输入商品—>客层键—>si按钮->清除键->客层键—>结帐，导致teandetail表中AFTDSCAMT字段还是折扣之后的值
            if (sourceState instanceof IdleState) {
                // Bruce/2003-12-24
                curTran.saveOriginalLineItemAfterDiscountAmount();
            }

            curTran.setChangeAmount(new HYIDouble(0));
            // app.getLineItemIndicator().setVisible(false);
            //curTran.setPayCash(new HYIDouble(0));
            app.getPayingPane().setTransaction(curTran);
            app.getPayingPane1().setTransaction(curTran);
            app.getPayingPane2().setTransaction(curTran);
            // Panel p = app.getItemListPanel();
            app.getItemList().setVisible(false);
            app.getPayingPane().setVisible(true);

        } else if (sourceState instanceof DaiFuIdleState
                || sourceState instanceof PaidOutIdleState
                || sourceState instanceof PaidInIdleState
                || sourceState instanceof DaiShou3IdleState) {

            curTran.initForAccum();
            curTran.initSIInfo();
            curTran.accumulate();
            curTran.setChangeAmount(new HYIDouble(0));
            curTran.setPayCash(new HYIDouble(0));

            // app.getLineItemIndicator().setVisible(false);
            app.getItemList().setVisible(false);
            app.getPayingPane().setTransaction(
                    curTran);
            app.getPayingPane1().setTransaction(curTran);
            app.getPayingPane2().setTransaction(curTran);
            app.getPayingPane().setVisible(true);
        } else if (sourceState instanceof ReadCrdNoState) {
            app.getMessageIndicator().setMessage(
                    res.getString("InputCrdAmount"));
        }

        if (sourceState instanceof DrawerOpenState) {
            ;
        }
        // if (sourceState instanceof Paying3State) {
        // app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("CashWarning"));
        // }

        /*
         * if (sourceState instanceof DiscountState) { DiscountState d
         * =(DiscountState)sourceState; this.discountState = d; this.array =
         * d.getArrayValue(); }
         */
        app.getPayingPane().repaint();
        // System.out.println(trans.getValues());

        CreamToolkit.showText(curTran, 1);
    }

    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");

        Transaction curTran = app.getCurrentTransaction();
        if (curTran.getSalesCanNotEnd() || changeInvoice) {
            return exitState;
        }
        app.getWarningIndicator().setMessage("");
        if (event.getSource() instanceof ClearButton) {
            if (app.getCurrentTransaction().getAlipayList().size() > 0 || app.getCurrentTransaction().getCmpayList().size() > 0
                    || app.getCurrentTransaction().getWeiXinList().size() > 0 || app.getCurrentTransaction().getUnionPayList().size() > 0){
                //2015-04-13 @pingping 已经刷过支付宝，不允许退到销售界面
                return this.getClass();
            }
            curTran.clearRoundDownDetail();
            curTran.initPaymentInfo();
            curTran.initForAccum();
            curTran.accumulate();

            // app.getLineItemIndicator().setVisible(false);
            app.getPayingPane().setVisible(false);
            app.getPayingPane().clear();
            app.getItemList().setVisible(true);
            // app.getButtonPanel().showLayer(5);
            // System.out.println(exitState);
            app.getWarningIndicator().setMessage("");
            app.getMessageIndicator().setMessage("");
//          if (curTran.getAnnotatedType() != null
//                  && !curTran.getAnnotatedType().trim().equals(""))
//              return PeiDa3OnlineState.class;
            if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.WHOLESALE_STATE
                    && sourceStateClazz == Numbering2State.class) {
                return WholesaleEndState.class;
            }
            if (PARAM.getCreateButtonPanel()) {
                if (exitState == IdleState.class) {
                    app.getButtonCardLayout().show(app.getButtonPanel(), "BUTTON_PANEL1");
                }
            }
            return exitState;
        }
        // System.out.println(trans.getValues());
        if (event.getSource() instanceof PaymentButton
                || event.getSource() instanceof EnterButton
                || event.getSource() instanceof PaymentMenu2Button
                || event.getSource() instanceof AlipayButton
                || event.getSource() instanceof WeiXinButton
                || event.getSource() instanceof UnionPayButton
                || event.getSource() instanceof CmpayButton) {

            // Bruce/2003-12-01
            // 现金支付也必须看是否为“自动结清余额”
            String payID;
            if (event.getSource() instanceof PaymentButton)
                payID = ((PaymentButton) event.getSource()).getPaymentID();
            else if (event.getSource() instanceof PaymentMenu2Button)
                payID = ((PaymentMenu2Button) event.getSource()).getPaymentID();
            else if (event.getSource() instanceof AlipayButton)
                payID = ((AlipayButton)event.getSource()).getPaymentID();
            else if (event.getSource() instanceof WeiXinButton)
                payID = ((WeiXinButton)event.getSource()).getPaymentID();
            else if (event.getSource() instanceof UnionPayButton)
                payID = ((UnionPayButton)event.getSource()).getPaymentID();
            else if (event.getSource() instanceof CmpayButton)
                payID = ((CmpayButton)event.getSource()).getPaymentID();
            else
                payID = "00";
            // if (payID.equals("00")) {
            // return Paying1State.class;
            // }

            Payment payment = Payment.queryByPaymentID(payID);
            if (payment == null) {
                return Warning2State.class;
            }

            // Bruce/20030416
            // 一般交易不允许使用“赊帐”支付。（Modified for 灿坤）
            //String payName = payment.getScreenName();
            if (!PARAM.isAllowShePayment()
                    && payID.equals(Payment.queryDownPaymentId())
                    && (curTran.getAnnotatedType() == null || !curTran.getAnnotatedType().equals("P"))) {
                setWarningMessage(CreamToolkit.getString("CannotUseCreditPayment"));
                return Warning2State.class;
            }

            if (event.getSource() instanceof AlipayButton) {//支付宝
                if (app.getReturnItemState() || app.getIsAllReturn()) {
                    setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseAlipay"));
                    return Warning2State.class;
                }
                if (CreamSession.getInstance().getWorkingState().equals(WorkingStateEnum.PERIODICAL_DRAW_STATE)) {
                    setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseAlipay2"));
                    return Warning2State.class;
                }
                if (curTran.getAlipayList().size() > 0) {
                    setWarningMessage(CreamToolkit.GetResource().getString("AlipayOnlyUse"));
                    return Warning2State.class;
                }
                return AlipayState.class;
            }

            if (event.getSource() instanceof WeiXinButton) {//微信
                if (app.getReturnItemState() || app.getIsAllReturn()) {
                    setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseWeiXin"));
                    return Warning2State.class;
                }
                if (CreamSession.getInstance().getWorkingState().equals(WorkingStateEnum.PERIODICAL_DRAW_STATE)) {
                    setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseWeiXin2"));
                    return Warning2State.class;
                }
                if (curTran.getWeiXinList().size() > 0) {
                    setWarningMessage(CreamToolkit.GetResource().getString("WeiXinOnlyUse"));
                    return Warning2State.class;
                }
                return WeiXinState.class;
            }

            if (event.getSource() instanceof UnionPayButton) {//微信
                if (app.getReturnItemState() || app.getIsAllReturn()) {
                    setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseUnionPay"));
                    return Warning2State.class;
                }
                if (CreamSession.getInstance().getWorkingState().equals(WorkingStateEnum.PERIODICAL_DRAW_STATE)) {
                    setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseUnionPay2"));
                    return Warning2State.class;
                }
                if (curTran.getUnionPayList().size() > 0) {
                    setWarningMessage(CreamToolkit.GetResource().getString("UnionPayOnlyUse"));
                    return Warning2State.class;
                }
                return UnionPayState.class;
            }
            if (event.getSource() instanceof CmpayButton) {//移动支付
                if (app.getReturnItemState() || app.getIsAllReturn()) {
                    setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseCmpay"));
                    return Warning2State.class;
                }
                if (CreamSession.getInstance().getWorkingState().equals(WorkingStateEnum.PERIODICAL_DRAW_STATE)) {
                    setWarningMessage(CreamToolkit.GetResource().getString("PeriodicalPartNotUseCmpay2"));
                    return Warning2State.class;
                }
                if (curTran.getCmpayList().size() > 0) {
                    setWarningMessage(CreamToolkit.GetResource().getString("CmpayOnlyUse"));
                    return Warning2State.class;
                }
                return CmpayState.class;
            }
            if (payment.isAutoTendering()) {
                return Paying1State.class;
            } else {
                if (curTran.getEcDeliveryHeadList().size() > 0) {
                    if (curTran.getEcDeliveryHeadList().get(0).getServiceType().equals("1") || curTran.getEcDeliveryHeadList().get(0).getServiceType().equals("3")) {
                        return Paying3State.class;
                    }
                }
                setWarningMessage(CreamToolkit.GetResource().getString("SummaryWarning"));
                return Warning2State.class;
            }
        }

        if (event.getSource() instanceof NumberButton) {
            if (curTran.getDealType2().equals("4")) {
                if ((curTran.getDaiFuAmount() != null && curTran.getDaiFuAmount()
                        .compareTo(new HYIDouble(0)) > 0))
                    return Numbering2State.class;
                else if ((curTran.getDaiFuAmount() != null && curTran
                        .getDaiFuAmount().compareTo(new HYIDouble(0)) < 0))
                    return SummaryState.class;
                else
                    return SummaryState.class;
            } else {
                return Numbering2State.class;
            }
        }

        if (event.getSource() instanceof SIButton) {
            if (app.getReturnItemState()) {
                setWarningMessage(CreamToolkit.GetResource().getString(
                        "ReturnSaleWarning"));
                return Warning2State.class;
            } else if (curTran.isPeiDa()) {
                setWarningMessage(CreamToolkit.GetResource().getString("PeiDaError9"));
                app.getMessageIndicator().setMessage(
                        CreamToolkit.GetResource().getString("Warning"));
                return Warning2State.class;
            } else {
                return SIDiscountState.class;
            }
        }

        return sinkState.getClass();
    }
}
