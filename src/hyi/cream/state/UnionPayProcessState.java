package hyi.cream.state;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Store;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.UnionPay_detail;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.sdk.UnionPayConsume;
import hyi.cream.sdk.UnionPayConsumeUndo;
import hyi.cream.sdk.UnionPayQuery;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventObject;
import java.util.Iterator;
import java.util.ResourceBundle;

//微信支付的业务处理
public class UnionPayProcessState extends State {

	private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    public static boolean isClearButton = false;
    public Class exitState = null;
    public static int seq = 0;
    public Transaction curTransaction;
    private String payAmount = "";
    public String payID = "";
    static UnionPayProcessState unionpayState = null;
    public static UnionPayProcessState getInstance() {
        if (unionpayState == null) {
            unionpayState = new UnionPayProcessState();
        }
        return unionpayState;
    }
	public void entry(EventObject event, State sourceState) {
        curTransaction =  app.getCurrentTransaction();
        if (sourceState instanceof UnionPayState) {
            this.payID = ((UnionPayState) sourceState).payID;
            isClearButton = false;
            app.setUnionPay(false);
            exitState = SummaryState.class;
            if (Store.getSdkMerId() == null) {
                app.getWarningIndicator().setMessage(res.getString("UnionPayProperyFail"));
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome2.getInstance().buttonPressed(e);
                return;
            }
            if (app.getTrainingMode()) {
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome2.getInstance().buttonPressed(e);
                exitState = Paying1State.class;
                return ;
            }
            Object eventSource = event.getSource();
            payAmount = curTransaction.getBalance().toString();
            String amt = curTransaction.getBalance().multiply(new HYIDouble(100)).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
            UnionPay_detail detail = processUnionPay(sourceState, amt) ;

            app.setUnionPay(false);
            if (isClearButton) {
                exitState = SummaryState.class;
                return ;
            }
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome2.getInstance().buttonPressed(e);
            if (detail == null)  {
                exitState = SummaryState.class;
                return;
            }
            exitState = Paying1State.class;
        }
	}

	public Class exit(EventObject event, State sinkState) {

		Object eventSource = event.getSource();
        if (eventSource instanceof ClearButton) {
            isClearButton = false;
            return exitState;
        }
        return exitState;
	}

    public UnionPay_detail processUnionPay(State state, String inputPayingAmount) {
        CreamToolkit.logMessage("check transaction for unionpay ...");
        System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date()) + "check transaction for unionpay ...");

        Object[] lineItemArray =  curTransaction.getLineItems();
        String  body = "";
        String goods_detail = "";
        boolean ok = false;
        for (int i = 0; i < lineItemArray.length; i++) {
            LineItem l = (LineItem)lineItemArray[i];
            if (!l.getRemoved()) {
                if (body.length() + l.getDescription().length() > 30) {
                    if (!ok) {
                        body = body.substring(1) + "等等...";
                        ok = true;
                    }
                } else {
                    body += "," + l.getDescription();
                }
                String detail = ",{\"goods_id\":\""+l.getPluNumber()+"\",\"goods_name\":\""+l.getDescription()
                        +"\",\"quantity\":" + l.getQuantity().intValue() + ",\"price\":"+l.getOriginalPrice().multiply(new HYIDouble(100.0)).setScale(0, BigDecimal.ROUND_HALF_UP)+"}";
//                if (j < 5)
                goods_detail += detail;
//                if (body.length() > 30) {
//                    body = body.substring(1) + "等等...";
//                    break;
//                }
            }
        }
        if (body.length() <= 30)
            body = body.substring(1);
        goods_detail = "{\"goods_detail\":["+goods_detail.substring(1)+"],\"cost_price\":" + inputPayingAmount+"}";
        Iterator it =  curTransaction.getPayments();
        int count = 0;
        while(it.hasNext()) {
            it.next();
            count++;
        }
        if (count > 0)
            body = "部分金额支付,商品:"+body;
        else
            body = "全额支付,商品:" + body;
        app.getMessageIndicator().setMessage(res.getString("UnionPayMicropay"));
        seq++;
        String seqStr = "";
        if (seq < 10)
            seqStr = "0"+seq;
        else
            seqStr = "" + seq;
        String out_trade_no = curTransaction.getStoreNumber()+curTransaction.getTerminalNumber()+curTransaction.getTransactionNumber()+seqStr;
        String auth_code = ((UnionPayState)state).getNumberStr();
//        if (!SystemInfo.isWanConnected()) {
//            CreamToolkit.logMessage("WanOfflineCanNotTrade... ");
//            app.getWarningIndicator().setMessage(res.getString("WanOfflineCanNotTrade"));
//            return null;
//        }

        System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())+" micropay ");
        CreamToolkit.logMessage("micropay");
        String xml = "";
        if (!auth_code.startsWith(curTransaction.getStoreNumber()+curTransaction.getTerminalNumber()+curTransaction.getTransactionNumber())) {
            String terminalNumber = curTransaction.getTerminalNumber().toString();
            while (terminalNumber.length() < 2){
                terminalNumber = "0" + terminalNumber;
            }
            terminalNumber = Store.getStoreID() + terminalNumber;
            while (terminalNumber.length() < 8){
                terminalNumber = "0" + terminalNumber;
            }
            xml = UnionPayConsume.doPost(inputPayingAmount,out_trade_no,auth_code,terminalNumber); //支付接口
        } else {
            out_trade_no = auth_code;
        }

        if (xml == null || xml.equals("")) {
            System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())+" orderQuery ");
            CreamToolkit.logMessage(" orderQuery");
            return orderQuery(inputPayingAmount, out_trade_no, seqStr);
        }
        String isSuccess  = getValueFromXml(xml,"<respCode>","</respCode>");
        if (isSuccess.equals("00")) {
            return orderQuery(inputPayingAmount, out_trade_no, seqStr);
        } else {
            if (isSuccess.equals("70")) {//支付等待，顾客确认密码
                System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())+" USERPAYING ");
                CreamToolkit.logMessage(" USREPAYING");
                String queryId = getValueFromXml(xml,"<queryId>","</queryId>");
                String txnTime = getValueFromXml(xml,"<txnTime>","</txnTime>");
                return processNotiryRequert(inputPayingAmount, seqStr, out_trade_no, queryId, txnTime);
            } else {//支付失败
                String err_code_des = getValueFromXml(xml,"<respMsg>","</respMsg>");
                CreamToolkit.logMessage(err_code_des);
                app.getWarningIndicator().setMessage(err_code_des);
            }
            String return_msg = getValueFromXml(xml,"<respMsg>","</respMsg>");
            CreamToolkit.logMessage(return_msg);
            app.getWarningIndicator().setMessage(return_msg);
        }
        //支付失败，则再次调用查询和撤销
        System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())+" orderQuery1 ");
        CreamToolkit.logMessage(" orderQuery1");
        return orderQuery(inputPayingAmount, out_trade_no, seqStr);
//        return null;
    }

    //异步请求
    public UnionPay_detail processNotiryRequert(String inputPayingAmount, String seqStr, String out_trade_no, String queryIds, String txnTime) {

        app.setUnionPay(true);
        app.getWarningIndicator().setMessage(res.getString("AlipayWaitBuyerPay"));
        for (int i = 0 ; i < 60; i++) {//请求五分钟
            try {
                Thread.sleep(1000);
                if (isClearButton) {//如果按了清除键，才停止请求
                    seq++;
                    String seqStrs = "";
                    if (seq < 10)
                        seqStrs = "0"+seq;
                    else
                        seqStrs = "" + seq;
                    String out_refund_no = curTransaction.getStoreNumber()+curTransaction.getTerminalNumber()+curTransaction.getTransactionNumber()+seqStrs;
                    UnionPayConsumeUndo.doPost(inputPayingAmount,out_refund_no,out_trade_no,queryIds,txnTime);
                    return null;
                }
                app.getMessageIndicator().setMessage(res.getString("RunTime") + (60-i));
                if ((i+1)%1==0) {//1秒请求一次
                    CreamToolkit.logMessage("isClearButton = " + isClearButton);
                    String xml = UnionPayQuery.doPost(out_trade_no);//查询
                    if (xml == null || xml.equals("")) {
                        CreamToolkit.logMessage("WanOfflineCanNotTrade... ");
                        app.getWarningIndicator().setMessage(res.getString("WanOfflineCanNotTrade"));
                        continue;
                    }
                    String return_code  = getValueFromXml(xml,"<respCode>","</respCode>");
                    if (return_code.equals("00")) {//查询成功
                        String result_code = getValueFromXml(xml,"<origRespCode>","</origRespCode>");
                        if (result_code.equals("00")) {//查询原交易是否支付成功
                            String queryId = getValueFromXml(xml, "<queryId>","</queryId>");
                            String orderId = getValueFromXml(xml,"<orderId>","</orderId>");
                            String txnTimes = getValueFromXml(xml,"<txnTime>","</txnTime>");
                            return addUnionPayDetail(inputPayingAmount,queryId,seqStr,orderId,txnTimes);
                        } else if (result_code.equals("70")) {
                            continue;
                        } else if (result_code.equals("05")) {
                            continue;
                        } else {
                            String origRespMsg  = getValueFromXml(xml,"<origRespMsg>","</origRespMsg>");
                            CreamToolkit.logMessage(origRespMsg);
                            app.getWarningIndicator().setMessage(origRespMsg);
                        }
                    } else {//查询失败
                        String respMsg  = getValueFromXml(xml,"<respMsg>","</respMsg>");
                        CreamToolkit.logMessage(respMsg);
                        app.getWarningIndicator().setMessage(respMsg);
                        continue;
                    }
                }
                continue;
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
        return null;
    }

    public UnionPay_detail orderQuery(String inputPayingAmount, String out_trade_no, String seqStr) {
        String xml = UnionPayQuery.doPost(out_trade_no);//查询
        if (xml == null || xml.equals("")) {
            CreamToolkit.logMessage("WanOfflineCanNotTrade... ");
            app.getWarningIndicator().setMessage(res.getString("WanOfflineCanNotTrade"));
            return null;
        }
        String return_code  = getValueFromXml(xml,"<respCode>","</respCode>");
        if (return_code.equals("00")) {//查询成功
            String result_code = getValueFromXml(xml,"<origRespCode>","</origRespCode>");
            if (result_code.equals("00")) {//查询原交易是否支付成功
                String totalFee = getValueFromXml(xml, "<txnAmt>", "</txnAmt>");
                if (new BigDecimal(totalFee).compareTo(new BigDecimal(inputPayingAmount)) != 0) {
                    BigDecimal t = new BigDecimal(totalFee).divide(new BigDecimal(100), 2, 4);
                    app.getWarningIndicator().setMessage(res.getString("AmmountError") + t);
                    return null;
                }
                String queryId = getValueFromXml(xml, "<queryId>","</queryId>");
                String orderId = getValueFromXml(xml,"<orderId>","</orderId>");
                String txnTime = getValueFromXml(xml,"<txnTime>","</txnTime>");
                return addUnionPayDetail(inputPayingAmount,queryId,seqStr,orderId,txnTime);
            }else if (result_code.equals("05")) {//支付等待，顾客确认密码
                CreamToolkit.logMessage(" USREPAYING");
                String queryId = getValueFromXml(xml,"<queryId>","</queryId>");
                String txnTime = getValueFromXml(xml,"<txnTime>","</txnTime>");
                return processNotiryRequert(inputPayingAmount, seqStr, out_trade_no, queryId, txnTime);
            } else {
                String origRespMsg  = getValueFromXml(xml,"<origRespMsg>","</origRespMsg>");
                CreamToolkit.logMessage(origRespMsg);
                app.getWarningIndicator().setMessage(origRespMsg);
            }
        } else {//查询失败
            String respMsg  = getValueFromXml(xml,"<respMsg>","</respMsg>");
            CreamToolkit.logMessage(respMsg);
            app.getWarningIndicator().setMessage(respMsg);
        }
        return null;
    }


    public UnionPay_detail addUnionPayDetail(String inputPayingAmount, String queryId, String seq, String orderId, String txnTime) {
        UnionPay_detail unionPay_detail;
        try{
            HYIDouble amt = new HYIDouble(inputPayingAmount).divide(new HYIDouble(100), 2, 4).setScale(2, BigDecimal.ROUND_HALF_UP);
            unionPay_detail = new UnionPay_detail();
            unionPay_detail.setSTORENUMBER(curTransaction.getStoreNumber());
            unionPay_detail.setPOSNUMBER(curTransaction.getTerminalNumber());
            unionPay_detail.setTRANSACTIONNUMBER(curTransaction.getTransactionNumber());
            unionPay_detail.setTOTAL_FEE(amt);
            unionPay_detail.setSEQ(seq);
            unionPay_detail.setSYSTEMDATE(new Date());
            unionPay_detail.setZSEQ(curTransaction.getZSequenceNumber());
            unionPay_detail.setQueryId(queryId);
            unionPay_detail.setMerId(Store.getSdkMerId());
            unionPay_detail.setOrderId(orderId);
            unionPay_detail.setTxnTime(txnTime);
            curTransaction.addUnionPayList(unionPay_detail);
            CreamToolkit.logMessage("transactionNumber：" + unionPay_detail.getTRANSACTIONNUMBER() + "，transaction_id："
                    + unionPay_detail.getTOTAL_FEE()+ ",systemDate：" + unionPay_detail.getSYSTEMDATE().toString()
                    + " is add trans.addUnionPayDetail");
            this.seq = 0;

            return unionPay_detail;
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return null;
    }

    public String getValueFromXml(String xml, String token1, String token2) {
        String result = "";
        int a = xml.indexOf(token1);
        int b = xml.indexOf(token2);

        if (a >= 0 && b >= 0) {
            result = xml.substring(a+token1.length(), b);
        }
        return result;
    }

    public String getTrade_state(String trade_state) {
        if (trade_state.equals("REFUND"))
            return "交易转入退款";
        else if (trade_state.equals("CLOSED"))
            return "交易已关闭";
        else if (trade_state.equals("REVERSE"))
            return "交易已冲正";
        else if (trade_state.equals("REVOK"))
            return "交易已撤销";
        else if (trade_state.equals("PAYERROR"))
            return "支付失败(其他原因，如银行返回失败)";
        else if (trade_state.equals("NOPAY"))
            return "未支付(确认支付超时)";
        return "";
    }

    public String getPayAmount() {
        return payAmount;
    }
    public String getPayID() {
        return payID;
    }
}
