package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.PLU;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.uibeans.AgeLevelButton;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.spos.JposException;
import hyi.spos.Scanner;
import hyi.spos.ScannerConst;
import hyi.spos.ToneIndicator;
import hyi.spos.events.DataEvent;

import java.util.EventObject;
import java.util.MissingResourceException;

//import jpos.JposException;
//import jpos.ToneIndicator;

public class PeiDa3AmountState extends State {

    private static PeiDa3AmountState peiDa3AmountState;

    private String amount;

    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    private ToneIndicator tone = null;

    private boolean scan = true;

    public static PeiDa3AmountState getInstance() {
        try {
            if (peiDa3AmountState == null) {
                peiDa3AmountState = new PeiDa3AmountState();
            }
        } catch (InstantiationException ex) {
        }
        return peiDa3AmountState;
    }

    /**
     * Constructor
     */
    public PeiDa3AmountState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof PeiDa3AmountState || sourceState instanceof SummaryState
                ) {
            app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("PeiDaConfirm"));
        }
        else if (scan || sourceState instanceof PeiDa3OfflineState)
            app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString(
                            "PeiDa3ScanAmount"));
    }

    public Class exit(EventObject event, State sinkState) {
        Object eventSource = event.getSource();
        if (eventSource instanceof Scanner) {
            try {
                DataEvent dataEvent = (DataEvent)event;
                int scanDataType = ((Scanner) eventSource).getScanDataType(dataEvent.seq);
                if (scanDataType == ScannerConst.SCAN_SDT_UNKNOWN
                    || scanDataType == ScannerConst.SCAN_SDT_Code39) {
                    setWarningMessage(CreamToolkit.GetResource().getString(
                            "InputWrong"));
                    return PeiDa3AmountState.class;
                }
                amount = new String(((Scanner) eventSource).getScanData(((DataEvent)event).seq));
            } catch (MissingResourceException e1) {
                e1.printStackTrace();
            } catch (JposException e1) {
                e1.printStackTrace();
            }
//            LineItem lineItem = PeiDa3OnlineState.createLineItem(PLU.queryByItemNumber("0960052"), new HYIDouble(1));
//            try {
//                app.getCurrentTransaction().addLineItem(lineItem);
//                POSTerminalApplication.getInstance().setTransactionEnd(false);
//            } catch (TooManyLineItemsException e) {
//                CreamToolkit.logMessage(e.toString());
//                CreamToolkit.logMessage("Too many LineItem exception at "
//                        + this);
//            }
            scan = false;
        } else if (eventSource instanceof AgeLevelButton) {
            return SummaryState.class;
        } else if (eventSource instanceof ClearButton) {
            amount = "";
            app.getCurrentTransaction().clearLineItem();
            app.getCurrentTransaction().clearDeliveryInfo();
            return PeiDa3IdleState.class;
        }

        return PeiDa3AmountState.class;
    }

}
