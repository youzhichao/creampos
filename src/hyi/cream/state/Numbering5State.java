// Copyright (c) 2000 HYI
package hyi.cream.state;

import java.util.*;

import hyi.cream.*;
import hyi.cream.uibeans.*;
import hyi.cream.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class Numbering5State extends State {

    private String printString          = "";
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static Numbering5State numbering5State = null;

    public static Numbering5State getInstance() {
        try {
            if (numbering5State == null) {
                numbering5State = new Numbering5State();
            }
        } catch (InstantiationException ex) {
        }
        return numbering5State;
    }

    /**
     * Constructor
     */
    public Numbering5State() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof IdleState) {
			printString = "";
		}

		if (event.getSource() instanceof NumberButton) {
			NumberButton pb = (NumberButton)event.getSource();
			printString = printString + pb.getNumberLabel();
			app.getMessageIndicator().setMessage(printString);
		}

		if (sourceState instanceof IdleState) {
			if (app.getBuyerNumberPrinted() == false) {
			    app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputBuyerNumberHint"));
			} else {
				app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("ErrorOfBuyerNo2"));
			}
		}
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof ClearButton) {
            printString = "";
			app.getMessageIndicator().setMessage(printString);
		}
		if (sinkState != null) {//sinkState instanceof BuyerNumberState &&
			if (app.getBuyerNumberPrinted() == false)
			    app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("BuyerNumberUpdated"));
			else 
				app.getWarningIndicator().setMessage("");
			return sinkState.getClass();
		} else
			return null;
		
    }

    public String getBuyerNumber() {
        return printString;
    }
}

 