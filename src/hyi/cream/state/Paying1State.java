// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.dac.Payment;
import hyi.cream.dac.Transaction;
import hyi.cream.state.wholesale.WholesaleClearingState;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.PaymentButton;
import hyi.cream.uibeans.PaymentMenu2Button;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.EventObject;

/**
 * Determine transaction's 支付代号和金额.
 *
 * @author dai
 */
public class Paying1State extends State {
    private String paymentID = "";
    private String payNumber = "";
    private boolean paymentAvailable = false;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Payment lastestPayment = null;

    private static Paying1State instance = null;
    private static HYIDouble ZERO = new HYIDouble(0);

    public static Paying1State getInstance() {
        try {
            if (instance == null) {
                instance = new Paying1State();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    /**
     * Constructor
     */
    public Paying1State() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        String payno = "";
        String payamt = "";
        Transaction curTransaction = app.getCurrentTransaction();
        Object eventSource = event.getSource();
        if (sourceState instanceof SummaryState) {
            if (eventSource instanceof PaymentButton) {
                paymentID = ((PaymentButton)eventSource).getPaymentID();
            } else if (eventSource instanceof EnterButton) {
                paymentID = "00";
            }
            payNumber = curTransaction.getBalance().toString();
        } else if (sourceState instanceof Numbering2State) {
            if (eventSource instanceof PaymentButton) {
                paymentID = ((PaymentButton)eventSource).getPaymentID();
            } else if (eventSource instanceof EnterButton) {
                paymentID = "00";
            } else if (eventSource instanceof PaymentMenu2Button) {
                paymentID = ((PaymentMenu2Button)event.getSource()).getPaymentID();
            }
            payNumber = ((Numbering2State)sourceState).getNumberString();
        } else if (sourceState instanceof ReadCrdNoState) {
            paymentID = ((ReadCrdNoState)sourceState).getPayMenuId();
            payNumber = ((ReadCrdNoState)sourceState).getCrdAmount();
        } else if (sourceState instanceof AlipayProcessState) {
            paymentID = ((AlipayProcessState)sourceState).getPayID();
            payNumber = ((AlipayProcessState)sourceState).getPayAmount();
        } else if (sourceState instanceof AlipayState) {
            paymentID = ((AlipayState)sourceState).getPayID();
            payNumber = ((AlipayState)sourceState).getPayAmount();
        }else if (sourceState instanceof CmpayState) {
            paymentID = ((CmpayState)sourceState).getPayID();
            payNumber = ((CmpayState)sourceState).getPayAmount();
        }else if (sourceState instanceof WeiXinProcessState) {
            paymentID = ((WeiXinProcessState)sourceState).getPayID();
            payNumber = ((WeiXinProcessState)sourceState).getPayAmount();
        }else if (sourceState instanceof UnionPayProcessState) {
            paymentID = ((UnionPayProcessState)sourceState).getPayID();
            payNumber = ((UnionPayProcessState)sourceState).getPayAmount();
        }

        //Bruce/2003-12-17
        //负项销售、paidout等状态时，输入的支付金额前面自动加上一个减号
        boolean paidout = "4".equals(curTransaction.getDealType2()) &&
                curTransaction.getDaiFuAmount().compareTo(ZERO) < 0;
        if (paidout || app.getReturnItemState()
                || "B".equals(curTransaction.getState1())
                || "X".equals(curTransaction.getState1())) // Nitori退着付殘金交易
            if (!payNumber.startsWith("-"))
                payNumber = "-" + payNumber;

        Payment curPayment = Payment.queryByPaymentID(paymentID);
        if (curPayment == null) {
            paymentAvailable = false;
            return;
        }

        if (curPayment.isCreditCard() && curTransaction.existsCreditCardPayment()) {
            paymentAvailable = false;
            setWarningMessage(CreamToolkit.getString("SupportOnlyOneCreditCard"));
            return;
        }

        String shePayId = Payment.queryDownPaymentId();
        if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.WHOLESALECLEARING_STATE
                && paymentID.equals(shePayId)) {
            // 团购结算交易不可以使用赊账支付方式
            paymentAvailable = false;
            setWarningMessage(MessageFormat.format(CreamToolkit.GetResource().getString(
                    "WholesaleWaring4"), curPayment.getPrintName()));
            return;
        }
        //if (CreamSession.getInstance().getWorkingState() != WorkingStateEnum.WHOLESALE_STATE
        //        && paymentID.equals(shePayId)
        //        && (curTransaction.getMemberID() == null
        //        || curTransaction.getMemberID().trim().equals(""))) {
        //    //只有会员才能使用赊账支付
        //    paymentAvailable = false;
        //    setWarningMessage(MessageFormat.format(CreamToolkit.GetResource().getString(
        //            "SheMessage2"), new String[] { curPayment.getPrintName() }));
        //    return;
        //}
        if (curPayment.getPaymentID().equals(PARAM.getRebatePaymentID())) {
            if (PARAM.isOnlyMemberCanUseRebate() && curTransaction.getMemberID() == null) {
                paymentAvailable = false;
                setWarningMessage(CreamToolkit.GetResource().getString("OnlyMemberUseRebate"));
                return;
            }
//            //每次最多能用还元金支付2000元
//            if (new HYIDouble(payNumber).compareTo(new HYIDouble(2000)) > 0) {
//                paymentAvailable = false;
//                setWarningMessage(CreamToolkit.GetResource().getString("TooManyRebate"));
//                return;
//            }
        }
        //Bruce/20030416
        //一般交易不允许使用“赊帐”支付。（Modified for 灿坤）
//        if (GetProperty.getIsForbidShePayment()) { //James/20080228 //參數控制
//            if (paymentID.equals(GetProperty.getShePaymentID())
//                    && curTransaction.isPeiDa()) {
//                setWarningMessage(CreamToolkit.GetResource().getString("CannotUseCreditPayment"));
//                paymentAvailable = false;
//                return;
//            }
//        }
        //Bruce/20080829 修改上面錯誤的邏輯。IsForbidShePayment=yes時，一般交易不允许使用“赊帐”支付，除了配達交易以外。
        //if (paymentID.equals(GetProperty.getShePaymentID())
        if (paymentID.equals(shePayId)
                && !PARAM.isAllowShePayment() // getIsForbidShePayment()
                && !curTransaction.isPeiDa()) {
            setWarningMessage(CreamToolkit.GetResource().getString("CannotUseCreditPayment"));
            paymentAvailable = false;
            return;
        }

        int state = curTransaction.addPayment(curPayment);
        //System.out.println(curPayment);
        /**
         *  if payment is null, then payment arraylist is clear
         *  if payment is not exist in payment arraylist and
         *        payment arraylist size less than 4,
         *     then payment arraylist add the payment, return 2
         *  if payment is exist in payment arraylist,
         *     then return 1
         *  if payment is not exist in payment arraylist and
         *        payment arraylist size more than 4,
         *     then return 0
         **/
        if (state == 0) {
            return;
        }
        lastestPayment = curPayment;
        curTransaction.setLastestPayment(lastestPayment);
        //System.out.println(lastestPayment);
        paymentAvailable = true;
        if (state == 1) {
            for (int i = 1; i < 5; i++) {
                payno = "PAYNO" + i;
                payamt = "PAYAMT" + i;
                if (curTransaction.getFieldValue(payno) == null) {
                    break;
                }
                if (((String)curTransaction.getFieldValue(payno)).equals(paymentID)) {
                    Payment payment = Payment.queryByPaymentID(paymentID);
                    //HYIDouble denomination = new HYIDouble(payment.getDenomination().doubleValue());
                    HYIDouble denomination = payment.getDenomination();
                    curTransaction.setFieldValue(
                            payamt,
                            ((HYIDouble)curTransaction.getFieldValue(payamt))
                                    .addMe((new HYIDouble(payNumber)).multiply(denomination))
                                    .setScale(2, BigDecimal.ROUND_HALF_UP));
                }
            }
        } else if (state == 3) { // 如果是信用卡支付，用新的覆盖旧的
            for (int i = 1; i < 5; i++) {
                payno = "PAYNO" + i;
                payamt = "PAYAMT" + i;
                if (curTransaction.getFieldValue(payno) == null) {
                    break;
                }
                Payment payment = Payment.queryByPaymentID(paymentID);
                Payment creditCardPayment = Payment.queryCreditCardPayment();
                Payment currPayment = Payment.queryByPaymentID((String)curTransaction.getFieldValue(payno));
                if (currPayment.equals(creditCardPayment) && payment.equals(creditCardPayment)) {
                    HYIDouble denomination = payment.getDenomination();
                    curTransaction.setFieldValue(payamt,
                            new HYIDouble(payNumber).multiply(denomination).setScale(2, BigDecimal.ROUND_HALF_UP));
                    curTransaction.setFieldValue(payno, paymentID);
                }
            }
        }
        ArrayList paynoArray = new ArrayList();
        ArrayList payamtArray = new ArrayList();
        if (state == 2) {
            if (!curPayment.isChangeable()) {
                paynoArray.add(paymentID);
                Payment payment = Payment.queryByPaymentID(paymentID);
                //HYIDouble denomination = new HYIDouble(payment.getDenomination().doubleValue());
                HYIDouble denomination = payment.getDenomination();
                payamtArray.add((new HYIDouble(payNumber)).multiply(denomination).setScale(2, BigDecimal.ROUND_HALF_UP));
                for (int i = 1; i < 5; i++) {
                    payno = "PAYNO" + i;
                    payamt = "PAYAMT" + i;
                    if (curTransaction.getFieldValue(payno) == null) {
                        break;
                    }
                    paynoArray.add(curTransaction.getFieldValue(payno));
                    payamtArray.add(curTransaction.getFieldValue(payamt));
                }
                for (int i = 0; i < paynoArray.size(); i++) {
                    payno = "PAYNO" + (i + 1);
                    payamt = "PAYAMT" + (i + 1);
                    curTransaction.setFieldValue(payno, paynoArray.get(i));
                    curTransaction.setFieldValue(payamt, payamtArray.get(i));
                }
            } else {
                for (int i = 1; i < 5; i++) {
                    payno = "PAYNO" + i;
                    payamt = "PAYAMT" + i;
                    if (curTransaction.getFieldValue(payno) == null
                            || curTransaction.getFieldValue(payno).toString().trim().equals("")) {
                        curTransaction.setFieldValue(payno, paymentID);
                        Payment payment = Payment.queryByPaymentID(paymentID);
                        //HYIDouble denomination = new HYIDouble(payment.getDenomination().doubleValue());
                        HYIDouble denomination = payment.getDenomination();
                        curTransaction.setFieldValue(
                                payamt,
                                (new HYIDouble(payNumber)).multiply(denomination).setScale(
                                        2,
                                        BigDecimal.ROUND_HALF_UP));
                        break;
                    }
                }
            }
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (!paymentAvailable) {
            if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.WHOLESALECLEARING_STATE)
                WarningState.setExitState(WholesaleClearingState.class);
            else
                WarningState.setExitState(SummaryState.class);
            return Warning2State.class;

        } else if (lastestPayment.isSlipping()) {
            return SlippingState.class;

        } else if (app.getTrainingMode() && lastestPayment.isCreditCard()) {
            //Bruce/20101121/ 在訓練模式下，禁止進入CAT連線刷卡模式。
            return CardReadingState.class;

        } else if (lastestPayment.isCreditCardWithoutCATConnected()) {
            // 存在Offline CAT的时候，才让收银员在我们的收银机上刷卡
            return CardReadingState.class;

        } else {
            return Paying3State.class;
        }
    }

    public void setPaymentAvailable(boolean paymentAvailable) {
        this.paymentAvailable = paymentAvailable;
    }

    public boolean getPaymentAvailable() {
        return paymentAvailable;
    }
}

