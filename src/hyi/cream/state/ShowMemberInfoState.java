/*
 * Created on 2003-5-28
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.state;

import hyi.cream.CreamSession;
//import hyi.cream.POSButtonHome;
import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.MemberCardBase;
import hyi.cream.dac.Member;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.state.periodical.HasBookedState;
import hyi.cream.state.periodical.PeriodicalNoReadyState;
import hyi.cream.state.wholesale.WholesaleClearingState;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.PopupMenuListener;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.ArrayList;
import java.util.EventObject;

public class ShowMemberInfoState extends State implements PopupMenuListener {
    private Member member;
    private MemberCardBase memberCardBase;
    static ShowMemberInfoState instance;
    private ArrayList menu = new ArrayList();
    private PopupMenuPane popup;
    private boolean isShowMemberActionBonus;

    public static ShowMemberInfoState getInstance() {
        try {
            if (instance == null) {
                instance = new ShowMemberInfoState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    public ShowMemberInfoState() throws InstantiationException {
        isShowMemberActionBonus = PARAM.getShowMemberActionBonus();
    }

    public void entry(EventObject event, State sourceState) {
        popup = POSTerminalApplication.getInstance().getPopupMenuPane();
        menu.clear();
        memberCardBase = GetMemberNumber2State.getMember();
        if (memberCardBase != null) {
//            menu.add(CreamToolkit.GetResource().getString("MemberNo") + ":" + memberCardBase.getMemNo());
            menu.add(CreamToolkit.GetResource().getString("CardNo") + ":" + 
            		(memberCardBase.getFcardnumber() == null ? "" : memberCardBase.getFcardnumber()));
            menu.add(CreamToolkit.GetResource().getString("Name") + ":" + 
            		(memberCardBase.getFmemname() == null ? "" : memberCardBase.getFmemname()));
            menu.add(CreamToolkit.GetResource().getString("TelephoneNo") + ":" + 
            		(memberCardBase.getFtelno() == null ? "" : memberCardBase.getFtelno()));
            if (memberCardBase.getEarningamt() == null || memberCardBase.getEarningamt().compareTo(new HYIDouble(0)) == 0)
            	menu.add(CreamToolkit.GetResource().getString("SheMessage") + ":" + new HYIDouble(0).setScale(2));
            else if (memberCardBase.getEarningamt() != null && memberCardBase.getEarningamt().compareTo(new HYIDouble(0)) > 0)
                menu.add(CreamToolkit.GetResource().getString("SheMessage") + ":" + memberCardBase.getEarningamt().setScale(2));
            else 
            	menu.add(CreamToolkit.GetResource().getString("PrePayMessage") + ":" + memberCardBase
                    .getEarningamt().negate().setScale(2));
            	
            //menu.add("身份证号: ");
            //menu.add(" " + memberCardBase.getIdentityID());
            //menu.add("");
            if (memberCardBase.isNeedConfirm()) {
                menu.add(CreamToolkit.GetResource().getString("CannotLookUpMember"));
                menu.add(CreamToolkit.GetResource().getString("SelectReceive"));
                menu.add(CreamToolkit.GetResource().getString("SelectCancle"));
            } else { 
                menu.add("");
                menu.add("");
                menu.add(CreamToolkit.GetResource().getString("PressClearCloseWindow"));
            }
            popup.setMenu(menu);
            popup.setPopupMenuListener(this);
            popup.setVisible(true);
            popup.repaint();
            return;
        }

        member = GetMemberNumberState.getMember();
        if (member != null) {
            menu.add(CreamToolkit.GetResource().getString("MemberNo") + ":" + member.getID());
            menu.add(CreamToolkit.GetResource().getString("CardNo") + ":" + member.getMemberCardID());
            menu.add(CreamToolkit.GetResource().getString("Name") + ":" + member.getName());
            menu.add(CreamToolkit.GetResource().getString("TelephoneNo") + ":" + member.getTelephoneNumber());
            menu.add(CreamToolkit.GetResource().getString("IdentityCardNo") + ":" + member.getIdentityID());
            if (isShowMemberActionBonus)
                menu.add(CreamToolkit.GetResource().getString("AccumulativeTotal") + ":" + member
                    .getMemberActionBonus());
            //menu.add("身份证号: ");
            //menu.add(" " + member.getIdentityID());
            //menu.add("");
            if (member.isNeedConfirm()) {
                menu.add(CreamToolkit.GetResource().getString("CannotLookUpMember"));
                menu.add(CreamToolkit.GetResource().getString("SelectReceive"));
                menu.add(CreamToolkit.GetResource().getString("SelectCancle"));
            } else { 
                menu.add("");
                menu.add("");
                menu.add(CreamToolkit.GetResource().getString("PressClearCloseWindow"));
            }
            popup.setMenu(menu);
            popup.setPopupMenuListener(this);
            popup.setVisible(true);
            popup.repaint();
        }
    }

    /**
     * 当在PopupMenuPane按数字键加回车，或者按了清除键了以后，都会callback此方法。
     */
    public void menuItemSelected() {
        int selectIndex = popup.getSelectedNumber();
        if (memberCardBase != null && memberCardBase.isNeedConfirm()) {
            // selectIndex = 0 时表示选择 "1：接受"
            // selectIndex = 1 时表示选择 "2：取消"
            if (selectIndex == 1) {
            	memberCardBase = null;
                POSTerminalApplication.getInstance().getCurrentTransaction().setMemberID("");
                POSTerminalApplication.getInstance().getCurrentTransaction().setBeforeTotalRebateAmt(new HYIDouble(0.00));
            }
            if (selectIndex == 0 || selectIndex == 1) {
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome2.getInstance().buttonPressed(e);
            } else {
                popup.setMenu(menu);
                popup.setPopupMenuListener(this);
                popup.setVisible(true);
                popup.repaint();
            }
            return;
        } else if (member != null && member.isNeedConfirm()) {
            // selectIndex = 0 时表示选择 "1：接受"
            // selectIndex = 1 时表示选择 "2：取消"
            if (selectIndex == 1) {
            	member = null;
                POSTerminalApplication.getInstance().getCurrentTransaction().setMemberID("");
                POSTerminalApplication.getInstance().getCurrentTransaction().setBeforeTotalRebateAmt(new HYIDouble(0.00));
            }
            if (selectIndex == 0 || selectIndex == 1) {
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome2.getInstance().buttonPressed(e);
            } else {
                popup.setMenu(menu);
                popup.setPopupMenuListener(this);
                popup.setVisible(true);
                popup.repaint();
            }
            return;
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome2.getInstance().buttonPressed(e);
        }
    }

    public Class exit(EventObject event, State sinkState) {
    	POSTerminalApplication.getInstance().getMessageIndicator().setMessage("");
        menu.clear();
        popup.setMenu(menu);
        popup.setVisible(false);
        if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_ORDER_STATE) {
        	return PeriodicalNoReadyState.class;
        } else if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_DRAW_STATE) {
        	return HasBookedState.class;
        } else if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_RETURN_STATE) {
        	return HasBookedState.class;
        } else if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.WHOLESALECLEARING_STATE) {
        	CreamSession.getInstance().setAttribute(WorkingStateEnum.WHOLESALECLEARING_STATE, "Member",
                memberCardBase);
        	return WholesaleClearingState.class;
        }
        return IdleState.class;
    }
}