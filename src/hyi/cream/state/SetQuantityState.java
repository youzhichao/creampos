
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.TaxType;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.state.periodical.GetOrderQtyState;
import hyi.cream.state.periodical.OrderQtyReadyState;
import hyi.cream.state.periodical.PeriodicalNoNumberingState;
import hyi.cream.state.periodical.PeriodicalNoReadyState;
import hyi.cream.uibeans.QuantityButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.EventObject;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class SetQuantityState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    private String numberString         = "";
    private HYIDouble quantity         = new HYIDouble(0);
    private boolean warning             = false;
    static SetQuantityState setQuantityState = null;
    private LineItem curLineItem        = null;
    private boolean isDaishou           = false;

    public static SetQuantityState getInstance() {
        try {
            if (setQuantityState == null) {
                setQuantityState = new SetQuantityState();
            }
        } catch (InstantiationException ex) {
        }
        return setQuantityState;
    }

    /**
     * Constructor
     */
    public SetQuantityState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
    	WarningState.setExitState(IdleState.class);
    	if (sourceState instanceof GetOrderQtyState)
    		WarningState.setExitState(OrderQtyReadyState.class);
    	else if (sourceState instanceof PeriodicalNoNumberingState)
    		WarningState.setExitState(PeriodicalNoReadyState.class);
        curLineItem = app.getCurrentTransaction().getCurrentLineItem();
        if (curLineItem == null) {
			return;
		}
        isDaishou = ("I".equals(curLineItem.getDetailCode())) ? true : false;
        HYIDouble pluMaxQuantity = new HYIDouble(PARAM.getPluMaxQuantity());
        if (event != null && event.getSource() instanceof QuantityButton
        		&& sourceState instanceof NumberingState) {
            numberString = ((NumberingState)sourceState).getNumberString();
        } else if (event != null && event.getSource() instanceof QuantityButton 
            		&& sourceState instanceof PeriodicalNoNumberingState) {
            numberString = ((PeriodicalNoNumberingState)sourceState).getNumberString();
	    	int ordIssnQty = (Integer) CreamSession.getInstance().getAttribute(
	    			WorkingStateEnum.PERIODICAL_ORDER_STATE, "OrderIssnQty");
	    	
			LineItem li = (LineItem) CreamSession.getInstance().getAttribute(
					WorkingStateEnum.PERIODICAL_ORDER_STATE , "CurLineItem");
			String desc = li.getDescriptionAndSpecification();
			li.setDescriptionAndSpecification(desc.substring(0, desc.lastIndexOf("."))
					+ "." + MessageFormat.format(CreamToolkit.GetResource().getString(
							"OrderPerioicalMessage1"), numberString));

	    	numberString = String.valueOf(ordIssnQty * Integer.parseInt(numberString));
	    } else if (sourceState instanceof GetOrderQtyState) {
	    	numberString = (String) CreamSession.getInstance().getAttribute(
	    			WorkingStateEnum.PERIODICAL_ORDER_STATE, "OrderQty");
	    }
        quantity = new HYIDouble(numberString);
        // 负项销售也可以修改数量
        if (curLineItem != null && !curLineItem.getPrinted() && curLineItem.getQuantityEnabled()
        		&& app.getReturnItemState()) {
            quantity = quantity.setScale(2, 4);
            curLineItem.setQuantity(quantity.negate());
            setWarning(false);
        } else if (curLineItem == null || curLineItem.getPrinted() 
//            || !(curLineItem.getDetailCode().equals("S") 
//            		|| curLineItem.getDetailCode().equals("I")
//            		|| curLineItem.getDetailCode().equals("J") //期刊预订
//            		)
			|| !curLineItem.getQuantityEnabled()) {
//            || (curLineItem.getWeight() != null && curLineItem.getWeight().compareTo(new HYIDouble(0)) != 0)) { //Bruce/2003-08-14
            setWarningMessage(CreamToolkit.GetResource().getString("NumberWarning"));
            setWarning(true);
        } else if (curLineItem.getRemoved()){
            setWarningMessage(CreamToolkit.GetResource().getString("NumberWarning"));
            setWarning(true);
        }else if (quantity.intValue() <= 0) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("number1"));
            setWarning(true);
        } else if (quantity.compareTo(pluMaxQuantity) == 1) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("number2") + pluMaxQuantity);
            setWarning(true);
        } else if (curLineItem.getDiscountType() != null && 
        		curLineItem.getDiscountType().equalsIgnoreCase("L") && quantity.intValue() > 1) {
            //限量销售商品每次只能录入一单品
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("LimitQtyNumber"));
			setWarning(true);
        } else {
            quantity = quantity.setScale(2, 4);
            curLineItem.setQuantity(quantity);
            setWarning(false);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (getWarning()) {
            return WarningState.class;
        } else {
        	WarningState.setExitState(null);

            Transaction curTransaction = app.getCurrentTransaction();
            if (curTransaction == null || curLineItem == null) {
				return IdleState.class;
			}

            if (isDaishou) {
                curTransaction.setDaiShouAmount(curLineItem.getAmount());
                curLineItem.setAfterDiscountAmount(curLineItem.getAmount());

                try {
                    app.getCurrentTransaction().changeLineItem(-1, curLineItem);
                } catch (LineItemNotFoundException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("LineItem not found at " + this);
                }

                return IdleState.class;
            } else {
                //Bruce/20030324
                curLineItem.setAfterDiscountAmount(curLineItem.getAmount());
                curLineItem.setAfterSIAmount(curLineItem.getAmount());
                String typeId = curLineItem.getTaxType();
                curLineItem.caculateAndSetTaxAmount();
                    
                //System.out.println("plu price = " + lineItem.getUnitPrice());
                //System.out.println("tax = " + lineItem.getTaxAmount());
                
                // setTaxAmountN to tranhead
                HYIDouble amt = CreamToolkit.getAmountMethod(curTransaction, "getTaxAmount" + typeId);
                if (amt != null){
                    amt = amt.addMe(curLineItem.getTaxAmount());
                    CreamToolkit.setAmountMethod(curTransaction, "setTaxAmount" + typeId, amt);
                } 
                
                /*if (curLineItem.getDiscountType() != null && 
                		curLineItem.getDiscountType().equalsIgnoreCase("O") ) {
              //变价时输入数量不能有还原金
			 
					curLineItem.setAddRebateAmount(new HYIDouble(0.00));
       			}
       			else {
                
               		curLineItem.setAddRebateAmount(curLineItem.getAfterDiscountAmount()
		               	.multiply(curLineItem.getRebateRate()).setScale(0, BigDecimal.ROUND_HALF_UP));
       			}*/
       				curLineItem.setAddRebateAmount(curLineItem.getAfterDiscountAmount()
                		.multiply(curLineItem.getRebateRate())
                        .setScale(PARAM.getRebateAmountScale(), BigDecimal.ROUND_HALF_UP));
                try {
                    app.getCurrentTransaction().changeLineItem(-1, curLineItem);
                } catch (LineItemNotFoundException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("LineItem not found at " + this);
                }

	            CreamToolkit.showText(app.getCurrentTransaction(), 0);
                return MixAndMatchState.class;
            }
        }
    }

    public void setWarning(boolean warning) {
        this.warning = warning;
    }

    public boolean getWarning() {
        return warning;
    }
}


