/*
 * Created on 2004-8-2
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package hyi.cream.state;

import java.util.*;
import hyi.cream.*;
import hyi.cream.util.*;

/**
 * @author pyliu
 *
 */
public class SalemanNumberingState extends SomeAGNumberingState {

	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();

	static SalemanNumberingState salemanNumberingState = null;

	public static SalemanNumberingState getInstance() {
		try {
			if (salemanNumberingState == null) {
				salemanNumberingState = new SalemanNumberingState();
			}
		} catch (InstantiationException ex) {
		}
		return salemanNumberingState;
	}

	/**
	 * Constructor
	 */
	SalemanNumberingState() throws InstantiationException {
	}
	
	public boolean checkAlphanumericData() {
		if (PARAM.getSalemanIDMaxLen() == 0) {
			return true;
		}
		String buf = getAlphanumericData();//get last value
		if (buf.length() >= PARAM.getSalemanIDMaxLen()) {
			app.getWarningIndicator().setMessage(res.getString("SalemanIDTooLong"));
			java.awt.Toolkit.getDefaultToolkit().beep();
			return false;
 		}
		return true;
	}
}
