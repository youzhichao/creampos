package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Payment;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;

import java.text.MessageFormat;
import java.util.EventObject;
import java.util.ResourceBundle;

public class ReadCrdNoState extends State {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    private String tempCardNumber = "";

    static ReadCrdNoState readCrdNoState = null;

    //private Transaction curTran = null;

    private ResourceBundle res = CreamToolkit.GetResource();

    private String crdAmount;

    private String payMenuId;

    private int maxCrdNoLength = 20;

    public static ReadCrdNoState getInstance() {
        try {
            if (readCrdNoState == null) {
                readCrdNoState = new ReadCrdNoState();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return readCrdNoState;
    }

    public ReadCrdNoState() throws InstantiationException {
        //maxCrdNoLength = GetProperty.getCrdNoLength();
    }

    @Override
    public void entry(EventObject event, State sourceState) {
        //Transaction curTran = app.getCurrentTransaction();
        if (sourceState instanceof Numbering2State) {
            crdAmount = ((Numbering2State) sourceState).getNumberString();
            payMenuId = ((Numbering2State) sourceState).getPayMenuId();
            Payment payment = Payment.queryByPaymentID(payMenuId);
            String message;
            if (payment.isRecordingCardNumber())
                message = MessageFormat.format(res.getString("InputCrdNo2"), payment.getPrintName());
            else
                message = res.getString("InputCrdNo");
            app.getMessageIndicator().setMessage(message);
        } else if (sourceState instanceof ReadCrdNoState) {
//            if (tempCardNumber.length() == 0) {
//                Payment payment = Payment.queryByPaymentID(payMenuId);
//                String message = "";
//                if (payment.getPaymentType().charAt(5) == '1')
//                    message = MessageFormat.format(res.getString("InputCrdNo2"), new String[]{ payment.getPrintName()});
//                else
//                    message = res.getString("InputCrdNo");
//                app.getMessageIndicator().setMessage(message);
//            }
        }
    }

    @Override
    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton) event.getSource();
            // System.out.println(" OverrideAmountState exit number" +
            // pb.getNumberLabel());
            if (pb.getNumberLabel().equals("-")
                    || (pb.getNumberLabel().equals(".") && tempCardNumber
                            .length() == 0)) {
                // System.out.println(" OverrideAmountState exit number 1 "
                // +tempPrice);
            } else {
                tempCardNumber = tempCardNumber + pb.getNumberLabel();
                // System.out.println(" OverrideAmountState exit number 2"
                // +tempPrice);
            }
            app.getMessageIndicator().setMessage(tempCardNumber);
            return ReadCrdNoState.class;
        }

        if (event.getSource() instanceof ClearButton) {
            if (tempCardNumber.length() == 0)
                return ReadCrdNoState.class;
            tempCardNumber = "";
            app.getMessageIndicator().setMessage(tempCardNumber);
            return ReadCrdNoState.class;
        }

        if (event.getSource() instanceof EnterButton) {
            Transaction curTran = app.getCurrentTransaction();
            if (maxCrdNoLength == 0) { // 不限制
                curTran.setPayCardNumber(tempCardNumber);
            } else if (tempCardNumber.trim().length() < maxCrdNoLength) {
                Payment payment = Payment.queryByPaymentID(payMenuId);
                String message;
                if (payment.isRecordingCardNumber())
                    message = MessageFormat.format(res.getString("InputCrdNoError2"),
                            payment.getPrintName());
                else
                    message = res.getString("InputCrdNoError");
                app.getMessageIndicator().setMessage(message);
                tempCardNumber = "";
                return ReadCrdNoState.class;
            } else if (tempCardNumber.trim().length() > maxCrdNoLength) {
                Payment payment = Payment.queryByPaymentID(payMenuId);
                String message;
                if (payment.isRecordingCardNumber())
                    message = MessageFormat.format(res.getString("InputCrdNoTooLong2"),
                            payment.getPrintName(), String.valueOf(maxCrdNoLength));
                else
                    message = res.getString("InputCrdNoTooLong");
                app.getMessageIndicator().setMessage(message);
                tempCardNumber = "";
                return ReadCrdNoState.class;
            } else {
                curTran.setPayCardNumber(tempCardNumber);
            }
        }
        tempCardNumber = "";
        return Paying1State.class;
    }

    public String getCrdAmount() {
        return crdAmount;
    }

    public String getPayMenuId() {
        return payMenuId;
    }
}
