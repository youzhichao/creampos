package hyi.cream.state;

import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * PluGrpSubMatch definition class.
 * 提供一个商品群组促销配对类
 * 
 * @author Meyer 
 * @version 1.0
 */
public class PluGrpSubMatch extends PromMatch {
    /**
     * 更新记录
     * 
     * Meyer / 1.0 / 2003-01-09
     * 		1. Create the class file
     * 
     */
    
	private String groupID;
	private int noMatchedQty;
	private HashMap matchedPluQtyMap;	
	private HashMap pluPriceMap;	
	
	public PluGrpSubMatch(String aPromID, String aGroupID, int aGroupQty, 
		HashMap aPluPriceMap) {
		this.groupID = aGroupID;
		this.noMatchedQty = aGroupQty;
		this.matchedPluQtyMap = new HashMap();
		this.pluPriceMap = aPluPriceMap;
		
		this.setPromID(aPromID);
		this.setTotalAmt(new HYIDouble(0));
		this.setMatchedFlag(false);
	}
	
	public boolean groupMatch(String aPluCode, int aPluQty) {
		int matchedPluQty = 0;
		
		boolean isGroupMatched = noMatchedQty <= aPluQty;	
		matchedPluQty = isGroupMatched ? noMatchedQty : aPluQty;
		
		if (matchedPluQty > 0) {
			matchedPluQtyMap.put(aPluCode, new Integer(matchedPluQty));
			noMatchedQty -= matchedPluQty;	
			//计算商品的累计金额
			this.setTotalAmt(this.getTotalAmt().addMe(
				((HYIDouble)pluPriceMap.get(aPluCode)).multiply(
					new HYIDouble(matchedPluQty).setScale(2, BigDecimal.ROUND_HALF_UP)
				)
			));		
		}
		this.setMatchedFlag(isGroupMatched);
		return isGroupMatched;				
	}	
	
	public HashMap getMatchedPluQtyMap() {
		return this.matchedPluQtyMap;
	}	   

	public static void main(String[] args) {
	}
}
