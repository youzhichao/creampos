package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.groovydac.CardDetailChinaTrust;
import hyi.cream.state.cat.CATAuthSalesState;
import hyi.cream.state.cat.CATAuthType;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;

import org.apache.commons.lang.StringUtils;

/**
 * 前笔误打 State class.
 *
 * @author dai
 */
public class VoidReadyState extends State {

    // Singleton --
    private static VoidReadyState instance = new VoidReadyState();
    private VoidReadyState() {}
    public static VoidReadyState getInstance() { return instance; }

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private java.util.ResourceBundle res = CreamToolkit.GetResource();
    private boolean noVoidableTransaction;
    private Class<? extends State> exitState;

    /**
     * (VoidLast:ID2VR)
     */
    public void entry(EventObject event, State sourceState) {

        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();

            int transNo = Transaction.getNextTransactionNumber() - 1;
            Transaction trans = Transaction.queryByTransactionNumber(connection, getPOSNumber(), transNo);
            if (trans == null
                || (!trans.getDealType1().equals("0") || !trans.getDealType2().equals("0") ||
                    (!trans.getDealType3().equals("0") && !trans.getDealType3().equals("4")))) {
                showWarningMessage("NoVoidableTransaction");
                noVoidableTransaction = true;
                exitState = IdleState.class;
                return;
            }

            if (trans.existsCreditCardPayment()) {

                // 如果是中國信託CAT，而且前筆交易的調閱編號或授權銀行編碼是空的，則無法作廢
                if (POSPeripheralHome3.getInstance().existsChinaTrustCAT()) {
                    CardDetailChinaTrust chinaTrustInfo = (CardDetailChinaTrust)trans.getCardDetail().addendum;
                    if (StringUtils.isEmpty(chinaTrustInfo.getInvoiceNo()) ||
                        StringUtils.isEmpty(chinaTrustInfo.getHostId())) {
                        showWarningMessage("TransactionCannotVoid1"); // "前筆交易的調閱編號是空的，無法作廢，按[清除]或[確認]返回"
                        noVoidableTransaction = true;
                        exitState = IdleState.class;
                        return;
                    }
                }

                showMessage("VoidConfirmWithCreditCardAmount", trans.getCreditCardAmount().toString());
                CATAuthSalesState.getInstance().setAuthType(CATAuthType.VOID);
                if (trans.existsCATPayment())
                    exitState = CATAuthSalesState.class; // (VoidLast:VR2CA)
                else
                    exitState = TransactionVoidState.class;
            } else {
                showMessage("VoidConfirm");
                exitState = TransactionVoidState.class;
            }

            getApp().setCurrentTransaction(trans);
            fireTransactionChanged();

/* Old code --
            Iterator iter = LineItem.queryByTransactionNumber(connection, posNumber, transactionID);
            trans.setDealType1("0");
            trans.setDealType2("0");
            trans.setDealType3("1");
            LineItem lineItem;
            while (iter!= null && iter.hasNext()) {
                lineItem = (LineItem)iter.next();

                / * check lineitem detail code
                    "S":销售
                    "R":退货
                    "D":SI折扣
                    "M":Mix & Match折扣
                    "B":退瓶
                    "I":代收
                    "O":营业外商品
                    "Q":代付
                    "V":指定更正
                    "E":立即更正
                    "N":PaidIn
                    "T":PaidOut
                * /
                String s = lineItem.getDetailCode();
                if (s.equals("D")) {
                    continue;
                } else if (s.equals("M")) {
                    trans.setTotalMMAmount(lineItem.getAmount().negate());
                    continue;
                } / *else if (s.equals("V") || s.equals("E")) {
                    continue;
                }* /

                //lineItem.setDescription(PLU.queryByPluNumber(lineItem.getPluNumber()).getScreenName());
                try {
                    trans.addLineItem(lineItem, false);
                } catch (TooManyLineItemsException e) {
                }
            }

            //app.getItemList().setTransaction(Transaction.queryByTransactionNumber(transactionID));
            showMessage("VoidConfirm");
*/

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        // init newCashierID
        POSTerminalApplication.setNewCashierID("");

        if (noVoidableTransaction) { // (VoidLast:VR2ID)
            noVoidableTransaction = false;
            initCurrentTransaciton();
            clearWarningMessage();
            return IdleState.class;
        } else if (event.getSource() instanceof ClearButton) { // (VoidLast:VR2ID)
            initCurrentTransaciton();
            return IdleState.class;
        } else if (event.getSource() instanceof EnterButton) {
            app.getMessageIndicator().setMessage(res.getString("DataTransfers"));
            return exitState;
        } else
            return sinkState.getClass();
    }

    private void initCurrentTransaciton() {
        Transaction trans = app.getCurrentTransaction();
        trans.clearLineItem();
        trans.clear();
        trans.setDealType1("0");
        trans.setDealType2("0");
        trans.setDealType3("0");
        trans.setInvoiceNumber(PARAM.getInvoiceNumber());
        trans.setInvoiceID(PARAM.getInvoiceID());
        app.getItemList().setItemIndex(0);
    }
}
