package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Payment;
import hyi.cream.dac.SI;
import hyi.cream.dac.Transaction;
import hyi.cream.event.TransactionEvent;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.state.cat.CATAuthSalesFailedState;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.spos.Scanner;

import java.util.EventObject;
import java.util.ResourceBundle;
import java.util.List;
import java.util.ArrayList;

public class ReturnShowState extends State {
    static ReturnShowState returnShowState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private Class returnNumberClass;
    public Transaction refundTran ;
    public static ReturnShowState getInstance() {
        try {
            if (returnShowState == null) {
                returnShowState = new ReturnShowState();
            }
        } catch (InstantiationException ex) {
        }
        return returnShowState;
    }

    /**
     * Constructor
     */
    public ReturnShowState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        Transaction trans = app.getCurrentTransaction();
        refundTran = CreamSession.getInstance().getTransactionToBeRefunded(); // 準備被退的原交易
        if (event.getSource() instanceof EnterButton
            || event.getSource() instanceof EmptyButton     // (DownPayment: RN2RS)
            || event.getSource() instanceof Scanner) {
            if (sourceState instanceof ReturnNumberState) { // 舊退貨
                returnNumberClass = ReturnNumberState.class;
            } else if (sourceState instanceof ReturnNumber2State) { // Nitori版退貨
                returnNumberClass = ReturnNumber2State.class;
            }
            fillLineItemsToCurrTran(refundTran);
        } else if (event.getSource() instanceof ClearButton // 部份退貨中途按[清除]取消
            && sourceState instanceof ReturnOneState) {
            
            List<LineItem> itemsToBeRemoved = new ArrayList<LineItem>();
            for (LineItem li : app.getCurrentTransaction().lineItems()) {
                if (li.getVoidSequence() > 0)
                    itemsToBeRemoved.add(li);
                else
                    li.setRemoved(false);
            }
            for (LineItem item : itemsToBeRemoved)
                app.getCurrentTransaction().removeTmpLineItem(item);
            ItemList i = app.getItemList();
            i.selectFinished();
            
            trans.clear();
            trans.clearLineItem();
            // 如果不加以下代码，退货是会多打一张payment的发票
            trans.setDealType1("0");
            trans.setDealType2("3");
            trans.setDealType3("4");

            fillLineItemsToCurrTran(refundTran);
            app.getItemList().setItemIndex(0);
        } else if (event.getSource() instanceof ClearButton
                && sourceState instanceof ReturnAllState) {
                Object[] l = app.getCurrentTransaction().getLineItems();
            for (Object aL : l) {
                ((LineItem)aL).setRemoved(false);
            }
        } else if (event.getSource() instanceof ClearButton
                    && (sourceState instanceof ReturnSummaryState
                        || sourceState instanceof CATAuthSalesFailedState /* From (Refund1: CA2RT) */)
                    && !refundTran.isPeiDa())
        {
            trans.clear();
            trans.clearLineItem();
            
            fillLineItemsToCurrTran(refundTran);
            app.getItemList().setItemIndex(0);
        }
        trans.setBuyerNumber(refundTran.getBuyerNumber());
        if (refundTran.isPeiDa()) {
            app.getMessageIndicator().setMessage(res.getString("PeiDaReturnContinue")); //"按[确认]键继续"
        } else
            app.getMessageIndicator().setMessage(res.getString("ReturnSelect1")); //"请选择：（1）全退，（2）部分退"
        ReturnSummaryState rss = (ReturnSummaryState)StateMachine.getInstance().getStateInstance("hyi.cream.state.ReturnSummaryState");
        rss.setReturnReasonID(null);
        rss.setReturnReasonIDHm(null);
    }

    public Class exit(EventObject event, State sinkState) {
        Transaction trans = app.getCurrentTransaction();
        if (event.getSource() instanceof ClearButton) {
            app.getMessageIndicator().setMessage("");
            app.getWarningIndicator().setMessage("");
            trans.clear();
            trans.clearLineItem();
            trans.setTotalMMAmount(new HYIDouble(0));
            return returnNumberClass;
        } else if (CreamSession.getInstance().getTransactionToBeRefunded().isPeiDa()) {
            // 按NumberButton 或者 EnterButton 都可以到这里.
            // ClearButton, NumberButton,EnterButton以外的其他键，在starechar3.conf中屏蔽掉了
            return ReturnSummaryState.class;
        } else if (event.getSource() instanceof NumberButton) {
            if (((NumberButton)event.getSource()).getNumberLabel().equals("1")
                || ((NumberButton)event.getSource()).getNumberLabel().equals("2")) {
                //2015-03-04 @pingping
                if (refundTran.getAlipayList() != null && refundTran.getAlipayList().size() > 0
                        && ((NumberButton)event.getSource()).getNumberLabel().equals("2")) {
                    app.getWarningIndicator().setMessage(res.getString("AlipayCanNotReturn"));
                    return ReturnShowState.class;
                }
                if (refundTran.getCmpayList() != null && refundTran.getCmpayList().size() > 0
                        && ((NumberButton)event.getSource()).getNumberLabel().equals("2")) {
                    app.getWarningIndicator().setMessage(res.getString("CmpayCanNotReturn"));
                    return ReturnShowState.class;
                }
                if (refundTran.getWeiXinList() != null && refundTran.getWeiXinList().size() > 0
                        && ((NumberButton)event.getSource()).getNumberLabel().equals("2")) {
                    app.getWarningIndicator().setMessage(res.getString("WeiXinCanNotReturn"));
                    return ReturnShowState.class;
                }
                if (refundTran.getUnionPayList() != null && refundTran.getUnionPayList().size() > 0
                        && ((NumberButton)event.getSource()).getNumberLabel().equals("2")) {
                    app.getWarningIndicator().setMessage(res.getString("UnionPayCanNotReturn"));
                    return ReturnShowState.class;
                }
                 // "选择：（1）全退，（2）部分退"
                app.getMessageIndicator().setMessage("");
                return ReturnSelect1State.class;
            } else {
//                transactionID = "";                 
//                app.getMessageIndicator().setMessage("");
                app.getWarningIndicator().setMessage(res.getString("InputWrong"));
//                trans.Clear();
//                trans.clearLineItem();
//                trans.setTotalMMAmount(new HYIDouble(0));
                return ReturnShowState.class;
            }
//        } else if (event.getSource() instanceof EnterButton
//                && oldTran.isPeiDa()) {
//            return ReturnSummaryState.class;
        }

        return sinkState == null ? null : sinkState.getClass();
    }

    /**
     * 把要被退的原交易的商品，適當的填到current transaction的line items，以做為顯示原交易內容之用
     */
    private void fillLineItemsToCurrTran(Transaction sourceTran) {
        Transaction currentTrans = app.getCurrentTransaction();

        HYIDouble mmAmount = new HYIDouble(0);
        app.getItemList().setSelectionMode(ItemList.MULTIPLE_SELECTION);

        for (LineItem lineItem : sourceTran.lineItems()) {
            // "S":销售
            // "R":退货
            // "D":SI折扣
            // "T":ItemDiscount折扣
            // "M":Mix & Match折扣
            // "B":退瓶
            // "I":代售
            // "O":代收
            // "Q":代付
            // "V":指定更正
            // "E":立即更正
            if (lineItem.getDetailCode().equals("M")) {
                mmAmount = mmAmount.addMe(lineItem.getAmount());
                currentTrans.addAppliedMM(lineItem.getPluNumber(), lineItem.getAmount());

            } else if (lineItem.getDetailCode().equals("D")) {
                String siID = lineItem.getPluNumber(); 
                SI siDac = SI.queryBySIID(siID);
                if (SI.DISC_TYPE_INPUT_PERCENTAGE == siDac.getType()) {
                    // 开放折扣 折扣值应该从trandetail.discno中取得
                    siDac.setValue(new HYIDouble(lineItem.getDiscountNumber()));
                }
                currentTrans.addAppliedSI(siDac, lineItem.getAmount());
            }

            if ((!lineItem.getDetailCode().equals("S")
                 && !lineItem.getDetailCode().equals("R")
                 && !lineItem.getDetailCode().equals("I")
                 && !lineItem.getDetailCode().equals("O"))
                || lineItem.getRemoved()) {
                if (!sourceTran.isPeiDa()) {
                    // (不是'S','R','I','O')或(被刪除(更正))的items，在一般非配送交易中就不要顯示出來
                    continue;
                } else if (lineItem.getQuantity().intValue() < 0) {
                    // (不是'S','R','I','O')或(被刪除(更正))的items，如果數量是負數，在配送交易中要以被槓掉方式顯示出來
                    app.getItemList().setSelectedItem(lineItem.getLineItemSequence());
                    lineItem.setRemoved(true);
                }
            }
            try {
                LineItem li = (LineItem) lineItem.clone();
                currentTrans.addLineItem(li, false);
                if (li.getQuantity().intValue() < 0) {
                    ////li.makeNegativeValue();
                    //// 如果還有負數量商品，不要槓掉
                    ////app.getItemList().setSelectedItem(li.getLineItemSequence());
                    li.setRemoved(true);
                }
                
            } catch (TooManyLineItemsException e) {
            }
        }
        app.getItemList().selectFinished();

        // 計算transactionToBeRefunded的"淨支付"：將可找零支付, 減去找零金額. (溢收部份保留不動)
        // 然後將支付抄到currentTransaction的支付項, 並變成負的, 以便於全退的時候顯示退貨金額.
        sourceTran.calcNetPayment();
        int idx = 1;
        for (Payment payment : sourceTran.getNetPayments().keySet()) {
            currentTrans.setPayNumberByID(idx, payment.getPaymentID());
            currentTrans.setPayAmountByID(idx, sourceTran.getNetPayments().get(payment).negate());
            idx++;
        }
        for (; idx <= 4; idx++) {
            currentTrans.setPayNumberByID(idx, null);
            currentTrans.setPayAmountByID(idx, null);
        }

        currentTrans.setTotalMMAmount(mmAmount.negate());
        app.getItemList().setItemIndex(0);
        app.getItemList().setTransaction(currentTrans);
        currentTrans.setMemberID(sourceTran.getMemberID()); //Bruce/2009-10-07
        currentTrans.setAnnotatedId(sourceTran.getAnnotatedId());
        currentTrans.setAnnotatedType(sourceTran.getAnnotatedType());

        currentTrans.fireEvent(new TransactionEvent(currentTrans, TransactionEvent.TRANS_INFO_CHANGED));
    }
}
