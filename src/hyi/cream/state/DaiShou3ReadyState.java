package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.PLU;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;

public class DaiShou3ReadyState extends State {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private LineItem lineItem = null;
    private PLU plu = null;
    private String number = "";

    static DaiShou3ReadyState daiShou3ReadyState = null;

    public static DaiShou3ReadyState getInstance() {
        try {
            if (daiShou3ReadyState == null) {
                daiShou3ReadyState = new DaiShou3ReadyState();
            }
        } catch (InstantiationException ex) {
        }
        return daiShou3ReadyState;
    }

    /**
     * Constructor
     */
    public DaiShou3ReadyState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof DaiShou3IdleState && event.getSource() instanceof SelectButton) {
            Transaction curTran = app.getCurrentTransaction();
            number = ((DaiShou3IdleState)sourceState).getPluNo();
            plu = PLU.queryBarCode(number);
            lineItem = new LineItem();
            lineItem.setDescription(plu.getScreenName());
            lineItem.setDetailCode("Q");
            lineItem.setPluNumber(String.valueOf(number));

            lineItem.setNeedPhoneCardNumber(((DaiShou3IdleState)sourceState).needPhoneCardNumber());

            /*
             * Meyer/2003-02-21/
             */
            lineItem.setItemNumber(plu.getItemNumber());
    		lineItem.setIsScaleItem(plu.isWeightedPlu());
    		lineItem.setOpenPrice(plu.isOpenPrice());

            lineItem.setQuantity(new HYIDouble(1));
            // lineItem.setTaxType("0");
            lineItem.setTerminalNumber(curTran.getTerminalNumber());
            lineItem.setTransactionNumber(new Integer(Transaction.getNextTransactionNumber()));

            lineItem.setUnitPrice(plu.getUnitPrice());

            lineItem.setOriginalPrice(plu.getUnitPrice());

            lineItem.setAmount(plu.getUnitPrice());
            try {
                curTran.addLineItem(lineItem);
            } catch (TooManyLineItemsException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Too many LineItem exception at " + this);
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                    CreamToolkit.GetResource().getString("TooManyLineItems"));
            }
        }
    }

    public Class exit(EventObject event, State sinkState) {
        app.getMessageIndicator().setMessage("");
        return ReadPhoneCardNumberState.class;
    }
}
