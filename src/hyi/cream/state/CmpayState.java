package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Cmpay_detail;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.util.HttpHepler;
import hyi.spos.JposException;
import hyi.spos.Scanner;
import hyi.spos.events.DataEvent;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventObject;
import java.util.Random;
import java.util.ResourceBundle;

//移动支付扫描条码
public class CmpayState extends State {
	private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private ResourceBundle res            = CreamToolkit.GetResource();

    private String numberStr = "";
    private String payID = "";
    private String payAmount = "";
    private Indicator warningIndicator = app.getWarningIndicator();
    private Indicator messgeIndicator = app.getMessageIndicator();
    static CmpayState cmpayState = null;
    private String type = "";
    public static SimpleDateFormat HHmmssSSS = new SimpleDateFormat("HHmmssSSS");

    public static CmpayState getInstance() {
        try {
            if (cmpayState == null) {
                cmpayState = new CmpayState();
            }
        } catch (InstantiationException ex) {
        }
        return cmpayState;
    }

    public CmpayState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        Object eventSource = event.getSource();
        if (sourceState instanceof Numbering2State) {
        	numberStr = "";
            payID = ((CmpayButton)eventSource).getPaymentID();
        	warningIndicator.setMessage("");
            messgeIndicator.setMessage(res.getString("PleaseInputCmpayNumber"));
        }
        if (sourceState instanceof SummaryState) {
        	numberStr = "";
            payID = ((CmpayButton)eventSource).getPaymentID();
        	warningIndicator.setMessage("");
            messgeIndicator.setMessage(res.getString("PleaseInputCmpayNumber"));
        } else if (sourceState instanceof CmpayState) {
            if (eventSource instanceof NumberButton) {
                numberStr = numberStr + ((NumberButton) eventSource).getNumberLabel();
                warningIndicator.setMessage("");
                messgeIndicator.setMessage(numberStr);
            } else if (eventSource instanceof ClearButton) {
                numberStr = "";
                warningIndicator.setMessage("");
                messgeIndicator.setMessage(res.getString("PleaseInputCmpayNumber"));
            } else if (eventSource instanceof EnterButton || eventSource instanceof Scanner) {
                numberStr = "";
                messgeIndicator.setMessage(res.getString("PleaseInputCmpayNumber"));
            } 
        }
	}

	public Class exit(EventObject event, State sinkState) {
		Object eventSource = event.getSource();
        if (eventSource instanceof EnterButton || eventSource instanceof Scanner) {
        	if (eventSource instanceof EnterButton) {
        		if (numberStr.equals("")) {
        			warningIndicator.setMessage("");
        			return this.getClass();
        		}
                type = "0";
        	}
        	if (eventSource instanceof Scanner) {
        		try {
                    DataEvent dataEvent = (DataEvent)event;
                    numberStr = new String(((Scanner)eventSource).getScanData(dataEvent.seq));
                } catch (JposException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("Jpos exception at " + this);
                }
                type = "1";
        	}
            Transaction trans = app.getCurrentTransaction();
            payAmount = trans.getBalance().toString();
            String amt = new HYIDouble(payAmount).multiply(new HYIDouble(100)).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
            String posno = trans.getTerminalNumber().toString();
            if (posno.length() < 2) {
                posno = "0" + posno;
            }
            //调移动支付接口
            messgeIndicator.setMessage(res.getString("CmpayPay"));
            Random random = new Random();
            int a = random.nextInt(10);
            String mid = a + HHmmssSSS.format(new Date());
            String orderId = trans.getStoreNumber() + posno + mid;
            String result = HttpHepler.pay(orderId, numberStr, amt, trans.getCashierNumber(),"教育超市消费", mid);
//            String result = "<?xml version=\"1.0\" encoding=\"GBK\"?><MPAY><HEAD><MCODE>101439</MCODE><MID>00073104328803</MID><DATE>20150626</DATE><TIME>104328</TIME><PAYDAY>20450221</PAYDAY><MSGPRO>SF</MSGPRO><VER>0001</VER><SCH>rp</SCH><MSGATR>00</MSGATR><RCODE>000000</RCODE><DESC>SUCCESS</DESC><SAFEFLG>00</SAFEFLG><MAC></MAC><REQSYS>888009941110054</REQSYS></HEAD><BODY><ORDERID>10010311594311</ORDERID><AMT>1980</AMT><COUPAMT>1980</COUPAMT><VCHAMT>0</VCHAMT><CASHAMT>0</CASHAMT><RESERVER1>1</RESERVER1><RESERVER2>2</RESERVER2><SIGN>89e61ed41e148c84c5079a467160af333be89c04a2807d05001950a548e7ee18d8c6a97c580452c97184578f829e44e95c426b9e3d70ce2818f377a60ad27b17ee9bc94f7d690e383065e5ecc2f4580587f07a6ff0b25e0ed989d4ecd2ecc4c2ea821ae536444a72d1feb2f8c5f6b1250c3cce857d6668744fa6a3bf1c62fc22</SIGN></BODY><BODY><ORDERID>10010311594311</ORDERID></BODY></MPAY>";
            messgeIndicator.setMessage("");
            CreamToolkit.logMessage(result);
            if (result == null || result.equals("")) {
                warningIndicator.setMessage(res.getString("CmpayPayFailed"));
                CreamToolkit.logMessage(res.getString("CmpayPayFailed"));
                return this.getClass();
            }
            if (CreamToolkit.getValueFromXml(result, "<HEAD>","</HEAD>").equals("")) {
                warningIndicator.setMessage(result);
                return this.getClass();
            }
            if (!CreamToolkit.getValueFromXml(result, "<RCODE>", "</RCODE>").equals("000000")) {
                String desc = CreamToolkit.getValueFromXml(result, "<DESC>", "</DESC>");
                warningIndicator.setMessage(HttpHepler.unicodeToChinese(desc));
                CreamToolkit.logMessage(HttpHepler.unicodeToChinese(desc));
                return this.getClass();
            }
//            String orderid = CreamToolkit.getValueFromXml(result, "<ORDERID>", "</ORDERID>");
            String amtStr = CreamToolkit.getValueFromXml(result, "<AMT>", "</AMT>");
            String coupAmtStr = CreamToolkit.getValueFromXml(result, "<COUPAMT>", "</COUPAMT>");
            String vchAmtStr = CreamToolkit.getValueFromXml(result, "<VCHAMT>", "</VCHAMT>");
            String cashAmtStr = CreamToolkit.getValueFromXml(result, "<CASHAMT>", "</CASHAMT>");
            String phoneNumber = CreamToolkit.getValueFromXml(result, "<RESERVER1>", "</RESERVER1>");
            String mark  = CreamToolkit.getValueFromXml(result, "<RESERVER2>", "</RESERVER2>");
            amtStr = (new HYIDouble(amtStr).divide(new HYIDouble(100), 2)).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
            coupAmtStr = (new HYIDouble(coupAmtStr).divide(new HYIDouble(100), 2)).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
            vchAmtStr = (new HYIDouble(vchAmtStr).divide(new HYIDouble(100), 2)).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
            cashAmtStr = (new HYIDouble(cashAmtStr).divide(new HYIDouble(100), 2)).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
            addCmpayDetail(amtStr, orderId, trans, coupAmtStr,vchAmtStr,cashAmtStr, mid , phoneNumber,mark);

            return Paying1State.class;

        } else if (eventSource instanceof ClearButton) {
            if (numberStr.equals("")) {

            	warningIndicator.setMessage("");
                return SummaryState.class;
            } else {
                return this.getClass();
            }
            
        } else if (eventSource instanceof NumberButton) {
            return this.getClass();
        }
		return null;
	}

    public Cmpay_detail addCmpayDetail(String amt,String orderId, Transaction trans,String coupamt, String vchamt, String cashamt, String mid, String phoneNumber, String mark) {
        Cmpay_detail cmpay_detail;
        try{
            cmpay_detail = new Cmpay_detail();
            cmpay_detail.setSTORENUMBER(trans.getStoreNumber());
            cmpay_detail.setPOSNUMBER(trans.getTerminalNumber());
            cmpay_detail.setTRANSACTIONNUMBER(trans.getTransactionNumber());
            cmpay_detail.setZseq(trans.getZSequenceNumber());
            cmpay_detail.setAmt(new HYIDouble(amt));
            cmpay_detail.setOrderId(orderId);
            cmpay_detail.setmid(mid);
            cmpay_detail.setCoupamt(new HYIDouble(coupamt));
            cmpay_detail.setVchamt(new HYIDouble(vchamt));
            cmpay_detail.setCashamt(new HYIDouble(cashamt));
            cmpay_detail.setSYSTEMDATE(new Date());
            cmpay_detail.setphonenumber(phoneNumber);
            cmpay_detail.setmark(mark);
            trans.addCmPayList(cmpay_detail);
            CreamToolkit.logMessage("transactionNumber：" + cmpay_detail.getTRANSACTIONNUMBER() + "，tradeNo："
                    + cmpay_detail.getOrderId() + "，systemDate："
                    + cmpay_detail.getSYSTEMDATE().toString()+ " is add trans.addCmpayDetailList");
            return cmpay_detail;
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
            CreamToolkit.logMessage("Cmpay process Fail");
        }
        return null;
    }

    public String getPayAmount() {
        return payAmount;
    }
    public String getPayID() {
        return payID;
    }
}
