package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;

public class ShutdownState extends State {

    private static ShutdownState instance = new ShutdownState();

    private ShutdownState() {}

    public static ShutdownState getInstance() {
        return instance;
    }

    public void entry(EventObject event, State sourceSate) {
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        app.getPopupMenuPane().setVisible(false);
        showMessage("ShutDown");
        StateMachine.getInstance().setEventProcessEnabled(false);
        CreamToolkit.halt();
    }

    public Class exit(EventObject event, State sinkSate) {
        return ShutdownState.class;
    }
}
