package hyi.cream.state;

import java.util.*;

import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

/**
 * Created by IntelliJ IDEA.
 * User: gllg
 * Date: Aug 12, 2009
 * Time: 4:53:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShopShareSelectState extends State {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static ShopShareSelectState shopShareSelectState = null;
    public String shopNoString = "";
    public String shopCategoryString = "";
    public String shopShareString = "";

    public String getShopBarcode() {
        String prefix = "28";
        String content = CreamToolkit.fillZero(shopNoString, 6)       // shopNo length is 6
                + CreamToolkit.fillZero(shopCategoryString, 2)  // shopCategory length is 2
                + CreamToolkit.fillZero(shopShareString, 2);    // shopShare length is 2
        return prefix + content + CreamToolkit.getEAN13CheckDigit(prefix + content);
    }

    public static ShopShareSelectState getInstance() {
        try {
            if (shopShareSelectState == null) {
                shopShareSelectState = new ShopShareSelectState();
            }
        } catch (InstantiationException ex) {
        }
        return shopShareSelectState;
    }

    /**
     * Constructor
     */
    public ShopShareSelectState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        Object eventSource = event.getSource();
		if ((sourceState instanceof ShopCategorySelectState && eventSource instanceof EnterButton)
            || (sourceState instanceof ShopShareSelectState && eventSource instanceof ClearButton)) {
            shopShareString = "";
            this.shopNoString = ((ShopCategorySelectState) sourceState).shopNoString;
            this.shopCategoryString = ((ShopCategorySelectState) sourceState).shopCategoryString;
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputShopShare"));
        }

        if (eventSource instanceof NumberButton) {
			String s = ((NumberButton)eventSource).getNumberLabel();
            if (sourceState instanceof ShopShareSelectState) {
                shopShareString = shopShareString + s;
                app.getMessageIndicator().setMessage(shopShareString);
            }
        }

	}

	public Class exit(EventObject event, State sinkState) {
        Object eventSource = event.getSource();
		if (sinkState instanceof IdleState && eventSource instanceof ClearButton) {
            shopShareString = "";
		    app.getWarningIndicator().setMessage("");
            return ShopCategorySelectState.class;
        }
        if (eventSource instanceof NumberButton) {
            return ShopShareSelectState.class;
        } else if (eventSource instanceof EnterButton) {
            if ("".equals(shopShareString))
                return ShopShareSelectState.class;
            else
                return PluReadyState.class;

        } else {
            return WarningState.class;
        }

	}
}


