package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;

import java.util.EventObject;

public class ExtNumberingState extends State {
    private static ExtNumberingState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private static StringBuffer number = new StringBuffer();

    public static ExtNumberingState getInstance() {
        try {
            if (instance == null) {
                instance = new ExtNumberingState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public ExtNumberingState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
		//super.entry(event, sourceState);
        if (event != null && event.getSource() instanceof NumberButton) {
            number.append(((NumberButton)event.getSource()).getNumberLabel());
            app.getMessageIndicator().setMessage(number.toString());
            return;
        }
        app.getWarningIndicator().setMessage("");
    }

    public Class exit(EventObject event, State sinkState) {
		//super.exit(event, sinkState);
        if (event.getSource() instanceof EnterButton) {
            return InitialState.class;

        } else if (event.getSource() instanceof ClearButton) {
            number.setLength(0);
            return sinkState.getClass();
        }

        if (number.length() == 0) {
            app.getWarningIndicator().setMessage("");
        }
        return sinkState.getClass();
    }

    public String getNumber() {
        return number.toString();
    }
}
