package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Reason;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;

public class PaidOutReadyState extends State {        
    private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private LineItem lineItem             = null;
    private Reason r                      = null;
    private String number                 = "";
    private Class exitState               = null;

    static PaidOutReadyState paidOutReadyState      = null;  

    public static PaidOutReadyState getInstance() {
        try {
            if (paidOutReadyState == null) {
                paidOutReadyState = new PaidOutReadyState();
            }
        } catch (InstantiationException ex) {
        }
        return paidOutReadyState;
    }

    /**
     * Constructor
     */
    public PaidOutReadyState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
        //System.out.println("PaidOutReadyState entry");
        
        if (sourceState instanceof PaidOutIdleState
            && event.getSource() instanceof SelectButton) {
            Transaction curTran = app.getCurrentTransaction(); 
            number = ((PaidOutIdleState)sourceState).getPluNo();
            //System.out.println("number = " + number);
            r = Reason.queryByreasonNumberAndreasonCategory(number, "11");
            lineItem = new LineItem();
            lineItem.setDescription(r.getreasonName());
            lineItem.setDetailCode("T");
            lineItem.setPluNumber(String.valueOf(number));
            lineItem.setQuantity(new HYIDouble(0));
            //lineItem.setTaxType("0");
            lineItem.setTerminalNumber(curTran.getTerminalNumber());
            lineItem.setTransactionNumber(new Integer(curTran.getNextTransactionNumber()));

            lineItem.setUnitPrice(new HYIDouble(0.00));
			/*
			 * Meyer/2003-02-21/
			 */
			lineItem.setItemNumber(String.valueOf(number));
			lineItem.setOriginalPrice(new HYIDouble(0.00)); 

            lineItem.setAmount(new HYIDouble(0.00)); 
            try {
                curTran.addLineItem(lineItem);
                exitState = PaidOutOpenPriceState.class;
            } catch (TooManyLineItemsException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Too many LineItem exception at " + this);
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyLineItems"));
                exitState = PaidOutIdleState.class;
            }
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("PaidOutReadyState exit");

		app.getMessageIndicator().setMessage("");
        return exitState;
    }
}

