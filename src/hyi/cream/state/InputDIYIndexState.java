package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;

public class InputDIYIndexState extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private static InputDIYIndexState inputDIYIndexState;

	private String tempIndex = "";

	public static InputDIYIndexState getInstance() {
		try {
			if (inputDIYIndexState == null) {
				inputDIYIndexState = new InputDIYIndexState();
			}
		} catch (InstantiationException ex) {
		}
		return inputDIYIndexState;
	}

	public InputDIYIndexState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
		if (sourceState instanceof IdleState)
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString("InputDIYIndex"));
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof NumberButton) {
			NumberButton pb = (NumberButton) event.getSource();
			if (pb.getNumberLabel().equals("-")
					|| (pb.getNumberLabel().equals(".") && tempIndex.length() == 0)) {
			} else {
				tempIndex = tempIndex + pb.getNumberLabel();
			}
			app.getMessageIndicator().setMessage(tempIndex);
			return InputDIYIndexState.class;
		}
		if (event.getSource() instanceof ClearButton) {
			if (tempIndex.length() == 0) {
				return IdleState.class;
			}
			tempIndex = "";
			app.getMessageIndicator().setMessage(tempIndex);
			return IdleState.class;
		}

		if (event.getSource() instanceof EnterButton) {
			if (tempIndex.length() == 1) {
				// if (trans.getDIYOverride().containsKey(tempPrice))
				app.getItemList().setDIY(true, tempIndex);
				app.getItemList().setBackLabel("DIY " + tempIndex);
				System.out.println("*********** diyIndex : " + app.getItemList().getCurrentIndex());
			} else {
				app.getMessageIndicator().setMessage(
						CreamToolkit.GetResource().getString("InputDIYIndex"));
				tempIndex = "";
				return InputDIYIndexState.class;
			}
			tempIndex = "";
			return IdleState.class;
		}
		return IdleState.class;
	}
}
