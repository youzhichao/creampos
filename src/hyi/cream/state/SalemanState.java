package hyi.cream.state;

import java.util.*;

//import jpos.*;
//import jpos.events.*;

import hyi.cream.*;
import hyi.cream.dac.Transaction;
import hyi.cream.util.*;

/**
 * @author pyliu
 *
 */
public class SalemanState extends SomeAGReadyState {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	static SalemanState salemanState = null;

	public static SalemanState getInstance() {
		try {
			if (salemanState == null) {
				salemanState = new SalemanState();
			}
		} catch (InstantiationException ex) {
		}
		return salemanState;
	}

	/**
	 * Constructor
	 */
	SalemanState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
		//super.entry(event, sourceState);
		app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSalemanNumber"));
		app.getPopupMenuPane().setEnabled(true);
		app.getPopupMenuPane().clear();
		app.setEnabledPopupMenu(true);
		app.getItemList().repaint();
		if (app.getBeginState()) {
            CreamToolkit.showTurnKeyToOffPositionMessage();
			return;
		}
		
		app.getPopupMenuPane().setEnabled(true);
		app.getSystemInfo().setSalesManNumber("");
        showWarningMessage("");
		if (app.getScanSalemanNumber()
			&& !app.getSalemanChecked()) {
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("CheckSalemanWarning"));
		} else {
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSalemanNumber"));
		}       
	}

	public Class exit(EventObject event, State sinkState) {
		//super.exit(event, sinkState);
		Transaction.generateABrandNewTransaction();
		IdleState.getInstance().setStart(false);
		return sinkState.getClass();
	}

	public String getWarningMessage() {  
		if (app.getScanSalemanNumber()
			&& !app.getSalemanChecked()) {
			return CreamToolkit.GetResource().getString("CheckSalemanWarning");
		} else {
			return CreamToolkit.GetResource().getString("InputSalemanNumber");
		}
	}
}
