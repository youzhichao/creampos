package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.CreditType;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;

public class CardNumberingState extends State {

    private String cardNumber           = "";
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static CardNumberingState cardNumberingState = null;
	private int maxCrdNoLength = 20;

    public static CardNumberingState getInstance() {
        try {
            if (cardNumberingState == null) {
                cardNumberingState = new CardNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return cardNumberingState;
    }

    public CardNumberingState() throws InstantiationException {
		//maxCrdNoLength = GetProperty.getCrdNoLength();
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState.getClass() == CardReadingState.class) {
            cardNumber = "";
        }
		NumberButton pb = (NumberButton)event.getSource();
        if (cardNumber.length() < maxCrdNoLength)
        {
            cardNumber = cardNumber + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(cardNumber);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (sinkState instanceof Paying3State) {
            Transaction curTran = app.getCurrentTransaction();
            curTran.setCreditCardNumber(cardNumber);
            CreditType creditCardType = CreditType.checkCreditNo(cardNumber);
            if (creditCardType != null) {
                curTran.setCreditCardType(creditCardType.getNoType());
                //curTran.setCreditCardExpireDate(expirationDate.getTime());
            }
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("CardNumber") +
                app.getCurrentTransaction().getCreditCardNumber());
        }
        return sinkState.getClass();
    }
}