package hyi.cream.state;

import java.util.*;

import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

/**
 * Created by IntelliJ IDEA.
 * User: gllg
 * Date: Aug 12, 2009
 * Time: 4:48:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShopCategorySelectState extends State {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static ShopCategorySelectState shopCategorySelectState = null;
    public String shopNoString = "";
    public String shopCategoryString = "";


    public static ShopCategorySelectState getInstance() {
        try {
            if (shopCategorySelectState == null) {
                shopCategorySelectState = new ShopCategorySelectState();
            }
        } catch (InstantiationException ex) {
        }
        return shopCategorySelectState;
    }

    /**
     * Constructor
     */
    public ShopCategorySelectState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        Object eventSource = event.getSource();
		if ((sourceState instanceof ShopNoSelectState && eventSource instanceof EnterButton)
            || (sourceState instanceof NumberingState && eventSource instanceof ShopButton)
            || (sourceState instanceof ShopCategorySelectState && eventSource instanceof ClearButton)) {
            shopCategoryString = "";
            if (eventSource instanceof EnterButton)
                this.shopNoString = ((ShopNoSelectState) sourceState).shopNoString;
            else if (eventSource instanceof ShopButton)
                this.shopNoString = ((NumberingState) sourceState).getNumberString();
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputShopCategory"));
        }

        if (sourceState instanceof ShopShareSelectState && eventSource instanceof ClearButton) {
            shopCategoryString = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputShopCategory"));
        }

        if (eventSource instanceof NumberButton) {
			String s = ((NumberButton)eventSource).getNumberLabel();
            if (sourceState instanceof ShopCategorySelectState) {
                shopCategoryString = shopCategoryString + s;
                app.getMessageIndicator().setMessage(shopCategoryString);
            }
        }

	}

	public Class exit(EventObject event, State sinkState) {
        Object eventSource = event.getSource();
		if (sinkState instanceof IdleState && eventSource instanceof ClearButton) {
            shopCategoryString = "";
		    app.getWarningIndicator().setMessage("");
            return ShopNoSelectState.class;
        }
        if (eventSource instanceof NumberButton) {
            return ShopCategorySelectState.class;
        } else if (eventSource instanceof EnterButton) {
            if ("".equals(shopCategoryString))
                return ShopCategorySelectState.class;
            else
                return ShopShareSelectState.class;

        } else {
            return WarningState.class;
        }


	}
}


