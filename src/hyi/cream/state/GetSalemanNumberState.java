package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.util.CreamToolkit;

import java.util.ResourceBundle;

/**
 * @author pyliu
 *
 */
public class GetSalemanNumberState extends GetSomeAGState {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();
	static GetSalemanNumberState getSalemanNumberState = null;

	public static GetSalemanNumberState getInstance() {
		try {
			if (getSalemanNumberState == null) {
				getSalemanNumberState = new GetSalemanNumberState();
			}
		} catch (InstantiationException ex) {
		}
		return getSalemanNumberState;
	}

	/**
	 * Constructor
	 */
	GetSalemanNumberState() throws InstantiationException {
	}

	public boolean checkValidity() {
		String salemanNumber = getAlphanumericData();
		if (salemanNumber.length() < PARAM.getSalemanIDMinLen()) {
			app.getWarningIndicator().setMessage(res.getString("SalemanIDTooShort"));
			java.awt.Toolkit.getDefaultToolkit().beep();
			return false;
		}
		//Cashier cashierDAC = Cashier.queryByCashierID(salemanNumber);
			//Bruce/20030430/
			//Add a property "LevelsCanSignOn". If doesn't exist, don't check; otherwise
			//check if the signon cashier's level match any in "LevelsCanSignOn"
		app.getCurrentTransaction().setSalesman(salemanNumber);
		app.getSystemInfo().setSalesManNumber(salemanNumber);
		app.setSalemanChecked(true);
		app.getWarningIndicator().setMessage("");
		return true;
	}

	public Class getUltimateSinkState() {
		return IdleState.class;
	}

	public Class getInnerInitialState() {
		return SalemanState.class;
	}
}
