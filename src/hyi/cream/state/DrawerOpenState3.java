package hyi.cream.state;

import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.CreamSession;
import hyi.cream.dac.CashForm;
import hyi.cream.dac.ShiftReport;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import hyi.spos.CashDrawer;
import hyi.spos.JposException;
import hyi.spos.events.StatusUpdateEvent;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;

public class DrawerOpenState3 extends State {

	static DrawerOpenState3 drawerOpenState = null;
	boolean showWarning = true;

	public static DrawerOpenState3 getInstance() {
		try {
			if (drawerOpenState == null) {
				drawerOpenState = new DrawerOpenState3();
			}
        } catch (InstantiationException ex) {
		}
		return drawerOpenState;
	}

    /**
     * Constructor
     */
	public DrawerOpenState3() throws InstantiationException {
	     drawerOpenState = this;
	}

	public void entry(EventObject event, State sourceState) {
        DbConnection connection = null;
        try { // TODO 这里有两个connection, 是否要改成一个
            connection = CreamToolkit.getPooledConnection();
    
            StateMachine.setFloppySaveError(false);
    	   	showWarning = false;
    		boolean checkDrawerClose = PARAM.isCheckDrawerClose();
    		final boolean shiftPrint = PARAM.isShiftPrint();
    		final ShiftReport shift = ShiftReport.getCurrentShift(connection);
    
            // 如果使用现金清点单，就不用检查drawer close，这里也不用开抽屉（因为已经在ShiftState开过了）
            final boolean useCashForm = PARAM.isUseCashForm();
    		if (useCashForm)
    			checkDrawerClose = false;  // Bruce> change default "CheckDrawerClose" to "no"
    
    		if (!checkDrawerClose) {
    			Runnable t = new Runnable() {
    				public void run() {
    					//POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
    					POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
                        DbConnection connection = null;
    					try {
                            connection = CreamToolkit.getPooledConnection();
    						CashDrawer cashi = posHome.getCashDrawer();
    						if (!posHome.getEventForwardEnabled())
    							posHome.setEventForwardEnabled(true);

                            if (!useCashForm) { //如果使用现金清点单，这里不用开抽屉（因为已经在ShiftState开过了）
                                try {
                                    cashi.setDeviceEnabled(true);
                                    cashi.openDrawer();
                                } catch (JposException e) {
                                    CreamToolkit.logMessage("Open drawer failed.");
                                }
                            }
							if (shiftPrint)
							   CreamPrinter.getInstance().printShiftReport(shift, drawerOpenState);

                            // Print CashForm
                            if (useCashForm) {
                                CashForm cf = CashForm.queryByZNumberAndShiftNumber(connection,
                                    shift.getZSequenceNumber().intValue(),
                                    shift.getSequenceNumber().intValue());
                                CreamPrinter.getInstance().printCashForm(cf);
                            }
							CreamToolkit.sleepInSecond(1);
    						posHome.statusUpdateOccurred(new StatusUpdateEvent(cashi, 0));
    					} catch (Exception e) {
                            CreamToolkit.logMessage(e);
                        } finally {
                            CreamToolkit.releaseConnection(connection);
                        }
    				}
    			};
    			new Thread(t).start();
    		} else {
    			//sourceState = sourceState;
    			Runnable t = new Runnable() {
    				public void run() {
    				//set posHome enabled to prepare for event forwarding
    					POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
    					CashDrawer cash = null;
    					try {
    						cash = posHome.getCashDrawer();
    					} catch (Exception ne) {
                            CreamToolkit.logMessage(ne);
                            return;
                        }
    					if (!posHome.getEventForwardEnabled())
    						posHome.setEventForwardEnabled(true);
    				//drive the cash drawer
    					try {
    						if (!cash.getDeviceEnabled())
    							cash.setDeviceEnabled(true);
    						cash.addStatusUpdateListener(posHome);
    						cash.openDrawer();
    						//
    						Thread.sleep(2000);
    						if (shiftPrint)
    						   CreamPrinter.getInstance().printShiftReport(shift, drawerOpenState);
    						 //Start a timer.
    						int wait = PARAM.getDrawerCloseTime();
    						showWarning = true;
    						startWarning(wait);
    						cash.waitForDrawerClose(wait * 1000, 0, 0, 100);
    						showWarning = false;
    						if (showWarningHint != null)
    						   showWarningHint.interrupt();
    						 //Start a timer.
    						cash.removeStatusUpdateListener(posHome);
    						cash.setDeviceEnabled(false);
    					} catch (Exception je) {
    						CreamToolkit.logMessage(je.toString());
    						return;
    					}
    				}
    			};
    			new Thread(t).start();
    		}
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
	}

	static Thread showWarningHint = null;

	private void startWarning(int w) {
		final int wait = w;
		Runnable startWarning = new Runnable() {
			public void run() {
				try {
					Thread.sleep(wait * 1000 + 1000);
					if (showWarning)
					   POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                           CreamToolkit.GetResource().getString("PleaseCloseDrawer"));
				} catch (Exception e) {
					return;
				}
			}
		};
		showWarningHint = new Thread(startWarning);
		showWarningHint.start();
	}

	public Class exit(EventObject event, State sinkState) {
		//System.out.println("This is DrawerOpenState3's Exit!");
	    POSTerminalApplication.getInstance().getWarningIndicator().setMessage("");
		//if (DacTransfer.getInstance().getStoZPrompt()) {
        if (CreamSession.getInstance().isDoingShiftBeforeZ()) {
			//DacTransfer.getInstance().setStoZPrompt(false);
            CreamSession.getInstance().setDoingShiftBeforeZ(false);
			return hyi.cream.state.ZState.class;
		} else {
			POSTerminalApplication.getInstance().setEnableKeylock(true);
	    	return KeyLock1State.class;
		}
	}
}
