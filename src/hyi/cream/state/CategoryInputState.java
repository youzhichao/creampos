package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Category;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;
import java.util.ResourceBundle;


public class CategoryInputState extends State {

    private static CategoryInputState categoryInputState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private String number = "";
    private String amount = "";

    public static CategoryInputState getInstance() {
        try {
            if (categoryInputState == null) {
                categoryInputState = new CategoryInputState();
            }
        } catch (InstantiationException ex) {
        }
        return categoryInputState;
    }

    /**
     * Constructor
     */
    public CategoryInputState() throws InstantiationException {
    }

    /**
     * Check if alphanumeric data is legal.
     *
     * @return true/false.
     */
    public boolean checkAlphanumericData() {
        if (number.length() != 3) {
            app.getMessageIndicator().setMessage(res.getString("InputCategoryNoWarning"));
            java.awt.Toolkit.getDefaultToolkit().beep();
            return false;
        }
        Category cat = Category.queryByCategoryNumber(String.valueOf(number.charAt(0)),
            String.valueOf(number.charAt(1)), String.valueOf(number.charAt(2)));
        if (cat == null) {
            app.getMessageIndicator().setMessage(res.getString("InputCategoryNoWarning"));
            java.awt.Toolkit.getDefaultToolkit().beep();
            return false;
        }
        if (PARAM.getExclusiveZhunguiDepsList().contains(number)) {
            app.getMessageIndicator().setMessage(res.getString("ZhuanGuiCanNotCategoryInput"));
            java.awt.Toolkit.getDefaultToolkit().beep();
            return false;
        }
        return true;
    }

    @Override
    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof NumberingState) {
            amount = ((NumberingState) sourceState).getNumberString();
            number = "";
        } else if (sourceState instanceof WarningState) {
            number = "";
        }
        app.getMessageIndicator().setMessage(res.getString("InputCategoryNo") + number);
    }

    @Override
    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof NumberButton) {
            number += ((NumberButton) event.getSource()).getNumberLabel();
            return CategoryInputState.class;
        }
        else if (event.getSource() instanceof ClearButton) {
            number = "";
        }
        else if (event.getSource() instanceof EnterButton) {
            if (!checkAlphanumericData()) {
                return WarningState.class;
            }
            return PluReadyState.class;
        }
        return sinkState.getClass();
    }

    public String getNumber() {
        return number;
    }

    public String getAmount() {
        return amount;
    }
}
