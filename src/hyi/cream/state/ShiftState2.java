package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.CreamSession;
import hyi.cream.dac.LineOffTransfer;
import hyi.cream.dac.ShiftReport;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;

public class ShiftState2 extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static ShiftState2 shiftState = null;
    private	boolean success = false;

    public static ShiftState2 getInstance() {
        try {
            if (shiftState == null) {
                shiftState = new ShiftState2();
            }
        } catch (InstantiationException ex) {
        }
        return shiftState;
    }

    /**
     * Constructor
     */
    public ShiftState2() throws InstantiationException {
        if (shiftState == null)
            shiftState = this;
        else
            throw new InstantiationException(this + "");
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("ShiftState2 Enter");
        if (!success) {
            if (StateMachine.getFloppySaveError())
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("SaveFloppyErrorInsertAgain"));
            else
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("SaveFloppyReady"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("SaveFloppy"));
        if (event.getSource() instanceof ClearButton) {
            return hyi.cream.state.CancelShiftState.class;
        }
        if (!success) {
            //Bruce/20030114
            // 如果是在做Z的时候是因为还没做shift而先跑来这里的话,将shift存软盘后(in ShiftState)
            // 不需将硬盘中的档案删除（因为在ZState2的存软盘亦需将shift/cashform一并再次储存）。
            // 否则如果是单纯做shift的话，在存盘后就将serialized shift删除掉。
            success = LineOffTransfer.saveToFloppyDisk(
                //!DacTransfer.getInstance().getStoZPrompt());
                !CreamSession.getInstance().isDoingShiftBeforeZ());

        }
        StateMachine.setFloppySaveError(!success);
        //System.out.println("ShiftState2 Exit");
        if (success) {
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getTransactionalConnection();
                ShiftReport shift = ShiftReport.getCurrentShift(connection);
                shift.setUploadState("1");
                shift.update(connection);
                connection.commit();
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("shiftEnd"));
                success = false;
                return DrawerOpenState3.class;
            } catch (EntityNotFoundException e) {
                CreamToolkit.logMessage(e);
                CreamToolkit.haltSystemOnDatabaseFatalError(e);
                return DrawerOpenState3.class;
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                CreamToolkit.haltSystemOnDatabaseFatalError(e);
                return DrawerOpenState3.class;
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
        } else
           return ShiftState2.class;
    }
}