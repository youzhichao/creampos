package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.uibeans.*;

import java.util.*;

abstract public class SomeAGNumberingState extends State 
        implements AlphanumericProvider {
    protected String str = "";
    protected POSTerminalApplication app = POSTerminalApplication.getInstance();

    /**
     * Invoked when entering this state.
     *
     * @param event the triggered event object.
     * @param sourceState the source state
     */
    public void entry(EventObject event, State sourceState) {
        //super.entry(event, sourceState);
		if (sourceState instanceof SomeAGReadyState) {
			// Clear data buffer, and get the number or alphabet from event
			// source and set it into data buffer.
			str = "";
		}
        if (!checkAlphanumericData()) return;
        if (sourceState instanceof SomeAGReadyState) {
            // Clear data buffer, and get the number or alphabet from event
            // source and set it into data buffer.
            str = "";
            Object eventSource = event.getSource();
            if (eventSource instanceof hyi.cream.uibeans.NumberButton) {
                str = ((NumberButton)eventSource).getNumberLabel();
            } /*else if (eventSource instanceof AlphabetButton) {
                str = ((AlphabetButton)eventSource).getAlphabetLabel();
            }*/
        } else if (sourceState instanceof SomeAGNumberingState) {
            // Get the number or alphabet from event source and append it to
            // data buffer.
            Object eventSource = event.getSource();
            if (eventSource instanceof hyi.cream.uibeans.NumberButton) {
                str = str + ((NumberButton)eventSource).getNumberLabel();
            } /*else if (eventSource instanceof AlphabetButton) {
                str = str + ((AlphabetButton)eventSource).getAlphabetLabel();
            }*/
        }
        // Redisplay the string from data buffer into message indicator.
        app.getMessageIndicator().setMessage(str);
    }

    /**
     * Invoked when exiting this state.
     *
     * @param event the triggered event object.
     * @param sinkState the sink state
     */
    public Class exit(EventObject event, State sinkState) {
    	//super.exit(event, sinkState);
        if (sinkState instanceof SomeAGReadyState) {
            // Clear data buffer, clear message indicator
            str = "";
            app.getMessageIndicator().setMessage("");
        }
        return sinkState.getClass();
    }

    /**
     * Get alphanumeric data inside.
     *
     * @return the alphanumeric data.
     */
    public String getAlphanumericData() {
        // return the String from data buffer.
        return str;
    }
    
	/**
	 * Check if alphanumeric data is legal.
	 *
	 * @return true/false.
	 */
    public boolean checkAlphanumericData() {
    	return true;
    }
}

