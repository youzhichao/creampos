package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.MSR;
import hyi.spos.Scanner;
import hyi.spos.events.DataEvent;

import java.util.EventObject;

//import jpos.JposException;
//import jpos.MSR;

abstract public class GetSomeAGState extends State 
        implements AlphanumericProvider {
    private String str = "";
    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    /**
     * Invoked when entering this state. 
     *
     * @param event the triggered event object.
     * @param sourceState the source state
     */
    public void entry(EventObject event, State sourceState) {
        //super.entry(event, sourceState);
        Object eventSource = event.getSource();
        if (eventSource instanceof hyi.cream.uibeans.ClearButton) {
            // Set alphanumericData to null.
            str = null;
        } else if (eventSource instanceof MSR) {
            // Get the bar code from Scanner and set it to alphanumericData.
            try {
                switch (((MSR) eventSource).getTracksToRead()) {
                case 1:
                    str = new String(((MSR) eventSource).getTrack1Data());
                    break;
                case 2:
                    str = new String(((MSR) eventSource).getTrack2Data());
                    break;
                case 3:
                    str = new String(((MSR) eventSource).getTrack3Data());
                    break;
                }
            } catch (JposException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        } else if (eventSource instanceof hyi.cream.uibeans.EnterButton) {
            // Treat the sourceState as a SomeAGNumberingState, get data from
            // it and set it to alphanumericData
        	str = "";
        	if (sourceState instanceof SomeAGNumberingState) {
        		str = ((SomeAGNumberingState)sourceState).getAlphanumericData();
        	}
        } else if (eventSource instanceof Scanner) {
            try {
                str = new String(((Scanner)eventSource).getScanData(((DataEvent)event).seq));
            } catch (JposException e) {
                System.out.println(e);
            }
        }
	}

	public boolean checkInteger() {
		 String number = getAlphanumericData();
		 if (number.indexOf(".") != -1)
			 return false;
		 else
		     return true;
	}

    /**
     * Invoked when exiting this state.
     *
     * @param event the triggered event object.
     * @param sinkState the sink state
     */
    public Class exit(EventObject event, State sinkState) {
    	//super.exit(event, sinkState);
        //System.out.println(this + " exit");
        //System.out.println(" data = " + str);
        //System.out.println("alph = " + getAlphanumericData());
        //System.out.println("valid = " + checkValidity());
        if (getAlphanumericData() == null || checkValidity()) {
            return getUltimateSinkState();
        } else {
            // Get warning message by getWarningMessage() and
            // display it on warningIndicator.
            //System.out.println(" return " + getInnerInitialState());
            app.getWarningIndicator().setMessage(getWarningMessage());
            return getInnerInitialState();
        }
    }

    /**
     * Return the real inner initial state's Class object.
     *
     * <P>This is method should be implemented by derived class.
     *
     * @return the inner initial state's Class object.
     */
    abstract public Class getInnerInitialState();

    /**
     * Return the ultimate sink state's Class object.
     *
     * <P>This is method should be implemented by derived class.
     *
     * @return the ultimate state's Class object.
     */
    abstract public Class getUltimateSinkState();

    /**
     * Check the validity of the alphanumeric data. This method also has to
     * set the warningMessage properly.
     *
     * <P>This is method should be implemented by derived class.
     *
     * @return the ultimate state's Class object.
     */
    abstract public boolean checkValidity();

    /**
     * Get alphanumeric data inside. 
     *
     * @return the alphanumeric data.
     */
    public String getAlphanumericData() {
        // return the alphanumeric data.
        return str;
    }
    
	/**
	 * Check if alphanumeric data is legal.
	 *
	 * @return true/false.
	 */
	public boolean checkAlphanumericData() {
		return true;
	}
}

