package hyi.cream.state;

import hyi.cream.*;
import java.util.*;

abstract public class SomeAGReadyState extends State {
    protected POSTerminalApplication app = POSTerminalApplication.getInstance();
    
    /**
     * Invoked when entering this state.
     *
     * <P>Just get a String from getPromptedMessage() and display it on the
     * warning indicator.
     *
     * @param event the triggered event object.
     * @param sourceState the ultimate source state
     */
    public void entry(EventObject event, State sourceState) {
		//super.entry(event, sourceState);
		app.getMessageIndicator().setMessage(getPromptedMessage());
        app.getWarningIndicator().setMessage(getWarningMessage());
    }

    /**
     * Invoked when exiting this state.
     *
     * <P>Clear warningIndicator and return sinkState.getClass().
     *
	 * @param event the triggered event object.
     * @param sinkState the sink state
     */
	public Class exit(EventObject event, State sinkState) {
		//super.exit(event, sinkState);
		app.getWarningIndicator().setMessage("");
        return sinkState.getClass();
    }

    /**
     * Get the prompted message which is used by entry().
     *
     * <P>This method should be implemented by derived class.
     *
     * @param event the triggered event object.
     * @param sinkState the sink state
     */
    public String getPromptedMessage() {
    	return "";
    }

	/**
     * Get the prompted message which is used by entry().
     *
     * <P>This method should be implemented by derived class.
     *
     * @param event the triggered event object.
     * @param sinkState the sink state
     */
    abstract public String getWarningMessage();
}

