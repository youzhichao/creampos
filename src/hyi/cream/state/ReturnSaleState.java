/*
 * Created on 2003-4-5
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Transaction;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * @author ll
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ReturnSaleState extends State {
	static ReturnSaleState returnSaleState = null;
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();
	public static ReturnSaleState getInstance() {
		try {
			if (returnSaleState == null) {
				returnSaleState = new ReturnSaleState();
			}
		} catch (InstantiationException ex) {
		}
		return returnSaleState;
	}

	/**
	 * Constructor
	 */
	public ReturnSaleState() throws InstantiationException {
	}


	/* (non-Javadoc)
	 * @see hyi.cream.state.State#entry(java.util.EventObject, hyi.cream.state.State)
	 */
	public void entry(EventObject event, State sourceState) {
		if (sourceState instanceof CashierRightsCheckState) {
		    Transaction trans = app.getCurrentTransaction();
			trans.setDealType2("3");
			trans.setDealType3("4");
			app.setReturnItemState(true);
			app.getItemList().setBackLabel(res.getString("BigReturnNumberLabel"));
			//System.out.println(res.getString("ReturnReady"));
		}
	}

	/* (non-Javadoc)
	 * @see hyi.cream.state.State#exit(java.util.EventObject, hyi.cream.state.State)
	 */
	public Class exit(EventObject event, State sinkSate) {
		return IdleState.class;
	}
}
