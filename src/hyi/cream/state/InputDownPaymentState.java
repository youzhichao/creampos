package hyi.cream.state;

import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;

import org.apache.commons.lang.StringUtils;

/**
 * 輸入訂金（手付金）金額state.
 */
public class InputDownPaymentState extends State {

    private static InputDownPaymentState instance;
    private String downPaymentAmount = "0";
    private String message;
    private Class exitState;

    public static InputDownPaymentState getInstance() {
        try {
            if (instance == null) {
                instance = new InputDownPaymentState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public InputDownPaymentState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        Object eventSource = event.getSource();
        if (sourceState instanceof PeiDa3IdleState) { // (DownPayment: IPD2ID)
            downPaymentAmount = "";
            message = PeiDa3IdleState.getInstance().isDownPayment() ? m("PleaseInputDownPayment") :
                PeiDa3IdleState.getInstance().isBackRefundPaymet() ? m("PleaseInputRefundBackPayment") :
                m("PleaseInputBackPayment");
            showMessage(message);
            exitState = PeiDa3OnlineState.class;
        } else if (sourceState instanceof ReturnNumber2State) { // (DownPayment: RN2IDP)
            downPaymentAmount = "";
            message = ReturnNumber2State.getInstance().getRefundType() == ReturnNumber2State.RefundType.DownPayment ?
                m("PleaseInputRefundDownPayment") : m("PleaseInputRefundBackPayment");
            showMessage(message);
            exitState = ReturnNumber2State.class;
        } else if (sourceState instanceof InputDownPaymentState) { // (DownPayment: ID2ID)
            if (eventSource instanceof ClearButton) {
                downPaymentAmount = "";
                showMessage(message);
            } else if (eventSource instanceof NumberButton) {
                NumberButton pb = (NumberButton) eventSource;
                downPaymentAmount = downPaymentAmount + pb.getNumberLabel();
                showMessage(downPaymentAmount);
                showWarningMessage("");
            }
        }
    }

    public Class exit(EventObject event, State sinkState) {
        Object eventSource = event.getSource();
        if (eventSource instanceof EnterButton && !downPaymentAmount.equals("")) { // (DownPayment: IPD2PD)
            showWarningMessage("");
            showMessage("PeiDaConfirm");
            getCurrentTransaction().setState1(
                PeiDa3IdleState.getInstance().isBackRefundPaymet() ? "X" :  // 記錄此交易為Nitori退着付殘金交易，此交易將不印發票
                "W"); // 記錄此交易為Nitori着付金交易，此交易將不印發票
            return exitState;
        } else
            return InputDownPaymentState.class;
    }

    public HYIDouble getDownPaymentAmount() {
        if (StringUtils.isEmpty(downPaymentAmount))
            return new HYIDouble(0);
        else
            return new HYIDouble(downPaymentAmount);
    }
}