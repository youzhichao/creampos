
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.util.CreamToolkit;

/**
 * A Class class.
 * <P>
 * @author Slackware
 */
public class GetCustomNumberState extends GetSomeAGState {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static GetCustomNumberState getCustomNumberState = null;

    public static GetCustomNumberState getInstance() {
        try {
            if (getCustomNumberState == null) {
                getCustomNumberState = new GetCustomNumberState();
            }
		} catch (InstantiationException ex) {
        }
        return getCustomNumberState;
    }

    /**
     * Constructor
     */
    public GetCustomNumberState() throws InstantiationException {
    }

    public boolean checkValidity() {
		String customNumber = getAlphanumericData();
		if (Integer.parseInt(customNumber) > 1) {
			app.getCurrentTransaction().setCustomerCount(Integer.parseInt(customNumber));
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("CustomNumberChanged"));
			return true;
		} else
		    return false;	
	}

	public Class getUltimateSinkState() {
			System.out.println("*" + CustomState.getInstance().getSourceState());
		if (CustomState.getInstance().getSourceState() != null)
			return CustomState.getInstance().getSourceState().getClass();
		else
		    return IdleState.class;
	}

    public Class getInnerInitialState() {
        return CustomState.class;
	}
}

 
