package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.Keylock;

import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * @author dai
 */
public class CheckKeyLockState extends State {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
//    private boolean keyLock = false;

    private static CheckKeyLockState checkKeyLockState = null;

    public static CheckKeyLockState getInstance() {
        try {
            if (checkKeyLockState == null) {
                checkKeyLockState = new CheckKeyLockState();
            }
        } catch (InstantiationException ex) {
        }
        return checkKeyLockState;
    }

    /**
     * Constructor
     */
    public CheckKeyLockState() throws InstantiationException {
    }                                 

    public void entry (EventObject event, State sourceState) {
        //System.out.println("CheckKeyLockState entry");
        app.getMessageIndicator().setMessage("");
        app.getWarningIndicator().setMessage(res.getString("TurnKeyLock"));
        POSTerminalApplication.getInstance().setTransactionEnd(true);
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("CheckKeyLockState exit");
        if (event.getSource() instanceof Keylock) {
            app.getPopupMenuPane().setVisible(false);
            app.getWarningIndicator().setMessage("");
            Keylock k = (Keylock)event.getSource();
            try {
                int kp = k.getKeyPosition();
                if (kp == 5) {
                    app.setKeyPosition(kp);
                    return KeyLock3State.class;
                } else if (kp == 3) {
                    app.setKeyPosition(kp);
                    return KeyLock2State.class;
                } else if (kp == 4) {
                    app.setKeyPosition(kp);
                    return KeyLock1State.class;
                } else if (kp == 6) {
                    app.setKeyPosition(kp);
                    return ConfigState.class;
                } else if (kp == 1) {
                    return CheckKeyLockState.class;
                }
            } catch (JposException e) {
            }
            return InitialState.class;
        }
        return sinkState.getClass();
    }
}

 
