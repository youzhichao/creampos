/*
 * Created on 2003-7-2
 *
 */
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.SI;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.util.EventObject;

import org.apache.commons.lang.StringUtils;

/**
 *  开放折扣　输入折扣率
 */
public class DiscountInputState extends State {
    private static DiscountInputState discountInputState;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private String discountRate = "";

    static char inputType; // SI.DISC_TYPE_INPUT_PERCENTAGE or SI.DISC_TYPE_INPUT_DISCOUNT_AMOUNT

    public DiscountInputState() throws InstantiationException {

    }

    public static DiscountInputState getInstance() {
        try {
            if (discountInputState == null) {
                discountInputState = new DiscountInputState();
            }
        } catch (InstantiationException ex) {
        }
        return discountInputState;
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof SIDiscountState || sourceState instanceof WarningState) {
            discountRate = "";
            showMessage(inputType == SI.DISC_TYPE_INPUT_PERCENTAGE ?
                "DiscountRateInput" : "DiscountAmountInput");

        } else if (sourceState instanceof DiscountInputState && event.getSource() instanceof NumberButton) {
            String s = ((NumberButton) event.getSource()).getNumberLabel();
            discountRate += s;
            app.getMessageIndicator().setMessage(discountRate);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            try {
                BigDecimal value = new BigDecimal(discountRate);
                if (inputType == SI.DISC_TYPE_INPUT_PERCENTAGE) {
                    if (value.compareTo(new BigDecimal(100)) >= 0) {
                        discountRate = "";
                        showMessage(m("DiscountRateError") + m("PleaseInputDiscountRate"));
                        return WarningState.class;
                    }
                } else {
                   // 如果輸入的折讓金額超過銷售淨額，則發出警報
                   if (new HYIDouble(discountRate).compareTo(getCurrentTransaction().getNetSalesAmount()) > 0) {
                       discountRate = "";
                       showMessage("DiscountAmountOverNetAmount");
                       return WarningState.class;
                   }
                }
            } catch (Exception e) {
                discountRate = "";
                showMessage("DiscountRateError");
                return WarningState.class;
            }
            return SIDiscountState.class;
        } if (event.getSource() instanceof ClearButton) {
            discountRate = "";
        }

        if (sinkState != null)
            return sinkState.getClass();
        else
            return null;
    }

    /**
     * 开放折扣的折扣率 
     * 　　　例如：如果折扣为8折，输入的数据应该为80，而返回的折扣率为0.20　 
     * @return HYIDouble
     */
    public HYIDouble getDiscountRate() {
        if (StringUtils.isEmpty(discountRate))
            return null;
        if (inputType == SI.DISC_TYPE_INPUT_PERCENTAGE) {
            HYIDouble bd = new HYIDouble(this.discountRate).divide(new HYIDouble(100.0), 2, BigDecimal.ROUND_HALF_UP);
            return  (new HYIDouble(1.0).subtract(bd));
        } else {
            return new HYIDouble(discountRate);
        }
    }

}
