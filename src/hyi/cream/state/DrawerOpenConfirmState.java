package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.text.MessageFormat;
import java.util.EventObject;

import org.apache.commons.lang.StringUtils;

public class DrawerOpenConfirmState extends State {

    private static DrawerOpenConfirmState instance = new DrawerOpenConfirmState();

    public static DrawerOpenConfirmState getInstance() {
        return instance;
    }

    @Override
    public void entry(EventObject event, State sourceState) {
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        String msg = "";
        if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.WHOLESALECLEARING_STATE) {
            String m = app.getCurrentTransaction().getOriginEarnAmount().compareTo(new HYIDouble(0)) <= 0
                    ? "WholesalePrePay"
                    : "WholesaleMessage2";
            msg = MessageFormat.format(CreamToolkit.GetResource().getString(m),
                    app.getCurrentTransaction().getTotalPaymentAmount().setScale(2).toString());
        }
        msg += CreamToolkit.GetResource().getString("CheckOutConfirm");
        app.getMessageIndicator().setMessage(msg);
    }

    @Override
    public Class exit(EventObject event, State sinkState) {
        Class<? extends State> exitState;

        if (event.getSource() instanceof EnterButton) {

//            // 若為傳票交易，再度檢查傳票版本，若版本不符，顯示警告訊息，不能讓user結帳
//            Transaction trans = getCurrentTransaction();
//            if ("P".equals(trans.getAnnotatedType()) && !StringUtils.isEmpty(trans.getAnnotatedId())) {
//                boolean versionCorrect = true;
//                try {
//                    /*
//                     *  -1=无法连接sc
//                     *  可結帳
//                     *      'H'; //批發配達
//                     *      'P'; //普通配達
//                     *      'M'; //窗簾修改
//                     *      'O'; //窗簾訂做
//                     *  1=不存在
//                     *  2=已作廢
//                     *  3=售後處理
//                     *  4=結清,pos不可結
//                     *  5=被退過貨的
//                     *  6=單據版本不符合
//                     *  7=退货机可退貨,pos不可结帐
//                     *  8=异常
//                     */
//                    String statusAndSrcBillNo = StateToolkit.getPeiDaStatus(trans.getAnnotatedId());
//                    // status = deliveryhead.結帳狀態 + "," + deliveryhead.srcBillNo(原始單號)
//                    String[] x = statusAndSrcBillNo.split(",");
//                    String statusStr = x[0];
//                    if ("6".equals(statusStr))  //6=單據版本不符合，不讓結帳
//                        versionCorrect = false;
//                } catch (java.net.SocketException e) {
//                    CreamToolkit.logMessage(e);
//                    //versionCorrect = false; 無法連線還是讓他結帳好了
//                } catch (Exception e) {
//                    CreamToolkit.logMessage(e);
//                    versionCorrect = false;
//                }
//                if (!versionCorrect) {
//                    showWarningMessage("PeiDaError6_2");
//                    return SummaryState.class;
//                }
//            }

            exitState = hyi.cream.state.DrawerOpenState.class;
        } else {
            if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.WHOLESALECLEARING_STATE)
                exitState = hyi.cream.state.wholesale.WholesaleClearingState.class;
            else
                exitState = hyi.cream.state.SummaryState.class;
        }
        return exitState;
    }
}
