/*
 * Created on 2003-4-1
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.state;

import java.util.EventObject;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;
import hyi.spos.JposException;
import hyi.spos.ToneIndicator;

//import jpos.*;
import java.util.*;

/**
 * @author ll
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WeiXiuState extends State {

	private String annotatedid          = "";
	private POSTerminalApplication app  = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();
	static  WeiXiuState weiXiuState = null;
	private ToneIndicator tone          = null;
	private String pluCode = "";
	private PLU plu                       = null;
	private LineItem lineItem             = null;
	private boolean exit = false; 
	
	public static WeiXiuState getInstance() {
		try {
			if (weiXiuState == null) {
				weiXiuState = new WeiXiuState();
			}
		} catch (InstantiationException ex) {
		}
		return weiXiuState;
	}

	/**
	 * Constructor
	 */
	public WeiXiuState() throws InstantiationException {
	}

	/* (non-Javadoc)
	 * @see hyi.cream.state.State#entry(java.util.EventObject, hyi.cream.state.State)
	 */
	public void entry(EventObject event, State sourceState) {
//		System.out.println("This is WeiXiuState Entry!");
		if (sourceState instanceof IdleState) {         
			annotatedid = "";
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("WeiXiuMessage"));
		} else if (sourceState instanceof WeiXiuState) {
			if (event.getSource() instanceof ClearButton) {
				if (annotatedid == "") {
					exit = true;
				} else {
					try {
						if (tone != null) {
							tone.clearOutput();
							tone.release();
						}
					} catch (JposException je) { }
					annotatedid = "";              
					app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("WeiXiuMessage"));
					app.getWarningIndicator().setMessage("");
				}
			} else if (event.getSource() instanceof NumberButton) {
				try {
					if (tone != null) {
						tone.clearOutput();
						tone.release();
					}
				} catch (JposException je) { }
				NumberButton pb = (NumberButton)event.getSource();
				annotatedid = annotatedid + pb.getNumberLabel();  
				app.getMessageIndicator().setMessage(annotatedid);
				app.getWarningIndicator().setMessage("");
			}
		}

	}

	/* (non-Javadoc)
	 * @see hyi.cream.state.State#exit(java.util.EventObject, hyi.cream.state.State)
	 */
	public Class exit(EventObject event, State sinkState) {
//		System.out.println("WeiXiuState exit");
		if (exit) {
			exit = false;
			app.getCurrentTransaction().setAnnotatedType("");						
			app.getCurrentTransaction().setAnnotatedId("");
			Iterator it = app.getCurrentTransaction().getLineItemsIterator();
			List list = new Vector();
			while (it.hasNext()) {
				Object object = it.next();
				if (it.hasNext())
					list.add(object);
			}
			app.getCurrentTransaction().clearLineItem();
			for (int i = 0; i < list.size(); i++) {
				try {
					app.getCurrentTransaction().addLineItem((LineItem) list.get(i), false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return IdleState.class;
		}
		String type = app.getCurrentTransaction().getAnnotatedType();
		if (type != null && type.equals("P"))
			return IdleState.class;

		if (event.getSource() instanceof EnterButton) {
			Transaction curTran = app.getCurrentTransaction();  
			if (annotatedid.equals("") || annotatedid.length() != 13) {
				//System.out.println("########################################");
				POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
				try {
					tone = posHome.getToneIndicator();
				} catch (Exception ne) {
                    CreamToolkit.logMessage(ne);
                }
				try {
					if (!tone.getDeviceEnabled())
						tone.setDeviceEnabled(true);
					if (!tone.getClaimed()) {
						 tone.claim(0);
					//tone.claim(0);
						 tone.setAsyncMode(true);
						 tone.sound(99999, 500);
					}
				} catch (JposException je) {System.out.println(je);}
				app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WeiXiuNumberLengthError"));
				return WeiXiuState.class;
			} else {
				app.getCurrentTransaction().setAnnotatedType("W");						
				app.getCurrentTransaction().setAnnotatedId(annotatedid);
//				System.out.println("annotatedid : " + app.getCurrentTransaction().getAnnotatedId());
				app.getWarningIndicator().setMessage("");
				return IdleState.class;
			}
		} else {
			if (!(event.getSource() instanceof NumberButton)
				&& !(event.getSource() instanceof ClearButton)) {
				//System.out.println("########################################");
				POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
				try {
					tone = posHome.getToneIndicator();
				} catch (Exception ne) {
                    CreamToolkit.logMessage(ne);
                }
				try {
					if (!tone.getDeviceEnabled())
						tone.setDeviceEnabled(true);
					if (!tone.getClaimed()) {
						 tone.claim(0);
					//tone.claim(0);
						 tone.setAsyncMode(true);
						 tone.sound(99999, 500);
					}
				} catch (JposException je) {System.out.println(je);}
				app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WeiXiuMessage"));
			}
			return WeiXiuState.class;
		}
	}
}
