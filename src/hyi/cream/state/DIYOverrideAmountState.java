package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.util.EventObject;

public class DIYOverrideAmountState extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private static DIYOverrideAmountState diyOverrideAmountState;

	private HYIDouble maxDiyDiscountRate;

	private String tempPrice = "";

	public static DIYOverrideAmountState getInstance() {
		try {
			if (diyOverrideAmountState == null) {
				diyOverrideAmountState = new DIYOverrideAmountState();
			}
		} catch (InstantiationException ex) {
		}
		return diyOverrideAmountState;
	}

	public DIYOverrideAmountState() throws InstantiationException {
		try {
			maxDiyDiscountRate = new HYIDouble(0.5); //GetProperty.getMaxDIYDiscountRate("0"));
		} catch (NumberFormatException e) {
			throw new InstantiationException(e.getMessage());
		}
	}

	public void entry(EventObject event, State sourceState) {
		if (sourceState instanceof IdleState)
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString(
							"DIYOverridePriceAndEnterOver"));
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof NumberButton) {
			NumberButton pb = (NumberButton) event.getSource();
			System.out.println(" OverrideAmountState exit number"
					+ pb.getNumberLabel());
			if (pb.getNumberLabel().equals("-")
					|| (pb.getNumberLabel().equals(".") && tempPrice.length() == 0)) {
				// System.out.println(" OverrideAmountState exit number 1 "
				// +tempPrice);
			} else {
				tempPrice = tempPrice + pb.getNumberLabel();
				// System.out.println(" OverrideAmountState exit number 2"
				// +tempPrice);
			}
			app.getMessageIndicator().setMessage(tempPrice);
			return DIYOverrideAmountState.class;
		}
		if (event.getSource() instanceof ClearButton) {
			if (tempPrice.length() == 0) {
				return IdleState.class;
			}
			tempPrice = "";
			app.getMessageIndicator().setMessage(tempPrice);
			return DIYOverrideAmountState.class;
		}

		if (event.getSource() instanceof EnterButton) {
			if (tempPrice.length() != 0) {
				HYIDouble overrideAmount = new HYIDouble(0);
				try {
					overrideAmount = new HYIDouble(tempPrice);
				} catch (NumberFormatException e) {
				}
				if (!isPermit(overrideAmount)) {
					tempPrice = "";
					return DIYOverrideAmountState.class;
				}
				Transaction trans = app.getCurrentTransaction();
				trans.setDIYOverrideAmount(app.getItemList().getCurrentIndex(), overrideAmount);
			}
			tempPrice = "";
			return MixAndMatchState.class;
		}
		return IdleState.class;
	}

	public boolean isPermit(HYIDouble amount) {
		if (maxDiyDiscountRate.compareTo(new HYIDouble(1)) == 0) {
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource()
							.getString("CannotOverrideAmount"));
			return false;
		} else if (maxDiyDiscountRate.compareTo(CreamCache.getInstance()
				.getHYIDouble0()) == 0)
			return true;
		Transaction trans = app.getCurrentTransaction();
		int len = trans.getLineItems().length;
		HYIDouble totalAmount = new HYIDouble(0);
		for (int i = 0; i < len; i++) {
			LineItem li = (LineItem) trans.getLineItems()[i];
			if (li.getDIYIndex() != null
					&& li.getDIYIndex().equals(
							app.getItemList().getCurrentIndex())
					&& li.getDetailCode().equalsIgnoreCase("S")) {
				totalAmount = totalAmount.add(li.getAfterDiscountAmount());
			}
		}
		HYIDouble maxDiscountAmount = totalAmount.multiply(
				(new HYIDouble(1)).subtract(maxDiyDiscountRate)).setScale(2,
				BigDecimal.ROUND_HALF_UP);
		if (maxDiscountAmount.compareTo(amount) < 0) {
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString("OverAmount")
					+ maxDiscountAmount
					+ CreamToolkit.GetResource().getString("PleaseReInput")
					);
			return false;
		}
		return true;
	}
}
