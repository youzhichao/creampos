package hyi.cream.state;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.DaiShouDef;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.PopupMenuListener;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.ResourceBundle;

public class DaiShou2WarningState extends State implements PopupMenuListener {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private PopupMenuPane p = app.getPopupMenuPane();

	private ResourceBundle res = CreamToolkit.GetResource();

	private static DaiShou2WarningState daiShou2WarningState = null;

	private DaiShouDef selectedDaiShoudef;

	private String barcode = "";

	private int selectItem = 0;

	public DaiShou2WarningState() throws InstantiationException {

	}

	public static DaiShou2WarningState getInstance() {
		if (daiShou2WarningState == null) {
			try {
				daiShou2WarningState = new DaiShou2WarningState();
			} catch (InstantiationException e) {
			}
		}
		return daiShou2WarningState;
	}

	public void entry(EventObject event, State sourceState) {
		if ((sourceState instanceof DaiShou2CheckState && event.getSource() instanceof SelectButton)
				|| sourceState instanceof DaiShou2WarningState
				|| sourceState instanceof DaiShou2State) {
			if (sourceState instanceof DaiShou2CheckState) {
				DaiShou2CheckState dscs = (DaiShou2CheckState) sourceState;
				selectedDaiShoudef = (DaiShouDef) dscs.getSelectedDaiShouDef();
				barcode = dscs.getBarcode();
			} else if (sourceState instanceof DaiShou2WarningState) {
				DaiShou2WarningState dscs = (DaiShou2WarningState) sourceState;
				selectedDaiShoudef = (DaiShouDef) dscs.getSelectedDaiShouDef();
				barcode = dscs.getBarcode();
			} else if (sourceState instanceof DaiShou2State) {
				selectedDaiShoudef = (DaiShouDef) ((DaiShou2State) sourceState).getMatchDefs().get(0);
				barcode = ((DaiShou2State) sourceState).getBarcode();
			}

			ArrayList menu = new ArrayList();
			String defName = selectedDaiShoudef.getName();
			HYIDouble amount = selectedDaiShoudef.getAmount(barcode);

			menu.add("." + defName + res.getString("Expenses"));
			menu.add("." + amount + res.getString("DollarUnit"));
			menu.add("." + res.getString("DaiShou2WarningStateConfirm"));
			menu.add("." + res.getString("DaiShou2WarningStateCancel"));

			p.setMenu(menu);
			p.setVisible(true);
			p.setPopupMenuListener(this);
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString("InputSelect"));
		} else {
			return;
		}
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof SelectButton) {
			if (selectItem != 0) {
				app.getMessageIndicator().setMessage("Error " + selectItem);
				return DaiShou2WarningState.class;
			}

			// selectedDaiShoudef =
			// (DaiShouDef)matchedDaiShoudefs.get(selectItem);
			// app.getMessageIndicator().setMessage("");
			// p.setInputEnabled(false);
		}
		return hyi.cream.state.DaiShou2ReadyState.class;
		//        
		// if (sinkState != null) {
		// return sinkState.getClass();
		// } else {
		// return null;
		// }
	}

	public void menuItemSelected() {
		if (p.getSelectedMode()) {
			selectItem = p.getSelectedNumber();
			System.out.println(selectItem);
			POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
			POSButtonHome2.getInstance().buttonPressed(e);
		} else {
			POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
			POSButtonHome2.getInstance().buttonPressed(e);

		}
	}

	public DaiShouDef getSelectedDaiShouDef() {
		return selectedDaiShoudef;
	}

	public String getBarcode() {
		return barcode;
	}

}
