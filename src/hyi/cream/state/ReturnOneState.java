package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.SI;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.HYIDouble;

import java.util.ArrayList;
import java.util.EventObject;

public class ReturnOneState extends State {
    static ReturnOneState returnOneState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ArrayList selectArray = new ArrayList();

    public static ReturnOneState getInstance() {
        try {
            if (returnOneState == null) {
                returnOneState = new ReturnOneState();
            }
        } catch (InstantiationException ex) {
        }
        return returnOneState;
    }

    /**
     * Constructor
     */
    public ReturnOneState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {

        if (sourceState instanceof MixAndMatchState ||
            sourceState instanceof ReturnSelect2State) {
            //selectArray = ((MixAndMatchState) sourceState).getSelectArray();
            selectArray = ReturnSelect2State.getInstance().getSelectedIndexes();
            if (selectArray.size() > 0) {
                showMessage("ReturnPLU2");  // "請選擇單品，或按'確認'鍵結束"
            } else {
                showMessage("ReturnPLU");   // "請選擇單品"
            }
            Transaction curTran = app.getCurrentTransaction();
            curTran.setTotalMMAmount(curTran.getTaxMMAmount());
            
            //clearSIInfo(curTran);
            //for (SI si : CreamSession.getInstance().getTransactionToBeRefunded().getAppliedSIs().keySet()) {
            //    SIDiscountState.getInstance().processDiscount(si, null, curTran, true, true);
            //}            
            
        } else {
            if (event.getSource() instanceof EnterButton && sourceState instanceof ReturnSelect1State) {
                showMessage("ReturnPLU");   // "請選擇單品"
                selectArray.clear();

            } else if (event.getSource() instanceof ClearButton) {
                app.getWarningIndicator().setMessage("");
                if (selectArray.size() > 0) {
                    showMessage("ReturnPLU2");
                } else {
                    showMessage("ReturnPLU");
                }
                                
            } else if (event.getSource() instanceof EnterButton && sourceState instanceof ReturnSelect2State) {
                if (selectArray.size() > 0) {
                    showMessage("ReturnPLU2");
                } else {
                    showMessage("ReturnPLU");
                }
            }
        }
        app.getItemList().selectFinished();
    }

    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");
        
        if (event.getSource() instanceof NumberButton && sinkState instanceof ReturnSelect2State) {
            return ReturnSelect2State.class;
        }

        if (event.getSource() instanceof EnterButton) {
            if (selectArray.size() == 0) {
                showMessage("ReturnPLU4");
                return ReturnOneState.class;
            }

            //  get new return amount in current transaction
            HYIDouble returnAmount = new HYIDouble(0);
            int n = 1;
            for (LineItem lineItem : CreamSession.getInstance().getTransactionToBeRefunded().lineItems()) {
                if (selectArray.contains(new Integer(n))) {
                    if ((lineItem.getDetailCode().equals("S")
                        || lineItem.getDetailCode().equals("R")
                        || lineItem.getDetailCode().equals("I")  //代售
                        || lineItem.getDetailCode().equals("O")) //代收
                        && !lineItem.getRemoved()) {
                        returnAmount = returnAmount.addMe(new HYIDouble(lineItem.getAfterDiscountAmount().intValue()));
                    }
                }
                n = n + 1;
            }

            app.getItemList().selectFinished();
            return ReturnSummaryState.class;
        }

        if (event.getSource() instanceof ClearButton) {
            ReturnSelect2State rs2s = (ReturnSelect2State)StateMachine.getInstance().getStateInstance("hyi.cream.state.ReturnSelect2State");
            if (rs2s != null)
                rs2s.clearTmpLineItemMap();
        }
        return sinkState.getClass();
    }

    //清除所有SI信息
    private void clearSIInfo(Transaction t) {
        t.initSIInfo();
        t.clearAppliedSIs();
        t.removeSILineItem();
    }
    
    public ArrayList getSelectArray() {
        return selectArray;
    }
}
