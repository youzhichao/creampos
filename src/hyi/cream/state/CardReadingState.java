package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.CreditType;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.spos.MSR;

import java.util.Calendar;
import java.util.EventObject;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;

public class CardReadingState extends State {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private String cardNumber = "";
    private String expirationDateStr = "";
    private ResourceBundle res = CreamToolkit.GetResource();
    static CardReadingState cardReadingState = null;

    public static CardReadingState getInstance() {
        try {
            if (cardReadingState == null) {
                cardReadingState = new CardReadingState();
            }
        } catch (InstantiationException ex) {
        }
        return cardReadingState;
    }

    public CardReadingState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof Paying1State || sourceState instanceof WarningState) {
            app.getMessageIndicator().setMessage(res.getString("CardReadingWarning"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        Transaction curTran = app.getCurrentTransaction();
        Object eventSource = event.getSource();
        Calendar expirationDate = null;
        if (eventSource instanceof MSR) {
            try {
                cardNumber = ((MSR)eventSource).getAccountNumber();
                expirationDateStr = ((MSR)eventSource).getExpirationDate();
                CreamToolkit.logMessage("Original card number='" + cardNumber
                    + "', expiration date=" + expirationDateStr);

                // 刷出来的卡号中间可能有空格 例：AE卡 "3763 484707 34004"，必須把空格去掉
                cardNumber = StringUtils.replace(cardNumber, " ", "");

                // 避免出現奇怪卡號
                if (cardNumber == null || cardNumber.length() > 20) 
                    throw new IllegalArgumentException();

                String expireYear = expirationDateStr.substring(2, 4);
                String expireMon = expirationDateStr.substring(0, 2);
                // Calendar nowDate = Calendar.getInstance();
                expirationDate = Calendar.getInstance();
                expirationDate.set(Integer.parseInt(expirationDate.get(Calendar.YEAR) / 100
                    + expireYear), Integer.parseInt(expireMon), 1);
                int max = expirationDate.getMaximum(Calendar.DAY_OF_MONTH);
                expirationDate.set(Calendar.DAY_OF_MONTH, max);

                // Bruce> Don't check expiration date, not need now.
                // if (expirationDate.compareTo(nowDate) < 0) {
                // setWarningMessage(MessageFormat.format(CreamToolkit.GetResource().getString(
                // "CardExpirationWarning"), expireYear, expireMon, String.valueOf(max)));
                // return WarningState.class;
                // }
            } catch (Exception e) {
                setWarningMessage(CreamToolkit.GetResource().getString("MemberWarning"));
                return WarningState.class;
            }

            CreditType cardType = CreditType.checkCreditNo(cardNumber);
            if (cardType == null) {
                setWarningMessage(CreamToolkit.GetResource().getString("MemberWarning"));
                return WarningState.class;
            }

            curTran.setCreditCardType(cardType.getNoType());
            curTran.setCreditCardNumber(cardNumber);
            curTran.setCreditCardExpireDate(expirationDate.getTime());
            app.getMessageIndicator().setMessage(
                CreamToolkit.GetResource().getString("CardNumber") + curTran.getCreditCardNumber());

        } else if (eventSource instanceof ClearButton) {
            for (int i = 1; i < 5; i++) {
                String payno = "PAYNO" + i;
                String payamt = "PAYAMT" + i;
                if (curTran.getFieldValue(payno) != null
                    && curTran.getFieldValue(payno).toString().equals(
                        curTran.getLastestPayment().getPaymentID())) {
                    curTran.setFieldValue(payno, null);
                    curTran.setFieldValue(payamt, 0);
                    for (int nextIndex = i + 1; nextIndex < 5; nextIndex++) {
                        int curIndex = nextIndex - 1;
                        if (curTran.getFieldValue("PAYNO" + nextIndex) != null) {
                            curTran.setFieldValue("PAYNO" + curIndex, curTran.getFieldValue("PAYNO"
                                + nextIndex));
                            curTran.setFieldValue("PAYAMT" + curIndex, curTran
                                .getFieldValue("PAYAMT" + nextIndex));
                            curTran.setFieldValue("PAYNO" + nextIndex, null);
                            curTran.setFieldValue("PAYAMT" + nextIndex, new HYIDouble(0));
                        }
                    }
                    break;
                }
            }
            curTran.removePayment(curTran.getLastestPayment());
        }

        return Paying3State.class;
    }
}
