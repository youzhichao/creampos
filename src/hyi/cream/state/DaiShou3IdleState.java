package hyi.cream.state;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Transaction;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.uibeans.AgeLevelButton;
import hyi.cream.uibeans.CancelButton;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.DaiShouButton3;
import hyi.cream.uibeans.PopupMenuListener;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.Keylock;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.StringTokenizer;

/**
 * @author dai
 */
public class DaiShou3IdleState extends State implements PopupMenuListener {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

//	private ResourceBundle res = CreamToolkit.GetResource();

	private PopupMenuPane p = app.getPopupMenuPane();

//	private ArrayList reasonArray = new ArrayList();

	private DaiShouButton3 daiShouButton3 = null;

	static DaiShou3IdleState daiShou3IdleState = null;

	private int daishouNumber = 0;

	private String pluNumber = "";

	private int daishouDetailNumber = 0;

	private String daishouDetailName = "";

	private String canPrint = "y";

	private String needPhoneCardNumber = "n";

	public static DaiShou3IdleState getInstance() {
		try {
			if (daiShou3IdleState == null) {
				daiShou3IdleState = new DaiShou3IdleState();
			}
		} catch (InstantiationException ex) {
		}
		return daiShou3IdleState;
	}

	/**
	 * Constructor
	 */
	public DaiShou3IdleState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
		// System.out.println("DaiFuIdleState entry");

		if (event != null && event.getSource() instanceof DaiShouButton3) {
			app.getSystemInfo().setIsDaiFu(true);
			daiShouButton3 = ((DaiShouButton3) event.getSource());

			ArrayList menu = new ArrayList();
			String filename = "daishoua.conf";
			menu = readFromFile(filename);

			PopupMenuPane pop = app.getPopupMenuPane();
			pop.setMenu(menu);
			pop.setVisible(true);
			pop.setPopupMenuListener(this);
			// menu.clear();
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString("InputSelect"));
		} else {
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString("Daishou3Message"));
		}
	}

	public Class exit(EventObject event, State sinkState) {
		// System.out.println("DaiFuIdleState exit");
		app.getMessageIndicator().setMessage("");
		app.getWarningIndicator().setMessage("");

		// check enter button
		if (event.getSource() instanceof AgeLevelButton) {
		    Transaction trans = app.getCurrentTransaction();
			int agelevel = ((AgeLevelButton) event.getSource()).getAgeID();
			trans.setCustomerAgeLevel(agelevel);

			/*
			 * if ( !CreamPrinter.getInstance().getHeaderPrinted() ) {
			 * CreamPrinter.getInstance().printHeader();
			 * CreamPrinter.getInstance().setHeaderPrinted(true); }
			 */

			// Object[] lineItemArray = trans.getLineItems();
			// LineItem curLineItem =
			// (LineItem)lineItemArray[lineItemArray.length-1];
			// CreamPrinter.getInstance().printLineItem(curLineItem);
			return SummaryState.class;
		}

		// check cancel button
		if (event.getSource() instanceof CancelButton) {
			return CancelState.class;
		}

		// check select button
		if (event.getSource() instanceof SelectButton) {
			return DaiShou3ReadyState.class;
		}

		// check clear button
		if (event.getSource() instanceof ClearButton) {
		    Transaction trans = app.getCurrentTransaction();
			if (trans.getCurrentLineItem() == null) {
				trans.setDealType2("0");
				return IdleState.class;
			} else {
				return DaiShou3IdleState.class;
			}
		}

		// check keylock
		if (event.getSource() instanceof Keylock) {
			Keylock k = (Keylock) event.getSource();
			try {
				int kp = k.getKeyPosition();
				app.setKeyPosition(kp);
				if (CreamToolkit.getSinkStateFromKeyLockCode(kp) != null) {
					return CreamToolkit.getSinkStateFromKeyLockCode(kp);
				} else {
					return DaiShou3IdleState.class;
				}
			} catch (JposException e) {
				System.out.println(e);
			}
			/*
			 * Keylock k = (Keylock)event.getSource(); try { int kp =
			 * k.getKeyPosition(); int ckp = app.getKeyPosition(); if (kp == 3) {
			 * app.setKeyPosition(kp); return KeyLock2State.class; } else if (kp ==
			 * 6) { app.setKeyPosition(kp); return ConfigState.class; } else if
			 * (kp == 2) { app.setKeyPosition(kp); return DaiFuIdleState.class; }
			 * else if (kp == 4) { app.setKeyPosition(kp); return
			 * KeyLock1State.class; } } catch (JposException e) { }
			 */
		}

		return sinkState.getClass();
	}

	public String getPluNo() {
		return pluNumber;
	}

	public void menuItemSelected() {
		if (p.getSelectedMode()) {
			int selectItem = p.getSelectedNumber();

			daishouDetailNumber = selectItem + 1;

			/*
			 * daishouDetailName =
			 * app.getPopupMenuPane().getSelectedDescription(); pluNumber =
			 * app.getPopupMenuPane().getPluNumber(selectItem);
			 * 
			 * //01.A4,6920687730202,y,y
			 */
			// 2004.10.21
			String str = app.getPopupMenuPane().getAllDescription();
			StringTokenizer tk = new StringTokenizer(str, ",");
			int count = 0;
			canPrint = "y";
			needPhoneCardNumber = "n";
			String subStr = "";
			while (tk.countTokens() != 0) {
				subStr = tk.nextToken();
				if (count == 0)
					daishouDetailName = subStr.substring(
							subStr.indexOf('.') + 1, subStr.length());
				else if (count == 1)
					pluNumber = subStr;
				else if (count == 2)
					canPrint = subStr;
				else if (count == 3)
					needPhoneCardNumber = subStr;
				count++;
			}
			CreamToolkit.logMessage("DaiShouButton |  daishouDetailName : "
					+ daishouDetailName + " | pluNumber: " + pluNumber
					+ " | canPrint : " + canPrint + " | needPhoneCardNumber : "
					+ needPhoneCardNumber);

			POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
			POSButtonHome2.getInstance().buttonPressed(e);
		} else {
			POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
			POSButtonHome2.getInstance().buttonPressed(e);
		}
	}

	public boolean needPhoneCardNumber() {
		if (needPhoneCardNumber.equalsIgnoreCase("Y"))
			return true;
		return false;
	}

	public ArrayList readFromFile(String filename) {
		if (filename.indexOf(File.separator) == -1)
			filename = CreamToolkit.getConfigDir() + filename;
		File propFile = new File(filename);
		ArrayList menuArrayList = new ArrayList();
		try {
			FileInputStream filein = new FileInputStream(propFile);
			InputStreamReader inst = null;
			inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
			BufferedReader in = new BufferedReader(inst);
			String line;
			char ch = ' ';
			int i;

			do {
				do {
					line = in.readLine();
					if (line == null) {
						break;
					}
					while (line.equals("")) {
						line = in.readLine();
					}
					i = 0;
					do {
						ch = line.charAt(i);
						i++;
					} while ((ch == ' ' || ch == '\t') && i < line.length());
				} while (ch == '#' || ch == ' ' || ch == '\t');

				if (line == null) {
					break;
				}
				menuArrayList.add(line);
			} while (true);
		} catch (FileNotFoundException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("File is not found: " + propFile + ", at "
					+ this);
			return null;
		} catch (IOException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("IO exception at " + this);
			return null;
		}
		return menuArrayList;
	}

}
