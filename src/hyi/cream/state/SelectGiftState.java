package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.GiftGroup;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.uibeans.AgeLevelButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * A Class class.
 * <P>
 * 
 * @author allan
 */
public class SelectGiftState /*extends State*/ {
//	private static SelectGiftState selectGiftState = null;
//	private POSTerminalApplication app = POSTerminalApplication.getInstance();
//	private Class exitState;
//	private String numberString;
//	private static Map triggerList; // 存放客显游戏或选择画面的触发源 GiftTrigger
//	private static Map oldTriggerList;
//	private static Map newTriggerList;
//	private Object currentTrigger;
//	private String itemNO;
//	private GiftGroup gg;
//
//	public static SelectGiftState getInstance() {
//		try {
//			if (selectGiftState == null) {
//				selectGiftState = new SelectGiftState();
//			}
//		} catch (InstantiationException ex) {
//		}
//		return selectGiftState;
//	}
//
//	/**
//	 * Constructor
//	 */
//	public SelectGiftState() throws InstantiationException {
//		triggerList = new HashMap();
//		oldTriggerList = new HashMap();
//		newTriggerList = new HashMap();
//		selectGiftState = this;
//	}
//
//	public void addTrigger(Object source) {
//		GiftTrigger gt = new GiftTrigger();
//		gt.setSource(source);
//		gt.setStatus(false);
//		gt.setGiftIndex(-1);
//		newTriggerList.put(source, gt);
//		if (!oldTriggerList.containsKey(source))
//			triggerList.put(source, gt);
//	}
//
//	/**
//	 * trigger有变动就可进入SelectGiftState
//	 *
//	 * @return
//	 */
//	public boolean isPermit() {
//		if (oldTriggerList.size() == 0 && newTriggerList.size() == 0)
//			return false;
//		else if (oldTriggerList.size() != newTriggerList.size()) {
//			return true;
//		}
//		return false;
//	}
//
//	public Map getTriggerList() {
//		return triggerList;
//	}
//
//	public void setTriggerUsed(Object source, boolean used) {
//		if (oldTriggerList.containsKey(source)) {
//			GiftTrigger gt = (GiftTrigger) oldTriggerList.get(source);
//			gt.setStatus(used);
//		}
//	}
//
//	public void setGiftIndex(Object source, int index) {
//		if (oldTriggerList.containsKey(source)) {
//			GiftTrigger gt = (GiftTrigger) oldTriggerList.get(source);
//			gt.setGiftIndex(index);
//		}
//	}
//
//	/**
//	 *
//	 * @param source
//	 * @return -1 : not found gift
//	 */
//	public int getGiftIndex(Object source) {
//		if (oldTriggerList.containsKey(source)) {
//			GiftTrigger gt = (GiftTrigger) oldTriggerList.get(source);
//			return gt.getGiftIndex();
//		}
//		return -1;
//	}
//
//	public Object getCurrentTrigger() {
//		return currentTrigger;
//	}
//
//	/**
//	 * Invoked when entering this state.
//	 *
//	 * <P>
//	 * Just get a String from getPromptedMessage() and display it on the warning
//	 * indicator.
//	 *
//	 * @param event
//	 *            the triggered event object.
//	 * @param sourceState
//	 *            the ultimate source state
//	 */
//	public void entry(EventObject event, State sourceState) {
//		exitState = null;
//		// super.entry(event, sourceState);
//		if (event.getSource() instanceof AgeLevelButton) {
//			numberString = "";
//			if (oldTriggerList.isEmpty()) {
//				app.getMessageIndicator().setMessage(
//						CreamToolkit.GetResource().getString(
//								"PleaseInputSelectGiftID"));
//				app.setDigPrizeMessage(
//						CreamToolkit.GetResource().getString(
//						"PleaseInputSelectGiftID"));
//				for (Iterator it = triggerList.keySet().iterator(); it
//						.hasNext();) {
//					Object key = it.next();
//					oldTriggerList.put(key, triggerList.get(key));
//				}
//				triggerList.clear();
//				// newTriggerList.clear();
//				currentTrigger = oldTriggerList.keySet().iterator().next();
//				System.out.println("------------- SelectGiftState | currentTrigger : " + currentTrigger.toString());
//				app.setAnimatedVisible(true, currentTrigger.toString());
//			} else if (!triggerList.isEmpty()) {
//				for (Iterator it = triggerList.keySet().iterator(); it
//						.hasNext();) {
//					Object key = it.next();
//					oldTriggerList.put(key, triggerList.get(key));
//				}
//			} else if (oldTriggerList.size() > newTriggerList.size()) {
//				app.getMessageIndicator().setMessage(
//						CreamToolkit.GetResource().getString("GiftRemoved2"));
//				app.setDigPrizeMessage(CreamToolkit.GetResource().getString(
//						"GiftRemoved1"));
//				exitState = IdleState.class;
//				Map tmpList = new HashMap();
//				for (Iterator it = oldTriggerList.keySet().iterator(); it
//						.hasNext();) {
//					Object key = it.next();
//					tmpList.put(key, oldTriggerList.get(key));
//				}
//				tmpList.remove(newTriggerList);
//				for (Iterator it = tmpList.keySet().iterator(); it.hasNext();) {
//					GiftTrigger gt = (GiftTrigger) tmpList.get(it.next());
//					Transaction curTran = app.getCurrentTransaction();
//					Object[] obj = curTran.getLineItemsWithoutCurrentOne();
//					// Object [] obj2 =
//					// curTransaction.getDisplayedLineItemsArray().toArray();
//					LineItem removeItem = (LineItem) obj[gt.getGiftIndex()];
//					if (removeItem.getRemoved()) {
//						return;
//					}
//					removeItem.setRemoved(true);
//					try {
//						LineItem voidItem = VoidState
//								.createVoidLineItem(removeItem);
//						voidItem.setDetailCode("V");
//						voidItem.setPrinted(false);
//						curTran.addLineItem(voidItem);
//					} catch (TooManyLineItemsException e) {
//						CreamToolkit.logMessage(e.toString());
//						CreamToolkit.logMessage("Too many LineItem at " + this);
//					}
//				}
//				for (Iterator it = tmpList.keySet().iterator(); it.hasNext();) {
//					Object key = it.next();
//					oldTriggerList.remove(key);
//				}
//				System.out.println("-------------delete trigger");
//			}
//		}
//	}
//
//	/**
//	 * Invoked when exiting this state.
//	 *
//	 * <P>
//	 * Clear warningIndicator and return sinkState.getClass().
//	 *
//	 * @param event
//	 *            the triggered event object.
//	 * @param sinkState
//	 *            the sink state
//	 */
//	public Class exit(EventObject event, State sinkState) {
//		app.getWarningIndicator().setMessage("");
//		if (exitState == null && event.getSource() instanceof NumberButton) {
//			NumberButton pb = (NumberButton) event.getSource();
//			numberString = numberString + pb.getNumberLabel();
//			app.getMessageIndicator().setMessage(numberString);
//			gg = app.fireDigPrizeEvent(Integer.parseInt(numberString));
//			app.setEnabledPopupMenu(true);
//			// app.setDigPrizeMessage("select ok..........");
//			exitState = PluReadyState.class;
//		}
//		return exitState;
//	}
//
//	public GiftGroup getRetGift() {
//		return gg;
//	}
//
//	public void initSelectGiftState() {
//		triggerList.clear();
//		newTriggerList.clear();
//		oldTriggerList.clear();
//	}
//
//	public void initSelectGiftState2() {
//		newTriggerList.clear();
//	}
//}
//
//class GiftTrigger {
//	private Object source;
//	private boolean status;
//	private int giftIndex;
//
//	public Object getSource() {
//		return source;
//	}
//
//	public void setSource(Object source) {
//		this.source = source;
//	}
//
//	public boolean getStatus() {
//		return status;
//	}
//
//	public void setStatus(boolean status) {
//		this.status = status;
//	}
//
//	public int getGiftIndex() {
//		return giftIndex;
//	}
//
//	public void setGiftIndex(int giftIndex) {
//		this.giftIndex = giftIndex;
//	}
}