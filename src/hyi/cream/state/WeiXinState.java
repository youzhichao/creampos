package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.Scanner;
import hyi.spos.events.DataEvent;

import java.util.EventObject;
import java.util.ResourceBundle;

//微信扫描条码
public class WeiXinState extends State {
	private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private ResourceBundle res            = CreamToolkit.GetResource();
    
    private String numberStr = "";
    private Indicator warningIndicator = app.getWarningIndicator();
    private Indicator messgeIndicator = app.getMessageIndicator();
    public String payID = "";
    static WeiXinState weiXinState = null;
    public static WeiXinState getInstance() {
        if (weiXinState == null) {
            weiXinState = new WeiXinState();
        }
        return weiXinState;
    }


	public void entry(EventObject event, State sourceState) {
        Object eventSource = event.getSource();
        if (sourceState instanceof Numbering2State) {
        	numberStr = "";
        	warningIndicator.setMessage("");
            payID = ((WeiXinButton)eventSource).getPaymentID();
            messgeIndicator.setMessage(res.getString("PleaseInputWeiXinNumber"));
        }
        if (sourceState instanceof SummaryState) {
        	numberStr = "";
        	warningIndicator.setMessage("");
            payID = ((WeiXinButton)eventSource).getPaymentID();
            messgeIndicator.setMessage(res.getString("PleaseInputWeiXinNumber"));
        } else if (sourceState instanceof WeiXinState) {
            if (eventSource instanceof NumberButton) {
                numberStr = numberStr + ((NumberButton) eventSource).getNumberLabel();
                warningIndicator.setMessage("");
                messgeIndicator.setMessage(numberStr);
            } else if (eventSource instanceof ClearButton) {
                numberStr = "";
                warningIndicator.setMessage("");
                messgeIndicator.setMessage(res.getString("PleaseInputWeiXinNumber"));
            } else if (eventSource instanceof EnterButton || eventSource instanceof Scanner) {
                numberStr = "";
                messgeIndicator.setMessage(res.getString("PleaseInputWeiXinNumber"));
            } 
        }
	}

	public Class exit(EventObject event, State sinkState) {
		Object eventSource = event.getSource();
        if (eventSource instanceof EnterButton || eventSource instanceof Scanner) {
        	if (eventSource instanceof EnterButton) {
        		if (numberStr.equals("")) {
        			warningIndicator.setMessage("");
        			return this.getClass();
        		}
        	}
        	if (eventSource instanceof Scanner) {
                try {
                    DataEvent dataEvent = (DataEvent)event;
                    numberStr = new String(((Scanner)eventSource).getScanData(dataEvent.seq));
                } catch (JposException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("Jpos exception at " + this);
                }
            }

            return WeiXinProcessState.class;

        } else if (eventSource instanceof ClearButton) {
            if (numberStr.equals("")) {

            	warningIndicator.setMessage("");
                return SummaryState.class;
            } else {
                return this.getClass();
            }
            
        } else if (eventSource instanceof NumberButton) {
            return this.getClass();
        }
		return null;
	}

    public String getNumberStr() {
        return numberStr;
    }
}
