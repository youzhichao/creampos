package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;

import java.util.EventObject;

import protector.Protector;

/**
 * 输入 license number State.
 */
public class LicenseNumberingState extends State {
    private static LicenseNumberingState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private static StringBuffer licenseID = new StringBuffer();

    public static LicenseNumberingState getInstance() {
        try {
            if (instance == null) {
                instance = new LicenseNumberingState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public LicenseNumberingState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
		//super.entry(event, sourceState);
        if (event != null && event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            licenseID.append(pb.getNumberLabel());
            app.getMessageIndicator().setMessage(licenseID.toString());
            return;
        }
        if (licenseID.length() == 0) {
            app.getMessageIndicator().setMessage("Please input license ID (Machine ID=" +
                Protector.getMachineUniqueID() + ")");
        } else {
            app.getWarningIndicator().setMessage("");
        }
    }

    public Class exit(EventObject event, State sinkState) {
		//super.exit(event, sinkState);
        if (event.getSource() instanceof EnterButton) {
            if (!Protector.generateLicenseID().equals(licenseID.toString())) {
                licenseID.setLength(0);
                app.getWarningIndicator().setMessage("License ID is not correct!");
                return LicenseNumberingState.class;
            }

            Protector.createUserLicenseFile(licenseID.toString());
            app.getWarningIndicator().setMessage("");
            app.getMessageIndicator().setMessage("");

            if (PARAM.getNeedTurnKeyAtStart())
                app.setBeginState(true);  // let it need turn key

            return InitialState.class;

        } else if (event.getSource() instanceof ClearButton) {
            licenseID.setLength(0);
            return sinkState.getClass();
        }

        if (licenseID.length() == 0) {
            app.getWarningIndicator().setMessage("");
        }
        return sinkState.getClass();
    }

    public static String getLicenseID() {
        return licenseID.toString();
    }
}
