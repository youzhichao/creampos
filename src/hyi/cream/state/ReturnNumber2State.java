package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.Store;
import hyi.cream.dac.Reason;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.inline.Client;
import hyi.cream.inline.ConnectionlessClient;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.uibeans.EmptyButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.util.NumberConfuser;
import hyi.spos.JposException;
import hyi.spos.Scanner;
import hyi.spos.events.DataEvent;

import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;
import java.util.List;
import java.util.ResourceBundle;
import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

//import jpos.JposException;

public class ReturnNumber2State extends State {
    static ReturnNumber2State returnNumber2State = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    //private Transaction toVoidTran = null;
    //private Transaction trans = app.getCurrentTransaction();
    private static String numberString = "";
    private String transactionID = "";
    private String voidPosNo = "";
    private ResourceBundle res = CreamToolkit.GetResource();
    private int step = 0;

    public static enum RefundType { Normal, DownPayment };
    private RefundType refundType;

    public static Transaction originalPeiDaTransaction;
    private Transaction toVoidTran;
    private String deliveryVersion = "";
    private String deliveryNo = "";
    private String storeID;
    private String origTransactionID = "";
    private String srcBillNo;

    public static ReturnNumber2State getInstance() {
        try {
            if (returnNumber2State == null) {
                returnNumber2State = new ReturnNumber2State();
            }
        } catch (InstantiationException ex) {
        }
        return returnNumber2State;
    }

    /**
     * Constructor
     */
    public ReturnNumber2State() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        app.getItemList().setBackLabel(res.getString("BigReturnNumberLabel"));
        Object eventSource = event.getSource();
        if (sourceState instanceof InputDownPaymentState) { // (DownPayment: IPD2RN)
            // generate a EmptyButton event to make it move to ReturnShowState
            StateMachine.getInstance().processEvent(new POSButtonEvent(new EmptyButton(0, 0, 0, "")));
        } else if (eventSource instanceof NumberButton && sourceState instanceof ReturnNumber2State) {
            NumberButton pb = (NumberButton)eventSource;
            numberString = numberString + pb.getNumberLabel(); // 接受合并输入的数字
            showMessage(numberString);
        } else if (step == 1 && eventSource instanceof EnterButton && sourceState instanceof ReturnNumber2State) {
            showMessage("ReturnReady2"); // "请输入交易序号"
        } else if (step == 2 && eventSource instanceof EnterButton && sourceState instanceof ReturnNumber2State) {
            showMessage("ReturnReady3"); // "本店退貨請按[確認];若為他店退貨,請輸入原始門市店號(如高雄店輸入4001)"
        } else {
            // 初次进入退货，初始化当前交易transaction资料
            transactionID = "";
            voidPosNo = "";
            step = 0;
            numberString = "";
            Transaction trans = app.getCurrentTransaction();
            trans.setDealType1("0");
            trans.setDealType2("3");
            trans.setDealType3("4");
            showMessage("ReturnReady1"); //"扫描返金传票单号或输入POS机号"
//          if (sourceState instanceof CashierRightsCheckState
//              || (eventSource instanceof SelectButton && sourceState instanceof KeyLock1State)) {
            app.getItemList().setItemIndex(0);
//          }
        }
    }

    private Date nDaysAgo(int n) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -n);
        c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        return c.getTime();
    }

    public Class exit(EventObject event, State sinkState) {
        Client inlineClient = Client.getInstance();
        boolean isPeiDa = false;
        Object eventSource = event.getSource();

        if (eventSource instanceof EnterButton || eventSource instanceof Scanner || eventSource instanceof EmptyButton) {
            if (eventSource instanceof EnterButton && step == 0) {
                voidPosNo = numberString; // 接受POS机号数字
                numberString = "";
                step = 1;
                return ReturnNumber2State.class;
            } else if (eventSource instanceof EnterButton && step == 1) {
                transactionID = numberString;
                numberString = "";
                step = 2;
                return ReturnNumber2State.class;
            } else if (eventSource instanceof Scanner) {
                // 扫描返金传票单号
                numberString = "";
                try {
                    //((Scanner) eventSource).setDecodeData(true);
                    numberString = new String(((Scanner) eventSource)
                            .getScanData(((DataEvent)event).seq));
                } catch (JposException e) {
                    e.printStackTrace();
                }

                // 传票退货处理
                if (numberString.startsWith("28")) {
                    deliveryNo = numberString.substring(2, 10);
                    deliveryVersion = numberString.substring(10, 12);
                    // 是否是返金传票，返会原单据号
                    int status = -1;
                    try {
                        /* -1=无法连接sc
                         * 可結帳
                         *    'P'; //普通配達
                         *    'M'; //窗簾修改
                         *    'O'; //窗簾訂做
                         * 1=不存在
                         * 2=已作廢
                         * 3=售後處理
                         * 4=結清,pos不可結
                         * 5=被退過貨的
                         * 6=單據版本不符合
                         * 7=退货机可退貨,pos不可结帐
                         * 8=异常
                         */
                        String statusAndSrcBillNo = StateToolkit.getPeiDaStatus(deliveryNo + deliveryVersion);
                        // status = deliveryhead.結帳狀態 + "," + deliveryhead.srcBillNo(原始單號)
                        String[] x = statusAndSrcBillNo.split(",");
                        status = Integer.parseInt(x[0]);
                        srcBillNo = x[1];
                        storeID = "S4" + srcBillNo.substring(0, 3); // 如果是他店退貨，是原始店號
                    } catch (Exception e) {
                        CreamToolkit.logMessage(e);
                    }

                    // 如果是退訂金，不檢查狀態
                    if (refundType == RefundType.DownPayment /*|| refundType == RefundType.BackPayment*/)
                        return InputDownPaymentState.class; // (DownPayment: RN2IDP)

                    if (status == -1 || status == 8 || storeID == null) { // 8=异常
                        showMessage("PeiDaError", deliveryNo);
                        CreamToolkit.logMessage("PeiDaError: status=" + status + ", storeID=" + storeID);
                        return WarningState.class;
                    } else if (status != 7) {
                        showMessage("PeiDaReturnWarning1", deliveryNo);
                        CreamToolkit.logMessage("PeiDaError: status=" + status + ", storeID=" + storeID);
                        return WarningState.class;
                    }

                    Class state = getOrigTranNoFromDeliveryID(true, storeID, deliveryNo, srcBillNo, inlineClient);
                    if (state != null)
                        return state;

                    isPeiDa = true;
                } else {
                    showMessage("PeiDaReturnWarning"); //"非传票条码,按[清除]键返回！"
                    return WarningState.class;
                }
            } else if (eventSource instanceof EmptyButton) { // (DownPayment: RN2RS)
                Class state = getOrigTranNoFromDeliveryID(false, storeID, deliveryNo, srcBillNo, inlineClient);
                if (state != null)
                    return state;
                isPeiDa = true;
            }

            if (step == 2 && !isPeiDa) {
                // 已经输入好pos机号和交易序號，非配达
                if (StringUtils.isEmpty(numberString))
                    storeID = Store.getStoreID();
                else
                    storeID = "S" + numberString;
                //voidPosNo = app.getCurrentTransaction().getTerminalNumber().toString();
            } else {
                transactionID = origTransactionID.substring(0, origTransactionID.length() - 2);
                voidPosNo = origTransactionID.substring(origTransactionID.length() - 2);
            }

            //如果交易序号是经过打乱的，把它还原
            if (PARAM.isConfuseTranNumber())
                transactionID = NumberConfuser.defuse(transactionID);
            
            if (voidPosNo.length() == 1)
                voidPosNo = "0" + voidPosNo;
            String voidTranID = transactionID + voidPosNo;
            
            //if (refundType != RefundType.BackPayment && // 退訂金尾款不用檢查是不是被退過
            if (Transaction.existByVoidTranID(voidTranID)) {
                showMessage(res.getString("ReturnWarning2")); //"此交易已退！"
                return WarningState.class;
            }

            if (storeID == null)
                storeID = Store.getStoreID();
            
            if (isPeiDa) {
                toVoidTran = new Transaction();
                toVoidTran.init();

                try {
                    toVoidTran = StateToolkit.posulTran2Tran(storeID, // 如果是他店退貨, storeID是他店店號，所以會去查他店的交易
                        voidPosNo, transactionID);
                    toVoidTran.setAccountingDate(CreamToolkit.getInitialDate());
                    toVoidTran.setStoreNumber(Store.getStoreID());    // 改填本店店號
                    if (!Store.getStoreID().equals(storeID))
                        toVoidTran.setOtherStoreID(storeID);          // 他店退貨時，otherStoreID暫存他店店號
                } catch (Exception e) {
                    showMessage("ReturnWarning3", transactionID);
                    CreamToolkit.logMessage(e.getMessage() + "! when got " + voidPosNo + "." + transactionID);
                    e.printStackTrace();
                    return WarningState.class;
                }
                originalPeiDaTransaction = (Transaction)toVoidTran.deepClone();
                CreamSession.getInstance().setTransactionToBeRefunded(toVoidTran);

                toVoidTran.clearLineItem();
                List<LineItem> lineItems;
                try {
                    lineItems = StateToolkit.getPeiDaDtl2LineItem(toVoidTran,  deliveryNo + deliveryVersion);
                } catch (Exception e) {
                    showMessage("PeiDaError", deliveryNo);
                    CreamToolkit.logMessage(e.getMessage() + "! when got " + voidPosNo + "." + transactionID);
                    e.printStackTrace();
                    return WarningState.class;
                }

                //if (refundType != RefundType.BackPayment) { // 如果是退訂金尾款，交易不需要顯示配送商品
                HYIDouble zero = new HYIDouble(0);
                for (LineItem lineItem : lineItems) {
                    try {
                        if (refundType == RefundType.DownPayment) { // 如果是退訂金，把原傳票中的商品金額全部設成零
                            lineItem.setUnitPrice(zero);
                            lineItem.setAmount(zero);
                            lineItem.setAfterDiscountAmount(zero);
                        }
                        toVoidTran.addLineItem(lineItem, false);
                    } catch (TooManyLineItemsException e) {
                        e.printStackTrace();
                    }
                }

                if (refundType != RefundType.Normal) {  // 如果是退訂金 /*或退訂金尾款*/，把剛剛在InputDownPaymentState輸入的金額新增一筆着付金明細
                    HYIDouble downPayment = InputDownPaymentState.getInstance().getDownPaymentAmount();
                    HYIDouble negativeAmount = downPayment.negate();

                    LineItem lineItem = new LineItem();
                    lineItem.setPluNumber(PARAM.getDownPaymentItemPluNo());
                    lineItem.setItemNumber(PARAM.getDownPaymentItemNo());
                    String itemName = m("DownPayment"); //PeiDa3IdleState.getInstance().isBackPayment() ?  m("BackPayment") : m("DownPayment");
                    lineItem.setDescriptionAndSpecification(itemName);
                    lineItem.setDescription(itemName);
                    lineItem.setTerminalNumber(getPOSNumber());
                    lineItem.setTransactionNumber(toVoidTran.getTransactionNumber());
                    lineItem.setDetailCode("R");
                    lineItem.setLineItemSequence(toVoidTran.getLineItems().length + 1);
                    String depNo = PARAM.getDownPaymentItemDepNo();
                    lineItem.setDepID(depNo);
                    lineItem.setCategoryNumber(depNo.substring(0, 1));
                    lineItem.setMidCategoryNumber(depNo.substring(1, 2));
                    lineItem.setMicroCategoryNumber(depNo.substring(2, 3));
                    lineItem.setOriginalPrice(downPayment);
                    lineItem.setUnitPrice(downPayment);
                    lineItem.setAfterDiscountAmount(negativeAmount);
                    lineItem.setQuantity(new HYIDouble(-1));
                    lineItem.setAmount(negativeAmount);
                    lineItem.setTaxType("1");
                    lineItem.setTaxAmount(negativeAmount.divide(new HYIDouble(1.05), BigDecimal.ROUND_HALF_UP)
                        .multiply(new HYIDouble(0.05)));
                    try {
                        toVoidTran.addLineItem(lineItem, false);
                    } catch (TooManyLineItemsException e) {
                        e.printStackTrace();
                    }
                }

                toVoidTran.setVoidTransactionNumber(Integer.parseInt(voidTranID));
                toVoidTran.setAnnotatedId(deliveryNo + deliveryVersion);
                originalPeiDaTransaction.setVoidTransactionNumber(Integer.parseInt(voidTranID));
                originalPeiDaTransaction.setAnnotatedId(deliveryNo + deliveryVersion);
                toVoidTran.initForAccum(); //对该tran中各个计算字段置初始值（零）
                toVoidTran.accumulate(); //由明细LintItems重算交易trand的一些汇总项目

            } else {
                // 不是配达的退货
                try {
                    originalPeiDaTransaction = null;
                    toVoidTran = StateToolkit.posulTran2Tran(storeID, // 如果是他店退貨, storeID是他店店號，所以會去查他店的交易
                        voidPosNo, transactionID);
                    CreamSession.getInstance().setTransactionToBeRefunded(toVoidTran);
                    toVoidTran.setStoreNumber(Store.getStoreID());    // 改填本店店號
                    if (!Store.getStoreID().equals(storeID))
                        toVoidTran.setOtherStoreID(storeID);          // 他店退貨時，otherStoreID暫存他店店號

                    if (toVoidTran == null) {
                        showMessage("ReturnWarning3"); // "无法在SC找到此交易，无法退货！"
                        CreamToolkit.logMessage("Not found " + voidPosNo + "." + transactionID);
                        return WarningState.class;
                    }

                    if (toVoidTran.isPeiDa()) {
                        showMessage("ReturnWarning4"); // "此交易是传票交易,请用返金传票进行退货！"
                        CreamToolkit.logMessage("Not found " + voidPosNo + "." + transactionID);
                        return WarningState.class;
                    }
                } catch (Exception e) {
                    showMessage("ReturnWarning3");
                    CreamToolkit.logMessage(e.getMessage() + "! when got " + voidPosNo + "." + transactionID);
                    e.printStackTrace();
                    return WarningState.class;
                }
            }
            
            if (toVoidTran == null
            // 一般交易 && 销售交易 && 正常或退货交易
                || !toVoidTran.getDealType1().equals("0")
                || !toVoidTran.getDealType2().equals("0")
                || !(toVoidTran.getDealType3().equals("0")
                    || toVoidTran.getDealType3().equals("4")) //  check daishou and daifu
                || (toVoidTran.getDaiFuAmount().compareTo(new HYIDouble(0)) == -1) //Bruce/20030519/
            //30天前的交易不允许退货
//                || toVoidTran.getSystemDateTime().before(nDaysAgo(30)) //Bruce/20030519/
            //For灿坤：隔日的配达交易不允许做退货
//                || (toVoidTran.getAnnotatedType() != null
//                    && toVoidTran.getAnnotatedType().equals("P")
//                    && toVoidTran.getSystemDateTime().before(
//                        nDaysAgo(0))) /*|| (oldTran.getDaiShouAmount().compareTo(new HYIDouble(0)) == 1)*/
                )
            {
                showWarningMessage("ReturnWarning");    // "此交易无法退货！"
                return ReturnNumber2State.class;
//            } else if (toVoidTran.getLineItems().length == 0){
//            //明细资料丢失
//                app.getWarningIndicator().setMessage(res.getString("ReturnDetailLostWarning"));
//                return ReturnNumber2State.class;
            } else {
                clearWarningMessage();
                app.setNewCashierID("");
                app.setReturnFromSC(true);
                toVoidTran.setVoidTransactionNumber( // 作废交易中记录: 新交易 的交易序号和 pos机号
                        Integer.parseInt(transactionID) * 100 + Integer.parseInt(voidPosNo));
                return ReturnShowState.class;
            }
        }

        if (eventSource instanceof ClearButton) {
            if (numberString.equals("")) {
                clearMessage();
                clearWarningMessage();
                app.getItemList().setBackLabel(null);
                originalPeiDaTransaction = null;
                return CashierRightsCheckState.getSourceState();
//                return KeyLock1State.class;
           } else {
                numberString = "";
                if (toVoidTran != null) {
                    toVoidTran = null;
                }
                showMessage("ReturnReady1");
                return ReturnNumber2State.class;
            }
        }

        if (eventSource instanceof NumberButton) {
            app.getWarningIndicator().setMessage("");
        }

        return sinkState.getClass();
    }

    public RefundType getRefundType() {
        return refundType;
    }

    public void setRefundType(RefundType refundType) {
        this.refundType = refundType;
    }

    private Class getOrigTranNoFromDeliveryID(boolean forRefund, String storeID, String deliveryNo, String srcBillNo, Client inlineClient) {
        // 根据原单据号，获得原交易号
        try {
            //String storeID = POSTerminalApplication.getInstance()
            //        .getCurrentTransaction().getStoreNumber();

            String cmd = forRefund ? "getOrigTranNoFromDeliveryIDForRefund " : "getOrigTranNoFromDeliveryID ";
            Object origTranNo;
            if (!Store.getStoreID().equals(storeID)) {
                // 取他店配達交易序號
                ConnectionlessClient client = null;
                try {
                    String storeIP = Reason.queryStoreIPAddress(storeID);
                    client = new ConnectionlessClient(storeIP, 3000);

                    // 店號給本店的，這樣才能讓他店的inline server知道這個request是來自他店的
                    client.processCommand(cmd + Store.getStoreID() + " " + srcBillNo);

                    origTranNo = client.getReturnObject2();
                } finally {
                    if (client != null)
                        client.closeConnection();
                }
            } else {
                inlineClient.processCommand(cmd + storeID + " " + deliveryNo);
                origTranNo = inlineClient.getReturnObject2();
            }
            if (origTranNo == null || origTranNo.toString().equals("null") || origTranNo.toString().equals("-1")) {
                showMessage("PeiDaReturnWarning3");
                return WarningState.class;
            } else {
                origTransactionID = origTranNo.toString();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            showMessage("PeiDaReturnWarning3");
            return WarningState.class;
        }
    }
}
