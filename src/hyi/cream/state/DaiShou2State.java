package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.DaiShouDef;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.DaiShouButton2;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.Scanner;
import hyi.spos.events.DataEvent;

import java.util.ArrayList;
import java.util.EventObject;

//import jpos.JposException;

/**
 * @author Administrator
 * 
 * To change this generated comment go to Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DaiShou2State extends State {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private static DaiShou2State instance = null;
    private String daiShouBarcode = "";
    private ArrayList daiShouDefs = new ArrayList();

    public DaiShou2State() throws InstantiationException {

    }

    public static DaiShou2State getInstance() {
        if (instance == null) {
            try {
                instance = new DaiShou2State();
            } catch (InstantiationException ex) {
            }
        }
        return instance;
    }

    /*
     * (non-Javadoc)
     * 
     * @see hyi.cream.state.State#entry(java.util.EventObject, hyi.cream.state.State)
     */
    public void entry(EventObject event, State sourceState) {
        Object eventSource = event.getSource();
        if ((sourceState instanceof IdleState && eventSource instanceof DaiShouButton2)
            || (sourceState instanceof DaiShou2State && eventSource instanceof ClearButton)) {
            daiShouBarcode = "";
            app.getMessageIndicator().setMessage(
                CreamToolkit.GetResource().getString("DaiShouNumber"));
        } else if (sourceState instanceof DaiShou2State && eventSource instanceof NumberButton) {
            String s = ((NumberButton)eventSource).getNumberLabel();
            daiShouBarcode += s;
            app.getMessageIndicator().setMessage(daiShouBarcode);
        } else if (sourceState instanceof DaiShou2State && eventSource instanceof Scanner) {
            try {
                //((Scanner)eventSource).setDecodeData(true);
                DataEvent dataEvent = (DataEvent)event;
                daiShouBarcode = new String(((Scanner)eventSource).getScanData(dataEvent.seq));
            } catch (JposException e) {
            	e.printStackTrace();
            	e.printStackTrace(CreamToolkit.getLogger());
            }
        }

    }

    public Class exit(EventObject event, State sinkState) {
    	Class exitClass = null;
        Object eventSource = event.getSource();
        if (eventSource instanceof ClearButton) {
            app.getWarningIndicator().setMessage("");
            if (daiShouBarcode.length() > 0)
            	exitClass = DaiShou2State.class; 
            else
            	exitClass = IdleState.class;
        } else if (eventSource instanceof EnterButton || eventSource instanceof Scanner) {
            if (eventSource instanceof Scanner) {
                try {
                    //((Scanner)eventSource).setDecodeData(true);
                    DataEvent dataEvent = (DataEvent)event;
                    daiShouBarcode = new String(((Scanner)eventSource).getScanData(dataEvent.seq));
                } catch (JposException e) {
        			CreamToolkit.logMessage(e);
                }
            }
            if (!PARAM.isDaiShou2BillNoDuplicatedAllowed() && Transaction.isDaiShou2BillInputed(daiShouBarcode)) {
                showMessage("DaiShou2BillCanNotRepeat");
                exitClass = DaiShou2State.class;
            } else {
                daiShouDefs = DaiShouDef.matchDaiShouDefs(daiShouBarcode);
                if (daiShouDefs.size() > 1) {
                    app.getMessageIndicator().setMessage("");
                    exitClass = DaiShou2CheckState.class;
                } else if (daiShouDefs.size() == 1) {
                    app.getMessageIndicator().setMessage("");
                    if (PARAM.isDaiShou2NeedWarning())
                        exitClass = DaiShou2WarningState.class;
                    else
                        exitClass = DaiShou2ReadyState.class;
                } else {
                    app.getWarningIndicator().setMessage(
                        CreamToolkit.GetResource().getString("DaiShouNumberWrong"));
                    exitClass = DaiShou2State.class;
                }
            }
        }

        if (exitClass != null){
        	return exitClass;
        }
        if (sinkState != null) {
            return sinkState.getClass();
        }
        return null;
    }

	public String getBarcode() {
        return daiShouBarcode;
    }

    public ArrayList getMatchDefs() {
        return daiShouDefs;
    }
}
