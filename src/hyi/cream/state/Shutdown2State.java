package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.Scanner;
import hyi.spos.events.DataEvent;

import java.util.EventObject;
import java.util.ResourceBundle;

//扫描关机
public class Shutdown2State extends State {
	private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private ResourceBundle res            = CreamToolkit.GetResource();
    
    private String numberStr = "";
    private Indicator warningIndicator = app.getWarningIndicator();
    private Indicator messgeIndicator = app.getMessageIndicator();
    public String payID = "";
    static Shutdown2State weiXinState = null;
    public static Shutdown2State getInstance() {
        if (weiXinState == null) {
            weiXinState = new Shutdown2State();
        }
        return weiXinState;
    }


	public void entry(EventObject event, State sourceState) {

        if (sourceState instanceof Shutdown2State) {
            if (event.getSource() instanceof NumberButton) {
                numberStr = numberStr + ((NumberButton) event.getSource()).getNumberLabel();
            } else {
                numberStr = "";
                warningIndicator.setMessage("");
                messgeIndicator.setMessage(res.getString("PleaseInputSUShutdownNumber"));
            }
        } else {
            messgeIndicator.setMessage(res.getString("PleaseInputShutdownNumber"));
        }
	}

	public Class exit(EventObject event, State sinkState) {
		Object eventSource = event.getSource();
        if (eventSource instanceof EnterButton || eventSource instanceof Scanner) {
            if (eventSource instanceof EnterButton) {
                if (numberStr.equals("")) {
                    warningIndicator.setMessage("");
                    messgeIndicator.setMessage(res.getString("PleaseInputSUShutdownNumber"));
                    return this.getClass();
                }
            }
            if (eventSource instanceof Scanner) {
                try {
                    DataEvent dataEvent = (DataEvent)event;
                    numberStr = new String(((Scanner)eventSource).getScanData(dataEvent.seq));
                } catch (JposException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("Jpos exception at " + this);
                }
            }
            if (numberStr.equals("88000038")) {
                CreamToolkit.halt2();
                return InitialState.class;
            } else {
                numberStr = "";
                messgeIndicator.setMessage(res.getString("PleaseInputSUShutdownNumber"));
                return this.getClass();
            }
        } else if (eventSource instanceof NumberButton) {
            return this.getClass();
        }
		return null;
	}

}
