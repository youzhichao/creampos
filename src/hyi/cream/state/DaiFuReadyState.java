package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Reason;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;

public class DaiFuReadyState extends State {        
    private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private LineItem lineItem             = null;
    private Reason r                      = null;
    private String number                 = "";
    private Class exitState               = null;

    static DaiFuReadyState daiFuReadyState      = null;  

    public static DaiFuReadyState getInstance() {
        try {
            if (daiFuReadyState == null) {
                daiFuReadyState = new DaiFuReadyState();
            }
        } catch (InstantiationException ex) {
        }
        return daiFuReadyState;
    }

    /**
     * Constructor
     */
    public DaiFuReadyState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
        //System.out.println("DaiFuReadyState entry");
        
        if (sourceState instanceof DaiFuIdleState
            && event.getSource() instanceof SelectButton) {
            Transaction curTran = app.getCurrentTransaction(); 
            number = ((DaiFuIdleState)sourceState).getPluNo();
            r = Reason.queryByreasonNumberAndreasonCategory(number, "09");
            lineItem = new LineItem();
            lineItem.setDescription(r.getreasonName());
            lineItem.setDetailCode("Q");
            lineItem.setPluNumber(String.valueOf(number));

			/*
			 * Meyer/2003-02-21/
			 */
            lineItem.setItemNumber(String.valueOf(number));
            
            
            lineItem.setQuantity(new HYIDouble(0));
            //lineItem.setTaxType("0");
            lineItem.setTerminalNumber(curTran.getTerminalNumber());
            lineItem.setTransactionNumber(new Integer(Transaction.getNextTransactionNumber()));

            lineItem.setUnitPrice(new HYIDouble(0.00));

			/*
			 * Meyer/2003-02-21/
			 */
			lineItem.setOriginalPrice(new HYIDouble(0.00));
						
			
            lineItem.setAmount(new HYIDouble(0.00)); 
            try {
                curTran.addLineItem(lineItem);
                exitState = DaiFuOpenPriceState.class;
            } catch (TooManyLineItemsException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Too many LineItem exception at " + this);
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyLineItems"));
                exitState = DaiFuIdleState.class;
            }
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("DaiFuReadyState exit");

		app.getMessageIndicator().setMessage("");
        return exitState;
    }
}

