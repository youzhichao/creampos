
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class BuyerState extends SomeAGReadyState {
    static BuyerState buyerState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private boolean isCancel = false;
    public static BuyerState getInstance() {
        try {
            if (buyerState == null) {
                buyerState = new BuyerState();
            }
        } catch (InstantiationException ex) {
        }
        return buyerState;
    }

    /**
	 * Constructor
     */
	public BuyerState() throws InstantiationException {
	}

    public void entry(EventObject event, State sourceState) {
		//super.entry(event, sourceState);
//		if (app.getBuyerNumberPrinted()) {
//			app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("ErrorOfBuyerNo1"));
    	isCancel = false;
		if (app.getCurrentTransaction().getBuyerNumber() != null
				&& app.getCurrentTransaction().getBuyerNumber().length() > 0) {
			isCancel = true;
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("CancelBuyerNumber"));
		} else
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputBuyerNumberHint"));
    }

	public Class exit(EventObject event, State sinkState) {
		/*if (getAlphanumericData() == null || checkValidity()) {
			return getUltimateSinkState();
		} else {
			// Get warning message by getWarningMessage() and
			// display it on warningIndicator.
			app.getWarningIndicator().setMessage(getWarningMessage());
			return getInnerInitialState();
		}*/
		app.getWarningIndicator().setMessage("");
		
		if (event == null)
			return BuyerState.class;
		
		if (event.getSource() instanceof NumberButton) {
//			if (app.getBuyerNumberPrinted()) {
//				/*POSPeripheralHome posHome = POSPeripheralHome.getInstance();
//					ToneIndicator tone = null;
//					try {
//						tone = posHome.getToneIndicator();er
//					} catch (NoSuchPOSDeviceException ne) { CreamToolkit.logMessage(ne + "!"); }
//					try {
//						if (!tone.getDeviceEnabled())
//							tone.setDeviceEnabled(true);
//						if (!tone.getClaimed()) {
//							 tone.claim(0);
//						//tone.claim(0);
//							 tone.setAsyncMode(true);
//							 tone.sound(99999, 500);
//						}
//					} catch (JposException je) {System.out.println(je);}*/
//				//super.exit(event, sinkState);
//				//System.out.println(app.getBuyerNumberPrinted());
//				return BuyerState.class;
//			} else {
//				//super.exit(event, sinkState);
//				return 	hyi.cream.state.BuyerNumberingState.class;
//			}
			return 	hyi.cream.state.BuyerNumberingState.class;
		} else if (event.getSource() instanceof EnterButton && isCancel) {
			app.getCurrentTransaction().setBuyerNumber("");
			if (app.getCurrentTransaction().isPeiDa())
				return PeiDa3OnlineState.class;
			else
				return IdleState.class;
		}
		return BuyerState.class;	
	}

	public String getWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}

