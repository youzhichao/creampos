package hyi.cream.state;

import hyi.cream.dac.CombGiftDetail;
import hyi.cream.dac.CombPromotionHeader;
import hyi.cream.dac.CombSaleDetail;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.PLU;
import hyi.cream.dac.Transaction;
import hyi.cream.event.TransactionEvent;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/**
 * NormalCombPromotion definition interface. 提供一个一般组合促销类
 * 
 * @author Meyer
 * @version 1.0
 */

public class NormalCombPromotion {
	/**
	 * 更新记录
	 * Meyer / 1.0 / 2003-01-03 1. Create the class file
	 */

	//private Transaction trans; // 要计算一般组合促销的交易

	private Object[] lineItemList;// 带有促销信息的交易明细的Array
	private TreeSet promIDSet; // 此交易需要验证的一般组合促销代号Set, 按promID反向排序
	private HashMap pluQtyMap = new HashMap(); // 此交易的商品品号与数量的Map, 品号不重复，数量累加
	private HashMap pluPriceMap = new HashMap();// 此交易的商品品号与单价的Map, 供计算商品总金额用
	private HashMap giftQtyMap = new HashMap();

	private Map combSaleDetailCache;
	
	public NormalCombPromotion() {
	    this.promIDSet = new TreeSet(new Comparator() {
	        public int compare(Object o1, Object o2) {
	            String s1 = (String) o1;
	            String s2 = (String) o2;
	            return -s1.compareTo(s2); // reverse order
	        }
	        
	        public boolean equals(Object obj) {
	            return ((String) obj).equals(this);
	        }
	    });
	}

	public NormalCombPromotion(Transaction trans, Map combSaleDetailCache) {
		this();
		this.combSaleDetailCache = combSaleDetailCache;
		init(trans);
	}

	/**
	 * 初试化 取得promIDSet, pluQtyMap
	 */
	private void init(Transaction trans) {
		if (trans != null) {
			trans.setMixAndMatchCount0(0);

			// 1. 取得交易中商品的品号与数量，存入pluQtyMap
			List matchedList = new ArrayList();
			lineItemList = trans.getLineItems();
			int tranDetailCount = lineItemList.length;
			LineItem curLineItem;// 当前交易明细
			// PLU curPlu;//当前商品
			List csdList;// 组合促销计划明细
			for (int i = 0; i < tranDetailCount; i++) {
				curLineItem = (LineItem) lineItemList[i];
				if (curLineItem.getItemNumber() == null)
					continue;
				PLU plu = PLU.queryByItemNumber(curLineItem.getItemNumber());
				if (plu == null)
					continue;
				curLineItem.setUnitPrice(plu.getUnitPrice());
				curLineItem.setAfterDiscountAmount(curLineItem.getAmount());// 促销开始前将AFTDSCAMT设值为AMT
				String detailCode = curLineItem.getDetailCode();
				if (!curLineItem.getRemoved()
						&& ("R".equalsIgnoreCase(detailCode) || "S"
								.equalsIgnoreCase(detailCode))) {
					// Meyer/2003-02-25/Added
					curLineItem.resetNoMatchedQty();

					String curPluCode = curLineItem.getItemNumber();
					int quantity = curLineItem.getQuantity().intValue();
					if (!pluQtyMap.containsKey(curPluCode)) {
						// 以出现机会多的条件作为判断条件
						// 2. 新品号，取得交易中待验证的组合促销ID，存入promIDSet
						if (!combSaleDetailCache.containsKey(curPluCode)) {
							csdList = CombSaleDetail.queryCphIDByArticleID(curPluCode);
							combSaleDetailCache.put(curPluCode, csdList);
						}
						Iterator csdIter = null;
						if (combSaleDetailCache.get(curPluCode) != null) {
							csdIter = ((List) combSaleDetailCache.get(curPluCode)).iterator();
						}
						if (csdIter != null) {
							while (csdIter.hasNext()) {
								String promID = ((CombSaleDetail) csdIter
										.next()).getCphID();
								CombPromotionHeader header = CombPromotionHeader.queryByID(promID);
								CreamToolkit.logMessage("****** Not Found : CombPromotionHeader | curPluCode : " + curPluCode + " | promID : " + promID + " | " + header);								
								if (promID != null && header != null
										&& !promIDSet.contains(promID) 
										&& (CreamToolkit.isInterzone(new Date(), header.getBeginDate(),
										header.getEndDate(), header.getBeginTime(), header.getEndTime())
										|| CreamToolkit.isInterzone(new Date(), header.getBeginDate(),
												header.getEndDate(), header.getBeginTime2(), header.getEndTime2())))
								{
									promIDSet.add(promID);
								}
							}
							matchedList.add(curPluCode);
							curLineItem
									.setUnitPrice(PLU.queryByItemNumber(curLineItem.getItemNumber()).getUnitPrice());
							curLineItem.setAfterDiscountAmount(curLineItem
									.getAmount());
						}
						pluPriceMap.put(curPluCode, curLineItem.getUnitPrice());// 保存商品单价
					} else {
						// 已有品号，数量累加
						quantity += ((Integer) pluQtyMap.get(curPluCode))
								.intValue();
						if (matchedList.contains(curPluCode)) {
							curLineItem
									.setUnitPrice(PLU.queryByItemNumber(curLineItem.getItemNumber()).getUnitPrice());
							curLineItem.setAfterDiscountAmount(curLineItem
									.getAmount());
						}
					}
					pluQtyMap.put(curPluCode, Integer.valueOf(String
							.valueOf(quantity)));

				}// end of if
			}// end of for
		}// end of if
		System.out.println("****	MM Start	****");
		System.out.println("Init...");
		printPromIDSet();
		printPluQtyMap();
	}

	/**
	 * 对promIDSet做match
	 */
	public void match(Transaction trans) {
		System.out.println();
		System.out.println("Matching...");
		Iterator promIDIter = promIDSet.iterator();
		String curPromID = "";

		// 保存同一promID的信息, 为新增MM明细使用
		// String promID = "";
		int promMatchCount = 0;
		HYIDouble totalPromAmt = new HYIDouble(0);

		// boolean isEverMatched = false;
		boolean needDupleMatch = false;
		boolean isGroupPromotion = false;
		// boolean isMatched = false;
		for (int i = 0; promIDIter.hasNext() || needDupleMatch; i++) {
			// 当还有未验证的promID或需要重复match时，则需再做一次match

			/*
			 * 1. 第一次无条件 next() 2. 当需要重复match时，保持原位，再对原promID作match动作
			 * 否则next()，对下一个promID作match
			 */
			if (i == 0 || !needDupleMatch) {
				curPromID = (String) promIDIter.next();
				isGroupPromotion = CombSaleDetail.isGroupPromotion(curPromID);// 是否群组促销
			}
			System.out.println();
			PromMatch promMatch = null;
			CombPromotionHeader combHeader = CombPromotionHeader
					.queryByID(curPromID);
			String disType = combHeader.getDiscountType();
			HYIDouble percentage = null;
			if (combHeader.getDiscountPercentage() == null
					|| combHeader.getDiscountPercentage().intValue() <= 0)
				percentage = new HYIDouble(0.4);
			else
				percentage = (new HYIDouble(10).subtract(new HYIDouble(
						combHeader.getDiscountPercentage().intValue())
						)).divide(new HYIDouble(10), BigDecimal.ROUND_HALF_UP);

			HYIDouble matchTotalAmt = new HYIDouble(0);
			int matchedQty = 0;
			if (disType.equals(GetProperty.getSecondPLUPromotionID())) {
				Object[] ret = secondPLUMatch(curPromID, percentage);
				if (ret != null) {
					matchTotalAmt = (HYIDouble) ret[0];
					matchedQty = ((Integer) ret[1]).intValue();
				}
			} else {
				promMatch = match(curPromID, isGroupPromotion);
				if (promMatch != null) {
					matchTotalAmt = promMatch.getTotalAmt();
					if (!isGroupPromotion) {
						matchedQty = ((PluPromMatch) promMatch)
								.getMatchedCount();
					} else {
						matchedQty++;
					}
					// isMatched = promMatch.isMatched();
				}
			}
			System.out.println("---- matchTotalAmt : " + matchTotalAmt);

			if (matchedQty > 0) {
				System.out.println(curPromID + " is matched");
				// isEverMatched = true;
				// 群组促销要多次match, 所以在match成功时需要重复match
				// 商品促销改为一次全部match，不需重复match
				needDupleMatch = isGroupPromotion;
				// 累计同一种组合促销
				// promID = promMatch.getPromID();
				if (!isGroupPromotion) {
					promMatchCount = matchedQty;
				} else {
					promMatchCount++;
				}
				totalPromAmt = totalPromAmt.addMe(matchTotalAmt);
			} else {
				System.out.println(curPromID + " is not matched");
				needDupleMatch = false;
				if (promMatchCount > 0) {
					createPromLineItem(trans, curPromID, promMatchCount, totalPromAmt);
					promMatchCount = 0;
					totalPromAmt = new HYIDouble(0);
				}
			}
		}
//		System.out.println("------------- " + pluQtyMap);
		lineItemList = trans.getLineItems();
		int tranDetailCount = lineItemList.length;
		LineItem curLineItem;// 当前交易明细
		for (int i = tranDetailCount - 1; i >= 0; i--) {
			curLineItem = (LineItem) lineItemList[i];
			String detailCode = curLineItem.getDetailCode();
			if (!curLineItem.getRemoved()
					&& ("R".equalsIgnoreCase(detailCode) || "S"
							.equalsIgnoreCase(detailCode))) {
				String curPluCode = curLineItem.getItemNumber();
				int quantity = curLineItem.getQuantity().intValue();
				
				if (pluQtyMap.containsKey(curPluCode) &&
						!(giftQtyMap.containsKey(curPluCode)
								&& curLineItem.getAfterDiscountAmount().compareTo(new HYIDouble(0)) == 0)) {
					int qty = 0;
					// System.out.println(curLineItem.getUnitPrice());
					if ((qty = ((Integer) pluQtyMap.get(curPluCode)).intValue()
							- quantity) >= 0) {
						curLineItem.setUnitPrice((HYIDouble) PLU.queryByItemNumber(
								curLineItem.getItemNumber()).getSalingPriceDetail()[0]);
						curLineItem.setAfterDiscountAmount(curLineItem
								.getUnitPrice().multiply(
										curLineItem.getQuantity()).setScale(2,
										BigDecimal.ROUND_HALF_UP));// 促销开始前将AFTDSCAMT设值为AMT
						curLineItem.setDiscountType("D");
						pluQtyMap.put(curPluCode, new Integer(qty));
					}
				}
			}
		}

		if (promMatchCount > 0) {
			// 新增末尾的一笔促销交易明细
			createPromLineItem(trans, curPromID, promMatchCount, totalPromAmt);
		}

		// Meyer / 2003-02-25 / Modified
		// if (isEverMatched) {
		updatePromTranHead(trans);
		trans.fireEvent(new TransactionEvent(trans,
				TransactionEvent.TRANS_INFO_CHANGED));
		// }

		System.out.println("****	MM End	****");
	}

	// 第二件商品打6折 id=9
	private Object[] secondPLUMatch(String aPromID, HYIDouble percentage) {
		// pluPriceMap(itemno,price) pluQtyMap(itemno,qty)
		List matchPluList = new ArrayList();
		Iterator csdIter = CombSaleDetail.queryByCphID(aPromID);
		while (csdIter.hasNext()) {
			CombSaleDetail csDetail = (CombSaleDetail) csdIter.next();
			if (!matchPluList.contains(csDetail.getPluCode())
					&& pluPriceMap.containsKey(csDetail.getPluCode()))
				matchPluList.add(csDetail.getPluCode());
		}
		ArrayList sortList = new ArrayList(pluPriceMap.values());
		Collections.sort(sortList);

		List sortPluList = new ArrayList();
		int tranDetailCount = lineItemList.length;
		int totMatchQty = 0;

		for (int i = sortList.size() - 1; i >= 0; i--) {
			HYIDouble price = (HYIDouble) sortList.get(i);
			for (int k = 0; k < tranDetailCount; k++) {
				LineItem curLineItem = (LineItem) lineItemList[k];
				if (!matchPluList.contains(curLineItem.getItemNumber())
						|| sortPluList.contains(curLineItem.getItemNumber()))
					continue;
				else if (curLineItem.getOriginalPrice().compareTo(price) == 0) {
					sortPluList.add(curLineItem.getItemNumber());
					totMatchQty += ((Integer) pluQtyMap.get(curLineItem
							.getItemNumber())).intValue();
				}
			}
		}

		Map matchedPluMap = new HashMap();
		HYIDouble totMatchDisAmt = new HYIDouble(0); // 促销折扣
		HYIDouble totDisAmt = new HYIDouble(0); // 特价折扣
		HYIDouble totAmt = new HYIDouble(0); // matched (原价)total amount
		if (totMatchQty < 2)
			return null;
		int matchQty = totMatchQty / 2;
		totMatchQty = matchQty * 2;
		int leaveMatchQty = matchQty;
		int needMatchQty = 0;
		List matchedList = new ArrayList();
		for (int i = 0; i < sortPluList.size(); i++) {
			String itemno = (String) sortPluList.get(i);
			HYIDouble salingPrice = (HYIDouble) PLU.queryByItemNumber(itemno).getSalingPriceDetail()[0];
			matchedList.add(itemno);
			int qty = ((Integer) pluQtyMap.get(itemno)).intValue();
			if (leaveMatchQty != 0) {
				int cc = leaveMatchQty > qty ? qty : leaveMatchQty;
				leaveMatchQty -= cc;
				totMatchDisAmt = totMatchDisAmt.add(((HYIDouble) pluPriceMap
						.get(itemno)).multiply(new HYIDouble(cc)).multiply(
						percentage));
				totDisAmt = totDisAmt.add(((HYIDouble) pluPriceMap.get(itemno))
						.subtract(salingPrice).multiply(
								new HYIDouble(cc)));
				totAmt = totAmt.add(((HYIDouble) pluPriceMap.get(itemno))
						.multiply(new HYIDouble(cc)));
				if (matchedPluMap.containsKey(itemno)) {
					matchedPluMap.put(itemno, new Integer(
							((Integer) matchedPluMap.get(itemno)).intValue()
									+ cc));
				} else {
					matchedPluMap.put(itemno, new Integer(cc));
				}
				if (leaveMatchQty == 0 && qty > cc) {
					int dd = (matchQty - needMatchQty) > (qty - cc) ? (qty - cc)
							: (matchQty - needMatchQty);
					needMatchQty += dd;
					totDisAmt = totDisAmt.add(((HYIDouble) pluPriceMap
							.get(itemno)).subtract(salingPrice).multiply(
							new HYIDouble(dd)));
					totAmt = totAmt.add(((HYIDouble) pluPriceMap.get(itemno))
							.multiply(new HYIDouble(dd)));
					if (matchedPluMap.containsKey(itemno)) {
						matchedPluMap.put(itemno, new Integer(
								((Integer) matchedPluMap.get(itemno))
										.intValue()
										+ dd));
					} else {
						matchedPluMap.put(itemno, new Integer(dd));
					}
					if (needMatchQty == matchQty)
						break;
				}
			} else {
				int dd = (matchQty - needMatchQty) > qty ? qty
						: (matchQty - needMatchQty);
				needMatchQty += dd;
				totDisAmt = totDisAmt.add(((HYIDouble) pluPriceMap.get(itemno))
						.subtract(salingPrice).multiply(
								new HYIDouble(dd)));
				totAmt = totAmt.add(((HYIDouble) pluPriceMap.get(itemno))
						.multiply(new HYIDouble(dd)));
				if (matchedPluMap.containsKey(itemno)) {
					matchedPluMap.put(itemno, new Integer(
							((Integer) matchedPluMap.get(itemno)).intValue()
									+ dd));
				} else {
					matchedPluMap.put(itemno, new Integer(dd));
				}
				if (needMatchQty == matchQty)
					break;
			}
		}
		if (totDisAmt.compareTo(totMatchDisAmt) > 0)
			return null;

		// 2. 分摊折让金额
		boolean isLastApportionPlu = false;
		HYIDouble leaveDiscAmt = totMatchDisAmt;
		for (int k = 0; k < tranDetailCount; k++) {
			LineItem curLineItem = (LineItem) lineItemList[k];
			if (!curLineItem.getRemoved()
					&& ("R".equalsIgnoreCase(curLineItem.getDetailCode()) || "S"
							.equalsIgnoreCase(curLineItem.getDetailCode()))) {
				if (matchedPluMap.containsKey(curLineItem.getItemNumber())) {
					curLineItem.setUnitPrice(curLineItem.getOriginalPrice());
					int cc = curLineItem.getQuantity().intValue();
					int matchedQty = ((Integer) matchedPluMap.get(curLineItem
							.getItemNumber())).intValue();
					int mqty = matchedQty > cc ? cc : matchedQty;
					if (pluQtyMap.containsKey(curLineItem.getItemNumber())) {
						pluQtyMap.put(curLineItem.getItemNumber(), new Integer(
								((Integer) pluQtyMap.get(curLineItem
										.getItemNumber())).intValue()
										- mqty));
					}
					totMatchQty -= mqty;
					if (totMatchQty == 0)
						isLastApportionPlu = true;
					matchedPluMap.put(curLineItem.getItemNumber(), new Integer(
							((Integer) matchedPluMap.get(curLineItem
									.getItemNumber())).intValue()
									- mqty));
					int apportionQty = curLineItem.apportion(
							isLastApportionPlu, mqty, totAmt, totMatchDisAmt,
							leaveDiscAmt);

					leaveDiscAmt = leaveDiscAmt.subtract(curLineItem
							.getUnitPrice().multiply(
									new HYIDouble(apportionQty)).divide(totAmt,
									4, BigDecimal.ROUND_HALF_UP).multiply(
									totMatchDisAmt).setScale(2,
									BigDecimal.ROUND_HALF_UP));
				}
			}
		}
		Object[] ret = new Object[2];
		ret[0] = totMatchDisAmt;
		ret[1] = new Integer(matchQty);
		return ret;
	}

	private void createPromLineItem(Transaction trans, String promID, int promMatchCount,
			HYIDouble totalPromAmt) {

		LineItem promLineItem = new LineItem();
		promLineItem.setPluNumber(promID);
		promLineItem.setQuantity(new HYIDouble(promMatchCount));

		HYIDouble unitAmount = totalPromAmt.divide(new HYIDouble(
				promMatchCount), 2, BigDecimal.ROUND_HALF_UP);
		promLineItem.setUnitPrice(unitAmount);
		/*
		 * Meyer/2003-02-21/
		 */
		promLineItem.setItemNumber(promID);
		promLineItem.setOriginalPrice(unitAmount);

		promLineItem.setAmount(totalPromAmt);
		trans.setMMDetail(promID, promLineItem);
		trans.setMixAndMatchCount0(trans.getMixAndMatchCount0().intValue()
				+ promMatchCount);

		System.out.println("###	INSERT ONE MM LINEITEM	###");
		System.out.println("	promID="
				+ promID
				+ "/count="
				+ promMatchCount
				+ "/unitAmount="
				+ totalPromAmt.divide(new HYIDouble(promMatchCount), 2,
						BigDecimal.ROUND_HALF_UP) + "/amount="
				+ totalPromAmt);
	}

	/**
	 * 更新tranHead, TAXAMT0,TAXAMT1,TAXAMT2,TAXAMT3,TAXAMT4,
	 * MNMAMT0,MNMAMT1,MNMAMT2,MNMAMT3,MNMAMT4,
	 * MNMCNT0,MNMAMT1,MNMAMT2,MNMAMT3,MNMAMT4,
	 * 
	 */
	private void updatePromTranHead(Transaction trans) {
		// 设初始值
		// 目前数据表中有5个税别栏位
		trans.setTaxAmount0(new HYIDouble(0));
		trans.setTaxAmount1(new HYIDouble(0));
		trans.setTaxAmount2(new HYIDouble(0));
		trans.setTaxAmount3(new HYIDouble(0));
		trans.setTaxAmount4(new HYIDouble(0));
		trans.setTaxAmount5(new HYIDouble(0));
		trans.setTaxAmount6(new HYIDouble(0));
		trans.setTaxAmount7(new HYIDouble(0));
		trans.setTaxAmount8(new HYIDouble(0));
		trans.setTaxAmount9(new HYIDouble(0));
		trans.setTaxAmount10(new HYIDouble(0));
		trans.setMixAndMatchAmount0(new HYIDouble(0));
		trans.setMixAndMatchAmount1(new HYIDouble(0));
		trans.setMixAndMatchAmount2(new HYIDouble(0));
		trans.setMixAndMatchAmount3(new HYIDouble(0));
		trans.setMixAndMatchAmount4(new HYIDouble(0));
		trans.setMixAndMatchAmount5(new HYIDouble(0));
		trans.setMixAndMatchAmount6(new HYIDouble(0));
		trans.setMixAndMatchAmount7(new HYIDouble(0));
		trans.setMixAndMatchAmount8(new HYIDouble(0));
		trans.setMixAndMatchAmount9(new HYIDouble(0));
		trans.setMixAndMatchAmount10(new HYIDouble(0));

		int tranDetailCount = lineItemList.length;
		LineItem lineItem;// 当前交易明细
		for (int i = 0; i < tranDetailCount; i++) {
			lineItem = (LineItem) lineItemList[i];
			String detailCode = lineItem.getDetailCode();
			if (!lineItem.getRemoved()
					&& ("R".equalsIgnoreCase(detailCode) || "S"
							.equalsIgnoreCase(detailCode))) {
				HYIDouble aftDscAmt = lineItem.getAfterDiscountAmount();
				HYIDouble mamAmt = lineItem.getAmount().subtract(aftDscAmt);
				String taxId = lineItem.getTaxType();

				HYIDouble a = CreamToolkit.getAmountMethod(trans,
						"getMixAndMatchAmount" + taxId);
				HYIDouble b = a.add(mamAmt);
				CreamToolkit.setAmountMethod(trans, "setMixAndMatchAmount"
						+ taxId, b);

				// CreamToolkit.setAmountMethod(trans, "setMixAndMatchAmount" +
				// taxType,
				// CreamToolkit.getAmountMethod(trans, "getMixAndMatchAmount" +
				// taxType).add(mamAmt)
				// );

				// CreamToolkit.setCountMethod(trans, "setMixAndMatchCount" +
				// taxType,
				// CreamToolkit.getCountMethod(trans, "getMixAndMatchCount" +
				// taxType) + 1
				// );
				a = CreamToolkit.getAmountMethod(trans, "getTaxAmount"
						+ taxId);
				b = a.add(lineItem.caculateAndSetTaxAmount());
				CreamToolkit
						.setAmountMethod(trans, "setTaxAmount" + taxId, b);
				// CreamToolkit.setAmountMethod(trans,"setTaxAmount" + taxType,
				// CreamToolkit.getAmountMethod(trans, "getTaxAmount" +
				// taxType).add(taxAmt)
				// );
			}
		}
	}
	/**
	 * 对指定促销做match
	 * 
	 */
	private PromMatch match(String aPromID, boolean isGroupPromotion) {
		PromMatch promMatch = null;
		int matchedCount = 0;
		// 取得赠品，折让信息
		CombPromotionHeader cph = CombPromotionHeader.queryByID(aPromID);
		HYIDouble matchDiscAmt2 = cph.getDiscountAmount();// 折让金额
		HYIDouble discAmt2 = new HYIDouble(0);
		giftQtyMap = new HashMap();// 赠品Map
		HYIDouble giftDiscAmt = new HYIDouble(0);
		
		Iterator cgdIter = CombGiftDetail.queryByCphID(aPromID);
		while (cgdIter != null && cgdIter.hasNext()) {
			CombGiftDetail cgDetail = (CombGiftDetail) cgdIter.next();
			String curPluCode = cgDetail.getPluCode();
			PLU plu = PLU.queryByItemNumber(curPluCode);
			if (plu != null) {
				Integer quantity = cgDetail.getQuantity();
				giftQtyMap.put(curPluCode, quantity);
				giftDiscAmt = giftDiscAmt.add(PLU.queryByItemNumber(curPluCode).getUnitPrice().multiply(new HYIDouble(quantity.intValue())));
			}
		}

		// 验证是否满足赠品条件
		int noMatchedGiftCount = giftQtyMap.size();
		Iterator giftQtyIter = giftQtyMap.keySet().iterator();
		while (giftQtyIter.hasNext()) {
			String curPluCode = (String) giftQtyIter.next();
			int curGiftQty = ((Integer) giftQtyMap.get(curPluCode)).intValue();
			Object pluQtyValue = pluQtyMap.get(curPluCode);
			int curPluQty = (pluQtyValue != null) ? ((Integer) pluQtyValue)
					.intValue() : 0;
			if (curPluQty >= curGiftQty) {
				noMatchedGiftCount--;
			}
		}

		Iterator csdIter = CombSaleDetail.queryByCphID(aPromID);
		if (noMatchedGiftCount == 0) {// 满足赠品条件
			int tranDetailCount = lineItemList.length;
			// 一. 群组促销
			if (isGroupPromotion) {
				System.out.println(CreamToolkit.GetResource().getString(
						"MatchingGroupMM")
						+ aPromID);
				// 将各个商品子群组放如群组促销配对
				HashMap pluGrpSubMatchMap = new HashMap();
				
				List sortPrice = new ArrayList();
				List sortCsdList = new ArrayList();
				{
					int index = 0;
					while (csdIter.hasNext()) {
						CombSaleDetail csDetail = (CombSaleDetail) csdIter.next();
						sortCsdList.add(csDetail);
						String curPluCode = csDetail.getPluCode();
						Map price = new HashMap();
						PLU plu = PLU.queryByItemNumber(curPluCode);
						if (plu != null) {
							price.put(curPluCode, plu.getUnitPrice());
							price.put("index", new Integer(index));
							sortPrice.add(price);
						}
						index++;
					}
					Collections.sort(sortPrice, new HYIComparator());
				}
				
				Iterator it = sortPrice.iterator();
				while (it.hasNext()) {
					int index = ((Integer) ((Map) it.next()).get("index")).intValue();
					CombSaleDetail csDetail = (CombSaleDetail) sortCsdList.get(index);
					String groupID = csDetail.getGroupID();
					int groupQty = csDetail.getGroupQty().intValue();
					if (!pluGrpSubMatchMap.containsKey(groupID)) {
						pluGrpSubMatchMap.put(groupID, new PluGrpSubMatch(
								aPromID, groupID, groupQty, pluPriceMap));
					}
					PluGrpSubMatch pluGrpSubMatch = (PluGrpSubMatch) pluGrpSubMatchMap
							.get(groupID);
					// 扣去赠品数
					String curPluCode = csDetail.getPluCode();
					Object pluQtyValue = pluQtyMap.get(curPluCode);
					int curPluQty = (pluQtyValue != null) ? ((Integer) pluQtyValue)
							.intValue()
							: 0;
					pluQtyValue = giftQtyMap.get(curPluCode);
					int curGiftQty = (pluQtyValue != null) ? ((Integer) pluQtyValue)
							.intValue()
							: 0;
					curPluQty = curPluQty - curGiftQty;

					pluGrpSubMatch.groupMatch(curPluCode, curPluQty);
				}

				promMatch = new PluGrpPromMatch(aPromID, pluGrpSubMatchMap
						.values());
				if (promMatch.isMatched()) {
					matchedCount = 1;

					HYIDouble totalAmt = promMatch.getTotalAmt();
					HYIDouble leaveDiscAmt = matchDiscAmt2;
					{//比较特价同促销哪个更优惠
						Collection pluGrpSubMatchList = ((PluGrpPromMatch) promMatch)
								.getPluGrpSubMatchList();
						// int subMatchListSize = pluGrpSubMatchList.size();
						Iterator pluGrpSubMatchIter = pluGrpSubMatchList
								.iterator();
						for (int i = 1; pluGrpSubMatchIter.hasNext(); i++) {
							// boolean isLastSubMatch = i == subMatchListSize;

							PluGrpSubMatch pluGrpSubMatch = (PluGrpSubMatch) pluGrpSubMatchIter
									.next();
							HashMap matchedPluQtyMap = pluGrpSubMatch
									.getMatchedPluQtyMap();
							Collection matchedPluCodeSet = matchedPluQtyMap
									.keySet();
							// int pluCodeSetSize = matchedPluCodeSet.size();
							Iterator matchedPluCodeIter = matchedPluCodeSet
									.iterator();

							for (int j = 1; matchedPluCodeIter.hasNext(); j++) {
								// boolean isLastMatchedPlu = j == pluCodeSetSize;
								String curPluCode = (String) matchedPluCodeIter.next();
								// 1. 扣减已matched商品的数量
								int matchQty = ((Integer) matchedPluQtyMap.get(curPluCode)).intValue();
								discAmt2 = discAmt2.add(((HYIDouble) pluPriceMap.get(curPluCode))
										.subtract((HYIDouble) PLU.queryByItemNumber(curPluCode)
												.getSalingPriceDetail()[0]).multiply(new HYIDouble(matchQty)));
							}

						}
					}
//System.out.println("discAmt : " + discAmt2 + " | matchAmt : " + matchDiscAmt2 + " | giftDiscAmt : " + giftDiscAmt);					
					if (discAmt2.compareTo(matchDiscAmt2.add(giftDiscAmt)) > 0) {
						return null;
					}
					Collection pluGrpSubMatchList = ((PluGrpPromMatch) promMatch)
							.getPluGrpSubMatchList();
					int subMatchListSize = pluGrpSubMatchList.size();
					Iterator pluGrpSubMatchIter = pluGrpSubMatchList.iterator();
					for (int i = 1; pluGrpSubMatchIter.hasNext(); i++) {
						boolean isLastSubMatch = i == subMatchListSize;

						PluGrpSubMatch pluGrpSubMatch = (PluGrpSubMatch) pluGrpSubMatchIter
								.next();
						HashMap matchedPluQtyMap = pluGrpSubMatch
								.getMatchedPluQtyMap();
						Collection matchedPluCodeSet = matchedPluQtyMap
								.keySet();
						int pluCodeSetSize = matchedPluCodeSet.size();
						Iterator matchedPluCodeIter = matchedPluCodeSet
								.iterator();

						for (int j = 1; matchedPluCodeIter.hasNext(); j++) {
							boolean isLastMatchedPlu = j == pluCodeSetSize;
							String curPluCode = (String) matchedPluCodeIter
									.next();
							// 1. 扣减已matched商品的数量
							pluQtyMap.put(curPluCode, Integer.valueOf(String
									.valueOf(((Integer) pluQtyMap
											.get(curPluCode)).intValue()
											- ((Integer) matchedPluQtyMap
													.get(curPluCode))
													.intValue())));

							// 2. 分摊折让金额
							boolean isLastApportionPlu = isLastSubMatch
									&& isLastMatchedPlu;
							int leaveApportionQty = Integer.MIN_VALUE;// Reset
							// to
							// init
							// value
							for (int k = 0; k < tranDetailCount; k++) {
								LineItem curLineItem = (LineItem) lineItemList[k];
								if (!curLineItem.getRemoved()
										&& ("R".equalsIgnoreCase(curLineItem
												.getDetailCode()) || "S"
												.equalsIgnoreCase(curLineItem
														.getDetailCode()))) {
									if (curPluCode.equals(curLineItem
											.getItemNumber())) {
										if (leaveApportionQty == Integer.MIN_VALUE) {
											leaveApportionQty = ((Integer) matchedPluQtyMap
													.get(curPluCode))
													.intValue();
										}

										int apportionQty = curLineItem
												.apportion(isLastApportionPlu,
														leaveApportionQty,
														totalAmt, matchDiscAmt2,
														leaveDiscAmt);

										leaveApportionQty = leaveApportionQty
												- apportionQty;
										leaveDiscAmt = leaveDiscAmt
												.subtract(curLineItem
														.getUnitPrice()
														.multiply(
																new HYIDouble(
																		apportionQty))
														.divide(
																totalAmt,
																4,
																BigDecimal.ROUND_HALF_UP)
														.multiply(matchDiscAmt2)
														.setScale(
																2,
																BigDecimal.ROUND_HALF_UP));
										if (leaveApportionQty <= 0) {
											break;
										}
									}
								}
							}
						}
					}
					printPluQtyMap();

					// 3. 赠送
					present(giftQtyMap, matchedCount);

					printPluQtyMap();
					return promMatch;
				}
				// 群组促销结束
			} else {
				System.out.println(CreamToolkit.GetResource().getString(
						"MatchingGroupMM")
						+ aPromID);
				// 二. 商品促销
				promMatch = new PluPromMatch(aPromID);
				if (((PluPromMatch) promMatch).mapMatch(pluQtyMap, giftQtyMap)) {
					HYIDouble totalAmt = promMatch.getTotalAmt();
					matchedCount = ((PluPromMatch) promMatch).getMatchedCount();
					matchDiscAmt2 = matchDiscAmt2.multiply(new HYIDouble(matchedCount))
							.setScale(2, BigDecimal.ROUND_HALF_UP);
					HYIDouble leaveDiscAmt = matchDiscAmt2;

					{//比较特价同促销哪个更优惠
						HashMap matchedPluQtyMap = ((PluPromMatch) promMatch)
								.getMatchedPluQtyMap();
						Collection matchedPluCodeSet = matchedPluQtyMap.keySet();

						// int pluCodeSetSize = matchedPluCodeSet.size();
						Iterator matchedPluCodeIter = matchedPluCodeSet.iterator();
						for (int j = 1; matchedPluCodeIter.hasNext(); j++) {
							// boolean isLastMatchedPlu = j == pluCodeSetSize;
							String curPluCode = (String) matchedPluCodeIter.next();
							// 1. 扣减已matched商品的数量
							int matchQty = ((Integer) matchedPluQtyMap.get(curPluCode)).intValue();
							discAmt2 = discAmt2.add(((HYIDouble) pluPriceMap.get(curPluCode))
									.subtract((HYIDouble) pluPriceMap.get(curPluCode)).multiply(
											new HYIDouble(matchQty)));
						}
					}
//System.out.println("----discAmt : " + discAmt2 + " | matchAmt : " + matchDiscAmt2);					
					if (discAmt2.compareTo(matchDiscAmt2) > 0) {
						return null;
					}
					
					HashMap matchedPluQtyMap = ((PluPromMatch) promMatch)
							.getMatchedPluQtyMap();
					Collection matchedPluCodeSet = matchedPluQtyMap.keySet();
					int pluCodeSetSize = matchedPluCodeSet.size();
					Iterator matchedPluCodeIter = matchedPluQtyMap.keySet()
							.iterator();
					for (int i = 1; matchedPluCodeIter.hasNext(); i++) {
						boolean isLastApportionPlu = i == pluCodeSetSize;
						String curPluCode = (String) matchedPluCodeIter.next();
						int matchedQty = ((Integer) matchedPluQtyMap
								.get(curPluCode)).intValue()
								* matchedCount;
						// 1. 扣减matched商品数量
						pluQtyMap.put(curPluCode, Integer.valueOf(String
								.valueOf(((Integer) pluQtyMap.get(curPluCode))
										.intValue()
										- matchedQty)));

						int leaveApportionQty = Integer.MIN_VALUE;// 单个商品的leaveApportionQty，Reset
						// to init
						// value
						for (int j = 0; j < tranDetailCount; j++) {
							LineItem curLineItem = (LineItem) lineItemList[j];
							if (!curLineItem.getRemoved()
									&& ("R".equalsIgnoreCase(curLineItem
											.getDetailCode()) || "S"
											.equalsIgnoreCase(curLineItem
													.getDetailCode()))) {
								// 2. 分摊Matched
								if (curPluCode.equals(curLineItem
										.getItemNumber())) {
									if (leaveApportionQty == Integer.MIN_VALUE) {
										leaveApportionQty = matchedQty;
									}
									int apportionQty = curLineItem.apportion(
											isLastApportionPlu,
											leaveApportionQty, totalAmt,
											matchDiscAmt2, leaveDiscAmt);
									leaveApportionQty = leaveApportionQty
											- apportionQty;
									leaveDiscAmt = leaveDiscAmt
											.subtract(curLineItem
													.getUnitPrice()
													.multiply(
															new HYIDouble(
																	apportionQty))
													.divide(
															totalAmt,
															4,
															BigDecimal.ROUND_HALF_UP)
													.multiply(matchDiscAmt2)
													.setScale(
															2,
															BigDecimal.ROUND_HALF_UP));

									if (leaveApportionQty == 0) {
										break;
									}
								}
							}
						}
					}
					printPluQtyMap();

					// 3. 赠送
					present(giftQtyMap, matchedCount);
					printPluQtyMap();
					return promMatch;
				} // 商品促销结束
			}
		}
		return null;
	}

	/**
	 * 赠送 1. 扣减赠品数量 2. 分摊赠品金额
	 * 
	 */
	private void present(HashMap giftQtyMap, int matchedCount) {
		int tranDetailCount = lineItemList.length;

		Iterator giftPluCodeIter = giftQtyMap.keySet().iterator();
		while (giftPluCodeIter.hasNext()) {
			int leaveGiftQty = Integer.MIN_VALUE;
			// boolean isPresentEnd = false;
			String giftPluCode = (String) giftPluCodeIter.next();

			// 扣减赠品数量
			pluQtyMap.put(giftPluCode, Integer.valueOf(String
					.valueOf(((Integer) pluQtyMap.get(giftPluCode)).intValue()
							- (((Integer) giftQtyMap.get(giftPluCode))
									.intValue() * matchedCount))));

			for (int i = 0; i < tranDetailCount; i++) {
				LineItem curLineItem = (LineItem) lineItemList[i];
				String curPluCode = curLineItem.getItemNumber();

				// Meyer / 2003-02-25 / Modified: Add Check Condition of
				// curLineItme
				String detailCode = curLineItem.getDetailCode();
				if (!curLineItem.getRemoved()
						&& ("R".equalsIgnoreCase(detailCode) || "S"
								.equalsIgnoreCase(detailCode))) {
					// 分摊赠品金额
					if (giftPluCode.equals(curPluCode)) {
						if (leaveGiftQty == Integer.MIN_VALUE) {
							leaveGiftQty = ((Integer) giftQtyMap
									.get(giftPluCode)).intValue()
									* matchedCount;
						}
						int matchedQty = curLineItem.present(leaveGiftQty);
						leaveGiftQty = leaveGiftQty - matchedQty;

						if (leaveGiftQty <= 0) {
							break;
						}
					}
				}
			}
		}
	}

	private void printPromIDSet() {
		System.out.println("Avaliable promID");
		Iterator iter = promIDSet.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}
	}

	private void printPluQtyMap() {
		System.out.println("Current pluQtyMap");
		Iterator iter = pluQtyMap.keySet().iterator();
		while (iter.hasNext()) {
			String pluCode = (String) iter.next();
			System.out.println(pluCode + "/" + pluQtyMap.get(pluCode));
		}
	}
}

class HYIComparator implements Comparator {
	public int compare(Object o1, Object o2) {
		Map map1 = (Map) o1;
		Map map2 = (Map) o2;
		HYIDouble price1 = null;
		HYIDouble price2 = null;
		for (Iterator it = map1.values().iterator(); it.hasNext();) {
			Object o = it.next();
			if (o instanceof HYIDouble) {
				price1 = (HYIDouble) o;
				break;
			}
		}
		for (Iterator it = map2.values().iterator(); it.hasNext();) {
			Object o = it.next();
			if (o instanceof HYIDouble) {
				price2 = (HYIDouble) o;
				break;
			}
		}
		return price1.compareTo(price2);
	}
}