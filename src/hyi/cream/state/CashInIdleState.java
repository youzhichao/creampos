package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.CancelButton;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class CashInIdleState extends State {
    static CashInIdleState cashInIdleState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private Class exitState;
    
    public static CashInIdleState getInstance() {
        try {
            if (cashInIdleState == null) {
                cashInIdleState = new CashInIdleState();
            }
        } catch (InstantiationException ex) {
        }
        return cashInIdleState;
    }

    /**
     * Constructor
     */
    public CashInIdleState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("CashInIdleState entry");
        if (event == null) {
            return;
        }           
        
      if (sourceState instanceof CashierRightsCheckState) {
    	  exitState = ((CashierRightsCheckState) sourceState).getSourceState();
      }
        // modify lxf //2003.03.03
//        if (sourceState instanceof KeyLock1State
//            && event.getSource() instanceof SelectButton) {

//        Collection fds = Cashier.getExistedFieldList("cashier");
//        
//        if ((!fds.contains("CASRIGHTS") || sourceState instanceof CashierRightsCheckState)
//            && event.getSource() instanceof EnterButton) {
        Transaction trans = app.getCurrentTransaction();
        trans.setDealType2("H");

        app.getMessageIndicator().setMessage(res.getString("CashInPriceMessage"));
//        app.getWarningIndicator().setMessage(""); 
        app.getItemList().setVisible(false);
        app.getPayingPane().setTransaction(trans);
		app.getPayingPane1().setTransaction(trans);
		app.getPayingPane2().setTransaction(trans);
        //app.getPayingPane().setMode(1);
        app.getPayingPane().setVisible(true);
//        }

        app.getPayingPane().repaint();

        app.getMessageIndicator().setMessage(res.getString("CashInPriceMessage"));
    }

    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");

        // init newCashierID
        POSTerminalApplication.getInstance().setNewCashierID("");
        if (event.getSource() instanceof ClearButton) {
            app.getMessageIndicator().setMessage("");
            app.getWarningIndicator().setMessage("");
            //app.getPayingPane().setMode(0);
            app.getPayingPane().clear();
            return exitState;
        } else if (event.getSource() instanceof EnterButton) {
            Transaction trans = app.getCurrentTransaction();
            if (trans.getLineItems().length == 0) {
                return CashInIdleState.class;
            }

            //  set to transaction
            trans.setGrossSalesAmount(new HYIDouble(0));
            trans.setNetSalesAmount(new HYIDouble(0));
            trans.setNetSalesAmount0(new HYIDouble(0));
            trans.setNetSalesAmount1(new HYIDouble(0));
            trans.setNetSalesAmount2(new HYIDouble(0));
            trans.setNetSalesAmount3(new HYIDouble(0));
            trans.setNetSalesAmount4(new HYIDouble(0));
            trans.setNetSalesAmount5(new HYIDouble(0));
            trans.setNetSalesAmount6(new HYIDouble(0));
            trans.setNetSalesAmount7(new HYIDouble(0));
            trans.setNetSalesAmount8(new HYIDouble(0));
            trans.setNetSalesAmount9(new HYIDouble(0));
            trans.setNetSalesAmount10(new HYIDouble(0));
            trans.setMixAndMatchAmount0(new HYIDouble(0));
            trans.setMixAndMatchAmount1(new HYIDouble(0));
            trans.setMixAndMatchAmount2(new HYIDouble(0));
            trans.setMixAndMatchAmount3(new HYIDouble(0));
            trans.setMixAndMatchAmount4(new HYIDouble(0));
            trans.setMixAndMatchAmount5(new HYIDouble(0));
            trans.setMixAndMatchAmount6(new HYIDouble(0));
            trans.setMixAndMatchAmount7(new HYIDouble(0));
            trans.setMixAndMatchAmount8(new HYIDouble(0));
            trans.setMixAndMatchAmount9(new HYIDouble(0));
            trans.setMixAndMatchAmount10(new HYIDouble(0));
            trans.setMixAndMatchCount0(0);
            trans.setMixAndMatchCount1(0);
            trans.setMixAndMatchCount2(0);
            trans.setMixAndMatchCount3(0);
            trans.setMixAndMatchCount4(0);   
            trans.setMixAndMatchCount5(0);
            trans.setMixAndMatchCount6(0);
            trans.setMixAndMatchCount7(0);
            trans.setMixAndMatchCount8(0);
            trans.setMixAndMatchCount9(0);
            trans.setMixAndMatchCount10(0);
            trans.clearMMDetail();

            app.getMessageIndicator().setMessage(res.getString("TransactionEnd"));
            app.getWarningIndicator().setMessage(""); 
            //app.getPayingPane().setMode(0);
            app.getPayingPane().clear();

            return DrawerOpenState.class;
        } else if (event.getSource() instanceof CancelButton) {
            return CancelState.class;
        } else if (event.getSource() instanceof NumberButton) {
            return CashInState.class;
        } else {
            return CashInIdleState.class;
        }
    }
}

