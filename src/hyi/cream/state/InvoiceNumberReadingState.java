package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.InvoiceButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.uibeans.touch.TouchPane;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import hyi.cream.util.ReceiptGenerator;

import java.sql.SQLException;
import java.util.EventObject;
import java.util.ResourceBundle;
import java.text.DecimalFormat;

import org.apache.commons.lang.StringUtils;

public class InvoiceNumberReadingState extends State {
    private static InvoiceNumberReadingState instance = new InvoiceNumberReadingState();
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    private String numbers = "";
    private String alphabets = "";
    private int step = 1;
    private Class exitState = null;
    private ResourceBundle res = CreamToolkit.GetResource();

    private InvoiceNumberReadingState() {}

    public static InvoiceNumberReadingState getInstance() {
        return instance;
    }
    
    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof InitialState
                || sourceState instanceof IdleState
                || sourceState instanceof DrawerOpenState
                || sourceState instanceof ReprintState
                || sourceState instanceof TransactionHoldShowState
                || event.getSource() instanceof InvoiceButton
                || event.getSource() instanceof ClearButton
                || sourceState instanceof SummaryState) {
            TouchPane touchPanel = TouchPane.getInstance();
            touchPanel.show("B");
            touchPanel.setVisible(true);
            touchPanel.setType2("invoice");
            // touchPanel.setBackground(this.getBackground());
            alphabets = "";
            numbers = "";
            step = 1;
            app.getMessageIndicator().setMessage(res.getString("InvoiceInput1"));
        }
        if (step == 1 && sourceState instanceof AlphabetReadingState
                && event.getSource() instanceof EnterButton) {
            String n = ((AlphabetReadingState) sourceState).getNumber();
            int n1 = Integer.parseInt(n);
            if (n1 > 26)
                app.getWarningIndicator().setMessage(res.getString("InvoiceInputWarning1"));
            else {
                n1 = 64 + Integer.parseInt(n);
                alphabets += String.valueOf((char) n1);
            }

            app.getMessageIndicator().setMessage(res.getString("InvoiceInput1") + alphabets);
        }
        if ((step == 1 && alphabets.length() < 2)) {
            TouchPane touchPanel = TouchPane.getInstance();
            touchPanel.show("B");
            touchPanel.setVisible(true);
            touchPanel.setType2("invoice");
            app.getMessageIndicator().setMessage(res.getString("InvoiceInput1") + alphabets);
        } else if ((step == 1 && alphabets.length() == 2)) {
            step++;
            TouchPane.getInstance().setVisible(false);
            app.getMessageIndicator().setMessage(res.getString("InvoiceInput2") + alphabets);
        } else if (step == 2) {
            app.getMessageIndicator().setMessage(res.getString("InvoiceInput2") + alphabets + numbers);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");
        if (event.getSource() instanceof EnterButton) {
            if (step == 1 && alphabets.length() == 2) {
                return InvoiceNumberReadingState.class;
            } else
            if (step == 1 && alphabets.length() != 2) {
                app.getWarningIndicator().setMessage(res.getString("InvoiceInputWarning2"));
                return InvoiceNumberReadingState.class;
            } else {
                if (!(alphabets.length() == 2 && numbers.length() == 8)) {
                    alphabets = "";
                    numbers = "";
                    step = 1;
                    app.getWarningIndicator().setMessage(res.getString("InvoiceInputWarning2"));
                    return InvoiceNumberReadingState.class;
                }
                if (!app.getTrainingMode())
                    updateInvoiceNumber();
            }
        } else if (event.getSource() instanceof ClearButton) {
            String invoiceID = PARAM.getInvoiceID();
            String invoiceNumber = PARAM.getInvoiceNumber();
            //Bruce/20100407/ 當發票號碼被清空時，不能回IdleState，因為一回IdleState後，
            // modifyInvoiceNoInTransaction會被設成false，這樣會導致tranhead的發票字軌和號碼沒被設值
            if (!StringUtils.isEmpty(alphabets) || StringUtils.isEmpty(invoiceID)
                || StringUtils.isEmpty(invoiceNumber)) {
                alphabets = "";
                numbers = "";
                step = 1;
                return InvoiceNumberReadingState.class;
            }
            TouchPane.getInstance().setVisible(false);
        } else if (event.getSource() instanceof NumberButton) {
            if (step == 1)
                return AlphabetReadingState.class;
            else {
                NumberButton pb = (NumberButton)event.getSource();
                numbers = numbers + pb.getNumberLabel();
                app.getMessageIndicator().setMessage(alphabets + numbers);
                return InvoiceNumberReadingState.class;
            }
        }
        app.getWarningIndicator().setMessage("");
        step = 1;
        if (exitState != null) {
            Class state = exitState;
            exitState = null;
            return state;
        }
        return IdleState.class;
    }

    private void updateInvoiceNumber() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();
            PARAM.updateInvoiceID(alphabets);
            PARAM.updateInvoiceNumber(numbers);
            connection.commit();

            /*
              Bruce/2010-04-21
              定義：
               * 設定[發票號碼]是指設定目前印字頭所接觸到的那張發票的號碼（無論交易是否開始）。
               * 交易頭檔中所記錄的發票號碼，是指此交易的首張發票號碼。

              因為目前交易頭檔中只記錄了一組發票號碼，如果是在交易跨紙卷輸入了新的發票編號，則不去
              修改原有的發票號碼（因為極有可能換了號碼不連續的發票）。也就是，如果以收銀員輸入的發票
              號碼推算出的首張發票不是在同一卷發票，則系統不修改頭檔中原有的發票號碼。其他情況都假設
              發票號碼為連續，然後按照目前已經列印的張數來推算第一張發票的號碼。

              邏輯：
               X = 收銀員設定的發票號碼
               C = 當前交易已列印張數，初始值為1，印超過1張則為2，以此類推

               if (X - C + 1 >= X / 250 × 250)  // 如果以收銀員輸入的發票號碼推算出的首張發票是在同一卷發票
                  頭檔發票號碼才設成: X - C + 1
               else
                  否則不做任何修改
             */
            int receiptPageCount = Math.max(ReceiptGenerator.getInstance().getPageNumber(), 1);
            try {
                int invNo = Integer.parseInt(numbers);
                int firstInvNo = invNo - receiptPageCount + 1;
                if (firstInvNo >= 0 && firstInvNo >= invNo / 250 * 250) {
                    Transaction tran = app.getCurrentTransaction();
                    tran.setInvoiceID(alphabets);
                    tran.setInvoiceNumber(new DecimalFormat("00000000").format(firstInvNo));
                }
            } catch (NumberFormatException e) {
                CreamToolkit.logMessage(e);
            }

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * @return the exitState
     */
    public Class getExitState() {
        return exitState;
    }

    /**
     * @param exitState the exitState to set
     */
    public void setExitState(Class exitState) {
        this.exitState = exitState;
    }
}
