package hyi.cream.state;

import java.util.*;
import java.math.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

/**
 * 盘点数量输入State.
 */
public class InventoryQuantityState extends State {
    private static InventoryQuantityState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Indicator messageIndicator = app.getMessageIndicator();
    private Indicator warningIndicator = app.getWarningIndicator();
    private ResourceBundle res = CreamToolkit.GetResource();
    private static StringBuffer quantity = new StringBuffer();
    private static int maxQtyAbs;
    private static boolean flag; //是否处于数量判断流程
    private static String choose; //是否可以超过规定的数量
    
    public static InventoryQuantityState getInstance() {
        try {
            if (instance == null) {
                instance = new InventoryQuantityState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public InventoryQuantityState() throws InstantiationException {
    	try {
    		maxQtyAbs = Math.abs(PARAM.getMaxInventoryQty());
    		flag = false;
            choose = "";
    	} catch (NumberFormatException e) {}
    }

    public void entry(EventObject event, State sourceState) {
        if (quantity.length() == 0)
            messageIndicator.setMessage(res.getString("PleaseInputInventoryItemQuantity"));
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            if (!flag) {
	            quantity.append(pb.getNumberLabel());
	            messageIndicator.setMessage(quantity.toString());
	            warningIndicator.setMessage("");
            } else {
            	choose += pb.getNumberLabel();
	            messageIndicator.setMessage(choose);
            }
            return sinkState.getClass();
        }
        if (event.getSource() instanceof ClearButton) {
    		flag = false;
    		choose = "";
            quantity.setLength(0);
            warningIndicator.setMessage("");
            return sinkState.getClass();
        }
        if (event.getSource() instanceof EnterButton
            || event.getSource() instanceof QuantityButton) {
            if (quantity.length() != 0) {
                try {
                	int invQty = Integer.parseInt(quantity.toString());
                	if (flag && choose.equals("1")) {
                		flag = false;
                		choose = "";
                        warningIndicator.setMessage("");
					} else if (Math.abs(invQty) > maxQtyAbs) {
                    	flag = true;
                        warningIndicator.setMessage(res.getString("InputQuantityIsTooLarge") + "? " + invQty);
                        app.getMessageIndicator().setMessage(res.getString("InventoryQuantityConfirm"));
                        return InventoryQuantityState.class;
                	}
                    Inventory inv = InventoryIdleState.getCurrentInventory();
                    HYIDouble qty = new HYIDouble(invQty);
                    if (qty.scale() > 4)
                        qty = qty.setScale(4, BigDecimal.ROUND_HALF_UP);
                    inv.setActStockQty(qty);
                    HYIDouble amt = qty.multiply(inv.getUnitPrice());
                    if (amt.scale() > 4)
                        amt = amt.setScale(4, BigDecimal.ROUND_HALF_UP);
                    inv.setAmount(amt);
                    app.getDacViewer().repaint();
                    warningIndicator.setMessage("");
                    choose = "";
                    return InventoryStoreState.class;
                } catch (NumberFormatException e) {
                    quantity.setLength(0);
                    warningIndicator.setMessage(res.getString("InputWrong"));
                }
            }
            return InventoryQuantityState.class;
        }
        return sinkState.getClass();
    }

    public static void clearQuantity() {
        quantity.setLength(0);
    }
}
