/**
 * State class
 * @since 2000
 * @author slackware
 */
 
package hyi.cream.state;

import java.util.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

public class InOutSelectState extends State {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private String label = "";

    static InOutSelectState inOutSelectState = null;

    private Class exitState = null;

    public static InOutSelectState getInstance() {
        try {
            if (inOutSelectState == null) {
                inOutSelectState = new InOutSelectState();
            }
        } catch (InstantiationException ex) {
        }
        return inOutSelectState;
    }

    /**
     * Constructor
     */
    public InOutSelectState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
		//System.out.println("This is InOutSelectState's Entry!");
        if (PARAM.isSendPunchInOutDetail()) {
        	app.getMessageIndicator().setMessage(res.getString("Attendance"));
        } else 
        	app.getMessageIndicator().setMessage(res.getString("InOut"));
        if (!(event.getSource() instanceof ClearButton) &&
                !((sourceState instanceof InOutSelectState) || (sourceState instanceof InOutNumberState)))
            exitState = sourceState.getClass();
	}

	public Class exit(EventObject event, State sinkState) {
		//System.out.println("This is InOutSelectState's Exit!");

	    Transaction trans = app.getCurrentTransaction();
        if (event.getSource() instanceof NumberButton) {
            label = ((NumberButton)event.getSource()).getNumberLabel();
            //System.out.println("label = " + label);
            if (label.equals("1")) {
                label = "";
                trans.setDealType2("B");
                return InOutNumberState.class;
            } else if (label.equals("2")) {  
                label = "";
                trans.setDealType2("C");   
                return InOutNumberState.class;
            }   else {                         
                label = "";
                return InOutSelectState.class;
            }
        }

        if (event.getSource() instanceof ClearButton) {
            label = "";
            trans.setDealType2("0");
//            return KeyLock1State.class;
            return exitState;
        }

        return sinkState.getClass();
	}
}
