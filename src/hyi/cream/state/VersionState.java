// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.Version;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;

/**
 * @author dai
 */
public class VersionState extends State {
	private POSTerminalApplication app  = POSTerminalApplication.getInstance(); 

	static VersionState versionState = null;

    public static VersionState getInstance() {
        try {
            if (versionState == null) {
                versionState = new VersionState();
            }
        } catch (InstantiationException ex) {
        }
        return versionState;
    }

    /**
     * Constructor
     */
    public VersionState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        app.getMessageIndicator().setMessage(CreamToolkit.getString("Warning"));
        app.getWarningIndicator().setMessage(Version.getVersion());
    }

    public Class exit(EventObject event, State sinkState) {
        return sinkState.getClass();
    }
}
