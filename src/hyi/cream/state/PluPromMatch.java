package hyi.cream.state;

import java.math.*;
import java.util.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;

/**
 * PluPromMatch definition class.
 * 提供一个商品促销配对类
 * 
 * @author Meyer 
 * @version 1.0
 */
public class PluPromMatch extends PromMatch{
    /**
     * 更新记录
     * 
     * Meyer / 1.0 / 2003-01-06
     * 		1. Create the class file
     * 
     */
    
    private HashMap matchedPluQtyMap;
    private int matchedCount;    
    	
	public PluPromMatch(String aPromID) {
		this.setPromID(aPromID);
		this.setMatchedFlag(false);
		this.setTotalAmt(new HYIDouble(0));
		
		this.matchedPluQtyMap = new HashMap();		
		Iterator csdIter = CombSaleDetail.queryByCphID(aPromID);
		while (csdIter.hasNext()) {
			CombSaleDetail csDetail = (CombSaleDetail)csdIter.next();
			String pluCode = csDetail.getPluCode();
			matchedPluQtyMap.put(pluCode, csDetail.getQuantity());
		}
		this.matchedCount = 0;	
	}
	

		
	/**
	 * @param pluQtyMap - HashMap, 
	 * 
	 */
	public boolean mapMatch(HashMap pluQtyMap, HashMap giftQtyMap) {
		int minMatchedCount = Integer.MAX_VALUE;
		Iterator iter = matchedPluQtyMap.keySet().iterator();
		//检查matchedPluQtyMap
		while (iter.hasNext()) {
			String matchedPluCode = (String)iter.next();
			int matchedPluQty = ((Integer)matchedPluQtyMap.get(matchedPluCode)).intValue();
			Object pluQtyValue = pluQtyMap.get(matchedPluCode);
			int curPluQty = (pluQtyValue != null) ? ((Integer)pluQtyValue).intValue() : 0;
			
			pluQtyValue = giftQtyMap.get(matchedPluCode);
			int giftPluQty = (pluQtyValue != null) ? ((Integer)pluQtyValue).intValue() : 0;
			
			//当matchedPluQtyMap中包含赠品时，同时检查赠品，
			if (curPluQty >= matchedPluQty + giftPluQty) {
				int count = curPluQty / (matchedPluQty + giftPluQty);
				minMatchedCount = minMatchedCount <= count ? minMatchedCount : count;
			} else {
				this.setMatchedFlag(false);
				return false;
			}						
		}	
		
		//再次单独检查赠品，以避免漏过matchedPluQtyMap中未包含赠品的情况
		iter = giftQtyMap.keySet().iterator();
		while (iter.hasNext()) {
			String giftPluCode = (String)iter.next();
			int giftPluQty = ((Integer)giftQtyMap.get(giftPluCode)).intValue();
			Object pluQtyValue = pluQtyMap.get(giftPluCode);
			int curPluQty = (pluQtyValue != null) ? ((Integer)pluQtyValue).intValue() : 0;
			
			if (curPluQty >= giftPluQty) {
				int count = curPluQty / giftPluQty;
				minMatchedCount = minMatchedCount <= count ? minMatchedCount : count;
			} else {
				this.setMatchedFlag(false);
				return false;
			}					
		}

		matchedCount = minMatchedCount;
		iter = matchedPluQtyMap.keySet().iterator();
		while (iter.hasNext()) {
			String matchedPluCode = (String)iter.next();
			int matchedPluQty = ((Integer)matchedPluQtyMap.get(matchedPluCode)).intValue();
			HYIDouble price = (HYIDouble)PLU.queryByInStoreCode(matchedPluCode).getSalingPriceDetail()[0];		
		 	this.setTotalAmt(this.getTotalAmt().addMe(
		 		price.multiply(new HYIDouble(matchedPluQty * matchedCount))
		 	).setScale(2, BigDecimal.ROUND_HALF_UP));
		}
		this.setMatchedFlag(true);
		return true;
	}
	
		
	public HashMap getMatchedPluQtyMap() {
		return this.matchedPluQtyMap;
	}
	
	public int getMatchedCount() {
		return this.matchedCount;
	}

	public static void main(String[] args) {
	}
	
}
