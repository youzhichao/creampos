package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.MMGroup;
import hyi.cream.dac.MixAndMatch;
import hyi.cream.dac.PLU;
import hyi.cream.dac.Rebate;
import hyi.cream.dac.TaxType;
import hyi.cream.dac.Transaction;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * @author Dai,Meyer
 * @version 1.2
 */
public class MixAndMatchState extends State {
    /**
     * 更新记录
     *
     * Dai/1.0 1. Create Class 2. Add mixAndMatch1()
     *
     * Meyer/1.1/2002-01-20 2. Add mixAndMatch2()
     *
     * Lxf /1.2/2005-03-01 1. Add type = 5 (for 灿坤)
     *
     */
    static MixAndMatchState mixAndMatchState = null;

    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    private static Object[] totalLineItem = null;

    private ArrayList totalPLU = new ArrayList();

    private ArrayList mixAndMatchIDArray = new ArrayList();

    private String mixAndMatchID = "";

    private ArrayList curPLUArray = new ArrayList();

    private ArrayList belongTo = new ArrayList();

    private LineItem curLineItem = null;

    private MixAndMatch curMixAndMatch = null;

    private static List group1Array = new ArrayList();

    private static List group2Array = new ArrayList();

    private static List group3Array = new ArrayList();

    private static List group4Array = new ArrayList();

    private static List group5Array = new ArrayList();

    private static List group6Array = new ArrayList();

    private int mixAndMatchAccount = 0;

    private ArrayList mixAndMatchedArray = new ArrayList();

    private PLU curPLU = null;

    private HYIDouble mixAndMatchedAmount = new HYIDouble(0);

    private HYIDouble tempMixAndMatchedAmount = new HYIDouble(0);

    private Hashtable taxMMAmount = new Hashtable();

    private Hashtable taxMMCount = new Hashtable();

    private ArrayList mmLineItemArray = new ArrayList();

    private Class exitState = null;

    //private ArrayList selectArray = null;

    private Map scalePluCacheMap = new HashMap();

    private Map mmGroupCacheMap = new HashMap();

    // private Map mmCacheMap = new HashMap();

    private Map rebateCacheMap = new HashMap();

    private Map pluPriceCacheMap = new HashMap();

//    private Map combPromotionHeaderCache = new HashMap();
    private Map combSaleDetailCache = new HashMap();

    /**
     * Meyer/2003-01-20 Get MixAndMatchVersion
     */
    private static final String MIX_AND_MATCH_VERSION = PARAM.getMixAndMatchVersion();

    public static MixAndMatchState getInstance() {
        try {
            if (mixAndMatchState == null) {
                mixAndMatchState = new MixAndMatchState();
            }
        } catch (InstantiationException ex) {
        }
        return mixAndMatchState;
    }

    public MixAndMatchState() throws InstantiationException {
        mixAndMatchState = this;
    }

    public void entry(EventObject event, State sourceState) {
        // LineItem lineItem = trans.getCurrentLineItem();
        boolean returnState = false;
//        ((SelectGiftState) StateMachine.getInstance().getStateInstance(
//                "hyi.cream.state.SelectGiftState")).initSelectGiftState2();

        if (sourceState instanceof ReturnSelect2State) {
            returnState = true;
            exitState = ReturnOneState.class;
            //selectArray = ((ReturnSelect2State) sourceState).getSelectedIndexes();
            // 清除跟M&M相关的内容以便重新计算
            initLineItemsForMM();

        } else if (sourceState instanceof ReturnSelect1State) {
            returnState = true;
            exitState = ReturnAllState.class;
            this.returnSelect1State = (ReturnSelect1State) sourceState;
            // selectArray = ((ReturnSelect2State)sourceState).getSelectedIndexes();
            // 清除跟M&M相关的内容以便重新计算
            initLineItemsForMM();

        } else if (sourceState instanceof SetQuantityState) {
            if (CreamSession.getInstance().getWorkingState() ==
                    WorkingStateEnum.PERIODICAL_ORDER_STATE)
                exitState = PrintPluState.class;
            else
                exitState = IdleState.class;
        } else if (sourceState instanceof ReturnSummaryState) {
            returnState = true;
            exitState = DrawerOpenState.class;
        } else if (sourceState instanceof VoidState) {
            // 清除跟M&M相关的内容以便重新计算
            initLineItemsForMM();
            exitState = PrintPluState.class;
        } else if (sourceState instanceof OverrideAmountState
                || sourceState instanceof OverrideReasonState) {
            exitState = IdleState.class;
        } else if (sourceState instanceof DIYOverrideAmountState) {
            exitState = IdleState.class;
        } else {
            exitState = PrintPluState.class;
        }

        //Bruce/20080414/
        if (PARAM.isJustCalcMMAtSummaryState())
            return;
        else
            calcMM(returnState);
    }

    public void calcMM(boolean returnState)
    {
        if (MIX_AND_MATCH_VERSION == null
                || MIX_AND_MATCH_VERSION.trim().equals("")
                || MIX_AND_MATCH_VERSION.trim().equals("1")) {

            mixAndMatch1();
        } else if (MIX_AND_MATCH_VERSION.trim().equals("2")) {
            mixAndMatch2();
            if (PARAM.isRoundDown()) {
                if (returnState) { // 退货时
                    HYIDouble tmpAmount = new HYIDouble(0);
                    Object[] lineItems = app.getCurrentTransaction().getLineItems();
                    for (int i = 0; i < lineItems.length; i++)
                        if (!((LineItem) lineItems[i]).getRemoved()) {
                            tmpAmount = tmpAmount
                                    .addMe(((LineItem) lineItems[i])
                                            .getAfterDiscountAmount());
                        }
                    BigDecimal amount1 = new BigDecimal(tmpAmount.toString());
                    BigDecimal rdAmount = amount1.subtract(
                            amount1.setScale(1, BigDecimal.ROUND_DOWN))
                            .negate();
                    HYIDouble roundDownAmount = (new HYIDouble(rdAmount
                            .doubleValue())).negate();
                    app.getCurrentTransaction().getMixAndMatchAmount0().addMe(roundDownAmount);
                } else { // 销售时
                    Object[] lineItems = app.getCurrentTransaction().getLineItems();
                    HYIDouble tmpAmount = new HYIDouble(0);
                    for (int i = 0; i < lineItems.length; i++) {
                        LineItem lineItemTmp = (LineItem) lineItems[i];
                        if (!lineItemTmp.getDetailCode().equalsIgnoreCase("O")
                                && !lineItemTmp.getRemoved()) {
                            tmpAmount = tmpAmount.addMe(((LineItem) lineItems[i])
                                    .getAfterDiscountAmount().setScale(2));
                        }
                    }
                    BigDecimal amount1 = new BigDecimal(tmpAmount.toString());
                    BigDecimal rdAmount = amount1.subtract(
                            amount1.setScale(1, BigDecimal.ROUND_DOWN))
                            .negate();
                    HYIDouble roundDownAmount = (new HYIDouble(rdAmount
                            .doubleValue())).negate();
                    app.getCurrentTransaction().getMixAndMatchAmount0().addMe(roundDownAmount);
                }
                // 置0 和 重算
                app.getCurrentTransaction().initForAccum();
                app.getCurrentTransaction().initSIInfo();
                app.getCurrentTransaction().accumulate();
            }
        } else if (MIX_AND_MATCH_VERSION.trim().equals("3")) {
            mixAndMatch3();
        }
        // add fro diy 2005-09-12
        processDIY();
    }

    // diy 相关处理
    private void processDIY() {
        if (PARAM.isDiySupport()
        // && app.getItemList().isDIY()
                && !app.getCurrentTransaction().getDIYOverride().isEmpty()) {

            Map diyMap = new HashMap();
            Map diyAmountMap = new HashMap();
            int len = app.getCurrentTransaction().getLineItems().length;
            for (int i = 0; i < len; i++) {
                LineItem li = (LineItem) app.getCurrentTransaction().getLineItems()[i];
                if (li.getDIYIndex() != null
                        && li.getDetailCode().equalsIgnoreCase("S")) {
                    if (diyMap.containsKey(li.getDIYIndex())) {
                        ((List) diyMap.get(li.getDIYIndex())).add(li);
                        diyAmountMap
                                .put(li.getDIYIndex(),
                                        ((HYIDouble) diyAmountMap.get(li
                                                .getDIYIndex())).add(li
                                                .getAfterDiscountAmount()));
                    } else {
                        List list = new ArrayList();
                        list.add(li);
                        diyMap.put(li.getDIYIndex(), list);
                        diyAmountMap.put(li.getDIYIndex(), li
                                .getAfterDiscountAmount());
                    }
                }
            }

            int diyCount = 0;
            HYIDouble allTotalAmount = new HYIDouble(0);
            // 分摊diy变价
            for (Iterator it = diyMap.keySet().iterator(); it.hasNext();) {
                String index = (String) it.next();
                List list = (List) diyMap.get(index);
                HYIDouble totalAmount = (HYIDouble) diyAmountMap.get(index);
                HYIDouble diyOverrideTotalAmount = (HYIDouble) app.getCurrentTransaction()
                        .getDIYOverrideAmount(index);
                if (diyOverrideTotalAmount == null
                        || diyOverrideTotalAmount.compareTo(new HYIDouble(0)) == 0)
                    continue;
                diyCount++;
                allTotalAmount = allTotalAmount.add(diyOverrideTotalAmount);
                String mmID = "9" + index;
                LineItem mmLineItem = new LineItem();
                mmLineItem.setPluNumber(mmID);

                mmLineItem.setQuantity(new HYIDouble(1));
                mmLineItem.setUnitPrice(diyOverrideTotalAmount);

                mmLineItem.setItemNumber(mmID);
                mmLineItem.setOriginalPrice(diyOverrideTotalAmount);

                mmLineItem.setAmount(diyOverrideTotalAmount);
                app.getCurrentTransaction().setMMDetail(mmID, mmLineItem);

                HYIDouble tmpTotalAmount = new HYIDouble(0);
                HYIDouble proportion = new HYIDouble(0);
                PLU plu = null;
                for (Iterator it2 = list.iterator(); it2.hasNext();) {
                    LineItem li = (LineItem) it2.next();
                    if (!it2.hasNext()) {
                        proportion = diyOverrideTotalAmount
                                .subtract(tmpTotalAmount);
                        li.setTempAfterDiscountAmount(li
                                .getTempAfterDiscountAmount().subtract(
                                        proportion));
                        plu = PLU.queryByItemNumber(li.getItemNumber());
                        setTaxMM(plu.getTaxType(), proportion);
                        System.out.println("---MixAndMacth | taxType : "
                                + plu.getTaxType() + " | proportion : "
                                + proportion);
                    } else {
                        proportion = diyOverrideTotalAmount.multiply(
                                li.getAfterDiscountAmount()).divide(
                                totalAmount, 2, BigDecimal.ROUND_HALF_UP);
                        tmpTotalAmount = tmpTotalAmount.add(proportion);
                        li.setTempAfterDiscountAmount(li
                                .getTempAfterDiscountAmount().subtract(
                                        proportion));
                        plu = PLU.queryByItemNumber(li.getItemNumber());
                        setTaxMM(plu.getTaxType(), proportion);
                        System.out.println("---MixAndMacth | taxType : "
                                + plu.getTaxType() + " | proportion : "
                                + proportion);
                    }
                }
            }
            app.getCurrentTransaction().addTaxMM(diyCount, allTotalAmount);

            System.out.println("--- MixAndMatch Amount : "
                    + app.getCurrentTransaction().getTotalMMAmount());
            app.getCurrentTransaction().initForAccum();
            app.getCurrentTransaction().initSIInfo();
            app.getCurrentTransaction().accumulate();
        }
    }

    ReturnSelect1State returnSelect1State;

    public ReturnSelect1State getReturnSelect1State() {
        return returnSelect1State;
    }

    /**
     * 退货或者更正的时候，清除所有跟M&M相关的内容以便重新计算M&M
     * By ZhaoHong
     */
    private void initLineItemsForMM() {
        Object[] lines = app.getCurrentTransaction().getLineItems();
        LineItem li = null;
        String discountNumber = null;
        for (int i = 0; i < lines.length; i++) {
            li = (LineItem) lines[i];

            if ((li.getDiscountType() != null && (li.getDiscountType().equals(
                    "M") || li.getDiscountType().equals("S")))
                    || (li.getTempDiscountType() != null && (li
                            .getTempDiscountType().equals("M") || li
                            .getTempDiscountType().equals("S")))) {
                // 去掉discountNumbers 中跟M&M相关的部分
                discountNumber = clearMMNumbers(li.getDiscountNumber());
                li.setAfterDiscountAmount(li.getAmount());
                li.setTempAfterDiscountAmount(new HYIDouble(0.00));
                li.setDiscountType(null);
                li.setTempDiscountType(null);
                li.setDiscountNumber(discountNumber);
                li.setTempDiscountNumber(null);
            }

        }
    }

    // By ZhaoHong 去掉discountNumbers 中跟M&M相关的部分
    private String clearMMNumbers(String discountNumbers) {
        if (discountNumbers == null || discountNumbers.length() == 0)
            return null;

        StringBuffer sb = new StringBuffer();
        StringTokenizer token = new StringTokenizer(discountNumbers, ",");
        while (token.hasMoreTokens()) {
            String tmpString = (String) token.nextToken();
            if (!tmpString.startsWith("M")) {
                sb.append(tmpString);
                if (token.hasMoreTokens())
                    sb.append(",");
            }
        }
        return sb.toString();
    }

    /**
     * Dai/ MixAndMatch Version 1
     *
     */
    private void mixAndMatch1() {
        // Map wholesalesMap = new HashMap();
        taxMMAmount.clear();
        taxMMCount.clear();
        app.getCurrentTransaction().setTaxMMAmount(new HYIDouble(0));
        app.getCurrentTransaction().setTaxMMCount(0);
        mmLineItemArray.clear();
        // Date curDate = new Date();
        totalLineItem = app.getCurrentTransaction().getLineItems();
        // get total PLU from total LineItem
        int length = totalLineItem.length;
        List pluTmpCacheList = new ArrayList();
        for (int i = 0; i < length; i++) {
            curLineItem = (LineItem) totalLineItem[i];
            // add for diy 2005-09-12 diy商品不参与组合促销
            if ((curLineItem.getDetailCode().equalsIgnoreCase("R")
                || curLineItem.getDetailCode().equalsIgnoreCase("S"))
                    && !curLineItem.getRemoved()
                    && (curLineItem.getDIYIndex() == null || curLineItem.getDIYIndex().equals(""))) {
                curLineItem.setTempAfterDiscountAmount(new HYIDouble(0));
                curPLU = getPLU(curLineItem);
                if (curPLU == null) {
                    continue;
                }
                if (curPLU.isWeightedPlu()) {
                    curPLU.setUID(i);
                    totalPLU.add(curPLU);
                    scalePluCacheMap.put(curPLU, curLineItem.getAmount());
                }

                if (pluTmpCacheList.contains(curPLU.getInStoreCode()))
                    ;
                else {
                    // pluTmpCacheList.add(curPLU.getInStoreCode());
                    Iterator it = MMGroup.queryByITEMNO(curPLU.getInStoreCode());
                    if (it == null)
                        continue;
                    if (it != null) {
                        while (it.hasNext()) {
                            MMGroup mmGroup = (MMGroup) it.next();
                            mixAndMatchID = mmGroup.getID();
                            if (MixAndMatch.queryByID(mixAndMatchID) == null)
                                continue;

                            if (mixAndMatchID != null
                                    && !mixAndMatchID.equalsIgnoreCase("000")
                                    && !mixAndMatchIDArray
                                            .contains(mixAndMatchID)) {
                                mixAndMatchIDArray.add(mixAndMatchID);
                                if (!mmGroupCacheMap.containsKey(mixAndMatchID
                                        + curPLU.getInStoreCode()))
                                    mmGroupCacheMap.put(mixAndMatchID
                                            + curPLU.getInStoreCode(), mmGroup);
                            }
                        }
                    }
                }

                if (!curPLU.isWeightedPlu()) {
                    int qty = curLineItem.getQuantity().intValue();
                    for (int j = 0; j < qty; j++) {
                        curPLU = (PLU) getPLU(curLineItem).clone();
                        curPLU.setUID(i);
                        totalPLU.add(curPLU);
                    }
                }
            }
        }

        pluTmpCacheList.clear();
        pluTmpCacheList = null;

        app.getCurrentTransaction().clearMMDetail();

        Collections.sort(mixAndMatchIDArray, new Comparator() {
            public int compare(Object o1, Object o2) {
                String s1 = (String) o1;
                String s2 = (String) o2;
                return -s1.compareTo(s2); // reverse order
            }

            public boolean equals(Object obj) {
                return ((String) obj).equals(this);
            }
        });

        int mmLength = mixAndMatchIDArray.size();
        System.out.println("(MM) mixandmatch count = " + mmLength);
        // ***************************Begin*************************************************//
        for (int i = 0; i < mmLength; i++) {
            boolean cankun = false;
            mixAndMatchID = (String) mixAndMatchIDArray.get(i);
            curMixAndMatch = validMM(mixAndMatchID);
            if (curMixAndMatch == null)
                continue;
            System.out.println("(MM) " + i + ". mixAndMatchID = "
                    + mixAndMatchID);
            // get all PLU Contained this MixAndMatchID
            curPLUArray.clear();
            length = totalPLU.size();
            for (int j = 0; j < length; j++) {
                curPLU = (PLU) totalPLU.get(j);
                if (compMixAndMatchID(mixAndMatchID, curPLU)) {
                    curPLUArray.add(curPLU);
                    System.out.println("(MM)     cur plu = "
                            + curPLU.getScreenName());
                }
            }

            // check MixAndMatch Type
            // type = "1", means "一般组合促销"
            System.out.println("(MM) MixAndMatch type = "
                    + curMixAndMatch.getType());
            int lLength = curPLUArray.size();
            if (curMixAndMatch.getType().equals("1")) {
                group1Array.clear();
                group2Array.clear();
                group3Array.clear();
                group4Array.clear();
                group5Array.clear();
                group6Array.clear();
                // check MixAndMatchGroupID
                // put PLU into GroupArray
                for (int j = 0; j < lLength; j++) {
                    curPLU = (PLU) curPLUArray.get(j);
                    String groupID = getMMGroupID(curPLU, mixAndMatchID);
                    if (groupID == null)
                        continue;
                    switch (Integer.parseInt(groupID)) {
                    case 1:
                        group1Array.add(curPLU);
                        break;
                    case 2:
                        group2Array.add(curPLU);
                        break;
                    case 3:
                        group3Array.add(curPLU);
                        break;
                    case 4:
                        group4Array.add(curPLU);
                        break;
                    case 5:
                        group5Array.add(curPLU);
                        break;
                    case 6:
                        group6Array.add(curPLU);
                        break;
                    }
                }

                // get MixAndMatch Account
                Integer aa = curMixAndMatch.getDiscountLimit();
                if (aa == null) {
                    mixAndMatchAccount = 0;
                } else {
                    mixAndMatchAccount = aa.intValue();
                }

                // check group quantity
                System.out.println("BEFORE checkGrpQty : ");
                if (checkGrpQty(curMixAndMatch)) {
                    System.out.println(" AFTER checkGrpQty : ");
                    // check MixAndMatch GroupQuantity and DiscountLimit
                    // get all MixAndMatched PLU
                    ArrayList unitMMArray = new ArrayList();
                    if (mixAndMatchAccount != 0) {
                        int mma = 0;
                        while (checkGrpQty(curMixAndMatch)
                                && mixAndMatchAccount != 0) {
                            mixAndMatchAccount = mixAndMatchAccount - 1;
                            mma++;
                            unitMMArray.clear();
                            // get MixAndMatched PLUArray
                            unitMMArray = getMMedPLU(unitMMArray,
                                    curMixAndMatch);
                            for (int m = 0; m < unitMMArray.size(); m++) {
                                mixAndMatchedArray.add(unitMMArray.get(m));
                            }
                            // mixAndMatchedArray =
                            // getMMedPLU(mixAndMatchedArray,
                            // curMixAndMatch);
                            HYIDouble amt = null;
                            System.out
                                    .println("(MM)            MixAndMatch Price Type = "
                                            + curMixAndMatch.getPriceType());
                            HYIDouble unitDiscountAmount = curMixAndMatch
                                    .getDiscountAmount();
                            // PriceType = "1", means "售价"
                            if (curMixAndMatch.getPriceType().equals("1")) {
                                // get MixAndMatched Amount
                                HYIDouble discountAmount = unitDiscountAmount;
                                System.out
                                        .println("(MM)                discount amount = "
                                                + discountAmount);
                                amt = getMixAndMatchAmount(unitMMArray)
                                        .subtract(discountAmount);
                                System.out
                                        .println("(MM)                get amount = "
                                                + amt);
                            } else if (curMixAndMatch.getPriceType()
                                    .equals("2")) {
                                // get MixAndMatched Amount
                                HYIDouble discountAmount = unitDiscountAmount
                                        .multiply(new HYIDouble(mma));
                                amt = discountAmount;
                                // System.out.println(" 折让" + amt );
                                // PriceType = "3", means "折扣（%）"
                            } else if (curMixAndMatch.getPriceType()
                                    .equals("3")) {
                                // get MixAndMatched Amount
                                HYIDouble pluAmount = getMixAndMatchAmount(unitMMArray);

                                // Bruce/20030401 折扣只算到小数位数1位
                                amt = pluAmount.multiply(unitDiscountAmount)
                                        .setScale(1, BigDecimal.ROUND_HALF_UP);

                                // PriceType = "4", means "优惠价"
                            } else if (curMixAndMatch.getPriceType()
                                    .equals("4")) {
                                // get MixAndMatched Amount
                                ArrayList newMixAndMatchedArray = new ArrayList();
                                for (int j = 0; j < mixAndMatchedArray.size(); j++) {
                                    PLU curPLU = (PLU) mixAndMatchedArray
                                            .get(j);
                                    if ((getMMGroupID(curPLU, mixAndMatchID))
                                            .equals("1")) {
                                        newMixAndMatchedArray.add(curPLU);
                                    }
                                }
                                HYIDouble grp1Amount = new HYIDouble(0);
                                HYIDouble grp1OrigAmount = getMixAndMatchAmount(newMixAndMatchedArray);
                                for (int j = 0; j < mixAndMatchedArray.size(); j++) {
                                    PLU curPLU = (PLU) mixAndMatchedArray
                                            .get(j);
                                    if ((getMMGroupID(curPLU, mixAndMatchID))
                                            .equals("1")) {
                                        grp1Amount = grp1Amount
                                                .addMe(curMixAndMatch
                                                        .getDiscountAmount());
                                    }
                                }
                                amt = grp1OrigAmount.subtract(grp1Amount);
                            } else if (curMixAndMatch.getPriceType()
                                    .equals("5")) { // for cankun
                                cankun = true;
                                amt = getPrices(unitMMArray, mixAndMatchID);
                            }
                            // mixAndMatchedArray.clear();
                            mixAndMatchedAmount = mixAndMatchedAmount
                                    .addMe(amt);
                            mmLineItemArray
                                    .add(new NewArray(mixAndMatchID, amt));
                        }
                        // get MixAndMatch Account
                        Integer cc = curMixAndMatch.getDiscountLimit();
                        if (cc == null) {
                            mixAndMatchAccount = 0 - mixAndMatchAccount;
                        } else {
                            mixAndMatchAccount = cc.intValue()
                                    - mixAndMatchAccount;
                        }
                        System.out.println("(MM)            current count = "
                                + mixAndMatchAccount);
                    } else {
                        mixAndMatchedArray.clear();
                        while (checkGrpQty(curMixAndMatch)) {
                            mixAndMatchAccount++;
                            unitMMArray.clear();
                            // get MixAndMatched PLUArray
                            unitMMArray = getMMedPLU(unitMMArray,
                                    curMixAndMatch);
                            for (int m = 0; m < unitMMArray.size(); m++) {
                                mixAndMatchedArray.add(unitMMArray.get(m));
                            }
                            HYIDouble amt = null;
                            System.out
                                    .println("(MM)            current type = "
                                            + curMixAndMatch.getPriceType());
                            // check MixAndMatch PriceType
                            // PriceType = "1", means "售价"
                            HYIDouble unitDiscountAmount = curMixAndMatch
                                    .getDiscountAmount();
                            if (curMixAndMatch.getPriceType().equals("1")) {
                                // get MixAndMatched Amount
                                HYIDouble discountAmount = unitDiscountAmount;
                                System.out
                                        .println("(MM)                discount amount = "
                                                + discountAmount);
                                amt = getMixAndMatchAmount(unitMMArray)
                                        .subtract(discountAmount);
                                System.out
                                        .println("(MM)                get amount = "
                                                + amt);
                                // PriceType = "2", means "折让"
                            } else if (curMixAndMatch.getPriceType()
                                    .equals("2")) {

                                // get MixAndMatched Amount
                                HYIDouble discountAmount = unitDiscountAmount;
                                System.out
                                        .println("(MM)                discount amount = "
                                                + discountAmount);
                                amt = discountAmount;

                                // PriceType = "3", means "折扣（%）"
                            } else if (curMixAndMatch.getPriceType()
                                    .equals("3")) {

                                // get MixAndMatched Amount
                                HYIDouble pluAmount = getMixAndMatchAmount(unitMMArray);
                                // Bruce/20030401 折扣只算到小数位数1位
                                amt = pluAmount.multiply(unitDiscountAmount)
                                        .setScale(1, BigDecimal.ROUND_HALF_UP);

                                // PriceType = "4", means "优惠价"
                            } else if (curMixAndMatch.getPriceType()
                                    .equals("4")) {
                                // get MixAndMatched Amount
                                ArrayList newMixAndMatchedArray = new ArrayList();
                                for (int j = 0; j < unitMMArray.size(); j++) {
                                    PLU curPLU = (PLU) mixAndMatchedArray
                                            .get(j);
                                    if ((getMMGroupID(curPLU, mixAndMatchID))
                                            .equals("1")) {
                                        newMixAndMatchedArray.add(curPLU);
                                    }
                                }
                                HYIDouble grp1Amount = new HYIDouble(0);
                                HYIDouble grp1OrigAmount = getMixAndMatchAmount(newMixAndMatchedArray);
                                for (int j = 0; j < unitMMArray.size(); j++) {
                                    PLU curPLU = (PLU) unitMMArray.get(j);
                                    if ((getMMGroupID(curPLU, mixAndMatchID))
                                            .equals("1")) {
                                        grp1Amount = grp1Amount
                                                .addMe(curMixAndMatch
                                                        .getDiscountAmount());
                                    }
                                }
                                amt = grp1OrigAmount.subtract(grp1Amount);
                            } else if (curMixAndMatch.getPriceType()
                                    .equals("5")) { // for cankun
                                cankun = true;
                                amt = getPrices(unitMMArray, mixAndMatchID);
                            }
                            // mixAndMatchedArray.clear();
                            mixAndMatchedAmount = mixAndMatchedAmount
                                    .addMe(amt);
                            mmLineItemArray
                                    .add(new NewArray(mixAndMatchID, amt));
                            System.out
                                    .println("(MM)                current amount = "
                                            + mixAndMatchedAmount);

                        }
                    }
                }
            }

            // type = "2", means "PACK组合促销"
            if (curMixAndMatch.getType().equals("2")) {
                String packAmountField = "";
                HYIDouble mmAmount = new HYIDouble(0);
                int packSeq;

                // get MixAndMatch Account
                Integer aa = curMixAndMatch.getDiscountLimit();
                if (aa == null) {
                    mixAndMatchAccount = 0;
                } else {
                    mixAndMatchAccount = aa.intValue();
                }

                // check MixAndMatch PackQuantity and DiscountLimit
                if (mixAndMatchAccount != 0) {
                    while ((packSeq = getPackQty(curMixAndMatch, curPLUArray)) != 0
                            && mixAndMatchAccount != 0) {
                        mixAndMatchAccount = mixAndMatchAccount - 1;
                        packAmountField = "PACK" + packSeq + "AMT";
                        mmAmount = (HYIDouble) curMixAndMatch
                                .getFieldValue(packAmountField);
                        NewArray arr = new NewArray(mixAndMatchID, mmAmount);
                        mmLineItemArray.add(arr);
                        // System.out.println(mixAndMatchID + " = " + mmAmount);
                        mixAndMatchedAmount = mixAndMatchedAmount
                                .addMe(mmAmount);
                    }

                    // get MixAndMatch Account
                    Integer bb = curMixAndMatch.getDiscountLimit();
                    if (bb == null) {
                        mixAndMatchAccount = 0 - mixAndMatchAccount;
                    } else {
                        mixAndMatchAccount = bb.intValue() - mixAndMatchAccount;
                    }
                    // /System.out.println(" current count = " +
                    // mixAndMatchAccount);

                } else {
                    while ((packSeq = getPackQty(curMixAndMatch, curPLUArray)) != 0) {
                        mixAndMatchAccount++;
                        packAmountField = "PACK" + packSeq + "AMT";
                        mmAmount = (HYIDouble) curMixAndMatch
                                .getFieldValue(packAmountField);
                        NewArray arr = new NewArray(mixAndMatchID, mmAmount);
                        mmLineItemArray.add(arr);
                        // System.out.println(mixAndMatchID + " = " + mmAmount);
                        mixAndMatchedAmount = mixAndMatchedAmount
                                .addMe(mmAmount);
                    }
                }

            }

            // type = "3", means "小计金额组合促销"
            if (curMixAndMatch.getType().equals("3")) {
            }

            // type = "0", means "Linked 组合促销"
            if (curMixAndMatch.getType().equals("0")) {
            }

            if (cankun)
                canKunProportin(mixAndMatchID, mixAndMatchedArray,
                        mixAndMatchedAmount, mixAndMatchAccount);
            else
                mmTaxProportion(mixAndMatchID, mixAndMatchedArray,
                        mixAndMatchedAmount, mixAndMatchAccount);
            // MMCatProportion(mixAndMatchedArray, mixAndMatchedAmount);
            curPLUArray.clear();
            mixAndMatchedArray.clear();
            tempMixAndMatchedAmount = tempMixAndMatchedAmount
                    .addMe(mixAndMatchedAmount);
            System.out.println("(MM) mixAndMatchedAmount = "
                    + mixAndMatchedAmount.toString());

            app.getCurrentTransaction().addTaxMM(mixAndMatchAccount, mixAndMatchedAmount);

            System.out.println("(MM) mixAndMatchedAmount = "
                    + mixAndMatchedAmount.toString());
            System.out.println("(MM) ********** MixAndMatch End **********");

            mixAndMatchedAmount = new HYIDouble(0);
        }

        // processWholesalesMM(wholesalesMap);

        app.getCurrentTransaction().setSalesCanNotEnd(false);
//        if (GetProperty.getSalesCanNotEnd("no").equalsIgnoreCase("yes")) {
//            for (Iterator it = totalPLU.iterator(); it.hasNext();) {
//                PLU plu = (PLU) it.next();
//                if (plu.getSaleType() != null
//                        && plu.getSaleType().trim().equals("1")) {
//                    app.getCurrentTransaction().setSalesCanNotEnd(true);
//                    break;
//                }
//            }
//        }
    }

    /**
     * allan/ MixAndMatch Version 3
     *
     */
    Map allMMMap = new HashMap();

    Map allMatchItems = new HashMap();

    private void mixAndMatch3() {
        taxMMAmount.clear();
        taxMMCount.clear();
        mmLineItemArray.clear();

        boolean cankun = false;
        allMMMap.clear();
        allMatchItems.clear();
        groupQty.clear();
        Transaction trans = app.getCurrentTransaction();
        trans.setTaxMMAmount(new HYIDouble(0));
        trans.setTaxMMCount(0);
        trans.clearMMDetail();

        totalLineItem = trans.getLineItems();
        int length = totalLineItem.length;
        for (int i = 0; i < length; i++) {
            LineItem item = (LineItem) totalLineItem[i];
            if (!((item.getDetailCode().equalsIgnoreCase("R") || item
                    .getDetailCode().equalsIgnoreCase("S"))
                    && !item.getRemoved() && (item.getDIYIndex() == null || item
                    .getDIYIndex().equals("")))) {
                continue;
            }
            item.setTempAfterDiscountAmount(new HYIDouble(0));

            Iterator it = MMGroup.queryByITEMNO(item.getItemNumber());
            if (it == null)
                continue;

            PLU plu = PLU.queryByItemNumber(item.getItemNumber());
            HashMap pluMM = new HashMap();
            pluMM.put("id", new Integer(i)); // lineitem index
            pluMM.put("plu", plu); // plu
            pluMM.put("itemno", plu.getItemNumber());
            pluMM.put("matchCount", new HYIDouble(0)); // 已经match的个数
            if (plu.isWeightedPlu()) {
                // 称重商品不计个数，统统视为一个
                pluMM.put("remainCount", new HYIDouble(1)); // 还未match的个数
                scalePluCacheMap.put(plu, item.getAmount());
            } else {
                pluMM.put("remainCount", item.getQuantity()); // 还未match的个数
            }

            for (int j = 1; it.hasNext(); j++) {
                MMGroup mmGroup = (MMGroup) it.next();
                if (MixAndMatch.queryByID(mmGroup.getID()) == null)
                    continue;
                String groupID = mmGroup.getGROUPNUM(); // mm group id
                pluMM.put("mmCount", new Integer(j)); // 改plu可参与j个组合;如果参与多个就不优先取该plu来match
                if (allMMMap.containsKey(mmGroup.getID())) {
                    Map allGroupMap = (Map) allMMMap.get(mmGroup.getID());
                    if (allGroupMap.containsKey(groupID)) {
                        List list = (List) allGroupMap.get(groupID);
                        list.add(pluMM);
                    } else {
                        List list = new ArrayList();
                        list.add(pluMM);
                        allGroupMap.put(groupID, list);
                    }
                } else {
                    Map allGroupMap = new HashMap();
                    List list = new ArrayList();
                    list.add(pluMM);
                    allGroupMap.put(groupID, list);
                    allMMMap.put(mmGroup.getID(), allGroupMap); // mm id
                }
            }
        }
        System.out.println("-----mixandmatch3 | allmap : " + allMMMap);

        List mmidList = Arrays.asList(allMMMap.keySet().toArray());
        Collections.sort(mmidList, new Comparator() {
            public int compare(Object o1, Object o2) {
                String s1 = (String) o1;
                String s2 = (String) o2;
                return -s1.compareTo(s2); // reverse order
            }

            public boolean equals(Object obj) {
                return ((String) obj).equals(this);
            }
        });
        System.out.println("-----mixandmatch3 | sort mmid list : " + mmidList);

        for (Iterator it = mmidList.iterator(); it.hasNext();) {
            List allMatchList = new ArrayList();
            String mixAndMatchID = (String) it.next();
            MixAndMatch mm = MixAndMatch.queryByID(mixAndMatchID);
            if (mm == null)
                continue;

            int limitMMCount = 0;
            Integer aa = mm.getDiscountLimit();
            if (aa == null) {
                limitMMCount = 0;
            } else {
                limitMMCount = aa.intValue();
            }
            boolean needCheckLimit = true;
            if (limitMMCount == 0)
                needCheckLimit = false;

            Map currentPluMMList = (Map) allMMMap.get(mixAndMatchID);
            if (mm.getType().equals("1")) {
                if (!makeGroupArrayType1(currentPluMMList, mm))
                    continue;
                HYIDouble amt = null;
                List matchList = new ArrayList();
                int minMultiple = multipleMatchType1(mm);
                // 每笔交易有促销限制
                if (needCheckLimit)
                    minMultiple = minMultiple > limitMMCount ? limitMMCount
                            : minMultiple;
                if (minMultiple <= 0)
                    continue;
                mixAndMatchAccount += minMultiple;
                matchList.addAll(matchGrpQty(group1Array, new HYIDouble(mm
                        .getGroup1Quantity().intValue())
                        .multiply(new HYIDouble(minMultiple)), 1));
                matchList.addAll(matchGrpQty(group2Array, new HYIDouble(mm
                        .getGroup2Quantity().intValue())
                        .multiply(new HYIDouble(minMultiple)), 2));
                matchList.addAll(matchGrpQty(group3Array, new HYIDouble(mm
                        .getGroup3Quantity().intValue())
                        .multiply(new HYIDouble(minMultiple)), 3));
                matchList.addAll(matchGrpQty(group4Array, new HYIDouble(mm
                        .getGroup4Quantity().intValue())
                        .multiply(new HYIDouble(minMultiple)), 4));
                matchList.addAll(matchGrpQty(group5Array, new HYIDouble(mm
                        .getGroup5Quantity().intValue())
                        .multiply(new HYIDouble(minMultiple)), 5));
                matchList.addAll(matchGrpQty(group6Array, new HYIDouble(mm
                        .getGroup6Quantity().intValue())
                        .multiply(new HYIDouble(minMultiple)), 6));

                if (matchList.isEmpty())
                    continue;
                HYIDouble unitDiscountAmount = mm.getDiscountAmount();
                if (mm.getPriceType().equals("1")) {
                    // PriceType = "1", means "售价"
                    HYIDouble discountAmount = unitDiscountAmount
                            .multiply(new HYIDouble(minMultiple));
                    System.out.println("(MM)                discount amount = "
                            + discountAmount);
                    amt = getMatchedPriceAmount(matchList).subtract(
                            discountAmount);
                    System.out.println("(MM)                get amount = "
                            + amt);
                } else if (mm.getPriceType().equals("2")) {
                    HYIDouble discountAmount = unitDiscountAmount
                            .multiply(new HYIDouble(minMultiple));
                    System.out.println("(MM)                discount amount = "
                            + discountAmount);
                    amt = discountAmount;
                } else if (mm.getPriceType().equals("3")) {
                    // PriceType = "3", means "折扣（%）"
                    // Bruce/20030401 折扣只算到小数位数1位
                    amt = getMatchedPriceAmount(matchList).multiply(
                            unitDiscountAmount).setScale(1,
                            BigDecimal.ROUND_HALF_UP);
                } else if (mm.getPriceType().equals("4")) {
                    // PriceType = "4", means "优惠价" 只对group1有效
                    List group1List = new ArrayList();
                    HYIDouble group1Qty = new HYIDouble(0);
                    for (Iterator it2 = matchList.iterator(); it.hasNext();) {
                        Map data = (Map) it2.next();
                        if (data.get("groupID").equals("1")) {
                            group1List.add(data);
                            group1Qty = group1Qty.add((HYIDouble) data
                                    .get("matchCount"));
                        }
                    }
                    // get MixAndMatched Amount
                    amt = getMatchedPriceAmount(group1List).subtract(
                            mm.getDiscountAmount().multiply(group1Qty));
                } else if (mm.getPriceType().equals("5")) {
                    // for cankun
                    cankun = true;
                    mixAndMatchedAmount = getPricesFromMMGroup(matchList,
                            mixAndMatchID);
                }/* else if (mm.getPriceType().compareTo("A") >= 0
                        && mm.getPriceType().compareTo("Z") <= 0) {
                    // 以促销价送礼物
                    // TODO "A" <= priceType <= "G" 直接选择礼物
                    // TODO "H" <= priceType <= "Z" 通过游戏选择
                    SelectGiftState selectGiftState = ((SelectGiftState) StateMachine
                            .getInstance().getStateInstance(
                                    "hyi.cream.state.SelectGiftState"));
                    selectGiftState.addTrigger(mm.getID());
                    System.out
                            .println("------------- mixAndMatch | triggerList : "
                                    + selectGiftState.getTriggerList());
                    matchList.clear();
                    amt = new HYIDouble(0);
                }*/
                if (needCheckLimit)
                    limitMMCount -= minMultiple;
                allMatchList.addAll(matchList);
                mixAndMatchedAmount = mixAndMatchedAmount.addMe(amt);
                System.out.println("(MM)                current amount = "
                        + mixAndMatchedAmount);
                System.out.println("--------- allMatchList : " + allMatchList);
            } else if (mm.getType().equals("2")) {
                // type = "2", means "PACK组合促销"
                // groupQty <==> packQty
                String packAmountField = "";
                if (!makeGroupArrayType2(currentPluMMList, mm))
                    break;

                HYIDouble totalQty = getGroupQty(group1Array);
                HYIDouble matchQty = new HYIDouble(0);
                // check MixAndMatch PackQuantity and DiscountLimit
                HYIDouble discountAmount = new HYIDouble(0);
                for (int i = groupQty.size() - 1; i >= 0; i--) {
                    int qty = ((HYIDouble) groupQty.get(i)).intValue();
                    int multiple = (totalQty.divide(new HYIDouble(qty), 0,
                            BigDecimal.ROUND_DOWN)).intValue();
                    if (needCheckLimit) {
                        multiple = limitMMCount > multiple ? multiple
                                : limitMMCount;
                        limitMMCount -= multiple;
                    }
                    if (multiple <= 0)
                        continue;
                    mixAndMatchAccount += multiple;
                    packAmountField = "PACK" + (i + 1) + "AMT";
                    discountAmount = discountAmount.add(((HYIDouble) mm
                            .getFieldValue(packAmountField))
                            .multiply(new HYIDouble(multiple)));
                    matchQty = matchQty.add((new HYIDouble(qty))
                            .multiply(new HYIDouble(multiple)));
                    totalQty = totalQty.subtract(matchQty);
                }
                mixAndMatchedAmount = mixAndMatchedAmount.add(discountAmount);
                allMatchList.addAll(packMatch(group1Array, matchQty));
                System.out.println("--------- allMatchList : " + allMatchList);
            } else if (mm.getType().equals("3")) {
                // type = "3", means "小计金额组合促销"
                if (!makeGroupArrayType3(currentPluMMList, mm))
                    break;
                // 每笔交易有促销限制
                if (needCheckLimit && limitMMCount < 0)
                    continue;

                List tmp = null;
                if (!group2Array.isEmpty()) {
                    tmp = matchGrpPrice(group2Array, new HYIDouble(mm
                            .getGroup2Quantity().intValue()), 2);
                    if (tmp == null)
                        continue;
                }
                if (!group3Array.isEmpty()) {
                    tmp = matchGrpPrice(group3Array, new HYIDouble(mm
                            .getGroup3Quantity().intValue()), 3);
                    if (tmp == null)
                        continue;
                }
                if (!group4Array.isEmpty()) {
                    tmp = matchGrpPrice(group4Array, new HYIDouble(mm
                            .getGroup4Quantity().intValue()), 4);
                    if (tmp == null)
                        continue;
                }
                if (!group5Array.isEmpty()) {
                    tmp = matchGrpPrice(group5Array, new HYIDouble(mm
                            .getGroup5Quantity().intValue()), 5);
                    if (tmp == null)
                        continue;
                }
                if (!group6Array.isEmpty()) {
                    tmp = matchGrpPrice(group6Array, new HYIDouble(mm
                            .getGroup6Quantity().intValue()), 6);
                    if (tmp == null)
                        continue;
                }
                allMatchList.addAll(group1Array);
                mixAndMatchAccount++;
                limitMMCount--;
            } else if (mm.getType().equals("0")) {
                // type = "0", means "Linked 组合促销"
                // 未实现
            }

            if (cankun)
                proportinFromMMGroup(mixAndMatchID, allMatchList,
                        mixAndMatchedAmount, mixAndMatchAccount);
            else if (mm.getType().equals("3")) {
                mixAndMatchedAmount = mmTaxProportionType3(mm, allMatchList,
                        mixAndMatchedAmount, mixAndMatchAccount);
            } else
                mmTaxProportionV3(mixAndMatchID, allMatchList,
                        mixAndMatchedAmount, mixAndMatchAccount);
            // MMCatProportion(mixAndMatchedArray, mixAndMatchedAmount);
            allMatchList.clear();
            trans.addTaxMM(mixAndMatchAccount, mixAndMatchedAmount);

            System.out.println("(MM) mixAndMatchedAmount = "
                    + mixAndMatchedAmount.toString());
            System.out.println("(MM) ********** MixAndMatch End **********");
            mixAndMatchedAmount = new HYIDouble(0);
            mixAndMatchAccount = 0;
        }
        trans.setSalesCanNotEnd(false);
//        if (GetProperty.getSalesCanNotEnd("no").equalsIgnoreCase("yes")) {
//            for (Iterator it = totalPLU.iterator(); it.hasNext();) {
//                PLU plu = (PLU) it.next();
//                if (plu.getSaleType() != null
//                        && plu.getSaleType().trim().equals("1")) {
//                    app.getCurrentTransaction().setSalesCanNotEnd(true);
//                    break;
//                }
//            }
//        }
    }

    // 获得最小倍数
    private int multipleMatchType1(MixAndMatch mm) {
        HYIDouble minMultiple = new HYIDouble(0);
        if (mm.getGroup1Quantity().intValue() > 0) {
            HYIDouble multiple1 = ((HYIDouble) groupQty.get(0)).divide(
                    new HYIDouble(mm.getGroup1Quantity().intValue()), 0,
                    BigDecimal.ROUND_DOWN);
            minMultiple = multiple1;
        }

        if (mm.getGroup2Quantity().intValue() > 0) {
            HYIDouble multiple2 = ((HYIDouble) groupQty.get(1)).divide(
                    new HYIDouble(mm.getGroup2Quantity().intValue()), 0,
                    BigDecimal.ROUND_DOWN);
            if (minMultiple.compareTo(multiple2) > 0)
                minMultiple = multiple2;
        }

        if (mm.getGroup3Quantity().intValue() > 0) {
            HYIDouble multiple3 = ((HYIDouble) groupQty.get(2)).divide(
                    new HYIDouble(mm.getGroup3Quantity().intValue()), 0,
                    BigDecimal.ROUND_DOWN);
            if (minMultiple.compareTo(multiple3) > 0)
                minMultiple = multiple3;
        }

        if (mm.getGroup4Quantity().intValue() > 0) {
            HYIDouble multiple4 = ((HYIDouble) groupQty.get(3)).divide(
                    new HYIDouble(mm.getGroup4Quantity().intValue()), 0,
                    BigDecimal.ROUND_DOWN);
            if (minMultiple.compareTo(multiple4) > 0)
                minMultiple = multiple4;
        }

        if (mm.getGroup5Quantity().intValue() > 0) {
            HYIDouble multiple5 = ((HYIDouble) groupQty.get(4)).divide(
                    new HYIDouble(mm.getGroup5Quantity().intValue()), 0,
                    BigDecimal.ROUND_DOWN);
            if (minMultiple.compareTo(multiple5) > 0)
                minMultiple = multiple5;
        }

        if (mm.getGroup6Quantity().intValue() > 0) {
            HYIDouble multiple6 = ((HYIDouble) groupQty.get(5)).divide(
                    new HYIDouble(mm.getGroup6Quantity().intValue()), 0,
                    BigDecimal.ROUND_DOWN);
            if (minMultiple.compareTo(multiple6) > 0)
                minMultiple = multiple6;
        }
        return minMultiple.intValue();
    }

    private HYIDouble getMatchedPriceAmount(List plusArray) {
        HYIDouble pluAmounts = new HYIDouble(0);
        for (Iterator it = plusArray.iterator(); it.hasNext();) {
            Map data = (Map) it.next();
            HYIDouble pluAmount = new HYIDouble(0);
            PLU plu = (PLU) data.get("plu");
            if (plu.isWeightedPlu())
                pluAmount = pluAmount.addMe((HYIDouble) scalePluCacheMap
                        .get(plu));
            else
                pluAmount = pluAmount.addMe(getPluPrice(plu));
            pluAmounts = pluAmounts.add(pluAmount.multiply((HYIDouble) data
                    .get("matchCount")));
        }
        return pluAmounts;
    }

    List groupQty = new ArrayList();

    private List packMatch(List groupArray, HYIDouble grpCount) {
        List matchList = new ArrayList();
        if (grpCount.compareTo(CreamCache.getInstance().getHYIDouble0()) > 0) {
            for (Iterator it = groupArray.iterator(); it.hasNext();) {
                Map matchData = new HashMap();
                Map data = (Map) it.next();
                HYIDouble remailCount = (HYIDouble) data.get("remainCount");
                HYIDouble matchCount = (HYIDouble) data.get("matchCount");
                if (remailCount.compareTo(CreamCache.getInstance()
                        .getHYIDouble0()) > 0) {
                    if (remailCount.compareTo(grpCount) >= 0) {
                        remailCount = remailCount.subtract(grpCount);
                        matchCount = matchCount.add(grpCount);
                        matchData.put("matchCount", grpCount);
                    } else {
                        matchCount = matchCount.add(remailCount);
                        grpCount = grpCount.subtract(remailCount);
                        matchData.put("matchCount", remailCount);
                        remailCount = new HYIDouble(0);
                    }
                    matchData.put("id", data.get("id"));
                    matchData.put("plu", data.get("plu"));
                    data.put("remainCount", remailCount);
                    data.put("matchCount", matchCount);
                    matchList.add(matchData);
                }
                if (grpCount
                        .compareTo(CreamCache.getInstance().getHYIDouble0()) == 0)
                    break;
            }
        }
        return matchList;
    }

    // 以数量为匹配单位
    private List matchGrpQty(List groupArray, HYIDouble grpCount, int groupID) {
        List matchList = new ArrayList();
        if (grpCount.compareTo(CreamCache.getInstance().getHYIDouble0()) > 0) {
            for (Iterator it = groupArray.iterator(); it.hasNext();) {
                Map matchData = new HashMap();
                Map data = (Map) it.next();
                HYIDouble remailCount = (HYIDouble) data.get("remainCount");
                HYIDouble matchCount = (HYIDouble) data.get("matchCount");
                if (remailCount.compareTo(CreamCache.getInstance()
                        .getHYIDouble0()) > 0) {
                    if (remailCount.compareTo(grpCount) >= 0) {
                        remailCount = remailCount.subtract(grpCount);
                        matchCount = matchCount.add(grpCount);
                        matchData.put("matchCount", grpCount);
                        groupQty.set(groupID - 1, ((HYIDouble) groupQty
                                .get(groupID - 1)).subtract(grpCount));
                        grpCount = new HYIDouble(0);
                    } else {
                        matchCount = matchCount.add(remailCount);
                        grpCount = grpCount.subtract(remailCount);
                        matchData.put("matchCount", remailCount);
                        groupQty.set(groupID - 1, ((HYIDouble) groupQty
                                .get(groupID - 1)).subtract(remailCount));
                        remailCount = new HYIDouble(0);
                    }
                    matchData.put("id", data.get("id"));
                    matchData.put("groupID", new Integer(groupID));
                    matchData.put("plu", data.get("plu"));
                    data.put("remainCount", remailCount);
                    data.put("matchCount", matchCount);
                    matchList.add(matchData);
                }
                if (grpCount
                        .compareTo(CreamCache.getInstance().getHYIDouble0()) == 0)
                    // 匹配完毕
                    break;
            }
            if (grpCount.compareTo(CreamCache.getInstance().getHYIDouble0()) > 0)
                // 没有匹配完全则全失败
                return null;
        }
        return matchList;
    }

    // 以金额为匹配单位
    private List matchGrpPrice(List groupArray, HYIDouble grpAmount, int groupID) {
        List matchList = new ArrayList();
        HYIDouble totalAmount = new HYIDouble(0);
        if (grpAmount.compareTo(CreamCache.getInstance().getHYIDouble0()) > 0) {
            for (Iterator it = groupArray.iterator(); it.hasNext();) {
                Map matchData = new HashMap();
                Map data = (Map) it.next();
                HYIDouble remailCount = (HYIDouble) data.get("remainCount");
                HYIDouble matchCount = (HYIDouble) data.get("matchCount");
                if (remailCount.compareTo(CreamCache.getInstance()
                        .getHYIDouble0()) > 0) {
                    PLU plu = (PLU) data.get("plu");
                    totalAmount = totalAmount.add(getPluPrice(plu).multiply(
                            remailCount));
                    matchCount = matchCount.add(remailCount);
                    matchData.put("matchCount", remailCount);
                    remailCount = new HYIDouble(0);
                    matchData.put("id", data.get("id"));
                    matchData.put("groupID", new Integer(groupID));
                    matchData.put("plu", data.get("plu"));
                    data.put("remainCount", remailCount);
                    data.put("matchCount", matchCount);
                    matchList.add(matchData);
                }
            }
            if (grpAmount.compareTo(totalAmount) > 0)
                // 没有匹配完全则全失败
                return null;
        }
        return matchList;
    }

//    private HYIDouble matchPack(MixAndMatch mm, List matchList) {
//        HYIDouble discountAmount = new HYIDouble(0);
//        for (int i = groupQty.size() - 1; i >= 0; i--) {
//
//        }
//        return discountAmount;
//    }

    // 生成groupXArray,并做初步的审核匹配
    private boolean makeGroupArrayType1(Map currentPluMMList, MixAndMatch mm) {
        groupQty.add(0, new HYIDouble(0));
        groupQty.add(1, new HYIDouble(0));
        groupQty.add(2, new HYIDouble(0));
        groupQty.add(3, new HYIDouble(0));
        groupQty.add(4, new HYIDouble(0));
        groupQty.add(5, new HYIDouble(0));
        group1Array.clear();
        group2Array.clear();
        group3Array.clear();
        group4Array.clear();
        group5Array.clear();
        group6Array.clear();

        if (mm.getGroup1Quantity() != null
                && mm.getGroup1Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("1"))
                group1Array.addAll((List) currentPluMMList.get("1"));
            else
                return false;
        }

        if (mm.getGroup2Quantity() != null
                && mm.getGroup2Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("2"))
                group2Array.addAll((List) currentPluMMList.get("2"));
            else
                return false;
        }

        if (mm.getGroup3Quantity() != null
                && mm.getGroup3Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("3"))
                group3Array.addAll((List) currentPluMMList.get("3"));
            else
                return false;
        }

        if (mm.getGroup4Quantity() != null
                && mm.getGroup4Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("4"))
                group4Array.addAll((List) currentPluMMList.get("4"));
            else
                return false;
        }

        if (mm.getGroup5Quantity() != null
                && mm.getGroup5Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("5"))
                group5Array.addAll((List) currentPluMMList.get("5"));
            else
                return false;
        }

        if (mm.getGroup6Quantity() != null
                && mm.getGroup6Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("6"))
                group6Array.addAll((List) currentPluMMList.get("6"));
            else
                return false;
        }

        HYIDouble count = getGroupQty(group1Array);
        if (count.intValue() >= mm.getGroup1Quantity().intValue())
            groupQty.set(0, count);
        else
            return false;

        count = getGroupQty(group2Array);
        if (count.intValue() >= mm.getGroup2Quantity().intValue())
            groupQty.set(1, count);
        else
            return false;

        count = getGroupQty(group3Array);
        if (count.intValue() >= mm.getGroup3Quantity().intValue())
            groupQty.set(2, count);
        else
            return false;

        count = getGroupQty(group4Array);
        if (count.intValue() >= mm.getGroup4Quantity().intValue())
            groupQty.set(3, count);
        else
            return false;

        count = getGroupQty(group5Array);
        if (count.intValue() >= mm.getGroup5Quantity().intValue())
            groupQty.set(4, count);
        else
            return false;

        count = getGroupQty(group6Array);
        if (count.intValue() >= mm.getGroup6Quantity().intValue())
            groupQty.set(5, count);
        else
            return false;

        sortGroupArray();
        return true;
    }

    // 生成groupXArray,并做初步的审核匹配 use pack
    // type = "2", means "PACK组合促销"
    private boolean makeGroupArrayType2(Map currentPluMMList, MixAndMatch mm) {
        group1Array.clear();
        group2Array.clear();
        group3Array.clear();
        group4Array.clear();
        group5Array.clear();
        group6Array.clear();
        groupQty.clear();

        if (currentPluMMList.containsKey("1"))
            group1Array.addAll((List) currentPluMMList.get("1"));
        if (group1Array.isEmpty())
            return false;
        int size = group1Array.size();

        if (mm.getPack1Quantity() != null
                && mm.getPack1Quantity().intValue() > 0
                && mm.getPack1Quantity().intValue() >= size)
            groupQty.add(0, new HYIDouble(mm.getPack1Quantity().intValue()));
        else
            return false;

        if (mm.getPack2Quantity() != null
                && mm.getPack2Quantity().intValue() > 0
                && mm.getPack2Quantity().intValue() >= size)
            groupQty.add(1, new HYIDouble(mm.getPack2Quantity().intValue()));
        else
            return true;

        if (mm.getPack3Quantity() != null
                && mm.getPack3Quantity().intValue() > 0
                && mm.getPack3Quantity().intValue() >= size)
            groupQty.add(2, new HYIDouble(mm.getPack3Quantity().intValue()));
        else
            return true;

        if (mm.getPack4Quantity() != null
                && mm.getPack4Quantity().intValue() > 0
                && mm.getPack4Quantity().intValue() >= size)
            groupQty.add(3, new HYIDouble(mm.getPack4Quantity().intValue()));
        else
            return true;

        if (mm.getPack5Quantity() != null
                && mm.getPack5Quantity().intValue() > 0
                && mm.getPack5Quantity().intValue() >= size)
            groupQty.add(4, new HYIDouble(mm.getPack5Quantity().intValue()));
        else
            return true;

        if (mm.getPack6Quantity() != null
                && mm.getPack6Quantity().intValue() > 0
                && mm.getPack6Quantity().intValue() >= size)
            groupQty.add(5, new HYIDouble(mm.getPack6Quantity().intValue()));
        else
            return true;

        if (mm.getPack7Quantity() != null
                && mm.getPack7Quantity().intValue() > 0
                && mm.getPack7Quantity().intValue() >= size)
            groupQty.add(6, new HYIDouble(mm.getPack7Quantity().intValue()));
        else
            return true;

        if (mm.getPack8Quantity() != null
                && mm.getPack8Quantity().intValue() > 0
                && mm.getPack8Quantity().intValue() >= size)
            groupQty.add(7, new HYIDouble(mm.getPack8Quantity().intValue()));
        else
            return true;

        sortGroupArray();
        return true;
    }

    // 生成groupXArray,并做初步的审核匹配 use price
    // type = "3", means "小计金额组合促销"
    private boolean makeGroupArrayType3(Map currentPluMMList, MixAndMatch mm) {
        groupQty.add(0, new HYIDouble(0));
        groupQty.add(1, new HYIDouble(0));
        groupQty.add(2, new HYIDouble(0));
        groupQty.add(3, new HYIDouble(0));
        groupQty.add(4, new HYIDouble(0));
        groupQty.add(5, new HYIDouble(0));
        group1Array.clear();
        group2Array.clear();
        group3Array.clear();
        group4Array.clear();
        group5Array.clear();
        group6Array.clear();

        if (mm.getGroup1Quantity() != null
                && mm.getGroup1Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("1"))
                group1Array.addAll((List) currentPluMMList.get("1"));
        }

        if (mm.getGroup2Quantity() != null
                && mm.getGroup2Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("2"))
                group2Array.addAll((List) currentPluMMList.get("2"));
            else
                return false;
        }

        if (mm.getGroup3Quantity() != null
                && mm.getGroup3Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("3"))
                group3Array.addAll((List) currentPluMMList.get("3"));
            else
                return false;
        }

        if (mm.getGroup4Quantity() != null
                && mm.getGroup4Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("4"))
                group4Array.addAll((List) currentPluMMList.get("4"));
            else
                return false;
        }

        if (mm.getGroup5Quantity() != null
                && mm.getGroup5Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("5"))
                group5Array.addAll((List) currentPluMMList.get("5"));
            else
                return false;
        }

        if (mm.getGroup6Quantity() != null
                && mm.getGroup6Quantity().intValue() > 0) {
            if (currentPluMMList.containsKey("6"))
                group6Array.addAll((List) currentPluMMList.get("6"));
            else
                return false;
        }

        sortGroupArray();
        return true;
    }

    private HYIDouble getGroupQty(List groupArray) {
        HYIDouble count = new HYIDouble(0);
        for (Iterator it = groupArray.iterator(); it.hasNext();) {
            Map pluMM = (Map) it.next();
            try {
                count = count.add((HYIDouble) pluMM.get("remainCount"));
            } catch (NumberFormatException e) {
            }
        }
        return count;
    }

    private void sortGroupArray() {
        Collections.sort(group1Array, new MMComparator());
        Collections.sort(group2Array, new MMComparator());
        Collections.sort(group3Array, new MMComparator());
        Collections.sort(group4Array, new MMComparator());
        Collections.sort(group5Array, new MMComparator());
        Collections.sort(group6Array, new MMComparator());
    }

    class MMComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            Map s1 = (Map) o1;
            Map s2 = (Map) o2;
            HYIDouble price1 = getPluPrice((PLU) s1.get("plu"));
            HYIDouble price2 = getPluPrice((PLU) s2.get("plu"));
            return -price1.compareTo(price2); // reverse order
        }

        public boolean equals(Object obj) {
            return ((String) obj).equals(this);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (!PARAM.isJustCalcMMAtSummaryState())
            postCalcMM();

        return exitState;
    }

    public void postCalcMM()
    {
        // System.out.println("MixAndMatchState exit");
        totalLineItem = null;
        totalPLU.clear();
        belongTo.clear();
        mixAndMatchIDArray.clear();
        mixAndMatchedAmount = new HYIDouble(0);

        if (exitState.equals(DrawerOpenState.class)) {
            Transaction tran = app.getCurrentTransaction();
            tran.initForAccum();
            tran.initSIInfo();
            tran.accumulate();
            CreamToolkit.showText(app.getCurrentTransaction(), 1);

            if (MIX_AND_MATCH_VERSION == null
                    || MIX_AND_MATCH_VERSION.trim().equals("")
                    || MIX_AND_MATCH_VERSION.trim().equals("1")
                    || MIX_AND_MATCH_VERSION.trim().equals("3")) {

                tran.updateTaxMM();
            }

            // Bruce> Remoe it.
            //tran.store();

//            DbConnection connection = null;
//            try {
//                connection = CreamToolkit.getTransactionalConnection();
//                CreamPrinter.getInstance().printPayment(connection, app.getCurrentTransaction());
//                connection.commit();
//            } catch (SQLException e) {
//                CreamToolkit.logMessage(e);
//            } finally {
//                CreamToolkit.releaseConnection(connection);
//            }
        }
    }

//    public static void setTransaction(Transaction trans) {
//        MixAndMatchState.trans = trans;
//    }

    class NewArray {
        private String one = "";

        private int two = 0;

        private HYIDouble three = null;

        public NewArray(String one, HYIDouble three) {
            this.one = one;
            this.three = three;
        }

        public String getOne() {
            return one;
        }

        public void setOne(String one) {
            this.one = one;
        }

        public int getTwo() {
            return two;
        }

        public void setTwo(int two) {
            this.two = two;
        }

        public HYIDouble getThree() {
            return three;
        }

        public void setThree(HYIDouble three) {
            this.three = three;
        }
    }

//    public ArrayList getSelectArray() {
//        return selectArray;
//    }

    // set to transaction
    public void setTaxMM(String fieldName, HYIDouble mmAmount) {
        if (taxMMAmount.containsKey(fieldName)) {
            taxMMAmount.put(fieldName, ((HYIDouble) taxMMAmount.get(fieldName))
                    .addMe(mmAmount));
        } else {
            taxMMAmount.put(fieldName, mmAmount);
        }

//        if (taxMMCount.containsKey(fieldName)) {
//            taxMMCount.put(fieldName, new Integer(((Integer) taxMMCount
//                    .get(fieldName)).intValue() + 1));
//        } else {
//            taxMMCount.put(fieldName, new Integer(1));
//        }
        app.getCurrentTransaction().setTaxMMAmount(taxMMAmount);
//        app.getCurrentTransaction().setTaxMMCount(taxMMCount);
    }

    /**
     * 折扣金额要指定分摊到指定的组合内单品上 指定还原金比例 mmgroup新增字段 price decimal(10,2) 指定售价
     * rebateRate decimal(10,2) 指定还原金比例
     *
     * @param mmID
     *            组合促销id
     * @param plusArray
     *            匹配的组合商品列表
     * @param amount
     *            折扣金额
     * @param qty
     *            限量
     */
    public void canKunProportin(String mmID, List plusArray, HYIDouble amount,
            int qty) {
        if (amount.compareTo(new HYIDouble(0)) == 0)
            return;

        LineItem mmLineItem = new LineItem();
        mmLineItem.setPluNumber(mmID);

        mmLineItem.setQuantity(new HYIDouble(qty));
        mmLineItem.setUnitPrice(amount);

        mmLineItem.setItemNumber(mmID);
        mmLineItem.setOriginalPrice(amount);

        mmLineItem.setAmount(amount);
        app.getCurrentTransaction().setMMDetail(mmID, mmLineItem);

        if (plusArray.size() == 0) {
            return;
        }

        HYIDouble unitPrice = new HYIDouble(0);
        HYIDouble mmPrice = new HYIDouble(0);
        HYIDouble subAmt = new HYIDouble(0);
        String fieldName = null;
        // int m = 0;
        Map uidMap = new HashMap();
        for (int i = 0; i < plusArray.size(); i++) {
            Integer uid = new Integer(((PLU) plusArray.get(i)).getUID());
            if (uidMap.containsKey(uid))
                uidMap.put(uid, new Integer(((Integer) uidMap.get(uid))
                        .intValue() + 1));
            else
                uidMap.put(uid, new Integer(1));
        }

        PLU plu = null;
        for (Iterator it = uidMap.keySet().iterator(); it.hasNext();) {
            int uid = ((Integer) it.next()).intValue();
            int quanty = ((Integer) uidMap.get(new Integer(uid))).intValue();
            LineItem lineItem = (LineItem) totalLineItem[uid];
            if (lineItem.isOnlyDiscount())
                continue;
            plu = PLU.queryByItemNumber(lineItem.getItemNumber());
            String key = mmID + lineItem.getItemNumber();
            MMGroup mmGroup = null;
            if (mmGroupCacheMap.containsKey(key))
                mmGroup = (MMGroup)mmGroupCacheMap.get(key);
            else
                mmGroup = (MMGroup)MMGroup.queryByIDAndItemNo(mmID, lineItem.getItemNumber());

            mmPrice = mmGroup.getDiscountPrice();
            unitPrice = getPluPrice(plu);
            subAmt = ((unitPrice.compareTo(mmPrice)) > 0) ? (unitPrice
                    .subtract(mmPrice)) : (new HYIDouble(0));
            fieldName = plu.getTaxType();
            setTaxMM(fieldName, subAmt);

            if (lineItem.getDiscountType() != null
                    && lineItem.getDiscountType().equalsIgnoreCase("O")
                    && lineItem.getOriginalAfterDiscountAmount() != null) {
                lineItem.setAfterDiscountAmount(lineItem
                        .getOriginalAfterDiscountAmount());
                lineItem.setTempDiscountType("M");
                lineItem.setTempDiscountNumber(mixAndMatchID);
                lineItem.setYellowAmount(null);
            }

            lineItem.setTempAfterDiscountAmount(lineItem
                    .getTempAfterDiscountAmount().subtract(
                            subAmt.multiply(new HYIDouble(quanty))));

            // 计算还元金
            if (StateToolkit.needCheckRebate(app.getCurrentTransaction())) {
                HYIDouble rateMM = mmGroup.getRebateRate();
                HYIDouble rateOrig = null;
                if (!rebateCacheMap.containsKey(lineItem.getItemNumber())) {
                    rateOrig = Rebate.getRebateRate(lineItem.getItemNumber());
                    rebateCacheMap.put(lineItem.getItemNumber(), rateOrig);
                } else
                    rateOrig = (HYIDouble) rebateCacheMap.get(lineItem
                            .getItemNumber());

                if (rateMM == null)
                    rateMM = rateOrig;
                lineItem.setRebateRate(rateMM);
                HYIDouble itemRebate = (unitPrice.multiply(lineItem
                    .getQuantity().subtract(new HYIDouble(quanty)))
                    .multiply(rateOrig)).add(mmPrice.multiply(new HYIDouble(quanty))
                        .multiply(rateMM)).setScale(PARAM.getRebateAmountScale(), BigDecimal.ROUND_HALF_UP);
                lineItem.setAddRebateAmount(itemRebate); // 本单品增加还元金
            } else {
                lineItem.setAddRebateAmount(new HYIDouble(0.0));
            }
        }
    }

    /**
     * 折扣金额要指定分摊到指定的组合内单品上 指定还原金比例 mmgroup新增字段 price decimal(10,2) 指定售价
     * rebateRate decimal(10,2) 指定还原金比例
     *
     * @param mmID
     *            组合促销id
     * @param plusArray
     *            匹配的组合商品列表
     * @param amount
     *            折扣金额
     * @param qty
     *            限量
     */
    public void proportinFromMMGroup(String mmID, List plusArray,
            HYIDouble amount, int qty) {
        if (amount.compareTo(new HYIDouble(0)) == 0)
            return;

        LineItem mmLineItem = new LineItem();
        mmLineItem.setPluNumber(mmID);

        mmLineItem.setQuantity(new HYIDouble(qty));
        mmLineItem.setUnitPrice(amount);

        mmLineItem.setItemNumber(mmID);
        mmLineItem.setOriginalPrice(amount);

        mmLineItem.setAmount(amount);
        app.getCurrentTransaction().setMMDetail(mmID, mmLineItem);

        if (plusArray.size() == 0) {
            return;
        }

        HYIDouble unitPrice = new HYIDouble(0);
        HYIDouble mmPrice = new HYIDouble(0);
        HYIDouble subAmt = new HYIDouble(0);
        String fieldName = null;
        // int m = 0;
        Map uidMap = new HashMap();
        for (int i = 0; i < plusArray.size(); i++) {
            Integer uid = new Integer(((PLU) plusArray.get(i)).getUID());
            if (uidMap.containsKey(uid))
                uidMap.put(uid, new Integer(((Integer) uidMap.get(uid))
                        .intValue() + 1));
            else
                uidMap.put(uid, new Integer(1));
        }

        PLU plu = null;
        for (Iterator it = plusArray.iterator(); it.hasNext();) {
            int uid = ((Integer) ((Map) it.next()).get("id")).intValue();
            int quanty = ((HYIDouble) ((Map) it.next()).get("matchCount"))
                    .intValue();
            LineItem lineItem = (LineItem) totalLineItem[uid];
            if (lineItem.isOnlyDiscount())
                continue;
            plu = (PLU) ((Map) it.next()).get("plu");
            String key = mmID + lineItem.getItemNumber();
            MMGroup mmGroup = null;
            if (mmGroupCacheMap.containsKey(key))
                mmGroup = (MMGroup) mmGroupCacheMap.get(key);
            else
                mmGroup = (MMGroup) MMGroup.queryByIDAndItemNo(mmID, lineItem
                        .getItemNumber());

            // mmgroup 指定价
            mmPrice = mmGroup.getDiscountPrice() != null ? mmGroup
                    .getDiscountPrice() : new HYIDouble(0);
            unitPrice = getPluPrice(plu); // 折扣价
            subAmt = ((unitPrice.compareTo(mmPrice)) > 0) ? (unitPrice
                    .subtract(mmPrice)) : (new HYIDouble(0));
            fieldName = plu.getTaxType();
            setTaxMM(fieldName, subAmt);

            if (lineItem.getDiscountType() != null
                    && lineItem.getDiscountType().equalsIgnoreCase("O")
                    && lineItem.getOriginalAfterDiscountAmount() != null) {
                lineItem.setAfterDiscountAmount(lineItem
                        .getOriginalAfterDiscountAmount());
                lineItem.setTempDiscountType("M");
                lineItem.setTempDiscountNumber(mixAndMatchID);
                lineItem.setYellowAmount(null);
            }

            lineItem.setTempAfterDiscountAmount(lineItem
                    .getTempAfterDiscountAmount().subtract(
                            subAmt.multiply(new HYIDouble(quanty))));

            // 计算还元金
            if (StateToolkit.needCheckRebate(app.getCurrentTransaction())) {
                HYIDouble rateMM = mmGroup.getRebateRate();
                HYIDouble rateOrig = null;
                if (!rebateCacheMap.containsKey(lineItem.getItemNumber())) {
                    rateOrig = Rebate.getRebateRate(lineItem.getItemNumber());
                    rebateCacheMap.put(lineItem.getItemNumber(), rateOrig);
                } else
                    rateOrig = (HYIDouble) rebateCacheMap.get(lineItem
                            .getItemNumber());

                if (rateMM == null)
                    rateMM = rateOrig;
                lineItem.setRebateRate(rateMM);
                HYIDouble itemRebate = (unitPrice.multiply(lineItem
                    .getQuantity().subtract(new HYIDouble(quanty)))
                    .multiply(rateOrig)).add(mmPrice.multiply(new HYIDouble(quanty))
                        .multiply(rateMM)).setScale(PARAM.getRebateAmountScale(), BigDecimal.ROUND_HALF_UP);
                lineItem.setAddRebateAmount(itemRebate); // 本单品增加还元金
            } else {
                lineItem.setAddRebateAmount(new HYIDouble(0.0));
            }
        }
    }

    // tax M&M amount in transaction
    public void mmTaxProportion(String mmID, ArrayList plusArray,
            HYIDouble amount, int qty) {

        LineItem mmLineItem = new LineItem();
        mmLineItem.setPluNumber(mmID);

        mmLineItem.setQuantity(new HYIDouble(qty));
        mmLineItem.setUnitPrice(amount);

        /*
         * Meyer/2003-02-21/
         */
        mmLineItem.setItemNumber(mmID);
        mmLineItem.setOriginalPrice(amount);

        mmLineItem.setAmount(amount);
        app.getCurrentTransaction().setMMDetail(mmID, mmLineItem);

        if (plusArray.size() == 0) {
            return;
        }

        HYIDouble unitPriceMax = new HYIDouble(-1);
        PLU pluMax = null;
        for (int i = 0; i < plusArray.size(); i++) {
            // System.out.println("plu uid = " +
            // ((PLU)plusArray.get(i)).getUID());
            if (unitPriceMax.compareTo(((PLU) plusArray.get(i)).getUnitPrice()) == -1) {
                unitPriceMax = ((PLU) plusArray.get(i)).getUnitPrice();
                pluMax = (PLU) plusArray.get(i);
                // System.out.println("max uid = " + pluMax.getUID());
            }
        }
        HYIDouble denominator = getMixAndMatchAmount(plusArray);
        HYIDouble numerator = null;
        HYIDouble modulus = null;
        HYIDouble proportion = null;
        String fieldName = "";
        PLU plu = null;
        HYIDouble tempAmount = new HYIDouble(0);
        // int m = 0;
        boolean state = true;
        for (int i = 0; i < plusArray.size(); i++) {
            plu = (PLU) plusArray.get(i);
            if (plu.equals(pluMax) && plu.getUID() == pluMax.getUID() && state) {
                state = false;
                continue;
            }
            numerator = getPluPrice(plu);

            // Bruce/20030328 advoid divide by zero
            if (denominator.doubleValue() == 0)
                modulus = new HYIDouble(0);
            else
                modulus = numerator.divide(denominator, 2,
                        BigDecimal.ROUND_HALF_UP);

            proportion = (amount.multiply(modulus)).setScale(2,
                    BigDecimal.ROUND_HALF_UP);
            tempAmount = tempAmount.addMe(proportion);
            fieldName = plu.getTaxType();
            setTaxMM(fieldName, proportion);
            LineItem lineItem = (LineItem) totalLineItem[plu.getUID()];
            lineItem.setTempAfterDiscountAmount(lineItem
                    .getTempAfterDiscountAmount().subtract(proportion));
            lineItem.setTempDiscountType("M");
            lineItem.setTempDiscountNumber(mixAndMatchID);
        }
        proportion = amount.subtract(tempAmount);
        fieldName = pluMax.getTaxType();
        setTaxMM(fieldName, proportion);
        LineItem lineItemMax = (LineItem) totalLineItem[pluMax.getUID()];
        lineItemMax.setTempAfterDiscountAmount(lineItemMax
                .getTempAfterDiscountAmount().subtract(proportion));
        lineItemMax.setTempDiscountType("M");
        lineItemMax.setTempDiscountNumber(mixAndMatchID);
    }

    // tax M&M amount in transaction
    public void mmTaxProportionV3(String mmID, List plusArray,
            HYIDouble amount, int qty) {

        LineItem mmLineItem = new LineItem();
        mmLineItem.setPluNumber(mmID);

        mmLineItem.setQuantity(new HYIDouble(qty));
        mmLineItem.setUnitPrice(amount);

        /*
         * Meyer/2003-02-21/
         */
        mmLineItem.setItemNumber(mmID);
        mmLineItem.setOriginalPrice(amount);

        mmLineItem.setAmount(amount);
        app.getCurrentTransaction().setMMDetail(mmID, mmLineItem);

        if (plusArray.size() == 0) {
            return;
        }

        HYIDouble denominator = this.getMatchedPriceAmount(plusArray);
        HYIDouble numerator = null;
        HYIDouble modulus = null;
        HYIDouble proportion = null;
        String fieldName = "";
        PLU plu = null;
        HYIDouble tempAmount = new HYIDouble(0);
        // 获得最大金额的lineItem的序号，留在最后做
        int maxID = 0;
        HYIDouble maxPrice = new HYIDouble(0);
        Map priceMap = new HashMap();
        for (int i = 0; i < plusArray.size(); i++) {
            Map data = (Map) plusArray.get(i);
            plu = (PLU) data.get("plu");
            HYIDouble price = getPluPrice(plu).multiply(
                    (HYIDouble) data.get("matchCount"));
            priceMap.put(new Integer(i), price);
            if (maxPrice.compareTo(price) < 0) {
                maxPrice = price;
                maxID = i;
            }
        }

        for (int i = 0; i < plusArray.size(); i++) {
            if (maxID == i)
                continue;
            plu = (PLU) ((Map) plusArray.get(i)).get("plu");

            numerator = (HYIDouble) priceMap.get(new Integer(i));

            // Bruce/20030328 advoid divide by zero
            if (denominator.doubleValue() == 0)
                modulus = new HYIDouble(0);
            else
                modulus = numerator.divide(denominator, 2,
                        BigDecimal.ROUND_HALF_UP);

            proportion = (amount.multiply(modulus)).setScale(2,
                    BigDecimal.ROUND_HALF_UP);
            tempAmount = tempAmount.addMe(proportion);
            fieldName = plu.getTaxType();
            setTaxMM(fieldName, proportion);
            LineItem lineItem = (LineItem) totalLineItem[((Integer) ((Map) plusArray
                    .get(i)).get("id")).intValue()];
            System.out.println("------------ 111 : "
                    + lineItem.getTempAfterDiscountAmount());
            lineItem.setTempAfterDiscountAmount(lineItem
                    .getTempAfterDiscountAmount().subtract(proportion));
            System.out.println("------------ 222 : "
                    + lineItem.getTempAfterDiscountAmount());

            lineItem.setTempDiscountType("M");
            lineItem.setTempDiscountNumber(mmID);
        }

        proportion = amount.subtract(tempAmount);
        plu = (PLU) ((Map) plusArray.get(maxID)).get("plu");
        fieldName = plu.getTaxType();
        setTaxMM(fieldName, proportion);
        LineItem lineItemMax = (LineItem) totalLineItem[((Integer) ((Map) plusArray
                .get(maxID)).get("id")).intValue()];
        lineItemMax.setTempAfterDiscountAmount(lineItemMax
                .getTempAfterDiscountAmount().subtract(proportion));
        lineItemMax.setTempDiscountType("M");
        lineItemMax.setTempDiscountNumber(mmID);
    }

    // tax M&M amount in transaction
    public HYIDouble mmTaxProportionType3(MixAndMatch mm, List plusArray,
            HYIDouble amount, int qty) {
        if (plusArray.size() == 0) {
            return amount;
        }
        String mmID = mm.getID();
        int mmGrp1Qty = mm.getGroup1Quantity().intValue();
        int grp1Qty = getGroupQty(plusArray).intValue();
        if (grp1Qty < mmGrp1Qty)
            return amount;
        List matchList = matchGrpQty(plusArray, new HYIDouble(mmGrp1Qty), 1);
        HYIDouble mmDisAmount = mm.getDiscountAmount();
        if (mm.getPriceType().equals("1")) {
            // PriceType = "1", means "售价"
            HYIDouble discountAmount = mmDisAmount;
            System.out.println("(MM)                discount amount = "
                    + discountAmount);
            amount = getMatchedPriceAmount(matchList).subtract(discountAmount);
            System.out.println("(MM)                get amount = " + amount);
        } else if (mm.getPriceType().equals("2")) {
            HYIDouble discountAmount = mmDisAmount;
            System.out.println("(MM)                discount amount = "
                    + discountAmount);
            amount = discountAmount;
        } else if (mm.getPriceType().equals("3")) {
            // PriceType = "3", means "折扣（%）"
            // Bruce/20030401 折扣只算到小数位数1位
            amount = getMatchedPriceAmount(matchList).multiply(mmDisAmount)
                    .setScale(1, BigDecimal.ROUND_HALF_UP);
        } else if (mm.getPriceType().equals("4")) {
            // PriceType = "4", means "优惠价" 只对group1有效
            // get MixAndMatched Amount
            amount = getMatchedPriceAmount(matchList).subtract(mmDisAmount);
        }

        LineItem mmLineItem = new LineItem();
        mmLineItem.setPluNumber(mmID);

        mmLineItem.setQuantity(new HYIDouble(qty));
        mmLineItem.setUnitPrice(amount);

        /*
         * Meyer/2003-02-21/
         */
        mmLineItem.setItemNumber(mmID);
        mmLineItem.setOriginalPrice(amount);

        mmLineItem.setAmount(amount);
        app.getCurrentTransaction().setMMDetail(mmID, mmLineItem);

        HYIDouble denominator = this.getMatchedPriceAmount(matchList);
        HYIDouble numerator = null;
        HYIDouble modulus = null;
        HYIDouble proportion = null;
        String fieldName = "";
        PLU plu = null;
        HYIDouble tempAmount = new HYIDouble(0);
        // 获得最大金额的lineItem的序号，留在最后做
        int maxID = 0;
        HYIDouble maxPrice = new HYIDouble(0);
        Map priceMap = new HashMap();
        for (int i = 0; i < matchList.size(); i++) {
            Map data = (Map) matchList.get(i);
            plu = (PLU) data.get("plu");
            HYIDouble price = getPluPrice(plu).multiply(
                    (HYIDouble) data.get("matchCount"));
            priceMap.put(new Integer(i), price);
            if (maxPrice.compareTo(price) < 0) {
                maxPrice = price;
                maxID = i;
            }
        }

        for (int i = 0; i < matchList.size(); i++) {
            if (maxID == i)
                continue;
            plu = (PLU) ((Map) matchList.get(i)).get("plu");

            numerator = (HYIDouble) priceMap.get(new Integer(i));

            // Bruce/20030328 advoid divide by zero
            if (denominator.doubleValue() == 0)
                modulus = new HYIDouble(0);
            else
                modulus = numerator.divide(denominator, 2,
                        BigDecimal.ROUND_HALF_UP);

            proportion = (amount.multiply(modulus)).setScale(2,
                    BigDecimal.ROUND_HALF_UP);
            tempAmount = tempAmount.addMe(proportion);
            fieldName = plu.getTaxType();
            setTaxMM(fieldName, proportion);
            LineItem lineItem = (LineItem) totalLineItem[((Integer) ((Map) matchList
                    .get(i)).get("id")).intValue()];
            lineItem.setTempAfterDiscountAmount(lineItem
                    .getTempAfterDiscountAmount().subtract(proportion));

            lineItem.setTempDiscountType("M");
            lineItem.setTempDiscountNumber(mixAndMatchID);
        }

        proportion = amount.subtract(tempAmount);
        plu = (PLU) ((Map) matchList.get(maxID)).get("plu");
        fieldName = plu.getTaxType();
        setTaxMM(fieldName, proportion);
        LineItem lineItemMax = (LineItem) totalLineItem[((Integer) ((Map) matchList
                .get(maxID)).get("id")).intValue()];
        lineItemMax.setTempAfterDiscountAmount(lineItemMax
                .getTempAfterDiscountAmount().subtract(proportion));
        lineItemMax.setTempDiscountType("M");
        lineItemMax.setTempDiscountNumber(mmID);
        return amount;
    }

    public ArrayList getLinkedArray(MixAndMatch curMM, ArrayList curArray) {
        // ArrayList mmedArray = new ArrayList();
        ArrayList linkedArray = new ArrayList();
        String linkedMMID = (String) curMM.getLinkedMixandMatch();
        for (int i = 0; i < totalPLU.size(); i++) {
            curPLU = (PLU) totalPLU.get(i);
            if (((String) getMMGroupID(curPLU, mixAndMatchID))
                    .equals(linkedMMID)) {
                linkedArray.add(curPLU);
            }
        }
        if (linkedArray.size() <= curArray.size()) {
            return linkedArray;
        } else {
            return getUnitMMedPLU(new ArrayList(), linkedArray, curArray.size()
                    - linkedArray.size());
        }
    }

    public int getPackQty(MixAndMatch curMM, ArrayList curArray) {
        int i = curArray.size();
        int seq = getMaxPackSeq(curMM);
        // String qtyField = "";
        ArrayList mmedArray = new ArrayList();
        int qty;
        for (int j = seq; j > 0; j--) {
            // qtyField = "PACK" + j + "QTY";
            qty = ((Integer) curMM.getFieldValue("PACK" + j + "QTY"))
                    .intValue();
            if (i >= qty) {
                mmedArray = getUnitMMedPLU(mmedArray, curArray, qty);
                this.mixAndMatchedArray = mmedArray;
                return j;
            }
        }
        return 0;
    }

    // get max number of pack meet request
    public int getMaxPackSeq(MixAndMatch curMM) {
        int i = 0;
        String qtyField = "";
        do {
            i = i + 1;
            qtyField = "PACK" + i + "QTY";
        } while (curMM.getFieldValue(qtyField) != null
                && ((Integer) curMM.getFieldValue(qtyField)).intValue() != 0);
        return i - 1;
    }

    /**
     * for cankun 从mmgroup中获得指定售价 pluAmount yCardDiscountAmount
     *
     * @param plusArray
     * @return 组合促销折扣价 == 0 不促销
     */
    public HYIDouble getPrices(ArrayList plusArray, String mixAndMatchID) {
        HYIDouble mmAmount = new HYIDouble(0); // 组合促销
        HYIDouble mmAmountR = new HYIDouble(0);
        HYIDouble pluAmount = new HYIDouble(0); // 特价
        HYIDouble pluAmountR = new HYIDouble(0);
        HYIDouble discountAmount = new HYIDouble(0); // 折后价
        PLU plu = null;
        List useredList = new ArrayList();
        for (int j = 0; j < plusArray.size(); j++) {
            plu = (PLU) plusArray.get(j);
            if (plu.isWeightedPlu()) {
                mmAmount = mmAmount
                        .addMe((HYIDouble) scalePluCacheMap.get(plu));
                pluAmount = pluAmount.addMe((HYIDouble) scalePluCacheMap
                        .get(plu));
            } else {
                String key = mixAndMatchID + plu.getItemNumber();
                MMGroup mmGroup = null;
                if (mmGroupCacheMap.containsKey(key))
                    mmGroup = (MMGroup) mmGroupCacheMap.get(key);
                else {
                    mmGroup = (MMGroup) MMGroup.queryByIDAndItemNo(
                            mixAndMatchID, plu.getItemNumber());
                    mmGroupCacheMap.put(key, mmGroup);
                }
                HYIDouble mmPrice = new HYIDouble(0);
                if (mmGroup.getDiscountPrice() == null)
                    mmPrice = getPluPrice(plu);
                else
                    mmPrice = mmGroup.getDiscountPrice();
                mmAmount = mmAmount.addMe(mmPrice);
                if (StateToolkit.needCheckRebate(app.getCurrentTransaction())) {
                    if (mmGroup.getRebateRate() == null) {
                        mmPrice = mmPrice.multiply((new HYIDouble(1))
                                .subtract(Rebate.getRebateRate(plu
                                        .getItemNumber())));
                    } else {
                        mmPrice = mmPrice.multiply((new HYIDouble(1))
                                .subtract(mmGroup.getRebateRate()));
                    }
                }
                mmAmountR = mmAmountR.addMe(mmPrice);

                pluAmount = pluAmount.addMe(getPluPrice(plu));
                if (StateToolkit.needCheckRebate(app.getCurrentTransaction())) {
                    pluAmountR = pluAmountR.addMe(getPluPrice(plu).multiply(
                            (new HYIDouble(1)).subtract(Rebate
                                    .getRebateRate(plu.getItemNumber()))));
                } else {
                    pluAmountR = pluAmountR.addMe(getPluPrice(plu));
                }
            }
            useredList.add(new Integer(plu.getUID()));
        }

        for (Iterator it = useredList.iterator(); it.hasNext();) {
            int id = ((Integer) it.next()).intValue();
            LineItem item = (LineItem) totalLineItem[id];
            discountAmount = discountAmount.addMe(item.getAmount());
        }

        HYIDouble amt = new HYIDouble(0);
        // 如果 特价 > 组合促销 则 促销 否则 不促销
        // 如果 折后价 > 组合促销 则 促销 否则 不促销
        amt = ((pluAmountR.subtract(mmAmountR).compareTo(new HYIDouble(0))) > 0) ? pluAmount
                .subtract(mmAmount)
                : new HYIDouble(0);
        amt = ((discountAmount.subtract(mmAmountR).compareTo(new HYIDouble(0))) > 0) ? amt
                : new HYIDouble(0);
        System.out.println("----- pluAmount : " + pluAmountR
                + " | discountAmount : " + discountAmount + " | mmAmount : "
                + mmAmountR + " | returnAmt : " + amt);

        if (amt.compareTo(new HYIDouble(0)) > 0) {
            for (int j = 0; j < plusArray.size(); j++) {
                plu = (PLU) plusArray.get(j);
                TaxType taxType = TaxType.queryByTaxID(plu.getTaxType());
                if (taxType != null) {
                    String taxTp = taxType.getType();
                    HYIDouble pluPrice = getPluPrice(plu);

                    HYIDouble taxPercent = taxType.getPercent();
                    LineItem item = (LineItem) totalLineItem[plu.getUID()];
                    if (taxTp.equalsIgnoreCase("2")) { //这里为何要对外含税 做特殊处理 ？
                        HYIDouble unit = new HYIDouble(1);
                        item.setUnitPrice(pluPrice.multiply(unit
                                .addMe(taxPercent)));
                    } else {
                        item.setUnitPrice(pluPrice);
                    }
                    item.setTempAfterDiscountAmount(new HYIDouble(0));
                    item.setTempDiscountNumber(null);
                    item.setTempDiscountType(null);
                }
            }
        } else {
            for (int j = 0; j < plusArray.size(); j++) {
                plu = (PLU) plusArray.get(j);
                LineItem item = (LineItem) totalLineItem[plu.getUID()];
                item.setOnlyDiscount(true);
            }
        }
        return amt;
    }

    /**
     * for cankun 从mmgroup中获得指定售价 pluAmount yCardDiscountAmount
     *
     * @param plusArray
     * @return 组合促销折扣价 == 0 不促销
     */
    public HYIDouble getPricesFromMMGroup(List plusArray, String mixAndMatchID) {
        HYIDouble mmAmount = new HYIDouble(0); // 组合促销
        HYIDouble mmAmountR = new HYIDouble(0);
        HYIDouble pluAmount = new HYIDouble(0); // 特价
        HYIDouble pluAmountR = new HYIDouble(0);
        HYIDouble discountAmount = new HYIDouble(0); // 折后价
        PLU plu = null;
        for (int i = 0; i < plusArray.size(); i++) {
            plu = (PLU) ((Map) plusArray.get(i)).get("plu");
            if (plu.isWeightedPlu()) {
                mmAmount = mmAmount
                        .addMe((HYIDouble) scalePluCacheMap.get(plu));
                pluAmount = pluAmount.addMe((HYIDouble) scalePluCacheMap
                        .get(plu));
            } else {
                String key = mixAndMatchID + plu.getItemNumber();
                MMGroup mmGroup = null;
                if (mmGroupCacheMap.containsKey(key))
                    mmGroup = (MMGroup) mmGroupCacheMap.get(key);
                else {
                    mmGroup = (MMGroup) MMGroup.queryByIDAndItemNo(
                            mixAndMatchID, plu.getItemNumber());
                    mmGroupCacheMap.put(key, mmGroup);
                }
                HYIDouble mmPrice = new HYIDouble(0);
                if (mmGroup.getDiscountPrice() == null)
                    mmPrice = getPluPrice(plu);
                else
                    mmPrice = mmGroup.getDiscountPrice();
                // mmPriceR 减掉还原金后的总价格，用于对比
                // mmPrice 实际总价格
                mmPrice = mmPrice.multiply((HYIDouble) ((Map) plusArray.get(i))
                        .get("matchCount"));
                mmAmount = mmAmount.addMe(mmPrice);
                pluAmount = pluAmount
                        .addMe(getPluPrice(plu).multiply(
                                (HYIDouble) ((Map) plusArray.get(i))
                                        .get("matchCount")));

                if (StateToolkit.needCheckRebate(app.getCurrentTransaction())) {

                    HYIDouble r = (new HYIDouble(1)).subtract(Rebate
                            .getRebateRate(plu.getItemNumber()));
                    pluAmountR = pluAmountR.addMe(pluAmount.multiply(r));
                    if (mmGroup.getRebateRate() == null) {
                        mmPrice = mmPrice.multiply(r);
                    } else {
                        mmPrice = mmPrice.multiply((new HYIDouble(1))
                                .subtract(mmGroup.getRebateRate()));
                    }
                    // mmPrice 减掉还原金后的价格，用于对比
                } else {
                    pluAmountR = pluAmountR.addMe(pluAmount);
                }
                mmAmountR = mmAmountR.addMe(mmPrice);
            }
            int id = ((Integer) ((Map) plusArray.get(i)).get("id")).intValue();
            LineItem item = (LineItem) totalLineItem[id];
            discountAmount = discountAmount.addMe(item.getAmount());
        }

        HYIDouble amt = new HYIDouble(0);
        // 如果 特价 > 组合促销 则 促销 否则 不促销
        // 如果 折后价 > 组合促销 则 促销 否则 不促销
        amt = ((pluAmountR.subtract(mmAmountR).compareTo(new HYIDouble(0))) > 0) ? pluAmount
                .subtract(mmAmount)
                : new HYIDouble(0);
        amt = ((discountAmount.subtract(mmAmountR).compareTo(new HYIDouble(0))) > 0) ? amt
                : new HYIDouble(0);
        System.out.println("----- pluAmount : " + pluAmountR
                + " | discountAmount : " + discountAmount + " | mmAmount : "
                + mmAmountR + " | returnAmt : " + amt);

        if (amt.compareTo(new HYIDouble(0)) > 0) {
            for (int i = 0; i < plusArray.size(); i++) {
                plu = (PLU) ((Map) plusArray.get(i)).get("plu");
                TaxType taxType = TaxType.queryByTaxID(plu.getTaxType());
                if (taxType != null) {
                    String taxTp = taxType.getType();
                    HYIDouble pluPrice = getPluPrice(plu);

                    HYIDouble taxPercent = taxType.getPercent();
                    int id = ((Integer) ((Map) plusArray.get(i)).get("id"))
                            .intValue();
                    LineItem item = (LineItem) totalLineItem[id];
                    // 对外含税 做特殊处理, 但是还未处理税金
                    if (taxTp.equalsIgnoreCase("2")) {
                        HYIDouble unit = new HYIDouble(1);
                        item.setUnitPrice(pluPrice.multiply(unit
                                .addMe(taxPercent)));
                    } else {
                        item.setUnitPrice(pluPrice);
                    }
                    item.setTempAfterDiscountAmount(new HYIDouble(0));
                    item.setTempDiscountNumber(null);
                    item.setTempDiscountType(null);
                }
            }
        } else {
            for (int i = 0; i < plusArray.size(); i++) {
                plu = (PLU) ((Map) plusArray.get(i)).get("plu");
                int id = ((Integer) ((Map) plusArray.get(i)).get("id"))
                        .intValue();
                LineItem item = (LineItem) totalLineItem[id];
                item.setOnlyDiscount(true);
            }
        }
        return amt;
    }

    public HYIDouble getMixAndMatchAmount(ArrayList plusArray) {
        HYIDouble pluAmount = new HYIDouble(0);
        for (int j = 0; j < plusArray.size(); j++) {
            curPLU = (PLU) plusArray.get(j);
            // CreamToolkit
            // .logMessage("MixAndMatchState getMixAndMatchAmount : curPLU = "
            // + curPLU);

            if (curPLU.isWeightedPlu())
                pluAmount = pluAmount.addMe((HYIDouble) scalePluCacheMap
                        .get(curPLU));
            else
                pluAmount = pluAmount.addMe(getPluPrice(curPLU));
        }
        return pluAmount;
    }

    public HYIDouble getPluPrice(PLU plu) {

        if (plu.isWeightedPlu()) {
            return (HYIDouble) scalePluCacheMap.get(plu);
        }

        HYIDouble pluPrice = new HYIDouble(0);
        if (pluPriceCacheMap.containsKey(plu.getPluNumber())) {
            Object[] tmp = (Object[]) pluPriceCacheMap.get(plu.getPluNumber());
            pluPrice = (HYIDouble) tmp[0];
        } else {
            Object[] tmp = (Object[]) plu.getSalingPriceDetail();
            pluPriceCacheMap.put(plu.getPluNumber(), tmp);
            pluPrice = (HYIDouble) tmp[0];
        }
        return pluPrice;
    }

    public PLU getPLU(LineItem lineItem) {
        return PLU.queryBarCode(lineItem.getPluNumber());
    }

    public ArrayList getUnitMMedPLU(ArrayList mmedArray, List curArray,
            int quantity) {
        if (GetProperty.getMixAndMatchQuick().equalsIgnoreCase("no")) {
            HYIDouble expensiveUnitPrice = new HYIDouble(0);
            PLU expensivePLU = (PLU) curArray.get(0);
            PLU plu = null;
            for (int j = 0; j < quantity; j++) {
                for (int i = 0; i < curArray.size(); i++) {
                    expensivePLU = (PLU) curArray.get(i);
                    plu = (PLU) curArray.get(i);
                    if (expensiveUnitPrice.compareTo(plu.getUnitPrice()) == -1) {
                        expensiveUnitPrice = plu.getUnitPrice();
                        expensivePLU = plu;
                    }
                }
                mmedArray.add(expensivePLU);
                curArray.remove(expensivePLU);
                totalPLU.remove(expensivePLU);
                expensiveUnitPrice = new HYIDouble(0);
                expensivePLU = null;
            }
        } else {
            System.out.println("--- curArray.size b : " + curArray.size());
            for (int i = 0; i < quantity; i++) {
                mmedArray.add(curArray.get(0));
                totalPLU.remove(curArray.get(0));
                curArray.remove(0);
            }
            System.out.println("--- curArray.size a : " + curArray.size());
        }
        return mmedArray;
    }

    public ArrayList getMMedPLU(ArrayList mmedArray, MixAndMatch mm) {
        // Group1Quantity:
        int grp1Qty = mm.getGroup1Quantity().intValue();
        mmedArray = getUnitMMedPLU(mmedArray, group1Array, grp1Qty);
        // Group2Quantity
        if (mm.getGroup2Quantity().intValue() == 0) {
            return mmedArray;
        }
        int grp2Qty = mm.getGroup2Quantity().intValue();
        mmedArray = getUnitMMedPLU(mmedArray, group2Array, grp2Qty);
        // Group3Quantity
        if (mm.getGroup3Quantity().intValue() == 0) {
            return mmedArray;
        }
        int grp3Qty = mm.getGroup3Quantity().intValue();
        mmedArray = getUnitMMedPLU(mmedArray, group3Array, grp3Qty);

        // Group4Quantity
        if (mm.getGroup4Quantity().intValue() == 0) {
            return mmedArray;
        }
        int grp4Qty = mm.getGroup4Quantity().intValue();
        mmedArray = getUnitMMedPLU(mmedArray, group4Array, grp4Qty);

        // Group5Quantity
        if (mm.getGroup5Quantity().intValue() == 0) {
            return mmedArray;
        }
        int grp5Qty = mm.getGroup5Quantity().intValue();
        mmedArray = getUnitMMedPLU(mmedArray, group5Array, grp5Qty);

        // Group6Quantity
        if (mm.getGroup6Quantity().intValue() == 0) {
            return mmedArray;
        }
        int grp6Qty = mm.getGroup6Quantity().intValue();
        mmedArray = getUnitMMedPLU(mmedArray, group6Array, grp6Qty);
        return mmedArray;
    }

    public String getMixAndMatchID(PLU plu) {
        String mmid = "000";
        Iterator it = MMGroup.queryByITEMNO(plu.getInStoreCode());
        if (it == null)
            return null;
        while (it.hasNext()) {
            MMGroup tmp = (MMGroup) it.next();
            String tmpid = tmp.getID();
            if (tmpid.compareTo(mmid) > 0)
                mmid = tmpid;
        }
        return mmid;
    }

    private boolean compMixAndMatchID(String mmID, PLU plu) {
        String key = mmID + plu.getInStoreCode();
        if (mmGroupCacheMap.containsKey(key))
            return true;
        CreamToolkit.logMessage("compMixAndMatchID not use cache");
        MMGroup mm = (MMGroup) MMGroup.queryByIDAndItemNo(mmID, plu
                .getInStoreCode());
        if (mm != null)
            return true;
        return false;
    }

    private String getMMGroupID(PLU plu, String mmID) {
        String key = mmID + plu.getInStoreCode();
        if (mmGroupCacheMap.containsKey(key))
            return ((MMGroup) mmGroupCacheMap.get(key)).getGROUPNUM();
        String grpid = null;
        System.out.println("plu: " + plu.getInStoreCode() + " mmID : " + mmID);
        MMGroup mm = (MMGroup) MMGroup.queryByIDAndItemNo(mmID, plu
                .getInStoreCode());
        if (mm.getID().equalsIgnoreCase(mmID)) {
            grpid = mm.getGROUPNUM();
        }
        return grpid;
    }

    public boolean checkGrpQty(MixAndMatch mm) {
        if (mm.getGroup1Quantity().intValue() == 0) {
            return false;
        } else if (mm.getGroup1Quantity().intValue() > group1Array.size()) {
            return false;
        }
        if (mm.getGroup2Quantity().intValue() == 0) {
            return true;
        } else if (mm.getGroup2Quantity().intValue() > group2Array.size()) {
            return false;
        }
        if (mm.getGroup3Quantity().intValue() == 0) {
            return true;
        } else if (mm.getGroup3Quantity().intValue() > group3Array.size()) {
            return false;
        }
        if (mm.getGroup4Quantity().intValue() == 0) {
            return true;
        } else if (mm.getGroup4Quantity().intValue() > group4Array.size()) {
            return false;
        }
        if (mm.getGroup5Quantity().intValue() == 0) {
            return true;
        } else if (mm.getGroup5Quantity().intValue() > group5Array.size()) {
            return false;
        }
        if (mm.getGroup6Quantity().intValue() == 0) {
            return true;
        } else if (mm.getGroup6Quantity().intValue() > group6Array.size()) {
            return false;
        }
        return true;
    }

    public static MixAndMatch validMM(String mixAndMatchID) {
        MixAndMatch mixAndMatch = MixAndMatch.queryByID(mixAndMatchID);
        if (mixAndMatch == null)
            return null;
        System.out
                .println("(MM)     query MixAndMatch from plu's MixAndMatchID : OK");

        if (!CreamToolkit.isInterzone(new Date(), mixAndMatch.getBeginDate(),
                mixAndMatch.getEndDate(), mixAndMatch.getBeginTime(),
                mixAndMatch.getEndTime()))
            return null;

        // check day of week
        int dayOfWeek = (new Date()).getDay();
        String weekCycle = mixAndMatch.getWeekCycle();
        if (weekCycle == null) {
            weekCycle = "1111111";
        } else if (weekCycle.length() < 7) {
            for (int m = weekCycle.length(); m < 8; m++) {
                weekCycle = weekCycle + "1";
            }
        }
        if (weekCycle.charAt(dayOfWeek) == '0') {
            return null;
        }
        return mixAndMatch;
    }

    public void clearCache() {
        mmGroupCacheMap.clear();
        // mmCacheMap.clear();
        scalePluCacheMap.clear();
        rebateCacheMap.clear();
        pluPriceCacheMap.clear();

//        combPromotionHeaderCache.clear();
        if (combSaleDetailCache.size() > 200)
            combSaleDetailCache.clear();
    }

    /**
     * Meyer/2003-01-20 Add MixAndMatch Version 2
     *
     */
    private void mixAndMatch2() {
        NormalCombPromotion ncp = new NormalCombPromotion(app.getCurrentTransaction()//);
                , combSaleDetailCache);
        ncp.match(app.getCurrentTransaction());
    }
}
