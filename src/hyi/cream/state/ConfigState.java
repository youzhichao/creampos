package hyi.cream.state;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.PopupMenuListener;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.ConfReader;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.Keylock;

import java.io.File;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class ConfigState extends State implements PopupMenuListener {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private PopupMenuPane p = app.getPopupMenuPane();
	private static ArrayList menu = new ArrayList();
	private static ArrayList states = new ArrayList();
	private static ArrayList properties = new ArrayList();
	private int selectItem = 0;
	static ConfigState configState = null;

	public static ConfigState getInstance() {
		try {
			if (configState == null) {
				configState = new ConfigState();
			}
		} catch (InstantiationException ex) {
		}
		return configState;
	}

	/**
	 * Constructor
	 */
	public ConfigState() throws InstantiationException {
	}

	static {
		File propFile = CreamToolkit.getConfigurationFile(ConfigState.class);
		try {
			//FileInputStream filein = new FileInputStream(propFile);
			//InputStreamReader inst = null;
			//inst = new InputStreamReader(filein, GetProperty.getConfFileLocale());
			//BufferedReader in = new BufferedReader(inst);
            ConfReader in = new ConfReader(propFile);

			String line = null;
			int i;
			char ch;

			do {
				line = in.readLine();
				while (line.equals("")) {
					line = in.readLine();
				}
				i = 0;
				do {
					ch = line.charAt(i);
					i++;
				} while ((ch == ' ' || ch == '\t') && i < line.length());
			} while (ch == '#' || ch == ' ' || ch == '\t');

			StringTokenizer t = null;
			while (line != null) {
				t = new StringTokenizer(line, ",");
				menu.add(t.nextToken());
				if (t.hasMoreElements()) {
					states.add(t.nextToken());
				} else {
					states.add("");
				}
				if (t.hasMoreElements()) {
					properties.add(t.nextToken());
				} else {
					properties.add("");
				}
				line = in.readLine();
			}
            in.close();
        } catch (NoSuchElementException e) {
            CreamToolkit.logMessage("Format error: " + propFile);
        }
	}

	public void entry(EventObject event, State sourceState) {
		// System.out.println("ConfigState entry");

		app.setKeyPosition(6);

		p.setMenu(menu);
		p.setVisible(true);
		p.setPopupMenuListener(this);
		p.clear();

		app.getMessageIndicator().setMessage(
				CreamToolkit.GetResource().getString("InputSelect"));
	}

	public Class exit(EventObject event, State sinkState) {
		app.getWarningIndicator().setMessage("");
		if (event.getSource() instanceof SelectButton) {
			String stateName = (String) states.get(selectItem);
			if (stateName.equalsIgnoreCase("halt")) {
				// Runtime currentApp = Runtime.getRuntime();
				// try {
				// Process c = currentApp.exec("halt");
				// //c.waitFor();
				// //result = c.exitValue();
				// } catch (Exception e) {
				// e.printStackTrace(CreamToolkit.getLogger());
				// System.out.println(e);
				// }
				return ShutdownState.class;

			} else if (stateName.equalsIgnoreCase("recoverDBFromBackUp")) {
				try {
//					boolean windows = (System.getProperties().get("os.name")
//							.toString().indexOf("Windows") != -1);
//					if (!windows)
//						hyi.cream.dac.Trigger.getInstance().recoverFromBackup();

					return ConfigState.class;

				} catch (Exception e) {
					e.printStackTrace(CreamToolkit.getLogger());
					System.out.println(e);
					return ConfigState.class;
				}
			} else {
				try {
					Class c = Class.forName("hyi.cream.state." + stateName);
					return c;
				} catch (ClassNotFoundException e) {
					System.out.println(e);
					return ConfigState.class;
				}
			}
		}

		if (event.getSource() instanceof ClearButton) {
			// app.getPopupMenuPane().setVisible(false);
			return ConfigState.class;
		}

		if (event.getSource() instanceof Keylock) {
			Keylock k = (Keylock) event.getSource();
			try {
				int kp = k.getKeyPosition();
				app.setKeyPosition(kp);
				Class exitState = CreamToolkit.getSinkStateFromKeyLockCode(kp);
				if (exitState != null) {
					if (exitState.equals(InitialState.class))
						p.setVisible(false);
					return exitState;
				} else {
					return ConfigState.class;
				}
			} catch (JposException e) {
				System.out.println(e);
			}
			/*
			 * Keylock k = (Keylock)event.getSource(); try { int kp =
			 * k.getKeyPosition(); if (kp == 2) { app.setKeyPosition(kp);
			 * app.getPopupMenuPane().setVisible(false); return
			 * InitialState.class; } else if (kp == 6) { app.setKeyPosition(kp);
			 * return ConfigState.class; } } catch (JposException e) { }
			 */
		}

		return (sinkState == null) ? null : sinkState.getClass();
	}

	public void menuItemSelected() {
		if (p.getSelectedMode()) {
			selectItem = p.getSelectedNumber();
			POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
			POSButtonHome2.getInstance().buttonPressed(e);
		} else {
			POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
			POSButtonHome2.getInstance().buttonPressed(e);
		}
	}

	public String getProperty() {
		return (String) properties.get(selectItem);
	}
}
