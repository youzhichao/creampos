package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;
import java.math.*;

/**
 * A Class class.
 * <P>
 * 
 * @author allan
 */
public class YellowAmountState extends State {

    // Singleton ---------------------------------------------------------------------------
    private static YellowAmountState instance = new YellowAmountState();
    private YellowAmountState() {}
        //computeDiscountRate = GetProperty.getComputeDiscountRate("no").equalsIgnoreCase("yes");
    public static YellowAmountState getInstance() { return instance; }
    //--------------------------------------------------------------------------------------

	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private String tempPrice = "";
	static YellowAmountState yellowAmountState = null;
	private LineItem curLineItem = null;
//	private boolean computeDiscountRate;
	private PLU plu;
	private boolean denyYellowState = false;

	public void entry(EventObject event, State sourceState) {
		denyYellowState = false;
		Transaction tran = app.getCurrentTransaction();
		curLineItem = tran.getCurrentLineItem();
		plu = PLU.queryByItemNumber(curLineItem.getItemNumber());
		if (plu.getMinPrice() == null 
				|| plu.getMinPrice().compareTo(new HYIDouble(0)) == 0 
				|| plu.getYellowLimit() == null
				|| plu.getYellowLimit().compareTo(new HYIDouble(0)) == 0) 
		{
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString(
							"PleaseContinueOverrideState"));
			denyYellowState = true;
			return;
		}

		if (sourceState instanceof CashierRightsCheckState)
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString(
							"PleaseInputYellowPriceAndEnterOver"));
	}

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof ClearButton) {
            if (tempPrice.length() == 0)
                return IdleState.class;
            tempPrice = "";
            app.getMessageIndicator().setMessage(tempPrice);
            return YellowAmountState.class;
        }
		
		if (denyYellowState)
			return OverrideAmountState.class;
		
		if (event.getSource() instanceof NumberButton) {
			NumberButton pb = (NumberButton)event.getSource();
                // System.out.println(" OverrideAmountState exit number" +
				// pb.getNumberLabel());
			if (pb.getNumberLabel().equals("-") || (pb.getNumberLabel().equals(".") && tempPrice.length() == 0)) {
                // System.out.println(" OverrideAmountState exit number 1 "
				// +tempPrice);
			} else { 
				tempPrice = tempPrice + pb.getNumberLabel();
                // System.out.println(" OverrideAmountState exit number 2"
				// +tempPrice);
			}
			app.getMessageIndicator().setMessage(tempPrice);
			return YellowAmountState.class;
		}

		if (event.getSource() instanceof EnterButton) {
            if (tempPrice.trim().length() <= 0 || tempPrice.trim().equals("0"))
                return OverrideAmountState.class;
			
        	HYIDouble minPrice = plu.getMinPrice();
			HYIDouble discountAmount = null;
            // Check number format
            try {
				discountAmount = new HYIDouble(tempPrice);
            } catch (NumberFormatException e) {
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PleaseInputPriceAgain"));
                tempPrice = "";
                return YellowAmountState.class;
            }

            // 20050613 黄卡变价不能低于 plu.minPrice * (1 - yellowLimit) [for cankun]
			HYIDouble maxDiscount = minPrice.multiply(plu.getYellowLimit().divide(
					new HYIDouble(100), 2, BigDecimal.ROUND_HALF_DOWN)).setScale(2, BigDecimal.ROUND_HALF_DOWN);
			if (discountAmount.compareTo(maxDiscount) > 0) 
			{
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputPriceCannotHigher")
						+ ", " + CreamToolkit.GetResource().getString("PleaseInputPriceAgain"));
                tempPrice = "";
                return YellowAmountState.class;
			}
				
			HYIDouble amount = plu.getMinPrice().subtract(discountAmount);
            // may be popup overrideamout reason pane
            if (PARAM.isOverrideAmountReasonPopup()) {
            	(new OverridePopupMenu(curLineItem)).popupMenu();
            }
            
			curLineItem.setUnitPrice(amount);
            // curLineItem.setQuantity(new HYIDouble(1.00));
			curLineItem.setAmount(curLineItem.getUnitPrice().multiply(curLineItem.getQuantity()));
			curLineItem.setOriginalAfterDiscountAmount(curLineItem.getAfterDiscountAmount());
            curLineItem.setAfterDiscountAmount(curLineItem.getAmount());
            // curLineItem.setAddRebateAmount(curLineItem.getAfterDiscountAmount().multiply(curLineItem.getRebateRate()));
            // modify 2004-03-29 有变价的商品还原金为0
            curLineItem.setRebateRate(new HYIDouble(0.00));
            curLineItem.setAddRebateAmount(new HYIDouble(0.00));
			curLineItem.setYellowAmount(discountAmount);
            
            curLineItem.setAfterSIAmount(curLineItem.getAmount());
            curLineItem.caculateAndSetTaxAmount();
            curLineItem.setDiscountType("O");
            
//            // Bruce/2003-08-08
//            if (computeDiscountRate) {
//                try {
//                    HYIDouble rate = curLineItem.getUnitPrice().divide(curLineItem.getOriginalPrice(),
//                        2, BigDecimal.ROUND_HALF_UP);
//                    curLineItem.setDiscountRate(rate);
//                    DiscountType dis = DiscountType.queryByDiscountRate(rate);
//                    if (dis != null) {
//                        curLineItem.setDiscountRateID(dis.getID());
//                    }
//                } catch (ArithmeticException e) {
//                // 初始价格为0，不设置折扣信息 zhaohong 2003-10-30
//                }
//            }
                
            try {
                app.getCurrentTransaction().changeLineItem(-1, curLineItem);
            } catch (LineItemNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("LineItem not found at " + this);
            }
            CreamToolkit.showText(app.getCurrentTransaction(), 0);
			tempPrice = "";
			System.out.println("YellowAmountState | exit IdleState");
// return IdleState.class;
			return MixAndMatchState.class;
		}
		
		return YellowAmountState.class;
	}
}