package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.CreamSession;
import hyi.cream.dac.*;
import hyi.cream.inline.Client;
import hyi.cream.inline.InlineCommandExecutor;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.awt.Toolkit;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * ZState state class.
 * 
 * @author dai, Bruce
 * @version 1.7
 */
public class ZState extends State {

    /**
     * /Bruce/1.7/2002-04-27/ 增加代收明细帐存盘.
     */
    transient public static final String VERSION = "1.7";

    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    private static ZState zState = null;
    private boolean isShiftCompleted = true;

    public static ZState getInstance() {
        try {
            if (zState == null) {
                zState = new ZState();
            }
        } catch (InstantiationException ex) {
        }
        return zState;
    }

    /**
     * Constructor
     */
    public ZState() throws InstantiationException {
    }

    // modify by lxf 2003.02.08 (new flag FloppyContent)
    public void entry(EventObject event, State sourceState) {
    	app.setEnableKeylock(false);
        DbConnection connection = null;
        ZReport z;
        try {
            connection = CreamToolkit.getTransactionalConnection();
            CreamToolkit.logMessage("ZState | entry | start ");
            Date initDate = CreamToolkit.getInitialDate();
            ShiftReport shift = ShiftReport.getCurrentShift(connection);
            if (!StringUtils.isEmpty(PARAM.getCashierNumber()) || shift != null) {
                // Cashier has signed off
                if (shift != null) {
                    if (CreamToolkit.compareDate(shift.getAccountingDate(), initDate) == 0) {
                        isShiftCompleted = false;
                        return;
                    }
                }
            }

            // 作日结
            z = ZReport.getOrCreateCurrentZReport(connection);
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("z"));
            Transaction currTrans = app.getCurrentTransaction();
            currTrans.clear();
            currTrans.init();
            currTrans.setDealType2("D");
            currTrans.setInvoiceCount(0);
            currTrans.setInvoiceID("");
            currTrans.setInvoiceNumber("");
            currTrans.store(connection);
            connection.commit();

            // only use when property "FloppyContent" is "full"
            boolean useSaveFull;
            String useSvaeStr = PARAM.getFloppyContent();
            useSaveFull = useSvaeStr.equalsIgnoreCase("full");
    
            if (!Client.getInstance().isConnected()) {
                CreamToolkit.logMessage("Save Z report to floppy disk");
                if (useSaveFull)
                    LineOffTransfer.getInstance().saveToDisk2(z);
                else
                    LineOffTransfer.getInstance().saveToDisk(z);
    
                CreamToolkit.logMessage("Save depsales to floppy disk");
                ArrayList objArray = new ArrayList();
                try {
                    for (Iterator it = DepSales.queryBySequenceNumber(connection, z.getSequenceNumber());
                        it.hasNext();) {
                        objArray.add(it.next());
                    }
                    if (!objArray.isEmpty()) {
                        if (useSaveFull)
                            LineOffTransfer.saveToDisk2(objArray.toArray());
                        else
                            LineOffTransfer.getInstance().saveToDisk(objArray.toArray());
                    }
                } catch (Exception e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }
    
                CreamToolkit.logMessage("Save daishousales to floppy disk");
                objArray.clear();
                try {
                    Iterator iter = DaishouSales.getCurrentDaishouSales(connection, z.getSequenceNumber()
                        .intValue());
                    if (iter != null) {
                        for (Iterator it = iter; it.hasNext();)
                            objArray.add(it.next());
    
                        if (!objArray.isEmpty()) {
                            if (useSaveFull)
                                LineOffTransfer.saveToDisk2(objArray.toArray());
                            else
                                LineOffTransfer.getInstance().saveToDisk(objArray.toArray());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }
    
                CreamToolkit.logMessage("Save daishousales2 to floppy disk");
                objArray.clear();
                try {
                    Iterator iter = DaiShouSales2.getCurrentDaishouSales(connection, z.getSequenceNumber()
                        .intValue());
                    if (iter != null) {
                        for (Iterator it = iter; it.hasNext();)
                            objArray.add(it.next());
    
                        if (!objArray.isEmpty()) {
                            if (useSaveFull)
                                LineOffTransfer.saveToDisk2(objArray.toArray());
                            else
                                LineOffTransfer.getInstance().saveToDisk(objArray.toArray());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }
    
                CreamToolkit.logMessage("ZState | entry | end ");
        }
        } catch (SQLException e) {
        	app.setEnableKeylock(true);
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
            return; // don't go on if failed to save z
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        CreamToolkit
            .logMessage("ZState | exit | connected : " + Client.getInstance().isConnected());
        if (!isShiftCompleted) {
            isShiftCompleted = true;
            //DacTransfer.getInstance().setStoZPrompt(true);
            CreamSession.getInstance().setDoingShiftBeforeZ(true);
            return ShiftState.class;
        }

        DbConnection connection = null;
        ZReport z;
        try {
            connection = CreamToolkit.getTransactionalConnection();
            z = ZReport.getOrCreateCurrentZReport(connection);
            connection.commit();
        } catch (SQLException e) {
        	app.setEnableKeylock(true);
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
            return ConfirmZState.getSourceState();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }

        if (PARAM.isZPrint())
            CreamPrinter.getInstance().printZReport(z, this);

        /*else if (zPrint.equals("debug")) {
            StringWriter data = new StringWriter();
            ReportGenerator r = new ReportGenerator(z);
            data = (StringWriter)r.generate(data);
            CreamToolkit.logMessage(data.toString());
        }*/

        if (Client.getInstance().isConnected()) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("zEnd"));
            app.setChecked(false);
            checkZTranAmount(z);
            InlineCommandExecutor.getInstance().execute("putAttendance " + z.getSequenceNumber());
            InlineCommandExecutor.getInstance().execute("putZ " + z.getSequenceNumber());
        } else {
            // Bruce/2003-11-06
            checkZTranAmount(z);
            app.getMessageIndicator().setMessage(
                CreamToolkit.GetResource().getString("ZNotUploadWarning"));
            CreamToolkit.logMessage("Z upload failed.");
            if (PARAM.isUseFloppyToSaveData())
                return ZState2.class;

            try {
                Toolkit.getDefaultToolkit().beep();
                Toolkit.getDefaultToolkit().beep();
                Toolkit.getDefaultToolkit().beep();
                Thread.sleep(2500);
            } catch (InterruptedException e) {
            }
        }
        if (PARAM.isRebootAfterZ()) {
            if (System.getProperties().get("os.name").toString().indexOf("Windows") == -1) {
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("UpdateCompletedAndReboot"));
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                }
                CreamToolkit.reboot();
            }
        }
    	app.setEnableKeylock(true);
        return ConfirmZState.getSourceState();
    }

    public void checkZTranAmount(ZReport z) {
        try {
            if (PARAM.isRecalcAfterZ()) {
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("z") + "---"
                        + CreamToolkit.GetResource().getString("AmountCheck"));
                int eodcnt = z.getSequenceNumber().intValue();
                String sql = "SELECT SUM(netsalamt0+netsalamt1+netsalamt2+netsalamt3+netsalamt4+daishouamt+daishouamt2) AS total FROM z WHERE eodcnt="
                    + eodcnt;
                Map data = CreamToolkit.getResultData(sql);
                HYIDouble zTotal = new HYIDouble(((Double)data.get("total")).doubleValue());
                sql = "SELECT SUM(netsalamt+daishouamt+daishouamt2) AS total FROM tranhead WHERE eodcnt="
                    + eodcnt;
                data = CreamToolkit.getResultData(sql);
                HYIDouble tranTotal = new HYIDouble(((Double)data.get("total")).doubleValue());
                if (zTotal.compareTo(tranTotal) != 0) {
                    CreamToolkit.logMessage("[" + eodcnt + "] z and tranhead recalc | z total : "
                        + zTotal + " | tran total : " + tranTotal);//
                    app.recalcFromTransactions(String.valueOf(z.getBeginTransactionNumber()
                        .intValue()), String.valueOf(z.getEndTransactionNumber().intValue()), z
                        .getAccountingDate(), false);
                } else
                    CreamToolkit.logMessage("[" + eodcnt + "] z and tranhead OK!!!");
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("z"));
            }
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }
}
