package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.alipay.AlipayHttpPostRequest;
import hyi.cream.alipay.AlipaySubmit;
import hyi.cream.groovydac.Param;
import hyi.cream.dac.*;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.sdk.UnionPayRefund;
import hyi.cream.state.cat.CATAuthSalesState;
import hyi.cream.state.cat.CATAuthType;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.*;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ReturnSummaryState extends State {
    static ReturnSummaryState returnSummaryState;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private Transaction currentTrans;
    private Transaction oldTran;
    private String returnReasonID;
    private HashMap returnReasonIDHm;
    private boolean partialRefund = false;
    private HYIDouble returnQty = new HYIDouble(0);

    private ReturnSummaryState() {}

    public static ReturnSummaryState getInstance() {
        if (returnSummaryState == null) {
            returnSummaryState = new ReturnSummaryState();
        }
        return returnSummaryState;
    }

    public void setReturnReasonID(String rid) {
        this.returnReasonID = rid;
    }

    public void setReturnReasonIDHm(HashMap ridHm) {
        this.returnReasonIDHm = ridHm;
    }

    public HashMap getReturnReasonIDHm() {
        return returnReasonIDHm;
    }

    public void entry(EventObject event, State sourceState) {
        oldTran = CreamSession.getInstance().getTransactionToBeRefunded();
        currentTrans = getCurrentTransaction();

        showMessage("ConfirmCancel");

        if (sourceState instanceof ReturnAllState) {
            // 全退 汇总确认
            dealForSourceFromReturnAllState();

        } else if (sourceState instanceof ReturnShowState) {
            // 傳票退貨
            dealForDeliveryRefund();

            // 处理退货支付
            currentTrans.initForAccum();            
            currentTrans.accumulate(false);
            HYIDouble refundAmount = currentTrans.getNetSalesAmount();
            processReturnPayment(refundAmount);

        } else if (sourceState instanceof ReturnOneState) {
            // 部分退货 选择输入欲退货的商品和数量结束后到此
            dealForSourceFromReturnOneState();

            // 处理退货支付
            HYIDouble refundAmount = ReturnSelect2State.getInstance().calcRefundAmount();
            processReturnPayment(refundAmount);
        }

        // 设置 payingPane 显示内容
        app.getPayingPane().setTransaction(currentTrans);
        app.getPayingPane1().setTransaction(currentTrans);
        app.getPayingPane2().setTransaction(currentTrans);
        app.getItemList().setVisible(false);
        app.getPayingPane1().setMode(2); // 目前退货时不显示
        app.getPayingPane2().setMode(2); // 目前退货时不显示
        app.getPayingPane().setVisible(true);
        app.getPayingPane().repaint();

        CreamToolkit.showText(currentTrans, 1); // 将交易相关资料显示在客显上
    }

    private void dealForSourceFromReturnOneState() {
        partialRefund = true;

        //oldTran = CreamSession.getInstance().getTransactionToBeRefunded();
        //selectArray = ((ReturnOneState) sourceState).getSelectArray();
        //Map<SI, HYIDouble> appliedSIs = oldTran.getAppliedSIs();
        //showTran = (Transaction) currentTrans.clone();
        //showTran.initSIInfo();
        //showTran.addAppliedSI(null, null);
        //showTran.removeSILineItem();
        //boolean first = true;
        //for (SI si : appliedSIs.keySet()) {
        //    SIDiscountState.getInstance().processDiscount(si, null, showTran, true, first);
        //    first = false;
        //}
        //showTran.summarize(); // 这样showTran.getSalesAmount()才会包含SI折扣金额
        //HYIDouble amount = showTran.getSalesAmount().subtract(oldTran.getNetSalesAmount());
        //showTran.setGrossSalesAmount(amount);
        //showTran.setSalesAmount(amount);
        //showTran.setInvoiceAmount(showTran.getSalesAmount());
        //showTran.setTotalMMAmount(showTran.getMixAndMatchTotalAmount()
        //        .subtract(oldTran.getTaxMMAmount()).negate());
        //showTran.setDaiShouAmount(new HYIDouble(0));
    }

    /**
     * 配送傳票退貨.
     */
    private void dealForDeliveryRefund() {
        Transaction refundTran = CreamSession.getInstance().getTransactionToBeRefunded();
        refundTran.setCustomerCount(-1);
        partialRefund = !isAllRefundDelivery(currentTrans);

//        oldTran = null;
//        try {
//            String voidTranNo = refundTran.getVoidTransactionNumber().toString();
//            String storeID = refundTran.getOtherStoreID() != null ?
//                refundTran.getOtherStoreID() : refundTran.getStoreNumber();
//            oldTran = StateToolkit.posulTran2Tran(storeID,  // 如果是他店退貨, storeID是他店店號，所以會去查他店的交易
//                    voidTranNo.substring(voidTranNo.length() - 2),
//                    voidTranNo.substring(0, voidTranNo.length() - 2));
//            oldTran.setAccountingDate(CreamToolkit.getInitialDate());
//        } catch (Exception e) {
//            e.printStackTrace();
//            e.printStackTrace(CreamToolkit.getLogger());
//        }
//
//        String payno = null;
//        String payamt = null;
//        for (int i = 1; i < 5; i++) {
//            payno = "PAYNO" + i;
//            payamt = "PAYAMT" + i;
//            if (oldTran.getFieldValue(payno) == null
//                    || oldTran.getFieldValue(payno).toString().trim().equals("")) {
//                break;
//            }
//            currentTrans.setFieldValue(payno, oldTran.getFieldValue(payno));
//            //currentTrans.setFieldValue(payamt, oldTran.getFieldValue(payamt));
//            HYIDouble payAmount = ((HYIDouble)oldTran.getFieldValue(payamt));
//            currentTrans.setFieldValue(payamt, payAmount == null ? new HYIDouble(0) : payAmount.negate());
//
//            currentTrans.addPayment(Payment.queryByPaymentID((String) oldTran.getFieldValue(payno)));
//        }
//        currentTrans.setSpillAmount(oldTran.getSpillAmount().negate());
//        currentTrans.setChangeAmount(oldTran.getChangeAmount().negate());
        
//        currentTrans.setAnnotatedId(refundTran.getAnnotatedId());
//        currentTrans.setAnnotatedType(refundTran.getAnnotatedType());
//        currentTrans.initForAccum();
//        currentTrans.accumulate();
//        showTran = (Transaction) currentTrans.clone();

//        newly004Trans.removeSILineItem();

//        HYIDouble amount = new HYIDouble(0);
//        amount = showTran.getSalesAmount().subtract(
//                oldTran.getSalesAmount());
//
//        showTran.setGrossSalesAmount(amount);
//        showTran.setSalesAmount(amount);
//        showTran.setInvoiceAmount(amount);
//
//        showTran.setTotalMMAmount(showTran.getMixAndMatchTotalAmount()
//                .subtract(oldTran.getTaxMMAmount()).negate());
//        showTran.setDaiShouAmount(new HYIDouble(0));

        //processReturnPayment(showTran);
    }

    private boolean isAllRefundDelivery(Transaction trans) {
        HYIDouble nHYIDouble = new HYIDouble(0);
        //if (nHYIDouble.compareTo(trans.getNetSalesAmount()) != 0)
        //    return false;
        // all amount in detail is zero
        // one not zero means not a isAllRefundDelivery

        //Bruce/20090206/ 檢查傳票交易是否為全退的邏輯：判斷是否有非退商品的金額不為零者，若存在，表示不是全退
        for (Object o : trans.getLineItems()){
            LineItem li = (LineItem)o;
            //System.out.println("li.getAmount()>" + li.getAmount() + " | li.getQuantity()>" + li.getQuantity());
            if (!li.getRemoved() && nHYIDouble.compareTo(li.getAmount()) != 0){
                return false;
            }
        }
        return true;
    }

    private void dealForSourceFromReturnAllState() {
        // 全退 汇总确认
        //oldTran = CreamSession.getInstance().getTransactionToBeRefunded();
        //currentTrans = getCurrentTransaction();
        //showTran = (Transaction) currentTrans.clone();
        //newly004Trans = (Transaction) currentTrans.clone();
        //newly004Trans.removeSILineItem();

        currentTrans.setCustomerCount(-1);
        partialRefund = false;
    }

    public Class exit(EventObject event, State sinkState) {
        Object eventSource = event.getSource();

        if (partialRefund && !isInvoiceEnough()) {
            showWarningMessage("InvoiceNotEnough");
            return backToRefund();

        } else if (eventSource instanceof ClearButton) {
            return backToRefund();

        } else if (eventSource instanceof EnterButton) {
            if (currentTrans.existsCATPayment()) {
                if (partialRefund)
                    CATAuthSalesState.getInstance().setAuthType(CATAuthType.PARTIAL_REFUND); // set to refund mode
                else
                    CATAuthSalesState.getInstance().setAuthType(CATAuthType.REFUND); // set to refund mode
                return CATAuthSalesState.class; // (Refund1:RE2CA)
            }
            // 开始保存数据和打印小票
            return saveRefundData(); // (Refund1:RE2DO)

        } else {
            return ReturnSummaryState.class;
        }
    }

    public Class backToRefund() {
        return partialRefund ? backToPartialRefund() : backToAllRefund();
    }

    public Class saveRefundData() {
        if (app.getTrainingMode())
            return DrawerOpenState.class;
        
        // partialRefund true means 部分退 false means 全退
        boolean error = false;
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();
            showMessage("DataTransfers");

            if (!partialRefund) {
                if (oldTran.getAlipayList().size() > 0 ) {
                    if (AlipayHttpPostRequest.getPropery() == null) {
                        app.getWarningIndicator().setMessage(res.getString("AlipayProperyFail"));
                        connection.commit();
                        return this.getClass();
                    }
                    boolean isReturn = alipayReturn(connection,(Transaction) oldTran.deepClone(), currentTrans, app, res);
                    if (!isReturn) {
                        connection.commit();
                        return this.getClass();
                    }
                    oldTran.getAlipayList().clear();
                    app.getWarningIndicator().setMessage(res.getString("AlipayRefundSuccess"));
                }
                if (oldTran.getCmpayList().size() > 0 ) {
                    boolean isReturn = cmpayReturn(connection, (Transaction)oldTran.deepClone(), currentTrans, app, res);
                    if (!isReturn) {
                        connection.commit();
                        return this.getClass();
                    }
                    oldTran.getCmpayList().clear();
                    app.getWarningIndicator().setMessage(res.getString("CmpayRefundSuccess"));
                }
                if (oldTran.getWeiXinList().size() > 0 ) {
                    if (WeiXinHttpPostRequest.getPropery() == null) {
                        app.getWarningIndicator().setMessage(res.getString("WeiXinProperyFail"));
                        connection.commit();
                        return this.getClass();
                    }
                    boolean isReturn = weiXinReturn(connection,(Transaction) oldTran.deepClone(), currentTrans, app, res);
                    if (!isReturn) {
                        connection.commit();
                        return this.getClass();
                    }
                    oldTran.getWeiXinList().clear();
                    app.getWarningIndicator().setMessage(res.getString("WeiXinRefundSuccess"));
                }
                if (oldTran.getUnionPayList().size() > 0 ) {
                    /*if (WeiXinHttpPostRequest.getPropery() == null) {
                        app.getWarningIndicator().setMessage(res.getString("WeiXinProperyFail"));
                        connection.commit();
                        return this.getClass();
                    }*/
                    boolean isReturn = unionPayReturn(connection,(Transaction) oldTran.deepClone(), currentTrans, app, res);
                    if (!isReturn) {
                        connection.commit();
                        return this.getClass();
                    }
                    oldTran.getUnionPayList().clear();
                    app.getWarningIndicator().setMessage(res.getString("UnionPayRefundSuccess"));
                }
            }
            // 作废以前的交易并保存
            if (!app.isReturnFromSC()) {
                cancelTran(connection, oldTran);
            }

            if (partialRefund) { //部分退
                generate034RefundTrans(connection);

                // 生成新的不包含已退明细的交易并打印
                setNewTranHead(connection);
                try {
                    //// 信用卡信息
                    //currentTrans.setCreditCardNumber(currentTrans.getCreditCardNumber());
                    //currentTrans.setCreditCardExpireDate(currentTrans.getCreditCardExpireDate());
                    //currentTrans.setCreditCardType(currentTrans.getCreditCardType());

                    currentTrans.setChangeAmount(new HYIDouble(0));
                    currentTrans.setAccountingDate(CreamToolkit.getInitialDate());
                    currentTrans.setInvoiceID(PARAM.getInvoiceID());
                    currentTrans.setInvoiceNumber(PARAM.getInvoiceNumber());

                    setNewTranItems();

                    app.setReturnQty(returnQty);

                    // 把折扣信息加入
                    for (SI si : oldTran.getAppliedSIs().keySet()) {
                        currentTrans.addAppliedSI(si, oldTran.getAppliedSIs().get(si));
                    }

                    //currentTrans.initForAccum();
                    //currentTrans.accumulate(false);
                    //StateToolkit.recalcTaxAmount(currentTrans);
                    currentTrans.store(connection);

                    if (currentTrans.getLineItems().length > 0) {
                        CreamPrinter.getInstance().setPrintEnabled(true);
                        // 打印头信息
                        CreamPrinter.getInstance().printHeader(connection, currentTrans);
                        for (LineItem lineItem : currentTrans.lineItems()) {
                            String detailCode = lineItem.getDetailCode();
                            if (!"D".equals(detailCode) && !"M".equals(detailCode))
                                CreamPrinter.getInstance()
                                    .printLineItem(connection, currentTrans, lineItem);
                        }
                        // 打印Payment
                        CreamPrinter.getInstance().printPayment(connection, currentTrans);
                    }
                } catch (Exception e) {
                    error = true;
                    e.printStackTrace();
                    if (e.getMessage().equals("ReturnAdjustItemNOError")) {
                        this.setWarningMessage(res.getString("ReturnAdjustItemNOError"));
                        return WarningState.class;
                    }
                } finally {
                    if (!error) {
                        currentTrans.setInvoiceCount(ReceiptGenerator.getInstance().getPageNumber());
                        currentTrans.update(connection);
                    }
                }

            } else { //全退 或 傳票退貨
                generate034RefundTrans(connection);

                app.setIsAllReturn(true);
                CreamToolkit.showText(currentTrans, 1);
            }
            app.getItemList().setBackLabel(null);
            connection.commit();
            CreamSession.getInstance().setTransactionToBeRefunded(null);
            ReturnNumber2State.originalPeiDaTransaction = null;
            return DrawerOpenState.class;

        } catch (Exception e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
            return ReturnSummaryState.class;
        } finally {

            CreamToolkit.releaseConnection(connection);
        }
    }

    private void generate034RefundTrans(DbConnection connection) throws SQLException {// 生成新的负向交易并保存
        Transaction refundTran;
        if (ReturnNumber2State.originalPeiDaTransaction != null) // 原配送交易
            refundTran = ReturnNumber2State.originalPeiDaTransaction;
        else
            refundTran = (Transaction)oldTran.deepClone();

        refundTran = makeReturnTran(connection, refundTran);
        refundTran.setAccountingDate(CreamToolkit.getInitialDate());
        refundTran.setAnnotatedId(currentTrans.getAnnotatedId());
        app.setReturnQty(returnQty);
        // 设置退货原因
        if (returnReasonID != null) {
            setTranDiscno(refundTran, returnReasonID);
            returnReasonID = null;
        }
        // 信用卡信息
        refundTran.setCreditCardNumber(currentTrans.getCreditCardNumber());
        refundTran.setCreditCardExpireDate(currentTrans.getCreditCardExpireDate());
        refundTran.setCreditCardType(currentTrans.getCreditCardType());

        // 如果目前做的是退訂金尾款交易，不印發票的那種，就把發票信息設空值
        String state1 = getCurrentTransaction().getState1(); // set in InputDownPaymentState
        boolean downPaymentTrans = "W".equals(state1) || "X".equals(state1);
        if (downPaymentTrans) {
            refundTran.setInvoiceCount(0);
            refundTran.setInvoiceID("");
            refundTran.setInvoiceNumber("");
        }
        refundTran.store(connection);
    }

    private Class backToAllRefund() {
        app.getPayingPane().setVisible(false);
        app.getItemList().setVisible(true);
        app.getPayingPane1().setMode(0);
        return ReturnShowState.class;
    }

    private Class backToPartialRefund() {
        app.getPayingPane().setVisible(false);
        app.getItemList().setVisible(true);

        if (currentTrans.isPeiDa())
            return ReturnShowState.class;
        return ReturnOneState.class;
    }

    private void setTranDiscno(Transaction t, String rid) {
        Iterator it = t.getLineItemsIterator();
        if (it != null) {
            while (it.hasNext()) {
                LineItem li = (LineItem) it.next();
                li.setDiscountNumber(getNewDiscno(li, rid));
            }
        }
    }

    private void setTranDiscno(Transaction t, HashMap ridHm) {
        Object[] lineItems = t.getLineItems();
        LineItem li = null;
        for (Iterator it = ridHm.keySet().iterator(); it.hasNext();) {
            String no = (String) it.next();
            String rid = (String) ridHm.get(no);
            int index = Integer.parseInt(no) - 1;
            li = (LineItem) lineItems[index];
            li.setDiscountNumber(getNewDiscno(li, rid));
        }
    }

    private String getNewDiscno(LineItem li, String rid) {
        String disCno = li.getDiscountNumber();
        if (disCno != null && !disCno.trim().equals("")) {
            int index = disCno.indexOf("R");// R stands for return reason
            if (index >= 0) {
                String left = disCno.substring(0, index);
                String right = disCno.substring(index, disCno.length());
                if (right.indexOf(",") > 0) {
                    right = right.substring(right.indexOf(",") + 1, right
                            .length());
                } else {
                    right = "";
                    if (left.length() > 0)
                        left = left.substring(0, left.length() - 1);
                }
                disCno = left + right;
            }
            if (!disCno.equals(""))
                disCno += ",";
        } else {
            disCno = "";
        }
        disCno += "R" + rid;
        // System.out.println(li + " " + disCno);
        return disCno;
    }

    private void cancelTran(DbConnection connection, Transaction t)
            throws SQLException, EntityNotFoundException {
        t.setDealType1("*");
        t.setDealType2("0");
        t.setDealType3("0");
        t.update(connection);
    }

    /**
     * 产生退货交易，该交易为以前交易的负向交易
     *
     * @param trans
     *            把该交易转换成退货交易
     * @return Transaction 退货交易(DealType 为 0,3,4)
     */
    private Transaction makeReturnTran(DbConnection connection, Transaction trans) {
        trans.setDealType1("0");
        trans.setDealType2("3");
        trans.setDealType3("4");

        trans.setTransactionNumber(Transaction.getNextTransactionNumber());
        trans.setTerminalNumber(PARAM.getTerminalNumber());
        if (trans.getVoidTransactionNumber() == null || trans.getVoidTransactionNumber() == 0)
            trans.setVoidTransactionNumber(oldTran.getTransactionNumber());
        trans.setStoreNumber(Store.getStoreID());
        trans.setSystemDateTime(new Date());
        trans.setTerminalPhysicalNumber(PARAM.getTerminalPhysicalNumber());
        trans.setZSequenceNumber(new Integer(ZReport.getCurrentZNumber(connection)));
        trans.setSignOnNumber(PARAM.getShiftNumber());
        //trans.setTransactionType(oldTran.getTransactionType());
        trans.setCashierNumber(PARAM.getCashierNumber());
        // 统一编号与原有交易相同，所以这里不要重置
        // trans.setBuyerNumber("");

        trans.setTotalMMAmount(new HYIDouble(0)
            .addMe(trans.getMixAndMatchAmount0())
            .addMe(trans.getMixAndMatchAmount1())
            .addMe(trans.getMixAndMatchAmount2())
            .addMe(trans.getMixAndMatchAmount3())
            .addMe(trans.getMixAndMatchAmount4()));
        returnQty = new HYIDouble(0);
        for (LineItem newLineItem : trans.lineItems()) {
            newLineItem.setTerminalNumber(trans.getTerminalNumber());
            newLineItem.setTransactionNumber(trans.getTransactionNumber());

            String detailCode = newLineItem.getDetailCode();
            if (detailCode.equalsIgnoreCase("M")
                || detailCode.equalsIgnoreCase("D")) {

                //// Negate discno的值
                //if (!StringUtils.isEmpty(newLineItem.getDiscountNumber())) {
                //    try {
                //        newLineItem.setDiscountNumber(
                //            new HYIDouble(newLineItem.getDiscountNumber()).negate().toString());
                //    } catch (Exception e) {
                //    }
                //}
                continue;
            } else if (!detailCode.equalsIgnoreCase("I") && !detailCode.equals("O")) {
                newLineItem.setDetailCode("R");
            }

            // 2003-06-16 Updated by zhaohong 如果是限价销售,实际销售数量减 1
            if (newLineItem.getDiscountType() != null
                && newLineItem.getDiscountType().equals("L")) {
                try {
                    Client.getInstance().processCommand(
                        "queryLimitQtySold " + newLineItem.getItemNumber() + " -1 ");
                } catch (ClientCommandException e) {
                }
            }
            // 計算需要累加到Z/Shift的退貨數量
            returnQty = returnQty.addMe(new HYIDouble(newLineItem.getQuantity().intValue()));
        }
        trans.makeNegativeValue();
        return trans;
    }

    // 设置新交易的头信息
    private void setNewTranHead(DbConnection connection) {
        currentTrans.setDealType1("0");
        currentTrans.setDealType2("0");
        currentTrans.setDealType3("4");
        currentTrans.setStoreNumber(Store.getStoreID());
        currentTrans.setSystemDateTime(new Date());
        currentTrans.setTerminalNumber(PARAM.getTerminalNumber());
        currentTrans.setTransactionNumber(Transaction.getNextTransactionNumber());
        currentTrans.setSignOnNumber(PARAM.getShiftNumber());
        currentTrans.setTransactionType(oldTran.getTransactionType());
        currentTrans.setTerminalPhysicalNumber(PARAM.getTerminalPhysicalNumber());
        currentTrans.setCashierNumber(PARAM.getCashierNumber());
        currentTrans.setInvoiceNumber(PARAM.getInvoiceNumber());
        currentTrans.setInvoiceID(PARAM.getInvoiceID());
        currentTrans.setVoidTransactionNumber(oldTran.getTransactionNumber());
        currentTrans.setMemberID(oldTran.getMemberID()); //Bruce/2009-10-07
        currentTrans.setZSequenceNumber(new Integer(ZReport.getCurrentZNumber(connection)));
        currentTrans.setInvoiceCount(1);
        // 统一编号与原有交易相同，所以这里不要重置
        // currentTrans.setBuyerNumber("");
        //currentTrans.setDetailCount(currentTrans.getDetailCount().intValue() - selectArray.size());

        //HYIDouble spillAmount = oldTran.getSpillAmount() == null ? new HYIDouble(0) : (HYIDouble)oldTran.getSpillAmount().clone();
        //HYIDouble changeAmount = oldTran.getChangeAmount() == null ? new HYIDouble(0) : (HYIDouble)oldTran.getChangeAmount().clone();

        // 將currentTransaction的支付部份（退款金額）加上transactionToBeRefunded的淨支付金額,
        // 並將溢收金額抄過來, 這樣可以得到部份退貨的正項發票金額
        Map<Payment, HYIDouble> currTranPayment = currentTrans.getPaymentMap();
        Map<Payment, HYIDouble> oldTranPayment = oldTran.getNetPayments();
        for (Payment payment : oldTranPayment.keySet()) {
            HYIDouble oldPayAmount = oldTranPayment.get(payment);
            HYIDouble currPayment = currTranPayment.get(payment);
            if (currPayment == null)
                currTranPayment.put(payment, oldPayAmount);
            else
                currPayment.addMe(oldPayAmount);
        }
        int idx = 1;
        for (Payment payment : currTranPayment.keySet()) {
            HYIDouble payAmount = currTranPayment.get(payment);
            if (!payAmount.equals(HYIDouble.zero())) {
                currentTrans.setPayNumberByID(idx, payment.getPaymentID());
                currentTrans.setPayAmountByID(idx, payAmount);
                idx++;
            }
        }
        if (idx == 1) {
            currentTrans.setPayNumberByID(idx, "00");
            currentTrans.setPayAmountByID(idx, new HYIDouble(0));
            idx++;
        }
        for (; idx <= 4; idx++) {
            currentTrans.setPayNumberByID(idx, null);
            currentTrans.setPayAmountByID(idx, null);
        }
        currentTrans.setSpillAmount(oldTran.getSpillAmount());

//        for (int i = 1; i < 5; i++) {
//            String payamt = "PAYAMT" + i;
//            String payid = "PAYNO" + i;
//            if (showTran.getFieldValue(payamt) == null || oldTran.getFieldValue(payid) == null)
//                continue;
//            //String payID = (String) oldTran.getFieldValue(payid);
//            //currentTrans.setFieldValue(payid, (String) oldTran.getFieldValue(payid));
//            currentTrans.setFieldValue(payamt, ((HYIDouble) oldTran.getFieldValue(payamt)).subtract(
//                    ((HYIDouble) showTran.getFieldValue(payamt)).negate()));
//        }
//
//        List payList = new ArrayList();
//        for (Iterator it = currentTrans.getPayments(); it.hasNext();) {
//            Payment payment = (Payment) it.next();
//            payList.add(payment);
//        }
//        Collections.sort(payList, new PaymentComparator());
//        // 取得 支付列表并排序后 按优先顺序扣减找零
//
//        for (Iterator it = payList.iterator(); it.hasNext();) {
//            Payment payment = (Payment) it.next();
//            String payid = "";
//            String payamt = "";
//
//            for (int i = 1; i < 5; i++) {
//                payamt = "PAYAMT" + i;
//                payid = "PAYNO" + i;
//
//                if (currentTrans.getFieldValue(payamt) == null)
//                    continue;
//                //Payment payment = Payment.queryByPaymentID(payID);
//
//                if (currentTrans.getFieldValue(payid) != null
//                        && currentTrans.getFieldValue(payid).equals(payment.getPaymentID())) {
//                    //HYIDouble payAmount = (HYIDouble) showTran.getFieldValue(payamt);
//
//                    if (payment != null && spillAmount.compareTo(new HYIDouble(0)) != 0 &&
//                            payment.isSpillable()) {// 可溢收
//                        // 對退貨時是否還要控制是否可溢收？
//                        if (((HYIDouble) currentTrans.getFieldValue(payamt)).compareTo(spillAmount) >= 0) {
//                            currentTrans.setFieldValue(payamt, ((HYIDouble) currentTrans.getFieldValue(payamt)).subtract(spillAmount));
//                            spillAmount = new HYIDouble(0);
//                        } else {
//                            spillAmount = spillAmount.subtract((HYIDouble) currentTrans.getFieldValue(payamt));
//                            currentTrans.setFieldValue(payamt, new HYIDouble(0));
//                        }
//                    }
//                    if (payment != null && changeAmount.compareTo(new HYIDouble(0)) != 0
//                            // && payment.getPaymentID().equals(GetProperty.getChangeAmtPaymentID("00"))
//                            // && payment.getPaymentType().charAt(4) == '1'
//                            // 對退貨時是否還要控制是否可找零？
//                                ) { // 可找零
//                        if (((HYIDouble) currentTrans.getFieldValue(payamt)).compareTo(changeAmount) >= 0) {
//                            currentTrans.setFieldValue(payamt, ((HYIDouble) currentTrans.getFieldValue(payamt)).subtract(changeAmount));
//                            changeAmount = new HYIDouble(0);
//                        } else {
//                            changeAmount = changeAmount.subtract((HYIDouble) currentTrans.getFieldValue(payamt));
//                            currentTrans.setFieldValue(payamt, new HYIDouble(0));
//                        }
//                    }
//                }
//            }
//        }
//        currentTrans.setSpillAmount(spillAmount);
//        currentTrans.setChangeAmount(changeAmount);
//        currentTrans.setAnnotatedType(showTran.getAnnotatedType());
//        currentTrans.setAnnotatedId(showTran.getAnnotatedId());
    }

    private boolean isInvoiceEnough() {
        if (!Param.getInstance().isCountInvoiceEnable())
            return true;

        int lineItemCount = 0;
        for (LineItem lineItem : new ArrayList<LineItem>(currentTrans.lineItems())) {
            String detailCode = lineItem.getDetailCode();
            if (!lineItem.getRemoved() &&
                (detailCode.equals("S")         // "S":销售
                || detailCode.equals("I")       // "I":代售
                || detailCode.equals("O")))     // "O":代收
                lineItemCount++;
        }
        if (lineItemCount == 0)
            return true;

        int lineItemPerPage = ReceiptGenerator.getInstance().getLineItemPerPage();
        int pageNeed = (lineItemCount - 1) / lineItemPerPage + 1;

        int invNo = 0;
        try {
            invNo = Integer.parseInt(Param.getInstance().getInvoiceNumber());
        } catch (Exception e) {
        }
        int countDown = 250 - (invNo % 250);

        return countDown >= pageNeed;
    }

    private void setNewTranItems() throws Exception {
        try {
            returnQty = new HYIDouble(0);

            // trandetail 的 sequenceNumber
            int seq = 1;
            int tranSeq = Transaction.getNextTransactionNumber();

            // discountMap: 記錄新的折扣代號與相應的折扣金額, 以及各分稅別的折扣額
            // Key:
            //    "M01": 組合促銷"01"的折扣金額
            //    "Y01": 編號01的組合促銷折扣稅額
            //    "S02": SI折扣"02"的折扣金額
            //    "Z02": SI折扣"02"的折扣稅額
            //    "T0": 稅別0商品的組合促銷折扣額
            //    "T1": 稅別1商品的組合促銷折扣額
            //    "U0": 稅別0商品的SI折扣額
            //    "U1": 稅別1商品的SI折扣額
            Map<String, HYIDouble> discountMap = new HashMap<String, HYIDouble>();

            for (LineItem lineItem : new ArrayList<LineItem>(currentTrans.lineItems())) {
                String detailCode = lineItem.getDetailCode();
                // 去掉无效的行
                if (lineItem.getRemoved()
                        || !(detailCode.equals("S")     // "S":销售
                        || detailCode.equals("R")       // "R":退货
                        || detailCode.equals("I")       // "I":代售
                        || detailCode.equals("O")       // "O":代收
                        || detailCode.equals("M")       // "M":Mix & Match折扣
                        || detailCode.equals("D"))) {   // "D":SI折扣
                    try {
                        currentTrans.removeLineItem(lineItem);
                        CreamToolkit.logMessage("ReturnSummaryState> remove: " + lineItem);
                        continue;
                    } catch (LineItemNotFoundException e) {
                        continue;
                    }
                } else {
                    CreamToolkit.logMessage("ReturnSummaryState> leave: " + lineItem);
                }
                lineItem.setTerminalNumber(currentTrans.getTerminalNumber());
                lineItem.setTransactionNumber(tranSeq);
                // 改写明细序号
                lineItem.setLineItemSequence(seq++);
                lineItem.setPrinted(false);

                // 計算需要累加到Z/Shift的退貨數量
                returnQty = returnQty.subtractMe(new HYIDouble(lineItem.getQuantity().intValue()));

                // 分配SI折扣和組合促銷折扣金額
                calcNewDiscountAmount(discountMap, lineItem);

                if (!lineItem.getDetailCode().equals("I") &&
                    !lineItem.getDetailCode().equals("O"))
                    lineItem.setDetailCode("R");

                // 如果是限量销售，并且不被包含在所选择的退货明细中，把限销数量加上来
                if (lineItem.getDiscountType() != null &&
                    lineItem.getDiscountType().equals("L")) {
                    try {
                        Client.getInstance().processCommand("queryLimitQtySold " +
                            lineItem.getItemNumber() + " " + lineItem.getQuantity());
                    } catch (ClientCommandException e) {
                    }
                }
            }

            currentTrans.clearAppliedSIs();
            currentTrans.clearAppliedMMs();

            // 加入折扣項'D' or 'M', 和填入tranhead的稅別SI和組合促銷折扣金額
            HYIDouble ZERO = HYIDouble.zero();
            for (String discNo : discountMap.keySet()) {
                HYIDouble discAmt = discountMap.get(discNo);

                // 若最後算出的折扣金額已經降為零，那就不要存任何折扣項了
                if (ZERO.equals(discAmt))
                    continue;

                if (discNo.startsWith("M") || discNo.startsWith("S")) {
                    String discId = discNo.substring(1);
                    LineItem discItem = new LineItem();
                    discItem.setTerminalNumber(currentTrans.getTerminalNumber());
                    discItem.setTransactionNumber(currentTrans.getTransactionNumber());
                    discItem.setLineItemSequence(seq++);
                    discItem.setPrinted(false);
                    discItem.setDetailCode(discNo.startsWith("M") ? "M" : "D");
                    discItem.setRemoved(false);
                    discItem.setPluNumber(discId);
                    discItem.setItemNumber(discId);
                    if (discNo.startsWith("M")) {
                        discItem.setDiscountNumber(discNo);
                        discItem.setDiscountType("M");
                    }
                    //discItem.setDiscountType(discNo.startsWith("M") ? "M" : "S");
                    discItem.setQuantity(new HYIDouble(1));
                    discItem.setUnitPrice(discAmt);
                    discItem.setAmount(discAmt);
                    discItem.setOriginalPrice(discAmt);
                    discItem.setAfterDiscountAmount(HYIDouble.zero());

                    // 重新填入applied SIs, 這樣打印出來的折扣額才會正確
                    if (discNo.startsWith("S")) {
                        SI si = SI.queryBySIID(discId);
                        currentTrans.addAppliedSI(si, discAmt);

                        // 填trandetail.discno
                        if (si.getType() == SI.DISC_TYPE_DISCOUNT_PERCENTAGE)
                            discItem.setDiscountNumber(si.getValue().toString());
                        else if (si.getType() == SI.DISC_TYPE_INPUT_PERCENTAGE)
                            discItem.setDiscountNumber(getDiscNoValueFromOriginalTransaction(discId));
                        else
                            discItem.setDiscountNumber(discAmt.negate().toString());

                    } else {
                        currentTrans.addAppliedMM(discId, discAmt);
                    }

                    // 為了可以先讓Transaction.store()計算tranhead各稅別稅額正確
                    discItem.setTaxType(PARAM.getDiscountTaxId());

                    // 如果單品的稅額是按折扣前的毛額計算，此時生成的折扣項的稅額才要填上
                    if (PARAM.isGrossTrandtlTaxAmt()) {
                        String taxAmountKey = (discNo.startsWith("M") ? "Y" : "Z") + discNo.substring(1);
                        HYIDouble taxAmount = discountMap.get(taxAmountKey);
                        discItem.setTaxAmount(taxAmount);
                    }

                    currentTrans.addLineItem(discItem, false);

                } else if (discNo.startsWith("T")) {
                    discAmt = discAmt.negate(); // 折扣金額存正數
                    switch (discNo.charAt(1)) {
                    case '0': currentTrans.setMixAndMatchAmount0(discAmt);
                              currentTrans.setMixAndMatchCount0(1); break;
                    case '1': currentTrans.setMixAndMatchAmount1(discAmt);
                              currentTrans.setMixAndMatchCount1(1); break;
                    case '2': currentTrans.setMixAndMatchAmount2(discAmt);
                              currentTrans.setMixAndMatchCount2(1); break;
                    case '3': currentTrans.setMixAndMatchAmount3(discAmt);
                              currentTrans.setMixAndMatchCount3(1); break;
                    case '4': currentTrans.setMixAndMatchAmount4(discAmt);
                              currentTrans.setMixAndMatchCount4(1); break;
                    case '5': currentTrans.setMixAndMatchAmount5(discAmt);
                              currentTrans.setMixAndMatchCount5(1); break;
                    case '6': currentTrans.setMixAndMatchAmount6(discAmt);
                              currentTrans.setMixAndMatchCount6(1); break;
                    case '7': currentTrans.setMixAndMatchAmount7(discAmt);
                              currentTrans.setMixAndMatchCount7(1); break;
                    case '8': currentTrans.setMixAndMatchAmount8(discAmt);
                              currentTrans.setMixAndMatchCount8(1); break;
                    case '9': currentTrans.setMixAndMatchAmount9(discAmt);
                              currentTrans.setMixAndMatchCount9(1); break;
                    }
                } else if (discNo.startsWith("U")) {
                    discAmt = discAmt.negate(); // 折扣金額存正數
                    switch (discNo.charAt(1)) {
                    case '0': currentTrans.setSIPercentOffAmount0(discAmt);
                              currentTrans.setSIPercentOffCount0(1); break;
                    case '1': currentTrans.setSIPercentOffAmount1(discAmt);
                              currentTrans.setSIPercentOffCount1(1); break;
                    case '2': currentTrans.setSIPercentOffAmount2(discAmt);
                              currentTrans.setSIPercentOffCount2(1); break;
                    case '3': currentTrans.setSIPercentOffAmount3(discAmt);
                              currentTrans.setSIPercentOffCount3(1); break;
                    case '4': currentTrans.setSIPercentOffAmount4(discAmt);
                              currentTrans.setSIPercentOffCount4(1); break;
                    case '5': currentTrans.setSIPercentOffAmount5(discAmt);
                              currentTrans.setSIPercentOffCount5(1); break;
                    case '6': currentTrans.setSIPercentOffAmount6(discAmt);
                              currentTrans.setSIPercentOffCount6(1); break;
                    case '7': currentTrans.setSIPercentOffAmount7(discAmt);
                              currentTrans.setSIPercentOffCount7(1); break;
                    case '8': currentTrans.setSIPercentOffAmount8(discAmt);
                              currentTrans.setSIPercentOffCount8(1); break;
                    case '9': currentTrans.setSIPercentOffAmount9(discAmt);
                              currentTrans.setSIPercentOffCount9(1); break;
                    }
                }
            }
//            if (GetProperty.getIsRoundDown("no").equalsIgnoreCase("yes")) {
//                Object[] lineItems = currentTrans.getLineItems();
//                HYIDouble tmpAmount = new HYIDouble(0);
//                System.out.println("size" + lineItems.length);
//                for (int i = 0; i < lineItems.length; i++) {
//                    LineItem li = (LineItem) lineItems[i];
//                    if (li.getDetailCode().equals("S"))
//                        tmpAmount = tmpAmount.addMe(((LineItem) lineItems[i])
//                                .getAfterDiscountAmount());
//                }
//
//                BigDecimal amount1 = new BigDecimal(tmpAmount.toString());
//                BigDecimal rdAmount = amount1.subtract(amount1.setScale(1,
//                        BigDecimal.ROUND_DOWN));
//                HYIDouble roundDownAmount = (new HYIDouble(rdAmount.doubleValue()))
//                        .negate();
//                if (roundDownAmount != new HYIDouble(0)) {
//                    LineItem roundDownItem = new LineItem();
//                    roundDownItem.setTerminalNumber(this.currentTrans.getTerminalNumber());
//                    roundDownItem.setPluNumber("rd");
//
//                    roundDownItem.setQuantity(new HYIDouble(1));
//                    roundDownItem.setUnitPrice(roundDownAmount);
//
//                    /*
//                     * jacky/2005-01-20/
//                     */
//                    roundDownItem.setItemNumber("rd");
//                    roundDownItem.setOriginalPrice(roundDownAmount);
//
//                    roundDownItem.setAmount(roundDownAmount);
//                    roundDownItem.setAfterDiscountAmount(roundDownAmount);
//                    roundDownItem.setDetailCode("A");
//                    roundDownItem.setDiscountType("A");
//                    roundDownItem.setCategoryNumber("007");
//                    currentTrans.setRoundDownItem(roundDownItem);
//                }
//            }

            currentTrans.setDetailCount(currentTrans.lineItems().size());
            currentTrans.initForAccum();

            // 不要重新整理組合促銷金額，因為上面已經算好了
            currentTrans.accumulate(false);

            //// 计算发票金额
            //currentTrans.calcInvoiceAmount();

        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
            throw e;
        }
    }

    /**
     * 計算SI和組合促銷折扣金額，以便生成'D' or 'M'的折扣項line item.
     */
    private void calcNewDiscountAmount(Map<String, HYIDouble> discountMap, LineItem lineItem) {
        if (!StringUtils.isEmpty(lineItem.getDiscountNumber())) {
            String taxType = lineItem.getTaxType();
            TaxType taxTypeDef = TaxType.queryByTaxID(taxType);
            HYIDouble itemDiscountAmount = lineItem.getAfterDiscountAmount().subtract(lineItem.getAmount());

            // percMap: 先把這個item發生的"折扣"的"相應原折扣額"記到percMap,
            // 我們要按原來的折扣金額比例，來分攤新的折扣金額
            Map<String, HYIDouble> percMap = new HashMap<String, HYIDouble>();

            String[] discNos = lineItem.getDiscountNumber().split(",");
            HYIDouble denominator = HYIDouble.zero();
            for (String discNo : discNos) {
                if (discNo.startsWith("M")) {
                    String mmId = discNo.substring(1);
                    HYIDouble amount = currentTrans.getAppliedMMs().get(mmId);
                    percMap.put(discNo, amount);
                    denominator.addMe(amount);
                } else if (discNo.startsWith("S")) {
                    String siId = discNo.substring(1);
                    HYIDouble amount = currentTrans.getSIDiscountAmountById(siId);
                    percMap.put(discNo, amount);
                    denominator.addMe(amount);
                }
            }

            // 然後將這個item的折扣金額itemDiscountAmount按percMap記錄的各個折扣金額的比例，
            // 分攤並累加到discountMap。將來會用discountMap來生成'D' or 'M'的折扣items。
            int numberOfDiscs = percMap.keySet().size();
            int idx = 0;
            HYIDouble accumDiscAmt = HYIDouble.zero();
            for (String discNo : percMap.keySet()) {
                HYIDouble toBeAddedDiscAmt;
                if (idx == numberOfDiscs - 1) { // last one
                    toBeAddedDiscAmt = itemDiscountAmount.subtract(accumDiscAmt);
                } else {
                    toBeAddedDiscAmt = itemDiscountAmount.multiply(percMap.get(discNo))
                        .divide(denominator, 2, BigDecimal.ROUND_HALF_UP);
                    accumDiscAmt.addMe(toBeAddedDiscAmt);
                }

                // 累加到discountMap
                accumDiscAmount(discountMap, discNo, toBeAddedDiscAmt);

                HYIDouble taxAmount = StateToolkit.calculateTaxAmt(taxTypeDef, toBeAddedDiscAmt, new HYIDouble(1));

                // For稅別SI和組合促銷折扣金額
                String taxedDiscNo; // 分稅別的折扣, T1: 稅別1的組合促銷金額, U1: 稅別1的SI折扣金額
                String taxAmountNo; // Y01: 編號01的組合促銷折扣稅額, Z02: 編號02的SI折扣稅額
                if (discNo.startsWith("M")) {
                    taxedDiscNo = "T" + taxType;
                    taxAmountNo = "Y" + discNo.substring(1);
                } else {
                    taxedDiscNo = "U" + taxType;
                    taxAmountNo = "Z" + discNo.substring(1);
                }
                accumDiscAmount(discountMap, taxedDiscNo, toBeAddedDiscAmt);
                accumDiscAmount(discountMap, taxAmountNo, taxAmount);

                idx++;
            }
        }
    }

    private void accumDiscAmount(Map<String, HYIDouble> discountMap, String discNo, HYIDouble toBeAddedDiscAmt) {
        HYIDouble origDisc = discountMap.get(discNo);
        if (origDisc == null)
            origDisc = HYIDouble.zero();
        origDisc.addMe(toBeAddedDiscAmt);
        discountMap.put(discNo, origDisc);
    }

    /**
     * refundAmount是退款金額, 根據transactionToBeRefunded的淨支付順序, 分配退款支付方式,
     * 並設置回currentTrans, 以做為顯示之用.
     */
    private void processReturnPayment(HYIDouble refundAmount) {
        Map<Payment, HYIDouble> paymentMap = new TreeMap<Payment, HYIDouble>(new PaymentComparator());
        HYIDouble zero = HYIDouble.zero();
        Transaction origTran = CreamSession.getInstance().getTransactionToBeRefunded();
        Map<Payment, HYIDouble> netPayment = origTran.getNetPayments();
        for (Payment payment : netPayment.keySet()) {
            HYIDouble amount = netPayment.get(payment);
            if (amount != null && !amount.equals(zero))
                paymentMap.put(payment, amount.negate());
        }

        HYIDouble remainAmount = refundAmount;
        int idx = 1;
        boolean over = false;
        for (Payment payment : paymentMap.keySet()) {
            HYIDouble payAmount = paymentMap.get(payment);
            if (remainAmount.compareTo(payAmount) < 0)
                remainAmount = remainAmount.subtract(payAmount);
            else {
                payAmount = remainAmount;
                over = true;
            }
            currentTrans.setPayNumberByID(idx, payment.getPaymentID());
            currentTrans.setPayAmountByID(idx, payAmount);
            idx++;
            if (over)
                break;
        }
        if (idx == 1) {
            currentTrans.setPayNumberByID(idx, "00");
            currentTrans.setPayAmountByID(idx, new HYIDouble(0));
            idx++;
        }
        for (; idx <= 4; idx++) {
            currentTrans.setPayNumberByID(idx, null);
            currentTrans.setPayAmountByID(idx, null);
        }
    }

    private String getDiscNoValueFromOriginalTransaction(String discNo) {
        if (StringUtils.isEmpty(discNo))
            return "";

        Transaction refundTran;
        if (ReturnNumber2State.originalPeiDaTransaction != null) // 原配送交易
            refundTran = ReturnNumber2State.originalPeiDaTransaction;
        else
            refundTran = oldTran;
        for (LineItem lineItem : refundTran.lineItems()) {
            if (discNo.equals(lineItem.getItemNumber()) && "D".equals(lineItem.getDetailCode())) {
                return lineItem.getDiscountNumber();
            }
        }
        return "";
    }

    public static boolean cmpayReturn(DbConnection connection, Transaction t, Transaction trans, POSTerminalApplication app, ResourceBundle res) {
        List list = t.getCmpayList();
        if (list == null)
            return true;
        String posno = trans.getTerminalNumber().toString();
        if (posno.length() < 2) {
            posno = "0" + posno;
        }

        String old_out_tran_no = "";
        String oldDate = "";
        String amt = "";
        for (int i = 0; i < list.size(); i++){
            Cmpay_detail a = (Cmpay_detail)list.get(i);
            old_out_tran_no = a.getOrderId();
            oldDate = new SimpleDateFormat("yyyyMMdd").format(a.getSYSTEMDATE());
            amt = a.getAmt().multiply(new HYIDouble(100)).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
        }
        Random random = new Random();
        int a = random.nextInt(10);
        String mid = a + new SimpleDateFormat("HHmmssSSS").format(new Date());
        String out_tran_no = trans.getStoreNumber() + posno + mid;
        app.getMessageIndicator().setMessage(res.getString("CmpayPayReturn"));
        String result = HttpHepler.refund(old_out_tran_no,oldDate,amt,out_tran_no, mid);
//        String result = "<?xml version=\"1.0\" encoding=\"GBK\"?><MPAY><HEAD><MCODE>101450</MCODE><MID>00070110221261</MID><DATE>20150626</DATE><TIME>110221</TIME><PAYDAY>20150626</PAYDAY><MSGPRO>SF</MSGPRO><VER>0001</VER><SCH>rp</SCH><MSGATR>00</MSGATR><RCODE>000000</RCODE><DESC>SUCCESS</DESC><SAFEFLG>00</SAFEFLG><MAC></MAC><REQSYS>888009941110054</REQSYS></HEAD><BODY><CNY_AMT>0</CNY_AMT><CMY_AMT>0</CMY_AMT><ECP_AMT>1980</ECP_AMT><VCH_AMT>0</VCH_AMT><PAY_NO>201506260227132944</PAY_NO><SIGN>52ae8951c3116b1454786134397f9a5b67d0b259055d9c9505551e40ad5b04dab9ee439373e9514c858bffcbb36d07eab7714f4eb7f60f38213b301e54051a16ac464cc46efb42f71367cf88b024d84acbf1e4a661289123948005e5e5df398f5483069ed464a42afc23e5a115920846184c7b897265c131fa65bd5e300b7d7a</SIGN></BODY></MPAY>";
        app.getMessageIndicator().setMessage("");
        if (result == null || result.equals("")) {
            app.getWarningIndicator().setMessage(res.getString("CmpayPayFailed"));
            CreamToolkit.logMessage(res.getString("CmpayPayFailed"));
            return false;
        }
        if (CreamToolkit.getValueFromXml(result, "<HEAD>","</HEAD>").equals("")) {
            app.getWarningIndicator().setMessage(result);
            return false;
        }
        if (!CreamToolkit.getValueFromXml(result, "<RCODE>", "</RCODE>").equals("000000")) {
            String desc = CreamToolkit.getValueFromXml(result, "<DESC>", "</DESC>");
            app.getWarningIndicator().setMessage(HttpHepler.unicodeToChinese(desc));
            return false;
        }
//        String payNo = CreamToolkit.getValueFromXml(result, "<PAY_NO>", "</PAY_NO>");//退货流水号
        for (int i = 0; i < list.size(); i++) {
            Cmpay_detail cmpay_detail = (Cmpay_detail)list.get(i);
            try{
                cmpay_detail.setSTORENUMBER(trans.getStoreNumber());
                cmpay_detail.setPOSNUMBER(trans.getTerminalNumber());
                cmpay_detail.setTRANSACTIONNUMBER(trans.getTransactionNumber());
                cmpay_detail.setZseq(trans.getZSequenceNumber());
                cmpay_detail.setAmt(cmpay_detail.getAmt().negate());
                cmpay_detail.setOrderId(old_out_tran_no);
                cmpay_detail.setmid(mid);
                cmpay_detail.setCoupamt(cmpay_detail.getCoupamt().negate());
                cmpay_detail.setVchamt(cmpay_detail.getVchamt().negate());
                cmpay_detail.setCashamt(cmpay_detail.getCashamt().negate());
                cmpay_detail.setSYSTEMDATE(new Date());
                cmpay_detail.insert(connection);
                CreamToolkit.logMessage("transactionNumber：" + cmpay_detail.getTRANSACTIONNUMBER() + "，tradeNo："
                        + cmpay_detail.getAmt()+ ",systemDate：" + cmpay_detail.getSYSTEMDATE().toString()
                        + " insert success");
            } catch (Exception e) {
                e.printStackTrace(CreamToolkit.getLogger());
                CreamToolkit.logMessage("Cmpay insert Fail");
                return false;
            }
        }
        return true;
    }

    public static  boolean alipayReturn(DbConnection connection,Transaction t,Transaction trans,POSTerminalApplication app,ResourceBundle res) {
        List list = t.getAlipayList();
        if (list == null)
            return true;
        String out_tran_no = "";
        HYIDouble amt = new HYIDouble(0);
        for (int i = 0; i < list.size(); i++){
            Alipay_detail a = (Alipay_detail)list.get(i);
            out_tran_no = a.getSTORENUMBER()+a.getPOSNUMBER()+a.getTRANSACTIONNUMBER()+a.getSEQ();
            amt = a.getTOTAL_FEE();
        }
        if (out_tran_no.equals(""))
            return true;
        app.getMessageIndicator().setMessage(res.getString("AlipayGetRefund"));
        AlipaySubmit.ALIPAY_GATEWAY_NEW = GetProperty.getAlipayURL();
        String xml = AlipayHttpPostRequest.getRefund(out_tran_no, amt.setScale(2, 4).toString(), "");//退货  其实是做撤销操作
//        String xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//                "<alipay>\n" +
//                "<is_success>T</is_success>\n" +
//                "<request>\n" +
//                "<param name=\"sign\">66b3a14ec95846f01d42564e6953c81b</param>\n" +
//                "<param name=\"_input_charset\">UTF-8</param>\n" +
//                "<param name=\"sign_type\">MD5</param>\n" +
//                "<param name=\"service\">alipay.acquire.cancel</param>\n" +
//                "<param name=\"partner\">2088101126765726</param>\n" +
//                "<param name=\"out_trade_no\">3406822113320232</param>\n" +
//                "</request>\n" +
//                "<response>\n" +
//                "<alipay>\n" +
//                "<result_code>SUCCESS</result_code>\n" +
//                "<out_trade_no>3406822113320232</out_trade_no>\n" +
//                "<retry_flag>N</retry_flag>\n" +
//                "<trade_no>2013111511001004390000105126</trade_no>\n" +
//                "</alipay>\n" +
//                "</response>\n" +
//                "<sign>3afc92ac4708425ab74ecb2c4e58ef56</sign>\n" +
//                "<sign_type>MD5</sign_type>\n" +
//                "</alipay>";//退货  其实是做撤销操作
        if (xml == null || xml.equals("")) {
            CreamToolkit.logMessage("WanOfflineCanNotTrade... ");
            app.getWarningIndicator().setMessage(res.getString("WanOfflineCanNotTrade"));
            return false;
        }
        String isSuccess  = CreamToolkit.getValueFromXml(xml,"<is_success>","</is_success>");
        if (isSuccess.equals("T")) {
            String resultCode = CreamToolkit.getValueFromXml(xml,"<result_code>","</result_code>");
            if (resultCode.equals("SUCCESS")) {
                String trade_no = CreamToolkit.getValueFromXml(xml,"<trade_no>","</trade_no>");
                if (trade_no.equals("")) {
                    app.getWarningIndicator().setMessage(res.getString("AlipayRefundFail"));
                    return false;
                }
                if(makeNegativeValue(connection,list, trade_no,trans))
                    return true;
            } else {
                String error =CreamToolkit.getValueFromXml(xml,"<detail_error_code>","</detail_error_code>");
                app.getWarningIndicator().setMessage(AlipayHttpPostRequest.getErrorMessage(error));
                return false;
            }
        } else {
            String error = CreamToolkit.getValueFromXml(xml,"<error>","</error>");
            app.getWarningIndicator().setMessage(AlipayHttpPostRequest.getErrorMessage(error));
            return false;
        }
        return true;
    }

    public static  boolean unionPayReturn(DbConnection connection,Transaction t,Transaction trans,POSTerminalApplication app,ResourceBundle res) {
        List list = t.getUnionPayList();
        if (list == null || list.size() == 0) {
            return true;
        }

        String origOrderId = "";
        String origQryId = "";
        String total_fee = "";
        String origTxnTime = "";
        for (int i = 0; i < list.size(); i++){
            UnionPay_detail a = (UnionPay_detail)list.get(i);
            origOrderId = a.getOrderId();
            origQryId = a.getQueryId();
            total_fee = a.getTOTAL_FEE().multiply(new HYIDouble(100)).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
            origTxnTime = a.getTxnTime();
        }
        if (origOrderId.equals(""))
            return true;
        String seqStr = "01";
        app.getMessageIndicator().setMessage(res.getString("UnionPayGetRefund"));
        String xml = UnionPayRefund.doPost(total_fee,origQryId,origOrderId,origTxnTime);

        /*if (xml == null || xml.equals("")) {
            return getRefundQuery(connection,transaction_id,out_tran_no,out_refund_no,app,total_fee,seqStr,openId,trans);
        }*/
        String return_code  = CreamToolkit.getValueFromXml(xml,"<respCode>","</respCode>");
        if (return_code.equals("00")) {
            String refund_id = CreamToolkit.getValueFromXml(xml,"<orderId>","</orderId>");
            String txnTime = CreamToolkit.getValueFromXml(xml,"<txnTime>","</txnTime>");
            String origQryIds = CreamToolkit.getValueFromXml(xml,"<origQryId>","</origQryId>");
            if(unionPayMakeNegativeValue(connection,total_fee,txnTime,seqStr,origQryIds,refund_id,trans)) {
                return true;
            }
        } else {
            String return_msg = CreamToolkit.getValueFromXml(xml,"<respMsg>","</respMsg>");
            CreamToolkit.logMessage(return_msg);
            app.getWarningIndicator().setMessage(return_msg);
            return false;
        }
        return false;
    }

    public static boolean unionPayMakeNegativeValue(DbConnection connection,String total_fee, String txnTime, String seq, String origQryId, String refundId, Transaction trans) {
        UnionPay_detail unionPay_detail;
        try{
            HYIDouble amt = new HYIDouble(total_fee).divide(new HYIDouble(100.0), 2).setScale(2, BigDecimal.ROUND_HALF_UP);
            unionPay_detail = new UnionPay_detail();
            unionPay_detail.setSTORENUMBER(trans.getStoreNumber());
            unionPay_detail.setPOSNUMBER(trans.getTerminalNumber());
            unionPay_detail.setTRANSACTIONNUMBER(trans.getTransactionNumber());
            unionPay_detail.setTOTAL_FEE(amt.negate());
            unionPay_detail.setSEQ(seq);
            unionPay_detail.setSYSTEMDATE(new Date());
            unionPay_detail.setZSEQ(trans.getZSequenceNumber());
            unionPay_detail.setQueryId(origQryId != null ? origQryId : "");
            unionPay_detail.setMerId(Store.getSdkMerId());
            unionPay_detail.setOrderId(refundId != null ? refundId : "");
            unionPay_detail.setTxnTime(txnTime != null ? txnTime : "");
            unionPay_detail.insert(connection);
            CreamToolkit.logMessage("transactionNumber：" + unionPay_detail.getTRANSACTIONNUMBER() + "，transaction_id："
                    + unionPay_detail.getTOTAL_FEE()+ ",systemDate：" + unionPay_detail.getSYSTEMDATE().toString()
                    + " is add trans.addUnionPayDetailList");
            return true;
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
            CreamToolkit.logMessage("UnionPay process Fail");
        }
        return true;
    }

    public static  boolean weiXinReturn(DbConnection connection,Transaction t,Transaction trans,POSTerminalApplication app,ResourceBundle res) {
        List list = t.getWeiXinList();
        if (list == null || list.size() == 0) {
            return true;
        }

        String out_tran_no = "";
        String transaction_id = "";
        String total_fee = "";
        String openId = "";
        for (int i = 0; i < list.size(); i++){
            WeiXin_detail a = (WeiXin_detail)list.get(i);
            out_tran_no = a.getSTORENUMBER()+a.getPOSNUMBER()+a.getTRANSACTIONNUMBER()+a.getSEQ();
            transaction_id = a.getTRANSACTION_ID();
            total_fee = a.getTOTAL_FEE().multiply(new HYIDouble(100)).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
            openId = a.getOPENID();
        }
        if (out_tran_no.equals(""))
            return true;
        String seqStr = "01";
        String out_refund_no = trans.getStoreNumber()+trans.getTerminalNumber()+trans.getTransactionNumber()+seqStr;
        app.getMessageIndicator().setMessage(res.getString("WeiXinGetRefund"));
        String xml = WeiXinHttpPostRequest.refund(out_tran_no, transaction_id, out_refund_no, total_fee, total_fee, trans.getCashierNumber());//退款

        if (xml == null || xml.equals("")) {
            return getRefundQuery(connection,transaction_id,out_tran_no,out_refund_no,app,total_fee,seqStr,openId,trans);
        }
        String return_code  = CreamToolkit.getValueFromXml(xml,"<return_code><![CDATA[","]]></return_code>");
        if (return_code.equals("SUCCESS")) {
            String resultCode = CreamToolkit.getValueFromXml(xml,"<result_code><![CDATA[","]]></result_code>");
            if (resultCode.equals("SUCCESS")) {
                String refund_id = CreamToolkit.getValueFromXml(xml,"<refund_id><![CDATA[","]]></refund_id>");
                if(weiXinMakeNegativeValue(connection,total_fee,transaction_id,seqStr,openId,refund_id,trans)) {
                    return true;
                }
            } else {
                String err_code = CreamToolkit.getValueFromXml(xml,"<err_code><![CDATA[","]]></err_code>");
                if (err_code.equals("SYSTEMERROR")) {//如果返回systemerror，则再查询一次
                    return getRefundQuery(connection,transaction_id,out_tran_no,out_refund_no,app,total_fee,seqStr,openId,trans);
                }
                String err_code_des = CreamToolkit.getValueFromXml(xml,"<err_code_des><![CDATA[","]]></err_code_des>");
                CreamToolkit.logMessage(err_code_des);
                app.getWarningIndicator().setMessage(err_code_des);
            }
        } else {
            String return_msg = CreamToolkit.getValueFromXml(xml,"<return_msg><![CDATA[","]]></return_msg>");
            CreamToolkit.logMessage(return_msg);
            app.getWarningIndicator().setMessage(return_msg);
            return false;
        }
        return false;
    }

    //微信退款查询
    public static boolean getRefundQuery(DbConnection connection,String transaction_id, String out_tran_no, String out_refund_no, POSTerminalApplication app, String total_fee, String seqStr, String openId, Transaction trans) {
        String xml = WeiXinHttpPostRequest.refundquery(transaction_id,out_tran_no, out_refund_no);//退款查询
        if (xml == null) {
            CreamToolkit.logMessage("WanOfflineCanNotTrade... ");
            app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WanOfflineCanNotTrade"));
            return false;
        }
        String return_code  = CreamToolkit.getValueFromXml(xml,"<return_code><![CDATA[","]]></return_code>");
        if (return_code.equals("SUCCESS")) {
            String resultCode = CreamToolkit.getValueFromXml(xml, "<result_code><![CDATA[", "]]></result_code>");
            if (resultCode.equals("SUCCESS")) {
                String refund_status_0 = CreamToolkit.getValueFromXml(xml,"<refund_status_0><![CDATA[","]]></refund_status_0>");
                if (refund_status_0.equals("SUCCESS") || refund_status_0.equals("PROCESSING")) {//返回PROCESSING也当做成功处理
                    String refund_id = CreamToolkit.getValueFromXml(xml, "<refund_id_0><![CDATA[", "]]></refund_id_0>");
                    if (weiXinMakeNegativeValue(connection,total_fee, transaction_id, seqStr, openId, refund_id, trans)) {
                        return true;
                    }
                } else {
                    CreamToolkit.logMessage(getRefund_status(refund_status_0));
                    app.getWarningIndicator().setMessage(getRefund_status(refund_status_0));
                    return false;
                }
            } else {
                String err_code_des = CreamToolkit.getValueFromXml(xml,"<err_code_des><![CDATA[","]]></err_code_des>");
                CreamToolkit.logMessage(err_code_des);
                app.getWarningIndicator().setMessage(err_code_des);
                return false;
            }
        } else {
            String return_msg = CreamToolkit.getValueFromXml(xml,"<return_msg><![CDATA[","]]></return_msg>");
            CreamToolkit.logMessage(return_msg);
            app.getWarningIndicator().setMessage(return_msg);
            return false;
        }
        return false;
    }

    public static boolean weiXinMakeNegativeValue(DbConnection connection,String total_fee, String transactionId, String seq, String openId, String refundId, Transaction trans) {
        WeiXin_detail weiXin_detail;
        try{
            HYIDouble amt = new HYIDouble(total_fee).divide(new HYIDouble(100.0), 2).setScale(2, BigDecimal.ROUND_HALF_UP);
            weiXin_detail = new WeiXin_detail();
            weiXin_detail.setSTORENUMBER(trans.getStoreNumber());
            weiXin_detail.setPOSNUMBER(trans.getTerminalNumber());
            weiXin_detail.setTRANSACTIONNUMBER(trans.getTransactionNumber());
            weiXin_detail.setTOTAL_FEE(amt.negate());
            weiXin_detail.setTRANSACTION_ID(transactionId);
            weiXin_detail.setREFUND_ID(refundId);
            weiXin_detail.setSEQ(seq);
            weiXin_detail.setOPENID(openId);
            weiXin_detail.setSYSTEMDATE(new Date());
            weiXin_detail.setZSEQ(trans.getZSequenceNumber());
            weiXin_detail.insert(connection);
            CreamToolkit.logMessage("transactionNumber：" + weiXin_detail.getTRANSACTIONNUMBER() + "，transaction_id："
                    + weiXin_detail.getTOTAL_FEE()+ ",systemDate：" + weiXin_detail.getSYSTEMDATE().toString()
                    + " is add trans.addWeiXinDetailList");
            return true;
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
            CreamToolkit.logMessage("WeiXin process Fail");
        }
        return true;
    }

    public static String getRefund_status(String refund_status_0) {
        //SUCCES--退款成功 FAIL--退款失败 PROCESSING--退款处理中 NOTSURE--未确定，需要商户原退款单号重新发起
        // CHANGE--转入代发，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，资金回流到商户的现金帐号，需要商户人工干预，通过线下或者财付通转账的方式进行退款。
        if (refund_status_0.equals("FAIL"))
            return "退款失败";
        if (refund_status_0.equals("NOTSURE"))
            return "未确定，需要商户原退款单号重新发起";
        if (refund_status_0.equals("CHANGE"))
            return "转入代发，需要商户人工干预";
        return "";
    }

    public static boolean makeNegativeValue(DbConnection connection, List list, String trade_no, Transaction trans) {
        for (int i = 0; i < list.size(); i++) {
            Alipay_detail alipay_detail = (Alipay_detail)list.get(i);
            try{
                alipay_detail.setSTORENUMBER(trans.getStoreNumber());
                alipay_detail.setPOSNUMBER(trans.getTerminalNumber());
                alipay_detail.setTRANSACTIONNUMBER(trans.getTransactionNumber());
                alipay_detail.setZSeq(trans.getZSequenceNumber());
                alipay_detail.setTOTAL_FEE(alipay_detail.getTOTAL_FEE().negate());
                alipay_detail.setSEQ(alipay_detail.getSEQ());
                alipay_detail.setBUYER_LOGON_ID(alipay_detail.getBUYER_LOGON_ID());
                alipay_detail.setTRADE_NO(trade_no);
                alipay_detail.setSYSTEMDATE(new Date());
                alipay_detail.insert(connection);
                CreamToolkit.logMessage("transactionNumber：" + alipay_detail.getTRANSACTIONNUMBER() + "，tradeNo："
                        + alipay_detail.getTOTAL_FEE()+ ",systemDate：" + alipay_detail.getSYSTEMDATE().toString()
                        + " insert success");
            } catch (Exception e) {
                e.printStackTrace(CreamToolkit.getLogger());
                CreamToolkit.logMessage("Alipay insert Fail");
                return false;
            }
        }
        return true;
    }

}
