package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.PLU;
import hyi.cream.dac.Reason;
import hyi.cream.dac.SI;
import hyi.cream.dac.TaxType;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.uibeans.SIButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class SIDiscountState extends State {
    private static SIDiscountState discountState;
    private boolean slipping;
    private SI siDac;
    private Class exitState;
    private TaxType discountTaxtype;

    public static SIDiscountState getInstance() {
        try {
            if (discountState == null) {
                discountState = new SIDiscountState();
            }
        } catch (InstantiationException ex) {
        }
        return discountState;
    }

    public SIDiscountState() throws InstantiationException {
        String typeId = PARAM.getDiscountTaxId();
        if (typeId != null) {
            discountTaxtype = TaxType.queryByTaxID(typeId.trim());
        }
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof SummaryState && event.getSource() instanceof SIButton) {
            SIButton si = (SIButton) event.getSource();
            siDac = SI.queryBySIID(si.getSiID());
            if (siDac == null || !siDac.isAfterSI()) // || !siDac.isValid())
                return;
            if (siDac.getType() == SI.DISC_TYPE_INPUT_PERCENTAGE ||
                siDac.getType() == SI.DISC_TYPE_INPUT_DISCOUNT_AMOUNT) {
                exitState = DiscountInputState.class;
                DiscountInputState.inputType = siDac.getType();
                return;
            }
            processDiscount(siDac);

        } else if (sourceState instanceof DiscountInputState) {
            HYIDouble rate = ((DiscountInputState) sourceState).getDiscountRate();
            if (rate != null) {
                siDac.setValue(rate);
                processDiscount(siDac);
            }
            exitState = SummaryState.class;
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //If this SI is需认证，return Slipping2State.class,
        if (exitState != null && !exitState.equals(SummaryState.class))
            return exitState;

        POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        posTerminal.getPayingPane().repaint();
        posTerminal.getPayingPane().setVisible(true);
        if (slipping) {
            slipping = false;
            return Slipping2State.class;
        }

        return SummaryState.class;

    } //KeylockWarningState Keylock

    void processDiscount(SI siDac) {
        processDiscount(siDac, null, null, false, false);
    }

    /**
     * @param siDac 折扣类型
     * @param discountAmount 固定或已知金额的折扣。现在用在信用卡红利折抵金额。
     * @param trans 要做折扣的交易
     * @param checkLineItemDiscount　是不是从LineItem中检查折扣是否符合
     */
    public void processDiscount(SI siDac, HYIDouble discountAmount, Transaction trans, boolean checkLineItemDiscount, boolean isReturn) {
        if (trans == null)
            trans = getCurrentTransaction();

        // 如果已經有任何應用的小計後折扣，則不能重覆折扣
        if (trans.getAppliedSIs().size() > 0)
            return;
        
        LineItem lineItem;
        HYIDouble summary = HYIDouble.zero();
        HYIDouble afterSISummary = HYIDouble.zero();
        //boolean siCompatible = false;
        int decimalBits = siDac.getAttribute().charAt(1) - '0';
        HYIDouble totalSIAmt;
        boolean base = false;
        int roundingMode;
        if (siDac.getBase().equals("R"))
            base = true;
        if (isReturn)
            base = true;

        boolean creditCardBonus = siDac.getType() == SI.DISC_TYPE_BONUS;
        boolean inputDiscountAmount = siDac.getType() == SI.DISC_TYPE_INPUT_DISCOUNT_AMOUNT;

        ////////////////////////////
        // CHAR[0]: 四舍五入flag
        //    ‘R’ - 四舍五入
        //    ‘C’ - 进位
        //    ‘D’ - 舍去
        // CHAR[1]:小数位数
        // CHAR[2]:SI相容
        // CHAR[3]:浮动%
        // CHAR[4]:认证
        // CHAR[5]:M&M相容
        // CHAR[6]:
        // CHAR[7]:'A'小计后折扣
        ////////////////////////////

        // 四舍五入flag
        if (siDac.getAttribute().charAt(0) == 'R')
            roundingMode = BigDecimal.ROUND_HALF_UP;
        else if (siDac.getAttribute().charAt(0) == 'C')
            roundingMode = BigDecimal.ROUND_UP;
        else
            roundingMode = BigDecimal.ROUND_DOWN;

        //// SI相容: 此折扣是否容得下和別人一起折扣
        //siCompatible = !siDac.isOnlyAllowStandalone();

        // Slip needed or not
        if (siDac.getAttribute().charAt(4) == '1')
            slipping = true;
        else
            slipping = false;

        Object[] lineItemArrayLast = trans.getLineItems();
        // Preventing duplicate discount
        for (int i = 0; i < lineItemArrayLast.length; i++) {
            lineItem = (LineItem) lineItemArrayLast[i];
            if (lineItem.getDetailCode().equals("D")) {
                //if (!siCompatible) {
                //    slipping = false;
                //    POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                //            CreamToolkit.getString("SIDiscountWarning"));
                //    return;
                //}
                String thatSiId = lineItem.getPluNumber();
                if (thatSiId.equals(siDac.getSIID())) { // already exists me
                    slipping = false;
                    return;
                }
                //SI thatSi = SI.queryBySIID(thatSiId);
                //if (thatSi.isOnlyAllowStandalone()) { // 如果已經存在一個只能單獨折扣的SI
                //    slipping = false;
                //    return;
                //}
            }
        }

        List needDiscount = new ArrayList();

        Object[] normalLineItemArray = trans.getLineItemsSortedByAmount();
        for (int i = 0; i < normalLineItemArray.length; i++) {
            lineItem = (LineItem) normalLineItemArray[i];
            if (lineItem.getAfterSIAmount() == null) {
                lineItem.setAfterSIAmount(lineItem.getAmount());
            }
            if (lineItem.getAfterDiscountAmount() == null) {
                lineItem.setAfterDiscountAmount(lineItem.getAmount());
            }

            if (inputDiscountAmount || // 若為手輸折扣金額, 則折扣時不排除任何商品
                (checkLineItemDiscount? lineItem.isDiscountedBySI(siDac): siDac.contains(lineItem))) {

                if (!inputDiscountAmount && !creditCardBonus && !checkLineItemDiscount
                    //Bruce/20081027/ "D" measn 此商品已經做了促銷期間特價，不能再行折扣
                    //Bruce/20081027/ "O" measn 此商品已經做了變價，不能再行折扣
                    && ("O".equals(lineItem.getDiscountType()) ||
                        "D".equals(lineItem.getDiscountType()) ||
                        lineItem.getDiscountNumber() != null)) {
                    //&& !siCompatible) {
                    continue;
                }

//              PLU plu = null;
//              if (StringUtils.isEmpty(lineItem.getPluNumber())) { // 部门商品也能打折
//                  if (lineItem.getCategoryNumber().equals("9"))
//                      continue;
//              } else {
//                  plu = PLU.queryBarCode(lineItem.getPluNumber());
//                  if (plu == null || plu.getCategoryNumber().equals("9")) {
//                      continue;
//                  }
//              }
//
//              if (plu != null) {
//                  // 33=不能使用si折扣的商品
//                  Iterator it = Reason.queryByreasonCategory("33");
//                  while (it != null && it.hasNext()) {
//                      Reason reason = (Reason) it.next();
//                      if (reason.getreasonName() != null &&
//                              reason.getreasonName().equals(plu.getPluNumber()))
//                          continue;
//                  }
//              }

                needDiscount.add(i);
                if (isReturn)
                    lineItem.setAfterSIAmount(lineItem.getAmount());

                //summary: 用於計算折扣金額的總商品銷售金額
                if (base)
                    summary = summary.addMe(lineItem.getAmount());
                else
                    summary = summary.addMe(lineItem.getAfterSIAmount());

                // afterSISummary: 用于计算折扣后实际总金额
                afterSISummary = afterSISummary.addMe(lineItem.getAfterSIAmount());   
            }
        }

        if (discountAmount != null)
            totalSIAmt = discountAmount;
        else
            totalSIAmt = siDac.computeDiscntValue(summary); // totalSIAmt is a negative value usually

        HYIDouble actualAmt = HYIDouble.zero();
        actualAmt = actualAmt.addMe(afterSISummary).addMe(totalSIAmt);
        // actualAmt: 折扣商品的總額減去折扣金額後的淨額

        HYIDouble accumulatedDiscountAmount = HYIDouble.zero();
        for (int i = 0; i < needDiscount.size(); i++) {
            lineItem = (LineItem) normalLineItemArray[(Integer) needDiscount.get(i)];
            HYIDouble percent;

            if (inputDiscountAmount || // 若為手輸折扣金額, 則折扣時不排除任何商品
                (checkLineItemDiscount? lineItem.isDiscountedBySI(siDac): siDac.contains(lineItem))) {
                // That means the item has got discounted 
                if (!creditCardBonus && !checkLineItemDiscount
                    && lineItem.getDiscountNumber() != null) {
                        //&& !siCompatible) {
                    continue;
                }
                
                if (lineItem.getDiscountType() == null) {
                    lineItem.setDiscountType("S"); // SI折扣
                }

                if (!checkLineItemDiscount) {
                    //DISCNO 字段形式　如：Mxxx,Sxx,Sxx 
                    if (lineItem.getDiscountNumber() != null) {
                        lineItem.setDiscountNumber("S" + siDac.getSIID() + "," + lineItem.getDiscountNumber());
                    } else {
                        lineItem.setDiscountNumber("S" + siDac.getSIID());
                    }
                }

                if (i == needDiscount.size() - 1) { // last one
                    HYIDouble itemDiscAmt = totalSIAmt.subtract(accumulatedDiscountAmount);

                    lineItem.setSiAmount(itemDiscAmt);
                    accumulate(itemDiscAmt, lineItem, trans, siDac);
                    HYIDouble afterDiscAmt = lineItem.getAfterSIAmount().addMe(itemDiscAmt);
                    lineItem.setAfterSIAmount(afterDiscAmt);
                    lineItem.setAfterDiscountAmount(afterDiscAmt);
                    break;
                }

                if (base)
                    percent = lineItem.getAmount().divide(summary, decimalBits + 5, roundingMode);
                else
                    percent = lineItem.getAfterSIAmount().divide(summary, decimalBits + 5, roundingMode);

                HYIDouble itemDiscAmt = totalSIAmt.multiply(percent).setScale(decimalBits, roundingMode);

                if (accumulatedDiscountAmount.add(itemDiscAmt).compareTo(totalSIAmt) < 0) {
                    // e.g. 1, 總的折扣金額totalSIAmt=-10, 目前累計的折扣額accumulatedDiscountAmount=-9,
                    // 此單品按比例分攤的理論折扣金額itemDiscAmt=-2, 那麼 -10 + (-2) < -10 (按2元折扣下去會超出總的折扣額),
                    // 所以此單品的實際分攤折扣金額itemDiscAmt = -10 - (-9）= -1
                    //
                    // e.g. 2, 總的折扣金額totalSIAmt=-10, 目前累計的折扣額accumulatedDiscountAmount=-10,
                    // 此單品按比例分攤的理論折扣金額itemDiscAmt=-2, 那麼 -10 + (-2) < -10 (按2元折扣下去會超出總的折扣額),
                    // 所以此單品的實際分攤折扣金額itemDiscAmt = -10 - (-10）= 0 
                    itemDiscAmt = totalSIAmt.subtract(accumulatedDiscountAmount);
                }

                lineItem.setSiAmount(itemDiscAmt);
                accumulate(itemDiscAmt, lineItem, trans, siDac);

                HYIDouble afterDiscAmt = lineItem.getAfterSIAmount().addMe(itemDiscAmt);
                lineItem.setAfterSIAmount(afterDiscAmt);
                lineItem.setAfterDiscountAmount(afterDiscAmt);
                accumulatedDiscountAmount.addMe(itemDiscAmt);
            }
        }
        
        LineItem siLineItem = new LineItem();
        siLineItem.setTransactionNumber(trans.getTransactionNumber());
        siLineItem.setTerminalNumber(trans.getTerminalNumber());
        siLineItem.setDetailCode("D");
        siLineItem.setPluNumber(siDac.getSIID());
        siLineItem.setItemNumber(siDac.getSIID());
        siLineItem.setRemoved(false);
        siLineItem.setDescription(siDac.getPrintName());
        siLineItem.setUnitPrice(totalSIAmt);
        siLineItem.setOriginalPrice(totalSIAmt);
        siLineItem.setQuantity(new HYIDouble(1));
        siLineItem.setAmount(totalSIAmt);

        if (discountTaxtype != null){
            siLineItem.setTaxType(discountTaxtype.getTaxID());
            //TODO 这行是否可以不做？
            //siLineItem.caculateAndSetTaxAmount();
        }

        // 保存折扣值
        siLineItem.setDiscountNumber(siDac.getValue().setScale(2).toString());
        try {
            trans.addAppliedSI(siDac, totalSIAmt);
            trans.addLineItem(siLineItem, false);
        } catch (TooManyLineItemsException te) {
            CreamToolkit.logMessage(te.toString());
        }

    }

    private void accumulate(HYIDouble shareAmt, LineItem lineItem, Transaction cTransac, SI siDac) {
        if (cTransac == null) {
            cTransac = POSTerminalApplication.getInstance().getCurrentTransaction();
        } 
        
        switch (siDac.getType()) {
        case SI.DISC_TYPE_DISCOUNT_PERCENTAGE:      // 折扣（百分比）
        case SI.DISC_TYPE_INPUT_PERCENTAGE:         // 开放折扣
        case SI.DISC_TYPE_INPUT_DISCOUNT_AMOUNT:    // 开放折讓
        case SI.DISC_TYPE_DISCOUNT_AMOUNT:          // 折让
        case SI.DISC_TYPE_FREE:                     // 全折
        case SI.DISC_TYPE_BONUS:                    // 信用卡红利折抵
            if (lineItem.getTaxType().equals("0")) {
                cTransac.getSIPercentOffAmount0().addMe(shareAmt.abs());
                cTransac.setSIPercentOffCount0(cTransac.getSIPercentOffCount0().intValue() + 1);
            } else if (lineItem.getTaxType().equals("1")) {
                cTransac.getSIPercentOffAmount1().addMe(shareAmt.abs());
                cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);
            } else if (lineItem.getTaxType().equals("2")) {
                cTransac.getSIPercentOffAmount2().addMe(shareAmt.abs());
                cTransac.setSIPercentOffCount2(cTransac.getSIPercentOffCount2().intValue() + 1);
            } else if (lineItem.getTaxType().equals("3")) {
                cTransac.getSIPercentOffAmount3().addMe(shareAmt.abs());
                cTransac.setSIPercentOffCount3(cTransac.getSIPercentOffCount3().intValue() + 1);
            } else if (lineItem.getTaxType().equals("4")) {
                cTransac.getSIPercentOffAmount4().addMe(shareAmt.abs());
                cTransac.setSIPercentOffCount4(cTransac.getSIPercentOffCount4().intValue() + 1);
            } else if (lineItem.getTaxType().equals("5")) {
                cTransac.getSIPercentOffAmount5().addMe(shareAmt.abs());
                cTransac.setSIPercentOffCount5(cTransac.getSIPercentOffCount5().intValue() + 1);
            } else if (lineItem.getTaxType().equals("6")) {
                cTransac.getSIPercentOffAmount6().addMe(shareAmt.abs());
                cTransac.setSIPercentOffCount6(cTransac.getSIPercentOffCount6().intValue() + 1);
            } else if (lineItem.getTaxType().equals("7")) {
                cTransac.getSIPercentOffAmount7().addMe(shareAmt.abs());
                cTransac.setSIPercentOffCount7(cTransac.getSIPercentOffCount7().intValue() + 1);
            } else if (lineItem.getTaxType().equals("8")) {
                cTransac.getSIPercentOffAmount8().addMe(shareAmt.abs());
                cTransac.setSIPercentOffCount8(cTransac.getSIPercentOffCount8().intValue() + 1);
            } else if (lineItem.getTaxType().equals("9")) {
                cTransac.getSIPercentOffAmount9().addMe(shareAmt.abs());
                cTransac.setSIPercentOffCount9(cTransac.getSIPercentOffCount9().intValue() + 1);
            } else if (lineItem.getTaxType().equals("10")) {
                cTransac.getSIPercentOffAmount10().addMe(shareAmt.abs());
                cTransac.setSIPercentOffCount10(cTransac.getSIPercentOffCount10().intValue() + 1);
            }
            break;

          //Bruce/20080913/ 不能記錄在tranhead.SIMAMT[0..4]，因為目前後端沒有對應欄位
//        case SI.DISC_TYPE_DISCOUNT_AMOUNT:     // 折让
//        case SI.DISC_TYPE_FREE:                // 全折
//        case SI.DISC_TYPE_BONUS:               // 信用卡红利折抵
//            if (lineItem.getTaxType().equals("0")) {
//                cTransac.setSIDiscountAmount0(cTransac.getSIDiscountAmount0().addMe(shareAmt.abs()));
//                cTransac.setSIDiscountCount0(cTransac.getSIDiscountCount0().intValue() + 1);
//            } else if (lineItem.getTaxType().equals("1")) {
//                cTransac.setSIDiscountAmount1(cTransac.getSIDiscountAmount1().addMe(shareAmt.abs()));
//                cTransac.setSIDiscountCount1(cTransac.getSIDiscountCount1().intValue() + 1);
//            } else if (lineItem.getTaxType().equals("2")) {
//                cTransac.setSIDiscountAmount2(cTransac.getSIDiscountAmount2().addMe(shareAmt.abs()));
//                cTransac.setSIDiscountCount2(cTransac.getSIDiscountCount2().intValue() + 1);
//            } else if (lineItem.getTaxType().equals("3")) {
//                cTransac.setSIDiscountAmount3(cTransac.getSIDiscountAmount3().addMe(shareAmt.abs()));
//                cTransac.setSIDiscountCount3(cTransac.getSIDiscountCount3().intValue() + 1);
//            } else if (lineItem.getTaxType().equals("4")) {
//                cTransac.setSIDiscountAmount4(cTransac.getSIDiscountAmount4().addMe(shareAmt.abs()));
//                cTransac.setSIDiscountCount4(cTransac.getSIDiscountCount4().intValue() + 1);
//            }
//            break;

        case SI.DISC_TYPE_ADDON_AMOUNT:        // 加成（百分比）
            if (lineItem.getTaxType().equals("0")) {
                cTransac.setSIPercentPlusAmount0(cTransac.getSIPercentPlusAmount0().addMe(shareAmt.abs()));
                cTransac.setSIPercentPlusCount0(cTransac.getSIPercentPlusCount0().intValue() + 1);
            } else if (lineItem.getTaxType().equals("1")) {
                cTransac.setSIPercentPlusAmount1(cTransac.getSIPercentPlusAmount1().addMe(shareAmt.abs()));
                cTransac.setSIPercentPlusCount1(cTransac.getSIPercentPlusCount1().intValue() + 1);
            } else if (lineItem.getTaxType().equals("2")) {
                cTransac.setSIPercentPlusAmount2(cTransac.getSIPercentPlusAmount2().addMe(shareAmt.abs()));
                cTransac.setSIPercentPlusCount2(cTransac.getSIPercentPlusCount2().intValue() + 1);
            } else if (lineItem.getTaxType().equals("3")) {
                cTransac.setSIPercentPlusAmount3(cTransac.getSIPercentPlusAmount3().addMe(shareAmt.abs()));
                cTransac.setSIPercentPlusCount3(cTransac.getSIPercentPlusCount3().intValue() + 1);
            } else if (lineItem.getTaxType().equals("4")) {
                cTransac.setSIPercentPlusAmount4(cTransac.getSIPercentPlusAmount4().addMe(shareAmt.abs()));
                cTransac.setSIPercentPlusCount4(cTransac.getSIPercentPlusCount4().intValue() + 1);
            } else if (lineItem.getTaxType().equals("5")) {
                cTransac.setSIPercentPlusAmount5(cTransac.getSIPercentPlusAmount5().addMe(shareAmt.abs()));
                cTransac.setSIPercentPlusCount5(cTransac.getSIPercentPlusCount5().intValue() + 1);
            } else if (lineItem.getTaxType().equals("6")) {
                cTransac.setSIPercentPlusAmount6(cTransac.getSIPercentPlusAmount6().addMe(shareAmt.abs()));
                cTransac.setSIPercentPlusCount6(cTransac.getSIPercentPlusCount6().intValue() + 1);
            } else if (lineItem.getTaxType().equals("7")) {
                cTransac.setSIPercentPlusAmount7(cTransac.getSIPercentPlusAmount7().addMe(shareAmt.abs()));
                cTransac.setSIPercentPlusCount7(cTransac.getSIPercentPlusCount7().intValue() + 1);
            } else if (lineItem.getTaxType().equals("8")) {
                cTransac.setSIPercentPlusAmount8(cTransac.getSIPercentPlusAmount8().addMe(shareAmt.abs()));
                cTransac.setSIPercentPlusCount8(cTransac.getSIPercentPlusCount8().intValue() + 1);
            } else if (lineItem.getTaxType().equals("9")) {
                cTransac.setSIPercentPlusAmount9(cTransac.getSIPercentPlusAmount9().addMe(shareAmt.abs()));
                cTransac.setSIPercentPlusCount9(cTransac.getSIPercentPlusCount9().intValue() + 1);
            } else if (lineItem.getTaxType().equals("10")) {
                cTransac.setSIPercentPlusAmount10(cTransac.getSIPercentPlusAmount10().addMe(shareAmt.abs()));
                cTransac.setSIPercentPlusCount10(cTransac.getSIPercentPlusCount10().intValue() + 1);
            }

            break;
        }
    }

}
