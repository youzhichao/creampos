package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.DaiShouDef;
import hyi.cream.uibeans.SelectButton;

import java.awt.Toolkit;
import java.util.EventObject;

public class DaiShou2ReadyState extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private static DaiShou2ReadyState instance = null;
	private DaiShouDef selectedDaiShoudef;
	private String barcode = "";

	public DaiShou2ReadyState() throws InstantiationException {
	}

	public static DaiShou2ReadyState getInstance() {
		if (instance == null) {
			try {
				instance = new DaiShou2ReadyState();
			} catch (InstantiationException e) {
			}
		}
		return instance;
	}

	/* 
	 * @see hyi.cream.state.State#entry(java.util.EventObject, hyi.cream.state.State)
	 */
	public void entry(EventObject event, State sourceState) {
		if (sourceState instanceof DaiShou2State) {
			DaiShou2State dss = (DaiShou2State) sourceState;
			selectedDaiShoudef = (DaiShouDef) dss.getMatchDefs().get(0);
			barcode = dss.getBarcode();
		} else if (sourceState instanceof DaiShou2CheckState && event.getSource() instanceof SelectButton) {
			DaiShou2CheckState dscs = (DaiShou2CheckState) sourceState;
			selectedDaiShoudef = (DaiShouDef) dscs.getSelectedDaiShouDef();
			barcode = dscs.getBarcode();
		} else if (sourceState instanceof DaiShou2WarningState && event.getSource() instanceof SelectButton) {
			DaiShou2WarningState dscs = (DaiShou2WarningState) sourceState;
			selectedDaiShoudef = (DaiShouDef) dscs.getSelectedDaiShouDef();
			barcode = dscs.getBarcode();
		} else {
			return;
		}
		app.getCurrentTransaction().addDaiShouLineItem(selectedDaiShoudef, barcode, null);
	}

	/* 
	 * @see hyi.cream.state.State#exit(java.util.EventObject, hyi.cream.state.State)
	 */
	public Class exit(EventObject event, State sinkState) {
		Toolkit.getDefaultToolkit().beep();
        return PrintPluState.class;
	}

}
