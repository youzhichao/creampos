package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * @author bruce
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DailyReportDateState extends State {
    private static DailyReportDateState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private static StringBuffer date = new StringBuffer();
    private State verySourceState;

    public static DailyReportDateState getInstance() {
        try {
            if (instance == null) {
                instance = new DailyReportDateState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public DailyReportDateState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            date.append(pb.getNumberLabel());
            app.getMessageIndicator().setMessage(date.toString());
            return;
        }

        if (sourceState instanceof IdleState
            || sourceState instanceof CashierState) {
            verySourceState = sourceState; 
        }

        if (date.length() == 0) {
            app.getMessageIndicator().setMessage(res.getString("InputDailyReportDate"));
        } else {
            app.getWarningIndicator().setMessage("");
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            String dateString;
            DateFormat yyyyMMdd = CreamCache.getInstance().getDateFormate();
            if (date.length() == 0) {   // get today if press enter directly
                dateString = yyyyMMdd.format(new Date());
            } else {
                try {
                    Date d = CreamCache.getInstance().getDateFormate2().parse(date.toString());
                    dateString = yyyyMMdd.format(d);
                } catch (ParseException e) {
                    date.setLength(0);
                    app.getWarningIndicator().setMessage(res.getString("DateFormatError"));
                    return DailyReportDateState.class;
                }
            }
            hyi.ereport.DailyReport.main(new String[] {
                "-sid=" + app.getSystemInfo().getStoreNumber(),  
                "-sname=" + app.getSystemInfo().getStoreName(),
                "-uid=999999",
                "-uname=Name",
                "-date=" + dateString,
                "-path=cream.conf"
            });
            date.setLength(0);
            return verySourceState.getClass();

        } else if (event.getSource() instanceof ClearButton) {
            if (date.length() == 0) {
                return verySourceState.getClass();
            } else {
                date.setLength(0);
                return DailyReportDateState.class;
            }
        }

        if (date.length() == 0) {
            app.getWarningIndicator().setMessage("");
        }
        return sinkState.getClass();
    }

    public static String getDate() {
        return date.toString();
    }

    public static void clearDate() {
        DailyReportDateState.date.setLength(0);
    }

}
