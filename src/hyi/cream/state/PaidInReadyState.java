package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Reason;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;

public class PaidInReadyState extends State {        
    private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private LineItem lineItem             = null;
    private Reason r                      = null;
    private String number                 = "";
    private Class exitState               = null;

    static PaidInReadyState paidInReadyState      = null;  

    public static PaidInReadyState getInstance() {
        try {
            if (paidInReadyState == null) {
                paidInReadyState = new PaidInReadyState();
            }
        } catch (InstantiationException ex) {
        }
        return paidInReadyState;
    }

    /**
     * Constructor
     */
    public PaidInReadyState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
        //System.out.println("PaidInReadyState entry");

        if (sourceState instanceof PaidInIdleState
            && event.getSource() instanceof SelectButton) {
            number = ((PaidInIdleState)sourceState).getPluNo();
            //System.out.println("number = " + number);
            r = Reason.queryByreasonNumberAndreasonCategory(number, "10");
            lineItem = new LineItem();
            lineItem.setDescription(r.getreasonName());
            lineItem.setDetailCode("N");


            lineItem.setPluNumber(String.valueOf(number));
            lineItem.setQuantity(new HYIDouble(0));
            //lineItem.setTaxType("0");
            Transaction curTran = app.getCurrentTransaction();
            lineItem.setTerminalNumber(curTran.getTerminalNumber());
            lineItem.setTransactionNumber(new Integer(Transaction.getNextTransactionNumber()));
            
            lineItem.setUnitPrice(new HYIDouble(0.00));

			/*
			 * Meyer/2003-02-21/
			 */
			lineItem.setItemNumber(String.valueOf(number));  
			lineItem.setOriginalPrice(new HYIDouble(0.00));            

            lineItem.setAmount(new HYIDouble(0.00)); 
            try {
                curTran.addLineItem(lineItem);
                exitState = PaidInOpenPriceState.class;
            } catch (TooManyLineItemsException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Too many LineItem exception at " + this);
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyLineItems"));
                exitState = PaidInIdleState.class;
            }
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("PaidInReadyState exit");

		app.getMessageIndicator().setMessage("");
        return exitState;
    }
}

