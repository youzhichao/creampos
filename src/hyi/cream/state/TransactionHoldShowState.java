package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.TransactionHold;
import hyi.cream.event.TransactionEvent;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.uibeans.CancelButton;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;
import java.util.ResourceBundle;

public class TransactionHoldShowState extends State {
    static TransactionHoldShowState transactionHoldShowState = null;

    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    //private Transaction holdTran;

    private int tranNumber;

    private ResourceBundle res = CreamToolkit.GetResource();

    public static TransactionHoldShowState getInstance() {
        try {
            if (transactionHoldShowState == null) {
                transactionHoldShowState = new TransactionHoldShowState();
            }
        } catch (InstantiationException ex) {
        }
        return transactionHoldShowState;
    }

    /**
     * Constructor
     */
    public TransactionHoldShowState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof CancelState)
            return;

        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();
            if (sourceState instanceof TransactionUnHoldState) {
                TransactionUnHoldState rtnHoldState = (TransactionUnHoldState) sourceState;
                tranNumber = rtnHoldState.getTranNumber();
            }
            Transaction holdTran = TransactionHold.queryByTransactionNumber(connection,
                PARAM.getTerminalNumber(), String.valueOf(tranNumber));
            Transaction trans = app.getCurrentTransaction();
            Object[] list = holdTran.getLineItems();
            for (int i = 0; i< list.length; i++) {
                LineItem lineItem = (LineItem) list[i];
                try {
                    LineItem tmp = (LineItem) lineItem.clone();
                    tmp.setTransactionNumber(trans.getTransactionNumber());
                    trans.addLineItem(tmp, false);
                } catch (TooManyLineItemsException e) {
                }
            }
            trans.setHoldTranNo(holdTran.getHoldTranNo());
            //trans.setTransactionType("01");
            trans.setTranType1("1");
            app.getItemList().setItemIndex(0);
            app.getItemList().setTransaction(trans);
            trans.fireEvent(new TransactionEvent(trans, TransactionEvent.TRANS_INFO_CHANGED));
            app.getMessageIndicator().setMessage(
                    res.getString("IsUnHoldThisTransaction"));
            connection.commit();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getTransactionalConnection();
                TransactionHold.deleteByTranNumber(connection, tranNumber);
                Transaction trans = app.getCurrentTransaction();
                if (!app.getTrainingMode()
                        && PARAM.getTranPrintType().equalsIgnoreCase("step")) {
                    CreamPrinter printer = CreamPrinter.getInstance();
                    if (!printer.getHeaderPrinted()) {
                        if (!printer.isPrinterAtRightStart()) { // 檢查印表機是否已定位
                            setWarningMessage(CreamToolkit.GetResource().getString("PrinterIsNotAtRightPosition"));
                            PrintPluWarningState.setExitState(IdleState.class);
                            return PrintPluWarningState.class;
                        }
                        printer.printHeader(connection, trans);
                        printer.setHeaderPrinted(true);
                    }
    
                    if (!printer.isPrinterHealthy()) { // 檢查印表機是否ready
                        setWarningMessage(CreamToolkit.GetResource().getString("PrinterIsNotReady"));
                        PrintPluWarningState.setExitState(IdleState.class);
                        return PrintPluWarningState.class;
                    }

                    for (Object lineItemObj : trans.getLineItems()) {
                        LineItem lineItem = (LineItem)lineItemObj;
                        if (!lineItem.getPrinted())
                            printer.printLineItem(connection, lineItem);
                    }
                }
                connection.commit();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                CreamToolkit.haltSystemOnDatabaseFatalError(e);
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
            return IdleState.class;
        } else if (event.getSource() instanceof CancelButton) {
            return CancelState.class;
        } else if (event.getSource() instanceof ClearButton) {
            Transaction trans = app.getCurrentTransaction();
            trans.setHoldTranNo(null);
            return TransactionUnHoldState.class;
        }
        return sinkState.getClass();
    }

    public Transaction getHoldTran() {
        return app.getCurrentTransaction();
    }
}
