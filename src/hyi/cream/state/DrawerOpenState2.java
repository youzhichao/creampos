package hyi.cream.state;

//import hyi.cream.POSPeripheralHome;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.ShiftReport;
import hyi.cream.dac.ZReport;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.util.CreamToolkit;
import hyi.spos.CashDrawer;
import hyi.spos.events.StatusUpdateEvent;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;

import org.apache.commons.lang.StringUtils;

//import jpos.CashDrawer;
//import jpos.events.StatusUpdateEvent;

public class DrawerOpenState2 extends State {

    static DrawerOpenState2 drawerOpenState = null;
    ZReport z = null;
    ShiftReport shift = null;
    boolean open = true;
    boolean showWarning = true;
    POSTerminalApplication app = POSTerminalApplication.getInstance();

    public static DrawerOpenState2 getInstance() {
        try {
            if (drawerOpenState == null) {
                drawerOpenState = new DrawerOpenState2();
            }
        } catch (InstantiationException ex) {
        }
        return drawerOpenState;
    }

    /**
     * Constructor
     */
    public DrawerOpenState2() throws InstantiationException {
    }

    private void updateDrawerOpenCount() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();
            z = ZReport.getOrCreateCurrentZReport(connection);
            if (z != null) {
                z.setDrawerOpenCount(new Integer(z.getDrawerOpenCount().intValue() + 1));
                z.update(connection);
            }
            shift = ShiftReport.getCurrentShift(connection);
            if (shift != null) {
                shift.setDrawerOpenCount(new Integer(shift.getDrawerOpenCount().intValue() + 1));
                shift.update(connection);
            }
            connection.commit();
        } catch (EntityNotFoundException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    public void entry(EventObject event, State sourceState) {
        System.out.println("This is DrawerOpenState2's Entry!");
        showWarning = false;
        // Chech drawer open or not
        /*
         * if (shift != null) { if
         * (shift.getAccountingDate().compareTo(CreamToolkit.getInitialDate()) != 0) open = false;
         * else open = true; } else { open = false; }
         */
        if (StringUtils.isEmpty(PARAM.getCashierNumber())) {
            open = false;
        } else
            open = true;
        if (!open) {
            Runnable t = new Runnable() {
                public void run() {
                    POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
                    try {
                        CashDrawer cashi = posHome.getCashDrawer();
                        if (!posHome.getEventForwardEnabled())
                            posHome.setEventForwardEnabled(true);
                        posHome.statusUpdateOccurred(new StatusUpdateEvent(cashi, 0));
                    } catch (Exception ne) {
                        CreamToolkit.logMessage(ne);
                        return;
                    }
                }
            };
            new Thread(t).start();
            return;
        }

        if (!PARAM.isCheckDrawerClose()) {
            Runnable t = new Runnable() {
                public void run() {
                    POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
                    try {
                        CashDrawer cashi = posHome.getCashDrawer();
                        if (!posHome.getEventForwardEnabled())
                            posHome.setEventForwardEnabled(true);
                        try {
                            cashi.setDeviceEnabled(true);
                            cashi.openDrawer();
                            updateDrawerOpenCount();
                            Thread.sleep(1000);
                        } catch (Exception e) {
                            CreamToolkit.logMessage(e);
                        }
                        posHome.statusUpdateOccurred(new StatusUpdateEvent(cashi, 0));
                    } catch (Exception ne) {
                        CreamToolkit.logMessage(ne);
                        return;
                    }
                }
            };
            new Thread(t).start();
        } else {
            // sourceState = sourceState;
            Runnable t = new Runnable() {
                public void run() {
                    // set posHome enabled to prepare for event forwarding
                    POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
                    CashDrawer cash = null;
                    try {
                        cash = posHome.getCashDrawer();
                    } catch (Exception ne) {
                        CreamToolkit.logMessage(ne);
                        return;
                    }
                    if (!posHome.getEventForwardEnabled())
                        posHome.setEventForwardEnabled(true);

                    try {
                        if (!cash.getDeviceEnabled())
                            cash.setDeviceEnabled(true);
                        cash.addStatusUpdateListener(posHome);
                        cash.openDrawer();
                        app.getMessageIndicator().setMessage(
                            CreamToolkit.GetResource().getString("CashDrawerIsOpened"));
                        updateDrawerOpenCount();
                        Thread.sleep(2000);
                        int wait = PARAM.getDrawerCloseTime();
                        showWarning = true;
                        startWarning(wait);
                        cash.waitForDrawerClose(wait * 1000, 0, 0, 100);
                        showWarning = false;
                        if (showWarningHint != null)
                            showWarningHint.interrupt();

                        // Start a timer.
                        cash.removeStatusUpdateListener(posHome);
                        cash.setDeviceEnabled(false);
                    } catch (Exception je) {
                        CreamToolkit.logMessage(je);
                        return;
                    }
                }
            };
            (new Thread(t)).start();
        }
        // System.out.println("DrawerOpenState2 entry finished");
    }

    static Thread showWarningHint = null;

    private void startWarning(int w) {
        final int wait = w;
        Runnable startWarning = new Runnable() {
            public void run() {
                CreamToolkit.sleepInSecond(wait + 1);
                if (showWarning)
                    app.getWarningIndicator().setMessage(
                        CreamToolkit.GetResource().getString("PleaseCloseCashDrawer"));
            }
        };
        showWarningHint = new Thread(startWarning);
        showWarningHint.start();
    }

    public Class exit(EventObject event, State sinkState) {
        System.out.println("This is DrawerOpenState2's Exit!");
        if (!open)
            app.getWarningIndicator().setMessage(
                CreamToolkit.GetResource().getString("PleaseSignOnThenOpenDrawer"));
        else {
            app.getMessageIndicator().setMessage("");
            app.getWarningIndicator().setMessage("");
        }
        // init newCashierID
        app.setNewCashierID("");
        return CashierRightsCheckState.getSourceState();
        // return KeyLock1State.class;
    }
}
