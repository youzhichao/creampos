/**
 * State class
 * @since 2000
 * @author slackware
 */

package hyi.cream.state;

public class CustomNumberingState extends SomeAGNumberingState {

    static CustomNumberingState customNumberingState = null;

    public static CustomNumberingState getInstance() {
        try {
            if (customNumberingState == null) {
                customNumberingState = new CustomNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return customNumberingState;
    }

    /**
     * Constructor
     */
    public CustomNumberingState() throws InstantiationException {
    }
}



