package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.groovydac.SsmpLog;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.inline.MasterDownloadThread;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.Date;
import java.util.EventObject;
import java.util.ResourceBundle;

public class PluUpdateState extends State {
    static PluUpdateState pluUpdateState = null;

    POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();

    boolean connected = false;

    Class exitState = ConfigState.class;

    public static PluUpdateState getInstance() {
        try {
            if (pluUpdateState == null) {
                pluUpdateState = new PluUpdateState();
            }
        } catch (InstantiationException ex) {
        }
        return pluUpdateState;
    }

    /**
     * Constructor
     */
    public PluUpdateState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        CreamToolkit.logMessage("PluUpdateState | source : "
                + sourceState.getClass());
        if (sourceState instanceof KeyLock1State)
            exitState = KeyLock1State.class;
        // System.out.println("This is PluUpdateState's Entry!");
        // KeylockWarningState Keylock
        // connected = DacTransfer.getInstance().isConnected(100);
        posTerminal.getMessageIndicator().setMessage(
                CreamToolkit.GetResource().getString(
                        "PleasePressEnterBeginUpload"));
        // if (!connected)
        // POSButtonHome.getInstance().buttonPressed(new POSButtonEvent(new
        // ClearButton(0,0,0,"")));
        // System.out.println("This is PluUpdateState's Entry!");
    }

    public Class exit(EventObject event, State sinkState) {
        // System.out.println("This is PluUpdateState's Exit!");
        // connected = DacTransfer.getInstance().isConnected(100);
        connected = Client.getInstance().isConnected();
        if (!connected) {
            posTerminal.getWarningIndicator()
                    .setMessage(
                            CreamToolkit.GetResource().getString(
                                    "NoConnectRetryLater"));
            return exitState;
        } /*
             * else { if
             * (DacTransfer.getInstance().isDateShiftRunning(DacTransfer.DATE_BIZDATE) ||
             * Trigger.getInstance().getRunning()) {
             * posTerminal.getWarningIndicator().setMessage("程序忙，请稍候再试。");
             * return exitState; } }
             */
        if (event.getSource() instanceof ClearButton) {
            return exitState;
        } else if (event.getSource() instanceof EnterButton) {
            downloadMaster();
//            try {
//                StateMachine.getInstance().setEventProcessEnabled(false);
//
//                int tableCount = Client.getInstance().downloadAllMastersForcedly();
//
//                DbConnection connection = null;
//                try {
//                    connection = CreamToolkit.getTransactionalConnection();
//                    // Bruce/20030505
//                    // Update "MasterVersion"
//                    String masterVersion = CreamCache.getInstance().getDateFormate().format(
//                        new Date());
//                    PARAM.updateMasterVersion(masterVersion);
//                    connection.commit();
//                } catch (SQLException e) {
//                    CreamToolkit.logMessage(e);
//                } finally {
//                    CreamToolkit.releaseConnection(connection);
//                }
//
//                String origMessage = posTerminal.getMessageIndicator().getMessage();
//                posTerminal.getMessageIndicator().setMessage(res.getString("ProgramDownloading"));
//                MasterDownloadThread.getInstance().downloadProgramFiles();
//                boolean downloadSomeFiles = Client.getInstance().getNumberOfFilesGotOrMoved() > 0;
//
//                MasterDownloadThread.afterDownloadingMaster(tableCount > 0, downloadSomeFiles);
//                posTerminal.getMessageIndicator().setMessage(origMessage);
//
//                SsmpLog.report10012();
//                if (downloadSomeFiles)
//                    SsmpLog.report10013(Client.getInstance().getScriptFiles());
//
////              if (Client.getInstance().getNumberOfFilesGotOrMoved() <= 0
////                      && GetProperty.getRebootAfterDownMaster("yes").equalsIgnoreCase("no")) {
////                  POSTerminalApplication.getInstance().getWarningIndicator()
////                      .setMessage(CreamToolkit.GetResource().getString("UpdateCompleted"));
////              } else {
////                  POSTerminalApplication.getInstance().getWarningIndicator()
////                      .setMessage(CreamToolkit.GetResource().getString("UpdateCompletedAndReboot"));
////                  DacTransfer.shutdown(5);
////              }
//
//            } catch (ClientCommandException e) {
//                e.printStackTrace(CreamToolkit.getLogger());
//                SsmpLog.report10011(e);
//                posTerminal.getMessageIndicator().setMessage(
//                        res.getString("MasterDownloadFailed"));
//            } finally {
//                StateMachine.getInstance().setEventProcessEnabled(true);
//            }
        }
        return exitState;
    }

    public static void downloadMaster() {
        POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        ResourceBundle res = CreamToolkit.GetResource();
        try {

            StateMachine.getInstance().setEventProcessEnabled(false);

            int tableCount = Client.getInstance().downloadAllMastersForcedly();

            DbConnection connection = null;
            try {
                connection = CreamToolkit.getTransactionalConnection();
                // Bruce/20030505
                // Update "MasterVersion"
                String masterVersion = CreamCache.getInstance().getDateFormate().format(
                    new Date());
                PARAM.updateMasterVersion(masterVersion);
                connection.commit();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            } finally {
                CreamToolkit.releaseConnection(connection);
            }

            String origMessage = posTerminal.getMessageIndicator().getMessage();
            posTerminal.getMessageIndicator().setMessage(res.getString("ProgramDownloading"));
            MasterDownloadThread.getInstance().downloadProgramFiles();
            boolean downloadSomeFiles = Client.getInstance().getNumberOfFilesGotOrMoved() > 0;

            MasterDownloadThread.afterDownloadingMaster(tableCount > 0, downloadSomeFiles);
            posTerminal.getMessageIndicator().setMessage(origMessage);

            SsmpLog.report10012();
            if (downloadSomeFiles)
                SsmpLog.report10013(Client.getInstance().getScriptFiles());

        } catch (ClientCommandException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            SsmpLog.report10011(e);
            posTerminal.getMessageIndicator().setMessage(
                    res.getString("MasterDownloadFailed"));
        } finally {
            StateMachine.getInstance().setEventProcessEnabled(true);
        }
    }
}
