package hyi.cream.state;

import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.groovydac.Param;
import hyi.cream.uibeans.AgeLevelButton;
import hyi.cream.uibeans.BuyerNoButton;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.HYIDouble;
import hyi.cream.util.ReceiptGenerator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

public class PeiDa3OnlineState extends State {

    private static PeiDa3OnlineState peiDa3OnlineState;

    private String billNo;

    private boolean loadOK;

    public static PeiDa3OnlineState getInstance() {
        try {
            if (peiDa3OnlineState == null) {
                peiDa3OnlineState = new PeiDa3OnlineState();
            }
        } catch (InstantiationException ex) {
        }
        return peiDa3OnlineState;
    }

    /**
     * Constructor
     */
    public PeiDa3OnlineState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        Transaction trans = getCurrentTransaction();
        loadOK = true;
        if (sourceState instanceof PeiDa3IdleState || sourceState instanceof InputDownPaymentState) {
            billNo = PeiDa3IdleState.getInstance().getBillNo();

            List<LineItem> lineItems;
            try {
                lineItems = StateToolkit.getPeiDaDtl2LineItem(trans,
                        PeiDa3IdleState.getBillNoPrefix() + billNo);
            } catch (Exception e) {
                loadOK = false;
                showMessage("    ", billNo );
                e.printStackTrace();
                return;
            }

            if (!PeiDa3IdleState.getInstance().isBackPayment()
                && !PeiDa3IdleState.getInstance().isBackRefundPaymet()) { // 如果是訂金尾款或退訂金尾款，交易不需要顯示配送商品
                //HYIDouble zero = new HYIDouble(0);
                for (LineItem lineItem : lineItems) {
                    try {
                        // Nitori着付二期：商品金額不用設為0
                        /****
                        // 如果是付訂金，畫面上顯示傳票的商品金額都改成零（而且不印發票）
                        if (sourceState instanceof InputDownPaymentState) {
                            lineItem.setUnitPrice(zero);
                            lineItem.setAmount(zero);
                            lineItem.setOriginalPrice(zero);
                            lineItem.setAfterDiscountAmount(zero);
                            lineItem.setTaxAmount(zero);
                        }
                        ****/
                        trans.addLineItem(lineItem, false);
                    } catch (TooManyLineItemsException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (sourceState instanceof InputDownPaymentState) {
                HYIDouble downPayment = InputDownPaymentState.getInstance().getDownPaymentAmount();

                LineItem lineItem = new LineItem();
                lineItem.setPluNumber(PARAM.getDownPaymentItemPluNo());
                lineItem.setItemNumber(PARAM.getDownPaymentItemNo());
                String itemName = PeiDa3IdleState.getInstance().isDownPayment() ? m("DownPayment") : m("BackPayment");
                lineItem.setDescriptionAndSpecification(itemName);
                lineItem.setDescription(itemName);
                lineItem.setTerminalNumber(getPOSNumber());
                lineItem.setTransactionNumber(trans.getTransactionNumber());
                lineItem.setDetailCode("S");
                lineItem.setLineItemSequence(trans.getLineItems().length + 1);
                String depNo = PARAM.getDownPaymentItemDepNo();
                lineItem.setDepID(depNo);
                lineItem.setCategoryNumber(depNo.substring(0, 1));
                lineItem.setMidCategoryNumber(depNo.substring(1, 2));
                lineItem.setMicroCategoryNumber(depNo.substring(2, 3));
                lineItem.setOriginalPrice(downPayment);
                lineItem.setUnitPrice(downPayment);
                lineItem.setAfterDiscountAmount(downPayment);
                lineItem.setQuantity(new HYIDouble(1));
                lineItem.setAmount(downPayment);
                lineItem.setTaxType("1");
                lineItem.setTaxAmount(downPayment.divide(new HYIDouble(1.05), BigDecimal.ROUND_HALF_UP)
                    .multiply(new HYIDouble(0.05)));

                if (PeiDa3IdleState.getInstance().isBackRefundPaymet()) {

                    downPayment = downPayment.negate();
                    lineItem.setQuantity(new HYIDouble(-1));
                    lineItem.setAmount(downPayment);
                    lineItem.setAfterDiscountAmount(downPayment);
                    lineItem.setTaxAmount(lineItem.getTaxAmount().negate());

                    trans.setDealType1("0");
                    trans.setDealType2("3");
                    trans.setDealType3("4");
                }

                try {
                    // Nitori着付二期：付頭款時不需增加着付商品
                    if (!PeiDa3IdleState.getInstance().isDownPayment())
                        trans.addLineItem(lineItem, false);
                } catch (TooManyLineItemsException e) {
                    e.printStackTrace();
                }
            }
            //app.getCurrentTransaction().setAnnotatedType("P");
            //app.getCurrentTransaction().setAnnotatedId(billNo);
            trans.initForAccum();
            trans.accumulate();

        }
        if (sourceState instanceof SummaryState
            || sourceState instanceof PeiDa3OnlineState
            || sourceState instanceof GetBuyerNumberState
            || sourceState instanceof BuyerState
            || sourceState instanceof GetMemberNumber2State
        ) {
            showMessage("PeiDaConfirm");
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (loadOK) {
            if (event.getSource() instanceof AgeLevelButton) {
                if (!isInvoiceEnough()) {
                    showWarningMessage("InvoiceNotEnough");
                    return PeiDa3OnlineState.class;
                } else
                    return SummaryState.class;
            } else if (event.getSource() instanceof ClearButton) {
                billNo =  "";
                getCurrentTransaction().clearLineItem();
                getCurrentTransaction().clearDeliveryInfo();
            } else if (event.getSource() instanceof BuyerNoButton) {
                return BuyerState.class;
            }
        }
        return PeiDa3IdleState.class;
    }

    private boolean isInvoiceEnough() {
        if (!Param.getInstance().isCountInvoiceEnable())
            return true;

        Transaction currentTrans = getCurrentTransaction();
        int lineItemCount = 0;
        for (LineItem lineItem : new ArrayList<LineItem>(currentTrans.lineItems())) {
            String detailCode = lineItem.getDetailCode();
            if (!lineItem.getRemoved() &&
                (detailCode.equals("S")         // "S":销售
                || detailCode.equals("I")       // "I":代售
                || detailCode.equals("O")))     // "O":代收
                lineItemCount++;
        }
        if (lineItemCount == 0)
            return true;

        int lineItemPerPage = ReceiptGenerator.getInstance().getLineItemPerPage();
        int pageNeed = (lineItemCount - 1) / lineItemPerPage + 1;

        int invNo = 0;
        try {
            invNo = Integer.parseInt(Param.getInstance().getInvoiceNumber());
        } catch (Exception e) {
        }
        int countDown = 250 - (invNo % 250);

        return countDown >= pageNeed;
    }
}
