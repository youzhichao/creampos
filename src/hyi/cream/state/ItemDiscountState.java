package hyi.cream.state;

//import java.util.*;
//import java.math.*;
//
//import hyi.cream.*;
//import hyi.cream.dac.*;
//import hyi.cream.exception.TooManyLineItemsException;
//import hyi.cream.util.*;
//import hyi.cream.uibeans.*;

/**
 * Bruce> To be removed, no use now.
 */
public class ItemDiscountState /*extends State*/ {
//    static ItemDiscountState itemDiscountState = null;
//    private POSTerminalApplication app = POSTerminalApplication.getInstance();
//    private ResourceBundle res = CreamToolkit.GetResource();
//    private String discountID = "";
//    private HYIDouble discountUnitPrice = new HYIDouble(0);
//    private Class exitState = null;
//    private SI itemDiscount = null;
//    private HYIDouble discountQuantity = new HYIDouble(0);
//    private LineItem curLineItem = null;
//
//    public static ItemDiscountState getInstance() {
//        try {
//            if (itemDiscountState == null) {
//                itemDiscountState = new ItemDiscountState();
//            }
//        } catch (InstantiationException ex) {
//        }
//        return itemDiscountState;
//    }
//
//    /**
//     * Constructor
//     */
//    public ItemDiscountState() throws InstantiationException {
//    }
//
//    public void entry(EventObject event, State sourceState) {
//        Transaction trans = app.getCurrentTransaction();
//        if (sourceState instanceof IdleState) {
//            curLineItem = trans.getCurrentLineItem();
//            if (curLineItem == null &&
//                !curLineItem.getDetailCode().equalsIgnoreCase("R")) {
//                setWarningMessage(res.getString("ItemDiscount"));
//                exitState = WarningState.class;
//                return;
//            }
//            discountID = ((ItemDiscountButton)event.getSource()).getID();
//            itemDiscount = SI.queryBySIID(discountID);
//            discountQuantity = new HYIDouble(1.00);
//        } else if (sourceState instanceof NumberingState) {
//            curLineItem = trans.getCurrentLineItem();
//            if (curLineItem == null &&
//                !curLineItem.getDetailCode().equalsIgnoreCase("R")) {
//                setWarningMessage(res.getString("ItemDiscount"));
//                exitState = WarningState.class;
//                return;
//            }
//            discountID = ((ItemDiscountButton)event.getSource()).getID();
//            itemDiscount = SI.queryBySIID(discountID);
//            discountQuantity = new HYIDouble(((NumberingState)sourceState).getNumberString());
//        }
//        try {
//            LineItem discountLineItem = new LineItem();
//            discountLineItem.setDescription(itemDiscount.getScreenName());
//            discountLineItem.setDetailCode("T");
//            discountLineItem.setQuantity(discountQuantity.negate());
//            discountLineItem.setTerminalNumber(trans.getTerminalNumber());
//            discountLineItem.setTransactionNumber(trans.getTransactionNumber());
//            discountLineItem.setPrinted(false);
//			discountLineItem.setRemoved(false);
//			discountLineItem.setTaxType(curLineItem.getTaxType());
//            discountLineItem.setPluNumber(discountID);
//            discountLineItem.setLineItemSequence((trans.getLineItems()).length + 1);
//            char discountType = itemDiscount.getType();
//            HYIDouble discountValue = itemDiscount.getValue();
//            //String discountBase = itemDiscount.getBase();
//            String discountAttr = itemDiscount.getAttribute();
//            int round = 0;
//            String discountRound = discountAttr.charAt(0) + "";
//            if (discountRound.equalsIgnoreCase("R")) {
//                round = BigDecimal.ROUND_HALF_UP;
//            } else if (discountRound.equalsIgnoreCase("C")) {
//                round = BigDecimal.ROUND_UP;
//            } else if (discountRound.equalsIgnoreCase("D")) {
//                round = BigDecimal.ROUND_DOWN;
//            }
//            int scale = Integer.parseInt(discountAttr.charAt(1) + "");
//            if (discountType == SI.DISC_TYPE_DISCOUNT_PERCENTAGE) {
//                discountUnitPrice = curLineItem.getUnitPrice().multiply(discountValue);
//                discountUnitPrice = discountUnitPrice.setScale(scale, round);
//            }
//            discountLineItem.setUnitPrice(discountUnitPrice);
//
//
//            HYIDouble discountAmount = discountUnitPrice.multiply(discountLineItem.getQuantity());
//            discountLineItem.setAmount(discountAmount);
//            discountLineItem.setDiscountNumber(discountID);
//            discountLineItem.setDiscountType("S");
//            trans.addLineItem(discountLineItem);
//            trans.addItemDiscount(discountUnitPrice);
//            String discountSlip = discountAttr.charAt(4) + "";
//            if (discountSlip.equals("0")) {
//                exitState = PrintPluState.class;
//            } else if (discountSlip .equals("1")) {
//                exitState = Slipping3State.class;
//            }
//
//            //  set to current LineItem properties
//            curLineItem.setSINo(String.valueOf(discountLineItem.getLineItemSequence()));
//
//            //  set to Transaction properties
//            String taxID = curLineItem.getTaxType();
//            String type = "";
//            if (discountType == SI.DISC_TYPE_DISCOUNT_PERCENTAGE) {
//                type = "SIP";
//            } else if (discountType == SI.DISC_TYPE_DISCOUNT_AMOUNT) {
//                type = "SIM";
//            } else if (discountType == SI.DISC_TYPE_ADDON_AMOUNT) {
//                type = "SIPLUS";
//            }
//            String fieldName = type + "AMT" + taxID;
//            String fieldName2 = type + "CNT" + taxID;
//            trans.setFieldValue(fieldName, ((HYIDouble)trans.getFieldValue(fieldName)).addMe(discountAmount));
//            trans.setFieldValue(fieldName2, new Integer(((Integer)trans.getFieldValue(fieldName2)).intValue() + 1));
//        } catch (TooManyLineItemsException e) {
//        }
//    }
//
//    public Class exit(EventObject event, State sinkState) {
//        return exitState;
//    }
}
