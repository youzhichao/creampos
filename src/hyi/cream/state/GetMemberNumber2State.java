
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Member;
import hyi.cream.dac.MemberCardBase;
import hyi.cream.dac.Transaction;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import org.apache.commons.lang.StringUtils;

/**
 * 用于memberbase
 * @author ll
 *
 */
public class GetMemberNumber2State extends GetSomeAGState {
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static GetMemberNumber2State getMemberNumberState = null;
    static private MemberCardBase member;

    public static GetMemberNumber2State getInstance() {
        try {
            if (getMemberNumberState == null) {
                getMemberNumberState = new GetMemberNumber2State();
            }
        } catch (InstantiationException ex) {
        }
        return getMemberNumberState;
    }

    public GetMemberNumber2State() throws InstantiationException {
    }

    public boolean checkValidity() {
        String memberID = getAlphanumericData();

        Transaction curTran = app.getCurrentTransaction();

        // Clear member info when entering [ENTER] button directly.
        if (StringUtils.isEmpty(memberID)) {
            curTran.setMemberID(null);
            curTran.setMemberEndDate(null);
            curTran.setBeforeTotalRebateAmt(new HYIDouble(0));
            return true;
        }

        if (PARAM.isQueryMemberInfo()) {
            member = MemberCardBase.queryByMemberID(memberID);
            setMember(member);
            if (member == null) {

                Member member = Member.queryByMemberID(memberID);
                if (member == null) {
                    setWarningMessage(CreamToolkit.getString("WrongMemberID") + ":" + memberID);
                    return false;
                }
                curTran.setMemberID(member.getMemberCardID());
                GetMemberNumberState.setMember(member);
                return true;

            } else {
                curTran.setMemberID(member.getFcardnumber());
                //app.getCurrentTransaction().setMemberEndDate(member.get);
                //app.getCurrentTransaction().setBeforeTotalRebateAmt(member.getMemberActionBonus());
                return true;
            }
        } else {
            Member member = Member.tryCreateAnUnkownMember(memberID);
            if (member != null) {
                curTran.setMemberID(member.getMemberCardID());
                GetMemberNumberState.setMember(member);
                return true;
            } else {
                setWarningMessage(CreamToolkit.getString("WrongMemberID") + ":" + memberID);
                return false;
            }
        }
    }
    
    public Class getUltimateSinkState() {
        return PARAM.isQueryMemberInfo() ? ShowMemberInfoState.class :
               app.getCurrentTransaction().isPeiDa() ? PeiDa3OnlineState.class :
               IdleState.class;
    }

    public Class getInnerInitialState() {
        return MemberState.class;
    }

    public static MemberCardBase getMember() {
        return member;
    }

    public static void setMember(MemberCardBase member) {
        GetMemberNumber2State.member = member;
    }
}

 