package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.WorkingStateEnum;
import hyi.cream.state.periodical.PeriodicalIdleState;
import hyi.cream.state.wholesale.WholesaleIdleState;
import hyi.cream.uibeans.ClearButton;

import java.util.EventObject;

/**
 * 输入会员卡的entry state.
 * 
 * @author Bruce
 */
public class MemberState extends SomeAGReadyState {
    static MemberState instance;

    public static MemberState getInstance() {
        try {
            if (instance == null)
                instance = new MemberState();
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    public MemberState() throws InstantiationException {
    }

    /**
     * Invoked when exiting this state.
     *
     * <P>Clear warningIndicator and return sinkState.getClass().
     *
	 * @param event the triggered event object.
     * @param sinkState the sink state
     */
	public Class exit(EventObject event, State sinkState) {
		//super.exit(event, sinkState);
		app.getWarningIndicator().setMessage("");
		if (event.getSource() instanceof ClearButton) {
	        if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_ORDER_STATE
	        		|| CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_DRAW_STATE
	        		|| CreamSession.getInstance().getWorkingState() == WorkingStateEnum.PERIODICAL_RETURN_STATE) {
	        	return PeriodicalIdleState.class;
	        } else if (CreamSession.getInstance().getWorkingState() == WorkingStateEnum.WHOLESALECLEARING_STATE) {
	        	return WholesaleIdleState.class;
	        } else {
	        	return IdleState.class;
	        }
		}
        return sinkState.getClass();
    }

    public String getPromptedMessage() {
        return hyi.cream.util.CreamToolkit.GetResource().getString("PleaseInputMemberID");
    }

    public String getWarningMessage() {
        return app.getWarningIndicator().getMessage();
    }
}
