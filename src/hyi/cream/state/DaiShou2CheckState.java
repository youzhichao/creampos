/*
 * Created on 2003-6-20
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.state;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.DaiShouDef;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.PopupMenuListener;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;

/**
 * @author Administrator
 * 
 * To change this generated comment go to Window>Preferences>Java>Code
 * Generation>Code and Comments
 */
public class DaiShou2CheckState extends State implements PopupMenuListener {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private static DaiShou2CheckState instance = null;

	private PopupMenuPane p = app.getPopupMenuPane();

//	private ResourceBundle res = CreamToolkit.GetResource();

	private ArrayList matchedDaiShouDefs = new ArrayList();

	private DaiShouDef selectedDaiShouDef;

	private String barcode = "";

	private int selectItem = 0;

	public DaiShou2CheckState() throws InstantiationException {

	}

	public static DaiShou2CheckState getInstance() {
		if (instance == null) {
			try {
				instance = new DaiShou2CheckState();
			} catch (InstantiationException e) {
			}
		}
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hyi.cream.state.State#entry(java.util.EventObject,
	 *      hyi.cream.state.State)
	 */
	public void entry(EventObject event, State sourceState) {
		HYIDouble amount;
		if (sourceState instanceof DaiShou2State) {
			ArrayList menu = new ArrayList();
			matchedDaiShouDefs = ((DaiShou2State) sourceState).getMatchDefs();
			barcode = ((DaiShou2State) sourceState).getBarcode();
			Iterator itor = ((ArrayList) matchedDaiShouDefs.clone()).iterator();
			String defName;
			DaiShouDef dsf;
			int i = 1;
			while (itor.hasNext()) {
				dsf = (DaiShouDef) itor.next();
				amount = dsf.getAmount(barcode);
				if (amount.intValue() != -1) {
					defName = dsf.getName();
					menu.add(i++ + "." + defName + "  " + amount);
				} else {
					matchedDaiShouDefs.remove(dsf);
				}
			}

			p.setMenu(menu);
			p.setVisible(true);
			p.setPopupMenuListener(this);
			app.getMessageIndicator().setMessage(
					CreamToolkit.GetResource().getString("InputSelect"));

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hyi.cream.state.State#exit(java.util.EventObject,
	 *      hyi.cream.state.State)
	 */
	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof SelectButton) {
			selectedDaiShouDef = (DaiShouDef) matchedDaiShouDefs
					.get(selectItem);
			app.getMessageIndicator().setMessage("");
			// p.setInputEnabled(false);
		}

		if (sinkState != null) {
			return sinkState.getClass();
		} else {
			return null;
		}
	}

	public void menuItemSelected() {
		if (p.getSelectedMode()) {
			selectItem = p.getSelectedNumber();
			POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
			POSButtonHome2.getInstance().buttonPressed(e);
		} else {
			POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
			POSButtonHome2.getInstance().buttonPressed(e);

		}
	}

	public DaiShouDef getSelectedDaiShouDef() {
		return selectedDaiShouDef;
	}

	public String getBarcode() {
		return barcode;
	}
}
