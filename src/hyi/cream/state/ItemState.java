
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.util.CreamToolkit;

import java.util.ResourceBundle;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class ItemState extends SomeAGReadyState {
    static ItemState buyerState = null;
//    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    public static ItemState getInstance() {
        try {
            if (buyerState == null) {
                buyerState = new ItemState();
            }
        } catch (InstantiationException ex) {
        }
        return buyerState;
    }

    /**
	 * Constructor
     */
    public ItemState() throws InstantiationException {
	}

	public String getWarningMessage() {
		ResourceBundle res = CreamToolkit.GetResource();
		return CreamToolkit.GetResource().getString("PleaseInputItemSeq");
	}
}
/*
public Class exit(EventObject event, State sinkState) {
		if (getAlphanumericData() == null || checkValidity()) {
			return getUltimateSinkState();
		} else {
			// Get warning message by getWarningMessage() and
			// display it on warningIndicator.
			app.getWarningIndicator().setMessage(getWarningMessage());
			return getInnerInitialState();
		}//
		app.getWarningIndicator().setMessage("");
	}if (event == null)
			return ItemState.class;

		if (event.getSource() instanceof NumberButton) {
			if (app.getBuyerNumberPrinted()) {
				/*POSPeripheralHome posHome = POSPeripheralHome.getInstance();
					ToneIndicator tone = null;
					try {
						tone = posHome.getToneIndicator();er
					} catch (NoSuchPOSDeviceException ne) { CreamToolkit.logMessage(ne + "!"); }
					try {
						if (!tone.getDeviceEnabled())
							tone.setDeviceEnabled(true);
						if (!tone.getClaimed()) {
							 tone.claim(0);
						//tone.claim(0);
							 tone.setAsyncMode(true);
							 tone.sound(99999, 500);
						}
					} catch (JposException je) {System.out.println(je);}
				//super.exit(event, sinkState);
				System.out.println(app.getBuyerNumberPrinted());
				return ItemState.class;
			} else {
				//super.exit(event, sinkState);
				return 	hyi.cream.state.BuyerNumberingState.class;
			}
		}
		return ItemState.class;*/
