// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.util.CreamToolkit;

/**
 * GetBuyerNumberState.
 * <P>
 * @author dai, Bruce
 */
public class GetBuyerNumberState extends GetSomeAGState {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static GetBuyerNumberState getBuyerNumberState = null;

    public static GetBuyerNumberState getInstance() {
        try {
            if (getBuyerNumberState == null) {
                getBuyerNumberState = new GetBuyerNumberState();
            }
        } catch (InstantiationException ex) {
        }
        return getBuyerNumberState;
    }

    public GetBuyerNumberState() throws InstantiationException {
    }

// Check台灣統一編號合法性, provided by 李更生.    
//    Public Function F_CHK_CONO(cw_co_no As String) As Boolean
//    'cw_id_no:公司統編
//    Dim pn_chk As Integer, pn_i As Integer, pn_j As Integer
//       pn_chk = 0
//       For pn_i = 1 To 8
//          pn_j = Val(Mid("12121241", pn_i, 1)) * Val(Mid(cw_co_no, pn_i, 1))
//          pn_chk = pn_chk + Int(pn_j / 10) + pn_j Mod 10
//          If InStr("0123456789", Mid(cw_co_no, pn_i, 1)) = 0 Then pn_i = 99
//       Next
//       F_CHK_CONO = (pn_chk Mod 10 = 0 Or Mid(cw_co_no, 7, 1) = "7" And pn_chk Mod 10 = 9) And pn_i <> 100
//    End Function

	public boolean checkValidity() {
        //Bruce/20091214> 檢查台灣統一編號合法性
		if (getAlphanumericData().length() == 8) {
		    int checkSum = 0;
            int[] factor = new int[] { 1, 2, 1, 2, 1, 2, 4, 1 };
            for (int i = 0; i < 8; i++) {
                int j = factor[i] * (getAlphanumericData().charAt(i) - 48);
                checkSum += j / 10 + j % 10;
            }
			if (checkSum % 10 == 0 || getAlphanumericData().charAt(6) == '7' && checkSum % 10 == 9) {
                app.getCurrentTransaction().setBuyerNumber(getAlphanumericData());
                return true;
            }
		}
        setWarningMessage(CreamToolkit.GetResource().getString("ErrorOfBuyerNo3"));
        return false;
    }

	public Class getUltimateSinkState() {
		if (!app.getBuyerNumberPrinted()
			&& !app.getCurrentTransaction().getBuyerNumber().equals(""))
            showMessage("BuyerNumberUpdated");

		if (app.getCurrentTransaction().isPeiDa())
			return PeiDa3OnlineState.class;
		else
			return IdleState.class;
	}

	public Class getInnerInitialState() {
        return BuyerState.class;
	}
}
