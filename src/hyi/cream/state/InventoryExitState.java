package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.Indicator;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * 离开盘点State. 结束时打印盘点总零售金额小计.
 */
public class InventoryExitState extends State {
    private static InventoryExitState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Indicator messageIndicator = app.getMessageIndicator();
    private ResourceBundle res = CreamToolkit.GetResource();
    private String inputNumber = "";

    public static InventoryExitState getInstance() {
        try {
            if (instance == null) {
                instance = new InventoryExitState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public InventoryExitState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            inputNumber += pb.getNumberLabel();
            app.getMessageIndicator().setMessage(inputNumber.toString());
            return;
        }
        if (inputNumber.length() == 0)
            messageIndicator.setMessage(res.getString("InventoryExitConfirm"));
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            if (inputNumber.toString().equals("1")) {
                inputNumber = "";
                app.getMessageIndicator().setMessage("");

                // 打印盘点小计
                CreamPrinter.getInstance().printInventoryFooter();

                app.getDacViewer().clearDacList();
                app.getDacViewer().setVisible(false);
                app.getItemList().setVisible(true);

//                return InventoryState.getOriginalState();
				return InventoryUploadState.getInstance().getClass();
            } else {
                inputNumber = "";
                return InventoryExitState.class;
            }
        } else if (event.getSource() instanceof ClearButton) {
            inputNumber = "";
        }
        return sinkState.getClass();
    }
}
