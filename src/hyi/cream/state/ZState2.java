package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineOffTransfer;
import hyi.cream.dac.ZReport;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;

public class ZState2 extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    private static ZState2 zState = null;
    private	boolean success = false;

    public static ZState2 getInstance() {
        try {
            if (zState == null) {
                zState = new ZState2();
            }
        } catch (InstantiationException ex) {
        }
        return zState;
    }

    /**
     * Constructor
     */
    public ZState2() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (!success) {
            if (StateMachine.getFloppySaveError())
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("SaveFloppyErrorInsertAgain"));
            else
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("SaveFloppyReady"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getTransactionalConnection();

            ZReport z = ZReport.getOrCreateCurrentZReport(connection);
            connection.commit();
            app.getMessageIndicator()
                .setMessage(CreamToolkit.GetResource().getString("SaveFloppy"));
            if (event.getSource() instanceof ClearButton) {
                return hyi.cream.state.CancelZState.class;
            }
            if (!success) {
                CreamToolkit.logMessage("ZState2 | start to saveToFloppyDisk...");
                success = LineOffTransfer.saveToFloppyDisk(true);
            }
            CreamToolkit.logMessage("ZState2 | saveToFloppyDisk : " + success);
            StateMachine.setFloppySaveError(!success);
            if (success) {
                if (PARAM.isZPrint())
                    CreamPrinter.getInstance().printZReport(z, this);
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("zEnd"));
                success = false;
                app.setChecked(false);
                // app.setSalemanChecked(false);
                // return KeyLock3State.class;
                return ConfirmZState.getSourceState();
            } else {
                return ZState2.class;
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
            return ConfirmZState.getSourceState();
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}