
// Copyright (c) 2000 hyi
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class CheckConnectState extends State {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();

    private static CheckConnectState checkConnectState = null;

    public static CheckConnectState getInstance() {
        try {
            if (checkConnectState == null) {
                checkConnectState = new CheckConnectState();
            }
        } catch (InstantiationException ex) {
        }
        return checkConnectState;
    }

    /**
     * Constructor
     */
    public CheckConnectState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        //System.out.println("CheckConnectState entry");
        //if (DacTransfer.getInstance().isConnected()) {
        if (hyi.cream.inline.Client.getInstance().isConnected()) {
            app.getWarningIndicator().setMessage(res.getString("isConnected"));
        } else {
            app.getWarningIndicator().setMessage(res.getString("notConnected"));
        }
    }

    public Class exit(EventObject event, State sinkState) {

        return sinkState.getClass();
    }
}


