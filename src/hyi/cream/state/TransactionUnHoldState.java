package hyi.cream.state;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.*;
import java.util.*;
import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.event.TransactionEvent;

public class TransactionUnHoldState extends State implements PopupMenuListener {

	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();
	static TransactionUnHoldState transactionUnHoldState = null;
	private ArrayList menu = new ArrayList();
	private List holdTrans = new ArrayList();
	private PopupMenuPane popup;
	private Class exitState;
	private int tranNumber;

	public static TransactionUnHoldState getInstance() {
		try {
			if (transactionUnHoldState == null) {
				transactionUnHoldState = new TransactionUnHoldState();
			}
		} catch (InstantiationException e) {
			e.printStackTrace(CreamToolkit.getLogger());
		}
		return transactionUnHoldState;
	}

	/**
	 * Constructor
	 */
	public TransactionUnHoldState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
    		if (sourceState instanceof TransactionHoldShowState ||
    				sourceState instanceof CancelState) {
    			app.getCurrentTransaction().init();
                app.getItemList().setTransaction(app.getCurrentTransaction());
                app.getCurrentTransaction().fireEvent(new TransactionEvent(
            		app.getCurrentTransaction(), TransactionEvent.TRANS_RENEW));
    		}
    		exitState = sourceState.getClass();
    		DateFormat sdf = CreamCache.getInstance().getTimeFormate();
    		app.getMessageIndicator().setMessage(
    				res.getString("HoldTransactionList"));
    
    		popup = POSTerminalApplication.getInstance().getPopupMenuPane();
    		menu.clear();
    		holdTrans.clear();
    		Iterator iter = TransactionHold.queryByPosNumber(connection, PARAM.getTerminalNumber());
    		if (iter != null) {
    			int idx = 1;
    			while (iter.hasNext()) {
    				Transaction th = (Transaction) iter.next();
    				String time = sdf.format(th.getSystemDateTime());
    				
    				menu.add(idx + ".[" + th.getTransactionNumber() 
    						+ "]--" + time);
    				holdTrans.add(th);
    				idx++;
    			}
    		}
    		popup.setMenu(menu);
    		popup.setVisible(true);
    		popup.setPopupMenuListener(this);
    		popup.clear();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof ClearButton) {
			app.getMessageIndicator().setMessage("");
			app.getWarningIndicator().setMessage("");
			return IdleState.class;
		}

		if (sinkState == null) {
			return exitState;
		} else {
			return sinkState.getClass();
		}
	}

	/**
	 * 当在PopupMenuPane按数字键加回车，或者按了清除键了以后，都会callback此方法。
	 */
	public void menuItemSelected() {
		int selectIndex = popup.getSelectedNumber();
		if (menu != null && menu.size() > 0) {
			if (popup.getSelectedMode()) {
				Transaction th = (Transaction) holdTrans.get(selectIndex);
				tranNumber = th.getTransactionNumber().intValue();
		        POSButtonEvent e = new POSButtonEvent(new EnterButton(0, 0, 0,""));
				POSButtonHome2.getInstance().buttonPressed(e);
				menu.clear();
				holdTrans.clear();
			} else {
				popup.setVisible(false);
//				popup.show();
//				popup.setPopupMenuListener(this);
				popup.clear();
				POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
				POSButtonHome2.getInstance().buttonPressed(e);
			}
			return;
		} else {
			POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
			POSButtonHome2.getInstance().buttonPressed(e);
		}
	}
	
	public int getTranNumber() {
		return tranNumber;
	}
}