
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.*;
import hyi.cream.event.TransactionEvent;
import hyi.cream.inline.Client;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class ECommerceOrdersState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
	private static ECommerceOrdersState ecmmerceOrdersState = null;
	private ResourceBundle res = CreamToolkit.GetResource();

	private String number = "";


	public static ECommerceOrdersState getInstance() {
        try {
            if (ecmmerceOrdersState == null) {
				ecmmerceOrdersState = new ECommerceOrdersState();
            }
        } catch (InstantiationException ex) {
        }
        return ecmmerceOrdersState;
    }

    /**
     * Constructor
     */
	public ECommerceOrdersState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
		if (sourceState instanceof IdleState) {
			app.getMessageIndicator().setMessage(res.getString("PleaseenterECommerceOrder"));
		} else if (sourceState instanceof  ECommerceOrdersState) {
			if(event.getSource() instanceof ClearButton) {
				app.getMessageIndicator().setMessage(res.getString("PleaseenterECommerceOrder"));
			}
		}
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof EnterButton) {
			if (!number.equals("")) {
				try {
					Transaction trans =  app.getCurrentTransaction();
					ECommerceHttp echttp = new ECommerceHttp();
					HashMap lm = echttp.getResult("api/store/order/getbysn/"+Store.getStoreID()+"/" + number + "", "", "GET");
					Object[] orders = (Object[]) lm.get("orders");
					if(orders.length > 0) {
						String status = ((HashMap)(orders[0])).get("status").toString();
						if (!status.equals("104") && !status.equals("201")) {
							number = "";
							app.getMessageIndicator().setMessage(res.getString("TheErderIsNotAllowedToTakeDelivery"));
							return ECommerceOrdersState.class;
						}
					} else {
						number = "";
						app.getMessageIndicator().setMessage(res.getString("TheErderIsNotAllowedToTakeDelivery"));
						return ECommerceOrdersState.class;
					}
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					trans.getEcDeliveryHeadList().clear();
					EcDeliveryHead edh = new EcDeliveryHead();
					edh.setStoreID(((HashMap)(orders[0])).get("storeId").toString());
					edh.setOrderNumber(((HashMap)(orders[0])).get("orderNumber").toString());
					edh.setAmount(new HYIDouble(((HashMap)(orders[0])).get("amount").toString()));
					edh.setServiceType(((HashMap)(orders[0])).get("serviceType").toString());
					edh.setOrderTime(format.parse(((HashMap)(orders[0])).get("orderTime").toString()));
					edh.setStatus(((HashMap)(orders[0])).get("status").toString());
					edh.setTransactionNumber(trans.getTransactionNumber());
					edh.setPosNo(trans.getTerminalNumber());
					edh.setMemberId(((HashMap)(orders[0])).get("memberId") == null ? "" : ((HashMap)(orders[0])).get("memberId").toString());
					edh.setMemberName(((HashMap)(orders[0])).get("memberName") == null ? "" : ((HashMap)(orders[0])).get("memberName").toString());
					edh.setPhone(((HashMap)(orders[0])).get("phone") == null ? "" : ((HashMap)(orders[0])).get("phone").toString());
					edh.setAddress(((HashMap)(orders[0])).get("address") == null ? "" : ((HashMap)(orders[0])).get("address").toString());
					edh.setNote(((HashMap)(orders[0])).get("note") == null ? "" : ((HashMap)(orders[0])).get("note").toString());
					edh.setDeliveryTime(((HashMap)(orders[0])).get("deliveryTime") == null ? null : format.parse(((HashMap)(orders[0])).get("deliveryTime").toString()));
					edh.setDeliveryType(((HashMap)(orders[0])).get("deliveryType") == null ? "" : ((HashMap)(orders[0])).get("deliveryType").toString());
					edh.setPayId(((HashMap)(orders[0])).get("payId") == null ? "" : ((HashMap)(orders[0])).get("payId").toString());
					edh.setOrderId(Integer.valueOf(((HashMap)(orders[0])).get("orderId").toString()));
					trans.addEcDeliveryHeadList(edh);
					trans.setCreditCardNumber(number);
					Object[] detail = (Object[]) ((HashMap)(orders[0])).get("products");
					for (int i = 0; i < detail.length; i++) {
						LineItem li = new LineItem();
						String itemNumber  = ((HashMap)detail[i]).get("itemNumber").toString();
						PLU plu = PLU.queryBarCode(itemNumber);
						if (plu == null) {
							plu = PLU.queryByInStoreCode(itemNumber);
							if (plu == null) {
								CreamToolkit.logMessage("该商品不存在:" + itemNumber);
								System.out.println("该商品不存在:" + itemNumber);
								number = "";
								app.getWarningIndicator().setMessage(res.getString("PluWaring"));
								app.getMessageIndicator().setMessage(res.getString("PleaseenterECommerceOrder"));
								return ECommerceOrdersState.class;
							}
						}
						li.setTransactionNumber(trans.getTransactionNumber());
						li.setLineItemSequence((i+1));
						li.setPluNumber(plu.getPluNumber());
						li.setDetailCode("S");
						li.setQuantity(new HYIDouble(((HashMap)detail[i]).get("quantity").toString()));
						li.setUnitPrice(new HYIDouble(((HashMap)detail[i]).get("price").toString()));
						li.setAmount(new HYIDouble(((HashMap)detail[i]).get("amount").toString()));
						li.setAfterDiscountAmount(new HYIDouble(((HashMap)detail[i]).get("price").toString()));
						li.setRemoved(false);
						li.setTerminalNumber(trans.getTerminalNumber());
						li.setCategoryNumber(plu.getCategoryNumber());
						li.setMidCategoryNumber(plu.getMidCategoryNumber());
						li.setMicroCategoryNumber(plu.getMicroCategoryNumber());
						li.setThinCategoryNumber(plu.getThinCategoryNumber());
						li.setTaxType(plu.getTaxType());
						li.setDescription(plu.getScreenName());
						li.setDescriptionAndSpecification(plu
								.getNameAndSpecification());
						li.setOpenPrice(plu.isOpenPrice());

						//Bruce/2003-08-08
						li.setSmallUnit(plu.getSmallUnit());
						li.setInvCycleNo(plu.getInvCycleNo());
						li.setSizeCategory(plu.getSizeCategory());
						li.setItemBrand(plu.getItemBrand());
						li.setSize(plu.getSize());
						li.setColor(plu.getColor());
						li.setStyle(plu.getStyle());
						li.setSeason(plu.getSeason());

						//Bruce/20080606/ Cache this for speedup.
						li.setIsScaleItem(plu.isWeightedPlu());
						li.setDepID(plu.getDepID());
						li.setItemNumber(plu.getItemNumber());
						li.setOriginalPrice(new HYIDouble(((HashMap)detail[i]).get("unitPrice").toString()));
						app.getCurrentTransaction().addLineItem(li);
					}
					number = "";
					app.getWarningIndicator().setMessage(res.getString("PleaseSubtotal"));
					return IdleState.class;
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				number = "";
				return ECommerceOrdersState.class;
			}
			number = "";
			return IdleState.class;
		} else if (event.getSource() instanceof ClearButton) {
			if (!number.equals("")) {
				number = "";
				app.getWarningIndicator().setMessage("");
				app.getMessageIndicator().setMessage("");
				return ECommerceOrdersState.class;
			}
			return IdleState.class;
		} else
		if (event.getSource() instanceof NumberButton) {
			number = number + ((NumberButton) event.getSource()).getNumberLabel();
			app.getMessageIndicator().setMessage(number);
			app.getWarningIndicator().setMessage("");
			return ECommerceOrdersState.class;
		}
		return sinkState.getClass();		
	}
}

 
