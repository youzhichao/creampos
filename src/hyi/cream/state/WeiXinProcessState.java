package hyi.cream.state;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.SystemInfo;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.WeiXin_detail;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.WeiXinButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.util.WeiXinHttpPostRequest;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventObject;
import java.util.Iterator;
import java.util.ResourceBundle;

//微信支付的业务处理
public class WeiXinProcessState extends State {

	private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    public static boolean isClearButton = false;
    public Class exitState = null;
    public static int seq = 0;
    public Transaction curTransaction;
    private String payAmount = "";
    public String payID = "";
    static WeiXinProcessState weiXinState = null;
    public static WeiXinProcessState getInstance() {
        if (weiXinState == null) {
            weiXinState = new WeiXinProcessState();
        }
        return weiXinState;
    }
	public void entry(EventObject event, State sourceState) {
        curTransaction =  app.getCurrentTransaction();
        if (sourceState instanceof WeiXinState) {
            this.payID = ((WeiXinState) sourceState).payID;
            isClearButton = false;
            app.setWeiXin(false);
            exitState = SummaryState.class;
            if (WeiXinHttpPostRequest.getPropery() == null) {
                app.getWarningIndicator().setMessage(res.getString("WeiXinProperyFail"));
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome2.getInstance().buttonPressed(e);
                return;
            }
            if (app.getTrainingMode()) {
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome2.getInstance().buttonPressed(e);
                exitState = Paying1State.class;
                return ;
            }
            Object eventSource = event.getSource();
            payAmount = curTransaction.getBalance().toString();
            String amt = curTransaction.getBalance().multiply(new HYIDouble(100)).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
            WeiXin_detail detail = processWeiXin(sourceState, amt) ;

            app.setWeiXin(false);
            if (isClearButton) {
                exitState = SummaryState.class;
                return ;
            }
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome2.getInstance().buttonPressed(e);
            if (detail == null)  {
                exitState = SummaryState.class;
                return;
            }
            exitState = Paying1State.class;
        }
	}

	public Class exit(EventObject event, State sinkState) {

		Object eventSource = event.getSource();
        if (eventSource instanceof ClearButton) {
            isClearButton = false;
            return exitState;
        }
        return exitState;
	}

    public WeiXin_detail processWeiXin(State state, String inputPayingAmount) {
        CreamToolkit.logMessage("check transaction for weixin ...");
        System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date()) + "check transaction for weixin ...");

        Object[] lineItemArray =  curTransaction.getLineItems();
        String  body = "";
        String goods_detail = "";
        boolean ok = false;
        for (int i = 0; i < lineItemArray.length; i++) {
            LineItem l = (LineItem)lineItemArray[i];
            if (!l.getRemoved()) {
                if (body.length() + l.getDescription().length() > 30) {
                    if (!ok) {
                        body = body.substring(1) + "等等...";
                        ok = true;
                    }
                } else {
                    body += "," + l.getDescription();
                }
                String detail = ",{\"goods_id\":\""+l.getPluNumber()+"\",\"goods_name\":\""+l.getDescription()
                        +"\",\"quantity\":" + l.getQuantity().intValue() + ",\"price\":"+l.getOriginalPrice().multiply(new HYIDouble(100.0)).setScale(0, BigDecimal.ROUND_HALF_UP)+"}";
//                if (j < 5)
                goods_detail += detail;
//                if (body.length() > 30) {
//                    body = body.substring(1) + "等等...";
//                    break;
//                }
            }
        }
        if (body.length() <= 30)
            body = body.substring(1);
        goods_detail = "{\"goods_detail\":["+goods_detail.substring(1)+"],\"cost_price\":" + inputPayingAmount+"}";
        Iterator it =  curTransaction.getPayments();
        int count = 0;
        while(it.hasNext()) {
            it.next();
            count++;
        }
        if (count > 0)
            body = "部分金额支付,商品:"+body;
        else
            body = "全额支付,商品:" + body;
        app.getMessageIndicator().setMessage(res.getString("WeiXinMicropay"));
        seq++;
        String seqStr = "";
        if (seq < 10)
            seqStr = "0"+seq;
        else
            seqStr = "" + seq;
        String out_trade_no = curTransaction.getStoreNumber()+curTransaction.getTerminalNumber()+curTransaction.getTransactionNumber()+seqStr;
        String auth_code = ((WeiXinState)state).getNumberStr();
//        if (!SystemInfo.isWanConnected()) {
//            CreamToolkit.logMessage("WanOfflineCanNotTrade... ");
//            app.getWarningIndicator().setMessage(res.getString("WanOfflineCanNotTrade"));
//            return null;
//        }

        System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())+" micropay ");
        CreamToolkit.logMessage(" micropay ");
        String xml = "";
        boolean query = true;
        if (!auth_code.startsWith(curTransaction.getStoreNumber()+curTransaction.getTerminalNumber()+curTransaction.getTransactionNumber())) {
            xml = WeiXinHttpPostRequest.micropay(auth_code, out_trade_no, body, inputPayingAmount, goods_detail); //支付接口
            query = false;
        } else {
            out_trade_no = auth_code;
        }

        if (xml == null || xml.equals("")) {
            System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())+" orderQuery ");
            CreamToolkit.logMessage(" orderQuery ");
            WeiXin_detail weiXin_detail = orderQuery(inputPayingAmount, out_trade_no, seqStr);
            if (weiXin_detail == null) {
                if (query)
                    return null;
                System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())+" reverse ");
                CreamToolkit.logMessage(" reverse ");
                WeiXinHttpPostRequest.reverse(out_trade_no);//冲正
                return null;
            } else
                return weiXin_detail;
        }
        String isSuccess  = getValueFromXml(xml,"<return_code><![CDATA[","]]></return_code>");
        if (isSuccess.equals("SUCCESS")) {
            String result_code = getValueFromXml(xml,"<result_code><![CDATA[","]]></result_code>");
            if (result_code.equals("SUCCESS")) {//支付成功
                String openId = getValueFromXml(xml,"<openid><![CDATA[","]]></openid>");
                String transactionId = getValueFromXml(xml, "<transaction_id><![CDATA[", "]]></transaction_id>");
                String promotionDetail = getValueFromXml(xml, "<promotion_detail><![CDATA[", "]]></promotion_detail>");
                return addWeiXinDetail(inputPayingAmount,transactionId,seqStr, openId, promotionDetail);
            } else {
                String err_code = getValueFromXml(xml,"<err_code><![CDATA[","]]></err_code>");
                if (err_code.equals("USERPAYING")) {//支付等待，顾客确认密码
                    System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())+" USERPAYING ");
                    CreamToolkit.logMessage(" USERPAYING ");
                    return processNotiryRequert(inputPayingAmount, seqStr, out_trade_no);
                } else {//支付失败
                    String err_code_des = getValueFromXml(xml,"<err_code_des><![CDATA[","]]></err_code_des>");
                    CreamToolkit.logMessage(err_code_des);
                    app.getWarningIndicator().setMessage(err_code_des);
                }
            }
        } else {
            String return_msg = getValueFromXml(xml,"<return_msg><![CDATA[","]]></return_msg>");
            CreamToolkit.logMessage(return_msg);
            app.getWarningIndicator().setMessage(return_msg);
        }
        //支付失败，则再次调用查询和撤销
        System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())+" orderQuery1 ");
        CreamToolkit.logMessage(" orderQuery1 ");
        WeiXin_detail weiXin_detail = orderQuery(inputPayingAmount, out_trade_no, seqStr);
        if (weiXin_detail == null) {
            System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS ").format(new Date())+" reverse2 ");
            CreamToolkit.logMessage(" reverse2 ");
            WeiXinHttpPostRequest.reverse(out_trade_no);//冲正
            return null;
        } else
            return weiXin_detail;
//        return null;
    }

    //异步请求
    public WeiXin_detail processNotiryRequert(String inputPayingAmount, String seqStr, String out_trade_no) {

        app.setWeiXin(true);
        app.getWarningIndicator().setMessage(res.getString("AlipayWaitBuyerPay"));
        for (int i = 0 ; i < 60*5; i++) {//请求五分钟
            try {
                Thread.sleep(1000);
                if (isClearButton) {//如果按了清除键，才停止请求
                    WeiXinHttpPostRequest.reverse(out_trade_no);//冲正
                    return null;
                }
                app.getMessageIndicator().setMessage(res.getString("RunTime") + (300-i));
                if ((i+1)%5==0) {//5秒请求一次
                    CreamToolkit.logMessage("isClearButton = " + isClearButton);
                    String xml = WeiXinHttpPostRequest.orderquery(out_trade_no);//查询
                    if (xml == null || xml.equals("")) {
                        CreamToolkit.logMessage("WanOfflineCanNotTrade... ");
                        app.getWarningIndicator().setMessage(res.getString("WanOfflineCanNotTrade"));
                        continue;
                    }
                    String return_code  = getValueFromXml(xml,"<return_code><![CDATA[","]]></return_code>");
                    if (return_code.equals("SUCCESS")) {//查询成功
                        String result_code = getValueFromXml(xml,"<result_code><![CDATA[","]]></result_code>");
                        if (result_code.equals("SUCCESS")) {//查询订单成功
                            String trade_state = getValueFromXml(xml,"<trade_state><![CDATA[","]]></trade_state>");
                            //SUCCESS--支付成功 REFUND--转入退款 NOTPAY--未支付 CLOSED--已关闭 REVERSE--已冲正 REVOK--已撤销
                            if (trade_state.equals("SUCCESS")) {
                                String openId = getValueFromXml(xml, "<openid><![CDATA[", "]]></openid>");
                                String transactionId = getValueFromXml(xml, "<transaction_id><![CDATA[", "]]></transaction_id>");
                                String promotionDetail = getValueFromXml(xml, "<promotion_detail><![CDATA[", "]]></promotion_detail>");
                                return addWeiXinDetail(inputPayingAmount,transactionId,seqStr,openId, promotionDetail);
                            } else if (trade_state.equals("USERPAYING")) {
                                continue;
                            } else {
                                CreamToolkit.logMessage(getTrade_state(trade_state));
                                app.getWarningIndicator().setMessage(getTrade_state(trade_state));
                                WeiXinHttpPostRequest.reverse(out_trade_no);//冲正
                                return null;
                            }
                        } else {
                            String err_code_des = getValueFromXml(xml,"<err_code_des><![CDATA[","]]></err_code_des>");
                            CreamToolkit.logMessage(err_code_des);
                            app.getWarningIndicator().setMessage(err_code_des);
                        }
                    } else {//查询失败
                        String return_msg = getValueFromXml(xml,"<return_msg><![CDATA[","]]></return_msg>");
                        CreamToolkit.logMessage(return_msg);
                        app.getWarningIndicator().setMessage(return_msg);
                    }
                }
                continue;
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
        WeiXinHttpPostRequest.reverse(out_trade_no);//冲正
        return null;
    }

    public WeiXin_detail orderQuery(String inputPayingAmount, String out_trade_no, String seqStr) {
        String xml = WeiXinHttpPostRequest.orderquery(out_trade_no);//查询
        if (xml == null || xml.equals("")) {
            CreamToolkit.logMessage("WanOfflineCanNotTrade... ");
            app.getWarningIndicator().setMessage(res.getString("WanOfflineCanNotTrade"));
            return null;
        }
        String return_code  = getValueFromXml(xml,"<return_code><![CDATA[","]]></return_code>");
        if (return_code.equals("SUCCESS")) {//查询成功
            String result_code = getValueFromXml(xml,"<result_code><![CDATA[","]]></result_code>");
            if (result_code.equals("SUCCESS")) {//查询订单成功
                String trade_state = getValueFromXml(xml,"<trade_state><![CDATA[","]]></trade_state>");
                //SUCCESS--支付成功 REFUND--转入退款 NOTPAY--未支付 CLOSED--已关闭 REVERSE--已冲正 REVOK--已撤销
                if (trade_state.equals("SUCCESS")) {
                    String openId = getValueFromXml(xml, "<openid><![CDATA[", "]]></openid>");
                    String transactionId = getValueFromXml(xml, "<transaction_id><![CDATA[", "]]></transaction_id>");
                    String totalFee = getValueFromXml(xml, "<total_fee>", "</total_fee>");
                    if (new BigDecimal(totalFee).compareTo(new BigDecimal(inputPayingAmount)) != 0) {
                        BigDecimal t = new BigDecimal(totalFee).divide(new BigDecimal(100), 2, 4);
                        app.getWarningIndicator().setMessage(res.getString("AmmountError") + t);
                        return null;
                    }
                    String promotionDetail = getValueFromXml(xml, "<promotion_detail><![CDATA[", "]]></promotion_detail>");
                    return addWeiXinDetail(inputPayingAmount,transactionId,seqStr,openId, promotionDetail);
                } /*else if (trade_state.equals("NOTPAY")) {
                    //return processNotiryRequert(inputPayingAmount,seqStr,out_trade_no);//等待确认
                    xml = WeiXinHttpPostRequest.closeorder(out_trade_no);//关闭订单接口
                    return_code  = getValueFromXml(xml,"<return_code><![CDATA[","]]></return_code>");
                    if (return_code.equals("SUCCESS")) {//关闭接收成功
                        result_code = getValueFromXml(xml, "<result_code><![CDATA[", "]]></result_code>");
                        if (result_code.equals("SUCCESS")) {//关闭订单成功
                            return null;
                        } else {
                            String err_code_des = getValueFromXml(xml,"<err_code_des><![CDATA[","]]></err_code_des>");
                            CreamToolkit.logMessage(err_code_des);
                            app.getWarningIndicator().setMessage(err_code_des);
                        }
                    } else {//查询失败
                        String return_msg = getValueFromXml(xml,"<return_msg><![CDATA[","]]></return_msg>");
                        CreamToolkit.logMessage(return_msg);
                        app.getWarningIndicator().setMessage(return_msg);
                    }
                }*/ else {
                    CreamToolkit.logMessage(getTrade_state(trade_state));
                    app.getWarningIndicator().setMessage(getTrade_state(trade_state));
                    return null;
                }
            } else {
                String err_code_des = getValueFromXml(xml,"<err_code_des><![CDATA[","]]></err_code_des>");
                CreamToolkit.logMessage(err_code_des);
                app.getWarningIndicator().setMessage(err_code_des);
            }
        } else {//查询失败
            String return_msg = getValueFromXml(xml,"<return_msg><![CDATA[","]]></return_msg>");
            CreamToolkit.logMessage(return_msg);
            app.getWarningIndicator().setMessage(return_msg);
        }
        return null;
    }


    public WeiXin_detail addWeiXinDetail(String inputPayingAmount, String transactionId, String seq, String openId, String promotionDetail) {
        WeiXin_detail weiXin_detail;
        String promotionId = "";
        try {
            JSONObject jsonStr = JSON.parseObject(promotionDetail);

            if (jsonStr.get("promotion_detail") != null) {

                app.getMessageIndicator().setMessage(res.getString("UsedCoupons"));
                int size = ((JSONArray) (jsonStr.get("promotion_detail"))).size();
                if (size >= 0) {
                    promotionId = ((JSONObject) ((JSONArray) (jsonStr.get("promotion_detail"))).get(0)).getString("promotion_id");
                }
            } else {
                app.getMessageIndicator().setMessage(res.getString("UnusedCoupons"));
            }
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
            CreamToolkit.logMessage("promotion_detail Fail");
        }
        try{
            HYIDouble amt = new HYIDouble(inputPayingAmount).divide(new HYIDouble(100), 2, 4).setScale(2, BigDecimal.ROUND_HALF_UP);
            weiXin_detail = new WeiXin_detail();
            weiXin_detail.setSTORENUMBER(curTransaction.getStoreNumber());
            weiXin_detail.setPOSNUMBER(curTransaction.getTerminalNumber());
            weiXin_detail.setTRANSACTIONNUMBER(curTransaction.getTransactionNumber());
            weiXin_detail.setTOTAL_FEE(amt);
            weiXin_detail.setTRANSACTION_ID(transactionId);
            weiXin_detail.setSEQ(seq);
            weiXin_detail.setOPENID(openId);
            weiXin_detail.setSYSTEMDATE(new Date());
            weiXin_detail.setZSEQ(curTransaction.getZSequenceNumber());
            weiXin_detail.setPromotion_id(promotionId);
            curTransaction.addWeiXinList(weiXin_detail);
            CreamToolkit.logMessage("transactionNumber：" + weiXin_detail.getTRANSACTIONNUMBER() + "，transaction_id："
                    + weiXin_detail.getTOTAL_FEE()+ ",systemDate：" + weiXin_detail.getSYSTEMDATE().toString()
                    + " is add trans.addWeiXinDetailList");
            this.seq = 0;

            return weiXin_detail;
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return null;
    }

    public String getValueFromXml(String xml, String token1, String token2) {
        String result = "";
        int a = xml.indexOf(token1);
        int b = xml.indexOf(token2);

        if (a >= 0 && b >= 0) {
            result = xml.substring(a+token1.length(), b);
        }
        return result;
    }

    public String getTrade_state(String trade_state) {
        if (trade_state.equals("REFUND"))
            return "交易转入退款";
        else if (trade_state.equals("CLOSED"))
            return "交易已关闭";
        else if (trade_state.equals("REVERSE"))
            return "交易已冲正";
        else if (trade_state.equals("REVOK"))
            return "交易已撤销";
        else if (trade_state.equals("PAYERROR"))
            return "支付失败(其他原因，如银行返回失败)";
        else if (trade_state.equals("NOPAY"))
            return "未支付(确认支付超时)";
        return "";
    }

    public String getPayAmount() {
        return payAmount;
    }
    public String getPayID() {
        return payID;
    }
}
