package hyi.cream.state;

import java.math.*;
import hyi.cream.util.*;

/**
 * PromMatch definition class.
 * 提供一个促销配对抽象类
 * 
 * @author Meyer 
 * @version 1.0
 */
public abstract class PromMatch {
    /**
     * 更新记录
     * 
     * Meyer / 1.0 / 2003-01-06
     * 		1. Create the class file
     * 
     */
	private String promID;
	private HYIDouble totalAmt;
	private boolean isMatched;
	
	public void setPromID(String aPromID) {
		this.promID = aPromID;
	}
	
	public void setTotalAmt(HYIDouble aTotalAmt) {
		this.totalAmt = aTotalAmt;		
	}
	
	public void setMatchedFlag(boolean matchedFlag) {
		this.isMatched = matchedFlag;
	}

	/**
	 * 取得促销编号
	 */
	public String getPromID() {
		return this.promID;
	}
	
	/**
	 * 取得此次促销配对涉及的总金额
	 */
	public HYIDouble getTotalAmt() {
		return this.totalAmt;
	}
	
	/**
	 * 查询是否已经Match
	 */ 
	public boolean isMatched() {
		return isMatched;
	}
}
