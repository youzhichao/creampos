
// Copyright (c) 2000 HYI
package hyi.cream.state;

//import hyi.cream.POSPeripheralHome;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.spos.JposException;
import hyi.spos.ToneIndicator;

import java.math.BigDecimal;
import java.util.EventObject;

//import jpos.JposException;
//import jpos.ToneIndicator;

/**
 * A Class class.
 * <P> 对OpenPrice会根据用户输入，把OriginalPrice 与 UnitPrice 设成一样
 * @author dai
 */
public class OpenPriceState extends State {

    private String openPrice            = "";
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static OpenPriceState openPriceState = null;
//	private Thread warningThread = null;
	private ToneIndicator tone          = null;

    public static OpenPriceState getInstance() {
        try {
            if (openPriceState == null) {
                openPriceState = new OpenPriceState();
            }
        } catch (InstantiationException ex) {
        }
        return openPriceState;
    }

    /**
     * Constructor
     */
    public OpenPriceState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
		//System.out.println("This is OpenState's Entry!");
		if (sourceState instanceof PluReadyState) {         
            openPrice = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
		} else if (sourceState instanceof WeiXiuState) {
			System.out.println("wei xiu to open price entry");
			openPrice = "";
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
        } else if (sourceState instanceof OpenPriceState) {
			if (event.getSource() instanceof ClearButton) {
			    try {
					if (tone != null) {
						tone.clearOutput();
						tone.release();
					}
				} catch (JposException je) { }
                openPrice = "";              
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
                app.getWarningIndicator().setMessage("");
            } else if (event.getSource() instanceof NumberButton) {
                try {
					if (tone != null) {
						tone.clearOutput();
						tone.release();
					}
				} catch (JposException je) { }
                NumberButton pb = (NumberButton)event.getSource();
                openPrice = openPrice + pb.getNumberLabel();  
                app.getMessageIndicator().setMessage(openPrice);
                app.getWarningIndicator().setMessage("");
            }
        }
    }


    public Class exit(EventObject event, State sinkState) {
        //System.out.println("OpenPriceState exit");
        HYIDouble price = null;
        //app.getMessageIndicator().setMessage("");
        if (event.getSource() instanceof EnterButton) {
            Transaction curTran = app.getCurrentTransaction();  
            if (openPrice.equals("")) {
                price = curTran.getCurrentLineItem().getUnitPrice();
                if (price.compareTo(new HYIDouble(0)) == 0) {
					//System.out.println("########################################");
					POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
					try {
						tone = posHome.getToneIndicator();
					} catch (Exception ne) {
                        CreamToolkit.logMessage(ne);
                    }
					try {
						if (!tone.getDeviceEnabled())
							tone.setDeviceEnabled(true);
						if (!tone.getClaimed()) {
							 tone.claim(0);
						//tone.claim(0);
							 tone.setAsyncMode(true);
							 tone.sound(99999, 500);
						}
					} catch (JposException je) {System.out.println(je);}
                    app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
                    return OpenPriceState.class;
                }
            } else {          

                if (!CreamToolkit.checkInput(openPrice, new HYIDouble(0))) {
                    openPrice = "";
                    app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                    return OpenPriceState.class;
                }
                price = new HYIDouble(openPrice);
                if (price.compareTo(PARAM.getMaxPrice()) == 1) {
                    POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyPrice"));
                    openPrice = "";
                    return OpenPriceState.class;
                }
            }
            LineItem lineItem = curTran.getCurrentLineItem();
            // 对OpenPrice会把OriginalPrice 与 UnitPrice 设成一样
    		lineItem.setOriginalPrice(price.setScale(2, BigDecimal.ROUND_HALF_UP));
            lineItem.setUnitPrice(price.setScale(2, BigDecimal.ROUND_HALF_UP));

            lineItem.setAfterDiscountAmount(lineItem.getAmount());
            lineItem.setAfterSIAmount(lineItem.getAmount());

            try {
                curTran.changeLineItem(-1, lineItem);
            } catch (LineItemNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("LineItem not found at " + this);
			}
			try {
				if (tone != null) {
					tone.clearOutput();
					tone.release();
				}
			} catch (JposException je) { } 
            app.getWarningIndicator().setMessage("");
			return MixAndMatchState.class;
        } else {
            if (!(event.getSource() instanceof NumberButton)
				&& !(event.getSource() instanceof ClearButton)) {
				//System.out.println("########################################");
				POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
				try {
					tone = posHome.getToneIndicator();
					if (!tone.getDeviceEnabled())
						tone.setDeviceEnabled(true);
					if (!tone.getClaimed()) {
						 tone.claim(0);
					//tone.claim(0);
						 tone.setAsyncMode(true);
						 tone.sound(99999, 500);
					}
				} catch (Exception ne) {
					CreamToolkit.logMessage(ne); 
				}
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
			}
			return OpenPriceState.class;
		}
	}
}

 


