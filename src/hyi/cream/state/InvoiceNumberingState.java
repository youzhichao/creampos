
// Copyright (c) 2000 HYI
package hyi.cream.state;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class InvoiceNumberingState extends SomeAGNumberingState {
    static InvoiceNumberingState invoiceNumberingState = null;

    public static InvoiceNumberingState getInstance() {
        try {
            if (invoiceNumberingState == null) {
                invoiceNumberingState = new InvoiceNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return invoiceNumberingState;
    }

    /**
     * Constructor
     */
    public InvoiceNumberingState() throws InstantiationException {
    }
}

 