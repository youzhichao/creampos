/*
 * Created on 2005-3-24
 *
 * To change the peidanumber
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.state;

//import hyi.cream.POSPeripheralHome;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.ToneIndicator;

import java.util.EventObject;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

//import jpos.JposException;
//import jpos.ToneIndicator;

/**
 * @author ll
 */
public class PeiDa2State extends State {

	private String annotatedid          = "";
	private POSTerminalApplication app  = POSTerminalApplication.getInstance();
	static  PeiDa2State peiDaState2 = null;
	private ToneIndicator tone          = null;
	private boolean exit = false; 
	
	public static PeiDa2State getInstance() {
		try {
			if (peiDaState2 == null) {
				peiDaState2 = new PeiDa2State();
			}
		} catch (InstantiationException ex) {
		}
		return peiDaState2;
	}

	/**
	 * Constructor
	 */
	public PeiDa2State() throws InstantiationException {
	}

	/* (non-Javadoc)
	 * @see hyi.cream.state.State#entry(java.util.EventObject, hyi.cream.state.State)
	 */
	public void entry(EventObject event, State sourceState) {
//		System.out.println("This is WeiXiuState Entry!");
		if (sourceState instanceof IdleState) {         
			annotatedid = "";
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PeiDa2Message"));
		} else if (sourceState instanceof PeiDa2State) {
			if (event.getSource() instanceof ClearButton) {
				if (annotatedid == "") {
					exit = true;
				} else {
					try {
						if (tone != null) {
							tone.clearOutput();
							tone.release();
						}
					} catch (JposException je) { }
					annotatedid = "";              
					app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PeiDa2Message"));
					app.getWarningIndicator().setMessage("");
				}
			} else if (event.getSource() instanceof NumberButton) {
				try {
					if (tone != null) {
						tone.clearOutput();
						tone.release();
					}
				} catch (JposException je) { }
				NumberButton pb = (NumberButton)event.getSource();
				annotatedid = annotatedid + pb.getNumberLabel();  
				app.getMessageIndicator().setMessage(annotatedid);
				app.getWarningIndicator().setMessage("");
			}
		}
	}

	/* (non-Javadoc)
	 * @see hyi.cream.state.State#exit(java.util.EventObject, hyi.cream.state.State)
	 */
	public Class exit(EventObject event, State sinkState) {
//		System.out.println("WeiXiuState exit");
		if (exit) {
			exit = false;
			app.getCurrentTransaction().setAnnotatedType("");						
			app.getCurrentTransaction().setAnnotatedId("");
			Iterator it = app.getCurrentTransaction().getLineItemsIterator();
			List list = new Vector();
			while (it.hasNext()) {
				Object object = it.next();
				if (it.hasNext())
					list.add(object);
			}
			app.getCurrentTransaction().clearLineItem();
			for (int i = 0; i < list.size(); i++) {
				try {
					app.getCurrentTransaction().addLineItem((LineItem) list.get(i), false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return IdleState.class;
		}
		String type = app.getCurrentTransaction().getAnnotatedType();
		if (type != null && type.equalsIgnoreCase("W"))  
			return IdleState.class;

		if (event.getSource() instanceof EnterButton) {
			Transaction curTran = app.getCurrentTransaction();  
			if (annotatedid.equals("")) {
				//System.out.println("########################################");
				POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
				try {
					tone = posHome.getToneIndicator();
				} catch (Exception ne) {
                    CreamToolkit.logMessage(ne);
                }
				try {
					if (!tone.getDeviceEnabled())
						tone.setDeviceEnabled(true);
					if (!tone.getClaimed()) {
						 tone.claim(0);
					//tone.claim(0);
						 tone.setAsyncMode(true);
						 tone.sound(99999, 500);
					}
				} catch (JposException je) {System.out.println(je);}
				
				return PeiDa2State.class;
			} else {
				app.getCurrentTransaction().setAnnotatedType("P");						
				app.getCurrentTransaction().setAnnotatedId(annotatedid);
//				System.out.println("annotatedid : " + app.getCurrentTransaction().getAnnotatedId());
				app.getWarningIndicator().setMessage("");
				return IdleState.class;
			}
		} else {
			if (!(event.getSource() instanceof NumberButton)
				&& !(event.getSource() instanceof ClearButton)) {
				//System.out.println("########################################");
				POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
				try {
					tone = posHome.getToneIndicator();
				} catch (Exception ne) {
                    CreamToolkit.logMessage(ne);
                }
				try {
					if (!tone.getDeviceEnabled())
						tone.setDeviceEnabled(true);
					if (!tone.getClaimed()) {
						 tone.claim(0);
					//tone.claim(0);
						 tone.setAsyncMode(true);
						 tone.sound(99999, 500);
					}
				} catch (JposException je) {
					System.out.println(je);
				}
				app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("PeiDa2Message"));
			}
			return PeiDa2State.class;
		}
	}
}
