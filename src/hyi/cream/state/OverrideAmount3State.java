package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;
import java.math.*;

public class OverrideAmount3State extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private Transaction trans = app.getCurrentTransaction();
	static OverrideAmount3State overrideAmount2State = null;
	static final String OverrideAmountLowerWholeSalePrice = "OverrideAmountLowerWholeSalePrice";
    
    public static OverrideAmount3State getInstance() {
        try {
            if (overrideAmount2State == null) {
                overrideAmount2State = new OverrideAmount3State();
            }
        } catch (InstantiationException ex) {
        }
        return overrideAmount2State;
    }
	
	public OverrideAmount3State() throws InstantiationException {
    }
	
	public void entry(EventObject event, State sourceState) {
	}
	
	public Class exit(EventObject event, State sinkState) {
		setForOverrideAmount(OverrideAmountState.tempPrice, trans.getCurrentLineItem());
		OverrideAmountState.tempPrice = "";
		
		//if (GetProperty.getOverrideAmountReasonPopup("no").equalsIgnoreCase("yes"))
        if (false)
        	return OverrideReasonState.class;
        else
        	return MixAndMatchState.class;
	}
	
	public void setForOverrideAmount(String tempPrice, LineItem curLineItem) {
        if (tempPrice != null && tempPrice.trim().length() > 0)
			curLineItem.setUnitPrice(new HYIDouble(tempPrice));
        else
            curLineItem.setUnitPrice(new HYIDouble(0.00));
        //curLineItem.setQuantity(new HYIDouble(1.00));
		curLineItem.setAmount(curLineItem.getUnitPrice().multiply(curLineItem.getQuantity()));
		curLineItem.setOriginalAfterDiscountAmount(curLineItem.getAfterDiscountAmount());
        curLineItem.setAfterDiscountAmount(curLineItem.getAmount());
        //curLineItem.setAddRebateAmount(curLineItem.getAfterDiscountAmount().multiply(curLineItem.getRebateRate()));
        //modify 2004-03-29 有变价的商品还原金为0
        curLineItem.setRebateRate(new HYIDouble(0.00));
        curLineItem.setAddRebateAmount(new HYIDouble(0.00));
        
        curLineItem.caculateAndSetTaxAmount();
        curLineItem.setAfterSIAmount(curLineItem.getAmount());
        curLineItem.setDiscountType("O");
        // TODO 是否变价后才会填入授权人编号 ?
        String nc = app.getNewCashierID();
        if (nc == null || nc.trim().length() == 0)
        	nc = trans.getCashierNumber();
        curLineItem.setAuthorNoContent(nc); // 暂时放到Content中

//        //Bruce/2003-08-08
//        if (GetProperty.getComputeDiscountRate("no").equalsIgnoreCase("yes")) {
//            try {
//                HYIDouble rate = curLineItem.getUnitPrice().divide(curLineItem.getOriginalPrice(),
//                    2, BigDecimal.ROUND_HALF_UP);
//                curLineItem.setDiscountRate(rate);
//                DiscountType dis = DiscountType.queryByDiscountRate(rate);
//                if (dis != null) {
//                    curLineItem.setDiscountRateID(dis.getID());
//                }
//            } catch (ArithmeticException e) {
//            //初始价格为0，不设置折扣信息 zhaohong 2003-10-30
//            }
//        }
        
        try {
        	app.getCurrentTransaction().changeLineItem(-1, trans.getCurrentLineItem());
        } catch (LineItemNotFoundException e) {
        	CreamToolkit.logMessage(e.toString());
        	CreamToolkit.logMessage("LineItem not found at " + this);
        }
        CreamToolkit.showText(app.getCurrentTransaction(), 0);
        
        
        
	}
	
	
	
}