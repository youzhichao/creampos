package hyi.cream.state.periodical;

import hyi.cream.CreamSession;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.LineItem;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.state.SomeAGReadyState;
import hyi.cream.state.State;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;
import java.util.ResourceBundle;

public class OrderEndMonthReadyState extends SomeAGReadyState {
//	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	static OrderEndMonthReadyState instance;

	private ResourceBundle res = CreamToolkit.GetResource();

	public static OrderEndMonthReadyState getInstance() {
		try {
			if (instance == null)
				instance = new OrderEndMonthReadyState();
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	public OrderEndMonthReadyState() throws InstantiationException {
	}

	
	public Class exit(EventObject event, State sinkState) {
		app.getWarningIndicator().setMessage("");
		if (event.getSource() instanceof ClearButton) {
			LineItem li = (LineItem) CreamSession.getInstance().getAttribute(
					WorkingStateEnum.PERIODICAL_ORDER_STATE , "CurLineItem");
			li.setDescriptionAndSpecification(li.getDescriptionAndSpecification().substring(0,
					li.getDescriptionAndSpecification().lastIndexOf(".")));
	        try {
	            app.getCurrentTransaction().changeLineItem(-1, li);
	        } catch (LineItemNotFoundException e) {
	            CreamToolkit.logMessage(e.toString());
	            CreamToolkit.logMessage("LineItem not found at " + this);
	        }
		}
		return sinkState.getClass();
	}

	public String getPromptedMessage() {
		return res.getString("InputOrderEndMonth");
	}
	
	@Override
	public String getWarningMessage() {
		return app.getWarningIndicator().getMessage();
	}
}
