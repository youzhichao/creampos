package hyi.cream.state.periodical;

import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import hyi.cream.util.DbConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;


public class PeriodicalUtils {

	/**
	 * 从inline server端获得可定刊号列表
	 * inline client端
	 * @return
	 */
	public static List<String> getBookableSubIssns(String itemNo, String start, String end) {
        if (!Client.getInstance().isConnected()) {
        	return null;
        }

        try {
            Client client = Client.getInstance();
            client.processCommand("getBookableSubIssns " + itemNo + " " + start + " " + end);
            List list = (List) client.getReturnObject2();
            return list;
        } catch (ClientCommandException e) {
            return null;
        }

	}

	/**
	 * 从cake.itembookcodes中获得可定刊号列表
	 * inline server端
	 * 给定品号和期刊月份给出可订的的刊号（2位年+2位刊号）
	 * @return
	 */
	public static List<String> processGetBookableSubIssns(String itemNo, String start, String end) {
		Server.log("processGetBookableSubIssns | " + itemNo + " " + start + " " + end);
		DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        Map<String, String> datas = new HashMap<String, String>();
        List<String> months = new ArrayList<String>();
		String sql = "SELECT month, codes FROM itembookcodes WHERE itemNo = '" + itemNo + "' ORDER BY month";
        try {
        	connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
            	String m = resultSet.getString(1);

            	// 数据库数据有误
            	try {
            		int t = Integer.parseInt(m);
            		if (t <= 0 || t > 12)
            			continue;
            	} catch (NumberFormatException  e) {
            		continue;
            	}

            	while (m.length() < 2)
            		m = "0" + m;
            	String str = resultSet.getString(2);
            	if (str == null || str.trim().length() == 0)
            		continue;
            	datas.put(m, str);
            	// example of datas:
            	// m    str
				// 01 	01,02
				// 02 	03,04
				// 03 	05,06
            	months.add(m);
            }
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
            Server.log("SQLException: " + sql);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();

            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }
            CreamToolkit.releaseConnection(connection);
        }

        List<String> bookableYearMonth = new ArrayList<String>();
        String startMonth = start.substring(2);
        String endMonth = end.substring(2);
        int startYear = Integer.parseInt(start.substring(0, 2));
        int endYear = Integer.parseInt(end.substring(0, 2));
        for (int curY = startYear; curY <= endYear; curY++) {
	        for (String m : months) {
	        	String y = String.valueOf(curY);
	        	while (y.length() < 2)
	        		y = "0" + y;
		        if (curY == startYear && curY== endYear) {
		        	if (m.compareTo(startMonth) >= 0 && m.compareTo(endMonth) <= 0)
		        		bookableYearMonth.add(y + m);
		        } else if (curY == startYear && curY < endYear) {
		        	// 预订的第一年，一年部分订
		        	if (m.compareTo(startMonth) >= 0)
		        		bookableYearMonth.add(y + m);
		        } else if (curY > startYear && curY == endYear) {
		        	// 预订的最后年,一年部分订
		        	if (m.compareTo(endMonth) <= 0)
		        		bookableYearMonth.add(y + m);
		        } else {
		        	// 预订的当中年,一年都订
	        		bookableYearMonth.add(y + m);
		        }
	        }
        }

        List<String> bookableIssns = new ArrayList<String>();
        for (String ym : bookableYearMonth) {
        	String m = ym.substring(2);
        	String y = ym.substring(0, 2);
        	if (datas.containsKey(m)) {
        		List<String> subIssns = Arrays.asList(StringUtils.split(datas.get(m), ","));
        		for (String m1 : subIssns) {
        			bookableIssns.add(y + m1);
                	// example of bookableIssns:
    				// 0801
    				// 0802
    				// 0803
        			// 0804
        			// 0805
        			// 0806
        		}
        	}
        }

        Server.log("processGetBookableSubIssns | " + bookableIssns);
		return bookableIssns;
	}

	/**
	 * 从cake.ordbookhead,cake.ordbookdtl中获得已定未领刊号列表
	 * inline client端
	 * @return
	 */
	public static List<BookedInfo> getBookedInfos(String memberID) {
        if (!Client.getInstance().isConnected()) {
        	return null;
        }

        try {
            Client client = Client.getInstance();
            client.processCommand("getBookedInfos " + memberID);
            List list = (List)client.getReturnObject2();
            return list;
        } catch (ClientCommandException e) {
            return null;
        }
	}

    public static List<BookedInfo> getReturnableInfos(String memberID, String state1) {
        if (!Client.getInstance().isConnected()) {
            return null;
        }

        try {
            Client client = Client.getInstance();
            client.processCommand("getReturnBookInfos " + memberID + " " + state1);
            List list = (List)client.getReturnObject2();
            return list;
        } catch (ClientCommandException e) {
            return null;
        }
    }

	/**
	 * 从cake.ordbookhead,cake.ordbookdtl中获得已定未领刊号列表
	 * inline server端
	 * @return
	 */
	public static List<BookedInfo> processGetBookedInfos(String memberID) {
		List<BookedInfo> bookedInfos = new ArrayList<BookedInfo>();
		String sql = "SELECT head.billno,head.custordprice,dtl.itemno,dtl.dtlcode1," +
				"dtl.planqty,dtl.qtybase,dtl.itemseq,dtl.dtlseq FROM " +
				"ordbookhead AS head LEFT JOIN ordbookdtl dtl " +
				//20080724/James 添加head.itemSeq=dtl.itemSeq以确保头明细对应一致
				"ON head.storeid = dtl.storeid AND head.billno = dtl.billno AND head.itemSeq=dtl.itemSeq WHERE " +
				"head.cardno = '" + memberID + "' AND (head.billstate = 1 OR head.billstate = 2) " +
				"AND dtl.state = 0" ;

        Server.log("processGetBookedInfos | sql : " + sql);
		DbConnection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
        	connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
            	BookedInfo info = new BookedInfo();
            	info.setBillNo(resultSet.getString("head.billno"));
                BigDecimal price = resultSet.getBigDecimal("head.custordprice");
                if (price != null) {
                	info.setCustOrdPrice1(new HYIDouble(price.doubleValue()));
                } else {
                	info.setCustOrdPrice1(new HYIDouble(0));
                }
            	info.setItemNo(resultSet.getString("dtl.itemno"));
            	String dtlcode1 = resultSet.getString("dtl.dtlcode1");
            	info.setYear(dtlcode1.substring(0, 2));
            	info.setDtlcode1(dtlcode1.substring(2, dtlcode1.length()));
            	info.setPlanQty(resultSet.getInt("dtl.planqty"));
            	info.setQtyBase(resultSet.getInt("dtl.qtybase"));
            	info.setItemSeq(resultSet.getInt("dtl.itemseq"));
            	info.setDtlSeq(resultSet.getInt("dtl.dtlseq"));
            	bookedInfos.add(info);
            }
            Server.log("processGetBookedInfos | " + bookedInfos);
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
            Server.log("SQLException: " + sql);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
            }
            CreamToolkit.releaseConnection(connection);
        }
		return bookedInfos;
	}

	public static void main(String[] args) {
		PeriodicalUtils.processGetBookableSubIssns(null, "0808", "1006");
	}
}
