package hyi.cream.state.periodical;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Transaction;
import hyi.cream.state.State;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;

public class PeriodicalReturnEndState extends State {
	static PeriodicalReturnEndState instance = null;
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	public static PeriodicalReturnEndState getInstance() {
		try {
			if (instance == null) {
				instance = new PeriodicalReturnEndState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	/**
	 * Constructor
	 */
	public PeriodicalReturnEndState() throws InstantiationException {
	}
	
	@Override
	public void entry(EventObject event, State sourceState) {
		app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PerioicalReturnMessage"));
	}

	@Override
	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof ClearButton) {
		    Transaction tran = app.getCurrentTransaction();
			tran.clearLineItem();
    		tran.setDefaultState();
//		} else if (event.getSource() instanceof AgeLevelButton) {
//			DbConnection connection = null;
//			try {
//				connection = CreamToolkit.getPooledConnection();
//				CreamPrinter printer = CreamPrinter.getInstance();
//				for (Object lineItemObj : app.getCurrentTransaction().getLineItems()) {
//					LineItem lineItem = (LineItem) lineItemObj;
//					if (!lineItem.getPrinted())
//						printer.printLineItem(connection, lineItem);
//				}
//			} catch (SQLException e) {
//				CreamToolkit.logMessage(e);
//				CreamToolkit.haltSystemOnDatabaseFatalError();
//				return PeriodicalReturnEndState.class;
//			} finally {
//				CreamToolkit.releaseConnection(connection);
//			}
//			return SummaryState.class;
		}
		return sinkState.getClass();
	}
}
