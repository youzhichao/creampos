package hyi.cream.state.periodical;

import hyi.cream.util.HYIDouble;

import java.io.Serializable;

public class BookedInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String billNo; // 期刊预订号
	private String itemNo; //品号
	private String year;   //期刊的年份
	private String dtlcode1; //期刊号
	private int planQty; //本期预订数量
	private int qtyBase; //未领数量
	private int itemSeq; //单品序号
	private int dtlSeq; //期刊序号
	private HYIDouble custOrdPrice1; // 预订单价
	
	public String getDtlcode1() {
		return dtlcode1;
	}
	public void setDtlcode1(String dtlcode1) {
		this.dtlcode1 = dtlcode1;
	}
	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}
	public int getPlanQty() {
		return planQty;
	}
	public void setPlanQty(int planQty) {
		this.planQty = planQty;
	}
	public int getQtyBase() {
		return qtyBase;
	}
	public void setQtyBase(int qtyBase) {
		this.qtyBase = qtyBase;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public int getDtlSeq() {
		return dtlSeq;
	}
	public void setDtlSeq(int dtlSeq) {
		this.dtlSeq = dtlSeq;
	}
	public int getItemSeq() {
		return itemSeq;
	}
	public void setItemSeq(int itemSeq) {
		this.itemSeq = itemSeq;
	}
	public HYIDouble getCustOrdPrice1() {
		return custOrdPrice1;
	}
	public void setCustOrdPrice1(HYIDouble custOrdPrice1) {
		this.custOrdPrice1 = custOrdPrice1;
	}
}
