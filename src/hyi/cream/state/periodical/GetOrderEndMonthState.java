// Copyright (c) 2000 HYI
package hyi.cream.state.periodical;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.LineItem;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.state.GetSomeAGState;
import hyi.cream.util.CreamToolkit;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;

public class GetOrderEndMonthState extends GetSomeAGState {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private ResourceBundle res = CreamToolkit.GetResource();

	static GetOrderEndMonthState instance = null;

	public static GetOrderEndMonthState getInstance() {
		try {
			if (instance == null) {
				instance = new GetOrderEndMonthState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	public GetOrderEndMonthState() throws InstantiationException {
	}

	public boolean checkValidity() {
		String str = getAlphanumericData();
		if (str.length() != 4 && str.length() > 0) {
			setWarningMessage(res.getString("InputOrderMonthWarning"));
			return false;
		}
		
		// 直接按【确认】键,系统提供默认数据
		if (str.length() == 0) {
			str = (String) CreamSession.getInstance().getAttribute(
					WorkingStateEnum.PERIODICAL_ORDER_STATE, "OrderStartYearMonth");
		} else {
			try {
				Integer.parseInt(str);
			} catch (NumberFormatException e) {
				setWarningMessage(res.getString("InputOrderMonthWarning"));
				return false;
			}
			try {
				if (Integer.parseInt(str.substring(2)) > 12) {
					setWarningMessage(res.getString("InputOrderMonthWarning1"));
					return false;
				}
			} catch (NumberFormatException e) {
				setWarningMessage(res.getString("InputOrderMonthWarning"));
				return false;
			}
		}
		
		String startYearMonth = (String) CreamSession.getInstance().getAttribute(
				WorkingStateEnum.PERIODICAL_ORDER_STATE, 
				"OrderStartYearMonth");
		
		if (startYearMonth.compareTo(str) > 0) {
			setWarningMessage(res.getString("InputOrderEndWarning"));
			return false;
		}

		LineItem li = (LineItem) CreamSession.getInstance().getAttribute(
				WorkingStateEnum.PERIODICAL_ORDER_STATE , "CurLineItem");
		li.setDescriptionAndSpecification(li.getDescriptionAndSpecification()
				+ "--" + str);
        String beginMonth = li.getContent().split("\\.")[0];
		li.setContent(beginMonth + "." + str);
		CreamSession.getInstance().setAttribute(WorkingStateEnum.PERIODICAL_ORDER_STATE, 
				"OrderEndYearMonth", str);
        try {
            app.getCurrentTransaction().changeLineItem(-1, li);
        } catch (LineItemNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("LineItem not found at " + this);
        }

		return true;
	}

	public Class getUltimateSinkState() {
		// 是否有预订
		return ListBookableState.class;
	}

	public Class getInnerInitialState() {
		return OrderEndMonthReadyState.class;
	}
}
