
// Copyright (c) 2000 HYI
package hyi.cream.state.periodical;

import hyi.cream.state.SomeAGNumberingState;

/**
 * A Class class.
 * <P>
 */
public class OrderEndMonthNumberingState extends SomeAGNumberingState {
    static OrderEndMonthNumberingState instance = null;

    public static OrderEndMonthNumberingState getInstance() {
        try {
            if (instance == null) {
                instance = new OrderEndMonthNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    /**
     * Constructor
     */
    public OrderEndMonthNumberingState() throws InstantiationException {
    }
}

 