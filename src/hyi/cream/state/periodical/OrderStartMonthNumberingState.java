
// Copyright (c) 2000 HYI
package hyi.cream.state.periodical;

import hyi.cream.state.SomeAGNumberingState;

/**
 * A Class class.
 * <P>
 */
public class OrderStartMonthNumberingState extends SomeAGNumberingState {
    static OrderStartMonthNumberingState instance = null;

    public static OrderStartMonthNumberingState getInstance() {
        try {
            if (instance == null) {
                instance = new OrderStartMonthNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    /**
     * Constructor
     */
    public OrderStartMonthNumberingState() throws InstantiationException {
    }
}

 