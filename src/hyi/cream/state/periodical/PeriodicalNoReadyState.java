package hyi.cream.state.periodical;

import hyi.cream.CreamSession;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.Transaction;
import hyi.cream.state.RemoveState;
import hyi.cream.state.SomeAGReadyState;
import hyi.cream.state.State;
import hyi.cream.state.VoidState;
import hyi.cream.state.WarningState;
import hyi.cream.uibeans.CancelButton;
import hyi.cream.uibeans.RemoveButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;
import java.util.ResourceBundle;

public class PeriodicalNoReadyState extends SomeAGReadyState {
	static PeriodicalNoReadyState instance;

	private ResourceBundle res = CreamToolkit.GetResource();

	public static PeriodicalNoReadyState getInstance() {
		try {
			if (instance == null)
				instance = new PeriodicalNoReadyState();
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	public PeriodicalNoReadyState() throws InstantiationException {
	}

    @Override
    public void entry(EventObject event, State sourceState) {
        super.entry(event, sourceState);

        //Bruce/20121017 Make it re-calc the transaction's SALEAMT
        app.getCurrentTransaction().initForAccum();
        app.getCurrentTransaction().accumulate();
        app.getItemList().repaint();
    }

    @Override
	public Class exit(EventObject event, State sinkState) {
		// super.exit(event, sinkState);
		Transaction curTran = app.getCurrentTransaction();
		app.getWarningIndicator().setMessage("");
		if (event.getSource() instanceof CancelButton) {
//			Collection fds = Cashier.getExistedFieldList("cashier");
//
//			if (!fds.contains("CASRIGHTS")) { // 不存在CASRIGHTS就直接让他做
//				return CancelState.class;
//			} else {
//				// modify by lxf 2003.02.13
//				CashierRightsCheckState.setSourceState("periodical.PeriodicalNoReadyState");
//				CashierRightsCheckState.setTargetState("CancelState");
//				return CashierRightsCheckState.class;
//				// return CancelState.class;
//			}
            app.getMessageIndicator().setMessage("");
            // init session
    		CreamSession.getInstance().setWorkingState(WorkingStateEnum.IDLE_STATE);
    		CreamSession.getInstance().removeAttributes(WorkingStateEnum.PERIODICAL_ORDER_STATE);
    		CreamSession.getInstance().removeAttributes(WorkingStateEnum.PERIODICAL_DRAW_STATE);
    		curTran.clearLineItem();
    		curTran.setDefaultState();
    		return PeriodicalIdleState.class;
//		} else if (event.getSource() instanceof AgeLevelButton) {
//			DbConnection connection = null;
//			try {
//				connection = CreamToolkit.getPooledConnection();
//				CreamPrinter printer = CreamPrinter.getInstance();
//                if (!printer.getHeaderPrinted()) {
//                    if (!printer.isPrinterAtRightStart()) { // 檢查印表機是否已定位
//                        setWarningMessage(CreamToolkit.GetResource().getString("PrinterIsNotAtRightPosition"));
//                        PrintPluWarningState.setExitState(PeriodicalNoReadyState.class);
//                        return PrintPluWarningState.class;
//                    }
//                    printer.printHeader(connection);
//                    printer.setHeaderPrinted(true);
//                }
//				for (Object lineItemObj : curTran.getLineItems()) {
//					LineItem lineItem = (LineItem) lineItemObj;
//					if (!lineItem.getPrinted())
//						printer.printLineItem(connection, lineItem);
//				}
//			} catch (SQLException e) {
//				CreamToolkit.logMessage(e);
//				CreamToolkit.haltSystemOnDatabaseFatalError();
//				return PeriodicalNoReadyState.class;
//			} finally {
//				CreamToolkit.releaseConnection(connection);
//			}
//			return SummaryState.class;
		} else if (event.getSource() instanceof RemoveButton) {
			if (curTran.getCurrentLineItem() == null
					|| curTran.getCurrentLineItem().getPrinted()
					|| curTran.getCurrentLineItem().getRemoved()) {
				this.setWarningMessage(res.getString("NoVoidPlu"));
				return WarningState.class;
			} else {
				String type = app.getCurrentTransaction().getAnnotatedType();
				if (type != null && type.equals("P")) {
					setWarningMessage(res.getString("CannotRemovePluInPeiDa"));
					return WarningState.class;
				}
				if (CreamSession.getInstance().getWorkingState() == 
						WorkingStateEnum.PERIODICAL_DRAW_STATE)
					return RemoveState.class;
				return VoidState.class;
			}
		} 
		return sinkState.getClass();
	}

	public String getPromptedMessage() {
		if (CreamSession.getInstance().getWorkingState() == 
				WorkingStateEnum.PERIODICAL_ORDER_STATE)
			return res.getString("InputOrderPeriodicalNo"); 
		else if (CreamSession.getInstance().getWorkingState() == 
				WorkingStateEnum.PERIODICAL_DRAW_STATE)
			return res.getString("InputDrawPeriodicalNo");
		return "";
	}

	@Override
	public String getWarningMessage() {
		return app.getWarningIndicator().getMessage();
	}
}
