// Copyright (c) 2000 HYI
package hyi.cream.state.periodical;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.PLU;
import hyi.cream.exception.CreateLineItemException;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.state.GetSomeAGState;
import hyi.cream.state.PluReadyState;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class GetPeriodicalNoState extends GetSomeAGState {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private ResourceBundle res = CreamToolkit.GetResource();

	static GetPeriodicalNoState instance = null;

	public static GetPeriodicalNoState getInstance() {
		try {
			if (instance == null) {
				instance = new GetPeriodicalNoState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	public GetPeriodicalNoState() throws InstantiationException {
	}

	public boolean checkValidity() {
		String dtlcode1 = "";
		String str = getAlphanumericData();
		if (str == null) {
			setWarningMessage(res.getString("PerioicalWaring"));
			return false;
		} else if (str.length() == 15) {
			// 期刊商品
			dtlcode1 = str.substring(13, 15);
			str = str.substring(0, 13);
		}
		PLU plu =
			PLU.queryByItemNumber(str);
//			PLU.queryBarCode(getAlphanumericData());
		if (plu == null) {
			plu = PLU.queryBarCode(str);
			if (plu == null) {
				setWarningMessage(res.getString("PerioicalWaring"));
				return false;
			}
		}
		if (plu.getItemtype() == null || 10 != plu.getItemtype()) { // 10=期刊商品
			setWarningMessage(res.getString("PerioicalWaring1"));
			return false;
		} else {
			if (CreamSession.getInstance().getWorkingState() == 
					WorkingStateEnum.PERIODICAL_ORDER_STATE) 
			{
				if (!"Y".equalsIgnoreCase(plu.getIsord())) {
					setWarningMessage(res.getString("PerioicalWaring2"));
					return false;
				}
				try {
					LineItem lineItem = PluReadyState.createLineItem(
					        app.getCurrentTransaction(), plu, new HYIDouble(0));
					lineItem.setDetailCode("J");
					app.getCurrentTransaction().addLineItem(lineItem);
					app.setTransactionEnd(false);
					CreamSession.getInstance().setAttribute(WorkingStateEnum.PERIODICAL_ORDER_STATE
							, "ItemNo", plu.getItemNumber());
					CreamSession.getInstance().setAttribute(WorkingStateEnum.PERIODICAL_ORDER_STATE
							, "CurLineItem", lineItem);
					// showText(lineItem);
				} catch (TooManyLineItemsException e) {
					CreamToolkit.logMessage(e.toString());
					CreamToolkit.logMessage("Too many LineItem exception at "
							+ this);
					setWarningMessage(res.getString("TooManyLineItems"));
					return false;
				} catch (CreateLineItemException e) {
					setWarningMessage(e.getMessage());
					return false;
				}
			} else if (CreamSession.getInstance().getWorkingState() == 
					WorkingStateEnum.PERIODICAL_DRAW_STATE) 
			{
				List<BookedInfo> drawingInfos = (List<BookedInfo>) CreamSession.getInstance().getAttribute(
						WorkingStateEnum.PERIODICAL_DRAW_STATE , "DrawInfos");
				if (drawingInfos != null) {
					for (BookedInfo info : drawingInfos) {
						if (info.getItemNo().equals(plu.getItemNumber()) 
								&& (dtlcode1.trim().length() == 0 
								|| info.getDtlcode1().equals(dtlcode1)))
						{
							setWarningMessage(res.getString("PerioicalWaring5"));
							return false;
						}
						
					}
				}
				
				List<BookedInfo> bookedInfos = (List<BookedInfo>) CreamSession.getInstance()
						.getAttribute(WorkingStateEnum.PERIODICAL_DRAW_STATE, "BookedInfos");

				List<BookedInfo> drawInfos = new ArrayList<BookedInfo>();
				app.getCurrentTransaction().setDealType2("K");
				for (BookedInfo info : bookedInfos) {
					if (!(info.getItemNo().equals(plu.getItemNumber()) 
							&& (dtlcode1.trim().length() == 0 
							|| info.getDtlcode1().equals(dtlcode1))))
						continue;
					
					try {
						LineItem lineItem = PluReadyState.createLineItem(
    					        app.getCurrentTransaction(), plu,
    							new HYIDouble(info.getQtyBase()));
						lineItem.setDtlcode1(info.getDtlcode1());
						lineItem.setDetailCode("K");
						lineItem.setContent(info.getBillNo() + "." + info.getItemSeq() + 
								"." + info.getDtlSeq());
						lineItem.setDescriptionAndSpecification(lineItem.getDescriptionAndSpecification()
								+ "." + (info.getYear() == null ? "" : info.getYear())
								+ "." + (lineItem.getDtlcode1() == null ? "" : lineItem.getDtlcode1()));
						app.getCurrentTransaction().addLineItem(lineItem);
						app.setTransactionEnd(false);
						drawInfos.add(info);
					} catch (TooManyLineItemsException e) {
						CreamToolkit.logMessage(e.toString());
						CreamToolkit.logMessage("Too many LineItem exception at "
								+ this);
						setWarningMessage(res.getString("TooManyLineItems"));
						return false;
					} catch (CreateLineItemException e) {
						setWarningMessage(e.getMessage());
						return false;
					}
				}
				if (drawInfos.size() == 0) {
					setWarningMessage(res.getString("PerioicalWaring4"));
					return false;
				}
				if (drawingInfos == null) 
					drawingInfos = new ArrayList<BookedInfo>();
				drawingInfos.addAll(drawInfos);
				CreamSession.getInstance().setAttribute(WorkingStateEnum.PERIODICAL_DRAW_STATE
						, "DrawInfos", drawingInfos);
			}
			return true;
		}
	}

	public Class getUltimateSinkState() {
		if (CreamSession.getInstance().getWorkingState() == 
				WorkingStateEnum.PERIODICAL_ORDER_STATE)
			return OrderStartMonthReadyState.class;
		else if (CreamSession.getInstance().getWorkingState() == 
				WorkingStateEnum.PERIODICAL_DRAW_STATE)
			return PeriodicalNoReadyState.class;
		return null;
	}

	public Class getInnerInitialState() {
		return PeriodicalNoReadyState.class;
	}
}
