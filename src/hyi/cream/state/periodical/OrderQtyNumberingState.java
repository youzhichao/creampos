
// Copyright (c) 2000 HYI
package hyi.cream.state.periodical;

import hyi.cream.state.SomeAGNumberingState;

/**
 * A Class class.
 * <P>
 */
public class OrderQtyNumberingState extends SomeAGNumberingState {
    static OrderQtyNumberingState instance = null;

    public static OrderQtyNumberingState getInstance() {
        try {
            if (instance == null) {
                instance = new OrderQtyNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    /**
     * Constructor
     */
    public OrderQtyNumberingState() throws InstantiationException {
    }
}

 