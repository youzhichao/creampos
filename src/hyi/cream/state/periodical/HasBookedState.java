package hyi.cream.state.periodical;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.state.MemberState;
import hyi.cream.state.State;
import hyi.cream.util.CreamToolkit;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventObject;
import java.util.List;
import java.util.ResourceBundle;

public class HasBookedState extends State {
	static HasBookedState instance = null;
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();

	public static HasBookedState getInstance() {
		try {
			if (instance == null) {
				instance = new HasBookedState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	/**
	 * Constructor
	 */
	public HasBookedState() throws InstantiationException {
	}
	
	@Override
	public void entry(EventObject event, State sourceState) {
	}

	@Override
	public Class exit(EventObject event, State sinkState) {
    	String memberID = app.getCurrentTransaction().getMemberID();
        WorkingStateEnum curState = CreamSession.getInstance().getWorkingState();
        List<BookedInfo> datas = null;
        if(curState == WorkingStateEnum.PERIODICAL_DRAW_STATE) {
            datas = PeriodicalUtils.getBookedInfos(memberID);
        } else if (curState == WorkingStateEnum.PERIODICAL_RETURN_STATE) {
            datas = PeriodicalUtils.getReturnableInfos(memberID,"L");
        }

        //WorkingStateEnum curState = CreamSession.getInstance().getWorkingState();
		if (datas == null || datas.isEmpty()) {
			String msg = "{0}";
			if (curState == WorkingStateEnum.PERIODICAL_DRAW_STATE)
				msg = res.getString("PerioicalDrawWaring");
			else if (curState == WorkingStateEnum.PERIODICAL_RETURN_STATE)
				msg = res.getString("PerioicalReturnWaring");
				
        	app.getWarningIndicator().setMessage(MessageFormat.format(
        			msg, memberID));
        	return MemberState.class; 
		}
		
		CreamSession.getInstance().setAttribute(curState, "BookedInfos", datas);
		if (curState == WorkingStateEnum.PERIODICAL_DRAW_STATE) {
            return PeriodicalNoReadyState.class;
        } else if (curState == WorkingStateEnum.PERIODICAL_RETURN_STATE) {
            return ListMayReturnState.class;
        }


		return sinkState.getClass();
	}
}
