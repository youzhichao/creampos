// Copyright (c) 2000 HYI
package hyi.cream.state.periodical;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.LineItem;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.state.GetSomeAGState;
import hyi.cream.state.SetQuantityState;
import hyi.cream.util.CreamToolkit;

import java.text.MessageFormat;
import java.util.ResourceBundle;

public class GetOrderQtyState extends GetSomeAGState {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private ResourceBundle res = CreamToolkit.GetResource();

	static GetOrderQtyState instance = null;

	public static GetOrderQtyState getInstance() {
		try {
			if (instance == null) {
				instance = new GetOrderQtyState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	public GetOrderQtyState() throws InstantiationException {
	}

	public boolean checkValidity() {
		String str = getAlphanumericData();
		// 直接按【确认】键,系统提供默认数据
		if (str.length() == 0) {
			str = "1";
		}
		try {
			if (Integer.parseInt(str) <= 0) {
				setWarningMessage(res.getString("number1"));
				return false;
			}
		} catch (NumberFormatException e) {
			return false;
		}
			
		LineItem li = (LineItem) CreamSession.getInstance().getAttribute(
				WorkingStateEnum.PERIODICAL_ORDER_STATE , "CurLineItem");

		int issnQty = (Integer) CreamSession.getInstance().getAttribute(
				WorkingStateEnum.PERIODICAL_ORDER_STATE, 
				"OrderIssnQty");
		String orderQty = String.valueOf(Integer.parseInt(str) * issnQty); 
		CreamSession.getInstance().setAttribute(
				WorkingStateEnum.PERIODICAL_ORDER_STATE, 
				"OrderQty", orderQty);

		li.setDescriptionAndSpecification(li.getDescriptionAndSpecification()
				+ "." + MessageFormat.format(CreamToolkit.GetResource().getString(
						"OrderPerioicalMessage1"), str));

        String[] bookInfo = li.getContent().split("\\.");
        String beginMonth = bookInfo[0];
        String endMonth = bookInfo[1];
        String issues = bookInfo[2];
		li.setContent(beginMonth + "." + endMonth + "." + issues + "." + str);

        try {
            app.getCurrentTransaction().changeLineItem(-1, li);
        } catch (LineItemNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("LineItem not found at " + this);
        }

		return true;
	}

	public Class getUltimateSinkState() {
		return SetQuantityState.class;
	}

	public Class getInnerInitialState() {
		return OrderQtyReadyState.class;
	}
}
