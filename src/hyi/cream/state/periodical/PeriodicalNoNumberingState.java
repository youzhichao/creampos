
// Copyright (c) 2000 HYI
package hyi.cream.state.periodical;

import hyi.cream.CreamSession;
import hyi.cream.WorkingStateEnum;
import hyi.cream.state.RemoveState;
import hyi.cream.state.SetQuantityState;
import hyi.cream.state.SomeAGNumberingState;
import hyi.cream.state.SomeAGReadyState;
import hyi.cream.state.State;
import hyi.cream.state.VoidState;
import hyi.cream.uibeans.QuantityButton;
import hyi.cream.uibeans.RemoveButton;

import java.util.EventObject;

/**
 * A Class class.
 * <P>
 */
public class PeriodicalNoNumberingState extends SomeAGNumberingState {
    static PeriodicalNoNumberingState instance = null;

    public static PeriodicalNoNumberingState getInstance() {
        try {
            if (instance == null) {
                instance = new PeriodicalNoNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    /**
     * Constructor
     */
    public PeriodicalNoNumberingState() throws InstantiationException {
    }
    
	@Override
	public Class exit(EventObject event, State sinkState) {
		// super.exit(event, sinkState);
		app.getWarningIndicator().setMessage("");
        if (sinkState instanceof SomeAGReadyState) {
            // Clear data buffer, clear message indicator
            str = "";
            app.getMessageIndicator().setMessage("");
		} else if (event.getSource() instanceof RemoveButton) {
			if (CreamSession.getInstance().getWorkingState() == 
					WorkingStateEnum.PERIODICAL_DRAW_STATE)
				return RemoveState.class;
			return VoidState.class;
		} else if (event.getSource() instanceof QuantityButton) {
            return SetQuantityState.class;
		}

		return sinkState.getClass();
	}

	public String getNumberString() {
        return str;
    }
}

 