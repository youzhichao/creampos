package hyi.cream.state.periodical;

import hyi.cream.state.SomeAGReadyState;
import hyi.cream.state.State;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;
import java.util.ResourceBundle;

public class OrderStartMonthReadyState extends SomeAGReadyState {
//	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	static OrderStartMonthReadyState instance;

	private ResourceBundle res = CreamToolkit.GetResource();

	public static OrderStartMonthReadyState getInstance() {
		try {
			if (instance == null)
				instance = new OrderStartMonthReadyState();
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	public OrderStartMonthReadyState() throws InstantiationException {
	}

	
	public Class exit(EventObject event, State sinkState) {
		app.getWarningIndicator().setMessage("");
		if (event.getSource() instanceof ClearButton) {
			
		}
		return sinkState.getClass();
	}

	public String getPromptedMessage() {
		return res.getString("InputOrderStartMonth");
	}
	
	@Override
	public String getWarningMessage() {
		return app.getWarningIndicator().getMessage();
	}
}
