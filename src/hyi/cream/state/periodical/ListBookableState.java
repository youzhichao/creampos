package hyi.cream.state.periodical;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.state.State;
import hyi.cream.util.CreamToolkit;

import java.text.MessageFormat;
import java.util.EventObject;
import java.util.List;
import java.util.ResourceBundle;

public class ListBookableState extends State {
	static ListBookableState instance = null;
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();

	public static ListBookableState getInstance() {
		try {
			if (instance == null) {
				instance = new ListBookableState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	/**
	 * Constructor
	 */
	public ListBookableState() throws InstantiationException {
	}
	
	@Override
	public void entry(EventObject event, State sourceState) {
	}

	@Override
	public Class exit(EventObject event, State sinkState) {
		String startYearMonth = (String) CreamSession.getInstance().getAttribute(
				WorkingStateEnum.PERIODICAL_ORDER_STATE, 
				"OrderStartYearMonth");
		String endYearMonth = (String) CreamSession.getInstance().getAttribute(
				WorkingStateEnum.PERIODICAL_ORDER_STATE, 
				"OrderEndYearMonth");
		String itemNo = (String) CreamSession.getInstance().getAttribute(
				WorkingStateEnum.PERIODICAL_ORDER_STATE, 
				"ItemNo");
		List datas = PeriodicalUtils.getBookableSubIssns(itemNo, startYearMonth, endYearMonth); 
//			PeriodicalUtils.processGetBookableSubIssns(itemNo, startYearMonth, endYearMonth);
		LineItem li = (LineItem) CreamSession.getInstance().getAttribute(
				WorkingStateEnum.PERIODICAL_ORDER_STATE , "CurLineItem");

		if (datas == null || datas.isEmpty()) {
			try {
				li.setRemoved(true);
				app.getCurrentTransaction().changeLineItem(-1, li);
//				app.getCurrentTransaction().removeLineItem(li);
			} catch (LineItemNotFoundException e) {
				e.printStackTrace();
			}
			app.getWarningIndicator().setMessage(res.getString("PerioicalWaring2"));
			return PeriodicalNoReadyState.class; 
		}
		
		li.setDescriptionAndSpecification(li.getDescriptionAndSpecification()
				+ "." + MessageFormat.format(CreamToolkit.GetResource().getString(
						"OrderPerioicalMessage"), String.valueOf(datas.size())));

        String[] bookInfo = li.getContent().split("\\.");
        String beginMonth = bookInfo[0];
        String endMonth = bookInfo[1];
		li.setContent(beginMonth + "." + endMonth + "." + datas.size());

        try {
            app.getCurrentTransaction().changeLineItem(-1, li);
        } catch (LineItemNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("LineItem not found at " + this);
        }
		
		CreamSession.getInstance().setAttribute(WorkingStateEnum.PERIODICAL_ORDER_STATE, 
				"OrderIssnQty", datas.size());
		return OrderQtyReadyState.class;
	}

}
