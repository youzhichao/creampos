package hyi.cream.state.periodical;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.PLU;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.CreateLineItemException;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.state.MemberState;
import hyi.cream.state.PluReadyState;
import hyi.cream.state.State;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;
import java.util.List;
import java.util.ResourceBundle;

public class ListMayReturnState extends State {
	static ListMayReturnState instance = null;
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();
    private String numberStr = "";
	public static ListMayReturnState getInstance() {
		try {
			if (instance == null) {
				instance = new ListMayReturnState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	/**
	 * Constructor
	 */
	public ListMayReturnState() throws InstantiationException {
	}
	
	@Override
    public void entry(EventObject event, State sourceState) {
        Object eventSource = null;
        if (event != null) {
            eventSource = event.getSource();
        }

        if (sourceState instanceof HasBookedState || sourceState instanceof PeriodicalPartReturnState) {
                app.getMessageIndicator().setMessage(CreamToolkit.getString("PeriodicalReturnType"));
        } else if (sourceState instanceof ListMayReturnState) {
            if (eventSource instanceof NumberButton) {
                numberStr = ((NumberButton) eventSource).getNumberLabel();
                app.getMessageIndicator().setMessage(numberStr);
            } else if (eventSource instanceof EnterButton) {
                app.getMessageIndicator().setMessage(CreamToolkit.getString("PeriodicalReturnType"));
                numberStr = "";
            }
        }
	}

	@Override
	public Class exit(EventObject event, State sinkState) {
        Object eventSource = event.getSource();
        if (eventSource instanceof EnterButton) {
            if (numberStr.equals("1")) {
                //@pingping 2015-05-08 修改输入会员后选择全退,再按清除,再输入会员选择全退时,商品金额为正的问题
                app.setReturnItemState(false);
                List<BookedInfo> bookedInfos = (List<BookedInfo>) CreamSession.getInstance().getAttribute(
                        WorkingStateEnum.PERIODICAL_RETURN_STATE , "BookedInfo" +
                                "s");
                Transaction trans = app.getCurrentTransaction();
                for (BookedInfo info : bookedInfos) {
                    PLU plu = PLU.queryByItemNumber(info.getItemNo());
                    try {
                        // 确保商品价格调整後,能获得销售时的商品价格
                        LineItem lineItem = PluReadyState.createLineItem(trans, plu,
                                new HYIDouble(info.getQtyBase()).negate(), info.getCustOrdPrice1());
                        lineItem.setDetailCode("L");
                        lineItem.setContent(info.getBillNo() + "." + info.getItemSeq() + "." + info.getDtlSeq());
                        lineItem.setDescriptionAndSpecification(lineItem.getDescriptionAndSpecification() + "." +info.getYear() + info.getDtlcode1());
                        trans.addLineItem(lineItem);
                        app.setTransactionEnd(false);

                    } catch (TooManyLineItemsException e) {
                        CreamToolkit.logMessage(e.toString());
                        CreamToolkit.logMessage("Too many LineItem exception at "
                                + this);
                        app.getWarningIndicator().setMessage(res.getString("TooManyLineItems"));
                        return MemberState.class;
                    } catch (CreateLineItemException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                        return MemberState.class;
                    }
                }
                trans.setDealType2("3");
                trans.setDealType3("4");
                app.setReturnItemState(true);
                app.getItemList().setBackLabel(res.getString("BigReturnNumberLabel"));

                return PeriodicalReturnEndState.class;
            } else if (numberStr.equals("2")) {

                //@pingping 2015-05-08 修改输入会员后选择部分退,再按清除,再输入会员选择部分退时,商品金额为正的问题
                app.setReturnItemState(false);
                List<BookedInfo> bookedInfos = (List<BookedInfo>) CreamSession.getInstance().getAttribute(
                        WorkingStateEnum.PERIODICAL_RETURN_STATE , "BookedInfo" +
                                "s");
                Transaction trans = app.getCurrentTransaction();
                for (BookedInfo info : bookedInfos) {
                    PLU plu = PLU.queryByItemNumber(info.getItemNo());
                    try {
                        // 确保商品价格调整後,能获得销售时的商品价格
                        LineItem lineItem = PluReadyState.createLineItem(trans, plu,
                                new HYIDouble(info.getQtyBase()).negate(), info.getCustOrdPrice1());
                        lineItem.setDetailCode("L");
                        lineItem.setContent(info.getBillNo() + "." + info.getItemSeq() + "." + info.getDtlSeq());
                        lineItem.setDescriptionAndSpecification(lineItem.getDescriptionAndSpecification() + "." +info.getYear() + info.getDtlcode1());
                        trans.addLineItem(lineItem);
                        app.setTransactionEnd(false);

                    } catch (TooManyLineItemsException e) {
                        CreamToolkit.logMessage(e.toString());
                        CreamToolkit.logMessage("Too many LineItem exception at "
                                + this);
                        app.getWarningIndicator().setMessage(res.getString("TooManyLineItems"));
                        return MemberState.class;
                    } catch (CreateLineItemException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                        return MemberState.class;
                    }
                }
                trans.setDealType2("3");
                trans.setDealType3("4");
                app.setReturnItemState(true);
                app.getItemList().setBackLabel(res.getString("BigReturnNumberLabel"));

                return PeriodicalPartReturnState.class;
            } else {

                return this.getClass();
            }

        } else if (eventSource instanceof ClearButton) {
            if (!numberStr.equals("")) {
                numberStr = "";
                app.getMessageIndicator().setMessage(CreamToolkit.getString("PeriodicalReturnType"));
                return this.getClass();
            } else {
                return PeriodicalIdleState.class;
            }
        } else if (eventSource instanceof NumberButton) {
            return ListMayReturnState.class;
        }
        return sinkState.getClass();
	}
}
