package hyi.cream.state.periodical;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.state.MemberState;
import hyi.cream.state.State;
import hyi.cream.state.SummaryState;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;

/**
 * 部分退期刊
 * Created by IntelliJ IDEA.
 * User: wangfh
 * Date: 2010-11-30
 * Time: 10:08:27
 * To change this template use File | Settings | File Templates.
 */
public class PeriodicalPartReturnState extends State {
    static PeriodicalPartReturnState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private String numberStr = "";
    public static PeriodicalPartReturnState getInstance() {
    try {
            if (instance == null) {
                instance = new PeriodicalPartReturnState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    public PeriodicalPartReturnState() throws InstantiationException{

    }

    @Override
    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof ListMayReturnState) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("DeleteNeedKeepPeriod"));
        } else if (sourceState instanceof PeriodicalPartReturnState) {
            if (event.getSource() instanceof NumberButton) {
                numberStr += ((NumberButton) event.getSource()).getNumberLabel();
                app.getMessageIndicator().setMessage(numberStr);
            } else if (event.getSource() instanceof RemoveButton) {
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("DeleteNeedKeepPeriod"));
                numberStr = "";
            } else if (event.getSource() instanceof ClearButton) {
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("DeleteNeedKeepPeriod"));

            }

        } else {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("DeleteNeedKeepPeriod"));    
        }


    }

    @Override
    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");
        if (event.getSource() instanceof ClearButton) {
            if (!numberStr.equals("")) {
                 numberStr = "";
                return this.getClass();
            } else {
                Transaction tran = app.getCurrentTransaction();
                tran.clearLineItem();
                tran.setDefaultState();             
                return MemberState.class;
            }
        } else if (event.getSource() instanceof RemoveButton) {
              if(!numberStr.equals("")) {
                  Transaction tran = app.getCurrentTransaction();
                  Object[] lineItems = tran.getDisplayedLineItemsArray().toArray();
                  int selectIndex = Integer.parseInt(numberStr);
                  LineItem selectItem = (LineItem) lineItems[selectIndex -1];
                  if (selectItem.getRemoved()) {
                      app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
                      return this.getClass();
                  }
                  selectItem.setRemoved(true);
                  LineItem voidItem = createVoidLineItem(selectItem);
                  try {
                      tran.addLineItem(voidItem,false);
                      tran.setGrossSalesAmount(new HYIDouble(0));
                      System.out.println("--------------------------------");
                  } catch (TooManyLineItemsException e) {
                      e.printStackTrace();
                  }
                  numberStr = "";

                  return this.getClass();
              } else {
                  app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
                  return this.getClass();
              }
        } else if (event.getSource() instanceof NumberButton) {
              return this.getClass();
        } else if (event.getSource() instanceof AgeLevelButton) {
             return SummaryState.class;
        }
        return sinkState.getClass();  //To change body of implemented methods use File | Settings | File Templates.
    }


    public  LineItem createVoidLineItem(LineItem removeItem) {
        Transaction curTransaction = app.getCurrentTransaction();
        LineItem voidItem = (LineItem)removeItem.clone();
        voidItem.makeNegativeValue();
        voidItem.setRemoved(true);
        voidItem.setLineItemSequence(curTransaction.getLineItems().length + 1);
        CreamToolkit.logMessage("Void " + voidItem.toString());
        voidItem.setDetailCode("V");
        voidItem.setPrinted(false);
        return voidItem;
    }
}
