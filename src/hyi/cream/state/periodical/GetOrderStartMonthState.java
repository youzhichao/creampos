// Copyright (c) 2000 HYI
package hyi.cream.state.periodical;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.LineItem;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.state.GetSomeAGState;
import hyi.cream.util.CreamToolkit;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;

public class GetOrderStartMonthState extends GetSomeAGState {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();

	private ResourceBundle res = CreamToolkit.GetResource();

	static GetOrderStartMonthState instance = null;

	public static GetOrderStartMonthState getInstance() {
		try {
			if (instance == null) {
				instance = new GetOrderStartMonthState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	public GetOrderStartMonthState() throws InstantiationException {
	}

	public boolean checkValidity() {
		String str = getAlphanumericData();
		if (str.length() != 4 && str.length() > 0) {
			setWarningMessage(res.getString("InputOrderMonthWarning"));
			return false;
		}
		
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MONTH, 1);
		SimpleDateFormat yyMM = new SimpleDateFormat("yyMM");
		String minYYMM = yyMM.format(now.getTime());
		
		// 直接按【确认】键,系统提供默认数据
		if (str.length() == 0) {
			str = minYYMM;
		} else {
			try {
				Integer.parseInt(str);
			} catch (NumberFormatException e) {
				setWarningMessage(res.getString("InputOrderMonthWarning"));
				return false;
			}
			try {
				if (Integer.parseInt(str.substring(2)) > 12) {
					setWarningMessage(res.getString("InputOrderMonthWarning1"));
					return false;
				}
			} catch (NumberFormatException e) {
				setWarningMessage(res.getString("InputOrderMonthWarning"));
				return false;
			}
		}
		
		if (str.compareTo(minYYMM) < 0) {
			setWarningMessage(res.getString("InputOrderMonthWarning2"));
			return false;
		}
		
		// issue <= 最大期号
		LineItem li = (LineItem) CreamSession.getInstance().getAttribute(
				WorkingStateEnum.PERIODICAL_ORDER_STATE , "CurLineItem");
		li.setDescriptionAndSpecification(li.getDescriptionAndSpecification()
				+ "." + str);
		li.setContent(str);
		CreamSession.getInstance().setAttribute(WorkingStateEnum.PERIODICAL_ORDER_STATE , 
				"OrderStartYearMonth", str);
        try {
            app.getCurrentTransaction().changeLineItem(-1, li);
        } catch (LineItemNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("LineItem not found at " + this);
        }

		return true;
	}

	public Class getUltimateSinkState() {
		return OrderEndMonthReadyState.class;
	}

	public Class getInnerInitialState() {
		return OrderStartMonthReadyState.class;
	}
}
