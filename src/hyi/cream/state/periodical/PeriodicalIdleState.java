package hyi.cream.state.periodical;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.Cashier;
import hyi.cream.state.CashierRightsCheckState;
import hyi.cream.state.IdleState;
import hyi.cream.state.MemberState;
import hyi.cream.state.State;
import hyi.cream.uibeans.CancelButton;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;

import java.util.Collection;
import java.util.EventObject;
import java.util.ResourceBundle;

public class PeriodicalIdleState extends State {
	static PeriodicalIdleState instance = null;
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();

	public static PeriodicalIdleState getInstance() {
		try {
			if (instance == null) {
				instance = new PeriodicalIdleState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	/**
	 * Constructor
	 */
	public PeriodicalIdleState() throws InstantiationException {
	}
	
	@Override
	public void entry(EventObject event, State sourceState) {
		if (event.getSource() instanceof CancelButton
				|| sourceState instanceof MemberState) {
    		CreamSession.getInstance().removeAttributes(WorkingStateEnum.PERIODICAL_ORDER_STATE);
    		CreamSession.getInstance().removeAttributes(WorkingStateEnum.PERIODICAL_DRAW_STATE);
    		CreamSession.getInstance().removeAttributes(WorkingStateEnum.PERIODICAL_RETURN_STATE);
    		app.getCurrentTransaction().setDefaultState();
			app.setReturnItemState(false);
			app.getItemList().setBackLabel(null);
			app.getItemList().repaint();
		}
		app.getMessageIndicator().setMessage(res.getString("PerioicalMessage"));
	}

	@Override
	public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");
        if (event.getSource() instanceof ClearButton) {
            app.getMessageIndicator().setMessage("");
            // init session
    		CreamSession.getInstance().setWorkingState(WorkingStateEnum.IDLE_STATE);
    		CreamSession.getInstance().removeAttributes(WorkingStateEnum.PERIODICAL_ORDER_STATE);
    		CreamSession.getInstance().removeAttributes(WorkingStateEnum.PERIODICAL_DRAW_STATE);
    		CreamSession.getInstance().removeAttributes(WorkingStateEnum.PERIODICAL_RETURN_STATE);
    		app.getCurrentTransaction().setDefaultState();
    		app.setReturnItemState(false);
            return IdleState.class;
        } else if (event.getSource() instanceof NumberButton) {
        	String select = ((NumberButton) event.getSource()).getNumberLabel();
        	if ("1".equals(select)) {
        		// 预订 order
        		CreamSession.getInstance().setWorkingState(WorkingStateEnum.PERIODICAL_ORDER_STATE);
        		app.getItemList().setBackLabel(res.getString("tdJ"));
    			app.getItemList().repaint();
        		app.getCurrentTransaction().setState1("J");
        		return MemberState.class;
        	} else if ("2".equals(select)) {
        		// 领刊 draw
        		CreamSession.getInstance().setWorkingState(WorkingStateEnum.PERIODICAL_DRAW_STATE);
        		app.getCurrentTransaction().setState1("K");
        		app.getItemList().setBackLabel(res.getString("tdK"));
    			app.getItemList().repaint();
        		return MemberState.class;
        	} else if ("3".equals(select)) {
        		// 退刊 return
        		app.getCurrentTransaction().setState1("L");
        		CreamSession.getInstance().setWorkingState(WorkingStateEnum.PERIODICAL_RETURN_STATE);
				Collection fds = Cashier.getExistedFieldList("cashier");
				if (!fds.contains("CASRIGHTS"))
					return MemberState.class;

				CashierRightsCheckState.setSourceState("InitialState");
				CashierRightsCheckState.setTargetState("MemberState");
				return CashierRightsCheckState.class;
//                return MemberState.class;
        	} else {
				app.getWarningIndicator().setMessage(res.getString("InputWrong"));
				return PeriodicalIdleState.class;
			}
    	} else {
			app.getWarningIndicator().setMessage(res.getString("InputWrong"));
			return PeriodicalIdleState.class;
        }
//		return sinkState.getClass();
	}

}
