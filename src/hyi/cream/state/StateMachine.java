package hyi.cream.state;

//import hyi.cream.POSPeripheralHome;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.SystemInfo;
import hyi.cream.groovydac.Param;
import hyi.cream.dac.Cashier;
import hyi.cream.dac.ShiftReport;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import hyi.spos.CashDrawer;
import hyi.spos.JposException;
import hyi.spos.Keylock;
import hyi.spos.KeylockConst;
import hyi.spos.MSR;
import hyi.spos.Scanner;
import hyi.spos.ToneIndicator;
import hyi.spos.events.DataEvent;

import java.io.*;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

import protector.Protector;
import org.apache.commons.lang.StringUtils;

public class StateMachine extends Thread {

    private static final String COMMENT = "#";

    private static StateMachine instance = null;
    private static hyi.cream.state.State currentState = null;
    private static hyi.cream.state.State nextState = null;
    private static Hashtable allStateInstance = null;
    private static boolean floppySaveError;

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private boolean traceState;
    private ResourceBundle res = CreamToolkit.GetResource();
    private Map stateChartMap = new HashMap();
    //private String checkPrinterPower = null;
    private boolean suspended;
    private boolean eventProcessEnabled = false;
    private boolean keyWarning = false;
    private int turnable = 0;
    private String warning = "";
    private int oldKey = 0;

    private BlockingQueue<EventObject> eventQueue = new LinkedBlockingQueue<EventObject>(38);
    static private DateFormat dateFormatter = new SimpleDateFormat("MM/dd HH:mm:ss.SSS");

    private List<Map<EventObject, Long>> recordEventLists = new ArrayList<Map<EventObject, Long>>();
    private int recorded = 0;
    private long lastTimes = 0L;
    private long lastEventTime = 0L;

    private long timeStamp;

    static public boolean getFloppySaveError() {
        return floppySaveError;
    }

    static public void setFloppySaveError(boolean b) {
        floppySaveError = b;
    }

    public StateMachine() throws InstantiationException {
        if (instance != null)
            throw new InstantiationException (this.toString());
        else {
            //initStateMachine();
            instance = this;
            setDaemon(true);
            setName("StateMachine");
            start();
        }
    }

    public static Hashtable getAllStateInstance() {
        return allStateInstance;
    }

    /**
     * Event handling thread.
     */
    public void run() {
        for (;;) {
            try {
                consumeEvent(eventQueue.take());
                // clearEvent(); // stop too fast event
            } catch (Exception e) {
                CreamToolkit.logMessage(e);
            }
        }
    }

    /**
     * Used by AutoTester. For preventing from queuing events.
     */
    public void waitForEventQueueEmpty() {
        try {
            while (eventQueue.peek() != null)
                sleep(150);
        } catch (InterruptedException e) {
        }
    }

    /**
     * Used by AutoTester. For "sync" command.
     */
    public void waitForTransactionEnd() {
        while (!currentState.getClass().equals(IdleState.class)
            && !currentState.getClass().equals(SalemanState.class)) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void clearEvent() {
        eventQueue.clear();
    }

    public void initStateMachine() {
        //checkPrinterPower = GetProperty.getCheckPrinterPower("");
        //if (checkPrinterPower.equals(""))
        //    checkPrinterPower = "yes";

        constructStateChart();

        traceState = Param.getInstance().isTraceState();
        currentState = InitialState.getInstance();
        allStateInstance = new Hashtable();
        allStateInstance.put(currentState.getClass().getName(), currentState);

        //Bruce/2003-08-27
        // if "Protector" exists, entry LicenseNumberingState instead.
        try {
            Class.forName("protector.Protector");
            if (!Protector.checkUserLicense()) 
            {
                currentState = LicenseNumberingState.getInstance();
                currentState.entry(null, null);
                app.setBeginState(false);
                return;
            }
        } catch (Exception e) {
            // if Protector class does not exist, let it pass.
        }

        currentState.entry(null, null);     // entry InitialState
        if (!isTriggerless(currentState.getClass().getName()))
            return;
        Class sink = currentState.exit(null, null); // exit InitialState if triggerless
        if (allStateInstance.containsKey(sink.getName())) {
            nextState = (hyi.cream.state.State)allStateInstance.get(sink.getName());
        } else {
            try {
                nextState = (hyi.cream.state.State)Class.forName(sink.getName()).getMethod("getInstance").invoke(null);
                //nextState = (State)Class.forName(sink.getName()).newInstance();
                allStateInstance.put(sink.getName(), nextState);
            } catch (Exception e) {
                CreamToolkit.logMessage(e);
                return;
            }
        }
        if (nextState instanceof IdleState) {
            //It is for TK3C, SalemanState must be entry.
            nextState.entry(null, currentState);    // entry sink state
        } else {
            //Now it is same.
            nextState.entry(null, currentState);    // entry sink state
        }
        currentState = nextState;
    }

//    void checkPrinterStatus() {
//        Runnable check = new Runnable() {
//            public void run () {//
//                POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
//                POSPrinter printer = null;
//                String originalMsg = app.getWarningIndicator().getMessage();
//                try {
//                    printer = posHome.getPOSPrinter();
//                } catch (NoSuchPOSDeviceException e) {
//                    CreamToolkit.logMessage(e.getMessage());
//                }
//                try {
//                    if (!printer.getClaimed())
//                        printer.claim(1000);
//                    if (!printer.getDeviceEnabled())
//                        printer.setDeviceEnabled(true);
//                    if (checkPrinterPower.equals("yes")) {
//                        while (true) {
//                            if (printer.getPowerState() != JposConst.JPOS_PS_ONLINE) {
//                                app.getWarningIndicator().setMessage(res.getString("PrinterDetectError"));
//                                java.awt.Toolkit.getDefaultToolkit().beep();
//                            } else {
//                                app.getWarningIndicator().setMessage(res.getString("WaitForPrinterDetection"));
//                                try {
//                                    Thread.sleep(2500);
//                                } catch (InterruptedException ie) {
//                                    ie.printStackTrace();
//                                }
//                                synchronized (StateMachine.getInstance()) {
//                                    StateMachine.getInstance().notifyAll();
//                                }
//                                app.getWarningIndicator().setMessage(originalMsg);
//                                return;
//                            }
//                            try {
//                                Thread.sleep(1000);
//                            } catch (InterruptedException ie) {
//                                ie.printStackTrace();
//                            }
//                        }
//                    }
//                } catch (JposException e) {
//                    CreamToolkit.logMessage(e.toString());
//                }
//            }
//        };
//        new Thread(check).start();
//    }

    public <T> T getStateInstance(Class<T> clazz) {
        return (T)getStateInstance(clazz.getName());
    }

    public hyi.cream.state.State getStateInstance(String nextClassName) {
        if (nextClassName == null) {
            System.out.println("StateMachine> cannot get state instance: " + nextClassName);
            return null;
        }
        try {
            hyi.cream.state.State instance =
                (hyi.cream.state.State)Class.forName(nextClassName).getMethod("getInstance").invoke(null);

            if (instance != null)
                return instance;
            else if (allStateInstance.containsKey(nextClassName)) {
                return (hyi.cream.state.State)allStateInstance.get(nextClassName);
            } else {
                instance = (hyi.cream.state.State)Class.forName(nextClassName).newInstance();
                allStateInstance.put(nextClassName, instance);
                return instance;
            }
        } catch (Exception e) {
            CreamToolkit.logMessage(e.getMessage());
            return null;
        }
    }

    public void setEventProcessEnabled (boolean eventProcessEnabled) {
        this.eventProcessEnabled  = eventProcessEnabled;
    }

    public boolean getEventProcessEnabled () {
        return eventProcessEnabled;
    }

    /**
     * Construct state chart from a text file.
     * 
     * <P>Data structure of stateChartMap:
     * <PRE>
     *   Key                     Value
     * -------------  -----------------------------------------------------------------
     * source state   ArrayList object contains: Object[3]
     * class name     +----------+-----------------------+----------------------------+
     *                | CondType | Event Source Classes  |      Sink State Class      |
     *                +----------+-----------------------+----------------------------+
     *                |"|" or "&"| "", class name or     | "" or class name           |
     *                | or null  | Object[]              |                            |
     *                +----------+-----------------------+----------------------------+
     *                |          |                       |                            |
     *                +----------+-----------------------+----------------------------+
     *                |          |                       |                            |
     *                +----------+-----------------------+----------------------------+
     *                |  ...     |      ...              |     ...                    |
     * 
     *                 CondType:
     *                      "|" is OR-style, like "xxxx | xxxx | xxxxx"
     *                      "&" is AND-style, like "!xxxx & !xxxx & !xxxxx"
     *                      null is normal style
     * </PRE>
     */
    public void constructStateChart() {
        try {
            //File stateChartFile = CreamToolkit.getConfigurationFile(StateMachine.class);
            //LineNumberReader chartDefFile = new LineNumberReader(new FileReader(stateChartFile));

            LineNumberReader chartDefFile = new LineNumberReader(new InputStreamReader(
                getClass().getResourceAsStream("/hyi/cream/state/statechart.conf"), "UTF-8"));

            String line;
            while ((line = chartDefFile.readLine()) != null) {
                line = line.trim();
                if (line.startsWith(COMMENT))    // ignore lines start with '#'
                    continue;

                // Format of line:
                // [source state class name], [event class name], [event source class name], [sink state class name]

                StringTokenizer tokener = new StringTokenizer(line, ",", true);
                try {
                    String sourceStateClassName = tokener.nextToken().trim();
                    if (sourceStateClassName.equals(","))   // ignore the lines without source state
                        continue;

                    if (!sourceStateClassName.startsWith("hyi."))
                        sourceStateClassName = "hyi.cream.state." + sourceStateClassName;

                    tokener.nextToken();                // ","

                    String eventClassName = tokener.nextToken().trim();
                    if (eventClassName.equals(","))
                        eventClassName = "";
                    else
                        tokener.nextToken();            // ","

                    if (!eventClassName.contains(".") && eventClassName.endsWith("Event"))
                        eventClassName = "hyi.cream.event." + eventClassName;

                    String eventSourceClassName = tokener.nextToken().trim();
                    if (eventSourceClassName.equals(","))
                        eventSourceClassName = "";
                    else
                        tokener.nextToken();            // ","

                    String sinkStateClassName;
                    try {
                        sinkStateClassName = tokener.nextToken().trim();
                    } catch (NoSuchElementException e) {
                        sinkStateClassName = "";        // lack of last element
                    }

                    if (!sinkStateClassName.contains(".") && sinkStateClassName.endsWith("State"))
                        sinkStateClassName = "hyi.cream.state." + sinkStateClassName;

                    // check the invalid syntax
                    if (eventSourceClassName.indexOf("|") != -1         // xxx | xxx & xxx
                        && eventSourceClassName.indexOf("&") != -1)
                        throw new NoSuchElementException();
                    if (eventSourceClassName.indexOf("|") != -1         // xxx | !xxx
                        && eventSourceClassName.indexOf("!") != -1)
                        throw new NoSuchElementException();
                    if (eventSourceClassName.indexOf("&") != -1         // xxx & xxx
                        && eventSourceClassName.indexOf("!") == -1)
                        throw new NoSuchElementException();
                                                                        // what if "xxx & !xxx" ? Look "THERE"

                    // condType
                    String condType = (eventSourceClassName.indexOf("|") != -1) ? "|" :
                        (eventSourceClassName.indexOf("&") != -1) ? "&" :
                        (eventSourceClassName.indexOf("!") != -1) ? "&" : null;

                    // event source classes
                    StringTokenizer tokener2 = new StringTokenizer(eventSourceClassName, "&|");
                    List eventSourceClasses = new ArrayList();
                    while (tokener2.hasMoreTokens()) {
                        String eventStateClassName2 = tokener2.nextToken().trim();
                        if (!eventStateClassName2.startsWith("!") 
                            && condType != null && condType.equals("&"))  // "THERE"
                            throw new NoSuchElementException();
                        if (eventStateClassName2.startsWith("!"))
                            eventStateClassName2 = eventStateClassName2.substring(1);

                        if (!eventStateClassName2.contains(".") && eventStateClassName2.endsWith("Button"))
                            eventStateClassName2 = "hyi.cream.uibeans." + eventStateClassName2;

                        eventSourceClasses.add(eventStateClassName2);
                    }

                    // retrieve original value
                    Object value = stateChartMap.get(sourceStateClassName);
                    List valueList = (value != null) ? (List)value : new ArrayList();

                    // construct new value
                    valueList.add(new Object[] {
                        condType,
                        (
                            (eventSourceClasses.size() == 0) ? "" :
                            (eventSourceClasses.size() == 1) ? eventSourceClasses.get(0) :
                                eventSourceClasses.toArray()
                        ),
                        sinkStateClassName});

                    // put into stateChartMap
                    stateChartMap.put(sourceStateClassName, valueList);

                } catch (NoSuchElementException e) {
                    CreamToolkit.logMessage("Statechart format error at line "
                        + chartDefFile.getLineNumber());
                    continue;
                }
            }
            chartDefFile.close();
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    /**
     * Determine the sink state class from a source state and an event source class.
     * 
     * @return Three cases:
     *      <P>1. "" if sink state is determined by source state's exit().
     *      <P>2. null if this event will not make any state change
     *      <P>3. otherwise return the sink state class name
     */
    private String getSinkState(String sourceStateClassName, String eventSourceClassName) {
        // assertion
        if (sourceStateClassName == null || eventSourceClassName == null)
            return null;

        List value = (List)stateChartMap.get(sourceStateClassName);
        if (value == null)
            return null;

        Iterator iter = value.iterator();
whileLabel:
        while (iter.hasNext()) {
            Object[] entry = (Object[])iter.next();
            String condType = (String)entry[0];
            Object eventSourceClasses0 = entry[1];
            String sinkClassName = (String)entry[2];

            if (eventSourceClasses0.equals(eventSourceClassName))
                return sinkClassName;

            if (condType != null && condType.equals("|")) {
                Object[] eventSourceClasses = (Object[])eventSourceClasses0;
                for (int i = 0; i < eventSourceClasses.length; i++) {
                    if (eventSourceClasses[i].equals(eventSourceClassName))
                        return sinkClassName;
                }
            } else if (condType != null && condType.equals("&")) {
                if (eventSourceClasses0 instanceof Object[]) {
                    Object[] eventSourceClasses = (Object[])eventSourceClasses0;
                    for (int i = 0; i < eventSourceClasses.length; i++) {
                        if (eventSourceClasses[i].equals(eventSourceClassName))
                            continue whileLabel;
                    }
                } else {
                    if (!eventSourceClasses0.equals(eventSourceClassName))
                        return sinkClassName;
                }
                return sinkClassName;
            }
        }
        return null;
    }

    public boolean isTriggerless(String stateClassName) {
        List value = (List)stateChartMap.get(stateClassName);
        if (value == null)
            return false;

        Iterator iter = value.iterator();
        while (iter.hasNext()) {
            Object[] entry = (Object[])iter.next();
            if (entry[1].equals(""))       // if contains a empty event source class entry
                return true;
        }
        return false;
    }

    synchronized public static StateMachine getInstance() {
        try {
            if (instance == null)
                instance = new StateMachine();
        } catch (InstantiationException ie){
            ie.printStackTrace(CreamToolkit.getLogger());
        }
            return instance;
    }

    public Class getCurrentState() {
        return currentState.getClass();
    }

    public boolean getKeyWarning() {
        return keyWarning;
    }

    public void setKeyWarning(boolean keyWarning) {
        this.keyWarning = keyWarning;
    }

    public void setTurnable(int turnable) {
        this.turnable = turnable;
    }

    public boolean getTurnable() {
        if (turnable != 0) {
            if (turnable == 1) {
                return true;
            } else if (turnable == -1) {
                return false;
            }
        }

        if (app.getTrainingMode()
            && !getKeyWarning()) {
            //if (!app.getScanCashierNumber()
            //    || (app.getScanCashierNumber()
            //        && app.getChecked())) {
            return true;
        }

        if (getCurrentState().equals(IdleState.class)
            && PopupMenuPane.getInstance().isShowing()) {
            if (!app.getTrainingMode()) {
                return false;
            }
        } else if (getCurrentState().equals(CashierRightsCheckState.class)) {
            if (Param.getInstance().isTurnKeyAtOverrideAmount())
                return true;
        }

        /*if (getCurrentState().equals(OpenPriceState.class)
            || getCurrentState().equals(DaiFuOpenPriceState.class)
            || getCurrentState().equals(PaidInOpenPriceState.class)
            || getCurrentState().equals(PaidOutOpenPriceState.class)
            || getCurrentState().equals(PriceLookupState.class)) {
            return false;
        }*/

        if (getCurrentState().equals(IdleState.class)
            || getCurrentState().equals(KeyLock1State.class)
            || getCurrentState().equals(ConfigState.class)
            || getCurrentState().equals(KeyLock2State.class)
            || getCurrentState().equals(KeyLock3State.class)
            || getCurrentState().equals(CheckKeyLockState.class)
            || getCurrentState().equals(CashierState.class)) {
            if (app.getTransactionEnd()
                || app.getCurrentTransaction().getDisplayedLineItemsArray().size() == 0) {
                if (!getKeyWarning()) {
                    if (!app.getScanCashierNumber()
                        || (app.getScanCashierNumber()
                            && app.getChecked())) {
                        return true;
                    }
                }
            }
        }

        /*if (getCurrentState().equals(IdleState.class)) {
            if (app.getKeyPosition() == 1) {
                if (!app.getScanCashierNumber()
                    || (app.getScanCashierNumber()
                        && app.getChecked())) {
                    return true;
                }
            }
        }*/

        return false;

        /* ((((getCurrentState().equals(IdleState.class)
               || getCurrentState().equals(KeyLock1State.class)
               || getCurrentState().equals(ConfigState.class)
               || getCurrentState().equals(KeyLock2State.class)
               || getCurrentState().equals(KeyLock3State.class)
               || getCurrentState().equals(CheckKeyLockState.class)
               || getCurrentState().equals(CashierState.class))
               && (app.getTransactionEnd()
                  || app.getCurrentTransaction().getCurrentLineItem() == null)
               && !getKeyWarning())
             || app.getKeyPosition() == 1)
            && (!app.getScanCashierNumber()
                || (app.getScanCashierNumber()
                    && app.getChecked()))) {
            return true;
        } else if ((app.getTrainingMode()
            && !getKeyWarning())
            && (!app.getScanCashierNumber()
            || (app.getScanCashierNumber()
                && app.getChecked()))) {
            return true;
        } else {
            return false;
        }*/
    }

    public void processEvent(EventObject event) {
        try {
            if (event.getSource() instanceof ReplayButton
                    || event.getSource() instanceof RecordButton
                  //|| event.getSource() instanceof jpos.Keylock
                    )
                ;
            else if (recorded == 1) {
                long nowTimes = (new java.util.Date()).getTime();
                long diff = lastTimes != 0 ? (nowTimes - lastTimes) : 0;
                Map<EventObject, Long> tmp = new HashMap<EventObject, Long>();
                tmp.put(event, diff);
                recordEventLists.add(tmp);
                lastTimes = nowTimes;
            }

            logEvent(event);
            eventQueue.put(event);
            
        } catch (InterruptedException e) {
            CreamToolkit.logMessage(e);
        }
    }

    private void logEvent(EventObject event) {
        Date now = new Date();
        if (lastEventTime == 0L)
            lastEventTime = now.getTime();

        String eventName;
        if (event.getSource() instanceof Scanner) {
            try {
                String barcode = new String(((Scanner)event.getSource()).getScanData(((DataEvent)event).seq));
                eventName = "Scanner," + barcode;
            } catch (Exception e) {
                eventName = "Scanner,?";
            }
        } else if (event.getSource() instanceof MSR) {
            String accountNumbe = ((MSR)event.getSource()).getAccountNumber();
            String expirationDate = ((MSR)event.getSource()).getExpirationDate();
            eventName = "MSR," + accountNumbe + " " + expirationDate;
        } else if (event.getSource() instanceof Keylock) {
            try {
                int keyPos = ((Keylock)event.getSource()).getKeyPosition();
                eventName = "Keylock," + keyPos;
            } catch (Exception e) {
                eventName = "Keylock,?";
            }
        } else if (event.getSource() instanceof CashDrawer) {
            try {
                boolean drawerOpened = ((CashDrawer)event.getSource()).getDrawerOpened();
                eventName = "CashDrawer," + (drawerOpened ? "true" : "false");
            } catch (Exception e) {
                eventName = "CashDrawer,?";
            }
        } else {
            eventName = event.getSource().toString();
        }
        //支付宝等待买家付款时，按了清除的处理
        if (POSTerminalApplication.getInstance().getAlipay()) {
            if (event.getSource() instanceof ClearButton) {
                AlipayState.isClearButton = true;
                CreamToolkit.logMessage("alipay click ClearButton!");
            }
        }
        //微信等待买家付款时，按了清除的处理
        if (POSTerminalApplication.getInstance().getWeiXin()) {
            if (event.getSource() instanceof ClearButton) {
                WeiXinProcessState.isClearButton = true;
                CreamToolkit.logMessage("weixin click ClearButton!");
            }
        }
        //银联等待买家付款时，按了清除的处理
        if (POSTerminalApplication.getInstance().getUnionPay()) {
            if (event.getSource() instanceof ClearButton) {
                UnionPayProcessState.isClearButton = true;
                CreamToolkit.logMessage("unionpay click ClearButton!");
            }
        }

        CreamToolkit.logEvent(dateFormatter.format(now) + "," +
            (now.getTime() - lastEventTime) + "ms," + eventName);
        lastEventTime = now.getTime();
    }

    public void consumeEvent(EventObject event)
    {
        //System.out.println("StateMachine: @@@ " + event.getSource());

        if (!getEventProcessEnabled())
                return;
        if (app.getBeginState()) {
            if (event.getSource() instanceof Keylock) {
                try {
                    if (((Keylock)event.getSource()).getKeyPosition() == KeylockConst.LOCK_KP_LOCK) {
                        app.setBeginState(false);
                        app.setKeyPosition(KeylockConst.LOCK_KP_LOCK);
                        CreamToolkit.showTurnKeyToSalesPositionMessage();
                        return;
                    } else {
                        return;
                    }
                } catch (JposException e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }
            } else {
                return;
            }
        }

        POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();

        if (app.getKeyPosition() == KeylockConst.LOCK_KP_LOCK) {
            if (!(event.getSource() instanceof Keylock)) {
                return;
            }
        }

        if (getKeyWarning() && !(event.getSource() instanceof Keylock)) {
            app.getWarningIndicator().setMessage(res.getString("TurnKeyLock"));
            if (!(event.getSource() instanceof  CashDrawer)) {
                return;
            }
        }

        if (PopupMenuPane.getInstance().isShowing()
            && event.getSource() instanceof Scanner) {
            if (!(getCurrentState().equals(KeyLock1State.class)
                 || getCurrentState().equals(KeyLock2State.class)
                 || getCurrentState().equals(KeyLock3State.class))) {
                return;
            }
        }

        // check key lock
        if (event.getSource() instanceof Keylock) {
            if (app.isEnableKeylock() && processKeylockEvent(posHome, event))
                return;
        }

        //  check scan saleman card
        if (app.getScanSalemanNumber()) {
            if (!app.getSalemanChecked()) {
                if (event.getSource() instanceof Scanner) {
                    String id = "";
                    try {
                        id = new String(((Scanner)event.getSource()).getScanData(((DataEvent)event).seq));
                        // Modify for Tscankun. Ref. ARKScanner class.
                        if (id.length() > 8)
                            id = id.substring(0, 8);
                    } catch (JposException e) {
                        e.printStackTrace();
                    }
                    app.setSalemanChecked(true);
                    app.getCurrentTransaction().setSalesman(id);
                    app.getSystemInfo().setSalesManNumber(id);
                    app.getMessageIndicator().setMessage(res.getString("SelectPlu"));
                    app.getWarningIndicator().setMessage("");
                    //return;
//                } else if (event.getSource() instanceof EnterButton) {
//                    String id = "";
//                    try {
//                        id = ((SomeAGNumberingState)currentState).getAlphanumericData();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    app.setSalemanChecked(true);
//                    app.getCurrentTransaction().setSalesman(id);
//                    System.out.println("+++++++++" + id);                    
//                    app.getSystemInfo().setSalesManNumber(id);
//                    app.getMessageIndicator().setMessage(res.getString("SelectPlu"));
//                    app.getWarningIndicator().setMessage("");
//                    return;
                } else if (event.getSource() instanceof Keylock) {
                    int keyp = 0;
                    try {
                        keyp = ((Keylock)event.getSource()).getKeyPosition();
                    } catch (JposException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                    }
                    if (app.getTrainingMode()) {
                        app.setTrainingMode(false);
                        app.setChecked(true);
                    } else if (app.getKeyPosition() == 1
                               && keyp == 2) {
                        app.setKeyPosition(2);
                    } else {
                        app.getWarningIndicator().setMessage(res.getString("TurnKeyLock"));
                        return;
                    }
                } else {
                    //return;
                }
            }
        }
        
        if (app.getScanCashierNumber()) {
            if (!app.getChecked()) {
                if (event.getSource() instanceof Scanner) {
                    //try {
                    //    if (((Scanner)event.getSource()).getScanDataType() != ScannerConst.SCAN_SDT_Code39) {
                    //        app.getWarningIndicator().setMessage(res.getString("InputWrong"));
                    //        return;
                    //    }
                    //} catch (JposException e) {
                    //    System.out.println(e);
                    //}
                    String id = "";
                    try {
                        id = new String(((Scanner)event.getSource()).getScanData(((DataEvent)event).seq));

                        //Bruce/2003-06-26/
                        // Modify for CStore. Ref. ARKScanner class.
                        if (id.length() > 7)
                            id = id.substring(0, 7);

                    } catch (JposException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                    }
                    Cashier c = Cashier.queryByCashierID(id);
                    if (c == null) {
                        app.getWarningIndicator().setMessage(res.getString("CashierNumberWrong"));
                    } else {
                        if (!app.getTrainingMode()) {
                            app.getCurrentTransaction().setSalesman(id);
                            app.getSystemInfo().setSalesManNumber(id);
                        }
                        app.setChecked(true);
                        if (app.getKeyPosition() == 1) {
                            app.getMessageIndicator().setMessage(res.getString("CashierNumberRight"));
                        } else if (app.getKeyPosition() == 2 || app.getTrainingMode()) {

                            DbConnection connection = null;
                            try {
                                connection = CreamToolkit.getPooledConnection();
                                if (StringUtils.isEmpty(Param.getInstance().getCashierNumber())
                                    || ShiftReport.getCurrentShift(connection) == null) {
                                    if (!app.getTrainingMode()) {
                                        app.getMessageIndicator().setMessage(res.getString("InputCashierNumber"));
                                    }
                                } else {
                                    app.getMessageIndicator().setMessage(res.getString("SelectPlu"));
                                }
                            } catch (SQLException e) {
                                CreamToolkit.logMessage(e);
                            } finally {
                                CreamToolkit.releaseConnection(connection);
                            }
                                
                        } else if ((app.getKeyPosition() == 3
                                    && !app.getTrainingMode())
                                   || app.getKeyPosition() == 4
                                   || app.getKeyPosition() == 5) {
                            app.getMessageIndicator().setMessage(res.getString("InputSelect"));
                        }
                        app.getWarningIndicator().setMessage("");
                    }
                    return;
                } else if (event.getSource() instanceof Keylock) {
                    int keyp = 0;
                    try {
                        keyp = ((Keylock)event.getSource()).getKeyPosition();
                    } catch (JposException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                    }
                    if (app.getTrainingMode()) {
                        app.setTrainingMode(false);
                        app.setChecked(true);
                    } else if (app.getKeyPosition() == 1
                               && keyp == 2) {
                        app.setKeyPosition(2);
                    } else {
                        app.getWarningIndicator().setMessage(res.getString("TurnKeyLock"));
                        return;
                    }
                } else if (event.getSource() instanceof CashDrawer) {
                } else {
                    return;
                }
            } else {
                if (event.getSource() instanceof Keylock) {
                    try {
                        if (((Keylock)event.getSource()).getKeyPosition() == 2) {
                            app.setKeyPosition(2);
                        }
                    } catch (JposException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                    }
                }
            }
        } else {
            if (event.getSource() instanceof Keylock) {
                try {
                    if (((Keylock)event.getSource()).getKeyPosition() == 2) {
                        app.setKeyPosition(2);
                    }
                } catch (JposException e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }
            }
        }

        if (event instanceof DataEvent && event.getSource() instanceof MSR) {
            MSR msr = (MSR) event.getSource();
            try {
                msr.setDeviceEnabled(true);
            } catch (JposException je) {
            }
        }

        // Bruce/20030323/ Refactoring...
        do {
            String eventSourceName;
            if (event == null) {
                eventSourceName = "";
            } else {
                if (event.getSource() instanceof Keylock)
                    eventSourceName = "hyi.spos.Keylock";
                else if (event.getSource() instanceof MSR)
                    eventSourceName = "hyi.spos.MSR";
                else if (event.getSource() instanceof CashDrawer)
                    eventSourceName = "hyi.spos.CashDrawer";
                else if (event.getSource() instanceof Scanner)
                    eventSourceName = "hyi.spos.Scanner";
                else
                    eventSourceName = event.getSource().getClass().getName();

                //System.out.println("StateMachine: @@@ eventSourceName=" + eventSourceName);
            }

            String sinkStateByStatechart = getSinkState(currentState.getClass().getName(),
                eventSourceName);
            //System.out.println("StateMachine: @@@ sinkStateByStatechart=" + sinkStateByStatechart);

            // sinkStateByStatechart has three cases:
            //   1. "" if sink state is determined by source state's exit().
            //   2. null if this event will not make any state change
            //   3. otherwise return the sink state class name
    
            if (sinkStateByStatechart == null) {
                if (currentState.getClass().getName().endsWith("IdleState")
                    && !(event.getSource() instanceof PageUpButton
                        || event.getSource() instanceof PageDownButton)) {
                    sinkStateByStatechart = "hyi.cream.state.WarningState";
                    //Bruce/20081217/ Reset exitState of WarningState to prevent redirecting to wrong state
                    WarningState.setExitState(null);
                } else {
                    return;     // if cannot make state change
                }
            } else {
                if (event != null && event.getClass().getName().endsWith("POSButtonEvent")
                    && event.getSource().getClass().getName().endsWith("ClearButton")) {
                    try {
                        Scanner scanner = POSPeripheralHome3.getInstance().getScanner();
                        try {
                            if (!scanner.getDataEventEnabled())
                                scanner.setDataEventEnabled(true);
                            scanner.clearInput();
                        } catch (JposException je) { }
                    } catch (Exception ne) {
                        ne.printStackTrace(CreamToolkit.getLogger());
                    }
                }
            }
    
            // determine the parameter "sinkState" passing to currentState.exit()
            hyi.cream.state.State sinkState;
            if (sinkStateByStatechart.equals(""))
                sinkState = null;
            else
                sinkState = getStateInstance(sinkStateByStatechart);
            if (traceState && event != null) {
                System.out.println("[" + dateFormatter.format(new java.util.Date()) + "] " + event.getSource().getClass().getName());
            }
            // exit() the current state
            if (traceState) {
                System.out.println("[" + dateFormatter.format(new java.util.Date()) + "] " +
                    "State< <- " + currentState.getClass().getSimpleName() + ".exit()");
                SystemInfo.stateExit(currentState);
                timeStamp = System.currentTimeMillis();
            }
            Class sinkStateClassReturnFromExit = currentState.exit(event, sinkState);
            String sinkStateReturnFromExit;
            if (sinkStateClassReturnFromExit == null)
                sinkStateReturnFromExit = null;
            else
                sinkStateReturnFromExit = sinkStateClassReturnFromExit.getName();
            if (traceState) {
                System.out.println("[" + dateFormatter.format(new java.util.Date()) + "] " + " <Ret> "
                    + (System.currentTimeMillis() - timeStamp) + "ms");
            }
    
            // Determine the sink state:
            // if statechart doc does not specifies the sink state, use the return value of exit()
            if (sinkStateByStatechart.equals(""))
                sinkState = getStateInstance(sinkStateReturnFromExit);
            
            if (sinkState == null) {
                CreamToolkit.logMessage("Err> Cannot get State: " + sinkStateReturnFromExit);
                return;
            }

            // entry() the sink state.
            if (traceState) {
                System.out.println("[" + dateFormatter.format(new java.util.Date()) + "] " +
                    "State> -> " + sinkState.getClass().getSimpleName() + ".entry()");
                SystemInfo.stateEntry(sinkState);
                timeStamp = System.currentTimeMillis();
            }
            sinkState.entry(event, currentState);
            if (traceState) {
                System.out.println("[" + dateFormatter.format(new java.util.Date()) + "] " + " <Ret> "
                    + (System.currentTimeMillis() - timeStamp) + "ms");
            }

            // skip to next state
            currentState = sinkState;

            // ...
            if (event != null && event.getClass().getName().equals("hyi.spos.events.DataEvent")
                && event.getSource().getClass().getName().endsWith("Scanner")) {
                Scanner scanner = (Scanner) event.getSource();
                try {
                    scanner.setDataEventEnabled(true);
                } catch (JposException je) {
                }
            }
            
            event = null;

        } while (isTriggerless(currentState.getClass().getName()));

      }

    private boolean processKeylockEvent(POSPeripheralHome3 posHome, EventObject event) {
        try {
            int keyPos = ((Keylock)event.getSource()).getKeyPosition();
            if (getKeyWarning()) {
                try {
                    if (keyPos == app.getKeyPosition()) {
                        app.getWarningIndicator().setMessage(warning);
                        setKeyWarning(false);
                        setTurnable(0);
                        ToneIndicator tone = posHome.getToneIndicator();
                        tone.clearOutput();
                    }
                } catch (Exception je) {
                    je.printStackTrace(CreamToolkit.getLogger());
                //} catch (NoSuchPOSDeviceException e) {
                //    e.printStackTrace(CreamToolkit.getLogger());
                }
                return true;
            }

            if (keyPos == KeylockConst.LOCK_KP_LOCK) {
                oldKey = app.getKeyPosition();
                app.setKeyPosition(KeylockConst.LOCK_KP_LOCK);
                PopupMenuPane p = app.getPopupMenuPane();
                ArrayList menu = new ArrayList();
                ResourceBundle res = CreamToolkit.GetResource();
                menu.add("");
                menu.add("");
                menu.add("");
                menu.add("");
                menu.add(res.getString("SystemSuspended"));
                p.setMenu(menu);
                p.setVisible(true);
                //p.setPopupMenuListener(this);
                p.setInputEnabled(false);
                setSuspended(true);

                // app.getItemList().setVisible(true);
                app.getWarningIndicator().setMessage(res.getString("LockState"));
                app.getMessageIndicator().setMessage("");
                return true;
            }

            app.getPopupMenuPane().setInputEnabled(true);
            setSuspended(false);

            // if (keyPos == 5) {
            // this.setKeyWarning(true);
            // this.setTurnable(0);
            // }
            if (keyPos >= KeylockConst.LOCK_KP_SUPR + 2) {
                // if (oldKey == 2) {

                if (app.getTransactionEnd()
                    || app.getCurrentTransaction().getDisplayedLineItemsArray().size() == 0) {

                    if (!app.getScanCashierNumber()
                        || (app.getScanCashierNumber() && app.getChecked())) {
                        setTurnable(1);
                    } else {
                        setTurnable(-1);
                    }
                } else {
                    setTurnable(-1);
                }
                // } else if (oldKey == 6) {
                // setTurnable(1);
                // } else {
                // setTurnable(-1);
                // }
            }

            if (keyPos == KeylockConst.LOCK_KP_NORM
                && app.getKeyPosition() == KeylockConst.LOCK_KP_LOCK) {
                app.getPopupMenuPane().setVisible(false);
                if (oldKey == KeylockConst.LOCK_KP_NORM) {
                    setTurnable(1);
                } else if (oldKey == 6) {
                    if (getCurrentState().equals(ConfigState.class)) {
                        setTurnable(1);
                    } else {
                        setTurnable(-1);
                    }
                } else {
                    setTurnable(1);
                }
            }

            if (!getTurnable()) {
                if (Param.getInstance().isIgnoreKeylockWithinTransaction()
                    && getCurrentState().equals(IdleState.class)) {
                    // && !app.getTransactionEnd()
                    // && app.getCurrentTransaction().getDisplayedLineItemsArray().size() != 0) {
                    return true;
                }

                warning = app.getWarningIndicator().getMessage();
                app.getWarningIndicator().setMessage(res.getString("TurnKeyLock"));
                try {
                    ToneIndicator tone = posHome.getToneIndicator();
                    if (!tone.getDeviceEnabled())
                        tone.setDeviceEnabled(true);
                    tone.setAsyncMode(true);
                    tone.sound(99999, 500);
                } catch (Exception e) {
                    CreamToolkit.logMessage(e);
                }
                setKeyWarning(true);
                return true;
            }

            if (app.getKeyPosition() == KeylockConst.LOCK_KP_LOCK) {
                if (keyPos >= KeylockConst.LOCK_KP_SUPR + 2) {
                    app.setKeyPosition(keyPos);
                } // else if (((Keylock)event.getSource()).getKeyPosition() == 2) {
            }

            if (app.getTrainingMode()) {
                Transaction trans = app.getCurrentTransaction();
                trans.clear();
                trans.setDealType2("0");
                trans.setInvoiceNumber(Param.getInstance().getInvoiceNumber());
                trans.setInvoiceID(Param.getInstance().getInvoiceID());
                app.getPopupMenuPane().setInputEnabled(true);
                app.getItemList().setItemIndex(0);
                app.getItemList().setTransaction(app.getCurrentTransaction());
                currentState = KeyLock2State.getInstance();
                app.getWarningIndicator().setMessage("");
                app.setTrainingMode(false);
                app.setChecked(true);
                try {
                    ToneIndicator tone = posHome.getToneIndicator();
                    tone.clearOutput();
                } catch (Exception je) {
                    je.printStackTrace(CreamToolkit.getLogger());
                }
            }
        } catch (JposException e) {
            CreamToolkit.logMessage(e);
        }

        return false;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public List<Map<EventObject, Long>> getRecordList() {
        return recordEventLists;
    }
    
    public void setRecorded(int recorded) {
        this.recorded = recorded;
        lastTimes = 0;
    }

    /**
     * 0 = normal
     * 1 = record
     * 2 = replay
     * @return
     */
    public int getRecorded() {
        return this.recorded;
    }

}
