package hyi.cream.state;

import java.util.EventObject;

public class ReadMemberState extends State {
	private static ReadMemberState readMemberState;
	
    public static ReadMemberState getInstance() {
        try {
            if (readMemberState == null) {
            	readMemberState = new ReadMemberState();
            }
        } catch (InstantiationException ex) {
        }
        return readMemberState;
    }
    
	public ReadMemberState() throws InstantiationException {
    }
		
	public void entry(EventObject event, State sourceState) {
		
	}
	
	public Class exit(EventObject event, State sinkState) {
		return IdleState.class;
	}	

}
