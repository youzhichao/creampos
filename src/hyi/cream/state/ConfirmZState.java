package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.event.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;

public class ConfirmZState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
	private static ConfirmZState zState = null;
    //记录从哪个state进来的
    private static Class sourceState = null;
    
    public static ConfirmZState getInstance() {
        if (zState == null)
			zState = new ConfirmZState();
        return zState;
    }

	public ConfirmZState() {
    }

    public void entry(EventObject event, State sourceState) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            ConfirmZState.sourceState = sourceState.getClass();
             
            Date initDate = CreamToolkit.getInitialDate();
    		ZReport z = ZReport.getCurrentZReport(connection);
            int count = ZReport.queryCount(connection);
            if (PARAM.getDailyLimit() != 0) {
                if (count >= PARAM.getDailyLimit()) {
                    app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("ZReady2"));
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                            }
                            POSButtonHome2.getInstance().buttonPressed(new POSButtonEvent(new ClearButton(0,0,0,"")));
                        }
                    }).start();
                    return;
                }
            }
    		int holdCount = 0;
    		if (z == null || CreamToolkit.compareDate(z.getAccountingDate(), initDate) != 0) {
    			app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("ZReady"));
                
                //Bruce/2003-05-29
                //模拟user按ClearButton. 在State里面必需在开一个thread来做。
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                        }
                        POSButtonHome2.getInstance().buttonPressed(new POSButtonEvent(new ClearButton(0,0,0,"")));
                    }
                }).start();

            } else if ((holdCount = TransactionHold.holdTranCount(connection, z.getTerminalNumber())) > 0) {
    			Object[] arg0 = { holdCount + "" };
    			String msg = MessageFormat.format(CreamToolkit.GetResource().getString("UnHoldTransactions"), arg0);
    			if (!PARAM.getEodWithHoldTrn().equalsIgnoreCase("allow")) {
    				app.getWarningIndicator().setMessage(msg);
    			}/* else if (GetProperty.getEODWithHoldTrn("allow").equalsIgnoreCase("deny")) {
    				app.getWarningIndicator().setMessage(msg);
//    				app.getWarningIndicator().setMessage(msg
//    					+ CreamToolkit.GetResource().getString("UnHoldTransactions2"));
    			}*/
            	TransactionHold.makeCancel(connection);
    		} 
    		app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("ZConfirm"));

        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof ClearButton) 
            return sourceState;
//		else if (event.getSource() instanceof EnterButton) {
//			if (GetProperty.getShiftWithHoldTrn(null) != null) { 
//                DbConnection connection = null;
//                try {
//                    connection = CreamToolkit.getTransactionalConnection();
//                    TransactionHold.deleteByPosNO(connection,
//                        ZReport.getCurrentZReport(connection).getTerminalNumber());
//                    connection.commit();
//                } catch (SQLException e) {
//                    CreamToolkit.logMessage(e);
//                    CreamToolkit.haltSystemOnDatabaseFatalError();
//                } finally {
//                    CreamToolkit.releaseConnection(connection);
//                }
//            }
//            return sinkState.getClass();		
//		} 
		else {
			app.getWarningIndicator().setMessage("");
            return sinkState.getClass();
		}
	}
    
    public static Class getSourceState() {
        return sourceState;
    }
}
