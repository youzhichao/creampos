package hyi.cream.state;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.*;
import java.util.*;
import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.event.POSButtonEvent;

/**
 * @author dai
 */
public class PrintVoidReceiptState extends State implements PopupMenuListener {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    static PrintVoidReceiptState PrintVoidReceiptState = null;
    private ArrayList menu = new ArrayList();
    private List receiptReports = new ArrayList();
    private PopupMenuPane popup;

    public static PrintVoidReceiptState getInstance() {
        try {
            if (PrintVoidReceiptState == null) {
                PrintVoidReceiptState = new PrintVoidReceiptState();
            }
        } catch (InstantiationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return PrintVoidReceiptState;
    }

    /**
     * Constructor
     */
    public PrintVoidReceiptState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        DateFormat sdf = CreamCache.getInstance().getDateTimeFormate();
        app.getMessageIndicator().setMessage(res.getString("ConfirmPrintVoidReceipt"));

        popup = POSTerminalApplication.getInstance().getPopupMenuPane();
        menu.clear();
        receiptReports.clear();
        Iterator iter = Transaction.queryRecentVoidReports();
        if (iter != null) {
            int idx = 1;
            while (iter.hasNext()) {
                Transaction voidtranhead = (Transaction)iter.next();
                String sysDate = sdf.format(voidtranhead.getSystemDateTime());
                
                    menu.add(idx + ". " + sysDate);
                receiptReports.add(voidtranhead);
                idx++;
            }
        }
        popup.setMenu(menu);
        popup.setVisible(true);
        popup.setPopupMenuListener(this);
        popup.clear();
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof ClearButton) {
            app.getMessageIndicator().setMessage("");
        }
        if (sinkState == null) {
            return null;
        } else {
            return sinkState.getClass();
        }
    }

    /**
     * 当在PopupMenuPane按数字键加回车，或者按了清除键了以后，都会callback此方法。
     */
    public void menuItemSelected() {
        DbConnection connection = null;
        try {
            int selectIndex = popup.getSelectedNumber();
            if (menu != null && menu.size() > 0) {
                connection = CreamToolkit.getTransactionalConnection();
                System.out.println("VoidTranhead > Select " + selectIndex);
                if (selectIndex >= 0 && selectIndex < 20) {
                    Transaction voidtranhead = (Transaction)receiptReports.get(selectIndex);
                    voidtranhead = Transaction.queryBySequenceNumber(connection, voidtranhead
                        .getSequenceNumber());
                    Iterator voidtrandtl = LineItem.queryByTransactionNumber(connection,
                        voidtranhead.getTerminalNumber(),
                        voidtranhead.getTransactionNumber());
                    // DateFormat sdf = CreamCache.getInstance().getDateFormate();
                    // String endDateTime = sdf.format(voidtranhead.getSystemDateTime());

                    CreamPrinter.getInstance().printHeader(connection, voidtranhead);
                    while (voidtrandtl.hasNext()) {
                        LineItem lineItem = (LineItem)voidtrandtl.next();
                        CreamPrinter.getInstance().printLineItem(connection, lineItem);
                    }
                    CreamPrinter.getInstance().printPayment(connection, voidtranhead);
                }
                if (selectIndex >= -1 && selectIndex < 10) {
                    POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                    POSButtonHome2.getInstance().buttonPressed(e);
                    menu.clear();
                    receiptReports.clear();
                } else {
                    popup.setVisible(true);
                    popup.setPopupMenuListener(this);
                    popup.clear();
                }
                connection.commit();
                return;
    
            } else {
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome2.getInstance().buttonPressed(e);
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}

 
