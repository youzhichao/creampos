package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.*;
import hyi.cream.exception.EntityNotFoundException;
import hyi.cream.groovydac.Param;
import hyi.cream.inline.Client;
import hyi.cream.inline.ConnectionlessClient;
import hyi.cream.util.CreamPropertyUtil;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class StateToolkit {

    private static final NumberFormat taiwanInvoiceNumberFormat = new DecimalFormat(
            "00000000");

    /**
     * 该交易是否要计算还元金
     */
    public static boolean needCheckRebate(Transaction tran) {
        boolean rtn = false;
        if (!StringUtils.isEmpty(Param.getInstance().getRebatePaymentID())) {
            if (Param.getInstance().isOnlyMemberCanUseRebate()) {
                if (tran.getMemberID() != null
                        && tran.getMemberID().length() > 0)
                    rtn = true;
            } else
                rtn = true;
        }
        return rtn;
    }

    /**
     * 打印交易时打印纸是否充足
     *
     * @param tran
     * @return
     */
    public static boolean invoicePageEnough(Transaction tran) {
        if (Param.getInstance().isCountInvoiceEnable()) {
            int invNo = 0;
            try {
                // 由于目前cutpage,property.invoicenumber就+1,
                // 所以此处应取tran.invoicenumber
                invNo = Integer.parseInt(Param.getInstance().getInvoiceNumber());

                // invNo = Integer.parseInt(tran.getInvoiceNumber());
            } catch (Exception e) {
            }
            int countDown = 250 - (invNo % 250);
//            if (countDown == 250)
//                countDown = 1;
//            else
//                countDown++;
            // int perPageLines = 0;
            // try {
            // perPageLines =
            // Integer.parseInt(GetProperty.getPrintLineItemPerPage(""));
            // } catch (Exception e) {
            // }

            // 以下是整单打印
            // int pages = tran.getNeedPrintLineItems().size() / perPageLines;
            // if ((tran.getNeedPrintLineItems().size() % perPageLines) > 0)
            // pages++;
            // 以下是单笔打印
            int pages = tran.getInvoiceCount();// ReceiptGenerator.getInstance().getPageNumber();

            if (countDown < pages) {
                POSTerminalApplication.getInstance().getMessageIndicator()
                        .setMessage(
                                CreamToolkit.GetResource().getString(
                                        "InvoiceNotEnough"));
                return false;
            }
        }
        return true;
    }

    /**
     * 發票號碼加一.
     */
    public static void countUpInvoiceNumber(DbConnection connection)
            throws SQLException, EntityNotFoundException {

        if (connection == null)
            return;

        genNextInvoiceNumber(connection, 1);
    }

    /**
     * 生成下一个发票号.
     * 如果发现下一个发票号是下一卷时把相关属性设置成‘’
     */
    public static String genNextInvoiceNumber(DbConnection connection, int count)
            throws SQLException, EntityNotFoundException {
        String lastInvoiceNumber = null;
        if (StringUtils.isEmpty(Param.getInstance().getInvoiceNumber()))
            return lastInvoiceNumber;
        String invoiceID = Param.getInstance().getInvoiceID();
        int nextInvoiceNumber = Integer.parseInt(Param.getInstance().getInvoiceNumber()) + count;
        if (((nextInvoiceNumber) % 250) == 0) {
            Param.getInstance().updateInvoiceID("");
            Param.getInstance().updateInvoiceNumber("");
        } else {
            Param.getInstance().updateInvoiceNumber(taiwanInvoiceNumberFormat.format(nextInvoiceNumber));
        }
        lastInvoiceNumber = invoiceID
                + taiwanInvoiceNumberFormat.format(nextInvoiceNumber - 1);
        ShiftReport currentShift = ShiftReport.getCurrentShift(connection);
        if (currentShift != null) {
            currentShift.setEndInvoiceNumber(connection, lastInvoiceNumber);
            currentShift.update(connection);
        }
        ZReport currentZ = ZReport.getCurrentZReport(connection);
        if (currentZ != null) {
            currentZ.setEndInvoiceNumber(connection, lastInvoiceNumber);
            currentZ.update(connection);
        }
        return lastInvoiceNumber;
    }

    /**
     * @return deliveryhead.結帳狀態 + "," + deliveryhead.srcBillNo(原始單號) 
     *
     * 0=可結帳 1=不存在 2=已作廢 3=已經結過帳 4=版本不符合 5=无法连接sc 6=可退货 7=已退货
     */
    public static String getPeiDaStatus(String deliveryIDWithPrefixAndVer) throws Exception {
        String storeID = POSTerminalApplication.getInstance()
                .getCurrentTransaction().getStoreNumber();
//        String[] params = { "storeID", storeID,
//                "deliveryID", deliveryIDWithPrefixAndVer.substring(0, deliveryIDWithPrefixAndVer.length() - 2),
//                "ver", deliveryIDWithPrefixAndVer.substring(deliveryIDWithPrefixAndVer.length() - 2)};
//        Object o = InlineClient.getDataFromHttp("deliveryStatus", params);
        //System.out.println("--------------- " + o);
        Client inlineClient = Client.getInstance();
        inlineClient.processCommand("getDeliveryStatus "
            + storeID + " " + deliveryIDWithPrefixAndVer.substring(0, deliveryIDWithPrefixAndVer.length() - 2)
            + " " + deliveryIDWithPrefixAndVer.substring(deliveryIDWithPrefixAndVer.length() - 2));
        String ret = (String)inlineClient.getReturnObject2();
        if (ret == null || ret.equals("null"))
            return "-1";
        return ret;
    }

    /**
     * 从后台posul_tran 获得交易 并转换 成Pos交易
     *
     * @param storeID 如果要取他店交易數據，storeID要給他店店號
     */
    public static Transaction posulTran2Tran(String storeID, String posNo,
            String tranNo) throws Exception {

        Transaction tran;
        if (!Store.getStoreID().equals(storeID)) {
            // 取他店交易
            ConnectionlessClient inlineClient = null;
            try {
                String storeIP = Reason.queryStoreIPAddress(storeID);
                inlineClient = new ConnectionlessClient(storeIP, 3000);
                inlineClient.processCommand("getTransaction " + posNo + " " + tranNo);
                tran = (Transaction)inlineClient.getReturnObject();
            } finally {
                if (inlineClient != null)
                    inlineClient.closeConnection();
            }
        } else {
            Client inlineClient = Client.getInstance();
            inlineClient.processCommand("getTransaction " + posNo + " " + tranNo);
            tran = (Transaction)inlineClient.getReturnObject();
        }

//        Object o = null;
//        o = InlineClient.getDataFromHttp("posulTranhead", new String[] {
//                "storeID", storeID, "posNo", posNo, "tranNo", tranNo });
//        if (o == null ||  o.toString().equals("null") || ((List) o).isEmpty())
//            return tran;
//        else {
//            List<Map> datas = (List<Map>) o;
//            Iterator it = datas.iterator();
//            if (it.hasNext()) {
//                Map head = (Map) it.next();
//                tran = new Transaction();
//                tran.init();
//                tran.getFieldMap().putAll(head);
//            }
//
//            while (it.hasNext()) {
//                Map dtl = (Map) it.next();
//                LineItem li = new LineItem();
//                li.setDescriptionAndSpecification((String) dtl
//                        .get("DescriptionAndSpecification"));
//                dtl.remove("DescriptionAndSpecification");
//                li.setDescription((String) dtl.get("Description"));
//                dtl.remove("Description");
//                li.setDaiShouBarcode((String) dtl.get("DaiShouBarcode"));
//                dtl.remove("DaiShouBarcode");
//                li.setDaiShouID((String) dtl.get("DaiShouID"));
//                dtl.remove("DaiShouID");
//                li.getFieldMap().putAll(dtl);
//                if (li.getPluNumber() == null || li.getPluNumber().trim().equals("")) {
//                    Category category = Category.queryByCategoryNumber(li.getCategoryNumber(),
//                            li.getMidCategoryNumber(), li.getMicroCategoryNumber());
//                    if (category != null && category.getScreenName() != null && !category.getScreenName().trim().equals("")) {
//                        li.setDescription(category.getScreenName());
//                        li.setDescriptionAndSpecification(category.getScreenName());
//                    }
//                }
//                tran.setLockEnable(true);
//                try {
//                    tran.addLineItem(li, false);
//                } catch (TooManyLineItemsException e) {
//                    e.printStackTrace();
//                }
//            }
//            String payno = null;
//            String payamt = null;
//            for (int i = 1; i < 5; i++) {
//                payno = "PAYNO" + i;
//                payamt = "PAYAMT" + i;
//                if (tran.getFieldValue(payno) == null
//                        || tran.getFieldValue(payno).toString().trim().equals("")) {
//                    break;
//                }
//                tran.addPayment(Payment.queryByPaymentID((String) tran.getFieldValue(payno)));
//            }
//            tran.setLockEnable(true);
//        }

        tran.initForAccum();
        tran.accumulate();
        return tran;
    }

    public static List<LineItem> getPeiDaDtl2LineItem(Transaction tran, String deliveryIDWithPrifixAndVer) throws Exception {
        String storeID = tran.getStoreNumber();
        List<LineItem> lis = new ArrayList<LineItem>();
        //String[] params = { "storeID", storeID, "deliveryID", deliveryID.substring(0, 8), "ver",  deliveryID.substring(8)};
        // 包含不定长前缀，所以固定截取2位version后缀
//        String[] params = { "storeID", storeID,
//                "deliveryID", deliveryIDWithPrifixAndVer.substring(0, deliveryIDWithPrifixAndVer.length() - 2),
//                "ver", deliveryIDWithPrifixAndVer.substring(deliveryIDWithPrifixAndVer.length() - 2)};
//        Object o = InlineClient.getDataFromHttp("deliveryDtls", params);

        Client inlineClient = Client.getInstance();
        inlineClient.processCommand("getPosDeliveryDtl "
            + storeID + " " + deliveryIDWithPrifixAndVer.substring(0, deliveryIDWithPrifixAndVer.length() - 2)
            + " " + deliveryIDWithPrifixAndVer.substring(deliveryIDWithPrifixAndVer.length() - 2));
        Object o = inlineClient.getReturnObject2();
        if (o == null || o.toString().equals("null"))
            ;
        else {
            List<Map> datas = (List<Map>) o;
            Iterator it = datas.iterator();
            if (it.hasNext()) {
                Map head = (Map) it.next();
                String billType = (String) head.get("billType");
                // if (billType != null && billType.equals("H")){
                // billType = "P"; // 目前还是共用 P 作为传票类型
                // }
                tran.setAnnotatedType(billType);
                // 如果能读到memberID 或 saleMan就填进去
                String memberID = (String) head.get("memberID");
                if (memberID != null){
                    tran.setMemberID(memberID);
                }
                String saleMan = (String) head.get("saleMan");
                if (saleMan != null){
                    tran.setSalesman(saleMan);
                }
            }
            while (it.hasNext()) {
                Map dtl = (Map) it.next();
                LineItem li = new LineItem();
                String authorNo = (String)dtl.get("authorNo");
                if (authorNo != null){
                    dtl.remove("authorNo");
                    li.setAuthorNoContent(authorNo);
                }
                String desc = (String) dtl.get("Description");
                dtl.remove("Description");
                li.setDescriptionAndSpecification(desc);
                li.setDescription(desc);
                li.setTerminalNumber(tran.getTerminalNumber());
                li.setTransactionNumber(tran.getTransactionNumber());
                li.setDetailCode("S");
                li.getFieldMap().putAll(dtl);
                lis.add(li);
            }
        }
        return lis;
    }

    /**
     * 发票是否换卷
     * @return
     */
    public static boolean isInvoiceChanged() {
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        String lastInvoiceID = app.getLastInvoiceID();
        String lastInvoiceNumber = app.getLastInvoiceNumber();
        String curInvoiceID = Param.getInstance().getInvoiceID();
        String curInvoiceNumber = Param.getInstance().getInvoiceNumber();
        // 发票字轨不一样
        if (!lastInvoiceID.equals(curInvoiceID))
            return true;
        // 发票号前5为不一样
        if (!lastInvoiceNumber.substring(0, 5).equals(curInvoiceNumber.substring(0, 5)))
            return true;
        // 是否同一卷
        int last = Integer.parseInt(lastInvoiceNumber.substring(5, 8)) / 250;
        int cur  = Integer.parseInt(curInvoiceNumber.substring(5, 8)) / 250;
        if (last != cur)
            return true;
        // 同一卷
        return false;
    }

    /**
     * 根据taxType与amt 计算taxAmt
     */
    public static HYIDouble calculateTaxAmt(TaxType taxType, HYIDouble amt, HYIDouble qty) {
        if (taxType == null || qty == null || qty.compareTo(new HYIDouble(0)) == 0){
            return new HYIDouble(0); // ? is it need set scale?
        }
        HYIDouble taxAmt = new HYIDouble(0);
        HYIDouble taxPercent = taxType.getPercent();
        String taxTp = taxType.getType();
        String taxRound = taxType.getRound();
        int taxDigit = taxType.getDecimalDigit().intValue();
        int round = 0;
        if (taxRound.equalsIgnoreCase("R")) {
            round = BigDecimal.ROUND_HALF_UP;
        } else if (taxRound.equalsIgnoreCase("C")) {
            round = BigDecimal.ROUND_UP;
        } else if (taxRound.equalsIgnoreCase("D")) {
            round = BigDecimal.ROUND_DOWN;
        }

        if (Param.getInstance().isCalculateTaxAmtByUnitPrice()){
            amt = amt.divide(qty, 4, BigDecimal.ROUND_HALF_UP);
        }

        if (taxTp.equalsIgnoreCase("1")) {
            taxAmt = (amt.multiply(taxPercent)).divide(
                    new HYIDouble(1).addMe(taxPercent), 2).setScale(taxDigit,
                    round);
        } else if (taxTp.equalsIgnoreCase("2")) {
            taxAmt = (amt.multiply(taxPercent)).setScale(taxDigit, round);
        } else {
            taxAmt = new HYIDouble(0.00).setScale(taxDigit, round);
        }

        if (Param.getInstance().isCalculateTaxAmtByUnitPrice()){
            taxAmt = taxAmt.multiply(qty).setScale(taxDigit, round);
        }
        return taxAmt;
    }

    public static void recalcTaxAmount(Transaction tran) {
        /*
         * 有關台灣稅額計算的修改 以稅別'1'為例：
         * tranhead.taxamt1 = 稅別為'1'銷售額(tranhead.netsalamt1) * 5 / 105（四捨五入到TAXTYPE記錄的小數位數）其中，
         * 稅別為'1'銷售額(tranhead.netsalamt1) == 稅別為'1'(trandetail.taxtype=='1')的trandetail.aftdscamt的合計
         * 
         * 然後再把 tranhead.taxamt1 分攤到稅別為'1'的trandetail.taxamt（四捨五入到小數兩位），
         * 使得稅別為'1'的trandetail.taxamt合計 == tranhead.taxamt1
         * 
         * depsales.taxamount累加相應分類商品的trandetail.taxamt
         * z.taxamt1累加相應稅別商品的的
         */
        HYIDouble zero = new HYIDouble(0);
        HYIDouble subTaxAmt0 = new HYIDouble(0);
        HYIDouble subTaxAmt1 = new HYIDouble(0);
        HYIDouble subTaxAmt2 = new HYIDouble(0);
        HYIDouble subTaxAmt3 = new HYIDouble(0);
        HYIDouble subTaxAmt4 = new HYIDouble(0);
        HYIDouble subTaxAmt5 = new HYIDouble(0);
        HYIDouble subTaxAmt6 = new HYIDouble(0);
        HYIDouble subTaxAmt7 = new HYIDouble(0);
        HYIDouble subTaxAmt8 = new HYIDouble(0);
        HYIDouble subTaxAmt9 = new HYIDouble(0);
        HYIDouble subTaxAmt10 = new HYIDouble(0);
        // 计算 tran 各种税额
        if (tran.getNetSalesAmount0() != null && tran.getNetSalesAmount0().compareTo(zero) != 0) {
            TaxType tax = TaxType.queryByTaxID("0");
            if (tax != null) {
                tran.setTaxAmount0(StateToolkit.calculateTaxAmt(tax, tran.getNetSalesAmount0(), new HYIDouble(1)));
                subTaxAmt0 = tran.getTaxAmount0();
            }
        }
        if (tran.getNetSalesAmount1() != null && tran.getNetSalesAmount1().compareTo(zero) != 0) {
            TaxType tax = TaxType.queryByTaxID("1");
            if (tax != null) {
                tran.setTaxAmount1(StateToolkit.calculateTaxAmt(tax, tran.getNetSalesAmount1(), new HYIDouble(1)));
                subTaxAmt1 = tran.getTaxAmount1();
            }
        }
        if (tran.getNetSalesAmount2() != null && tran.getNetSalesAmount2().compareTo(zero) != 0) {
            TaxType tax = TaxType.queryByTaxID("2");
            if (tax != null) {
                tran.setTaxAmount2(StateToolkit.calculateTaxAmt(tax, tran.getNetSalesAmount2(), new HYIDouble(1)));
                subTaxAmt2 = tran.getTaxAmount2();
            }
        }
        if (tran.getNetSalesAmount3() != null && tran.getNetSalesAmount3().compareTo(zero) != 0) {
            TaxType tax = TaxType.queryByTaxID("3");
            if (tax != null) {
                tran.setTaxAmount3(StateToolkit.calculateTaxAmt(tax, tran.getNetSalesAmount3(), new HYIDouble(1)));
                subTaxAmt3 = tran.getTaxAmount3();
            }
        }
        if (tran.getNetSalesAmount4() != null && tran.getNetSalesAmount4().compareTo(zero) != 0) {
            TaxType tax = TaxType.queryByTaxID("4");
            if (tax != null) {
                tran.setTaxAmount4(StateToolkit.calculateTaxAmt(tax, tran.getNetSalesAmount4(), new HYIDouble(1)));
                subTaxAmt4 = tran.getTaxAmount4();
            }
        }
        if (tran.getNetSalesAmount5() != null && tran.getNetSalesAmount5().compareTo(zero) != 0) {
            TaxType tax = TaxType.queryByTaxID("5");
            if (tax != null) {
                tran.setTaxAmount5(StateToolkit.calculateTaxAmt(tax, tran.getNetSalesAmount5(), new HYIDouble(1)));
                subTaxAmt5 = tran.getTaxAmount5();
            }
        }
        if (tran.getNetSalesAmount6() != null && tran.getNetSalesAmount6().compareTo(zero) != 0) {
            TaxType tax = TaxType.queryByTaxID("6");
            if (tax != null) {
                tran.setTaxAmount6(StateToolkit.calculateTaxAmt(tax, tran.getNetSalesAmount6(), new HYIDouble(1)));
                subTaxAmt6 = tran.getTaxAmount6();
            }
        }
        if (tran.getNetSalesAmount7() != null && tran.getNetSalesAmount7().compareTo(zero) != 0) {
            TaxType tax = TaxType.queryByTaxID("7");
            if (tax != null) {
                tran.setTaxAmount7(StateToolkit.calculateTaxAmt(tax, tran.getNetSalesAmount7(), new HYIDouble(1)));
                subTaxAmt7 = tran.getTaxAmount7();
            }
        }
        if (tran.getNetSalesAmount8() != null && tran.getNetSalesAmount8().compareTo(zero) != 0) {
            TaxType tax = TaxType.queryByTaxID("8");
            if (tax != null) {
                tran.setTaxAmount8(StateToolkit.calculateTaxAmt(tax, tran.getNetSalesAmount8(), new HYIDouble(1)));
                subTaxAmt8 = tran.getTaxAmount8();
            }
        }
        if (tran.getNetSalesAmount9() != null && tran.getNetSalesAmount9().compareTo(zero) != 0) {
            TaxType tax = TaxType.queryByTaxID("9");
            if (tax != null) {
                tran.setTaxAmount9(StateToolkit.calculateTaxAmt(tax, tran.getNetSalesAmount9(), new HYIDouble(1)));
                subTaxAmt9 = tran.getTaxAmount9();
            }
        }
        if (tran.getNetSalesAmount10() != null && tran.getNetSalesAmount10().compareTo(zero) != 0) {
            TaxType tax = TaxType.queryByTaxID("10");
            if (tax != null) {
                tran.setTaxAmount10(StateToolkit.calculateTaxAmt(tax, tran.getNetSalesAmount10(), new HYIDouble(1)));
                subTaxAmt10 = tran.getTaxAmount10();
            }
        }

        // 从tran税额，分摊到dtl税额
        /**
         *  "S":销售 "R":退货 "D":SI折扣 "M":Mix & Match折扣 "B":退瓶 "I":代收 "O":代收公共事业费 "Q":代支 "V":指定更正 "E":立即更正 "H":借零
         *  "G":投库 "N":PaidIn "T":PaidOut "A":RoundDown
         */
        // todo 如果税额 < 0.5 改如何
        Map<String, List> sortByTaxID = new HashMap<String, List>();
        for (int i = 0; i < tran.getLineItems().length; i++) {
            LineItem li = (LineItem) tran.getLineItems()[i];
            if (li.getDetailCode() != null && !li.getRemoved() &&
                    (li.getDetailCode().equalsIgnoreCase("S") ||
                    li.getDetailCode().equalsIgnoreCase("R") ||
                    (li.getDetailCode().equalsIgnoreCase("D") && Param.getInstance().isGrossTrandtlTaxAmt()))
                )
            {
                if (sortByTaxID.containsKey(li.getTaxType())) {
                    sortByTaxID.get(li.getTaxType()).add(li);
                } else {
                    List lis = new ArrayList();
                    lis.add(li);
                    sortByTaxID.put(li.getTaxType(), lis);
                }
            }
        }
        // 分摊税额到dtl,按金额从小到大依次分摊
        for (Iterator it = sortByTaxID.keySet().iterator(); it.hasNext();) {
            List list = sortByTaxID.get(it.next());
            Collections.sort(list, new TT());
            for (Iterator it2 = list.iterator(); it2.hasNext();) {
                LineItem li = (LineItem) it2.next();
                if (zero.compareTo(tran.getNetSalesAmount0()) != 0 && "0".equals(li.getTaxType())) {
                    if (!it2.hasNext()) {
                        li.setTaxAmount(subTaxAmt0);
                        subTaxAmt0 = new HYIDouble(0);
                    } else {
                        HYIDouble percent = li.getAfterDiscountAmount().divide(tran.getNetSalesAmount0(), 5, BigDecimal.ROUND_HALF_UP);
                        li.setTaxAmount(tran.getTaxAmount0().multiply(percent).setScale(2, BigDecimal.ROUND_HALF_UP));
                        subTaxAmt0 = subTaxAmt0.subtract(li.getTaxAmount());
                    }
                } else if (zero.compareTo(tran.getNetSalesAmount1()) != 0 && "1".equals(li.getTaxType())) {
                    if (!it2.hasNext()) {
                        li.setTaxAmount(subTaxAmt1);
                        subTaxAmt1 = new HYIDouble(0);
                    } else {
                        HYIDouble percent = li.getAfterDiscountAmount().divide(tran.getNetSalesAmount1(), 5, BigDecimal.ROUND_HALF_UP);
                        li.setTaxAmount(tran.getTaxAmount1().multiply(percent).setScale(2, BigDecimal.ROUND_HALF_UP));
                        subTaxAmt1 = subTaxAmt1.subtract(li.getTaxAmount());
                    }
                } else if (zero.compareTo(tran.getNetSalesAmount2()) != 0 && "2".equals(li.getTaxType())) {
                    if (!it2.hasNext()) {
                        li.setTaxAmount(subTaxAmt2);
                        subTaxAmt2 = new HYIDouble(0);
                    } else {
                        HYIDouble percent = li.getAfterDiscountAmount().divide(tran.getNetSalesAmount2(), 5, BigDecimal.ROUND_HALF_UP);
                        li.setTaxAmount(tran.getTaxAmount2().multiply(percent).setScale(2, BigDecimal.ROUND_HALF_UP));
                        subTaxAmt2 = subTaxAmt2.subtract(li.getTaxAmount());
                    }
                } else if (zero.compareTo(tran.getNetSalesAmount3()) != 0 && "3".equals(li.getTaxType())) {
                    if (!it2.hasNext()) {
                        li.setTaxAmount(subTaxAmt3);
                        subTaxAmt3 = new HYIDouble(0);
                    } else {
                        HYIDouble percent = li.getAfterDiscountAmount().divide(tran.getNetSalesAmount3(), 5, BigDecimal.ROUND_HALF_UP);
                        li.setTaxAmount(tran.getTaxAmount3().multiply(percent).setScale(2, BigDecimal.ROUND_HALF_UP));
                        subTaxAmt3 = subTaxAmt3.subtract(li.getTaxAmount());
                    }
                } else if (zero.compareTo(tran.getNetSalesAmount4()) != 0 && "4".equals(li.getTaxType())) {
                    if (!it2.hasNext()) {
                        li.setTaxAmount(subTaxAmt4);
                        subTaxAmt4 = new HYIDouble(0);
                    } else {
                        HYIDouble percent = li.getAfterDiscountAmount().divide(tran.getNetSalesAmount4(), 5, BigDecimal.ROUND_HALF_UP);
                        li.setTaxAmount(tran.getTaxAmount4().multiply(percent).setScale(2, BigDecimal.ROUND_HALF_UP));
                        subTaxAmt4 = subTaxAmt4.subtract(li.getTaxAmount());
                    }
                } else if (zero.compareTo(tran.getNetSalesAmount5()) != 0 && "5".equals(li.getTaxType())) {
                    if (!it2.hasNext()) {
                        li.setTaxAmount(subTaxAmt5);
                        subTaxAmt5 = new HYIDouble(0);
                    } else {
                        HYIDouble percent = li.getAfterDiscountAmount().divide(tran.getNetSalesAmount5(), 5, BigDecimal.ROUND_HALF_UP);
                        li.setTaxAmount(tran.getTaxAmount5().multiply(percent).setScale(2, BigDecimal.ROUND_HALF_UP));
                        subTaxAmt5 = subTaxAmt5.subtract(li.getTaxAmount());
                    }
                } else if (zero.compareTo(tran.getNetSalesAmount6()) != 0 && "6".equals(li.getTaxType())) {
                    if (!it2.hasNext()) {
                        li.setTaxAmount(subTaxAmt6);
                        subTaxAmt6 = new HYIDouble(0);
                    } else {
                        HYIDouble percent = li.getAfterDiscountAmount().divide(tran.getNetSalesAmount6(), 5, BigDecimal.ROUND_HALF_UP);
                        li.setTaxAmount(tran.getTaxAmount6().multiply(percent).setScale(2, BigDecimal.ROUND_HALF_UP));
                        subTaxAmt6 = subTaxAmt6.subtract(li.getTaxAmount());
                    }
                } else if (zero.compareTo(tran.getNetSalesAmount7()) != 0 && "7".equals(li.getTaxType())) {
                    if (!it2.hasNext()) {
                        li.setTaxAmount(subTaxAmt7);
                        subTaxAmt7 = new HYIDouble(0);
                    } else {
                        HYIDouble percent = li.getAfterDiscountAmount().divide(tran.getNetSalesAmount7(), 5, BigDecimal.ROUND_HALF_UP);
                        li.setTaxAmount(tran.getTaxAmount7().multiply(percent).setScale(2, BigDecimal.ROUND_HALF_UP));
                        subTaxAmt7 = subTaxAmt7.subtract(li.getTaxAmount());
                    }
                } else if (zero.compareTo(tran.getNetSalesAmount8()) != 0 && "8".equals(li.getTaxType())) {
                    if (!it2.hasNext()) {
                        li.setTaxAmount(subTaxAmt8);
                        subTaxAmt8 = new HYIDouble(0);
                    } else {
                        HYIDouble percent = li.getAfterDiscountAmount().divide(tran.getNetSalesAmount8(), 5, BigDecimal.ROUND_HALF_UP);
                        li.setTaxAmount(tran.getTaxAmount8().multiply(percent).setScale(2, BigDecimal.ROUND_HALF_UP));
                        subTaxAmt8 = subTaxAmt8.subtract(li.getTaxAmount());
                    }
                } else if (zero.compareTo(tran.getNetSalesAmount9()) != 0 && "9".equals(li.getTaxType())) {
                    if (!it2.hasNext()) {
                        li.setTaxAmount(subTaxAmt9);
                        subTaxAmt9 = new HYIDouble(0);
                    } else {
                        HYIDouble percent = li.getAfterDiscountAmount().divide(tran.getNetSalesAmount9(), 5, BigDecimal.ROUND_HALF_UP);
                        li.setTaxAmount(tran.getTaxAmount9().multiply(percent).setScale(2, BigDecimal.ROUND_HALF_UP));
                        subTaxAmt9 = subTaxAmt9.subtract(li.getTaxAmount());
                    }
                } else if (zero.compareTo(tran.getNetSalesAmount10()) != 0 && "10".equals(li.getTaxType())) {
                    if (!it2.hasNext()) {
                        li.setTaxAmount(subTaxAmt10);
                        subTaxAmt10 = new HYIDouble(0);
                    } else {
                        HYIDouble percent = li.getAfterDiscountAmount().divide(tran.getNetSalesAmount10(), 5, BigDecimal.ROUND_HALF_UP);
                        li.setTaxAmount(tran.getTaxAmount10().multiply(percent).setScale(2, BigDecimal.ROUND_HALF_UP));
                        subTaxAmt10 = subTaxAmt10.subtract(li.getTaxAmount());
                    }
                }
            }
        }
    }
}

class TT implements Comparator {
    public int compare(Object o1, Object o2) {
        LineItem li1 = (LineItem) o1;
        LineItem li2 = (LineItem) o2;
        return li1.getAfterDiscountAmount().compareTo(li2.getAfterDiscountAmount());
    }
}
