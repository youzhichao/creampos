package hyi.cream.state.wholesale;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.DaiShouDef;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.TooManyLineItemsException;
import hyi.cream.state.State;
import hyi.cream.state.WarningState;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.text.MessageFormat;
import java.util.*;

public class ListWholesaleState extends State {
    static ListWholesaleState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Transaction trans;
    private ResourceBundle res = CreamToolkit.GetResource();

    public static ListWholesaleState getInstance() {
        try {
            if (instance == null) {
                instance = new ListWholesaleState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    /**
     * Constructor
     */
    public ListWholesaleState() throws InstantiationException {
    }

    @Override
    public void entry(EventObject event, State sourceState) {

    }

    @Override
    public Class exit(EventObject event, State sinkState) {
        trans = app.getCurrentTransaction();
        String billNo = (String)CreamSession.getInstance().getAttribute(WorkingStateEnum.WHOLESALE_STATE, "BillNo");
        int billTypeChoosed;
        String s = (String)CreamSession.getInstance().getAttribute(WorkingStateEnum.WHOLESALE_STATE, "billType");

        try { //异常
            billTypeChoosed = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            setWarningMessage(MessageFormat.
                format(res.getString("WholesaleWaring7"), billNo));
            WarningState.setExitState(WholesaleState.class);
            return WarningState.class;
        }

        if ((billTypeChoosed == 7 || billTypeChoosed == 8)
            && Transaction.isExistInTranhead(billNo, billTypeChoosed)) {
            //"{0}单据已经交易过"
            setWarningMessage(MessageFormat.format(res.getString(
                "WholesaleWaring8"), billNo));
            WarningState.setExitState(WholesaleState.class);
            return WarningState.class;
        }

        // 到sc查找该团购单
        List wholesales = WholesaleUtils.getWholesales(trans.getStoreNumber(), billNo);

        if (wholesales == null || wholesales.isEmpty()) {
            //"{0}单据不存在"
            setWarningMessage(MessageFormat.format(res.getString(
                "WholesaleWaring"), billNo));
            WarningState.setExitState(WholesaleState.class);
            return WarningState.class;
        }

        DaiShouDef dsf = null;
        Iterator it = wholesales.iterator();
        Map head = (Map)it.next();
        // 5 团购
        // 6 开票
        // 7 代配领取 （开票，不收钱）
        // 8 代配退回 （要退钱)
        int billType = (Integer)head.get("BILLTYPE");
        boolean selectWholeSales = billTypeChoosed == 5;
        if (!(selectWholeSales && (billType == 5 || billType == 6))
            && billTypeChoosed != billType) {
            setWarningMessage(MessageFormat.
                format(res.getString("WholesaleWaring7"), billNo));
            WarningState.setExitState(WholesaleState.class);
            return WarningState.class;
        }

        HYIDouble grossAmount = (HYIDouble)head.get("gamt");
        if (billType == 8) {
            ArrayList<DaiShouDef> daiShouDefs = DaiShouDef.matchDaiShouDefs(billNo);
            if (daiShouDefs.size() != 1) {
                //"{0}单号无法匹配唯一编码定义, 请确认单号正确性"
                setWarningMessage(MessageFormat.
                    format(res.getString("WholesaleWaring5"), billNo));
                WarningState.setExitState(WholesaleState.class);
                return WarningState.class;
            }
            dsf = daiShouDefs.get(0);
            if (dsf.getAmount(billNo).compareTo(grossAmount) != 0) {
                //金额不一致, 报错, 下发单据时应有明细合计出总金额，放在单号中
                setWarningMessage(MessageFormat.
                    format(res.getString("WholesaleWaring6"), billNo));
                WarningState.setExitState(WholesaleState.class);
                return WarningState.class;
            }
        }

        // update to transaction now!

        trans.setMemberID((String)head.get("CARDNO"));
        trans.setAnnotatedId(billNo);
        while (it.hasNext()) {
            Map dtl = (Map)it.next();
            LineItem li = new LineItem();
            String desc = (String)dtl.get("Description");
            dtl.remove("Description");
            li.setDescription(desc);
            li.setDescriptionAndSpecification(desc);
            li.setTerminalNumber(trans.getTerminalNumber());
            li.setTransactionNumber(trans.getTransactionNumber());
            li.setDetailCode("S");
            li.getFieldMap().putAll(dtl);
            try {
                System.out.println("Add item: " + li.toString());
                trans.addLineItem(li, false);
            } catch (TooManyLineItemsException e) {
                CreamToolkit.logMessage(e);
            }
        }
        trans.initForAccum();
        trans.accumulate();

        if (billType == 5) {
            trans.setState1("T");
        } else if (billType == 6) {
            trans.setDealType1("0");
            trans.setDealType2("P"); //开票单打印
            trans.setDealType3("0");
            //trans.setState1("T");
            trans.setState1("P");
        } else if (billType == 7) {
            trans.setDealType1("0");
            trans.setDealType2("P"); //开票单打印
            trans.setDealType3("0");
            trans.setState1("D");
            //trans.setState1("P");
        } else if (billType == 8) {
            //ArrayList<DaiShouDef> daiShouDefs = DaiShouDef.matchDaiShouDefs(billNo);
            if (dsf != null) {
                //该交易不计算营收，但是要字段附加一笔代收
                CreamSession.getInstance().setAttribute(WorkingStateEnum.WHOLESALE_STATE,
                    "appendDaiShouDef", dsf);
            }
            app.setReturnItemState(true); //设为负向销售
            trans.makeNegativeValue();
            trans.setDealType1("0");
            trans.setDealType2("P"); //开票单打印
            trans.setDealType3("0");
            trans.setState1("B");
        }

        return WholesaleEndState.class;
    }

//	/**
//	 * @param billNo
//	 * @param wholesales
//	 * @return
//	 */
//	private int fillWholesaleToTran(String billNo, List wholesales, Transaction t) {
//		Iterator it = wholesales.iterator();
//		Map head = (Map) it.next();
//		// 5 团购
//		// 6 开票
//		// 7 代配领取 （开票，不收钱）
//		// 8 代配退回 （要退钱)
//		int billType = (Integer) head.get("BILLTYPE");
//		t.setMemberID((String) head.get("CARDNO"));
//		t.setAnnotatedId(billNo);
//    	while (it.hasNext()) {
//    		Map dtl = (Map) it.next();
//    		LineItem li = new LineItem();
//    		String desc = (String) dtl.get("Description");
//    		dtl.remove("Description");
//    		li.setDescription(desc);
//    		li.setDescriptionAndSpecification(desc);
//			li.setTerminalNumber(t.getTerminalNumber());
//			li.setTransactionNumber(t.getTransactionNumber());
//			li.setDetailCode("S");
//    		li.getFieldMap().putAll(dtl);
//            try {
//            	t.addLineItem(li);
//			} catch (TooManyLineItemsException e) {
//				CreamToolkit.logMessage(e);
//			}
//    	}
//		t.initForAccum();
//		t.accumulate();
//		return billType;
//	}
//

}
