
// Copyright (c) 2000 HYI
package hyi.cream.state.wholesale;

import hyi.cream.state.SomeAGNumberingState;

/**
 * A Class class.
 * <P>
 */
public class WholesaleNoNumberingState extends SomeAGNumberingState {
	
    static WholesaleNoNumberingState instance = null;

    public static WholesaleNoNumberingState getInstance() {
        try {
            if (instance == null) {
                instance = new WholesaleNoNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    /**
     * Constructor
     */
    public WholesaleNoNumberingState() throws InstantiationException {
    }
    
}

 