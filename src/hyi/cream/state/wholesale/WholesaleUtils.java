package hyi.cream.state.wholesale;

import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.inline.Server;
import hyi.cream.state.GetProperty;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DBToolkit;
import hyi.cream.util.HYIDouble;
import hyi.cream.groovydac.Param;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WholesaleUtils {

	/**
	 * 从inline server端获得团购明细
	 * inline client端
	 * @return 
	 */
	public static List<String> getWholesales(String storeID, String billNo) {
		
        if (!Client.getInstance().isConnected()) {
        	return null;
        }
        	
        try {
            Client client = Client.getInstance();
            client.processCommand("getWholesales " + storeID + " " + billNo);
            List list = (List)client.getReturnObject2();
            return list;
        } catch (ClientCommandException e) {
            return null;
        }
	}
	
	/*
	 * 获取团购资料==>List<Map> 
	 * inline server 端
	 */
	public static List<Map> processGetWholesales(String storeID, String billNo) {
		List<Map> wholesale = new ArrayList<Map>();
		DbConnection connection = null;
		try {
			connection = CreamToolkit.getPooledConnection();
			String sql = "SELECT * FROM wholesalehead WHERE storeID = '"
					+ storeID + "' AND billNo = '" + billNo + "' AND billState = 2";
			Server.log("processGetWholesales | sql : " + sql);
			List<Map> wholesalehead = new ArrayList<Map>();
			try {
				wholesalehead = DBToolkit.query2(connection, sql);
			} catch (SQLException e) {
				e.printStackTrace(Server.getLogger());
				return wholesale;
			}
			if (wholesalehead.size() == 0)
				return wholesale;
			Server.log("processGetWholesales | wholesalehead.size : " + wholesalehead.size());

			int billType = 0;
			Map head = wholesalehead.get(0);
			Server.log("processGetWholesales | head : " + head);
			Map<String, Object> pd_head = new HashMap<String, Object>();
			billType = (Integer) head.get("billType");
			pd_head.put("BILLTYPE", billType);
			pd_head.put("BILLNO", head.get("billNo"));
			pd_head.put("CARDNO", head.get("cardNo"));
			pd_head.put("BILLSTATE", head.get("billState"));

			sql = "SELECT * FROM wholesaledtl WHERE storeID = '" + storeID
					+ "' AND billNo = '" + billNo + "'";

			Server.log("processGetWholesales | sql : " + sql);
			List<Map> wholesaledtls = new ArrayList<Map>();
			try {
				wholesaledtls = DBToolkit.query2(connection, sql);
			} catch (SQLException e) {
				e.printStackTrace(Server.getLogger());
				wholesale.clear();
				return wholesale;
			}
			Server.log("processGetWholesales | wholesaledtls.size : " + wholesaledtls.size());

			HYIDouble grossAmount = new HYIDouble(0);
			int seq = 1;
			Map<String, Object> specialDtl = new HashMap<String, Object>();
			List<Map> dtls = new ArrayList<Map>();
			Map<String, Object> pd = new HashMap<String, Object>();
			for (Map dtl : wholesaledtls) {
				pd = new HashMap<String, Object>();
				String pluNo = (String) dtl.get("pluNo");
				pd.put("PLUNO", pluNo);
				pd.put("STOREID", storeID);
				pd.put("ITEMSEQ", seq);
				pd.put("CATNO", dtl.get("categoryNumber"));
				pd.put("MIDCATNO", dtl.get("midCategoryNumber"));
				pd.put("MICROCATNO", dtl.get("microCategoryNumber"));
				pd.put("UNITPRICE", dtl.get("sellPrice"));
				pd.put("QTY", dtl.get("qtyBase"));
				pd.put("AMT", dtl.get("amt"));
				pd.put("TAXTYPE", String.valueOf(dtl.get("taxType")));
				pd.put("TAXAMT", dtl.get("taxAmt"));
				pd.put("ITEMNO", dtl.get("itemNo"));
				pd.put("ORIGPRICE", pd.get("UNITPRICE"));
				pd.put("AFTDSCAMT", pd.get("AMT"));
				pd.put("DetailCode", "S");
				pd.put("Description", dtl.get("displayName"));
				pd.put("DTLCODE1", dtl.get("dtlcode1"));
				System.out.println(pd);
                System.out.println("WholesaleUtils> pluNo=" + pluNo);
				if (billType == 5 && GetProperty.getWholesaleSpecialPluno().equals(pluNo.trim())) {
					if (specialDtl.isEmpty()) {
						seq++;
						specialDtl.putAll(pd);
						dtls.add(specialDtl);
					} else {
						specialDtl.put("QTY", 1);
						specialDtl.put("AMT", ((HYIDouble) specialDtl
								.get("AMT")).add((HYIDouble) dtl.get("amt")));
						specialDtl.put("TAXAMT", ((HYIDouble) specialDtl
								.get("TAXAMT")).add((HYIDouble) dtl.get("taxAmt")));
						specialDtl.put("UNITPRICE", specialDtl.get("AMT"));
						specialDtl.put("ORIGPRICE", specialDtl.get("AMT"));
						specialDtl.put("AFTDSCAMT", specialDtl.get("AMT"));
					}
					continue;
				}
				dtls.add(pd);
				HYIDouble amt = (HYIDouble)pd.get("AMT");
				if (amt != null){
					grossAmount.addMe(amt);
				}
				Server.log("wholesaledtl : " + pd);
				seq++;
			}
			pd_head.put("gamt", grossAmount);
			wholesale.add(pd_head);
			wholesale.addAll(dtls);
		} catch (SQLException e) {
			e.printStackTrace(Server.getLogger());
		} catch (Exception e) {
			e.printStackTrace(Server.getLogger());
		} finally {
			CreamToolkit.releaseConnection(connection);
		}

		return wholesale;
	}
	
	public static void main(String[] args) {
	}
}
