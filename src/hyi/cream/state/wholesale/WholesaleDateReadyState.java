package hyi.cream.state.wholesale;

import hyi.cream.state.SomeAGReadyState;
import hyi.cream.util.CreamToolkit;

import java.util.ResourceBundle;

public class WholesaleDateReadyState extends SomeAGReadyState {
	static WholesaleDateReadyState instance;

	private ResourceBundle res = CreamToolkit.GetResource();

	public static WholesaleDateReadyState getInstance() {
		try {
			if (instance == null)
				instance = new WholesaleDateReadyState();
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	public WholesaleDateReadyState() throws InstantiationException {
	}
	
	public String getPromptedMessage() {
		// "请输入团购单日期[YMMDD]:"
		return res.getString("InputWholesaleDate"); 
	}

	@Override
	public String getWarningMessage() {
		return app.getWarningIndicator().getMessage();
	}
}
