package hyi.cream.state.wholesale;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.Cashier;
import hyi.cream.state.CashierRightsCheckState;
import hyi.cream.state.IdleState;
import hyi.cream.state.MemberState;
import hyi.cream.state.State;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;

import java.util.Collection;
import java.util.EventObject;
import java.util.ResourceBundle;

public class WholesaleIdleState extends State {
	static WholesaleIdleState instance = null;
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();

	public static WholesaleIdleState getInstance() {
		try {
			if (instance == null) {
				instance = new WholesaleIdleState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	/**
	 * Constructor
	 */
	public WholesaleIdleState() throws InstantiationException {
	}
	
	@Override
	public void entry(EventObject event, State sourceState) {
		//"1-团购销售 2-团购结算 3-代配领取 4-代配退回"
		app.getMessageIndicator().setMessage(res.getString("WholesaleSelectMessage"));
	}

	@Override
	public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");
        if (event.getSource() instanceof ClearButton) {
            app.getMessageIndicator().setMessage("");
            // init session
    		CreamSession.getInstance().setWorkingState(WorkingStateEnum.IDLE_STATE);
    		CreamSession.getInstance().removeAttributes(WorkingStateEnum.WHOLESALE_STATE);
    		CreamSession.getInstance().removeAttributes(WorkingStateEnum.WHOLESALECLEARING_STATE);
    		app.getCurrentTransaction().setDefaultState();
    		app.setReturnItemState(false);
            return IdleState.class;
        } else if (event.getSource() instanceof NumberButton) {
        	String select = ((NumberButton) event.getSource()).getNumberLabel();
        	if ("1".equals(select) || "3".equals(select) || "4".equals(select)) {
        		// 销售 
        		CreamSession.getInstance().setWorkingState(WorkingStateEnum.WHOLESALE_STATE);
        		if ("1".equals(select)) {
                    CreamSession.getInstance().setAttribute(WorkingStateEnum.WHOLESALE_STATE, "billType", "5");
                } else if ("3".equals(select)) {
        			CreamSession.getInstance().setAttribute(WorkingStateEnum.WHOLESALE_STATE, "billType", "7");
        		} else if ("4".equals(select)){
        			CreamSession.getInstance().setAttribute(WorkingStateEnum.WHOLESALE_STATE, "billType", "8");
        		}
        		return WholesaleState.class;
        	} else if ("2".equals(select)) {
        		// 结算
        		CreamSession.getInstance().setWorkingState(WorkingStateEnum.WHOLESALECLEARING_STATE);
        		return MemberState.class;
        	} else {
				app.getWarningIndicator().setMessage(res.getString("InputWrong"));
				return WholesaleIdleState.class;
			}
        }
		return sinkState.getClass();
	}

}
