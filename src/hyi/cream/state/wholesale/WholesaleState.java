package hyi.cream.state.wholesale;

import hyi.cream.CreamSession;
import hyi.cream.WorkingStateEnum;
import hyi.cream.state.SomeAGReadyState;
import hyi.cream.state.State;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;
import java.util.ResourceBundle;

public class WholesaleState extends SomeAGReadyState {
	static WholesaleState instance;

	private ResourceBundle res = CreamToolkit.GetResource();

	public static WholesaleState getInstance() {
		try {
			if (instance == null)
				instance = new WholesaleState();
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	public WholesaleState() throws InstantiationException {
	}
	
	@Override
	public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");
        if (event.getSource() instanceof ClearButton) {
            app.getMessageIndicator().setMessage("");
            // init session
    		CreamSession.getInstance().setWorkingState(WorkingStateEnum.IDLE_STATE);
    		CreamSession.getInstance().removeAttributes(WorkingStateEnum.WHOLESALE_STATE);
    		app.getCurrentTransaction().setDefaultState();
            return WholesaleIdleState.class;
        }
        return sinkState.getClass();
	}
	
	public String getPromptedMessage() {
		return res.getString("InputWholesaleNo"); 
	}

	@Override
	public String getWarningMessage() {
		return app.getWarningIndicator().getMessage();
	}
}
