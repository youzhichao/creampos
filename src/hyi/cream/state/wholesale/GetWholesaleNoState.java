// Copyright (c) 2000 HYI
package hyi.cream.state.wholesale;

import hyi.cream.CreamSession;
import hyi.cream.WorkingStateEnum;
import hyi.cream.state.GetSomeAGState;
import hyi.cream.util.CreamToolkit;

import java.util.ResourceBundle;

public class GetWholesaleNoState extends GetSomeAGState {

	private ResourceBundle res = CreamToolkit.GetResource();

	static GetWholesaleNoState instance = null;
	
	private Class ultimateSinkState = WholesaleDateReadyState.class;

	public static GetWholesaleNoState getInstance() {
		try {
			if (instance == null) {
				instance = new GetWholesaleNoState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	public GetWholesaleNoState() throws InstantiationException {
	}

	public boolean checkValidity() {
		String billType = (String)CreamSession.getInstance().getAttribute(WorkingStateEnum.WHOLESALE_STATE, "billType");
		String str = getAlphanumericData();
		if (str == null) {
			//"单号输入错误"
			setWarningMessage(res.getString("WholesaleWaring2"));
			return false;
		} else if (str.length() == 0) {
			return false;
		} else if ("7".equals(billType)){
			// 这里还可以加入更多的校验条件，不过太多会限制得太死
			// 认为是完整的单号，不用去输入日期画面
			CreamSession.getInstance().setAttribute(
					WorkingStateEnum.WHOLESALE_STATE, "BillNo", str);
			ultimateSinkState = ListWholesaleState.class;
			return true;
		} else if ("8".equals(billType)){
			// 这里还可以加入更多的校验条件，不过太多会限制得太死
			// 认为是完整的单号，不用去输入日期画面
			CreamSession.getInstance().setAttribute(
					WorkingStateEnum.WHOLESALE_STATE, "BillNo", str);
			ultimateSinkState = ListWholesaleState.class;
			return true;
		} else if (str.length() > GetWholesaleDateState.WHOLESALE_SEQ_NO_LEN) {
			//"输入的单号不能大于4位"
			setWarningMessage(res.getString("WholesaleWaring1"));
			return false;
		} else {
			// 去输入日期画面
			CreamSession.getInstance().setAttribute(
					WorkingStateEnum.WHOLESALE_STATE, "BillSeq", str);
			ultimateSinkState = WholesaleDateReadyState.class;
			return true;
		}
		
	}

	public Class getUltimateSinkState() {
		return ultimateSinkState;
	}

	public Class getInnerInitialState() {
		return WholesaleState.class;
	}
}
