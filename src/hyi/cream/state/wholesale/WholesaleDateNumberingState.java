
// Copyright (c) 2000 HYI
package hyi.cream.state.wholesale;

import hyi.cream.state.SomeAGNumberingState;

/**
 * A Class class.
 * <P>
 */
public class WholesaleDateNumberingState extends SomeAGNumberingState {
    static WholesaleDateNumberingState instance = null;

    public static WholesaleDateNumberingState getInstance() {
        try {
            if (instance == null) {
                instance = new WholesaleDateNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    /**
     * Constructor
     */
    public WholesaleDateNumberingState() throws InstantiationException {
    }
    
}

 