// Copyright (c) 2000 HYI
package hyi.cream.state.wholesale;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.state.GetSomeAGState;
import hyi.cream.util.CreamToolkit;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

public class GetWholesaleDateState extends GetSomeAGState {

	private ResourceBundle res = CreamToolkit.GetResource();

	static GetWholesaleDateState instance = null;
	private SimpleDateFormat yMMdd = new SimpleDateFormat("yMMdd");
	public static final int WHOLESALE_DATE_LEN = 5;
	public static final int WHOLESALE_SEQ_NO_LEN = 4;

	public static GetWholesaleDateState getInstance() {
		try {
			if (instance == null) {
				instance = new GetWholesaleDateState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	public GetWholesaleDateState() throws InstantiationException {
	}

	public boolean checkValidity() {
		String dateStr = getAlphanumericData();
		if (dateStr == null) {
			setWarningMessage(res.getString("WholesaleWaring3"));
			return false;
		} else if (dateStr.length() == 0) {
			dateStr = yMMdd.format(new Date());
		} else if (dateStr.length() != WHOLESALE_DATE_LEN) {
			setWarningMessage(res.getString("WholesaleWaring3"));
			return false;
		}
		
		// 补全团购单号
		String billSeq = (String) CreamSession.getInstance().getAttribute(
				WorkingStateEnum.WHOLESALE_STATE, "BillSeq");
		String billNo = genWholesaleBillNo(dateStr, billSeq);
		CreamSession.getInstance().setAttribute(
				WorkingStateEnum.WHOLESALE_STATE, "BillNo", billNo);
		return true;
	}

	public Class getUltimateSinkState() {
		return ListWholesaleState.class;
	}

	public Class getInnerInitialState() {
		return WholesaleState.class;
	}
	
	private String genWholesaleBillNo(String dateStr, String no) {
		if (dateStr.length() > WHOLESALE_DATE_LEN) {
			dateStr = dateStr.substring(dateStr.length() - WHOLESALE_DATE_LEN, dateStr.length());
		}
		String billNo = "";
		while (no.length() < WHOLESALE_SEQ_NO_LEN)
			no = "0" + no;
		billNo = POSTerminalApplication.getInstance().getCurrentTransaction().getStoreNumber() 
				+ "TG" + dateStr + no;
		return billNo;
		
	}
}
