package hyi.cream.state.wholesale;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.dac.MemberCardBase;
import hyi.cream.dac.Transaction;
import hyi.cream.state.DrawerOpenConfirmState;
import hyi.cream.state.ShowMemberInfoState;
import hyi.cream.state.State;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.ResourceBundle;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class WholesaleClearingState extends State {
    static WholesaleClearingState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    
    public static WholesaleClearingState getInstance() {
        try {
            if (instance == null) {
                instance = new WholesaleClearingState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    /**
     * Constructor
     */
    public WholesaleClearingState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("CashInIdleState entry");
        if (event == null) {
            return;
        }           
        
// modify lxf //2003.03.03
//        if (sourceState instanceof KeyLock1State
//            && event.getSource() instanceof SelectButton) {

//        Collection fds = Cashier.getExistedFieldList("cashier");
//        
//        if ((!fds.contains("CASRIGHTS") || sourceState instanceof CashierRightsCheckState)
//            && event.getSource() instanceof EnterButton) {
		app.getMessageIndicator().setMessage("");
		Transaction trans = app.getCurrentTransaction();
        if (sourceState instanceof ShowMemberInfoState) {
			trans.setDealType2("Q");
			trans.setState1("Q");
			app.getMessageIndicator().setMessage(
					res.getString("WholesaleMessage1"));
			// app.getWarningIndicator().setMessage("");
			app.getItemList().setVisible(false);
			app.getPayingPane().setTransaction(trans);
			app.getPayingPane1().setTransaction(trans);
			app.getPayingPane2().setTransaction(trans);
			app.getDyanPayingPane().setTransaction(trans);
			MemberCardBase mcb = (MemberCardBase) CreamSession.getInstance()
					.getAttribute(WorkingStateEnum.WHOLESALECLEARING_STATE,
							"Member");
			trans.setOriginEarnAmount(mcb.getEarningamt() == null ? new HYIDouble(
					0) : mcb.getEarningamt());
			
			// 显示画面
			// app.getPayingPane().setMode(1);
			app.setDynaPayingPaneVisible(true);
			List headers = new ArrayList();
			List fields = new ArrayList();
			List types = new ArrayList();
			if (trans.getOriginEarnAmount() != null && trans.getOriginEarnAmount().doubleValue() <= 0){
				headers.add(res.getObject("PrePayMessage"));
				fields.add("OriginEarnAmountAbs");
			} else {
				headers.add(res.getObject("SheMessage"));
				fields.add("OriginEarnAmount");
			}
			headers.add(res.getObject("Balance"));
			fields.add("SheBalance");
			types.add("y");
			types.add("y");
			app.getDyanPayingPane().setDisplayContent(headers, fields, types);

			app.getPayingPane().setVisible(true);
		}
        
        app.getPayingPane().forceRepaint();//.repaint();
		CreamToolkit.showText(app.getCurrentTransaction(), 1);

//        app.getMessageIndicator().setMessage(res.getString("CashInPriceMessage"));
    }

    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");

        // init newCashierID
        if (event.getSource() instanceof ClearButton) {
            Transaction trans = app.getCurrentTransaction();
        	trans.init();
			trans.clearRoundDownDetail();
			trans.initPaymentInfo();

			app.getPayingPane().setVisible(false);
			app.getPayingPane().clear();
			app.getItemList().setVisible(true);

        	app.getMessageIndicator().setMessage("");
            app.getWarningIndicator().setMessage("");
        } else if (event.getSource() instanceof NumberButton) {
//			return Numbering2State.class;
		} else if (event.getSource() instanceof EnterButton) {
//            if (trans.getLineItems().length == 0) {
//                return WholesaleClearingState.class;
//            }

            //  set to transaction
		    Transaction trans = app.getCurrentTransaction();
            trans.setGrossSalesAmount(new HYIDouble(0));
            trans.setNetSalesAmount(new HYIDouble(0));
            trans.setNetSalesAmount0(new HYIDouble(0));
            trans.setNetSalesAmount1(new HYIDouble(0));
            trans.setNetSalesAmount2(new HYIDouble(0));
            trans.setNetSalesAmount3(new HYIDouble(0));
            trans.setNetSalesAmount4(new HYIDouble(0));
            trans.setNetSalesAmount5(new HYIDouble(0));
            trans.setNetSalesAmount6(new HYIDouble(0));
            trans.setNetSalesAmount7(new HYIDouble(0));
            trans.setNetSalesAmount8(new HYIDouble(0));
            trans.setNetSalesAmount9(new HYIDouble(0));
            trans.setNetSalesAmount10(new HYIDouble(0));
            trans.setMixAndMatchAmount0(new HYIDouble(0));
            trans.setMixAndMatchAmount1(new HYIDouble(0));
            trans.setMixAndMatchAmount2(new HYIDouble(0));
            trans.setMixAndMatchAmount3(new HYIDouble(0));
            trans.setMixAndMatchAmount4(new HYIDouble(0));
            trans.setMixAndMatchAmount5(new HYIDouble(0));
            trans.setMixAndMatchAmount6(new HYIDouble(0));
            trans.setMixAndMatchAmount7(new HYIDouble(0));
            trans.setMixAndMatchAmount8(new HYIDouble(0));
            trans.setMixAndMatchAmount9(new HYIDouble(0));
            trans.setMixAndMatchAmount10(new HYIDouble(0));
            trans.setMixAndMatchCount0(0);
            trans.setMixAndMatchCount1(0);
            trans.setMixAndMatchCount2(0);
            trans.setMixAndMatchCount3(0);
            trans.setMixAndMatchCount4(0);   
            trans.setMixAndMatchCount5(0);
            trans.setMixAndMatchCount6(0);
            trans.setMixAndMatchCount7(0);
            trans.setMixAndMatchCount8(0);
            trans.setMixAndMatchCount9(0);
            trans.setMixAndMatchCount10(0);
            trans.clearMMDetail();

            app.getMessageIndicator().setMessage(res.getString("TransactionEnd"));
            app.getWarningIndicator().setMessage(""); 
            //app.getPayingPane().setMode(0);
            app.getPayingPane().clear();

            return DrawerOpenConfirmState.class;
        } else {
            return WholesaleClearingState.class;
        }
        return sinkState.getClass();
    }
}

