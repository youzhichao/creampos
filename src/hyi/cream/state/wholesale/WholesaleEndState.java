package hyi.cream.state.wholesale;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Transaction;
import hyi.cream.state.DrawerOpenState;
import hyi.cream.state.GetProperty;
import hyi.cream.state.State;
import hyi.cream.state.SummaryState;
import hyi.cream.uibeans.AgeLevelButton;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;
import java.util.ResourceBundle;

public class WholesaleEndState extends State {
	static WholesaleEndState instance = null;
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();

	public static WholesaleEndState getInstance() {
		try {
			if (instance == null) {
				instance = new WholesaleEndState();
			}
		} catch (InstantiationException ex) {
		}
		return instance;
	}

	/**
	 * Constructor
	 */
	public WholesaleEndState() throws InstantiationException {
	}
	
	@Override
	public void entry(EventObject event, State sourceState) {
		//"按[结帐]键结帐,按[清除]键返回!"
		app.getMessageIndicator().setMessage(res.getString("WholesaleMessage"));
	}

	@Override
	public Class exit(EventObject event, State sinkState) {
	    Transaction curTran = app.getCurrentTransaction();
		if (event.getSource() instanceof AgeLevelButton) {
			String state1 = curTran.getState1(); 
			if ("T".equals(state1) || "B".equals(state1)) {
//				DbConnection connection = null;
//				try {
//					connection = CreamToolkit.getPooledConnection();
//					CreamPrinter printer = CreamPrinter.getInstance();
//	                if (!printer.getHeaderPrinted()) {
//	                    if (!printer.isPrinterAtRightStart()) { // 檢查印表機是否已定位
//	                        setWarningMessage(CreamToolkit.GetResource().getString("PrinterIsNotAtRightPosition"));
//	                        PrintPluWarningState.setExitState(PeriodicalNoReadyState.class);
//	                        return PrintPluWarningState.class;
//	                    }
//	                    printer.printHeader(connection);
//	                    printer.setHeaderPrinted(true);
//	                }
//					for (Object lineItemObj : curTran.getLineItems()) {
//						LineItem lineItem = (LineItem) lineItemObj;
//						if (!lineItem.getPrinted())
//							printer.printLineItem(connection, lineItem);
//					}
//				} catch (SQLException e) {
//					CreamToolkit.logMessage(e);
//					CreamToolkit.haltSystemOnDatabaseFatalError();
//					return PeriodicalNoReadyState.class;
//				} finally {
//					CreamToolkit.releaseConnection(connection);
//				}

				return SummaryState.class;
			} else if ("P".equals(state1) || "D".equals(state1)) {
				if (GetProperty.getIsNeedNotPayShowPayingPane()){
					// Bruce/2003-12-24
					curTran.saveOriginalLineItemAfterDiscountAmount();
					
					curTran.setChangeAmount(new HYIDouble(0));
					// app.getLineItemIndicator().setVisible(false);
					curTran.setPayCash(new HYIDouble(0));
//				app.getPayingPane().setTransaction(curTran);
//				app.getPayingPane1().setTransaction(curTran);
//				app.getPayingPane2().setTransaction(curTran);
					app.getPayingPane().setTransaction(null);
					app.getPayingPane1().setTransaction(null);
					app.getPayingPane2().setTransaction(null);
					// Panel p = app.getItemListPanel();
					app.getItemList().setVisible(false);
					app.getPayingPane().setVisible(true);
				}
				return DrawerOpenState.class;
			}
		} else if (event.getSource() instanceof ClearButton) {
			curTran.clearLineItem();
			curTran.setDefaultState();

		}
		return sinkState.getClass();
	}
}
