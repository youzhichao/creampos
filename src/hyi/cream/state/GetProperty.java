package hyi.cream.state;

import hyi.cream.util.CreamPropertyUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * ////把属性方法化，以便于集中管理属性.
 * ////命名规则: 1.首字母大写
 * ////取值规则: 1.开关: yes/no/true/false;   2.列表 a:b:c:d 分隔符用":"(英文半角字符)
 * ////默认取值: 1.以合理为原则，优先考虑不更新原有客户
 * </pre>
 *
 * (Bruce/2009-03-19)這個class放這兩類參數：<p/>
 * 1. Inline server: 取得在cream.conf中的參數<br/>
 * 2. POS: 想集中管理但目前不太會變化的參數, 且目前這些參數沒有放在property表中
 *
 * @author ll, Bruce
 * @since 2005.10.31
 */
final public class GetProperty {

    private static String getProperty(String name, String defaultValue) {
        return CreamPropertyUtil.getInstance().getProperty(name, defaultValue);
    }

    private static boolean getProperty(String name, boolean defaultValue) {
        String stringValue = CreamPropertyUtil.getInstance().getProperty(name,
            defaultValue ? "yes" : "no");
        return stringValue.equalsIgnoreCase("yes") || stringValue.equalsIgnoreCase("true");
    }

    ///////////////////////////////////////////////////////////////////////////////////
    ///// PROPERTIES USED IN INLINE SERVER ////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////

    public static String getLocale() {
        return getProperty("Locale", "zh_TW");
    }

    public static String getEnableMessageDownload(String defaultValue) {
        return getProperty("EnableMessageDownload", defaultValue);
    }

    // 是否下plu.minprice && 是否允许打折时低于最低价
    public static String getDownloadMinPrice(String defaultValue) {
        return getProperty("DownloadMinPrice", defaultValue);
    }

    public static String getOverrideVersion(String defaultValue) {
        return getProperty("OverrideVersion", defaultValue);
    }

    public static String getAccountingDateShiftTime(String defaultValue) {
        // used by inline server
        return getProperty("AccountingDateShiftTime", defaultValue);
    }

    public static String getInlineServerLogFile() {
        return getProperty("InlineServerLogFile", "inline.log");
    }

    /**
     * InlineLongTimeout Inline client/server 上下傳的long timeout(秒)，
     * 在等候可能會有較長時間的response時使用。
     */
    public static int getInlineLongTimeout() {
        try {
            String s = getProperty("InlineLongTimeout", "360");
            return Integer.parseInt(s);
        } catch (Exception e) {
            e.printStackTrace();
            return 360;
        }
    }

    public static long getInlineInsertTranFromXmlTime(String defaultValue) {
        long i= Long.parseLong(defaultValue);
        String s = getProperty("InlineInsertTranFromXmlTime", defaultValue);
        try {
            i = Long.parseLong(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }

    public static String getDebugInlineServer(String defaultValue) {
        return getProperty("DebugInlineServer", defaultValue);
    }

    public static String getUpdateInventory(String defaultValue) {
        return getProperty("UpdateInventory", defaultValue);
    }

    public static String getMaxThreadCount(String defaultValue) {
        return getProperty("MaxThreadCount", defaultValue);
    }

    public static String getTransactionCheckerThread(String defaultValue) {
        return getProperty("TransactionCheckerThread", defaultValue);
    }

    public static String getCacheDownloadFile(String defaultValue) {
        return getProperty("CacheDownloadFile", defaultValue);
    }

    public static String getConnectionStatusFile(String defaultValue) {
        return getProperty("ConnectionStatusFile", defaultValue);
    }

    public static String getPostProcessor(String defaultValue) {
        return getProperty("PostProcessor", defaultValue);
    }

    /**
     * 购物袋计入的支付id，默认为"其他金种"
     * defaultValue 29
     * @return
     */
    public static String getBagAmtPaymentID() {
        return getProperty("BagAmtPaymentID", "29");
    }

    /**
     * 配送费计入的支付id，默认为"其他金种"
     * defaultValue 29
     * @return
     */
    public static String getSendFeeAmtPaymentID() {
        return getProperty("SendFeeAmtPaymentID", "29");
    }

    /**
     * 是否移动购物袋到支付其他金种别
     * @return
     * at inline server PostProcessorImplA
     */
    public static boolean isMoveOutBagAmountToOtherPayment() {
        return getProperty("MoveOutBagAmountToOtherPayment", false);
    }

    /**
     * 是否移动配送费到支付其他金种别
     * @return
     * at inline server PostProcessorImplA
     */
    public static boolean isMoveOutSendFeeToOtherPayment() {
        return getProperty("MoveOutSendFeeToOtherPayment", false);
    }

    /**
     * 是否把溢收金额现金日报中从新加会到各种支付方式中
     * PostProcessorImplA
     */
    public static boolean isOverAmtAddBackToPaymentInAccdayrpt() {
        return getProperty("OverAmtAddBackToPaymentInAccdayrpt", true);
    }

    /**
     * 團購特殊商品no.
     */
    public static String getWholesaleSpecialPluno() {
        return getProperty("WholesaleSpecialPluno", "999999");
    }
    public static String getAlipayURL() {
        return getProperty("AlipayURL", "https://mapi.alipay.com/gateway.do?");
    }

    //移动支付url
    public static String getCMPAYAddress() {
        return getProperty("CMPAYAddress","http://211.136.101.107:7008/CMPay/CMPAYProcess");
    }

    //移动支付商户号
    public static String getCMPAYMerId() {
        return getProperty("CMPAYMerId","888000959990013");
    }
    /**
     * 如果POS端的門市代號與server-side不同，是否拒絕連線。
     */
    public static boolean isCheckStoreID() {
        return getProperty("CheckStoreID", false);
    }

    ///////////////////////////////////////////////////////////////////////////////////
    ///// PROPERTIES WITH FIXED DEFAULT VALUE /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////

    public static String getLogFile() {
        return "cream.log";
    }

    public static String getInlineClientLogFile() {
        return "inline.log";
    }

    public static String getMixAndMatchQuick() {
        return "no";
    }

    public static String getConfFileLocale() {
        return "UTF-8";
    }

    /**
     * 弹出触屏的版本
     * version=1 // 从cat.ISTOUCH属性判断哪些plu可以显示在触屏上
     * version=2 // 从touchbutton.conf中得到哪些button可以显示在触屏上
     *
     * @param defaultValue
     * @return
     */
    public static String getTouchMenuVersion() {
        return "2";
    }

    /**
     * 第二件商品促销id,对应combination_discount_type.cdt_id
     *
     * @param defaultValue
     * @return
     */
    public static String getSecondPLUPromotionID() {
        return "9";
    }

    /**
     * 是否开启CombPromotionHeader的cache功能 默认=true
     *
     * @param defaultValue
     * @return
     */
    public static String getCombPromotionHeaderUserCache() {
        return "true";
    }

    /**
     * 找零对应的支付id，默认为现金 00
     * @param defaultValue 00
     * @return
     */
    public static String getChangeAmtPaymentID() {
        return "00";
    }

    /**
     * 是否生成TouchPanel和其相关按键。//**如果Disable，可以减少import Swing相关classes/interfaces。**
     */
    public static boolean isEnableTouchPanel() {
        return true;
    }

    /**
     * 不需要支付时，是否显示支付画面（空白的，不包含任何支付信息）
     * 例如：开票单，代配领取单
     * @return
     */
    public static boolean getIsNeedNotPayShowPayingPane() {
        return true;
    }

    /**
     * 允许的配达传票类型类型
     * PostProcessorAdapter, Transaction
     * inline server: cream.conf,  db.cream.property
     * @return
     */
    public static List<String> getAnnotatedTypeList() {
        List<String> ret = new ArrayList<String>();
        ret.add("H");
        ret.add("P");
        ret.add("M");
        ret.add("O");
        return ret;
    }

    /**
     * 批发销售传票类型
     * @return
     */
    public static String getAnnotatedTypeWholesale() {
        return "H";
    }

    /**
     * 对printer打印内容做电子存根的文件的路径和文件名
     * @return
     */
    public static String getPrinterRecordFile() {
        return "log/invoice.log";
    }


///// OBSOLETE PROPERTIES /////////////////////////////////////////////////////////
//
//    // AdPanel.java
//    public static String getkxX1(String defaultValue) {
//        return getProperty("kxX1", defaultValue);
//    }
//
//    public static String getkxY1(String defaultValue) {
//        return getProperty("kxY1", defaultValue);
//    }
//
//    public static String getkxLen(String defaultValue) {
//        return getProperty("kxLen", defaultValue);
//    }
//
//    public static String getkxFont(String defaultValue) {
//        return getProperty("kxFont", defaultValue);
//    }
//
//    public static String getkxFontSize(String defaultValue) {
//        return getProperty("kxFontSize", defaultValue);
//    }
//
//    // 媒体文件存放的路径
//    public static String getAdDir(String defaultValue) {
//        return getProperty("AdDir", defaultValue);
//    }

//    public static String getCreateAdPanel(String defaultValue) {
//        return getProperty("CreateAdPanel", defaultValue);
//    }

//    public static String getTwoScreen(String defaultValue) {
//        return getProperty("TwoScreen", defaultValue);
//    }

//    public static String getposX(String defaultValue) {
//        return getProperty("posX", defaultValue);
//    }
//
//    public static String getposY(String defaultValue) {
//        return getProperty("posY", defaultValue);
//    }

//    public static String getLicenseInfo(String defaultValue) {
//        return getProperty("LicenseInfo", defaultValue);
//    }

//    // dac.DacBase.java
//    public static String getFlushOSBuffer(String defaultValue) {
//        return getProperty("FlushOSBuffer", defaultValue);
//    }

//    public static String getMySQLDataDirectory(String defaultValue) {
//        return getProperty("MySQLDataDirectory", defaultValue);
//    }
//
//    public static String getMySQLBackupDirectory(String defaultValue) {
//        return getProperty("MySQLBackupDirectory", defaultValue);
//    }

    // public static String getDatabaseURL(String defaultValue) {
    // return databaseURL == null ? defaultValue : databaseURL;
    // }

//    public static String getInlineClientUseJVM(String defaultValue) {
//        return getProperty("InlineClientUseJVM", defaultValue);
//    }

//    // dac.DacTransfer.java
//    public static String getMYSQL_BASE(String defaultValue) {
//        return getProperty("MYSQL_BASE", defaultValue);
//    }

//    // dac.Message
//    public static String getDBType(String defaultValue) {
//        return getProperty("DBType", defaultValue);
//    }

//    public static String getSCDBType(String defaultValue) {
//        return getProperty("SCDBType", defaultValue);
//    }

//    public static String getUndoDirectory(String defaultValue) {
//        return getProperty("UndoDirectory", defaultValue);
//    }
//
//    public static String getCrashStep(String defaultValue) {
//        return getProperty("CrashStep", defaultValue);
//    }
//
//    public static String getNeedUndo(String defaultValue) {
//        return getProperty("NeedUndo", defaultValue);
//    }

//    public static String getExcludeRebateInSum(String defaultValue) {
//        return getProperty("ExcludeRebateInSum", defaultValue);
//    }

//    public static String getCrdPaymentID(String defaultValue) {
//        return getProperty("CrdPaymentID", defaultValue);
//    }

//    public static String getMySQLRemoteBackupDirectory(String defaultValue) {
//        return getProperty("MySQLRemoteBackupDirectory", defaultValue);
//    }

//    public static String getRuOkInterval(String defaultValue) {
//        return getProperty("RuOkInterval", defaultValue);
//    }

//    // inline.PostProcessorAdapter.java
//    public static String getStoreID(String defaultValue) {
//        return getProperty("StoreID", defaultValue);
//    }

//    // state.CashierRightsCheckState
//    public static String getReturnableLevel(String defaultValue) {
//        return getProperty("ReturnableLevel", defaultValue);
//    }

//    // state.DiscountAmountState
//    public static String getDiscountAmountOrOriginalPrice(String defaultValue) {
//        return getProperty("DiscountAmountOrOriginalPrice", defaultValue);
//    }

//    // state.DIYOverrideAmountState
//    public static String getMaxDIYDiscountRate(String defaultValue) {
//        return getProperty("MaxDIYDiscountRate", defaultValue);
//    }

//    public static String getSalesCanNotEnd(String defaultValue) {
//        return getProperty("SalesCanNotEnd", defaultValue);
//    }

//    // state.OVerrideAmountState
//    public static String getComputeDiscountRate(String defaultValue) {
//        return getProperty("ComputeDiscountRate", defaultValue);
//    }

//    // state.SetPropertyState
//    public static String getproperty(String defaultValue) {
//        return getProperty("property", defaultValue);
//    }

//    // state.StateMachine
//    public static String getCheckPrinterPower(String defaultValue) {
//        return getProperty("CheckPrinterPower", defaultValue);
//    }

//    public static String getShowAmountWithScale2(String defaultValue) {
//        return getProperty("ShowAmountWithScale2", defaultValue);
//    }

//    // uibean.PasswordDialog
//    public static String getPasswordDialogFont(String defaultValue) {
//        return getProperty("PasswordDialogFont", defaultValue);
//    }

//    /**
//     * state.ReadCrdNoState
//     * international credit card 16
//     * max 20 : it is the max length of field of tranhead.
//     * @return
//     */
//    public static int getCrdNoLength() {
//        return 20;
//    }

//    // util.CreamPrinter_zh_CN.java
//    public static String getBottomLogo1(String defaultValue) {
//        return getProperty("BottomLogo1", defaultValue);
//    }

//    public static String getBottomLogo2(String defaultValue) {
//        return getProperty("BottomLogo2", defaultValue);
//    }

//    public static String getTopLogo1(String defaultValue) {
//        return getProperty("TopLogo1", defaultValue);
//    }

//    public static String getTopLogo2(String defaultValue) {
//        return getProperty("TopLogo2", defaultValue);
//    }

//    public static String getPrinterType(String defaultValue) {
//        return getProperty("PrinterType", defaultValue);
//    }

    // Bruce/20070314/ Remove it.
    // public static String getPrint_Started(String defaultValue) {
    // return getProperty("Print_Started", defaultValue);
    // }

//    // util.CreamToolkit
//    public static String getServerDatabaseDriver(String defaultValue) {
//        return getProperty("ServerDatabaseDriver", defaultValue);
//    }

//    public static String getmaxconns(String defaultValue) {
//        return getProperty("maxconns", defaultValue);
//    }

//    public static String getlogintimeout(String defaultValue) {
//        return getProperty("logintimeout", defaultValue);
//    }

//    public static String getServerDatabaseUser(String defaultValue) {
//        return getProperty("ServerDatabaseUser", defaultValue);
//    }

//    public static String getServerDatabaseUserPassword(String defaultValue) {
//        return getProperty("ServerDatabaseUserPassword", defaultValue);
//    }

//    public static String getServerDatabaseURL(String defaultValue) {
//        return getProperty("ServerDatabaseURL", defaultValue);
//    }

    // public static String getDatabaseDriver(String defaultValue) {
    // return databaseDriver == null ? defaultValue : databaseDriver;
    // }
    //
    // public static String getDatabaseUser(String defaultValue) {
    // return databaseUser == null ? defaultValue : databaseUser;
    // }
    //
    // public static String getDatabaseUserPassword(String defaultValue) {
    // return databaseUserPassword == null ? defaultValue : databaseUserPassword;
    // }

//    public static String getServerDatabaseInitialConnections(String defaultValue) {
//        return getProperty("ServerDatabaseInitialConnections", defaultValue);
//    }

//    public static String getDatabaseInitialConnections(String defaultValue) {
//        return getProperty("DatabaseInitialConnections", defaultValue);
//    }

//    public static String getConfFileLocale() {
//        return getProperty("ConfFileLocale", "UTF-8");
//    }

//    public static String getLineDisplay(String defaultValue) {
//        return getProperty("LineDisplay", defaultValue);
//    }

//    public static String getDatabaseConnectionIncrement(String defaultValue) {
//        return getProperty("DatabaseConnectionIncrement", defaultValue);
//    }

//    // service.ARKScanner.java
//    public static String getScannerPrefixCharValue(String defaultValue) {
//        return getProperty("ScannerPrefixCharValue", defaultValue);
//    }

//Bruce/20081203> Use "ActionAfterMasterDownload" instead.
//    /**
//     * 下传主档后是否需要重起pos机（不管程序是否更新）
//     *
//     * @param defaultValue
//     * @return
//     */
//    public static String getRebootAfterDownMaster(String defaultValue) {
//        return getProperty("RebootAfterDownMaster", defaultValue);
//    }

//    public static String getMainScreenX(String defaultValue) {
//        return getProperty("MainScreenX", defaultValue);
//    }

//    public static String getCustomerScreenX(String defaultValue) {
//        return getProperty("CustomerScreenX", defaultValue);
//    }

//    public static String getTopTitleHeight(String defaultValue) {
//        return getProperty("TopTitleHeight", defaultValue);
//    }

//    public static String getVideoPath(String defaultValue) {
//        return getProperty("VideoPath", defaultValue);
//    }

//    public static String getVideoEnable(String defaultValue) {
//        return getProperty("VideoEnable", defaultValue);
//    }

//    /**
//     * 是否需要在shift,z是check sc 是否漏交易
//     *
//     * @param defaultValue
//     * @return
//     */
//    public static String getNeedSynTranAtShift(String defaultValue) {
//        return getProperty("NeedSynTranAtShift", defaultValue);
//    }

//    /**
//     * getUseTouchButtonInScreen 是否显示touch button 默认为no
//     *
//     * @param defaultValue
//     * @return
//     */
//    public static String getUseTouchButtonInScreen(String defaultValue) {
//        return getProperty("UseTouchButtonInScreen", defaultValue);
//    }

//    public static String getTouchButtonXCount(String defaultValue) {
//        return getProperty("TouchButtonXCount", defaultValue);
//    }
//
//    public static String getTouchButtonYCount(String defaultValue) {
//        return getProperty("TouchButtonYCount", defaultValue);
//    }

//    // uibean.ScreenButton
//    public static String getTouchButtonFont(String defaultValue) {
//        return getProperty("TouchButtonFont", defaultValue);
//    }

//    public static String getTouchButtonFontSize(String defaultValue) {
//        return getProperty("TouchButtonFontSize", defaultValue);
//    }

//    public static String getPrinterCutPaperType(String defaultValue) {
//        return getProperty("PrinterCutPaperType", defaultValue);
//    }

    //Bruce/20080903/ Remove it.
//  public static String getIsGenHistoryTrans(String defaultValue) {
//      return getProperty("IsGenHistoryTrans", defaultValue);
//  }

//    public static String getTopBitmapName(String defaultValue) {
//        return getProperty("TopBitmapName", defaultValue);
//    }

//    public static String getBottomBitmapName(String defaultValue) {
//        return getProperty("BottomBitmapName", defaultValue);
//    }

//    public static String getHistoryTransSendPreCount(String defaultValue) {
//        return getProperty("HistoryTransSendPreCount", defaultValue);
//    }

//    public static String getDepSalesPrintBlank(String defaultValue) {
//        return getProperty("DepSalesPrintBlank", defaultValue);
//    }

//    public static String getNeedJudgeAccdate(String defaultValue) {
//        return getProperty("NeedJudgeAccdate", defaultValue);
//    }

//    /**
//     * 历史交易打印，是否需要切纸（多个交易之间） 默认＝false（不切纸）
//     *
//     * @param defaultValue
//     * @return
//     */
//    public static String getIsHisTransPrintNeedCut(String defaultValue) {
//        return getProperty("IsHisTransPrintNeedCut", defaultValue);
//    }

//    public static String getReceiptConfName(String defaultValue) {
//        return getProperty("ReceiptConfName", defaultValue);
//    }

//    /**
//     * 打印z,shift等报表时是否需要换空白纸,用于小票是发票的客户 default=no
//     *
//     * @param defaultValue
//     * @return
//     */
//    public static String getNeedChangePaper(String defaultValue) {
//        return getProperty("NeedChangePaper", defaultValue);
//    }

//    public static String getDatabaseURL(String string) {
//        return null;
//    }

//    /**
//     * 每张小票(发票)最多打印的明细笔数
//     *
//     * @param defaultValue
//     * @return
//     */
//    public static String getPrintLineItemPerPage(String defaultValue) {
//        return getProperty("PrintLineItemPerPage", defaultValue);
//    }

//    /**
//     * 退货调整单品
//     * @param defaultValue
//     * @return
//     */
//    public static String getReturnAdjustItemNO(String defaultValue) {
//        return getProperty("ReturnAdjustItemNO", defaultValue);
//    }

//    /**
//     * 传票条码前缀
//     * @param defaultValue
//     * @return
//     */
//    public static String getPeiDaPrefixCharValue(String defaultValue) {
//        return getProperty("PeiDaPrefixCharValue", defaultValue);
//    }

//    public static String getDepUse(String defaultValue) {
//        return getProperty("ReturnVersion", defaultValue);
//    }

//    public static String getInvoiceIsZeroMsg(String defaultValue) {
//        return getProperty("InvoiceIsZeroMsg", defaultValue);
//    }

//    /**
//     * 是否需要退货调整功能
//     * @param defaultValue
//     * @return
//     */
//    public static String getNeedReturnAdjust(String defaultValue) {
//        return getProperty("NeedReturnAdjust", defaultValue);
//    }

//    /**
//     * 赊账支付id
//     * @param defaultValue
//     * @return
//     */
//    public static String getShePaymentID(String defaultValue) {
//        return getProperty("ShePaymentID", defaultValue);
//    }

//    /**
//     * 会员版本
//     * @param defaultValue
//     * @return
//     */
//    public static String getMemberVersion(String defaultValue) {
//        return getProperty("MemberVersion", defaultValue);
//    }

//    /**
//     * 对非正常plu，需要根据分类去猜测所属depID并填写depsales,
//     * 1. 按照小分类
//     * 2. 按照大分类
//     * 3. 依次安装大中小，大中，大 查找depsales中可用的depID
//     * @return
//     */
//    public static String getCatNoToDepIdStrategy() {
//        return getProperty("CatNoToDepIdStrategy", "3");
//    }

//    /**
//     * 限量促销每笔交易限制的数量
//     * 默认 1
//     * @return
//     */
//    public static int getLimitQtySoldQty() {
//        try {
//            return Integer.parseInt(getProperty("LimitQtySoldQty", "1"));
//        } catch (NumberFormatException e) {
//            return 1;
//        }
//    }

//    /**
//     * 是否允许赊账支付
//     * @param defaultValue
//     * @return
//     */
//    public static boolean getAllowShePayment(String defaultValue) {
//        return getProperty("AllowShePayment", true);
//    }

//    /**
//     * 赊账支付id
//     * @return
//     * default -
//     */
//    public static String getShePaymentID() {
//        return getProperty("ShePaymentID", "-");
//    }

//    /**
//     * If forbid She Payment for normal transaction
//     * 默认：no
//     * yes 除了配達，其他交易都不允許賒帳支付交易
//     */
//    public static boolean getIsForbidShePayment() {
//        return getProperty("IsForbidShePayment", false);
//    }


}