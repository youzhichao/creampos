/*
 * Created on 2004-10-22
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package hyi.cream.state;

import java.util.EventObject;
import java.util.ResourceBundle;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
/**
 * @author ll
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ReadPhoneCardNumberState extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private String tempCardNumber = "";
	static ReadPhoneCardNumberState readPhoneCardNumber = null;
    private LineItem curLineItem = null;
    private static ResourceBundle res = CreamToolkit.GetResource();

    public static ReadPhoneCardNumberState getInstance() {
        try {
            if (readPhoneCardNumber == null) {
            	readPhoneCardNumber = new ReadPhoneCardNumberState();
            }
        } catch (InstantiationException ex) {
        }
        return readPhoneCardNumber;
    }
	
	public ReadPhoneCardNumberState() throws InstantiationException {
    }
	
	public void entry(EventObject event, State sourceState) {
	    Transaction trans = app.getCurrentTransaction();
		curLineItem = trans.getCurrentLineItem();
        if (sourceState instanceof DaiShou3ReadyState)
			app.getMessageIndicator().setMessage( res.getString("InputPhoneCardNumber"));
        //if (sourceState instanceof Validate)
        //    app.getMessageIndicator().setMessage("请输入价格，按[回车]键结束");
	}
	
	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof NumberButton) {
			NumberButton pb = (NumberButton)event.getSource();
                //System.out.println(" OverrideAmountState exit number" + pb.getNumberLabel());
			if (pb.getNumberLabel().equals("-") || (pb.getNumberLabel().equals(".") && tempCardNumber.length() == 0)) {
                //System.out.println(" OverrideAmountState exit number 1 " +tempPrice);
			} else { 
				tempCardNumber = tempCardNumber + pb.getNumberLabel();
                //System.out.println(" OverrideAmountState exit number 2" +tempPrice);
			}
			app.getMessageIndicator().setMessage(tempCardNumber);
			return ReadPhoneCardNumberState.class;
		}

        if (event.getSource() instanceof ClearButton) {
            if (tempCardNumber.length() == 0)
                return IdleState.class;
            tempCardNumber = "";
            app.getMessageIndicator().setMessage(tempCardNumber);
            return ReadPhoneCardNumberState.class;
        }

		if (event.getSource() instanceof EnterButton) {
            if (tempCardNumber.trim().length() <= 0) {
    	        tempCardNumber = "";
                return ReadPhoneCardNumberState.class;
            } else { 
            	curLineItem.setContent(tempCardNumber);
            }
		}
        tempCardNumber = "";
		return DaiShou3IdleState.class;
	}
}
