package hyi.cream.state;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.*;
import java.util.*;
import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.event.POSButtonEvent;

public class PrintCurrentZReportState extends State implements PopupMenuListener {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    static PrintCurrentZReportState printCurrentZReportState = null;
    private ArrayList menu = new ArrayList();
    private List zReports = new ArrayList();
    private PopupMenuPane popup;
    private Class exitState;

    public static PrintCurrentZReportState getInstance() {
        try {
            if (printCurrentZReportState == null) {
                printCurrentZReportState = new PrintCurrentZReportState();
            }
        } catch (InstantiationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return printCurrentZReportState;
    }

    /**
     * Constructor
     */
    public PrintCurrentZReportState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        exitState = sourceState.getClass();
        DateFormat sdf = CreamCache.getInstance().getDateTimeFormate();
        app.getMessageIndicator().setMessage(res.getString("ConfirmPrintZReport"));

        popup = POSTerminalApplication.getInstance().getPopupMenuPane();
        menu.clear();
        zReports.clear();
        Iterator iter = ZReport.queryRecentReports();
        if (iter != null) {
            int idx = 1;
            while (iter.hasNext()) {
                ZReport z = (ZReport)iter.next();
                String endDateTime = sdf.format(z.getEndSystemDateTime());
                if (endDateTime.startsWith("1970-01-01"))
                    menu.add(idx + ". " + res.getString("CurrentZReport"));
                else
                    menu.add(idx + ". " + endDateTime);
                zReports.add(z);
                idx++;
            }
        }
        popup.setMenu(menu);
        popup.setVisible(true);
        popup.setPopupMenuListener(this);
        popup.clear();
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof ClearButton) {
            app.getMessageIndicator().setMessage("");
        }

        if (sinkState == null) {
            //Bruce/2003-09-25/
            //如果在statechart3.conf中：
            // hyi.cream.state.PrintCurrentZReportState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.ClearButton,
            //也就是没有sink state的时候，那就从哪里来就到哪里去
            //这样可以解决喜士多的“打印X帐”在Keylock2的位置也有的问题
            //但是喜士多的keylock2.conf的文字要改成“打印X/Z帐”
            return exitState;
        } else {
            return sinkState.getClass();
        }
    }

    /**
     * 当在PopupMenuPane按数字键加回车，或者按了清除键了以后，都会callback此方法。
     */
    public void menuItemSelected() {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();

            int selectIndex = popup.getSelectedNumber();
            if (menu != null && menu.size() > 0) {
                if (selectIndex >= 0 && selectIndex < 20) {
                    ZReport z = (ZReport)zReports.get(selectIndex);
                    z = ZReport.queryBySequenceNumber(connection, z.getSequenceNumber());
    
                    DateFormat sdf = CreamCache.getInstance().getDateFormate();
                    String endDateTime = sdf.format(z.getEndSystemDateTime());
                    if (endDateTime.startsWith("1970-01-01"))
                        CreamPrinter.getInstance().printXReport(z, this);
                    else
                        CreamPrinter.getInstance().printZReport(z, this);
                }
                if (selectIndex >= -1 && selectIndex < 10) {
                    POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                    POSButtonHome2.getInstance().buttonPressed(e);
                    menu.clear();
                    zReports.clear();
                } else {
                    popup.setVisible(true);
                    popup.setPopupMenuListener(this);
                    popup.clear();
                }
                return;
            } else {
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome2.getInstance().buttonPressed(e);
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }
}