// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Reason;
import hyi.cream.dac.Transaction;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.uibeans.CancelButton;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.PaidOutButton;
import hyi.cream.uibeans.PopupMenuListener;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PaidOutIdleState extends State implements PopupMenuListener {
    private POSTerminalApplication app    = POSTerminalApplication.getInstance();
//	private ResourceBundle res            = CreamToolkit.GetResource(); 
    private PopupMenuPane p               = app.getPopupMenuPane();
    private ArrayList reasonArray         = new ArrayList();
    private String pluNo                  = "";
    private PaidOutButton paidOutButton   = null;
    
    static PaidOutIdleState paidOutIdleState      = null;

    public static PaidOutIdleState getInstance() {
        try {
            if (paidOutIdleState == null) {
                paidOutIdleState = new PaidOutIdleState();
            }
        } catch (InstantiationException ex) {
        }
        return paidOutIdleState;
    }

    /**
     * Constructor
     */
    public PaidOutIdleState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
        //System.out.println("PaidOutIdleState entry");

        if (event != null && event.getSource() instanceof PaidOutButton) {
            paidOutButton = ((PaidOutButton)event.getSource());
            ArrayList menu = new ArrayList();
            Iterator paidOutItem = Reason.queryByreasonCategory("11");
            if (paidOutItem == null)
                return;
            Reason r = null;
            //String menuString = "";
            //PLU plu = null;
            int i = 1;
            while (paidOutItem.hasNext()) {
                r = (Reason)paidOutItem.next();
                reasonArray.add(r);
                menu.add(i + "." + r.getreasonName());
                i++;
            }

            p.setMenu(menu);
            p.setVisible(true);
            p.setPopupMenuListener(this);

            Transaction trans = app.getCurrentTransaction();
            trans.setDealType2("4");
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSelect"));
        } else {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PaidOutSelect"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("PaidOutIdleState exit");
        
		app.getMessageIndicator().setMessage("");
        app.getWarningIndicator().setMessage("");

        Iterator paidOutItem = Reason.queryByreasonCategory("11");
        if (paidOutItem == null)
            return IdleState.class;

        Transaction trans = app.getCurrentTransaction();
        
        //  check enter button
        if (event.getSource() instanceof EnterButton) {
                                     
            //  accounting daifu amount and count
            Object lineItems[] = trans.getLineItems();
            LineItem lineItem = null;
            HYIDouble amount = new HYIDouble(0);
            int count = 0;
            for (int i = 0; i < lineItems.length; i++) {
                lineItem = (LineItem)lineItems[i];
                amount = amount.addMe(lineItem.getAmount());
                count = count + 1;
            }              

            //  set to transaction
            trans.setDaiFuAmount(amount);

            return SummaryState.class;
        }

        //  check cancel button
        if (event.getSource() instanceof CancelButton) {
            return CancelState.class;
        }

        //  check select button
        if (event.getSource() instanceof SelectButton) {
            return PaidOutReadyState.class;
        }

        //  check clear button
        if (event.getSource() instanceof ClearButton) {
            if (trans.getCurrentLineItem() == null) {

                trans.setDealType2("0");
                return IdleState.class;
            } else {
                return PaidOutIdleState.class;
            }
        }

        return sinkState.getClass();
    }

    public String getPluNo() {
        return pluNo;
    }

    public void menuItemSelected() {
        if (p.getSelectedMode()) {
            int index = p.getSelectedNumber();
            pluNo = ((Reason)reasonArray.get(index)).getreasonNumber();
            POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
            POSButtonHome2.getInstance().buttonPressed(e);
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome2.getInstance().buttonPressed(e);
        }
    }
}

