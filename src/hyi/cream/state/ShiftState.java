package hyi.cream.state;

//import hyi.cream.POSButtonHome;
import hyi.cream.POSButtonHome2;
//import hyi.cream.POSPeripheralHome;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.CreamSession;
import hyi.cream.dac.Attendance;
import hyi.cream.dac.CashForm;
import hyi.cream.dac.LineOffTransfer;
import hyi.cream.dac.ShiftReport;
import hyi.cream.dac.TransactionHold;
import hyi.cream.dac.ZReport;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.inline.InlineCommandExecutor;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.SignOffForm;
import hyi.cream.util.CreamToolkit;
import hyi.spos.CashDrawer;

import java.awt.Toolkit;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.EventObject;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;

//import jpos.CashDrawer;

public class ShiftState extends State {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static ShiftState shiftState = null;
    private boolean success = true;
    private boolean canDoShift = false;
    private ResourceBundle res = CreamToolkit.GetResource();

    public static ShiftState getInstance() {
        try {
            if (shiftState == null) {
                shiftState = new ShiftState();
            }
        } catch (InstantiationException ex) {
        }
        return shiftState;
    }

    /**
     * Constructor
     */
    public ShiftState() throws InstantiationException {
        if (shiftState == null)
            shiftState = this;
        else
            throw new InstantiationException(this + "");
    }

    public void entry(EventObject event, State sourceState) {
        DbConnection connection = null;
        app.setEnableKeylock(false);
        try {
            connection = CreamToolkit.getTransactionalConnection();
            // System.out.println("ShiftState Enter");
            CreamToolkit.logMessage("ShiftState> Entering ShiftState...");
            canDoShift = false;
            ShiftReport shift = ShiftReport.getCurrentShift(connection);
            Date initDate = CreamToolkit.getInitialDate();
            if (StringUtils.isEmpty(PARAM.getCashierNumber()) || shift == null) {
                POSButtonHome2.getInstance().buttonPressed(
                    new POSButtonEvent(new EnterButton(0, 0, 0, "")));
                CreamToolkit.logMessage("ShiftState> CashierNumber is empty");
                return;
            }
            if (shift != null) {
                if (CreamToolkit.compareDate(shift.getAccountingDate(), initDate) != 0) {
                    POSButtonHome2.getInstance().buttonPressed(
                        new POSButtonEvent(new EnterButton(0, 0, 0, "")));
                    CreamToolkit.logMessage("ShiftState> No current ShiftReport");
                    return;
                }
            }
            canDoShift = true;
            
            int holdCount = 0;
            //if (DacTransfer.getInstance().getStoZPrompt()) {
            if (CreamSession.getInstance().isDoingShiftBeforeZ()) {
                // 模拟user按ClearButton. 在State里面必需在开一个thread来做。
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                        }
                        POSButtonHome2.getInstance().buttonPressed(
                            new POSButtonEvent(new EnterButton(0, 0, 0, "")));
                    }
                }).start();
                CreamToolkit.logMessage("ShiftState> From Z");
                return;
            } else if ((holdCount = TransactionHold.holdTranCount(connection, shift.getTerminalNumber())) > 0) {
    			String message = MessageFormat.format(res.getString("UnHoldTransactions"), holdCount);
            	if (!PARAM.getShiftWithHoldTrn().equalsIgnoreCase("allow")) {
                    app.getWarningIndicator().setMessage(message);
                }/* else if (GetProperty.getEODWithHoldTrn("allow").equalsIgnoreCase("deny")) {
                    app.getWarningIndicator().setMessage(message);
//                    app.getWarningIndicator().setMessage(message
//                            + CreamToolkit.GetResource().getString("UnHoldTransactions2"));
                }*/
            	TransactionHold.makeCancel(connection);
            }
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("ShiftConfirm"));
            connection.commit();
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
    }

    private void openDrawer() {
        POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
        try {
            CashDrawer cashi = posHome.getCashDrawer();
            if (!posHome.getEventForwardEnabled())
                posHome.setEventForwardEnabled(true);
            cashi.setDeviceEnabled(true);
            cashi.openDrawer();
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (!canDoShift) {
            POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                res.getString("NoLoginWarning"));
            canDoShift = false;
            CreamToolkit.logMessage("ShiftState> Cannot do shift now");
            app.setEnableKeylock(true);
            return hyi.cream.state.KeyLock1State.class;
        }

        if (event.getSource() instanceof EnterButton) {
            DbConnection connection = null;
            try {
                connection = CreamToolkit.getTransactionalConnection();

                final ShiftReport shift = ShiftReport.getCurrentShift(connection);
                //if (GetProperty.getShiftWithHoldTrn(null) != null) {
                if (!PARAM.getShiftWithHoldTrn().equalsIgnoreCase("allow")) {
                    TransactionHold.deleteByPosNO(connection, shift.getTerminalNumber());
                }

                app.getCurrentTransaction().clear();
                app.getCurrentTransaction().init();
                app.getCurrentTransaction().setDealType2("9");
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("shift"));
                app.getCurrentTransaction().setInvoiceCount(0);
                app.getCurrentTransaction().setInvoiceID("");
                app.getCurrentTransaction().setInvoiceNumber("");
                app.getCurrentTransaction().store(connection);
                CreamToolkit.logMessage("ShiftState> Signoff record created.");
                connection.commit();
                CreamToolkit.releaseConnection(connection);

                connection = CreamToolkit.getPooledConnection();
                // 现金清点单 only use when property "UseCashForm" is "yes"
                boolean useCashForm;
                useCashForm = PARAM.isUseCashForm();
                if (PARAM.isSendPunchInOutDetail()) {
                    Attendance.genAttendance(String.valueOf(ZReport.getCurrentZNumber(connection)));
                    try {
                        Client.getInstance().processCommand("putWorkCheck");
                    } catch (ClientCommandException e) {
                    }
                }
                if (PARAM.isUploadVersion()) {
                    hyi.cream.dac.Version.genVersion();
                    try {
                        Client.getInstance().processCommand("putPosVersion");
                    } catch (ClientCommandException e) {
                    }
                }

                if (useCashForm) {
                    openDrawer();
                    SignOffForm p = SignOffForm.getInstance();
                    p.setBounds();
                    p.modify(shift.getZSequenceNumber().intValue(), shift.getSequenceNumber()
                        .intValue(), false);
                }

                if (Client.getInstance().isConnected()) {
                    InlineCommandExecutor.getInstance().execute("putShift " +
                        shift.getZSequenceNumber() + " " + shift.getSequenceNumber());
                    success = true;
                }

//              if (shift.getUploadState().equals("2")) { // 上次上传失败
//                  CreamToolkit.logMessage("ShiftState> Start retransmitting shift");
//                  InlineCommandExecutor.getInstance().execute("putShift " +
//                      shift.getZSequenceNumber() + " " + shift.getSequenceNumber());
//                  // return hyi.cream.state.KeyLock1State.class;
//              }

                // if (!DacTransfer.getInstance().isConnected()) {
                if (!Client.getInstance().isConnected()) {
                    success = false;
                    CreamToolkit.logMessage("ShiftState> Save ShiftReport/CashForm to floppy disk");

                    // modify by lxf 2003.02.08
                    // only use when property "FloppyContent" is "full"
                    boolean useSave = PARAM.getFloppyContent().equalsIgnoreCase("full");
                    if (useSave)
                        LineOffTransfer.getInstance().saveToDisk2(shift);
                    else
                        LineOffTransfer.getInstance().saveToDisk(shift);

                    if (useCashForm) {
                        CashForm cf = CashForm.queryByZNumberAndShiftNumber(connection,
                            shift.getZSequenceNumber().intValue(), shift.getSequenceNumber().intValue());
                        if (cf != null)
                            LineOffTransfer.getInstance().saveToDisk2(cf);
                    }
                }
                if (success) {
                    app.getMessageIndicator().setMessage(
                        CreamToolkit.GetResource().getString("shiftEnd"));
                    // return hyi.cream.state.DrawerOpenState3.class;
                } else {
                    success = true;
                    app.getMessageIndicator().setMessage(
                        CreamToolkit.GetResource().getString("ShiftNotUploadWarning"));
                    CreamToolkit.logMessage("Shift upload failed.");
                    // Bruce/2003-11-06
                    if (PARAM.isUseFloppyToSaveData())
                        return ShiftState2.class;
                    try {
                        Toolkit.getDefaultToolkit().beep();
                        Toolkit.getDefaultToolkit().beep();
                        Toolkit.getDefaultToolkit().beep();
                        Thread.sleep(2500);
                    } catch (InterruptedException e) {
                    }
                }

                // new Thread() {
                // public void run() {
                // try {
                // Client.getInstance().processCommand(
                // "SyncTransaction "
                // + shift.getBeginTransactionNumber() + " "
                // + shift.getEndTransactionNumber());
                // } catch (ClientCommandException e) {
                // e.printStackTrace();
                // }
                // }
                // };

            } catch (SQLException e) {
            	app.setEnableKeylock(true);
                CreamToolkit.logMessage(e);
                CreamToolkit.haltSystemOnDatabaseFatalError(e);
            } finally {
                CreamToolkit.releaseConnection(connection);
            }
            return hyi.cream.state.DrawerOpenState3.class;

        } else {
            CreamToolkit.logMessage("ShiftState> Exit state, not a EnterButton.");
            app.setEnableKeylock(true);
            return sinkState.getClass();
        }
    }
}
