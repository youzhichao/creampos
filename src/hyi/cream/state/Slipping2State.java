/**
 * State class
 * @since 2000
 * @author slackware
 */
 
package hyi.cream.state;

//for event processing
import java.util.*;

import hyi.cream.*;
import hyi.cream.util.*;

public class Slipping2State extends State {
	private boolean validated = false;
    static Slipping2State slipping2State = null;

    public static Slipping2State getInstance() {
        try {
            if (slipping2State == null) {
                slipping2State = new Slipping2State();
            }
        } catch (InstantiationException ex) {
        }
        return slipping2State;
    }

    /**
     * Constructor
     */
    public Slipping2State() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
		//System.out.println("This is Slipping2State's Entry!");
		//POSPeripheralHome posHome = POSPeripheralHome.getInstance();
		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
		//DiscountState	null
		if (sourceState.getClass().getName().endsWith("DiscountState")
			&& event == null) {
//Show message "请按[认证]键打印, [确认]键继续" on messageIndicator.
			posTerminal.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("SlippingHint"));
			//posTerminal.showIndicator("message");
		} else if (sourceState.getClass().getName().endsWith("Slipping2State")
			&& event.getSource().getClass().getName().endsWith("SlipButton")) {//Slipping2State	SlipButton
//Print a string on slip.
			CreamPrinter.getInstance().printSlip(this);
		  	validated = true;
		}//KeylockWarningState	Keylock
    }
//exit()
//Sink State	Event Source	Action	Collaborated Things
	public Class exit(EventObject event, State sinkState) {
		//System.out.println("This is Slipping2State's Exit!");
		//Slipping2State	SlipButton	Do nothing.
		//Paying2State	EnterButton	Clear messageIndicator.
		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
		if (event.getSource().getClass().getName().endsWith("EnterButton")) {
			if (validated) {
				posTerminal.getMessageIndicator().setMessage("");
				validated = false;
				return SummaryState.class;
			}
			return Slipping2State.class;
		}
		if (sinkState != null) {
			return sinkState.getClass();
		} else
            return null;
	}	//KeylockWarningState Keylock

}


