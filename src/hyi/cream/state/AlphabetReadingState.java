package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.NumberButton;

import java.util.EventObject;

public class AlphabetReadingState extends State {
    private static AlphabetReadingState instance = new AlphabetReadingState();
    private POSTerminalApplication app = POSTerminalApplication.getInstance();

    private String number = "";

    private AlphabetReadingState() {}
    public static AlphabetReadingState getInstance() {
        return instance;
    }

    public void entry(EventObject event, State sourceState) {
        if (!(sourceState instanceof AlphabetReadingState))
            number = "";
        NumberButton pb = (NumberButton)event.getSource();
        number = number + pb.getNumberLabel();
        app.getMessageIndicator().setMessage(number);
    }

    public Class exit(EventObject event, State sinkState) {
        return sinkState.getClass();
    }

    public String getNumber() {
        return number;
    }
}
