/**
 * State class
 * @since 2000
 * @author slackware
 */

package hyi.cream.state;

import hyi.cream.CreamSession;
import hyi.cream.POSTerminalApplication;
import hyi.cream.WorkingStateEnum;
import hyi.cream.groovydac.SsmpLog;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.TransactionHold;
import hyi.cream.dac.Store;
import hyi.cream.state.periodical.PeriodicalNoReadyState;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.ReceiptGenerator;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.EventObject;
import java.util.Iterator;
import java.util.ResourceBundle;

public class CancelState extends State {
    private POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();

    private ResourceBundle res = CreamToolkit.GetResource();

    private Class enterExitState = null;

    private Class clearExitState = null;

    private boolean printCancel = false;

    private boolean keyLock = false;

    private String warning = "";

    private String message = "";

    static CancelState cancelState = null;

    public static CancelState getInstance() {
        try {
            if (cancelState == null) {
                cancelState = new CancelState();
            }
        } catch (InstantiationException ex) {
        }
        return cancelState;
    }

    /**
     * Constructor
     */
    public CancelState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        // System.out.println("This is CancelState's Entry!");

        message = posTerminal.getMessageIndicator().getMessage();
        warning = posTerminal.getWarningIndicator().getMessage();

        posTerminal.getMessageIndicator().setMessage(res.getString("CancelConfirm"));
        
        if (sourceState instanceof IdleState
            || sourceState instanceof CashierRightsCheckState
            || sourceState instanceof PeriodicalNoReadyState
        ) 
        {
            DbConnection connection = null;
            try {
                if (!posTerminal.getTrainingMode() && !posTerminal.getReturnItemState()
                        && PARAM.getTranPrintType().equalsIgnoreCase("step")) {
                    connection = CreamToolkit.getTransactionalConnection();
                    if (!CreamPrinter.getInstance().getHeaderPrinted() ) {
                        CreamPrinter.getInstance().printHeader(connection);
                        CreamPrinter.getInstance().setHeaderPrinted(true);
                    }
                    if (POSTerminalApplication.getInstance().getCurrentTransaction().getCurrentLineItem() != null) {
                        CreamPrinter.getInstance().printLineItem(connection, POSTerminalApplication.getInstance().getCurrentTransaction().getCurrentLineItem());
                    }
                    connection.commit();
                }
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                CreamToolkit.haltSystemOnDatabaseFatalError(e);
            } catch (Exception e) {
                e.printStackTrace(CreamToolkit.getLogger());
            } finally {
                CreamToolkit.releaseConnection(connection);
                enterExitState = InitialState.class;
                clearExitState = IdleState.class;
            }
        } else if (sourceState instanceof TransactionHoldShowState) {
            enterExitState = TransactionUnHoldState.class;
            clearExitState = TransactionHoldShowState.class;
        } else if (sourceState instanceof DaiFuIdleState) {
            enterExitState = InitialState.class;
            clearExitState = DaiFuIdleState.class;
        } else if (sourceState instanceof PaidInIdleState) {
            enterExitState = InitialState.class;
            clearExitState = PaidInIdleState.class;
        } else if (sourceState instanceof PaidOutIdleState) {
            enterExitState = InitialState.class;
            clearExitState = PaidOutIdleState.class;
        } else {
            enterExitState = KeyLock1State.class;
            clearExitState = sourceState.getClass();
            keyLock = true;
        }

        // check print
        if (sourceState instanceof CashInIdleState
            || sourceState instanceof CashInState
            || sourceState instanceof CashOutIdleState
            || sourceState instanceof CashOutState
            || sourceState instanceof DaiFuIdleState
            || sourceState instanceof PaidInIdleState
            || sourceState instanceof PaidOutIdleState
        ) {
            printCancel = false;
        } else {
            printCancel = true;
        }
    }

    public Class exit(EventObject event, State sinkState) {
        // System.out.println("This is CancelState's Exit!");
        POSTerminalApplication posTerminal = POSTerminalApplication
                .getInstance();
        // init newCashierID
        posTerminal.setNewCashierID("");

        if (sinkState instanceof WarningState)
            return WarningState.class;

        posTerminal.getMessageIndicator().setMessage("");
        if (event.getSource().getClass().getName().endsWith("EnterButton")) {
            posTerminal.setChecked(false);
            // posTerminal.setSalemanChecked(false);
            if (posTerminal.getTrainingMode()) {
                return enterExitState;
            }

            if (posTerminal.getCurrentTransaction() == null) {
                keyLock = false;
                return enterExitState;
            }
            Object[] l = posTerminal.getCurrentTransaction()
                    .getLineItems();
            if (l.length > 0) {
                if (printCancel) {
                    posTerminal.getWarningIndicator().setMessage(
                            res.getString("ReturnEndMessage"));
                }
                DbConnection connection = null;
                try {
                    connection = CreamToolkit.getTransactionalConnection();
                    // do for current transaction
                    Transaction current = posTerminal.getCurrentTransaction();
                    if (PARAM.getTranPrintType().equalsIgnoreCase("once")) {
                        current.setInvoiceNumber("");
                    }
                    current.setInvoiceCount(ReceiptGenerator.getInstance().getPageNumber());
                    current.setDealType1("*");
                    current.setDealType2("0");
                    current.setDealType3("0");
                    current.initForAccum();
                    current.initSIInfo();
                    current.accumulate();
                    current.store(connection);
                    SsmpLog.report10008(current.getTransactionNumber());

                    // 挂单交易处理,挂单无需打印
                    if (current.getHoldTranNo() != null) {
                        TransactionHold.deleteByTranNumber(connection, current.getHoldTranNo());
                        printCancel = false;
                    }

                    if (posTerminal.getReturnItemState())
                        printCancel = false;

                    // print
                    if (printCancel) {
                        CreamPrinter.getInstance().printCancel(connection, "CANCELED!");
                        CreamPrinter.getInstance().setHeaderPrinted(false);
                    }

                    // current.Clear();
                    // do for new transaction
                    int nextTranNumber = Transaction.getNextTransactionNumber();
                    Transaction newTran = Transaction.queryByTransactionNumber(connection,
                        current.getTerminalNumber(), current.getTransactionNumber());
                    //newTran.setTransactionNumber(nextTranNumber - 1);
                    newTran.setDealType1("0");
                    newTran.setDealType2("0");
                    newTran.setDealType3("3");
                    newTran.setState2("0"); //恢复未转档初始状态
                    newTran.setStoreNumber(Store.getStoreID());
                    newTran.setTerminalNumber(PARAM.getTerminalNumber());
                    newTran.setVoidTransactionNumber((nextTranNumber - 1) * 100 + newTran.getTerminalNumber());
                    newTran.setTransactionNumber(nextTranNumber);
                    newTran.setSignOnNumber(PARAM.getShiftNumber());
                    newTran.setTransactionType(current.getTransactionType());
                    newTran.setTerminalPhysicalNumber(PARAM.getTerminalPhysicalNumber());
                    newTran.setCashierNumber(PARAM.getCashierNumber());
                    newTran.setInvoiceCount(current.getInvoiceCount());
                    newTran.setInvoiceID(current.getInvoiceID());
                    newTran.setInvoiceNumber(current.getInvoiceNumber());
                    // 统一编号与原有交易相同，所以这里不要重置
                    // newTran.setBuyerNumber("");
                    newTran.makeNegativeValue();
                    // for daishou
                    Iterator ite = current.getLineItemsIterator();
                    Iterator ite2 = newTran.getLineItemsIterator();
                    while (ite.hasNext()) {
                        LineItem li = (LineItem) ite.next();
                        LineItem li2 = (LineItem) ite2.next();
                        li2.setDaishouNumber(li.getDaishouNumber());
                    }

                    newTran.store(connection);

                    posTerminal.getMessageIndicator().setMessage(
                            res.getString("TransactionEnd"));

                    newTran.clear();

                    if (keyLock) {
                        Transaction currTran = posTerminal.getCurrentTransaction();
                        posTerminal.getItemList().setTransaction(currTran);
                        currTran.clear();
                        currTran.clearLineItem();
                        currTran.setDealType1("0");
                        currTran.setDealType2("0");
                        currTran.setDealType3("0");
                        currTran.setInvoiceNumber(PARAM.getInvoiceNumber());
                        currTran.setInvoiceID(PARAM.getInvoiceID());
                        currTran.setStoreNumber(Store.getStoreID());
                        currTran.setTerminalPhysicalNumber(PARAM.getTerminalPhysicalNumber());
                        if (PARAM.getTranPrintType().equalsIgnoreCase("once")) {
                            currTran.setInvoiceNumber("");
                        }
                        keyLock = false;
                    }
                    connection.commit();

                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                    CreamToolkit.haltSystemOnDatabaseFatalError(e);
                } finally {
                    CreamToolkit.releaseConnection(connection);
                }
                CreamSession.getInstance().removeAttributes(WorkingStateEnum.PERIODICAL_ORDER_STATE);
                CreamSession.getInstance().setWorkingState(WorkingStateEnum.IDLE_STATE);
                return enterExitState;
            } else if (CreamSession.getInstance().getWorkingState() ==
                    WorkingStateEnum.PERIODICAL_ORDER_STATE)
            {
                // 在期刊状态下【交易取消】键 含 【清除】键功能 无明细直接返回InitialState
                CreamSession.getInstance().removeAttributes(WorkingStateEnum.PERIODICAL_ORDER_STATE);
                CreamSession.getInstance().setWorkingState(WorkingStateEnum.IDLE_STATE);
                return enterExitState;
            }
        } else if (event.getSource().getClass().getName().endsWith(
                "ClearButton")) {
            keyLock = false;
            posTerminal.getMessageIndicator().setMessage(message);
            posTerminal.getWarningIndicator().setMessage(warning);
            return clearExitState;
        }

        return sinkState.getClass();
    }
}
