package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamCache;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;

import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;
import java.util.List;
import java.util.ResourceBundle;

public class PrintHisTransState extends State {

    private static PrintHisTransState instance = new PrintHisTransState();
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private String numberString = "";
    private int step = 1;
    private String start = "";
    private String end = "";
    private int selectType = 0;

    private PrintHisTransState() {}

    public static PrintHisTransState getInstance() {
        return instance;
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof PrintHisTramsSelectModeState) {
            selectType = ((PrintHisTramsSelectModeState) sourceState).getSelectType();
        }

        if (event.getSource() instanceof NumberButton && sourceState instanceof PrintHisTransState) {
            NumberButton pb = (NumberButton) event.getSource();
            numberString = numberString + pb.getNumberLabel();
        }
        String message = "";
        if (step == 1) {
            if (selectType == 1)
                message = res.getString("InputStartTransNumber") +numberString;
            else if (selectType == 2)
                message = res.getString("InputStartTime") +numberString;
        } else if (step == 2) {
            if (selectType == 1)
                message = res.getString("InputEndTransNumber")+numberString;
            else if (selectType == 2)
                message = res.getString("InputEndTime") +numberString;
        } else if (step == 3)
            message = MessageFormat.format(res.getString("StartPrintTrans"), start, end);

        app.getMessageIndicator().setMessage(message);
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof NumberButton) {
            app.getWarningIndicator().setMessage("");
        }

        if (event.getSource() instanceof ClearButton) {
            if (numberString.equals("")) {
                if (step == 1)
                    return PrintHisTramsSelectModeState.class;
                step--;
            }
            numberString = "";
            return PrintHisTransState.class;
        }

        if (event.getSource() instanceof EnterButton) {
            if (step == 3) {
                step = 1;

                int startNo = 0;
                int endNo = 0;
                if (selectType == 1) {
                    startNo = Integer.parseInt(start);
                    endNo = Integer.parseInt(end);
                } else if (selectType == 2) {
                    List list = Transaction.getTransactionsByTime(parseDateStr2(start), parseDateStr2(end));
                    int size = list.size();
                    if (size >= 2) {
                        startNo = ((Integer) list.get(0)).intValue();
                        endNo = ((Integer) list.get(size - 1)).intValue();
                    } else if (size == 1) {
                        startNo = endNo = ((Integer) list.get(0)).intValue();
                    }
                }

                for (int i = startNo; i <= endNo; i++) {
                    app.getMessageIndicator().setMessage(res.getString("PrintTran") + i);

                    //Bruce/20070314/ 為了簡化CreamPrinter暫時拿掉IsHisTransPrintNeedCut
                    // boolean isPageCut = true;
                    // if (GetProperty.getIsHisTransPrintNeedCut("false").trim().equals("false"))
                    // isPageCut = false;
                    // if (i == endNo)
                    // isPageCut = true;

                    DbConnection connection = null;
                    try {
                        connection = CreamToolkit.getTransactionalConnection();
                        CreamPrinter.getInstance().reprint(connection, i); //, isPageCut, true);
                        connection.commit();
                    } catch (SQLException e) {
                        CreamToolkit.logMessage(e);
                        CreamToolkit.haltSystemOnDatabaseFatalError(e);
                    } finally {
                        CreamToolkit.releaseConnection(connection);
                    }
                }
                app.getMessageIndicator().setMessage("");
                return PrintHisTramsSelectModeState.getSourceState();
            }
            if (selectType == 1) {
                
                DbConnection connection = null;
                try {
                    connection = CreamToolkit.getPooledConnection();
                    Transaction trans = Transaction.queryByTransactionNumber(connection,
                        PARAM.getTerminalNumber(), Integer.parseInt(numberString));
                    if (trans == null) {
                        app.getWarningIndicator().setMessage(numberString+res.getString("TransactionNotFound")
                                +res.getString("PleaseReInput"));
                        numberString = "";
                    } else {
                        if (step == 1)
                            start = numberString;
                        else if (step == 2)
                            end = numberString;
                        numberString = "";
                        step++;
                    }
                } catch (SQLException e) {
                    CreamToolkit.logMessage(e);
                } finally {
                    CreamToolkit.releaseConnection(connection);
                }
            } else if (selectType == 2) {
                if (step == 1)
                    start = numberString;
                else if (step == 2)
                    end = numberString;
                String errMsg = parseDateStr(numberString);
                if (errMsg != null)
                    app.getWarningIndicator().setMessage(errMsg
                            + res.getString("PleaseReInput"));
                else
                    step++;
                numberString = "";
            }
        }
        return PrintHisTransState.class;
    }

    private String parseDateStr(String dateStr) {
        String errMsg = null;
        if (dateStr.length() < 12)
            errMsg = res.getString("InputTimeError");
        Calendar c1 = Calendar.getInstance();
        try {
            Date date = CreamCache.getInstance().getDateTimeFormate4().parse(dateStr);
            c1.setTime(date);
        } catch (ParseException e) {
//            e.printStackTrace();
            errMsg = res.getString("InputTimeError");
        }
//
//        Calendar c2 = Calendar.getInstance();
//        c2.setTime(new Date());
//        c2.add(Calendar.MONTH, -3);
//        if (c1.before(c2))
//            errMsg = res.getString("InputTimeError");
        return errMsg;
    }

    private String parseDateStr2(String dateStr) {
        dateStr = dateStr.substring(0, 4) + "-" + dateStr.substring(4, 6) + "-"
                  + dateStr.substring(6, 8) + " " + dateStr.substring(8, 10)
                  + ":" + dateStr.substring(10, 12);
        return dateStr;
    }

}
