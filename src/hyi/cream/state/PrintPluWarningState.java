package hyi.cream.state;

//import hyi.cream.POSPeripheralHome;
import hyi.cream.POSPeripheralHome3;
import hyi.cream.POSTerminalApplication;
import hyi.cream.exception.NoSuchPOSDeviceException;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.CreamToolkit;
import hyi.spos.JposException;
import hyi.spos.ToneIndicator;

import java.util.EventObject;

//import jpos.JposException;
//import jpos.ToneIndicator;

public class PrintPluWarningState extends State {

    private static PrintPluWarningState warningState = null;

    private String printString = "";
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ToneIndicator tone = null;
    private static Class<? extends State> exitState;

    public static PrintPluWarningState getInstance() {
        try {
            if (warningState == null) {
                warningState = new PrintPluWarningState();
            }
        } catch (InstantiationException ex) {
        }
        return warningState;
    }

    public PrintPluWarningState() throws InstantiationException {
        if (warningState == null)
            warningState = this;
        else
            throw new InstantiationException(this + "");
    }

    public void entry(EventObject event, State sourceState) {
        POSPeripheralHome3 posHome = POSPeripheralHome3.getInstance();
        try {
            tone = posHome.getToneIndicator();
            if (!tone.getDeviceEnabled())
                tone.setDeviceEnabled(true);
            tone.setAsyncMode(true);
            tone.sound(99999, 500);
        } catch (Exception ne) {
            CreamToolkit.logMessage(ne);
        }
        printString = sourceState.getWarningMessage();
        sourceState.setWarningMessage("");
        if (printString.equals("") && this.getWarningMessage().compareTo("") == 0) {
            printString = CreamToolkit.GetResource().getString("Warning");
        } else if (this.getWarningMessage().compareTo("") != 0) {
            printString = getWarningMessage();
            this.setWarningMessage("");
        }
        app.getWarningIndicator().setMessage(printString);
    }

    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");
        app.getMessageIndicator().setMessage("");

        if (tone != null) {
            try {
                tone.clearOutput();
            } catch (JposException e) {
                CreamToolkit.logMessage(e);
            }
        }

        if (event.getSource() instanceof EnterButton) {
            Class exitState = getExitState();
            if (exitState != null) {
                setExitState(null);
                return exitState;
            } else {
                return PrintPluState.class;
            }
        } 
//        else if (event.getSource() instanceof ClearButton) {
//            CashierRightsCheckState.setSourceState("PrintPluState");
//            CashierRightsCheckState.setTargetState("CancelState");
//            return CashierRightsCheckState.class;
//        } else {
//            return null;
//        }
    	return IdleState.class;
    }

    public static Class<? extends State> getExitState() {
        return exitState;
    }

    public static void setExitState(Class<? extends State> exitStateArg) {
        exitState = exitStateArg;
    }
}
