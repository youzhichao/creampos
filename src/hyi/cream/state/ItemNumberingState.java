
// Copyright (c) 2000 HYI

package hyi.cream.state;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class ItemNumberingState extends SomeAGNumberingState {
    static ItemNumberingState ItemNumberingState = null;

    public static ItemNumberingState getInstance() {
        try {
            if (ItemNumberingState == null) {
                ItemNumberingState = new ItemNumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return ItemNumberingState;
    }

    /**
     * Constructor
     */
    public ItemNumberingState() throws InstantiationException {
    }
}

  
