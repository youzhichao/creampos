
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.state.periodical.PeriodicalNoNumberingState;
import hyi.cream.state.periodical.PeriodicalNoReadyState;
import hyi.cream.uibeans.ItemList;
import hyi.cream.uibeans.WeiXiuButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;

/**
 * 直接删除trandetail
 * 只有在整笔打印的情况下
 * 如：PeriodicalDraw,Paidin...
 * 
 */
public class RemoveState extends State {

    private static POSTerminalApplication app   = POSTerminalApplication.getInstance();
    private String removeNumber          = "";
    private boolean pluAvailable         = false;
    static RemoveState voidState = null;

    public static RemoveState getInstance() {
        try {
            if (voidState == null) {
                voidState = new RemoveState();
            }
        } catch (InstantiationException ex) {
        }
        return voidState;
    }

    /**
     * Constructor
     */
    public RemoveState() throws InstantiationException {
    }

    public static LineItem createVoidLineItem(LineItem removeItem) {
        Transaction curTransaction = app.getCurrentTransaction();
        LineItem voidItem = (LineItem)removeItem.clone();
        
        voidItem.makeNegativeValue();

        voidItem.setRemoved(true);
        voidItem.setLineItemSequence(curTransaction.getLineItems().length + 1);
        if (WeiXiuButton.getInstance() != null) {
            String number = WeiXiuButton.getInstance().getPluCode();
            //System.out.println("number : " + number + " | plunumber : " + removeItem.getPluNumber());                   
            if (removeItem.getPluNumber().equals(number)) {
                //System.out.println("remove weixiu flag success!");
                app.getCurrentTransaction().setAnnotatedType("");
                app.getCurrentTransaction().setAnnotatedId("");
            }
        }

        if (voidItem.getDiscountType() != null && voidItem.getDiscountType().equals("L")){
            try{
                Client.getInstance().processCommand("queryLimitQtySold " + voidItem.getItemNumber() + " -1 "); // + memberID);
            }catch(ClientCommandException e){
            }
        }

        CreamToolkit.logMessage("Void " + voidItem.toString());
        return voidItem;
    }

    public void entry(EventObject event, State sourceState) {
        Transaction curTransaction = app.getCurrentTransaction();
        
        if (sourceState instanceof NumberingState
        		|| sourceState instanceof PeriodicalNoNumberingState) {
        	if (sourceState instanceof NumberingState) {
        		removeNumber = ((NumberingState)sourceState).getNumberString();
                WarningState.setExitState(IdleState.class);
        	} else if (sourceState instanceof PeriodicalNoNumberingState) {
        		removeNumber = ((PeriodicalNoNumberingState)sourceState).getNumberString();
                WarningState.setExitState(PeriodicalNoReadyState.class);
        	}
            if (!CreamToolkit.checkInput(removeNumber, 0)) {
                return;
			}
			Object[] obj = curTransaction.getDisplayedLineItemsArray().toArray();
            if (Integer.parseInt(removeNumber.trim()) > obj.length
                || Integer.parseInt(removeNumber.trim()) < 1) {
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("VoidWarning"));
                setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
//                WarningState.setExitState(IdleState.class);
                setPluAvailable(false);
				return;
			}
			//Object [] obj2 = curTransaction.getDisplayedLineItemsArray().toArray();
			LineItem removeItem = (LineItem)obj[Integer.parseInt(removeNumber) - 1];

			try {
				curTransaction.removeLineItem(removeItem);
			} catch (LineItemNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
			}
            
            setPluAvailable(true);

        } else if (sourceState instanceof IdleState
        		|| sourceState instanceof PeriodicalNoReadyState) {
            WarningState.setExitState(sourceState.getClass());
            ItemList itemList = app.getItemList();
            int[] squ = itemList.getSelectedItems();
            if (squ != null && squ.length > 0) {
                removeNumber = squ[0] + "";
                Object[] obj = curTransaction.getDisplayedLineItemsArray().toArray();
                LineItem removeItem = (LineItem)obj[Integer.parseInt(removeNumber)];
    			try {
    				curTransaction.removeLineItem(removeItem);
    			} catch (LineItemNotFoundException e) {
                    CreamToolkit.logMessage(e.toString());
    			}
            } else {
                LineItem removeItem = curTransaction.getCurrentLineItem();
                if (removeItem == null) {
                    setPluAvailable(false);
                    setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
//                  WarningState.setExitState(IdleState.class);
                    return;
                }
                if (removeItem.getRemoved()) {
                    setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
//                    WarningState.setExitState(IdleState.class);
                    setPluAvailable(false);
                    return;
                }
                
    			try {
    				curTransaction.removeLineItem(removeItem);
    			} catch (LineItemNotFoundException e) {
                    CreamToolkit.logMessage(e.toString());
    			}
            }
            setPluAvailable(true);
        } else {
            setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
            WarningState.setExitState(IdleState.class);
            setPluAvailable(false);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (getPluAvailable()) {
            WarningState.setExitState(null);
            return MixAndMatchState.class;
        } else {
            setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
            return WarningState.class;
        }
    }

    public void setPluAvailable(boolean pluAvailable) {
        this.pluAvailable = pluAvailable;
    }

    public boolean getPluAvailable() {
        return pluAvailable;
    }
}

 
