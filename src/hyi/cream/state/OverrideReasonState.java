package hyi.cream.state;

import hyi.cream.POSButtonHome2;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Reason;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.PopupMenuListener;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.util.CreamToolkit;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class OverrideReasonState extends State implements PopupMenuListener {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	static OverrideReasonState overrideReasonState = null;
    private PopupMenuPane popupMenu = null;
    private ArrayList menu = new ArrayList();
	private LineItem lineItem;

    public static OverrideReasonState getInstance() {
        try {
            if (overrideReasonState == null) {
            	overrideReasonState = new OverrideReasonState();
            }
        } catch (InstantiationException ex) {
        }
        return overrideReasonState;
    }
	
	public OverrideReasonState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
		popupMenu = PopupMenuPane.getInstance();
		if (sourceState instanceof OverrideAmountState) {
			lineItem = ((OverrideAmountState) sourceState).getCurLineItem();
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputReasonID"));
		}
		
		if (popupMenu.isVisible()) {
			return;
		}
	    menu = getMenu();
        if (menu == null || menu.isEmpty())
            return;
		popupMenu.setMenu(menu);
		popupMenu.centerPopupMenu();
		popupMenu.setSelectMode(0);
		popupMenu.setVisible(true);
		if (!popupMenu.isVisible()) {
            menu.clear();
		} else {
			popupMenu.setPopupMenuListener(this);
		}
	}

	public Class exit(EventObject event, State sinkState) {
        menu.clear();
        popupMenu.setMenu(menu);
        popupMenu.setVisible(false);
        return MixAndMatchState.class;
	}

	protected ArrayList getMenu() {
		ArrayList list = new ArrayList();
		// 18 : 变价原因
		int i = 1;
		Iterator it = Reason.queryByreasonCategory("18");
		if (it != null) {
			while (it.hasNext()) {
				Reason r = (Reason) it.next();
				String name = r.getreasonName();
                if (name != null && !name.trim().equals("")) {
                    list.add(i + ". " + name + "/" + r.getreasonNumber());
                    i = i + 1;
                }
			}
		}
		return list;
	}
	
    public void menuItemSelected() {
        if (popupMenu.getSelectedMode()) {
            int selectIndex = popupMenu.getSelectedNumber();
            String reasonID = (String)menu.get(selectIndex);
            System.out.println("reasonID : " + reasonID);

            StringTokenizer tk = new StringTokenizer(reasonID, " /");
            try {
                tk.nextToken();
                tk.nextToken();
                reasonID = tk.nextToken();

                String disCno = lineItem.getDiscountNumber();
                if (disCno != null && !disCno.trim().equals("")) { 
                    int index = disCno.indexOf("O");
                    if (index >= 0) {
                    	String left = disCno.substring(0, index);
                    	String right = disCno.substring(index, disCno.length());
                    	if (right.indexOf(",") > 0) {
                    		right = right.substring(right.indexOf(",") + 1
                    				, right.length());
                    	} else {
                    		right = "";
                    		if (left.length() > 0)
                    			left = left.substring(0, left.length() - 1);
                    	}
                    	disCno = left + right;
                    }
					if (!disCno.equals(""))
                    	disCno += ",";

                    index = disCno.indexOf("M");
                    if (index >= 0) {
                    	String left = disCno.substring(0, index);
                    	String right = disCno.substring(index, disCno.length());
                    	if (right.indexOf(",") > 0) {
                    		right = right.substring(right.indexOf(",") + 1
                    				, right.length());
                    	} else {
                    		right = "";
                    		if (left.length() > 0)
                    			left = left.substring(0, left.length() - 1);
                    	}
                    	disCno = left + right;
                    }
					if (!disCno.equals(""))
                    	disCno += ",";
				} else {
                	disCno = "";
                }
            	disCno += "O" + reasonID;
            	lineItem.setDiscountNumber(disCno);

            } catch (NoSuchElementException e) {}
            menu.clear();
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome2.getInstance().buttonPressed(e);
		} else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome2.getInstance().buttonPressed(e);
		}
    }

}
