package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.TaxType;
import hyi.cream.dac.Transaction;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.math.BigDecimal;
import java.util.EventObject;

/**
 * @author zzy
 */
public class DiscountAmountState extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private String tempPrice = "";
	static DiscountAmountState discountAmountState = null;
    private LineItem curLineItem = null;
    //private boolean discountAmountOrOriginalPrice;
    private HYIDouble price = null;
    
    
    public static DiscountAmountState getInstance() {
        try {
            if (discountAmountState == null) {
                discountAmountState = new DiscountAmountState();
            }
        } catch (InstantiationException ex) {
        }
        return discountAmountState;
    }
	
	public DiscountAmountState() throws InstantiationException {
        //discountAmountOrOriginalPrice = GetProperty.getDiscountAmountOrOriginalPrice("yes")
        //    .equalsIgnoreCase("yes");
    }
	
	public void entry(EventObject event, State sourceState) {
	    Transaction trans = app.getCurrentTransaction();
		curLineItem = trans.getCurrentLineItem();
        if (sourceState instanceof CashierRightsCheckState)
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PleaseInputDiscountAmount"));
        
	}
	
	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof NumberButton) {
			NumberButton pb = (NumberButton)event.getSource();
                //System.out.println(" OverrideAmountState exit number" + pb.getNumberLabel());
			if (pb.getNumberLabel().equals("-") || (pb.getNumberLabel().equals(".") && tempPrice.length() == 0)) {
                //System.out.println(" OverrideAmountState exit number 1 " +tempPrice);
			} else { 
				tempPrice = tempPrice + pb.getNumberLabel();
                //System.out.println(" OverrideAmountState exit number 2" +tempPrice);
			}
			app.getMessageIndicator().setMessage(tempPrice);
			return DiscountAmountState.class;
		}

        if (event.getSource() instanceof ClearButton) {
            if (tempPrice.length() == 0)
                return IdleState.class;
            tempPrice = "";
            app.getMessageIndicator().setMessage(tempPrice);
            return DiscountAmountState.class;
        }

		if (event.getSource() instanceof EnterButton) {

            if (tempPrice.trim().length() <= 0)
                tempPrice = "0";

            // Check number format
            try {
                HYIDouble d = new HYIDouble(tempPrice);
                //Bruce/20030826/
                //检查是否为一个很大或很小的数
                if (d.compareTo(new HYIDouble(999.99)) > 0
                    || d.compareTo(new HYIDouble(0)) < 0)
                    throw new NumberFormatException("");
            } catch (NumberFormatException e) {
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PleaseInputDiscountAmountAgain"));
                tempPrice = "";
                return DiscountAmountState.class;
            }
            
            price = new HYIDouble(tempPrice);
            boolean isWeight = false;
            if (curLineItem.getWeight() != null &&     
                curLineItem.getWeight().compareTo(new HYIDouble(0)) != 0)
                isWeight = true;
               
            if (tempPrice.trim().length() > 0 && !isWeight) {
                curLineItem.setUnitPrice(curLineItem.getOriginalPrice().
            		subtract(price));
				curLineItem.setAmount(curLineItem.getOriginalPrice().multiply(curLineItem.getQuantity()));
            } else if (isWeight) {
            	curLineItem.setAmount(curLineItem.getAmount().
            		subtract(price));
            }
            curLineItem.setAfterDiscountAmount(curLineItem.getAmount());
            //curLineItem.setDiscountAmount(price);
            //curLineItem.setAddRebateAmount(curLineItem.getAfterDiscountAmount().multiply(curLineItem.getRebateRate()));
            curLineItem.caculateAndSetTaxAmount();
            curLineItem.setAfterSIAmount(curLineItem.getAmount());
            curLineItem.setDiscountType("D");
            
            //Jacky/2004-11-25
            try {
                app.getCurrentTransaction().changeLineItem(-1, curLineItem);
            } catch (LineItemNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("LineItem not found at " + this);
            }
            CreamToolkit.showText(app.getCurrentTransaction(), 0);
			tempPrice = "";
			System.out.println("DiscountAmountState | exit IdleState");
			return IdleState.class;
		}
		
		return DiscountAmountState.class;
	}
	
	
	}	