package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Inventory;
import hyi.cream.dac.PLU;
import hyi.cream.uibeans.BarCodeButton;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.InStoreCodeButton;
import hyi.cream.uibeans.Indicator;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.uibeans.RemoveButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;
import hyi.spos.JposException;
import hyi.spos.Scanner;
import hyi.spos.events.DataEvent;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

//import jpos.JposException;
//import jpos.Scanner;

/**
 * 输入盘点数据idle state.
 */
public class InventoryIdleState extends State {
    private HYIDouble weightAmount;
    private HYIDouble weightQuantity;
    private static InventoryIdleState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Indicator messageIndicator = app.getMessageIndicator();
    private ResourceBundle res = CreamToolkit.GetResource();
	private String dtlcode1;
    private static StringBuffer itemNumber = new StringBuffer();
    public static Inventory currentInventory;

    public static InventoryIdleState getInstance() {
        try {
            if (instance == null) {
                instance = new InventoryIdleState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public InventoryIdleState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (itemNumber.length() == 0)
            messageIndicator.setMessage(res.getString("PleaseInputInventoryItem"));

    }

    public Class exit(EventObject event, State sinkState) {
    	dtlcode1 = "";
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            itemNumber.append(pb.getNumberLabel());
            messageIndicator.setMessage(itemNumber.toString());
            return sinkState.getClass();
        }

        if (event.getSource() instanceof ClearButton) {
            if (itemNumber.length() != 0) {
                itemNumber.setLength(0);
                return InventoryIdleState.class;
            } else {
                return InventoryExitState.class;
            }
        }

        if (event.getSource() instanceof RemoveButton) {
            try {
                List dacList = app.getDacViewer().getDacList();
                if (itemNumber.length() == 0 && dacList.size() > 0) { // 立即更正
                    Inventory inv = (Inventory)((Inventory)dacList.get(dacList.size() - 1));
                    currentInventory = (Inventory) inv.clone();
                    if (currentInventory.isRemove()) {
                        return InventoryIdleState.class;
                    }
                    inv.setRemove(true);
                    currentInventory.setRemove(true);
                        currentInventory.setItemSequence(Inventory.queryMaxItemSequence() + 1);
                    currentInventory.setActStockQty(currentInventory.getActStockQty().negate());
                    currentInventory.setAmount(currentInventory.getAmount().negate()); 
                    app.getDacViewer().addDacToDacList(currentInventory);
                    return InventoryStoreState.class;
                } else {    // 指定更正
                    for (int i = dacList.size() - 1; i >= 0; i--) {
                        Inventory inv = (Inventory)dacList.get(i);
                        if (inv.getItemSequence().equals(new Integer(itemNumber.toString()))) {
                            currentInventory = (Inventory)inv.clone();
                            if (currentInventory.isRemove()) {
                                return InventoryIdleState.class;
                            }
                            inv.setRemove(true);
                            currentInventory.setRemove(true);
                            currentInventory.setItemSequence(Inventory.queryMaxItemSequence() + 1);
                            currentInventory.setActStockQty(currentInventory.getActStockQty().negate());
                            currentInventory.setAmount(currentInventory.getAmount().negate()); 
                            app.getDacViewer().addDacToDacList(currentInventory);
                            return InventoryStoreState.class;
                        }
                    }
                }
            } catch (SQLException e) {
                CreamToolkit.logMessage(e);
                CreamToolkit.haltSystemOnDatabaseFatalError(e);
            }
            return InventoryIdleState.class;
        }

        PLU plu = null;
        boolean isWeighted = false;
        if (event.getSource() instanceof Scanner) {
            try {
                //((Scanner)event.getSource()).setDecodeData(true);
                String number = new String(((Scanner)event.getSource()).getScanData(((DataEvent)event).seq));
                plu = PLU.queryBarCode(number);
                if (plu == null)
                    plu = PLU.queryByInStoreCode(number);
                if (plu == null) {
                    plu = processScaleBarCode(number);
                    if (plu != null)
                        isWeighted = true;
                }
    	    	if (plu != null && plu.getItemtype().intValue() == 10 ){
    	    		plu = null; // 如果标记是期刊商品，则不正确, 因为期刊商品必须输入15码 
    	    	}
                
                if (plu == null) {
    				if (number.length() == 15) {
    					// 期刊
    					dtlcode1 = number.substring(13, 15);
    					number = number.substring(0, 13);
                        plu = PLU.queryBarCode(number);
                        if (plu == null)
                            plu = PLU.queryByInStoreCode(number);
                        if (plu != null && plu.getItemtype().intValue() != 10){
                        	plu = null; // 从plu.itemtype中判断出它不是期刊商品, 当成不存在该商品  
                        	//dtlcode1=""; // 从plu.itemtype中判断出它不是期刊商品, dtlcode1 不填入，当成非期刊商品处理  
                        }
    				}
                }
            } catch (JposException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        } else if (event.getSource() instanceof EnterButton
            || event.getSource() instanceof InStoreCodeButton
            || event.getSource() instanceof BarCodeButton) {
            plu = PLU.queryBarCode(itemNumber.toString());
            if (plu == null)
                plu = PLU.queryByInStoreCode(itemNumber.toString());
            if (plu == null) {
                plu = processScaleBarCode(itemNumber.toString());
                if (plu != null)
                    isWeighted = true;
            }
	    	if (plu != null && plu.getItemtype().intValue() == 10 ){
	    		plu = null; // 如果标记是期刊商品，则不正确, 因为期刊商品必须输入15码 
	    	}
            if (plu == null) {
				if (itemNumber.length() == 15) {
					// 期刊
					dtlcode1 = itemNumber.substring(13, 15);
					String itemNumberS = itemNumber.substring(0, 13);
	                plu = PLU.queryBarCode(itemNumberS);
	                if (plu == null)
	                    plu = PLU.queryByInStoreCode(itemNumberS);
                    if (plu != null && plu.getItemtype().intValue() != 10){
                    	plu = null; // 从plu.itemtype中判断出它不是期刊商品, 当成不存在该商品  
                    	//dtlcode1=""; // 从plu.itemtype中判断出它不是期刊商品, dtlcode1 不填入，当成非期刊商品处理  
                    }
				}
            }
        }

        if (plu == null) {
            itemNumber.setLength(0);
            return InventoryIdleState.class;
        }
        try {
            currentInventory = new Inventory();
            currentInventory.setStoreID(app.getSystemInfo().getStoreNumber());
            currentInventory.setBusiDate(getBusinessDate());
            currentInventory.setPOSNumber(Integer.parseInt(app.getSystemInfo().getTerminalID()));
            currentInventory.setItemSequence(Inventory.queryMaxItemSequence() + 1);
            currentInventory.setStockNumber(InventoryStockState.getStockNumber());
            currentInventory.setItemNumber(plu.getItemNumber());
            currentInventory.setDescription(plu.getScreenName());
            currentInventory.setUnitPrice(plu.getUnitPrice());
            currentInventory.setUploadState("0");
            currentInventory.setUpdateUserID(InventoryState.getCashierNumber());
            currentInventory.setUpdateDateTime(new Date());
            currentInventory.setDtlcode1(dtlcode1);
            if (isWeighted) {
                currentInventory.setActStockQty(this.weightQuantity);
                currentInventory.setAmount(this.weightAmount);
            } else {
                currentInventory.setActStockQty(new HYIDouble(0));
                currentInventory.setAmount(new HYIDouble(0));
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
            CreamToolkit.haltSystemOnDatabaseFatalError(e);
        }

        app.getDacViewer().addDacToDacList(currentInventory);

        if (isWeighted)
            return InventoryStoreState.class;
        else if (plu.isOpenPrice())
            return InventoryOpenPriceState.class;
        else
            return InventoryQuantityState.class;
    }

    public static void clearItemNumber() {
        itemNumber.setLength(0);
    }

    private Date getBusinessDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    public static Inventory getCurrentInventory() {
        return currentInventory;
    }

    public PLU processScaleBarCode(String number) {
    	PLU plu = null;
	        //System.out.println("number : " + number);
		java.util.Map formatMap = app.getScaleBarCodeFormat();
		if (formatMap == null)
			return plu;
		for (Iterator it = formatMap.keySet().iterator(); it.hasNext();) {
	        try {
	            int totalLength = Integer.parseInt((String) it.next());
	            java.util.List formatList =  
	            		(java.util.List) formatMap.get(String.valueOf(totalLength));
	            if (formatList == null || formatList.isEmpty())
	                continue;
	            if (number.length() != totalLength)
	                continue;
	            String pref = (String) formatList.get(0);
	            if (!number.startsWith(pref)) {
	                continue;
	            }
	
	            int iStart = ((Integer) formatList.get(1)).intValue();
	            int iEnd   = ((Integer) formatList.get(2)).intValue();
	            int wStart = -1;
	            int wEnd = -1;
	            int wComma = -1;
	            int pStart = -1;
	            int pEnd = -1;
	            int pComma = -1;
	            String itemNo = number.substring(iStart, iEnd);
	            HYIDouble wCount = null;
	            HYIDouble pCount = null;
	            
	            // X-plu    Q-重量   B-金额
	            if (totalLength == 8) { 
	            	// 8码:  2XXXXXXC 自编码 价格从plu取
	            } else if (totalLength == 13) { 
	            	// 13码: 2XXXXXXCBBBBBC 价格从条码中取
		            pStart = ((Integer) formatList.get(6)).intValue();
		            pEnd = ((Integer) formatList.get(7)).intValue();
		            pComma = ((Integer) formatList.get(8)).intValue();
		            String price = number.substring(pStart, pEnd);
		            price = price.substring(0, pComma) + "." 
		                + price.substring(pComma, price.length());
		            pCount = new HYIDouble(price);
		            pCount = pCount.setScale(2, BigDecimal.ROUND_HALF_UP);
	            } else if (totalLength == 18) {
	            	// 18码: 2XXXXXXCQQQQQBBBBBC 重量,价格从条码中取
		            wStart = ((Integer) formatList.get(3)).intValue();
		            wEnd = ((Integer) formatList.get(4)).intValue();
		            wComma = ((Integer) formatList.get(5)).intValue();
		            pStart = ((Integer) formatList.get(6)).intValue();
		            pEnd = ((Integer) formatList.get(7)).intValue();
		            pComma = ((Integer) formatList.get(8)).intValue();
		            String weight = number.substring(wStart, wEnd);
		            weight = weight.substring(0, wComma) + "." 
		                + weight.substring(wComma, weight.length());
		            String price = number.substring(pStart, pEnd);
		            price = price.substring(0, pComma) + "." 
		                + price.substring(pComma, price.length());
		            wCount = new HYIDouble(weight);
		            wCount = wCount.setScale(2, BigDecimal.ROUND_HALF_UP);
		            pCount = new HYIDouble(price);
		            pCount = pCount.setScale(2, BigDecimal.ROUND_HALF_UP);
	            }
	            plu = PLU.queryByInStoreCode(itemNo);
	            break;
	            /*
	            sub(plu);
	            LineItem lineItem = curTransaction.getCurrentLineItem();
	            lineItem.setWeight(wCount);
	            lineItem.setAmount(pCount);
	            */
	        } catch (Exception e) {
	            //e.printStackTrace(CreamToolkit.getLogger());
	        }
		} 
        return plu;
    }
}
