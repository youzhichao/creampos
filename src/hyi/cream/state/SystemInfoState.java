// Copyright (c) 2000 hyi
package hyi.cream.state;

import java.util.*;

import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * Displaying VM information.
 * <P>
 * @author dai
 */

public class SystemInfoState extends State implements PopupMenuListener {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private PopupMenuPane p = app.getPopupMenuPane();

    static SystemInfoState systemInfoState = null;

    public static SystemInfoState getInstance() {
        try {
            if (systemInfoState == null) {
                systemInfoState = new SystemInfoState();
            }
        } catch (InstantiationException ex) {
        }
        return systemInfoState;
    }

    /**
     * Constructor
     */
    public SystemInfoState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        System.gc();
        Properties pro = System.getProperties();
        ArrayList menu = new ArrayList();
        ResourceBundle res = CreamToolkit.GetResource();
        menu.add(res.getString("ApplicatoinVersion") + Version.getVersion() + " " + Version.getVersionDate());
        menu.add("");
        menu.add(res.getString("OSName") + pro.get("os.name"));
        menu.add(res.getString("OSKernelVersion") + pro.get("os.version"));
        menu.add(res.getString("JVMVersion") + pro.get("java.vm.version"));
        menu.add(res.getString("TotalMemory") + (Runtime.getRuntime().totalMemory() / 1024) + "KB");
        menu.add(res.getString("FreeMemory") + (Runtime.getRuntime().freeMemory() / 1024) + "KB");
        p.setMenu(menu);
        p.setVisible(true);
        p.setPopupMenuListener(this);
        p.setInputEnabled(false);
        app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("Warning"));
    }

	public Class exit(EventObject event, State sinkState) {
        p.setVisible(false);
        p.setInputEnabled(true);
        return sinkState.getClass();
	}

    public void menuItemSelected() {
        POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
        POSButtonHome2.getInstance().buttonPressed(e);
    }
}


