
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Member;
import hyi.cream.util.CreamToolkit;

public class GetMemberNumberState extends GetSomeAGState {
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static GetMemberNumberState getMemberNumberState = null;
    static private Member member;

    public static GetMemberNumberState getInstance() {
        try {
            if (getMemberNumberState == null) {
                getMemberNumberState = new GetMemberNumberState();
            }
        } catch (InstantiationException ex) {
        }
        return getMemberNumberState;
    }

    public GetMemberNumberState() throws InstantiationException {
    }

    public boolean checkValidity() {
        String memberID = getAlphanumericData();
        
        member = Member.queryByMemberID(memberID);
        setMember(member);
        if (member == null) {
        	setWarningMessage(CreamToolkit.GetResource().getString("WrongMemberID"));
            return false;
        } else {
            app.getCurrentTransaction().setMemberID(member.getMemberCardID());
            app.getCurrentTransaction().setMemberEndDate(member.getMemberEndDate());
            app.getCurrentTransaction().setBeforeTotalRebateAmt(member.getMemberActionBonus());
            return true;
        }
    }
    
    public Class getUltimateSinkState() {
        return ShowMemberInfoState.class;
    }

    public Class getInnerInitialState() {
        return MemberState.class;
    }

    public static Member getMember() {
        return member;
    }

    public static void setMember(Member member) {
        GetMemberNumberState.member = member;
    }
}

 