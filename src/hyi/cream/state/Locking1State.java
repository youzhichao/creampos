package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;

public class Locking1State extends State {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private static Locking1State instance = new Locking1State();

    private Locking1State() {}

    public static Locking1State getInstance() {
        return instance;
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof IdleState)
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("LockAndCancle"));

    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof ClearButton) {
            return IdleState.class;
        }

        if (event.getSource() instanceof NumberButton) {
            NumberButton nb = (NumberButton)event.getSource();
            if (nb.getNumberLabel().equalsIgnoreCase("1")) {
                app.getMessageIndicator().setMessage(nb.getNumberLabel());
            }
            if (nb.getNumberLabel().equalsIgnoreCase("2")) {
                app.getMessageIndicator().setMessage(nb.getNumberLabel());
            }
        }

        if (event.getSource() instanceof EnterButton) {
            if (app.getMessageIndicator().getMessage().trim().equalsIgnoreCase("1")) {
                return LockingState.class;
            }
            if (app.getMessageIndicator().getMessage().trim().equalsIgnoreCase("2")) {
                return IdleState.class;
            }
        }
        return IdleState.class;
    }

}