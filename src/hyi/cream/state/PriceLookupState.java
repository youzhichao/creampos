/**
 * State class
 * @since 2000
 * @author slackware
 */
// Copyright (c) 2000 HYI

package hyi.cream.state;

import java.util.*;

import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

public class PriceLookupState extends State {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static PriceLookupState priceLookupState = null;

    public static PriceLookupState getInstance() {
        try {
            if (priceLookupState == null) {
                priceLookupState = new PriceLookupState();
            }
        } catch (InstantiationException ex) {
        }
        return priceLookupState;
    }

    /**
     * Constructor
     */
    public PriceLookupState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        //System.out.println("PriceLookupState entry");
        Object eventSource = event.getSource();
		if ((sourceState instanceof IdleState && eventSource instanceof InquiryButton)
            || (sourceState instanceof Numbering4State && eventSource instanceof ClearButton)) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PriceLookMessage"));
        }
	}

	public Class exit(EventObject event, State sinkState) {
        //System.out.println("priceLookupState exit");
        Object eventSource = event.getSource();
		if (sinkState instanceof IdleState && eventSource instanceof ClearButton) {
		    app.getWarningIndicator().setMessage("");
        }
	    if (sinkState != null) {
            return sinkState.getClass();
        } else {
			return null;
        }
	}
}


