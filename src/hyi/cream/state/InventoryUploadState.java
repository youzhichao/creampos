package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;


public class InventoryUploadState extends State { //implements SendObjectCallBack {
    private static InventoryUploadState InventoryUploadState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
//    private ResourceBundle res = CreamToolkit.GetResource();
    private String inputNumber = "";
    private Class originalState = null;

    public static InventoryUploadState getInstance() {
        try {
            if (InventoryUploadState == null) {
                InventoryUploadState = new InventoryUploadState();
            }
        } catch (InstantiationException ex) {
        }
        return InventoryUploadState;
    }

    public InventoryUploadState() throws InstantiationException {
    }

    //public void callBack(String message) {
    //    app.getMessageIndicator().setMessage("盘点数据发送中，请稍侯... 传送笔数：" + message);
    //}

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            inputNumber = inputNumber + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(inputNumber);
            return;
        } else if (sourceState.getClass().getName().indexOf("KeyLock") != -1) {
            originalState = sourceState.getClass();
        } else if (sourceState.getClass().getName().indexOf("InventoryExitState") != -1) {
			originalState =InventoryState.getOriginalState();        	
        }       	
		if (inputNumber.length() == 0)
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("SelectUploadInventoryType"));
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            if (inputNumber.equals("1") || inputNumber.equals("2")) {
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("WaitForInventoryUpload"));
                try {
                    //Client.getInstance().setSendObjectCallBack(this);
                    
//					if (!ifSingleDoingInventory()) {
//						canNotDo = true;
//						try {
//							Thread.sleep(3000);
//						} catch (InterruptedException e) {
//							e.printStackTrace(CreamToolkit.getLogger());
//						}
//						return;
//					} else { 
//						canNotDo = false;
//					}
//                    
//                                       
                    if (inputNumber.equals("1"))
                        Client.getInstance().processCommand("putInventory");
                    else 
		                Client.getInstance().processCommand("putAllInventory");
					if (!Client.getInstance().compareInventoryRecCount()) {
						app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("PleaseRetryUpLoadInventory"));
						app.getMessageIndicator().setMessage("");
					} else {
						app.getWarningIndicator().setMessage("");				
						app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("UploadInventorySuccess"));
						String t = Client.getInstance().turnSingleUlTable("posul_inventory");
						if (t != null && t.equals("OK")) 
							app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("ImportInventorySuccess")); 
						else 
							app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("ImportInventoryFail")); 
					}				   
                } catch (ClientCommandException e) {
                    app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("LunchInventoryFail"));
                }
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e2) {
                	e2.printStackTrace(CreamToolkit.getLogger());
                }
                //finally {
                //    Client.getInstance().setSendObjectCallBack(null);
                //}
                inputNumber = "";
                return originalState;
            } else {
                inputNumber = "";
                return InventoryUploadState.class;
            }
        }
        if (event.getSource() instanceof ClearButton) {
            return originalState;
        }
        if (sinkState != null) {
			return sinkState.getClass();
        }
		return InventoryUploadState.class;
    }
    
//	private boolean ifSingleDoingInventory() {
//		boolean result = false;
////		try {
//			int count = -1;
//			count = Client.getInstance().countDoingInventory();
//			if (count == 1 ) {	          // Server have single pandin Inventory
//				app.getWarningIndicator().setMessage("");					
//				result = true;
//			} else if (count == 0 ) {	  // Server have not init pandin Inventory yet
//				app.getWarningIndicator().setMessage("后台未<盘点初始化>，请到后台做<盘点初始化>");
//				app.getMessageIndicator().setMessage("按 清除键 返回");
//			} else {                     // Something wrong on tables inventoryhead 
//				app.getWarningIndicator().setMessage("数据异常，无法盘点");
//				//连接错误 或 后台数据库inventoryhead表异常。
//				app.getMessageIndicator().setMessage("按 清除键 返回");				
//			}
////		} catch (ClientCommandException e) {
////		} finally {
//			return result;    	
////		}
//	}   
    
}
