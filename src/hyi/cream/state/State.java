package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.groovydac.Param;
import hyi.cream.event.TransactionEvent;
import hyi.cream.util.CreamToolkit;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.LineItem;

import java.util.EventObject;
import java.util.List;
import java.text.MessageFormat;

/**
 * The super class of all real state.
 */
public abstract class State {

    transient protected static final Param PARAM = Param.getInstance();

    private String warningMessage = "";
    abstract public void entry (EventObject event, State sourceState);

    abstract public Class exit(EventObject event, State sinkState);

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }

    /**
     * Convenience method for getting POSTerminalApplication instance.
     */
    public POSTerminalApplication getApp() {
        return POSTerminalApplication.getInstance();
    }

    /**
     * Convenience method for getting current transaction.
     */
    protected Transaction getCurrentTransaction() {
        return POSTerminalApplication.getInstance().getCurrentTransaction();
    }

    /**
     * Convenience method for getting all line items.
     */
    protected List<LineItem> getLineItems() {
        return POSTerminalApplication.getInstance().getCurrentTransaction().lineItems();
    }

    /**
     * Convenience method for showing informational message.
     */
    protected void showMessage(String messageId, Object... params) {
        POSTerminalApplication.getInstance().getMessageIndicator().setMessage(
            CreamToolkit.getString(messageId, params));
    }

    /**
     * Convenience method for showing warning message.
     */
    protected void showWarningMessage(String messageId, Object... params) {
        POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
            CreamToolkit.getString(messageId, params));
    }

    /**
     * Convenience method for clearing informational message.
     */
    protected void clearMessage() {
        POSTerminalApplication.getInstance().getMessageIndicator().setMessage("");
    }

    /**
     * Convenience method for clearing warning message.
     */
    protected void clearWarningMessage() {
        POSTerminalApplication.getInstance().getWarningIndicator().setMessage("");
    }

    /**
     * Convenience method for getting POS number.
     */
    protected int getPOSNumber() {
        return PARAM.getTerminalNumber();
    }

    /**
     * Convenience method for firing transaction changed event.
     */
    protected void fireTransactionChanged() {
        Transaction trans = getCurrentTransaction();
        trans.fireEvent(new TransactionEvent(trans, TransactionEvent.TRANS_INFO_CHANGED));
    }

    /**
     * Convenience method for firing transaction changed event.
     */
    protected void fireTransactionChanged(int changedType) {
        Transaction trans = getCurrentTransaction();
        trans.fireEvent(new TransactionEvent(trans, changedType));
    }

    /**
     * Convenient method for getting string.
     */
    protected String m(String resourceId, Object... arguments) {
        return CreamToolkit.getString(resourceId, arguments);
    }
}