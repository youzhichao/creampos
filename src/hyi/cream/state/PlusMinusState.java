
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.LineItem;
import hyi.cream.exception.LineItemNotFoundException;
import hyi.cream.uibeans.MinusButton;
import hyi.cream.uibeans.PlusButton;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.HYIDouble;

import java.util.EventObject;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PlusMinusState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    private HYIDouble quantity         = new HYIDouble(0);
    private boolean warning             = false;
    static PlusMinusState plusMinusState = null;

    public static PlusMinusState getInstance() {
        try {
            if (plusMinusState == null) {
                plusMinusState = new PlusMinusState();
            }
        } catch (InstantiationException ex) {
        }
        return plusMinusState;
    }

    /**
     * Constructor
     */
    public PlusMinusState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        LineItem curLineItem = app.getCurrentTransaction().getCurrentLineItem();
        if (curLineItem == null) {
            return;
        }
        if (curLineItem.getPrinted()) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("NumberWarning"));
            setWarning(true);
        } else {
            quantity = app.getCurrentTransaction().getCurrentLineItem().getQuantity();
            if (event.getSource() instanceof PlusButton) {
                quantity = quantity.addMe(new HYIDouble(1));
            } else if (event.getSource() instanceof MinusButton) {
                quantity = quantity.subtract(new HYIDouble(1));
            }
            HYIDouble pluMaxQuantity = new HYIDouble(PARAM.getPluMaxQuantity());
            if (quantity.intValue() <= 0) {
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("number1"));
                setWarning(true);
            } else if (quantity.compareTo(pluMaxQuantity) == 1) {
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("number2") + pluMaxQuantity);
                setWarning(true);
            } else {
                curLineItem.setQuantity(quantity);
                try {
                    app.getCurrentTransaction().changeLineItem(-1, curLineItem);
                } catch (LineItemNotFoundException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("LineItem not found at " + this);
                }
                setWarning(false);
            }
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (getWarning()) {
            return WarningState.class;
        } else {
            return IdleState.class;
        }
    }

    public void setWarning(boolean warning) {
        this.warning = warning;
    }

    public boolean getWarning() {
        return warning;
    }
}

  
