package hyi.cream.state;

import hyi.cream.util.HYIDouble;

import java.util.Collection;
import java.util.Iterator;

/**
 * PluGrpPromMatch definition class.
 * 提供一个商品群组促销配对类
 * 
 * @author Meyer 
 * @version 1.01
 */
public class PluGrpPromMatch extends PromMatch {
    /**
     * 更新记录
     * 
     * Meyer / 1.0 / 2003-01-06
     * 		1. Create the class file
     * 
     * Meyer / 1.01 / 2003-01-09
     * 		1. 改为多个群组同时Match，促销成立
     * 
     * 
     */

    private Collection pluGrpSubMatchList;
	
	public PluGrpPromMatch(String aPromID, Collection aPluGrpSubMatchList) {
		this.setPromID(aPromID);
		
		this.setMatchedFlag(false);				
		this.setTotalAmt(new HYIDouble(0));
		
		this.pluGrpSubMatchList = aPluGrpSubMatchList;
		
		
		PluGrpSubMatch pluGrpSubMatch;
		Iterator iter = aPluGrpSubMatchList.iterator();
		while (iter.hasNext()) {
			pluGrpSubMatch = (PluGrpSubMatch)iter.next();
			if (pluGrpSubMatch.isMatched()) {
				this.setTotalAmt(
					this.getTotalAmt().addMe(pluGrpSubMatch.getTotalAmt())
				);
				this.setMatchedFlag(true);
			} else {
				this.setMatchedFlag(false); 
				break;
			}			
		}	
	}
	
	public Collection getPluGrpSubMatchList() {
		return this.pluGrpSubMatchList;
	}
		   

	public static void main(String[] args) {
	}
}
