package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.awt.Toolkit;
import hyi.cream.util.DbConnection;
import java.sql.SQLException;
import java.util.*;

public class CheckCashierPasswordState extends State {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    static CheckCashierPasswordState getCashierNumberState = null;
    private String pwd = "";
    private String display ="";
    private int times = 1;

    private void init () {
        pwd = "";
        display ="";
        //times = 0;
    }

    public static CheckCashierPasswordState getInstance() {
        try {
            if (getCashierNumberState == null) {
                getCashierNumberState = new CheckCashierPasswordState();
            }
        } catch (InstantiationException ex) {
        }
        return getCashierNumberState;
    }

    /**
     * Constructor
     */
    public CheckCashierPasswordState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("ShiftState2 Enter");
        app.getMessageIndicator().setMessage(display);
        if (sourceState instanceof GetCashierNumberState) {
            app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("Password"));
            return;
        } else if (sourceState instanceof CheckCashierPasswordState) {
            if (event.getSource() instanceof ClearButton || event.getSource() instanceof EnterButton)
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WrongPassword"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            app.getMessageIndicator().setMessage(res.getString("LookingAt"));
            Cashier cashierDAC = Cashier.CheckValidCashier(app.getCurrentCashierNumber(), pwd);
            init();
            if (cashierDAC == null) {
                Toolkit.getDefaultToolkit().beep();
                if (times >= 3) {
                    times = 1;
                    app.setCurrentCashierNumber("");
                    return InitialState.class;
                } else {
                    times ++;
                    return CheckCashierPasswordState.class;
                }
            } else {
                /*
                   Otherwise set CashierNumber in
                   CreamProperties, create/insert a new ShiftReport, meanwhile, check
                   to see if the current ZReport's SystemDataTime is '1970/01/01 00:00:00',
                   if it is not, create/insert a new ZReport and CategorySales. Finally,
                   clear messageIndicator and return IdleState.class.
                */
                synchronized (POSTerminalApplication.waitForTransactionStored) {    // -> see DrawerOpenState.java
                    
                    DbConnection connection = null;
                    try {
                        connection = CreamToolkit.getTransactionalConnection();
                    
                        PARAM.updateCashierNumber(app.getCurrentCashierNumber());

                        //TODO Refactoring to ZReport.isNotClosedYet()/ShiftReport.isNotClosedYet()
                        Date initDate = CreamToolkit.getInitialDate();
                        ZReport z = ZReport.getOrCreateCurrentZReport(connection);
                        if (CreamToolkit.compareDate(z.getAccountingDate(), initDate) != 0) {
                            z = ZReport.createZReport(connection);
                        }
                         ShiftReport.createShiftReport(connection);
                         // 如果sc,pos数据被清空,下面开班交易会获得－1的z序号,所以先把z save
                         connection.commit();

                        Transaction trans = app.getCurrentTransaction();
                        trans.clear();
                        trans.init();
                        trans.setDealType2("8");
                        trans.setInvoiceCount(0);
                        trans.setInvoiceID("");
                        trans.setInvoiceNumber("");
                        trans.store(connection);
                        //DacTransfer.getInstance().uploadTransaction(app.getCurrentTransaction());

                        //Bruce/20031121
                        //int nextInvoicNumber = Integer.parseInt(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
                        //int nextTranNumber = Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber"));
                        //CreamProperties.getInstance().setProperty("NextInvoiceNumber", Integer.toString(++nextInvoicNumber));
                        //CreamProperties.getInstance().setProperty("NextTransactionSequenceNumber", Integer.toString(++nextTranNumber));
                        //CreamProperties.getInstance().deposit();

                        String logMsg = CreamToolkit.GetResource().getString("CashierloginI") + " " + app.getCurrentCashierNumber() + " " + CreamToolkit.GetResource().getString("CashierloginII");
                        app.getMessageIndicator().setMessage(logMsg);
                        app.getWarningIndicator().setMessage("");
                        connection.commit();

                    } catch (SQLException e) {
                        CreamToolkit.logMessage(e);
                        CreamToolkit.haltSystemOnDatabaseFatalError(e);
                    } finally {
                        CreamToolkit.releaseConnection(connection);
                    }
                }
                return IdleState.class;
            }
        } else if (event.getSource() instanceof NumberButton) {
            NumberButton nb = (NumberButton)event.getSource();
            pwd += nb.getNumberLabel().trim();
            for (int i=0; i < nb.getNumberLabel().length(); i++)
                display += "*";
            return CheckCashierPasswordState.class;
        } else if (event.getSource() instanceof ClearButton) {
             if (pwd.equals("")) {
                init();
                   times = 1;
                   app.setCurrentCashierNumber("");
                return InitialState.class;
             } else {
                init();
                return CheckCashierPasswordState.class;
             }
        } else
            return CheckCashierPasswordState.class;
    }
}

