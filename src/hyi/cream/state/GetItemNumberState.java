
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class GetItemNumberState extends GetSomeAGState {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static GetItemNumberState GetItemNumberState = null;

    public static GetItemNumberState getInstance() {
        try {
            if (GetItemNumberState == null) {
                GetItemNumberState = new GetItemNumberState();
            }
        } catch (InstantiationException ex) {
        }
        return GetItemNumberState;
    }

    /**
     * Constructor
     */
    public GetItemNumberState() throws InstantiationException {
    }

	public boolean checkValidity() {
		String itemNo = this.getAlphanumericData();
		if (!checkInteger())
			return false;
		int max = app.getItemList().getDisplayedItemNo();
		//System.out.println(max + "&&&&&&&&&&&&&&&&&&&");
		if (Integer.parseInt(itemNo) <= max) {
			app.getItemList().setSelectedItem(Integer.parseInt(itemNo));
			return true;
		}
		else
			return false;
    }

	public Class getUltimateSinkState() {
	    return IdleState.class;
	}

	public Class getInnerInitialState() {
         return ItemState.class;
	}


}

 
