/*
 * Created on 2005-2-18
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package hyi.cream.game;

import hyi.cream.dac.GiftGroup;
import hyi.cream.dac.GiftRule;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.DbConnection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * @author yuki,allan
 */
public class DigPrize extends JPanel {
    private static final long serialVersionUID = 1L;

    private JButton[] butt;
	private JLabel label;
	private Random random;
	private int row = 0;
	private int column = 0;
	private int maxClickTimes = 3;
	private String beginImage;
	private String firstImage;
	private String secondImage;
	private String thirdImage;
	private List giftGroupList;
	private int firstPrize = 2;
	private int secondPrize = 4;
	private int thirdPrize = 6;
	private int maxFirstPrize = 10;
	private int maxSecondPrize = 20;
	private int maxThirdPrize = 30;
	private int firstPrizeCount = 0;
	private int secondPrizeCount = 0;
	private int thirdPrizeCount = 0;
	private int buttCount = 0;
	private int[] buttPrize;
	private int[] buttArr;
	private Panel buttPane = new Panel();
	private int digTimes;

	public DigPrize() {
	}

	public void initData(String id) {
        DbConnection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
			giftGroupList = new ArrayList();
			GiftRule rule = GiftRule.queryByMMID(connection, id);
			Iterator it = GiftGroup.getGiftGroups(connection, rule.getID());
			while (it.hasNext()) {
				giftGroupList.add(it.next());
			}
			System.out.println(giftGroupList);
			setRow(rule.getYDimension().intValue());
			setColumn(rule.getXDimension().intValue());
			setMaxClickTimes(rule.getRetryTimes().intValue());
			// setBeginImage(root + File.separator + sButtBeginImage);
			// setFirstImage(root + File.separator + sFirstImage);
			// setSecondImage(root + File.separator + sSecondImage);
			// setThirdImage(root + File.separator + sThirdImage);
			setFirstPrize(rule.getGroup1Scale().intValue());
			setSecondPrize(rule.getGroup2Scale().intValue());
			setThirdPrize(rule.getGroup3Scale().intValue());
			setMaxFirstPrize(rule.getGroup1Scale().intValue());
			setMaxSecondPrize(rule.getGroup2Scale().intValue());
			setMaxThirdPrize(rule.getGroup3Scale().intValue());
        } catch (SQLException e) {
            CreamToolkit.logMessage(e);
        } finally {
            CreamToolkit.releaseConnection(connection);
        }
	}

	public void setRow(int rowNumber) {
		this.row = rowNumber;
	}

	public int getRow() {
		return this.row;
	}

	public void setColumn(int columnNumber) {
		this.column = columnNumber;
	}

	public int getColumn() {
		return this.column;
	}

	public void setMaxClickTimes(int maxClickTimesNumber) {
		this.maxClickTimes = maxClickTimesNumber;
	}

	public int getMaxClickTimes() {
		return this.maxClickTimes;
	}

	public void setBeginImage(String beginImageStr) {
		this.beginImage = beginImageStr;
	}

	public String getBeginImage() {
		return this.beginImage;
	}

	public void setFirstImage(String firstImageStr) {
		this.firstImage = firstImageStr;
	}

	public String getFirstImage() {
		return this.firstImage;
	}

	public void setSecondImage(String secondImageStr) {
		this.secondImage = secondImageStr;
	}

	public String getSecondImage() {
		return this.secondImage;
	}

	public void setThirdImage(String thirdImageStr) {
		this.thirdImage = thirdImageStr;
	}

	public String getThirdImage() {
		return this.thirdImage;
	}

	public void setFirstPrize(int firstPrizeNumber) {
		this.firstPrize = firstPrizeNumber;
	}

	public int getFirstPrize() {
		return this.firstPrize;
	}

	public void setSecondPrize(int secondPrizeNumber) {
		this.secondPrize = secondPrizeNumber;
	}

	public int getSecondPrize() {
		return this.secondPrize;
	}

	public void setThirdPrize(int thirdPrizeNumber) {
		this.thirdPrize = thirdPrizeNumber;
	}

	public int getThirdPrize() {
		return this.thirdPrize;
	}

	public void setMaxFirstPrize(int maxFirstPrizeNumber) {
		this.maxFirstPrize = maxFirstPrizeNumber;
	}

	public int getMaxFirstPrize() {
		return this.maxFirstPrize;
	}

	public void setMaxSecondPrize(int maxSecondPrizeNumber) {
		this.maxSecondPrize = maxSecondPrizeNumber;
	}

	public int getMaxSecondPrize() {
		return this.maxSecondPrize;
	}

	public void setMaxThirdPrize(int maxThirdPrizeNumber) {
		this.maxThirdPrize = maxThirdPrizeNumber;
	}

	public int getMaxThirdPrize() {
		return this.maxThirdPrize;
	}

	public GiftGroup fireButtonEvent(int index) {
		if (index >= 0 && index <= butt.length) {
			return buttonAction(butt[index]);
		}
		return null;
	}

	class ButtListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			buttonAction((JButton) e.getSource());
		}
	}

	private GiftGroup buttonAction(JButton jb) {
		GiftGroup ret = null;
		if (jb.getActionCommand().equalsIgnoreCase(((GiftGroup)giftGroupList.get(0)).getItemNO())) {
			firstPrizeCount++;
			// butt[i].setIcon(null);
			// butt[i].setIcon(new ImageIcon(firstImage));
			System.out.println("you win the " + ((GiftGroup)giftGroupList.get(0)).getItemNO() + " and GAME OVER !");
			label.setText("you win the " + ((GiftGroup)giftGroupList.get(0)).getItemNO() + "!");
			jb.setText("Win " + ((GiftGroup)giftGroupList.get(0)).getItemNO());
			jb.setEnabled(false);
			jb.setActionCommand("no prize");
			ret = (GiftGroup)giftGroupList.get(0);
		} else if (jb.getActionCommand().equalsIgnoreCase(((GiftGroup)giftGroupList.get(1)).getItemNO())) {
			secondPrizeCount++;
			// butt[i].setIcon(null);
			// butt[i].setIcon(new ImageIcon(secondImage));
			System.out.println("you win the " + ((GiftGroup)giftGroupList.get(1)).getItemNO() + "  and GAME OVER !");
			label.setText("you win the " + ((GiftGroup)giftGroupList.get(1)).getItemNO() + "!");
			jb.setText("Win " + ((GiftGroup)giftGroupList.get(1)).getItemNO());
			jb.setEnabled(false);
			jb.setActionCommand("no prize");
			ret = (GiftGroup)giftGroupList.get(1);
		} else if (jb.getActionCommand().equalsIgnoreCase(((GiftGroup)giftGroupList.get(2)).getItemNO())) {
			thirdPrizeCount++;
			// butt[i].setIcon(null);
			// butt[i].setIcon(new ImageIcon(thirdImage));
			System.out.println("you win the " + ((GiftGroup)giftGroupList.get(2)).getItemNO() + "  and GAME OVER !");
			label.setText("you win the " + ((GiftGroup)giftGroupList.get(2)).getItemNO() + "!");
			jb.setText("Win " + ((GiftGroup)giftGroupList.get(2)).getItemNO());
			jb.setEnabled(false);
			jb.setActionCommand("no prize");
			ret = (GiftGroup)giftGroupList.get(2);
		} else if (jb.getActionCommand().equalsIgnoreCase("no prize")) {
			digTimes++;
			System.out.println(digTimes);
			// butt[i].setIcon(null);
			jb.setText("Welcome");
			jb.setEnabled(false);
			label.setText("nothing !");
			noPrizeResult();
		}
		return ret;
	}

	private void noPrizeResult() {
		if (digTimes >= maxClickTimes) {
			for (int i = 0; i < butt.length; i++) {
				// butt[i].setIcon(null);
				butt[i].setEnabled(false);
			}
			label.setText("you win nothing and  GAME OVER !");
		}
	}

	public void initButtPane() {
		buttPane.removeAll();
		butt = null;

		buttPane.setLayout(new GridLayout(row, column));
		buttCount = row * column;
		butt = new JButton[buttCount];

		for (int i = 0; i < buttCount; i++) {
			butt[i] = createButton(String.valueOf(i));
     		// butt[i].setIcon(new ImageIcon(beginImage));
			// butt[i].setText();
			butt[i].addActionListener(new ButtListener());
			butt[i].setActionCommand("no prize");
			buttPane.add(butt[i]);
		}
		random = new Random();
		int flagCount = 0;
		System.out.println("----------- buttCount : " + buttCount);
		List tmp = new ArrayList();
		for (int i = 0; i < buttCount; i++) {
			tmp.add(new Integer(i));
		}
		buttArr = new int[buttCount];
		while (flagCount < buttCount) {
			int index = 0;
			if (buttCount - flagCount - 1 > 0)
				index = random.nextInt(buttCount - flagCount - 1);
			System.out.println(flagCount + " | tmp.szie : " + tmp.size());
			int rand = ((Integer) tmp.get(index)).intValue();
			tmp.remove(index);
			System.out.println(rand + "." + flagCount);

			if (!butt[rand].getActionCommand().equals("no prize"))
				continue;
			if (firstPrize > 0) {
				firstPrize--;
				butt[rand].setActionCommand(((GiftGroup)giftGroupList.get(0)).getItemNO());
				flagCount++;
			} else if (secondPrize > 0) {
				secondPrize--;
				butt[rand].setActionCommand(((GiftGroup)giftGroupList.get(1)).getItemNO());
				flagCount++;
			} else if (thirdPrize > 0) {
				thirdPrize--;
				butt[rand].setActionCommand(((GiftGroup)giftGroupList.get(2)).getItemNO());
				flagCount++;
			} else {
				flagCount++;
			}
			System.out.println("firstPrize : " + firstPrize
					+ " | secondPrize : " + secondPrize + " | thirdPrize : "
					+ thirdPrize + " | flagCount : " + flagCount);
			buttArr[rand] = rand;
		}

		for (int i = 0; i < butt.length; i++) {
			System.out.println(i + " : " + butt[i].getActionCommand() + " ");
		}
	}

	private JButton createButton(String label) {
		Color bgColor = new Color(195, 208, 255);
		JButton tt = new JButton(label);
		tt.setFont(new Font(CreamToolkit.GetResource().getString("Song"),
				Font.PLAIN, 14));
		tt.setBackground(bgColor);
		return tt;
	}

	public void init() {
		this.setLayout(new BorderLayout());
		label = new JLabel("Welcome to here !");
		label.setPreferredSize(new Dimension(this.getWidth(), 50));
		label.setFont(new Font("SimHei", Font.BOLD, 16));
		label.setBackground(new Color(0xff384779));
		label.setForeground(Color.black);
		this.add(label, BorderLayout.NORTH);
		this.add(buttPane, BorderLayout.CENTER);
	}

	public void setMessage(String msg) {
		label.setText(msg);
		label.repaint();
	}
}
