package hyi.cream;

import hyi.cream.dac.Transaction;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CreamSession {

    private static CreamSession instance = new CreamSession();

    private WorkingStateEnum workingState;

    private Map<WorkingStateEnum, Map<String, Object>> session = Collections.synchronizedMap(new HashMap<WorkingStateEnum, Map<String, Object>>());

    // Session data within Transaction lifetime --
    Transaction transactionToBeRefunded;

    private boolean doingShiftBeforeZ;

    private CreamSession() {
    }

    public static CreamSession getInstance() {
        return instance;
    }
    
    public Map<String, Object> getAttributes(WorkingStateEnum workingState) {
        return session.get(workingState);
    }
    
    public Object getAttribute(WorkingStateEnum workingState, String attribute) {
        //return session.get(workingState).get(attribute);
        Map<String, Object> map = session.get(workingState);
        if (map != null){
            return map.get(attribute);
        } else {
            return null;
        }
    }
    
    public void setAttribute(WorkingStateEnum workingState, String attribute, Object value) {
        Map<String, Object> attributes = session.get(workingState);
        if (attributes == null) {
            attributes = new HashMap<String, Object>();
        }
        attributes.put(attribute, value);
        session.put(workingState, attributes);
    }

    public void removeAttributes(WorkingStateEnum workingState) {
        session.remove(workingState);
    }
    
    public void removeAll() {
        session.clear();
    }
    
    public WorkingStateEnum getWorkingState() {
        return workingState;
    }

    public void setWorkingState(WorkingStateEnum workingState) {
        System.out.println("CremSession.setWorkingState(" + workingState.toString() + ")");
        this.workingState = workingState;
    }

    public void clearTransactionSessionData() {
        transactionToBeRefunded = null;
    }

    public Transaction getTransactionToBeRefunded() {
        return transactionToBeRefunded;
    }

    public void setTransactionToBeRefunded(Transaction transactionToBeRefunded) {
        this.transactionToBeRefunded = transactionToBeRefunded;
    }

    public boolean isDoingShiftBeforeZ() {
        return doingShiftBeforeZ;
    }

    public void setDoingShiftBeforeZ(boolean doingShiftBeforeZ) {
        this.doingShiftBeforeZ = doingShiftBeforeZ;
    }
}
