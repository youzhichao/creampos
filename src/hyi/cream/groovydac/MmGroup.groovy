package hyi.cream.groovydac

/**
 * Entity class for mmgroup.
 *
 * @author Bruce You
 * @since 2009/2/20 14:25:13
 */
@Table (nameAtPOS = 'mmgroup', nameAtServer = 'posdl_mmgroup')
public class MmGroup extends GroovyEntity {
    @PrimaryKey String mmid // character(3) NOT NULL DEFAULT '0'::bpchar,
    @PrimaryKey String groupno // character(1) NOT NULL,
    @PrimaryKey String item // character(20) NOT NULL,
}