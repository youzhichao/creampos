package hyi.cream.groovydac

import hyi.cream.gwt.client.device.ParamData
import hyi.cream.gwt.client.device.ParamValues
import hyi.cream.util.ChineseConverter
import hyi.cream.util.CreamToolkit
import hyi.cream.util.HYIDouble
import org.apache.commons.lang.StringUtils
import org.codehaus.groovy.runtime.InvokerHelper

import java.lang.reflect.Field
import java.text.DecimalFormat

/**
 * System parameters from table 'property'.
 *
 * @author Bruce You
 * @since Mar 3, 2009 5:11:24 PM
 */
public class Param implements GroovyInterceptable {

    // Singleton ----------------------------------------
    private static Param instance = new Param()
    private Param() { init() }
    public static Param getInstance() { return instance }
    //---------------------------------------------------

    boolean _initialized = false
    boolean _traditional = false // 正體中文

    void setProperty(String name, Object value) {
        try {
            getMetaClass().setProperty(this, name, value)
        } catch (Exception e) {
            name = StringUtils.uncapitalize(name)
            getMetaClass().setProperty(this, name, value)
        }

        if (_initialized) {
            // update into database
            def field = this.class.getDeclaredField(name)
            def annot = field.getAnnotation(ParamAnnotation.class)
            def dbPropertyName = annot.name()
            def prop = new Property(name: dbPropertyName, value: value)
            prop.setAllFieldsDirty(true)
            prop.present()
            println "Param.setProperty(): Set '${dbPropertyName}' to '${value}'."
        }
    }

    /**
     * 當Grooy program使用Param.instance.setXyz()來賦值時會利用這個方法
     * 走setProperty() hook.
     */
    Object invokeMethod(String methodName, Object args) {
        if (methodName.startsWith("set") && methodName.length() > 3) {
            System.out.println "invokeMethod(): ${methodName}(${args[0]})"
            def paramName = methodName[3].toLowerCase() +
                (methodName.length() > 4 ? methodName[4..-1] : '')
            this."${paramName}" = args[0]
            return
        }

        return InvokerHelper.getMetaClass(this).invokeMethod(this, methodName, args);
    }

    /**
     * Load property values from table 'property' to Param instance's properties.
     */
    private init() {
        def propMap = Property.queryAll()
        if (propMap == null || propMap.isEmpty())
            return
        for (Field field in this.class.getDeclaredFields()) {
            String propertyName = field.name
            if (propertyName.startsWith('_') || propertyName.contains('$'))
                continue
            if (['metaClass', 'class', 'instance'].contains(propertyName))
                continue

            def annot = field.getAnnotation(ParamAnnotation.class)
            def defaultValue = annot.defaultValue()
            def dbPropName = annot.name();
            def valueType = field.getType()
            def dbValue = propMap[dbPropName]
            if (dbValue != null) {
                if (valueType.isInstance(dbValue))
                    this."${propertyName}" = dbValue
                else
                    this."${propertyName}" = cast(dbValue, valueType)
            } else {
                if (valueType.isInstance(defaultValue))
                    this."${propertyName}" = defaultValue
                else
                    this."${propertyName}" = cast(defaultValue, valueType)
            }
        }
        _initialized = true
        _traditional = this.locale.contains("TW")
    }

    private cast(value, valueType) {
        if (value instanceof String && (valueType == boolean.class || valueType == Boolean.class))
            return (value.equalsIgnoreCase('yes') || value.equalsIgnoreCase('true'))
        else if (value instanceof String && valueType == HYIDouble.class)
            return new HYIDouble(value); 
        else if (value instanceof Integer && valueType == HYIDouble.class)
            return new HYIDouble(value.toString());
        else if (value instanceof Float && valueType == HYIDouble.class)
            return new HYIDouble(value.toString());
        else if (value instanceof String && valueType == int.class)
            return value.toInteger(); 

        return value.asType(valueType)
    }

    private hyiDoubleToFloat(value) {
        if (value instanceof HYIDouble)
            return new Float(value.toString());
        else
            return value;
    }

    private castFromString(value, valueType) {
        if (valueType == boolean.class || valueType == Boolean.class)
            return (value.equalsIgnoreCase('yes') || value.equalsIgnoreCase('true'))
        else if (valueType == HYIDouble.class)
            return ((HYIDouble)value).toBigDecimal().floatValue();
        else if (valueType == int.class)
            return value.toInteger();

        return value.asType(valueType)
    }

    /** Service method for CreamGWT. */
    public ParamData retrieveAll() {
        ParamData params = new ParamData()
        Map<String, Map<String, ParamValues>> paramData = params.getParamData()
        def chineseConverter = ChineseConverter.instance
        for (Field field in this.class.getDeclaredFields()) {
            String propertyName = field.name
            if (propertyName.startsWith('_') || propertyName.contains('$'))
                continue
            if (['metaClass', 'class', 'instance'].contains(propertyName))
                continue

            def annot = field.getAnnotation(ParamAnnotation.class)
            def category = annot.category().toString()
            def dbPropName = annot.name()
            def description = chineseConverter.convert(annot.description())
            def comment = chineseConverter.convert(annot.comment())
            def valueType = field.getType()
            def possibleValuesString = annot.possibleValues()
            def seq = annot.seq()

            Map<String, ParamValues> paramMap = paramData[category]
            if (paramMap == null) {
                paramMap = new HashMap<String, ParamValues>()
                paramData[category] = paramMap
            }

            ParamValues paramValues = new ParamValues();
            paramValues.name = dbPropName
            paramValues.fieldName = propertyName
            paramValues.description = description
            paramValues.comment = comment
            paramValues.value = hyiDoubleToFloat(this."${propertyName}")
            paramValues.seq = seq
            if (!StringUtils.isEmpty(possibleValuesString)) {
                def stringValues = possibleValuesString.split(/[,]+/)
                paramValues.possibleValues = new ArrayList();
                if (stringValues?.length > 0) {
                    for (option in stringValues)
                        paramValues.possibleValues << castFromString(option, valueType)
                }
            }
            paramMap[description] = paramValues
        }
        return params
    }

    public void updateParams(HashMap<String, Object> modifiedProperties) {
        def conn = CreamToolkit.getTransactionalConnection()
        modifiedProperties.each { key, value ->
            def field = this.class.getDeclaredField(key)
            def valueType = field.getType()
            value = cast(value, valueType)
            println "set ${key} to ${value}"
            this."${key}" = cast(value, valueType)
        }
        conn?.commit()
        conn?.close()
    }

    boolean tranditionalChinese() { _traditional }

    void switchBetweenTraditionalAndSimplifiedChinese() {
        _traditional = !_traditional
        ChineseConverter.instance.traditional = _traditional
    }

    ///////////////////////////////////////////////////////////////////////////////////
    // PARAMETER DECLARATIONS /////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'TerminalNumber',
        description = 'POS機號',
        comment = 'POS機號一般從1開始編。',
        defaultValue = '1',
        controlType = ControlType.EditBox,
        seq = 1)
    int terminalNumber

    private String _ssmpPosNo;
    String getSsmpPosNo() {
        if (_ssmpPosNo != null) return _ssmpPosNo
        _ssmpPosNo = new DecimalFormat('000').format(terminalNumber)
        return _ssmpPosNo
    }

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'TerminalName',
        description = 'POS機名稱',
        defaultValue = '',
        controlType = ControlType.EditBox,
        seq = 2)
    String terminalName

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'TerminalPhysicalNumber',
        description = '實體機號',
        comment = '實體機號記錄每台POS機的唯一序號，目前{program}僅供參考。',
        defaultValue = '',
        controlType = ControlType.EditBox,
        seq = 3)
    String terminalPhysicalNumber

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'CompanyName',
        description = '用戶公司名稱',
        comment = '可以用於顯示在畫面ScreenBanner上。',
        defaultValue = '"泓远便利店"',
        controlType = ControlType.EditBox,
        seq = 4)
    String companyName

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'welcomeMessage',
        description = '歡迎{message}',
        comment = '客戶顯示器顯示的歡迎{message}。',
        defaultValue = '',
        controlType = ControlType.EditBox,
        seq = 5)
    String welcomeMessage
    void updateWelcomeMessage(v) { instance.welcomeMessage = v }

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'SecondScreenMarquee',
        description = '歡迎{message}',
        comment = '客戶顯示器顯示的歡迎{message}。',
        defaultValue = '',
        controlType = ControlType.EditBox,
        seq = 5)
    String secondScreenMarquee
    void updateSecondScreenMarquee(v) { instance.secondScreenMarquee = v }

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'postype',
        description = 'POS種類',
        comment = '用來控制不同機型的特殊處理，可以不設。(如: tec7000, tec, wincor)',
        defaultValue = '',
        controlType = ControlType.EditBox,
        seq = 6)
    String posType

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'CheckDrawerClose',
        description = '關抽屜檢查',
        comment = '結帳時彈出抽屜後，是否檢查抽屜必需在一定時間內關閉。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 7)
    boolean checkDrawerClose

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'DrawerCloseTime',
        description = '抽屜關閉時間',
        comment = '檢查抽屜關閉的時間（秒）。',
        defaultValue = '20',
        controlType = ControlType.EditBox,
        seq = 8)
    int drawerCloseTime
    void updateDrawerCloseTime(v) { instance.drawerCloseTime = v }

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'ScanCashierNumber',
        description = '收銀員刷卡',
        comment = '是否在每筆交易之前刷收銀員條碼。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 9)
    boolean scanCashierNumber

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'ScanSalemanNumber',
        description = '銷售員刷卡',
        comment = '是否在每筆交易要刷銷售員條碼。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 10)
    boolean scanSalemanNumber

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'Locale',
        description = '界面語系',
        comment = '在畫面、{printer}、客顯上用的語系。',
        defaultValue = 'zh_TW',
        controlType = ControlType.ComboBox,
        possibleValues = "zh_TW,zh_CN",
        seq = 11)
    String locale

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'NeedCheckOutConfirm',
        description = '支付金額足額後，是否仍需再按一次[確認]結帳',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 12)
    boolean needCheckOutConfirm

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'AllowShePayment',
        description = '一般交易是否允許用賒帳支付',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox,
        seq = 13)
    boolean allowShePayment

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'ShePaymentID',
        description = '賒帳支付ID',
        defaultValue = '',
        controlType = ControlType.EditBox,
        seq = 14)
    String shePaymentID

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'ReturnVersion',
        description = '退貨系統',
        comment = '1:本機退貨,只能退本機交易、2:可跨台退貨,從SC獲得交易{data}',
        defaultValue = '2',
        controlType = ControlType.ComboBox,
        possibleValues = '1,2',
        seq = 15)
    String returnVersion

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'AllowPartialReturn',
        description = '允許做部分退貨',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox,
        seq = 16)
    boolean allowPartialReturn

    @ParamAnnotation(category = ParamCategory.S05System1,
        name = 'ReturnReasonPopup',
        description = '退貨時需輸入原因',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 17)
    boolean returnReasonPopup

    @ParamAnnotation(category = ParamCategory.S05System1,
            name = 'CMPAYAddress',
            description = '移动支付URL',
            defaultValue = 'http://211.136.101.107:7008/CMPay/CMPAYProcess',
            controlType = ControlType.EditBox,
            seq = 18)
    String CMPAYAddress

    @ParamAnnotation(category = ParamCategory.S05System1,
            name = 'CMPAYMerId',
            description = '移动支付商户编号',
            defaultValue = '',
            controlType = ControlType.EditBox,
            seq = 19)
    String CMPAYMerId

    @ParamAnnotation(category = ParamCategory.S05System1,
            name = 'CMPAYPIKFLG',
            description = '移动支付是否支持捡起支付',
            comment = '移动支付是否支持捡起支付，0是支持，1是不支持',
            defaultValue = '1',
            controlType = ControlType.ComboBox,
            possibleValues = '0,1',
            seq = 20)
    String CMPAYPIKFLG

    @ParamAnnotation(category = ParamCategory.S05System1,
            name = 'WeiXinDeviceInfo',
            description = '微信终端设备号',
            defaultValue = '123456',
            controlType = ControlType.EditBox,
            seq = 19)
    String weiXinDeviceInfo


    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'UseCashForm',
        description = '現金清點單輸入',
        comment = '交班時是否需要收銀員輸入現金清點單。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 1)
    boolean useCashForm

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'MixAndMatchVersion',
        description = '組合促銷版本',
        comment = '組合促銷版本1是使用mixandmatch/mmgroup表，2是使用combination*表。',
        defaultValue = '1',
        controlType = ControlType.ComboBox,
        possibleValues = '1,2,3',
        seq = 2)
    String mixAndMatchVersion

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'MixAndMatchPrompt',
        description = '顯示組合促銷商品提示',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 3)
    boolean mixAndMatchPrompt

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'InitialCashInAmount',
        description = '初始借零金額',
        comment = '收銀員Sign-On生成Shift帳時，固定自動添加的借零金額（零用金）。',
        defaultValue = '0',
        controlType = ControlType.EditBox,
        width = 100,
        seq = 4)
    HYIDouble initialCashInAmount

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'ConfigDir',
        description = '{config}{file}目錄',
        defaultValue = './conf',
        controlType = ControlType.EditBox,
        width = 200,
        seq = 5)
    String configDir

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'CanDaiShou2Return',
        description = '公共事業代收允許退貨',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox,
        seq = 6)
    boolean canDaiShou2Return

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'DaiShou2NeedWarning',
        description = '公共事業代收需要收銀員確認',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 7)
    boolean daiShou2NeedWarning

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'DaiShou2MixSales',
        description = '代收是否可以和一般商品混合銷售',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox,
        seq = 8)
    boolean daiShou2MixSales

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'IgnoreKeylockWithinTransaction',
        description = '在交易輸入當中忽略鑰匙轉動事件',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 9)
    boolean ignoreKeylockWithinTransaction

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'TraceState',
        description = '追蹤顯示系統內部狀態',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 10)
    boolean traceState

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'MessageSound',
        description = '顯示通報時是否要發出鳴叫聲',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 11)
    boolean messageSound

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'ShowLicenseInfo',
        description = '顯示{software}版權{message}',
        comment = '是否顯示{software}版權{message}，true表示要顯示。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 12)
    boolean showLicenseInfo

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'OriginalPriceVersion',
        description = 'trandetail.origprice填寫方式',
        comment = '1:如果是會員使用了促銷價，則填會員價、2:填原價',
        defaultValue = '1',
        controlType = ControlType.ComboBox,
        possibleValues = '1,2',
        seq = 13)
    String originalPriceVersion

    @ParamAnnotation(category = ParamCategory.S10System2,
        name = 'CashdrawerAmountMaximum',
        description = '提醒錢箱已满之最大金额',
        comment = '0表示不提醒，其他表示有提醒。',
        defaultValue = '0',
        controlType = ControlType.EditBox,
        width = 50,
        seq = 14)
    HYIDouble cashdrawerAmountMaximum

    // Don't allow to edit this.
//    @ParamAnnotation(category = ParamCategory.S10System2,
//        name = 'LicenseInfo',
//        description = '{software}版權{message}',
//        comment = '在右上方捲動顯示的{software}版權{message}。',
//        defaultValue = '',
//        controlType = ControlType.EditBox,
//        width = 200)
//    String licenseInfo

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'RecalcAfterZ',
        description = '是否在日結後重算',
        comment = '在日結檢查帳是否平，若不平會重新從交易明細計算',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 1)
    boolean recalcAfterZ

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'RebootAfterZ',
        description = '在日結後重新啟動系統',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 2)
    boolean rebootAfterZ

//    @ParamAnnotation(category = ParamCategory.S15System3,
//        name = 'RebootAfterDownMaster',
//        description = '是否在下傳主檔後重新啟動系統，不管有無更新{program}',
//        defaultValue = 'no',
//        controlType = ControlType.CheckBox)
//    boolean rebootAfterDownMaster

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'ActionAfterMasterDownload',
        description = '下傳主檔後，可以選擇系統是要"Reboot"、"AP Restart"、或"Nothing"',
        comment = '如果有{program}{file}下傳，那系統一定只會Reboot (on Linux)。',
        defaultValue = 'AP Restart',
        controlType = ControlType.ComboBox,
        possibleValues = 'Reboot,AP Restart,Nothing',
        seq = 3)
    String actionAfterMasterDownload

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'CountInvoiceEnable',
        description = '發票快用完是否提示(台灣)',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 4)
    boolean countInvoiceEnable

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'ShiftWithHoldTrn',
        description = '交班時的掛單處理',
        comment = 'allow:允許交班不提示尚有掛單{message} warning:允許交班但提示尚有掛單{message}。',
        defaultValue = 'warning',
        controlType = ControlType.ComboBox,
        possibleValues = 'allow,warning',
        width = 100,
        seq = 5)
    String shiftWithHoldTrn

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'EODWithHoldTrn',
        description = '日結帳時的掛單處理',
        comment = 'allow:允許日結不提示尚有掛單{message} warning:允許日結但提示尚有掛單{message}。',
        defaultValue = 'warning',
        controlType = ControlType.ComboBox,
        possibleValues = 'allow,warning',
        width = 100,
        seq = 6)
    String eodWithHoldTrn

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'PeiDaVersion',
        description = '配達單處理方式',
        comment = '1: 配達單號自動生成 2: 配達單手動輸入 3: 掃描配達單號條碼，並從SC取得配達明細。',
        defaultValue = '1',
        controlType = ControlType.ComboBox,
        possibleValues = '1,2,3',
        width = 30,
        seq = 7)
    String peiDaVersion

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'RebateAmountScale',
        description = '還圓金小數位數',
        defaultValue = '1',
        controlType = ControlType.EditBox,
        width = 30,
        seq = 8)
    int rebateAmountScale

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'RebatePaymentID',
        description = '還圓金支付ID',
        controlType = ControlType.EditBox,
        width = 30,
        seq = 9)
    String rebatePaymentID

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'AllowSpecialRebate',
        description = '計算燦坤特別還圓金',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 16)
    boolean allowSpecialRebate

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'SpecialRebateRate',
        description = '燦坤特別還圓金比率',
        defaultValue = '0.00',
        controlType = ControlType.EditBox,
        width = 30,
        seq = 17)
    String specialRebateRate

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'PadItemNumber',
        description = '品號長度',
        comment = '若不足時，自動前補零。',
        defaultValue = '0',
        controlType = ControlType.EditBox,
        width = 20,
        seq = 18)
    int padItemNumber

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'UseDiscountRate',
        description = '反查單品折扣率代號',
        comment = '從distype查出單品查詢率代號。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 19)
    boolean useDiscountRate

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'PreventScannerCatNo',
        description = '禁止{through}Scanner銷售的商品分類(plu.catno)',
        comment = '比如可以限制代售商品只能{through}代售鍵銷售。',
        defaultValue = '',
        controlType = ControlType.EditBox,
        width = 30,
        seq = 20)
    String preventScannerCatNo

    @ParamAnnotation(category = ParamCategory.S15System3,
        name = 'JustCalcMMAtSummaryState',
        description = '在按小計鍵後才計算組合促銷折扣',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 21)
    boolean justCalcMMAtSummaryState

    @ParamAnnotation(category = ParamCategory.S15System3,
            name = 'DailyLimit',
            description = '每天日结次数限制',
            comment = '0为不限制',
            defaultValue = '2',
            controlType = ControlType.EditBox,
            seq = 22)
    int dailyLimit

    @ParamAnnotation(category = ParamCategory.S15System3,
            name = 'ImmediateRemove',
            description = '是否允许立即更正',
            defaultValue = 'no',
            controlType = ControlType.CheckBox,
            seq = 23)
    boolean immediateRemove

    @ParamAnnotation(category = ParamCategory.S17Member,
        name = 'MemberCardIDPrefix',
        description = '會員卡前置碼',
        defaultValue = '',
        controlType = ControlType.EditBox,
        width = 40,
        seq = 1)
    String memberCardIDPrefix

    @ParamAnnotation(category = ParamCategory.S17Member,
        name = 'QueryMemberInfo',
        description = '是否查詢和顯示會員資料',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox,
        width = 40,
        seq = 2)
    boolean queryMemberInfo

    @ParamAnnotation(category = ParamCategory.S17Member,
        name = 'OnlyMemberCanUseRebate',
        description = '只有會員能使用還圓金支付',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 3)
    boolean onlyMemberCanUseRebate

    @ParamAnnotation(category = ParamCategory.S17Member,
        name = 'ShowMemberActionBonus',
        description = '會員資訊顯示畫面中顯示會員積分',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 4)
    boolean showMemberActionBonus

    @ParamAnnotation(category = ParamCategory.S17Member,
        name = 'QueryMemberFromPos',
        description = '直接從POS讀取會員信息，而不是連線到SC讀取',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 5)
    boolean queryMemberFromPos

    @ParamAnnotation(category = ParamCategory.S17Member,
        name = 'QueryMemberBlur',
        description = '直接從POS查詢會員時，使用模糊匹配',
        comment = '匹配卡號以外的屬性，如身分證號、電話號碼。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 6)
    boolean queryMemberBlur

    @ParamAnnotation(category = ParamCategory.S17Member,
        name = 'InputMemberCardIDInTheMiddleOfTransaction',
        description = '是否可以在交易當中輸入會員卡號',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 7)
    boolean inputMemberCardIDInTheMiddleOfTransaction

    @ParamAnnotation(category = ParamCategory.S17Member,
        name = 'MemberUseRegularPrice',
        description = '會員不使用PLU的會員價',
        comment = '會員是否仍採用一般顧客的售價判斷方式',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 8)
    boolean memberUseRegularPrice

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'TranPrintType',
        description = '收據/發票{print}時機',
        comment = 'step:逐項商品{print}、once:結帳後才全部{print}。',
        defaultValue = 'step',
        controlType = ControlType.ComboBox,
        possibleValues = 'step,once')
    String tranPrintType

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'ReceiptPrint',
        description = '{print}收據/發票',
        comment = '結帳時是否{print}收據/發票。',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox)
    boolean receiptPrint

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'NeedCheckPrinterAtRightStart',
        description = '{print}時檢查發票機是否定位',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox)
    boolean needCheckPrinterAtRightStart

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'ShiftPrint',
        description = '是否{print}Shift帳',
        comment = '交班時是否{print}Shift帳。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean shiftPrint

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'ZPrint',
        description = '是否{print}Z帳',
        comment = '日結時是否{print}Z帳。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean zPrint
    //Bruce/20090929/
    //   Don't know why Java couldn't call isZPrint() after upgrading to Groovy
    //   1.6.4 if we don't explicitly declare getter/setter. Maybe it is a bug when
    //   method name starts with a single lowercase letter.
    boolean isZPrint() { instance.zPrint }
    void setZPrint(boolean z) { instance.zPrint = z }

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'DepSalesPrint',
        description = '是否{print}部門帳',
        comment = '日結時是否{print}部門帳。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean depSalesPrint

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'NeedChangePaper',
        description = '是否提示更換紙卷',
        comment = '{print}Shift/Z帳時是否要提示更換紙卷。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean needChangePaper

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'PrintImmediateRemove',
        description = '立即更正是否需要{print}',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean printImmediateRemove

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'PluPrintField',
        description = '{print}時用PLU的哪個欄位',
        defaultValue = 'pluname',
        controlType = ControlType.ComboBox,
        possibleValues = 'pluname,printname')
    String pluPrintField

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'ConfuseTranNumber',
        description = '打亂{print}出的交易序號',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean confuseTranNumber

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'PrintDaiFu',
        description = '{print}代付交易',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox)
    boolean printDaiFu

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'PrintLineItemWithDtlCode1',
        description = '商品名後面加上期刊尾碼',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox)
    boolean printLineItemWithDtlCode1

    @ParamAnnotation(category = ParamCategory.S20Printing,
            name = 'ReprintGenerateVoidTransaction',
            description = '重印產生新交易',
            defaultValue = 'yes',
            controlType = ControlType.CheckBox)
    boolean reprintGenerateVoidTransaction

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'PrintPaidInOut',
        description = '{print}PaidIn/PaidOut在收據/發票上',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean printPaidInOut

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'InvoicePrintingUnfinished',
        description = '是否一張發票只{print}到一半，尚未結束',
        comment = 'true表示發票只{print}到一半，尚未結束; false表示已經切紙並結束。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean invoicePrintingUnfinished
    void updateInvoicePrintingUnfinished(v) { instance.invoicePrintingUnfinished = v }

    @ParamAnnotation(category = ParamCategory.S20Printing,
        name = 'OpenPrinterWaitTime',
        description = '等待{printer}打開時間',
        comment = '0: 代表不设定, n: 代表 n 秒， n秒超过后，就等待n秒，超時就禁用{printer}',
        defaultValue = '0',
        controlType = ControlType.EditBox,
        width = 40)
    int openPrinterWaitTime

    @ParamAnnotation(category = ParamCategory.S20Printing,
            name = 'PrintAreaNumber',
            description = '门店电话是否打印区号',
            comment = 'yes表示打印，no表示不打印',
            defaultValue = 'yes',
            controlType = ControlType.EditBox)
    boolean printAreaNumber

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'CanOverrideAmount',
        description = '是否開放變價',
        comment = '是否開放手工變價功能。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean canOverrideAmount

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'OnlyMemberCanUseOverrideAmount',
        description = '是否只有會員才能開放變價',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean onlyMemberCanUseOverrideAmount

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'OverrideVersion',
        description = '變價功能版本',
        comment = '1：一般變價，2：燦坤黃卡變價。',
        defaultValue = '1',
        controlType = ControlType.ComboBox,
        possibleValues = '1,2')
    String overrideVersion

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'OverrideAmountReasonPopup',
        description = '變價時是否要選擇原因',
        comment = '變價時是否要彈出選擇原因的popup。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean overrideAmountReasonPopup

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'DownloadMinPrice',
        description = '是否下載商品變價最低金額',
        comment = '是否下載商品變價最低金額plu.minprice欄位。',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox)
    boolean downloadMinPrice

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'OverrideUseCat',
        description = '變價金額限制取自分類表',
        comment = '變價金額限制取自分類表cat的overrideamountlimit字段。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean overrideUseCat

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'PriceChangeVersion',
        description = '變價單版本號',
        comment = '最後一次下傳的變價單版本號，此由{program}自動維護。',
        controlType = ControlType.EditBox,
        width = 150)
    String priceChangeVersion
    void updatePriceChangeVersion(v) { instance.priceChangeVersion = v }

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'OverrideAmountRangeLimit',
        description = '變價金額的上下限',
        comment = '如果非取自分類表，則使用此變價金額的上下限。若為10%則設置為0.1。',
        defaultValue = '0.5',
        controlType = ControlType.EditBox)
    HYIDouble overrideAmountRangeLimit

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'onlyMemberUseOverrideAmount',
        description = '只有會員能變價銷售',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean OnlyMemberUseOverrideAmount

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'TurnKeyAtOverrideAmount',
        description = '變價時必須轉動鑰匙',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean turnKeyAtOverrideAmount

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'RoundDown',
        description = '捨零去分',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean roundDown

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'RoundItemNo',
        description = '四捨五入或設零對應商品品號',
        defaultValue = 'rd',
        controlType = ControlType.EditBox,
        width = 30,
        seq = 10)
    String roundItemNo

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'RoundItemCatetory',
        description = '四捨五入或設零對應商品分類',
        defaultValue = '007',
        controlType = ControlType.EditBox,
        width = 30,
        seq = 10)
    String roundItemCategory

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'InquiryPirceShowAllSpecialPriceInfo',
        description = '查價畫面是否顯示促銷價',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean inquiryPirceShowAllSpecialPriceInfo

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'InquiryPriceNeedMemberInfo',
        description = '查價畫面是否顯示會員價',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean inquiryPriceNeedMemberInfo

    @ParamAnnotation(category = ParamCategory.S25PriceOverriding,
        name = 'DIYSupport',
        description = '是否啟用燦坤DIY折扣功能',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean diySupport

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'ScannerDevice',
        description = '{scanner}類型',
        comment = '選擇連接的{scanner}類型。',
        controlType = ControlType.ComboBox,
        possibleValues = ',KeyboardScanner',
        seq = 1)
    String ScannerDevice

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'KeylockDevice',
        description = '鑰匙鎖類型',
        comment = '選擇連接的鑰匙鎖類型。',
        controlType = ControlType.ComboBox,
        possibleValues = ',PosiflexKB6600Keylock,TEC6400Keylock,GenericKeylock,TecM8000Keylock',
        seq = 2)
    String KeylockDevice

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'MSRDevice',
        description = '刷卡機類型',
        comment = '選擇連接的刷卡機類型。',
        controlType = ControlType.ComboBox,
        possibleValues = ',PosiflexKB6600MSR',
        seq = 3)
    String MSRDevice

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'POSKeyboardDevice',
        description = 'POS鍵盤類型',
        comment = '選擇連接的POS鍵盤類型。',
        controlType = ControlType.ComboBox,
        possibleValues = ''',PosiflexKB6600POSKeyboard,TecS78aPOSKeyboard,TEC6400POSKeyboard,\
WincorNixdorfKeyboard,PartnerKB68MKeyboard,Tec8500POSKeyboard''',
        seq = 4)
    String POSKeyboardDevice

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'CashDrawerDevice',
        description = 'POS{cashdrawer}類型',
        comment = '選擇連接的POS{cashdrawer}類型。',
        controlType = ControlType.ComboBox,
        possibleValues = ',CashDrawerPP2000,CashDrawerTSP600,Tec6400CashDrawer,Tec7000CashDrawer,EpsonRPU420CashDrawer,GPrinterCashDrawer,MeiSongUsbCashDrawer',
        seq = 5)
    String CashDrawerDevice

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'POSPrinterDevice',
        description = 'POS{printer}類型',
        comment = '選擇連接的POS{printer}類型。',
        controlType = ControlType.ComboBox,
        possibleValues = ',VirtualPOSPrinter,EpsonPOSPrinter,EpsonRPU420POSPrinter,POSPrinterTSP600,POSPrinter8500,HprtTP805POSPrinter,GPrinterPOSPrinter',
        seq = 6)
    String POSPrinterDevice

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'ToneIndicatorDevice',
        description = '發聲器類型',
        comment = '選擇連接的發聲器類型。',
        controlType = ControlType.ComboBox,
        possibleValues = ',ToneIndicatorPCSpeaker',
        seq = 7)
    String ToneIndicatorDevice

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'LineDisplayDevice',
        description = '客戶顯示器類型',
        comment = '選擇連接的客戶顯示器類型。',
        controlType = ControlType.ComboBox,
        possibleValues = ',CD7110,CD7220,Tec6400Linedisplay,IBM4840LineDisplay',
        seq = 8)
    String LineDisplayDevice

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'CATDevice',
        description = '信用卡授權終端類型',
        comment = '選擇連接的信用卡授權終端類型。',
        controlType = ControlType.ComboBox,
        possibleValues = ',ChinaTrustCAT',
        seq = 9)
    String CATDevice

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'KeyboardType',
        description = '鍵盤類型',
        comment = '對應的鍵盤設定檔為:posbutton3{鍵盤類型}.conf。',
        defaultValue = '',
        controlType = ControlType.EditBox,
        width = 30,
        seq = 10)
    String keyboardType

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'Tec7000CashDrawerNo',
        description = 'TEC-7000的錢箱接口位置',
        defaultValue = '1',
        controlType = ControlType.ComboBox,
        possibleValues = '1,2',
        seq = 11)
    int tec7000CashDrawerNo

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'NeedTurnKeyAtStart',
        description = '{program}啟動後需要轉鑰匙',
        comment = '在{program}開始的時候是否需要先轉鑰匙到OFF再轉到銷售位置，true表示需要轉動。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 12)
    boolean needTurnKeyAtStart

    @ParamAnnotation(category = ParamCategory.S30Peripherals,
        name = 'ScaleBarCodeFormat',
        description = '秤重商品條碼格式',
        comment = '『I』：貨號、『W』：重量、『P』：價格、『C』：校驗位、『.』：小數點所在位置。',
        defaultValue = '',
        controlType = ControlType.EditBox,
        seq = 13)
    String scaleBarCodeFormat

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'SCIPAddress',
        description = '後台網路地址',
        comment = '後台SC的IP地址或domain name',
        defaultValue = 'sc',
        controlType = ControlType.EditBox,
        width = 100,
        seq = 1)
    String scIpAddress

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'UseRuok',
        description = '定時送RUOK',
        comment = '是否定時送RUOK到伺服器(Are you OK?)。',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox,
        seq = 2)
    boolean useRuok

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'AutoSyncTransaction',
        description = '自動補傳未上傳資料',
        comment = '是否自動上傳未上傳交易和報表。',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox,
        seq = 3)
    boolean autoSyncTransaction

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'AutoSyncTransactionTimes',
        description = '檢查未上傳資料間隔時間(n)',
        comment = '檢查未上傳資料間隔時間(31.68秒 x n)。',
        defaultValue = '50',
        controlType = ControlType.EditBox,
        width = 60,
        seq = 4)
    int autoSyncTransactionTimes

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'SyncTransactionRange',
        description = '交易同步比對的天數',
        defaultValue = '7',
        controlType = ControlType.EditBox,
        width = 30,
        seq = 5)
    int syncTransactionRange

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'InlineServerProgramDirectory',
        description = '後台{program}下傳目錄',
        defaultValue = '/home/hyi/inline/posdl/',
        controlType = ControlType.EditBox,
        width = 200,
        seq = 6)
    String inlineServerProgramDirectory

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'MasterVersion',
        description = '主檔版本日期',
        comment = '最後一次下傳主檔的日期',
        defaultValue = '',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 7)
    String masterVersion
    void updateMasterVersion(v) { instance.masterVersion = v }

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'DownloadMasterHour',
        description = '下傳主檔和{program}文件的時點',
        comment =
        '''此參數設定主檔和{program}{file}的版本檢查和下傳時點。內容為regular expressions for:
           “master下傳時點/program下傳時點”。<br/>
           例如:<br/>
           <li>(00|16):01/00:01 表示在每日00:01和16:01下傳master、在00:01下傳program。
           <li>\\d{1,2}:(00|30)/0[1,9]:00 表示每個小時00分和30分下傳master、在01:00和09:00下傳program。
               \\d代表一位數字、{1,2}表示1或2位、[1,9]表示數字1或9。
           <li>也可以像這樣窮舉 (00|04|08|09|12|15|20|21|22|23):(00|15|30|45)/(00|15):30 。
           <li>再一個窮舉例子 00:00|03:00|08:00|09:00|12:00|15:00|20:00|23:00/00:30|15:30 。
           <li>另外，如果只簡寫一個一到兩位數字h，則意思是 >=h:00/>=h:00。例如，只寫“1”時，
               表示在超過凌晨1點後同時下傳master和program。這樣和舊的設置兼容了。
           <li>實際可下傳時間還要加上delay time，參閱參數DownloadMasterDelayBetweenPos。''',
        defaultValue = '0',
        controlType = ControlType.EditBox,
        seq = 8)
    String downloadMasterHour

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'DownloadMasterDelayBetweenPos',
        description = '每台POS主檔下傳的時間間隔(秒)',
        comment = '實際下傳時間為MasterDownloadHour時點加上: (POS編號-1)*MasterDownloadDelayBetweenPos。',
        defaultValue = '30',
        controlType = ControlType.EditBox,
        seq = 9)
    int downloadMasterDelayBetweenPos

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'EnableMessageDownload',
        description = '是否下傳通報{message}',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 10)
    boolean enableMessageDownload

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'SaveCommand',
        description = '存行動碟執行命令',
        comment = '離線交班或日結儲存行動碟的執行命令，例如：tar cvf /dev/fd0 serialized',
        defaultValue = '',
        controlType = ControlType.EditBox,
        width = 200,
        seq = 11)
    String saveCommand

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'FloppyContent',
        description = '離線儲存格式',
        comment = 'partial只把Shift/Z序列化並儲存；full則以文本方式儲存，並且在交班時會連同交易明細一併儲存。',
        defaultValue = 'partial',
        controlType = ControlType.ComboBox,
        possibleValues="full,partial",
        width = 70,
        seq = 12)
    String floppyContent

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'InventoryInitDate',
        description = '盤點初始化日期',
        comment = '最後一次做盤點初始化的日期。',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 13)
    String inventoryInitDate
    void updateInventoryInitDate(v) { instance.inventoryInitDate = v }

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'NeedUpdateHead',
        description = '是否定時檢查主檔版本',
        comment = '是否定時檢查主檔版本，並提示下載。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 14)
    boolean needUpdateHead

//    @ParamAnnotation(category = ParamCategory.S35Inline,
//        name = 'NeedSynTranAtShift',
//        description = '在交班日結同步交易',
//        comment = '是否需要在交班、日結時檢查後台是否漏交易。',
//        defaultValue = 'no',
//        controlType = ControlType.CheckBox)
//    boolean needSynTranAtShift

//    @ParamAnnotation(category = ParamCategory.S35Inline,
//        name = 'PosButtonDownloadFromTable',
//        description = '鍵盤設定是否是{through}後端的posdl_posbutton3表下傳',
//        defaultValue = 'no',
//        controlType = ControlType.CheckBox)
//    boolean posButtonDownloadFromTable

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'UseFloppyToSaveData',
        description = '是否使用{peripheral}保存離線{data}',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 15)
    boolean useFloppyToSaveData

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'UploadVersion',
        description = '交班時上傳POS版本號',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 16)
    boolean uploadVersion

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'GenerateSSMPLog',
        description = '是否生成和上傳SSMP Log',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox,
        seq = 17)
    boolean generateSsmpLog

    @ParamAnnotation(category = ParamCategory.S35Inline,
        name = 'InlineLongTimeout',
        description = 'Inline client/server 上下傳的long timeout(秒)',
        comment = '在等候可能會有較長時間的response時使用。',
        defaultValue = '360',
        controlType = ControlType.EditBox,
        seq = 18)
    int inlineLongTimeout

    @ParamAnnotation(category = ParamCategory.S40Cashier,
        name = 'CheckIDLengthOnCheckInOut',
        description = '上下班簽到是否不用檢查員工碼長度',
        comment = '如果是，下面其他相關選項都不起作用',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox)
    boolean checkIDLengthOnCheckInOut

    @ParamAnnotation(category = ParamCategory.S40Cashier,
        name = 'OnlyCheckIDLenOnCheckInOut',
        description = '是否僅僅檢查員工碼長度',
        comment = '如果是，必須設置員工碼長度; 若“是否檢查校驗位”設置為“是”，那也會檢查校驗位',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean onlyCheckIDLenOnCheckInOut

    @ParamAnnotation(category = ParamCategory.S40Cashier,
        name = 'CheckInOutIDLens',
        description = '員工碼長度',
        defaultValue = '7',
        controlType = ControlType.EditBox,
        width = 30)
    int checkInOutIDLens

    @ParamAnnotation(category = ParamCategory.S40Cashier,
        name = 'CheckCashierDigit',
        description = '是否檢查校驗位',
        comment = '單獨使用時只檢查校驗位',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean checkCashierDigit

    @ParamAnnotation(category = ParamCategory.S40Cashier,
        name = 'ContinuousLogon',
        description = '連續上下班輸入',
        comment = '上下班簽到可連續輸入',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean continuousLogon

    @ParamAnnotation(category = ParamCategory.S40Cashier,
        name = 'SendPunchInOutDetail',
        description = '是否上傳簽到簽退明細到後台',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean sendPunchInOutDetail

    @ParamAnnotation(category = ParamCategory.S40Cashier,
        name = 'SalemanIDMinLen',
        description = '售貨員碼最小長度',
        defaultValue = '7',
        controlType = ControlType.EditBox,
        width = 30)
    int salemanIDMinLen

    @ParamAnnotation(category = ParamCategory.S40Cashier,
        name = 'SalemanIDMaxLen',
        description = '售貨員碼最大長度',
        defaultValue = '8',
        controlType = ControlType.EditBox,
        width = 30)
    int salemanIDMaxLen

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'ShowAmountWithScale2',
        description = '金額是否顯示兩位小數',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean showAmountWithScale2

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'WeightDigit',
        description = '秤重商品小數位數',
        defaultValue = '3',
        controlType = ControlType.EditBox,
        width = 30)
    int WeightDigit

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'QuantityDigit',
        description = '數量小數位數',
        defaultValue = '3',
        controlType = ControlType.EditBox,
        width = 30)
    int QuantityDigit

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'ShowMouseCursor',
        description = '是否顯示Mouse ',
        comment = '當鼠標停留在POS{program}{window}上時，是否要顯示{mouse}游標。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox)
    boolean showMouseCursor

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'CreateButtonPanel',
        description = '是否顯示按鈕工具列',
        comment = '是否在POS{window}下方顯示一條按鈕工具列。通常在Windows+Mouse環境下使用。',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox)
    boolean createButtonPanel

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'ApplicationTitle',
        description = '{window}標題',
        comment = '用於Frame Title的顯示',
        defaultValue = 'ePOS By Hongyuan Software',
        controlType = ControlType.EditBox,
        width = 200)
    String applicationTitle

//    @ParamAnnotation(category = ParamCategory.S45UserInterface,
//        name = 'ScreenBannerFont',
//        description = 'ScreenBanner字體名',
//        defaultValue = 'cwTeXHeiBold',
//        controlType = ControlType.EditBox,
//        width = 100)
//    String screenBannerFont

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'PayingPaneFont',
        description = 'Form字體名',
        comment = '用在現金清點單和信用卡輸入表格',
        defaultValue = 'cwTeXHeiBold',
        controlType = ControlType.EditBox,
        width = 100)
    String payingPaneFont
    String getPayingPaneFont() {
        (!_traditional && payingPaneFont == 'cwTeXHeiBold') ? 'SimHei' : payingPaneFont
    }

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'ScreenButtonFont',
        description = 'ScreenBanner字體名',
        defaultValue = 'cwTeXHeiBold',
        controlType = ControlType.EditBox,
        width = 100)
    String screenButtonFont

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'ItemListFont',
        description = 'ItemList字體名',
        defaultValue = 'cwTeXHeiBold',
        controlType = ControlType.EditBox,
        width = 100)
    String itemListFont

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'PopupMenuPaneFont',
        description = 'PopupMenuPane字體名',
        defaultValue = 'cwTeXHeiBold',
        controlType = ControlType.EditBox,
        width = 100)
    String popupMenuPaneFont
    String getPopupMenuPaneFont() {
        (!_traditional && popupMenuPaneFont == 'cwTeXHeiBold') ? 'SimHei' : popupMenuPaneFont
    }

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'MenuHigh',
        description = 'Popup menu一頁行數',
        defaultValue = '10',
        controlType = ControlType.ComboBox,
        possibleValues = '10,13')
    int menuHigh

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'IndicatorFont',
        description = 'Indicator字體名',
        defaultValue = 'cwTeXHeiBold',
        controlType = ControlType.EditBox,
        width = 200)
    String indicatorFont
    String getIndicatorFont() {
        (!_traditional && indicatorFont == 'cwTeXHeiBold') ? 'SimHei' : indicatorFont
    }

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'IndicatorFontSize',
        description = 'Indicator字體大小',
        defaultValue = '17',
        controlType = ControlType.EditBox,
        width = 60)
    int indicatorFontSize

    @ParamAnnotation(category = ParamCategory.S45UserInterface,
        name = 'NeedMouseRobot',
        description = '啟動mouse robot',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox)
    boolean needMouseRobot

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'screenbanner',
        description = '主畫面左下方文字塊',
        controlType = ControlType.EditBox,
        seq = 1)
    String screenbanner

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'screenbanner2',
        description = '主畫面上方橫排文字塊',
        controlType = ControlType.EditBox,
        seq = 2)
    String screenbanner2

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'connmsg',
        description = '主畫面左上方連線離線文字',
        controlType = ControlType.EditBox,
        seq = 3)
    String connmsg

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'pagemsg',
        description = '主畫面商品頁數文字',
        controlType = ControlType.EditBox,
        seq = 4)
    String pagemsg

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'productinfo',
        description = '主畫面PRODUCT大文字',
        controlType = ControlType.EditBox,
        seq = 5)
    String productinfo

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'totalmsg',
        description = '主畫面金額合計文字塊',
        controlType = ControlType.EditBox,
        seq = 6)
    String totalmsg

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'itemlist',
        description = '主畫面商品銷售明細表',
        controlType = ControlType.EditBox,
        seq = 7)
    String itemlist

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'payingpane1',
        description = '結帳畫面左上方銷售金額文字塊',
        controlType = ControlType.EditBox,
        seq = 8)
    String payingpane1

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'payingpane',
        description = '結帳畫面右上方支付金額文字塊',
        controlType = ControlType.EditBox,
        seq = 9)
    String payingpane

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'payingpane2',
        description = '結帳畫面下方應收找零金額文字塊',
        controlType = ControlType.EditBox,
        seq = 10)
    String payingpane2

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'dynapayingpane',
        description = '結帳畫面批發銷售文字塊',
        controlType = ControlType.EditBox,
        seq = 11)
    String dynapayingpane

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'agelevel',
        description = '客層代號定義',
        controlType = ControlType.EditBox,
        seq = 12)
    String agelevel

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'config',
        description = '鑰匙PRG檔位{menu}',
        controlType = ControlType.EditBox,
        seq = 12)
    String config

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'keylock1',
        description = '[收銀員]鍵{menu}',
        controlType = ControlType.EditBox,
        seq = 13)
    String keylock1

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'keylock2',
        description = '鑰匙2檔位{menu}',
        controlType = ControlType.EditBox,
        seq = 14)
    String keylock2

    @ParamAnnotation(category = ParamCategory.S47UserInterface2,
        name = 'keylock3',
        description = '鑰匙3檔位{menu}',
        controlType = ControlType.EditBox,
        seq = 15)
    String keylock3

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'CashierNumber',
        description = '當班收銀員編號',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 1)
    String cashierNumber
    void updateCashierNumber(v) { instance.cashierNumber = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'ShiftNumber',
        description = '目前shift帳序號',
        controlType = ControlType.EditBox,
        width = 50,
        seq = 2)
    int shiftNumber
    void updateShiftNumber(v) { instance.shiftNumber = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'ZNumber',
        description = '目前Z序號',
        controlType = ControlType.EditBox,
        width = 50,
        seq = 3)
    int zNumber
    void updateZNumber(v) { instance.zNumber = v }
    //Bruce/20090929/
    //   Don't know why Java couldn't call getZNumber() after upgrading to Groovy
    //   1.6.4 if we don't explicitly declare getter/setter. Maybe it is a bug when
    //   method name starts with a single lowercase letter.
    int getZNumber() { instance.zNumber }
    void setZNumber(int z) { instance.zNumber = z }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'InvoiceID',
        description = '發票字軌',
        defaultValue = 'AA',
        controlType = ControlType.EditBox,
        width = 20,
        seq = 4)
    String invoiceID
    void updateInvoiceID(v) { instance.invoiceID = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'InvoiceNumber',
        description = '目前發票號',
        defaultValue = '00000000',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 5)
    String invoiceNumber
    void updateInvoiceNumber(v) { instance.invoiceNumber = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'NextTransactionNumber',
        description = '下一筆交易序號',
        controlType = ControlType.EditBox,
        defaultValue = '1',
        width = 70,
        seq = 6)
    int nextTransactionNumber
    void updateNextTransactionNumber(v) { instance.nextTransactionNumber = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'BeginInvoiceNumberOfCashier',
        description = 'Shift帳的起始發票號',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 7)
    String beginInvoiceNumberOfCashier
    void updateBeginInvoiceNumberOfCashier(v) { instance.beginInvoiceNumberOfCashier = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'EndInvoiceNumberOfCashier',
        description = 'Shift帳的結束發票號',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 8)
    String endInvoiceNumberOfCashier
    void updateEndInvoiceNumberOfCashier(v) { instance.endInvoiceNumberOfCashier = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'BeginTransactionNumberOfCashier',
        description = 'Shift帳的起始交易序號',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 9)
    int beginTransactionNumberOfCashier
    void updateBeginTransactionNumberOfCashier(v) { instance.beginTransactionNumberOfCashier = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'EndTransactionNumberOfCashier',
        description = 'Shift帳的結束交易序號',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 10)
    int endTransactionNumberOfCashier
    void updateEndTransactionNumberOfCashier(v) { instance.endTransactionNumberOfCashier = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'BeginInvoiceNumberOfZ',
        description = 'Z帳的起始發票序號',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 11)
    String beginInvoiceNumberOfZ
    void updateBeginInvoiceNumberOfZ(v) { instance.beginInvoiceNumberOfZ = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'EndInvoiceNumberOfZ',
        description = 'Z帳的結束發票號',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 12)
    String endInvoiceNumberOfZ
    void updateEndInvoiceNumberOfZ(v) { instance.endInvoiceNumberOfZ = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'BeginTransactionNumberOfZ',
        description = 'Z帳開始交易序號',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 13)
    int beginTransactionNumberOfZ
    void updateBeginTransactionNumberOfZ(v) { instance.beginTransactionNumberOfZ = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'EndTransactionNumberOfZ',
        description = 'Z帳結束序號',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 14)
    int endTransactionNumberOfZ
    void updateEndTransactionNumberOfZ(v) { instance.endTransactionNumberOfZ = v }

    @ParamAnnotation(category = ParamCategory.S50Numbers,
        name = 'PeiDaNumber',
        description = '目前配達單號',
        controlType = ControlType.EditBox,
        width = 120,
        seq = 15)
    String peiDaNumber
    void updatePeiDaNumber(v) { instance.peiDaNumber = v }

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'ChangeAmountUpperlimit',
        description = '找零金額上限',
        defaultValue = '2000',
        controlType = ControlType.EditBox,
        width = 60,
        seq = 1)
    HYIDouble changeAmountUpperlimit

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'SpillAmountUpperlimit',
        description = '溢收金額上限',
        defaultValue = '500',
        controlType = ControlType.EditBox,
        width = 60,
        seq = 2)
    HYIDouble spillAmountUpperlimit

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'MaxTransactionNumber',
        description = '最大交易序號',
        comment = '到達此交易序號後會自動回復為1',
        defaultValue = '99999999',
        controlType = ControlType.EditBox,
        width = 100,
        seq = 3)
    String maxTransactionNumber
    boolean maxTransactionNumberIsNoLimit() { maxTransactionNumber == '99999999' }

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'TransactionReserved',
        description = '交易保留天數',
        defaultValue = '300',
        controlType = ControlType.EditBox,
        width = 60,
        seq = 4)
    int transactionReserved

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'MaxPrice',
        description = '金額輸入上限',
        comment = '用在Paid In/Out,Cash In/Out,代收/代付,和變價的金額輸入檢查。',
        defaultValue = '99999',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 5)
    HYIDouble maxPrice

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'PluMaxQuantity',
        description = '最大單品輸入數量',
        defaultValue = '9999',
        controlType = ControlType.EditBox,
        width = 70,
        seq = 6)
    int pluMaxQuantity

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'MaxLineItems',
        description = '交易最大品項數',
        comment = '單筆交易的最大單品項數',
        defaultValue = '50',
        controlType = ControlType.EditBox,
        width = 60,
        seq = 7)
    int maxLineItems

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'LevelsCanSignOn',
        description = '可SignOn層級',
        comment = '哪些Level收銀員可以SignOn',
        defaultValue = '0,1,2,3,4,5,6,7',
        controlType = ControlType.EditBox,
        width = 120,
        seq = 8)
    String levelsCanSignOn

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'LevelsCanSaleThings',
        description = '售貨員層級',
        comment = '哪些Level收銀員可以作為售貨員輸入',
        defaultValue = '0,1,2,3,4,5,6,7',
        controlType = ControlType.EditBox,
        width = 120,
        seq = 9)
    String levelsCanSaleThings

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'MaxInventoryQty',
        description = '盤點商品可輸入的最大數量',
        defaultValue = '999',
        controlType = ControlType.EditBox,
        width = 50,
        seq = 10)
    int maxInventoryQty

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'OnlyMemberCanUseSpecialPrice',
        description = '只有會員能享受時段促銷價',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 11)
    boolean onlyMemberCanUseSpecialPrice

    @ParamAnnotation(category = ParamCategory.S55Constrains,
        name = 'DontAllowGenerateSpillAmount',
        description = '結賬支付不允許有溢收金額產生',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 12)
    boolean dontAllowGenerateSpillAmount

    @ParamAnnotation(category = ParamCategory.S60Nitori,
        name = 'ShoppingBagDepID',
        description = '購物袋特殊DepID',
        defaultValue = '9',
        controlType = ControlType.EditBox,
        width = 30,
        seq = 1)
    String shoppingBagDepID

    @ParamAnnotation(category = ParamCategory.S60Nitori,
        name = 'SendFeeDepID',
        description = '宅配費用特殊DepID',
        defaultValue = '91',
        controlType = ControlType.EditBox,
        width = 30,
        seq = 2)
    String sendFeeDepID

    @ParamAnnotation(category = ParamCategory.S60Nitori,
        name = 'GatherShoppingBagInfo',
        description = '統計購物袋收入信息',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 3)
    boolean gatherShoppingBagInfo

    @ParamAnnotation(category = ParamCategory.S60Nitori,
        name = 'GatherSendFeeInfo',
        description = '統計宅配費用收入信息',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 4)
    boolean gatherSendFeeInfo

    @ParamAnnotation(category = ParamCategory.S60Nitori,
        name = 'CalculateTaxAmtByUnitPrice',
        description = '先按照單價計算稅金然後再乘以數量',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 5)
    boolean calculateTaxAmtByUnitPrice

    @ParamAnnotation(category = ParamCategory.S60Nitori,
        name = 'IsGrossTrandtlTaxAmt',
        description = '在交易明細稅項中填入毛額税金',
        comment = '應該與DiscountTaxId一起使用',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 6)
    boolean grossTrandtlTaxAmt

    @ParamAnnotation(category = ParamCategory.S60Nitori,
        name = 'DiscountTaxId',
        description = '交易明细中折扣项税率id',
        comment = '不設表示不對折扣設定税率, trandetail折扣行中的taxamt將不填; 否則對應税别定義檔taxtype.taxid, 對折扣計税時將按照對應税别定義的種種參數計算',
        defaultValue = '',
        controlType = ControlType.EditBox,
        width = 30,
        seq = 7)
    String discountTaxId

    @ParamAnnotation(category = ParamCategory.S60Nitori,
        name = 'ExclusiveZhunguiDeps',
        description = '專櫃商品部門',
        comment = '此列表中包含了所有的專櫃部門, 這些部門不可以與普通商品混合銷售, 以冒號分隔, 如 "931:932"。',
        defaultValue = '',
        controlType = ControlType.EditBox,
        width = 60,
        seq = 8)
    String exclusiveZhunguiDeps

    @ParamAnnotation(category = ParamCategory.S60Nitori,
        name = 'DownPaymentItem',
        description = 'Nitori着付金商品“條碼,品號,分類”',
        defaultValue = '91600035,9160003,933',
        controlType = ControlType.EditBox,
        width = 60,
        seq = 9)
    String downPaymentItem

    @ParamAnnotation(category = ParamCategory.S60Nitori,
        name = 'CreditCardBonusRegardedAsPayment',
        description = '信用卡紅利抵用是否視為支付處理（抑或是做為折扣處理）',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox,
        seq = 10)
    boolean creditCardBonusRegardedAsPayment

//    @ParamAnnotation(category = ParamCategory.S65CampusMart,
//        name = 'WholesaleSpecialPluno',
//        description = '團購特殊商品no',
//        defaultValue = '999999',
//        controlType = ControlType.EditBox,
//        width = 50)
//    String wholesaleSpecialPluno

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
        name = 'ControlPriceChangeLowest',
        description = '檢查是否低於批發價或會員最低價',
        defaultValue = 'yes',
        controlType = ControlType.CheckBox,
        seq = 1)
    boolean controlPriceChangeLowest

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
        name = 'DaiShou2BillNoDuplicatedAllowed',
        description = '代收單號可以重複輸入',
        comment = '控制教育超市圖書代配收款（{through}代收）不可重複输入。',
        defaultValue = 'no',
        controlType = ControlType.CheckBox,
        seq = 2)
    boolean daiShou2BillNoDuplicatedAllowed

      @ParamAnnotation(category = ParamCategory.S65CampusMart,
        name = 'UnionpayEwalletURL',
        description = '银联钱包URL',
        comment = '银联钱包URL',
        defaultValue = 'https://gateway.95516.com',
        controlType = ControlType.CheckBox,
        seq = 2)
    String unionpayEwalletURL

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
            name = 'WeiXinDeviceInfo',
            description = '终端设备号(商户自定义)',
            comment = '终端设备号(商户自定义)',
            defaultValue = '123456',
            controlType = ControlType.CheckBox,
            seq = 2)
    String deviceInfo

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
            name = 'WeiXinUrl',
            description = '微信接口地址',
            comment = '微信接口地址',
            defaultValue = 'https://api.mch.weixin.qq.com/',
            controlType = ControlType.CheckBox,
            seq = 2)
    String weiXinUrl

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
            name = 'WeiXinGoodsTag',
            description = '微信订单优惠标记',
            comment = '微信订单优惠标记',
            defaultValue = '是',
            controlType = ControlType.CheckBox,
            seq = 2)
    String weiXinGoodsTag

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
            name = 'maxorderid',
            description = '自助收银接口返回的maxid',
            comment = '自助收银接口返回的maxid',
            defaultValue = '0',
            controlType = ControlType.CheckBox,
            seq = 2)
    String maxorderid
    void updateMaxorderid(v) { instance.maxorderid = v }

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
            name = 'selfstartdate',
            description = '自助收银接口时间往前推几天',
            comment = '自助收银接口时间往前推几天',
            defaultValue = '60',
            controlType = ControlType.CheckBox,
            seq = 2)
    int selfstartdate

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
            name = 'selfbuySleep',
            description = '自助收银接口调用时间间隔',
            comment = '自助收银接口调用时间间隔',
            defaultValue = '60',
            controlType = ControlType.CheckBox,
            seq = 2)
    String selfbuySleep

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
            name = 'selfAppid',
            description = '自助收银appid',
            comment = '自助收银appid',
            defaultValue = '2088712752359142',
            controlType = ControlType.CheckBox,
            seq = 2)
    String selfAppid

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
            name = 'selfSign',
            description = '自助收银秘钥',
            comment = '自助收银秘钥',
            defaultValue = 'aa7bfdf76863fc26cd17e9a24e4a9d8c',
            controlType = ControlType.CheckBox,
            seq = 2)
    String selfSign

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
            name = 'selfUrl',
            description = '自助收银接口url',
            comment = '自助收银接口url',
            defaultValue = 'http://kb-selfbuy-exerp.devtest.clouds-pos.com/order/query',
            controlType = ControlType.CheckBox,
            seq = 2)
    String selfUrl

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
            name = 'PingURL',
            description = '外网url',
            comment = '外网url',
            defaultValue = 'www.baidu.com',
            controlType = ControlType.CheckBox,
            seq = 2)
    String pingURL

    @ParamAnnotation(category = ParamCategory.S65CampusMart,
            name = 'ecommerceUrl',
            description = '电商url',
            comment = '电商url',
            defaultValue = 'http://180.168.135.212:8080/',
            controlType = ControlType.CheckBox,
            seq = 2)
    String ecommerceUrl

    @ParamAnnotation(category = ParamCategory.S05System1,
            name = 'ButtonSize',
            description = '按鈕大小',
            comment = '按鈕大小',
            defaultValue = '200',
            controlType = ControlType.EditBox,
            seq = 1)
    int buttonSize


    ///////////////////////////////////////////////////////////////////////////////////
    // OTHER COMPUTED PARAMETERS //////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////

    String getNextInvoiceNumber() {
        return invoiceID + invoiceNumber;
    }

    String getDownPaymentItemPluNo() {
        return downPaymentItem.split(",")[0];
    }

    String getDownPaymentItemNo() {
        return downPaymentItem.split(",")[1];
    }

    String getDownPaymentItemDepNo() {
        return downPaymentItem.split(",")[2];
    }

    /**
     * <li>此DepID列表中包含的列表為專柜商品列表，不可以與普通商品混合銷售，【:】分隔
     * <li>列表為空表示不設限制
     * <li>default 空
     * <li>Nitori special values: "931:932"
     */
    List<String> getExclusiveZhunguiDepsList() {
        try {
            return Arrays.asList(exclusiveZhunguiDeps.split(":"));
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<String>();
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////

/*
    static void main(s) {
        println ">> ${Param.instance.downloadMasterHour instanceof Integer}"
        println ">> ${Param.instance.downloadMasterHour}"
        Param.instance.downloadMasterHour = 2
        //println ">> ${Param.get('downloadMasterHour')}"
        println ">> ${Param.instance.scIpAddress}"
        println ">> ${Param.instance.useRuok}"
        println ">> ${Param.instance.configDir}"
        //Param.instance.setDownloadMasterHour(3)
        println Param.instance.getDownloadMasterHour()
        println Param.instance.getConfigDir()

        println '1'
        Param.instance.downloadMasterHour = 2
        println '2'
        Param.getInstance().updateDownloadMasterHour(3)
    }
*/

//    /**
//     * 當使用Groovy code: Param.instance.Xyz來取值時會利用這個方法獲取property value.
//     * 當值為null時，會取ParamAnnotation.defaultValue.
//     */
//    Object getProperty(String name) {
//        //vvvvvvvvvvvvvvvvvvvvvvv GetProperty vvvvvvvvvvvvvvvvvvvvvvv
//        def metaClass = InvokerHelper.getMetaClass(this)
//        def val = metaClass.getProperty(this, name)
//        if (val == null) {
//            def field = this.class.getDeclaredField(name)
//            def annot = field.getAnnotation(ParamAnnotation.class)
//            def defaultValue = annot.defaultValue()
//            def valueType = field.getType()
//            if (valueType.isInstance(defaultValue))
//                return defaultValue
//            else
//                return defaultValue.asType(valueType)
//        } else
//            return val
//        //^^^^^^^^^^^^^^^^^^^^^^^ GetProperty ^^^^^^^^^^^^^^^^^^^^^^^
//    }
//
//    /**
//     * 當使用Param.getInstance().getXyz()來取值時會利用這個方法獲取property value.
//     * 當值為null時，會取ParamAnnotation.defaultValue.
//     */
//     Object invokeMethod(String methodName, Object args) {
//        System.out.println "invokeMethod(): ${methodName}"
//
//        def metaClass = InvokerHelper.getMetaClass(this)
//
//        if (methodName.startsWith("get") && methodName.length() > 3) {
//            def name = methodName[3].toLowerCase() +
//                (methodName.length() > 4 ? methodName[4..-1] : '')
//            if (name != 'MetaClass') {
//                // 這裡只能照抄getProperty()中的取值代碼。如果直接call getProperty()，
//                // 會導致recursively call invokeMethod(), 然後stack overflow.
//                //vvvvvvvvvvvvvvvvvvvvvvv GetProperty vvvvvvvvvvvvvvvvvvvvvvv
//                def val = metaClass.getProperty(this, name)
//                if (val == null) {
//                    def field = this.class.getDeclaredField(name)
//                    def annot = field.getAnnotation(ParamAnnotation.class)
//                    def defaultValue = annot.defaultValue()
//                    def valueType = field.getType()
//                    if (valueType.isInstance(defaultValue))
//                        return defaultValue
//                    else
//                        return defaultValue.asType(valueType)
//                } else
//                    return val
//                //^^^^^^^^^^^^^^^^^^^^^^^ GetProperty ^^^^^^^^^^^^^^^^^^^^^^^
//            }
//        }
//
//        return metaClass.invokeMethod(this, methodName, args);
//    }

}