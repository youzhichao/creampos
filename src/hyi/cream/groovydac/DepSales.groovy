package hyi.cream.groovydac

import hyi.cream.util.HYIDouble

/**
 * Depsales entity.
 *
 * @author Bruce You
 * @since 2009/3/11 10:45:32
 */
@Table(nameAtPOS = 'depsales', nameAtServer = 'posul_depsales')
public class DepSales extends GroovyEntity {
    String storenumber              // character varying(6) | not null default ''::character varying
    @PrimaryKey int posnumber       // smallint             | not null default (0)::smallint
    @PrimaryKey int sequencenumber  // integer              | not null default 0
    @PrimaryKey String depid        // character varying(4) | not null default ''::character varying
    Date accountdate                // date                 | not null default '1970-01-01'::date
    String uploadstate              // character(1)         | not null default '0'::bpchar
    HYIDouble grosssaletotalamount  // numeric(12,2)        |
    HYIDouble siplustotalamount     // numeric(12,2)        |
    HYIDouble discounttotalamount   // numeric(12,2)        |
    HYIDouble mixandmatchtotalamount// numeric(12,2)        |
    HYIDouble notincludedtotalsale  // numeric(12,2)        |
    HYIDouble netsaletotalamount    // numeric(12,2)        |
    HYIDouble taxamount             // numeric(12,2)        |
}