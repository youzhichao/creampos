package hyi.cream.groovydac;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for specifying GroovyEntity's primary key field.
 *
 * @author Bruce You
 * @since 2009/2/10 下午 11:21:55
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface PrimaryKey {
}