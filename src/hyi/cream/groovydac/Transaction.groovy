package hyi.cream.groovydac

import hyi.cream.util.HYIDouble
import java.sql.Connection
import hyi.cream.util.CreamToolkit
import org.apache.commons.lang.StringUtils

/**
 * Entity class for tranhead.
 *
 * @author Bruce You
 * @since Oct 11, 2008 9:42:00 PM
 */
@Table(nameAtPOS = 'tranhead', nameAtServer = 'posul_tranhead')
class Transaction extends GroovyEntity {

    @PrimaryKey int tmtranseq           // integer NOT NULL DEFAULT 0,
    String trantype         // character(2) NOT NULL DEFAULT ''::bpchar,
    String dealtype1        // character(1) NOT NULL DEFAULT ''::bpchar,
    String dealtype2        // character(1) NOT NULL DEFAULT ''::bpchar,
    String dealtype3        // character(1) NOT NULL DEFAULT ''::bpchar,
    String tmcodep          // character varying(6) NOT NULL DEFAULT ''::character varying,
    int posno               // smallint NOT NULL DEFAULT (0)::smallint,
    int signonid            // smallint NOT NULL DEFAULT (0)::smallint,
    int eodcnt              // integer NOT NULL DEFAULT 0,
    Date accdate            // date NOT NULL DEFAULT '1970-01-01'::date,
    Date sysdate            // timestamp without time zone NOT NULL DEFAULT '1970-01-01 00:00:00'::timestamp without time zone,
    String cashier          // character varying(8),
    String storeno          // character varying(6) NOT NULL DEFAULT ''::character varying,
    String invnohead        // character(2) NOT NULL DEFAULT ''::bpchar,
    String invno            // character varying(8) NOT NULL DEFAULT ''::character varying,
    int invcnt              // integer NOT NULL DEFAULT 0,
    Integer voidseq         // integer,
    String idno             // character varying(8) NOT NULL DEFAULT ''::character varying,
    int detailcnt           // integer NOT NULL DEFAULT 0,
    HYIDouble saleamt       // numeric(12,2) NOT NULL DEFAULT 0.00,
    String payno1           // character(2) NOT NULL DEFAULT ''::bpchar,
    String payno2           // character(2) NOT NULL DEFAULT ''::bpchar,
    String payno3           // character(2) NOT NULL DEFAULT ''::bpchar,
    String payno4           // character(2) NOT NULL DEFAULT ''::bpchar,
    HYIDouble payamt1       // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble payamt2       // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble payamt3       // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble payamt4       // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble taxamt1       // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble taxamt2       // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble taxamt3       // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble taxamt4       // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble taxamt0       // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble changeamt     // numeric(12,2) NOT NULL DEFAULT 0.00,
    String crdtype          // character(2) NOT NULL DEFAULT ''::bpchar,
    String crdno            // character varying(20) NOT NULL DEFAULT ''::character varying,
    HYIDouble overamt       // numeric(12,2) NOT NULL DEFAULT 0.00,
    String empno            // character varying(16) NOT NULL DEFAULT ''::character varying,
    String memberid         // character varying(16) NOT NULL DEFAULT ''::character varying,
    String orderno          // character varying(11) NOT NULL DEFAULT ''::character varying,
    HYIDouble preordamt     // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble tolpmamt      // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble totmamt       // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble invamt        // numeric(12,2) NOT NULL DEFAULT 0.00,
    String inout            // character(1) NOT NULL DEFAULT '0'::bpchar,
    int custid              // integer NOT NULL DEFAULT 0,
    int custcnt             // integer NOT NULL DEFAULT 0,
    HYIDouble rcvtmp        // numeric(12,2) NOT NULL DEFAULT 0.00,
    String saleman          // character varying(8) NOT NULL DEFAULT ''::character varying,
    HYIDouble changamt      // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble grosalamt     // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble grosaltx0amt  // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble grosaltx1amt  // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble grosaltx2amt  // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble grosaltx3amt  // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble grosaltx4amt  // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble siplusamt0    // numeric(12,2) NOT NULL DEFAULT 0.00,
    int sipluscnt0          // integer NOT NULL DEFAULT 0,
    HYIDouble sipamt0       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int sipcnt0             // integer NOT NULL DEFAULT 0,
    HYIDouble simamt0       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int simcnt0             // integer NOT NULL DEFAULT 0,
    HYIDouble mnmamt0       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int mnmcnt0             // integer NOT NULL DEFAULT 0,
    HYIDouble rcpgifamt0    // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble netsalamt0    // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble siplusamt1    // numeric(12,2) NOT NULL DEFAULT 0.00,
    int sipluscnt1          // integer NOT NULL DEFAULT 0,
    HYIDouble sipamt1       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int sipcnt1             // integer NOT NULL DEFAULT 0,
    HYIDouble simamt1       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int simcnt1             // integer NOT NULL DEFAULT 0,
    HYIDouble mnmamt1       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int mnmcnt1             // integer NOT NULL DEFAULT 0,
    HYIDouble rcpgifamt1    // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble netsalamt1    // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble siplusamt2    // numeric(12,2) NOT NULL DEFAULT 0.00,
    int sipluscnt2          // integer NOT NULL DEFAULT 0,
    HYIDouble sipamt2       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int sipcnt2             // integer NOT NULL DEFAULT 0,
    HYIDouble simamt2       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int simcnt2             // integer NOT NULL DEFAULT 0,
    HYIDouble mnmamt2       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int mnmcnt2             // integer NOT NULL DEFAULT 0,
    HYIDouble rcpgifamt2    // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble netsalamt2    // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble siplusamt3    // numeric(12,2) NOT NULL DEFAULT 0.00,
    int sipluscnt3          // integer NOT NULL DEFAULT 0,
    HYIDouble sipamt3       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int sipcnt3             // integer NOT NULL DEFAULT 0,
    HYIDouble simamt3       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int simcnt3             // integer NOT NULL DEFAULT 0,
    HYIDouble mnmamt3       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int mnmcnt3             // integer NOT NULL DEFAULT 0,
    HYIDouble rcpgifamt3    // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble netsalamt3    // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble siplusamt4    // numeric(12,2) NOT NULL DEFAULT 0.00,
    int sipluscnt4          // integer NOT NULL DEFAULT 0,
    HYIDouble sipamt4       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int sipcnt4             // integer NOT NULL DEFAULT 0,
    HYIDouble simamt4       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int simcnt4             // integer NOT NULL DEFAULT 0,
    HYIDouble mnmamt4       // numeric(12,2) NOT NULL DEFAULT 0.00,
    int mnmcnt4             // integer NOT NULL DEFAULT 0,
    HYIDouble rcpgifamt4    // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble netsalamt4    // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble netsalamt     // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble daishouamt    // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble daishouamt2   // numeric(12,2) NOT NULL DEFAULT 0.00,
    HYIDouble daifuamt      // numeric(12,2) NOT NULL DEFAULT 0.00,
    Date crdend             // date,
    String annotatedid      // character varying(15),
    String annotatedtype    // character(1),
    String paycrdno         // character varying(16) NOT NULL DEFAULT ''::character varying,

    public List<LineItem> lineItems = []

    static Transaction queryByTransactionNumber(Connection connection,
        int posNumber, int transactionNumber) {

        Transaction trans = selectSingleRow(connection, Transaction.class, """
            SELECT * FROM ${tableName(Transaction.class)} WHERE posno=${posNumber} AND
            tmtranseq=${transactionNumber}""")

        if (trans != null) {
            trans.lineItems = selectMultipleRows(connection, LineItem.class, """
                SELECT * FROM ${tableName(LineItem.class)} WHERE tmcode=${posNumber} AND
                tmtranseq=${transactionNumber} ORDER BY itemseq""")
        }
        return trans
    }

    def getNitoriPaymentEnglishName(int idx) {
        String payno = this."payno${++idx}".trim()
        return StringUtils.isEmpty(payno) ? '' :
            payno == '00' ? 'Cash' :
            payno == '01' ? 'Credit Card' :
            payno == '02' ? 'LiJuan' :
            payno == '03' ? 'DiYongJuan' : 'Other'
    }
/*
    static void main(s) {
        def x = queryByTransactionNumber(CreamToolkit.getPooledConnection(), 1, 411)
        println x.netsalamt
        for (lineItem in x.lineItems)
            println lineItem.aftdscamt
    }
*/
}