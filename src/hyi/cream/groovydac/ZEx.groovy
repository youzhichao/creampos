package hyi.cream.groovydac

import hyi.cream.util.HYIDouble

/**
 * Entity class for zex.
 *
 * @author Bruce You
 * @since 2009/2/18 21:49:07
 */
@Table(nameAtPOS = 'zex', nameAtServer = 'posul_zex')
public class ZEx extends GroovyEntity {

    @PrimaryKey int posno // smallint NOT NULL,
    @PrimaryKey int eodcnt // integer NOT NULL,
    @PrimaryKey String code // character varying(24) NOT NULL,
    HYIDouble amount // numeric(12,2) NOT NULL,
    Date accdate // date DEFAULT '1970-01-01'::date,
    String storeno // character(6) NOT NULL,
    String tcpflg // character(1) NOT NULL,

}