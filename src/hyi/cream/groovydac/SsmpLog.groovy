package hyi.cream.groovydac

import hyi.cream.dac.Store
import hyi.cream.inline.Client
import hyi.cream.util.HYIDouble
import hyi.cream.util.ChineseConverter
import java.util.concurrent.LinkedBlockingQueue
import hyi.cream.inline.Server
import hyi.cream.Version
import groovy.sql.Sql

import static hyi.cream.util.CreamToolkit.logMessage
import hyi.cream.util.CreamToolkit

/**
 * ssmp_log entity.
 *
 * @author Bruce You
 * @since 2009/4/25 23:51:37
 */
@Table(nameAtPOS = 'ssmp_log', nameAtServer = 'commul_ssmp_log')
public class SsmpLog extends GroovyEntity {

    private static final long serialVersionUID = 1L;

    transient public static final Param PARAM = Param.instance

    //////////////////////////////////////////////////////////////////////
    // CONSTANTS /////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    /** 事件類型 I: 信息 */
    public static final String LOGLEVEL_INFO = 'I'
    /** 事件類型 W: 警告 */
    public static final String LOGLEVEL_WARN = 'W'
    /** 事件類型 E: 錯誤 */
    public static final String LOGLEVEL_ERROR = 'E'

    /** 事件大分類 01:硬件网络 */
    public static final String CAT_HW = '01'
    /** 事件大分類 02:软件环境 */
    public static final String CAT_SW = '02'
    /** 事件大分類 03:程序版本 */
    public static final String CAT_VERSION = '03'
    /** 事件大分類  04:业务数据 */
    public static final String CAT_DATA = '04'
    /** 事件大分類 05:通讯系统 */
    public static final String CAT_COMM = '05'
    /** 事件大分類 06:用户操作 */ 
    public static final String CAT_USEROP = '06'

    /** 事件中分類 SA: 销售 */
    public static final String MIDCAT_SALES = 'SALE'
    /** 事件中分類 OR: 订货 */
    public static final String MIDCAT_ORDER = 'ORD'
    /** 事件中分類 DL: 验收 */
    public static final String MIDCAT_DELIVERY = 'DLV'
    /** 事件中分類 PD: 配送传票 */
    public static final String MIDCAT_PEIDA = 'PD'

    //////////////////////////////////////////////////////////////////////
    // TABLE FIELDS //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    long id // int(10) unsigned NOT NULL auto_increment COMMENT 'Unique id',

    @FieldName(nameAtPOS = 'sc_id', nameAtServer = '')
    long scId // bigint, -- Unique ID on SC

    @FieldName(nameAtPOS = 'store_id', nameAtServer = 'storeID')
    String storeId // varchar(8) NOT NULL COMMENT '门市代号',

    @FieldName(nameAtPOS = '', nameAtServer='updateBeginDate')
    Date updateBeginDate // date NOT NULL COMMENT '传输用日期',

    @FieldName(nameAtPOS = '', nameAtServer='sequenceNumber')
    short sequenceNumber // tinyint(3) NOT NULL COMMENT '传输用当日序号',

    @FieldName(nameAtPOS = 'log_time', nameAtServer = 'logTime')
    Date logTime    // datetime NOT NULL COMMENT '日期时间',

    @FieldName(nameAtPOS = 'log_level', nameAtServer = 'logLevel')
    String logLevel // char(1) character set latin1 NOT NULL COMMENT '''I'': 信息, ''W'': 警告, ''E'': 错误',

    String severity // char(1) character set latin1 default NULL COMMENT '1 ~ 5: 轻微 ~ 致命 (type=''I'' 时可以为NULL)',
    String source   // varchar(3) character set latin1 NOT NULL COMMENT '''SC'': SC, ''EC'': eComm, ''001’: POS1, ''002'': POS2, ...',
    String cat      // char(2) character set latin1 NOT NULL COMMENT '事件分类',
    String midcat   // char(4) character set latin1 NOT NULL COMMENT '''SALE'': 销售, ''ORD'': 订货, ''DLV'': 验收, ''PD'': 配送传票, ...',
    int code        // smallint(6) default NULL COMMENT '错误代码 (type=''I'' 时可以为NULL)',
    String brief    // varchar(64) NOT NULL COMMENT '简要说明',
    String detail   // varchar(512) default NULL COMMENT '详细描述或程序Exception log',

    //////////////////////////////////////////////////////////////////////
    // WORKER THREAD /////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    private static def ssmpLogQueue = new LinkedBlockingQueue<SsmpLog>(38)
    private static final CURR_STORE_ID = Store.getStoreID()
    private static final SSMP_POS_NO = PARAM.getSsmpPosNo()
    private static final boolean GENERATE_SSMP_LOG = PARAM.generateSsmpLog

    static {
        if (!Server.isAtServerSide() && GENERATE_SSMP_LOG) {
            Thread.startDaemon("SsmpLog thread") {
                def inlineClient = Client.instance
                while (true) {
                    try {
                        def ssmpLog = ssmpLogQueue.take()
                        ssmpLog.setStoreId CURR_STORE_ID
                        ssmpLog.setSource SSMP_POS_NO
                        ssmpLog.insert()
                        if (ssmpLogQueue.isEmpty()) {
                            def retry = 1
                            while (!inlineClient.isConnected()) {
                                if (++retry > 5)
                                    break
                                sleep 10 * 1000L
                            }
                            if (retry <= 5)
                                inlineClient.processCommand 'putSsmpLog'
                        }
                    } catch (Exception e) {
                        e.printStackTrace()
                    }
                }
            }.setPriority Thread.MIN_PRIORITY
        }
    }
    
    //////////////////////////////////////////////////////////////////////
    // QUERY METHODS /////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    @Override
    public String toString() {
        return "SsmpLog {logTime=${logTime}, logLevel='${logLevel}', severity='${severity}'" +
            ", source='${source}', cat='${cat}', midcat='${midcat}', code=${code}}'"
    }

    /**
     * Find all unsent records' ids. Only used on POS side.
     */
    static List<Integer> findUnsentSsmpLogIds() {
        return selectMultipleRows('SELECT id FROM ssmp_log WHERE sc_id=0 OR sc_id IS NULL').sort()
    }

    //////////////////////////////////////////////////////////////////////
    // REPORT METHODS ////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    private static def chineseConverter = ChineseConverter.instance
    private static def c(msg) { chineseConverter.convert(msg) }

    private static stackTrace(Exception e) {
        def sw = new StringWriter()
        e.printStackTrace(new PrintWriter(sw))
        String stackTrace = (sw.getBuffer().length() > 500) ? sw.getBuffer().substring(0, 500) : sw.getBuffer().toString()
        // replace line-feed with symbol '\n', the workaround for the problem of eComm
        stackTrace.replaceAll(/\n/, /\\n/)
    }

    static void report10001(String barcode) {
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'W', severity: '2', cat: CAT_DATA, midcat: MIDCAT_SALES, code: 10001,
                brief: c("POS掃描查無此商品:${barcode}"))
    }

    static void report10002(Exception e) {
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'E', severity: '5', cat: CAT_DATA, midcat: MIDCAT_SALES, code: 10002,
                brief: c('POS{database}異常'), detail: stackTrace(e))
    }

    static void report10003(String cashierNo, String pluNo, String pluName,
            HYIDouble originalPrice, HYIDouble newPrice) {
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'I', cat: CAT_DATA, midcat: MIDCAT_SALES, code: 10003,
                brief: c('商品變價'),
                detail: c("收銀員${cashierNo}將商品\"${pluNo}${pluName}\"從原價${originalPrice}元變價成${newPrice}元"))
    }

    static void report10004(String taxId, String depId, HYIDouble amount) {
        def depStr = (depId != null) ? ",部門'${depId}'）" : ''
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'I', cat: CAT_USEROP, midcat: MIDCAT_SALES, code: 10004,
                brief: c("手輸稅別金額${amount}元（稅別'${taxId}'${depStr}）"))
    }

    static void report10005(String catId, HYIDouble amount) {
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'I', cat: CAT_USEROP, midcat: MIDCAT_SALES, code: 10005,
                brief: c("手輸分類金額${amount}元（分類代碼'${catId}'）"))
    }

    static void report10006(String productCode) {
        //if (GENERATE_SSMP_LOG)
        //    ssmpLogQueue.put new SsmpLog(logTime: new Date(),
        //        logLevel: 'I', cat: CAT_USEROP, midcat: MIDCAT_SALES, code: 10006,
        //        brief: c("手輸條碼或品號'${productCode}'"))
    }

    static void report10007(int tranNo) {
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'I', cat: CAT_USEROP, midcat: MIDCAT_SALES, code: 10007,
                brief: c("重印交易:${tranNo}"))
    }

    static void report10008(int tranNo) {
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'I', cat: CAT_USEROP, midcat: MIDCAT_SALES, code: 10008,
                brief: c("交易取消:${tranNo}"))
    }

    static void report10009(String productCode) {
        //if (GENERATE_SSMP_LOG)
        //    ssmpLogQueue.put new SsmpLog(logTime: new Date(),
        //        logLevel: 'I', cat: CAT_USEROP, midcat: MIDCAT_SALES, code: 10009,
        //        brief: c("單品更正:${productCode}"))
    }

    static void report10010() {
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'I', cat: CAT_VERSION, midcat: MIDCAT_SALES, code: 10010,
                brief: c("POS系統啟動（版本:${Version.getVersion()}）"))
    }

    static void report10011(Exception e) {
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'E', cat: CAT_COMM, midcat: MIDCAT_SALES, code: 10011, severity: 4,
                brief: c("主檔下傳失敗"), detail: stackTrace(e))
    }

    static void report10012() {
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'I', cat: CAT_COMM, midcat: MIDCAT_SALES, code: 10012,
                brief: c("主檔下傳成功"))
    }

    static void report10013(List scriptFiles) {
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'I', cat: CAT_COMM, midcat: MIDCAT_SALES, code: 10013,
                brief: c("{program}{file}下傳成功: $scriptFiles"))
    }

    static void report11001() {
        if (GENERATE_SSMP_LOG)
            ssmpLogQueue.put new SsmpLog(logTime: new Date(),
                logLevel: 'W', cat: CAT_HW, midcat: MIDCAT_SALES, code: 11001, severity: 1,
                brief: c('{printer}沒正確定位'))
    }

    /**
     * Delete ssmp logs seven days ago.
     */
    static int deleteOutdatedData() {
        if (GENERATE_SSMP_LOG) {
        def dbConn
            try {
                dbConn = dbConnection()
                Sql sql = new Sql(dbConn)
                def dueDate = CreamToolkit.shiftDay(new Date(), -7)
                return sql.executeUpdate("DELETE FROM ${tableName(SsmpLog)} WHERE logTime<?".toString(), [ dueDate ])
            } catch (Exception e) {
                logMessage(e)
                return 0
            } finally {
                dbConn?.closeIfNeeds()
            }
        }
        return 0;
    }

    /*static void main(s) {
        report10001('123')
        //Server.setFakeExist()
        new SsmpLog(storeId: 'STORE', updateBeginDate: new Date(), sequenceNumber: 1,
            logTime: new Date(), logLevel: 'I', source: '001', cat: '00', midcat: 'SA', code: 123,
            brief: 'Just a test').insert()

        def ssmpLog = new SsmpLog(id: 1)
        println ssmpLog.load()
        println ssmpLog.scId
        println ssmpLog.logLevel
        println ssmpLog.brief
        println findUnsentSsmpLogIds()
    }*/
}