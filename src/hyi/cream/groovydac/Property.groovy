package hyi.cream.groovydac

import hyi.cream.inline.Server

/**
 * Property entity.
 *
 * @author Bruce You
 * @since 2009/2/9 17:52:57
 */
@Table(nameAtPOS = 'property', nameAtServer = 'property')
public class Property extends GroovyEntity {

    @PrimaryKey
    String name

    String value

    static Map<String, String> queryAll() {
        if (!Server.isAtServerSide()) {
            def props = selectMultipleRows(Property.class, "SELECT * FROM property")
            def map = [:]
            for (prop in props)
                map[prop.name] = prop.value
            return map
        } else {
            return [:]
        }
    }
}