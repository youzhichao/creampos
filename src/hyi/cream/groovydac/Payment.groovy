package hyi.cream.groovydac

import hyi.cream.util.HYIDouble

/**
 * Payment entity.
 *
 * @author Bruce
 * @since 2009/2/10 19:23:08
 */
@Table(nameAtPOS = 'payment', nameAtServer = 'posdl_payment')
public class Payment extends GroovyEntity {

    @PrimaryKey String payid    // character(2) NOT NULL DEFAULT ''::bpchar,
    String payname          // character varying(10) NOT NULL DEFAULT ''::character varying,
    String paycname = ''    // character varying(14) NOT NULL DEFAULT ''::character varying,
    String paytype          // character varying(8) NOT NULL DEFAULT ''::character varying,
    String payrate          // character(2),
    HYIDouble denomination  // numeric(12,2) NOT NULL DEFAULT 1.00,
    String paycategory      // character(2),
    int priority            // integer DEFAULT 0,
    String seqno            // character varying(2) DEFAULT 0,

    /*static void main(s) {
        new Payment([
            payid: '98',
            payname: '消費券',
            paycname: '消費券',
            paytype: '10100000',
            payrate: '0',
            denomination: new HYIDouble(1.00),
            paycategory: null,
            priority: 0,
            seqno: '95']).present()
    }*/
}