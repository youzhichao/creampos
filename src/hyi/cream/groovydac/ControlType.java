package hyi.cream.groovydac;

public enum ControlType {
    EditBox, CheckBox, ComboBox;
}