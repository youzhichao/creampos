package hyi.cream.groovydac

import hyi.cream.util.HYIDouble
import java.sql.Time
import java.sql.Connection

/**
 * Plu entity.
 *
 * @author Bruce You
 * @since 2009/2/11 18:19:15
 */
@Table(nameAtPOS = 'plu', nameAtServer = 'posdl_plu')
public class Plu extends GroovyEntity {

    @PrimaryKey String pluno // character varying(20) NOT NULL DEFAULT ''::character varying,
    String itemno       // character varying(20) NOT NULL DEFAULT ''::character varying,
    String saletype     // character(1),
    String pluname      // character varying(64),
    String printname    // character varying(64),
    String catno        // character varying(4) NOT NULL DEFAULT ''::character varying,
    String midcatno     // character varying(4),
    String microcatno   // character varying(4),
    HYIDouble pluprice  // numeric(8,2) NOT NULL DEFAULT 0.00,
    HYIDouble plupmprice // numeric(8,2),
    Date plupmdates     // date,
    Date plupmdatee     // date,
    Time plupmtimes     // time without time zone,
    Time plupmtimee     // time without time zone,
    HYIDouble plumbprice    // numeric(8,2),
    HYIDouble pluopprice1   // numeric(8,2),
    HYIDouble pluopprice2   // numeric(8,2),
    int plusign1        // smallint,
    int plusign2        // smallint,
    Date discbegdt = new Date(0)  // date NOT NULL DEFAULT '1970-01-01'::date,
    Date discenddt = new Date(0)  // date NOT NULL DEFAULT '1970-01-01'::date,
    int plusign3        // smallint,
    String plutax       // character(1),
    String mamno        // character varying(4),
    String depid        // character(4) NOT NULL DEFAULT ''::bpchar,
    String shipno       // character(1),
    String smallunit    // character varying(2),

    static Plu queryByItemNo(String itemNo) {
        return selectSingleRow(Plu.class,
                "SELECT * FROM ${tableName(Plu.class)} WHERE itemno=?", [itemNo])
    }

    /*static void main(s) {
        Plu p = new Plu([pluno: '40000527'])
        def conn = newDBConnection()
        println '--------------'
        if (p.load(conn)) {
            println p.itemno
            println p.pluprice
            println p.plupmdates
            println p.plupmtimee
        }
        println '--------------'
        p = queryByItemNo('4000052')
        if (p != null) {
            println p.itemno
            println p.pluprice
            println p.plupmdates
            println p.plupmtimee
        }
        conn.close()
    }*/
}