package hyi.cream.groovydac;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for specifying GroovyEntity's field name.
 *
 * @author Bruce You
 * @since 2009/4/26 00:05:20
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldName {
    String nameAtPOS() default "";
    String nameAtServer() default "";
}
