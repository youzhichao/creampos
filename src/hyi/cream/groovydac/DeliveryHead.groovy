package hyi.cream.groovydac

import hyi.cream.util.HYIDouble
import java.sql.Connection
import groovy.sql.Sql
import org.apache.commons.lang.StringUtils
import hyi.cream.util.CreamToolkit
import hyi.cream.dac.Store
import hyi.cream.inline.Server

/**
 * DeliveryHead entity at server side.
 *
 * @author Bruce You
 * @since 2008/9/11 下午 06:06:55
 */
@Table(nameAtPOS = 'deliveryhead', nameAtServer = 'deliveryhead')
class DeliveryHead extends GroovyEntity {

    // Table fields --

    String storeID          // | varchar(12)         | NO   | PRI | NULL
    Date accountDate        // | date                | NO   | PRI | 1970-01-01
    String deliveryNo       // | varchar(13)         | NO   | PRI | NULL
    Integer ver             // | tinyint(3) unsigned | YES  |     | NULL
    String salesClerk       // | varchar(13)         | YES  |     | NULL
    String coBillNo         // | varchar(13)         | YES  |     | NULL
    String srcBillNo        // | varchar(13)         | YES  |     | NULL
    String billType         // | char(1)             | YES  |     | NULL
    Integer posNumber       // | tinyint(3) unsigned | YES  |     | NULL
    Integer transactionNumber // | int(10) unsigned    | YES  |     | NULL
    HYIDouble totalAmount   // | decimal(13,3)       | YES  |     | NULL
    String billStatus       // | char(1)             | YES  |     | NULL
    String payStatus        // | char(1)             | YES  |     | NULL
    String turnStatus       // | char(1)             | YES  |     | NULL
    String isToBuyerHome    // | char(1)             | YES  |     | NULL
    String custName         // | varchar(28)         | YES  |     | NULL
    String gender           // | char(1)             | YES  |     | NULL
    String phoneNo0         // | varchar(16)         | YES  |     | NULL
    String phoneNo1         // | varchar(16)         | YES  |     | NULL
    String zipCode          // | varchar(8)          | YES  |     | NULL
    String addrCity         // | varchar(10)         | YES  |     | NULL
    String addrCityArea     // | varchar(10)         | YES  |     | NULL
    String addrRoadVillage  // | varchar(30)         | YES  |     | NULL
    String addrRoadSect     // | varchar(4)          | YES  |     | NULL
    String addrLane         // | varchar(12)         | YES  |     | NULL
    String addrAlley        // | varchar(12)         | YES  |     | NULL
    String addrNo           // | varchar(10)         | YES  |     | NULL
    String addrNoSubNo      // | varchar(10)         | YES  |     | NULL
    String addrFloorNo      // | varchar(10)         | YES  |     | NULL
    String addrFloorSubNo   // | varchar(10)         | YES  |     | NULL
    String addrRoom         // | varchar(10)         | YES  |     | NULL
    String addrNearCross    // | varchar(20)         | YES  |     | NULL
    String addrNearSite     // | varchar(10)         | YES  |     | NULL
    String addrBuildName    // | varchar(10)         | YES  |     | NULL
    String addrMemo         // | varchar(120)        | YES  |     | NULL
    String houseType        // | char(1)             | YES  |     | NULL
    Integer houseFloors     // | tinyint(3)          | YES  |     | NULL
    String houseLift        // | char(1)             | YES  |     | NULL
    String carrier          // | varchar(8)          | YES  |     | NULL
    String carryType        // | varchar(8)          | YES  |     | NULL
    String carryFrom        // | varchar(8)          | YES  |     | NULL
    Integer targetFloor     // | tinyint(3)          | YES  |     | NULL
    Date arriveDate         // | date                | YES  |     | NULL
    String arriveWellTime   // | varchar(20)         | YES  |     | NULL
    String withDraw         // | varchar(10)         | YES  |     | NULL
    String custInfo         // | varchar(255)        | YES  |     | NULL
    String memo             // | varchar(255)        | YES  |     | NULL
    String updateUserId     // | varchar(8)          | YES  |     | NULL
    Date updateDateTime     // | datetime            | YES  |     | NULL

    // Constants --

    public static final String DELIVERY_BILL_STATUS_REVOKE = '0'; //配送狀態 作廢
    public static final String DELIVERY_BILL_STATUS_NORMAL = '1'; //配送狀態 正常
    public static final String DELIVERY_BILL_STATUS_BE_RETURNED = '2'; //配送狀態 被退過貨的
    //public static final String DELIVERY_BILL_STATUS_RETURN = '3'; //配送狀態 退貨

//  /**
//   * 配送退貨費用調節專用商品之條碼
//   * 該商品應該 可以變價，不可以庫存,不可以做報廢，不可以訂貨
//   */
//  public static final String DELIVERY_RETURN_FEE_ADJUST_PLUNO = "91099204"; //配送退貨費用調節專用商品之條碼

    public static final String DELIVERY_PAYSTATUS_NOT_PAY= '1';     //未付款
    public static final String DELIVERY_PAYSTATUS_POS_PAID= '2';    //pos結清
    public static final String DELIVERY_PAYSTATUS_MANUAL_PAID= '3'; //人工結清"
    public static final String DELIVERY_PAYSTATUS_POS_PART= '4';    //pos部分結
    public static final String DELIVERY_PAYSTATUS_MANUAL_PART= '5'; //人工部分結

    public static final String DELIVERY_TURNSTATUS_NOT_TURN= '0';   //未做上傳檔
    public static final String DELIVERY_TURNSTATUS_TURNED= '1';     //已做上傳檔
    public static final String DELIVERY_TURNSTATUS_UPLOADED= '2';   //已上傳

    public static final String DELIVERY_BILL_TYPE_NORMAL = 'P';         //普通配送
    public static final String DELIVERY_BILL_TYPE_CURTAIN_MODIFY = 'M'; //窗簾修改
    public static final String DELIVERY_BILL_TYPE_CURTAIN_ORDER = 'O';  //窗簾訂做
    public static final String DELIVERY_BILL_TYPE_AFTER = 'A';          //售後處理
    public static final String DELIVERY_BILL_TYPE_WHOLESALE = 'H';      //wholeSale

    public static final String DELIVERY_BILL_TYPE1_SALES = 'S';     //售後處理
    public static final String DELIVERY_BILL_TYPE1_RETURN = 'R';    //售後處理

    public static final int DELIVERY_NUMBER_STOREID_LENGTH = 3;

    public static final String DELIVERY_DISCOUNT_NUMBER_PREFIX = "O";
    public static final String DELIVERY_DISCOUNT_TYPE= 'O';
    public static final String DELIVERY_DETAILCODE_SALES= 'S';	        //正常銷售
    public static final String DELIVERY_DETAILCODE_RETURN= 'R';         //退貨(紅沖)
    public static final String DELIVERY_DETAILCODE_SEND_OUT= 'O';       //送出
    public static final String DELIVERY_DETAILCODE_WITHDRAW_IN= 'I';    //取回
    public static final String DELIVERY_DETAILCODE_EXCHANGE= 'X';       //交換

    public static final String DELIVERY_HOUSE_NO_LIFT = '0';    //無電梯
    public static final String DELIVERY_HOUSE_HAVE_LIFT = '1';  //有電梯

    public static final String DELIVERY_HOUSETYPE_APARTMENT = '1';  //公寓住宅
    public static final String DELIVERY_HOUSETYPE_VILLA = '2';      //獨院住宅

    public static final String DELIVERY_IS_TO_BUYER_HOME = '0';     // 自家
    public static final String DELIVERY_NOT_TO_BUYER_HOME = '1';    // 別家

    public static final String DELIVERY_CARRY_TYPE_TO_HOME = '1';   // 送貨到府
    public static final String DELIVERY_CARRY_TYPE_SELF_TAKE = '2'; // 來店自取

//  public static final String DELIVERY_CARRY_FROM_SUPPPLIER = '1'; // 供應商
//  public static final String DELIVERY_CARRY_FROM_STORE = '2'; // 門店
//  public static final String DELIVERY_CARRY_FROM_DEPOT = '3'; // 倉庫

    /**
     * Used by inline server.
     */
    static Collection queryObject(String command, String storeID, String deliveryNo, String ver) {
        Connection connection
        try {
            connection = CreamToolkit.getPooledConnection()

            List returnObjects = []
            switch (command) {
            case 'getDeliveryStatus':
                returnObjects << queryDeliveryStatus(connection, storeID, deliveryNo, ver.toInteger())
                break

            case 'getOrigTranNoFromDeliveryIDForRefund':
                returnObjects << queryOrigTranNoFromDeliveryIDForRefund(connection, storeID, deliveryNo)
                break

            case 'getOrigTranNoFromDeliveryID':
                returnObjects << queryOrigTranNoFromDeliveryID(connection, storeID, deliveryNo)
                break

            case 'getPosDeliveryDtl':
                returnObjects = queryPosDeliveryDtlByHeadPrimaryKey(connection, storeID, deliveryNo, ver.toInteger())
                break
            }
            return returnObjects;
        } finally {
            connection?.close()
        }
    }

    static Collection queryObject(String command, String storeID, String deliveryNo, String posNumber,
        String transactionNumber, String updateUserId) {
        Connection connection
        try {
            connection = CreamToolkit.getPooledConnection()
            List returnObjects = []
            switch (command) {
            case 'updateDeliveryStatus':
                returnObjects << updateDeliveryStatus(connection, storeID, deliveryNo, posNumber,
                    transactionNumber, updateUserId)
                break
            }
            return returnObjects;
        } finally {
            connection?.close()
        }
    }


    /**
     * @param connection the transaction context
     * @param storeID store number
     * @param deliveryNo 傳票單號
     */
    static queryDeliveryHeads(Connection connection, String storeID, String deliveryNo) {
        if (deliveryNo.length() > 8) // 如果傳票編號包括版本號，把版本號截掉
            deliveryNo = deliveryNo.substring(0, 8)
        return selectMultipleRows(connection, DeliveryHead.class,
            """select * from deliveryhead where storeID='${storeID}' AND deliveryNo='${deliveryNo}'
               order by accountDate desc""")
    }

    static DeliveryHead queryDeliveryHead(Connection connection, String storeID, String deliveryNo) {
        def list = queryDeliveryHeads(connection, storeID, deliveryNo)
        return (list != null && !list.isEmpty()) ? list[0] : null
    }

    /**
     * @return deliveryhead.結帳狀態 + "," + deliveryhead.srcBillNo(原始單號)
     *      -1=无法连接sc、1=不存在、2=已作廢、3=售後處理、4=結清,pos不可結、5=被退過貨的、6=單據版本不符合、
     *      7=退货机可退貨,pos不可结帐、8=异常
     *      可結帳 -> P=普通配達、M=窗簾修改、O=窗簾訂做
     */
    static def queryDeliveryStatus(Connection connection, String storeID, String deliveryNo, int ver) {
        DeliveryHead h = queryDeliveryHead(connection, storeID, deliveryNo)
        if (h == null)
            return '1,x'     // 不存在
        def srcBillNo = ",${h.srcBillNo}"
        if (DELIVERY_BILL_STATUS_REVOKE.equals(h.getBillStatus()))
            return '2' + srcBillNo // 已作廢
        if (DELIVERY_BILL_TYPE_AFTER.equals(h.getBillType()))
            return '3' + srcBillNo // 售後處理
        if (!DELIVERY_PAYSTATUS_NOT_PAY.equals(h.getPayStatus()))
            return '4' + srcBillNo // 結清,pos不可結
        if (DELIVERY_BILL_STATUS_BE_RETURNED.equals(h.getBillType()))
            return '5' + srcBillNo // 被退過貨的

        def v = h.getVer()
        if (v == null)
            return '6' + srcBillNo // 單據版本不符合
        String vStr = v.toString()
        if (vStr.length() > 2) // 修改版本大于兩位的時候，只檢測最後兩位
            v = vStr.substring(vStr.length() - 3, vStr.length() - 1).toInteger()
        if (v != ver)
            return '6' + srcBillNo // 單據版本不符合

        if (DELIVERY_BILL_STATUS_NORMAL.equals(h.getBillStatus())) { // 正常
            if (DELIVERY_PAYSTATUS_NOT_PAY.equals(h.getPayStatus())) {
                if (h.getSrcBillNo() != null
                    && !h.getSrcBillNo().trim().equals("")) {
                    return '7' + srcBillNo // 退货机可退貨
                }
                // 未結帳狀態,pos可結
                if (DELIVERY_BILL_TYPE_NORMAL.equals(h.getBillType()))  { // 普通配送
                    return DELIVERY_BILL_TYPE_NORMAL + srcBillNo
                }
                else if (DELIVERY_BILL_TYPE_WHOLESALE.equals(h.getBillType())) //Wholesale
                    return DELIVERY_BILL_TYPE_WHOLESALE + srcBillNo
                else if (DELIVERY_BILL_TYPE_CURTAIN_MODIFY.equals(h.getBillType())) // 窗簾修改
                    return DELIVERY_BILL_TYPE_CURTAIN_MODIFY + srcBillNo
                else if (DELIVERY_BILL_TYPE_CURTAIN_ORDER.equals(h.getBillType())) // 窗簾訂做
                    return DELIVERY_BILL_TYPE_CURTAIN_ORDER + srcBillNo
                else
                    return '8' + srcBillNo
            }
        }
        return '8' + srcBillNo // 异常
    }

    static List<Map> queryPosDeliveryDtlByHeadPrimaryKey(Connection connection,
        String storeID, String deliveryNo, int ver) {
        //deliveryNo = addPrefix(storeID, deliveryNo)
        DeliveryHead h = queryDeliveryHead(connection, storeID, deliveryNo)
        if (h == null || h.getVer() == null || !h.getVer() == ver)
            return null
        List<DeliveryDtl> resultList = selectMultipleRows(connection, DeliveryDtl.class,
            "select * from deliverydtl where storeID=? and accountDate=? AND deliveryNo=? order by itemSeq",
            [storeID, h.accountDate, deliveryNo])

        List<Map> posDtls = new ArrayList<Map>()

        // pos端需要知道head的信息
        def head = [:]
        head.put("billType", h.getBillType().toString())
        head.put("saleMan", h.getSalesClerk())
        //head.put("memberID", h.getMemberID())
        posDtls.add(head)
        int seq = 0
        for (DeliveryDtl o: resultList) {
            Map<String, Object> pd = new HashMap<String, Object>()
            posDtls.add(pd)
            seq++
            pd.put("StoreID", storeID)
            pd.put("ITEMSEQ", seq)
            pd.put("CATNO", o.getCategoryNumber())
            pd.put("MIDCATNO", o.getMidCategoryNumber())
            pd.put("MICROCATNO", o.getMicroCategoryNumber())
            pd.put("PLUNO", o.getPluNumber())
            pd.put("UNITPRICE", o.getUnitPrice())
            pd.put("QTY", o.getQuantity())
            pd.put("AMT", o.getAmount())
            pd.put("TAXTYPE", o.getTaxType() == null ? null : o.getTaxType().toString())
            pd.put("TAXAMT", o.getTaxAmount())
            pd.put("ITEMNO", o.getItemNumber())
            pd.put("ORIGPRICE", o.getOriginalPrice())
            pd.put("AFTDSCAMT", o.getAfterDiscountAmount())
            pd.put("DetailCode", o.getDetailCode() == null ? null : o.getDetailCode().toString())
            pd.put("Description", o.getDisplayName())
            //pd.put("authorNo", o.getOpenWholeSalePriceUser())
        }
        return posDtls
    }

    /**
     * 如果是他店來查，storeID會是那家店，deliveryNo會是原始傳票編號。否則storeID是本店，deliveryNo是傳票的編號。
     */
    static int queryOrigTranNoFromDeliveryID(Connection connection, String storeID, String deliveryNo) {
        if (Store.getStoreID().equals(storeID)) {
            // 從退貨傳票的srcBillNo查出原始傳票編號
            DeliveryHead h = queryDeliveryHead(connection, storeID, deliveryNo);
            if (h != null && !StringUtils.isEmpty(h.getSrcBillNo())) {
                ////// ---> && DELIVERY_PAYSTATUS_NOT_PAY.equals(h.getPayStatus())) { 現在不能檢查這個，因為付殘金和退殘金的時候，在已結帳時仍需進行交易
                // 退货机可退貨

                // 如果查到id一样的取交易序号最小的一笔(這樣在退訂金的時候，才不會抓錯到付尾款那筆交易)

                Sql sql = new Sql(connection)
                def row = sql.firstRow("SELECT transactionNumber, posNumber FROM posul_tranhead WHERE annotatedid LIKE '"
                    + h.getSrcBillNo() + "%' ORDER BY transactionNumber LIMIT 1")
                if (row != 0) {
                    def tranNo = row[0];
                    def posNo = row[1];
                    return tranNo * 100 + posNo;
                }
            }
        } else {
            Sql sql = new Sql(connection)
            def row = sql.firstRow("SELECT transactionNumber, posNumber FROM posul_tranhead WHERE annotatedid LIKE '"
                + deliveryNo + "%' ORDER BY transactionNumber LIMIT 1")
            if (row != 0) {
                def tranNo = row[0];
                def posNo = row[1];
                return tranNo * 100 + posNo;
            }
        }
        return -1;
    }

    /**
     * 如果是他店來查，storeID會是那家店，deliveryNo會是原始傳票編號。否則storeID是本店，deliveryNo是退貨傳票的編號。
     */
    static int queryOrigTranNoFromDeliveryIDForRefund(Connection connection, String storeID, String deliveryNo) {
        if (Store.getStoreID().equals(storeID)) {
            // 從退貨傳票的srcBillNo查出原始傳票編號
            DeliveryHead h = queryDeliveryHead(connection, storeID, deliveryNo);
            if (h != null && !StringUtils.isEmpty(h.getSrcBillNo())) {
                ////// ---> && DELIVERY_PAYSTATUS_NOT_PAY.equals(h.getPayStatus())) { 現在不能檢查這個，因為付殘金和退殘金的時候，在已結帳時仍需進行交易
                // 退货机可退貨

                // 如果查到id一样的取交易序号最大的一笔 (這樣在取部份退货原交易的时候，才不會抓錯到0-3-4那筆交易)

                Sql sql = new Sql(connection)
                def row = sql.firstRow("SELECT transactionNumber, posNumber FROM posul_tranhead WHERE annotatedid LIKE '"
                    + h.getSrcBillNo() + "%' ORDER BY transactionNumber DESC LIMIT 1")
                if (row != 0) {
                    def tranNo = row[0];
                    def posNo = row[1];
                    return tranNo * 100 + posNo;
                }
            }
        } else {
            Sql sql = new Sql(connection)
            def row = sql.firstRow("SELECT transactionNumber, posNumber FROM posul_tranhead WHERE annotatedid LIKE '"
                + deliveryNo + "%' ORDER BY transactionNumber DESC LIMIT 1")
            if (row != 0) {
                def tranNo = row[0];
                def posNo = row[1];
                return tranNo * 100 + posNo;
            }
        }
        return -1;
    }

    /**
     * @return 0: 保存成功, 1: 保存錯誤
     */
    static int updateDeliveryStatus(Connection connection, String storeID, String deliveryNo, String posNumber,
        String transactionNumber, String updateUserId) {
        String deliveryNumber
        int deliveryVersion = -1
        if (deliveryNo.length() > 8) { // 如果傳票編號包括版本號，取出版本號
            deliveryNumber = deliveryNo.substring(0, 8)
            deliveryVersion = deliveryNo.substring(8).toInteger()
        } else {
            deliveryNumber = deliveryNo
        }
        DeliveryHead deliveryHead = queryDeliveryHead(connection, storeID, deliveryNumber)
        if (deliveryHead != null) {
            Sql sql = new Sql(connection)
            def updateCount = 0
            def statement
            if (deliveryVersion == -1) { // 如果沒有給版本號，就不管版本號，直接update
                statement = """update deliveryhead set posNumber=?,transactionNumber=?,updateUserId=?,updateDateTime=?,payStatus=?
                    where storeID=? and accountDate=? and deliveryNo=?"""
                Server.log statement
                updateCount = sql.executeUpdate(statement,
                    [posNumber, transactionNumber, updateUserId, new Date(), DELIVERY_PAYSTATUS_POS_PAID,
                        deliveryHead.storeID, deliveryHead.accountDate, deliveryHead.deliveryNo])
            } else { // 如果有給版本號，就根據指定版本號更新，並將版本號加1
                statement = """update deliveryhead set posNumber=?,transactionNumber=?,updateUserId=?,updateDateTime=?,payStatus=?,ver=ver+1
                    where storeID=? and accountDate=? and deliveryNo=? and ver=?"""
                Server.log statement
                updateCount = sql.executeUpdate(statement,
                    [posNumber, transactionNumber, updateUserId, new Date(), DELIVERY_PAYSTATUS_POS_PAID,
                        deliveryHead.storeID, deliveryHead.accountDate, deliveryHead.deliveryNo, deliveryVersion])
            }
            return updateCount >= 1 ? 0 : 1;
        } else {
            Server.log "DeliveryHead.updateDeliveryStatus()> Cannot find deliveryhead, storeID=${storeID}, deliveryNo=${deliveryNo}"
            return 1;
        }
    }

    /*
    static void main(s) {
        Server.setFakeExist()
        def conn = CreamToolkit.getPooledConnection()
        def t = System.currentTimeMillis();
        println queryDeliveryStatus(conn, 'S4001', '00100010', 4)
        println "time: ${System.currentTimeMillis() - t}"

        t = System.currentTimeMillis();
        println queryPosDeliveryDtlByHeadPrimaryKey(conn, 'S4001', '00100010', 4)
        println "time: ${System.currentTimeMillis() - t}"

        t = System.currentTimeMillis();
        println queryOrigTranNoFromDeliveryID(conn, 'S4001', '00100725')
        println "time: ${System.currentTimeMillis() - t}"
        conn.close()
    }
    */
}