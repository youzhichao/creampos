package hyi.cream.groovydac;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.io.Serializable;

/**
 * Annotation for specifying GroovyEntity's table name.
 *
 * @author Bruce You
 * @since 2008/9/4 下午 08:51:55
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ParamAnnotation {

    /** Property category. */
    ParamCategory category();

    /** Property name. */
    String name() default "";

    /** Property description. */
    String description() default "";

    /** Property detailed description. */
    String comment() default "";

    /** Property default value. */
    String defaultValue() default "";

    /** Control type. */
    ControlType controlType();

    /** Possible values. */
    String possibleValues() default "";

    /** Control width. (No use now) */
    int width() default 30;

    /** Displaying sequence in POS Setting dialog. */
    int seq() default 1;
}