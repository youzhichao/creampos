package hyi.cream.groovydac

import hyi.cream.util.HYIDouble
import java.sql.Time

/**
 * Entity class for mixandmatch.
 *
 * @author Bruce You
 * @since 2009/2/20 14:18:35
 */
@Table (nameAtPOS = 'mixandmatch', nameAtServer = 'posdl_mm')
public class MixAndMatch extends GroovyEntity {

    @PrimaryKey String id // character(3) NOT NULL DEFAULT ''::bpchar,
    String sname // character varying(20) NOT NULL DEFAULT ''::character varying,
    String pname // character varying(10) NOT NULL DEFAULT ''::character varying,
    Date bdate // date NOT NULL DEFAULT '1970-01-01'::date,
    Date edate // date NOT NULL DEFAULT '1970-01-01'::date,
    Time btime // time without time zone NOT NULL DEFAULT '00:00:00'::time without time zone,
    Time etime // time without time zone NOT NULL DEFAULT '00:00:00'::time without time zone,
    String cycle // character varying(7),
    String type // character(1),
    String prctype // character(1),
    HYIDouble amount // numeric(10,2) NOT NULL DEFAULT 0.00,
    Integer limits // integer,
    HYIDouble tamount // numeric(10,2) NOT NULL DEFAULT 0.00,
    Integer group1qty // integer,
    Integer group2qty // integer,
    Integer group3qty // integer,
    Integer group4qty // integer,
    Integer group5qty // integer,
    Integer group6qty // integer,
    Integer pack1qty // integer,
    HYIDouble pack1amt // numeric(10,2),
    Integer pack2qty // integer,
    HYIDouble pack2amt // numeric(10,2),
    Integer pack3qty // integer,
    HYIDouble pack3amt // numeric(10,2),
    Integer pack4qty // integer,
    HYIDouble pack4amt // numeric(10,2),
    Integer pack5qty // integer,
    HYIDouble pack5amt // numeric(10,2),
    Integer pack6qty // integer,
    HYIDouble pack6amt // numeric(10,2),
    Integer pack7qty // integer,
    HYIDouble pack7amt // numeric(10,2),
    Integer pack8qty // integer,
    HYIDouble pack8amt // numeric(10,2),
    String linkmmid // character(3),
}