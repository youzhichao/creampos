package hyi.cream.groovydac;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for specifying GroovyEntity's table name.
 *
 * @author Bruce You
 * @since 2008/9/4 下午 08:51:55
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Table {
    String nameAtPOS() default "";
    String nameAtServer() default "";
}
