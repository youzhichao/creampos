package hyi.cream.groovydac

import hyi.cream.util.HYIDouble

/**
 * Entity class for trandetail.
 *
 * @author Bruce You
 * @since Oct 11, 2008 9:56:30 PM
 */
@Table(nameAtPOS = 'trandetail', nameAtServer = 'posul_trandtl')
class LineItem extends GroovyEntity {

    @PrimaryKey int tmtranseq  // integer NOT NULL DEFAULT 0,
    @PrimaryKey int tmcode     // smallint NOT NULL DEFAULT (0)::smallint,
    @PrimaryKey int itemseq    // smallint NOT NULL DEFAULT (0)::smallint,
    String codetx           // character(1) NOT NULL DEFAULT 'S'::bpchar,
    String itemvoid         // character(1) NOT NULL DEFAULT '0'::bpchar,
    String catno            // character varying(4),
    String midcatno         // character varying(4) NOT NULL DEFAULT ''::character varying,
    String microcatno       // character varying(4) NOT NULL DEFAULT ''::character varying,
    String thincatno        // character varying(4) NOT NULL DEFAULT ''::character varying,
    String pluno            // character varying(20),
    HYIDouble unitprice     // numeric(12,2),
    HYIDouble qty           // numeric(12,2),
    HYIDouble weight        // numeric(12,2),
    String discno           // character varying(48),
    HYIDouble amt           // numeric(12,2),
    String taxtype          // character(1),
    HYIDouble taxamt        // numeric(12,2),
    HYIDouble aftdscamt     // numeric(12,2) NOT NULL DEFAULT 0.00,
    String disctype         // character(1),
    HYIDouble origprice     // numeric(12,2) DEFAULT 0.00,
    String itemno           // character varying(20),
    String content          // character varying(255),
}