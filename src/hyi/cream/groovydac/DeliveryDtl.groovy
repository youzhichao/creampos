package hyi.cream.groovydac

import hyi.cream.util.HYIDouble

/**
 * DeliveryDtl: detail of DeliveryHead.
 *
 * @author Bruce You
 * @since 2008/9/11 下午 06:15:26
 */
@Table(nameAtPOS = 'deliverydtl', nameAtServer = 'deliverydtl')
class DeliveryDtl extends GroovyEntity {
    String storeID                  // | varchar(12)         | NO   | PRI | NULL       |       |
    Date accountDate                // | date                | NO   | PRI | 1970-01-01 |       |
    String deliveryNo               // | varchar(13)         | NO   | PRI | NULL       |       |
    int itemSeq                     // | tinyint(3) unsigned | NO   | PRI | 0          |       |
    String detailCode               // | char(1)             | YES  |     | NULL       |       |
    String pluNumber                // | varchar(13)         | YES  |     | NULL       |       |
    HYIDouble unitPrice             // | decimal(13,3)       | YES  |     | NULL       |       |
    HYIDouble unitCost              // | decimal(13,3)       | YES  |     | NULL       |       |
    HYIDouble quantity              // | decimal(13,3)       | YES  |     | NULL       |       |
    String discountNumber           // | varchar(13)         | YES  |     | NULL       |       |
    HYIDouble amount                // | decimal(13,3)       | YES  |     | NULL       |       |
    HYIDouble afterDiscountAmount   // | decimal(13,3)       | YES  |     | NULL       |       |
    String discountType             // | char(1)             | YES  |     | NULL       |       |
    HYIDouble originalPrice         // | decimal(13,3)       | YES  |     | NULL       |       |
    String itemNumber               // | varchar(13)         | YES  |     | NULL       |       |
    String displayName              // | varchar(48)         | YES  |     | NULL       |       |
    String categoryNumber           // | varchar(4)          | YES  |     | NULL       |       |
    String midCategoryNumber        // | varchar(4)          | YES  |     | NULL       |       |
    String microCategoryNumber      // | varchar(4)          | YES  |     | NULL       |       |
    String smallUnitID              // | varchar(2)          | YES  |     | NULL       |       |
    String storeArea                // | varchar(8)          | YES  |     | NULL       |       |
    String taxType                  // | char(1)             | YES  |     | NULL       |       |
    HYIDouble taxAmount             // | decimal(13,3)       | YES  |     | NULL       |       |
    String itemMemo                 // | varchar(255)        | YES  |     | NULL       |       |
    String updateUserId             // | varchar(8)          | YES  |     | NULL       |       |
    Date updateDateTime             // | datetime            | YES  |     | NULL       |       | 
}