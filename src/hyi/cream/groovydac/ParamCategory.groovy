package hyi.cream.groovydac

/**
 * Parameter category for ParamAnnotation.category.
 *
 * @author Bruce You
 * @since Mar 4, 2009 11:53:31 AM
 */
public enum ParamCategory {
    S05System1,
    S10System2,
    S15System3,
    S17Member,
    S20Printing,
    S25PriceOverriding,
    S30Peripherals,
    S35Inline,
    S40Cashier,
    S45UserInterface,
    S47UserInterface2,
    S50Numbers,
    S55Constrains,
    S60Nitori,     // 宜得利家居
    S65CampusMart  // 教育超市
}