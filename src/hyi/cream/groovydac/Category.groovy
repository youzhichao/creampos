package hyi.cream.groovydac

import hyi.cream.util.HYIDouble

/**
 * Category entity.
 *
 * @author Bruce You
 * @since 2009/2/10 18:48:30
 */
@Table(nameAtPOS = 'cat', nameAtServer = 'posdl_cat')
public class Category extends GroovyEntity {

    @PrimaryKey String catno      // character varying(4) NOT NULL DEFAULT ''::character varying
    @PrimaryKey String midcatno   // character varying(4) NOT NULL DEFAULT ''::character varying
    @PrimaryKey String microcatno // character varying(4) NOT NULL DEFAULT ''::character varying
    String thincatno = '' // character varying(4) NOT NULL DEFAULT ''::character varying
    String catname        // character varying(20) NOT NULL DEFAULT ''::character varying
    String catpname = ''  // character varying(20) NOT NULL DEFAULT ''::character varying
    String deptax         // character(1) NOT NULL DEFAULT ''::bpchar
    HYIDouble overrideamountlimit = new HYIDouble(1.00) // numeric(8,2) DEFAULT 1.00

    /*static void main(s) {
        new Category([
            catno: 'x',
            midcatno: '0',
            microcatno: '0',
            catname: '400small Class',
            deptax: '1']).present()
    }*/
}