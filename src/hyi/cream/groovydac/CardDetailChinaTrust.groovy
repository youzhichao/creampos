package hyi.cream.groovydac

import hyi.cream.util.HYIDouble
import hyi.cream.groovydac.GroovyEntity

/**
 * CardDetailChinaTrust entity.
 *
 * @author Bruce You
 * @since 2008/9/1 下午 02:16:49
 */
@Table(nameAtPOS = 'carddetail_chinatrust', nameAtServer = 'posul_carddetail_chinatrust')
class CardDetailChinaTrust extends GroovyEntity {
    private static final long serialVersionUID = 1L;

    int id                       // id serial NOT NULL, -- Unique ID
    int carddetailId             // carddetailId integer, -- FK to carddetail.id
    int posNumber                // posNumber smallint, -- POS機號
    String storeId               // storeId varchar(6) NOT NULL, -- 門市代號
    Date accountDate             // accountDate date NOT NULL, -- 營業日期
    int zSequenceNumber          // zSequenceNumber integer NOT NULL, -- Z帳序號
    String transType             // transType char(2), -- 交易類別（'01':SALE銷售、'02':REFUND退貨、'03':OFFLINE離線、'04':PRE_AUTH預取授權、'30':VOID取消、'40':TIP小費、'41':ADJUST調整、'50':SETTLEMENT調整、'98':ECHO測試）
    String respCode              // respCode char(4), -- 回覆代碼
    String hostId                // hostId char(2), -- 授權銀行編碼
    String invoiceNo             // invoiceNo char(6), -- 調閱編號
    String refNo                 // refNo char(12), -- 序號
    HYIDouble bonusPaid          // bonusPaid numeric(12,2), -- 紅利抵用的實付金額
    HYIDouble bonusDiscount      // bonusDiscount numeric(12,2), -- 紅利抵用的折抵金額
    int installmentPeriod        // installmentPeriod smallint, -- 期數
    HYIDouble installmentFee     // installmentFee numeric(12,2), -- 手續費
    String installmentType       // installmentType char(1), -- 收費方式（'1':一般分期、'2':收費分期）
    String installmentPutOffType // installmentPutOffType char(1), -- 延後付款方式（'1':無延後付款、'2':延後付款、'3':彈性付款）
    HYIDouble installmentAmount1 // installmentAmount1 numeric(12,2), -- 首期金額（若為彈性付款，此欄位為後期的首期金額）
    HYIDouble installmentAmount2 // installmentAmount2 numeric(12,2), -- 每期金額（若為彈性付款，此欄位為後期的每期金額）
    HYIDouble installmentAmount3 // installmentAmount3 numeric(12,2), -- 彈性付款前期的每期金額
    String rawData               // rawData char(144), -- CAT機返回的原始數據（目前中國信託的應該是144 bytes）

}