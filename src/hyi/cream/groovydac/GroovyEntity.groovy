package hyi.cream.groovydac

import groovy.sql.Sql
import hyi.cream.autotest.AutoTester
import hyi.cream.inline.Server
import hyi.cream.util.CreamToolkit
import static hyi.cream.util.CreamToolkit.logMessage
import hyi.cream.util.HYIDouble
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import org.apache.commons.lang.StringUtils
import java.lang.reflect.Field
import hyi.cream.util.DbConnection

/**
 * GroovyEntity. Base class for Groovy Cream entity. 
 * <p/>
 * Coding convention: <br/>
 * 1. Table field欄位不要加任何public/private，其他非table field請加上public or private.<br/>
 * 2. 不要在entity class裡面寫任何 "get" 開頭的method, 用別的prefix取代.<br/>
 * 3. Please always name your unique-and-auto-generated key field as "id."
 *
 * @author Bruce You
 * @since 2008/9/1 14:23:52
 */
abstract class GroovyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private static Map<Class, Object> idFieldsCache = [:]

    // only cache those whose got FieldName annotation
    private static Map<Class, Map> dbFieldToPropertyMapForPos = [:]
    private static Map<Class, Map> dbFieldToPropertyMapForServer = [:]

    /**
     * A map contains entity Class to
     * [getterNames: x, notExistFieldIdx: y, dbFieldNames: z]
     */
    private static Map fieldCache = [:]

    private dirtyFields = new HashSet()
    private boolean allFieldsDirty


    /**
     * Clear dirty fields.
     */
    void clearDirtyFields() {
        dirtyFields.clear()
        allFieldsDirty = false
    }

    void addDirtyProperty(String property) {
        Field field = this.class.getDeclaredField(property)
        FieldName fieldNameAnnot = field.getAnnotation(FieldName.class)
        if (fieldNameAnnot == null)
            dirtyFields << property.toLowerCase()
        else {
            def fieldName = Server.isAtServerSide() ? fieldNameAnnot.nameAtServer() : fieldNameAnnot.nameAtPOS()
            if (!StringUtils.isEmpty(fieldName))
                dirtyFields << fieldName.toLowerCase()
        }
    }

    /** Make all fields dirty. */
    void setAllFieldsDirty(boolean dirty) {
        allFieldsDirty = dirty
        if (!dirtyFields) dirtyFields = new HashSet()
    }
    
    /**
     * A convenient method to test if at server side.
     */
    static boolean isAtServerSide() { return Server.isAtServerSide() }

    /** Get database table name of the given entity class. */
    static String tableName(Class clazz) {
        def table = clazz.getAnnotation(Table.class)
        return (isAtServerSide()) ? table.nameAtServer() : table.nameAtPOS()
    }

    /** Get database table name. */
    String tableName() { tableName(this.class) }

    /** Convenient method for getting a new database connection. */
    static DbConnection dbConnection() { CreamToolkit.getTransactionalConnection() }

    /**
     * Hook to record dirty fields and convert BigDecimal to HYIDouble.
     */
    void setProperty(String property, newValue) {
        dirtyFields << property.toLowerCase()
        if (newValue != null) {
            if (newValue instanceof BigDecimal)
                newValue = new HYIDouble(newValue.toString())
            else if (newValue instanceof Short)
                newValue = newValue.toInteger()     
        }
        //println "set ${property} to ${newValue}"
        getMetaClass().setProperty(this, property, newValue);
    }

    /**
     * Get GroovyDac's identifier objects.
     */
    private getIdFields() {     
        def dacClass = this.class
        def idField = idFieldsCache[dacClass]
        if (idField == null) {
            def pkFieldNames = getPrimaryKeyFieldNames()
            if (pkFieldNames.isEmpty())
                idFieldsCache[dacClass] = idField = 'id'
            else
                idFieldsCache[dacClass] = idField = pkFieldNames
        }
        return idField
    }

    /**
     * Load an entity by id.
     */
    static GroovyEntity loadById(Connection connection, Class entityClass, int id) {
        return selectSingleRow(connection, entityClass,
            "SELECT * FROM ${tableName(entityClass)} WHERE id=${id}")
    }

    /**
     * Load an entity.
     */
    boolean load() {
        def dbConn
        try {
            dbConn = dbConnection()
            return load(dbConn)
        } finally {
            dbConn?.closeIfNeeds()
        }
    }

    /**
     * Load an entity.
     */
    boolean load(Connection connection) {
        return ('id' == getIdFields()) ? loadById(connection) :
            loadByPrimaryKey(connection)
    }

    /**
     * Load an entity by id.
     */
    boolean loadById(Connection connection) {
        return selectSingleRow(connection, this,
            "SELECT * FROM ${tableName()} WHERE id=${this.id}")
    }

    /**
     * Query entity by primary key field value of 'this'.
     *
     * @return true if found, false otherwise.
     */
    boolean loadByPrimaryKey(Connection connection) {
        def pkFieldNamesAndValues = getPrimaryKeyFieldNamesAndValues()
        def pkFieldNames = pkFieldNamesAndValues.fieldNames
        def pkFieldValues = pkFieldNamesAndValues.fieldValues

        if (pkFieldNames.isEmpty())
            return false

        // select by primary key fields
        def params = []
        def i = 0
        def selectSql = "select * from ${tableName()} where "
        for (pkFieldName in pkFieldNames) {
            if (i > 0)
                selectSql += ' and '
            selectSql += pkFieldName + '=?'
            params << pkFieldValues[i]
            i++
        }

        return selectSingleRow(connection, this, selectSql, params)
    }

    /**
     * Check if this entity exists.
     */
    static exists() {
        def dbConn
        try {
            dbConn = dbConnection()
            return exists(dbConn)
        } finally {
            dbConn?.closeIfNeeds()
        }
    }

    /**
     * Check if this entity exists.
     */
    boolean exists(Connection connection) {
        def idField = getIdFields()

        if ('id' == idField) {
            return rowExists(connection,
                    "SELECT id FROM ${tableName()} WHERE id=${this.id}")

        } else {
            def pkFieldNamesAndValues = getPrimaryKeyFieldNamesAndValues(idField)
            def pkFieldNames = pkFieldNamesAndValues.fieldNames
            def pkFieldValues = pkFieldNamesAndValues.fieldValues

            if (pkFieldNames.isEmpty())
                return false

            // select by primary key fields
            def params = []
            def i = 0
            def selectSql = "SELECT ${pkFieldNames[0]} FROM ${tableName()} WHERE "
            for (pkFieldName in pkFieldNames) {
                if (i > 0)
                    selectSql += ' and '
                selectSql += pkFieldName + '=?'
                params << pkFieldValues[i]
                i++
            }

            return rowExists(connection, selectSql, params)
        }
    }

    /**
     * Create an insert statement and execute insert for this entity.
     */
    def insert() {
        def dbConn
        try {
            dbConn = dbConnection()
            return insert(dbConn)
        } finally {
            dbConn?.commitAndcloseIfNeeds()
        }
    }

    /**
     * Create an insert statement and execute insert for this entity.
     */
    def insert(Connection connection) { insert(connection, false) }

    /**
     * Create an insert statement and execute insert for this entity. If getAutoGeneratedKey
     * is true, it'll return the id of the newly inserted row.
     */
    def insert(Connection connection, boolean getAutoGeneratedKey) {
        def fieldNamesAndValues = getDbFieldNamesAndValues()
        def fieldNames = fieldNamesAndValues.fieldNames
        def fieldValues = fieldNamesAndValues.fieldValues

        // insert into with field names
        def insertSql = "insert into ${tableName()} ${fieldNames} values ("
        insertSql = insertSql.replaceAll(/\"/, '')

        // prepared statment parameters with question mark
        insertSql += ('?,' * fieldNames.size())[0..-2] + ");"

        // replace bracket with parenthesis
        insertSql = insertSql.replaceAll(/\[/, '(').replaceAll(/\]/, ')')

        // execute insert (and get auto-generated key value) 
        if (CreamToolkit.isDatabasePostgreSQL()) {
            // PostgreSQL JDBC 沒有支援 Statement.RETURN_GENERATED_KEYS, use currval() to get it by ourself
            if (getAutoGeneratedKey)
                insertSql += "; SELECT currval('${tableName()}_id_seq')"
            def statement = connection.prepareStatement(insertSql);
            //println insertSql
            setParameters(fieldValues, statement);
            statement.execute();
            if (getAutoGeneratedKey) { // get auto-generated key value
                def inserted = statement.getUpdateCount();
                if (inserted == 1 && statement.getMoreResults()) {
                    ResultSet rs = statement.getResultSet();
                    if (rs.next())
                        return rs.getInt(1);
                }
            }
        } else {
            Sql sql = new Sql(connection)
            def ids = sql.executeInsert(insertSql, fieldValues)
            if (getAutoGeneratedKey && ids != null && !ids.isEmpty())
                return ids[0][0].toInteger()
        }
    }

    private Map loadFieldCache() {
        List getterNames = this.class.methods.name.grep(~/get.*/).grep {
            // excludes "id", "getMetaClass", "getProperty", "getClass"
            it != 'getId' && !it.contains('Meta') &&
            it != 'getProperty' && it != 'getClass' &&
            it != 'getAllObjectsForPOS'
        }
        //def fieldNames = getterNames*.substring(3)
        def idx = 0
        def notExistFieldIdx = []
        def notExistMark = '#@!~'
        def dbFieldNames = getterNames.collect {
            String fieldName = it[3].toLowerCase() + (it.length() > 4 ? it[4..-1] : '')
            def field = this.class.getDeclaredField(fieldName)
            def fieldNameAnnot = field.getAnnotation(FieldName.class)
            if (fieldNameAnnot != null) {
                fieldName = Server.isAtServerSide() ? fieldNameAnnot.nameAtServer() :
                    fieldNameAnnot.nameAtPOS()
                if (fieldName == '') { // means this field doesn't exist in DB
                    notExistFieldIdx << idx
                    fieldName = notExistMark
                }
            }
            idx++
            return fieldName
        }
        dbFieldNames = dbFieldNames.grep { it != notExistMark }
        def entry = [getterNames: getterNames,
                     notExistFieldIdx: notExistFieldIdx,
                     dbFieldNames: dbFieldNames]
        fieldCache[this.class] = entry
        return entry
    }

    private Map getDbFieldNamesAndValues() {
        def entry = fieldCache[this.class]
        if (entry == null)
            entry = loadFieldCache();
        def getterNames = entry.getterNames
        def notExistFieldIdx = entry.notExistFieldIdx
        def dbFieldNames = entry.dbFieldNames
        def notExistMark = '#@!~'

        // field values
        def idx = -1
        def fieldValues = getterNames.collect {
            idx++
            if (notExistFieldIdx.contains(idx))
                return notExistMark
            else {
                def value = this.metaClass.invokeMethod(this, it)
                return (value instanceof java.util.Date) ? new java.sql.Timestamp(value.time) :
                    (value instanceof HYIDouble) ? value.toBigDecimal() : value
            }
        }
        fieldValues = fieldValues.grep { it != notExistMark }
        return [fieldNames: dbFieldNames, fieldValues: fieldValues]
    }

    private List getPrimaryKeyFieldNames() {
        def fieldNames = []
        for (field in this.class.getDeclaredFields()) {
            def name = field.name
            if (name.startsWith('_') || name.contains('$'))
                continue
            if (field.getAnnotation(PrimaryKey.class) != null) {
                fieldNames << name
            }
        }
        return fieldNames
    }

    private Map getPrimaryKeyFieldNamesAndValues(List primaryKeyFieldNames) {
        // field values
        def fieldValues = primaryKeyFieldNames.collect {
            //def getterName = "get" + it[0].toUpperCase() + it[1..-1]
            //def value = this.metaClass.invokeMethod(this, getterName)
            def value = this."${it}"
            (value instanceof java.util.Date) ? new java.sql.Timestamp(value.time) :
                (value instanceof HYIDouble) ? value.toBigDecimal() : value
        }
        return [fieldNames: primaryKeyFieldNames, fieldValues: fieldValues]
    }

    private Map getPrimaryKeyFieldNamesAndValues() {
        def idFields = getIdFields()
        return getPrimaryKeyFieldNamesAndValues(idFields)
    }

    private void setParameters(List params, PreparedStatement statement) {
        def i = 1;
        for (value in params)
            statement.setObject(i++, value);
    }

    /**
     * Update 'this'.
     *
     * @return Return the number of rows affected.
     */
    def update() {
        def dbConn
        try {
            dbConn = dbConnection()
            return update(dbConn)
        } finally {
            dbConn?.commitAndcloseIfNeeds()
        }
    }

    /**
     * Update 'this'.
     *
     * @return Return the number of rows affected.
     */
    int update(Connection connection) {
        def idField = getIdFields()

        if ('id' == idField)
            return updateById(connection)
        else
            return updateByPrimaryKey(connection)
    }

    /**
     * Update 'this' by id.
     */
    int updateById(Connection connection) {
        if (!allFieldsDirty && dirtyFields?.isEmpty())
            return

        def dbFieldNamesAndValues = getDbFieldNamesAndValues()
        def dbFieldNames = dbFieldNamesAndValues.fieldNames
        def dbFieldValues = dbFieldNamesAndValues.fieldValues
        def updateValues = []

        // update with field names
        def updateSql = "update ${tableName()} set "
        int i = 0
        int idx = 0
        for (fieldName in dbFieldNames) {
            if (allFieldsDirty || dirtyFields.contains(fieldName.toLowerCase())) {
                if (i > 0)
                    updateSql += ','
                updateSql += fieldName + '=?'
                updateValues << dbFieldValues[idx]
                i++
            }
            idx++
        }
        if (i == 0)
            return 0

        updateSql += " where id=${id}"
        //println updateSql 

        Sql sql = new Sql(connection)
        def rows = sql.executeUpdate(updateSql, updateValues)

        clearDirtyFields()
        return rows
    }

    /**
     * Update 'this' by primary key.
     */
    int updateByPrimaryKey(Connection connection) {
        if (!allFieldsDirty && dirtyFields?.isEmpty())
            return

        def pkFieldNamesAndValues = getPrimaryKeyFieldNamesAndValues()
        def pkFieldNames = pkFieldNamesAndValues.fieldNames
        def pkFieldValues = pkFieldNamesAndValues.fieldValues

        def fieldNamesAndValues = getDbFieldNamesAndValues()
        def fieldNames = fieldNamesAndValues.fieldNames
        def fieldValues = fieldNamesAndValues.fieldValues
        def updateValues = []

        // update with field names
        def updateSql = "update ${tableName()} set "
        int i = 0
        int idx = 0
        for (fieldName in fieldNames) {
            if ((allFieldsDirty || dirtyFields.contains(fieldName.toLowerCase()))
                    && !pkFieldNames.contains(fieldName)) {
                if (i > 0)
                    updateSql += ','
                updateSql += fieldName + '=?'
                updateValues << fieldValues[idx]
                i++
            }
            idx++
        }
        updateSql += " where "
        i = 0
        for (pkFieldName in pkFieldNames) {
            if (i > 0)
                updateSql += ' and '
            updateSql += pkFieldName + '=?'
            updateValues << pkFieldValues[i]
            i++
        }

        Sql sql = new Sql(connection)
        def rows = sql.executeUpdate(updateSql, updateValues)

        clearDirtyFields()
        return rows
    }

    /** Check if any entity exists. */
    static boolean rowExists(String sqlStatement, List params = []) {
        def dbConn
        try {
            dbConn = dbConnection()
            return rowExists(dbConn, sqlStatement, params)
        } finally {
            dbConn?.closeIfNeeds()
        }
    }

    /** Check if any entity exists. */
    static boolean rowExists(Connection connection, String sqlStatement, List params = []) {
        return new Sql(connection).firstRow(sqlStatement, params) != null
    }

    /**
     * Construct objects of type 'entityClass' by executing a SELECT statement.
     */
    static GroovyEntity selectSingleRow(Class entityClass, String selectStatement, List params = []) {
        def dbConn
        try {
            dbConn = dbConnection()
            return selectSingleRow(dbConn, entityClass, selectStatement, params)
        } finally {
            dbConn?.closeIfNeeds()
        }
    }

    /**
     * Construct a object of type 'entityClass' by executing a SELECT statement.
     */
    static GroovyEntity selectSingleRow(Connection connection, Class entityClass,
            String selectStatement, List params = []) {
        Sql sql = new Sql(connection)
        GroovyEntity entity = null
        sql.query(selectStatement, params) { rs ->
            if (rs.next()) {
                entity = entityClass.newInstance()
                def meta = rs.metaData
                for (i in 1..meta.columnCount) {
                    def matchedFieldName = searchPropertyFromDbField(entityClass, meta.getColumnName(i))
                    if (matchedFieldName != null)
                        entity."${matchedFieldName}" = rs.getObject(i)
                }
                entity.clearDirtyFields()
            }
        }
        return entity
    }

    /**
     * Query one row by executing a SELECT statement and fill into the fields of 'entity' .
     */
    static boolean selectSingleRow(Connection connection, Object entity,
            String selectStatement, List params = []) {
        Sql sql = new Sql(connection)
        def found = false
        sql.query(selectStatement, params) { rs ->
            if (rs.next()) {
                found = true
                def meta = rs.metaData
                for (i in 1..meta.columnCount) {
                    def matchedFieldName = searchPropertyFromDbField(entity.class, meta.getColumnName(i))
                    if (matchedFieldName != null)
                        entity."${matchedFieldName}" = rs.getObject(i)
                }
            }
            entity.clearDirtyFields()
        }
        return found
    }

    /**
     * Construct some List objects (which may contain List objects) by executing a SELECT statement.
     */
    static List selectMultipleRows(String selectStatement, List params = []) {
        return selectMultipleRows(null, selectStatement, params)
    }

    /**
     * Construct objects of type 'entityClass' by executing a SELECT statement.
     * If entityClass is null, then it'll return a List object (which may contain List objects).
     */
    static List selectMultipleRows(Class entityClass, String selectStatement, List params = []) {
        def dbConn
        try {
            dbConn = dbConnection()
            return selectMultipleRows(dbConn, entityClass, selectStatement, params)
        } finally {
            dbConn?.closeIfNeeds()
        }
    }

    /**
     * Construct objects of type 'entityClass' by executing a SELECT statement.
     * If entityClass is null, then it'll return a List object (which may contain List objects).
     */
    static List selectMultipleRows(Connection connection, Class entityClass,
            String selectStatement, List params = []) {
        Sql sql = new Sql(connection)
        def entities = []
        sql.query(selectStatement, params) { ResultSet rs ->
            while (rs.next()) {
                def metaData = rs.metaData
                def entity
                if (entityClass == null) {
                    if (metaData.columnCount == 1) {
                        entity = rs.getObject(1)
                        entities << entity
                        continue
                    } else
                        entity = []
                } else {
                    entity = entityClass.newInstance()
                }
                for (i in 1..metaData.columnCount) {
                    if (entityClass == null) {
                        entity << rs.getObject(i)
                    } else {
                        def matchedFieldName = searchPropertyFromDbField(entityClass, metaData.getColumnName(i))
                        if (matchedFieldName != null)
                            entity."${matchedFieldName}" = rs.getObject(i)
                    }
                }
                if (entityClass != null)
                    entity.clearDirtyFields()

                entities << entity
            }
        }
        //if (entities.isEmpty())
        //    CreamToolkit.logMessage "GroovyEntity.selectMultipleRows()> Find nothing: ${selectStatement}"

        return entities
    }

    private static String searchPropertyFromDbField(Class entityClass, dbFieldName) {
        // case-insensitive search
        List fields = entityClass.declaredFields.name.grep(~/(?i)${dbFieldName}/)
        if (fields != null && !fields.isEmpty())
            return fields[0]

        return getDbFieldToPropertyMap(entityClass)[dbFieldName]
    }

    private static Map getDbFieldToPropertyMap(Class entityClass) {
        Map dbFieldToPropertyMap = Server.isAtServerSide() ? dbFieldToPropertyMapForServer[entityClass] :
            dbFieldToPropertyMapForPos[entityClass]
        if (dbFieldToPropertyMap != null)
            return dbFieldToPropertyMap

        dbFieldToPropertyMap = [:]
        for (Field field : entityClass.declaredFields) {
            FieldName fieldNameAnnot = field.getAnnotation(FieldName.class)
            if (fieldNameAnnot != null) {
                def dbFieldName = Server.isAtServerSide() ? fieldNameAnnot.nameAtServer() : fieldNameAnnot.nameAtPOS()
                dbFieldToPropertyMap[dbFieldName] = field.getName()
            }
        }

        if (Server.isAtServerSide())
            dbFieldToPropertyMapForServer[entityClass] = dbFieldToPropertyMap
        else
            dbFieldToPropertyMapForPos[entityClass] = dbFieldToPropertyMap
        return dbFieldToPropertyMap
    }

    /**
     * Delete all records.
     */
    int deleteAll(Connection connection) {
        try {
            Sql sql = new Sql(connection)
            sql.execute("DELETE FROM ${tableName()}".toString())
            return sql.getUpdateCount()
        } catch (Exception e) {
            logMessage(e)
        }
    }

    /**
     * Check if the entity exists finding by its primary key;
     * if it exists, update it by its current values;
     * if not exist, insert a new record.
     */
    void present() {
        def dbConn
        try {
            dbConn = dbConnection()

            if (exists(dbConn)) {
                //setAllFieldsDirty(true)
                update dbConn
            } else {
                insert dbConn
            }
        } finally {
            dbConn?.commitAndcloseIfNeeds()
        }
    }

    boolean match(List checkingFields) {
        def dbConn
        try {
            dbConn = dbConnection()

            def pkFieldNamesAndValues = getPrimaryKeyFieldNamesAndValues()
            def pkFieldNames = pkFieldNamesAndValues.fieldNames
            def pkFieldValues = pkFieldNamesAndValues.fieldValues
            if (pkFieldNames.isEmpty()) {
                AutoTester.addMessage "Cannot find primary key of ${this.class}"
                return false
            }

            def entityInDb = this.class.newInstance()
            for (pkFieldName in pkFieldNames)
                entityInDb."${pkFieldName}" = this."${pkFieldName}"
            if (!entityInDb.loadByPrimaryKey(dbConn)) {
                AutoTester.addMessage "Cannot find record in DB, table=${tableName()}, " +
                    "PK fields=${pkFieldNames}, values=${pkFieldValues}."
                return false
            }

            def matched = true
            def notMatchedMessagePrinted = false
            for (String field in checkingFields) {
                def expectValue = this."${field}"
                def actualValue = entityInDb."${field}"
                if (expectValue != null && expectValue instanceof String)
                    expectValue = expectValue.trim()
                if (actualValue != null && actualValue instanceof String)
                    actualValue = actualValue.trim()
                def expectValueIsEmpty = expectValue == null || (
                    expectValue instanceof String && StringUtils.isEmpty(expectValue))
                def actualValueIsEmpty = actualValue == null || (
                    actualValue instanceof String && StringUtils.isEmpty(actualValue))
                if (expectValueIsEmpty && actualValueIsEmpty)
                    continue
                if (expectValue != actualValue) {
                    if (!notMatchedMessagePrinted) {
                        AutoTester.addMessage "[Not matched]: ${tableName()}, PK fields=${pkFieldNames}, values=${pkFieldValues}."
                        notMatchedMessagePrinted = true
                    }
                    AutoTester.addMessage "   ${tableName()}.${field}: expect ${expectValue}, but actually is ${actualValue}"
                    matched = false
                }
            }
            if (matched)
                AutoTester.addMessage "[Matched]: ${tableName()}, PK fields=${pkFieldNames}, values=${pkFieldValues}."
            return matched

        } finally {
            dbConn?.closeIfNeeds()
        }
    }

    boolean matchDiff(GroovyEntity origEntity, List checkingFields) {
        def dbConn
        try {
            dbConn = dbConnection()

            def pkFieldNamesAndValues = getPrimaryKeyFieldNamesAndValues()
            def pkFieldNames = pkFieldNamesAndValues.fieldNames
            def pkFieldValues = pkFieldNamesAndValues.fieldValues
            if (pkFieldNames.isEmpty()) {
                AutoTester.addMessage "Cannot find primary key of ${this.class}"
                return false
            }

            def entityInDb = this.class.newInstance()
            for (pkFieldName in pkFieldNames)
                entityInDb."${pkFieldName}" = this."${pkFieldName}"
            if (!entityInDb.loadByPrimaryKey(dbConn)) {
                AutoTester.addMessage "Cannot find record in DB, table=${tableName()}, " +
                    "PK fields=${pkFieldNames}, values=${pkFieldValues}."
                return false
            }

            def matched = true
            def notMatchedMessagePrinted = false
            for (String field in checkingFields) {
                if (pkFieldNames.contains(field)) // don't check PKs
                    continue

                def type = this.class.getDeclaredField(field).getType()
                def expectValue
                def actualValue
                switch (type) {
                    case int:
                    case short:
                    case long:
                    case Integer:
                    case Short:
                    case Long:
                        if (origEntity."${field}" == null)
                            expectValue = this."${field}"
                        else
                            expectValue = origEntity."${field}" + this."${field}"
                        actualValue = entityInDb."${field}"
                        break
                    case HYIDouble:
                        if (origEntity."${field}" == null)
                            expectValue = this."${field}"
                        else
                            expectValue = (origEntity."${field}").add(this."${field}")
                        actualValue = entityInDb."${field}"
                        break
                    default:
                        expectValue = this."${field}"
                        actualValue = entityInDb."${field}"
                        break
                }

                if (expectValue != null && expectValue instanceof String)
                    expectValue = expectValue.trim()
                if (actualValue != null && actualValue instanceof String)
                    actualValue = actualValue.trim()
                def expectValueIsEmpty = expectValue == null || (
                    expectValue instanceof String && StringUtils.isEmpty(expectValue))
                def actualValueIsEmpty = actualValue == null || (
                    actualValue instanceof String && StringUtils.isEmpty(actualValue))
                if (expectValueIsEmpty && actualValueIsEmpty)
                    continue
                if (expectValue != actualValue) {
                    if (!notMatchedMessagePrinted) {
                        AutoTester.addMessage "[Not matched]: ${tableName()}, PK fields=${pkFieldNames}, values=${pkFieldValues}."
                        notMatchedMessagePrinted = true
                    }
                    AutoTester.addMessage "   ${tableName()}.${field}: expect ${expectValue}, but actually is ${actualValue}"
                    matched = false
                }
            }
            if (matched)
                AutoTester.addMessage "[Matched]: ${tableName()}, PK fields=${pkFieldNames}, values=${pkFieldValues}."

            return matched

        } finally {
            dbConn?.closeIfNeeds()
        }
    }
}