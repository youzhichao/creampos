package hyi.cream.groovydac

/**
 * Entity class for reason.
 *
 * @author Bruce You
 * @since 2009/2/20 14:27:23
 */
@Table (nameAtPOS = 'reason', nameAtServer = 'posdl_reason')
public class Reason extends GroovyEntity {
    @PrimaryKey String reasoncategory // character(2) NOT NULL DEFAULT ''::bpchar,
    @PrimaryKey String reasonnumber // character(8) NOT NULL DEFAULT ''::bpchar,
    String reasonname // character(30) NOT NULL DEFAULT ''::bpchar,
}