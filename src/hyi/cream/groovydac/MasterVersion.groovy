package hyi.cream.groovydac

import hyi.cream.dac.Store
import org.apache.commons.lang.ClassUtils

/**
 * MasterVersion entity.
 *
 * @author Bruce You
 * @since 2009/5/25 15:57:02
 */
@Table(nameAtPOS = 'master_version', nameAtServer = 'master_version')
public class MasterVersion extends GroovyEntity {
    private static final long serialVersionUID = 1L;

    String storeID          // character varying(8) NOT NULL, -- 门市代号

    @PrimaryKey
    String masterName       // character varying(64) NOT NULL, -- 主档名称

    String masterVersion    // character varying(20) DEFAULT NULL, -- 主档版本


    String toString() {
        "MasterVersion(storeID=${storeID}, masterName=${masterName}, masterVersion=${masterVersion})"
    }

    boolean equals(o) {
        if (this.is(o)) return true;
        if (!o || getClass() != o.class) return false;
        MasterVersion that = (MasterVersion) o;
        if (masterName ? !masterName.equals(that.masterName) : that.masterName != null) return false;
        return true;
    }

    int hashCode() {
        return (masterName ? masterName.hashCode() : 0);
    }

    /**
     * Get all data for downloading to POS. This method is used by inline
     * server.
     */
    static Collection getAllObjectsForPOS() {
        def clazz = MasterVersion.class
        selectMultipleRows(clazz, """SELECT * FROM ${tableName(clazz)}
            WHERE storeID='${Store.getStoreID()}' AND masterVersion IS NOT NULL""")
    }

    /**
     * 根據server的MasterVersion, 和POS端的MasterVersion做比較，挑出版本不同者，
     * 並且包含於downloadlist.conf所設定的主檔中者。
     *
     * @return 將這些需要下傳的server-side MasterVersions返回。
     */
    static List<MasterVersion> diffMasterVersion(List<MasterVersion> masterVersionsAtServer) {
        def clazz = MasterVersion.class
        def masterVersionsAtPos = selectMultipleRows(clazz,
            "SELECT * FROM ${tableName(clazz)} WHERE storeID='${Store.getStoreID()}'")

        def diffMv = []
        for (MasterVersion mvServer in masterVersionsAtServer) {
            def idx = masterVersionsAtPos.indexOf(mvServer) // search by masterName
            if (idx == -1)
                diffMv << mvServer
            else {
                def mvPos = masterVersionsAtPos.get(idx)
                if (mvPos.masterVersion != mvServer.masterVersion)
                    diffMv << mvServer
            }
        }

/* Bruce/20110329/ 因为類名並不一定等同於表名 posdl_{類名}, 所以這樣會過濾掉需要下載的tables，先去掉。
        def downListFile = [Param.instance.configDir, "downloadlist.conf"] as File
        if (downListFile.exists()) {
            // convert to table name at server side
            def masterNeedDownload = downListFile.readLines().collect {
                def dacClass = ClassUtils.getShortClassName(it).toLowerCase()  // class name as table name
                'posdl_' + dacClass
            }
            diffMv = diffMv.findAll() {
                masterNeedDownload.contains(it.masterName.toLowerCase())
            }
        }
*/

        return diffMv
    }
}