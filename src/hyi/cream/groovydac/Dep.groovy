package hyi.cream.groovydac

/**
 * Dep entity.
 *
 * @author Bruce You
 * @since 2009/2/10 10:46:02
 */
@Table(nameAtPOS = 'dep', nameAtServer = 'posdl_dep')
public class Dep extends GroovyEntity {

    @PrimaryKey
    String depid    // character varying(4) NOT NULL DEFAULT ''::character varying,

    String depname  // character varying(20) NOT NULL DEFAULT ''::character varying,
    String deptax   // character(1) NOT NULL DEFAULT '1'::bpchar,

    /*static void main(s) {
        new Dep([depid: '4', depname: '4Big Class', deptax: '1']).present()
    }*/
}