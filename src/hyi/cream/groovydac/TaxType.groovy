package hyi.cream.groovydac

import hyi.cream.util.HYIDouble

/**
 * Entity class for taxtype.
 *
 * @author Bruce You
 * @since 2009/2/20 13:23:18
 */
@Table(nameAtPOS = 'taxtype', nameAtServer = 'posdl_taxtype')
public class TaxType extends GroovyEntity {

    @PrimaryKey String taxid // character(1) NOT NULL DEFAULT ''::bpchar,
    String taxnm // character varying(12) NOT NULL DEFAULT ''::character varying,
    String type // character(1) NOT NULL DEFAULT ''::bpchar,
    String round // character(1) NOT NULL DEFAULT ''::bpchar,
    int decimalcnt // integer NOT NULL DEFAULT 0,
    HYIDouble percent // numeric(4,2) NOT NULL DEFAULT 0.00,

}