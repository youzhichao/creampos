package hyi.cream.groovydac

import hyi.cream.util.HYIDouble
import java.sql.Time

/**
 * Entity class for si.
 *
 * @author Bruce You
 * @since 2009/2/20 13:58:35
 */
@Table(nameAtPOS = 'si', nameAtServer = 'posdl_si')
public class Si extends GroovyEntity {

    @PrimaryKey String siid // character(2) NOT NULL DEFAULT ''::bpchar, -- 折扣代號
    String sicnames // character varying(14), -- 折扣名稱
    String sinamep // character varying(10) NOT NULL DEFAULT ''::character varying,-- 列印用折扣名稱
    String sino // character varying(8) NOT NULL DEFAULT ''::character varying, -- SI群組
    String sitype // character(1) NOT NULL DEFAULT ''::bpchar,
        // -- 折扣類型（'0'：折扣（百分比）、'1'：折讓、'2'：加成（百分比）、'3'：全折、'4'：開放折扣數輸入、'5'：信用卡紅利折抵、'6'：開放折讓金額輸入）

    HYIDouble dvalue // numeric(4,2) NOT NULL DEFAULT 0.00,  -- 折扣百分比或折讓金額
    String dbase // character(1) NOT NULL DEFAULT ''::bpchar, -- 折扣基準（'R'：依折扣前單價計算、'N'：依折扣後單價計算）
    String attr // character varying(8) NOT NULL DEFAULT ''::character varying,
        // -- attr[0]: 四捨五入flag('R':四捨五入、'C':進位、'D':捨去)
        // -- attr[1]: 小數位數
        // -- attr[2]: SI相容（‘0’不相容）; i.e., 此折扣是否容不下和別人一起折扣（是否只能單獨折扣）
        // -- attr[3]: N/A
        // -- attr[4]: 認證
        // -- attr[5]: M&M相容（‘0’不相容）
        // -- attr[6]: N/A
        // -- attr[7]: N/A

    String catno // character varying(4), -- 折扣適用大分類（'00'表示所有商品）
    String midcatno // character varying(4), -- 折扣適用中分類
    String microcatno // character varying(4), -- 折扣適用小分類
    Date bdate // date, -- 折扣開始日期
    Date edate // date, -- 折扣結束日期
    Time btime // time without time zone, -- 折扣每日開始時間
    Time etime // time without time zone, -- 折扣每日結束時間
    String cycle // character varying(7), -- 適用星期（星期日至星期六）

}