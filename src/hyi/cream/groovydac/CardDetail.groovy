package hyi.cream.groovydac

import hyi.cream.groovydac.GroovyEntity
import hyi.cream.util.HYIDouble
import java.sql.Connection

/**
 * CardDetail entity.
 *
 * @author Bruce You
 * @since 2008/9/1 下午 02:00:18
 */
@Table(nameAtPOS = 'carddetail', nameAtServer = 'posul_carddetail')
class CardDetail extends GroovyEntity {
    private static final long serialVersionUID = 1L;

    int id                    // id serial NOT NULL, -- Unique ID
    String storeId            // storeId varchar(6) NOT NULL, -- 門市代號
    Date accountDate          // accountDate date NOT NULL, -- 營業日期
    int posNumber             // posNumber smallint, -- POS機號
    int transactionNumber     // transactionNumber integer NOT NULL, --  交易序號
    int zSequenceNumber       // zSequenceNumber integer NOT NULL, -- Z帳序號
    String isVoid             // isVoid char(1), -- 是否被作廢（取消/退貨/調整）（'0':沒有被作廢、'1':被作廢）
    int voidTransactionNumber // voidTransactionNumber integer NOT NULL, -- 若此筆為被作廢交易，記錄作廢此筆交易的相應取消/退貨/調整的交易序號；若此筆為作廢交易（取消/退貨/調整），則記錄被作廢的交易序號
    Date transDateTime        // transDateTime timestamp without time zone NOT NULL, -- 交易日期與時間
    String cardNo             // cardNo char(19) NOT NULL, -- 卡號
    String cardExpDate        // cardExpDate char(4) NOT NULL, -- 有效期
    String approvalCode       // approvalCode char(9) NOT NULL, -- 授權碼
    String terminalId         // terminalID char(8) NOT NULL, -- 端末機編號
    HYIDouble transAmt        // transAmt numeric(12,2) NOT NULL, -- 總交易金額
    String source             // source char(1) NOT NULL, -- 數據來源 ('C': CAT數據、'L': 缺乏明細數據、'S': SC補登數據)
    Date updateDate           // updateDate timestamp without time zone NOT NULL -- 最後修改日期
    String updateUserId       // updateUserId varchar(8), -- 數據補登人員代號

    public addendum           // Brand-dependent info

    static CardDetail queryByTransactionNumber(Connection connection,
        int posNumber, int transactionNumber) {

        def cardDetail = selectSingleRow(connection, CardDetail.class, """
            SELECT * FROM ${tableName(CardDetail.class)} WHERE posnumber=${posNumber} AND
            transactionnumber=${transactionNumber}""")

        if (cardDetail != null
            /*&& POSPeripheralHome3.instance.exsitsChinaTrustCAT()*/) // this will make inline client failed
        {
            def cardDetailChinaTrust = new CardDetailChinaTrust()
            if (selectSingleRow(connection, cardDetailChinaTrust, """
                SELECT * FROM ${cardDetailChinaTrust.tableName()}
                WHERE carddetailid=${cardDetail.id}"""))
                cardDetail.addendum = cardDetailChinaTrust
        }
        return cardDetail
    }

    /*
    static void main(s) {
        def x = selectSingleRow(CreamToolkit.getPooledConnection(), CardDetail.class, 
          "select * from carddetail")
        println x.id
    }
    static void main(s) {
        def x = new CardDetail(
            accountDate: [1970 - 1900, 0, 1] as Date,  // 營業日期
            posNumber: 1,                       // POS機號
            transactionNumber: 12345,           // 交易序號
            zSequenceNumber: 2,                 // Z帳序號
            isVoid: '0',                        // 是否被作廢（取消/退貨/調整）（'0':沒有被作廢、'1':被作廢）
            voidTransactionNumber: 0,           // 若此筆為被作廢交易，記錄作廢此筆交易的相應取消/退貨/調整的交易序號；若此筆為作廢交易（取消/退貨/調整），則記錄被作廢的交易序號
            transDateTime: [2008 - 1900, 8, 1] as Date, // 交易日期與時間
            cardNo: '1234567890123456',         // 卡號
            cardExpDate: '1212',                // 有效期
            approvalCode: '123456789',          // 授權碼
            terminalId: '12345678',             // 端末機編號
            transAmt: 12.34)                    // 總交易金額
            .insert(CreamToolkit.getPooledConnection())
    }
    static void main(s) {
        def conn = CreamToolkit.getPooledConnection()
        def x = loadById(conn, CardDetail.class, 27)
        println x.transactionNumber
        x.isVoid = 3
        x.transDateTime = new Date()
        x.update(conn)
    }
    */
}