package hyi.cream

import hyi.cream.POSTerminalApplication
import hyi.cream.dac.Payment
import hyi.cream.exception.ConfigurationNotFoundException
import hyi.cream.exception.NoSuchPOSDeviceException
import hyi.cream.groovydac.Param
import hyi.cream.state.StateMachine
import hyi.cream.util.CreamPrinter
import hyi.cream.util.CreamToolkit
import static hyi.cream.util.CreamToolkit.logMessage as log
import hyi.spos.services.ChinaTrustCAT
import hyi.spos.*
import hyi.spos.events.*
import org.apache.commons.io.FileUtils

/**
 * Groovy implementation of POSPeripheralHome.
 * <p/>
 * POSPeripheralHome is a container
 *
 * @author Bruce You
 * @since 2009/4/14 13:47:45
 */
class POSPeripheralHome3 implements DataListener, DirectIOListener,
    ErrorListener, OutputCompleteListener, StatusUpdateListener {

    // Singleton ----------------------------------------
    private static POSPeripheralHome3 instance = new POSPeripheralHome3()
    private POSPeripheralHome3() { init() }
    static POSPeripheralHome3 getInstance() { return instance }
    //---------------------------------------------------

    /**
     * Private fields which store the device control instance in a Map.<b/>
     * Key: Device category<b/>
     * Value: Device control object.
     */
    private Map<String, BaseControl> deviceControlsMap = [:]

    /**
     * Private fields which store the device logical name with its enties in a Map.<b/>
     * Key: {device_category}.{device_logical_name}<b/>
     * Value: SPOS entries.
     */
    private Map<String, Map<String, Object>> deviceEntries = [:]

    /**
     * Private fields which store the device control's logical name in a Map.
     * Key: Device category<b/>
     * Value: Device logical name.
     */
    private Map<String, String> deviceControlsLogicalNameMap = [:]

    private boolean eventForwardEnabled // flag to enable the event forwarding of

    private boolean isPrinterOK
    private Object printerTesterLocker

    private void init() {
        createDeviceEntries()
        openDeviceControls()
    }

    private void createDeviceEntries() {
        File jposXmlFile = CreamToolkit.getConfigurationFile("POSPeripheralHome");
        if (jposXmlFile == null) return

        def root = new XmlParser().parse(jposXmlFile)
        root.each {
            def logicalName = it.@logicalName
            //def factoryClass = it.creation[0].@factoryClass
            def serviceClass = it.creation[0].@serviceClass
            def category = it.jpos[0].@category
            //def description = it.product[0].@description
            //def name = it.product[0].@name
            //println "$logicalName($name: $description): $category, $factoryClass, $serviceClass"

            def entryMap = [:]
            entryMap['serviceClass'] = serviceClass
            it.prop.each {
                def pname = it.@name
                def type = it.@type
                def value = it.@value
                switch (type) {
                case 'Integer':
                case 'int':  value = value.toInteger(); break
                case 'Character':
                case 'char': value = value.charAt(0); break
                default:
                    // Bruce/20190702> Converting pattern like {0x1b}{0x69}. Why do I do this?
                    // Because XML doesn't support special characters like ESC character
                    // (might specify as &#x1b; in XML)
                    value = value.replaceAll(/\{0x[0-9a-fA-F]{1,4}\}/, { w ->
                        try {
                            String.valueOf([Integer.parseInt(w[3..-2], 16)] as char [])
                        } catch (Exception e) {
                            return w
                        }
                    })
                    break
                }
                entryMap[pname] = value
            }
            //println entryMap
            deviceEntries["$category.$logicalName"] = entryMap
        }
    }

    private Properties loadProperties() throws  ConfigurationNotFoundException {
        def 系統參數 = Param.instance
        Properties properties = new Properties()
        try {
            def posdevicesFile = CreamToolkit.getConfigurationFile(POSPeripheralHome3.class)
            if (posdevicesFile.exists()) {
                def posdevicesInput = new FileInputStream(posdevicesFile)
                properties.load(posdevicesInput)
                properties.each { deviceType, logicalName ->
                    系統參數."${deviceType}Device" = logicalName
                }
                posdevicesInput.close()

                // move conf file to subdirectory 'trash'
                def trashDir = [posdevicesFile.parent, 'trash'] as File
                FileUtils.copyFileToDirectory posdevicesFile, trashDir
                posdevicesFile.delete()
            }
            if (系統參數.ScannerDevice)       properties.Scanner = 系統參數.ScannerDevice
            if (系統參數.KeylockDevice)       properties.Keylock = 系統參數.KeylockDevice
            if (系統參數.MSRDevice)           properties.MSR = 系統參數.MSRDevice
            if (系統參數.POSKeyboardDevice)   properties.POSKeyboard = 系統參數.POSKeyboardDevice
            if (系統參數.CashDrawerDevice)    properties.CashDrawer = 系統參數.CashDrawerDevice
            if (系統參數.POSPrinterDevice)    properties.POSPrinter = 系統參數.POSPrinterDevice
            if (系統參數.ToneIndicatorDevice) properties.ToneIndicator = 系統參數.ToneIndicatorDevice
            if (系統參數.LineDisplayDevice)   properties.LineDisplay = 系統參數.LineDisplayDevice
            if (系統參數.CATDevice)           properties.CAT = 系統參數.CATDevice
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger())
            throw new ConfigurationNotFoundException("Can't load peripherals configuration.")
        }
        return properties
    }

    /**
     * Create all instances and open relevant services by reading posdevices3.conf and store them
     * in a Map.
     */
    public void openDeviceControls() throws ConfigurationNotFoundException, JposException {

        loadProperties().each { String controlType, String logicalName ->
            try {
                Map<String, Object> entry = deviceEntries["$controlType.$logicalName"]
                if (entry == null)
                    return
                String serverClass = entry.serviceClass
                if (serverClass == null) return

                log("Device: $controlType = $logicalName")
                log("        Construct ${serverClass} ...")
                BaseControl deviceControl = createDeviceControl(serverClass , entry)
                log("        Open ${logicalName} ...")

                int printTimeOut = Param.getInstance().getOpenPrinterWaitTime()
                if (printTimeOut > 0 && deviceControl instanceof POSPrinter)
                    openPrinterForTimeOut(deviceControl, logicalName, printTimeOut)
                else
                    deviceControl.open(logicalName)

                log("        Successfully open device: ${logicalName}.")
                deviceControlsMap[controlType] = deviceControl
                deviceControlsLogicalNameMap[controlType] = logicalName

                // Associate the corresponding POS device of some payments, such as CAT
                String paymentID = entry['PaymentID']
                if (paymentID != null) {
                    Payment payment = Payment.queryByPaymentID(paymentID)
                    if (payment != null)
                        payment.setPosDevice(deviceControl)
                }

            } catch (Exception e) {
                log e
            }
        }
    }

    private BaseControl createDeviceControl(String controlClassName , Map<String, Object> entry)
            throws ConfigurationNotFoundException {
        BaseControl controlInstance = null
        try {
            controlInstance = Class.forName(controlClassName).newInstance(entry)
        } catch (Exception e) {
            log e
        }
        return controlInstance;
    }

    /**
     * One thread to open printer, another thread check time out
     * when timeout, setDeviceEnabled(false).
     *
     * @param printer
     * @param logicalName
     * @param printTimeOut
     * @throws Exception
     */
    private void openPrinterForTimeOut(final BaseControl printer, final String logicalName, final int printTimeOut){

        Thread openPrinterThread = new Thread() {
            void run() {
                try {
                    printer.open(logicalName)
                    CreamPrinter.getInstance().setPrintEnabled(true)
                    CreamPrinter.getInstance().tryPrint()
                    log("Printer open and try print successfully")
                    synchronized (printerTesterLocker) {
                        isPrinterOK = true
                        printerTesterLocker.notifyAll()
                    }
                } catch (Exception e) {
                    log e
                    synchronized (printerTesterLocker) {
                        isPrinterOK = false
                        printerTesterLocker.notifyAll()
                    }
                }
            }
        }
        printerTesterLocker = new Object()
        synchronized (printerTesterLocker) {
            openPrinterThread.start()
            try {
                isPrinterOK = false
                log("tryPrinter ... (timeout limit ${printTimeOut}s)")
                printerTesterLocker.wait(printTimeOut * 1000L)
            } catch (InterruptedException e) {
                log e
            }
            if (!isPrinterOK) {
                log("printer faild or waiting DeviceEnabled timeout(" + printTimeOut + "s)")
                CreamPrinter.getInstance().isPrintToConsoleInsteadOfPrinter = true
            }
        }
    }

    static void main(args) {
        POSPeripheralHome3.getInstance()
    }

    public void setEventForwardEnabled (boolean enabled) {
        this.eventForwardEnabled = enabled;
    }

    public boolean getEventForwardEnabled () {
        return eventForwardEnabled;
    }

    void dataOccurred(DataEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine.instance.processEvent e
    }

    void directIOOccurred(DirectIOEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine.instance.processEvent e
    }

    void errorOccurred(ErrorEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine.instance.processEvent e
    }

    void outputCompleteOccurred(OutputCompleteEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine.instance.processEvent e
    }

    void statusUpdateOccurred(StatusUpdateEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine.instance.processEvent e
    }

    boolean existsChinaTrustCAT() {
        for (BaseControl control : deviceControlsMap.values())
            if (control instanceof ChinaTrustCAT)
                return true
        return false
    }

    /**
     * 取得當前交易的連線信用卡支付的device。
     */
    CAT getCAT() {
        def trans = POSTerminalApplication.instance.currentTransaction
        String creditCardPaymentID = null
        if (trans != null) {
            Payment payment = trans.getCreditCardPayment()
            if (payment != null)
                creditCardPaymentID = payment.getPaymentID()
        }
        if (creditCardPaymentID == null)
            return null;

        for (String typeName in deviceControlsMap.keySet()) {
            if (typeName.startsWith("CAT")) {
                def cat = deviceControlsMap[typeName]
                def logicalName = deviceControlsLogicalNameMap[typeName]
                def paymentID = deviceEntries["$typeName.$logicalName"]['PaymentID'];
                if (creditCardPaymentID != null && creditCardPaymentID.equals(paymentID))
                    return cat;
            }
        }
        return null;
    }

    POSPrinter getPOSPrinter() {
        def posPrinter = deviceControlsMap.POSPrinter
        if (posPrinter != null)
            return posPrinter

        throw new NoSuchPOSDeviceException();
    }

    ToneIndicator getToneIndicator() {
        def toneIndicator = deviceControlsMap.ToneIndicator
        if (toneIndicator != null)
            return toneIndicator

        throw new NoSuchPOSDeviceException();
    }

    CashDrawer getCashDrawer() {
        def cashDrawer = deviceControlsMap.CashDrawer
        if (cashDrawer != null)
            return cashDrawer

        throw new NoSuchPOSDeviceException();
    }

    LineDisplay getLineDisplay() {
        def lineDisplay = deviceControlsMap.LineDisplay
        if (lineDisplay != null)
            return lineDisplay

        throw new NoSuchPOSDeviceException();
    }

    String getLineDisplayLogicalName() {
        return deviceControlsLogicalNameMap.LineDisplay
    }

    Keylock getKeylock() {
        def keylock = deviceControlsMap.Keylock
        if (keylock != null)
            return keylock

        throw new NoSuchPOSDeviceException();
    }

    String getKeylockLogicalName() {
        return (String)deviceControlsLogicalNameMap.get("Keylock");
    }

    POSKeyboard getPOSKeyboard() {
        def posKeyboard = deviceControlsMap.POSKeyboard
        if (posKeyboard != null)
            return posKeyboard

        throw new NoSuchPOSDeviceException();
    }

    hyi.spos.Scanner getScanner() {
        def scanner = deviceControlsMap.Scanner
        if (scanner != null)
            return scanner

        throw new NoSuchPOSDeviceException();
    }

    MSR getMSR() {
        def msr = deviceControlsMap.MSR
        if (msr != null)
            return msr

        throw new NoSuchPOSDeviceException();
    }

    /** Method to close all pos device controls. */
    void closeAll() throws JposException {
        deviceControlsMap?.values().each { it.close() }
    }

    void clearAll() {
        deviceControlsMap?.clear()
        deviceControlsMap = null
    }
}