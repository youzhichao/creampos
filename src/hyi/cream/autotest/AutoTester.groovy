package hyi.cream.autotest

import hyi.cream.POSButtonHome2
import hyi.cream.POSPeripheralHome3
import hyi.cream.dac.Payment
import hyi.cream.uibeans.*
import hyi.cream.util.CreamToolkit
import hyi.cream.util.HYIDouble
import hyi.spos.POSKeyboard
import hyi.spos.ScannerConst
import java.lang.reflect.Method
import java.sql.Connection
import java.sql.Time
import org.apache.commons.lang.StringUtils
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.hssf.util.HSSFColor
import org.apache.poi.poifs.filesystem.POIFSFileSystem
import java.text.SimpleDateFormat
import org.apache.poi.hssf.util.CellRangeAddress
import hyi.spos.events.StatusUpdateEvent
import hyi.cream.state.StateMachine
import hyi.cream.ScreenCapture
import org.apache.commons.io.FilenameUtils
import hyi.cream.groovydac.Param


/**
 * Auto tester.
 *
 * @author Bruce You
 */
class AutoTester {

    private static Binding binding
    private static GroovyShell shell
    private static POSPeripheralHome3 posPeripheralHome = POSPeripheralHome3.instance
    private static POSKeyboard posKeyboard = posPeripheralHome.POSKeyboard
    private static POSButtonHome2 posButtonHome = POSButtonHome2.instance
    private static boolean failed
    private static boolean stopOnFailed
    private static int lastRow
    private static List<String> messages = []

    /**
     * Run auto-test Excel scripts under directory "autotest", sorted by file names.
     * Then append result back to Excel file and copy them into a subdirectory named with
     * the current timestamp. The resulted Excel file name will be added a "_OK" or "Failed."
     */
    static void run() {
        failed = false

        if (!new File('autotest').isDirectory()) {
            println 'Cannot find subdirectory: autotest.'
            return
        }

        def resultDirName = new SimpleDateFormat('yyyyMMdd_HHmmss').format(new Date())
        def resultDir = new File("autotest/${resultDirName}")
        resultDir.mkdir()

        def files = new File('autotest').listFiles()
        Arrays.sort(files)
        for (file in files) {
            if (file.isFile() && file.name.endsWith('.xls')) {
                println "Run ${file.name}..."
                def cont = run(file, resultDir)
                if (!cont)
                    break
            }
        }
    }

    /**
     * Run auto-testing Excel script file.
     *
     * @return whether or to continue
     */
    static boolean run(File testCaseExecelFile, File resultDir) {
        Connection conn
        try {
            InputStream excelFile = testCaseExecelFile.newInputStream()
            HSSFWorkbook workbook = new HSSFWorkbook(new POIFSFileSystem(excelFile))
            HSSFSheet sheet = workbook.getSheetAt(0)
            HSSFSheet checkSheet = workbook.getSheetAt(1)

            stopOnFailed = false
            try {
                int stopOnFailedRow = locateSection(sheet, 'Stop on failed') - 1
                if (stopOnFailedRow != -1) {
                    def val = sheet.getRow(stopOnFailedRow).getCell(2).getRichStringCellValue().toString()
                    val = val.toLowerCase()
                    stopOnFailed = (val == 'yes' || val == 'true');
                }
            } catch (Exception e) {}

            int precondRow = locateSection(sheet, 'Precondition')
            //println "Precondition section is at row: ${precondRow}"
            int preprocessingRow = locateSection(sheet, 'Preprocessing script')
            //println "Preprocessing script section is at row: ${preprocessingRow}"
            int eventsRow = locateSection(sheet, 'Events injected')
            //println "Events injected section is at row: ${eventsRow}"
            doPrecondition(sheet, precondRow, preprocessingRow - 2)

            conn = CreamToolkit.getTransactionalConnection()
            binding = new Binding([conn: conn, PARAM: Param.instance])
            shell = new GroovyShell(AutoTester.class.getClassLoader(), binding)
            doProcessingScript(sheet, preprocessingRow)
            //println binding.getVariables()
            injectEvents(sheet, testCaseExecelFile, eventsRow, resultDir)

            int postProcessingRow = locateSection(checkSheet, 'Postprocessing script')
            //println "Postprocessing script is at row: ${postProcessingRow}"
            doProcessingScript(checkSheet, postProcessingRow)

            int checkingRow = locateSection(checkSheet, 'Checking')
            //println "Checking section is at row: ${checkingRow}"
            doChecking(checkSheet, checkingRow)

            writeResult(workbook, sheet, checkSheet, testCaseExecelFile, resultDir)

            return !(failed && stopOnFailed)
        } catch (Exception e) {
            e.printStackTrace()
        } finally {
            conn?.close()
            shell = null
            binding = null
            messages.clear()
            failed = false
        }
    }

    private static locateSection(HSSFSheet sheet, String sectionText) {
        for (row in 0..300) {
            def sheetRow = sheet.getRow(row)
            if (sheetRow != null) {
                String val = readStringValue(sheetRow, 0)
                if ('#' == val)
                    continue

                val = readStringValue(sheetRow, 1)
                if (StringUtils.isEmpty(val))
                    continue
                val = val - ':'
                if (val.equalsIgnoreCase(sectionText))
                    return row + 1
            }
        }
        return -1
    }

    private static void doPrecondition(HSSFSheet sheet, int rowFrom, int rowTo) {
        Class dacClass
        def dacObject = null
        Method method = null

        for (int row = rowFrom; row <= rowTo; row++) {
            def sheetRow = sheet.getRow(row)
            def nextSheetRow = sheet.getRow(row + 1)
            if (sheetRow == null) return

            def val = readStringValue(sheetRow, 0)
            if ('#' == val) continue

            val = readStringValue(sheetRow, 1)
            if (!StringUtils.isEmpty(val)) {
                if (dacObject != null)
                    method.invoke(dacObject)

                def tmp = val.split('\\.')
                dacClass = Class.forName("hyi.cream.groovydac.${tmp[0]}")
                dacObject = dacClass.newInstance()
                method = dacClass.getMethod(tmp[1])
            }

            def emptyCnt = 0
            boolean propertyNameAndValuesAreAtTheSameRow = true
            for (col in 2..99) {
                val = readStringValue(sheetRow, col)
                if (StringUtils.isEmpty(val)) {
                    if (++emptyCnt >= 3)
                        break
                    continue
                } else {
                    emptyCnt = 0
                }

                def propertyName
                def propertyValue
                val = val.split(/[\n\r]+/)[0].trim() // ignore the second line
                if (val.contains('=')) {
                    def tmp = val.split('=')
                    propertyName = tmp[0]
                    propertyValue = tmp[1]
                    setPropertyValue(dacObject, propertyName, propertyValue)
                } else {
                    propertyNameAndValuesAreAtTheSameRow = false
                    propertyName = val
                    setPropertyValue(dacObject, propertyName, nextSheetRow, col)
                }
            }

            if (!propertyNameAndValuesAreAtTheSameRow)
                row++
        }
        if (dacObject != null)
            method?.invoke(dacObject)
    }

    private static void doChecking(HSSFSheet sheet, int rowFrom) {
        Class dacClass
        def dacObject = null
        String methodName
        def methodParam = null
        List checkingFields = []

        for (int row = rowFrom; true; row++) {
            def sheetRow = sheet.getRow(row)
            def nextSheetRow = sheet.getRow(row + 1)
            if (sheetRow == null) {
                lastRow = row + 1
                break
            }

            def val = readStringValue(sheetRow, 0)
            if ('#' == val)
                continue

            val = readStringValue(sheetRow, 1)
            if (!StringUtils.isEmpty(val)) {
                if (dacObject != null) {
                    if (methodParam != null)
                        dacObject.metaClass.invokeMethod(dacObject, methodName, methodParam, checkingFields)
                    else
                        dacObject.metaClass.invokeMethod(dacObject, methodName, checkingFields)
                }

                def tmp = val.split(/\./)
                dacClass = Class.forName("hyi.cream.groovydac.${tmp[0]}")
                dacObject = dacClass.newInstance()
                methodName = tmp[1]
                checkingFields.clear()

                // get method parameter
                def param = readRawStringValue(nextSheetRow, 1)
                if (StringUtils.isEmpty(param))
                    methodParam = null
                else if (param != null && param ==~ /\$\{.*\}/)
                    methodParam = binding."${param[2..-2]}"
                else
                    methodParam = param
            }

            def emptyCnt = 0
            boolean propertyNameAndValuesAreAtTheSameRow = true
            for (col in 2..99) {
                val = readStringValue(sheetRow, col)
                if (StringUtils.isEmpty(val)) {
                    if (++emptyCnt >= 3)
                        break
                    continue
                } else {
                    emptyCnt = 0
                }

                def propertyName
                def propertyValue
                val = val.split(/[\n\r]+/)[0].trim() // ignore the second line
                if (val.contains('=')) {
                    def tmp = val.split('=')
                    propertyName = tmp[0]
                    propertyValue = tmp[1]
                    setPropertyValue(dacObject, propertyName, propertyValue)
                } else {
                    propertyNameAndValuesAreAtTheSameRow = false
                    propertyName = val
                    setPropertyValue(dacObject, propertyName, nextSheetRow, col)
                }
                if (propertyName.equalsIgnoreCase('category')) {
                    checkingFields << 'catno'
                    checkingFields << 'midcatno'
                    checkingFields << 'microcatno'
                    checkingFields << 'thincatno'
                } else if (propertyName.equalsIgnoreCase('dealtype')) {
                    checkingFields << 'dealtype1'
                    checkingFields << 'dealtype2'
                    checkingFields << 'dealtype3'
                } else
                    checkingFields << propertyName
            }

            if (!propertyNameAndValuesAreAtTheSameRow)
                row++
        }
        if (dacObject != null) {
            if (methodParam != null)
                dacObject.metaClass.invokeMethod(dacObject, methodName, methodParam, checkingFields)
            else
                dacObject.metaClass.invokeMethod(dacObject, methodName, checkingFields)
        }
    }

    private static void setPropertyValue(dacObject, String propertyName,
        String propertyValue) {
        Class dacClass = dacObject.class
        def field
        def type
        try {
            field = dacClass.getDeclaredField(propertyName)
            type = field.getType()
        } catch (Exception e) {
            type = String.class
        }
        try {
            switch (type) {
                case Time:
                    dacObject."${propertyName}" = parseTime(propertyValue)
                    break
                case Date:
                    dacObject."${propertyName}" = parseDate(propertyValue)
                    break
                case HYIDouble:
                    dacObject."${propertyName}" = new HYIDouble(propertyValue)
                    break
                case Integer:
                    dacObject."${propertyName}" = Integer.parseInt(propertyValue)
                    break;
                default:
                    if (propertyName.equalsIgnoreCase('dealtype')) {
                        try {
                            def val = propertyValue.split(/-/)
                            dacObject.dealtype1 = val[0]
                            dacObject.dealtype2 = val[1]
                            dacObject.dealtype3 = val[2]
                        } catch (Exception e) {}
                    } else if (propertyName.equalsIgnoreCase('category')) {
                        try {
                            def val = propertyValue.split(/-/)
                            dacObject.catno = val[0]
                            dacObject.midcatno = val[1]
                            dacObject.microcatno = val[2]
                            dacObject.thincatno = val[3]
                        } catch (Exception e) {}
                    } else
                        dacObject."${propertyName}" = propertyValue
            }
        } catch (Exception e) {
            addMessage "Cannot set property: ${propertyName} for ${dacObject.class.name}, type: ${type}"
        }
    }

    private static void setPropertyValue(dacObject, String propertyName, HSSFRow sheetRow,
        int col) {
        Class dacClass = dacObject.class
        def field
        def type
        try {
            field = dacClass.getDeclaredField(propertyName)
            type = field.getType()
        } catch (Exception e) {
            type = String.class
        }
        //println "Property: ${propertyName}, Type: ${type}"
        try {
            switch (type) {
                case Time:
                    dacObject."${propertyName}" = readTimeValue(sheetRow, col)
                    break
                case Date:
                    dacObject."${propertyName}" = readDateValue(sheetRow, col)
                    break
                case HYIDouble:
                    def propertyValue = readNumericValue(sheetRow, col)
                    if (propertyValue != null) {
                        propertyValue = new HYIDouble(propertyValue)
                        dacObject."${propertyName}" = propertyValue
                    }
                    break
                case Short:
                case Integer:
                case int:
                    def propertyValue = readIntValue(sheetRow, col)
                    if (propertyValue != null) {
                        propertyValue = doubleToInt(propertyValue)
                        dacObject."${propertyName}" = propertyValue
                    }
                    break;
                default:
                    def propertyValue = readStringValue(sheetRow, col)
                    if (propertyName.equalsIgnoreCase('dealtype')) {
                        try {
                            def val = propertyValue.split(/-/)
                            dacObject.dealtype1 = val[0]
                            dacObject.dealtype2 = val[1]
                            dacObject.dealtype3 = val[2]
                        } catch (Exception e) {}
                    } else if (propertyName.equalsIgnoreCase('category')) {
                        try {
                            def val = propertyValue.split(/-/)
                            dacObject.catno = val[0]
                            dacObject.midcatno = val[1]
                            dacObject.microcatno = val[2]
                            dacObject.thincatno = val[3]
                        } catch (Exception e) {}
                    } else
                        dacObject."${propertyName}" = propertyValue
            }
        } catch (Exception e) {
            addMessage "Cannot set property: ${propertyName} for ${dacObject.class.name}, type: ${type}"
        }
    }

    private static void doProcessingScript(HSSFSheet sheet, int rowFrom) {
        for (int row = rowFrom; true; row++) {
            def sheetRow = sheet.getRow(row)
            if (sheetRow == null)
                break
            String scriptText = readStringValue(sheetRow, 0)
            if ('#' == scriptText)
                continue

            scriptText = readStringValue(sheetRow, 1)
            if (StringUtils.isEmpty(scriptText))
                break
            //println "Excuting ${scriptText}"
            shell.evaluate("""
                import hyi.cream.groovydac.*
                import hyi.cream.state.*
                import hyi.cream.util.*
                ${scriptText}
            """)
        }
    }

    private static void injectEvents(HSSFSheet sheet, File testCaseExecelFile,
        int rowFrom, File resultDir) {
        def col = 1
        try {
            def sheetRow = sheet.getRow(rowFrom)
            while (!(readTrimmedStringValue(sheetRow, col) =~ /(?i)Delay\sbefore/)) if (++col > 88) return
            def delayBeforeCol = col++
            while (!(readTrimmedStringValue(sheetRow, col) =~ /(?i)Event/)) if (++col > 88) return
            def eventCol = col++
            while (!(readTrimmedStringValue(sheetRow, col) =~ /(?i)Parameters/)) if (++col > 88) return
            def parametersCol = col++
            while (!(readTrimmedStringValue(sheetRow, col) =~ /(?i)Delay\safter/)) if (++col > 88) return
            def delayafterCol = col

            for (int row = rowFrom + 1; true; row++) {
                sheetRow = sheet.getRow(row)
                if (sheetRow == null)
                    break
                String scriptText = readStringValue(sheetRow, 0)
                if ('#' == scriptText)
                    continue

                sleepAWhile readStringValue(sheetRow, delayBeforeCol)
                def eventSource = readStringValue(sheetRow, eventCol)
                if (StringUtils.isEmpty(eventSource))
                    continue

                def parameters = readTrimmedStringValue(sheetRow, parametersCol)
                try {
                    def count = 1
                    if (eventSource ==~ /.*\*\s*\d+\s*/) { // in format like: "XXX * 3"
                        def pos = eventSource.lastIndexOf('*')
                        count = eventSource.substring(pos + 1).trim().toInteger()
                        eventSource = eventSource.substring(0, pos).trim()
                    }
                    count.times {
                        if (eventSource ==~ /[0-9\.\-]+/) { // a series of numbers
                            eventSource.each {
                                fireEvent(it, parameters, resultDir, testCaseExecelFile)
                                StateMachine.instance.waitForEventQueueEmpty()
                            }
                        } else {
                            fireEvent(eventSource, parameters, resultDir, testCaseExecelFile)
                            StateMachine.instance.waitForEventQueueEmpty()
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace()
                }

                sleepAWhile readStringValue(sheetRow, delayafterCol)
            }
        } finally {
            if (col > 88)
                println 'Cannot find appropriate event table header.'
        }
    }

    /**
     * @param duration Available format: "120ms", "1s", "1m".
     */
    private static void sleepAWhile(String duration) {
        try {
            if (StringUtils.isEmpty(duration))
                return
            if ('sync' == duration) {
                StateMachine.instance.waitForTransactionEnd()
                return
            }
            duration = duration.trim()
            if (duration.endsWith('ms'))
                sleep(duration[0..-3].toLong())
            else if (duration.endsWith('s'))
                sleep(duration[0..-2].toLong() * 1000L)
            else if (duration.endsWith('m'))
                    sleep(duration[0..-2].toLong() * 60L * 1000L)
        } catch (Exception e) {
        }
    }

    private static void fireEvent(String eventSource, String parameters, File resultDir,
        File testCaseExecelFile) {
        POSButton button

        switch (eventSource) {
            case ~/(?i).*(钥匙|鑰匙|Keylock).*/:
                posPeripheralHome.keylock.fireEvent(parameters.toInteger())
                return

            case ~/(?i).*(掃描|扫描|Scan).*/:
                posPeripheralHome.scanner.fireDataEvent(ScannerConst.SCAN_SDT_EAN13, parameters)
                return

            case ~/(?i).*(刷卡|磁卡|MSR).*/:
                def card = parameters.split(/\s/)
                if (card.length != 2)
                    println 'Please give me 2 parameters for MSR.'
                else
                    posPeripheralHome.MSR.fireEvent(card[0], card[1]);
                return

            case ~/(?i).*(抽屜|抽屉|CashDrawer).*/:
                posPeripheralHome.cashDrawer.fireStatusUpdateEvent(
                    new StatusUpdateEvent(posPeripheralHome.cashDrawer))
                return

        // POSButtons --
            case ~/[0-9\.\-]/:
                button = new NumberButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(數字|数字|Number).*/:
                button = new NumberButton(0, 0, 0, parameters)
                break

            case ~/(?i).*(截圖|截屏|ScreenCapture).*/:
                button = new ScreenCaptureButton(0, 0, 0, eventSource)
                def captureFile = "${FilenameUtils.getBaseName(testCaseExecelFile.getName())}_${parameters}"
                if (!captureFile.endsWith('.png'))
                    captureFile += '.png'
                ScreenCapture.nextCaptureFile = [resultDir, captureFile] as File
                break

            case ~/(?i)(小計後折扣|小计后折扣|SI).*/:
                button = new SIButton(0, 0, 0, eventSource, parameters)
                break

            case ~/(?i).*(小計|小计|AgeLevel|Tender).*/:
                button = new AgeLevelButton(0, 0, 0, eventSource, parameters)
                break

            case ~/(?i).*(數量|数量|Quantity|QTY).*/:
                button = new QuantityButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(退貨|退货|Return|Refund).*/:
                button = new ReturnButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(投庫|投库|CashOut|Cash-Out).*/:
                button = new CashOutButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(借零|CashIn|Cash-In).*/:
                button = new CashInButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(入金|PaidIn|Paid-In).*/:
                button = new PaidInButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(出金|PaidOut|Paid-Out).*/:
                button = new PaidOutButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(保留|挂单|掛單|Hold).*/:
                button = new TransactionHoldButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(負項銷售|负项销售|ReturnSale|NegativeSale).*/:
                button = new ReturnSaleButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(訂金|订金|DownPayment).*/:
                button = new DownPaymentButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(尾款|BackPayment).*/:
                button = new BackPaymentButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(退訂金|退订金|DownPaymentRefund).*/:
                button = new DownPaymentRefundButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(退尾款|BackPaymentRefund).*/:
                button = new BackPaymentRefundButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(價格調查|价格调查|查價|查价|Inquiry).*/:
                button = new InquiryButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(前筆誤打|前笔误打|Transaction.*Void|Void.*Transaction|VoidLast|CancelLast).*/:
                button = new TransactionVoidButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(傳票|传票|PeiDa).*/:
                button = new PeiDaButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(重印|Reprint|PrintLast).*/:
                button = new ReprintButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(開.*屜|开.*屉|開錢箱|开钱箱|DrawerOpen|OpenDrawer).*/:
                button = new DrawerOpenButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(發票NO|发票NO|發票號|发票号|Invoice).*/:
                button = new InvoiceButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(收銀員|收银员|Menu|CheckInOut).*/:
                button = new CheckInOutButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(統一編號|统一编号|統編|统编|BuyerNo).*/:
                button = new BuyerNoButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(變價|变价|統編|Override).*/:
                button = new OverrideAmountButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(部門|部门|Category).*/:
                button = new CategoryAmountButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(清除|Clear|ESC).*/:
                button = new ClearButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(交易取消|Cancel).*/:
                button = new CancelButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(修正|更正|Remove|Delete).*/:
                button = new RemoveButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(上頁|上页|PageUp).*/:
                button = new PageUpButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(下頁|下页|PageDown).*/:
                button = new PageDownButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(確認|确认|現金|现金|Enter|Cash).*/:
                button = new EnterButton(0, 0, 0, eventSource)
                break

            case ~/(?i).*(支付|Payment).*/:
                if (eventSource =~ /支付.+/) {
                    try {
                        // Matching pattern examples:
                        // '支付 信用卡'
                        // '支付:信用卡'
                        // '支付: 信用卡'
                        // '支付：信用卡'
                        // '支付-信用卡'
                        // '支付- 信用卡'
                        // '支付－信用卡'
                        // '支付.信用卡'
                        def paymentName = eventSource.split(/[:\s\.\uff1a\uff0d-]+/)[1]
                        // \uff1a: 全形(全角)冒號, \uff0d: 全形(全角)破折號
                        def paymentId = Payment.queryPaymentIdByName(paymentName)
                        if (paymentId != null) {
                            if (paymentName =~ /現金|现金/)
                                button = new EnterButton(0, 0, 0, eventSource)
                            else
                                button = new PaymentButton(0, 0, 0, eventSource, paymentId)
                            break
                        }
                    } catch (Exception e) {
                    }
                }
                button = new PaymentButton(0, 0, 0, eventSource, parameters)
                break

            default:
                def params = [0, 0, 0, eventSource]
                params.addAll(Arrays.asList(parameters.split(/\s/)))
                def className = eventSource.startsWith('hyi.') ? eventSource :
                    "hyi.cream.uibeans.${eventSource}"
                button = Class.forName(className).newInstance(* params)
                break
        }

        def keyCode = posButtonHome.findKeyCode(button)
        if (keyCode != null) {
            posKeyboard.setPOSKeyData(keyCode);
            posKeyboard.fireEvent();
        } else {
            println "Cannot find event: ${eventSource}, ${parameters}. Could not find this button."
        }
    }

    private static String readTrimmedStringValue(HSSFRow sheetRow, int col) {
        def x = readStringValue(sheetRow, col)
        if (x != null)
            return x.trim()
        return x
    }

    private static String readRawStringValue(HSSFRow sheetRow, int col) {
        try {
            if (col < 0)
                return ''
            else {
                HSSFCell cell = sheetRow.getCell(col)
                return cell.getRichStringCellValue().toString()
            }
        } catch (Exception e) {
            try {
                return readNumericValue(sheetRow, col).longValue() + ''
            } catch (Exception e2) {
                return ''
            }
        }
    }

    private static String readStringValue(HSSFRow sheetRow, int col) {
        try {
            HSSFCell cell = sheetRow.getCell(col)
            String stringValue = cell.getRichStringCellValue().toString()
            if (stringValue ==~ /\$\{.*\}/) {
                return shell.evaluate(stringValue[2..-2])
            } else
                return stringValue
        } catch (Exception e) {
            try {
                return readNumericValue(sheetRow, col).longValue() + ''
            } catch (Exception e2) {
                return ''
            }
        }
    }

    private static Double readNumericValue(HSSFRow sheetRow, int col) {
        try {
            HSSFCell cell = sheetRow.getCell(col)
            try {
                String stringValue = cell.getRichStringCellValue().toString()
                if (StringUtils.isEmpty(stringValue)) // check if emtpy
                    return null // means empty cell

                if (stringValue ==~ /\$\{.*\}/)
                    return shell.evaluate(stringValue[2..-2])
            } catch (IllegalStateException e) {
                // normal
            }
            return cell.getNumericCellValue()
        } catch (NumberFormatException e) {
            return null
        } catch (Exception e) {
            return null
        }
    }

    private static Integer readIntValue(HSSFRow sheetRow, int col) {
        try {
            HSSFCell cell = sheetRow.getCell(col)
            try {
                String stringValue = cell.getRichStringCellValue().toString()
                if (StringUtils.isEmpty(stringValue)) // check if emtpy
                    return null // means empty cell

                if (stringValue ==~ /\$\{.*\}/) {
                    return shell.evaluate(stringValue[2..-2])
                    //String n = stringValue[2..-2]
                    //def v = binding.getVariable(n)
                    //return v
                    //return binding."${stringValue[2..-2]}"
                }
            } catch (IllegalStateException e) {
                // normal
            }
            return cell.getNumericCellValue().toInteger()
        } catch (NumberFormatException e) {
            return null
        } catch (Exception e) {
            return null
        }
    }

    private static Date readDateValue(HSSFRow sheetRow, int col) {
        String stringValue
        HSSFCell cell = sheetRow.getCell(col)
        try {
            return cell.getDateCellValue()
        } catch (Exception e) {}

        try {
            return parseDate(cell.getRichStringCellValue().toString())
        } catch (Exception e) {}

        try {
            return parseDate(doubleToInt(cell.getNumericCellValue()) + '')
        } catch (Exception e) {}

        return null
    }

    private static Date parseDate(String stringValue) {
        if (stringValue ==~ /\$\{.*\}/)
            return shell.evaluate(stringValue[2..-2])

        def date
        if (stringValue.length() == 8) {
            date = java.sql.Date.valueOf(new StringBuilder(stringValue)
                .insert(4, '-').insert(7, '-').toString())
        } else {
            stringValue = stringValue.replace('/', '-')
            date = java.sql.Date.valueOf(stringValue)
        }
        Calendar cal = Calendar.getInstance()
        cal.setTime(date)
        cal.set(Calendar.HOUR, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        return cal.getTime()
    }

    private static Time parseTime(String stringValue) {
        if (stringValue ==~ /\$\{.*\}/)
            return shell.evaluate(stringValue[2..-2])
        else
            return Time.valueOf(stringValue)
    }

    private static Time readTimeValue(HSSFRow sheetRow, int col) {
        try {
            Date date = readDateValue(sheetRow, col)
            return new Time(date.getTime())
        } catch (Exception e) {}

        try {
            def cell = sheetRow.getCell(col)
            def stringValue = cell.getRichStringCellValue().toString()
            if (!StringUtils.isEmpty(stringValue))
                return parseTime(stringValue)
            else
                return null
        } catch (Exception e) {}
        
        return null
    }

    static int doubleToInt(Double doubleValue) {
        return doubleValue == null ? 0 :
            new BigDecimal(doubleValue).setScale(0, BigDecimal.ROUND_HALF_UP).intValue()
    }

    static void addMessage(String message) {
        messages << message
        println message
        if (!ok(message))
            failed = true
    }

    /** Treat it as failed if it contains negative words. */
    private static boolean ok(String m) {
        !m.contains('Not matched') && !m.contains('but actually') && !m.contains('Cannot')
    }

    /**
     * Write result back to Excel file.
     */
    private static void writeResult(HSSFWorkbook wb, HSSFSheet sheet, HSSFSheet checkSheet,
        File testCaseExecelFile, File resultDir) {
        String result = 'OK'
        for (m in messages)
            if (m.contains('Not matched')) {
                result = 'Failed'
                break
            }

        def okColor = result == 'OK'

        // Test date
        setCellText(wb, sheet, 1, 2,
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), okColor, 2)

        // Test result
        setCellText(wb, sheet, 2, 2, result, okColor, 2)

        // Test detail result
        def row = lastRow
        for (m in messages)
            setCellText(wb, checkSheet, row++, 1, m, ok(m), 6)

        // save it to result directory
        def fileName = testCaseExecelFile.name
        fileName = new StringBuilder(fileName).insert(fileName.lastIndexOf('.'), "_${result}")
        FileOutputStream out = new FileOutputStream([resultDir, fileName.toString()] as File)
        wb.write(out);
        out.close();
    }

    private static def setCellText(HSSFWorkbook wb, HSSFSheet sheet, int row, int col,
        String text, boolean okColor, int mergedColumns) {
        HSSFRow cellRow = sheet.getRow(row)
        if (cellRow == null)
            cellRow = sheet.createRow(row)
        HSSFCell cell = cellRow.getCell(col)
        if (cell == null)
            cell = sheet.getRow(row).createCell(col)
        cell.setCellValue(text)
        setCellColor(wb, cell, okColor)

        sheet.addMergedRegion(new CellRangeAddress(
            row, //first row (0-based)
            row, //last row  (0-based)
            col, //first column (0-based)
            col + mergedColumns - 1  //last column  (0-based)
        ));
    }

    private static setCellColor(HSSFWorkbook wb, HSSFCell cell, boolean okColor) {
        // OK: white on green, Failed: white on red
        HSSFCellStyle style = wb.createCellStyle();
        style.setFillForegroundColor(okColor ? HSSFColor.GREEN.index : HSSFColor.RED.index);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        HSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short)10);
        font.setFontName('Lucida Console');
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        cell.setCellStyle(style)
    }
}
