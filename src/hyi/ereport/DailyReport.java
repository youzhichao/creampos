package hyi.ereport;

import hyi.ereport.util.*;
import hyi.ereport.table.*;

import java.io.*;
import java.io.OutputStream;
import java.math.*;
import java.util.*;
import java.util.zip.*;
import java.io.*;
import java.text.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.sql.*;

import inetsoft.report.*;
import inetsoft.report.lens.*;
import inetsoft.report.lens.swing.*;

import hyi.cream.util.*;

public class DailyReport extends JFrame implements ActionListener {
    private final DecimalFormat decFormatter = new DecimalFormat("###,##0.00");
    private final DecimalFormat percentFormatter = new DecimalFormat("###,##0.0%");
    private ArrayList uploadTables = new ArrayList();
    private Properties prop = new Properties();
    private DBAccess dbAccess = null;
    private String configFile = "";
      
    //会计科目－－金额
    private final HashMap accountItemAndAmount = new HashMap();
    
    private boolean isEdited = false;
	
    //营业日期
    private String reportDate = null;
    //门店编号
    private String storeID = "";
    private String storeName = "";
    //销售人员
    private String salePersonID = "";
    private String salePersonName = "";
    
	private JButton jbPrintPreview = null;
	private JButton jbSave = null;
	private JButton jbCancel = null;
	private JButton jbExit = null;
	private JTable jtSaleDetail = null;
	private JTable jtDiscountInfo = null;
	private JTable jtReport = null;

	/**
	 * 
	 */
	public DailyReport() {
		super();
	}

	public DailyReport(String s) {
		super(s);

	}

	public static void main(String[] args) {
		final DailyReport dailyReport = new DailyReport();
        
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-sid"))
                dailyReport.storeID = args[i].substring(args[i].indexOf("=") + 1);
            else if (args[i].startsWith("-sname")) {
                dailyReport.storeName = args[i].substring(args[i].indexOf("=") + 1);
            } else if (args[i].startsWith("-uid")) {
                dailyReport.salePersonID = args[i].substring(args[i].indexOf("=") + 1);
            } else if (args[i].startsWith("-uname")) { 
                dailyReport.salePersonName = args[i].substring(args[i].indexOf("=") + 1);        
            }else if (args[i].startsWith("-date")) {
                dailyReport.reportDate = args[i].substring(args[i].indexOf("=") + 1);        
            } else if (args[i].startsWith("-path")) {
                dailyReport.configFile = args[i].substring(args[i].indexOf("=") + 1);
            }
        }
        
        if (dailyReport.configFile == null || dailyReport.configFile.length() == 0) {
            System.out.println("Error! the path of config file must be specified");   
            return ;
        }
        
        dailyReport.prop = new ReportProperties(dailyReport.configFile);
        dailyReport.dbAccess = new DBAccess(dailyReport.prop);

        dailyReport.init();
        dailyReport.resize(new Dimension(700,500));

        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dr = dailyReport.getSize();
        dailyReport.move((ds.width - dr.width)/2,(ds.height - dr.height)/2);

        dailyReport.setVisible(true);
        dailyReport.resizeTables();

        dailyReport.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        dailyReport.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
                if (dailyReport.isEdited) {
                    int r = JOptionPane.showConfirmDialog(
                                    (Component)dailyReport,
                                    "保存所作的更改？",
                                    "提示！",
                                    JOptionPane.YES_NO_CANCEL_OPTION
                                    ); 
                     if (r == JOptionPane.YES_OPTION) {
                         dailyReport.saveAccDayRpt();               
                     } else if (r == JOptionPane.CANCEL_OPTION) {
                         return;
                     }
                }

                DBAccess.freeConnection();
                dailyReport.dispose();
            }
		});

	}

    private void init() {
        SimpleDateFormat sdf = CreamCache.getInstance().getDateFormate();
		if (reportDate == null || reportDate.length() == 0)
			reportDate = sdf.format(new java.util.Date());
		this.setTitle(reportDate + CreamToolkit.GetResource().getString("DailyReport"));

		JPanel buttonsPanel = new JPanel();
		jbPrintPreview = new JButton(CreamToolkit.GetResource().getString("PrintPreview"));
		jbPrintPreview.addActionListener(this);

		jbSave = new JButton(CreamToolkit.GetResource().getString("SaveAndCreateUploadFile"));
		jbSave.addActionListener(this);

		jbExit = new JButton(CreamToolkit.GetResource().getString("Quit"));
		jbExit.addActionListener(this);

		buttonsPanel.add(jbPrintPreview);
		buttonsPanel.add(jbSave);

		buttonsPanel.add(jbExit);
		getContentPane().add(buttonsPanel, BorderLayout.SOUTH);

		JTabbedPane jtpTab = new JTabbedPane(3);

		jtSaleDetail = creatSaleDetailTable();
        jtpTab.add(CreamToolkit.GetResource().getString("SaleDetail"), new JScrollPane(jtSaleDetail));

		jtDiscountInfo = this.creatDiscountInfoTable();
		jtpTab.add(CreamToolkit.GetResource().getString("DiscountInfo"), new JScrollPane(jtDiscountInfo));

		jtReport = this.creatAccountReportTable();
        jtpTab.add(CreamToolkit.GetResource().getString("AccountDailyReport"), new JScrollPane(jtReport));
        this.reCalculate((AccountInfoTableModel)jtReport.getModel());
        
		getContentPane().add(jtpTab, BorderLayout.CENTER);
        
    }

    private void resizeTables() {
        TableColumnModel cm;
        int totalWidth  = this.jtSaleDetail.getParent().getSize().width;
        
        cm = this.jtReport.getColumnModel();
        cm.getColumn(0).setPreferredWidth(100);
        cm.getColumn(1).setPreferredWidth(totalWidth - 200);
        cm.getColumn(2).setPreferredWidth(100);
        
        cm = this.jtSaleDetail.getColumnModel();
        cm.getColumn(0).setPreferredWidth(50);
        cm.getColumn(1).setPreferredWidth(150);
        cm.getColumn(2).setPreferredWidth(150);
        
        cm = this.jtDiscountInfo.getColumnModel();
        cm.getColumn(0).setPreferredWidth(100);
        cm.getColumn(1).setPreferredWidth(totalWidth - 350);
        cm.getColumn(2).setPreferredWidth(150);
        cm.getColumn(3).setPreferredWidth(100);
    }

	//销售明细
	private JTable creatSaleDetailTable() {
		JTable tmpTable = null;
        Vector colVector = new Vector();
		Vector rowVector = getSaleDetail();

		colVector.add(CreamToolkit.GetResource().getString("TransactionNumber"));
		colVector.add(CreamToolkit.GetResource().getString("SaleTime"));
		colVector.add(CreamToolkit.GetResource().getString("ItemNo"));
		colVector.add(CreamToolkit.GetResource().getString("Size"));

		colVector.add(CreamToolkit.GetResource().getString("Color"));
		colVector.add(CreamToolkit.GetResource().getString("UnitPrice"));
		colVector.add(CreamToolkit.GetResource().getString("Quantity"));
		colVector.add(CreamToolkit.GetResource().getString("DiscountNo"));
		colVector.add(CreamToolkit.GetResource().getString("DiscountRate"));
		colVector.add(CreamToolkit.GetResource().getString("Amount"));

		DefaultTableModel dm = new DefaultTableModel() {
			public boolean isCellEditable(int row, int col) {
				return false;
			}

		};
		
        dm.setDataVector(rowVector, colVector);
        tmpTable = new JTable(dm);
		
        DecimalRender decRender = new DecimalRender(decFormatter);
        tmpTable.getColumn(CreamToolkit.GetResource().getString("UnitPrice")).setCellRenderer(decRender);
        tmpTable.getColumn(CreamToolkit.GetResource().getString("Quantity")).setCellRenderer(decRender);
        tmpTable.getColumn(CreamToolkit.GetResource().getString("Amount")).setCellRenderer(decRender);
        
        DecimalRender percentRender = new DecimalRender(percentFormatter);
        tmpTable.getColumn(CreamToolkit.GetResource().getString("DiscountRate")).setCellRenderer(percentRender);
        
        return tmpTable;

	}

	private Vector getSaleDetail() {
		Vector rowVector = new Vector();
		Connection con = dbAccess.getConnection();
		ResultSet rst = null;
		String tmpString = "";
		if (con != null) {

			String sqlSaleDetail =
				"Select d.systemDate, d.pluNumber, d.unitprice, d.originalPrice, "
					+ "d.quantity, d.discountRateID, d.Amount," 
                    + "d.discountRate, p.pluName, p.color, p.size "
					+ "from  posul_trandtl d left join  posdl_plu p "
					+ "on d.pluNumber = p.pluNumber "
					+ "where date_format(systemDate,'%Y-%m-%d') = "
					+ "'" + reportDate + "' " 
                    + "and d.storeID = '" + storeID + "' "
                    + " and itemVoid = '0' and (detailCode = 'S' or detailCode = 'R') "
                    + "order by d.systemDate, d.lineItemSequence" ;
			try {
				rst = con.createStatement().executeQuery(sqlSaleDetail);
				int i = 1;
				HYIDouble totalQty = new HYIDouble(0.00);
                HYIDouble totalAmt = new HYIDouble(0.00);
                
                while (rst.next()) {
					Vector tmpVector = new Vector();

					tmpVector.add(new Integer(i++).toString());
                    
                    java.util.Date sDate = rst.getTimestamp("systemDate");
                    tmpVector.add(new SimpleDateFormat("yyyy/MM/dd hh:mm").format(sDate));
					
                    tmpVector.add(rst.getObject("pluNumber"));
					tmpVector.add(rst.getObject("color"));
					tmpVector.add(rst.getObject("size"));
					tmpVector.add(rst.getBigDecimal("originalPrice"));
					
                    HYIDouble qty = new HYIDouble(rst.getDouble("quantity"));
                    tmpVector.add(qty);
					totalQty = totalQty.addMe(qty);
                    
                    tmpVector.add(rst.getObject("discountRateID"));
					tmpVector.add(rst.getBigDecimal("discountRate"));
					
                    HYIDouble amount = new HYIDouble(rst.getDouble("amount"));
                    
                    tmpVector.add(amount);
                    totalAmt = totalAmt.addMe(amount);
                    
                    
					rowVector.add(tmpVector);

				}
                //增加空白行
                rowVector.add(new Vector(10));
                //增加"合计"行
                Vector totalVector = new Vector(10);
                totalVector.add(CreamToolkit.GetResource().getString("Totalize"));
                totalVector.add("");
                totalVector.add("");
                totalVector.add("");
                totalVector.add("");
                totalVector.add(null);
                totalVector.add(totalQty);
                totalVector.add("");
                totalVector.add(null);
                totalVector.add(totalAmt);
                
                rowVector.add(totalVector); 
              
                rst.close();
				rst = null;
			} catch (SQLException se) {
				se.printStackTrace();
				try {
    				rst.close();
					rst = null;
				} catch (Exception e) {
                    rst = null;
				}
			}
		}

		return rowVector;
	}

	//折扣信息
	private JTable creatDiscountInfoTable() {
        Vector colVector = new Vector();
		Vector rowVector = getDiscountInfo();

		colVector.add(CreamToolkit.GetResource().getString("DiscountNo"));
		colVector.add(CreamToolkit.GetResource().getString("DiscountName"));
		colVector.add(CreamToolkit.GetResource().getString("DiscountRate"));
		colVector.add(CreamToolkit.GetResource().getString("AmountTotalize"));
        
        DefaultTableModel dm = new DefaultTableModel() {
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        
        dm.setDataVector(rowVector, colVector);
        JTable tmpTable = new JTable(dm);

        DecimalRender decRender = new DecimalRender(decFormatter);
        tmpTable.getColumn(CreamToolkit.GetResource().getString("AmountTotalize")).setCellRenderer(decRender);
        
		return tmpTable;
	}

	private Vector getDiscountInfo() {
		Vector rowSet = new Vector();
		Connection con = dbAccess.getConnection();
		ResultSet rst = null;
		String tmpString = "";
		if (con != null) {

			String sqlDiscountInfo =
				"select d.discountRateID, t.disname, sum(d.amount) as amount, "
                + "concat(disuplimit,'--', dislowlimit) as rate "
                + "from posul_trandtl d left join posdl_distype t "
                + "on d.discountRateID = t.disno "
                + "where date_format(d.systemDate,'%Y-%m-%d') = "
                + "'" + reportDate + "' and "
                + "d.storeID = '" + storeID + "' and " 
                + "(d.discountRate is not null and d.discountRate <> 1.00) and "
                + "d.itemvoid = '0' and (d.detailcode = 'R' or d.detailCode = 'S') "
                + "group by d.discountRateID, t.disname "
                + "order by d.discountRateID";

			try {
                rst = con.createStatement().executeQuery(sqlDiscountInfo);
                HYIDouble totalAmount = new HYIDouble(0.00);
                
                Vector otherVector = new Vector(4);
                while (rst.next()) {
                    String rateID = rst.getString("discountRateID");
                    HYIDouble amt = new HYIDouble(rst.getDouble("amount"));
                    if (rateID == null || rateID.equals("")) { 
                        otherVector.add(CreamToolkit.GetResource().getString("Other"));
                        otherVector.add(rst.getString("disname"));
                        otherVector.add(rst.getString("rate"));
                        otherVector.add(amt);
                    } else {
                        Vector tmpVector = new Vector(4);
                        tmpVector.add(rateID);
                        tmpVector.add(rst.getString("disname"));
                        tmpVector.add(rst.getString("rate"));
                        tmpVector.add(amt);
                        rowSet.add(tmpVector);
                    }
                    totalAmount = totalAmount.addMe(amt);    
                }
                
                rowSet.add(otherVector);
                
                //增加一行空白
                rowSet.add(new Vector(4));
                
                //增加"合计"行
                Vector totalVector = new Vector(4);                 
                totalVector.add(CreamToolkit.GetResource().getString("Total"));
                totalVector.add("");
                totalVector.add("");
                totalVector.add(totalAmount);
                
                rowSet.add(totalVector);
                
                rst.close();
                rst = null;

			} catch (SQLException se) {
				se.printStackTrace();
				try {
    				rst.close();
					rst = null;
				} catch (Exception e) {
                    rst = null;
				}
			}
		}
		return rowSet;
	}

	//  折扣信息
	private JTable creatAccountReportTable() {
        //设置可修改的科目
        final HashSet editableSet = new HashSet();
        editableSet.add("1003");
        editableSet.add("1005");
        editableSet.add("1007");
        editableSet.add("1097");
        editableSet.add("1098");
        editableSet.add("1099");
                        
		Vector colVector = new Vector();
		Vector rowVector = getAccountInfo();
		colVector.add(CreamToolkit.GetResource().getString("AccountSubject"));
		colVector.add(CreamToolkit.GetResource().getString("AccountName"));
		colVector.add(CreamToolkit.GetResource().getString("Amount"));

        AccountInfoTableModel dm = new AccountInfoTableModel();
        dm.setEditableSet(editableSet);
                        
        dm.setDataVector(rowVector, colVector);        

		JTable tmpTable = new JTable(dm) {
            public void editingStopped(ChangeEvent e) {
              isEdited = true;
              super.editingStopped(e);
              if (getEditorComponent() instanceof JComboBox) {
                  System.out.println("jcombobox");
                  JComboBox jbo = (JComboBox)getEditorComponent();
                  jbo.setSelectedIndex(((HYIDouble)getCellEditor().getCellEditorValue()).intValue());   
              }
              reCalculate((AccountInfoTableModel)getModel());
//              System.out.println("after recalculate");
              repaint();
            }
            
		};
		tmpTable.setRowHeight(20);
        
        //天气列表
        JComboBox jboWeather = getWeatherComboBox();
            
        EachRowRenderer  rowRenderer = new EachRowRenderer();
//        DecimalRender decRender = new DecimalRender(decFormatter);

        int j = -1;
        for (int i = 0; i < rowVector.size(); i++) {
            if (((Vector)rowVector.get(i)).get(0).equals("1097")) {
                j = i;
                rowRenderer.add(i, new ComboBoxRender(jboWeather));
            } else 
                rowRenderer.add(i, new DecimalRender(decFormatter));    
        }

        EachRowEditor rowEditor = new EachRowEditor(tmpTable);
        DefaultCellEditor conboBoxEditor = new DefaultCellEditor(jboWeather);
        if (j != -1)
            rowEditor.setEditorAt(j,conboBoxEditor);

        tmpTable.getColumn(CreamToolkit.GetResource().getString("Amount")).setCellEditor(rowEditor);

        tmpTable.getColumn(CreamToolkit.GetResource().getString("Amount")).setCellRenderer(rowRenderer);

        return tmpTable;
	}

    //把天气配置文件中的内容加到JComboBox中
    private JComboBox getWeatherComboBox() {
        Vector list = new Vector();
        try {
            BufferedReader bf = new BufferedReader(new FileReader(
            	new File(CreamToolkit.getConfigDir() + "weather.conf")));
            String line;
            while ((line = bf.readLine()) != null) {
                if (!line.trim().equals(""))
                    list.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        JComboBox tmpCBO = new JComboBox(list);
        tmpCBO.setFont(new Font(CreamToolkit.GetResource().getString("Song"), Font.PLAIN, 12));

        return tmpCBO;
    }
    
	private Vector getAccountInfo() {
		Vector rowSet = new Vector();
        Connection con = dbAccess.getConnection();
        ResultSet rst = null;
        HYIDouble storeAmount = new HYIDouble(0.00);
        if (con != null) {
            String sqlAccountInfo =
                "select accountNumber, accountName, storeAmount " 
               + "from accdayrpt where storeID = '" + storeID + "' and "
               + "date_format(accountDate,'%Y-%m-%d') = '" + reportDate + "' "
               + "order by displaySequence";
            
            try {
                rst = con.createStatement().executeQuery(sqlAccountInfo);
                while (rst.next()) {
                    Vector tmpVector = new Vector(4);
                    String accountNumber = (String)rst.getObject("accountNumber");
                    tmpVector.add(accountNumber);
                    
                    tmpVector.add(rst.getObject("accountName"));
                    
                    storeAmount.reset(rst.getDouble("storeAmount"));
                    //如果昨日实际留存周转金为0　取前一天的"本日实际留存周转金"
                    if (accountNumber.equals("1001") &&
                        storeAmount.compareTo(new HYIDouble(0.00)) < 1) {
                        storeAmount = getAmountFromYesterday(storeID,reportDate,"1005");
                        tmpVector.add(storeAmount);
                    } else if (accountNumber.equals("1097")) {
                        JComboBox jb = this.getWeatherComboBox();
                        jb.setSelectedIndex(storeAmount.intValue() - 1);
                        tmpVector.add(jb);
                    } else 
                        tmpVector.add(storeAmount);
                        
                    rowSet.add(tmpVector);
                }
            } catch (SQLException se) {
                se.printStackTrace();
                try {
                    rst.close();
                    rst = null;
                } catch (Exception e) {
                    rst = null;
                }
            }             
        }

		return rowSet;
	}

    public HYIDouble getAmountFromYesterday(String storeID, String baseDate, String accountID) {
        java.sql.Date tmpDate = java.sql.Date.valueOf(baseDate);

        long tmpTime = tmpDate.getTime() - 1000*3600*24;
        String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(tmpTime));
        
        Connection con = dbAccess.getConnection();
        ResultSet rst = null;
        
        HYIDouble storeAmount = new HYIDouble(0.00);
        
        if (con != null) {
            String sql = "select storeAmount from accdayrpt where storeID = '" + storeID + "' and "
                       + "accountDate = '" + yesterday + "' and accountNumber = '" + accountID + "'";
            try {
                rst = con.createStatement().executeQuery(sql);
                if (rst.next()) {
                    storeAmount.reset(rst.getDouble("storeAmount")); 
                }
                
                rst.close();
                rst = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return storeAmount;
    }

    public void reCalculate(AccountInfoTableModel tm) {
       
        //本日实际销售收入
        HYIDouble amt006 = (HYIDouble)tm.getValueAt(tm.getRowIndexOfAccountNo("006"),tm.COLUMN_AMOUNT);

        //支票
        HYIDouble amt501 = (HYIDouble)tm.getValueAt(tm.getRowIndexOfAccountNo("501"),tm.COLUMN_AMOUNT);
        //礼券
        HYIDouble amt502 = (HYIDouble)tm.getValueAt(tm.getRowIndexOfAccountNo("502"),tm.COLUMN_AMOUNT);
        //其他
        HYIDouble amt599 = (HYIDouble)tm.getValueAt(tm.getRowIndexOfAccountNo("599"),tm.COLUMN_AMOUNT);
        //昨日留存周转金
        HYIDouble amt1001 = (HYIDouble)tm.getValueAt(tm.getRowIndexOfAccountNo("1001"),tm.COLUMN_AMOUNT);
        accountItemAndAmount.put("1001",amt1001);
  
        //本日应有现金小计 = 本日实际销售收入－支票－礼券－其他
        HYIDouble amt1001B = amt006.subtract(amt501).subtract(amt502).subtract(amt599);
        accountItemAndAmount.put("1001B",amt1001B);
        tm.setValueAt(amt1001B, tm.getRowIndexOfAccountNo("1001B"),tm.COLUMN_AMOUNT);
        
        //累计应有现金小计
        HYIDouble amt1002 = new HYIDouble(0);
        amt1002 = amt1002.addMe(amt1001).addMe(amt1001B);
        accountItemAndAmount.put("1002",amt1002);
        tm.setValueAt(amt1002, tm.getRowIndexOfAccountNo("1002"),tm.COLUMN_AMOUNT);

        //本日银行汇款额
        HYIDouble amt1003 = (HYIDouble)tm.getValueAt(tm.getRowIndexOfAccountNo("1003"),tm.COLUMN_AMOUNT);
        accountItemAndAmount.put("1003",amt1003);
        
        //本日应有周转金
        HYIDouble amt1004 = amt1002.subtract(amt1003);
        accountItemAndAmount.put("1004", amt1004);
        tm.setValueAt(amt1004, tm.getRowIndexOfAccountNo("1004"),tm.COLUMN_AMOUNT);
        
        //本日实际留存周转金
        HYIDouble amt1005 = (HYIDouble)tm.getValueAt(tm.getRowIndexOfAccountNo("1005"),tm.COLUMN_AMOUNT);
        accountItemAndAmount.put("1005",amt1005);
       
        //本日现金短溢 = 本日实际留存周转金 - 本日应有周转金
        HYIDouble amt1006 = amt1005.subtract(amt1004);
        accountItemAndAmount.put("1006",amt1006);
        tm.setValueAt(amt1006, tm.getRowIndexOfAccountNo("1006"),tm.COLUMN_AMOUNT);
        
        //本日增减周转金
        HYIDouble amt1007 = (HYIDouble)tm.getValueAt(tm.getRowIndexOfAccountNo("1007"),tm.COLUMN_AMOUNT);
        accountItemAndAmount.put("1007",amt1007);
        //天气
        HYIDouble amt1097 = (HYIDouble)tm.getValueAt(tm.getRowIndexOfAccountNo("1097"),tm.COLUMN_AMOUNT);
        accountItemAndAmount.put("1097",amt1097);
        
        //最高温度
        HYIDouble amt1098 = (HYIDouble)tm.getValueAt(tm.getRowIndexOfAccountNo("1098"),tm.COLUMN_AMOUNT);
        accountItemAndAmount.put("1098",amt1098);
        
        //最低温度
        HYIDouble amt1099 = (HYIDouble)tm.getValueAt(tm.getRowIndexOfAccountNo("1099"),tm.COLUMN_AMOUNT);
        accountItemAndAmount.put("1099",amt1099);
        
    }
    
    private PreviewView previewer = null;
    private StyleSheet report = null;

    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource().equals(jbExit)) {
            if (this.isEdited) {
                int r = JOptionPane.showConfirmDialog(
                                (Component)this,
                                CreamToolkit.GetResource().getString("IsSaveRejigger"),
                                CreamToolkit.GetResource().getString("Prompt"),
                                JOptionPane.YES_NO_CANCEL_OPTION
                                ); 
                 if (r == JOptionPane.YES_OPTION) {
                     saveAccDayRpt();               
                 } else if (r == JOptionPane.CANCEL_OPTION) {
                     return;
                 } else {
                     
                 }
            }
            
            DBAccess.freeConnection();
            if (previewer != null) {
                previewer.setVisible(false);
                report = null;
                previewer = null;
            }
            this.dispose();    
            
		} else if (e.getSource().equals(jbPrintPreview)) {
            if (this.isEdited)  {
                saveAccDayRpt();
                this.isEdited = false;
            }
               
            report = createReport();
			previewer = Previewer.createPreviewer();
			
            previewer.setOrientation(StyleConstants.LANDSCAPE);
            previewer.setPageWidth(StyleConstants.PAPER_LETTER.width);
            previewer.setPageHeight(StyleConstants.PAPER_LETTER.height);
			previewer.pack();
            previewer.print(report);
			previewer.setVisible(true);

		} else if (e.getSource().equals(jbSave)) {
            if (this.isEdited) 
                saveAccDayRpt();    

            String fileDir = (String)prop.getProperty("LocalUploadDir");
            exportDataAndZip(fileDir);
            
            this.isEdited = false;
            repaint();
        }
	}

	private void saveAccDayRpt() {
        Connection con = dbAccess.getConnection();
        String sql = "";
        try {
            Statement stmt = con.createStatement();
            Iterator itr = accountItemAndAmount.keySet().iterator();

            while (itr.hasNext()) {
                String accountNumber = (String)itr.next();
                sql = "update accdayrpt set storeAmount = " + accountItemAndAmount.get(accountNumber)            
                    + " where storeID = '" + storeID + "' and accountDate = '" + reportDate + "' and "
                    + "accountNumber = '" + accountNumber + "'";
     
                stmt.executeUpdate(sql);
             }
        
            stmt.close();
            stmt = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
    
    private void exportDataAndZip(String dir) {
        Hashtable fileList = new Hashtable();
        Hashtable cmdLine = new Hashtable();
        
        if (!dir.endsWith((String)System.getProperty("file.separator")))
            dir += (String)System.getProperty("file.separator");

        String mysqlBase = (String)prop.get("MysqlBase");
        String dataBaseURL = (String)prop.get("ServerDatabaseURL");
        String db = null;

        if (dataBaseURL != null && dataBaseURL.length() >0 )
            db = dataBaseURL.substring(dataBaseURL.lastIndexOf("/") + 1);
        else
            db = "cake";
        
        fileList.put("accdayrpt", "accdayrpt.sql");
        fileList.put("posul_tranhead", "posul_tranhead.sql");
        fileList.put("posul_trandtl", "posul_trandtl.sql");
        fileList.put("posul_inventory","posul_inventory.sql");    
        
        if (mysqlBase != null && mysqlBase.length() > 0) {
            if (!mysqlBase.endsWith((String)System.getProperty("file.separator")))
                mysqlBase += (String)System.getProperty("file.separator");
                    
            String command = "";
            String beginTime = reportDate + " 00:00:01";
            String endTime = reportDate + " 23:59:59";

            Iterator itr = fileList.keySet().iterator();
                
            while(itr.hasNext()) {
                String tmp = (String)itr.next();
                if (tmp.equals("accdayrpt"))
                command = mysqlBase + "mysqldump -t -c --where=\"accountDate='" 
                                + reportDate + "'\" " + db + " " + tmp;
                else
                  command = mysqlBase + "mysqldump -t -c --where=\"systemDate between '" 
                          + beginTime + "' and '" + endTime + "'\" " 
                          + db + " " + tmp; 
//                System.out.println(command);
                //文件名，命令行
                cmdLine.put(fileList.get(tmp),command);
            }
                
            String compressFileName = dir + storeID + "_" + new SimpleDateFormat("yyyyMMdd").format(java.sql.Date.valueOf(reportDate)) + ".zip";
            File f = new File(compressFileName);
            if (f.exists()) 
                f.delete();
            try {    
                FileOutputStream fout = new FileOutputStream(f);
                ZipOutputStream zipOut = new ZipOutputStream(fout);
                
                Iterator cmdIter = cmdLine.keySet().iterator();      
                while (cmdIter.hasNext()) {
                    String exportFileName = (String)cmdIter.next();   
//                 System.out.println("cmd " + (String)cmdLine.get(exportFileName));                      
                    Process pro = Runtime.getRuntime().exec((String)cmdLine.get(exportFileName));    
                    InputStream in = pro.getInputStream();
                    BufferedReader fin = new BufferedReader(new InputStreamReader(in));
                    
                    ZipEntry zipEntry = new ZipEntry(exportFileName);
                    zipOut.putNextEntry(zipEntry);
                    
                    String line ;
                    while ((line = fin.readLine()) != null) {
                        if (!line.startsWith("#") && line.length() > 0) 
                            zipOut.write((line + "\n").getBytes());
                    }
                    fin.close();
                    in.close();
                    zipOut.closeEntry();  
                }
                zipOut.close();
                fout.close();
            
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
               
        } else {
            System.out.println("Error -->> MysqlBase is null ");
        }
    }
    
    
    private StyleSheet createReport() {
		StyleSheet report = new StyleSheet();

		// set header and footer
		String titlestr = this.getTitle();
		report.setCurrentAlignment(StyleSheet.H_LEFT);
		report.addFooterText(CreamToolkit.GetResource().getString("StoreMaster"));
		report.setCurrentAlignment(StyleSheet.H_RIGHT);
		report.addFooterText(CreamToolkit.GetResource().getString("Director"));

		report.setCurrentAlignment(StyleSheet.H_CENTER);
		report.setCurrentFont(new Font("Serif", Font.BOLD, 16));
		report.addText(titlestr);
		report.setCurrentFont(new Font("Serif", Font.PLAIN, 10));
		report.addNewline(2);

		report.setCurrentAlignment(StyleSheet.H_LEFT);
		report.addText(CreamToolkit.GetResource().getString("Store")+": " + storeID + "  " + storeName);

		report.setCurrentAlignment(StyleSheet.H_RIGHT);
		report.addText(CreamToolkit.GetResource().getString("CreateTableTime")+": " + new SimpleDateFormat("yyyy/MM/dd hh:mm").format(new java.util.Date()));

		report.addNewline(1);

		report.setCurrentAlignment(StyleSheet.H_LEFT);
		report.addText(CreamToolkit.GetResource().getString("Date")+": " + reportDate);

		report.setCurrentAlignment(StyleSheet.H_RIGHT);
		report.addText(CreamToolkit.GetResource().getString("CreateTableMan")+": " + salePersonID + " " + salePersonName);

		report.addNewline(2);

		JTableLens lens = new JTableLens(jtSaleDetail);
		lens.setColAutoSize(true);
        lens.setRowAutoSize(true);
        lens.setFont(report.getCurrentFont());
        report.addTable(lens);

		report.addNewline(1);
		JTableLens discountLens = new JTableLens(jtDiscountInfo);
        discountLens.setColAutoSize(true);
        discountLens.setRowAutoSize(true);
        discountLens.setFont(report.getCurrentFont());
		report.addTable(discountLens);

		report.addNewline(1);
		JTableLens accountLens = new JTableLens(jtReport);
        accountLens.setColAutoSize(true);
        accountLens.setRowAutoSize(true);
        accountLens.setFont(report.getCurrentFont());
		report.addTable(accountLens);

		return report;
	}

}

class AccountInfoTableModel extends DefaultTableModel {
    final int COLUMN_AMOUNT = 2;
    final int COLUMN_NO = 0;
    private HashSet editableSet = new HashSet();
    
    public boolean isCellEditable(int row, int col) {
        if (col == COLUMN_AMOUNT) {
            Vector tmpVector = (Vector)dataVector.get(row);
             return editableSet.contains(tmpVector.get(COLUMN_NO));
        }
        return false;
    }

    
    public void setValueAt(Object value, int row, int col) {
        Vector rowVector = (Vector)dataVector.elementAt(row);
        if (col == COLUMN_AMOUNT) {   
           if(value instanceof HYIDouble) {
               rowVector.setElementAt((HYIDouble)value, col);
           } else if (value instanceof String) {
               HYIDouble d = new HYIDouble((String)value);    
               rowVector.setElementAt(d, col);
           }
        }
    }
    
    public Object getValueAt(int row, int column) {
        Vector rowVector = (Vector)dataVector.elementAt(row);
        Object obj = rowVector.elementAt(column); 
        if (obj instanceof JComboBox) {
            return new HYIDouble(((JComboBox)obj).getSelectedIndex() + 1);
        }
                
        return rowVector.elementAt(column);
    }
    
    public void setEditableSet(HashSet s) {
        this.editableSet = new HashSet(s);
    }
            
    public int getRowIndexOfAccountNo(String accountNo) {
        for (int i = 0; i < dataVector.size(); i++) {
            Vector rowVector = (Vector)dataVector.elementAt(i);
            if(((String)rowVector.get(0)).equals(accountNo))
                return i;    
        }
                
        return -1;
    }

    public void moveColumn(int column, int targetColumn) {
    
    }
}
