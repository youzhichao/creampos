/*
 * Created on 2003-8-8
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.ereport.table;

import java.math.*;
import java.awt.*;
import javax.swing.table.*;
import javax.swing.*;
import java.text.*;
import hyi.cream.util.*;

/**
 * @author Administrator
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

public class DecimalRender extends DefaultTableCellRenderer {
    DecimalFormat  formatter;
    private Color unselectedForeground; 
    private Color unselectedBackground; 
    
    public DecimalRender(String pattern) {
        this(new DecimalFormat(pattern));
    }
  
    public DecimalRender(DecimalFormat formatter) {
        this.formatter = formatter;
        setHorizontalAlignment(JLabel.RIGHT);
    }
  
    public void setValue(Object value) { 
        setText((value == null) ? ""
            : formatter.format(((HYIDouble)value).doubleValue())); 
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value,
                          boolean isSelected, boolean hasFocus, int row, int column) {

    if (isSelected) {
       super.setForeground(table.getSelectionForeground());
       super.setBackground(table.getSelectionBackground());
    }
    else {
        super.setForeground((unselectedForeground != null) ? unselectedForeground 
                                                           : table.getForeground());
        super.setBackground((unselectedBackground != null) ? unselectedBackground 
                                                           : table.getBackground());
    }
    
    Font f = table.getFont();
    if (table.getModel().isCellEditable(row,column)) {
        setForeground(Color.blue);
        f = new Font(f.getFontName(),Font.BOLD, f.getSize());
    }
    
    setFont(f);
    
    if (hasFocus) {
        setBorder( UIManager.getBorder("Table.focusCellHighlightBorder") );
        if (table.isCellEditable(row, column)) {
            super.setForeground( UIManager.getColor("Table.focusCellForeground") );
            super.setBackground( UIManager.getColor("Table.focusCellBackground") );
        }
    } else {
        setBorder(noFocusBorder);
    }

        setValue(value); 

    // ---- begin optimization to avoid painting background ----
    Color back = getBackground();
    boolean colorMatch = (back != null) && ( back.equals(table.getBackground()) ) && table.isOpaque();
        setOpaque(!colorMatch);
    // ---- end optimization to aviod painting background ----

    return this;
    }

}
