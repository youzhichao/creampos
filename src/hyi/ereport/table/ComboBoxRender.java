/*
 * Created on 2003-8-27
 */
package hyi.ereport.table;

import java.awt.*;
import javax.swing.*;
import java.math.*;
import javax.swing.table.*;
import hyi.cream.util.*;

/**
 * @author Administrator
 *
 */
public class ComboBoxRender extends JComboBox implements TableCellRenderer {
	/**
	 * 
	 */
	public ComboBoxRender(JComboBox jcb) {
        super(jcb.getModel());

	}

    public Component getTableCellRendererComponent(JTable table, Object value,
                           boolean isSelected, boolean hasFocus,
                            int row, int column) {
      if (isSelected) {
        setForeground(table.getSelectionForeground());
        setBackground(table.getSelectionBackground());
      } else {
        setForeground(table.getForeground());
        setBackground(table.getBackground());
      }

      Font f = table.getFont();
      if (table.getModel().isCellEditable(row,column)) {
          setForeground(Color.blue);
      }

      setFont(f);
      
      HYIDouble b = new HYIDouble(0);
      if (value != null) b = (HYIDouble)value; 
      
      if (this.getItemCount() > 0)
        setSelectedIndex(b.intValue() - 1);  
      return this;
    }
 
}
