package hyi.jpos.services;

import hyi.cream.util.CreamToolkit;

public class CashDrawer7000Local {

	public native static char openCashDrawer(int no);

	public native static int getDrawerState(int no);

	static {
		try {
			System.loadLibrary("cashdrawer7000");
		} catch (Exception e) {
			e.printStackTrace(CreamToolkit.getLogger());
		}
	}
}
