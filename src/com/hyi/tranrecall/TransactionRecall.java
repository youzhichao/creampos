package com.hyi.tranrecall;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class TransactionRecall
{
    private static final String NAME = "TransactionRecall";
    private static final String VERSION = "20040603.01";
    private static final String TITLE = "[TransactionRecall]";
    private static Logger log;
    public static final String TRECALL_HOME_DIR = ".";
    public static final String TRECALL_LOG__DIR;
    public static final String TRECALL_CONF__DIR = TRECALL_HOME_DIR + File.separator + "conf";
    public static final String TRECALL_CONF_APP__FILE;
    public static final String TRECALL_CONF_LOG__FILE;
    public static final String TRECALL_CONF_TRANDTL_FILE;
    public static final String TRECALL_CONF_TRANHEAD_FILE;
    public static final String TRECALL_CONF_ALIPAYDTL_FILE;
    public static final String TRECALL_CONF_WEIXINDTL_FILE;
    public static final String TRECALL_CONF_UNIONPAYDTL_FILE;
    public static final String TRECALL_CONF_CMPAYDTL_FILE;
    private String companyTitle = "\n            +--------------------------------------------+" +
                                  "\n            +          历史交易数据回复工具(SC->POS)     +" +
                                  "\n            +                    Ver 1.0                 +" +
                                  "\n            +          Hongyuan Software(Shanghai)Co.    +" +
                                  "\n            +                Copyright (c) 2004          +" +
                                  "\n            +--------------------------------------------+\n";

    private static void init()
    {
        File localFile = new File(TRECALL_HOME_DIR);
        if ((!(localFile.exists())) || (!(localFile.isDirectory())))
            localFile.mkdirs();
        localFile = new File(TRECALL_CONF__DIR);
        if ((!(localFile.exists())) || (!(localFile.isDirectory())))
            localFile.mkdirs();
        localFile = new File(TRECALL_LOG__DIR);
        if ((!(localFile.exists())) || (!(localFile.isDirectory())))
            localFile.mkdirs();
        localFile = new File(TRECALL_CONF_APP__FILE);
        if ((!(localFile.exists())) || (!(localFile.isFile())))
        {
            System.out.println("Config file: " + TRECALL_CONF_LOG__FILE + " doesn't exit!");
            System.exit(0);
        }
        localFile = new File(TRECALL_CONF_LOG__FILE);
        if ((!(localFile.exists())) || (!(localFile.isFile())))
        {
            System.out.println("Config file: " + TRECALL_CONF_LOG__FILE + " doesn't exit!");
            System.exit(0);
        }
        localFile = new File(TRECALL_CONF_TRANHEAD_FILE);
        if ((!(localFile.exists())) || (!(localFile.isFile())))
        {
            System.out.println("Config file: " + TRECALL_CONF_TRANHEAD_FILE + " doesn't exit!");
            System.exit(0);
        }
        localFile = new File(TRECALL_CONF_TRANDTL_FILE);
        if ((!(localFile.exists())) || (!(localFile.isFile())))
        {
            System.out.println("Config file: " + TRECALL_CONF_TRANDTL_FILE + " doesn't exit!");
            System.exit(0);
        }
        localFile = new File(TRECALL_CONF_ALIPAYDTL_FILE);
        if ((!(localFile.exists())) || (!(localFile.isFile())))
        {
            System.out.println("Config file: " + TRECALL_CONF_ALIPAYDTL_FILE + " doesn't exit!");
            System.exit(0);
        }
        localFile = new File(TRECALL_CONF_WEIXINDTL_FILE);
        if ((!(localFile.exists())) || (!(localFile.isFile())))
        {
            System.out.println("Config file: " + TRECALL_CONF_WEIXINDTL_FILE + " doesn't exit!");
            System.exit(0);
        }
        localFile = new File(TRECALL_CONF_UNIONPAYDTL_FILE);
        if ((!(localFile.exists())) || (!(localFile.isFile())))
        {
            System.out.println("Config file: " + TRECALL_CONF_UNIONPAYDTL_FILE + " doesn't exit!");
            System.exit(0);
        }
        localFile = new File(TRECALL_CONF_CMPAYDTL_FILE);
        if ((!(localFile.exists())) || (!(localFile.isFile())))
        {
            System.out.println("Config file: " + TRECALL_CONF_CMPAYDTL_FILE + " doesn't exit!");
            System.exit(0);
        }
    }

    public static void main(String[] paramArrayOfString)
    {
        TransactionRecall localTransactionRecall = new TransactionRecall();
        if (paramArrayOfString.length == 0)
            localTransactionRecall.user();
        else
            localTransactionRecall.doDateUpdate(paramArrayOfString);
    }

    private void user()
    {
        String[] arrayOfString = null;
        int i = 0;
        int j = 0;
        System.out.println(this.companyTitle);
        System.out.println("退出: exit");
        System.out.println("根据时间范围恢复数据，选 1");
        System.out.println("根据交易序号恢复数据，选 2");
        System.out.print("请选择 >");
        String str = read();
        while (true)
        {
            while (str.equalsIgnoreCase("exit"))
                System.exit(0);
            if (str.equals("1"))
            {
                i = 1;
                break;
            }
            if (str.equals("2"))
            {
                j = 1;
                break;
            }
            System.out.println("错误的序号,请重新输入:");
            System.out.print(">");
            str = read();
        }
        if (i == 1)
        {
            System.out.print("输入POS机号(x or x-y):");
            arrayOfString = new String[6];
            arrayOfString[0] = "-1";
            str = read();
            while (true)
            {
                while (str.equalsIgnoreCase("exit"))
                    System.exit(0);
                if (Recall.validatePosNo(str))
                {
                    arrayOfString[1] = str;
                    break;
                }
                System.out.print("错误的POS机号,请重新输入:");
                str = read();
            }
            System.out.print("输入开始日期时间(yyyy-MM-dd HH:mm:ss):");
            str = read();
            while (true)
            {
                while (str.equalsIgnoreCase("exit"))
                    System.exit(0);
                if (Recall.validateDate(str))
                {
                    arrayOfString[2] = str.substring(0, str.indexOf(" "));
                    arrayOfString[3] = str.substring(str.indexOf(" ") + 1);
                    break;
                }
                System.out.print("错误的日期格式,请重新输入:");
                str = read();
            }
            System.out.print("输入结束日期时间(yyyy-MM-dd HH:mm:ss):");
            str = read();
            while (true)
            {
                while (str.equalsIgnoreCase("exit"))
                    System.exit(0);
                if (Recall.validateDate(str))
                {
                    arrayOfString[4] = str.substring(0, str.indexOf(" "));
                    arrayOfString[5] = str.substring(str.indexOf(" ") + 1);
                    break;
                }
                System.out.print("错误的日期格式,请重新输入:");
                str = read();
            }
        }
        if (j == 1)
        {
            System.out.print("输入POS机号(n):");
            arrayOfString = new String[4];
            arrayOfString[0] = "-2";
            str = read();
            while (true)
            {
                while (str.equalsIgnoreCase("exit"))
                    System.exit(0);
                if (Recall.validateInt(str))
                {
                    arrayOfString[1] = str;
                    break;
                }
                System.out.print("错误的POS机号,请重新输入:");
                str = read();
            }
            System.out.print("输入开始交易序号:");
            str = read();
            while (true)
            {
                while (str.equalsIgnoreCase("exit"))
                    System.exit(0);
                if (Recall.validateInt(str))
                {
                    arrayOfString[2] = str;
                    break;
                }
                System.out.print("错误的交易序号,请重新输入:");
                str = read();
            }
            System.out.print("输入结束交易序号:");
            str = read();
            while (true)
            {
                while (str.equalsIgnoreCase("exit"))
                    System.exit(0);
                if (Recall.validateInt(str))
                {
                    arrayOfString[3] = str;
                    break;
                }
                System.out.print("错误的交易序号,请重新输入:");
                str = read();
            }
        }
        Recall localRecall = new Recall();
        localRecall.run(arrayOfString);
    }

    private String read()
    {
        BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String str = "";
        try
        {
            str = localBufferedReader.readLine();
        }
        catch (IOException localIOException)
        {
            localIOException.printStackTrace();
        }
        return str;
    }

    private void doDateUpdate(String[] paramArrayOfString)
    {
        System.out.println(this.companyTitle);
        if ((paramArrayOfString[0].equalsIgnoreCase("-help")) || (paramArrayOfString[0].equalsIgnoreCase("-?")) || (paramArrayOfString[0].equalsIgnoreCase("-h")))
        {
            doHelp();
        }
        else if (paramArrayOfString[0].equalsIgnoreCase("-v"))
        {
            log.info("Version: 20040603.01");
        }
        else if ((paramArrayOfString[0].equalsIgnoreCase("-1")) || (paramArrayOfString[0].equalsIgnoreCase("-2")))
        {
            Recall localRecall = new Recall();
            localRecall.run(paramArrayOfString);
        }
        else
        {
            log.info("Error [-options], please see help");
        }
    }

    private void doHelp()
    {
        String str = "Usage: java -jar TranRecall.jar [-options] [args...]   \n" +
                "\nwhere options include:" +
                "\n  -? -h -help         print this help message           " +
                "\n  -v                  print product version and exit    " +
                "\n  -1 [pos_no] [begin_date_time] [end_date_time]         " +
                "\n                      recall by transaction's time.     " +
                "\n  -2 [pos_no] [begin_tran_no]   [end_tran_no]           " +
                "\n                      recall by transaction's number.   " +
                "\n\n  examples :                                             " +
                "\n    java -jar TranRecall.jar -1 1 2003-11-25 05:59:56 2003-11-25 07:11:34" +
                "\n    java -jar TranRecall.jar -2 1 16 25                " +
                "\n    java -jar TranRecall.jar -1 1-3 2003-11-25 05:59:56 2003-11-25 07:11:34" +
                "\n\nNo options will enter into user interface." +
                "\n\n  examples :                                             " +
                "\n    java -jar TranRecall.jar    " +
                "\n\n";
        System.out.println(str);
    }

    static
    {
        TRECALL_LOG__DIR = TRECALL_HOME_DIR + File.separator + "log";
        TRECALL_CONF_APP__FILE = TRECALL_CONF__DIR + File.separator + "app.properties";
        TRECALL_CONF_LOG__FILE = TRECALL_CONF__DIR + File.separator + "log4j.properties";
        TRECALL_CONF_ALIPAYDTL_FILE = TRECALL_CONF__DIR + File.separator + "alipaydtl.conf";
        TRECALL_CONF_WEIXINDTL_FILE = TRECALL_CONF__DIR + File.separator + "weixindtl.conf";
        TRECALL_CONF_UNIONPAYDTL_FILE = TRECALL_CONF__DIR + File.separator + "unionpaydtl.conf";
        TRECALL_CONF_CMPAYDTL_FILE = TRECALL_CONF__DIR + File.separator + "cmpaydtl.conf";
        TRECALL_CONF_TRANDTL_FILE = TRECALL_CONF__DIR + File.separator + "trandtl.conf";
        TRECALL_CONF_TRANHEAD_FILE = TRECALL_CONF__DIR + File.separator + "tranhead.conf";
        init();
        try
        {
            PropertyConfigurator.configure(TRECALL_CONF_LOG__FILE);
        }
        catch (Exception localException)
        {
            BasicConfigurator.configure();
        }
        log = Logger.getLogger(TransactionRecall.class.getName());
        log.info("################## BEGIN ##################");
        log.info("TRECALL_CONF_LOG__FILE: " + TRECALL_CONF_LOG__FILE);
        log.info("TRECALL_CONF_TRANHEAD_FILE: " + TRECALL_CONF_TRANHEAD_FILE);
        log.info("TRECALL_CONF_TRANDTL_FILE: " + TRECALL_CONF_TRANDTL_FILE);
        log.info("TRECALL_CONF_TRANDTL_FILE: " + TRECALL_CONF_TRANDTL_FILE);
        Util.loadProperties();
    }
}