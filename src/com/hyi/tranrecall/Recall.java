package com.hyi.tranrecall;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;

public class Recall
{
    private static Logger log = Logger.getLogger(Recall.class.getName());
    private HashMap headFieldMap = new HashMap();
    private HashMap dtlFieldMap = new HashMap();
    private HashMap alipayDtlFieldMap = new HashMap();
    private HashMap weixinDtlFieldMap = new HashMap();
    private HashMap unionpayDtlFieldMap = new HashMap();
    private HashMap cmpayDtlFieldMap = new HashMap();

    public Recall()
    {
        this.headFieldMap = Util.headFieldMap;
        this.dtlFieldMap = Util.dtlFieldMap;
        this.alipayDtlFieldMap = Util.alipayDtlFieldMap;
        this.weixinDtlFieldMap = Util.weixinDtlFieldMap;
        this.unionpayDtlFieldMap = Util.unionpayDtlFieldMap;
        this.cmpayDtlFieldMap = Util.cmpayDtlFieldMap;
    }

    public void run(String[] paramArrayOfString)
    {
        String str1;
        String str2;
        String str3;
        if ((paramArrayOfString.length != 4) && (paramArrayOfString.length != 6))
        {
            System.out.println("你输入的参数数量错误，请参照帮助...");
            return;
        }
        for (int i = 0; i < paramArrayOfString.length; ++i)
            log.info("args[" + i + "]=" + paramArrayOfString[i]);
        if (paramArrayOfString[0].equalsIgnoreCase("-1"))
        {
            str1 = paramArrayOfString[1];
            str2 = paramArrayOfString[2] + " " + paramArrayOfString[3];
            str3 = paramArrayOfString[4] + " " + paramArrayOfString[5];
            judgePosNo(str1);
            judgeDate(str2);
            judgeDate(str3);
            recallByTime(str1, str2, str3);
        }
        else if (paramArrayOfString[0].equalsIgnoreCase("-2"))
        {
            str1 = paramArrayOfString[1];
            str2 = paramArrayOfString[2];
            str3 = paramArrayOfString[3];
            if (!(validateInt(str1)))
            {
                System.out.println("错误的POS机号:'" + str1 + "' !");
                System.out.println("POS机号格式应为1位整数");
                System.exit(0);
            }
            judgeTranNo(str2);
            judgeTranNo(str3);
            recallByTranNo(str1, str2, str3);
        }
        else
        {
            System.out.println("你输入的参数：'" + paramArrayOfString[0] + "' 错误!");
        }
    }

    public void recallByTime(String paramString1, String paramString2, String paramString3)
    {
        String[] arrayOfString1 = new String[6];
        String[] arrayOfString2 = parsePosNo(paramString1);
        for (int i = 0; i < arrayOfString2.length; ++i)
        {
            arrayOfString1[0] = "SELECT * FROM posul_tranhead WHERE posNumber='" + arrayOfString2[i] + "' " + "AND systemDate BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            arrayOfString1[1] = "SELECT * FROM posul_trandtl WHERE posNumber='" + arrayOfString2[i] + "' " + "AND systemDate BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            arrayOfString1[2] = "SELECT * FROM posul_alipay_detail WHERE posNumber='" + arrayOfString2[i] + "' " + "AND systemDate BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            arrayOfString1[4] = "SELECT * FROM posul_weixin_detail WHERE posNumber='" + arrayOfString2[i] + "' " + "AND systemDate BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            arrayOfString1[5] = "SELECT * FROM posul_unionpay_detail WHERE posNumber='" + arrayOfString2[i] + "' " + "AND systemDate BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            arrayOfString1[3] = "SELECT * FROM posul_cmpay_detail WHERE posNumber='" + arrayOfString2[i] + "' " + "AND systemDate BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            executeInsert(arrayOfString2[i], arrayOfString1);
        }
    }

    public void recallByTranNo(String paramString1, String paramString2, String paramString3)
    {
        String[] arrayOfString1 = new String[6];
        String[] arrayOfString2 = parsePosNo(paramString1);
        for (int i = 0; i < arrayOfString2.length; ++i)
        {
            arrayOfString1[0] = "SELECT * FROM posul_tranhead WHERE posNumber='" + arrayOfString2[i] + "' " + "AND transactionNumber BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            arrayOfString1[1] = "SELECT * FROM posul_trandtl WHERE posNumber='" + arrayOfString2[i] + "' " + "AND transactionNumber BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            arrayOfString1[2] = "SELECT * FROM posul_alipay_detail WHERE posNumber='" + arrayOfString2[i] + "' " + "AND transactionNumber BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            arrayOfString1[4] = "SELECT * FROM posul_weixin_detail WHERE posNumber='" + arrayOfString2[i] + "' " + "AND transactionNumber BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            arrayOfString1[5] = "SELECT * FROM posul_unionpay_detail WHERE posNumber='" + arrayOfString2[i] + "' " + "AND transactionNumber BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            arrayOfString1[3] = "SELECT * FROM posul_cmpay_detail WHERE posNumber='" + arrayOfString2[i] + "' " + "AND transactionNumber BETWEEN '" + paramString2 + "' and " + "'" + paramString3 + "'";
            executeInsert(arrayOfString2[i], arrayOfString1);
        }
    }

    private void executeInsert(String paramString, String[] paramArrayOfString)
    {
        log.info("---------------开始恢复POS数据--------------");
        log.info("##1=" + paramArrayOfString[0]);
        log.info("##2=" + paramArrayOfString[1]);
        log.info("##2=" + paramArrayOfString[2]);
        log.info("##2=" + paramArrayOfString[3]);
        log.info("##2=" + paramArrayOfString[4]);
        log.info("##2=" + paramArrayOfString[5]);
        showProgressOnConsole(paramString);
        Connection localConnection1 = null;
        Statement localStatement1 = null;
        ResultSet localResultSet = null;
        ResultSetMetaData localResultSetMetaData = null;
        Connection localConnection2 = null;
        Statement localStatement2 = null;
        String str1 = "";
        String str2 = "";
        String str3 = "";
        int i = 0;
        int j = 0;
        int k = 0;
        int l = 0;
        int i1 = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int j1 = 0;
        int j2 = 0;
        int j3 = 0;
        int j4 = 0;
        int w1 = 0;
        int w2 = 0;
        int w3 = 0;
        int w4 = 0;
        int u1 = 0;
        int u2 = 0;
        int u3 = 0;
        int u4 = 0;
        int k1 = 0;
        int k2 = 0;
        int k3 = 0;
        int k4 = 0;
        try
        {
            int i7;
            String str4;
            Object localObject1;
            String str7;
            localConnection1 = Util.getSCConnection();
            localStatement1 = localConnection1.createStatement();
            localResultSet = localStatement1.executeQuery(paramArrayOfString[0]);
            localResultSetMetaData = localResultSet.getMetaData();
            localConnection2 = Util.getPOSConnection(paramString);
            localConnection2.setAutoCommit(false);
            localStatement2 = localConnection2.createStatement();
            int i6 = localResultSetMetaData.getColumnCount();
            while (localResultSet.next())
            {
                String str5;
                ++j;
                str2 = "INSERT INTO tranhead (";
                str3 = " VALUES(";
                i7 = 1;
                do
                {
                    localObject1 = localResultSet.getObject(i7);
                    str5 = localResultSetMetaData.getColumnName(i7);
                    str4 = (String)this.headFieldMap.get(str5);
                    if ((str4 == null) && (j == 1))
                        log.info("cake.tranhead." + str5 + "---- no corresponding field in cream.tranhead");
                    ++i7;
                }
                while (str4 == null);
                str2 = str2 + str4;
                if (localObject1 == null)
                    str3 = str3 + "NULL";
                else
                    str3 = str3 + "'" + localObject1.toString() + "'";
                while (i7 <= i6)
                {
                    localObject1 = localResultSet.getObject(i7);
                    str5 = localResultSetMetaData.getColumnName(i7);
                    str7 = (String)this.headFieldMap.get(str5);
                    if (str7 == null)
                    {
                        if (j == 1)
                            log.info("cake.tranhead." + str5 + "---- no corresponding field in cream.tranhead");
                    }
                    else
                    {
                        str2 = str2 + "," + str7;
                        if (localObject1 == null)
                            str3 = str3 + ",NULL";
                        else
                            str3 = str3 + ",'" + localObject1.toString() + "'";
                    }
                    ++i7;
                }
                str2 = str2 + ")";
                str3 = str3 + ");";
                str1 = str2 + str3;
                try
                {
                    localStatement2.executeUpdate(str1);
                    localConnection2.commit();
                    ++k;
                }
                catch (SQLException localSQLException2)
                {
                    localConnection2.rollback();
                    log.info("Insert sql wrong: " + str1);
                    if (localSQLException2.toString().indexOf("Duplicate entry") != -1
                            || localSQLException2.toString().indexOf("已经存在") != -1)
                        ++i1;
                    else
                        ++l;
                    log.info(localSQLException2.toString());
                }
            }
            localResultSet = localStatement1.executeQuery(paramArrayOfString[1]);
            localResultSetMetaData = localResultSet.getMetaData();
            str1 = "";
            str2 = "";
            str3 = "";
            i6 = localResultSetMetaData.getColumnCount();
            while (localResultSet.next())
            {
                String str6;
                ++i2;
                str2 = "INSERT INTO trandetail (";
                str3 = " VALUES(";
                i7 = 1;
                do
                {
                    localObject1 = localResultSet.getObject(i7);
                    str6 = localResultSetMetaData.getColumnName(i7);
                    str4 = (String)this.dtlFieldMap.get(str6);
                    if ((str4 == null) && (i2 == 1))
                        log.info("cake.trandtl." + str6 + " <----> no corresponding field in cream.trandetail");
                    ++i7;
                }
                while (str4 == null);
                str2 = str2 + str4;
                if (localObject1 == null)
                    str3 = str3 + "NULL";
                else
                    str3 = str3 + "'" + localObject1.toString() + "'";
                while (i7 <= i6)
                {
                    localObject1 = localResultSet.getObject(i7);
                    str6 = localResultSetMetaData.getColumnName(i7);
                    str7 = (String)this.dtlFieldMap.get(str6);
                    if (str7 == null)
                    {
                        if (i2 == 1)
                            log.info("cake.trandtl." + str6 + " <----> no corresponding field in cream.trandetail");
                    }
                    else
                    {
                        str2 = str2 + "," + str7;
                        if (localObject1 == null)
                            str3 = str3 + ",NULL";
                        else
                            str3 = str3 + ",'" + localObject1.toString() + "'";
                    }
                    ++i7;
                }
                str2 = str2 + ")";
                str3 = str3 + ");";
                str1 = str2 + str3;
                try
                {
                    localStatement2.executeUpdate(str1);
                    localConnection2.commit();
                    ++i3;
                }
                catch (SQLException localSQLException3)
                {
                    localConnection2.rollback();
                    log.info("Insert sql wrong: " + str1);
                    if (localSQLException3.toString().indexOf("Duplicate entry") != -1
                            || localSQLException3.toString().indexOf("已经存在") != -1)
                        ++i5;
                    else
                        ++i4;
                    log.info(localSQLException3.toString());
                }
            }
            localResultSet = localStatement1.executeQuery(paramArrayOfString[2]);
            localResultSetMetaData = localResultSet.getMetaData();
            str1 = "";
            str2 = "";
            str3 = "";
            i6 = localResultSetMetaData.getColumnCount();
            while (localResultSet.next())
            {
                String str6;
                ++j1;
                str2 = "INSERT INTO alipay_detail (";
                str3 = " VALUES(";
                i7 = 1;
                do
                {
                    localObject1 = localResultSet.getObject(i7);
                    str6 = localResultSetMetaData.getColumnName(i7);
                    str4 = (String)this.alipayDtlFieldMap.get(str6);
                    if ((str4 == null) && (j1 == 1))
                        log.info("cake.posul_alipay_detail." + str6 + " <----> no corresponding field in cream.alipay_detail");
                    ++i7;
                }
                while (str4 == null);
                str2 = str2 + str4;
                if (localObject1 == null)
                    str3 = str3 + "NULL";
                else
                    str3 = str3 + "'" + localObject1.toString() + "'";
                while (i7 <= i6)
                {
                    localObject1 = localResultSet.getObject(i7);
                    str6 = localResultSetMetaData.getColumnName(i7);
                    str7 = (String)this.alipayDtlFieldMap.get(str6);
                    if (str7 == null)
                    {
                        if (j1 == 1)
                            log.info("cake.posul_alipay_detail." + str6 + " <----> no corresponding field in cream.alipay_detail");
                    }
                    else
                    {
                        str2 = str2 + "," + str7;
                        if (localObject1 == null)
                            str3 = str3 + ",NULL";
                        else
                            str3 = str3 + ",'" + localObject1.toString() + "'";
                    }
                    ++i7;
                }
                str2 = str2 + ")";
                str3 = str3 + ");";
                str1 = str2 + str3;
                try
                {
                    localStatement2.executeUpdate(str1);
                    localConnection2.commit();
                    ++j2;
                }
                catch (SQLException localSQLException3)
                {
                    localConnection2.rollback();
                    log.info("Insert sql wrong: " + str1);
                    if (localSQLException3.toString().indexOf("Duplicate entry") != -1
                            || localSQLException3.toString().indexOf("已经存在") != -1)
                        ++j4;
                    else
                        ++j3;
                    log.info(localSQLException3.toString());
                }
            }
            localResultSet = localStatement1.executeQuery(paramArrayOfString[4]);
            localResultSetMetaData = localResultSet.getMetaData();
            str1 = "";
            str2 = "";
            str3 = "";
            i6 = localResultSetMetaData.getColumnCount();
            while (localResultSet.next())
            {
                String str6;
                ++w1;
                str2 = "INSERT INTO weixin_detail (";
                str3 = " VALUES(";
                i7 = 1;
                do
                {
                    localObject1 = localResultSet.getObject(i7);
                    str6 = localResultSetMetaData.getColumnName(i7);
                    str4 = (String)this.weixinDtlFieldMap.get(str6);
                    if ((str4 == null) && (w1 == 1))
                        log.info("cake.posul_weixin_detail." + str6 + " <----> no corresponding field in cream.weixin_detail");
                    ++i7;
                }
                while (str4 == null);
                str2 = str2 + str4;
                if (localObject1 == null)
                    str3 = str3 + "NULL";
                else
                    str3 = str3 + "'" + localObject1.toString() + "'";
                while (i7 <= i6)
                {
                    localObject1 = localResultSet.getObject(i7);
                    str6 = localResultSetMetaData.getColumnName(i7);
                    str7 = (String)this.weixinDtlFieldMap.get(str6);
                    if (str7 == null)
                    {
                        if (w1 == 1)
                            log.info("cake.posul_weixin_detail." + str6 + " <----> no corresponding field in cream.weixin_detail");
                    }
                    else
                    {
                        str2 = str2 + "," + str7;
                        if (localObject1 == null)
                            str3 = str3 + ",NULL";
                        else
                            str3 = str3 + ",'" + localObject1.toString() + "'";
                    }
                    ++i7;
                }
                str2 = str2 + ")";
                str3 = str3 + ");";
                str1 = str2 + str3;
                try
                {
                    localStatement2.executeUpdate(str1);
                    localConnection2.commit();
                    ++w2;
                }
                catch (SQLException localSQLException3)
                {
                    localConnection2.rollback();
                    log.info("Insert sql wrong: " + str1);
                    if (localSQLException3.toString().indexOf("Duplicate entry") != -1
                            || localSQLException3.toString().indexOf("已经存在") != -1)
                        ++w4;
                    else
                        ++w3;
                    log.info(localSQLException3.toString());
                }
            }
            localResultSet = localStatement1.executeQuery(paramArrayOfString[5]);
            localResultSetMetaData = localResultSet.getMetaData();
            str1 = "";
            str2 = "";
            str3 = "";
            i6 = localResultSetMetaData.getColumnCount();
            while (localResultSet.next())
            {
                String str6;
                ++u1;
                str2 = "INSERT INTO unionpay_detail (";
                str3 = " VALUES(";
                i7 = 1;
                do
                {
                    localObject1 = localResultSet.getObject(i7);
                    str6 = localResultSetMetaData.getColumnName(i7);
                    str4 = (String)this.unionpayDtlFieldMap.get(str6);
                    if ((str4 == null) && (u1 == 1))
                        log.info("cake.posul_unionpay_detail." + str6 + " <----> no corresponding field in cream.unionpay_detail");
                    ++i7;
                }
                while (str4 == null);
                str2 = str2 + str4;
                if (localObject1 == null)
                    str3 = str3 + "NULL";
                else
                    str3 = str3 + "'" + localObject1.toString() + "'";
                while (i7 <= i6)
                {
                    localObject1 = localResultSet.getObject(i7);
                    str6 = localResultSetMetaData.getColumnName(i7);
                    str7 = (String)this.unionpayDtlFieldMap.get(str6);
                    if (str7 == null)
                    {
                        if (u1 == 1)
                            log.info("cake.posul_unionpay_detail." + str6 + " <----> no corresponding field in cream.unionpay_detail");
                    }
                    else
                    {
                        str2 = str2 + "," + str7;
                        if (localObject1 == null)
                            str3 = str3 + ",NULL";
                        else
                            str3 = str3 + ",'" + localObject1.toString() + "'";
                    }
                    ++i7;
                }
                str2 = str2 + ")";
                str3 = str3 + ");";
                str1 = str2 + str3;
                try
                {
                    localStatement2.executeUpdate(str1);
                    localConnection2.commit();
                    ++u2;
                }
                catch (SQLException localSQLException3)
                {
                    localConnection2.rollback();
                    log.info("Insert sql wrong: " + str1);
                    if (localSQLException3.toString().indexOf("Duplicate entry") != -1
                            || localSQLException3.toString().indexOf("已经存在") != -1)
                        ++u4;
                    else
                        ++u3;
                    log.info(localSQLException3.toString());
                }
            }
            localResultSet = localStatement1.executeQuery(paramArrayOfString[3]);
            localResultSetMetaData = localResultSet.getMetaData();
            str1 = "";
            str2 = "";
            str3 = "";
            i6 = localResultSetMetaData.getColumnCount();
            while (localResultSet.next())
            {
                String str6;
                ++k1;
                str2 = "INSERT INTO cmpay_detail (";
                str3 = " VALUES(";
                i7 = 1;
                do
                {
                    localObject1 = localResultSet.getObject(i7);
                    str6 = localResultSetMetaData.getColumnName(i7);
                    str4 = (String)this.cmpayDtlFieldMap.get(str6);
                    if ((str4 == null) && (k1 == 1))
                        log.info("cake.posul_cmpay_detail." + str6 + " <----> no corresponding field in cream.cmpay_detail");
                    ++i7;
                }
                while (str4 == null);
                str2 = str2 + str4;
                if (localObject1 == null)
                    str3 = str3 + "NULL";
                else
                    str3 = str3 + "'" + localObject1.toString() + "'";
                while (i7 <= i6)
                {
                    localObject1 = localResultSet.getObject(i7);
                    str6 = localResultSetMetaData.getColumnName(i7);
                    str7 = (String)this.cmpayDtlFieldMap.get(str6);
                    if (str7 == null)
                    {
                        if (k1 == 1)
                            log.info("cake.posul_cmpay_detail." + str6 + " <----> no corresponding field in cream.cmpay_detail");
                    }
                    else
                    {
                        str2 = str2 + "," + str7;
                        if (localObject1 == null)
                            str3 = str3 + ",NULL";
                        else
                            str3 = str3 + ",'" + localObject1.toString() + "'";
                    }
                    ++i7;
                }
                str2 = str2 + ")";
                str3 = str3 + ");";
                str1 = str2 + str3;
                try
                {
                    localStatement2.executeUpdate(str1);
                    localConnection2.commit();
                    ++k2;
                }
                catch (SQLException localSQLException3)
                {
                    localConnection2.rollback();
                    log.info("Insert sql wrong: " + str1);
                    if (localSQLException3.toString().indexOf("Duplicate entry") != -1
                            || localSQLException3.toString().indexOf("已经存在") != -1)
                        ++k4;
                    else
                        ++k3;
                    log.info(localSQLException3.toString());
                }
            }
        }
        catch (SQLException localSQLException1)
        {
            System.out.println("  ->数据库操作时发生错误！");
            i = 1;
            log.info("SQLException：" + localSQLException1.toString());
        }
        catch (Exception localException)
        {
            System.out.println("  ->发生错误！");
            i = 1;
            log.info("Exception：" + localException.toString());
        }
        finally
        {
            if (localResultSet != null)
                localResultSet = null;
            if (localStatement1 != null)
                localStatement1 = null;
            if (localStatement2 != null)
                localStatement2 = null;
            if (localConnection1 != null)
                localConnection1 = null;
            if (localConnection2 != null)
                localConnection2 = null;
        }
        log.info("Total select " + j + " records from posul_tranhead.");
        log.info("Insert into cream.tranhead : " + k + " records.");
        log.info("Wrong records : " + l);
        log.info("Total select " + i2 + " records from posul_trandtl.");
        log.info("Insert into cream.trandetail : " + i3 + " records.");
        log.info("Wrong records : " + i4);
        log.info("Total select " + j1 + " records from posul_alipay_detail.");
        log.info("Insert into cream.alipay_detail : " + j2 + " records.");
        log.info("Wrong records : " + j3);
        log.info("Total select " + w1 + " records from posul_weixin_detail.");
        log.info("Insert into cream.weixin_detail : " + w2 + " records.");
        log.info("Wrong records : " + w3);
        log.info("Total select " + u1 + " records from posul_unionpay_detail.");
        log.info("Insert into cream.unionpay_detail : " + u2 + " records.");
        log.info("Wrong records : " + u3);
        log.info("Total select " + k1 + " records from posul_cmpay_detail.");
        log.info("Insert into cream.cmpay_detail : " + k2 + " records.");
        log.info("Wrong records : " + k3);
        if (i != 0)
        {
            System.out.println("   ->数据恢复中断.\n");
        }
        else
        {
            System.out.println("  ==>需要被恢复的交易表头共有：" + j + "笔.");
            System.out.println("   ->成功恢复：" + k + "笔.");
            System.out.println("   ->冲突记录：" + i1 + "笔.");
            System.out.println("   ->不能恢复：" + l + "笔.");
            System.out.println("  ==>需要被恢复的交易明细共有：" + i2 + "笔.");
            System.out.println("   ->成功恢复：" + i3 + "笔.");
            System.out.println("   ->冲突记录：" + i5 + "笔.");
            System.out.println("   ->不能恢复：" + i4 + "笔.");
            System.out.println("  ==>需要被恢复的支付宝明细共有：" + j1 + "笔.");
            System.out.println("   ->成功恢复：" + j2 + "笔.");
            System.out.println("   ->冲突记录：" + j4 + "笔.");
            System.out.println("   ->不能恢复：" + j3 + "笔.");
            System.out.println("  ==>需要被恢复的微信明细共有：" + w1 + "笔.");
            System.out.println("   ->成功恢复：" + w2 + "笔.");
            System.out.println("   ->冲突记录：" + w4 + "笔.");
            System.out.println("   ->不能恢复：" + w3 + "笔.");
            System.out.println("  ==>需要被恢复的银联明细共有：" + u1 + "笔.");
            System.out.println("   ->成功恢复：" + u2 + "笔.");
            System.out.println("   ->冲突记录：" + u4 + "笔.");
            System.out.println("   ->不能恢复：" + u3 + "笔.");
            System.out.println("  ==>需要被恢复的移动支付明细共有：" + k1 + "笔.");
            System.out.println("   ->成功恢复：" + k2 + "笔.");
            System.out.println("   ->冲突记录：" + k4 + "笔.");
            System.out.println("   ->不能恢复：" + k3 + "笔.");
            System.out.println("#POS" + paramString + "数据恢复完毕。\n");
        }
    }

    private void judgePosNo(String paramString)
    {
        if (!(validatePosNo(paramString)))
        {
            System.out.println("错误的POS机号: '" + paramString + "' !");
            System.out.println("POS机号格式应为：1 or 1-2");
            System.exit(0);
        }
    }

    private void judgeTranNo(String paramString)
    {
        if (!(validateInt(paramString)))
        {
            System.out.println("错误的交易序号：'" + paramString + "' !");
            System.out.println("交易序号格式应为：1 or 1-2");
            System.exit(0);
        }
    }

    private void judgeDate(String paramString)
    {
        if (!(validateDate(paramString)))
        {
            System.out.println("错误的日期格式：'" + paramString + "' !");
            System.out.println("日期格式应为：yyyy-MM-dd hh:mm:ss");
            System.exit(0);
        }
    }

    public static boolean validatePosNo(String paramString)
    {
        Object localObject;
        String str;
        boolean i = true;
        int j = paramString.indexOf("-");
        if (j > 0)
        {
            localObject = paramString.substring(0, j);
            str = paramString.substring(j + 1);
            if ((!(validateInt((String)localObject))) || (!(validateInt(str))))
                i = false;
            if (Integer.parseInt((String)localObject) >= Integer.parseInt(str))
                i = false;
        }
        else if (paramString.indexOf(",") > 0)
        {
            localObject = new StringTokenizer(paramString, ",");
            while (((StringTokenizer)localObject).hasMoreTokens())
            {
                str = ((StringTokenizer)localObject).nextToken();
                if (!(validateInt(str)))
                {
                    i = false;
                    break;
                }
            }
        }
        else if (!(validateInt(paramString)))
        {
            i = false;
        }
        return i;
    }

    public static boolean validateInt(String paramString)
    {
        boolean i = true;
        try
        {
            Integer.parseInt(paramString);
        }
        catch (Exception localException)
        {
            i = false;
        }
        return i;
    }

    public static boolean validateDate(String paramString)
    {
        boolean i = true;
        try
        {
            Timestamp.valueOf(paramString);
        }
        catch (Exception localException)
        {
            i = false;
        }
        return i;
    }

    public String[] parsePosNo(String paramString)
    {
        String[] arrayOfString;
        int i = paramString.indexOf("-");
        if (paramString.indexOf("-") > 0)
        {
            int j = Integer.parseInt(paramString.substring(0, i));
            int k = Integer.parseInt(paramString.substring(i + 1));
            if (j > k)
                k = j;
            arrayOfString = new String[k - j + 1];
            int l = 0;
            for (int i1 = j; i1 <= k; ++i1)
                arrayOfString[(l++)] = String.valueOf(i1);
        }
        else if (paramString.indexOf(",") > 0)
        {
            StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ",");
            ArrayList localArrayList = new ArrayList();
            while (localStringTokenizer.hasMoreTokens())
            {
                String str = localStringTokenizer.nextToken();
                localArrayList.add(str);
            }
            arrayOfString = new String[localArrayList.size()];
            localArrayList.toArray(arrayOfString);
        }
        else
        {
            arrayOfString = new String[1];
            arrayOfString[0] = paramString;
        }
        return arrayOfString;
    }

    private void showProgressOnConsole(String paramString)
    {
        System.out.print("#开始恢复POS" + paramString + "数据,请稍侯");
        int i = 0;
        try
        {
            for (int j = 0; j < 6; ++j)
            {
                System.out.print(".");
                Thread.sleep(500L);
            }
            if (i++ > 2)
            {
                System.out.print("\n");
                return;
            }
            System.out.print("\b\b\b\b\b\b");
            System.out.print("      ");
            System.out.print("\b\b\b\b\b\b");
        }
        catch (Exception localException)
        {
        }
    }

    public static void main(String[] paramArrayOfString)
    {
        Recall localRecall = new Recall();
        String[] arrayOfString = { "-2", "2", "3", "4" };
        localRecall.run(arrayOfString);
    }
}