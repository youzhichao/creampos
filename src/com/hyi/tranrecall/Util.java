package com.hyi.tranrecall;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import org.apache.log4j.Logger;

public class Util
{
    private static Properties properties = null;
    private static Logger log = Logger.getLogger(Util.class.getName());
    private static String SC_DRIVERS;
    private static String SC_URL;
    private static String SC_USER;
    private static String SC_PASSWORD;
    private static String POS_DRIVERS;
    private static String POS_URL;
    private static String POS_USER;
    private static String POS_PASSWORD;
    public static HashMap headFieldMap = new HashMap();
    public static HashMap dtlFieldMap = new HashMap();
    public static HashMap alipayDtlFieldMap = new HashMap();
    public static HashMap weixinDtlFieldMap = new HashMap();
    public static HashMap unionpayDtlFieldMap = new HashMap();
    public static HashMap cmpayDtlFieldMap = new HashMap();
    public static String[][] headField = { { "storeID", "STORENO" }, { "systemDate", "SYSDATE" }, { "posNumber", "POSNO" }, { "transactionNumber", "TMTRANSEQ" }, { "shiftCount", "SIGNONID" }, { "zSequenceNumber", "EODCNT" }, { "accountDate", "ACCDATE" }, { "transactionType", "TRANTYPE" }, { "dealType1", "DEALTYPE1" }, { "dealType2", "DEALTYPE2" }, { "dealType3", "DEALTYPE3" }, { "voidTransactionNumber", "VOIDSEQ" }, { "machineNumber", "TMCODEP" }, { "cashier", "CASHIER" }, { "invoiceHead", "INVNOHEAD" }, { "invoiceNumber", "INVNO" }, { "invoiceCount", "INVCNT" }, { "idNumber", "IDNO" }, { "detailCount", "DETAILCNT" }, { "customerCount", "CUSTCNT" }, { "inOurID", "INOUT" }, { "customerCategory", "CUSTID" }, { "saleMan", "SALEMAN" }, { "grossSaleTotalAmount", "GROSALAMT" }, { "grossSaleTax0Amount", "GROSALTX0AMT" }, { "grossSaleTax1Amount", "GROSALTX1AMT" }, { "grossSaleTax2Amount", "GROSALTX2AMT" }, { "grossSaleTax3Amount", "GROSALTX3AMT" }, { "grossSaleTax4Amount", "GROSALTX4AMT" }, { "siPlusTax1Amount", "SIPLUSAMT1" }, { "siPlusTax2Amount", "SIPLUSAMT2" }, { "siPlusTax3Amount", "SIPLUSAMT3" }, { "siPlusTax4Amount", "SIPLUSAMT4" }, { "discountTax0Amount", "SIPAMT0" }, { "discountTax1Amount", "SIPAMT1" }, { "discountTax2Amount", "SIPAMT2" }, { "discountTax3Amount", "SIPAMT3" }, { "discountTax4Amount", "SIPAMT4" }, { "mixAndMatchTax0Amount", "MNMAMT0" }, { "mixAndMatchTax1Amount", "MNMAMT1" }, { "mixAndMatchTax2Amount", "MNMAMT2" }, { "mixAndMatchTax3Amount", "MNMAMT3" }, { "mixAndMatchTax4Amount", "MNMAMT4" }, { "notIncludedTax0Sale", "RCPGIFAMT0" }, { "notIncludedTax1Sale", "RCPGIFAMT1" }, { "notIncludedTax2Sale", "RCPGIFAMT2" }, { "notIncludedTax3Sale", "RCPGIFAMT3" }, { "notIncludedTax4Sale", "RCPGIFAMT4" }, { "netSaleTax0Amount", "NETSALAMT0" }, { "netSaleTax1Amount", "NETSALAMT1" }, { "netSaleTax2Amount", "NETSALAMT2" }, { "netSaleTax3Amount", "NETSALAMT3" }, { "netSaleTax4Amount", "NETSALAMT4" }, { "daiShouAmount", "DAISHOUAMT" }, { "daiShouAmount2", "DAISHOUAMT2" }, { "daiFuAmount", "DAIFUAMT" }, { "payID1", "PAYNO1" }, { "payID2", "PAYNO2" }, { "payID3", "PAYNO3" }, { "payID4", "PAYNO4" }, { "payAmount1", "PAYAMT1" }, { "payAmount2", "PAYAMT2" }, { "payAmount3", "PAYAMT3" }, { "payAmount4", "PAYAMT4" }, { "tax0Amount", "TAXAMT0" }, { "tax1Amount", "TAXAMT1" }, { "tax2Amount", "TAXAMT2" }, { "tax3Amount", "TAXAMT3" }, { "tax4Amount", "TAXAMT4" }, { "changeAmount", "CHANGEAMT" }, { "spillAmount", "OVERAMT" }, { "exchangeDifference", "CHANGAMT" }, { "creditCardType", "CRDTYPE" }, { "creditCardNumber", "CRDNO" }, { "creditCardExpireDate", "CRDEND" }, { "employeeNumber", "EMPNO" }, { "memberID", "MEMBERID" }, { "annotatedID", "ANNOTATEDID" }, { "annotatedType", "ANNOTATEDTYPE" }, { "rebateChangeAmount", "rebateChangeAmount" }, { "rebateAmount", "rebateAmount" }, { "totalRebateAmount", "totalRebateAmount" } };
    public static String[][] dtlField = { { "posNumber", "TMCODE" }, { "transactionNumber", "TMTRANSEQ" }, { "lineItemSequence", "ITEMSEQ" }, { "detailCode", "CODETX" }, { "itemVoid", "ITEMVOID" }, { "categoryNumber", "CATNO" }, { "midCategoryNumber", "MIDCATNO" }, { "microCategoryNumber", "MICROCATNO" }, { "thinCategoryNumber", "THINCATNO" }, { "pluNumber", "PLUNO" }, { "unitPrice", "UNITPRICE" }, { "quantity", "QTY" }, { "weight", "WEIGHT" }, { "discountID", "DISCNO" }, { "amount", "AMT" }, { "taxID", "TAXTYPE" }, { "taxAmount", "TAXAMT" }, { "itemNumber", "ITEMNO" }, { "originalPrice", "ORIGPRICE" }, { "discountType", "DISCTYPE" }, { "afterDiscountAmount", "AFTDSCAMT" }, { "discountRateID", "discountRateID" }, { "discountRate", "discountRate" }, { "rebateAmount", "rebateAmount" } };

    public static void loadProperties()
    {
        FileInputStream localFileInputStream;
        try
        {
            localFileInputStream = new FileInputStream(TransactionRecall.TRECALL_CONF_APP__FILE);
            properties = new Properties();
            properties.load(localFileInputStream);
            log.info("Load: " + TransactionRecall.TRECALL_CONF_APP__FILE);
        }
        catch (Exception localException)
        {
            log.error("Unable to load file:" + TransactionRecall.TRECALL_CONF_APP__FILE, localException);
            properties = null;
        }
        SC_DRIVERS = properties.getProperty("sc.drivers");
        SC_URL = properties.getProperty("sc.url");
        SC_USER = properties.getProperty("sc.user");
        SC_PASSWORD = properties.getProperty("sc.password");
        log.info("SC_DRIVERS  : " + SC_DRIVERS);
        log.info("SC_URL      : " + SC_URL);
        log.info("SC_USER     : " + SC_USER);
        log.info("SC_PASSWORD : " + SC_PASSWORD);
//        POS_DRIVERS = properties.getProperty("pos.drivers");
//        POS_URL = properties.getProperty("pos.url");
//        POS_USER = properties.getProperty("pos.user");
//        POS_PASSWORD = properties.getProperty("pos.password");
//        log.info("POS_DRIVERS : " + POS_DRIVERS);
//        log.info("POS_URL     : " + POS_URL);
//        log.info("POS_USER    : " + POS_USER);
//        log.info("POS_PASSWORD: " + POS_PASSWORD);
        File localFile1 = new File(TransactionRecall.TRECALL_CONF_TRANHEAD_FILE);
        File localFile2 = new File(TransactionRecall.TRECALL_CONF_TRANDTL_FILE);
        File localFile3 = new File(TransactionRecall.TRECALL_CONF_ALIPAYDTL_FILE);
        File localFile5 = new File(TransactionRecall.TRECALL_CONF_WEIXINDTL_FILE);
        File localFile6 = new File(TransactionRecall.TRECALL_CONF_UNIONPAYDTL_FILE);
        File localFile4 = new File(TransactionRecall.TRECALL_CONF_CMPAYDTL_FILE);
        headFieldMap = loadConfFile(localFile1);
        dtlFieldMap = loadConfFile(localFile2);
        alipayDtlFieldMap = loadConfFile(localFile3);
        weixinDtlFieldMap = loadConfFile(localFile5);
        unionpayDtlFieldMap = loadConfFile(localFile6);
        cmpayDtlFieldMap = loadConfFile(localFile4);
    }

    public static String getProperty(String paramString)
    {
        if (properties != null)
            return properties.getProperty(paramString);
        return null;
    }

    public static Connection getSCConnection()
    {
        Connection localConnection = null;
        try
        {
            Class.forName(SC_DRIVERS).newInstance();
            localConnection = DriverManager.getConnection(SC_URL, SC_USER, SC_PASSWORD);
        }
        catch (ClassNotFoundException localClassNotFoundException)
        {
            log.error("ClassNotFoundException in getSCConnection, can not get connection!", localClassNotFoundException);
        }
        catch (InstantiationException localInstantiationException)
        {
            log.error("InstantiationException in getSCConnection, can not get connection!", localInstantiationException);
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
            log.error("IllegalAccessException in getSCConnection, can not get connection!", localIllegalAccessException);
        }
        catch (SQLException localSQLException)
        {
            log.error("SQLException in getSCConnection, can not get connection!", localSQLException);
        }
        return localConnection;
    }

    public static Connection getPOSConnection(String paramString)
    {
        System.out.println("posNo: " + paramString);
        Connection localConnection = null;
        String str = "";
        try
        {
            POS_DRIVERS = properties.getProperty("pos" + paramString + ".drivers");
            POS_URL = properties.getProperty("pos" + paramString + ".url");
            POS_USER = properties.getProperty("pos" + paramString + ".user");
            POS_PASSWORD = properties.getProperty("pos" + paramString + ".password");
            log.info("POS_DRIVERS : " + POS_DRIVERS);
            log.info("POS_URL     : " + POS_URL);
            log.info("POS_USER    : " + POS_USER);
            log.info("POS_PASSWORD: " + POS_PASSWORD);
            Class.forName(POS_DRIVERS).newInstance();
            localConnection = DriverManager.getConnection(POS_URL, POS_USER, POS_PASSWORD);
        }
        catch (ClassNotFoundException localClassNotFoundException)
        {
            log.error("ClassNotFoundException in getPOSConnection, can not get connection!", localClassNotFoundException);
        }
        catch (InstantiationException localInstantiationException)
        {
            log.error("InstantiationException in getPOSConnection, can not get connection!", localInstantiationException);
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
            log.error("IllegalAccessException in getPOSConnection, can not get connection!", localIllegalAccessException);
        }
        catch (SQLException localSQLException)
        {
            log.error("SQLException in getPOSConnection, can not get connection!", localSQLException);
        }
        return localConnection;
    }

    public static HashMap loadConfFile(File paramFile)
    {
        HashMap localHashMap = new HashMap();
        Properties localProperties = new Properties();
        try
        {
            FileInputStream localFileInputStream = new FileInputStream(paramFile);
            localProperties.load(localFileInputStream);
            Enumeration localEnumeration = localProperties.keys();
            int i = 0;
            while (localEnumeration.hasMoreElements())
            {
                ++i;
                String str1 = (String)localEnumeration.nextElement();
                String str2 = localProperties.getProperty(str1);
                localHashMap.put(str1, str2);
            }
            log.info("Finish load " + paramFile.toString());
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            log.error(paramFile.toString() + " not found!", localFileNotFoundException);
        }
        catch (IOException localIOException)
        {
            log.error("Error in reading " + paramFile.toString(), localIOException);
        }
        return localHashMap;
    }

    public static void main(String[] paramArrayOfString)
    {
        String str1;
        try
        {
            str1 = "pos1.cstore.com.cn";
            String str2 = InetAddress.getByName(str1).toString();
            str2 = str2.substring(str2.indexOf("/") + 1);
            System.out.println("@@posIP=" + str2);
        }
        catch (Exception localException)
        {
            localException.printStackTrace();
        }
    }
}