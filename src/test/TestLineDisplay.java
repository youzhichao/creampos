package test;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;

import javax.comm.*;

public class TestLineDisplay {
    Enumeration portList;
    CommPortIdentifier portId;
    SerialPort serialPort;
    OutputStream outputStream;
	public void init(String comPort) {
        portList = CommPortIdentifier.getPortIdentifiers();
        while (portList.hasMoreElements()) {
            portId = (CommPortIdentifier) portList.nextElement();
            System.out.println("CD7220: Iterating " + portId.getName());
            //System.out.println("portlist = " + portId + " at " + this);
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL
              && portId.getName().equals(comPort)) {
//                && portId.getName().equals((String)entry.getPropertyValue("DevicePortName"))) {
                //System.out.println("find port at " + (String)entry.getPropertyValue("DevicePortName"));
                if (portId.isCurrentlyOwned()) {
                	System.out.println("lineDisplay error : The device is being use!");
                    return;
                }

                System.out.println("CD7220: Open " + portId.getName());
                try {
                    serialPort = (SerialPort)portId.open("CD7220", 10000);
                    System.out.println("CD7220: open success ");
                } catch (PortInUseException e1) {
                	e1.printStackTrace();
                	System.out.println("Port in use: " + portId + ", at " + this);
                    break;
                }
                try {
                    outputStream = serialPort.getOutputStream();
                    System.out.println("CD7220: getOutputStream ");
                } catch (IOException e2) {
                    e2.printStackTrace();
                    System.out.println("IO exception at " + this);
                    break;
                }
                break;
            }
        }
        try {
            serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,
                                                 SerialPort.STOPBITS_1,
                                                 SerialPort.PARITY_NONE);
//            SerialPort.PARITY_MARK
            System.out.println("CD7220 : getFlowControlMode : " + serialPort.getFlowControlMode());
            System.out.println("CD7220: setSerialPortParams ");
        } catch (UnsupportedCommOperationException e3) {
            e3.printStackTrace();
            System.out.println("Unsupported comm operation at " + this);
            return;
        }

        try {
            outputStream.write(27);     //initialize display
            outputStream.write(64);
            outputStream.write(12);
	        try {
	        	Thread.sleep(1000);
	        } catch (Exception e) {
	        	
	        }
//            outputStream.write(18);
            
//            outputStream.write(27);
//            outputStream.write(59);
//            outputStream.write(1);
            
//            outputStream.write(27);     //set overwrite mode
//            outputStream.write(17);
//	        outputStream.flush();

            System.out.println("CD7220: init linedisplay .....");
	        outputStream.write(27);
	        outputStream.write(115);
	        outputStream.write(1);
	        outputStream.write(1);
	        try {
	        	Thread.sleep(1000);
	        } catch (Exception e) {
	        }
	        outputStream.flush();
	        System.out.println("CD7220: flush ");
	        outputStream.write(27);
	        outputStream.write(81);
	        outputStream.write(65);
	        try {
	        	Thread.sleep(100);
	        } catch (Exception e) {
	        }
	        outputStream.write(0x30);
	        outputStream.write(0x31);
	        outputStream.write(0x32);
	        outputStream.write(50);
	        outputStream.write('.');
//	        outputStream.write(31);
//	        outputStream.write(3);
	        outputStream.write(13);
	        outputStream.flush();
	        try {
	        	Thread.sleep(1000);
	        } catch (Exception e) {
	        }
//	        System.out.println("CD7220: flush 2");
	        
            
            /*outputStream.write(27);      //set cursor ON
            outputStream.write(95);
            outputStream.write(1);*/
        } catch (IOException e4) {
            e4.printStackTrace();
            System.out.println("IO exception at " + this);
        }

	}
	public static void main(String[] args) {
		TestLineDisplay display = new TestLineDisplay();
		System.out.println("---- comPort : " + args[0]);
		display.init(args[0]);
	}

}
