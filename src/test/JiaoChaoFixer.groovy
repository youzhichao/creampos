package tw.chaozhuyin.util

import groovy.sql.Sql

/**
 * 调查教超帐不平问题。
 *
 * @author Bruce You
 * @since 2018/10/16
 */

//@GrabConfig(systemClassLoader=true)
//@Grab(group='org.postgresql', module='postgresql', version='9.4-1205-jdbc42')
def db = Sql.newInstance('jdbc:postgresql:cream_jiaochao_pos1', 'org.postgresql.Driver')

def payIdToAmountMap = [:]
TreeMap cashTrans = [:]
def cashTranIds = []
def sql = "select * from tranhead where accdate='2018-10-13'"
db.eachRow(sql) { tranhead ->
    boolean cash = false
    (1..4).each { idx ->
        def payId = tranhead."payno$idx"
        if (payId == '00')
            cash = true
        if (payIdToAmountMap[payId] == null)
            payIdToAmountMap[payId] = 0.0
        BigDecimal payAmount
        if (cash)
            payAmount = tranhead."payamt$idx" - tranhead.changeamt
        else
            payAmount = tranhead."payamt$idx"
        payIdToAmountMap[payId] += payAmount
    }
    if (cash) {
        cashTranIds << tranhead.tmtranseq
        cashTrans[tranhead.tmtranseq] = tranhead
        tranhead.with {
            println "$tmtranseq, $netsalamt, $payno1, $payno2, $payamt1, $payamt2, $changeamt"
        }
    }
}

println "现金支付交易: $cashTranIds"
println "tranhead.现金支付金额=${payIdToAmountMap['00']}"
println "tranhead.现金支付笔数=${cashTranIds.size()}"

db.eachRow('select * from z where eodcnt=0') { z ->
    println "z.现金支付金额=${z.pay00amt}"
    println "z.现金支付笔数=${z.pay00cnt}"
}


def cashTransString = cashTranIds.toString().replaceAll("\\[", '').replaceAll(']', '')
println cashTransString

def totalCash = 0.0
sql = "select * from trandetail where tmtranseq=3043034".toString()
db.eachRow(sql) { row ->
    //totalCash += row.amt
    println row
}
//println "detail totalCash=$totalCash"

db.close()
