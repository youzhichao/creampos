package test;

import java.util.zip.CRC32;

public class Test {
	public static void valid(String barCode) {
		System.out.println(10 % 8);
	}
	
	public static void checkSum(String num) {
        byte[] content = num.getBytes();
        CRC32 c = new CRC32();
        c.update(content);
        System.out.println(num + " | " + c.getValue());
	}
	
	public static void main(String[] args) {
		checkSum("1");
		checkSum("2");
		checkSum("3");
		checkSum("1000");
		checkSum("1001");
		checkSum("589001");
		checkSum("589002");
		checkSum("589003");
		checkSum("589011");
		checkSum("589011");
	}
}
