#!/bin/sh

SRC_DIR=./src
CLASS_FILE_DIR=./out/production/CreamPOS_trunk
#GWT_USR_JAR=~/Java/gwt-linux-1.5.3/gwt-user.jar
#GWT_DEV_JAR=~/Java/gwt-linux-1.5.3/gwt-dev-linux.jar
GWT_USR_JAR=~/Java/gwt-mac-1.5.1/gwt-user.jar
GWT_DEV_JAR=~/Java/gwt-mac-1.5.1/gwt-dev-mac.jar
GWTEXT_JAR=lib/gwtext.jar
GWTEXTUX_JAR=lib/gwtextux.jar
#SMARTGWT_JAR=lib/smartgwt.jar
#SMARTGWT_SKINS_JAR=lib/smartgwt-skins.jar

echo $CLASS_FILE_DIR
CLASS_PATH=$SRC_DIR:$CLASS_FILE_DIR:$GWT_USR_JAR:$GWT_DEV_JAR:$GWTEXT_JAR:$GWTEXTUX_JAR
#CLASS_PATH=$SRC_DIR:$CLASS_FILE_DIR:$GWT_USR_JAR:$GWT_DEV_JAR:$SMARTGWT_JAR:$SMARTGWT_SKINS_JAR
echo $CLASS_PATH
java -Xmx256M -classpath "$CLASS_PATH" \
   com.google.gwt.dev.GWTCompiler -out "./www" "$@" hyi.cream.gwt.CreamGWT
